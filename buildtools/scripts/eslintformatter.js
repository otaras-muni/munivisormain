module.exports = function(results) {
  var results = results || []

  const summary = results.reduce(
    (seq, current) => {
      current.messages.forEach((msg) => {
        const logMessage = {
          filePath: current.filePath,
          ruleId: msg.ruleId,
          message: msg.message,
          line: msg.line,
          column: msg.column
        }

        if (msg.severity === 1) {
          logMessage.type = "warning"
          seq.warnings.push(logMessage)
        }
        if (msg.severity === 2) {
          logMessage.type = "error"
          seq.errors.push(logMessage)
        }
      })
      return seq
    },
    {
      errors: [],
      warnings: []
    }
  )

  if (summary.errors.length > 0 || summary.warnings.length > 0) {
    const lines = summary.errors
      .concat(summary.warnings)
      .map((msg) => (
        `\n${ 
          msg.type 
        } ${ 
          msg.ruleId 
        }\n  ${ 
          msg.filePath 
        }:${ 
          msg.line 
        }:${ 
          msg.column}`
      ))
      .join("\n")

    return `${lines  }\n`
  }
}
