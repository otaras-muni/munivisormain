const path = require("path")
const webpack = require("webpack")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin") // installed via npm
// const CleanWebpackPlugin = require("clean-webpack-plugin")
const fs = require("fs")
const Dotenv = require("dotenv-webpack")

const approot = fs.realpathSync(process.cwd())
const HtmlwebpackHarddiskPlugin = require("html-webpack-harddisk-plugin")
const dotenv = require("dotenv")

const env = dotenv.config().parsed
  
// reduce it to a nice object, the same as before
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next])
  return prev
}, {})

console.log(JSON.stringify(envKeys,null,2))

// Clean and Regenerate Files const filesToClean = ["dist"]

const clientConfig = {
  mode: "development",
  devtool: "source-map", // any "source-map"-like devtool is possible
  node: {
    fs: "empty"
  },
  resolve: {
    alias: {
      AppComp: path.resolve(approot, "client/src/app/FunctionalComponents"),
      Global: path.resolve(approot, "client/src/app/GlobalComponents"),
      GlobalUtils: path.resolve(approot, "client/src/globalutilities"),
      AppState: path.resolve(approot, "client/src/app/StateManagement"),
      Auth: path.resolve(approot, "client/src/app/LoginAuthentication"),
      Validation: path.resolve(approot, "client/src/app/Validations"),
      TranRFP: path.resolve(approot, "client/src/app/FunctionalComponents/TransactionRFP"),
      TranDeals: path.resolve(approot, "client/src/app/FunctionalComponents/TransactionDeals"),
      Task: path.resolve(approot, "client/src/app/FunctionalComponents/TaskManagement"),
      CRM: path.resolve(approot, "client/src/app/FunctionalComponents/Relationships"),
      Platform: path.resolve(approot, "client/src/app/FunctionalComponents/PlatformManagement"),
      Tenant: path.resolve(approot, "client/src/app/FunctionalComponents/TenantConfiguration"),
      Admin: path.resolve(approot, "client/src/app/FunctionalComponents/AdminManagement")
    }
  },
  entry: [
    // "babel-polyfill", // "webpack-dev-server/client", //
    // "react-hot-loader/patch", // "webpack/hot/only-dev-server",
    "webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true",
    path.resolve(approot, "client/src/index.js"),
    path.resolve(approot, "client/src/scss/main.scss")
  ],
  output: {
    path: path.resolve(approot, "dist"),
    filename:"[name]_[hash]_[id].js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      }, {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use: ["css-loader"]})
      }, {
        test: /\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      }, {
        test: /\.(ttf|eot|woff|woff2|svg|otf)?(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "fonts/[name].[ext]"
        }
      }, {
        test: /\.(jpg|png|svg|gif|ico)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "images/[name].[ext]"
          }
        }
      },
      // {
      //   test: /\.(ico)$/,
      //   use: {
      //     loader: "file-loader",
      //     options: {
      //       name: "[name].[ext]"
      //     }
      //   }
      // }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(approot, "client/src/public/index.html"),
      // favicon:path.resolve(approot, "client/src/public/favicon.ico"),
      alwaysWriteToDisk: true
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    //    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({filename: "bundled.[hash].css", disable: false, allChunks: true}),
    // new CleanWebpackPlugin(filesToClean),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlwebpackHarddiskPlugin({
      outputPath: path.resolve(approot, "dist")
    }),
    new webpack.DefinePlugin(envKeys),
    new Dotenv(),
    /*     new BrowserSyncPlugin({
      host: "localhost",
      port: 3001,
      server: { baseDir: ["dist"] }
    }) */
  ]
}

module.exports = clientConfig
