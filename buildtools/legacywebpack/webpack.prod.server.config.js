const path = require("path")
const webpack = require("webpack")
const UglifyJSPlugin = require("uglifyjs-webpack-plugin")

// Clean and Regenerate Files

const buildDir = path.resolve(__dirname, "build")

const serverConfig = {
  entry: ["./server/serversrc/index"],
  watch: false,
  devtool: "sourcemap",
  mode: "production",
  target: "node",
  node: {
    __filename: true,
    __dirname: true
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              babelrc: false,
              presets: [
                [
                  "env", {
                    modules: false
                  }
                ],
                "stage-0"
              ],
              plugins: ["transform-regenerator", "transform-runtime"]
            }
          }
        ],
        exclude: /node_modules/
      }, {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        use: {
          loader: "raw-loader"
        }
      }
    ]
  },
  // optimization: {
  //   minimizer: [
  //     new UglifyJSPlugin({
  //       uglifyOptions: {
  //         compress: {
  //           drop_console: true
  //         },
  //         output: {
  //           comments: false
  //         }
  //       },
  //     }),
  //   ],
  // },
  plugins: [
    new webpack.NamedModulesPlugin(),
    // new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      "process.env": {
        BUILD_TARGET: JSON.stringify("server")
      }
    }),
    // new UglifyJSPlugin(),
    // new webpack.BannerPlugin({banner: "require(\"source-map-support\").install();", raw: true, entryOnly: false})
  ],
  output: {
    path: buildDir,
    filename: "server.js"
  }
}

module.exports = serverConfig
