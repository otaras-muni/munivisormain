const path = require("path")
const webpack = require("webpack")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin") // installed via npm
const CleanWebpackPlugin = require("clean-webpack-plugin")
const UglifyJSPlugin = require("uglifyjs-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CompressionPlugin = require("compression-webpack-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const cssnano = require("cssnano")
const Dotenv = require("dotenv-webpack")
// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin

// Clean and Regenerate Files

const buildDir = path.resolve(__dirname, "build/client")
const filesToClean = ["build/client"]

const clientConfig = {
  mode: "production",
  entry: [
    path.resolve(__dirname, "client/src/index.js"),
    path.resolve(__dirname, "client/src/scss/main.scss")
  ],
  resolve: {
    alias: {
      AppComp: path.resolve(__dirname, "client/src/app/FunctionalComponents"),
      Global: path.resolve(__dirname, "client/src/app/GlobalComponents"),
      GlobalUtils: path.resolve(__dirname, "client/src/globalutilities"),
      AppState: path.resolve(__dirname, "client/src/app/StateManagement"),
      Auth: path.resolve(__dirname, "client/src/app/LoginAuthentication"),
      Validation: path.resolve(__dirname, "client/src/app/Validations"),
      TranRFP: path.resolve(__dirname, "client/src/app/FunctionalComponents/TransactionRFP"),
      TranDeals: path.resolve(__dirname, "client/src/app/FunctionalComponents/TransactionDeals"),
      Task: path.resolve(__dirname, "client/src/app/FunctionalComponents/TaskManagement"),
      CRM: path.resolve(__dirname, "client/src/app/FunctionalComponents/Relationships"),
      Platform: path.resolve(__dirname, "client/src/app/FunctionalComponents/PlatformManagement"),
      Tenant: path.resolve(__dirname, "client/src/app/FunctionalComponents/TenantConfiguration"),
      Admin: path.resolve(__dirname, "client/src/app/FunctionalComponents/AdminManagement")
    }
  },
  output: {
    path: buildDir,
    filename:"[name]_[contenthash]_[id].js",
    publicPath: "/"
  },
  
  // optimization: {
  //   minimizer: [
  //     new UglifyJSPlugin({
  //       uglifyOptions: {
  //         compress: {
  //           drop_console: true
  //         },
  //         output: {
  //           comments: false
  //         }
  //       },
  //     }),
  //   ],
  // },
  // optimization: {
  //   splitChunks: {
  //     chunks: "initial"
  //   },
  //   // minimizer: [
  //   //   new UglifyJSPlugin({
  //   //     parallel: true,
  //   //     cache: path.resolve(__dirname, "webpack-cache/uglify-cache"),
  //   //     test: /\.js($|\?)/i,
  //   //     uglifyOptions: {
  //   //       compress: false,
  //   //       output: {
  //   //         comments: false,
  //   //       }
  //   //     }
  //   //   })
  //   // ]
  // },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          babelrc: false,
          presets: [[ "env", { modules: false }], "react", "stage-0","stage-1"],
          plugins: [
            "react-hot-loader/babel",
            ["transform-runtime", {
              polyfill: false,
              regenerator: true
            }],
            [
              "direct-import",
              [
                "@appbaseio/reactivesearch",
                {
                  name: "@appbaseio/reactivesearch",
                  indexFile: "@appbaseio/reactivesearch/lib/index.es.js"
                }
              ]
            ]
          ]
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use: ["css-loader"]})
      },
      {
        test: /\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg|otf)?(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "fonts/[name].[ext]"
        }
      }, {
        test: /\.(jpg|png|svg|gif|ico)$/,
        use: {
          loader: "file-loader?name=[name].[ext]",
          options: {
            name: "images/[name].[ext]"
          }
        }
      }
    ]
  },
  plugins: [
    // new webpack.optimize.AggressiveMergingPlugin(), // Merge chunks
    // new BundleAnalyzerPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new CompressionPlugin({asset: "[path].gz[query]", algorithm: "gzip", test: /\.js$|\.css$|\.html$/, threshold: 10240, minRatio: 0.8}),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "client/src/public/index.html"),
      // favicon: path.resolve(__dirname, "client/src/public/favicon.ico"),
      inject: true
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new ExtractTextPlugin({filename: "bundled.[hash].css", disable: false, allChunks: true}),
    new CleanWebpackPlugin(filesToClean),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    }),
    new OptimizeCSSAssetsPlugin({
      cssProcessor: cssnano,
      cssProcessorOptions: {
        discardComments: {
          removeAll: true,
        },
        // Run cssnano in safe mode to avoid
        // potentially unsafe transformations.
        safe: true,
      },
      canPrint: false,
    }),
    new Dotenv(),
  ]
}

module.exports = clientConfig
