import time
import argparse
import json
import boto3


class SyncWithS3:
    def __init__(self):
        # Command line arguments
        parser = argparse.ArgumentParser()
        parser.add_argument("--asg_name", required=True,
                            help="Please specify Auto Scale Group name. ")
        parser.add_argument("--instance_id", required=True,
                            help="Please specify the instance id. ")
        args = vars(parser.parse_args())

        # Derived variables
        self.region = "us-east-1"
        self.s3_bucket_name = "munivisor-sync"
        self.instance_id = args["instance_id"]

        self.json_file_name = '{}.json'.format(args["asg_name"])
        self.is_file_uploaded_to_s3 = False
        self.common_dict = {}

    def _copy_dict_to_s3(self, s3_obj):
        """
        - copy dictionary content directly into the s3 bucket bucket as json file
        :return:
        """
        try:

            obj = s3_obj.Object(self.s3_bucket_name, self.json_file_name)
            output = obj.put(Body=json.dumps(self.common_dict))
            if output['ResponseMetadata']['HTTPStatusCode'] == 200:
                self.is_file_uploaded_to_s3 = True
        except Exception as error:
            print("Exception in copy_to_s3:",error)
            
    def _read_dict_from_s3(self, s3_obj):
        """
        - reads dictionary content directly from the json file in s3 bucket 
        :return:
        """
        try:

            obj = s3_obj.Object(self.s3_bucket_name, self.json_file_name)
            output = obj.get()
            if output['ResponseMetadata']['HTTPStatusCode'] == 200:
                print("File read successful.")
                self.common_dict = json.load(output["Body"])
        except Exception as error:
            print("Exception in read_from_s3:",error)

    def _update_dict(self):
        """
        - Update the dict for current time and current winner.
        :return:
        """
        try:
            print("Updating the time_stamp and setting winner")
            #Update the check-in time.
            self.common_dict[self.instance_id] = int(time.time())

            #check all the instanceIds and find winner
            winner = self.instance_id
            for k in self.common_dict.keys():
                if k == 'Winner':
                    continue
                if k < winner:
                    if (self.common_dict[self.instance_id] - self.common_dict[k]) < 172800:
                        winner = k
            self.common_dict["Winner"] = winner
        except Exception as error:
            print("Exception in update_dict:",error)
            
    def main(self):
        """
        Connects to aws using boto
        """
        try:
            s3 = boto3.resource('s3', region_name=self.region)
            self._read_dict_from_s3(s3)
            self._update_dict()
            self._copy_dict_to_s3(s3)
            #sleep for 5 seconds, for other nodes to have time
            time.sleep(5)
            self._read_dict_from_s3(s3)
            #check_winner
            if self.common_dict["Winner"] == self.instance_id:
                print("We run the command here!")
                return(0)
            else:
                print("Some other instance will run the command.")
                return(1)
        except Exception as error:
            print("Exception in main:",error)
            return(1)


if __name__ == '__main__':
    exit(SyncWithS3().main())
