#!/bin/bash

tmpFileName="/tmp/aws-instance-info"
CURRENT_DIR=`pwd`
if [ -s $tmpFileName ]; then
        region=$( /bin/grep "Region" $tmpFileName | /usr/bin/cut -d " " -f 2 )
        instanceID=$( /bin/grep "InstanceID" $tmpFileName | /usr/bin/cut -d " " -f 2 )
        autoscaleName=$( /bin/grep "AutoScaleName" $tmpFileName | /usr/bin/cut -d " " -f 2 )
else
        avZone=$( /usr/bin/curl http://169.254.169.254/latest/meta-data/placement/availability-zone )
        instanceID=$( /usr/bin/curl http://169.254.169.254/latest/meta-data/instance-id )

        region=$( /bin/echo ${avZone%?} )
        autoscaleName=$( /usr/local/bin/aws ec2 describe-tags --region $region --filters "Name=resource-type,Values=instance" "Name=resource-id,Values=$instanceID" "Name=key,Values=aws:autoscaling:groupName" | /bin/grep "Value" | /usr/bin/tr -s " " | /usr/bin/cut -d " " -f 3 | /bin/sed 's/\"//g' | /bin/sed 's/,//' )

        /bin/echo "Region: $region" > $tmpFileName
        /bin/echo "InstanceID: $instanceID" >> $tmpFileName
        /bin/echo "AutoScaleName: $autoscaleName" >> $tmpFileName
fi

if [ ! -z "$region" ] && [ ! -z "$autoscaleName" ]; then
        /usr/bin/python $CURRENT_DIR/elect_in_asg.py   --asg_name $autoscaleName --instance_id $instanceID
        if [ $? -eq 0 ]; then
            exit 0
        fi
        exit 1
fi
