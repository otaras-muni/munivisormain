import {
  setupTenantIndex,
  deleteTenantIndex
} from "../serversrc/api/elasticsearch/esHelper"

const main = async () => {
  try {
    const sampleTenantId = "abc123"

    const createRes = await setupTenantIndex(sampleTenantId)
    console.log("Create Index Res", createRes)

    setTimeout(async () => {
      const deleteIndexRes = await deleteTenantIndex(sampleTenantId)
      console.log("Delete Index Res", deleteIndexRes)
    }, 5000)
  } catch (err) {
    console.error("Err", err)
  }
}

main()
