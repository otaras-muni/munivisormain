import express from "express"
import { authRouter } from "./authsessionmanagement"

export const authorizationRouter = express.Router()
// The authorization routes go here
authorizationRouter.use("/",authRouter)