import { ExtractJwt } from "passport-jwt"
import configkeys from "./../../config"
import { User } from "./user.model"
import { EntityUser } from "./../../api/appresources/entityUser/entityUser.model"
import  logger from "./../../api/modules/logger"


const passport = require("passport")
const JwtStrategy = require("passport-jwt").Strategy
const LocalStrategy = require("passport-local")
const bcrypt = require("bcrypt-nodejs")

// Ceate Local Strategy
const localOptions = {usernameField:"email"}
const MAX_ATTEMPTS = parseInt(process.env.MAX_ATTEMPTS||4, 10)
const isSecurityLoginEnabled = process.env.ENABLE_SECURITY_LOGIN || "No"

const localLoginNew = new LocalStrategy(localOptions, async (email, password, done) => {

  const regexemail = new RegExp(email,"i")
  console.log("REGEX EMAIL", regexemail)

  // verify the email and password
  try{

    const userInfo = await EntityUser.findOne({"userLoginCredentials.userEmailId": {$regex:regexemail,$options:"i"}})

    if(!userInfo) return done("Either password or user ID doesn't match", false)
    if(userInfo.isLocked) return done(`Your account has been locked for trying maximum invalid attempts. Please check after ${process.env.LOCK_TIME || 15} minutes`, false)
    const {userLoginCredentials} = userInfo
    const {password:userhashedpassword} = userLoginCredentials
    let { loginAttempts } = userLoginCredentials
    const matchStatus = await bcrypt.compareSync(password, userhashedpassword)
    if(!matchStatus) {
      if(isSecurityLoginEnabled === "Yes"){
        if(userInfo.lastInvalidAttempt){
          await userInfo.resetLoginAttempts()
          loginAttempts = 0
       }
       logger.error(`The passwords did not match for the user ${email}`)
       const updatedStatus = await userInfo.updateLoginAttempts()
       console.log("Stop HERE");
       if(updatedStatus.isLocked) return done(`Your account has been locked for trying maximum invalid attempts. Please check after ${process.env.LOCK_TIME || 15} minutes`, false)
       if(loginAttempts > 1) return done(`Incorrect credentials. You have ${MAX_ATTEMPTS - loginAttempts} attempt(s) left`, false)
      }
      return done("Either password or user ID doesn't match", false)
    }
    if(loginAttempts > 0) await userInfo.resetLoginAttempts()
    
    const existingUsers = await EntityUser.aggregate([
      {
        $match:     {
          "userLoginCredentials.userEmailId": {$regex:regexemail,$options:"i"},
        }
      },
      {
        $addFields:{
          canWeAllowThisUser:{
            $or:[
              {"$eq":["$userLoginCredentials.onboardingStatus","Done"] },
              {
                $and:[
                  {"$eq":["$userLoginCredentials.onboardingStatus","created"] },
                  {"$eq":["$userLoginCredentials.isUserSTPEligible",true] }              ]
              }
            ]
          },
          userRevisedStatus:{$ifNull:["$userStatus","active"]},
        }
      },
      {
        $match: { userRevisedStatus: "active",canWeAllowThisUser:true }
      },
      {
        $lookup:{
          from: "entityrels",
          localField: "entityId",
          foreignField: "entityParty2",
          as: "entityrelationships"
        }
      },
      {
        $unwind:"$entityrelationships"
      },
      {
        $addFields: {
          relationshipType:"$entityrelationships.relationshipType",
          tenantId:"$entityrelationships.entityParty1",
        }
      },
      {
        $addFields: {
          checkEntitlement: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                    ]},
                  "then": false
                } ],
              "default": true
            }
          },
          isSuperVisor: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $or:[
                      {$in:["Compliance Officer","$userFlags"]},
                      {$in:["Supervisory Principal","$userFlags"]},
                    ]},
                  "then": true
                } ],
              "default": false
            }
          }
        }
      }
    ])

    if (existingUsers.length > 0 ) {
      // Passport assigns this to req.user
      return done(null, existingUsers[0])
    }
    logger.error(`No user exists with the specified email ID who is active in the system - ${email}`)
    return done("No user exists with the specified email ID who is active in the system",false)
  }
  catch(err) {
    logger.error(err)
    console.log(err)
    return done("System was unable to process the details", false)
  }
})

// Set JWT Options
const jwtOptions = {
  jwtFromRequest : ExtractJwt.fromHeader("authorization"),
  secretOrKey : configkeys.secrets.JWT_SECRET
}

const jwtLoginNew = new JwtStrategy(jwtOptions, (payload, done) => {
  
  console.log("CHECKING LOGIN USING JWT")
  // eslint-disable-next-line consistent-return
  EntityUser.aggregate([
    {
      $match:     {
        "userLoginCredentials.userEmailId": {$regex:payload.sub,$options:"i"},
      }
    },
    {
      $addFields:{
        canWeAllowThisUser:{
          $or:[
            {"$eq":["$userLoginCredentials.onboardingStatus","Done"] },
            {
              $and:[
                {"$eq":["$userLoginCredentials.onboardingStatus","created"] },
                {"$eq":["$userLoginCredentials.isUserSTPEligible",true] }              ]
            }
          ]
        },
        userRevisedStatus:{$ifNull:["$userStatus","active"]},
      }
    },
    {
      $match: { userRevisedStatus: "active",canWeAllowThisUser:true }
    },    {
      $lookup:{
        from: "entityrels",
        localField: "entityId",
        foreignField: "entityParty2",
        as: "entityrelationships"
      }
    },
    {
      $unwind:"$entityrelationships"
    },
    {
      $addFields: {
        relationshipType:"$entityrelationships.relationshipType",
        tenantId:"$entityrelationships.entityParty1"
      }
    },
    {
      $addFields: {
        checkEntitlement: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                  ]},
                "then": false
              } ],
            "default": true
          }
        },
        isSuperVisor: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $or:[
                    {$in:["Compliance Officer","$userFlags"]},
                    {$in:["Supervisory Principal","$userFlags"]},
                  ]},
                "then": true
              } ],
            "default": false
          }
        }
      }
    }
  ],(err,user) => {
    if(err) { return done(err, false) }
    if(user.length>0) {
      done(null, user[0])
    } else {
      done(null, false)
    }
  })
})

// Use JWT Login Details to get the JWT Checklist Login

passport.use(jwtLoginNew)

// Use the Local Login strategy to review the password and user before issuing a token
passport.use(localLoginNew)
