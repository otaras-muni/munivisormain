import mongoose,{Schema} from "mongoose"

// Import the User Model from appresources


const userSchema = new Schema ({
  email: {
    type:String,
    required:true,
    unique:true,
    lowercase:true
  },
  password:String
})

export const User =  mongoose.model("user",userSchema)