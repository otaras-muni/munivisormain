import axios from "axios"
import config from "../../config"

const muniApiBaseURL = `http://localhost:${config.port || 4001}`

export const stpSignIn = async (email, password) => {
  if(!email || !password) {
    console.log(" no email or password in stpSignIn")
    return
  }

  try {
    const loginResponse = await axios.post(`${muniApiBaseURL}/auth/signin/`, {email, password})
    const { token } = loginResponse.data || {}
    return token
  } catch(err) {
    console.log("err in stpSignIn : ", err)
  }
}
