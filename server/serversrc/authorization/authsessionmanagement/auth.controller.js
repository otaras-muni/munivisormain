import bcrypt from "bcrypt-nodejs"
// import jwt from "jwt-simple"
import jwt from "jsonwebtoken"
import isEmpty from "lodash/isEmpty"
import crypto from "crypto"
import { templEmailConfirm, changePasswordEmail } from "./emailTemplates"
import { sendAWSEmail } from "./../../api/modules/sendAwsSESEmails"
import { passwordStrength } from "../../api/appresources/commonservices/constants"

import { EntityUser } from "./../../api/appresources/entityUser/entityUser.model"

import { Config } from "./../../api/appresources/config/config.model"
import {
  getMappedFinancialAdvisor,
  findUsersForSignUpByType,
  checkUserExists,
  canUserPerformAction,
  getUrlsForTenantBasedOnLoggedInUserAtSignIn,
  isUserPlatformAdmin
} from "./../../api/commonDbFunctions"
import { stpSignIn } from "./helpers"
import {
  manageEligibleIdsForLoggedInUser,
  updateEligibleIdsForLoggedInUserFirm
} from "./../../api/entitlementhelpers"

import configkeys from "./../../config"

const querystring = require("querystring")

// get the expiration interval in seconds
const expirationInterval =
  process.env.NODE_ENV === "development"
    ? 30 * 24 * 60 * 60
    : process.env.JWTEXPIRE * 60

const tokenForUser = (user, loginDetails) => {
  try {
    const timestamp = new Date().getTime()
    return jwt.sign(
      {
        sub: user.userLoginCredentials.userEmailId,
        iat: timestamp,
        entityDetails: loginDetails.relatedFaEntities[0],
        exp: Math.floor(Date.now() / 1000) + expirationInterval
      },
      configkeys.secrets.JWT_SECRET
    )
  } catch (err) {
    throw err
  }
  // return jwt.encode({ sub: user.userLoginCredentials.userEmailId, iat: timestamp, exp: 30 }, configkeys.secrets.JWT_SECRET)
}

export const signin = async (req, res) => {
  const userEmail = req.user.userLoginCredentials.userEmailId
  try {
    const [
      mappedFaDetails,
      platformAdmin
    ] = await Promise.all([
      getMappedFinancialAdvisor(userEmail),
      isUserPlatformAdmin(req.user)
    ])

    console.log("The current user is a platform admin", platformAdmin)
    if (!platformAdmin) {
      const { userEntitlement, relationshipType } = req.user
      if (
        relationshipType === "Self" &&
        ["global-edit", "global-readonly"].includes(userEntitlement)
      ) {
        console.log(
          "There is no need to run the entitlement engine to recompute everything, skipping the process"
        )
      } else {
        await manageEligibleIdsForLoggedInUser(req.user)
      }
    }
    if (Object.keys(mappedFaDetails).length > 0) {
      res.send({
        authenticated: true,
        token: tokenForUser(req.user, mappedFaDetails),
        loginDetails: mappedFaDetails,
        error: ""
      })
    } else {
      res.status(422).send({
        authenticated: false,
        token: "",
        loginDetails: {},
        error: `Incorrect email ID : ${userEmail}`
      })
    }
  } catch (e) {
    console.log("The error while sign in is", e)
    res.status(422).send({
      authenticated: false,
      token: "",
      loginDetails: {},
      error: `Unable to Login using email - ${userEmail}`
    })
  }
}

export const updateUserDetails = async (queryCriteria, parameters) => {
  const updatedUser = await EntityUser.findOneAndUpdate(
    { $or: { ...queryCriteria } },
    {
      $set: {
        ...parameters
      }
    },
    {
      new: true,
      upsert: true,
      runValidators: true
    }
  )
  return updatedUser
}

export const sendEmailAndUpdateTempPassword = async updateParams => {
  const {
    emailParams,
    url,
    queryString,
    updateString,
    incrementString,
    context
  } = updateParams

  console.log(`In send email and update password: ${context}`)
  // Check if the parameters have been passed properly
  if (
    Object.keys(emailParams).length === 0 ||
    Object.keys(queryString).length === 0 ||
    !url
  ) {
    return {
      done: false,
      error: "Please pass correct parameters",
      success: "",
      user: null,
      status: 3
    }
  }
  // Find the user with the specified parameters
  const userFound = await EntityUser.findOne({ ...queryString })

  if (userFound) {
    const confirmString = crypto.randomBytes(40).toString("hex")
    const { emailExpiry } = configkeys.signup
    const validateEmailURL = `${url}?id=${confirmString}`
    console.log({ confirmString, emailExpiry, validateEmailURL })
    let contextUpdate
    if (context === "signup") {
      contextUpdate = {
        $set: {
          ...updateString,
          "userLoginCredentials.userEmailConfirmString": confirmString,
          "userLoginCredentials.userEmailConfirmExpiry":
            Date.now() + parseFloat(emailExpiry)
        }
      }
    } else if (context === "changepassword") {
      contextUpdate = {
        $set: {
          ...updateString,
          "userLoginCredentials.userEmailConfirmString": confirmString,
          "userLoginCredentials.userEmailConfirmExpiry":
            Date.now() + parseFloat(emailExpiry)
        }
      }
    } else {
      console.log("in else")
      contextUpdate = {
        $set: {
          ...updateString,
          "userLoginCredentials.passwordConfirmString": confirmString,
          "userLoginCredentials.passwordConfirmExpiry":
            Date.now() + parseFloat(emailExpiry)
        },
        $inc: { ...incrementString }
      }
      console.log(contextUpdate)
    }
    console.log("querystring")
    console.log(queryString)
    const updatedUser = await EntityUser.findOneAndUpdate(
      { ...queryString },
      {
        ...contextUpdate
      },
      {
        new: false,
        upsert: true,
        runValidators: true
      }
    )

    if (updatedUser) {
      const { body, template, ...restemailparams } = emailParams
      const emailParameters = {
        ...restemailparams,
        bodytemplate: template({ body, url: validateEmailURL })
      }
      const returnValue = await sendAWSEmail({ ...emailParameters })
      if (returnValue.status === "success") {
        return {
          done: true,
          error: "",
          success: "User info updated and confirmation email sent",
          user: updatedUser,
          status: 1
        }
      }
      return {
        done: false,
        error: "User info updated and confirmation could not be set",
        success: "",
        user: {},
        status: 2
      }
    }
    // The user could not be updated properly and no email was sent
    return {
      done: false,
      error: "Yor URL is expired.",
      success: "",
      user: {},
      status: 3
    }
  }
  // send the status back saying something is not right
  return {
    done: false,
    error: "No user found that match specific query condition",
    success: "",
    user: {},
    status: 3
  }
}

export const signupnew = async (req, res) => {
  // console.log(req.body)
  const { email, password } = req.body
  const regexemail = new RegExp(email, "i")
  const strongRegex = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})(?=.*[!@#$%^&*])"
  )

  // check if the email is valid or not
  if (
    !email ||
    !password ||
    (!strongRegex.test(password) && passwordStrength === "true")
  ) {
    return res.status(200).send({
      done: false,
      error: "A valid email and/or password should be provided",
      success: ""
    })
  }

  const salt = bcrypt.genSaltSync(10)
  const hashedpassword = bcrypt.hashSync(password, salt)
  try {
    // Get all the users with a given email ID
    const users = await EntityUser.find({
      "userLoginCredentials.userEmailId": { $regex: regexemail, $options: "i" },
      "userLoginCredentials.onboardingStatus": { $in : ["created", "validate"] }
    }).select({
      userFirstName: 1,
      userLastName: 1,
      userLoginCredentials: 1,
      _id: 1
    })
    // Update all the users who have the same password with the token
    console.log("USERS:", users)
    if (users.length > 0) {
      const updatedUsers = await Promise.all(
        users.map(async user => {
          try {
            // construct URL ? https: http:
            const urlLink = req.connection.encrypted ? "https" : "http"
            const {
              fromEmail,
              replyToEmail,
              defaultFromEmail
            } = configkeys.systemEmails
            const validateEmailURL = `${urlLink}://${req.get(
              "host"
            )}/auth/signupconfirm`
            const emailParams = {
              to: [user.userLoginCredentials.userEmailId],
              cc: [fromEmail],
              body:
                "Thanks for signing up with MuniVisor. Please validate the email by clicking the link below.",
              subject:
                "MuniVisor - Please confirm your email and login credentials",
              template: templEmailConfirm,
              replyto: [replyToEmail]
            }
            const queryString = { _id: user.id }

            const updateString = {
              "userLoginCredentials.onboardingStatus": "validate",
              "userLoginCredentials.password": hashedpassword
            }
            const returnValue = await sendEmailAndUpdateTempPassword({
              emailParams,
              url: validateEmailURL,
              queryString,
              updateString,
              incrementString: {},
              context: "signup"
            })
            return returnValue
          } catch (err) {
            console.log("WHAT IS GETTING REPORTED AS ERROR", err)
            return {
              done: false,
              error: "No user found that match specific query condition",
              success: "",
              err
            }
          }
        })
      )

      // Map through the result set and send appropriate error messages
      const returnValue = updatedUsers.reduce(
        (finalStatus, { done }) => done && finalStatus,
        true
      )

      if (returnValue) {
        res.send({
          done: true,
          error: "",
          success: "Updated User and Sent confirmation Email"
        })
      } else {
        res.status(422).send({
          done: false,
          error: `There is a system Error Or the User with email - ${email} has already signed up`,
          success: ""
        })
      }
    } else {
      res.status(422).send({
        done: false,
        error: `Already signed up please Login OR contact your admin to configure your user email - ${email}`,
        success: ""
      })
    }
  } catch (err) {
    console.log(err)
    res.status(422).send({
      done: false,
      error: `There is an error signing up user ${email}`,
      success: ""
    })
  }
}

export const forgotPassword = async (req, res) => {
  // get the email from the body
  const { email } = req.body
  const regexemail = new RegExp(email, "i")

  // check if the email is valid or not
  if (!email) {
    return res.status(200).send({
      done: false,
      error: "A valid email should be provided",
      success: ""
    })
  }

  try {
    // Get all the users with a given email ID
    const users = await EntityUser.find({
      "userLoginCredentials.userEmailId": { $regex: regexemail, $options: "i" },
      // "userLoginCredentials.onboardingStatus": "Done"
    }).select({
      userFirstName: 1,
      userLastName: 1,
      userLoginCredentials: 1,
      _id: 1
    })
    // Update all the users who have the same password with the token
    if (users.length > 0) {
      const updatedUsers = await Promise.all(
        users.map(async user => {
          try {
            console.log("forgotPasswor")
            console.log(user)
            // construct URL
            console.log("Constructing URL")
            const {
              fromEmail,
              replyToEmail,
              defaultFromEmail
            } = configkeys.systemEmails
            const urlLink = req.connection.encrypted ? "https" : "http"
            // const validateEmailURL = `${urlLink}://${req.get("host")}/auth/resetpassconfirm`
            const validateEmailURL = `${
              process.env.FRONTEND_URL
            }/changeforgotpass`
            const toEmail = email || defaultFromEmail
            const emailParams = {
              to: [toEmail],
              cc: [fromEmail],
              body:
                "You have requrested the password to be reset. Please click on the link to reset the password",
              subject: "MuniVisor - You have requested password to be Reset",
              template: templEmailConfirm,
              replyto: [replyToEmail]
            }
            const queryString = { $and: [{ _id: user.id }] }
            const updateString = {
              "userLoginCredentials.passwordResetStatus": "open"
            }
            const incrementString = {
              "userLoginCredentials.passwordResetIteration": 1
            }
            const returnValue = await sendEmailAndUpdateTempPassword({
              emailParams,
              url: validateEmailURL,
              queryString,
              incrementString,
              updateString,
              context: "forgotpass"
            })
            return returnValue
          } catch (err) {
            return {
              done: false,
              error: "No user found that match specific query condition",
              success: ""
            }
          }
        })
      )

      // Map through the result set and send appropriate error messages
      const returnValue = updatedUsers.reduce(
        (finalStatus, { done }) => done && finalStatus,
        true
      )

      if (returnValue) {
        res.send({
          done: true,
          error: "",
          success: "Updated User and Sent confirmation Email"
        })
      } else {
        res.status(200).send({
          done: false,
          error: `There was a problem in sending password confirmation email - ${email}`,
          success: ""
        })
      }
    } else {
      res.status(200).send({
        done: false,
        error: `You cannot request password for this email. They are not onboarded to MuniVisor - ${email}`,
        success: ""
      })
    }
  } catch (err) {
    res.status(200).send({
      done: false,
      error: `There is an error sending email ${email}`,
      success: ""
    })
  }
}

export const signupConfirm = async (req, res) => {
  console.log(req.query)
  let { id } = req.query
  if (!id) {
    return res.status(422).send({ error: "There is no token in the request" })
  }
  id = id.replace(/\\/, "")
  const { redirectSignInUrl, requestConfirmUrl } = configkeys.signup
  console.log({ redirectSignInUrl, requestConfirmUrl })

  try {

    const updatedUsers = await EntityUser.updateOne(
      {
        "userLoginCredentials.onboardingStatus": "validate",
        "userLoginCredentials.userEmailConfirmString": id,
        "userLoginCredentials.userEmailConfirmExpiry": { $gte: Date.now() }
      },
      {
        $set: {
          "userLoginCredentials.onboardingStatus": "Done",
          "userLoginCredentials.userEmailConfirmString": null,
          "userLoginCredentials.userEmailConfirmExpiry": null,

          // If they go through the normal signup process then they will no longer be allowed on the STP flow.
          "userLoginCredentials.isUserSTPEligible": false,
          "userLoginCredentials.authSTPToken": null,
          "userLoginCredentials.authSTPPassword": null
        }
      },
      {
        new: false,
        runValidators: true
      }
    )

    if (isEmpty(updatedUsers)) {
      res.redirect(`${process.env.FRONTEND_URL}${requestConfirmUrl}`)
    } else {
      res.redirect(`${process.env.FRONTEND_URL}${redirectSignInUrl}`)
    }
  } catch (e) {
    console.log(e)
    res.redirect(`${process.env.FRONTEND_URL}${redirectSignInUrl}`)
  }
}

export const getSignupUserFromResetId = async (req, res) => {
  console.log(req.query)
  let { id } = req.query
  id = id.replace(/\\/, "")
  if (!id) {
    return res.status(422).send({ error: "There is no token in the request" })
  }

  try {
    const updatedUsers = await EntityUser.findOne({
      "userLoginCredentials.passwordResetStatus": "open",
      "userLoginCredentials.userEmailConfirmString": id,
      "userLoginCredentials.userEmailConfirmExpiry": { $gte: Date.now() }
    })
    console.log(updatedUsers)
    if (isEmpty(updatedUsers)) {
      res.status(422).send({ error: "Token is invalid" })
    }
    res.send(updatedUsers)
  } catch (e) {
    console.log(e)
    return res.status(503).send({ error: "Error in fetching user" })
  }
}

export const passwordResetConfirm = async (req, res) => {
  let { id } = req.query
  let updatedUsers = {}
  if (!id) {
    return res.status(422).send({ error: "There is no token in the request" })
  }
  id = id.replace(/\\/, "")
  try {
    const { redirectSignInUrl, passwordResetUrl } = configkeys.signup
    updatedUsers = await EntityUser.findOneAndUpdate(
      {
        "userLoginCredentials.passwordConfirmString": id,
        "userLoginCredentials.passwordResetStatus": "open",
        "userLoginCredentials.passwordConfirmExpiry": { $gte: Date.now() }
      },
      {
        $set: {
          "userLoginCredentials.onboardingStatus": "Done",
          "userLoginCredentials.passwordResetStatus": "done",
          "userLoginCredentials.passwordConfirmString": null,
          "userLoginCredentials.passwordConfirmExpiry": null
        }
      },
      {
        new: false,
        upsert: true,
        runValidators: true
      }
    )
  } catch (e) {
    console.log(e)
    return res.send({})
  }
  // Check if the return values exist
  // if (isEmpty(updatedUsers)) {
  // res.redirect(`${process.env.FRONTEND_URL}${redirectSignInUrl}`)
  // } else {
  return res.send(updatedUsers)
  // }
}

export const getUsersForSignUp = async (req, res) => {
  const { type } = req.query
  const retValue = await findUsersForSignUpByType([type])
  res.json(retValue)
}

export const checkAuth = async (req, res) => {
  const userEmail = req.user.userLoginCredentials.userEmailId
  const token = req.headers.authorization
  const decoded = await jwt.verify(token, configkeys.secrets.JWT_SECRET)
  console.log("DECODED TOKEN", JSON.stringify(decoded, null, 2))
  // console.log("headers : ", JSON.stringify(req.headers, null, 2))
  if (userEmail) {
    try {
      const mappedFaDetails = await getMappedFinancialAdvisor(userEmail)

      if (Object.keys(mappedFaDetails).length > 0) {
        res.send({
          authenticated: true,
          token,
          exp: (decoded && decoded.exp) || 0,
          loginDetails: mappedFaDetails,
          error: ""
        })
      } else {
        res.send({
          authenticated: false,
          token: "",
          loginDetails: {},
          exp: (decoded && decoded.exp) || 0,
          error: { message: `Incorrect email ID : ${userEmail}` }
        })
      }
    } catch (e) {
      res.send({
        authenticated: false,
        token: "",
        loginDetails: {},
        exp: (decoded && decoded.exp) || 0,
        error: { message: `Unable to Login using email - ${userEmail}` }
      })
    }
  } else {
    res.send({
      authenticated: false,
      token: "",
      loginDetails: {},
      exp: (decoded && decoded.exp) || 0,
      error: { message: `Email ID doesn't exist - ${userEmail}` }
    })
  }
}

export const getEntitlementsForLoggedInUser = async (req, res, next) => {
  const methodMap = { GET: [1], PUT: [2], POST: [0, 2], DELETE: [3] }
  const { entityId } = req.user
  const lookupMethod = methodMap[req.method]
  console.log(lookupMethod)
  const resourcerequested = req.path.split("/")[1] // Tested this function with "firms"
  const retValue = await Config.aggregate([
    {
      $match: {
        entityId
      }
    },
    {
      $project: {
        _id: 0,
        entityId: 1,
        backendResources: "$accessPolicy.resourceAccess"
      }
    },
    {
      $unwind: "$backendResources"
    },
    {
      $unwind: "$backendResources"
    },
    {
      $match: {
        "backendResources.resource": resourcerequested
      }
    }
  ])
  const { backendResources } = retValue[0]
  const { CRUD } = backendResources
  const entitlementCheck = lookupMethod.reduce(
    (finalDecision, access) => finalDecision && CRUD[access],
    true
  )

  res.json({ permission: entitlementCheck })
}

export const checkUserExistance = async (req, res) => {
  const { email } = req.query

  let retValue
  try {
    retValue = await checkUserExists(email)
    if (Object.keys(retValue).length > 0) {
      res.json({ ...retValue })
    } else {
      res.json({ foundemail: false, error: "Enter a valid email ID/password" })
    }
  } catch (err) {
    retValue = { foundemail: false, error: "Unable to validate email from DB" }
    res.json({ ...retValue })
  }
}

export const checkEntitlement = async (req, res) => {
  const user = { _id: "5b2d24851aa04457c0bc5c19" }

  const resRequested = [
    { resource: "Entity", access: 1 },
    { resource: "EntityUser", access: 1 },
    { resource: "Deals", access: 1 },
    { resource: "RFP", access: 1 }
  ]

  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res.status(422).send({
        done: false,
        error: "User is not entitled to access the requested services",
        success: "",
        requestedServices: resRequested
      })
    }
    res.send({
      done: true,
      error: "",
      success: "Authorized to Perform action",
      requestedServices: resRequested
    })
  } catch (e) {
    res.status(422).send({
      done: false,
      error: "There is a failure in retrieving information for entitlements",
      success: "",
      requestedServices: resRequested
    })
  }
}

export const stpAuth = async (req, res) => {
  const { token, redirect } = req.query
  if (!token || !redirect) {
    return res.status(422).send({ error: "No token or redirect url found" })
  }

  try {
    const user = await EntityUser.findOne({
      "userLoginCredentials.authSTPToken": token
    })
    if (user) {
      const {
        userLoginCredentials: { userEmailId, authSTPPassword }
      } = user
      if (!userEmailId || !authSTPPassword) {
        return res.status(401).send({ error: "Unauthorized" })
      }
      const authToken = await stpSignIn(userEmailId, authSTPPassword)
      if (!authToken) {
        return res.status(401).send({ error: "Unauthorized" })
      }
      const query = querystring.stringify({
        token: authToken,
        email: userEmailId,
        redirect
      })
      res.redirect(`${process.env.FRONTEND_URL || ""}/stp-redirect?${query}`)
    } else {
      return res.status(401).send({ error: "Unauthorized" })
    }
  } catch (err) {
    console.log("err in stpAuth : ", err)
    return res.status(503).send({ error: "Error in checking authorization" })
  }
}

export const changePassword = async (req, res) => {
  // console.log(req.body)
  const { email, password } = req.body
  const { type } = req.query
  const regexemail = new RegExp(email, "i")

  // check if the email is valid or not
  if (!email || !password) {
    return res.status(422).send({
      done: false,
      error: "A valid email and/or password should be provided",
      success: ""
    })
  }

  const salt = bcrypt.genSaltSync(10)
  const hashedpassword = bcrypt.hashSync(password, salt)

  console.log("hashedpassword========>", hashedpassword)
  try {
    // Get all the users with a given email ID
    let query = {
      "userLoginCredentials.userEmailId": { $regex: regexemail, $options: "i" }
    }
    if(type !== "UserProfile"){
      query = {
        ...query,
        "userLoginCredentials.passwordResetStatus": { $in: ["open", "done"] }
      }
    }
    const users = await EntityUser.find(query).select({
      userFirstName: 1,
      userLastName: 1,
      userLoginCredentials: 1,
      _id: 1
    })
    // Update all the users who have the same password with the token
    console.log("query:", query)
    console.log("USERS:", users)
    if (users.length > 0) {
      const updatedUsers = await Promise.all(
        users.map(async user => {
          try {
            // construct URL
            const {
              fromEmail,
              replyToEmail,
              defaultFromEmail
            } = configkeys.systemEmails
            const validateEmailURL = " "
            const emailParams = {
              to: [defaultFromEmail],
              cc: [fromEmail],
              body: "Your MuniVisor password has been changed successfully.",
              subject: "MuniVisor password change notification",
              template: changePasswordEmail,
              replyto: [replyToEmail]
            }
            const queryString = { _id: user.id }

            const updateString = {
              "userLoginCredentials.password": hashedpassword
            }
            const returnValue = await sendEmailAndUpdateTempPassword({
              emailParams,
              url: validateEmailURL,
              queryString,
              updateString,
              incrementString: {},
              context: "changepassword"
            })
            return returnValue
          } catch (err) {
            console.log("WHAT IS GETTING REPORTED AS ERROR", err)
            return {
              done: false,
              error: "No user found that match specific query condition",
              success: "",
              err
            }
          }
        })
      )

      // Map through the result set and send appropriate error messages
      const returnValue = updatedUsers.reduce(
        (finalStatus, { done }) => done && finalStatus,
        true
      )

      if (returnValue) {
        res.send({
          done: true,
          error: "",
          success: "Password changed and Sent confirmation Email"
        })
      } else {
        res.status(422).send({
          done: false,
          error: "Error in changing password for the user",
          success: ""
        })
      }
    } else {
      res.status(422).send({
        done: false,
        error: "Error in getting user information",
        success: ""
      })
    }
  } catch (err) {
    console.log(err)
    res.status(422).send({
      done: false,
      error: "There is an error in changign password",
      success: ""
    })
  }
}
