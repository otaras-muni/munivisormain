const keys = require("./../../config/index")

export const templEmailConfirm = signUpConfirmation => `
	<html>
	<body>
		<div style="text-align: center">
		<h2> Welcome to MuniVisor. Please validate your email</h2>
		<p>${signUpConfirmation.body}</p>
		<div>
			<h3>The Link is here - please access and confirm your email<a href="${signUpConfirmation.url}">Confirm Email</a></h3>
			<h4>On successful confirmation you will be presented with a signin page and automatically authenticated</h4>
		</div>
		</div>
	</body>
	</html>
	`
export const passwordResetEmail = resetInfo => `
	<html>
	<body>
		<div style="text-align: center">
		<h2> This is an email to reset your password </h2>
		<p>${resetInfo.body}</p>
		<div>
			<h3>Please click link to reset the password<a href="${resetInfo.url}">Reset Email</a></h3>
		</div>
		</div>
	</body>
	</html>
	`
export const changePasswordEmail = resetInfo => `
	<html>
	<body>
		<div style="text-align: center">
		<p>${resetInfo.body}</p>
		</div>
	</body>
	</html>
	`
