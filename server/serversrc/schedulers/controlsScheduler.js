import { Entity } from "./../api/appresources/models"

const schedule = require("node-schedule")

require("dotenv").config()

schedule.scheduleJob({second:2}, async () => {
  // Run the shell script
  // Return Code
  // if = 0
  console.log("Time for tea!")
  const a = await Entity.find({isMuniVisorClient:true}).select({firmName:1,_id:1})
  console.log("The value is",JSON.stringify(a, null,2))
})