const URLCONSTS = {
  deals:{
    summary:"/deals/___ID___/summary",
    details:"/deals/___ID___/details",
    participants:"/deals/___ID___/participants",
    pricing:"/deals/___ID___/pricing",
    ratings:"/deals/___ID___/ratings",
    documents:"/deals/___ID___/documents",
    "check-track":"/deals/___ID___/check-track",
    "audit-trail":"/deals/___ID___/audit-trail",
  },
  rfps:{
    summary:"/rfp/___ID___/summary",
    distribute:"/rfp/___ID___/distribute",
    manage:"/rfp/___ID___/manage",
    "check-track":"/rfp/___ID___/check-track",
    "audit-trail":"/rfp/___ID___/audit-trail",
  },
  bankloans:{
    summary:"/loan/___ID___/summary",
    details:"/loan/___ID___/details",
    participants:"/loan/___ID___/participants",
    pricing:"/loan/___ID___/pricing",
    ratings:"/loan/___ID___/ratings",
    documents:"/loan/___ID___/documents",
    "check-track":"/loan/___ID___/check-track",
    "audit-trail":"/loan/___ID___/audit-trail",
  },
  marfps:{
    summary:"/marfp/___ID___/summary",
    details:"/marfp/___ID___/details",
    participants:"/marfp/___ID___/participants",
    pricing:"/marfp/___ID___/pricing",
    ratings:"/marfp/___ID___/ratings",
    documents:"/marfp/___ID___/documents",
    "check-track":"/marfp/___ID___/check-track",
    "audit-trail":"/marfp/___ID___/audit-trail",
  },
  derivatives:{
    summary:"/derivative/___ID___/summary",
    details:"/derivative/___ID___/details",
    participants:"/derivative/___ID___/participants",
    pricing:"/derivative/___ID___/pricing",
    ratings:"/derivative/___ID___/ratings",
    documents:"/derivative/___ID___/documents",
    "check-track":"/derivative/___ID___/check-track",
    "audit-trail":"/derivative/___ID___/audit-trail",
  },
  others:{
    summary:"/other/___ID___/summary",
    details:"/other/___ID___/details",
    participants:"/other/___ID___/participants",
    pricing:"/other/___ID___/pricing",
    ratings:"/other/___ID___/ratings",
    documents:"/other/___ID___/documents",
    "check-track":"/other/___ID___/check-track",
    "audit-trail":"/other/___ID___/audit-trail",
  },
  busdevs:{
    summary:"/bus-development/___ID___/",
    details:"/bus-development/___ID___/",
    participants:"/bus-development/___ID___/",
    pricing:"/bus-development/___ID___/",
    ratings:"/bus-development/___ID___/",
    documents:"/bus-development/___ID___/",
    "check-track":"/bus-development/___ID___/",
    "audit-trail":"/bus-development/___ID___/",
  },

}

export const fetchUrlForTransactions = (tranType, page, TRANID) => {
  const URL = URLCONSTS[tranType][page]
  const REVISEDURL = URL.replace("___ID___", TRANID)
  return REVISEDURL
}