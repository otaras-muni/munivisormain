/* eslint-disable indent */
import AWS from "aws-sdk"
import dotenv from "dotenv"

import { differenceBy, intersectionBy } from "lodash"
import { getTemplatesInfo } from "./templateDetails"

dotenv.config()

const fs = require("fs")
const path = require("path")

AWS.config.update({
  region: process.env.SESREGION || "us-east-1",
  accessKeyId: process.env.SESACCESSKEYID || "AKIAIMCK6CR7IJIR7DDA",
  secretAccessKey: process.env.SESSECRETKEY || "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",
})

const ITEMS_COUNT = 0

const isDevOnly = process.env.NODE_ENV === "development"

const listTemplates = async () => {
  const listAllTemplatesPromise = new AWS.SES({ apiVersion: "2010-12-01" }).listTemplates({ MaxItems: ITEMS_COUNT }).promise()
  let listAllTemplates = await listAllTemplatesPromise
  if(isDevOnly){
    listAllTemplates = listAllTemplates.TemplatesMetadata
                          .filter( template => template.Name.indexOf("DEV_") > -1)
  }else{
    listAllTemplates = listAllTemplates.TemplatesMetadata
                          .filter( template => template.Name.indexOf("DEV_") === -1 )
  }
  return listAllTemplates
}

const pushTemplatesToSES = async (emailTemplates, type) => {
  console.log(`${type} templates`)
  console.log(emailTemplates)
  const result = Promise.all(emailTemplates.map(async (template) => {
    const htmlContent = fs.readFileSync(path.join(__dirname, template.filepath), "utf-8")
    const params = {
      Template: {
        TemplateName: template.Name,
        HtmlPart: htmlContent,
        SubjectPart: template.SubjectPart,
      },
    }
    let templatePromsie = ""
    if (type === "create") templatePromsie = new AWS.SES({ apiVersion: "2010-12-01" }).createTemplate(params).promise()
    if (type === "update") templatePromsie = new AWS.SES({ apiVersion: "2010-12-01" }).updateTemplate(params).promise()
    const status = await templatePromsie
    return status
  }))
  const allPromises = await result
  return allPromises
}

const removeTemplatesFromSES = async (emailTemplates) => {
  console.log("Remove Templates")
  console.log(emailTemplates)
  const result = Promise.all(emailTemplates.map(async (template) => {
    const templatePromise = new AWS.SES({ apiVersion: "2010-12-01" }).deleteTemplate({ TemplateName: template.Name }).promise()
    const status = await templatePromise
    return status
  }))
  const allPromises = await result
  return allPromises
}

const upsertTemplates = async () => {
  const availableTemplates = await listTemplates()
  let listOfTemplates = getTemplatesInfo()
  
  const removableTemplates = differenceBy(availableTemplates, listOfTemplates, "Name")

  listOfTemplates = listOfTemplates.filter( template => template.isUpdatable)

  const newTemplates = differenceBy(listOfTemplates, availableTemplates, "Name")
  const oldTemplates = intersectionBy(listOfTemplates, availableTemplates, "Name")

  try {
    if(removableTemplates.length > 0) await removeTemplatesFromSES(removableTemplates)
    if (newTemplates.length > 0) await pushTemplatesToSES(newTemplates, "create")
    if (oldTemplates.length > 0) await pushTemplatesToSES(oldTemplates, "update")
  } catch (e) {
    console.log(e)
  }
  process.exit()
}

export default upsertTemplates