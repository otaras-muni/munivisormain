import mongoose from "mongoose"
import { Entity } from "../entity/entity.model"

const {Schema} = mongoose
import timestamps from "mongoose-timestamp"
// mongoose.Promise = global.Promise

const LOCK_TIME = parseInt(process.env.LOCK_TIME || 15,10) * 60 * 1000
const MAX_ATTEMPTS = parseInt(process.env.MAX_ATTEMPTS || 4, 10)

export const userAddressSchema = Schema({
  addressName:String,
  addressType:String, // Office address, headquarter or residence,
  isPrimary:Boolean,
  isActive:{type:Boolean, default:true},
  addressLine1:String,
  addressLine2:String,
  country:String,
  state:String,
  city:String,
  zipCode:{zip1:String, zip2:String},
  formatted_address:String,
  url:String,
  location:{longitude:String,latitude:String}
})

const userAddonSchema = Schema({
  serviceType:String,
  serviceEnabled:String
})

// eslint-disable-next-line no-unused-vars
const userCredentialsSchema = Schema({
  _id:String,
  userId:String, // Should come from the user Collection
  userEmailId:String, // We can get rid of this if we are able to just use the email ID.
  password:String,  // This is the salted password.
  onboardingStatus:String,
  userEmailConfirmString:String,
  userEmailConfirmExpiry:Date,
  passwordConfirmString:String,
  passwordConfirmExpiry:Date,
  isUserSTPEligible:Boolean,
  authSTPToken:String,
  authSTPPassword:String,
  passwordResetIteration:Number,
  passwordResetStatus:String,
  passwordResetDate:Date,
  loginAttempts: { type: Number, default: 0},
  lockUntil: { type: Number},
  lastLoginAttempt: { type: Date}
})

// eslint-disable-next-line no-unused-vars
const userLoginSession = Schema({
  _id:String,
  userId:String,
  ipAddress:String,
  sessionStartTime:Date,
  sessionEndTime:Date,
})

const documentsSchema = Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  docNote:String,
  docStatus:String,
  markedPublic:{ publicFlag:Boolean, publicDate:Date},
  sentToEmma: { emmaSendFlag:Boolean, emmaSendDate:Date},
  createdBy: {type: Schema.Types.ObjectId },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const userSchema = Schema({
  userId:String, // Should we make this the same as the primary email
  entityId: { type: Schema.Types.ObjectId, ref: Entity },
  userStatus: String,
  timeZone: {},
  userFirmName:String,
  userFlags: [String], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
  userRole:String, // [ Admin, ReadOnly, Backup ]
  userEntitlement:String, // [ Global, Transaction ]
  userFirstName:String,
  userMiddleName:String,
  userLastName:String,
  userEmails:[{
    emailId:String,
    emailPrimary:Boolean,
  }],
  userPhone: [{
    phoneNumber: "",
    extension: "",
    phonePrimary: false
  }],
  userFax: [{
    faxNumber: "",
    faxPrimary: false
  }],
  userEmployeeID:String,
  userJobTitle:String,
  userManagerEmail:String,
  userJoiningDate:Date,
  userEmployeeType:String,
  userDepartment:String,
  userExitDate:Date,
  userCostCenter:String,
  userAddresses:[userAddressSchema],
  userAddOns:[String],
  userLoginCredentials:userCredentialsSchema,
  userAddDate:Date,
  userUpdateDate:Date,
  userDocuments:[documentsSchema],
  notes:[notesSchema],
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
})

userSchema.methods.updateLoginAttempts = async function ULA() {
  if(this.userLoginCredentials["lockUntil"] && this.userLoginCredentials["lockUntil"] < Date.now()) {
    const updateAttempts = await this.update({
      $set: { "userLoginCredentials.loginAttempts": 1, "userLoginCredentials.lastLoginAttempt": Date.now()},
      $unset: { "userLoginCredentials.lockUntil": 1}
    })
    if(updateAttempts) return {done: true, message:"Successfully updated login attempts for the user", isLocked: this.isLocked}
    return {done: false, message: "Could not update login attempts for ths user", isLocked: false}
  }

  const increaseAttempts = { $inc: { "userLoginCredentials.loginAttempts": 1}, $set: {"userLoginCredentials.lastLoginAttempt": Date.now()}}
  if(this.userLoginCredentials["loginAttempts"] + 1 > MAX_ATTEMPTS && !this.isLocked){
    increaseAttempts.$set = { "userLoginCredentials.lockUntil": Date.now() + LOCK_TIME}
  }
  console.log(increaseAttempts)
  const results = await this.update(increaseAttempts)
  console.log(results);
  if(results) return {done: true, message: "Successfully updated login attempts for the user", isLocked: this.userLoginCredentials["loginAttempts"] + 1 > MAX_ATTEMPTS}
  return { done: false, message: "Successfully updated login attempts for the user", isLocked: false}
}

userSchema.methods.resetLoginAttempts = async function RLA() {
  const updates = {
    $set: { "userLoginCredentials.loginAttempts": 0 },
    $unset: { "userLoginCredentials.lockUntil": 1, "userLoginCredentials.lastLoginAttempt": 1 }
  }
  const results = await this.update(updates)
  if(results) return {done: true, message: "Login Attempts have been reset successfully"}
  return {done: false, message: "Could not reset login attempts"}
}


userSchema.virtual("isLocked").get(function getLock(){
  return !!(this.userLoginCredentials["lockUntil"] && this.userLoginCredentials["lockUntil"] > Date.now())
})

userSchema.virtual("lastInvalidAttempt").get(function LIA(){
  return Date.now() - this.userLoginCredentials["lastLoginAttempt"] > LOCK_TIME
})

userSchema.plugin(timestamps)

userSchema.index({entityId:1})

export const EntityUser =  mongoose.model("entityuser", userSchema)
