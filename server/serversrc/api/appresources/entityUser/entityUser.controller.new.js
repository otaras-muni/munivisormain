import {EntityUser} from "./entityUser.model"
import {EntityRel} from "../entityRel/entityRes.model"

import {canUserPerformAction} from "../../commonDbFunctions"
import {getAllEligibleActivityForSearch} from "../../commonDbFunctions/getActivityForSearch"
import {isUserPlatformAdmin} from "../../commonDbFunctions/isUserPlatformAdmin"
import {checkIfUserIsSTPEligible, signUpSTPUserOnCreate} from "./clientThirdPartySTPServices"
import {getAllEligibleUserIds, doesIdExistsInEntitledUsers, updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {Entity} from "../entity/entity.model"
import moment from "moment"
import {checkDuplicateEmailForTenant} from "./../../commonDbFunctions/checkDuplicateEmailForTenant"


import {elasticSearchUpdateFunction,elasticSearchNormalizationAndBulkInsert} from "./../../elasticsearch/esHelper"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: EntityUser, _id: id, esType: "entityusers"})

const {ObjectID} = require("mongodb")

export const saveUser = async(req, res) => {
  const {user} = req
  const {userDetails, duplicateExists} = req.body
  const {_id} = userDetails
  try {
    if (duplicateExists) {
      let {userEmails} = userDetails
      userEmails = userEmails.map(email => email.emailId)
      let {duplicateEmailIds} = req.body
      duplicateEmailIds = duplicateEmailIds.map(email => email.emailId)
      duplicateEmailIds = [...new Set(duplicateEmailIds)]
      const duplError = []
      userEmails.forEach((email, idx) => {
        if (duplicateEmailIds.indexOf(email) !== -1)
          duplError.push({email, value: idx, label: `${email} is already exists`})
      })
      res
        .status(422)
        .send({
          done: false,
          error: "Duplicates emails are found",
          success: "",
          duplicateExists,
          duplError,
          isPrimaryAddress: true
        })
      return false
    }

    const result = await EntityUser.create(userDetails)
    const isPrimaryContact = result && result.userFlags.indexOf("Primary Contact") !== -1
    if(isPrimaryContact){
      const entityResult = await Entity.findById(result.entityId).select("primaryContactId")
      if (entityResult && entityResult.primaryContactId) {
        const entityUser = await EntityUser.findOne({_id: ObjectID(entityResult.primaryContactId)}).select("userFlags")
        if(entityUser && entityUser.userFlags){
          const flagIndex = entityUser.userFlags.indexOf("Primary Contact")
          if(flagIndex !== -1){
            entityUser.userFlags.splice(flagIndex, 1)
            await EntityUser.updateOne({_id: ObjectID(entityResult.primaryContactId)}, {$set: {userFlags: entityUser.userFlags}})
          }
        }
      }
      await Entity.updateOne({ _id: ObjectID(result.entityId) || ""}, {$set: {primaryContactId: result._id} })
    }
    res.json({done: true, error: "", success: result, isPrimaryAddress: true, duplicateExists: false})
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", duplicateExists: false, isPrimaryAddress: true})
  }
}

export const getUsers = async(req, res) => {}

export const getUsersForEntity = async(req, res) => {
  const {user} = req
  const {required, entityId} = req.body

  console.log("in getUsersForEntity : ", user && user._id, required, entityId)
  if (!user || !entityId) {
    res
      .status(422)
      .send("No user auth or entityId found")
  }

  const resRequested = [
    {
      resource: "EntityUser",
      access: 1
    }
  ]

  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const users = await EntityUser
      .find({entityId})
      .select(required || {})
      .lean()
    users.forEach(user => {
      user.userEmailId = user.userLoginCredentials.userEmailId
      user.userLoginCredentials = user.userLoginCredentials.onboardingStatus
    })
    res.json(users)
  } catch (error) {
    res
      .status(422)
      .send({error})
  }
}

export const getUserById = async(req, res) => {
  const reqUser = req.user
  const {userId} = req.params

  if (!userId) {
    res
      .status(422)
      .send("No entity id provided")
  }

  const resRequested = [
    {
      resource: "EntityUser",
      access: 1
    }
  ]

  try {
    const entitled = await canUserPerformAction(reqUser, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const user = await EntityUser
      .findOne({_id: userId})
      .lean()
    user.userLoginCredentials = user.userLoginCredentials.onboardingStatus
    res.json(user)
  } catch (error) {
    res
      .status(422)
      .send({error})
  }
}

export const updateUserStatusFlag = async(req, res) => {
  const {user} = req
  const {ids,status} = req.body

  try {
    const result = await EntityUser.updateMany(
      {_id:{$in:[...ids]}},
      {$set: {userStatus:status }},
      {"new":true, "upsert": false, "runValidators": true}
    )
    const findUsersUpdated = await EntityUser.find({_id:[...ids]}).select({_id:1, userStatus:1})

    let bulkUpdateEs
    if(findUsersUpdated.length > 0){
      const dataForEs = findUsersUpdated.map(d => d._id)
      bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType: "entityusers", ids: dataForEs, tenantId: user.tenantId})
    }
    res.json({done: true, error: "", success: "Updated Successfully", respose:{findUsersUpdated,bulkUpdateEs}})
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", message:"Unable to edit status"})
  }
}


export const updateUser = async(req, res) => {
  const {user} = req
  const {userDetails, duplicateInfo} = req.body
  const {userId} = req.params
  const resRequested = [
    {
      resource: "EntityUser",
      access: 2
    }
  ]
  try {
    const { _id }  = user
    let allowedId = (userId && userId.toString() === _id.toString()) || false
    if(userId && !allowedId){
      allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"edit")
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity Users"})
      }
    }
    /*  const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled && !allowedId) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    } */

    const {userEmails} = userDetails

    if (duplicateInfo) {
      if (duplicateInfo.duplicateExists) {
        let {userEmails} = userDetails
        userEmails = userEmails.map(email => email.emailId)
        const {duplicateEmailIds, duplicateExists} = duplicateInfo
        const duplicateEmailIdsUniq = [...new Set(duplicateEmailIds.map(email => email.emailId))]
        const duplError = []
        userEmails.forEach((email, idx) => {
          if (duplicateEmailIdsUniq.indexOf(email) !== -1)
            duplError.push({email, value: idx, label: `${email} exists already`})
        })
        res
          .status(422)
          .send({
            done: false,
            error: "Duplicates emails are found",
            success: "",
            duplicateExists,
            duplError,
            isPrimaryAddress: true
          })
        return false
      }
    }

    if (userEmails && userEmails.length) {
      const emails = []
      userEmails.forEach(email => {
        if (email.emailPrimary) {
          emails.push(email.emailId)
        }
      })

      const userDetailResult = await EntityUser
        .findById(userId)
        .select({userLoginCredentials: 1})

      const {userLoginCredentials} = userDetailResult
      const {userEmailId, onboardingStatus} = userLoginCredentials
      if (onboardingStatus !== "created" && !emails.includes(userEmailId)) {
        res
          .status(422)
          .send({done: false, error: "Primary emails can not be changed.", success: "", requestedServices: resRequested})
        return false
      }

      if (onboardingStatus === "created") {
        userLoginCredentials.userEmailId = emails[0]
        userDetails.userLoginCredentials = userLoginCredentials
      }
    }

    const entityResult = await Entity.findById(userDetails.entityId).select("primaryContactId")
    if (userDetails && userDetails.userFlags){
      let entityUsers = []
      entityUsers = await EntityUser.find({entityId: userDetails.entityId, userFlags: "Primary Contact"}).select("userFlags")
      if(entityUsers && entityUsers.length && !userDetails.userFlags.includes("Primary Contact") && (entityResult && entityResult.primaryContactId)){
        const index = entityUsers.findIndex(e => e._id.toString() === entityResult.primaryContactId.toString())
        if(index !== -1){
          entityUsers.splice(index, 1)
        }
      }
      if(entityUsers && entityUsers.length){
        const entityUsersIds = entityUsers.map(e => e._id.toString() )
        await EntityUser.updateMany({_id: { $in: entityUsersIds } },   {$pull: {userFlags: "Primary Contact"}})
      }
    }

    const result = await EntityUser.findByIdAndUpdate(userId, {
      $set: {
        ...userDetails
      }
    }, {
      new: true,
      projection: {
        "userLoginCredentials": 0
      }
    })

    if(userDetails && userDetails.userFlags){
      const isPrimaryContact = userDetails && userDetails.userFlags.indexOf("Primary Contact") !== -1
      const primaryContactId = entityResult && entityResult.primaryContactId || ""
      const id = result && result._id || ""
      if(isPrimaryContact){
        await Entity.updateOne({ _id: ObjectID(result.entityId) || ""}, {$set: {primaryContactId: result._id} })
      } else if(primaryContactId.toString() === id.toString() && !isPrimaryContact){
        await Entity.updateOne({ _id: ObjectID(result.entityId) || ""}, {$unset: {primaryContactId: result._id} })
      }
    }

    await returnFromES(req.user.tenantId, result._id)
    await updateEligibleIdsForLoggedInUserFirm(req.user)

    res.json({done: true, error: "", success: result})
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: resRequested})
  }
}
