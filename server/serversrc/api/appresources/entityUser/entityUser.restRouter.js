import express from "express"
import entityUserController from "./entityUser.controller"
import { getUsers, saveUser, updateUser, getUserById, getUsersForEntity,updateUserStatusFlag } from "./entityUser.controller.new"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import { duplicateEmailCheckMiddlewareForUserUpdate} from "./duplicateEmailCheckMiddleware"

export const entityUserRouter = express.Router()

entityUserRouter.route("/firms")
  .post(requireAuth,entityUserController.signUpSTPUserOnCreateMiddleware, entityUserController.saveUserDetail)

entityUserRouter.route("/teststpuser")
  .post(entityUserController.signUpSTPUserOnCreateMiddleware, async(req,res) => {
    console.log("4. I AM BACK IN THE CONTROLLER AFTER THE MIDDLEWARE", JSON.stringify(req.body,null,2))
    res.send(req.body)
  })

entityUserRouter.route("/firms/:userId")
  .get(requireAuth,entityUserController.getFirmUserById)
  .put(requireAuth, entityUserController.updateUserDetailById)

entityUserRouter.route("/third-parties/:userId")
  .get(requireAuth,entityUserController.getThirdPartyFirmUserById)
  .put(requireAuth, entityUserController.updateUserDetailById)

entityUserRouter.route("/entity/:entityId")
  .get(requireAuth, entityUserController.getUsersByEntity)

entityUserRouter.route("/entitySupplier/:finAdvisorId/:serviceType")
  .get(requireAuth, entityUserController.getUsersByAdvisorIdAndServiceType)

entityUserRouter.param("id", entityUserController.findByParam)

entityUserRouter.route("/userAddOns/:userId")
  .put(requireAuth, entityUserController.updateUserAddOns)

entityUserRouter.route("/user/userslist")
  .post(requireAuth, entityUserController.searchUsersByName)

entityUserRouter.route("/checkPlatformAdmin")
  .get(requireAuth,entityUserController.checkPlatFormAdmin)

entityUserRouter.route("/checkSTPEligible")
  .get(requireAuth,entityUserController.checkIfUserIsSTPEligible)
  // .get(entityUserController.checkIfUserIsSTPEligible)

entityUserRouter.route("/search-configs")
  .post(requireAuth, entityUserController.getSearchConfigs)

entityUserRouter.route("/document/:userId")
  .post(requireAuth, entityUserController.postUserDocuments)
  .put(requireAuth, entityUserController.putUserDocumentStatus)
  .delete(requireAuth, entityUserController.pullUserDocument)

entityUserRouter.route("/get-users-by-flags")
  .post(requireAuth, entityUserController.getUsersByFlags)

entityUserRouter.route("/users")
  .get(requireAuth, getUsers)
  .post(requireAuth, entityUserController.signUpSTPUserOnCreateMiddleware, saveUser)

entityUserRouter.route("/users/:userId")
  .get(requireAuth, getUserById)
  .post(requireAuth, duplicateEmailCheckMiddlewareForUserUpdate,updateUser)

entityUserRouter.route("/get-entity-users")
  .post(requireAuth, getUsersForEntity)

entityUserRouter.route("/update-user-status")
  .post(requireAuth, updateUserStatusFlag)
