import { checkDuplicateEmailForTenant } from "./../../commonDbFunctions/checkDuplicateEmailForTenant"
import {EntityUser} from "./entityUser.model"

export const duplicateEmailCheckMiddlewareForUserUpdate = async (req,res,next) => {
  console.log("1. ENTERED THE DUPLICATE EMAIL CHECK FUNCTION", JSON.stringify(req.body,null,2))
  const userInfo = req && req.body
  const {userId} = req.params
  const user = userInfo && userInfo.userDetails
    
  if ( !userId ) {
    // The request body seems to be empty let us do something with this.
    next()
  }
  const {_doc:userInfoForCheckingDuplicates} = await EntityUser.findOne({_id:userId}).select({_id:1,userId:1, entityId:1})
  const userEmails = [...user && user.userEmails || []]
  const duplicateInfo = await checkDuplicateEmailForTenant(userInfoForCheckingDuplicates,userEmails,userId)
  console.log("The duplicate information", JSON.stringify(duplicateInfo,null,2))
  req.body = { userDetails:{...user }, duplicateInfo}
  next()
}

/*

{"userDetails":{"userEmails":[{"_id":"5c6503d39063814025f63dbe","emailId":"user0_finadvisor1@tampa.com","emailPrimary":true},{"_id":"5c04716079475a4344ab6cfe","emailId":"Tristian.Skiles27@gmail.com","emailPrimary":false},{"emailId":"user0@finadvisor1.com","emailPrimary":false}],"userPhone":[{"_id":"5c04716079475a4344ab6cfb","phoneNumber":"1-863-232-8323 x5086","extension":111729,"phonePrimary":true},{"_id":"5c04716079475a4344ab6cfa","phoneNumber":"(081) 570-0315 x94259","extension":421066,"phonePrimary":false}],"userFax":[{"_id":"5c04716079475a4344ab6cfd","faxNumber":"654.538.4589 x6874","faxPrimary":true},{"_id":"5c04716079475a4344ab6cfc","faxNumber":"239-563-2360 x75377","faxPrimary":false}]}}

*/