import clonedeep from "lodash.clonedeep"
import {generateControllers} from "../../modules/query"
import {EntityUser} from "./entityUser.model"
import {EntityRel} from "../entityRel/entityRes.model"
import {validateUserDetail} from "../../Validations/userDetail"
import {canUserPerformAction} from "../../commonDbFunctions"
import { getAllEligibleActivityForSearch } from "../../commonDbFunctions/getActivityForSearch"
import {isUserPlatformAdmin} from "../../commonDbFunctions/isUserPlatformAdmin"
import {checkIfUserIsSTPEligible,signUpSTPUserOnCreate} from "./clientThirdPartySTPServices"
import {getAllEligibleUserIds, doesIdExistsInEntitledUsers,updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {Entity} from "../entity/entity.model"
import moment from "moment"



const {ObjectID} = require("mongodb")

const getUsersById = () => async (req, res, next) => {
  const {user} = req
  const resRequested = [
    {resource:"EntityUser",access:1}
  ]
  const {entityId} = req.params
  try {
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const users = await EntityUser.find({entityId}).select({
      _id: 1,
      userFirstName: 1,
      userMiddleName: 1,
      userLastName: 1
    })
    res.json(users)
  }catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}
const getUsersByEntity = () => async (req, res, next) => {
  const {user} = req
  const resRequested = [
    {resource:"EntityUser",access:1}
  ]
  const {entityId} = req.params
  try {
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const users = await EntityUser.find({entityId}).select({
      _id: 1,
      userFirstName: 1,
      userMiddleName: 1,
      userLastName: 1
    })
    res.json(users)
  }catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const getUsersByAdvisorIdAndServiceType = () => async (req, res, next) => {
  try {
    const {finAdvisorId,serviceType} = req.params
    /* let serviceTypes = []
    if (serviceType.indexOf("Bank Loans And Lease") !== -1) {
      serviceTypes = ["Bank Loans / Lease"] || []
    } else {
      serviceTypes = (serviceType && serviceType.split(",")) || []
    } */
    const serviceTypes = (serviceType && decodeURIComponent(serviceType).split(",")) || []

    const {issuerId, tranType} = req.query
    const query = [{
      $match: {
        relationshipType: "Third Party",
        entityParty1: ObjectID(finAdvisorId)
      }
    },
    {
      $project: {
        "entityParty1": 1,
        "entityParty2": 1
      }
    },
    {
      "$lookup": {
        "from": "entities",
        "localField": "entityParty1",
        "foreignField": "_id",
        "as": "faDetails"
      }
    },
    {
      $unwind: "$faDetails"
    },
    {
      $project: {
        "entityParty2": 1,
        "faDetails.firmName": 1,
        "faDetails._id": 1
      }
    },
    {
      "$lookup": {
        "from": "entities",
        "localField": "entityParty2",
        "foreignField": "_id",
        "as": "thirdPartyDetails"
      }
    },
    {
      $project: {
        "faDetails.firmName": 1,
        "faDetails._id": 1,
        "thirdPartyDetails.firmName": 1,
        "thirdPartyDetails._id": 1,
        "thirdPartyDetails.entityFlags": 1
      }
    },
    {
      $match: {
        "thirdPartyDetails.entityFlags.marketRole": {
          $in: serviceTypes
        }
      }
    },
    {
      $project: {
        "thirdPartyDetails.entityFlags": 1,
        "thirdPartyDetails.firmName": 1,
        "thirdPartyDetails._id": 1,
      }
    },
    {
      $unwind: "$thirdPartyDetails"
    },
    {
      "$group": {
        "_id": "$faDetails",
        "thirdPartyDetails": {
          "$push": "$thirdPartyDetails"
        }
      }
    },
    {
      $project: {
        _id: 0,
        faDetails: "$_id",
        thirdPartyDetails: 1
      }
    }
    ]
    if(serviceType === "All") {
      query.splice(7, 1)
    }

    const aggReturn = await EntityRel.aggregate(query)
    const entityIds = (aggReturn[0] && aggReturn[0].thirdPartyDetails.map(e =>
      e._id)) || [] //eslint-disable-line

    if(serviceType === "All") {
      entityIds.push(finAdvisorId)
      if(issuerId){
        entityIds.push(issuerId)
      }
    }

    const findQuery = {
      entityId: {
        $in: entityIds
      }
    }

    /* if(tranType === "RFP"){
      findQuery.userRole = {
        $in: ["tran-edit"]
      }
    } */

    const entitiesUsers = await EntityUser.find(findQuery).populate({
      path: "entityId",
      model: "entity",
      select: "firmName msrbRegistrantType"
    }).select(
      "userFirstName userMiddleName userLastName userRole userPhone userFax userEmails entityId userFlags userAddresses userStatus"
    )
    const data = {
      thirdPartyDetails: (aggReturn[0] && aggReturn[0].thirdPartyDetails) || [],
      userList: entitiesUsers
    }
    res.json(data)
  } catch (error) {
    next(error)
  }
}

const getUserDetailsById = (id) => EntityUser.findById(id)


const saveUserDetail = () => async(req, res) => {
  const {user} = req
  const {userDetails, duplicateExists} = req.body
  const {_id} = userDetails
  const resRequested = [
    {resource:"EntityUser",access:2}
  ]
  try {
    if(_id){
      const allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"edit")
      if (!allowedId) {
        res.status(500).send({error:"Not Authorized to access Entity Users"})
      }
    }
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",isPrimaryAddress:true,duplicateExists:false,requestedServices:resRequested})
    }
    if(duplicateExists){
      let {userEmails} = userDetails
      userEmails = userEmails.map(email=>email.emailId)
      let {duplicateEmailIds} = req.body
      duplicateEmailIds = duplicateEmailIds.map(email=>email.emailId)
      duplicateEmailIds = [...new Set(duplicateEmailIds)]
      const duplError=[]
      userEmails.forEach((email,idx) => {
        if(duplicateEmailIds.indexOf(email)!==-1)
          duplError.push({value:idx,label:`${email} is already exists`})
      })
      res.status(422).send({done:false,error:"Duplicates emails are found",success:"",duplicateExists,duplError,isPrimaryAddress:true})
      return false
    }
    const {userAddresses} = userDetails
    const primaryAddress = userAddresses.filter(item=>item.isPrimary)
    const cloneUserDetails = clonedeep(userDetails)
    delete cloneUserDetails.userLoginCredentials
    const errorData = validateUserDetail(cloneUserDetails)
    if(errorData.error || primaryAddress.length!==1){
      let isPrimaryAddress = false
      if(primaryAddress.length===1)
        isPrimaryAddress = true
      res.status(422).send({done:false,error:errorData.error,success:"",isPrimaryAddress,duplicateExists:false})
    }else{
      userDetails._id = userDetails._id ? userDetails._id : ObjectID()
      let result = await EntityUser.create(userDetails)
      result = await EntityUser.findById(result._id).select({userLoginCredentials:0})
      await updateEligibleIdsForLoggedInUserFirm(req.user)
      res.status(200).json({done:true,error:"",success:result,isPrimaryAddress:true,duplicateExists:false})
    }
  }
  catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",duplicateExists:false,isPrimaryAddress:true})
  }
}

const updateUserDetailById = () => async (req, res, next) => {
  const {user} = req
  const {userDetails, mergeAddresses} = req.body
  const {userId} = req.params
  const resRequested = [
    {resource:"EntityUser",access:2}
  ]
  try {
    if(userId){
      const allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"edit")
      if (!allowedId) {
        res.status(500).send({error:"Not Authorized to access Entity Users"})
      }
    }
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const primaryAddress = mergeAddresses.filter(item=>item.isPrimary)
    const errorData = validateUserDetail(userDetails)
    if(errorData.error || primaryAddress.length!==1){
      if(primaryAddress.length===1)
        errorData.isPrimaryAddress=true
      else
        errorData.isPrimaryAddress=false
      res.json(errorData)
    }else{
      userDetails.userAddresses = mergeAddresses
      const {userEmails} = userDetails
      const emails = []
      userEmails.forEach(email => {
        if(email.emailPrimary){
          emails.push(email.emailId)
        }
      })
      const userDetailResult = await EntityUser.findById(userId)
      const {userLoginCredentials} = userDetailResult
      const {userEmailId,onboardingStatus} = userLoginCredentials
      if(onboardingStatus!=="created" && !emails.includes(userEmailId)){
        res.status(422).send({done:false,error:"Primary emails can not be changed.",success:"",requestedServices:resRequested})
        return false
      }
      if(onboardingStatus==="created") {
        userLoginCredentials.userEmailId = emails
        userDetails.userLoginCredentials = userLoginCredentials
      }
      const result = await EntityUser.findByIdAndUpdate(userId, userDetails, {
        new: true,
        upsert: true,
        projection: { "userLoginCredentials" : 0}
      })
      await updateEligibleIdsForLoggedInUserFirm(req.user)
      res.status(201).json({result, isPrimaryAddress:true})
    }
  }catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const searchUsersByName = () => async (req, res, next) => {
  const{user} = req
  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EntityUser",access:1},
    {resource:"EntityRel",access:1},
    {resource:"Deals",access:1},
    {resource:"RFP",access:1}
  ]
  try {
    const entitled = await canUserPerformAction(user,resRequested)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const { userName } = req.params
    const searchResult = await EntityUser.aggregate([
      {
        "$lookup": {
          "from": "entity",
          "localField": "entityId",
          "foreignField": "_id",
          "as": "fDetails"
        }
      },
      {
        $project: {
          fullName: { $concat: [ "$userFirstName"," ","$userMiddleName"," ","$userLastName"] },
          id:"$_id",
          isMuniVisorClient:"$fDetails.isMuniVisorClient"
        }
      },
      {$match : {$or:[
        { "fullName":{$regex:userName,$options:"i"},
          "isMuniVisorClient":true
        }
      ]}},
    ]
    )
    res.json(searchResult)
  } catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}


const getFirmUserById = ()=>async (req, res,next)=>{
  const {user} = req
  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EntityUser",access:1},
    {resource:"EntityRel",access:1},
    {resource:"Deals",access:1},
    {resource:"RFP",access:1}
  ]
  const {userId} = req.params
  try {
    if(userId){
      const allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"view")
      if (!allowedId) {
        res.status(500).send({error:"Not Authorized to access Entity Users"})
      }
    }
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    let result = null
    result = await EntityUser.findById(userId).select("_id entityId userFlags userRole userEntitlement userFirmName userFirstName userMiddleName userLastName userEmails userPhone userFax userEmployeeID userExitDate userJobTitle userManagerEmail userJoiningDate userCostCenter userAddresses userAddOns userLoginCredentials userDocuments")

    const primaryEmails ={}
    primaryEmails.isPrimary = false
    primaryEmails.emailId = ""
    if(result.userLoginCredentials && result.userLoginCredentials.onboardingStatus!=="created"){
      primaryEmails.isPrimary = true
      primaryEmails.emailId = result.userLoginCredentials.userEmailId
    }
    result = JSON.parse(JSON.stringify(result))
    delete result.userLoginCredentials
    res.json({userDetails:result,primaryEmails})
  }catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}
const getThirdPartyFirmUserById = ()=>async (req, res,next)=>{
  const {user} = req
  const {userId} = req.params
  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EntityUser",access:1},
    {resource:"EntityRel",access:1},
    {resource:"Deals",access:1},
    {resource:"RFP",access:1}
  ]
  try {
    if(userId){
      const allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"view")
      if (!allowedId) {
        res.status(500).send({error:"Not Authorized to access Entity Users"})
      }
    }
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    let result = null
    result = await EntityUser.findById(userId).select("_id entityId userFlags userRole userEntitlement userFirmName userFirstName userMiddleName userLastName userEmails userPhone userFax userEmployeeID userEmployeeType userJobTitle userManagerEmail userDepartment userCostCenter userAddresses userLoginCredentials")
    const primaryEmails ={}
    primaryEmails.isPrimary = false
    primaryEmails.emailId = ""
    if(result.userLoginCredentials && result.userLoginCredentials.onboardingStatus!=="created"){
      primaryEmails.isPrimary = true
      primaryEmails.emailId = result.userLoginCredentials.userEmailId
    }
    result = JSON.parse(JSON.stringify(result))
    delete result.userLoginCredentials
    res.json({userDetails:result,primaryEmails})
  }catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const entityUsersList = ()=>async(req, res, next)=>{
  const {user} = req
  const {userId} = req.params
  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EntityUser",access:1},
    {resource:"EntityRel",access:1},
    {resource:"Deals",access:1},
    {resource:"RFP",access:1}
  ]
  try {
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const result = await EntityUser.findById(userId).select("_id entityId userFlags userRole userEntitlement userFirmName userFirstName userMiddleName userLastName userEmails userPhone userFax userEmployeeID userEmployeeType userJobTitle userManagerEmail userDepartment userCostCenter userAddresses")
    res.json(result)
  }catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const getSearchConfigs = () => async (req, res) => {
  try {
    const data = await getAllEligibleActivityForSearch(req.body)
    res.json(data)
  } catch (err) {
    res.json(err)
  }
}

const checkPlatFormAdmin = () => async (req, res) => {
  try {
    const response = await isUserPlatformAdmin(req.user)
    res.json(response)
  } catch (err) {
    res.json(err)
  }
}
const updateUserAddOns = () => async (req, res) => {
  const {userAddOns} = req.body
  const {userId} = req.params
  const resRequested = [
    {resource:"EntityUser",access:2},
  ]
  try {
    if(userId){
      const allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"edit")
      if (!allowedId) {
        res.status(500).send({error:"Not Authorized to access Entity Users"})
      }
    }
    const entitled = await canUserPerformAction(req.user,resRequested)
    if( !entitled) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const result = await EntityUser.findByIdAndUpdate(ObjectID(userId),
      { "$set":{userAddOns}},{
        upsert:true
      })
    res.json(result)
  } catch (error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

export const putUserDocumentStatus = () => async (req, res) => {
  const { userId } = req.params
  const {type} = req.query

  try {
    const { _id, docStatus } = req.body

    if(type === "status") {
      await EntityUser.updateOne(
        { _id: ObjectID(userId), "userDocuments._id": ObjectID(_id)},
        { $set: { "userDocuments.$.docStatus" : docStatus } }
      )
    }else if(type === "meta") {
      req.body.LastUpdatedDate = moment(new Date()).format("MM/DD/YYYY hh:mm A")
      await EntityUser.updateOne(
        { _id: ObjectID(userId), "userDocuments._id": ObjectID(_id)},
        { $set: { "userDocuments.$" : req.body } }
      )
    }
    const document = await EntityUser.findById(userId).select("userDocuments")
    res.status(200).json(document)
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})
  }
}

export const postUserDocuments = () => async (req, res) => {
  const { userId } = req.params
  try {

    await EntityUser.update({ _id: userId}, {$addToSet: {userDocuments: req.body.userDocuments} })
    const document = await EntityUser.findOne({_id: userId}).select("userDocuments")

    res.status(200).json(document)
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

export const pullUserDocument = () => async (req, res) => {
  const { userId } = req.params
  const { docId } = req.query
  try {

    await EntityUser.update({ _id: userId}, { $pull: { userDocuments: { _id: docId } }})
    const document = await EntityUser.findById({_id: userId}).select("userDocuments")

    if(document) {
      res.status(200).json(document)
    }else {
      res.status(422).send({done:false,error:"This Document doesn't exists."})
    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

const getUsersByFlags = () => async (req, res) => {
  const { user } = req
  const { flags } = req.body
  if(!user || !user.entityId) {
    res.status(422).send({done:false,error:"entity id not found for the user"})
  }

  if(!flags || !flags.length) {
    res.status(422).send({done:false,error:"no user flags provided in the request"})
  }

  try {
    const users = await EntityUser.find({
      entityId: user.entityId,
      userFlags: {$all: flags }
    }).select({ userLoginCredentials: 0 })
    res.json(users)
  } catch(error) {
    res.status(422).send({done:false, error })
  }
}

export default generateControllers(EntityUser, {
  getUsersByAdvisorIdAndServiceType: getUsersByAdvisorIdAndServiceType(),
  getUsersByEntity: getUsersByEntity(),
  saveUserDetail: saveUserDetail(),
  updateUserDetailById: updateUserDetailById(),
  searchUsersByName: searchUsersByName(),
  getFirmUserById:getFirmUserById(),
  getThirdPartyFirmUserById:getThirdPartyFirmUserById(),
  entityUsersList:entityUsersList(),
  getSearchConfigs: getSearchConfigs(),
  checkPlatFormAdmin: checkPlatFormAdmin(),
  updateUserAddOns:updateUserAddOns(),
  checkIfUserIsSTPEligible:checkIfUserIsSTPEligible(),
  signUpSTPUserOnCreateMiddleware:signUpSTPUserOnCreate(),
  postUserDocuments:postUserDocuments(),
  putUserDocumentStatus:putUserDocumentStatus(),
  pullUserDocument:pullUserDocument(),
  getUsersByFlags:getUsersByFlags()
})
