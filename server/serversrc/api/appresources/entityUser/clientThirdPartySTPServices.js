
import isUndefined from "lodash/isUndefined"
import bcrypt from "bcrypt-nodejs"
import  crypto from "crypto"
import isEmpty from "lodash/isEmpty"


import { EntityRel } from "../models"
import { checkDuplicateEmailForTenant } from "./../../commonDbFunctions/checkDuplicateEmailForTenant"

const {ObjectID} = require("mongodb")
// Pass the Object when Updating or Creating the User for Client or Third Party

export const signUpSTPUserOnCreate = () => async (req,res,next) => {

  console.log("1. ENTERED STP FUNCTION", JSON.stringify(req.body,null,2))

  // Define the types of relationships that would qualify for STP
  const STPRELTYPES = [ "Client","Prospect","Third Party"]

  // extract the user from the request Object
  const userInfo = req && req.body
  const user = userInfo && userInfo.userDetails

  if ( !user ) {
    // The request body seems to be empty let us do something with this.
    next()
  }

  // is the user existing or not
  const doesUserExist = user && user._id
  
  // if exists get the entity and validate whether the entity is part
  const doesUserHaveEntityId = user && user.entityId

  // Is the user already STP or has the user already onboarded.
  // const {userLoginCredentials:{isUserSTPEligible}} = user
  const loginDetailsInRequest = {...user && user.userLoginCredentials}
  const userEmails = [...user && user.userEmails || []]

  let userQualifiesForSTP = false
  let duplicateInfo = {duplicateEmailIds:[], duplicateExists:false}
  
  if ( doesUserHaveEntityId ) {
    const retEntity = await EntityRel.aggregate([
      {"$match": {entityParty2:ObjectID(user.entityId), "relationshipType": {"$in": [...STPRELTYPES]}}}
    ])
    console.log("1.1. QUALIFICATION PROCESS CHECK WHAT WAS RETURNED", JSON.stringify(retEntity,null,2))

    userQualifiesForSTP = retEntity.length > 0
    duplicateInfo = await checkDuplicateEmailForTenant(user,userEmails,user._id)
  }

  console.log("2. USER QUALIFIED FOR STP", JSON.stringify(user,null,2))
  

  // Extract the Primary Email into this one
  const primaryEmail = userEmails.filter( ({emailPrimary}) => emailPrimary)
  const userLoginEmail = primaryEmail[0].emailId
  console.log("loginDetailsInRequest===>>>",loginDetailsInRequest)
  const isUserSTPEligible = loginDetailsInRequest && loginDetailsInRequest.isUserSTPEligible
  console.log("2.1 DOES THE USER ALREADY HAVE STP ELIGIBILITY INFORMATION", JSON.stringify(isUserSTPEligible,null,2))

  // If Login details doesn't exist in request then just initialize with an empty object

  // Identify if the data needs to be inserted.
  const setStp = !!(((isUndefined(isUserSTPEligible) && userQualifiesForSTP) || ( !doesUserExist && userQualifiesForSTP ) || (isUserSTPEligible)))
  
  // 1. If there is no user ID then check entity Id
  let revisedLoginDetails = {...loginDetailsInRequest}
  console.log("3. DO WE NEED TO WRITE INFORMATION IN THE DB", JSON.stringify(setStp,null,2))

  if (setStp){
    console.log("3.1 I ENTERED THE FUNCTION")
    const authSTPPassword = crypto.randomBytes(64).toString("hex")
    const authSTPToken = crypto.randomBytes(40).toString("hex")
    console.log("3.2 WERE THE TOKENS GENERATED", JSON.stringify({authSTPPassword,authSTPToken},null,2))
    
    const salt = bcrypt.genSaltSync(10)
    const stpHashedPassword = bcrypt.hashSync(authSTPPassword, salt )
    console.log("3.3 DID WE GET ALL THE PASSWORDS CREATED", JSON.stringify({authSTPPassword,authSTPToken,stpHashedPassword},null,2))


    revisedLoginDetails={
      ...loginDetailsInRequest,
      userEmailId:userLoginEmail,
      isUserSTPEligible:true,
      password:stpHashedPassword,
      onboardingStatus : "created",
      userEmailConfirmString:null,
      userEmailConfirmExpiry:null,
      authSTPToken,
      authSTPPassword,
    }
  }  else {
    revisedLoginDetails={
      ...loginDetailsInRequest,
      onboardingStatus : "created",
      userEmailId:userLoginEmail,
      isUserSTPEligible:false,
      password:null,
      authSTPToken:null,
      authSTPPassword:null,
    }
  }
  req.body = { userDetails:{...user, userLoginCredentials:{...revisedLoginDetails}}, ...duplicateInfo }
  next()
}

export const checkIfUserIsSTPEligible = () => async (req, res) => {
  // this middleware needs to be chained after the require Authentication
  const { userId } = req.query
  if ( !userId ) {
    console.log("There is no User")
    res.send({stp:false}) 
  }
  // Check if the User belongs to the tenant Umbrella or not.

  const isUserSTPEligible = await EntityRel.aggregate([
    {$match:{entityParty2:ObjectID(req.user.entityId)}},
    {
      $lookup:
            {
              from: "entityrels",
              localField: "entityParty1",
              foreignField: "entityParty1",
              as: "alltenantentities"
            }
    },
    { $unwind : "$alltenantentities"},
    { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}},
    {
      $lookup:
            {
              from: "entityusers",
              localField: "entityId",
              foreignField: "entityId",
              as: "alltenantentityusers"
            }
    },
    {
      $unwind:"$alltenantentityusers"
    },
    {
      $project:{
        entityId:1,
        userId:"$alltenantentityusers._id",
        userLoginCredentials:"$alltenantentityusers.userLoginCredentials",
        stpEligible:{$if:{"alltenantentityusers.userLoginCredentials.isUserSTPEligible":{$exists:true}},then:"$alltenantentityusers.userLoginCredentials.isUserSTPEligible",else:false}
      }
    },
    {"$match": {userId, stpEligible:true}},
    {"$project": {userId:1, entityId:1,stpToken:"$userLoginCredentials.authSTPToken"}}
  ])
  res.send({stpUser:isUserSTPEligible.length > 0, stpUserToken:isEmpty(isUserSTPEligible) ? null : isUserSTPEligible[0]}) 
}
