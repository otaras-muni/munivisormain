import express from "express"
import transactionController from "./transaction.controller"

export const transactionRouter = express.Router()

transactionRouter.param("id", transactionController.findByParam)

transactionRouter.route("/")
  .get(transactionController.getAll)
  .post(transactionController.createOne)

transactionRouter.route("/:id")
  .get(transactionController.getOne)
  .put(transactionController.updateOne)
  .delete(transactionController.createOne)
