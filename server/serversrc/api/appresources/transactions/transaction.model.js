import mongoose from "mongoose"

const transactionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Transaction Must Have a Description"]
  },

})

export const Transaction = mongoose.model("transaction", transactionSchema)
