import { generateControllers } from "../../modules/query"
import { Transaction } from "./transaction.model"

export default generateControllers(Transaction)
