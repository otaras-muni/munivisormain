import express from "express"
import {
  readTransactionInfoAndSendEmails,
  sendEmailsAfterCreatingTransactions,
  sendEmailGeneric,
  sendOnboardingEmailForNewUser,
  sendEmailRelatedToCRMAndCompliance,
  sendAttachment
} from "./email.controller"

import {requireAuth} from "./../../../authorization/authsessionmanagement/auth.middleware"

export const sendEmailRouter = express.Router()
sendEmailRouter.route("/sendtranemail")
  .post(requireAuth,readTransactionInfoAndSendEmails)
  // .post(requireAuth,(req,res) => res.send({data:req.body, user:req.user}))

sendEmailRouter.route("/createtranemail")
  .post(requireAuth,sendEmailsAfterCreatingTransactions)

sendEmailRouter.route("/sendmail")
  .post(requireAuth,sendEmailGeneric)

sendEmailRouter.route("/sendonboardingemail/:onboardedUserId")
  .post(requireAuth,sendOnboardingEmailForNewUser)

sendEmailRouter.route("/sendcomplianceemail")
  .post(requireAuth,sendEmailRelatedToCRMAndCompliance)

sendEmailRouter.route("/sendcrmemail")
  .post(requireAuth,sendEmailRelatedToCRMAndCompliance)

sendEmailRouter.route("/send-attachment")
  .post(sendAttachment)
