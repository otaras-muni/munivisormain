import ip from "ip"

import { sendAWSEmail, sendAWSEmailWithAttachment, sendSESEmail} from "./../../modules/sendAwsSESEmails"
import isEmpty from "lodash/isEmpty"
import uniqBy from "lodash/uniqBy"
import { EntityUser } from "../entityUser"
import {
  EntityRel, AuditLog, Deals, RFP, BankLoans, Derivatives, ActMaRFP, Others, ActBusDev,
  SuperVisoryObligations,
  ProfQualifications,
  PoliticalContributions,
  GiftsAndGratuities,
  GeneralAdmin,
  ComplSupervisor,
  ComplaintDetails,
  BusConduct, Config
} from "./../models"
import { getSupervisorDetails } from "./../../commonDbFunctions"
import {getS3DownloadURLs, getFilesFromS3} from "./../integrations/s3/helpers"


const {ObjectID} = require("mongodb")

const sendEmailConfig = process.env.SENDEMAILS
const frontEndUrl = process.env.FRONTEND_URL

console.log("SEND EMAIL CONFIG", sendEmailConfig)


const getTransactionDescription = async(processId) => {

  const dealDesc = await Deals.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$dealIssueTranIssueName",""]},{"$eq":["$dealIssueTranIssueName",null]}]},
            then:"$dealIssueTranProjectDescription",
            else:"$dealIssueTranIssueName"},
        },
        clientActive:1
      }
    }])

  const rfpDesc = await RFP.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$rfpTranIssueName",""]},{"$eq":["$rfpTranIssueName",null]}]},
            then:"$rfpTranProjectDescription",
            else:"$rfpTranIssueName"},
        },
        clientActive:1

      }
    }
  ])

  const loansDesc = await BankLoans.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
            then:"$actTranProjectDescription",
            else:"$actTranIssueName"},
        },
        clientActive:1
      }
    }
  ])

  const derivDesc = await Derivatives.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
            then:"$actTranProjectDescription",
            else:"$actTranIssueName"},
        },
        clientActive:1

      }
    }
  ])

  const maRfpDesc = await ActMaRFP.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actIssueName",null]}]},
            then:"$actProjectName",
            else:"$actIssueName"},
        },
        clientActive:1
      }
    }
  ])

  const otherDesc = await Others.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranProjectDescription",null]}]},
            then:"$actTranProjectDescription",
            else:"$actTranIssueName"},
        },
        clientActive:1
      }
    }
  ])

  const busDevDesc = await ActBusDev.aggregate([
    {
      $match : { _id:ObjectID(processId)}
    },
    {
      $project: {
        processProjectDesc : {
          $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actProjectName",null]}]},
            then:"$actProjectName",
            else:"$actIssueName"},
        },
        clientActive:{$ifNull:["$clientActive",false]},
      }
    }
  ])

  const processInfo = [...dealDesc,...derivDesc,...rfpDesc,...loansDesc,...maRfpDesc,...otherDesc,...busDevDesc]
  return processInfo[0]
}

const getQueryConfig =(type, allTenantUsersPrimaryEmailId) => {
  const queryConfigHelper = {
    deals:{
      lkup:{
        from: "tranagencydeals",
        localField: "entityParty1",
        foreignField: "dealIssueTranClientId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.dealIssueUnderwriters.dealPartFirmId",
        "$transactions.dealIssueParticipants.dealPartFirmId",
        ["$transactions.dealIssueTranIssuerId","$transactions.dealIssueTranClientId"]
      ],
      userinfo:[
        "$transactions.dealIssueParticipants.dealPartContactId",
        "$transactions.dealIssueTranAssignedTo"
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:"$transactions.dealIssueParticipants.dealPartContactId" },
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map: {
                  input: "$transactions.dealIssueParticipants",
                  as: "participants",
                  in: {
                    id: "$$participants.dealPartContactId" ,
                    emailId: "$$participants.dealPartContactEmail",
                    entityId:"$$participants.dealPartFirmId"
                  }
                }
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        }

        // $map: {
        //   input: "$transactions.dealIssueParticipants",
        //   as: "participants",
        //   in: {
        //     id: "$$participants.dealPartContactId" ,
        //     emailId: "$$participants.dealPartContactEmail",
        //     entityId:"$$participants.dealPartFirmId"
        //   }
        // }
      }
    },
    bankloans: {
      lkup:{
        from: "tranbankloans",
        localField: "entityParty1",
        foreignField: "actTranFirmId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.bankLoanParticipants.partFirmId",
        ["$transactions.actTranFirmId","$transactions.actTranClientId"]
      ],
      userinfo:[
        "$transactions.bankLoanParticipants.partContactId",
        ["$transactions.actTranFirmLeadAdvisorId"]
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:"$transactions.bankLoanParticipants.partContactId" },
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map: {
                  input: "$transactions.bankLoanParticipants",
                  as: "participants",
                  in: {
                    id: "$$participants.partContactId" ,
                    emailId: "$$participants.partContactEmail",
                    entityId:"$$participants.partFirmId"
                  }
                }
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        },
      }

    },
    derivatives: {
      lkup:{
        from: "tranderivatives",
        localField: "entityParty1",
        foreignField: "actTranFirmId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.derivativeParticipants.partFirmId",
        ["$transactions.actTranFirmId","$transactions.actTranClientId"]
      ],
      userinfo:[
        "$transactions.derivativeParticipants.partContactId",
        ["$transactions.actTranFirmLeadAdvisorId"]
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:"$transactions.derivativeParticipants.partContactId" },
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map: {
                  input: "$transactions.derivativeParticipants",
                  as: "participants",
                  in: {
                    id: "$$participants.partContactId" ,
                    emailId: "$$participants.partContactEmail",
                    entityId:"$$participants.partFirmId"
                  }
                }
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        }
      }
    },
    marfps: {
      lkup:{
        from: "actmarfps",
        localField: "entityParty1",
        foreignField: "actTranFirmId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.maRfpParticipants.partFirmId",
        ["$transactions.actTranFirmId","$transactions.actIssuerClient"]
      ],
      userinfo:[
        "$transactions.maRfpParticipants.partContactId",
        ["$transactions.actLeadFinAdvClientEntityId"]
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:"$transactions.maRfpParticipants.partContactId" },
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map: {
                  input: "$transactions.maRfpParticipants",
                  as: "participants",
                  in: {
                    id: "$$participants.partContactId" ,
                    emailId: "$$participants.partContactEmail",
                    entityId:"$$participants.partFirmId"
                  }
                }
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        }
      },
    },
    rfps: {
      lkup:{
        from: "tranagencyrfps",
        localField: "entityParty1",
        foreignField: "rfpTranClientId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.rfpParticipants.rfpParticipantFirmId",
        ["$transactions.rfpTranClientId","$transactions.rfpTranIssuerId"]
      ],
      userinfo:[
        "$transactions.rfpParticipants.rfpParticipantContactId",
        "$transactions.rfpEvaluationTeam.rfpSelEvalContactId",
        "$transactions.rfpProcessContacts.rfpProcessContactId",
        "$transactions.rfpTranAssignedTo"
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:{$setUnion:[
        "$transactions.rfpParticipants.rfpParticipantContactId",
        "$transactions.rfpEvaluationTeam.rfpSelEvalContactId",
        "$transactions.rfpProcessContacts.rfpProcessContactId",
      ]}},
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map:{
                  input:"$transactions.rfpParticipants",
                  as:"part",
                  in :{id:"$$part.rfpParticipantContactId",emailId:"$$part.rfpParticipantContactEmail"}}
              },
              {
                $map:{
                  input:"$transactions.rfpEvaluationTeam",
                  as:"part",
                  in :{id:"$$part.rfpSelEvalContactId", emailId:"$$part.rfpSelEvalEmail"}}
              },
              {
                $map:{
                  input:"$transactions.rfpProcessContacts",
                  as:"part",
                  in :{id:"$$part.rfpProcessContactId", emailId:"$$part.rfpProcessContactRealEmailId"}}
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        }
      }
    },
    others:{
      lkup:{
        from: "tranagencyothers",
        localField: "entityParty1",
        foreignField: "actTranFirmId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.actTranUnderwriters.partFirmId",
        "$transactions.participants.partFirmId",
        ["$transactions.actTranClientId","$transactions.actTranFirmId"]
      ],
      userinfo:[
        "$transactions.participants.partContactId",
        ["$transactions.actTranFirmLeadAdvisorId"]
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:"$transactions.participants.partContactId" },
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map: {
                  input: "$transactions.participants",
                  as: "participants",
                  in: {
                    id: "$$participants.partContactId" ,
                    emailId: "$$participants.partContactEmail",
                    entityId:"$$participants.partFirmId"
                  }
                }
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        }

      }
    },
    busdev:{
      lkup:{
        from: "actbusdevs",
        localField: "entityParty1",
        foreignField: "actTranFirmId",
        as: "transactions"
      },
      firminfo:[
        "$transactions.participants.partFirmId",
        ["$transactions.actIssuerClient","$transactions.actTranFirmId"]
      ],
      userinfo:[
        "$transactions.participants.partContactId",
        ["$transactions.actLeadFinAdvClientEntityId"]
      ],
      allParticipantsWithEmail:{tranAllParticipantUsers:"$transactions.participants.partContactId" },
      partEmails:{
        $map:{
          input:{$map:{
            input: {$setUnion: [
              {
                $map: {
                  input: "$transactions.participants",
                  as: "participants",
                  in: {
                    id: "$$participants.partContactId" ,
                    emailId: "$$participants.partContactEmail",
                    entityId:"$$participants.partFirmId"
                  }
                }
              }
            ]
            },
            as:"allparts",
            in:{
              id:"$$allparts.id",
              emailId:"$$allparts.emailId",
              g:{$arrayElemAt:[
                {$filter:{
                  input:allTenantUsersPrimaryEmailId,
                  as:"alltendetails",
                  cond:{$eq:["$$alltendetails.id","$$allparts.id"]}
                }},0]
              }
            }
          }
          },
          as: "final",
          in:{id:"$$final.id",emailId:"$$final.emailId",entityId:"$$final.g.entityId",stpEligible:"$$final.g.stpEligible", authSTPToken:"$$final.g.authSTPToken", relType: "$$final.g.relType" }
        }

      }
    },

  }
  return queryConfigHelper[type]
}

export const getAllTenantUsersWithPrimaryEmailID = async (user, specificEmailIds) => {

  let finalSelectedEmailIds =     {$match:{"userEmails.emailPrimary":true}}

  if (!isEmpty(specificEmailIds)) {

    finalSelectedEmailIds = {$match:{"userEmails.emailPrimary":true,"userEmails.emailId":{$in:[...specificEmailIds]}}}
  }

  let allTenantUsersPrimaryEmailId = []
  try {
    allTenantUsersPrimaryEmailId = await EntityRel.aggregate([
      {$match:{entityParty2:ObjectID(user.entityId)}},
      {
        $lookup:
              {
                from: "entityrels",
                localField: "entityParty1",
                foreignField: "entityParty1",
                as: "alltenantentities"
              }
      },
      { $unwind : "$alltenantentities"},
      { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}},
      {
        $lookup:
              {
                from: "entityusers",
                localField: "entityId",
                foreignField: "entityId",
                as: "alltenantentityusers"
              }
      },
      {
        $unwind:"$alltenantentityusers"
      },
      {
        $project:{
          entityId:1,
          relType: 1,
          userId:"$alltenantentityusers._id",
          userEmails:"$alltenantentityusers.userEmails",
          stpEligible:{$ifNull:["$alltenantentityusers.userLoginCredentials.isUserSTPEligible",false]},
          authSTPToken:{$ifNull:["$alltenantentityusers.userLoginCredentials.authSTPToken",""]}
        }
      },
      {$unwind:"$userEmails"},
      {...finalSelectedEmailIds},
      {$project:{_id:0, id:"$userId",emailId:"$userEmails.emailId", entityId:1, stpEligible:1,authSTPToken:1, relType: 1}}
    ])
    console.log("1.EXTRACTED ALL EMAILS",allTenantUsersPrimaryEmailId[0])
    return {message:"Successfully Extracted Email Information", emailDetails:allTenantUsersPrimaryEmailId}

  }  catch (e) {
    console.log(e)
    return {error:"Error in extracting all the users with primary email",message:"", emailDetails:[]}
  }
}

export const sendEmailHelper = async ( {to,cc,replyto,bodytemplate, subject}) => {

  console.log("*******SEND EMAIL CONFIGURATION", process.env.SENDEMAILS)
  const toRecipientEmailList = (process.env.SENDEMAILS === "Yes") ?  [...to] : [process.env.FROMEMAIL]
  const ccRecipientEmailList = (process.env.SENDEMAILS === "Yes")?  [...cc] : [process.env.REPLYTOEMAIL]
  const replyToRecipientEmailList = (process.env.SENDEMAILS === "Yes") ?  [...replyto] : [process.env.REPLYTOEMAIL]
  console.log("********EMAILS BEING SENT TO",{toRecipientEmailList,ccRecipientEmailList,replyToRecipientEmailList})

  const revisedConfig = {
    to:[...toRecipientEmailList],
    cc:[...ccRecipientEmailList],
    bodytemplate,
    subject,
    replyto:[...replyToRecipientEmailList]
  }

  return revisedConfig
}

export const sendEmailsToParticipants = async ({protocol,ip,host, participantsForEmail,url,tranId,message, subject,currentUserDetails,sendEmailUserChoice,user,supervisorEmails}) => {

  try {
    if(participantsForEmail.length > 0)
    {
      const {processProjectDesc} = await getTransactionDescription(tranId)

      console.log("Now we can send emails")
      // Segment the emails based on the presence of STP Users and non STP Users
      const {emailIdsToSend} = participantsForEmail[0]
      // if stpEligible then we need to send a email with token and the link
      console.log("EMAILS TO BE SENT",JSON.stringify(emailIdsToSend,null,2))

      if ( sendEmailUserChoice && sendEmailConfig )
      {

        let notificationConfig = await Config.findOne({entityId: user.entityId}).select("notifications")
        console.log("notificationConfig----------",notificationConfig)
        // For Firm Users Let us have the default as Send Unless they don't want it explicitly on the configuration

        notificationConfig = {
          Client: (notificationConfig.notifications && notificationConfig.notifications.client) || false,
          Prospect: (notificationConfig.notifications && notificationConfig.notifications.client) || false,
          Self: (notificationConfig.notifications && notificationConfig.notifications.self) || true,
          "Third Party": (notificationConfig.notifications && notificationConfig.notifications.thirdParty) || false
        }

        notificationConfig = Object.keys(notificationConfig).filter(key => notificationConfig[key] && key)
        console.log("GET NOTIFICATIONS CONFIG",JSON.stringify(notificationConfig,null,2))

        const stpEligibleEmailIds = emailIdsToSend.filter( f => f.stpEligible && notificationConfig.indexOf(f.relType) !== -1)
        const emailNotSend = emailIdsToSend.filter( f => notificationConfig.indexOf(f.relType) === -1)

        const emailStatusesForSTPUsers = await (stpEligibleEmailIds || []).reduce(async(aggregatedEmailStatuses, stpEmailDetails) => {
          const newaggregatedEmailStatuses = await aggregatedEmailStatuses
          const {authSTPToken,emailId} = stpEmailDetails
          const revisedUrl = `${protocol}://${host}/auth/stp/?token=${authSTPToken}&redirect=${url}`
          const emailconfig = await sendEmailHelper({
            to:[emailId],
            cc:[currentUserDetails.emailId],
            bodytemplate:`<div>
                    <div>${message}</div>
                    ${sendEmailConfig === "No" ? `<h4>Emails to be Sent to : ${JSON.stringify(emailId,null,2)}</h4>` : ""}
                    <h3>Please get more details by a accessing the <a href=${revisedUrl}>${processProjectDesc}</a></h3>
                </div>`,
            subject,
            replyto:[currentUserDetails.emailId]
          })

          const retStatus = await sendAWSEmail(emailconfig)
          return Promise.resolve([...newaggregatedEmailStatuses,retStatus.status])
        }, Promise.resolve([]))

        // Just get the email IDs if not STP eligible



        const nonStpEligibleEmailIds = emailIdsToSend.filter( f => !f.stpEligible && notificationConfig.indexOf(f.relType) !== -1).map( a => a.emailId)
        console.log("---NON STP ELIGIBLE EMAILS")

        let emailconfig = null
        let retStatusNonSTPUsers = null
        if ( nonStpEligibleEmailIds.length > 0 )
        {
          emailconfig = await sendEmailHelper({
            to:[...nonStpEligibleEmailIds,...supervisorEmails],
            cc:[currentUserDetails.emailId],

            subject,
            bodytemplate:`<div>
              <div>${message}</div>
              ${sendEmailConfig === "No" ? `<h2>Emails to be Sent to : ${JSON.stringify([...nonStpEligibleEmailIds,...supervisorEmails],null,2)}</h2>` : ""}
              <p>Please get more details by a accessing the <a href=${frontEndUrl}/${url}>${processProjectDesc}</a></p>
            </div>`,
            replyto:[currentUserDetails.emailId]
          })
          console.log("----BEFORE SENDING EMAILS", JSON.stringify(emailconfig,null,2))
          retStatusNonSTPUsers = await sendAWSEmail(emailconfig)
          console.log("The return status is", JSON.stringify(retStatusNonSTPUsers,null,2))
        }

        console.log("--------stpEligibleEmailIds-------------",stpEligibleEmailIds)
        if(!nonStpEligibleEmailIds.length && !stpEligibleEmailIds.length && !emailNotSend.length){
          return {done:false, message:"There was an error sending email messages"}
        }

        const consolidatedSendStatus = retStatusNonSTPUsers ? [...emailStatusesForSTPUsers,retStatusNonSTPUsers.status ] : [...emailStatusesForSTPUsers ]

        if(consolidatedSendStatus.includes("fail")) {
          return {done:false,message:"There was an error sending email messages to either STP or Non STP Users"}
        }

        const logs = []

        if(stpEligibleEmailIds.length || nonStpEligibleEmailIds.length){
          logs.push(
            {
              type:ObjectID(tranId),
              groupId: "superAdmin",
              changeLog:[{
                userName:`${user.userFirstName} ${user.userLastName}`,
                ip,
                log:`Email Sent to - ${JSON.stringify([...stpEligibleEmailIds.map(a => a.emailId)],null,2)}`,
                userId:ObjectID(user._id),
                date:new Date()
              }]
            }
          )
        }

        if(emailNotSend.length){
          const email = JSON.stringify([...emailNotSend.map(a => a.emailId),],null,2)
          const relType = emailNotSend.map(a => a.relType)
          const c = []
          relType.forEach(key => {
            if(c.indexOf(key) === -1){
              c.push(key)
            }
          })
          logs.push(
            {
              type:ObjectID(tranId),
              groupId: "superAdmin",
              changeLog:[{
                userName:`${user.userFirstName} ${user.userLastName}`,
                ip,
                log:`Email related to ${email} is part of ${JSON.stringify(c)} and is not configured to receive any notification.`,
                userId:ObjectID(user._id),
                date:new Date()
              }]
            }
          )
        }

        if(logs.length){
          const updateAuditLog = await AuditLog.insertMany(logs)
          console.log("successfully updated the change log",updateAuditLog)
        }

        if(stpEligibleEmailIds.length || nonStpEligibleEmailIds.length){
          return {done:true, message:`Successfully sent email messages to STP and non STP Users - ${JSON.stringify(emailIdsToSend.map(a => a.emailId),null,2)}`}
        }
        const email = JSON.stringify([...emailNotSend.map(a => a.emailId),],null,2)
        const relType = emailNotSend.map(a => a.relType)
        const c = []
        relType.forEach(key => {
          if(c.indexOf(key) === -1){
            c.push(key)
          }
        })
        return {done:false, statusCode: 203, message:`Email related to document to ${email} is part of ${JSON.stringify(c)} and is not configured to receive any notification.`}


        // return {done:true,message:`Successfully sent email messages to STP and non STP Users - ${JSON.stringify(emailIdsToSend.map(a => a.emailId),null,2)}`}
      }
      return {done:true,message:"No Email to be sent out based on env email configuration and user choice from modal"}
    }
    return {done:true,message:"No emails to be found. Exiting from the function"}
  } catch (e) {
    console.log("There was an error with either sending email or updating audit log", e)
    return {done:false, message:"Email update could not be sent because of issues", error:e}
  }
}

export const readTransactionInfoAndSendEmails = async (req,res,next) => {

  // get the relationship type and the user entity - Done
  // get the entitlement from the user object - Done
  // access the transaction based on the mapper and get the information
  // get emails of My Deal Team, My firm Users and Me as a User - Done
  // if a user exists in the assignee list and doesn't exist in emails then get the primary email of the user - Done
  // return the email information in appropriate buckets to the calling function - Done
  // Send email with the relevant body - Done
  // Check if the user is a STP user or not, if yes then reconstruct the URL - Done
  // Check if the user is not a STP user then just pass on the URL that was given - Done
  // Get STP Eligible Flag - Done

  const { tranId, type, sendEmailUserChoice, emailParams:{ url, message,sendEmailTo, category, subject, sendDocEmailLinks, docIds}} = req.body
  const {user} = await req
  const token = req.headers.authorization

  let newRevisedMessage = message

  let sendEmailDetails = []
  if(category === "custom" && isEmpty(sendEmailTo)) {
    res.status(500).send({done:false, message:"You selected to send emails to specific participants and forgot to add emails"})
  }
  else if ( category === "custom" && !isEmpty(sendEmailTo)) {
    const ret = await getAllTenantUsersWithPrimaryEmailID(user,sendEmailTo )
    sendEmailDetails = ret.emailDetails
    console.log("Send Emails to Custom IDs", JSON.stringify(sendEmailDetails))
  }

  // Get the  Signed URL Link to send in the email
  if( sendDocEmailLinks && docIds.length > 0 ) {
    console.log("Entered the Sending Email Links Part", docIds)
    const signedUrls = await getS3DownloadURLs(docIds, token)
    console.log("The S3 download URL returnd the right messages", signedUrls)
    const signedUrlMessage = signedUrls.reduce ( (acc, su) => `${acc}<br/>${su}`, "")
    newRevisedMessage = `${message}
    <hr/>
    ${signedUrlMessage}
    `
    console.log("The New Message that includes email links is",newRevisedMessage )
  }

  // change the body of the email to get the signed URL links for the doc IDs

  console.log(JSON.stringify({body:req.body, user:user._id, entityId:user.entityId},null,2))

  const categoryMap = {
    all:{emailIds:"$allParticipantsEmailIds"},
    myfirm:{emailIds:"$allMyFirmEmailIds"},
    otherfirms:{emailIds:"$allNonFirmUsersEmailIds"},
    onlyme:{emailIds:"$myEmailId"},
    custom:{emailIds:[...sendEmailDetails]}
  }

  // console.log("CHECKING STP STATUS", !!(user && user.userLoginCredentials && user.userLoginCredentials.isUserSTPEligible))

  // const stpEligible = !!(user && user.userLoginCredentials && user.userLoginCredentials.isUserSTPEligible)
  // const authSTPToken = stpEligible ? user.userLoginCredentials.authSTPToken : ""

  // console.log("0.IDENTIFIED STP ELIGIBILITY",{stpEligible,authSTPToken})

  let allTenantUsersPrimaryEmailId
  try {
    const {emailDetails} = await getAllTenantUsersWithPrimaryEmailID(user)
    allTenantUsersPrimaryEmailId = emailDetails
  }
  catch (e) {
    res.send({error:"There was an error fetching emails for all tenants",e})
  }

  console.log("1. FETCHED EMAILS")

  // get the current user details
  let currentUserDetails = allTenantUsersPrimaryEmailId.reduce ( (acc,e) => {
    // console.log( {tenantEmailId:e.id.toString(), userEmailId:user._id.toString()})
    if (e.id.toString() === user._id.toString()) {
      return e
    }
    return acc
  },{})

  console.log("2. FETCHED CURRENT USER EMAIL INFO")


  if ( isEmpty(currentUserDetails)) {
    const randEmail = req.user.userEmails[0]
    currentUserDetails = {
      emailId:randEmail.emailId,
      entityId:req.user.entityId,
      stpEligible: (req.user.userLoginCredentials && req.user.userLoginCredentials.stpEligible) || false,
      authSTPToken: (req.user.userLoginCredentials && req.user.userLoginCredentials.authSTPToken) || ""
    }
  }

  console.log("2.CURRENT USER EMAIL EXTRACTED",currentUserDetails)
  const tranConfig = await getQueryConfig(type,allTenantUsersPrimaryEmailId)
  console.log("3.OBTAINED TRANCONFIG", JSON.stringify(tranConfig, null,2))

  let participantsForEmail = []
  try {

    participantsForEmail = await EntityRel.aggregate([
      {$match:{entityParty2:ObjectID(user.entityId)}},
      {
        $lookup:{
          ...tranConfig.lkup
        }
      },
      {
        $unwind:"$transactions"
      },
      {
        $match:{"transactions._id":ObjectID(tranId)}
      },
      {
        $project:{
          tranId:"$transactions._id",
          tranAllFirms:{
            $setUnion:[
              ...tranConfig.firminfo
            ]},
          tranAllUsers:{
            $setUnion: [
              ...tranConfig.userinfo
            ]
          },
          ...tranConfig.allParticipantsWithEmail,
          tranParticipants:{
            ...tranConfig.partEmails
          },
        }
      },
      {
        $addFields:{
          tranUserNotExistsInParticipantList:{
            $setDifference:["$tranAllUsers","$tranAllParticipantUsers"]
          }
        }
      },
      {
        $addFields:{
          allParticipantsEmailIds:{
            $setUnion:[
              {
                $filter: {
                  input: allTenantUsersPrimaryEmailId,
                  as: "diff",
                  cond: {
                    $in: ["$$diff.id","$tranUserNotExistsInParticipantList"]
                  }
                }
              },
              "$tranParticipants"
            ]
          }
        }
      },
      {
        $addFields:{
          allMyFirmEmailIds:
            {
              $filter: {
                input: "$allParticipantsEmailIds",
                as: "parts",
                cond: {
                  $in: ["$$parts.entityId",[ObjectID(user.entityId)]]
                }
              }
            } ,
          allNonFirmUsersEmailIds:  {
            $filter: {
              input: "$allParticipantsEmailIds",
              as: "parts",
              cond: {
                $ne:["$$parts.entityId",ObjectID(user.entityId)]
              }
            }
          },
          myEmailId:[currentUserDetails]
        }
      },
      {
        $project:{...categoryMap[category], tranId:1,_id:0}
      },
      {
        $project:{emailIdsToSend:"$emailIds"}
      }

    ])
  } catch (e) {
    console.log(e)
    res.send({error:"There was an error fetching email information",e})
  }

  console.log("THE PARTICIPANTS TO WHOM THE EMAIL NEEDS TO BE SENT ARE", JSON.stringify(participantsForEmail, null, 2))

  const emailStatus = await sendEmailsToParticipants({
    protocol:req.protocol,
    ip:ip.address(),
    // ip:req.clientIp,
    // ip:req.ip || req.headers["x-forwarded-for"] || req.connection.remoteAddress,
    host:req.get("host"),
    participantsForEmail,
    url,
    tranId,
    message:newRevisedMessage,
    subject,
    currentUserDetails,
    sendEmailUserChoice,
    defaultMessage:`Email for Transaction - ${tranId}`,
    user,
    supervisorEmails:[]
  })

  console.log("3. EMAILS SENT TO PARTICIPANTS", emailStatus)

  if(emailStatus.done) {
    res.status(200).send({...emailStatus})
  } else {
    res.status(emailStatus.statusCode || 500).send({...emailStatus, error: emailStatus.message})
  }
}

export const sendEmailsAfterCreatingTransactions = async(req, res, next) => {
  // for the logged in user get the transaction details
  // get the emails of the people to whom the emails need to be sent
  // check if each of the users are STP users are not
  // `createdByUser`
  // Check if the user wants to send emails -
  // get the primary email of the createdBy e - Done

  try {

    const queryConfig = {
      deals:{
        lkup:{
          from: "tranagencydeals",
          localField: "entityParty1",
          foreignField: "dealIssueTranClientId",
          as: "transactions"
        },
        union:["$transactions.dealIssueTranAssignedTo",["$transactions.createdByUser"]]
      },
      rfps:{
        lkup:{
          from: "tranagencyrfps",
          localField: "entityParty1",
          foreignField: "rfpTranClientId",
          as: "transactions"
        },
        union:["$transactions.rfpTranAssignedTo" ,["$transactions.createdByUser"]]
      },
      bankloans:{
        lkup:{
          from: "tranbankloans",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        },
        union:[["$transactions.actTranFirmLeadAdvisorId"],["$transactions.createdByUser"]]

      },
      derivatives:{
        lkup:{
          from: "tranderivatives",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        },
        union:[["$transactions.actTranFirmLeadAdvisorId"],["$transactions.createdByUser"]]
      },
      marfps:{
        lkup:{
          from: "actmarfps",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        },
        union:[["$transactions.actLeadFinAdvClientEntityId"]  ,["$transactions.createdByUser"]]
      },
      others:{
        lkup:{
          from: "tranagencyothers",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        },
        union:[["$transactions.createdByUser"],["$transactions.actTranFirmLeadAdvisorId"]]
      },
      busdev:{
        lkup:{
          from: "actbusdevs",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        },
        union:[["$transactions.actLeadFinAdvClientEntityId"],["$transactions.createdByUser"]]
      },

    }

    const { tranId, type, sendEmailUserChoice, emailParams:{ url, message, subject}} = req.body
    const {emailDetails} = await getAllTenantUsersWithPrimaryEmailID(req.user)

    console.log("1. AFTER GETTING THE TENANTS")
    const newEmailDetails = emailDetails.map( a => ({...a, id:ObjectID(a.id)}))
    const superVisors = await getSupervisorDetails(req.user)
    const supervisorEmails = superVisors.map( ({emailId}) => emailId)
    console.log("The supervisor emails are", supervisorEmails)

    const detailsFromTransaction = await getTransactionDescription(tranId)
    console.log("1")
    const {clientActive, processProjectDesc} = await getTransactionDescription(tranId)

    let revisedMessage = message
    if(!clientActive && !isEmpty(supervisorEmails)) {
      revisedMessage = `${message}
    <hr/>
    New Transaction ${processProjectDesc} has been created. There are no active contracts associated with the client.
    <hr/>
    Supervisors please access your dashboards and take the necessary action
    `
    }

    if(!clientActive && isEmpty(supervisorEmails)) {
      revisedMessage = `${message}
    <hr/>
    New Transaction ${processProjectDesc} has been created. There are no active contracts associated with the client
    <hr/>
    There is neither a Supervisory Principal or Compliance Officer designated for your firm. Please contact admin to set up one
    `
    }


    console.log("1. GOT EMAIL DETAILS", newEmailDetails)

    let participantsForEmail = []
    participantsForEmail = await EntityRel.aggregate([
      {$match:{entityParty2:ObjectID(req.user.entityId)}},
      {
        $lookup:{
          ...queryConfig[type].lkup
        }
      },
      {
        $unwind:"$transactions"
      },
      {
        $match:{"transactions._id":ObjectID(tranId)}
      },
      {
        $project:{
          allEmailIds:{
            $setUnion:[
              ...queryConfig[type].union
            ]},
          emailIds:{
            $filter:{
              input:newEmailDetails,
              as:"alldetails",
              cond:{$in:["$$alldetails.id",{
                $filter:
              {
                input:{
                  $setUnion:[
                    ...queryConfig[type].union
                  ]},
                as:"allusers",
                cond:{$not:{$eq:["$$allusers",null]}}
              }

              }]}
            }
          }
        }
      },
      {
        $project:{emailIdsToSend:"$emailIds", allEmailIds:1}
      }
    ])

    console.log("2. PARTICIPANT DETAILS OBTAINED", JSON.stringify(participantsForEmail,null,2))

    let currentUserDetails = emailDetails.reduce ( (acc,e) => {
      if (e.id.toString() === req.user._id.toString()) {
        return e
      }
      return acc
    },{})

    if ( isEmpty(currentUserDetails)) {
      const randEmail = req.user.userEmails[0]
      currentUserDetails = {
        emailId:randEmail.emailId,
        entityId:req.user.entityId,
        stpEligible: (req.user.userLoginCredentials && req.user.userLoginCredentials.stpEligible) || false,
        authSTPToken: (req.user.userLoginCredentials && req.user.userLoginCredentials.authSTPToken) || ""
      }
    }

    const emailStatus = await sendEmailsToParticipants({
      protocol:req.protocol,
      // ip:req.clientIp,
      ip:ip.address(),

      // ip:req.ip || req.headers["x-forwarded-for"] || req.connection.remoteAddress,
      host:req.get("host"),
      participantsForEmail,
      url,
      tranId,
      message:revisedMessage,
      subject,
      currentUserDetails,
      sendEmailUserChoice,
      defaultMessage:`Created New transaction - ${tranId}`,
      user:req.user,
      supervisorEmails:(!clientActive && !isEmpty(supervisorEmails)) ? supervisorEmails : []
    })

    console.log("3. EMAILS SENT TO PARTICIPANTS", emailStatus)

    if(emailStatus.done) {
      res.status(200).send({...emailStatus})
    } else {
      res.status(500).send({...emailStatus})
    }
  } catch (e) {
    console.log(e)
    res.send({error:"There was an error fetching email information",e})
  }
}

export const sendEmailGeneric = async (req, res, next) => {

  console.log("1. Entered the function to send emails")

  const {to,cc,replyto,bodytemplate, subject}  = req.body

  if ((to && to.length===0) || !subject) {
    res.status(500).send({done:false,message:"the request doesn't have recipeint List"})
  }

  const emailconfig = await sendEmailHelper({
    to:[...to],
    cc:[...cc],
    bodytemplate,
    subject,
    replyto:[...replyto,req.user.userLoginCredentials.userEmail]
  })

  const retStatus = await sendAWSEmail(emailconfig)
  res.send(retStatus)
}

// const generateEmailBody = ({heading, body, urlText,urlLinkText, url}) => {
//   console.log("INSIDE GENERATE BODY", {heading, body, urlText,urlLinkText, url})
//   const returnObject = `<div>
//     <h4>${heading}</h4>
//     <p>${body}</h2>
//     <h3>${urlText} <a href=${url}>${urlLinkText}</a></h3>
//   </div>
//    `
//   console.log("The BODY IS AS FOLLOWS", returnObject)
//   return returnObject
// }

const generateEmailBody = ({heading, body, urlText,urlLinkText, url}) => {
  console.log("INSIDE GENERATE BODY", {heading, body, urlText,urlLinkText, url})

  // console.log("The BODY IS AS FOLLOWS", returnObject)
  return body
}

export const sendOnboardingEmailForNewUser = async(req, res,next) => {

// get the the primary email of the user
// get the user who has last updated the user information
// get the the user who is logged in
// get the emails of the user who is creating or updating the user
// update the ID of the user who has updated the
// Get the details of the Loggied In User
// Send email with the required messages

  console.log("1. Entered email sending functionality", req.params)

  try {
    const newlyOnboardedEmailId = req.params.onboardedUserId
    const {userFirstName, userLastName, userFirmName } = req.user
    const emailofUserCreatingNewUser = req.user.userLoginCredentials.userEmailId

    const userData =  await EntityUser.aggregate([
      {
        $match:{_id:ObjectID(newlyOnboardedEmailId)}
      },
      {
        $project:{ newlyOnboardedUserPrimaryId:"$userLoginCredentials.userEmailId", entityId:1}
      }
    ])

    const {newlyOnboardedUserPrimaryId,entityId}  = userData[0]
    const {relationshipType}  = await EntityRel.findOne({entityParty2:ObjectID(entityId)}).select({"relationshipType":1})

    // if (relationshipType !== "Self") {
    //   return res.status(203).send({done:false, type:relationshipType,message:"No Email will be sent to newly onboarded Client / Third party user", emailConfirm:""})
    // }

    let typeString = null
    switch (relationshipType) {
    case "Self":
      typeString = "fa"
      break
    case "Client":
      typeString = "iss"
      break
    case "Prospect":
      typeString = "iss"
      break
    case "Third Party":
      typeString = "oth"
      break
    default:
      typeString = "comp"
    }

    console.log("2. Obtained Relationships", {newlyOnboardedUserPrimaryId, typeString, relationshipType})

    let notificationConfig = await Config.findOne({entityId: req.user.entityId}).select("notifications")
    notificationConfig = {
      Client: (notificationConfig.notifications && notificationConfig.notifications.client) || false,
      Prospect: (notificationConfig.notifications && notificationConfig.notifications.client) || false,
      Self: (notificationConfig.notifications && notificationConfig.notifications.self) || true,
      "Third Party": (notificationConfig.notifications && notificationConfig.notifications.thirdParty) || false
    }

    // Overriding the above configuration for enabling them to receive onboarding emails
    notificationConfig = {
      Client: true,
      Prospect:true,
      Self: true,
      "Third Party": true
    }

    const notificationConfigs = Object.keys(notificationConfig).filter(key => notificationConfig[key] && key)
    console.log("GET NOTIFICATIONS CONFIG",JSON.stringify(notificationConfigs,null,2))

    const emailEligible = notificationConfig[relationshipType]
    let emailSend = {}
    let emailNotSend = {}

    if(emailEligible){
      emailSend = {newlyOnboardedUserPrimaryId, typeString, relationshipType}
    } else {
      emailNotSend = {newlyOnboardedUserPrimaryId, typeString, relationshipType}
    }

    if(!emailSend.relationshipType && !emailNotSend.relationshipType){
      return res.status(500).send({done:false, type:relationshipType,message:"There was an error sending email messages"})
    }

    if ( !emailEligible) {
      return res.status(203).send({done:false,type:relationshipType, message:"The tenant has configured not to send onboarding emails to users"})
    }

    // const emailTemplate = await getEmailTemplate('LANDING_NEW_USER', userLastName, userFirmName)
    // console.log(emailTemplate)

    const templateInfo = {
      templateData:{
        userFirstName, userFirmName, userLastName, frontEndUrl, typeString
      },
      templateName: "LANDING_NEW_USER"
    }

    const welcomeEmailConfiguration = {
      subject:`Welcome to MuniVisor - Invitation from ${userFirmName}`,
      heading:"Welcome to MuniVisor",
      // body:`You have been invited by ${userLastName} - ${userFirstName} from ${userFirmName} to Signup and collaborate on MuniVisor platform`,
      body:"HTML part",
      urlText:"Please sign into the platform using the link",
      urlLinkText:"Munivisor Sign Up Link",
      url: `${frontEndUrl}/signup?type=${typeString}`
    }

    const bodytemplate = generateEmailBody(welcomeEmailConfiguration)
    console.log("2.1. CONSTRUCTED EMAIL BODY",JSON.stringify({bodytemplate},null,2))


    const emailconfig = await sendEmailHelper({
      to:[emailSend.newlyOnboardedUserPrimaryId] || [],
      cc:[emailofUserCreatingNewUser],
      bodytemplate,
      subject:welcomeEmailConfiguration.subject,
      replyto:[emailofUserCreatingNewUser]
    })

    console.log("3. JUST BEFORE SENDING EMAILS",JSON.stringify({emailconfig},null,2))

    const retStatus = await sendSESEmail(emailconfig, templateInfo)
    console.log("the return status from the async call is", retStatus)
    if( retStatus.status === "fail") {
      return res.status(500).send({done:false, type:relationshipType,message:"Error in Sending email", emailConfirm:retStatus})
    }
    // res.status(201).send({done:true, message:"Successfully sent email to the onboarded User", emailConfirm:retStatus})
    if(emailSend && emailSend.relationshipType){
      return res.status(201).send({done:true, type:relationshipType,message:"Successfully sent email to the onboarded User", emailConfirm:retStatus})
    }
    return res.status(203).send({done:false, type:relationshipType,message:`Email related to [${emailNotSend.newlyOnboardedUserPrimaryId}] is part of [${emailNotSend.relationshipType}] and is not configured to receive any notification.`, emailConfirm:retStatus})

  } catch (e) {
    console.log("There is an error sending the onboarding email", e)
    res.status(500).send({done:false,message:"There was a problem in sending onboarding email for newly onboarded User"})
  }
}

export const sendEmailRelatedToCRMAndCompliance = async(req, res,next) => {

  const { id, type, sendEmailUserChoice, emailParams:{ url, message,sendEmailTo, category, sendDocEmailLinks,subject, docIds}} = req.body
  const {user} = await req
  const token = req.headers.authorization
  const currentUserEmail = user.userLoginCredentials.userEmailId

  try {

    const auditTrailModelReference = {
      "cmp-sup-general":(auditBody) => GeneralAdmin.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "cmp-sup-prof":(auditBody) => ProfQualifications.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "cmp-sup-busconduct":(auditBody) => BusConduct.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "cmp-sup-gifts":(auditBody) => GiftsAndGratuities.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "cmp-sup-political":(auditBody) => PoliticalContributions.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "cmp-sup-complaints":(auditBody) => ComplaintDetails.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "cmp-sup-compobligations":(auditBody) => SuperVisoryObligations.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "Policies & Procedures":(auditBody) => ComplSupervisor.update({ finAdvisorEntityId: ObjectID(req.user.entityId)}, {$addToSet: {auditLogs: auditBody} }),
      "clients-propects":(auditBody) => AuditLog.findOneAndUpdate({groupId:id},{$addToSet: {changeLog: auditBody} },{upsert:true}),
      "third-parties":(auditBody) =>  AuditLog.findOneAndUpdate({groupId:id},{$addToSet: {changeLog: auditBody} },{upsert:true}),
      "clients-prospects-users":(auditBody) =>  AuditLog.findOneAndUpdate({groupId:id},{$addToSet: {changeLog: auditBody} },{upsert:true}),
      "thirdparty-users":(auditBody) =>  AuditLog.findOneAndUpdate({groupId:id},{$addToSet: {changeLog: auditBody} },{upsert:true}),
      "firm-users":(auditBody) =>  AuditLog.findOneAndUpdate({groupId:id},{$addToSet: {changeLog: auditBody} },{upsert:true}),
      "cac":(auditBody) =>  AuditLog.findOneAndUpdate({groupId:id},{$addToSet: {changeLog: auditBody} },{upsert:true})
    }

    const emailSubjectReference = {
      "cmp-sup-general":"Compliance Notification - Supervisor General",
      "cmp-sup-prof":"Compliance Notification - Supervisor General",
      "cmp-sup-busconduct":"Compliance Notification - Supervisor General",
      "cmp-sup-gifts":"Compliance Notification - Gifts and Entertainment",
      "cmp-sup-political":"Compliance Notification - Political Contributions",
      "cmp-sup-complaints":"Compliance Notification - Compliance Complaints",
      "cmp-sup-compobligations":"Email Notifications - Compliance Obligations ",
      "Policies & Procedures": "Email Notifications - Policies and Procedures",
      "clients-propects": "Email Notifications - Clients and Prospects",
      "third-parties": "Email Notifications - Third Parties",
      "clients-prospects-users": "Email Notifications - Client & Prospect Users",
      "thirdparty-users": "Email Notifications - Third Party User(s)",
      "firm-users": "Email Notifications - Tenant User(s)",
      "Control Action Center": "Compliance Notifications - Control Action Center"
    }
    console.log("1. AFTER CREATING OBJECTS")

    let newRevisedMessage = `${message}<hr/>Access Details to the Module by using the URL : ${frontEndUrl}/${url}`
    let sendEmailDetails = []

    if(category === "custom" && isEmpty(sendEmailTo)) {
      return res.status(500).send({done:false, message:"You selected to send emails to specific participants and forgot to add emails"})
    }
    else if ( category === "custom" && !isEmpty(sendEmailTo)) {
      const ret = await getAllTenantUsersWithPrimaryEmailID(user,sendEmailTo )
      sendEmailDetails = ret.emailDetails
      console.log("Send Emails to Custom IDs", JSON.stringify(sendEmailDetails))
    }
    else if ( category === "onlyme") {
      const ret = await getAllTenantUsersWithPrimaryEmailID(user,[currentUserEmail] )
      sendEmailDetails = ret.emailDetails
      console.log("Send Emails to Custom IDs", JSON.stringify(sendEmailDetails))
    }
    // Get the  Signed URL Link to send in the email
    if( sendDocEmailLinks && docIds.length > 0 ) {
      console.log("Entered the Sending Email Links Part", docIds)
      const signedUrls = await getS3DownloadURLs(docIds, token)
      console.log("The S3 download URL returnd the right messages", signedUrls)
      const signedUrlMessage = signedUrls.reduce ( (acc, su) => `${acc}<br/>${su}`, "")
      newRevisedMessage = `${newRevisedMessage} <br/> <bold>Document Details Below : </bold> ${signedUrlMessage}`
      console.log("The New Message that includes email links is",newRevisedMessage )
    } else if (sendDocEmailLinks && docIds.length === 0  ) {
      return res.status(500).send({done:false, error:"There are no Documents for which links need to be sent out"})
    }

    console.log("2. AFTER CONSTRUCTING THE DOCUMENT ID AND REVISING THE MESSAGE")


    let notificationConfig = await Config.findOne({entityId: user.entityId}).select("notifications")
    notificationConfig = {
      Client: (notificationConfig && notificationConfig.notifications && notificationConfig.notifications.client) || false,
      Prospect: (notificationConfig && notificationConfig.notifications && notificationConfig.notifications.client) || false,
      Self: (notificationConfig && notificationConfig.notifications && notificationConfig.notifications.self) || true,
      "Third Party": (notificationConfig && notificationConfig.notifications && notificationConfig.notifications.thirdParty) || false
    }

    const notificationConfigs = Object.keys(notificationConfig).filter(key => notificationConfig[key] && key)
    console.log("GET NOTIFICATIONS CONFIG",JSON.stringify(notificationConfigs,null,2))

    const emailNotSend = sendEmailDetails.filter( f => notificationConfigs.indexOf(f.relType) === -1)
    sendEmailDetails = sendEmailDetails.filter( f => notificationConfigs.indexOf(f.relType) !== -1)

    if(!sendEmailDetails.length && !emailNotSend.length){
      return res.status(500).send({done:false, message:"There was an error sending email messages"})
    }

    if ( sendEmailUserChoice && sendEmailConfig)
    {
      console.log("2.1 => THE EMAILS TO BE SENT TO THE USERS ARE ARE", sendEmailDetails)
      const toEmails = sendEmailDetails.map( ({emailId}) => emailId)
      const emailconfig = await sendEmailHelper({
        to:[...toEmails],
        cc:[currentUserEmail],
        bodytemplate:newRevisedMessage,
        subject:subject || emailSubjectReference[type],
        replyto:[currentUserEmail]
      })

      console.log("3. JUST BEFORE SENDING EMAILS",JSON.stringify({emailconfig},null,2))
      const retStatus = await sendAWSEmail(emailconfig)
      if( retStatus.status === "fail") {
        return res.status(500).send({done:false, message:"Error in Sending email", emailConfirm:retStatus})
      }

      const logs = []
      const normalLogs = ["clients-propects","third-parties","clients-prospects-users","thirdparty-users","firm-users"]
      let logMessage = {}

      if(sendEmailDetails.length){
        if (normalLogs.includes(type)) {
          logMessage = {
            userId: req.user._id,
            userName: `${req.user.userFirstName} ${req.user.userLastName}`,
            log: `Sent email related to document to users : ${JSON.stringify(sendEmailDetails.map(a => a.emailId),null,2)}`,
            ip: ip.address(),
            date:new Date()
          }
        }
        else {
          logMessage= {
            superVisorModule: type, // General Admin Activities,
            superVisorSubSection: type, // Documents
            userId: req.user._id,
            userName: `${req.user.userFirstName} ${req.user.userLastName}`,
            log: `Sent email related to document to users : ${JSON.stringify(sendEmailDetails.map(a => a.emailId),null,2)}`,
            ip: ip.address(),
            date:new Date()
          }
        }
        logs.push(logMessage)
      }

      if(emailNotSend.length){
        const email = JSON.stringify([...emailNotSend.map(a => a.emailId),],null,2)
        const relType = emailNotSend.map(a => a.relType)
        const c = []
        relType.forEach(key => {
          if(c.indexOf(key) === -1){
            c.push(key)
          }
        })

        if (normalLogs.includes(type)) {
          logMessage = {
            userId: req.user._id,
            userName: `${req.user.userFirstName} ${req.user.userLastName}`,
            log: `Email related to document to ${email} is part of ${JSON.stringify(c)} and is not configured to receive any notification.`,
            ip: ip.address(),
            date:new Date()
          }
        }
        else {
          logMessage= {
            superVisorModule: type, // General Admin Activities,
            superVisorSubSection: type, // Documents
            userId: req.user._id,
            userName: `${req.user.userFirstName} ${req.user.userLastName}`,
            log: `Email related to document to ${email} is part of ${JSON.stringify(c)} and is not configured to receive any notification.`,
            ip: ip.address(),
            date:new Date()
          }
        }
        logs.push(logMessage)
      }

      console.log("4. CONSTRUCTED AUDIT TRAIL", JSON.stringify(logs, null, 2))

      const auditFunctionToRun = auditTrailModelReference[type]
      const retObject = await auditFunctionToRun(logs)
      console.log("5. INSERTED THE AUDIT TRAIL", retObject)
      if(sendEmailDetails.length){
        res.status(201).send({done:true, message:`Successfully sent document email for the Module and Updated Activity log - ${type}`, emailConfirm:retStatus})
      } else {
        const email = JSON.stringify([...emailNotSend.map(a => a.emailId),],null,2)
        const relType = emailNotSend.map(a => a.relType)
        const c = []
        relType.forEach(key => {
          if(c.indexOf(key) === -1){
            c.push(key)
          }
        })
        res.status(203).send({done:false, message:`Email related to document to ${email} is part of ${JSON.stringify(c)} and is not configured to receive any notification.`, emailConfirm:retStatus})
      }

    } else {
      res.status(201).send({done:true, message:"No emails to be sent"})
    }

    // Get tenant for logged in User
    // Get email ID of the person who is sending the email. - Done
    // Get the Module for which the email is being sent - Done
    // Have a configurable standard email subject and body for every Module.- Done
    // Get the list of email IDs from the request - Done
    // Based on custom option select emails - Done
    // Construct the audit trail Log to Insert into the DB- Done
    // Send email to appropriate recipients - Done
  } catch (e) {
    console.log("There is an error in seding emails for the module",e)
    res.status(500).send({done:false,error:"There was an error sending emails to the intended recipients for the module"})
  }

}

export const sendAttachment = async (req, res) => {
  console.log("1. Entered the function to send emails")

  const subject = "testing"

  const bodytemplate = "Test email"

  const { docIds } = req.body

  const attachments = await getFilesFromS3(docIds)

  const emailconfig = await sendEmailHelper({
    to:["kapil.gahlot@otaras.com"],
    bodytemplate,
    subject,
    replyto:["kapil.gahlot@otaras.com"]
  })

  const retStatus = await sendAWSEmailWithAttachment({ ...emailconfig, attachments })
  res.send(retStatus)
}

export const sendOnboardingEmailForNewPlatformUser = async (user) => {

// get the the primary email of the user
// get the user who has last updated the user information
// get the the user who is logged in
// get the emails of the user who is creating or updating the user
// update the ID of the user who has updated the
// Get the details of the Loggied In User
// Send email with the required messages

  try {
    const { userFirstName, userFirmName } = user
    let { userLastName } = user
    const emailofUserCreatingNewUser = user.userLoginCredentials.userEmailId
    const newlyOnboardedUserPrimaryId = user.userLoginCredentials.userEmailId
    const {entityId}  = user
    const {relationshipType}  = await EntityRel.findOne({entityParty2:ObjectID(entityId)}).select({"relationshipType":1})

    let typeString = null
    switch (relationshipType) {
    case "Self":
      typeString = "fa"
      break
    case "Client":
      typeString = "iss"
      break
    case "Prospect":
      typeString = "iss"
      break
    case "Third Party":
      typeString = "oth"
      break
    default:
      typeString = "comp"
    }

    console.log("2. Obtained Relationships", {newlyOnboardedUserPrimaryId, typeString, relationshipType})

    const notificationConfig = {
      Client: true,
      Prospect:true,
      Self: true,
      "Third Party": true
    }

    const notificationConfigs = Object.keys(notificationConfig).filter(key => notificationConfig[key] && key)
    console.log("GET NOTIFICATIONS CONFIG",JSON.stringify(notificationConfigs,null,2))

    const emailEligible = notificationConfig[relationshipType]
    let emailSend = {}
    let emailNotSend = {}

    if(emailEligible){
      emailSend = {newlyOnboardedUserPrimaryId, typeString, relationshipType}
    } else {
      emailNotSend = {newlyOnboardedUserPrimaryId, typeString, relationshipType}
    }

    if(!emailSend.relationshipType && !emailNotSend.relationshipType){
      return {done:false, message:"There was an error sending email messages"}
    }

    if ( !emailEligible) {
      return {done:false, message:"The tenant has configured not to send onboarding emails to users"}
    }

    // const emailTemplate = await getEmailTemplate('LANDING_NEW_USER', userLastName, userFirmName)
    // console.log(emailTemplate)
    userLastName = `${userFirstName} ${userLastName}`
    const templateInfo = {
      templateData:{
        // userFirstName, userFirmName, userLastName, frontEndUrl, typeString
        userFirstName: "", userFirmName, userLastName, frontEndUrl, typeString

      },
      templateName: "LANDING_NEW_USER"
    }

    const welcomeEmailConfiguration = {
      subject:`Welcome to MuniVisor - Invitation from ${userFirmName}`,
      heading:"Welcome to MuniVisor",
      // body:`You have been invited by ${userLastName} - ${userFirstName} from ${userFirmName} to Signup and collaborate on MuniVisor platform`,
      body:"HTML part",
      urlText:"Please sign into the platform using the link",
      urlLinkText:"Munivisor Sign Up Link",
      url: `${frontEndUrl}/signup?type=${typeString}`
    }

    const bodytemplate = generateEmailBody(welcomeEmailConfiguration)
    console.log("2.1. CONSTRUCTED EMAIL BODY",JSON.stringify({bodytemplate},null,2))


    const emailconfig = await sendEmailHelper({
      to:[emailSend.newlyOnboardedUserPrimaryId] || [],
      cc:[emailofUserCreatingNewUser],
      bodytemplate,
      subject:welcomeEmailConfiguration.subject,
      replyto:[emailofUserCreatingNewUser]
    })

    console.log("3. JUST BEFORE SENDING EMAILS",JSON.stringify({emailconfig},null,2))

    const retStatus = await sendSESEmail(emailconfig, templateInfo)
    console.log("the return status from the async call is", retStatus)
    if( retStatus.status === "fail") {
      return {done:false, message:"Error in Sending email", emailConfirm:retStatus}
    }
    // res.status(201).send({done:true, message:"Successfully sent email to the onboarded User", emailConfirm:retStatus})
    if(emailSend && emailSend.relationshipType){
      return {done:true, message:"Successfully sent email to the onboarded User", emailConfirm:retStatus}
    }
    return {done:false, message:`Email related to [${emailNotSend.newlyOnboardedUserPrimaryId}] is part of [${emailNotSend.relationshipType}] and is not configured to receive any notification.`, emailConfirm:retStatus}

  } catch (e) {
    return {done:false, message:"There was a problem in sending onboarding email for newly onboarded User"}
  }
}
