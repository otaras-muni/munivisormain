import { generateControllers } from "../../modules/query"
import { Messages } from "./messages.model"

export const overrideGetAll = async (req, res, next) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user or entityId found" })
  }
  const filter = { userId: user._id }

  try {
    const docs = await Messages.find(filter)
    res.json(docs)
  } catch (error) {
    next(error)
  }
}

const overrides = {
  getAll: overrideGetAll
}

export default generateControllers(Messages, overrides)
