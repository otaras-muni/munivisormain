import { getAllEntitiesForTenant } from "../commonservices/getAllUserAndEntityDetails"
import { getAllTenantUserDetailsByUser } from "../commonservices/services.controller"
import { ObjectID } from "mongodb"

/**
 * Class used for converting client request to server friendly object model
 */
export class DealsDataModelMapper {

  getdealUnderwriterSchema = (data) => {
    const dealPartFirmName = data["dealUnderwriterSchema-dealPartFirmName"] ? data["dealUnderwriterSchema-dealPartFirmName"] : ""
    const dealPartFirm = this.allUsersForTenant.find(data => data.entityName.toLowerCase() === dealPartFirmName.toLowerCase())
    return {
      dealPartFirmId: dealPartFirm && dealPartFirm.entityId ? ObjectID(dealPartFirm.entityId) : null,
      dealPartType: "Underwriters",
      dealPartFirmName,
      roleInSyndicate: "Senior Manager",
      liabilityPerc: 0,
      managementFeePerc: 0,
      dealPartContactAddToDL: false,
    }
  }

  getdealIssueParticipants = (data, userDetailsForTenant) => {
    const uwCouncel = data["dealPartDistSchema-dealPartFirmName"] ? data["dealPartDistSchema-dealPartFirmName"] : ""
    const bondCouncel = data["dealPartDistSchemaBond-dealPartFirmName"] ? data["dealPartDistSchemaBond-dealPartFirmName"] : ""
    const uwDealPartFirm = this.allUsersForTenant.find(data => data.entityName.toLowerCase() === uwCouncel.toLowerCase())
    const bondDealPartFirm = this.allUsersForTenant.find(data => data.entityName.toLowerCase() === bondCouncel.toLowerCase())
    const resultData = [{
      dealPartFirmId: uwDealPartFirm && uwDealPartFirm.entityId ? ObjectID(uwDealPartFirm.entityId) : null,
      dealPartType: "UW Counsel",
      dealPartFirmName: uwCouncel,
      dealPartContactAddToDL: false
    }, {
      dealPartFirmId: bondDealPartFirm && bondDealPartFirm.entityId ? ObjectID(bondDealPartFirm.entityId) : null,
      dealPartType: "Bond Counsel",
      dealPartFirmName: bondCouncel,
      dealPartContactAddToDL: false
    }]
    if (userDetailsForTenant) {
      resultData.push({
        dealPartType: "Municipal Advisor",
        dealPartFirmId: userDetailsForTenant.tenantId ? userDetailsForTenant.tenantId : null,
        dealPartFirmName: userDetailsForTenant.entityName,
        dealPartContactId: userDetailsForTenant.userId ? userDetailsForTenant.userId : null,
        dealPartContactName: userDetailsForTenant.userFirstName,
        dealPartContactEmail: userDetailsForTenant.sendEmailTo,
        dealPartContactPhone: "",
        dealPartContactAddToDL: false
      })
    }
    return resultData
  }

  getdealSeriesInfo = (data) => ({
    seriesName: "",
    description: "",
    tag: "",
    seriesRatings: [{
      seriesName: "",
      ratingAgencyName: "",
      longTermRating: data["dealSeriesRatingSchema-longTermRating"] ? data["dealSeriesRatingSchema-longTermRating"] : "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: "",
    }],
    cepRatings: [{
      cepName: "",
      seriesName: "",
      cepType: "",
      longTermRating: data["dealCepRatingsSchema-longTermRating"] ? data["dealCepRatingsSchema-longTermRating"] : "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: "",
    }],
    seriesPricingDetails: null,
    seriesPricingData: [],
  })

  allEntitiesForTenant = [];
  allUsersForTenant = [];

  mapData = async (user, requestData, requestuser) => {
    const res = {}
    this.allEntitiesForTenant = await getAllEntitiesForTenant({ "entityId": user.entityId })
    this.allUsersForTenant = await getAllTenantUserDetailsByUser(requestuser)
    
    console.log(this.allUsersForTenant)

    const resultData = Object.values(requestData).map((data) => {
      const userDetailsForTenant = data.dealIssueTranFirmLeadEmail ? this.allUsersForTenant.find(userData => userData.sendEmailTo.toLowerCase() === data.dealIssueTranFirmLeadEmail.toLowerCase()) : null
      const entityForTenant = this.allEntitiesForTenant.find(entity => entity.name.toLowerCase() === data.dealIssueTranClientFirmName.toLowerCase())
      return {
        dealIssueTranIssuerId: entityForTenant && entityForTenant.entityId ? ObjectID(entityForTenant.entityId) : null,
        dealIssueTranClientId:requestuser.entityId,
        dealIssueTranIssuerFirmName: data.dealIssueTranClientFirmName ? data.dealIssueTranClientFirmName : null,
        dealIssueTranClientFirmName: requestuser.userFirmName ? requestuser.userFirmName : "No tenant Info",
        dealIssueTranClientMSRBType: data.dealIssueTranClientMSRBType ? data.dealIssueTranClientMSRBType : null,
        dealIssueTranIssuerMSRBType: data.dealIssueTranIssuerMSRBType ? data.dealIssueTranIssuerMSRBType : null,
        dealIssueBorrowerName: data.dealIssueBorrowerName ? data.dealIssueBorrowerName : null,
        dealIssueGuarantorName: data.dealIssueGuarantorName ? data.dealIssueGuarantorName : null,
        dealIssueTranName: data.dealIssueTranName ? data.dealIssueTranName : null,
        dealIssueTranType: data.dealIssueTranType ? data.dealIssueTranType : "Debt",
        dealIssueTranPurposeOfRequest: data.dealIssueTranPurposeOfRequest ? data.dealIssueTranPurposeOfRequest : null,
        dealIssueTranState: data.dealIssueTranState ? data.dealIssueTranState : null,
        dealIssueTranCounty: data.dealIssueTranCounty ? data.dealIssueTranCounty : null,
        dealIssueTranPrimarySector: data.dealIssueTranPrimarySector ? data.dealIssueTranPrimarySector : null,
        dealIssueTranSecondarySector: data.dealIssueTranSecondarySector ? data.dealIssueTranSecondarySector : null,
        dealIssueTranDateHired: data.dealIssueTranDateHired ? data.dealIssueTranDateHired : null,
        dealIssueTranStartDate: data.dealIssueTranStartDate ? data.dealIssueTranStartDate : null,
        dealIssueTranExpectedEndDate: data.dealIssueTranExpectedEndDate ? data.dealIssueTranExpectedEndDate : null,
        dealIssueTranStatus: data.dealIssueTranStatus ? data.dealIssueTranStatus : null,
        dealIssueTransNotes: data.dealIssueTransNotes ? data.dealIssueTransNotes : null,
        dealIssueTranSubType: data.dealIssueTranSubType ? data.dealIssueTranSubType : "Bond Issue",
        dealIssueTranOtherTransactionType: data.dealIssueTranOtherTransactionType ? data.dealIssueTranOtherTransactionType : null,
        createdByUser: userDetailsForTenant && userDetailsForTenant._id ? ObjectID(userDetailsForTenant._id) : null,
        dealIssueTranAssignedTo: userDetailsForTenant && userDetailsForTenant.userId ? [ObjectID(userDetailsForTenant.userId)] : null,
        dealIssueTranIsClientMsrbRegistered: data.dealIssueTranIsClientMsrbRegistered ? data.dealIssueTranIsClientMsrbRegistered : false,
        dealIssueTranIsConduitBorrowerFlag: data.dealIssueTranIsConduitBorrowerFlag ? data.dealIssueTranIsConduitBorrowerFlag : null,
        dealIssueTranIssueName: data.dealIssueTranName ? data.dealIssueTranName : "----HISTORICAL MIGRATION ----",
        dealIssueTranProjectDescription: data.dealIssueTranName ? data.dealIssueTranName : "---HISTORICAL MIGRATION----",
        dealIssueofferingType: data.dealIssueofferingType ? data.dealIssueofferingType : "",
        dealIssueSecurityType: data.dealIssueSecurityType ? data.dealIssueSecurityType : "",
        dealIssueBankQualified: data.dealIssueBankQualified ? data.dealIssueBankQualified : "",
        dealIssueCorpType: data.dealIssueCorpType ? data.dealIssueCorpType : "",
        dealIssueParAmount: data.dealIssueParAmount ? data.dealIssueParAmount : "",
        dealIssuePricingDate: data.dealIssuePricingDate ? data.dealIssuePricingDate : "",
        dealIssueExpAwardDate: data.dealIssueExpAwardDate ? data.dealIssueExpAwardDate : "",
        dealIssueActAwardDate: data.dealIssueActAwardDate ? data.dealIssueActAwardDate : "",
        dealIssueSdlcCreditPerc: data.dealIssueSdlcCreditPerc ? data.dealIssueSdlcCreditPerc : "",
        dealIssueEstimatedRev: data.dealIssueEstimatedRev ? data.dealIssueEstimatedRev : "",
        dealIssueUseOfProceeds: data.dealIssueUseOfProceeds ? data.dealIssueUseOfProceeds : "",
        dealIssueUnderwriters: this.getdealUnderwriterSchema(data),
        dealIssueParticipants: this.getdealIssueParticipants(data, userDetailsForTenant),
        dealIssueSeriesDetails: this.getdealSeriesInfo(data),
        dealIssueDocuments: [],
        dealChecklists: [],
        dealIssueCostOfIssuanceNotes: data.dealIssueCostOfIssuanceNotes ? data.dealIssueCostOfIssuanceNotes : "",
        dealIssuePlaceholder: data.dealIssuePlaceholder ? data.dealIssuePlaceholder : "",
        dealIssueTradingInformation: null,
        dealIssueContractInformation: null,
        dealIssueReceiveLeg: null,
        dealIssuePayLeg: null,
        dealIssueCompliance: null,
        dealMigration:{
          onboardingStatus:"historical"
        },
        created_at: new Date(),
        updateDate: new Date()
      }
    })
    return resultData
  }
}