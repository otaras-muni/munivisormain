
/**
 * Class used for converting client request to server friendly object model
 */
export class UserDataModelMapper {

  getUserEmails = (data) => {
    const emailIds = []
    if (data.emailId) {
      emailIds.push({
        emailId: data.emailId,
        emailPrimary: true
      })
    }
    if (data.alternateEmailId) {
      emailIds.push({
        emailId: data.alternateEmailId,
        emailPrimary: false
      })
    }
    return emailIds
  }

  getUserPhone = (data) => {
    const phones = []
    if (data.phoneNumber) {
      phones.push({
        phoneNumber: data.phoneNumber,
        extension: data.extension ? data.extension : null,
        phonePrimary: true
      })
    }
    return phones
  }

  getUserFax = (data) => {
    const faxDetails = []
    if (data.faxNumber) {
      faxDetails.push({
        faxNumber: data.faxNumber,
        faxPrimary: true
      })
    }
    return faxDetails
  }

  getUserAddress = (data) => {
    const userAddress = []
    if (data.addressName || data.addressLine1) {
      userAddress.push({
        addressName: data.addressName ? data.addressName : "ATTN : ---NO ADDRESS FROM MIGRATION---PLEASE CHANGE----",
        addressType: data.addressType ? data.addressType : "ATTN : ---NO ADDRESS FROM MIGRATION---PLEASE CHANGE----",
        addressLine1: data.addressLine1 ? data.addressLine1 : "ATTN : ---NO ADDRESS FROM MIGRATION---PLEASE CHANGE----",
        addressLine2: data.addressLine2 ? data.addressLine2 : "ATTN : ---NO ADDRESS FROM MIGRATION---PLEASE CHANGE----",
        country: data.country ? data.country : "United States",
        state: data.state ? data.state : "NO STATE",
        city: data.city ? data.city : "NO CITY",
        zipCode: { zip1: data.zipCode ? data.zipCode : 99999, zip2: data.zipCode1 ? data.zipCode1 : "" },
        formatted_address: [data.addressLine1, data.addressLine2, data.country, data.state, data.city, data.zipCode].filter(val => val !== "undefined").join()
      })
    }
    return userAddress
  }

  getUserLoginCredentials = (data) => ({
    userEmailId: data.emailId,
    password: "",
    onboardingStatus: "created",
    userEmailConfirmString: "",
    userEmailConfirmExpiry: "",
    passwordConfirmString: "",
    passwordConfirmExpiry: "",
    isUserSTPEligible: false,
    authSTPToken: "",
    authSTPPassword: "",
    passwordResetIteration: 0,
    passwordResetStatus: 0,
    passwordResetDate: ""
  })

  getEntityData = (data) => data.firmName ? {
    firmName: data.firmName ? data.firmName : "ATTN: ---- CANNOT INSERT ENTITIES WITHOUT A FIRM NAME",
    firmType: data.firmType ? data.firmType : null,
    taxId: data.taxId ? data.taxId : "NOTAXIDS",
    "entityFlags" : {
      "marketRole" : ["Not Configured"],
      "issuerFlags" : data.firmType === "Client" || data.firmType === "Prospect" ? ["Other"] : []
    },
    entityType:data.firmType === "Client" || data.firmType === "Prospect" ? "Governmental Entity / Issuer" : "Third Party",
    addresses: [{
      addressName: data.firmAddressLine1 ? data.firmAddressLine1 : "ATTN: ---NO ADDRESS PROVIDED WITH MIGRATION" ,
      isPrimary: true,
      isActie:true,
      website: data.firmWebsite ? data.firmWebsite : "ATTN : ---NO ADDRESS FROM MIGRATION---PLEASE CHANGE----",
      officePhone: [{
        countryCode: data.firmCountry ? data.firmCountry : "+1",
        phoneNumber: data.officePhone ? data.officePhone : "9999999999",
        extension: data.firmExtension ? data.firmExtension : ""
      }],
      officeFax: [{
        faxNumber: data.officeFax ? data.officeFax : ""
      }],
      officeEmails: [{
        emailId: data.firmEmailId ? data.firmEmailId : ""
      }],
      addressLine1: data.firmAddressLine1 ? data.firmAddressLine1 : "ATTN : ---NO ADDRESS FROM MIGRATION---PLEASE CHANGE----",
      addressLine2: data.firmAddressLine2 ? data.firmAddressLine2 : "",
      country: data.firmCountry ? data.firmCountry : "United States",
      state: data.firmState ? data.firmState : "NO STATE",
      city: data.firmCity ? data.firmCity : "NO CITY",
      zipCode: { zip1: data.firmZipCode ? data.firmZipCode : "9999999", zip2: data.firmZipCode1 ? data.firmZipCode1 : "" }
    }]
  } : null

  mapData = (user, requestData) => {
    const userDataArray = {}
    const entityDataArray = []
    Object.values(requestData).map((data, index) => {
      let entity = entityDataArray.find(entity => entity.firmName === data.firmName)
      if (!entity) {
        entity = { ...this.getEntityData(data), dataMigrationEntityKey: index }
        entityDataArray.push(entity)
      }
      (userDataArray[entity.dataMigrationEntityKey] || (userDataArray[entity.dataMigrationEntityKey] = [])).push({
        userFirmName: data.firmName ? data.firmName : "ATTN: NO FIRM NAME",
        userFirstName: data.userFirstName ? data.userFirstName : "ATTN - NO FIRSTNAME",
        userMiddleName: data.userMiddleName ? data.userMiddleName : "",
        userLastName: data.userLastName ? data.userLastName : "ATTN - NO LASTNAME",
        userEntitlement:"tran-readonly",
        userEmails: this.getUserEmails(data),
        userPhone: this.getUserPhone(data),
        userFax: this.getUserFax(data),
        userEmployeeID: data.userEmployeeId ? data.userEmployeeId : "",
        userJobTitle: data.userJobTitle ? data.userJobTitle : "",
        userManagerEmail: data.userManagerEmail ? data.userManagerEmail : "",
        userJoiningDate: data.userJoiningDate ? data.userJoiningDate : "",
        userEmployeeType: data.userEmployeeType ? data.userEmployeeType : "",
        userDepartment: data.userDepartment ? data.userDepartment : "",
        userExitDate: data.userExitDate ? data.userExitDate : "",
        userCostCenter: data.userCostCenter ? data.userCostCenter : "",
        userAddresses: this.getUserAddress(data),
        userLoginCredentials: this.getUserLoginCredentials(data)
      })
    })
    return { userData: userDataArray, entityData: entityDataArray }
  }
}
