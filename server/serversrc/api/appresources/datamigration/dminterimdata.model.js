import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../entityUser/entityUser.model"

const { Schema } = mongoose

export const migrationDocumentReference=Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const migrationInterimDataSchema = new Schema({
  tenantId:Schema.Types.ObjectId,
  userId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  informationType:String, 
  data: [{}],
  migrationDocuments:[migrationDocumentReference]
})

migrationInterimDataSchema.plugin(timestamps)
migrationInterimDataSchema.index({tenantId:1})
migrationInterimDataSchema.index({tenantId:1, userId:1})

export const DataOnboarding = mongoose.model("tenantonboarding", migrationInterimDataSchema)
