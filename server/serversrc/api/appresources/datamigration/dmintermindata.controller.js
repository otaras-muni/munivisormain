import mongoose from "mongoose"
import ip from "ip"
import _ from "lodash"
import {DataOnboarding} from "./dminterimdata.model.js"
import {UserDataModelMapper} from "./userdata.mapper.js"
import {DealsDataModelMapper} from "./deals.mapper.js"
import {
  Entity,
  EntityRel,
  EntityUser,
  Deals,
  ActMaRFP,
  BankLoans,
  Derivatives,
  Others,
  RFP,
  ActBusDev,
  AuditLog
} from "./../../appresources/models"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {generateConfigData} from "./../../seeddata/revisedseedata/generateConfigData"
import {elasticSearchNormalizationAndBulkInsert} from "./../../elasticsearch/esHelper"
import {generateConfigForEntities} from "../config/generateConfigForDifferentEntityTypes"
import {getTransactionInformationForLoggedInUser} from "../commonservices/getTransactionsForLoggedInUser"

// import  { dbCon }  from "./models" const firmName = "Ford & Associates" const
// adminUserEmail = "munivisorproddev@otaras.com"

const addConfig = (entity) => generateConfigData(entity).save()

const {ObjectID} = require("mongodb")

export const getInterimData = async(req, res) => {
  console.log("Entered the data migration get service", req.body)

  const {id} = req.params
  const findQuery = {
    _id: ObjectID(id)
  }
  try {
    const interimObject = await DataOnboarding.findOne({
      ...findQuery
    })
    res
      .status(200)
      .send({done: true, message: "Successfully retrieved data for the interim Object", data: interimObject})
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: "Unable to fetch data migration data"})
  }
}

export const getInterimDataByInformationType = async(req, res) => {
  const {infotype} = req.params
  const userId = req.user._id
  const findQuery = {
    userId: ObjectID(userId),
    informationType: infotype
  }
  try {
    const interimObject = await DataOnboarding.findOne({
      ...findQuery
    })
    res
      .status(200)
      .send({done: true, message: `Successfully retrieved information for type ${infotype} - for User : ${userId}`, data: interimObject})
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: "Unable to fetch data migration data"})
  }
}

export const deleteInterimDataByInformationType = async(req, res) => {
  const {infotype} = req.params
  const userId = req.user._id
  const findQuery = {
    userId: ObjectID(userId),
    informationType: infotype
  }
  try {
    const interimObject = await DataOnboarding.deleteOne({
      ...findQuery
    })
    res
      .status(200)
      .send({done: true, message: `Successfully removed information for type ${infotype} - for User : ${userId}`, data: interimObject})
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: `Unable to delete interim data for Information Type ${infotype} => User : ${userId}`})
  }
}

export const postInterimDataByInformationType = async(req, res) => {
  console.log("Entered the data migration service", req.body)
  const {data, migrationDocuments} = req.body
  const {infotype} = req.params

  const userId = req.user._id
  const findQuery = {
    userId: ObjectID(userId),
    informationType: infotype
  }

  try {
    const interimObject = await DataOnboarding.findOneAndUpdate({
      ...findQuery
    }, {
      "$set": {
        tenantId: req.user.entityId,
        userId: req.user._id,
        userFirstName: req.user.userFirstName,
        userLastName: req.user.userLastName,
        informationType: infotype,
        data,
        migrationDocuments: migrationDocuments || []
      }
    }, {
      upsert: true,
      new: true
    })

    res
      .status(200)
      .send({done: true, message: `Successfully saved interim data for User for Information Type ${infotype} => User : ${userId}`, data: interimObject})
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: `Unable to Post interim data for Information Type ${infotype} => User : ${userId}`})
  }
}

export const saveInterimData = async(req, res) => {
  console.log("Entered the data migration service", req.body)
  const {_id, informationType, data, migrationDocuments} = req.body
  const findQuery = {
    _id: ObjectID(_id)
  }
  try {
    await DataOnboarding.findOneAndUpdate({
      ...findQuery
    }, {
      "$set": {
        tenantId: req.user.entityId,
        userId: req.user._id,
        userFirstName: req.user.userFirstName,
        userLastName: req.user.userLastName,
        informationType,
        data,
        migrationDocuments: migrationDocuments || []
      }
    }, {
      upsert: true,
      new: true
    })
    res
      .status(200)
      .send({done: true, message: "Inserted data - Data Migration and Onboarding"})
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: "Unable to Insert data"})
  }
}

const mapMigrationDataByType = async(user, infotype, migrationData, requestuser) => {
  let mappedData = null
  switch (infotype) {
  case "userdata":
    var dataMapper = new UserDataModelMapper()
    mappedData = await dataMapper.mapData(user, migrationData)
    break
  case "trandata":
    var dataMapper = new DealsDataModelMapper()
    mappedData = await dataMapper.mapData(user, migrationData, requestuser)
    break
  default:
    mappedData
  }
  return mappedData
}

const data = {
  "userData": {
    "0": [
      {
        "userFirstName": "fndrec",
        "userMiddleName": "Szndeen",
        "userLastName": "Debengehp",
        "userEmails": [
          {
            "emailId": "fndrec.debengehp@rgh.hor",
            "emailPrimary": true
          }
        ],
        "userPhone": [
          {
            "phoneNumber": 7529108389,
            "extension": null,
            "phonePrimary": true
          }
        ],
        "userFax": [],
        "userEmployeeID": null,
        "userJobTitle": null,
        "userManagerEmail": null,
        "userJoiningDate": null,
        "userEmployeeType": null,
        "userDepartment": null,
        "userExitDate": null,
        "userCostCenter": null,
        "userAddresses": [],
        "userLoginCredentials": {
          "userEmailId": "fndrec.debengehp@rgh.hor",
          "password": "",
          "onboardingStatus": "created",
          "userEmailConfirmString": "",
          "userEmailConfirmExpiry": "",
          "passwordConfirmString": "",
          "passwordConfirmExpiry": "",
          "isUserSTPEligible": false,
          "authSTPToken": "",
          "authSTPPassword": "",
          "passwordResetIteration": 0,
          "passwordResetStatus": 0,
          "passwordResetDate": ""
        }
      }, {
        "userFirstName": "Degrf",
        "userMiddleName": "Trefeqner",
        "userLastName": "Fisher",
        "userEmails": [
          {
            "emailId": "degrf.fisher@rghhr.hor",
            "emailPrimary": true
          }
        ],
        "userPhone": [
          {
            "phoneNumber": 8678475854,
            "extension": null,
            "phonePrimary": true
          }
        ],
        "userFax": [],
        "userEmployeeID": null,
        "userJobTitle": null,
        "userManagerEmail": null,
        "userJoiningDate": null,
        "userEmployeeType": null,
        "userDepartment": null,
        "userExitDate": null,
        "userCostCenter": null,
        "userAddresses": [],
        "userLoginCredentials": {
          "userEmailId": "degrf.fisher@rghhr.hor",
          "password": "",
          "onboardingStatus": "created",
          "userEmailConfirmString": "",
          "userEmailConfirmExpiry": "",
          "passwordConfirmString": "",
          "passwordConfirmExpiry": "",
          "isUserSTPEligible": false,
          "authSTPToken": "",
          "authSTPPassword": "",
          "passwordResetIteration": 0,
          "passwordResetStatus": 0,
          "passwordResetDate": ""
        }
      }
    ]
  },
  "entityData": [
    {
      "firmName": "RBC Capital Markets, LLC",
      "firmType": "Third Party",
      "taxId": null,
      "addresses": [
        {
          "isPrimary": true,
          "website": null,
          "officePhone": [
            {
              "countryCode": null,
              "phoneNumber": null,
              "extension": null
            }
          ],
          "officeFax": [
            {
              "faxNumber": null
            }
          ],
          "officeEmails": [
            {
              "emailId": null
            }
          ],
          "addressLine1": null,
          "addressLine2": null,
          "country": null,
          "state": null,
          "city": null,
          "zipCode": {
            "zip1": null,
            "zip2": null
          }
        }
      ],
      "dataMigrationEntityKey": 0
    }
  ]
}

const insertMigratedEntitiesAndUsers = async (alreadyExistsFirm, entityData, tenantId, userData) => {
  let newEntities = []
  let newEntityRels = []
  let newInsertedUsers = []

  if(entityData && entityData.length){
    for (const i in entityData) {
      const ent = entityData[i]
      let insertedEntity = {}
      const firmId  = mongoose.Types.ObjectId()
      const findFirm = alreadyExistsFirm.find(af => af.firmName.toLowerCase() === ent.firmName.toLowerCase()) || null
      let isNewEntity = false
      if (!findFirm) {
        insertedEntity = {
          _id: firmId,
          ...ent,
          createdAt:new Date(),
          updatedAt:new Date()
        }
        const entityRel = {
          _id: ObjectID(),
          entityParty1: ObjectID(tenantId),
          entityParty2: ObjectID(firmId),
          relationshipType: "Undefined"
        }
        newEntities.push(insertedEntity)
        newEntityRels.push(entityRel)
        isNewEntity = true
      }
      const usersToBeInserted = (userData.filter(user => user.userFirmName.toLowerCase() === ent.firmName.toLowerCase()) || []).map(user => ({
        ...user,
        entityId: (findFirm && findFirm._id) || firmId,
        _id: ObjectID()
      }))

      if(isNewEntity && Object.keys(insertedEntity).length && usersToBeInserted.length === 1){
        usersToBeInserted[0].userFlags = ["Primary Contact"]
      }
      if(isNewEntity && Object.keys(insertedEntity).length && usersToBeInserted.length === 1){
        await Entity.findByIdAndUpdate({_id: firmId}, { primaryContactId: usersToBeInserted[0]._id })
      }
      newInsertedUsers.push(...usersToBeInserted)
    }

    newEntities = await Entity.insertMany(newEntities)
    newEntityRels = await EntityRel.insertMany(newEntityRels)
    newInsertedUsers = await EntityUser.insertMany(newInsertedUsers)
    return {newInsertedEntities: newEntities, newInsertedEntitiesRels: newEntityRels, newInsertedUsers}
  }
}

const insertMigratedEntities = async (alreadyExistsFirm, entityData, tenantId) => {
  let newEntities = []
  let newEntityRels = []

  if(entityData && entityData.length){
    for (const i in entityData) {
      const ent = entityData[i]
      let insertedEntity = {}
      const firmId  = mongoose.Types.ObjectId()
      const findFirm = alreadyExistsFirm.find(af => af.firmName.toLowerCase() === ent.firmName.toLowerCase()) || null
      if (!findFirm) {
        insertedEntity = {
          _id: firmId,
          ...ent,
          createdAt:new Date(),
          updatedAt:new Date()
        }
        const entityRel = {
          _id: ObjectID(),
          entityParty1: ObjectID(tenantId),
          entityParty2: ObjectID(firmId),
          relationshipType: "Undefined"
        }
        newEntities.push(insertedEntity)
        newEntityRels.push(entityRel)
      }
    }
    newEntities = await Entity.insertMany(newEntities)
    newEntityRels = await EntityRel.insertMany(newEntityRels)
    return {newInsertedEntities: newEntities, newInsertedEntitiesRels: newEntityRels}
  }
}

const insertMigratedUsers = async (alreadyExistsFirm, groupByUsers) => {
  let newInsertedUsers = []

  if(alreadyExistsFirm && alreadyExistsFirm.length){
    for (const i in alreadyExistsFirm) {
      const firm = alreadyExistsFirm[i]
      if (groupByUsers[firm.firmName] && groupByUsers[firm.firmName].length) {
        groupByUsers[firm.firmName] = groupByUsers[firm.firmName].map(user => ({
          ...user,
          entityId: ObjectID(firm._id)
        }))
        newInsertedUsers.push(...groupByUsers[firm.firmName])
      }
      delete groupByUsers[firm.firmName]
    }

    newInsertedUsers = await EntityUser.insertMany(newInsertedUsers)
    return {newInsertedUsers: newInsertedUsers || []}
  }
}

const getTenantsFirmsFromFirmNames = async (tenantId, firmNames) => {
  try {
    return await EntityRel.aggregate([
      {$match:{entityParty1:ObjectID(tenantId)}},
      {
        $lookup:
          {
            from: "entities",
            localField: "entityParty2",
            foreignField: "_id",
            as: "entitydetails"
          }
      },
      {
        $match : {
          "entitydetails.firmName": {
            $in: firmNames
          }
        }
      },
      {
        $unwind:"$entitydetails"
      },
      {
        $project:{
          _id:"$entitydetails._id",
          entityId:"$entitydetails._id",
          firmName:"$entitydetails.firmName",
          entityFlags:"$entitydetails.entityFlags",
          tenantId:"$entityParty1",
          relationshipType:1,
        }
      }
    ])
  } catch (e) {
    console.log("get Tenants Firms From FirmNames =>", e)
    return []
  }
}

export const uploadMigrationData = async(req, res) => {
  console.log("Entered the data migration service for upload data")
  const {users, entities} = req.body
  const {infotype} = req.params

  try {
    const tenantId = req.user.entityId
    const userData = users
    let entityData = entities
    // const newInsertedEntities = []
    // const newInsertedUsers = []
    const tenantSetting = await Entity.findOne({_id: tenantId}).select("settings")
    let isAuditEnabled = false
    if(tenantSetting && tenantSetting.settings &&	tenantSetting.settings.auditFlag){
      isAuditEnabled = tenantSetting.settings.auditFlag !== "no"
    }


    if (infotype === "UsersAndEntities") {
      try {
        const firms = entityData.map(entity => entity.firmName)
        const alreadyExistsFirm = await getTenantsFirmsFromFirmNames(tenantId, firms)
        console.log("1. Already Exists Firm", JSON.stringify(alreadyExistsFirm))

        const { newInsertedEntities, newInsertedEntitiesRels, newInsertedUsers } = await insertMigratedEntitiesAndUsers(alreadyExistsFirm, entityData, tenantId, userData)
        const parameters = {
          ids: newInsertedEntities.map(({_id}) => _id),
          esType: "entities",
          tenantId
        }
        const parametersUsers = {
          ids: newInsertedUsers.map(({_id}) => _id),
          esType: "entityusers",
          tenantId
        }
        const [dataIntoElasticSearchEntities,
          dataIntoElasticSearchUsers] = await Promise.all([
          elasticSearchNormalizationAndBulkInsert(parameters),
          elasticSearchNormalizationAndBulkInsert(parametersUsers),
          updateEligibleIdsForLoggedInUserFirm(req.user)
        ])
        if(isAuditEnabled){
          const insertedEntityAudit = []
          newInsertedEntities.forEach(entity => {
            const log = {
              groupId: tenantId,
              type: entity._id,
              changeLog: [
                {
                  userName: `${req.user.userFirstName} ${req.user.userLastName}`,
                  log: "Created Migrated Entity",
                  date: new Date(),
                  key: "entities",
                  ip: ip.address(),
                  userId: req.user._id
                }
              ]
            }
            insertedEntityAudit.push(log)
          })
          await AuditLog.insertMany(insertedEntityAudit)
        }
        res
          .status(200)
          .send({
            done: true,
            message: "Successfully Loaded User and Entity Data",
            data: {
              newInsertedEntities: newInsertedEntities.length,
              newInsertedUsers: newInsertedUsers.length
              // esentities: dataIntoElasticSearchEntities,
              // esusers: dataIntoElasticSearchUsers
            }
          })
      } catch (e) {
        console.log("The error is as follows", e)
        res
          .status(500)
          .send({done: false, message: "Issue Loading User and EntityData", data: {}})
      }
    } else if (infotype === "Users") {
      try {
        const groupByUsers = {}
        userData.forEach(user => {
          if (groupByUsers.hasOwnProperty(user.userFirmName)) {
            groupByUsers[user.userFirmName].push(user)
          } else {
            groupByUsers[user.userFirmName] = [user]
          }
        })

        const firms = userData.map(entity => entity.userFirmName)
        console.log(`Firms................${tenantId}`, JSON.stringify(firms))
        const alreadyExistsFirm = await getTenantsFirmsFromFirmNames(tenantId, firms)
        console.log("2. Already Exists Firm 1", JSON.stringify(alreadyExistsFirm))

        const {newInsertedUsers} = await insertMigratedUsers(alreadyExistsFirm, groupByUsers)
        const parameters = {
          ids: newInsertedUsers.map(({_id}) => _id),
          esType: "entityusers",
          tenantId
        }
        const [dataIntoElasticSearch] = await Promise.all([
          elasticSearchNormalizationAndBulkInsert(parameters),
          updateEligibleIdsForLoggedInUserFirm(req.user)
        ])
        res
          .status(200)
          .send({
            done: true,
            message: "Successfully Loaded Users",
            data: {
              newInsertedUsers: newInsertedUsers.length,
              // es: dataIntoElasticSearch
            }
          })
      } catch (e) {
        console.log("The error is as follows", e)
        res
          .status(500)
          .send({done: false, message: "Issue Loading Users", data: {}})
      }
    } else if (infotype === "Entities") {
      try {
        const firms = entityData.map(entity => entity.firmName)
        const alreadyExistsFirm = await getTenantsFirmsFromFirmNames(tenantId, firms)
        const existsFirmNames = alreadyExistsFirm.map(alrFirm => alrFirm.firmName.toLowerCase())
        entityData = entityData.filter(entFirm => existsFirmNames.indexOf(entFirm.firmName.toLowerCase()) === -1)

        console.log("1. Already Exists Firm", JSON.stringify(alreadyExistsFirm))
        const { newInsertedEntities, newInsertedEntitiesRels } = await insertMigratedEntities(alreadyExistsFirm, entityData, tenantId)
        const parameters = {
          ids: newInsertedEntities.map(({_id}) => _id),
          esType: "entities",
          tenantId
        }
        /* const newEntitiesIds = newInsertedEntities.map(e => e._id)
        console.log(`3. successfully inserted users for entity - ${JSON.stringify(newEntitiesIds)}`)
        await generateConfigForEntities(newEntitiesIds) */
        const [dataIntoElasticSearch] = await Promise.all([
          elasticSearchNormalizationAndBulkInsert(parameters),
          updateEligibleIdsForLoggedInUserFirm(req.user)
        ])
        if(isAuditEnabled) {
          const insertedEntityAudit = []
          newInsertedEntities.forEach(entity => {
            const log = {
              groupId: tenantId,
              type: entity._id,
              changeLog: [
                {
                  userName: `${req.user.userFirstName} ${req.user.userLastName}`,
                  log: "Created Migrated Entity",
                  date: new Date(),
                  key: "entities",
                  ip: ip.address(),
                  userId: req.user._id
                }
              ]
            }
            insertedEntityAudit.push(log)
          })
          await AuditLog.insertMany(insertedEntityAudit)
        }
        res
          .status(200)
          .send({
            done: true,
            message: "Successfully Loaded EntityData",
            data: {
              newInsertedEntities: newInsertedEntities.length,
              // es: dataIntoElasticSearch
            }
          })
      } catch (e) {
        console.log("The error is as follows", e)
        res
          .status(500)
          .send({done: false, message: "Issue Loading EntityData", data: {}})
      }
    } else {
      res
        .status(200)
        .send({done: false, message: "Please pass valid parameter"})
    }
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: "Unable to Insert data"})
  }
}

const setParticipants = (transaction, assignees, leadManagers) => {
  try{
    let participant = ""
    let lead = transaction.dealIssueTranAssignedTo && leadManagers && Object.keys(leadManagers).length ? leadManagers[transaction.dealIssueTranAssignedTo] : ""
    if(!lead && transaction.dealIssueTranAssigneeId){
      lead = assignees.find(m => m._id.toString() === transaction.dealIssueTranAssigneeId.toString())
    }
    let email = ""
    let phone = ""
    if (
      lead &&
      Array.isArray(lead.userEmails) &&
      lead.userEmails.length
    ) {
      email = lead.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : lead.userEmails[0].emailId
    }
    if (
      lead &&
      Array.isArray(lead.userPhone) &&
      lead.userPhone.length
    ) {
      phone = lead.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : lead.userPhone[0].phoneNumber
    }

    let userAddress = {}
    if (lead && Array.isArray(lead.userAddresses) && lead.userAddresses.length) {
      userAddress = lead.userAddresses.find(e => e.isPrimary)
      userAddress = !userAddress ? lead.userAddresses[0] : userAddress
      // console.log("***********************", userAddress)
    }

    if(lead){
      participant = {
        partType: "Municipal Advisor",
        partFirmId: lead.entityId || "",
        partFirmName: lead.userFirmName || "",
        partContactId: lead.id || "",
        partContactName: lead.name || lead.userFirstName ? `${lead.userFirstName} ${lead.userLastName}` : "",
        partContactEmail: email || "",
        partContactPhone: phone || "",
        partContactAddToDL: false,
        createdDate: new Date()
      }
      if(userAddress && userAddress._id){
        let addr = (userAddress && userAddress.addressLine1) || ""
        /* if(userAddress.addressLine2){
          addr = addr ? `${addr}, ${userAddress.addressLine2}` : userAddress.addressLine2
        } */
        if(userAddress.state){
          addr = addr ? `${addr}, ${userAddress.state}` : userAddress.state
        }
        if(userAddress.city){
          addr = addr ? `${addr}, ${userAddress.city}` : userAddress.city
        }
        if(userAddress.zipCode && userAddress.zipCode.zip1){
          addr = addr ? `${addr}, ${userAddress.zipCode.zip1}` : userAddress.zipCode.zip1
        }
        if(userAddress.country){
          addr = addr ? `${addr}, ${userAddress.country}` : userAddress.country
        }
        participant.partUserAddress = (userAddress && userAddress._id) || ""
        participant.partContactAddrLine1= addr || ""
        // participant.partContactAddrLine2= (userAddress && userAddress.addressLine2) || ""
        // participant.participantState= (userAddress && userAddress.state) || ""
      }
    }
    return { participant, lead}
  }catch (e) {
    // return {done: false, message: "Participants fething error"}
    console.log("Participants fetching error")
    return { participant: "", lead: "" }
  }
}

const createMultipleDeals = async(trans, entities, thirdParties, leadManagers, allDeals, user, isAuditEnabled) => {
  try {
    const allTypes = [
      "deals",
      "bankloans",
      "rfps",
      "marfps",
      "derivatives",
      "others",
      "busDev"
    ]
    const transactions = {
      count: 0,
      duplicateCount: 0
    }
    let auditLogs = []
    const transKeys = Object.keys(trans)

    let userIds = []
    Object.keys(trans).forEach(key => {
      const users = trans[key].map(t => t.dealIssueTranAssigneeId)
      userIds = userIds.concat(users)
    })
    const assignees = await EntityUser.find({_id: { $in: userIds }}).select("userFirstName userMiddleName userLastName userEmails entityId userFirmName userPhone userAddresses")
    const uniqTransactions = {
      deals: [],
      bankloans: [],
      derivatives: [],
      rfps: [],
      others: [],
      marfps: [],
      busDev: []
    }

    for (const i in transKeys) {
      const tranType = transKeys[i]

      const modal = tranType === "deals"
        ? Deals
        : tranType === "marfps"
          ? ActMaRFP
          : tranType === "derivatives"
            ? Derivatives
            : tranType === "bankloans"
              ? BankLoans
              : tranType === "rfps"
                ? RFP
                : tranType === "others"
                  ? Others
                  : tranType === "busDev"
                    ? ActBusDev
                    : ""

      if (trans[tranType] && Array.isArray(trans[tranType])) {

        // const userIds = trans[tranType].map(t => t.dealIssueTranAssigneeId)
        // const assignees = await EntityUser.find({_id: { $in: userIds }}).select("userFirstName userMiddleName userLastName userEmails entityId userFirmName userPhone userAddresses")
        trans[tranType].forEach(async  tran => {
          // trans[tranType]._id = ObjectID()
          const result = setParticipants(tran, assignees, leadManagers)
          const lead = result.lead || ""
          let participant = result.participant || ""

          // console.log("***********participant*************", JSON.stringify(participant))
          // console.log("***********lead*************", JSON.stringify(lead))
          let isDuplcate = null
          if(tran.dealIssueTranSubType  || tran.actTranSubType){
            const subtype = (tran.dealIssueTranSubType && tran.dealIssueTranSubType.toLowerCase()) || (tran.actTranSubType && tran.actTranSubType.toLowerCase()) || ""
            const issueName = (tran.dealIssueTranIssueName && tran.dealIssueTranIssueName.toLowerCase())  || (tran.actTranIssueName && tran.actTranIssueName.toLowerCase()) || ""
            const clientName = (tran.dealIssueTranIssuerFirmName && tran.dealIssueTranIssuerFirmName.toLowerCase())  || (tran.actTranClientName && tran.actTranClientName.toLowerCase()) || ""
            isDuplcate = allDeals.find(d => d.clientEntityName.toLowerCase() === clientName && d.issueName.toLowerCase() === issueName && d.subType.toLowerCase() === subtype)
          }
          if(isDuplcate){
            transactions.duplicateCount += 1
            // console.log("++++duplicateCount+++++", transactions.duplicateCount)
          }else {
            if (tranType === "deals") {
              const entity = entities.find(ent => ent.firmName.toLowerCase() === tran.dealIssueTranIssuerFirmName.toLowerCase())
              if (tran && tran.dealIssueParticipants) {
                tran
                  .dealIssueParticipants
                  .forEach(part => {
                    const thirdParty = thirdParties.find(ent => ent.firmName.toLowerCase() === part.dealPartFirmName.toLowerCase())
                    part.dealPartFirmId = thirdParty._id
                  })
              }
              if(tran && tran.dealIssueUnderwriters){
                tran
                  .dealIssueUnderwriters
                  .forEach(part => {
                    const thirdParty = thirdParties.find(ent => ent.firmName.toLowerCase() === part.dealPartFirmName.toLowerCase())
                    part.dealPartFirmId = thirdParty._id
                  })
              }
              if(lead){
                tran.dealIssueTranAssignedTo = [lead._id]
              }
              if(participant){
                const dealparticipant = {
                  dealPartType: participant.partType || "",
                  dealPartFirmId: participant.partFirmId || "",
                  dealPartFirmName: participant.partFirmName || "",
                  dealPartContactId: participant.partContactId || "",
                  dealPartContactName: participant.partContactName || "",
                  dealPartContactEmail: participant.partContactEmail || "",
                  dealPartContactPhone: participant.partContactPhone || "",
                  createdDate: participant.createdDate || new Date(),
                  dealPartContactAddToDL: false
                }
                if(participant.partUserAddress){
                  dealparticipant.dealPartUserAddress = participant.partUserAddress
                  dealparticipant.dealPartContactAddrLine1 = participant.partContactAddrLine1
                  dealparticipant.dealPartContactAddrLine2 = participant.partContactAddrLine2
                  dealparticipant.dealParticipantState = participant.participantState
                }
                // console.log("*********dealparticipant**********", dealparticipant)
                tran.dealIssueParticipants.push(dealparticipant)
              }
              tran.dealIssueTranIssuerId = (entity && entity._id) || ""
            }
            if (tranType === "marfps" || tranType === "derivatives" || tranType === "bankloans" || tranType === "others" || tranType === "busDev") {
              const tranClientName = tran.actIssuerClientEntityName || tran.actTranClientName || ""
              const participants = tran.maRfpParticipants || tran.derivativeParticipants || tran.bankLoanParticipants || tran.participants || []

              const entity = entities.find(ent => ent.firmName.toLowerCase() === tranClientName.toLowerCase())
              if (participants) {
                participants.forEach(part => {
                  const thirdParty = thirdParties.find(ent => ent.firmName.toLowerCase() === part.partFirmName.toLowerCase())
                  part.partFirmId = thirdParty._id
                })
              }
              if(participant){
                participants.push(participant)
              }
              if (tranType === "marfps" || tranType === "busDev") {
                if(lead){
                  tran.actTranFirmLeadAdvisorId = lead._id
                  tran.actTranFirmLeadAdvisorName = `${lead.userFirstName} ${lead.userLastName}`
                }
                tran.actIssuerClient = (entity && entity._id) || ""
              } else {
                if(lead){
                  tran.actTranFirmLeadAdvisorId = lead._id
                  tran.actTranFirmLeadAdvisorName = `${lead.userFirstName} ${lead.userLastName}`
                }
                tran.actTranClientId = (entity && entity._id) || ""
              }
            }
            if (tranType === "rfps") {
              const entity = entities.find(ent => ent.firmName.toLowerCase() === tran.rfpTranIssuerFirmName.toLowerCase())
              if (tran && tran.rfpParticipants) {
                tran
                  .rfpParticipants
                  .forEach(part => {
                    const thirdParty = thirdParties.find(ent => ent.firmName.toLowerCase() === part.rfpParticipantFirmName.toLowerCase())
                    part.rfpParticipantFirmId = thirdParty._id
                  })
              }
              if(lead){
                tran.rfpTranAssignedTo = [lead._id]
              }
              if(participant){
                participant = {
                  rfpSelEvalContactId: participant.partContactId || "",
                  rfpSelEvalRealFirstName: participant.partContactName || "",
                  rfpSelEvalRealLastName: participant.partContactLastName || "",
                  rfpSelEvalRealEmailId: participant.partContactEmail || "",
                  rfpSelEvalRole: "Municipal Advisor",
                  rfpSelEvalFirmId: participant.partFirmId,
                  rfpSelEvalContactName: participant.partContactName || "",
                  rfpSelEvalEmail: participant.partContactEmail || "",
                  rfpSelEvalPhone: participant.partContactPhone || "",
                  rfpSelEvalAddToDL: false
                }
                tran.rfpParticipants.push(participant)
              }
              tran.rfpTranIssuerId = (entity && entity._id) || ""
            }
            uniqTransactions[tranType].push(tran)
          }

        })
      }
      try {
        if (allTypes.indexOf(tranType) !== -1 && uniqTransactions[tranType] && Array.isArray(uniqTransactions[tranType]) && uniqTransactions[tranType].length) {
          /* let insertedDeals = []
          if (tranType === "deals") {
            for (const i in uniqTransactions[tranType]) {
              const tran = uniqTransactions[tranType][i]
              const insertedTran = await modal.create(tran)
              // console.log("3. insertedTran transaction ", JSON.stringify(insertedTran))
              if (insertedTran && insertedTran.dealIssueSeriesDetails && insertedTran.dealIssueSeriesDetails.length) {
                if (insertedTran.dealIssueSeriesDetails[0] && insertedTran.dealIssueSeriesDetails[0].seriesRatings) {
                  insertedTran
                    .dealIssueSeriesDetails[0]
                    .seriesRatings
                    .forEach(serRating => {
                      // console.log("4. serRating transaction ", JSON.stringify(serRating))
                      const ser = insertedTran
                        .dealIssueSeriesDetails
                        .find(series => series.seriesName === serRating.seriesName)
                      serRating.seriesId = ser._id
                    })
                }
                if (insertedTran.dealIssueSeriesDetails[1] && insertedTran.dealIssueSeriesDetails[1].seriesRatings) {
                  insertedTran
                    .dealIssueSeriesDetails[1]
                    .seriesRatings
                    .forEach(serRating => {
                      // console.log("4. serRating transaction ", JSON.stringify(serRating))
                      const ser = insertedTran
                        .dealIssueSeriesDetails
                        .find(series => series.seriesName === serRating.seriesName)
                      serRating.seriesId = ser._id
                    })
                }
              }
              // console.log("5. serRating transaction ", JSON.stringify(insertedTran.dealIssueSeriesDetails))
              await modal.findByIdAndUpdate({
                _id: insertedTran._id
              }, {dealIssueSeriesDetails: insertedTran.dealIssueSeriesDetails})

              insertedDeals.push(insertedTran)
            }
          } else {
            insertedDeals = await modal.insertMany(uniqTransactions[tranType])
          } */

          const insertedDeals = await modal.insertMany(uniqTransactions[tranType])
          const insertedDealsAudit = []
          insertedDeals.forEach(deal => {
            const log = {
              groupId: user.entityId,
              type: deal._id,
              changeLog: [
                {
                  userName: `${user.userFirstName} ${user.userLastName}`,
                  log: "Created transaction - Client Onboarding Data Migration",
                  date: new Date(),
                  key: "newTransaction",
                  ip: ip.address(),
                  userId: user._id
                }
              ]
            }
            insertedDealsAudit.push(log)
          })
          // console.log("33. Audit log ", JSON.stringify(insertedDealsAudit))
          auditLogs = [...insertedDealsAudit]
          transactions[tranType] = insertedDeals
          transactions.count += insertedDeals.length
        }
        if(isAuditEnabled) {
          await AuditLog.insertMany(auditLogs)
        }
      } catch (e) {
        console.log(e)
        return {done: false, message: "Unable to process with migrated data"}
      }
    }
    console.log("Checking what is getting inserted into the elastic search")
    const dataIntoElasticSearch = await Promise.all(transKeys.map(tranKey => {
      const ids = (transactions[tranKey] || []).map(({_id}) => _id)
      const parameters = {
        ids,
        esType: tranKey,
        tenantId: user.tenantId
      }
      return elasticSearchNormalizationAndBulkInsert(parameters)
    }))
    return {transactions, es: dataIntoElasticSearch}
  } catch (e) {
    console.log(e)
    return {done: false, message: "Unable to Insert data"}
  }
}

const createEntityAndThirdParties = async(entityData, relationshipType, tenantId, thirdparties) => {
  try {
    let newEntityRels = []
    let newInsertedEntities = []
    if(entityData && Object.keys(entityData).length){
      for (const i in entityData) {
        const newFirm = typeof entityData[i] === "object" ? entityData[i] : (thirdparties && thirdparties[entityData[i]]) || entityData[i] || {}
        const firmId  = mongoose.Types.ObjectId()
        const insertedEntity = {
          _id: firmId,
          entityType: typeof entityData[i] === "object" ? entityData[i].entityType : "Governmental Entity / Issuer",
          firmName: typeof entityData[i] === "object" ? entityData[i].name : (newFirm && newFirm.name) || entityData[i],
          msrbFirmName: typeof entityData[i] === "object" ? entityData[i].name : (newFirm && newFirm.name) || entityData[i],
          OnBoardingDataMigrationHistorical: true,
          createdAt:new Date(),
          updatedAt:new Date()
        }
        if(typeof entityData[i] === "object" && relationshipType === "Client" && entityData[i] && entityData[i].issuerType){
          insertedEntity.entityFlags = {
            issuerFlags: [entityData[i].issuerType]
          }
          insertedEntity.entityAliases =  _.uniqBy((entityData[i] && entityData[i].entityAliases) || [])
        }
        if (relationshipType === "Third Party") {
          delete insertedEntity.entityType
          insertedEntity.entityAliases =  _.uniqBy((thirdparties[entityData[i]] && thirdparties[entityData[i]].entityAliases) || [])
          insertedEntity.entityFlags = {
            marketRole: (thirdparties[entityData[i]] && thirdparties[entityData[i]].marketRole) || []
          }
        }
        const entityRel = {
          _id: ObjectID(),
          entityParty1: ObjectID(tenantId),
          entityParty2: ObjectID(insertedEntity._id),
          relationshipType
        }
        newInsertedEntities.push(insertedEntity)
        newEntityRels.push(entityRel)
      }
      newInsertedEntities = await Entity.insertMany(newInsertedEntities)
      newEntityRels = await EntityRel.insertMany(newEntityRels)
    }

    const parameters = {
      ids: newInsertedEntities.map(({_id}) => _id),
      esType: "entities",
      tenantId
    }
    const dataIntoElasticSearch = await elasticSearchNormalizationAndBulkInsert(parameters)
    console.log("The elastic search data",dataIntoElasticSearch )
    return {newInsertedEntities, es: dataIntoElasticSearch}
  } catch (e) {
    console.log(e)
    return {done: false, message: "Unable to Insert data"}
  }
}

const getAllDeals  = async (user, entityId) => {
  const allTransactionDetails = await getTransactionInformationForLoggedInUser(user, entityId)
  return allTransactionDetails.data.map(deal => ({
    clientEntityName: (deal && deal.tranAttributes && deal.tranAttributes.clientName) || "",
    issueName: (deal && deal.tranAttributes && deal.tranAttributes.issueName) || "",
    type: (deal && deal.tranAttributes && deal.tranAttributes.type) || "",
    subType: (deal && deal.tranAttributes && deal.tranAttributes.subType) || "",
    // pricingData: (deal && deal.tranAttributes && deal.tranAttributes.pricingDate) || ""
  }))
}

export const saveDealsMigrationData = async(req, res) => {
  console.log("Entered the data migration service for upload data")
  const {
    deals,
    bankloans,
    derivatives,
    rfps,
    others,
    marfps,
    busDev,
    clients,
    thirdPartiesList,
    picklists,
    leadManagers
  } = req.body
  const tenantId = req.user.entityId
  const tenantFirmName = req.user.userFirmName
  try {
    const allDeals = await getAllDeals(req.user, tenantId)
    const tenantSetting = await Entity.findOne({_id: tenantId}).select("settings")
    let isAuditEnabled = false
    if(tenantSetting && tenantSetting.settings &&	tenantSetting.settings.auditFlag){
      isAuditEnabled = tenantSetting.settings.auditFlag !== "no"
    }
    // console.log(allDeals)
    const thirdParties = Object.keys(thirdPartiesList || {})
    const entitiesNames = thirdParties.map(th => new RegExp(th, "i"))
    clients.forEach(c => entitiesNames.push(new RegExp(c.name, "i")))

    const entitiesNamesLC = thirdParties.map(th => th.toLowerCase())
    clients.forEach(c => entitiesNamesLC.push(c.name.toLowerCase()))

    let allEntities = await getTenantsFirmsFromFirmNames(tenantId, entitiesNames)
    allEntities = allEntities.filter(ent => entitiesNamesLC.indexOf(ent.firmName.toLowerCase()) !== -1)

    console.log("===========allEntities==============", JSON.stringify(allEntities))
    const existsThirdParty = allEntities.filter(ent => ent.relationshipType === "Third Party")
    const existsMigratedEntity = allEntities.filter(ent => ent.relationshipType === "Undefined")
    let alreadyExistsFirm = allEntities.filter(ent => ent.relationshipType === "Client" || ent.relationshipType === "Prospect" || ent.relationshipType === "Undefined")

    const existsThirdpartyNames = existsThirdParty.map(alrFirm => alrFirm.firmName.toLowerCase())
    const existsMigrated = existsMigratedEntity.map(alrFirm => alrFirm.firmName.toLowerCase())
    let thirdPartyData = thirdParties.filter(entFirm => existsThirdpartyNames.indexOf(entFirm.toLowerCase()) === -1)
    const alreadyExistsData = thirdPartyData.filter(entFirm => existsMigrated.indexOf(entFirm.toLowerCase()) !== -1)

    alreadyExistsFirm = alreadyExistsFirm.filter(clt => alreadyExistsData.indexOf(clt.firmName.toLowerCase()) === -1)
    thirdPartyData = thirdPartyData.filter(entFirm => existsMigrated.indexOf(entFirm.toLowerCase()) === -1)

    /* console.log("=====", JSON.stringify({
      existsThirdParty,
      existsMigratedEntity,
      alreadyExistsFirm,
      existsThirdpartyNames,
      existsMigrated,
      alreadyExistsData,
      thirdPartyData
    }))
    return  res
    .status(200)
    .send({
      existsThirdParty,
      existsMigratedEntity,
      alreadyExistsFirm,
      existsThirdpartyNames,
      existsMigrated,
      alreadyExistsData,
      thirdPartyData
    }) */

    for (const i in alreadyExistsData) {
      const data = existsMigratedEntity.find(e => e.firmName.toLowerCase() === alreadyExistsData[i])
      const id = (data && data._id) || ""
      const newTh = (thirdPartiesList && thirdPartiesList[alreadyExistsData[i]]) || {}

      if (id) {
        console.log("firm=======migrate convert===================>", data.firmName)
        await EntityRel.updateOne({
          entityParty2: ObjectID(id)
        }, {
          $set: {
            "relationshipType": "Third Party"
          }
        })
        const payload = {
          _id: new ObjectID(),
          marketRole: (newTh && newTh.marketRole) || [],
          issuerFlags: []
        }
        const entityAliases = _.uniqBy((newTh && newTh.entityAliases) || [])
        await Entity.findOneAndUpdate({
          _id: ObjectID(id)
        }, {
          $set: {
            "entityFlags": payload,
            entityAliases
          }
        })
      }
    }

    for (const i in existsThirdpartyNames) {
      const data = existsThirdParty.find(e => e.firmName.toLowerCase() === existsThirdpartyNames[i].toLowerCase())
      const id = (data && data._id) || ""
      const role = (thirdPartiesList && thirdPartiesList[existsThirdpartyNames[i]] && thirdPartiesList[existsThirdpartyNames[i]].marketRole) || []
      const entityAliases = (thirdPartiesList && thirdPartiesList[existsThirdpartyNames[i]] && thirdPartiesList[existsThirdpartyNames[i]].entityAliases) || []

      if (id) {
        console.log("firm===th update=======================>", data.firmName)
        const marketRole = _.uniq([
          ...role,
          ...((data.entityFlags && data.entityFlags.marketRole) || [])
        ])
        const mergeEntityAliases = _.uniq([
          ...entityAliases,
          ...((data && data.entityAliases) || [])
        ])
        const payload = {
          _id: new ObjectID(),
          marketRole,
          issuerFlags: []
        }
        await Entity.findOneAndUpdate({
          _id: ObjectID(id)
        }, {
          $set: {
            entityFlags: payload,
            entityAliases: mergeEntityAliases,
          }
        })
      }
    }

    const convertedEntities = []
    for (const i in alreadyExistsFirm) {
      const data = clients.find(entFirm => entFirm.name.toLowerCase() === alreadyExistsFirm[i].firmName.toLowerCase())
      const id = (alreadyExistsFirm[i] && alreadyExistsFirm[i]._id) || ""
      if (id) {
        console.log("firm===client update=======================>", (data && data.name))
        const updateUbj = {}
        const issuerFlags = (alreadyExistsFirm[i] && alreadyExistsFirm[i].issuerFlags) || []
        if(data && data.issuerType){
          issuerFlags.push(data.issuerType)
        }
        updateUbj.entityFlags = {
          _id: new ObjectID(),
          marketRole: [],
          issuerFlags: _.uniq(issuerFlags)
        }
        updateUbj.entityAliases = _.uniq([
          ...((alreadyExistsFirm[i] && alreadyExistsFirm[i].entityAliases) || []),
          ...((data && data.entityAliases) || [])
        ])

        if(alreadyExistsFirm[i] && alreadyExistsFirm[i].relationshipType === "Undefined"){
          updateUbj.entityType = (data && data.entityType) || (data && data.issuerType) || ""
          await EntityRel.updateOne({
            entityParty2: ObjectID(id)
          }, {
            $set: {
              relationshipType: "Client"
            }
          })
          convertedEntities.push(id)
        }
        await Entity.findOneAndUpdate({
          _id: ObjectID(id)
        }, {
          $set: updateUbj
        })
      }
    }

    const existsFirmNames = alreadyExistsFirm.map(alrFirm => alrFirm.firmName.toLowerCase())
    const entityData = clients.filter(entFirm => existsFirmNames.indexOf(entFirm.name.toLowerCase()) === -1)

    console.log("1 entityData", entityData)
    const {newInsertedEntities, es: esClients} = await createEntityAndThirdParties(entityData, "Client", tenantId)
    // console.log("Post Inserting Clients / Prospects - ",
    // JSON.stringify(esClients, null, 2))

    const {newInsertedEntities: newInsertedThirdParties, es: esThirdParties} = await createEntityAndThirdParties(thirdPartyData, "Third Party", tenantId, thirdPartiesList)
    // console.log("Post Inserting Third Parties - ", JSON.stringify(esThirdParties,
    // null, 2))

    /* const picklistsKeys = Object.keys(picklists || {})
    const findPicklists = await Config.aggregate([
      {
        $match: {
          entityId: tenantId
        }
      }, {
        $limit: 1
      }, {
        $project: {
          picklists: 1
        }
      }, {
        $project: {
          picklists: {
            $filter: {
              input: "$picklists",
              as: "picklist",
              cond: {
                $in: ["$$picklist.meta.systemName", picklistsKeys]
              }
            }
          }
        }
      }
    ])

    for (const i in picklistsKeys) {

      const pickValue = findPicklists[0]
        .picklists
        .find(pickVal => pickVal.meta.systemName === picklistsKeys[i])
      picklists[picklistsKeys[i]].forEach(val => {
        const findVal = pickValue
          .items
          .find(item => item.label && val && item.label.toLowerCase() === val.toLowerCase())
        if (!findVal) {
          pickValue
            .items
            .push({label: val, included: true, visible: true, items: []})
        }
      })

      await Config.updateOne({
        entityId: ObjectID(tenantId),
        "picklists._id": pickValue._id
      }, {
        $set: {
          "picklists.$.items": pickValue.items,
          "picklists.$.meta": pickValue.meta,
          "picklists.$.title": pickValue.title
        }
      })

    } */

    // console.log(newInsertedEntities)
    const newEntitiesIds = newInsertedEntities.map(e => e._id)
    const newInsertedEntitiesIds = newInsertedEntities.map(e => e._id)
    // console.log(`3. successfully inserted users for entity - ${JSON.stringify([...newEntitiesIds, ...newInsertedEntitiesIds])}`)
    await generateConfigForEntities([...newEntitiesIds, ...newInsertedEntitiesIds, ...convertedEntities])

    let newInsertedLeadManager = []
    if(leadManagers && Object.keys(leadManagers).length){
      Object.keys(leadManagers).forEach(key => {
        const data = leadManagers[key]
        const user = {
          userFirstName: data.userFirstName || "",
          userLastName: data.userLastName || "",
          userMiddleName: data.userMiddleName || "",
          userEmails: [
            {
              emailPrimary: true,
              emailId: data.emailId.toLowerCase() || ""
            }
          ],
          userFlags: ["Series 50"],
          userRole: "tran-edit",
          userEntitlement: "tran-edit",
          entityId: tenantId,
          userFirmName: tenantFirmName,
          userLoginCredentials: {
            userEmailId: data.emailId.toLowerCase() || "",
            password: "",
            onboardingStatus: "created",
            userEmailConfirmString: "",
            userEmailConfirmExpiry: "",
            passwordConfirmString: "",
            passwordConfirmExpiry: "",
            isUserSTPEligible: false,
            authSTPToken: "",
            authSTPPassword: "",
            passwordResetIteration: 0,
            passwordResetStatus: 0,
            passwordResetDate: ""
          },
          OnBoardingDataMigrationHistorical: true,
          updatedAt: new Date(),
          createdAt: new Date()
        }
        newInsertedLeadManager.push(user)
      })
      newInsertedLeadManager = await EntityUser.insertMany(newInsertedLeadManager)
      Object.keys(leadManagers).forEach(key => {
        const item = leadManagers[key]
        leadManagers[key] = newInsertedLeadManager.find(manager => manager.userLoginCredentials.userEmailId.toLowerCase() === item.emailId.toLowerCase())
      })
    }

    const {transactions, es: esTransactions} = await createMultipleDeals({
      deals,
      bankloans,
      derivatives,
      rfps,
      others,
      marfps,
      busDev
    }, [
      ...alreadyExistsFirm,
      ...newInsertedEntities
    ], [
      ...existsThirdParty,
      ...existsMigratedEntity,
      ...newInsertedThirdParties
    ],
    leadManagers,
    allDeals,
    req.user,
    isAuditEnabled)

    // console.log("Post Inserting Transactions - Elastic Search Feedback", JSON.stringify(esTransactions, null, 2))
    // console.log(" NewInsertedLeadManager ==================> ", JSON.stringify(newInsertedLeadManager))

    await updateEligibleIdsForLoggedInUserFirm(req.user)

    if(isAuditEnabled) {
      const insertedEntityThirdPartyAudit = []
      newInsertedEntities.forEach(entity => {
        const log = {
          groupId: tenantId,
          type: entity._id,
          changeLog: [
            {
              userName: `${req.user.userFirstName} ${req.user.userLastName}`,
              log: "Created Migrated Entity",
              date: new Date(),
              key: "entities",
              ip: ip.address(),
              userId: req.user._id
            }
          ]
        }
        insertedEntityThirdPartyAudit.push(log)
      })
      newInsertedThirdParties.forEach(party => {
        const log = {
          groupId: tenantId,
          type: party._id,
          changeLog: [
            {
              userName: `${req.user.userFirstName} ${req.user.userLastName}`,
              log: "Created Migrated Third Party",
              date: new Date(),
              key: "thirdParties",
              ip: ip.address(),
              userId: req.user._id
            }
          ]
        }
        insertedEntityThirdPartyAudit.push(log)
      })
      // console.log("33. Audit log ", JSON.stringify(insertedEntityThirdPartyAudit))
      await AuditLog.insertMany(insertedEntityThirdPartyAudit)
    }
    res
      .status(200)
      .send({
        done: true,
        message: "Successfully Loaded Deals Data",
        data: {
          // transactions, entities: [ ...alreadyExistsFirm, ...newInsertedEntities ],
          // thirdParties: [ ...existsThirdParty, ...newInsertedThirdParties ] ,
          transactionsDuplicateCount: (transactions && transactions.duplicateCount) || 0,
          transactionsCount: (transactions && transactions.count) || 0,
          entitiesCount: newInsertedEntities.length,
          thirdPartiesCount: newInsertedThirdParties.length,
          leadManagers: newInsertedLeadManager.length
        }
      })
  } catch (e) {
    console.log(e)
    res
      .status(500)
      .send({done: false, message: "Unable to Insert data"})
  }
}
