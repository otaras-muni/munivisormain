import express from "express"
import {generateOnBoardingTenant} from "./tenantonboarding.controller"

export const onBoardinTenantRouter = express.Router()

onBoardinTenantRouter
  .route("/firm")
  .post(generateOnBoardingTenant)
