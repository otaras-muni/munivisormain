import crypto from "crypto"
import isEmpty from "lodash/isEmpty"

import {
  Deals,
  RFP,
  BankLoans,
  Derivatives,
  Tasks,
  EntityUser,
  Others
} from "../models"
import {ObjectID} from "mongodb"

import {createNotification} from "../notification/helpers"
import {ActMaRFP} from "../activities"
import {putBulkData, elasticSearchNormalizationAndBulkInsert, deleteData, deleteDataInBulkForTenant} from "./../../elasticsearch/esHelper"

const checklists = [
  {
    data: [
      {
        headers: [
          "test1", "something else"
        ],
        items: [
          {
            _id: "1",
            label: "ITEM 1",
            assignedTo: [
              {
                _id: "5b1a63bd471a1919e0ca9808",
                name: "naveen",
                type: "bond counsel"
              }, {
                _id: "5b1a63c8471a1919e0ca9dde",
                name: "naveen",
                type: "bond counsel"
              }
            ],
            startDate: "20180101",
            endDate: "20180131",
            resolved: false,
            priority: "low"
          }, {
            _id: "2",
            label: "ITEM 2",
            assignedTo: [
              {
                _id: "5b1a63bd471a1919e0ca9808",
                name: "naveen",
                type: "bond counsel"
              }
            ],
            startDate: "20180101",
            endDate: "20180131",
            resolved: false,
            priority: "high"
          }
        ],
        title: "Manage Deal Items"
      }, {
        headers: [
          "test1", "something else"
        ],
        items: [
          {
            _id: "3",
            label: "ITEM 11",
            assignedTo: [
              {
                _id: "5b1a63bd471a1919e0ca9808",
                name: "naveen",
                type: "bond counsel"
              }, {
                _id: "5b1a63c8471a1919e0ca9dde",
                name: "deepak",
                type: "bond counsel"
              }
            ],
            startDate: "20180101",
            endDate: "20180131",
            resolved: false,
            priority: "low"
          }, {
            _id: "4",
            label: "ITEM 21",
            assignedTo: [
              {
                _id: "5b1a63bd471a1919e0ca9808",
                name: "naveen",
                type: "bond counsel"
              }
            ],
            startDate: "20180101",
            endDate: "20180131",
            resolved: false,
            priority: "high"
          }
        ],
        title: "Manage Deal Items"
      }
    ],
    id: "CL1",
    type: "template4"
  }
]

const existingTasksInDb = [
  // The content of the information has changed
  {
    "taskUniqueIdentifier": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "taskAssigneeUserId": "u2",
      "activityContextTaskId": "3"
    },
    "taskDetails": {
      "taskAssigneeUserId": "u2",
      "taskAssigneeName": "deepak",
      "taskAssigneeType": "bond counsel",
      "taskDescription": "ITEM 11",
      "taskStartDate": "20180101",
      "taskEndDate": "20180131",
      "taskPriority": "Medium",
      "taskStatus": "open"
    },
    "relatedActivityDetails": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "activityContextSection": "Manage Deal Items",
      "activityContextTaskId": "3"
    }
  },

  // this is the same there is no change
  {
    "taskUniqueIdentifier": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "taskAssigneeUserId": "5b1a63bd471a1919e0ca9808",
      "activityContextTaskId": "4"
    },
    "taskDetails": {
      "taskAssigneeUserId": "5b1a63bd471a1919e0ca9808",
      "taskAssigneeName": "naveen",
      "taskAssigneeType": "bond counsel",
      "taskDescription": "ITEM 21",
      "taskStartDate": "20180101",
      "taskEndDate": "20180131",
      "taskPriority": "high",
      "taskStatus": "open"
    },
    "relatedActivityDetails": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "activityContextSection": "Manage Deal Items",
      "activityContextTaskId": "4"
    }
  },
  // This one doesn't exist in the list so this needs to be deleted.
  {
    "taskUniqueIdentifier": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "taskAssigneeUserId": "5b1a63bd471a1919e0ca9808",
      "activityContextTaskId": "5"
    },
    "taskDetails": {
      "taskAssigneeUserId": "5b1a63bd471a1919e0ca9808",
      "taskAssigneeName": "naveen",
      "taskAssigneeType": "bond counsel",
      "taskDescription": "ITEM 21",
      "taskStartDate": "20180101",
      "taskEndDate": "20180131",
      "taskPriority": "high",
      "taskStatus": "open"
    },
    "relatedActivityDetails": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "activityContextSection": "Manage Deal Items",
      "activityContextTaskId": "5"
    }
  }
]

// For a given ID get the following details
/*
  - checklists
  - ID, type, subtype
  - clientId
  - clientName
*/
// For the given ID get all the tasks - store that Information in a variable

const getProcessDetails = async(processId) => {
  const existsInDeal = await Deals.aggregate([
    {
      $match: {
        _id: ObjectID(processId)
      }
    }, {
      $project: {
        processContext: "$_id",
        processType: "Debt",
        processSubType: "Bond Issue",
        processClientId: "$dealIssueTranIssuerId",
        processClientName: "$dealIssueTranIssuerFirmName",
        processProjectDesc: {
          $cond: {
            if: {
              $or: [
                {
                  "$eq": ["$dealIssueTranIssueName", ""]
                }, {
                  "$eq": ["$dealIssueTranIssueName", null]
                }
              ]
            },
            then: "$dealIssueTranProjectDescription",
            else : "$dealIssueTranIssueName"
          }
        },
        processCheckList: "$dealChecklists"
      }
    }
  ])

  const existsInRFP = await RFP.aggregate([
    {
      $match: {
        _id: ObjectID(processId)
      }
    }, {
      $project: {
        processContext: "$_id",
        processType: "Managed Client RFP",
        processSubType: "$rfpTranPurposeOfRequest",
        processClientId: "$rfpTranIssuerId",
        processClientName: "$rfpTranIssuerFirmName",
        processProjectDesc: {
          $cond: {
            if: {
              $or: [
                {
                  "$eq": ["$rfpTranIssueName", ""]
                }, {
                  "$eq": ["$rfpTranIssueName", null]
                }
              ]
            },
            then: "$rfpTranProjectDescription",
            else : "$rfpTranIssueName"
          }
        },
        processCheckList: "$rfpBidCheckList"
      }
    }
  ])

  const existsInBankLoans = await BankLoans.aggregate([
    {
      $match: {
        _id: ObjectID(processId)
      }
    }, {
      $project: {
        processContext: "$_id",
        processType: "$actTranType",
        processSubType: "$actTranSubType",
        processClientId: "$actTranClientId",
        processClientName: "$actTranClientName",
        processProjectDesc: {
          $cond: {
            if: {
              $or: [
                {
                  "$eq": ["$actTranIssueName", ""]
                }, {
                  "$eq": ["$actTranIssueName", null]
                }
              ]
            },
            then: "$actTranProjectDescription",
            else : "$actTranIssueName"
          }
        },
        processCheckList: "$bankLoanChecklists"
      }
    }
  ])

  const existsInDerivatives = await Derivatives.aggregate([
    {
      $match: {
        _id: ObjectID(processId)
      }
    }, {
      $project: {
        processContext: "$_id",
        processType: "$  ",
        processSubType: "$actTranSubType",
        processClientId: "$actTranClientId",
        processClientName: "$actTranClientName",
        processProjectDesc: {
          $cond: {
            if: {
              $or: [
                {
                  "$eq": ["$actTranIssueName", ""]
                }, {
                  "$eq": ["$actTranIssueName", null]
                }
              ]
            },
            then: "$actTranProjectDescription",
            else : "$actTranIssueName"
          }
        },
        processCheckList: []
      }
    }
  ])

  const existsinMaRFP = await ActMaRFP.aggregate([
    {
      $match: {
        _id: ObjectID(processId)
      }
    }, {
      $project: {
        processContext: "$_id",
        processType: "$  ",
        processSubType: "$actType",
        processClientId: "$actSubType",
        processClientName: "$actIssuerClientEntityName",
        processProjectDesc: {
          $cond: {
            if: {
              $or: [
                {
                  "$eq": ["$actIssueName", ""]
                }, {
                  "$eq": ["$actIssueName", null]
                }
              ]
            },
            then: "$actProjectName",
            else : "$actIssueName"
          }
        },
        processCheckList: []
      }
    }
  ])

  const existsInOthers = await Others.aggregate([
    {
      $match: {
        _id: ObjectID(processId)
      }
    }, {
      $project: {
        processContext: "$_id",
        processType: "$actTranType",
        processSubType: "$actTranSubType",
        processClientId: "$actTranClientId",
        processClientName: "$actTranClientName",
        processProjectDesc: {
          $cond: {
            if: {
              $or: [
                {
                  "$eq": ["$actTranIssueName", ""]
                }
              ]
            },
            then: "$actTranProjectDescription",
            else : "$actTranIssueName"
          }
        },
        processCheckList: "$actTranChecklists"
      }
    }
  ])

  const processInfo = [
    ...existsInDeal,
    ...existsInDerivatives,
    ...existsInRFP,
    ...existsInBankLoans,
    ...existsinMaRFP,
    ...existsInOthers
  ]
  return processInfo[0]
}

const getTaskDetailsForProcess = async(processId) => {
  const taskInfo = await Tasks.aggregate([
    {
      $match: {
        "relatedActivityDetails.activityId": ObjectID(processId)
      }
    }, {
      $project: {
        taskUniqueIdentifier: 1,
        taskDetails: 1,
        taskIdentifier: 1,
        taskContentHask: 1
      }
    }
  ])

  // console.log("----OBTAINED TASK INFO", taskInfo)
  return taskInfo
}

const insertTasks = async(taskDetails, user) => {
  if ((taskDetails || []).length === 0) {
    return {done: true, message: "no tasks to insert"}
  }
  try {
    const updatedTaskDetails = taskDetails.map(t => ({
      ...t,
      createdAt: new Date(),
      updatedAt: new Date()
    }))
    const insertedData = await Tasks.insertMany(updatedTaskDetails)
    // console.log("The Insert Tasks are", JSON.stringify(insertedData,null,2))
    const dataForEs = insertedData.map(d => d._id)
    const dataForReturn = insertedData.map(d => d._doc)
    const bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: user.tenantId})
    console.log("The return value from Elastic search", bulkUpdateEs)
    return {done: true, message: `Successfully inserted tasks ; status from ES - ${bulkUpdateEs.done} ; message from ES - ${bulkUpdateEs.message}`, tasks: dataForReturn}
  } catch (e) {
    console.log(e)
    return {done: false, message: "unable to insert tasks", tasks: taskDetails}
  }
}

const modifyTasks = async(taskDetails, user) => {
  if ((taskDetails || []).length === 0) {
    return {done: true, message: "no tasks to modify"}
  }

  try {
    const updatedTasks = []
    const results = await Promise.all(taskDetails.map(async(task) => {
      const updatedTask = await Tasks.findOneAndUpdate({
        "taskIdentifier": task.taskIdentifier
      }, {
        ...task
      }, {
        "new": true,
        "upsert": true,
        "runValidators": true
      })
      // console.log("Updated Task", task.taskIdentifier)
      updatedTasks.push(updatedTask)
      return updatedTask
    }))
    // console.log("The Tasks that are being updated are",
    // JSON.stringify(updatedTasks,null, 2))
    const dataForEs = updatedTasks.map(d => d._id)
    const bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: user.tenantId})

    return {done: true, message: `Updated All Taks - status from ES - ${bulkUpdateEs.done} ; message from ES - ${bulkUpdateEs.message}`, tasks: results}

  } catch (e) {
    console.log("there is an error updateing tasks", e)
    return {done: false, message: "unable to update tasks", tasks: taskDetails}
  }
}

const deleteTasks = async(taskDetails, user) => {

  if ((taskDetails || []).length === 0) {
    return {done: true, message: "no tasks to delete"}
  }

  try {
    const results = await Promise.all(taskDetails.map(async(task) => {
      const res = await Tasks.findOneAndRemove({"taskIdentifier": task})
      console.log("Deleted Task", res._doc)
      const resEs = await deleteData("mvtasks", res && res._doc && res._doc._id)
      console.log("The response from ES is", JSON.stringify(resEs, null, 2))
      return res
    }))
    await deleteDataInBulkForTenant({
      tenantId: user.tenantId,
      ids: results.map(({_id}) => _id)
    })
    return {done: true, message: "deletedalltasks", tasks: results}
  } catch (e) {
    console.log("there is an error updating tasks", e)
    return {done: false, message: "unable to update tasks", tasks: taskDetails}
  }
}

const getUserDetails = async(userId) => {
  if (!userId) {
    return {done: false, message: "No User ID has been passed", userInfo: null}
  }
  let res

  try {
    res = await EntityUser.aggregate([
      {
        $match: {
          _id: ObjectID(userId)
        }
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityId",
          "foreignField": "_id",
          "as": "userEntityInfo"
        }
      }, {
        $unwind: "$userEntityInfo"
      }, {
        $project: {
          _id: 0,
          userId: "$_id",
          userFirstName: "$userFirstName",
          userLastName: "$userLastName",
          userEntityId: "$entityId",
          userEntityName: "$userEntityInfo.firmName"
        }
      }
    ])
    return {done: true, message: "successfully retreived information", userInfo: res[0]}

  } catch (e) {
    console.log("there is an error in getting user Information", e)
    return {done: false, message: "unable to get user Information", userInfo: null}
  }

}

export const manageTasksForProcessChecklistsOld = async(processId, loggedInUser) => {
  // Get all the process Information
  const processDetails = await getProcessDetails(processId)
  // console.log("----OBTAINED PROCESS
  // INFO",JSON.stringify(processDetails,null,2)) Get Logged In User Details
  const {userInfo: loggedInUserInfo} = await getUserDetails(loggedInUser._id)
  console.log("----LOGGED IN USER INFO")

  // Get all the tasks
  const existingTasksFromDB = await getTaskDetailsForProcess(processId)

  // Destructure the process Details
  const {
    processContext,
    processType,
    processSubType,
    processClientId,
    processClientName,
    processProjectDesc,
    processCheckList
  } = processDetails

  // Get only those checklists that are of a specific type
  const taskEligibleChecklists = (processCheckList || []).filter(({type}) => type === "template4" || type === "template1")
  // console.log("TASKS ELIGIBLE
  // CHECKLISTS",JSON.stringify(taskEligibleChecklists,null,2)) Flatten the
  // checklists to
  const mappedTasksFromCheckLists = await(taskEligibleChecklists || []).reduce(async(allCheckListFlatItems, chklist) => {
    const {id: chkListContext, data: checkListData} = chklist
    const allTaskData = await(checkListData || []).reduce(async(allFlatAccordions, chkListAccordion) => {
      const {items: checkListDataItems, title} = chkListAccordion
      const allFlatItems = await(checkListDataItems || []).reduce(async(unwindItems, item) => {
        // console.log("----ITEM---ITEM", JSON.stringify(item,null,2))
        const {
          assignedTo,
          _id: itemId,
          label,
          startDate,
          endDate,
          eventStatus,
          resolved,
          priority
        } = item
        const computeStatus = eventStatus || (resolved
          ? "Closed"
          : "Open")
        const revisedStartDate = startDate || Date.now()
        console.log({assignedTo, startDate, endDate, itemId})
        // Make sure that the start date and end date are populated
        if ((assignedTo && revisedStartDate && endDate) || (assignedTo && endDate)) {
          console.log("---------Entered Here - 1 ")
          const modifiedAssignees = await(assignedTo || []).reduce(async(massignees, {
            _id: assigneeUser,
            name,
            type
          }) => {

            console.log({assigneeUser, eventStatus, label, name})

            const {userInfo: taskAssigneeInfo} = await getUserDetails(assigneeUser)
            const retObject = {
              taskUniqueIdentifier: {
                activityId: processContext,
                activityContext: chkListContext,
                taskAssigneeUserId: ObjectID(assigneeUser),
                activityContextTaskId: itemId
              },
              taskDetails: {
                taskAssigneeUserId: ObjectID(assigneeUser),
                taskAssigneeName: name,
                taskAssigneeType: type,
                taskDescription: label,
                taskStartDate: startDate,
                taskEndDate: endDate,
                taskPriority: priority || "low",
                taskStatus: computeStatus,
                taskType: processType,
                taskFollowing: true,
                taskUnread: true
              },
              relatedActivityDetails: {
                activityId: processContext,
                activityType: processType,
                activitySubType: processSubType,
                activityProjectName: processProjectDesc,
                activityIssuerClientId: processClientId,
                activityIssuerClientName: processClientName,
                activityContext: chkListContext,
                activityContextSection: title,
                activityContextTaskId: itemId
              },
              taskAssigneeUserDetails: {
                ...taskAssigneeInfo
              },
              taskCreatedBy: {
                ...loggedInUserInfo
              }
            }

            // don't consider the userfollowing information to compute the hash
            const {taskDetails} = retObject
            const {
              taskFollowing,
              ...coreTaskInfo
            } = taskDetails
            // Generate a unique String Information
            const taskIdentifier = crypto
              .createHash("md5")
              .update(JSON.stringify(retObject.taskUniqueIdentifier))
              .digest("hex")
            const taskContentHash = crypto
              .createHash("md5")
              .update(JSON.stringify(coreTaskInfo))
              .digest("hex")

            const massigneesAcc = await massignees
            return Promise.resolve([
              ...massigneesAcc, {
                ...retObject,
                taskIdentifier,
                taskContentHash
              }
            ])
          }, Promise.resolve([]))
          const unwindItemsAcc = await unwindItems
          return Promise.resolve([
            ...unwindItemsAcc,
            ...modifiedAssignees
          ])
        }
        const unwindItemsAcc = await unwindItems
        return Promise.resolve(unwindItemsAcc)
      }, Promise.resolve([]))
      const allFlatAccordionsAcc = await allFlatAccordions
      return Promise.resolve([
        ...allFlatAccordionsAcc,
        ...allFlatItems
      ])
    }, Promise.resolve([]))
    const allCheckListFlatItemsAcc = await allCheckListFlatItems
    return Promise.resolve([
      ...allCheckListFlatItemsAcc,
      ...allTaskData
    ])
  }, Promise.resolve([]))

  console.log("----MAPPING DONE ")

  // Get the tasks associated with the deal ID Run a loop to compare the
  // information from the checklist and the deal ID

  const tasksForComparison = existingTasksFromDB.reduce((overallTasks, oldTask) => {
    const {taskDetails} = oldTask
    const {
      taskFollowing,
      ...coreTaskInfo
    } = taskDetails
    const taskIdentifier = crypto
      .createHash("md5")
      .update(JSON.stringify(oldTask.taskUniqueIdentifier))
      .digest("hex")
    const taskContentHash = crypto
      .createHash("md5")
      .update(JSON.stringify(coreTaskInfo))
      .digest("hex")
    return {
      ...overallTasks,
      [taskIdentifier]: {
        taskIdentifier,
        taskContentHash,
        oldTask
      }
    }
  }, {})

  console.log("----EXISTING TASK MAPPING DONE")

  // Get IDs from existing and new set of tasks

  const existingTasksUniqueIds = Object.keys(tasksForComparison)
  const newTaskUniqueIds = mappedTasksFromCheckLists.map(({taskIdentifier}) => taskIdentifier)

  // Capture all the new tasks that need to be inserted
  const listNewAndUpdatedTasks = mappedTasksFromCheckLists.reduce((finalStatus, newTask) => {
    const {addTask, modifyTask, noaction} = finalStatus
    const {taskIdentifier, taskContentHash} = newTask
    const matchStatus = existingTasksUniqueIds.includes(taskIdentifier)

    let sameContent = false
    if (matchStatus) {
      sameContent = tasksForComparison[taskIdentifier].taskContentHash === taskContentHash
    }
    // console.log({taskIdentifier,taskContentHash,matchStatus,sameContent})

    if (matchStatus && !sameContent) {
      // doing this piece of code to get the following flag
      const {oldTask} = tasksForComparison[taskIdentifier]
      const {taskDetails: oldTaskDetails} = oldTask
      const {
        taskDetails: newTaskDetails,
        ...restNewTaskDetails
      } = newTask
      const newRevisedTask = {
        taskDetails: {
          ...newTaskDetails,
          taskFollowing: oldTaskDetails.taskFollowing,
          taskUnread: oldTaskDetails.taskUnread || true
        },
        ...restNewTaskDetails
      }
      return {
        ...finalStatus,
        ...{
          modifyTask: [
            ...modifyTask,
            newRevisedTask
          ]
        }
      }
    }
    if (!matchStatus && !sameContent) {
      return {
        ...finalStatus,
        ... {
          addTask : [
            ...addTask,
            newTask
          ]
        }
      }
    }
    if (!matchStatus && sameContent) {
      return {
        ...finalStatus,
        ... {
          addTask : [
            ...addTask,
            newTask
          ]
        }
      }
    }
    if (matchStatus && sameContent) {
      return {
        ...finalStatus,
        ... {
          noaction : [
            ...noaction,
            newTask
          ]
        }
      }
    }
  }, {
    "addTask": [],
    "modifyTask": [],
    "noaction": []
  })
  console.log("----TAGGING TASKS TO BE INSERTED/UPDATED")

  // Find Tasks that need to be deleted
  const tasksToBeDeleted = existingTasksUniqueIds.reduce((deletedTasks, oldTaskIdentifier) => {
    const {deleteTask} = deletedTasks
    const matchStatus = newTaskUniqueIds.includes(oldTaskIdentifier)
    if (!matchStatus) {
      return {
        ...deletedTasks,
        ... {
          deleteTask : [
            ...deleteTask,
            oldTaskIdentifier
          ]
        }
      }
    }
    return deletedTasks
  }, {deleteTask: []})
  console.log("----TASKS TO BE DELETED DONE")
  // Consolidate all the items into one Object
  const consolidatedTaskAction = {
    ...listNewAndUpdatedTasks,
    ...tasksToBeDeleted
  }
  // console.log("----ALL CRUD OPERATIONS SUMMARY",consolidatedTaskAction) Insert
  // , Update and Delete tasks
  const returnInsert = await insertTasks(consolidatedTaskAction.addTask, loggedInUser)
  const insertStatus = returnInsert.done
  console.log("----INSERTING NEW TASKS")
  // KG change
  if (consolidatedTaskAction.addTask && consolidatedTaskAction.addTask.length && insertStatus) {
    const {
      taskDetails: {
        taskDescription,
        taskStartDate,
        taskEndDate,
        taskPriority,
        taskStatus
      },
      relatedActivityDetails: {
        activityId,
        activityType,
        activitySubType,
        activityProjectName
      }
    } = consolidatedTaskAction.addTask[0]
    const options = {
      taskDescription,
      taskStartDate,
      taskEndDate,
      taskPriority,
      taskStatus,
      modes: ["app"],
      recurring: false,
      frequency: 0,
      unit: ""
    }
    const meta = {
      contextId: activityId,
      contextType: `${activityType} - ${activitySubType}`,
      contextName: activityProjectName
    }
    const userIds = consolidatedTaskAction
      .addTask
      .map(e => e.taskDetails.taskAssigneeUserId)
    createNotification("", "NOTIFY_TASK_ASSIGNED", userIds, options, meta)
  }
  const returnModify = await modifyTasks(consolidatedTaskAction.modifyTask, loggedInUser)
  const modifyStatus = returnModify.done
  console.log("----UPDATING TASKS")

  const returnDelete = await deleteTasks(consolidatedTaskAction.deleteTask, loggedInUser)
  const deleteStatus = returnDelete.done
  console.log("----DELETING TASKS")

  if (insertStatus && modifyStatus && deleteStatus) {
    return {done: true, message: "Insert/Update/Delete successful for tasks", tasks: consolidatedTaskAction}
  }

  return {done: false, message: "Error Updating Tasks", tasks: consolidatedTaskAction}
}

export const manageTasksForProcessChecklists = async (id, user) => {
  const commonTaskPipeline = [
    {
      $unwind:"$checklists"
    },
    {
      $addFields:{
        checkListStatus:{$ifNull:["$checklists.status","active"]}
      }
    },
    {
      $match:{"checkListStatus":"active","checklists.type":{$in:["template1","template4"]}}
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistLinkId:"$checklists.id",
        checklistName:"$checklists.name",
        checklistTemplateType:"$checklists.type",
        checklistData:"$checklists.data",
      }
    },
    {
      $unwind:"$checklistData"
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:1,
        checklistTaskCategoryTitle:"$checklistData.title",
        checklistTemplateType:1,
        checklistLinkId:1,
        checklistItems:"$checklistData.items"
      }
    },
    {
      $unwind:"$checklistItems"
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:1,
        checklistTaskCategoryTitle:1,
        checklistTemplateType:1,
        checklistLinkId:1,
        checklistItemTaskAssignees:"$checklistItems.assignedTo",
        checklistItemTaskLinkId:"$checklistItems._id",
        checklistItemTaskDesc: "$checklistItems.label",
        checklistItemTaskStartDate:"$checklistItems.startDate",
        checklistItemTaskDueDate:"$checklistItems.endDate",
        checklistItemStatus:"$checklistItems.eventStatus"
      }
    },
    {
      $unwind:"$checklistItemTaskAssignees"
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:1,
        checklistTemplateType:1,
        checklistLinkId:1,
        checklistTaskCategoryTitle:1,
        checklistItemTaskLinkId:1,
        checklistItemTaskDesc: 1,
        checklistItemTaskStartDate:1,
        checklistItemTaskDueDate:1,
        checklistItemTransactionStatus:1,
        checklistItemAssigneeId:"$checklistItemTaskAssignees._id",
        checklistItemAssigneeName:"$checklistItemTaskAssignees.name",
        checklistItemAssigneeType:"$checklistItemTaskAssignees.type",
      }
    },
    {
      $addFields:{
        checklistItemTaskUniqueId:{ $concat: [ 
          {
            $toString: "$tranId"
          }, "|", 
          {
            $toString:"$checklistItemTaskLinkId"
          },"|",
          {
            $toString:"$checklistItemAssigneeId" 
          }
        ]
        }
      }
    },
    {
      $addFields:{
        taskUniqueIdentifier:{
          _id:"$checklistItemTaskUniqueId",
          activityId:"$tranId",
          activityContext:"$checklistItemTaskUniqueId",
          taskAssigneeUserId:"$checklistItemAssigneeId",
          activityContextTaskId:"$checklistItemTaskUniqueId"
        },
        taskDetails:{
          taskAssigneeUserId:"$checklistItemAssigneeId",
          taskAssigneeName:"$checklistItemAssigneeName",
          taskAssigneeType:"$checklistItemAssigneeType",
          taskDescription:"$checklistItemTaskDesc",
          taskStartDate:"$checklistItemTaskStartDate",
          taskEndDate:"$checklistItemTaskDueDate",
          taskType:"$tranType",
          taskInsertContext:"Transactions/Projects"
        },
        relatedActivityDetails:{
          activityId:"$tranId",
          activityType:"$tranType",
          activitySubType:"$tranSubType",
          activityProjectName: "$tranActivityDescription",
          activityIssuerClientId:"$tranClientEntityId",
          activityIssuerClientName:"$tranActivityDescription",
          activityContextTemplateType:"$checklistTemplateType",
          activityContext:"$checklistLinkId",
          activityContextSection:"$checklistName",
          activityContextTaskUniqueId:"$checklistItemTaskUniqueId",
          activityContextTaskId: "$checklistItemTaskLinkId"
        },
      }
    }
  ]

  try {
  
    let chklist = []

    if(isEmpty(chklist)){
      chklist = await Deals.aggregate([
        {
          $match:{_id:ObjectID(id)}
        },
        {
          $project:{
            tranId:"$_id",
            tranType:"$dealIssueTranType",
            tranSubType:"$dealIssueTranSubType",
            tranClientEntityId: "$dealIssueTranIssuerId",
            tranActivityDescription : {
              $cond:{if:{$or:[{"$eq":["$dealIssueTranIssueName",""]},{"$eq":["$dealIssueTranIssueName",null]}]},
                then:"$dealIssueTranProjectDescription",
                else:"$dealIssueTranIssueName"},
            },
            checklists:{$ifNull:["$dealChecklists",[]]},
          }
        },
        ...commonTaskPipeline
      ])
    }

    if(isEmpty(chklist)){
      chklist = await RFP.aggregate([
        {
          $match:{_id:ObjectID(id)}
        },
        {
          $project:{
            tranId:"$_id",
            tranType:"$rfpTranType",
            tranSubType:"$rfpTranSubType",
            tranClientEntityId: "$rfpTranIssuerId",
            tranActivityDescription : {
              $cond:{if:{$or:[{"$eq":["$rfpTranIssueName",""]},{"$eq":["$rfpTranIssueName",null]}]},
                then:"$rfpTranProjectDescription",
                else:"$rfpTranIssueName"},
            },
            checklists:{$ifNull:["$rfpBidCheckList",[]]},
          }
        },
        ...commonTaskPipeline
      ])
    }
    if(isEmpty(chklist)){

      chklist = await Others.aggregate([
        {
          $match:{_id:ObjectID(id)}
        },
        {
          $project:{
            tranId:"$_id",
            tranType:"$actTranType",
            tranSubType:"$actTranSubType",
            tranClientEntityId: "$actTranClientId",
            tranActivityDescription : {
              $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
                then:"$actTranProjectDescription",
                else:"$actTranIssueName"},
            },
            checklists:{$ifNull:["$actTranChecklists",[]]},
          }
        },
        ...commonTaskPipeline
      ])
    }

    if(isEmpty(chklist)){
      chklist = await BankLoans.aggregate([
        {
          $match:{_id:ObjectID(id)}
        },
        {
          $project:{
            tranId:"$_id",
            tranType:"$actTranType",
            tranSubType:"$actTranSubType",
            tranClientEntityId: "$actTranClientId",
            tranActivityDescription : {
              $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
                then:"$actTranProjectDescription",
                else:"$actTranIssueName"},
            },
            checklists:{$ifNull:["$bankLoanChecklists",[]]},
          }
        },
        ...commonTaskPipeline
      ])
    }

    if(isEmpty(chklist)){
      chklist = await ActMaRFP.aggregate([
        {
          $match:{_id:ObjectID(id)}
        },
        {
          $project:{
            tranId:"$_id",
            tranType:"$actType",
            tranSubType:"$actSubType",
            tranClientEntityId: "$actIssuerClient",
            tranActivityDescription : {
              $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actIssueName",null]}]},
                then:"$actProjectName",
                else:"$actIssueName"},
            },
            checklists:{$ifNull:["$marfpChecklists",[]]},
          }
        },
        ...commonTaskPipeline
      ])
    }



    // Get the unique task IDs for comparison with existing tasks
    const uniqueTaskIds = chklist.map( a => a.checklistItemTaskUniqueId)
    const type = chklist.reduce( (acc,{tranType}) => tranType,null)

    const convertToObject = chklist.reduce( ( acc, {checklistItemTaskUniqueId,taskUniqueIdentifier,taskDetails,relatedActivityDetails}) => ({...acc,
      [checklistItemTaskUniqueId]:{
        taskUniqueIdentifier,
        taskDetails,
        relatedActivityDetails
      }
    }),{})

    const getExistingTasksForProcess = await Tasks.aggregate([
      {
        $match:{"relatedActivityDetails.activityId":ObjectID(id),"taskDetails.taskInsertContext":"Transactions/Projects"}
      },
      {
        $addFields:{
          "taskUniqueIdentifier":{$ifNull:["$taskUniqueIdentifier._id",""]}
        }
      }
    ])

    // Get unique Ids for comparison

    const tasksExistingInGlobalForProcessContext = getExistingTasksForProcess.map( ({taskUniqueIdentifier}) => taskUniqueIdentifier)
  

    // Get the case where the tasks already exist. Just 
    const processTasksUpdate = await Promise.all(getExistingTasksForProcess
      .filter(({taskUniqueIdentifier}) => uniqueTaskIds.includes(taskUniqueIdentifier))
      .map(async ({_id,taskUniqueIdentifier,taskDetails,relatedActivityDetails,createdAt,...rest}) => {
        const newTaskDetails = convertToObject[taskUniqueIdentifier].taskDetails
        const newRelatedActivityDetails = convertToObject[taskUniqueIdentifier].relatedActivityDetails
        return Tasks.findOneAndUpdate({_id},{
          taskDetails:{...taskDetails,...newTaskDetails},
          relatedActivityDetails:{...relatedActivityDetails,...newRelatedActivityDetails},
          ...rest
        },{
          upsert:false,
          new:true
        })
      }))

    const createdDate = new Date()
    const updatedDate = createdDate

    const processTasksInsert = chklist
      .filter(({checklistItemTaskUniqueId}) => !tasksExistingInGlobalForProcessContext.includes(checklistItemTaskUniqueId) )
      .map( ({taskUniqueIdentifier,taskDetails,relatedActivityDetails}) =>({
        taskDetails:{...taskDetails,taskStatus:"Open",taskUnread:true},
        taskUniqueIdentifier,
        relatedActivityDetails,
        updatedAt:updatedDate,
        createdAt:createdDate
      }))
      
    const processTasksDelete = await Promise.all(getExistingTasksForProcess
      .filter( x => !uniqueTaskIds.includes(x.taskUniqueIdentifier) )
      .map( async ( {_id}) => Tasks.findOneAndRemove({_id})))
      
    const esTaskInsertStatus =[]
    if(!isEmpty(processTasksInsert)){
      const insertedData = await Tasks.insertMany(processTasksInsert)
      const dataForEs = insertedData.map(d => d._id)
      const bulkEsInsertStatus = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: user.tenantId})
      esTaskInsertStatus.push({transactionId:id,transactionType:type,operation:"Insert Tasks",esOutput:bulkEsInsertStatus})  
    }
    
    if(!isEmpty(processTasksUpdate)) {
      const dataForEs = processTasksUpdate.map(d => d._id)
      const bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: user.tenantId})
      esTaskInsertStatus.push({transactionId:id,transactionType:type,operation:"Update",esOutput:bulkUpdateEs})  
    }

    if(!isEmpty(processTasksDelete)){
      await deleteDataInBulkForTenant({
        tenantId: user.tenantId,
        ids: processTasksDelete.map(({_id}) => _id)
      })
      esTaskInsertStatus.push({transactionId:id,transactionType:type,operation:"Delete Tasks",esOutput:"done with es delete"})  
    }
    console.log(JSON.stringify({processTasksUpdate,processTasksInsert,processTasksDelete,esTaskInsertStatus},null,2))
    return {processTasksUpdate,processTasksInsert,processTasksDelete,esTaskInsertStatus}
  } catch(e) {
    console.log("there is error in updating checklists", e)
    return {processTasksUpdate:[],processTasksInsert:[],processTasksDelete:[],esTaskInsertStatus:{}}
  }
}
