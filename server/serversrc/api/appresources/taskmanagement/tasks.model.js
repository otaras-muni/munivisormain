import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"
import {EntityUser} from "../entityUser/entityUser.model"

// const taskAssgineesSchema = Schema({
//   userId : { type: Schema.Types.ObjectId, ref: EntityUser },
//   userFullName : String,
//   userEntityId : { type: Schema.Types.ObjectId, ref: Entity },
//   userEntityName : String,
//   userTaskFollowing:{type:Boolean,default:true}
// })

// const taskRelatedActivitySchema = Schema({
//   activityId : { type: Schema.Types.ObjectId },
//   activityType : String,
//   activityName : String,
//   activityTaskContext:{ type: String, default: "General"},
//   activityClient: Schema.Types.ObjectId,
//   activityClientName:String
// })

const taskDocuments = Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  docNote:String,
  docStatus:String,
  markedPublic:{ publicFlag:Boolean, publicDate:Date},
  sentToEmma: { emmaSendFlag:Boolean, emmaSendDate:Date},
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now },
  docId: String
}, {strict: false})

// const taskSchema = Schema({
//   taskName:String,
//   taskNotes:String,
//   taskType:String,
//   taskStartDate:Date,
//   taskDueDate:Date,
//   taskStatus: { type: String, default: "Open" },
//   taskPriority:{type:String, default:""},
//   taskCreatedBy:{userId:Schema.Types.ObjectId, userFirstName:String, userLastName:String,userEntityName:String,userEntityId:String},
//   taskRelatedActivity:taskRelatedActivitySchema,
//   taskAssignees:taskAssgineesSchema,
//   taskRelatedDocuments:[taskDocuments]
// })



const taskSchema = Schema({
  taskIdentifier:String,
  taskContentHash:String,
  taskUniqueIdentifier:Schema.Types.Mixed, // The unique identifier can vary depending on the context of the task
  // taskUniqueIdentifier:{
  //   activityId:Schema.Types.ObjectId,
  //   activityContext:String,
  //   taskAssigneeUserId:Schema.Types.ObjectId,
  //   activityContextTaskId:Schema.Types.ObjectId
  // },
  taskDetails:{
    taskAssigneeUserId:Schema.Types.ObjectId,
    taskAssigneeName:String,
    taskAssigneeType:String,
    taskNotes:String,
    taskDescription:String,
    taskStartDate:Date,
    taskEndDate:Date,
    taskPriority:String,
    taskStatus: String,
    taskType: String,
    taskFollowing:{type:Boolean,default:true},
    taskUnread:{type:Boolean,default:true},
    complianceTask:Boolean,
    tranUrl: String
  },
  taskHistory: [{
    userId: Schema.Types.ObjectId,
    userFirstName: String,
    userLastName: String,
    taskNotes:String,
    taskDescription:String,
    createdAt: { type: Date, required: true, default: Date.now },
    updateddAt: { type: Date, required: true, default: Date.now },
  }],
  relatedActivityDetails:{
    activityId:Schema.Types.ObjectId,
    activityType:String,
    activitySubType:String,
    activityProjectName:String,
    activityIssuerClientId:Schema.Types.ObjectId,
    activityIssuerClientName:String,
    activityContext:String,
    activityContextSection:String,
    activityContextTaskId:String
  },
  taskAssigneeUserDetails:{
    userId:{ type: Schema.Types.ObjectId },
    userFirstName:String,
    userLastName:String,
    userEntityId:Schema.Types.ObjectId,
    userEntityName:String,
  },
  relatedEntityDetails:{
    entityId:Schema.Types.ObjectId,
    entityName:String
  },
  relatedUserDetails:{
    relatedUserId:Schema.Types.ObjectId,
    relatedUserName:String,
    relatedUserEntityId:Schema.Types.ObjectId,
    relatedUserEntityName:String
  },
  relatedContacts: [{
    userId:Schema.Types.ObjectId,
    userName:String,
    entityId:Schema.Types.ObjectId,
    entityName:String
  }],
  taskRelatedDocuments:[taskDocuments],
  taskCreatedBy:{
    userId:{ type: Schema.Types.ObjectId },
    userFirstName:String,
    userLastName:String,
    userEntityId:String,
    userEntityName:String,
  },
  taskRefId: String,
}, {strict: false}).plugin(timestamps)

const updateTimestemps = function (next){
  const self = this


  if(!self.createdAt) {
    self.createdDate = new Date()
    // or self.update({},{ $set: { createdDate : new Date(), updatedDate: new Date() } });
  } else {
    self.updatedDate= new Date()
    // or self.update({},{ $set: {updatedDate: new Date() } });
  }
  next()
}


taskSchema.pre("save",updateTimestemps)
taskSchema.pre("update",updateTimestemps)
taskSchema.pre("findOneAndUpdate",updateTimestemps)

// taskSchema.plugin(timestamps)
taskSchema.index({"taskDetails.taskAssigneeUserId":1})
taskSchema.index({"relatedActivityDetails.activityId":1})

export const Tasks =  mongoose.model("mvtasks", taskSchema)
