import express from "express"
import tasksController from "./tasks.controller"
import {Tasks} from "./tasks.model"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Tasks, _id: id, esType: "mvtasks"})

export const tasksRouter = express.Router()

tasksRouter.get("/get-task-details", requireAuth, async(req, res) => {
  const {tid} = req.query
  if (!tid) {
    return res
      .status(422)
      .send({error: "No task id provided"})
  }
  try {
    const result = await Tasks.findOne({_id: tid})
    res.json(result)
  } catch (err) {
    res
      .status(422)
      .send({err})
  }
})

tasksRouter.post("/save-simple-task", requireAuth, async(req, res) => {
  const {taskDetails} = req.body
  try {
    const result = await Tasks.create({taskDetails})
    await Promise.all([
      updateEligibleIdsForLoggedInUserFirm(req.user),
      returnFromES(req.user.tenantId, result._id)
    ])
    res.json(result)
  } catch (err) {
    res
      .status(422)
      .send({err})
  }
})

tasksRouter.post("/update-from-ui", requireAuth, async(req, res) => {
  const {tid} = req.query
  if (!tid) {
    return res
      .status(422)
      .send({error: "No task id provided"})
  }
  try {
    const result = await Tasks.update({
      _id: tid
    }, {
      $set: {
        ...req.body
      }
    })
    await Promise.all([
      updateEligibleIdsForLoggedInUserFirm(req.user),
      returnFromES(req.user.tenantId, tid)
    ])
    res.json(result)
  } catch (err) {
    return res
      .status(422)
      .send({error: err})
  }
})

tasksRouter.post("/add-from-ui", requireAuth, async(req, res) => {
  try {
    const result = await Tasks.create(req.body)
    await Promise.all([
      updateEligibleIdsForLoggedInUserFirm(req.user),
      returnFromES(req.user.tenantId, result._id)
    ])
    res.json(result)
  } catch (err) {
    return res
      .status(422)
      .send({error: err})
  }
})

tasksRouter.post("/get-checklist-tasks", requireAuth, async(req, res) => {
  try {
    const result = await Tasks.find({
      ...req.body
    })
    res.json(result)
  } catch (err) {
    return res
      .status(422)
      .send({error: err})
  }
})

tasksRouter.post("/update-task", requireAuth, tasksController.updateTask)

tasksRouter
  .route("/")
  .get(requireAuth, tasksController.getAllTask)
  .post(requireAuth, tasksController.saveTask)

tasksRouter
  .route("/taskId/:taskId")
  .get(requireAuth, tasksController.getTasksById)
  .put(requireAuth, tasksController.updateTask)

tasksRouter
  .route("/contextdata")
  .get(requireAuth, tasksController.getTasksContextData)

tasksRouter
  .route("/activefirmtask")
  .get(requireAuth, tasksController.getTasksTaskByFirm)

tasksRouter
  .route("/processtasks")
  .post(requireAuth, tasksController.manageTasksForProcessCheckLists)

tasksRouter
  .route("/activityTasks")
  .get(requireAuth, tasksController.fetchActivityTask)
  .post(requireAuth, tasksController.postActivityTask)

tasksRouter
  .route("/transaction/:taskId")
  .put(requireAuth, tasksController.putTransactionDocStatuse)
  .delete(requireAuth, tasksController.delTransactionDocStatuse)

tasksRouter
  .route("/toggletask/:taskId")
  .post(requireAuth, tasksController.toggleReadStatus)

tasksRouter
  .route("/getSpecificTask")
  .get(requireAuth, tasksController.getSpecificTask)
