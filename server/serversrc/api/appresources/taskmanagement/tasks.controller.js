import {generateControllers} from "../../modules/query"
import {Tasks} from "./tasks.model"
import {EntityUser} from "../entityUser/entityUser.model"
import {EntityRel} from "../entityRel"
import {RFP} from "../rfp"
import {Deals} from "../deals"
import {ActBusDev} from "../activities/businessdevelopment/busdev.model"
import {canUserPerformAction} from "../../commonDbFunctions"
import {manageTasksForProcessChecklists} from "./taskProcessInteraction"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {elasticSearchUpdateFunction, putBulkData, elasticSearchNormalizationAndBulkInsert} from "../../elasticsearch/esHelper"

const {ObjectID} = require("mongodb")

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Tasks, _id: id, esType: "mvtasks"})

const getTaskContextData = () => async(req, res) => {
  const {entityId} = req.query
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }, {
      resource: "EntityUser",
      access: 1
    }, {
      resource: "EntityRel",
      access: 1
    }, {
      resource: "Deals",
      access: 1
    }, {
      resource: "RFP",
      access: 1
    }, {
      resource: "Tasks",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const assigneeUsers = await EntityUser.aggregate([
      {
        $match: {
          entityId: ObjectID(entityId)
        }
      }, {
        $project: {
          "entityId": 1,
          "userId": "$_id",
          userFirstName: 1,
          userLastName: 1
        }
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityId",
          "foreignField": "_id",
          "as": "entitydetails"
        }
      }, {
        $unwind: "$entitydetails"
      }, {
        $project: {
          "entityId": 1,
          "userId": "$_id",
          userFirstName: 1,
          userLastName: 1,
          "entitydetails": 1
        }
      }, {
        $group: {
          _id: {
            entityId: "$entityId",
            relationshipType: "Tenant",
            firmName: "$entitydetails.firmName"
          },
          userDetails: {
            $addToSet: {
              userId: "$userId",
              userFirstName: "$userFirstName",
              userLastName: "$userLastName"
            }
          }
        }
      }, {
        $project: {
          _id: 0,
          entityId: "$_id.entityId",
          firmName: "$_id.firmName",
          relationshipType: "$_id.relationshipType",
          userDetails: 1
        }
      }
    ])
    const reducedEntityUsers = assigneeUsers.reduce((finalEntityUsers, {userDetails, entityId, firmName, relationshipType}) => {
      const consolidatedInfo = userDetails.map(user => ({
        ...user,
        entityId,
        firmName,
        relationshipType
      }))
      return [
        ...finalEntityUsers,
        ...consolidatedInfo
      ]
    }, [])

    // Get users for the related parties
    const relatedPartyUsers = await EntityRel.aggregate([
      {
        $match: {
          entityParty1: ObjectID(entityId)
        }
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "relatedEntityInfo"
        }
      }, {
        $unwind: "$relatedEntityInfo"
      }, {
        $project: {
          "entityId": "$entityParty2",
          firmName: "$relatedEntityInfo.firmName",
          "relationshipType": 1
        }
      }, {
        $lookup: {
          "from": "entityusers",
          "localField": "entityId",
          "foreignField": "entityId",
          "as": "relatedEntityUsers"
        }
      }, {
        $unwind: "$relatedEntityUsers"
      }, {
        $project: {
          "entityId": 1,
          "relationshipType": 1,
          firmName: 1,
          userId: "$relatedEntityUsers._id",
          userFirstName: "$relatedEntityUsers.userFirstName",
          userLastName: "$relatedEntityUsers.userLastName"
        }
      }, {
        $group: {
          _id: {
            entityId: "$entityId",
            relationshipType: "$relationshipType",
            firmName: "$firmName"
          },
          userDetails: {
            $addToSet: {
              userId: "$userId",
              userFirstName: "$userFirstName",
              userLastName: "$userLastName"
            }
          }
        }
      }, {
        $project: {
          _id: 0,
          entityId: "$_id.entityId",
          firmName: "$_id.firmName",
          relationshipType: "$_id.relationshipType",
          userDetails: 1
        }
      }
    ])

    const reducedRelatedEntityUsers = relatedPartyUsers.reduce((finalEntityUsers, {userDetails, entityId, relationshipType, firmName}) => {
      const consolidatedInfo = userDetails.map(user => ({
        ...user,
        entityId,
        relationshipType,
        firmName
      }))
      return [
        ...finalEntityUsers,
        ...consolidatedInfo
      ]
    }, [])

    const allEntitiesConsolidated = {
      assigneeUsers: [
        ...reducedEntityUsers,
        ...reducedRelatedEntityUsers
      ]
    }

    // Get information from deals
    const relatedDealTransactions = await Deals.aggregate([
      {
        $match: {
          dealIssueTranClientId: ObjectID(entityId)
        }
      }, {
        $project: {
          "activityId": "$_id",
          "activityName": "$dealIssueTranName",
          "activityType": "Deals",
          "activityClient": "$dealIssueTranClientId",
          "dealIssueParticipants": "$dealIssueParticipants"
        }
      }
    ])

    const relatedRFPTransactions = await RFP.aggregate([
      {
        $match: {
          rfpTranClientId: ObjectID(entityId)
        }
      }, {
        $project: {
          "activityId": "$_id",
          "activityName": "$rfpTranName",
          "activityType": "RFP",
          "activityClient": "$rfpTranIssuerId",
          "rfpParticipants": "$rfpParticipants"
        }
      }
    ])
    const transformedDealInformation = relatedDealTransactions.reduce((consDealDataObject, deal) => {
      const {
        dealIssueParticipants,
        ...otherParams
      } = deal

      const findUniqueParticipants = dealIssueParticipants.reduce((uniqueValues, ent, index) => {
        const {dealPartFirmId, dealPartFirmName} = ent
        if (index === 0) {
          return {
            [dealPartFirmId]: {
              entityId: dealPartFirmId,
              firmName: dealPartFirmName
            }
          }
        }
        if (uniqueValues[dealPartFirmId]) {
          return uniqueValues
        }
        return {
          ...uniqueValues,
          ...{
            [dealPartFirmId]: {
              [dealPartFirmId]: {
                entityId: dealPartFirmId,
                firmName: dealPartFirmName
              }
            }
          }
        }
      }, {})

      // get unique users for entity
      const findUniqueUsers = dealIssueParticipants.reduce((uniqueValues, part) => {
        const {dealPartContactName, dealPartContactId, dealPartFirmId, dealPartFirmName} = part
        if (uniqueValues[dealPartFirmId]) {
          const users = uniqueValues[dealPartFirmId]

          if (!users[dealPartContactId]) {
            users[dealPartContactId] = {
              userId: dealPartContactId
            }
          }
          return {
            ...uniqueValues,
            ...{
              [dealPartFirmId]: users
            }
          }
        }
        return {
          ...uniqueValues,
          ...{
            [dealPartFirmId]: {
              [dealPartContactId]: {
                userId: dealPartContactId,
                userFirstName: dealPartContactName,
                userLastName: dealPartContactName,
                entityId: dealPartFirmId,
                firmName: dealPartFirmName,
                relationshipType: "test"
              }
            }
          }
        }
      }, {})
      const {tranInfo, tranRelatedEntities, tranRelatedUsers} = consDealDataObject
      tranInfo[deal.activityId] = {
        ...otherParams
      }
      tranRelatedEntities[deal.activityId] = [...Object.values(findUniqueParticipants)]
      tranRelatedUsers[deal.activityId] = findUniqueUsers

      return {tranInfo, tranRelatedEntities, tranRelatedUsers}
    }, {
      tranInfo: {},
      tranRelatedEntities: {},
      tranRelatedUsers: {}
    })

    const transformedRFPInformation = relatedRFPTransactions.reduce((consRfpDataObject, rfp) => {

      const {
        rfpParticipants,
        ...otherParams
      } = rfp
      const findUniqueParticipants = rfpParticipants.reduce((uniqueValues, part, index) => {
        const {rfpParticipantFirmId, rfpParticipantFirmName} = part
        if (index === 0) {
          return {
            [rfpParticipantFirmId]: {
              entityId: rfpParticipantFirmId,
              firmName: rfpParticipantFirmName
            }
          }
        }
        if (uniqueValues[rfpParticipantFirmId]) {
          return uniqueValues
        }
        return {
          ...uniqueValues,
          ...{
            [rfpParticipantFirmId]: {
              entityId: rfpParticipantFirmId,
              firmName: rfpParticipantFirmName
            }
          }
        }
      }, {})
      // get unique users for entity
      const findUniqueUsers = rfpParticipants.reduce((uniqueValues, part) => {
        const {rfpParticipantContactName, rfpParticipantContactId, rfpParticipantFirmId, rfpParticipantFirmName} = part
        if (uniqueValues[rfpParticipantFirmId]) {
          const users = uniqueValues[rfpParticipantFirmId]

          if (!users[rfpParticipantContactId]) {
            users[rfpParticipantContactId] = {
              userId: rfpParticipantContactId
            }
          }
          return {
            ...uniqueValues,
            ...{
              [rfpParticipantFirmId]: users
            }
          }
        }
        return {
          ...uniqueValues,
          ...{
            [rfpParticipantFirmId]: {
              [rfpParticipantContactId]: {
                userId: rfpParticipantContactId,
                userFirstName: rfpParticipantContactName,
                userLastName: rfpParticipantContactName,
                entityId: rfpParticipantFirmId,
                firmName: rfpParticipantFirmName,
                relationshipType: "test"
              }
            }
          }
        }
      }, {})

      const {tranInfo, tranRelatedEntities, tranRelatedUsers} = consRfpDataObject
      tranInfo[rfp.activityId] = {
        ...otherParams
      }
      tranRelatedEntities[rfp.activityId] = [...Object.values(findUniqueParticipants)]
      tranRelatedUsers[rfp.activityId] = findUniqueUsers

      return {tranInfo, tranRelatedEntities, tranRelatedUsers}
    }, {
      tranInfo: {},
      tranRelatedEntities: {},
      tranRelatedUsers: {}
    })

    const consolidatedObject = {
      tranInfo: {
        ...transformedDealInformation.tranInfo,
        ...transformedRFPInformation.tranInfo
      },
      tranRelatedEntities: {
        ...transformedDealInformation.tranRelatedEntities,
        ...transformedRFPInformation.tranRelatedEntities
      },
      tranRelatedUsers: {
        ...transformedDealInformation.tranRelatedUsers,
        ...transformedRFPInformation.tranRelatedUsers
      }
    }
    res.send({
      ...allEntitiesConsolidated,
      ...consolidatedObject
    })
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveTask = () => async(req, res) => {

  const {user} = req
  const resRequested = [
    {
      resource: "Tasks",
      access: 2
    }
  ]
  const {taskData} = req.body
  const {taskRelatedActivities, taskAssignees} = taskData
  const taskList = []
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
      return
    }
    if (taskData.taskType !== "general" && (taskRelatedActivities && taskRelatedActivities.length > 0)) {
      taskRelatedActivities.forEach(activity => {
        taskAssignees.forEach(assignee => {
          const task = {
            taskDetails: {
              taskAssigneeUserId: ObjectID(assignee.userId),
              taskAssigneeName: `${assignee.userFirstName} ${assignee.userLastName}`,
              taskAssigneeType: "N/A",
              taskNotes: taskData.taskNotes,
              taskDescription: taskData.taskName,
              taskStartDate: taskData.taskStartDate,
              taskEndDate: taskData.taskDueDate,
              taskPriority: taskData.taskPriority,
              taskStatus: "Open",
              taskType: taskData.taskType,
              taskFollowing: taskData.userTaskFollowing
                ? taskData.userTaskFollowing
                : true
            },
            relatedActivityDetails: {
              activityId: ObjectID(activity.activityId),
              activityType: activity.activityType,
              activitySubType: activity.activitySubType,
              activityProjectName: activity.activityDescription
                ? activity.activityDescription
                : "",
              activityIssuerClientId: ObjectID(activity.activityTranClientId),
              activityIssuerClientName: activity.activityTranClientName,
              activityContext: "All",
              activityContextSection: "All",
              activityContextTaskId: null
            },
            taskAssigneeUserDetails: {
              userId: ObjectID(assignee.userId),
              userFirstName: assignee.userFirstName,
              userLastName: assignee.userLastName,
              userEntityId: ObjectID(assignee.entityId),
              userEntityName: assignee.userEntityName
            },
            relatedEntityDetails: {
              entityId: ObjectID(assignee.entityId),
              entityName: assignee.userEntityName,
              activityId: ObjectID(activity.activityId),
              activityName: activity.activityName,
              activityType: activity.activityType
            },
            relatedUserDetails: {
              relatedUserId: ObjectID(assignee.userId),
              relatedUserName: `${assignee.userFirstName} ${assignee.userLastName}`,
              relatedUserEntityId: ObjectID(assignee.entityId),
              relatedUserEntityName: assignee.userEntityName
            },
            taskRelatedDocuments: [taskData.taskRelatedDocuments],
            taskCreatedBy: taskData.taskCreatedBy
          }
          taskList.push(task)
        })
      })
    } else {
      taskAssignees.forEach(assignee => {
        const task = {
          taskDetails: {
            taskAssigneeUserId: ObjectID(assignee.userId),
            taskAssigneeName: `${assignee.userFirstName} ${assignee.userLastName}`,
            taskAssigneeType: "N/A",
            taskNotes: taskData.taskNotes,
            taskDescription: taskData.taskName,
            taskStartDate: taskData.taskStartDate,
            taskEndDate: taskData.taskDueDate,
            taskPriority: taskData.taskPriority,
            taskStatus: "Open",
            taskType: taskData.taskType,
            taskFollowing: taskData.userTaskFollowing
          },
          taskAssigneeUserDetails: {
            userId: ObjectID(assignee.userId),
            userFirstName: assignee.userFirstName,
            userLastName: assignee.userLastName,
            userEntityId: ObjectID(assignee.entityId),
            userEntityName: assignee.userEntityName
          },
          taskRelatedDocuments: [taskData.taskRelatedDocuments],
          taskCreatedBy: taskData.taskCreatedBy
        }
        taskList.push(task)
      })
    }
    const result = await Tasks.insertMany(taskList)
    const dataForEs = result.map(d => d._id)
    const bulkUpdateEs = elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: req.user.tenantId})
    await Promise.all([
      updateEligibleIdsForLoggedInUserFirm(req.user),
      bulkUpdateEs
    ])
    res.send({error: "", result})
  } catch (e) {
    console.log("error in task", e)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    // res.status(422).send({done:false,error:e,success:"",requestedServices:resRequ
    // ested})
  }
}

const updateTask = () => async(req, res) => {

  const {user} = req
  const resRequested = [
    {
      resource: "Tasks",
      access: 2
    }
  ]
  const {taskId, taskData} = req.body
  const {taskRelatedActivities, taskAssignees} = taskData
  let task
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
      return
    }
    if (taskData.taskType !== "general" && (taskRelatedActivities && taskRelatedActivities.length > 0)) {
      taskRelatedActivities.forEach(activity => {
        taskAssignees.forEach(assignee => {
          task = {
            taskDetails: {
              taskAssigneeUserId: ObjectID(assignee.userId),
              taskAssigneeName: `${assignee.userFirstName} ${assignee.userLastName}`,
              taskAssigneeType: "N/A",
              taskNotes: taskData.taskNotes,
              taskDescription: taskData.taskName,
              taskStartDate: taskData.taskStartDate,
              taskEndDate: taskData.taskDueDate,
              taskPriority: taskData.taskPriority,
              taskStatus: "Open",
              taskType: taskData.taskType,
              taskUnread: taskData.taskUnread
                ? taskData.taskUnread
                : true,
              taskFollowing: taskData.userTaskFollowing
                ? taskData.userTaskFollowing
                : true
            },
            relatedActivityDetails: {
              activityId: ObjectID(activity.activityId),
              activityType: activity.activityType,
              activitySubType: activity.activitySubType,
              activityProjectName: activity.activityDescription
                ? activity.activityDescription
                : "",
              activityIssuerClientId: ObjectID(activity.activityTranClientId),
              activityIssuerClientName: activity.activityTranClientName,
              activityContext: "All",
              activityContextSection: "All",
              activityContextTaskId: null
            },
            taskAssigneeUserDetails: {
              userId: ObjectID(assignee.userId),
              userFirstName: assignee.userFirstName,
              userLastName: assignee.userLastName,
              userEntityId: ObjectID(assignee.entityId),
              userEntityName: assignee.userEntityName
            },
            relatedEntityDetails: {
              entityId: ObjectID(assignee.entityId),
              entityName: assignee.userEntityName,
              activityId: ObjectID(activity.activityId),
              activityName: activity.activityName,
              activityType: activity.activityType
            },
            relatedUserDetails: {
              relatedUserId: ObjectID(assignee.userId),
              relatedUserName: `${assignee.userFirstName} ${assignee.userLastName}`,
              relatedUserEntityId: ObjectID(assignee.entityId),
              relatedUserEntityName: assignee.userEntityName
            },
            taskRelatedDocuments: [taskData.taskRelatedDocuments],
            taskCreatedBy: taskData.taskCreatedBy
          }
        })
      })
    } else {
      taskAssignees.forEach(assignee => {
        task = {
          taskDetails: {
            taskAssigneeUserId: ObjectID(assignee.userId),
            taskAssigneeName: `${assignee.userFirstName} ${assignee.userLastName}`,
            taskAssigneeType: "N/A",
            taskNotes: taskData.taskNotes,
            taskDescription: taskData.taskName,
            taskStartDate: taskData.taskStartDate,
            taskEndDate: taskData.taskDueDate,
            taskPriority: taskData.taskPriority,
            taskStatus: "Open",
            taskType: taskData.taskType,
            taskFollowing: taskData.userTaskFollowing,
            taskUnread: taskData.taskUnread || true

          },
          taskAssigneeUserDetails: {
            userId: ObjectID(assignee.userId),
            userFirstName: assignee.userFirstName,
            userLastName: assignee.userLastName,
            userEntityId: ObjectID(assignee.entityId),
            userEntityName: assignee.userEntityName
          },
          taskRelatedDocuments: [taskData.taskRelatedDocuments],
          taskCreatedBy: taskData.taskCreatedBy
        }
      })
    }
    const result = await Tasks.update({
      _id: taskId
    }, {
      $set: {
        ...task
      }
    })
    await Promise.all([
      updateEligibleIdsForLoggedInUserFirm(req.user),
      returnFromES(req.user.tenantId, result._id)
    ])

    res.send({error: "", result})
  } catch (e) {
    console.log("error in task", e)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    // res.status(422).send({done:false,error:e,success:"",requestedServices:resRequ
    // ested})
  }
}

const getAllTask = () => async(req, res) => {
  res.send("test")
}

const manageTasksForProcessCheckLists = () => async(req, res) => {
  const {processId} = req.body
  try {
    const ret = await manageTasksForProcessChecklists(processId, req.user)
    res
      .status(200)
      .send({
        ...ret
      })
  } catch (e) {
    console.log(e)
    res
      .status(422)
      .send({done: false, message: `Unable to manage tasks for the transaction - ${processId}`})
  }
}

const getTasksTaskByFirm = () => async(req, res) => {
  try {
    const {filter} = req.query
    let tasks = []
    if (filter === "My Firm Tasks" || filter === "My Tasks") {
      const query = filter === "My Firm Tasks"
        ? {
          "taskAssigneeUserDetails.userEntityId": req.user.entityId
        }
        : {
          "taskAssigneeUserDetails.userId": req.user._id
        }
      tasks = await Tasks
        .find(query)
        .select("taskIdentifier taskDetails relatedActivityDetails")
      if (tasks && tasks.length) {
        tasks = tasks.map(task => ({
          _id: task._id,
          taskIdentifier: task.taskIdentifier,
          ...task.taskDetails,
          ...task.relatedActivityDetails
        }))
      }
    }

    res.json(tasks)
  } catch (e) {
    console.log(e)
    res
      .status(422)
      .send({done: false, message: e.message})
  }
}

const postActivityTask = () => async(req, res) => {
  try {
    const {
      taskDetails,
      relatedActivityDetails,
      taskAssigneeUserDetails,
      relatedEntityDetails,
      relatedContacts,
      taskCreatedBy,
      _id
    } = req.body
    const {taskHistory} = req.body
    if (_id) {
      const taskDetail = await Tasks.findById({_id})
      const history = taskDetail.taskHistory
      history.push(taskHistory)
      const payload = {
        taskDetails,
        relatedActivityDetails,
        taskHistory: history,
        taskAssigneeUserDetails,
        relatedEntityDetails,
        relatedContacts,
        taskCreatedBy
      }

      await Tasks.update({
        _id
      }, payload)
      const tasksData = Tasks.find({"relatedActivityDetails.activityId": relatedActivityDetails.activityId})
      const es = returnFromES(req.user.tenantId, _id)
      const [tasks] = await Promise.all([tasksData, es])
      const taskGroupBy = {}
      tasks.forEach(task => {
        if (taskGroupBy.hasOwnProperty(`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`)) {
          taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`].push(task)
        } else {
          taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`] = [task]
        }
      })
      res.json({tasks, taskGroupBy})
    } else {
      const payload = {
        taskDetails,
        relatedActivityDetails,
        taskHistory: [taskHistory],
        taskAssigneeUserDetails,
        relatedEntityDetails,
        relatedContacts,
        taskCreatedBy
      }
      const newTask = new Tasks(payload)

      newTask.save(async(err, addedTask) => {
        if (err) {
          res
            .status(422)
            .send(err)
        } else {
          await ActBusDev.update({
            _id: ObjectID(addedTask.relatedActivityDetails.activityId)
          }, {
            $addToSet: {
              tasks: ObjectID(addedTask._id)
            }
          })
          const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
          const tasksData = Tasks.find({"relatedActivityDetails.activityId": relatedActivityDetails.activityId})
          const es = returnFromES(req.user.tenantId, addedTask._id)
          const [tasks] = await Promise.all([tasksData, es, ent])

          const taskGroupBy = {}

          tasks.forEach(task => {
            if (taskGroupBy.hasOwnProperty(`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`)) {
              taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`].push(task)
            } else {
              taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`] = [task]
            }
          })
          res.json({tasks, taskGroupBy})
        }
      })
    }
  } catch (e) {
    console.log(e)
    res
      .status(422)
      .send({done: false, message: e.message})
  }
}

const fetchActivityTask = () => async(req, res) => {
  try {
    const {activityId} = req.query
    const tranTask = await ActBusDev
      .findOne({_id: activityId})
      .select("tasks actTranFirmId actIssuerClient actProjectName actIssuerClientEntityName act" +
          "Type")
      .populate({path: "tasks", model: Tasks})

    if (tranTask) {
      const tasks = tranTask.tasks || []
      const taskGroupBy = {
        [`${tranTask.actProjectName} (${tranTask.actIssuerClientEntityName})`]: []
      }
      tasks.forEach(task => {
        taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`].push(task)
      })
      const taskDetails = {
        [`${tranTask.actProjectName} (${tranTask.actIssuerClientEntityName})`]: {
          activityId: `${tranTask._id}`,
          activityProjectName: `${tranTask.actProjectName}`,
          activitySubType: "businessDevelopment",
          activityType: "BusinessDevelopment",
          activityIssuerClientId: `${tranTask.actIssuerClient}`,
          activityIssuerClientName: `${tranTask.actIssuerClientEntityName}`
        }
      }
      res
        .status(200)
        .json({tasks, taskGroupBy, taskDetails})
    } else {
      res
        .status(404)
        .json({tasks: [], taskGroupBy: {}, taskDetails: {}, done: true, message: "Activity not found"})
    }
  } catch (e) {
    console.log(e)
    res
      .status(422)
      .send({done: false, message: e.message})
  }
}

const getTasksById = () => async(req, res) => {
  const {taskId} = req.params
  const {user} = req
  const resRequested = [
    {
      resource: "Tasks",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
      return
    }
    const taskData = await Tasks.findById(taskId)
    const taskResult = {
      taskName: taskData.taskDetails.taskDescription,
      taskNotes: taskData.taskDetails.taskNotes,
      taskType: taskData.taskDetails.taskType,
      taskPriority: taskData.taskDetails.taskPriority,
      taskStartDate: taskData.taskDetails.taskStartDate,
      taskDueDate: taskData.taskDetails.taskEndDate,
      userTaskFollowing: taskData.taskDetails.taskFollowing,
      taskCreatedBy: {
        ...taskData.taskCreatedBy
      }
    }
    taskResult.taskAssignees = {
      entityId: taskData.taskAssigneeUserDetails.userEntityId,
      userEntityName: taskData.taskAssigneeUserDetails.userEntityName,
      userId: taskData.taskAssigneeUserDetails.userId,
      userFirstName: taskData.taskAssigneeUserDetails.userFirstName,
      userLastName: taskData.taskAssigneeUserDetails.userLastName
    }
    if (taskData.taskDetails.taskType !== "general") {
      taskResult.taskRelatedActivities = {
        activityId: taskData.relatedActivityDetails.activityId,
        activityType: taskData.relatedActivityDetails.activityType,
        activitySubType: taskData.relatedActivityDetails.activitySubType,
        activityDescription: taskData.relatedActivityDetails.activityProjectName,
        activityTranClientId: taskData.relatedActivityDetails.activityIssuerClientId,
        activityTranClientName: taskData.relatedActivityDetails.activityIssuerClientName
      }
      taskResult.taskRelatedEntities = {
        activityId: taskData.relatedActivityDetails.activityId,
        activityName: taskData.relatedActivityDetails.activityProjectName,
        activityType: taskData.relatedActivityDetails.activityType,
        entityId: taskData.relatedEntityDetails.entityId,
        firmName: taskData.relatedEntityDetails.entityName
      }
    } else {
      taskResult.taskRelatedActivities = {}
      taskResult.taskRelatedEntities = {}
    }
    taskResult.taskRelatedDocuments = [...taskData.taskRelatedDocuments]
    res.json(taskResult)

  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putTransactionDocStatuse = () => async(req, res) => {
  const {taskId} = req.params
  const {docStatus} = req.body
  try {
    if (docStatus) {
      const {_id, docStatus} = req.body
      await Tasks.updateOne({
        _id: ObjectID(taskId),
        "taskRelatedDocuments._id": ObjectID(_id)
      }, {
        $set: {
          "taskRelatedDocuments.$.docStatus": docStatus
        }
      })
    } else {
      await Tasks.update({
        _id: taskId
      }, {
        $addToSet: {
          taskRelatedDocuments: req.body.taskRelatedDocuments
        }
      })
    }

    const [tasksDocuments] = await Promise.all([
      Tasks
        .findOne({_id: taskId})
        .select("taskRelatedDocuments"),
      returnFromES(req.user.tenantId, taskId)
    ])
    res.json(tasksDocuments)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: ""})
  }
}

const delTransactionDocStatuse = () => async(req, res) => {
  const {taskId} = req.params
  const {documentId} = req.query
  try {
    await Tasks.update({
      _id: taskId
    }, {
      $pull: {
        "taskRelatedDocuments": {
          _id: documentId
        }
      }
    })
    const [taskDocuments] = await Promise.all([
      Tasks
        .findOne({_id: taskId})
        .select("taskRelatedDocuments"),
      returnFromES(req.user.tenantId, taskId)
    ])

    res.json(taskDocuments)
  } catch (error) {
    console.log("******Others*******removeParticipantByPartId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: ""})
  }
}

const toggleReadAndUnreadStatusForTask = () => async(req, res) => {
  const {taskId} = req.params
  const existingDocument = await Tasks.aggregate([
    {
      $match: {
        _id: ObjectID(taskId)
      }
    }, {
      $project: {
        unreadStatus: {
          $ifNull: ["$taskDetails.taskUnread", true]
        }
      }
    }
  ])

  const {unreadStatus} = existingDocument[0] || {
    unreadStatus: undefined
  }

  try {
    let message = ""
    if (unreadStatus === true) {
      await Tasks.findOneAndUpdate({
        _id: ObjectID(taskId)
      }, {
        $set: {
          "taskDetails.taskUnread": false
        }
      }, {upsert: true}).select({_id: 1})
      message = "Task toggled to unread from read"
      await returnFromES(req.user.tenantId, taskId)
      res
        .status(200)
        .send({done: true, message})
    } else if (unreadStatus === false) {
      await Tasks.findOneAndUpdate({
        _id: ObjectID(taskId)
      }, {
        $set: {
          "taskDetails.taskUnread": true
        }
      }, {upsert: true}).select({_id: 1})
      message = "Task Toggled to read to unread"
      await returnFromES(req.user.tenantId, taskId)
      res
        .status(200)
        .send({done: true, message})
    } else {
      res
        .status(400)
        .send({done: false, message: "The task that you are trying to edit doesn't exist"})
    }
  } catch (e) {
    res
      .status(500)
      .send({done: false, message: "There was an error toggling the read status on the flag"})
  }
}

const getSpecificTask = () => async(req, res) => {
  try {
    const {id} = req.query
    const task = await Tasks.findOne({"taskDetails.taskNotes": id})

    if (task) {
      res.json(task)
    } else {
      res.json({status: true, message: "not found"})
    }
  } catch (error) {
    res
      .status(422)
      .send({done: false, error})
  }
}

export default generateControllers(Tasks, {
  getTasksContextData: getTaskContextData(),
  manageTasksForProcessCheckLists: manageTasksForProcessCheckLists(),
  getAllTask: getAllTask(),
  saveTask: saveTask(),
  updateTask: updateTask(),
  getTasksTaskByFirm: getTasksTaskByFirm(),
  postActivityTask: postActivityTask(),
  fetchActivityTask: fetchActivityTask(),
  getTasksById: getTasksById(),
  putTransactionDocStatuse: putTransactionDocStatuse(),
  delTransactionDocStatuse: delTransactionDocStatuse(),
  toggleReadStatus: toggleReadAndUnreadStatusForTask(),
  getSpecificTask: getSpecificTask()
})
