import {generateControllers} from "./../../../modules/query"
import {SuperVisoryObligations} from "./svandobligations.model"
import {Entity} from "../../entity/entity.model"
import {ProfQualifications} from "../ProfessionalQualifications/profqualifications.model";

const {ObjectID} = require("mongodb")

const getSuperVisoryObligationsDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "SuperVisoryObligations", access: 1},
  ]
  const {type} = req.query
  try {
    let select = ""
    if(type === "documents"){
      select = "finAdvisorEntityId supervisoryObligationsDocuments"
    }else {
      select = "finAdvisorEntityId annualCertification responsibleSupervision auditLogs supervisoryObligationsDocuments"
    }
    const svcoDetails = await SuperVisoryObligations.findOne({finAdvisorEntityId: req.user.entityId}).select(select)
    res.json(svcoDetails)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const putSvcoSupervisionCertifications = () => async (req, res) => {
  const resRequested = [
    {resource:"SuperVisoryObligations",access:2},
    {resource:"Entity",access:2},
  ]
  const {type} = req.query
  try {
    let svcoDetails = await SuperVisoryObligations.findOne({finAdvisorEntityId: req.user.entityId})
    const details = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const {_id } = req.body

    if(type === "responsibleSupervision" || type === "annualCertification") {
      if (svcoDetails) {
        if (_id) {
          const dndString1 = `${type}._id`
          const dndString2 = `${type}.$`
          const query1 = {finAdvisorEntityId: ObjectID(req.user.entityId), [dndString1]: ObjectID(_id)}
          const query2 = {$set: {[dndString2]: req.body}}
          await SuperVisoryObligations.updateOne(query1, query2)
        } else {
          await SuperVisoryObligations.updateOne(
            {finAdvisorEntityId: ObjectID(req.user.entityId)},
            {$addToSet: {[type]: req.body}}
          )
        }
      } else {
        details.push(req.body)
        const newGenAdmin = new SuperVisoryObligations({
          finAdvisorEntityId: req.user.entityId,
          finAdvisorEntityName: (entity && entity.firmName) || "",
          [type]: details
        })
        await newGenAdmin.save()
      }

      svcoDetails = await SuperVisoryObligations.findOne({finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(svcoDetails)

    }else {
      res.status(404).send({done:false,error:"pass query params"})
    }

  } catch(error) {
    console.log("======================", error)
    res.status(422).send({done:false, error})
  }
}

const pullSvcoSupervisionCertifications = () => async (req, res) => {
  const { pullId } = req.params
  const {type} = req.query
  const resRequested = [
    {resource:"SuperVisoryObligations",access:2},
  ]
  try {
    if(type === "responsibleSupervision" || type === "annualCertification") {
      await SuperVisoryObligations.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { [type]: { _id: pullId } }})
      const svcoDetails = await SuperVisoryObligations.findOne({ finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(svcoDetails)
    }else {
      res.status(404).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putSvcoDocuments = () => async (req, res) => {
  const {details} = req.query
  const resRequested = [
    {resource:"SuperVisoryObligations",access:2},
  ]
  try {
    let svcoDetails = await SuperVisoryObligations.findOne({finAdvisorEntityId: req.user.entityId})
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")

    if(details === "docStatus"){
      const { _id, docStatus} = req.body
      await SuperVisoryObligations.updateOne(
        { finAdvisorEntityId: req.user.entityId, "supervisoryObligationsDocuments._id": ObjectID(_id)},
        { $set: { "supervisoryObligationsDocuments.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const {_id} = req.body
      await SuperVisoryObligations.updateOne(
        { finAdvisorEntityId: req.user.entityId, "supervisoryObligationsDocuments._id": ObjectID(_id)},
        { $set: { "supervisoryObligationsDocuments.$" : req.body } })
    }else if (svcoDetails) {
      await SuperVisoryObligations.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {supervisoryObligationsDocuments: req.body.supervisoryObligationsDocuments}})
    } else {
      const newSvcoDetails = new SuperVisoryObligations({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        supervisoryObligationsDocuments: req.body.supervisoryObligationsDocuments
      })
      await newSvcoDetails.save()
    }
    svcoDetails = await SuperVisoryObligations.findOne({ finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId supervisoryObligationsDocuments")
    res.json(svcoDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export const pullSvcoDocuments = () => async (req, res) => {
  const { docId } = req.query
  try {
    if(docId) {

      await SuperVisoryObligations.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { supervisoryObligationsDocuments: { _id: docId } }})

      const document = await SuperVisoryObligations.findOne({ finAdvisorEntityId: req.user.entityId}).select("supervisoryObligationsDocuments")

      res.status(200).json(document)
    }else {

      res.status(422).send({done:false,error:"This Document doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"SuperVisoryObligations",access:2},
  ]
  try {
    await SuperVisoryObligations.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {auditLogs: req.body} })
    const svcoDetails = await SuperVisoryObligations.findOne({ finAdvisorEntityId: req.user.entityId}).select("auditLogs")
    res.json(svcoDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export default generateControllers(SuperVisoryObligations, {
  getSuperVisoryObligationsDetails: getSuperVisoryObligationsDetails(),
  putSvcoSupervisionCertifications: putSvcoSupervisionCertifications(),
  pullSvcoSupervisionCertifications: pullSvcoSupervisionCertifications(),
  putSvcoDocuments: putSvcoDocuments(),
  pullSvcoDocuments: pullSvcoDocuments(),
  putAuditLogs: putAuditLogs(),
})
