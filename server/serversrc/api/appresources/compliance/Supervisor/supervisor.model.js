import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docNote:String,
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const auditLogs = Schema({
  superVisorModule:String, // General Admin Activities,
  superVisorSubSection:String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const politicalContributionsByUsers = Schema({
  userId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  // yearOfDisclosureUser:Number, // Store this as an Integer - Should be the same as yearOfDisclosureFirm
  // quarterOfDisclosureUser:Number, // Store 1,2,,4 - Should be the same as quarterOfDisclosureFirm
  userHasPoliticalContribForQuarter:{type:Boolean,default:false}, // Default to False

  disclosureDetails:{
    disclosureSection1WithNegativeAnswers:[{
      sectionHeader:String,
      sectionItems:[{
        sectionItemDesc:String,
        sectionAffirm:Boolean
      }]
    }],
    disclosureSectionWithPositiveAnswers:[{
      sectionHeader:String,
      sectionItems:[{
        sectionItemDesc:String,
        sectionAffirm:Boolean
      }]
    }],
    disclosureFor:String,
    disclosureAffirmationNotes:String,
    disclosureG37Obligation:Boolean,

    // This section will be enabled if the firm
    disclosureContributor:{
      address:String
    },
    contribMuniEntityByState:[{
      state:String,
      contributionReceipientDetails:{
        userId:Schema.Types.ObjectId,
        userFirstName:String,
        userLastName:String,
        userAddressConsolidated:String
      },
      contributorCategory:String,
      contributionAmount:Number,
      contributionDate:Date,
      exemptionG37RuleExemption:String
    }],

    paymentsToPartiesByState:[{
      state:String,
      politicalPartyDetails:{
        partyName:String,
        partyAddress:String
      },
      contributorCategory:String,
      paymentAmount:Number,
      paymentDate:Date
    }],
    ballotContribByState:[{
      state:String,
      ballotDetail:String,
      contributorCategory:String,
      contributionAmount:Number,
      contributionDate:Date
    }],

    reimbursements:[{
      state:String,
      ballotDetail:String,
      thirdPartyDetails:{
        userId:Schema.Types.ObjectId,
        userFirstName:String,
        userLastName:String,
        userAddressConsolidated:String
      },
      thirdPartyCategory:String,
      reimbursementAmt:Number,
      reimbursementDate:Date
    }],

    ballotApprovedOfferings:[{
      entityId:Schema.Types.ObjectId,
      nameMunicipal: String,
      processDescription:String, // Needs to be Project description from deal issue only
      reportablesaleDate:Date,
    }],
    contributionsRelatedDocuments:[superVisorDocumentSchema],
  }
})

const contributorSchema = Schema({
  entityId:Schema.Types.ObjectId,
  entityName:String,
  userId:Schema.Types.ObjectId,
  userFirstName:String,
  userMiddleName:String,
  userLastName:String,
  userFirmName:String,
  userPrimaryEmailId:String,
  userPrimaryPhone:String,
  userPrimaryFax:String,
  userAddressName:String,
  userAddressType:String,
  userAddressLine1:String,
  userAddressLine2:String,
  country:String,
  state:String,
  city:String,
  zip1:String,
  zip2:String,
})

const politicalContributionsOverallSchema = Schema({
  polContribYearOfDisclosureFirm:Number, // Store this as an Integer
  polContribQuarterOfDisclosureFirm:Number, // Store 1,2,,4
  polContribUsers:[politicalContributionsByUsers],
  polContribMsrbSubmissions:[superVisorDocumentSchema],
  polContribG37Generated:Boolean,
  polContribG37GeneratedDate:Boolean,
  lastUpdateDate:{type:Date,required: true,default: Date.now},
})

const policiesAndProceduresSchema = Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  status:String,
  reviewer: String,
  subject: String,
  notes: String,
  reviewerRole: String,
  lastReviewedOn: Date,
  lastUpdateBy: String,
  docAWSFileLocation:String,
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docActions:{
    actionType:String,
    actionDate:Date
  },
  history: [],
  auditLogs: [],
  docCommunication:{
    owners:[{
      userId:Schema.Types.ObjectId,
      userFirstName:String,
      userEntityId:Schema.Types.ObjectId,
      userLastName:String,
      userPrimaryEmailId:String,
      sendEmailTo:String,
      name: String,
      role: String
    }],
    toRecipients:[{
      userId:Schema.Types.ObjectId,
      userFirstName:String,
      userEntityId:Schema.Types.ObjectId,
      sendEmailTo:String,
      userLastName:String,
      userPrimaryEmailId:String,
    }],
    ccRecipients:[{
      userId:Schema.Types.ObjectId,
      userFirstName:String,
      userEntityId:Schema.Types.ObjectId,
      sendEmailTo:String,
      userLastName:String,
      userPrimaryEmailId:String,
    }],
    // communicationSubject:String,
    // communicationNotes:String
  },
  docReviewers:[{
    userId:Schema.Types.ObjectId,
    userFirstName:String,
    userEntityId:Schema.Types.ObjectId,
    userLastName:String,
    userPrimaryEmailId:String,
    userDocReviewDate:Date
  }],
  createDate:Date,
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }

})

const cmplsupervisor = Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,

  // Policies and Procedures Schema
  svPoliciesAndProcedures:[policiesAndProceduresSchema],

  // This is one consolidated audit trail for all modules
  auditLogs:[auditLogs]
}).plugin(timestamps)


export const ComplSupervisor =  mongoose.model("cmplsupervisor", cmplsupervisor)
