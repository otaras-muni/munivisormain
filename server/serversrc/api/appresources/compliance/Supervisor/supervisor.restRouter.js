import express from "express"
import supervisorController from "./supervisor.controller"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const supervisorRouter = express.Router()

supervisorRouter.param("id", supervisorController.findByParam)

supervisorRouter.route("/")
  .get(requireAuth,supervisorController.getAll)
  .post(requireAuth, supervisorController.createOne)

supervisorRouter.route("/details")
  .get(requireAuth, supervisorController.getSupervisorDetails)

supervisorRouter.route("/policiesandprocedures")
  .put(requireAuth, supervisorController.putSupervisorPoliciesAndProcedures)
  .get(requireAuth, supervisorController.getSupervisorList)
  .delete(requireAuth, supervisorController.pullSupervisorPoliciesAndProcedures)

supervisorRouter.route("/auditlogs")
  .post(requireAuth, supervisorController.putAuditLogs)
