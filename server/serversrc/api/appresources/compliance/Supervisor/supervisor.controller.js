import {generateControllers} from "./../../../modules/query"
import {ComplSupervisor} from "./supervisor.model"
import {Entity} from "../../entity/entity.model"
import {EntityUser} from "../../entityUser/entityUser.model"

const {ObjectID} = require("mongodb")

const getSupervisorDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "ComplSupervisor", access: 1},
  ]
  const {type} = req.query
  try {
    let select = ""
    if(type === "policies"){
      select = "finAdvisorEntityId svPoliciesAndProcedures auditLogs"
    }else {
      select = "finAdvisorEntityId"
    }
    const supervisorDetail = await ComplSupervisor.findOne({finAdvisorEntityId: req.user.entityId}).select(select)
    res.json(supervisorDetail)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const putSupervisorPoliciesAndProcedures = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplSupervisor",access:2},
    {resource:"Entity",access:2},
  ]
  try {
    let supervisorDetail = await ComplSupervisor.findOne({finAdvisorEntityId: req.user.entityId})
    const details = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const {_id } = req.body

    if (supervisorDetail) {
      if (_id) {
        const query1 = {finAdvisorEntityId: ObjectID(req.user.entityId), "svPoliciesAndProcedures._id": ObjectID(_id)}
        const query2 = {$set: {"svPoliciesAndProcedures.$": req.body}}
        await ComplSupervisor.updateOne(query1, query2)
      } else {
        await ComplSupervisor.updateOne(
          {finAdvisorEntityId: ObjectID(req.user.entityId)},
          {$addToSet: {svPoliciesAndProcedures: req.body}}
        )
      }
    } else {
      details.push(req.body)
      const newGenAdmin = new ComplSupervisor({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        svPoliciesAndProcedures: details
      })
      await newGenAdmin.save()
    }
    supervisorDetail = await ComplSupervisor.findOne({finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId svPoliciesAndProcedures")
    res.json(supervisorDetail)
  } catch(error) {
    console.log("======================", error)
    res.status(422).send({done:false, error})
  }
}

const getSupervisorList = () => async (req, res) => {

  const resRequested = [
    {resource:"ComplSupervisor",access:2},
    {resource:"Entity",access:2},
  ]
  try {
    const a = ["Supervisory Principal", "Compliance Officer"]
    let supervisorDetail = await EntityUser.find({userFlags: { $in: a }, entityId: req.user.entityId }).select("entityId userFlags userFirmName userFirstName userLastName userEmails _id")
    supervisorDetail = supervisorDetail.map((item) => ({
      ...item._doc,
      role: item.userFlags,
    }))

    supervisorDetail.map((detail, i) => {
      detail.role.map((roles) => {
        if (roles === "Compliance Officer" || roles === "Supervisory Principal" ) {
          const supervisorRole = roles === "Compliance Officer" ? "Compliance Officer" : roles === "Supervisory Principal" ? "Supervisory Principal" : roles
          supervisorDetail[i].role.splice(0, supervisorDetail[i].role.length)
          supervisorDetail[i].role = supervisorRole
        }
      })
      delete detail.userFlags
    })

    console.log("====================88=====================>", supervisorDetail)

    res.json(supervisorDetail)
  } catch(error) {
    console.log("======================", error)
    res.status(422).send({done:false, error})
  }
}

export const pullSupervisorPoliciesAndProcedures = () => async (req, res) => {
  const { docId } = req.query
  try {
    if(docId) {

      await ComplSupervisor.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { svPoliciesAndProcedures: { _id: docId } }})

      const document = await ComplSupervisor.findOne({ finAdvisorEntityId: req.user.entityId}).select("svPoliciesAndProcedures")

      res.status(200).json(document)
    }else {

      res.status(422).send({done:false,error:"This Document doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplSupervisor",access:2},
  ]
  try {
    await ComplSupervisor.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {auditLogs: req.body} })
    const supervisorDetail = await ComplSupervisor.findOne({ finAdvisorEntityId: req.user.entityId}).select("auditLogs")
    res.json(supervisorDetail)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export default generateControllers(ComplSupervisor,{
  getSupervisorDetails: getSupervisorDetails(),
  putSupervisorPoliciesAndProcedures: putSupervisorPoliciesAndProcedures(),
  getSupervisorList: getSupervisorList(),
  pullSupervisorPoliciesAndProcedures: pullSupervisorPoliciesAndProcedures(),
  putAuditLogs: putAuditLogs(),
})
