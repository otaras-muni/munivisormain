import ip from "ip"
import {generateControllers} from "./../../../modules/query"
import {ComplaintDetails} from "./svcomplaintdetails.model"
import {Entity} from "../../entity/entity.model"
import {EntityUser} from "../../entityUser/entityUser.model"
const {ObjectID} = require("mongodb")

const getComplaints = () => async (req, res) => {
  const resRequested = [
    {resource: "ComplaintDetails", access: 1},
  ]
  try {
    const complaintDetails = await ComplaintDetails.find({finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId finAdvisorEntityName keyDetails createdAt updatedAt lastUpdateDate")
    res.json(complaintDetails)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const getComplaintDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "ComplaintDetails", access: 1},
  ]
  const { type } = req.params
  const { comId } = req.query
  try {
    if(type && comId) {
      let select = "finAdvisorEntityId finAdvisorEntityName"

      if(type === "keyDetails" || type === "complainantDetails" || type === "complainantAssocPersonDetails") {
        select = `finAdvisorEntityId finAdvisorEntityName ${type}`
      }else if(type === "alldetails") {
        select = "finAdvisorEntityId finAdvisorEntityName keyDetails complainantDetails complainantAssocPersonDetails complaintDocuments auditLogs"
      }

      const complaintDetails = await ComplaintDetails.findOne({_id: comId}).select(select)
      res.json(complaintDetails)
    }else {
      res.status(404).send({done:false,error:"pass correct params"})
    }
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const putComplaintDetails = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplaintDetails",access:2},
  ]
  const { type } = req.params
  const { comId } = req.query
  try {

    let complaintDetails = {}
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const { _id } = req.body

    if(type === "keyDetails" || type === "complainantDetails" || type === "complainantAssocPersonDetails") {

      if(comId) {

        if(type === "keyDetails" || type === "complainantDetails") {
          if(type === "complainantDetails" && !req.body.userId){
            const newUser = new EntityUser({
              _id: ObjectID(),
              entityId: req.body.entityId,
              userFirmName: req.body.entityName,
              userFirstName: req.body.userFirstName,
              userMiddleName: req.body.userMiddleName,
              userLastName: req.body.userLastName,
              userEmails:[{
                emailId:req.body.userPrimaryEmailId,
                emailPrimary: true,
              }],
              userPhone: [{
                phoneNumber: req.body.userPrimaryPhone,
                extension: "",
                phonePrimary: true
              }],
              userFax: [{
                faxNumber: req.body.userFax,
                faxPrimary: true
              }],
              userAddresses:[req.body.userAddressConsolidated],
            })
            newUser.save(async (err, user) => {
              if(err){
                res.status(422).send(err)
              }else {
                const details = req.body
                details.userId = user._id
                await ComplaintDetails.findByIdAndUpdate({_id: comId}, { [type] : details })
                complaintDetails = await ComplaintDetails.findOne({_id: comId}).select(`finAdvisorEntityId finAdvisorEntityName ${type}`)
                return res.json(complaintDetails)
              }
            })
          }else {
            await ComplaintDetails.findByIdAndUpdate({_id: comId}, { [type] : req.body })
          }
        }else {
          if (_id) {
            const dndString1 = `${type}._id`
            const dndString2 = `${type}.$`
            const query1 = { _id: ObjectID(comId), [dndString1]: ObjectID(_id)}
            const query2 = {$set: {[dndString2]: req.body}}
            await ComplaintDetails.updateOne(query1, query2)
          } else {
            await ComplaintDetails.update({ _id: comId}, {$addToSet: { complainantAssocPersonDetails: req.body} })
          }
        }
        complaintDetails = await ComplaintDetails.findOne({_id: comId}).select(`finAdvisorEntityId finAdvisorEntityName ${type}`)

      }else {
        ip.address()
        if (type === "keyDetails") {
          const newComplain = new ComplaintDetails({
            finAdvisorEntityId: req.user.entityId,
            finAdvisorEntityName: (entity && entity.firmName) || "",
            auditLogs: [{
              userName: `${req.user.userFirstName} ${req.user.userLastName}`,
              date: new Date(),
              log: "Compliance Created",
              ip: ip.address(),
              superVisorModule: "Client Complaints",
              superVisorSubSection: "Complaints"
            }],
            [type]: req.body
          })
          complaintDetails = await newComplain.save()
        }else {
          return  res.status(404).send({ done:false, error:`pass correct params ${type}`})
        }
      }

      res.json(complaintDetails)
    }else {
      res.status(404).send({done:false,error:"pass correct params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullCmplAssocPersonDetails = () => async (req, res) => {
  const { comId, pullId } = req.query
  const { type } = req.params
  const resRequested = [
    {resource:"ComplaintDetails",access:2},
  ]
  try {
    if(type === "complainantAssocPersonDetails") {
      await ComplaintDetails.update({ _id: comId }, { $pull: { [type]: { _id: pullId } }})
      const complaintDetails = await ComplaintDetails.findOne({  _id: comId }).select(`finAdvisorEntityId ${type}`)
      res.json(complaintDetails)
    }else {
      res.status(404).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putsvCmplDocuments = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplaintDetails",access:2},
  ]
  const { comId, details} = req.query

  try {
    if(details === "docStatus"){
      const { _id, docStatus} = req.body
      await ComplaintDetails.updateOne(
        { _id:  ObjectID(comId), "complaintDocuments._id": ObjectID(_id)},
        { $set: { "complaintDocuments.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const { _id } = req.body
      await ComplaintDetails.updateOne(
        { _id:  ObjectID(comId), "complaintDocuments._id": ObjectID(_id)},
        { $set: { "complaintDocuments.$" : req.body } })
    } else {
      await ComplaintDetails.update({ _id: comId }, {$addToSet: {complaintDocuments: req.body.complaintDocuments}})
    }
    const complaintDetails = await ComplaintDetails.findOne({  _id: comId }).select("finAdvisorEntityId complaintDocuments")
    res.json(complaintDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullsvCmplDocuments = () => async (req, res) => {
  const { comId, docId } = req.query
  try {
    if(docId) {

      await ComplaintDetails.update({ _id: comId }, { $pull: { complaintDocuments: { _id: docId } }})

      const document = await ComplaintDetails.findOne({ _id: comId }).select("complaintDocuments")

      res.status(200).json(document)
    }else {

      res.status(422).send({done:false,error:"This Document doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplaintDetails",access:2},
  ]
  const { comId } = req.query

  try {
    await ComplaintDetails.update({ _id: comId}, {$addToSet: { auditLogs: req.body} })
    const complaintDetails = await ComplaintDetails.findOne({  _id: comId  }).select("auditLogs")
    res.json(complaintDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export const pullComplaintsDocuments = () => async (req, res) => {
  const { docId } = req.query
  try {
    if(docId) {
      await ComplaintDetails.update(
        { _id: docId },
        { $set: { "keyDetails.docAWSFileLocation": "", "keyDetails.docFileName": ""}}
      )
      const document = await ComplaintDetails.findOne({ _id: docId }).select("keyDetails")
      res.status(200).json(document)
    } else {
      res.status(422).send({done:false,error:"This Document doesn't exists."})
    }
  } catch(error) {
    console.log("==============error=============>", error.message)
    res.status(422).send({done:false, error: error.message})
  }
}

export const pullComplaintsDetails = () => async (req, res) => {
  const { pullId } = req.query
  try {
    if(pullId) {
      await ComplaintDetails.remove({ _id: pullId })
      const complaintDetails = await ComplaintDetails.find({finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId finAdvisorEntityName keyDetails createdAt updatedAt lastUpdateDate")
      res.json(complaintDetails)
    } else {
      res.status(422).send({done:false,error:"This Complaints doesn't exists."})
    }
  } catch(error) {
    console.log("==============error=============>", error.message)
    res.status(422).send({done:false, error: error.message})
  }
}

export default generateControllers(ComplaintDetails, {
  getComplaints: getComplaints(),
  getComplaintDetails: getComplaintDetails(),
  putComplaintDetails: putComplaintDetails(),
  pullCmplAssocPersonDetails: pullCmplAssocPersonDetails(),
  putsvCmplDocuments: putsvCmplDocuments(),
  pullsvCmplDocuments: pullsvCmplDocuments(),
  putAuditLogs: putAuditLogs(),
  pullComplaintsDocuments: pullComplaintsDocuments(),
  pullComplaintsDetails: pullComplaintsDetails()
})
