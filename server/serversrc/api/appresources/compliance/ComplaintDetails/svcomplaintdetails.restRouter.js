import express from "express"
import complaintDetailsController from "./svcomplaintdetails.controller"
import { errorHandling } from "./validation"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const complaintDetailsRouter = express.Router()

complaintDetailsRouter.param("id", complaintDetailsController.findByParam)

complaintDetailsRouter.route("/")
  .get(requireAuth,complaintDetailsController.getAll)
  .post(requireAuth, complaintDetailsController.createOne)

complaintDetailsRouter.route("/complaints")
  .get(requireAuth, complaintDetailsController.getComplaints)

complaintDetailsRouter.route("/details/:type")
  .get(requireAuth, complaintDetailsController.getComplaintDetails)
  .put(requireAuth, errorHandling, complaintDetailsController.putComplaintDetails)
  .delete(requireAuth, complaintDetailsController.pullCmplAssocPersonDetails)

complaintDetailsRouter.route("/documents")
  .put(requireAuth, complaintDetailsController.putsvCmplDocuments)
  .delete(requireAuth, complaintDetailsController.pullsvCmplDocuments)

complaintDetailsRouter.route("/auditlogs")
  .post(requireAuth, complaintDetailsController.putAuditLogs)

complaintDetailsRouter.route("/complaintsDocuments")
  .delete(requireAuth, complaintDetailsController.pullComplaintsDocuments)

complaintDetailsRouter.route("/complaintsDetails")
  .delete(requireAuth, complaintDetailsController.pullComplaintsDetails)
