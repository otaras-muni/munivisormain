import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:{type: Schema.Types.ObjectId},
  docFileName:String,
  docNote:String,
  markedPublic: { publicFlag:Boolean, publicDate:Date},
  docStatus: String,
  sentToEmma: { emmaSendFlag:Boolean, emmaSendDate:Date},
  createdBy: {type: Schema.Types.ObjectId},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const svComplaintDetails = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  keyDetails:{
    complaintsName: String,
    organizationName: String,
    assPersonName: String,
    productCode:String,
    problemCode:String,
    complaintStatus:String,
    docAWSFileLocation: String,
    docFileName: String,
    dateReceived:Date,
    dateOfActivity:Date,
    activityDetails:[{
      relTranId: { type: Schema.Types.ObjectId},
      relTranActivityType:String,
      relTranActivitySubType:String,
      relTranIssueName: String,
      relTranProjectName: String,
      relTranClientId: { type: Schema.Types.ObjectId},
      relTranClientName: String,
    }],
    complaintDescription:String,
    actionAgainstComplaint:String,
    createdDate: { type: Date, required: true, default: Date.now },
  },
  complainantDetails:{
    entityId:Schema.Types.ObjectId,
    entityName:String,
    userId:Schema.Types.ObjectId,
    userFirstName:String,
    userLastName:String,
    userMiddleName:String,
    userPrimaryEmailId:String,
    userPrimaryPhone:String,
    userFax:String,
    userAddressConsolidated:{
      country:String,
      state:String,
      city:String,
      zipCode:{
        zip1:String,
        zip2:String
      },
      addressName:String,
      addressType:String,
      addressLine1:String,
      addressLine2:String
    }
  },
  complainantAssocPersonDetails:[{
    userId:Schema.Types.ObjectId,
    userFirstName:String,
    userMiddleName:String,
    userEntityId:Schema.Types.ObjectId,
    userEntityName:String,
    userLastName:String,
    userPrimaryEmailId:String,
    userPrimaryPhone:String,
    userPrimaryAddress:String,
  }],
  complaintDocuments:[superVisorDocumentSchema],
  auditLogs:[auditLogs],
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

export const ComplaintDetails = mongoose.model("svcomplaintdetails", svComplaintDetails)
