import { generateControllers } from "./../../../modules/query"
import { PoliContributionSummary } from "./poliContributionSummary.model"
import { Entity } from "../../entity/entity.model"

const getSummaryDetails = () => async (req, res) => {
  try {
    const { quarter, year, entityId } = req.query
    if(quarter && year && entityId){
      const summaryList = await PoliContributionSummary.find({quarter, year, entityId})
      res.json(summaryList)
    } else {
      res.status(400).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putSummaryDetails = () => async (req, res) => {
  try {
    const { quarter, year, entityId, type } = req.query

    const summaryList = await PoliContributionSummary.findOne({quarter, year, entityId})
    const firmName = await Entity.findOne({_id: entityId}).select("firmName")

    if(summaryList && summaryList._id){
      if(summaryList && summaryList[type] && summaryList[type].length){
        // const dndString1 = `${type}._id`
        const dndString2 = `${type}`
        const query1 = {_id: summaryList._id, entityId, /* [dndString1]: typeList[0][type][0]._id */ }
        const query2 = {$set: {[dndString2]: req.body}}
        await PoliContributionSummary.updateOne(query1, query2)
      } else {
        await PoliContributionSummary.updateOne(
          {_id: summaryList._id, entityId},
          {$addToSet: {[type]: req.body}}
        )
      }
      const contDetails = await PoliContributionSummary.findOne({quarter, year, entityId})
      res.json(contDetails)
    } else {
      const newConDetails = new PoliContributionSummary({
        entityId,
        entityName: firmName && firmName.firmName || "",
        year,
        quarter,
        [type]: req.body,
      })
      const contDetails = await newConDetails.save()
      res.json(contDetails)
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putSummaryDetailsStatus = () => async (req, res) => {
  try {
    const { quarter, year, entityId } = req.query

    const summaryList = await PoliContributionSummary.findOne({quarter, year, entityId})
    const firmName = await Entity.findOne({_id: entityId}).select("firmName")

    if(summaryList && summaryList._id){
      const query1 = {_id: summaryList._id, entityId, /* [dndString1]: typeList[0][type][0]._id */ }
      const query2 = {$set: {emmaSubmissionStatus: req.body.emmaSubmissionStatus}}
      await PoliContributionSummary.updateOne(query1, query2)
      const contDetails = await PoliContributionSummary.findOne({quarter, year, entityId})
      res.json(contDetails)
    } else {
      const newConDetails = new PoliContributionSummary({
        entityId,
        entityName: firmName && firmName.firmName || "",
        year,
        quarter,
        emmaSubmissionStatus: req.body.emmaSubmissionStatus,
      })
      const contDetails = await newConDetails.save()
      res.json(contDetails)
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const postSummaryDetailsDocuments = () => async (req, res) => {
  const { details, summaryId, year, quarter, entityId} = req.query
  try {

    const firmName = await Entity.findOne({_id: entityId}).select("firmName")
    if(summaryId){

      let politicalContSummary = await PoliContributionSummary.findOne({ _id: summaryId })

      if(details === "docStatus"){
        const { _id, docStatus} = req.body
        await PoliContributionSummary.updateOne(
          { _id: summaryId, "poliContributionSummaryDocuments._id": _id},
          { $set: { "poliContributionSummaryDocuments.$.docStatus" : docStatus } })
      }else if(details === "updateMeta"){
        const {_id} = req.body
        await PoliContributionSummary.updateOne(
          { _id: summaryId, "poliContributionSummaryDocuments._id": _id},
          { $set: { "poliContributionSummaryDocuments.$" : req.body } })
      } else if(politicalContSummary){
        await PoliContributionSummary.update({ _id: summaryId }, {$addToSet: {poliContributionSummaryDocuments: req.body.poliContributionSummaryDocuments}})
      }else {
        res.status(400).send({done:false,error:"incorrect query params"})
      }
      politicalContSummary = await PoliContributionSummary.findOne({ _id: summaryId  }).select("entityId poliContributionSummaryDocuments status")
      res.json(politicalContSummary)
    } else {
      const newConDetails = new PoliContributionSummary({
        entityId,
        entityName: firmName && firmName.firmName || "",
        year,
        quarter,
        poliContributionSummaryDocuments: req.body.poliContributionSummaryDocuments,
      })
      const contDetails = await newConDetails.save()
      res.json(contDetails)
    }

  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullSummaryDetailsDocuments = () => async (req, res) => {
  const { pullId, summaryId } = req.query
  try {
    const politicalContDetails = await PoliContributionSummary.findOne({ _id: summaryId })

    if(politicalContDetails) {
      await PoliContributionSummary.update({ _id: summaryId }, { $pull: { poliContributionSummaryDocuments: { _id: pullId } } })
      const complaintDetails = await PoliContributionSummary.findOne({  _id: summaryId })
      res.json(complaintDetails)
    }else {
      res.status(400).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}


export default generateControllers(PoliContributionSummary, {
  putSummaryDetails: putSummaryDetails(),
  putSummaryDetailsStatus: putSummaryDetailsStatus(),
  postSummaryDetailsDocuments: postSummaryDetailsDocuments(),
  pullSummaryDetailsDocuments: pullSummaryDetailsDocuments(),
  getSummaryDetails: getSummaryDetails(),
})
