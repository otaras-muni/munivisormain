import express from "express"
import poliContributionsController from "./poliContributionSummary.controller"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const poliContributionSummaryRouter = express.Router()

poliContributionSummaryRouter.param("id", poliContributionsController.findByParam)

poliContributionSummaryRouter.route("/summary")
  .get(requireAuth, poliContributionsController.getSummaryDetails)
  .put(requireAuth,poliContributionsController.putSummaryDetails)
  .post(requireAuth,poliContributionsController.postSummaryDetailsDocuments)

poliContributionSummaryRouter.route("/status")
  .put(requireAuth,poliContributionsController.putSummaryDetailsStatus)

poliContributionSummaryRouter.route("/documents")
  .post(requireAuth,poliContributionsController.postSummaryDetailsDocuments)
  .delete(requireAuth,poliContributionsController.pullSummaryDetailsDocuments)
