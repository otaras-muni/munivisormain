import Joi from "joi-browser"

const designateFirmSchema = Joi.object().keys({
  userId: Joi.string().allow("").required(),
  name: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().required(),
  userPrimaryPhone: Joi.string().required(),
  userAddress: Joi.string().required(),
  userContactType: Joi.string().required(),
  _id: Joi.string().required().optional(),
})

export const PeopleDesignateValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, designateFirmSchema, { abortEarly: false, stripUnknown:false })

const qualifiedSchema = Joi.object().keys({
  qualification: Joi.string().required(),
  userId: Joi.string().allow("").required(),
  name: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().required(),
  userPrimaryPhone: Joi.string().required(),
  profFeePaidOn: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  series50PassDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  series50ValidityEndDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PeopleQualifiedValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, qualifiedSchema, { abortEarly: false, stripUnknown:false })

const businessActivitiesSchema = Joi.object().keys({
  activity: Joi.string().required(),
  subActivity: Joi.string().required(),
  notes: Joi.string().allow("").optional(),
  _id: Joi.string().required().optional(),
})

export const BusinessActivitiesValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, businessActivitiesSchema, { abortEarly: false, stripUnknown:false })

const firmRegisterSchema = Joi.object().keys({
  registeringBody: Joi.string().required(),
  regFeePaidOn: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  feeAmount: Joi.number().required(),
  regValidTill: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: Joi.string().required().optional(),
})

export const FirmRegisterValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, firmRegisterSchema, { abortEarly: false, stripUnknown:false })

const firmInfoSchema = Joi.array().items({
  field: Joi.string().required(),
  notes: Joi.string().required(),
  information: Joi.string().allow("").optional(),
  isField: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

export const GeneralFirmInfoValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, firmInfoSchema, { abortEarly: false, stripUnknown:false })

export const errorHandling = (req, res, next) => {
  const { type } = req.query
  let errors
  if(type === "gaDesignateFirmContacts") {
    errors = PeopleDesignateValidate(req.body)
  }
  if(type === "gaQualifiedAssPersons") {
    errors = PeopleQualifiedValidate(req.body)
  }
  if(type === "gaFirmRegInformation") {
    errors = FirmRegisterValidate(req.body)
  }
  if(type === "businessActivities") {
    errors = BusinessActivitiesValidate(req.body)
  }
  if(type === "gaGeneralFirmInfo") {
    errors = GeneralFirmInfoValidate(req.body)
  }
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
