import express from "express"
import generalAdminController from "./generaladmin.controller"
import { errorHandling } from "./validation"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const generalAdminRouter = express.Router()

generalAdminRouter.param("id", generalAdminController.findByParam)

generalAdminRouter.route("/")
  .get(requireAuth,generalAdminController.getAll)
  .post(requireAuth, generalAdminController.createOne)

generalAdminRouter.route("/contactsorpersons")
  .get(requireAuth, generalAdminController.getGeneralAdminDetails)
  .put(requireAuth, errorHandling, generalAdminController.putGenAdminContactsOrPersons)

generalAdminRouter.route("/contactsorpersons/:pullId")
  .delete(requireAuth, generalAdminController.pullGenAdminContactsOrPersons)
  .get(requireAuth, generalAdminController.getGenAdminContactsOrPersons)

generalAdminRouter.route("/documents")
  .put(requireAuth, generalAdminController.putGenAdminDocuments)
  .delete(requireAuth, generalAdminController.pullGenAdminDocuments)

generalAdminRouter.route("/auditlogs")
  .post(requireAuth, generalAdminController.putAuditLogs)
