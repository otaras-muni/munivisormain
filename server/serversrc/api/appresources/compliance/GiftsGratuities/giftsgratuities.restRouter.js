import express from "express"
import giftsGratuitiesController from "./giftsgratuities.controller"
import { errorHandling } from "./validation"
import {requireAuth} from "../../../../authorization/authsessionmanagement/auth.middleware"

export const giftsGratuitiesRouter = express.Router()

giftsGratuitiesRouter.param("id", giftsGratuitiesController.findByParam)

giftsGratuitiesRouter.route("/")
  .get(requireAuth, giftsGratuitiesController.getAllGifts)
  .post(requireAuth, giftsGratuitiesController.createOne)

giftsGratuitiesRouter.route("/statedetails")
  .get(requireAuth, giftsGratuitiesController.getGiftsGratuitiesStateDetails)
  .post(requireAuth, giftsGratuitiesController.postCheckGiftsGratuities)
  .put(requireAuth, giftsGratuitiesController.putGiftsGratuitiesStateDetails)

giftsGratuitiesRouter.route("/details/:giftsId")
  .put(requireAuth, errorHandling, giftsGratuitiesController.putGiftsGratuitiesDetails)
  .delete(requireAuth, giftsGratuitiesController.pullGiftsGratuitiesDetails)

giftsGratuitiesRouter.route("/preapproval")
  .get(requireAuth, giftsGratuitiesController.getAllPreApproval)

giftsGratuitiesRouter.route("/documents")
  .put(requireAuth, giftsGratuitiesController.putGiftsDocuments)

giftsGratuitiesRouter.route("/auditlogs")
  .post(requireAuth, giftsGratuitiesController.putAuditLogs)

giftsGratuitiesRouter.route("/giftdetails")
  .get(requireAuth, giftsGratuitiesController.getUserGiftsDetails)

giftsGratuitiesRouter.route("/getgiftdetail")
  .get(requireAuth, giftsGratuitiesController.getGiftDetail)

giftsGratuitiesRouter.route("/getAllDisclosure")
  .get(requireAuth, giftsGratuitiesController.getAllDisclosure)
