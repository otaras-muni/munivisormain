import moment from "moment"
import {generateControllers} from "../../../modules/query"
import {GiftsAndGratuities} from "./giftsgratuitiesdetails.model"
import {Entity} from "../../entity/entity.model"
import {EntityUser} from "../../entityUser/entityUser.model"
import {ControlsActions} from "../../cac/controls_actions.model"
import {Tasks} from "../../taskmanagement/tasks.model"
import {updateEligibleIdsForLoggedInUserFirm} from "../../../entitlementhelpers"
import {elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Tasks, _id: id, esType: "mvtasks"})
const {ObjectID} = require("mongodb")

const currentYear = parseInt(moment(new Date()).utc().format("YYYY"), 10)
const currentQuarter = moment(new Date()).utc().quarter()

const getGiftsGratuitiesStateDetails = () => async (req, res) => {
  try {
    const {year, quarter, disclosureFor} = req.query
    const giftsDetails = await GiftsAndGratuities.findOne({ year, quarter, userOrEntityId: disclosureFor })

    if(giftsDetails) {
      res.json(giftsDetails)
    }else {
      res.json({status: true, message: "not found"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putGiftsGratuitiesStateDetails = () => async (req, res) => {
  try {
    const {year, quarter, disclosureFor} = req.body
    if(!year || !quarter || !disclosureFor){
      res.status(422).send({done:false, error: "Params missing year or quarter or disclosureFor"})
    }
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const user = await EntityUser.findOne({_id: disclosureFor}).select("userFirstName userLastName userFirmName entityId")
    // const giftsDetails = await GiftsAndGratuities.findOne({ year, userOrEntityId: disclosureFor }).select("finAdvisorEntityId finAdvisorEntityName userOrEntityId year quarter userFirstName userLastName userFirmName status")

    const newGiftsAndGratuities = new GiftsAndGratuities({
      finAdvisorEntityId: req.user.entityId,
      finAdvisorEntityName: (entity && entity.firmName) || "",
      year,
      quarter,
      entityOrUserFlag: false,
      userOrEntityId: disclosureFor,
      userFirstName: user.userFirstName || "",
      userLastName: user.userLastName || "",
      entityName: user.userFirmName || "",
      status: "Pending", // flag ? "Pre-approval" : "Pending",
      ...req.body,
      submitter: `${req.user.userFirstName} ${req.user.userLastName}`,
      preApprovalSought: year > currentYear ? "Yes" : "No",
      cac: true
    })

    const giftsDetails = newGiftsAndGratuities.save()
    res.json(giftsDetails)
  } catch(error) {
    console.log("================", error)
    res.status(422).send({done:false, error})
  }
}

const getAllGifts = () => async (req, res) => {
  try {
    const {type} = req.query
    const gifts = []
    if(type === "supervisor"){
      const gift = await GiftsAndGratuities.find({finAdvisorEntityId: req.user.entityId }).select("userOrEntityId userFirstName userLastName userFirmName status year quarter cac noDisclosure")
      gift.forEach(g => {
        const flag = (parseInt(g.year,10) > currentYear) || ((parseInt(g.quarter,10) > currentQuarter) && (parseInt(g.year,10) === currentYear))
        if(!g.cac || (g.cac && !flag) || (g.cac && g.status !== "Pending" && !flag)) {
          gifts.push(g)
        }
      })

    }else {
      const gift = await GiftsAndGratuities.find({ userOrEntityId: req.user._id }).select("userOrEntityId userFirstName userLastName userFirmName status year quarter cac noDisclosure")
      gift.forEach(g => {
        const flag = (parseInt(g.year,10) > currentYear) || ((parseInt(g.quarter,10) > currentQuarter) && (parseInt(g.year,10) === currentYear))
        if(!g.cac || (g.cac && !flag) || (g.cac && g.status !== "Pending" && !flag)) {
          gifts.push(g)
        }
      })
    }
    res.json(gifts)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getAllPreApproval = () => async (req, res) => {
  try {
    const {type} = req.query
    let gift = []
    if(type === "supervisor"){
      gift = await GiftsAndGratuities.find({finAdvisorEntityId: req.user.entityId, status: "Pre-approval"  }).select("userOrEntityId userFirstName userLastName userFirmName status year quarter")
    }else {
      gift = await GiftsAndGratuities.find({userOrEntityId: req.user._id, status: "Pre-approval"  }).select("userOrEntityId userFirstName userLastName userFirmName status year quarter")
    }
    res.json(gift)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const postCheckGiftsGratuities = () => async (req, res) => {
  try {
    const {year, quarter, disclosureFor, fetchId} = req.body
    const {discloseType} = req.query

    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const user = await EntityUser.findById({_id: disclosureFor}).select("userFirstName userLastName userFirmName entityId")
    // const disclosureForId = disclosureFor === "Firm" ? req.user.entityId : disclosureFor === "Self" ? req.user._id : ""  // eslint-disable-line
    let giftsDetails = await GiftsAndGratuities.findOne({ _id: fetchId})
    const flag = (parseInt(year,10) > currentYear) || ((parseInt(quarter,10) > currentQuarter) && (parseInt(year,10) === currentYear))

    if(giftsDetails && req.body.status === "Complete"){
      await ControlsActions.findOneAndUpdate({_id: giftsDetails.taskRefId }, {status: "complete"}, {
        new: true,
        upsert: true
      })
      const task = await Tasks.findOne({"taskDetails.taskNotes": giftsDetails.taskRefId})
      if(task){
        await Tasks.update({
          "taskDetails.taskNotes": giftsDetails.taskRefId
        }, {
          $set: {
            "taskDetails.taskStatus": "Closed"
          }
        })
        await Promise.all([
          updateEligibleIdsForLoggedInUserFirm(req.user),
          returnFromES(req.user.tenantId, task._id)
        ])
      }
    }
    if(giftsDetails) {
      // req.body.cac = false
      if(discloseType){
        req.body.discloseType = discloseType
      }
      await GiftsAndGratuities.updateOne({_id: ObjectID(fetchId)}, req.body)
      giftsDetails = await GiftsAndGratuities.findOne({  _id: fetchId })
      res.json(giftsDetails)
    }else {
    // const entityOrUserFlag = disclosureFor === "Firm"
      const newGiftsAndGratuities = new GiftsAndGratuities({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        submitter: `${req.user.userFirstName} ${req.user.userLastName}`,
        year, quarter,
        entityOrUserFlag: false,
        preApprovalSought: year > currentYear ? "Yes" : "No",
        userOrEntityId: disclosureFor,
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        entityName: user.userFirmName,
        ...req.body,
        discloseType,
        status: (req.body && req.body.status) // flag ? "Pre-approval" : "Pending",
      })
      newGiftsAndGratuities.save(async (err, returned) => {
        if(err){
          res.status(422).send(err)
        }else {
          res.json(returned)
        }
      })
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putGiftsGratuitiesDetails = () => async (req, res) => {
  try {
    const { giftsId } = req.params
    const { type, year, quarter, discloseType  } = req.query
    const { _id } = req.body
    const types = ["giftsToRecipients", "details", "notes"]
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    let giftsDetails = await GiftsAndGratuities.findOne({_id: giftsId})

    if(giftsDetails && types.indexOf(type) !== -1) {

      if(type === "giftsToRecipients") {

        if (_id) {

          const dndString1 = `${type}._id`
          const dndString2 = `${type}.$`
          const query1 = {_id: ObjectID(giftsId), [dndString1]: ObjectID(_id)}
          const query2 = {$set: {[dndString2]: req.body}}
          if(discloseType){
            query2.discloseType = discloseType
          }
          await GiftsAndGratuities.updateOne(query1, query2)

        } else {
          const update = {$addToSet: {[type]: req.body}}
          if(discloseType){
            update.discloseType = discloseType
          }
          await GiftsAndGratuities.updateOne({_id: ObjectID(giftsId)}, update)

        }
      }else if(type === "details") {

        if(req.body && req.body.notes && Object.keys(req.body).length) {
          await GiftsAndGratuities.updateOne({_id: ObjectID(giftsId)}, {$addToSet: {notes: req.body.notes}})
          delete req.body.notes
        }
        // req.body.cac = false
        if(discloseType){
          req.body.discloseType = discloseType
        }
        await GiftsAndGratuities.updateOne({_id: ObjectID(giftsId)}, req.body)
      }

      giftsDetails = await GiftsAndGratuities.findOne({_id: giftsId})
      res.json(giftsDetails)

    }else {
      const user = await EntityUser.findById({_id: giftsId}).select("userFirstName userLastName userFirmName entityId")
      const flag = (parseInt(year,10) > currentYear) || ((parseInt(quarter,10) > currentQuarter) && (parseInt(year,10) === currentYear))

      const newGiftsAndGratuities = new GiftsAndGratuities({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        submitter: `${req.user.userFirstName} ${req.user.userLastName}`,
        year, quarter,
        entityOrUserFlag: false,
        preApprovalSought: year > currentYear ? "Yes" : "No",
        userOrEntityId: giftsId,
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        entityName: user.userFirmName,
        [type]: [req.body],
        discloseType,
        status: "Pending", // flag ? "Pre-approval" : "Pending",
      })

      newGiftsAndGratuities.save(async (err, returned) => {
        if(err){
          res.status(422).send(err)
        }else {
          res.json(returned)
        }
      })

    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullGiftsGratuitiesDetails = () => async (req, res) => {
  const { giftsId } = req.params
  const { pullId, type } = req.query
  try {
    const giftsDetails = await GiftsAndGratuities.findOne({ _id: giftsId })
    const types = ["giftsToRecipients", "giftDisclosureDocuments"]

    if(giftsDetails || types.indexOf(type) !== -1 || pullId) {
      await GiftsAndGratuities.updateOne({ _id: ObjectID(giftsId) }, { $pull: { [type]: { _id:  ObjectID(pullId) } }})
      const giftsDetails = await GiftsAndGratuities.findOne({  _id: giftsId }).select(`userOrEntityId entityOrUserFlag ${type}`)
      res.json(giftsDetails)
    }else {
      res.status(400).send({done:false,error:"invalid giftsId or query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putGiftsDocuments = () => async (req, res) => {
  const resRequested = [
    {resource:"GiftsAndGratuities",access:2},
  ]
  const { giftsId, details } = req.query

  try {
    if(details === "docStatus"){
      const { _id, docStatus} = req.body
      await GiftsAndGratuities.updateOne(
        { _id: giftsId, "giftDisclosureDocuments._id": ObjectID(_id)},
        { $set: { "giftDisclosureDocuments.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const {_id} = req.body
      await GiftsAndGratuities.updateOne(
        { _id: giftsId, "giftDisclosureDocuments._id": ObjectID(_id)},
        { $set: { "giftDisclosureDocuments.$" : req.body } })
    }else {
      await GiftsAndGratuities.updateOne({ _id: giftsId }, {$addToSet: {giftDisclosureDocuments: req.body.giftDisclosureDocuments}, cac: false})
    }
    const giftsDetails = await GiftsAndGratuities.findOne({  _id: giftsId }).select("finAdvisorEntityId giftDisclosureDocuments")
    res.json(giftsDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"GiftsAndGratuities",access:2},
  ]
  const { giftsId } = req.query
  try {
    await GiftsAndGratuities.update({ _id: giftsId}, {$addToSet: { auditLogs: req.body} })
    const giftsDetails = await GiftsAndGratuities.findOne({  _id: giftsId  }).select("auditLogs")
    res.json(giftsDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getUserGiftsDetails = () => async (req, res) => {
  try {
    const {id, supervisor} = req.query
    let giftsDetails = []
    const select = ("year quarter userFirstName userLastName status notes noDisclosure preApprovalSought submitter userFirstName userLastName cac discloseType controlEffectiveDate giftsAffirmCompletionOfG20ForPeriod affirmationNotes controlName actionBy")
    if(supervisor) {
      giftsDetails = await GiftsAndGratuities.find({finAdvisorEntityId: req.user.entityId}).select(select)
    } else {
      giftsDetails = await GiftsAndGratuities.find({ userOrEntityId: id }).select(select)
    }

    if(giftsDetails) {
      res.json(giftsDetails)
    }else {
      res.json({status: true, message: "not found"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getGiftDetail = () => async (req, res) => {
  try {
    const {id} = req.query
    const giftsDetails = await GiftsAndGratuities.findOne({ _id : id })

    if(giftsDetails) {
      res.json(giftsDetails)
    }else {
      res.json({status: true, message: "not found"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getAllDisclosure = () => async (req, res) => {
  try {
    const {sortBy} = req.query
    let giftsDetails = []
    if(sortBy) {
      const startDate = moment(new Date())
      giftsDetails = await GiftsAndGratuities.find({createdAt: {$gte: moment(sortBy) , $lte: startDate }}).select("giftsToRecipients preApprovalDate status")
    }
    if(giftsDetails) {
      giftsDetails = giftsDetails.filter(g => g.status === "Complete")
      res.json(giftsDetails)
    }else {
      res.json({status: true, message: "not found"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export default generateControllers(GiftsAndGratuities, {
  postCheckGiftsGratuities: postCheckGiftsGratuities(),
  putGiftsGratuitiesStateDetails: putGiftsGratuitiesStateDetails(),
  getAllPreApproval: getAllPreApproval(),
  getAllGifts: getAllGifts(),
  getGiftsGratuitiesStateDetails: getGiftsGratuitiesStateDetails(),
  putGiftsGratuitiesDetails: putGiftsGratuitiesDetails(),
  pullGiftsGratuitiesDetails: pullGiftsGratuitiesDetails(),
  putGiftsDocuments: putGiftsDocuments(),
  putAuditLogs: putAuditLogs(),
  getUserGiftsDetails: getUserGiftsDetails(),
  getGiftDetail: getGiftDetail(),
  getAllDisclosure: getAllDisclosure(),
})
