import Joi from "joi-browser"

const busCondActivitiesSchema = Joi.object().keys({
  /* userId: Joi.string().allow("").required(),
  userFirstName:Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userPrimaryEmailId: Joi.string().email().allow("").optional(),
  userPrimaryPhone: Joi.string().allow("").optional(), */
  violationType: Joi.string().required(),
  violataionDate: Joi.date().required().optional(),
  violationNotes: Joi.string().allow("").optional(),
  recordDate: Joi.date().allow([null,""]).optional(),
  closeDate: Joi.date().allow([null,""]).optional(),
  assPersonName: Joi.string().required(),
  docAWSFileLocation: Joi.string().allow("").optional(),
  docFileName: Joi.string().allow("").optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const BusinessConductValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, busCondActivitiesSchema, { abortEarly: false, stripUnknown:false })

export const errorHandling = (req, res, next) => {
  let errors = BusinessConductValidate(req.body)
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  next()
}
