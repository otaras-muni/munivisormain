import express from "express"
import businessConductController from "./businessconduct.controller"
import { errorHandling } from "./validation"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const businessConductRouter = express.Router()

businessConductRouter.param("id", businessConductController.findByParam)

businessConductRouter.route("/")
  .get(requireAuth,businessConductController.getAll)
  .post(requireAuth, businessConductController.createOne)

businessConductRouter.route("/bcaDetails")
  .get(requireAuth, businessConductController.getBusinessConductDetails)
  .put(requireAuth, errorHandling, businessConductController.putBusinessConduct)

businessConductRouter.route("/bcaDetails/:bcaId")
  .delete(requireAuth, businessConductController.pullBusinessConduct)

businessConductRouter.route("/auditlogs")
  .post(requireAuth, businessConductController.putAuditLogs)

businessConductRouter.route("/documents")
  .delete(requireAuth, businessConductController.pullBusinessDocuments)
