import express from "express"
import clientEducationG10Controller from "./clienteducationg-10.controller"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const clientEducationG10Router = express.Router()

clientEducationG10Router.route("/admin")
  .put(requireAuth, clientEducationG10Controller.putClientG10AdminDetails)
  .get(requireAuth, clientEducationG10Controller.getClientG10AdminDetails)
  .delete(requireAuth, clientEducationG10Controller.pullClientG10AdminDetails)

clientEducationG10Router.route("/recordkeeping")
  .put(requireAuth, clientEducationG10Controller.putClientG10RecordKeeping)
  .get(requireAuth, clientEducationG10Controller.getClientG10RecordKeeping)
