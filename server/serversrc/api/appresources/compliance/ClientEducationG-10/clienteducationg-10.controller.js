import {generateControllers} from "./../../../modules/query"
import {ClientEducationG10} from "./clienteducationg-10.model"
import {Entity} from "../../entity/entity.model"
import {GeneralAdmin} from "../GenAdminActivities/generaladmin.model"
const {ObjectID} = require("mongodb")

const putClientG10AdminDetails = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplaintDetails",access:2},
    {resource:"Entity",access:2},
  ]
  const {type} = req.query
  try {
    let adminDetail = await ClientEducationG10.findOne({finAdvisorEntityId: req.user.entityId})
    const details = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")

    if(type === "administrationList" || type === "disclosureMessage") {
      const value = type === "disclosureMessage" ? req.body.body : req.body
      if (adminDetail) {
        await ClientEducationG10.updateOne(
          {finAdvisorEntityId: ObjectID(req.user.entityId)},
          {$set: {[type]: value}}
        )
      } else {
        details.push(value)
        const newGenAdmin = new ClientEducationG10({
          finAdvisorEntityId: req.user.entityId,
          finAdvisorEntityName: (entity && entity.firmName) || "",
          [type]: (type === "administrationList" || type === "disclosureMessage") ? value : details
        })
        await newGenAdmin.save()
      }

      adminDetail = await ClientEducationG10.findOne({finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId disclosureMessage ${type}`)
      res.json(adminDetail)

    }else {
      res.status(404).send({done:false,error:"pass query params"})
    }

  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getClientG10AdminDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "ComplaintDetails", access: 1},
  ]
  const {type} = req.query
  try {
    let select = ""
    if(type === "documents"){
      select = "finAdvisorEntityId complaintDocuments"
    }else {
      select = "finAdvisorEntityId administrationList recordKeeping disclosureMessage"
    }
    const adminDetail = await ClientEducationG10.findOne({finAdvisorEntityId: req.user.entityId}).select(select)
    res.json(adminDetail)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const pullClientG10AdminDetails = () => async (req, res) => {
  const {type, _id} = req.query
  const resRequested = [
    {resource:"GeneralAdmin",access:2},
  ]
  try {
    if(type === "administrationList") {
      await ClientEducationG10.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { [type]: { _id } }})
      const adminDetail = await ClientEducationG10.findOne({ finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(adminDetail)
    }else {
      res.status(404).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putClientG10RecordKeeping = () => async (req, res) => {
  const resRequested = [
    {resource:"ComplaintDetails",access:2},
    {resource:"Entity",access:2},
  ]
  const { type, _id } = req.query
  try {
    let adminDetail = await ClientEducationG10.findOne({finAdvisorEntityId: req.user.entityId})
    const details = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")

    console.log({adminDetail, type})
    if(type === "recordKeeping") {
      if (adminDetail) {
        if(_id && type ){
          const query1 = `${type}._id`
          const query2 = {
            [`${type}.$`]: req.body
          }
          await ClientEducationG10.updateOne({finAdvisorEntityId: ObjectID(req.user.entityId), [query1]: ObjectID(_id) }, {$set: query2})
        } else {
          await ClientEducationG10.updateOne(
            {finAdvisorEntityId: ObjectID(req.user.entityId)},
            {$addToSet: {[type]: req.body}}
          )
        }
      } else {
        details.push(req.body)
        const newGenAdmin = new ClientEducationG10({
          finAdvisorEntityId: req.user.entityId,
          finAdvisorEntityName: (entity && entity.firmName) || "",
          [type]: req.body
        })
        await newGenAdmin.save()
      }

      adminDetail = await ClientEducationG10.findOne({finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(adminDetail)

    }else {
      res.status(404).send({done:false,error:"pass query params"})
    }

  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getClientG10RecordKeeping = () => async (req, res) => {
  const resRequested = [
    {resource: "ComplaintDetails", access: 1},
  ]
  const {type, id} = req.query
  try {
    let select = ""
    if(type === "documents"){
      select = "finAdvisorEntityId complaintDocuments"
    }else {
      select = "finAdvisorEntityId recordKeeping"
    }
    let adminDetail = []
    adminDetail = await ClientEducationG10.findOne({finAdvisorEntityId: req.user.entityId}).select(select)
    if(id){
      adminDetail = adminDetail.recordKeeping.find(a => a.fileAWSId === id)
    }
    res.json(adminDetail)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

export default generateControllers(ClientEducationG10, {
  putClientG10AdminDetails: putClientG10AdminDetails(),
  getClientG10AdminDetails: getClientG10AdminDetails(),
  pullClientG10AdminDetails: pullClientG10AdminDetails(),
  putClientG10RecordKeeping: putClientG10RecordKeeping(),
  getClientG10RecordKeeping: getClientG10RecordKeeping()
})
