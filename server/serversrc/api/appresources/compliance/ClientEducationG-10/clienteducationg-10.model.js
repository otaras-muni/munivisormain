import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose


const clientEducationG10 = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  disclosureMessage:String,
  administrationList:[{
    field: String,
    information: String,
    isField: Boolean,
  }],
  recordKeeping:[{
    entityName: String,
    entityId: String,
    fileName: String,
    recipientEmail: String,
    fileAWSId: String,
    recordKeepingType: String,
    deliveryAttemptDate: Date,
    deliveryStatus: String,
  }],
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

export const ClientEducationG10 = mongoose.model("clientEducationG10", clientEducationG10)
