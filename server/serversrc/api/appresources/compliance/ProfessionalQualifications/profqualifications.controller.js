import {generateControllers} from "./../../../modules/query"
import {ProfQualifications} from "./profqualifications.model"
import {Entity} from "../../entity/entity.model"
import {GeneralAdmin} from "../GenAdminActivities/generaladmin.model"
const {ObjectID} = require("mongodb")

const getProfQualificationsDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "ProfQualifications", access: 1},
  ]
  const {type} = req.query
  try {
    let select = ""
    if(type === "documents"){
      select = "finAdvisorEntityId pqDocuments"
    }else {
      select = "finAdvisorEntityId pqQualifiedPersons pqTrainingPrograms auditLogs createdAt pqTrainingDetail"
    }
    const pqDetails = await ProfQualifications.findOne({finAdvisorEntityId: req.user.entityId}).select(select)
    res.json(pqDetails)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const putProfQualificationsPersonsOrTraining = () => async (req, res) => {
  const resRequested = [
    {resource:"ProfQualifications",access:2},
    {resource:"Entity",access:2},
  ]
  const {type} = req.query
  try {
    let pqDetails = await ProfQualifications.findOne({finAdvisorEntityId: req.user.entityId})
    const details = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const {_id } = req.body

    if(type === "pqQualifiedPersons" || type === "pqTrainingPrograms" || type === "pqTrainingDetail") {
      if (pqDetails) {
        if (_id) {
          const dndString1 = `${type}._id`
          const dndString2 = `${type}.$`
          const query1 = {finAdvisorEntityId: ObjectID(req.user.entityId), [dndString1]: ObjectID(_id)}
          const query2 = {$set: {[dndString2]: req.body}}
          await ProfQualifications.updateOne(query1, query2)
        } else {
          await ProfQualifications.updateOne(
            {finAdvisorEntityId: ObjectID(req.user.entityId)},
            {$addToSet: {[type]: req.body}}
          )
        }
      } else {
        details.push(req.body)
        const newGenAdmin = new ProfQualifications({
          finAdvisorEntityId: req.user.entityId,
          finAdvisorEntityName: (entity && entity.firmName) || "",
          [type]: details
        })
        await newGenAdmin.save()
      }

      pqDetails = await ProfQualifications.findOne({finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(pqDetails)

    }else {
      res.status(404).send({done:false,error:"pass query params"})
    }

  } catch(error) {
    console.log("======================", error)
    res.status(422).send({done:false, error})
  }
}

const pullProfQualificationsPersonsOrTraining = () => async (req, res) => {
  const { pullId } = req.params
  const {type} = req.query
  const resRequested = [
    {resource:"ProfQualifications",access:2},
  ]
  try {
    if(type === "pqQualifiedPersons" || type === "pqTrainingPrograms" || type === "pqTrainingDetail") {
      await ProfQualifications.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { [type]: { _id: pullId } }})
      const pqDetails = await ProfQualifications.findOne({ finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(pqDetails)
    }else {
      res.status(404).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putProfQualificationsDocuments = () => async (req, res) => {
  const {details} = req.query
  const resRequested = [
    {resource:"ProfQualifications",access:2},
  ]
  try {
    let pqDetails = await ProfQualifications.findOne({finAdvisorEntityId: req.user.entityId})
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")

    if(details === "docStatus"){
      const { _id, docStatus} = req.body
      await ProfQualifications.updateOne(
        { finAdvisorEntityId: req.user.entityId, "pqDocuments._id": ObjectID(_id)},
        { $set: { "pqDocuments.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const {_id } = req.body
      await ProfQualifications.updateOne(
        { finAdvisorEntityId: req.user.entityId, "pqDocuments._id": ObjectID(_id)},
        { $set: { "pqDocuments.$" : req.body } })
    }else if (pqDetails) {
      await ProfQualifications.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {pqDocuments: req.body.pqDocuments}})
    } else {
      const newGenAdmin = new ProfQualifications({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        pqDocuments: req.body.pqDocuments
      })
      await newGenAdmin.save()
    }
    pqDetails = await ProfQualifications.findOne({ finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId pqDocuments")
    res.json(pqDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export const pullDocuments = () => async (req, res) => {
  const { docId } = req.query
  try {
    if(docId) {

      await ProfQualifications.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { pqDocuments: { _id: docId } }})

      const document = await ProfQualifications.findOne({ finAdvisorEntityId: req.user.entityId}).select("pqDocuments")

      res.status(200).json(document)
    }else {

      res.status(422).send({done:false,error:"This Document doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"ProfQualifications",access:2},
  ]
  try {
    await ProfQualifications.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {auditLogs: req.body} })
    const pqDetails = await ProfQualifications.findOne({ finAdvisorEntityId: req.user.entityId}).select("auditLogs")
    res.json(pqDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getSeriesPassedAndValid = () => async (req, res) => {
  const resRequested = [
    {resource:"GeneralAdmin",access:1},
  ]
  try {
    const { userId } = req.params
    const genAdminDetails = await GeneralAdmin.findOne({ finAdvisorEntityId: req.user.entityId, "gaQualifiedAssPersons.userId": userId}).select("gaQualifiedAssPersons")
    const userDetails = (genAdminDetails && genAdminDetails.gaQualifiedAssPersons && genAdminDetails.gaQualifiedAssPersons.find(user => user.userId.toString() === userId.toString())) || {}
    res.json(userDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}


export default generateControllers(ProfQualifications, {
  getProfQualificationsDetails: getProfQualificationsDetails(),
  putProfQualificationsPersonsOrTraining: putProfQualificationsPersonsOrTraining(),
  pullProfQualificationsPersonsOrTraining: pullProfQualificationsPersonsOrTraining(),
  putProfQualificationsDocuments: putProfQualificationsDocuments(),
  pullDocuments: pullDocuments(),
  putAuditLogs: putAuditLogs(),
  getSeriesPassedAndValid: getSeriesPassedAndValid(),
})
