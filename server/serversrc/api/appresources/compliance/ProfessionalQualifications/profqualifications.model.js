import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation: {type: Schema.Types.ObjectId, ref: Docs},
  docFileName:String,
  docNote:String,
  docStatus:String,
  markedPublic: { publicFlag:Boolean, publicDate:Date},
  sentToEmma: { emmaSendFlag:Boolean, emmaSendDate:Date},
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const professionalQualifications = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  pqQualifiedPersons:[{
    qualification: String,
    userId:Schema.Types.ObjectId,
    userFirstName:String,
    userLastName:String,
    userPrimaryEmailId:String,
    userPrimaryPhone:String,
    // userOrgRole:String,
    profFeePaidOn:Date,
    userSeries50PassedOnDate:Date,
    userSeries50ValidEndDate:Date,
    createdDate: { type: Date, required: true, default: Date.now },
  }],

  pqTrainingPrograms:[{
    trgProgramName:String,
    trgOrganizationDate:Date,
    trgConductedBy:{
      userId:Schema.Types.ObjectId,
      userFirstName:String,
      userLastName:String
    },
    trgAttendees:[{
      userId:Schema.Types.ObjectId,
      userFirstName:String,
      userLastName:String
    }],
    trgNotes:String,
    createdDate: { type: Date, required: true, default: Date.now },
  }],
  pqTrainingDetail: [{
    topic: String,
    programName: String,
    trainingDate: Date,
    conductedOn: Date,
    conductedBy: String,
    AWSFileLocation: String,
    fileName: String,
    attendees: [{
      id: String,
      name: String,
      userEmailId: String,
      userEntityId: String,
      userFirmName: String,
    }],
    notes: String
  }],
  pqDocuments:[superVisorDocumentSchema],
  auditLogs:[auditLogs],
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

export const ProfQualifications = mongoose.model("professionalqualifications", professionalQualifications)
