import express from "express"
import poliContributionsController from "./policontributions.controller"
import { errorHandling, contributorErrorHandling } from "./validation"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const poliContributionsRouter = express.Router()

poliContributionsRouter.param("id", poliContributionsController.findByParam)

poliContributionsRouter.route("/")
  .get(requireAuth,poliContributionsController.getAllPoliticalDisclosure)
  .post(requireAuth, poliContributionsController.createOne)

poliContributionsRouter.route("/user")
  .post(requireAuth, contributorErrorHandling, poliContributionsController.addNewContributor)

poliContributionsRouter.route("/statedetails")
  .get(requireAuth, poliContributionsController.getPoliContributionDetailsById)
  .post(requireAuth, poliContributionsController.postCheckPoliContribution)
  .put(requireAuth, poliContributionsController.putPoliContribution)

poliContributionsRouter.route("/details/:contributeId")
  .get(requireAuth, poliContributionsController.getPoliContributionDetails)
  .put(requireAuth, errorHandling, poliContributionsController.putPoliContributionDetails)
  .post(requireAuth, poliContributionsController.postPoliContributionDetailsDocuments)
  .delete(requireAuth, poliContributionsController.pullPoliContributionDetails)

poliContributionsRouter.route("/preapproval")
  .get(requireAuth, poliContributionsController.getAllPreApproval)

poliContributionsRouter.route("/questionanswer/:contributeId")
  .put(requireAuth, poliContributionsController.putPoliContributionQuestionAnswes)

poliContributionsRouter.route("/documents")
  .get(requireAuth, poliContributionsController.getPoliContDocuments)
  .put(requireAuth, poliContributionsController.putPoliContDocuments)
  .delete(requireAuth, poliContributionsController.pullPoliContributionDocuments)

poliContributionsRouter.route("/auditlogs/:detailsId")
  .post(requireAuth, poliContributionsController.putPoliContDetailsAuditLogs)

poliContributionsRouter.route("/auditlogs")
  .post(requireAuth, poliContributionsController.putAuditLogs)
