import moment from "moment"
import {generateControllers} from "./../../../modules/query"
import {PoliticalContributions} from "./policontributions.model"
import {PoliContributionDetails} from "./poliContributionDetails.model"
import {Entity} from "../../entity/entity.model"
import {EntityUser} from "../../entityUser/entityUser.model"
import {ControlsActions} from "../../cac/controls_actions.model"
import {Tasks} from "../../taskmanagement/tasks.model"
import {updateEligibleIdsForLoggedInUserFirm} from "../../../entitlementhelpers"
import {elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Tasks, _id: id, esType: "mvtasks"})
const {ObjectID} = require("mongodb")

const currentYear = parseInt(moment(new Date()).utc().format("YYYY"), 10)
const currentQuarter = moment(new Date()).utc().quarter()

const getAllPoliticalDisclosure = () => async (req, res) => {
  try {
    const {type} = req.query
    let disclosures = []
    const select = "_id cac finAdvisorEntityId finAdvisorEntityName year quarter userOrEntityId status submitter preApprovalSought notes affirmationNotes G37Obligation controlEffectiveDate userFirstName userLastName discloseType controlName actionBy"
    if(type === "supervisor"){
      disclosures = await PoliContributionDetails.find({ finAdvisorEntityId: req.user.entityId }).select(select)
    }else {
      disclosures = await PoliContributionDetails.find({userOrEntityId: req.user._id }).select(select)
    }
    res.json(disclosures)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getPoliContributionDetails = () => async (req, res) => {
  try {
    const disclosureFor = req.params.contributeId
    const {year, quarter} = req.query
    const politicalContDetails = await PoliticalContributions.findOne({ finAdvisorEntityId: req.user.entityId, year, quarter })

    if(politicalContDetails) {
      const {_id} = politicalContDetails
      const contDetails = await PoliContributionDetails.findOne({contributeId: _id, userOrEntityId: disclosureFor})
        .populate({
          path: "contributeId",
          model: "politicalcontributions",
          select: "finAdvisorEntityId finAdvisorEntityName year quarter"
        })
      if(contDetails) {
        res.json(contDetails)
      }else {
        res.json({status: true, message: "not found"})
      }
    }else {
      res.json({status: true, message: "not found"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const postCheckPoliContribution = () => async (req, res) => {
  try {
    const {year, quarter, disclosureFor} = req.body
    const {type, discloseType} = req.query
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const user = await EntityUser.findById({_id: disclosureFor}).select("userFirstName userLastName userFirmName entityId")
    let politicalContDetails = await PoliticalContributions.findOne({ finAdvisorEntityId: req.user.entityId, year, quarter })
    const flag = (parseInt(year,10) > currentYear) || ((parseInt(quarter,10) > currentQuarter) && (parseInt(year,10) === currentYear))

    if(politicalContDetails && req.body.status === "Complete"){
      await ControlsActions.findOneAndUpdate({_id: politicalContDetails.taskRefId }, {status: "complete"}, {
        new: true,
        upsert: true
      })
      const task = await Tasks.findOne({"taskDetails.taskNotes": politicalContDetails.taskRefId})
      if(task){
        await Tasks.update({
          "taskDetails.taskNotes": politicalContDetails.taskRefId
        }, {
          $set: {
            "taskDetails.taskStatus": "Closed"
          }
        })
        await Promise.all([
          updateEligibleIdsForLoggedInUserFirm(req.user),
          returnFromES(req.user.tenantId, task._id)
        ])
      }
    }
    if(req.body._id){
      try {
        if(type === "notes" || (req.body && req.body.notes && Object.keys(req.body).length)) {
          if(req.body && req.body.notes && req.body.notes.note){
            await PoliContributionDetails.updateOne({_id: req.body._id}, {$addToSet: { notes: req.body.notes }, /* cac: false, */ noDisclosure: false})
          }
        }
        delete req.body.notes
        // req.body.cac = false
        req.body.noDisclosure = req.body.hasOwnProperty("noDisclosure") ? req.body.noDisclosure :   false
        if(discloseType){
          req.body.discloseType = discloseType
        }
        await PoliContributionDetails.updateOne({_id: req.body._id}, req.body)

        politicalContDetails = await PoliContributionDetails.findOne({ _id: req.body._id })
        res.json(politicalContDetails)
      } catch (error) {
        res.status(422).send({done:false, error})
      }
    } else {
      const newConDetails = new PoliContributionDetails({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        year, quarter,
        entityOrUserFlag: false,
        userOrEntityId: disclosureFor,
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        entityName: user.userFirmName,
        submitter: `${req.user.userFirstName} ${req.user.userLastName}`,
        // cac: false,
        noDisclosure: false,
        preApprovalSought: flag ? "Yes" : "No",
        discloseType,
        ...req.body,
        status: (req.body && req.body.status) || "Pending", // flag ? "Pre-approval" : "Pending",
      })
      await newConDetails.save()
      politicalContDetails = await PoliContributionDetails.findOne({ userOrEntityId: disclosureFor }) // eslint-disable-line
      res.json(politicalContDetails)
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putPoliContribution = () => async (req, res) => {
  try {
    const {year, quarter, disclosureFor} = req.body
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const user = await EntityUser.findById({_id: disclosureFor}).select("userFirstName userLastName userFirmName entityId")
    const flag = (parseInt(year,10) > currentYear) || ((parseInt(quarter,10) > currentQuarter) && (parseInt(year,10) === currentYear))

    const newConDetails = new PoliContributionDetails({
      entityOrUserFlag: false,
      finAdvisorEntityId: req.user.entityId,
      finAdvisorEntityName: (entity && entity.firmName) || "",
      year, quarter,
      userOrEntityId: disclosureFor,
      userFirstName: user.userFirstName,
      userLastName: user.userLastName,
      entityName: user.userFirmName,
      submitter: `${req.user.userFirstName} ${req.user.userLastName}`,
      preApprovalSought: flag ? "Yes" : "No",
      status: "Pending", // flag ? "Pre-approval" : "Pending",
      ...req.body,
      cac: true
    })
    const poliDetails = await newConDetails.save()
    res.json({done:true, poliDetails})

  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putPoliContributionDetails = () => async (req, res) => {
  try {
    // const { contributeId } = req.params
    const year = req.params.contributeId
    const { quarter, userOrEntityId, type, discloseType } = req.query
    const { _id } = req.body
    console.log("===============userOrEntityId==================", userOrEntityId)
    const user = await EntityUser.findOne({_id: userOrEntityId}).select("userFirstName userLastName userFirmName entityId")
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const flag = (parseInt(year,10) > currentYear) || ((parseInt(quarter,10) > currentQuarter) && (parseInt(year,10) === currentYear))

    // const politicalContDetails = await PoliticalContributions.findOne({_id: contributeId})
    console.log("===================================")
    console.log({
      finAdvisorEntityId: req.user.entityId,
      finAdvisorEntityName: (entity && entity.firmName) || "",
      year, quarter,
      user : req.user
    })
    console.log("===================================")

    if(req.query._id) {
      const types = ["contribMuniEntityByState", "paymentsToPartiesByState", "ballotContribByState", "reimbursements", "ballotApprovedOfferings"]
      if(types.indexOf(type) !== -1) {
        if (_id) {
          const dndString1 = `${type}._id`
          const dndString2 = `${type}.$`
          const query1 = {_id: req.query._id, userOrEntityId: ObjectID(userOrEntityId), [dndString1]: ObjectID(_id)}
          const query2 = {$set: {[dndString2]: req.body}}
          if(discloseType){
            query2.discloseType = discloseType
          }
          await PoliContributionDetails.updateOne(query1, query2)
        } else {
          const update = {$addToSet: {[type]: req.body}, /* cac: false, */ noDisclosure: false}
          if(discloseType){
            update.discloseType = discloseType
          }
          await PoliContributionDetails.updateOne(
            {_id: req.query._id, userOrEntityId: ObjectID(userOrEntityId)},
            update
          )
        }
        const contDetails = await PoliContributionDetails.findOne({_id: req.query._id}) /* .select(`userOrEntityId entityOrUserFlag contributeId ${type} status`) */
        res.json(contDetails)
      }else {
        res.status(400).send({done:false,error:"pass query params"})
      }
    }else{
      const newConDetails = new PoliContributionDetails({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        year, quarter,
        entityOrUserFlag: false,
        userOrEntityId,
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        entityName: user.userFirmName,
        submitter: `${req.user.userFirstName} ${req.user.userLastName}`,
        preApprovalSought: flag ? "Yes" : "No",
        status: "Pending",
        discloseType,
        [type]: [req.body],
      })
      const contDetails = await newConDetails.save()
      res.json(contDetails)
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullPoliContributionDetails = () => async (req, res) => {
  const detailsId = req.params.contributeId
  const { pullId, type } = req.query
  try {
    const politicalContDetails = await PoliContributionDetails.findOne({ _id: detailsId })
    const types = ["contribMuniEntityByState", "paymentsToPartiesByState", "ballotContribByState", "reimbursements", "ballotApprovedOfferings", "contributionsRelatedDocuments"]

    if(politicalContDetails || types.indexOf(type) !== -1) {
      await PoliContributionDetails.update({ _id: detailsId }, { $pull: { [type]: { _id: pullId } }})
      const complaintDetails = await PoliContributionDetails.findOne({  _id: detailsId })
      res.json(complaintDetails)
    }else {
      res.status(400).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const postPoliContributionDetailsDocuments = () => async (req, res) => {
  const { details} = req.query
  try {
    const detailsId = req.params.contributeId
    let politicalContDetails = await PoliContributionDetails.findOne({ _id: detailsId })

    if(details === "docStatus"){
      const { _id, docStatus} = req.body
      await PoliContributionDetails.updateOne(
        { _id: detailsId, "contributionsRelatedDocuments._id": ObjectID(_id)},
        { $set: { "contributionsRelatedDocuments.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const {_id} = req.body
      await PoliContributionDetails.updateOne(
        { _id: detailsId, "contributionsRelatedDocuments._id": ObjectID(_id)},
        { $set: { "contributionsRelatedDocuments.$" : req.body } })
    } else if(politicalContDetails){
      await PoliContributionDetails.update({ _id: detailsId }, {$addToSet: {contributionsRelatedDocuments: req.body.contributionsRelatedDocuments}, /* cac: false, */ noDisclosure: false})
    }else {
      res.status(400).send({done:false,error:"incorrect query params"})
    }
    politicalContDetails = await PoliContributionDetails.findOne({ _id: detailsId  }).select("finAdvisorEntityId contributionsRelatedDocuments status")
    res.json(politicalContDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putPoliContributionQuestionAnswes = () => async (req, res) => {
  try {
    const { contributeId } = req.params
    const { userOrEntityId } = req.query
    const user = await EntityUser.findById({_id: userOrEntityId}).select("userFirstName userLastName userFirmName entityId")
    const politicalContDetails = await PoliticalContributions.findOne({_id: contributeId})
    let contDetails = await PoliContributionDetails.findOne({contributeId, userOrEntityId})

    if(contDetails) {
      await PoliContributionDetails.updateOne({contributeId: ObjectID(contributeId), userOrEntityId: ObjectID(userOrEntityId)}, { $set: { ...req.body } })
    }else {
      // const entityOrUserFlag = politicalContDetails.disclosureFor === "Firm"
      const newContribution = new PoliContributionDetails({
        contributeId,
        entityOrUserFlag: false,
        userOrEntityId: /* entityOrUserFlag ? req.user.entityId : req.user._id */  ObjectID(userOrEntityId),  // eslint-disable-line
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        entityName: user.userFirmName,
        ...req.body,
      })
      await newContribution.save()
    }
    contDetails = await PoliContributionDetails.findOne({contributeId}).select("negativeAnswers contributeId positiveAnswers G37Obligation userOrEntityId entityOrUserFlag status")
    res.json(contDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putPoliContDetailsAuditLogs = () => async (req, res) => {
  try {
    const { detailsId } = req.params
    const contDetails = await PoliContributionDetails.findOne({_id: detailsId})
    if(contDetails) {
      await PoliContributionDetails.updateOne({_id: ObjectID(detailsId)}, {$addToSet: {auditLogs: req.body} })
      const politicalContDetails = await PoliContributionDetails.findOne({_id: detailsId}).select("userOrEntityId entityOrUserFlag contributeId auditLogs status")
      res.json(politicalContDetails)
    }else {
      res.status(400).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const addNewContributor = () => async (req, res) => {
  try {
    const newUser = new EntityUser({
      _id: ObjectID(),
      entityId: req.body.entityId,
      userFirmName: req.body.entityName,
      userFirstName: req.body.userFirstName,
      userMiddleName: req.body.userMiddleName,
      userLastName: req.body.userLastName,
      userEmails:[{
        emailId:req.body.userPrimaryEmailId,
        emailPrimary: true,
      }],
      userPhone: [{
        phoneNumber: req.body.userPrimaryPhone,
        extension: "",
        phonePrimary: true
      }],
      userFax: [{
        faxNumber: req.body.userFax,
        faxPrimary: true
      }],
      userAddresses:[req.body.userAddressConsolidated],
    })
    const user = await newUser.save()
    res.json(user)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getPoliContDocuments = () => async (req, res) => {
  try {
    const { year, quarter } = req.query
    if(year && quarter) {
      const politicalContDetails = await PoliticalContributions.findOne({ finAdvisorEntityId: req.user.entityId, year, quarter}).select("finAdvisorEntityId year quarter msrbSubmissions auditLogs")
      res.json(politicalContDetails)
    }else {
      res.status(400).send({done:false,error:"incorrect query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putPoliContDocuments = () => async (req, res) => {
  try {
    const { year, quarter, details } = req.query
    let politicalContDetails = await PoliticalContributions.findOne({ finAdvisorEntityId: req.user.entityId, year, quarter})
    if(details === "docStatus"){
      const {_id, docStatus } = req.body
      await PoliticalContributions.updateOne(
        { year, quarter,  "msrbSubmissions._id": ObjectID(_id)},
        { $set: { "msrbSubmissions.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const {_id} = req.body
      await PoliticalContributions.updateOne(
        { year, quarter, "msrbSubmissions._id": ObjectID(_id)},
        { $set: { "msrbSubmissions.$" : req.body } })
    }else if(year && quarter && politicalContDetails) {
      await PoliticalContributions.update({ year, quarter}, {$addToSet: {msrbSubmissions: req.body.msrbSubmissions}})
    }else {
      res.status(400).send({done:false,error:"contribute not exists or incorrect query params"})
    }
    politicalContDetails = await PoliticalContributions.findOne({ finAdvisorEntityId: req.user.entityId, year, quarter }).select("finAdvisorEntityId msrbSubmissions")
    res.json(politicalContDetails)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullPoliContributionDocuments = () => async (req, res) => {
  const { pullId, type, contributeId } = req.query
  try {
    const politicalContDetails = await PoliticalContributions.findOne({ _id: contributeId })
    const types = ["msrbSubmissions"]

    if(politicalContDetails || types.indexOf(type) !== -1) {
      await PoliticalContributions.update({ _id: contributeId }, { $pull: { [type]: { _id: pullId } }})
      const complaintDetails = await PoliticalContributions.findOne({  _id: contributeId }).select(`userOrEntityId entityOrUserFlag contributeId ${type} status`)
      res.json(complaintDetails)
    }else {
      res.status(400).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putAuditLogs = () => async (req, res) => {
  try {
    const { year, quarter } = req.query

    if(year && quarter ) {
      await PoliticalContributions.updateOne({ year, quarter}, {$addToSet: {auditLogs: req.body} })
      const politicalContDetails = await PoliticalContributions.findOne({ finAdvisorEntityId: req.user.entityId, year, quarter}).select("finAdvisorEntityId auditLogs")
      res.json(politicalContDetails)
    }else {
      res.status(400).send({done:false,error:"incorrect query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getAllPreApproval = () => async (req, res) => {
  try {
    const {type} = req.query
    const contriButers = await PoliticalContributions.find({ finAdvisorEntityId: req.user.entityId })
    const contriButersIds = contriButers.map(cont => cont._id) // eslint-disable-line
    let disclosures = []
    if(type === "supervisor"){
      disclosures = await PoliContributionDetails.find({contributeId: {$in: contriButersIds}, status: "Pre-approval" }).select("userOrEntityId contributeId userFirstName userLastName userFirmName status").populate({
        path: "contributeId",
        model: "politicalcontributions",
        select: "finAdvisorEntityId finAdvisorEntityName year quarter"
      })
    }else {
      disclosures = await PoliContributionDetails.find({userOrEntityId: req.user._id, status: "Pre-approval" }).select("userOrEntityId contributeId userFirstName userLastName userFirmName status").populate({
        path: "contributeId",
        model: "politicalcontributions",
        select: "finAdvisorEntityId finAdvisorEntityName year quarter"
      }) // eslint-disable-line
    }
    res.json(disclosures)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getPoliContributionDetailsById = () => async (req, res) => {
  try {
    let disclosures = []
    if(req.query) {
      disclosures = await PoliContributionDetails.find(req.query)
    }
    res.json(disclosures)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export default generateControllers(PoliticalContributions, {
  getAllPoliticalDisclosure: getAllPoliticalDisclosure(),
  getPoliContributionDetails: getPoliContributionDetails(),
  postCheckPoliContribution: postCheckPoliContribution(),
  getPoliContributionDetailsById: getPoliContributionDetailsById(),
  putPoliContribution: putPoliContribution(),
  putPoliContributionQuestionAnswes: putPoliContributionQuestionAnswes(),
  putPoliContributionDetails: putPoliContributionDetails(),
  postPoliContributionDetailsDocuments: postPoliContributionDetailsDocuments(),
  pullPoliContributionDetails: pullPoliContributionDetails(),
  addNewContributor: addNewContributor(),
  getPoliContDocuments: getPoliContDocuments(),
  putPoliContDocuments: putPoliContDocuments(),
  pullPoliContributionDocuments: pullPoliContributionDocuments(),
  putPoliContDetailsAuditLogs: putPoliContDetailsAuditLogs(),
  putAuditLogs: putAuditLogs(),
  getAllPreApproval: getAllPreApproval(),
})
