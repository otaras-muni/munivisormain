import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation: {type: Schema.Types.ObjectId, ref: Docs},
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docNote:String,
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const politicalContributionsByUsers = Schema({
  entityOrUserFlag: Boolean, // Entity Id or User Id
  userOrEntityId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  entityName:String,
  userHasPoliticalContribForQuarter:{type:Boolean,default:false}, // Default to False
  disclosureDetails: {
    disclosureSection1WithNegativeAnswers:[{
      sectionHeader:String, // Populate Other when adding new
      sectionItems:[{
        sectionItemDesc:String,
        sectionAffirm:Boolean
      }]
    }],
    disclosureSectionWithPositiveAnswers:[{
      sectionHeader:String, // Populate Other when adding new.
      sectionItems:[{
        sectionItemDesc:String,
        sectionAffirm:Boolean
      }]
    }],
    disclosureAffirmationNotes:String,
    disclosureG37Obligation:Boolean,

    contribMuniEntityByState:[{
      state:String,
      contributionReceipientDetails:{
        userId:Schema.Types.ObjectId,
        userFirstName:String,
        userLastName:String,
        userAddressConsolidated:String,
      },
      contributorCategory:String,
      contributionAmount:Number,
      contributionDate:Date,
      exemptionG37RuleExemption:String,
      updatedByUser:Schema.Types.ObjectId,
      updatedDate:Date,
      createdDate: { type: Date, required: true, default: Date.now },
    }],

    paymentsToPartiesByState:[{
      state:String,
      politicalPartyDetails:{
        partyName:String,
        partyAddress:String
      },
      paymentAmount:Number,
      paymentDate:Date,
      updatedByUser:Schema.Types.ObjectId,
      updatedDate:Date,
      createdDate: { type: Date, required: true, default: Date.now },
    }],

    ballotContribByState:[{
      state:String,
      ballotDetail:String,
      contributorCategory:String,
      contributionAmount:Number,
      contributionDate:Date,
      updatedByUser:Schema.Types.ObjectId,
      updatedDate:Date,
      createdDate: { type: Date, required: true, default: Date.now },
    }],

    reimbursements:[{
      state:String,
      ballotDetail:String,
      thirdPartyDetails:{
        userId:Schema.Types.ObjectId,
        userFirstName:String,
        userLastName:String,
        userAddressConsolidated:String
      },
      thirdPartyCategory:String,
      reimbursementAmt:Number,
      reimbursementDate:Date,
      updatedByUser:Schema.Types.ObjectId,
      updatedDate:Date,
      createdDate: { type: Date, required: true, default: Date.now },
    }],

    ballotApprovedOfferings:[{
      entityId:Schema.Types.ObjectId,
      processDescription:String, // Needs to be Project description from deal issue only
      reportablesaleDate:Date,
      updatedByUser:Schema.Types.ObjectId,
      updatedDate:Date,
      createdDate: { type: Date, required: true, default: Date.now },
    }],
    contributionsRelatedDocuments:[superVisorDocumentSchema],
  }
})

const politicalContributionsOverall = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  year: Number, // Store this as an Integer
  quarter: Number, // Store 1,2,,4
  G37Generated: Boolean,
  G37GeneratedDate:Boolean,
  // users:[politicalContributionsByUsers],
  msrbSubmissions:[superVisorDocumentSchema],
  auditLogs:[auditLogs],
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

export const PoliticalContributions = mongoose.model("politicalcontributions", politicalContributionsOverall)
