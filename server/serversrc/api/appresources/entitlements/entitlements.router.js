import express from "express"
import {getConsolidatedAccessAndEntitlementsForLoggedInUser,getEntitlementForTransaction,getEntitlementDetailsForIds} from "./../../entitlementhelpers"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const entitlementRouter = express.Router()

entitlementRouter.route("/:reqtype")
  .get(requireAuth,async (req,res) => {
    console.log(req.params)
    try{
      const retObject = await getConsolidatedAccessAndEntitlementsForLoggedInUser(req.user,req.params.reqtype)
      // console.log (JSON.stringify(retObject,null,2))
      res.status(200).send(retObject)
    } catch(e){
      console.log("THERE IS AN ERROR",e)
      res.status(500).send({error:`Unable to fetch Entitlements for ${req.params}`})
    }
  })

entitlementRouter.route("/getentitlementforids")
  .post(requireAuth,async (req,res) => {
    const {ids} = req.body
    try{
      const retObject = await getEntitlementDetailsForIds(req.user,ids)
      res.status(200).send(retObject)
    } catch(e){
      console.log("THERE IS AN ERROR",e)
      res.status(500).send({error:"Unable to fetch Entitlements based on IDs"})
    }
  })

entitlementRouter.route("/test/newtest")
  .get(requireAuth,async (req,res) => {
    try{
      console.log("entered the route")
      const retObject = await getEntitlementForTransaction(req.user,req.params.reqtype)
      // console.log (JSON.stringify(retObject,null,2))
      res.status(200).send({data:retObject})
    } catch(e){
      console.log("THERE IS AN ERROR",e)
      res.status(500).send({error:`Unable to fetch Entitlements for ${req.params}`})
    }
  })

  
  // .post(requireAuth,(req,res) => res.send({data:req.body, user:req.user}))
