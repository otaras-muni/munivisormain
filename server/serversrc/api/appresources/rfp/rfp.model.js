import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { Entity } from "../entity/entity.model"
import { EntityUser } from "../entityUser/entityUser.model"
import { Docs } from "../docs/docs.model"

const { Schema } = mongoose

export const ratingSchema = new Schema({
  evalItem: String,
  evalPriority: {
    type: String
  },
  evalRating: Number
})

export const rfpDistributeSchema = Schema({
  rfpSelEvalContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  rfpSelEvalFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  rfpSelEvalRealFirstName: String,
  rfpSelEvalRealLastName: String,
  rfpSelEvalRealEmailId: String,
  rfpSelEvalRole: String,
  rfpSelEvalContactName: String,
  rfpSelEvalEmail: String,
  rfpSelEvalPhone: String,
  rfpSelEvalAddToDL: Boolean
})

export const rfpProcessContacts = Schema({
  rfpProcessContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  rfpProcessFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  rfpProcessContactRealFirstName: String,
  rfpProcessContactRealLastName: String,
  rfpProcessContactRealEmailId: String,
  rfpContactFor: String,
  rfpContactName: String, // this can be extracted from the populate query when populating information
  rfpContactEmail: String, // It is important to store this because a user can have multiple emails.
  rfpContactPhone: String, // Same is the case here, it is important to select an ID.
  rfpContactAddToDL: Boolean
})

export const rfpParticipantDistSchema = Schema({
  rfpParticipantFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  rfpParticipantFirmName: String, // May not need this
  rfpParticipantRealFirmName: String,
  rfpParticipantRealMSRBType: String,
  rfpParticipantContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  rfpParticipantRealFirstName: String,
  rfpParticipantRealLastName: String,
  rfpParticipantRealEmailId: String,
  rfpParticipantContactName: String,
  rfpParticipantContactEmail: String, // Same reasoning as above
  rfpParticipantContactPhone: String,
  rfpParticipantContactAddToDL: Boolean
})

export const rfpBidPacket = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: String,
  docFileName: String,
  docNote: String,
  docStatus: String,
  markedPublic: { publicFlag: Boolean, publicDate: Date },
  sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
  createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

// Need to check with Kapil on the best way to model this
export const rfpBidPacketChecklist = Schema({
  checkListCategoryId: String,
  checkListItems: [
    {
      rfpListItemDetails: String,
      rfpListItemConsider: Boolean,
      rfpListItemPriority: String,
      rfpListItemEndDate: Date,
      rfpListItemResolved: Boolean,
      rfpListItemAssignees: [
        {
          rfpTeamAssigneeId: { type: Schema.Types.ObjectId, ref: EntityUser },
          rfpTeamAssigneeFirstName: String,
          rfpTeamAssigneeLastName: String,
          rfpTeamAssigneeEmailId: String
        }
      ]
    }
  ]
})

export const checklistSchema = new Schema(
  {
    title: String,
    headers: [String],
    items: [{}]
  },
  { _id: false }
)

export const checklistsSchema = new Schema(
  {
    type: String,
    id: String,
    name: String,
    attributedTo: String,
    bestPractice: String,
    notes: String,
    lastUpdated: {
      date: Date,
      by: String
    },
    data: [checklistSchema]
  },
  { _id: false }
)

export const rfpQuestionsPostsSchema = Schema({
  postDetails: String,
  postDate: { type: Date, required: true, default: Date.now },
  postVisibilityFilter: String,
  postUserFirstName: String,
  postUserLastName: String,
  postUserEmailId: String,
  postContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  postDocument: { type: Schema.Types.ObjectId, ref: Docs },
  postParentId: { type: Schema.Types.ObjectId },
  postVisibilityFlag: Boolean
})

export const rfpParticipantQuestionsSchema = Schema({
  rfpPartUserFirstName: String,
  rfpPartUserLastName: String,
  rfpPartUserEmailId: String,
  rfpPartContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  rfpPartQuestDocId: { type: Schema.Types.ObjectId, ref: Docs },
  rfpPartQuestDetails: String,
  rfpPartQuestPosts: [rfpQuestionsPostsSchema],
  createdAt: Date,
})

export const rfpSupplierResponseSchema = Schema({
  PartContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  PartUserFirstName: String,
  PartUserLastName: String,
  PartUserEmailId: String,
  PartFirmId: String,
  PartFirmName: String,
  DocId: { type: Schema.Types.ObjectId, ref: Docs },
  DatePosted: Date,
  CurrentAction: String,
  LastUpdatedDate: Date,
  Comment: String,
  keyInformation: {
    proposalDate: String,
    tenorLoan: String,
    averageLife: String,
    paymentType: String,
    fixedRate: Number,
    floatingRateType: String,
    floatingRateRatio: Number,
    floatingRateSpread: Number,
    underlyingCredit: String,
    sectorCode: String,
    state: String,
    bankLender: String,
    provisionType: String
  },
  keyInterest: [
    {
      rateType: String,
      loanAmountFrom: Number,
      loanAmountTo: Number,
      maturities: String,
      rate: Number
    }
  ]
})

export const rfpEvaluationAggregationSchema = Schema({
  rfpParticipantFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  rfpEvaluationCategories: [
    {
      categoryName: String,
      categoryAggregatedScore: Number
    }
  ]
})

export const rfpFinalSelectionSchema = Schema({
  rfpParticipantFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  rfpParticipantFirmName: String,
  rfpParticipantMSRBType: String,
  rfpFinalContractDocuments: [
    {
      rfpFinalContractName: String,
      rfpFinalContractLocation: Number,
      rfpFinalContractDocType: String
    }
  ],
  evaluationComments: String,
  notesWinningBidder: String,
  documentId: { type: Schema.Types.ObjectId, ref: Docs },
  documentName: ""
})

function validateFirmId(firmId) {
  const firms = this.rfpParticipants || []
  return firmId.every(p =>
    firms.find(o => o.rfpParticipantFirmId === p.rfpParticipantFirmId)
  )
}
function validateMemberId(memberId) {
  const members = this.rfpEvaluationTeam || []
  return memberId.every(p =>
    members.find(
      o => o.rfpSelEvalContactId === p.rfpEvaluationCommitteeMemberId
    )
  )
}
export const rfpEvaluateSchema = new Schema({
  rfpParticipantFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  rfpEvaluationCommitteeMemberId: {
    type: Schema.Types.ObjectId,
    ref: EntityUser
  }, // TODO: It will be Object Id
  rfpParticipantFirmName: String,
  rfpParticipantMSRBType: String,
  rfpCommitteeMemberFirstName: String,
  rfpCommitteeMemberLastName: String,
  rfpCommitteeMemberEmailId: String,
  rfpEvaluationCategories: [
    {
      categoryName: String,
      categoryItems: [
        {
          evalItem: String,
          evalPriority: String,
          evalRating: Number
        }
      ]
    }
  ],
  evalNotes: String,
  rfpEvaluationDocuments: [
    {
      docCategory: String,
      docSubCategory: String,
      docType: String,
      docAWSFileLocation: String,
      docFileName: String,
      docNote: String,
      docStatus: String,
      markedPublic: { publicFlag: Boolean, publicDate: Date },
      sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
      createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
      createdUserName: String,
      LastUpdatedDate: { type: Date, required: true, default: Date.now }
    }
  ]
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const rfpSchema = new mongoose.Schema({
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  transactionId: Schema.Types.ObjectId,
  rfpTranClientId: { type: Schema.Types.ObjectId, ref: Entity }, // this is the financial advisor who is the client of munivisor
  rfpTranIssuerId: { type: Schema.Types.ObjectId, ref: Entity }, // this is the issuer client
  rfpTranClientFirmName: String,
  rfpTranClientMSRBType: String,
  rfpTranIssuerFirmName: String,
  rfpTranIssuerMSRBType: String,
  rfpTranName: String,
  rfpTranType: String,
  rfpTranPurposeOfRequest: String,
  rfpTranOtherSubtype: String,
  rfpTranAssignedTo: [{ type: Schema.Types.ObjectId, ref: EntityUser }], // There can be a combination of users from financial advisors and issuer users
  rfpTranRelatedTo: [
    Schema({
      relTranId: { type: Schema.Types.ObjectId },
      relType: String,
      relTranIssueName: String,
      relTranProjectName: String,
      relTranClientId: { type: Schema.Types.ObjectId },
      relTranClientName: String,
      tranType: String
    })
  ],
  rfpTranState: String,
  rfpTranCounty: String,
  rfpTranPrimarySector: String,
  rfpTranSecondarySector: String,
  rfpTranDateHired: Date,
  rfpTranStartDate: Date,
  rfpTranExpectedEndDate: Date,
  rfpTranStatus: String,
  rfpTransNotes: String,
  tranNotes: [notesSchema],
  // add new rfp //
  rfpTranSubType: String,
  rfpTranSecurityType: String,
  rfpParAmount: Number,
  rfpEstimatedRev: Number,
  rfpTranIsClientMsrbRegistered: String,
  rfpTranIsConduitBorrowerFlag: String,
  rfpTranIssueName: String,
  rfpTranProjectDescription: String,
  // add new rfp //
  //rfpEvaluations: [rfpEvaluateSchema], // {type:[rfpEvaluateSchema], validate:[{validator: validateFirmId,message:"Participant FirmId is not valid"}, {validator: validateMemberId,message:"Committee MemberId is not valid"}]},
  rfpEvaluationTeam: [rfpDistributeSchema],
  rfpProcessContacts: [rfpProcessContacts],
  rfpParticipants: [rfpParticipantDistSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  rfpBidDocuments: [rfpBidPacket],
  rfpBidCheckList: [checklistsSchema],
  rfpParticipantQuestions: [rfpParticipantQuestionsSchema],
  rfpMemberEvaluations: [rfpEvaluateSchema],
  rfpFinalEvaluations: [rfpEvaluationAggregationSchema],
  rfpFinalSelections: rfpFinalSelectionSchema,
  rfpSupplierResponses: [rfpSupplierResponseSchema],
  created_at: { type: Date, required: true, default: Date.now },
  updateDate: { type: Date, required: true, default: Date.now },
  updateUser: { type: Schema.Types.ObjectId, ref: EntityUser },
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

rfpSchema.index({rfpTranClientId:1})
rfpSchema.index({rfpTranIssuerId:1})
rfpSchema.index({"rfpEvaluationTeam.rfpSelEvalFirmId":1})
rfpSchema.index({"rfpProcessContacts.rfpProcessFirmId":1})
rfpSchema.index({"rfpParticipants.rfpParticipantFirmId":1})

export const RFP = mongoose.model("tranagencyrfps", rfpSchema)
