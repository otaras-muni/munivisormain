import ip from "ip"
import {generateControllers} from "../../modules/query"
import {
  EntityUser,
  Deals,
  RFP,
  ActMaRFP,
  Derivatives,
  BankLoans,
  Others,
  ActBusDev
} from "./../models"
import {canUserPerformAction} from "../../commonDbFunctions"
import {manageTasksForProcessChecklists} from "../taskmanagement/taskProcessInteraction"
import {getEligibleTransactionsForLoggedInUser, checkTranEligiblityForLoggedInUser} from "../../commonDbFunctions/getEligibleTransactionsForLoggedInUser"
import {getAllTransactionDetailsForLoggedInUser} from "../../commonDbFunctions/getAllDetailedTransactionsForLoggedInUser"
import {putData, getData, elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"
import {updateEligibleIdsForLoggedInUserFirm, getAllAccessAndEntitlementsForLoggedInUser} from "./../../entitlementhelpers"
import {RelatedToAssign} from "../commonservices/services.controller"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: RFP, _id: id, esType: "tranagencyrfps"})

const {ObjectID} = require("mongodb")

const postRFPTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]

  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (entitled) {
      const { rfpTranRelatedTo } = req.body
      const newRFPTransaction = new RFP({
        ...req.body
      })
      const insertedRFPTransaction = await newRFPTransaction.save()
      await RelatedToAssign(insertedRFPTransaction, rfpTranRelatedTo, "RFP", "multi")
      const es = returnFromES(req.user.tenantId, insertedRFPTransaction._id)
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      await Promise.all([es, ent])
      res.json(insertedRFPTransaction)
    } else {
      res
        .status(500)
        .send({done: false, error: "User not entitled to create RFP Transaction"})
    }
  } catch (error) {
    res
      .status(500)
      .send({done: false, error: "Error saving information on RFP Transactions"})
  }
}

const putTransaction = () => async(req, res) => {
  const {tranId} = req.params
  const {type} = req.query
  const {_id} = req.body

  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    // console.log("*******entitled******************", entitled)
    console.log("*******isEligible******************", isEligible)

    if (/* !entitled || */
    (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let tasks = []
    if (_id) {
      if (type === "rfpEvaluationTeam") {
        await RFP.updateOne({
          _id: ObjectID(tranId),
          "rfpEvaluationTeam._id": ObjectID(_id)
        }, {
          $set: {
            "rfpEvaluationTeam.$": req.body
          }
        })
      } else if (type === "rfpProcessContacts") {
        await RFP.updateOne({
          _id: ObjectID(tranId),
          "rfpProcessContacts._id": ObjectID(_id)
        }, {
          $set: {
            "rfpProcessContacts.$": req.body
          }
        })
      } else if (type === "rfpParticipants") {
        await RFP.updateOne({
          _id: ObjectID(tranId),
          "rfpParticipants._id": ObjectID(_id)
        }, {
          $set: {
            "rfpParticipants.$": req.body
          }
        })
      }
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      if(type === "check-track"){
        tasks = manageTasksForProcessChecklists(tranId, req.user)
      }
      const rfp = RFP
        .findOne({_id: tranId})
        .select("rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranName rfpTranType rfpTra" +
            "nPurposeOfRequest rfpEvaluationTeam rfpProcessContacts rfpParticipants rfpBidChe" +
            "ckList rfpParAmount rfpEstimatedRev rfpTranSecurityType")
      const es = returnFromES(req.user.tenantId, tranId)
      const [rfpdata] = await Promise.all([rfp, ent, tasks, es])
      res.json(rfpdata)
    } else {
      if (type === "rfpEvaluationTeam") {
        await RFP.updateOne({
          _id: ObjectID(tranId)
        }, {
          $addToSet: {
            rfpEvaluationTeam: req.body
          }
        })
      } else if (type === "rfpProcessContacts") {
        await RFP.updateOne({
          _id: ObjectID(tranId)
        }, {
          $addToSet: {
            rfpProcessContacts: req.body
          }
        })
      } else if (type === "rfpParticipants") {
        await RFP.updateOne({
          _id: ObjectID(tranId)
        }, {
          $addToSet: {
            rfpParticipants: req.body
          }
        })
      } else {
        await RFP.update({
          _id: tranId
        }, req.body)
      }
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      if(type === "check-track"){
        tasks = manageTasksForProcessChecklists(tranId, req.user)
      }
      const rfp = await RFP
        .findOne({_id: tranId})
        .select("rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranName rfpTranType rfpTra" +
            "nPurposeOfRequest rfpEvaluationTeam rfpProcessContacts rfpParticipants rfpBidChe" +
            "ckList rfpParAmount rfpEstimatedRev rfpTranIsConduitBorrowerFlag rfpTranSecurity" +
            "Type")
      const es = returnFromES(req.user.tenantId, tranId)
      const [rfpdata] = await Promise.all([rfp, ent, tasks, es])
      res.json(rfpdata)
    }
  } catch (error) {
    console.log("*******RFP**********putTransaction********", error)
    res
      .status(422)
      .send({done: false, error: error.message, success: ""})
  }
}

const getRfpByTransFirmMember = () => async(req, res) => {
  const {transId, memberId, firmId} = req.query
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, transId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let rfp = {}
    if (transId && memberId && firmId) {
      rfp = await RFP.aggregate([
        {
          $match: {
            _id: ObjectID(transId)
          }
        }, {
          $unwind: "$rfpMemberEvaluations"
        }, {
          $match: {
            "rfpMemberEvaluations.rfpEvaluationCommitteeMemberId": ObjectID(memberId),
            "rfpMemberEvaluations.rfpParticipantFirmId": ObjectID(firmId)
          }
        }, {
          $project: {
            rfpMemberEvaluations: 1,
            rfpTranClientId: 1,
            rfpTranIssuerId: 1
          }
        }
      ])
      rfp = Array.isArray(rfp) && rfp.length
        ? rfp[0]
        : rfp
    }
    if (transId && !memberId && !firmId) {
      rfp = await RFP
        .findOne({_id: transId})
        .select("rfpTranClientId rfpTranIssuerId rfpEvaluationTeam rfpParticipants")
    }

    res.json(rfp)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveRfpByTransFirmMember = () => async(req, res) => {
  const rfp = req.body
  const {type, evaluateId} = req.query
  const {transactionId, rfpMemberEvaluations} = rfp
  const {rfpParticipantFirmId, rfpEvaluationCommitteeMemberId} = rfpMemberEvaluations
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  const entitled = await canUserPerformAction(req.user, resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, transactionId)

  if (!entitled || (isEligible && !isEligible.canEditTran)) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }

  return RFP
    .findOne({_id: transactionId})
    .then(async docs => {
      if (docs) {
        if (type && type === "rfpEvaluationDocuments" && evaluateId) {
          await RFP.update({
            _id: transactionId,
            "rfpMemberEvaluations._id": evaluateId
          }, {
            $push: {
              "rfpMemberEvaluations.$.rfpEvaluationDocuments": {
                $each: req.body.rfpEvaluationDocuments
              }
            }
          })
          await returnFromES(req.user.tenantId, transactionId)

          return res.json(req.body)
        }
        const rfpEvals = (docs && docs.rfpMemberEvaluations) || []
        const findIndexEval = rfpEvals.findIndex(p => p.rfpParticipantFirmId.toString() === rfpParticipantFirmId && p.rfpEvaluationCommitteeMemberId.toString() === rfpEvaluationCommitteeMemberId)

        if (findIndexEval !== -1) {
          rfpEvals.splice(findIndexEval, 1, rfpMemberEvaluations)
        } else {
          rfpEvals.push(rfpMemberEvaluations)
        }
        docs.rfpMemberEvaluations = rfpEvals
        await docs.save()
        const evaluate = docs
          .rfpMemberEvaluations
          .find(memEval => memEval.rfpEvaluationCommitteeMemberId.toString() === req.user._id.toString() && memEval.rfpParticipantFirmId.toString() === memEval.rfpParticipantFirmId.toString())
        await returnFromES(req.user.tenantId, transactionId)
        return res.json(evaluate)

        /* const rfp = {
          transactionId,
          rfpMemberEvaluations: [rfpMemberEvaluations],
          rfpFinalValuations: [],
          rfpFinalWinner: {},
        }
        const newRfp = await RFP.create(rfp)
        await manageTasksForProcessChecklists(transactionId, req.user)
        res.json(newRfp) */
      }
    })
    .catch(error => res.status(422).send({done: false, error: error.message, success: "", requestedServices: resRequested}))
}

const putRfpEvaluationDocument = () => async(req, res) => {
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }
  ]
  try {
    const {_id} = req.body
    const {evaluateId} = req.query
    const resRequested = [
      {
        resource: "RFP",
        access: 2
      }
    ]
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, _id)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    await RFP.update({
      _id,
      "rfpMemberEvaluations._id": evaluateId
    }, {
      $push: {
        "rfpMemberEvaluations.$.rfpEvaluationDocuments": {
          $each: req.body.rfpEvaluationDocuments
        }
      }
    })

    const rfp = RFP
      .findById({_id})
      .select("rfpMemberEvaluations._id rfpMemberEvaluations.rfpEvaluationDocuments")
    const es = returnFromES(req.user.tenantId, _id)
    const [rfpData] = await Promise.all([rfp, es])

    const documents = rfpData
      .rfpMemberEvaluations
      .find(doc => doc._id.toString() === evaluateId.toString())
    res.json(documents)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: error.message, success: "", requestedServices: resRequested})
  }
}

const pullRfpEvaluationDocument = () => async(req, res) => {
  const {tranId, evaluateId, docId} = req.query
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await RFP.update({
      _id: ObjectID(tranId),
      "rfpMemberEvaluations._id": ObjectID(evaluateId)
    }, {
      $pull: {
        "rfpMemberEvaluations.$.rfpEvaluationDocuments": {
          _id: ObjectID(docId)
        }
      }
    })

    const rfp = RFP
      .findOne({_id: tranId})
      .select("rfpMemberEvaluations._id rfpMemberEvaluations.rfpEvaluationDocuments")
    const es = returnFromES(req.user.tenantId, tranId)
    const [rfpData] = await Promise.all([rfp, es])

    const documents = rfpData
      .rfpMemberEvaluations
      .find(doc => doc._id.toString() === evaluateId.toString())
    res.json(documents)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getRfpDistribution = () => async(req, res) => {
  const {tranId} = req.params
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const rfp = await RFP
      .findById({_id: tranId})
      .select("rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranName rfpTranType rfpPar" +
          "ticipantQuestions rfpTranPurposeOfRequest rfpEvaluationTeam rfpProcessContacts r" +
          "fpParticipants rfpBidCheckList rfpParAmount ")
    res.json(rfp)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getUserRfpDealsTransactions = () => async(req, res) => {
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }, {
      resource: "EntityUser",
      access: 1
    }, {
      resource: "Deals",
      access: 1
    }
  ]
  try {
    const {type} = req.query
    const entitled = await canUserPerformAction(req.user, resRequested)


    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }



    let rfpSelect = "rfpTranIssueName rfpTranIssueName rfpTranProjectDescription rfpTranIssuerId rfpT" +
        "ranIssuerFirmName rfpTranClientId created_at rfpTranIssuerId rfpTranType rfpTran" +
        "ClientFirmName rfpTranSubType"

    let dealSelect = "dealIssueTranIssueName dealIssueTranProjectDescription dealIssueTranClientId dea" +
        "lIssueTranClientFirmName dealIssueTranIssuerId dealIssueTranType dealIssueTranSu" +
        "bType created_at"

    let loanSelect = "actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranFirmName a" +
        "ctTranClientId actTranClientName actTranIssueName actTranProjectDescription"

    let derivativeSelect = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId actTranType a" +
        "ctTranClientName actTranIssueName actTranProjectDescription"

    let maRfpSelect = "actTranFirmId actType actSubType actUniqIdentifier actLeadFinAdvClientEntityName" +
        " actIssuerClient actIssuerClientEntityName actIssueName actProjectName actIssuer"

    const otherSelect = "actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranFirmName a" +
        "ctTranClientId actTranClientName actTranIssueName actTranProjectDescription"

    const busDevSelect = "actTranFirmId actType actSubType actUniqIdentifier actIssuerClient actIssuerClie" +
        "ntEntityName actIssueName actProjectName actIssuer"

    if (type === "calender") {
      rfpSelect = "rfpTranType rfpTranIssueName rfpTranProjectDescription"
      dealSelect = "dealIssueTranName dealIssueTranType dealIssueTranSubType dealIssuePricingDate de" +
          "alIssueExpAwardDate dealIssueTranIssueName dealIssueTranProjectDescription"
      loanSelect = "actTranType actTranSubType actTranUniqIdentifier bankLoanSummary.actTranClosingD" +
          "ate actTranIssueName actTranProjectDescription"
      derivativeSelect = "actTranType actTranSubType actTranUniqIdentifier actTranIssueName actTranProject" +
          "Description derivativeSummary.tranEffDate"
      maRfpSelect = "actType actSubType actIssueName actUniqIdentifier actProjectName maRfpSummary.ac" +
          "tExpAwaredDate  maRfpSummary.actPricingDate"
    }

    let detailsIds = {}
    let isEligibleForView = []
    let findQuery = {}
    if (req.user.checkEntitlement) {
      detailsIds = await getAllAccessAndEntitlementsForLoggedInUser(
        req.user,
        "nav"
      )
      isEligibleForView = (detailsIds && detailsIds.view) || []
      findQuery = { _id: { $in: isEligibleForView } }


      const [rfp,deals,loans,derivatives,marfps,others,busDev] = await Promise.all([RFP.find(findQuery).select(
        rfpSelect
      ),

      Deals.find(findQuery).select(
        dealSelect
      ),

      BankLoans.find(findQuery).select(loanSelect),
      Derivatives.find(findQuery).select(derivativeSelect),
      ActMaRFP.find(findQuery).select(maRfpSelect),
      Others.find(findQuery).select(otherSelect),
      ActBusDev.find(findQuery).select(busDevSelect)
      ])

      let transactions = rfp.concat(deals)
      transactions = transactions.concat(loans)
      transactions = transactions.concat(derivatives)
      transactions = transactions.concat(marfps)
      transactions = transactions.concat(others)
      transactions = transactions.concat(busDev)
      res.json(transactions)
    } else {

      const [rfp,deals,loans,derivatives,marfps,others,busDev] = await Promise.all([
        RFP.find({rfpTranClientId:ObjectID(req.user.tenantId)}).select(rfpSelect),
        Deals.find({dealIssueTranClientId:ObjectID(req.user.tenantId)}).select(dealSelect),
        BankLoans.find({actTranFirmId:ObjectID(req.user.tenantId)}).select(loanSelect),
        Derivatives.find({actTranFirmId:ObjectID(req.user.tenantId)}).select(derivativeSelect),
        ActMaRFP.find({actTranFirmId:ObjectID(req.user.tenantId)}).select(maRfpSelect),
        Others.find({actTranFirmId:ObjectID(req.user.tenantId)}).select(otherSelect),
        ActBusDev.find({actTranFirmId:ObjectID(req.user.tenantId)}).select(busDevSelect)
      ])

      let transactions = rfp.concat(deals)
      transactions = transactions.concat(loans)
      transactions = transactions.concat(derivatives)
      transactions = transactions.concat(marfps)
      transactions = transactions.concat(others)
      transactions = transactions.concat(busDev)
      res.json(transactions)
    }


  } catch (error) {
    console.log("======", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const userIsEligibleForActions = () => async(req, res) => {
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }, {
      resource: "EntityUser",
      access: 1
    }, {
      resource: "Deals",
      access: 1
    }, {
      resource: "BankLoans",
      access: 1
    }, {
      resource: "Derivatives",
      access: 1
    }, {
      resource: "ActMaRFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const userDetails = await getAllTransactionDetailsForLoggedInUser(req.user)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const isEligibleForRfpsView = (userDetails && userDetails.payload && userDetails.payload.rfps && userDetails.payload.rfps.transactions && userDetails.payload.rfps.transactions.map(activity => ({type: "rfp", tranId: activity.activityId, canEditTran: activity.canEditTran, canViewTran: activity.canViewTran}))) || []

    const isEligibleForDealsView = (userDetails && userDetails.payload && userDetails.payload.deals && userDetails.payload.deals.transactions && userDetails.payload.deals.transactions.map(activity => ({type: "deals", tranId: activity.activityId, canEditTran: activity.canEditTran, canViewTran: activity.canViewTran}))) || [] || []

    const isEligibleForLoansView = (userDetails && userDetails.payload && userDetails.payload.bankloans && userDetails.payload.bankloans.transactions && userDetails.payload.bankloans.transactions.map(activity => ({type: "bankloans", tranId: activity.activityId, canEditTran: activity.canEditTran, canViewTran: activity.canViewTran}))) || [] || []

    const isEligibleForDerivativesView = (userDetails && userDetails.payload && userDetails.payload.derivatives && userDetails.payload.derivatives.transactions && userDetails.payload.derivatives.transactions.map(activity => ({type: "derivatives", tranId: activity.activityId, canEditTran: activity.canEditTran, canViewTran: activity.canViewTran}))) || [] || []

    const isEligibleForMaRfpsView = (userDetails && userDetails.payload && userDetails.payload.marfps && userDetails.payload.marfps.transactions && userDetails.payload.marfps.transactions.map(activity => ({type: "marfps", tranId: activity.activityId, canEditTran: activity.canEditTran, canViewTran: activity.canViewTran}))) || [] || []

    res.json(isEligibleForRfpsView.concat(isEligibleForDealsView, isEligibleForLoansView, isEligibleForDerivativesView, isEligibleForMaRfpsView))
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

const addQuestion = () => async(req, res) => {
  const {tranId} = req.params
  const {rfpParticipantQuestions} = req.body
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const rfp = RFP.update({
      _id: tranId
    }, {$addToSet: {
        rfpParticipantQuestions
      }})
    const [rfpData] = await Promise.all([
      rfp,
      returnFromES(req.user.tenantId, tranId)
    ])
    res.json(rfpData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const addResponse = () => async(req, res) => {
  const {tranId} = req.params
  const {rfpSupplierResponses} = req.body
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)
    const {_id} = rfpSupplierResponses

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    if (_id) {
      await RFP.updateOne({
        _id: ObjectID(tranId),
        "rfpSupplierResponses._id": ObjectID(_id)
      }, {
        $set: {
          "rfpSupplierResponses.$": rfpSupplierResponses
        }
      })
    } else {
      console.log("==================================", rfpSupplierResponses)
      await RFP.update({
        _id: tranId
      }, {$addToSet: {
          rfpSupplierResponses
        }})
    }
    const rfp = RFP
      .findOne({_id: tranId})
      .select("rfpSupplierResponses")
    const es = returnFromES(req.user.tenantId, tranId)
    const [rfpData] = await Promise.all([rfp, es])
    res.json(rfpData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getResponseListByTrans = () => async(req, res) => {
  const {tranId} = req.params
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }, {
      resource: "Entity",
      access: 1
    }, {
      resource: "EntityUser",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const rfp = await RFP.aggregate([
      {
        $match: {
          _id: ObjectID(tranId)
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "rfpSupplierResponses.PartContactId",
          foreignField: "_id",
          as: "contacts"
        }
      }, {
        $lookup: {
          from: "entities",
          localField: "contacts.entityId",
          foreignField: "_id",
          as: "firms"
        }
      }, {
        $project: {
          rfpSupplierResponses: 1,
          "contacts.userFirstName": 1,
          "contacts.userLastName": 1,
          "contacts._id": 1,
          "contacts.entityId": 1,
          "firms.firmName": 1,
          "firms._id": 1
        }
      }
    ])
    res.json(rfp)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getRfpTransactionDetails = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "Deals",
      access: 1
    }, {
      resource: "RFP",
      access: 1
    }, {
      resource: "EntityUser",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    // const selectRelated = "rfpTranName rfpTranType rfpTransNotes"
    if (type === "summary") {
      select = "rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranStatus rfpTranIssuerFir" +
          "mName rfpTranName rfpTranIssueName rfpTranProjectDescription rfpTranType rfpTran" +
          "SubType rfpTranPurposeOfRequest rfpTranOtherTransactionType rfpTranRelatedTo rfp" +
          "TranIsConduitBorrowerFlag rfpTranState rfpTranCounty rfpTranPrimarySector rfpTra" +
          "nSecondarySector rfpTranDateHired rfpTranStartDate rfpTranExpectedEndDate rfpTra" +
          "nsNotes rfpEvaluationTeam rfpProcessContacts rfpParticipants rfpTranSecurityType" +
          " rfpTranSecurityType rfpParAmount opportunity rfpEstimatedRev OnBoardingDataMigr" +
          "ationHistorical historicalData tranNotes"
    }
    if (type === "distribute") {
      select = "rfpTranClientId rfpTranClientFirmName rfpTranIssuerId rfpTranIssuerFirmName rfpT" +
          "ranAssignedTo rfpTranIssueName rfpTranProjectDescription rfpTranRelatedTo rfpBid" +
          "Documents rfpTranName rfpTranType rfpTranSubType rfpTranPurposeOfRequest rfpTran" +
          "OtherTransactionType rfpTranState rfpTranCounty rfpTranPrimarySector rfpTranSeco" +
          "ndarySector rfpTranDateHired rfpTranStartDate rfpTranExpectedEndDate rfpTransNot" +
          "es rfpEvaluationTeam rfpProcessContacts rfpParticipants tranNotes"
    }
    if (type === "manage") {
      select = "rfpTranClientId rfpTranClientFirmName rfpTranIssuerId rfpTranIssuerFirmName rfpT" +
          "ranAssignedTo rfpTranIssueName rfpTranProjectDescription rfpTranRelatedTo rfpBid" +
          "Documents rfpTranName rfpTranType rfpTranSubType rfpTranPurposeOfRequest rfpTran" +
          "OtherTransactionType rfpTranState rfpTranCounty rfpTranPrimarySector rfpTranSeco" +
          "ndarySector rfpTranDateHired rfpTranStartDate rfpTranExpectedEndDate rfpTransNot" +
          "es rfpEvaluationTeam rfpProcessContacts rfpParticipants rfpFinalSelections"
    }
    if (type === "question") {
      select = "rfpTranClientId rfpTranAssignedTo rfpParticipantQuestions rfpTranIssueName rfpTr" +
          "anProjectDescription rfpTranIssuerId rfpProcessContacts rfpParticipants rfpEvalu" +
          "ationTeam"
    }
    if (type === "check-track") {
      select = "rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranName rfpTranIssueName r" +
          "fpTranProjectDescription rfpParticipants rfpTranType rfpTranSubType rfpTranPurpo" +
          "seOfRequest rfpTranOtherTransactionType rfpTranState rfpTranCounty rfpTranPrimar" +
          "ySector rfpTranSecondarySector rfpTranDateHired rfpTranStartDate rfpTranExpected" +
          "EndDate rfpTransNotes rfpBidCheckList rfpEvaluationTeam rfpProcessContacts rfpTr" +
          "anAssignedTo "
    }
    if (type === "supplierResponse" || type === "audit-trail") {
      select = "rfpTranClientId rfpSupplierResponses rfpTranIssueName rfpTranProjectDescription " +
          "rfpTranIssuerId rfpProcessContacts rfpParticipants rfpEvaluationTeam"
    }
    const rfp = await RFP
      .findById({_id: tranId})
      .select(`${select} rfpTranStatus`)
      .populate({path: "rfpTranAssignedTo", model: EntityUser, select: "userFirstName"})

    res.json(select
      ? rfp
      : {})
  } catch (error) {
    console.log("*******RFP*********getRfpTransactionDetails********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const addRfpTransactionMedia = () => async(req, res) => {
  const {transId, queId} = req.query
  const {rfpPost} = req.body
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, transId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    return await RFP.findOne({
      _id: transId
    }, {
      rfpParticipantQuestions: {
        $elemMatch: {
          _id: queId
        }
      }
    }).then(async rfp => {
      if (rfp && Array.isArray(rfp.rfpParticipantQuestions) && rfp.rfpParticipantQuestions.length) {
        rfp
          .rfpParticipantQuestions[0]
          .rfpPartQuestPosts
          .push(rfpPost)
        await rfp.save()
      }
      await returnFromES(req.user.tenantId, transId)
      res.json(rfp)
    })
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "Error failing to to save the Questions posted by participants", success: "", requestedServices: resRequested})
  }
}

const getRfpTransactionMediaDetails = () => async(req, res) => {
  const {transId, queId} = req.query
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }, {
      resource: "EntityUser",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, transId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let rfp = await RFP.aggregate([
      {
        $match: {
          _id: ObjectID(transId.toString())
        }
      }, {
        $unwind: "$rfpParticipantQuestions"
      }, {
        $lookup: {
          from: "entityusers",
          localField: "rfpParticipantQuestions.rfpPartContactId",
          foreignField: "_id",
          as: "rfpParticipantQuestions.rfpContact"
        }
      }, {
        $match: {
          "rfpParticipantQuestions._id": ObjectID(queId.toString())
        }
      }, {
        $project: {
          rfpParticipantQuestions: 1,
          rfpTranClientId: 1,
          rfpTranIssuerId: 1
        }
      }
    ])
    if (rfp.length) {
      rfp[0].rfpParticipantQuestions.rfpContact = rfp[0].rfpParticipantQuestions.rfpContact[0].userFirstName
      rfp = rfp[0]
    }
    res.json(rfp)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getDecideDataByTrans = () => async(req, res) => {
  const {tranId} = req.params
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const rfp = await RFP.aggregate([
      {
        $match: {
          _id: ObjectID(tranId)
        }
      }, {
        $project: {
          rfpMemberEvaluations: 1
        }
      }, {
        $unwind: "$rfpMemberEvaluations"
      }, {
        $project: {
          rfpParticipantFirmId: "$rfpMemberEvaluations.rfpParticipantFirmId",
          rfpEvaluationCommitteeMemberId: "$rfpMemberEvaluations.rfpEvaluationCommitteeMemberId",
          rfpEvaluationCategories: "$rfpMemberEvaluations.rfpEvaluationCategories"
        }
      }, {
        $unwind: "$rfpEvaluationCategories"
      }, {
        $project: {
          "rfpEvaluationCategories.categoryItems": 1,
          "rfpEvaluationCategories.categoryName": 1,
          rfpParticipantFirmId: 1,
          rfpEvaluationCommitteeMemberId: 1
        }
      }, {
        $unwind: "$rfpEvaluationCategories.categoryItems"
      }, {
        $addFields: {
          categoryItem: "$rfpEvaluationCategories.categoryItems.evalItem",
          categoryName: "$rfpEvaluationCategories.categoryName",
          categoryPriority: "$rfpEvaluationCategories.categoryItems.evalPriority",
          categoryItemRating: "$rfpEvaluationCategories.categoryItems.evalRating",
          revisedRating: {
            $switch: {
              branches: [
                {
                  case: {
                    $eq: ["High", "$rfpEvaluationCategories.categoryItems.evalPriority"]
                  },
                  then: 3
                }, {
                  case: {
                    $eq: ["Medium", "$rfpEvaluationCategories.categoryItems.evalPriority"]
                  },
                  then: 2
                }, {
                  case: {
                    $eq: ["Low", "$rfpEvaluationCategories.categoryItems.evalPriority"]
                  },
                  then: 1
                }
              ],
              default: -1
            }
          }
        }
      }, {
        $project: {
          _id: 1,
          categoryItem: 1,
          rfpParticipantFirmId: 1,
          categoryName: 1,
          categoryPriority: 1,
          rfpEvaluationCommitteeMemberId: 1,
          categoryItemRating: 1,
          revisedRating: 1,
          consolidatedRating: {
            $multiply: ["$revisedRating", "$categoryItemRating"]
          }
        }
      }, {
        $group: {
          _id: {
            participant: "$rfpParticipantFirmId",
            category: "$categoryName",
            consolidatedRating: "$consolidatedRating"
          },
          count: {
            $sum: 1
          }
        }
      }, {
        $project: {
          participant: "$_id.participant",
          category: "$_id.category",
          consolidatedRating: "$_id.consolidatedRating",
          count: 1
        }
      }, {
        $group: {
          _id: {
            participant: "$participant",
            category: "$category"
          }, // build any group key ypo need
          numerator: {
            $sum: {
              $multiply: ["$consolidatedRating", "$count"]
            }
          },
          denominator: {
            $sum: "$count"
          }
        }
      }, {
        $project: {
          participant: "$_id.participant",
          category: "$_id.category",
          average: {
            $divide: ["$numerator", "$denominator"]
          }
        }
      }, {
        $group: {
          _id: "$participant",
          categoryScores: {
            $push: {
              category: "$category",
              averageScore: "$average"
            }
          }
        }
      }, {
        $addFields: {
          totalRfpScore: {
            $sum: "$categoryScores.averageScore"
          }
        }
      }
    ])
    const rfpFinalSelections = await RFP
      .findOne({_id: tranId})
      .select("rfpFinalSelections")
    res.json({rfp, rfpFinalSelections})
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const postTransactionEvalWinner = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    console.log("*******entitled******************", entitled)
    console.log("*******isEligible******************", isEligible)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    if (type === "winner") {
      select = "rfpFinalSelections"
      await RFP.update({
        _id: tranId
      }, req.body)
    }
    if (type === "documents") {
      select = "rfpBidDocuments"
      const {_id} = req.body
      await RFP.update({
        _id
      }, {
        $addToSet: {
          rfpBidDocuments: req.body.rfpBidDocuments
        }
      })
    }
    const rfp = RFP
      .findOne({_id: tranId})
      .select(select)
    const es = returnFromES(req.user.tenantId, tranId)
    const [rfpData] = await Promise.all([rfp, es])
    res.json(rfpData)
  } catch (error) {
    console.log("*******RFP**********postTransactionEvalWinner********", error)
    res
      .status(422)
      .send({done: false, error: error.message, success: ""})
  }
}

const isValidRfpEvaluations = () => async(req, res) => {
  const {id} = req.query
  const resRequested = [
    {
      resource: "RFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, id)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const rfp = await RFP
      .findById({_id: id})
      .select("rfpEvaluationTeam rfpProcessContacts rfpParticipants rfpBidDocuments")
    const isValid = rfp.rfpEvaluationTeam && rfp.rfpEvaluationTeam.length && rfp.rfpParticipants && rfp.rfpParticipants.length && rfp.rfpBidDocuments && rfp.rfpBidDocuments.length
    res
      .status(200)
      .json(!!isValid)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putRelatedTran = () => async(req, res) => {
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  const {tranId} = req.params
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)
    const select = "rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranStatus rfpTranIssuerFir" +
        "mName rfpTranName rfpTranIssueName rfpTranType rfpTranPurposeOfRequest rfpTranOt" +
        "herTransactionType rfpTranRelatedTo rfpTranIsConduitBorrowerFlag rfpTranState rf" +
        "pTranCounty rfpTranPrimarySector rfpTranProjectDescription rfpTranSecondarySecto" +
        "r rfpTranDateHired rfpTranStartDate rfpTranExpectedEndDate rfpTransNotes rfpEval" +
        "uationTeam rfpProcessContacts rfpParticipants rfpTranSecurityType rfpTranSecurit" +
        "yType"

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await RFP.update({
      _id: tranId
    }, {
      $addToSet: {
        rfpTranRelatedTo: req.body
      }
    })

    const rfpDetails = await RFP.findById(tranId).select("rfpTranIssueName rfpTranProjectDescription rfpTranIssuerId rfpTranIssuerFirmName")
    await RelatedToAssign(rfpDetails, req.body, "RFP", "single")

    const rfp = RFP
      .findById({_id: tranId})
      .select(select)
    const es = returnFromES(req.user.tenantId, tranId)
    const [rfpData] = await Promise.all([rfp, es])

    res.json(rfpData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getIpAddress = () => async(req, res) => {
  try {
    res.json({
      connect: ip.address()
    })
  } catch (error) {
    res
      .status(422)
      .send({done: false})
  }
}

const pullDistribute = () => async(req, res) => {
  const {tranId} = req.params
  const {type, delId} = req.query
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    if (type === "rfpEvaluationTeam") {
      await RFP.update({
        _id: ObjectID(tranId)
      }, {
        $pull: {
          rfpEvaluationTeam: {
            _id: delId
          }
        }
      })
    } else if (type === "rfpProcessContacts") {
      await RFP.update({
        _id: ObjectID(tranId)
      }, {
        $pull: {
          rfpProcessContacts: {
            _id: delId
          }
        }
      })
    } else if (type === "rfpParticipants") {
      await RFP.update({
        _id: ObjectID(tranId)
      }, {
        $pull: {
          rfpParticipants: {
            _id: delId
          }
        }
      })
    }
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const rfp = RFP
      .findOne({_id: tranId})
      .select("rfpTranClientId rfpTranIssuerId rfpTranAssignedTo rfpTranName rfpTranType rfpTra" +
          "nPurposeOfRequest rfpEvaluationTeam rfpProcessContacts rfpParticipants rfpBidChe" +
          "ckList rfpParAmount rfpEstimatedRev rfpTranIsConduitBorrowerFlag rfpTranSecurity" +
          "Type")
    const es = returnFromES(req.user.tenantId, tranId)
    const [rfpData] = await Promise.all([rfp, es, ent])
    res.json(rfpData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const postRFPNotes = () => async(req, res) => {
  const {tranId} = req.params
  const {_id} = req.body
  const resRequested = [
    {
      resource: "RFP",
      access: 2
    }
  ]
  try {

    if (_id) {
      await RFP.updateOne({
        _id: ObjectID(tranId),
        "tranNotes._id": ObjectID(_id)
      }, {
        $set: {
          "tranNotes.$": req.body
        }
      })
    } else {
      await RFP.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          tranNotes: req.body
        }
      })
    }
    const rfp = await RFP.findOne({_id: tranId}).select("tranNotes")
    const es = returnFromES(req.user.tenantId, tranId)
    await Promise.all([rfp, es])
    res.json(rfp)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export default generateControllers(RFP, {
  postRFPTransaction: postRFPTransaction(),
  putTransaction: putTransaction(),
  pullDistribute: pullDistribute(),
  getRfpByTransFirmMember: getRfpByTransFirmMember(),
  saveRfpByTransFirmMember: saveRfpByTransFirmMember(),
  putRfpEvaluationDocument: putRfpEvaluationDocument(),
  pullRfpEvaluationDocument: pullRfpEvaluationDocument(),
  getRfpDistribution: getRfpDistribution(),
  getUserRfpDealsTransactions: getUserRfpDealsTransactions(),
  userIsEligibleForActions: userIsEligibleForActions(),
  addQuestion: addQuestion(),
  addResponse: addResponse(),
  getResponseListByTrans: getResponseListByTrans(),
  getRfpTransactionDetails: getRfpTransactionDetails(),
  getRfpTransactionMediaDetails: getRfpTransactionMediaDetails(),
  addRfpTransactionMedia: addRfpTransactionMedia(),
  getDecideDataByTrans: getDecideDataByTrans(),
  postTransactionEvalWinner: postTransactionEvalWinner(),
  isValidRfpEvaluations: isValidRfpEvaluations(),
  putRelatedTran: putRelatedTran(),
  getIpAddress: getIpAddress(),
  postRFPNotes: postRFPNotes()
})
