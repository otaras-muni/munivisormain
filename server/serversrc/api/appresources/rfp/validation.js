import Joi from "joi-browser"

const relatedItems = Joi.object().keys({
  id: Joi.string().required(),
  type: Joi.string().required()
})

const contractRefSchema = Joi.object().keys({
  contractId: Joi.string()
    .allow("")
    .optional(),
  contractName: Joi.string()
    .allow("")
    .optional(),
  contractType: Joi.string()
    .allow("")
    .optional(),
  endDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  startDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const contractsSchema = Joi.object().keys({
  contractRef: contractRefSchema,
  digitize: Joi.boolean().optional(),
  nonTranFees: Joi.array().optional(),
  retAndEsc: Joi.array().optional(),
  sof: Joi.array().optional(),
  agree: Joi.string()
    .allow("")
    .optional(),
  reviewed: Joi.boolean().optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const tranNotesSchema = Joi.object().keys({
  note: Joi.string().allow("").optional(),
  createdAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedByName: Joi.string().allow("").optional(),
  updatedById: Joi.string().allow("").optional(),
  _id: Joi.string().allow("").optional()
})

const tranShema = Joi.object().keys({
  rfpTranName: Joi.string().required().optional(),
  rfpTranType: Joi.string().required().optional(),
  rfpTranSubType: Joi.string().required().optional(),
  rfpTranOtherSubtype: Joi.string().allow("").optional(),
  rfpTranClientId: Joi.string().required().optional(),
  rfpTranClientFirmName: Joi.string().required().optional(),
  rfpTranIssuerId: Joi.string().required().optional(),
  rfpTranIssuerFirmName: Joi.string().required().optional(),
  rfpTranIssuerMSRBType: Joi.string().required().optional(),
  rfpTranFirmLeadAdvisorId: Joi.string().required().optional(),
  rfpTranFirmLeadAdvisorName: Joi.string().required().optional(),
  rfpTranIsClientMsrbRegistered: Joi.boolean().required().optional(),
  rfpTranIsConduitBorrowerFlag: Joi.string().optional(),
  rfpTranIssueName: Joi.string().allow("").optional(),
  rfpTranRelatedTo: Joi.array().optional(),
  rfpTranPrimarySector: Joi.string().allow("").optional(),
  rfpTranSecondarySector: Joi.string().allow("").optional(),
  // rfpTransNotes: Joi.string().allow("").optional(),
  tranNotes: tranNotesSchema,
  rfpTranProjectDescription: Joi.string().allow("").optional(),
  rfpTranAssignedTo: Joi.array().min(1).required(),
  rfpTranPurposeOfRequest: Joi.string().optional(),
  createdByUser: Joi.string().required(),
  clientActive: Joi.boolean().required().optional(),
  rfpTranStatus: Joi.string().required(),
  rfpEvaluationTeam: Joi.array().min(1).required().optional(),
  contracts: contractsSchema,
  opportunity: Joi.object().optional()
})

const tranShema1 = Joi.object().keys({
  rfpTranName: Joi.string()
    .required()
    .optional(),
  rfpTranType: Joi.string()
    .required()
    .optional(),
  rfpTranSubType: Joi.string()
    .required()
    .optional(),
  rfpTranOtherTransactionType: Joi.string()
    .allow("")
    .optional(),
  rfpTranClientId: Joi.string()
    .required()
    .optional(),
  rfpTranClientFirmName: Joi.string()
    .required()
    .optional(),
  rfpTranIssuerId: Joi.string()
    .required()
    .optional(),
  rfpTranIssuerFirmName: Joi.string()
    .required()
    .optional(),
  rfpTranIssuerMSRBType: Joi.string()
    .required()
    .optional(),
  rfpTranFirmLeadAdvisorId: Joi.string()
    .required()
    .optional(),
  rfpTranFirmLeadAdvisorName: Joi.string()
    .required()
    .optional(),
  rfpTranIsClientMsrbRegistered: Joi.boolean()
    .required()
    .optional(),
  rfpTranIsConduitBorrowerFlag: Joi.string().optional(),
  rfpTranIssueName: Joi.string()
    .allow("")
    .optional(),
  rfpTranRelatedTo: Joi.array().optional(),
  rfpTranPrimarySector: Joi.string()
    .allow("")
    .optional(),
  rfpTranSecondarySector: Joi.string()
    .allow("")
    .optional(),
  rfpTransNotes: Joi.string()
    .allow("")
    .optional(),
  rfpTranProjectDescription: Joi.string()
    .allow("")
    .optional(),
  rfpTranAssignedTo: Joi.array()
    .min(1)
    .required(),
  rfpTranPurposeOfRequest: Joi.string()
    .optional()
    .required(),
  rfpTranDateHired: Joi.date()
    .min(new Date())
    .required(),
  rfpTranStartDate: Joi.date()
    .min(Joi.ref("rfpTranDateHired"))
    .optional()
    .required(),
  rfpTranExpectedEndDate: Joi.date()
    .min(Joi.ref("rfpTranStartDate"))
    .optional()
    .required()
})

const rfpEvaluationTeam = Joi.object().keys({
  rfpSelEvalContactId: Joi.string().required(),
  rfpSelEvalFirmId: Joi.string().required(),
  rfpSelEvalRealFirstName: Joi.string().allow("").optional(),
  rfpSelEvalRealLastName: Joi.string().allow("").optional(),
  rfpSelEvalRealEmailId: Joi.string().allow("").optional(),
  rfpSelEvalRole: Joi.string().allow("").optional(),
  rfpSelEvalContactName: Joi.string().required(),
  rfpSelEvalEmail: Joi.string().email().required(),
  rfpSelEvalPhone: Joi.string().allow("").optional(),
  rfpSelEvalAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional()
})

const rfpProcessContacts = Joi.object().keys({
  rfpProcessContactId: Joi.string().required(),
  rfpProcessFirmId: Joi.string().required(),
  rfpProcessContactRealFirstName: Joi.string().allow("").optional(),
  rfpProcessContactRealLastName: Joi.string().allow("").optional(),
  rfpProcessContactRealEmailId: Joi.string().allow("").optional(),
  rfpContactFor: Joi.string().required(),
  rfpContactName: Joi.string().required(),
  rfpContactEmail: Joi.string().email().required(),
  rfpContactPhone: Joi.string().required(),
  rfpContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional()
})

const rfpParticipants = Joi.object().keys({
  rfpParticipantFirmId:  Joi.string().required(),
  rfpParticipantRealFirmName: Joi.string().allow("").optional(),
  rfpParticipantRealMSRBType: Joi.string().allow("").optional(),
  rfpParticipantRealFirstName: Joi.string().allow("").optional(),
  rfpParticipantRealLastName: Joi.string().allow("").optional(),
  rfpParticipantRealEmailId: Joi.string().allow("").optional(),
  rfpParticipantFirmName: Joi.string().required(),
  rfpParticipantContactName: Joi.string().required(),
  rfpParticipantContactEmail: Joi.string().email().required(),
  rfpParticipantContactPhone: Joi.string().required(),
  rfpParticipantContactId: Joi.string().required(),
  rfpParticipantContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional()
})

const docAction = Joi.object().keys({
  actionType: Joi.string()
    .required()
    .optional(),
  user: Joi.string()
    .required()
    .optional(),
  userFirstName: Joi.string()
    .required()
    .optional(),
  userLastName: Joi.string()
    .required()
    .optional(),
  userEmailId: Joi.string()
    .required()
    .optional(),
  date: Joi.string()
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})
const rfpBidDocuments = Joi.object().keys({
  rfpBidDocName: Joi.string().required(),
  rfpBidDocId: Joi.string().required(),
  rfpBidDocType: Joi.string().required(),
  rfpBidDocStatus: Joi.string().required(),
  rfpBidDocAction: Joi.array().optional(),
  rfpBidDocUploadUser: Joi.string().required(),
  rfpBidDocUploadFirstName: Joi.string().required(),
  rfpBidDocUploadLastName: Joi.string().required(),
  rfpBidDocUploadEmailId: Joi.string().required(),
  _id: Joi.string()
    .required()
    .optional()
})

const transDistributeSchema = Joi.object().keys({
  rfpEvaluationTeam: Joi.array()
    .items(rfpEvaluationTeam)
    .required(),
  rfpProcessContacts: Joi.array()
    .items(rfpProcessContacts)
    .required(),
  rfpParticipants: Joi.array()
    .items(rfpParticipants)
    .required()
  /*  rfpBidDocuments:  Joi.array().items(rfpBidDocuments).required(),*/
})

const rfpTeamAssignee = Joi.object().keys({
  _id: Joi.string()
    .required()
    .optional(),
  rfpTeamAssigneeId: Joi.string().required(),
  rfpTeamAssigneeFirstName: Joi.string()
    .required()
    .optional(),
  rfpTeamAssigneeLastName: Joi.string()
    .required()
    .optional(),
  rfpTeamAssigneeEmailId: Joi.string()
    .required()
    .optional()
})

const checkListItems = Joi.object().keys({
  _id: Joi.string()
    .required()
    .optional(),
  rfpListItemDetails: Joi.string().required(),
  rfpListItemConsider: Joi.boolean().required(),
  rfpListItemPriority: Joi.string().required(),
  rfpListItemEndDate: Joi.string().required(),
  rfpListItemResolved: Joi.boolean()
    .required()
    .optional(),
  rfpListItemAssignees: Joi.array()
    .min(1)
    .items(rfpTeamAssignee)
    .required()
})

const rfpBidPacketChecklist = Joi.object().keys({
  checkListCategoryId: Joi.string().required(),
  checkListItems: Joi.array()
    .items(checkListItems)
    .required(),
  _id: Joi.string()
    .required()
    .optional()
})

const checkInTrackSchema = Joi.object().keys({
  rfpBidCheckList: Joi.array()
    .items(rfpBidPacketChecklist)
    .required()
})

const rfpParticipantQuestions = Joi.object().keys({
  rfpPartContactId: Joi.string().required(),
  rfpPartUserFirstName: Joi.string().required(),
  rfpPartUserLastName: Joi.string().required(),
  rfpPartUserEmailId: Joi.string().required(),
  rfpPartQuestionId: Joi.string()
    .optional()
    .empty(""),
  rfpPartQuestDocId: Joi.string().optional(),
  rfpPartQuestDetails: Joi.string().required(),
  rfpPartQuestPosts: Joi.array()
    .required()
    .optional(),
  createdAt: Joi.date().allow(""),
})

const rfpSupplierResponses = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .required()
    .optional(),
  PartContactId: Joi.string().required(),
  PartFirmId: Joi.string().required(),
  PartFirmName: Joi.string().required(),
  PartUserFirstName: Joi.string().required(),
  PartUserLastName: Joi.string().required(),
  PartUserEmailId: Joi.string().required(),
  DatePosted: Joi.string().required(),
  DocId: Joi.string().required(),
  LastUpdatedDate: Joi.string().required(),
  Comment: Joi.string()
    .required()
    .optional(),
  keyInformation: Joi.object({
    proposalDate: Joi.date()
      .example(new Date("2016-01-01"))
      .allow("")
      .optional(),
    tenorLoan: Joi.string()
      .allow("")
      .optional(),
    averageLife: Joi.string()
      .allow("")
      .optional(),
    paymentType: Joi.string()
      .allow("")
      .optional()
      .required(),
    fixedRate: Joi.number()
      .allow(null)
      .optional(),
    floatingRateType: Joi.string()
      .allow("")
      .optional(),
    floatingRateRatio: Joi.number()
      .allow(null)
      .optional(),
    floatingRateSpread: Joi.number()
      .allow(null)
      .optional(),
    underlyingCredit: Joi.string()
      .allow("")
      .optional(),
    sectorCode: Joi.string()
      .required()
      .optional(),
    state: Joi.string()
      .required()
      .optional(),
    bankLender: Joi.string()
      .allow("")
      .optional(),
    provisionType: Joi.string()
      .allow("")
      .optional(),
    _id: Joi.string().optional()
  })
    .required()
    .optional(),
  keyInterest: Joi.array()
    .required()
    .optional()
})

const rfpPost = Joi.object().keys({
  postDetails: Joi.string().required(),
  postDate: Joi.string().required(),
  postVisibilityFilter: Joi.string()
    .optional()
    .empty(""),
  postContactId: Joi.string().required(),
  postUserFirstName: Joi.string().required(),
  postUserLastName: Joi.string().required(),
  postUserEmailId: Joi.string().required(),
  postDocument: Joi.string()
    .required()
    .optional(),
  postParentId: Joi.string().required(),
  postVisibilityFlag: Joi.boolean()
    .required()
    .optional(),
  rfpPartQuestPosts: Joi.array()
    .required()
    .optional()
})

const rfpSupplierResponsesSchema = Joi.object().keys({
  rfpSupplierResponses
})
const rfpQuestionSchema = Joi.object().keys({
  rfpParticipantQuestions
})
const rfpPostSchema = Joi.object().keys({
  rfpPost
})
export const CheckInTrackValid = input => {
  return Joi.validate(input, checkInTrackSchema, {
    abortEarly: false,
    stripUnknown: false
  })
}

export const RfpQuestionValid = input => {
  return Joi.validate(input, rfpQuestionSchema, {
    abortEarly: false,
    stripUnknown: false
  })
}

export const RfpSupplierResponseValid = input => {
  return Joi.validate(input, rfpSupplierResponsesSchema, {
    abortEarly: false,
    stripUnknown: false
  })
}

export const RfpPostValid = input => {
  return Joi.validate(input, rfpPostSchema, {
    abortEarly: false,
    stripUnknown: false
  })
}

export const TransValidate = input => {
  return Joi.validate(input, tranShema, {
    abortEarly: false,
    stripUnknown: false
  })
}

export const TransDistribute = input => {
  return Joi.validate(input, transDistributeSchema, {
    abortEarly: false,
    stripUnknown: false
  })
}

export const postErrorHandling = (req, res, next) => {
  const errors = RfpPostValid(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const responseErrorHandling = (req, res, next) => {
  const errors = RfpSupplierResponseValid(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const questionErrorHandling = (req, res, next) => {
  const errors = RfpQuestionValid(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const createNewErrorHandling = (req, res, next) => {
  const errors = TransValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const errorHandling = (req, res, next) => {
  const { type } = req.query
  let errors
  if (type === "distributeSave") {
    errors = TransDistribute(req.body)
  }
  if (type === "check-track") {
    //errors = CheckInTrackValid(req.body)
  }
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}
