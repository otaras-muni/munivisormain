import express from "express"
import rfpController from "./rfp.controller"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import {
  createNewErrorHandling,
  errorHandling,
  postErrorHandling,
  questionErrorHandling,
  responseErrorHandling
} from "./validation"
import {checkClosedTransaction} from "../commonservices/services.controller";

export const rfpRouter = express.Router()

rfpRouter.param("id", rfpController.findByParam)

rfpRouter.route("/")
  .get(requireAuth, rfpController.getRfpByTransFirmMember)
  .post(createNewErrorHandling, requireAuth, rfpController.postRFPTransaction)

rfpRouter.route("/ipAddress")
  .get(rfpController.getIpAddress)

rfpRouter.route("/rfpEvaluation")
  .get(requireAuth, rfpController.isValidRfpEvaluations)
  .post(requireAuth, checkClosedTransaction, rfpController.saveRfpByTransFirmMember)
  .put(requireAuth, checkClosedTransaction, rfpController.putRfpEvaluationDocument)
  .delete(requireAuth, checkClosedTransaction, rfpController.pullRfpEvaluationDocument)

rfpRouter.route("/eligibleAction")
  .get(requireAuth, rfpController.userIsEligibleForActions)

rfpRouter.route("/:id")
  .get(requireAuth, rfpController.getOne)
  .delete(requireAuth, checkClosedTransaction, rfpController.deleteOne)

rfpRouter.route("/rfpDistribution/:tranId")
  .get(requireAuth, rfpController.getRfpDistribution)
  .put(requireAuth, checkClosedTransaction, rfpController.putRelatedTran)
  .post(requireAuth, checkClosedTransaction, errorHandling,rfpController.putTransaction)
  .delete(requireAuth, checkClosedTransaction, rfpController.pullDistribute)

rfpRouter.route("/transaction/:type/:tranId")
  .get(requireAuth, rfpController.getRfpTransactionDetails)
  .put(requireAuth, checkClosedTransaction, rfpController.postTransactionEvalWinner)

rfpRouter.route("/transaction/media")
  .get(requireAuth, rfpController.getRfpTransactionMediaDetails)
  .put(requireAuth, checkClosedTransaction, postErrorHandling, rfpController.addRfpTransactionMedia)

rfpRouter.route("/transactions/:rfpTranClientId")
  .get(requireAuth, rfpController.getUserRfpDealsTransactions)

rfpRouter.route("/question/:tranId")
  .put(requireAuth, checkClosedTransaction, questionErrorHandling, rfpController.addQuestion)

rfpRouter.route("/response/:tranId")
  .get(requireAuth, rfpController.getResponseListByTrans)
  .put(requireAuth, checkClosedTransaction, responseErrorHandling, rfpController.addResponse)

rfpRouter.route("/decide/:tranId")
  .get(requireAuth, rfpController.getDecideDataByTrans)

rfpRouter.route("/notes/:tranId")
  .post(requireAuth, checkClosedTransaction, rfpController.postRFPNotes)
