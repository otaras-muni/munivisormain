import express from "express"
import notificationsController from "./notification.controller"

import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import { getAllTransactionDetailsForLoggedInUser } from
  "../../commonDbFunctions/getAllDetailedTransactionsForLoggedInUser"
import { getAllUsersForFirm } from
  "../../commonDbFunctions/getAllUsersForFirm"

export const notificationsRouter = express.Router()

notificationsRouter.get("/related-info", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  try {
    const result = await getAllTransactionDetailsForLoggedInUser(user)
    return res.json(result)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting related info" })
  }
})

notificationsRouter.get("/users", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  try {
    const result = await getAllUsersForFirm(user.entityId)
    return res.json(result.map(e => ({
      id: e._id,
      name: `${e.userFirstName} ${e.userMiddleName} ${e.userLastName}`,
      emails: e.userEmails
    })))
  } catch (err) {
    return res.status(422).send({ error: "Error in getting users info" })
  }
})

notificationsRouter.param("id", notificationsController.findByParam)

notificationsRouter.route("/")
  .get(notificationsController.getAll)
  .post(notificationsController.createOne)

notificationsRouter.route("/:id")
  .get(notificationsController.getOne)
  .put(notificationsController.updateOne)
  .delete(notificationsController.createOne)
