import axios from "axios"
import moment from "moment"
import { Notifications } from "./notification.model"

import config from "../../../config"

const muniApiBaseURL = `http://localhost:${config.port || 4001}`

export const createMessage = (userId, from, sentDate, description, sourceType, sourceId, docIds) => {
  from = from || "system"
  const payload = {userId, from, sentDate, description, sourceType, sourceId, docIds}
  axios.post(`${muniApiBaseURL}/api/messages/`, payload)
    .then(res => console.log("ok in createMessage: ", res.status))
    .catch(err => console.log("err in createMessage : "))
}

export const processNotifications = () => {
  axios.get(`${muniApiBaseURL}/api/notifications/`).then(res => console.log("res : ", res.status, res.data.length))
    .catch(err => console.log("err1 in processNotifications: "))
  Notifications.find({}).then(notifications => {
    notifications.forEach(n => {
      const { _id, userId, modes, description, recurring, meta, lastSent,
        startDate, endDate, frequency: {value, unit }, docIds } = n
      // console.log("last sent : ", lastSent)
      const currDate = new Date()
      if(recurring && modes.includes("app") && (currDate >= startDate && currDate <= endDate)) {
        const nextDate = moment(new Date(lastSent)).add(value, `${unit}s`).toDate()
        // console.log("startDate, lastSent, val, unit, nextDate : ", startDate, lastSent, value, unit, nextDate)
        if(nextDate <= currDate) {
          const sourceType = meta.contextType || "system"
          const sourceId = _id
          const sentDate = new Date(nextDate)
          // console.log("sentDate : ", sentDate)
          createMessage(userId, "", sentDate, description, sourceType, sourceId, docIds)
          // console.log("sentDate : ", sentDate)
          // const payload = { ...n, lastSent: sentDate }
          Notifications.findOneAndUpdate({ _id }, { $set: {lastSent: sentDate } })
            .then(res => console.log("ok in createMessage: ", res._id))
            .catch(err => console.log("err2 in processNotifications : "))
          // axios.put(`${muniApiBaseURL}/api/notifications/${_id}`, payload)
          //   .then(res => console.log("ok in createMessage: ", res.status))
          //   .catch(err => console.log("err : ", err))
        }
      }
    })
  })
}

export const getNotificationDescription = (name, options, meta) => {
  switch(name) {
  case "NOTIFY_COMPLIANCE_CONTROL_ASSIGNED":
  {
    const { contextType, topic } = meta
    const description = `${contextType ?  `${contextType  } ` : ""}notification - ${topic ? `topic: ${topic} ` : ""}`
    return description
  }
  case "NOTIFY_TASK_ASSIGNED":
  {
    const { contextType, contextId, contextName } = meta
    const { taskStartDate, taskEndDate, taskDescription, taskPriority, taskStatus } = options
    const description = `A new taks ${taskDescription} is assigned for ${contextType ?  `${contextType  } ` : ""}${contextId ?  `${contextId  } ` : ""}${contextName ?  `${contextName  } ` : ""}}.
    Start date: ${taskStartDate}, End date: ${taskEndDate}, Priority: ${taskPriority}, Status: ${taskStatus}`
    return description
  }
  default:
    return "No description"
  }
}

export const createNotification = (token, name, userIds, options, meta={}) => {
  if(!name || !userIds || !userIds.length || !options) {
    // console.log("Input missing")
    return
  }
  // console.log("token, name, userIds, options, meta : ", token, name, userIds, options, meta)
  const { modes, recurring, startDate, endDate, frequency, unit, limit,
    docIds } = options
  const lastSent = options.lastSent || (new Date())
  const description = getNotificationDescription(name, options, meta)
  // console.log("lastSent server  : ", lastSent)
  userIds.forEach(userId => {
    const payload = { userId, name, modes, description, recurring, meta,
      startDate, endDate, frequency: {value: frequency, unit }, limit, docIds,
      lastSent }
    axios.post(`${muniApiBaseURL}/notifications/`, payload)
      .then(res => {
        // console.log("res : ", res.status)
        if(!recurring) {
          const sentDate = new Date()
          const sourceType = meta.contextType || "system"
          const sourceId = meta.contextId || ""
          createMessage(userId, "", sentDate, description, sourceType, sourceId, docIds)
        }
      })
      .catch(err => console.log("err : ", err))
  })
}
