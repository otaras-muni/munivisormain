import mongoose from "mongoose"

const { Schema } = mongoose

export const notificationsSchema = new Schema({
  userId: String,
  name: String,
  description: String,
  modes: [String],
  meta: {_id: false},
  recurring: Boolean,
  startDate: Date,
  endDate: Date,
  frequency: {
    _id: false,
    value: Number,
    unit: String
  },
  limit: Number,
  canUserChange: Boolean,
  pausedAt: Date,
  pausedDuration: {
    _id: false,
    value: Number,
    unit: String
  },
  docIds: [String],
  lastSent: Date
}, { strict: false })

notificationsSchema.index({userId:1})

export const Notifications = mongoose.model("notification", notificationsSchema)
