import { generateControllers } from "../../modules/query"
import { Notifications } from "./notification.model"

// export const overrideGetAll = async (req, res, next) => {
//   const { user } = req
//   const { entityId } = user || {}
//   if(!user || !entityId) {
//     return res.status(422).send({ error: "No user or entityId found" })
//   }
//   const filter = { entityId }
//
//   try {
//     const docs = await Controls.find(filter)
//     res.json(docs[0])
//   } catch (error) {
//     next(error)
//   }
// }
//
// const overrides = {
//   getAll: overrideGetAll
// }

export default generateControllers(Notifications)
