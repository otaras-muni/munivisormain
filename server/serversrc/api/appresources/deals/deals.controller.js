import moment from "moment"
import {generateControllers} from "../../modules/query"
import {
  EntityUser,
  Deals,
  RFP,
  ActMaRFP,
  Derivatives,
  BankLoans,
  Others,
  Entity
} from "./../models"
import {canUserPerformAction} from "../../commonDbFunctions"
import {manageTasksForProcessChecklists} from "../taskmanagement/taskProcessInteraction"
import {checkTranEligiblityForLoggedInUser} from "../../commonDbFunctions/getEligibleTransactionsForLoggedInUser"
import {putData, getData, deleteData, elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import logger from "./../../modules/logger"
import {RelatedToAssign} from "../commonservices/services.controller"

const {ObjectID} = require("mongodb")

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Deals, _id: id, esType: "tranagencydeals"})

const postDealTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (entitled) {
      const { dealIssueTranRelatedTo } = req.body
      const newDeal = new Deals({
        ...req.body
      })
      const dealTransaction = await newDeal.save()

      await RelatedToAssign(dealTransaction, dealIssueTranRelatedTo, "Bond Issue", "multi")

      const es = returnFromES(req.user.tenantId, dealTransaction._id)
      // const dataFromES = await getData("tranagencydeals", dealTransaction._id)
      // console.log("Validating whether the data is getting saved in ES", dataFromES)
      await Promise.all([
        updateEligibleIdsForLoggedInUserFirm(req.user),
        es
      ])
      res.json(dealTransaction)
    } else {
      res
        .status(500)
        .send({done: false, error: "User not entitled to create transaction"})
    }
  } catch (error) {
    res
      .status(500)
      .send({done: false, error: "Error saving information on Deals"})
  }
}

const getDealTransactionDetails = () => async(req, res) => {
  const {type, dealId} = req.params
  const resRequested = [
    {
      resource: "Deals",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    console.log("------CHECKING THE ENTITLEMENTS THAT ARE BEING RRETURNED FROM SERVICE", {entitled, isEligible})

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    if (type === "summary") {
      select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranClientId dealIssueTra" +
          "nIssuerId dealIssueTranType dealIssueTranName dealIssueTranIssuerFirmName rfpTra" +
          "nIsConduitBorrowerFlag opportunity dealIssueTranIssueName dealIssueTranProjectDe" +
          "scription dealIssueTranSubType dealIssueGuarantorName dealIssueParticipants deal" +
          "IssueParAmount dealIssueTransNotes dealIssueBorrowerName dealIssueSecurityType d" +
          "ealIssueTranRelatedTo tranNotes"
    }
    if (type === "details") {
      select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranIssuerFirmName dealIs" +
          "sueTranClientId dealIssueTranIssuerId dealIssueTranType dealIssueTranSubType OnB" +
          "oardingDataMigrationHistorical historicalData dealIssueParticipants dealIssueTra" +
          "nName dealIssueTranPurposeOfRequest dealIssueTranOtherTransactionType dealIssueT" +
          "ranState dealIssueTranCounty dealIssueTranPrimarySector dealIssueTranRelatedTo d" +
          "ealIssueTranSecondarySector dealIssueTranDateHired dealIssueTranStartDate dealIs" +
          "sueTranExpectedEndDate dealIssueTransNotes dealIssueCostOfIssuanceNotes created_" +
          "at dealIssueBorrowerName dealIssueGuarantorName dealIssueofferingType dealIssueS" +
          "ecurityType dealIssueBankQualified dealIssueCorpType dealIssueParAmount termsOfI" +
          "ssue tic bbri nic gsp mgtf exp tkdn other dealIssuePricingDate dealIssueExpAwardDate d" +
          "ealIssueActAwardDate dealIssueSdlcCreditPerc dealIssueEstimatedRev dealIssueEsti" +
          "matedRev dealIssueUseOfProceeds dealIssuePlaceholder dealIssueTranIssueName deal" +
          "IssueTranProjectDescription dealMigration tranNotes"
    }
    if (type === "participants") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranAssignedTo dealIssueTr" +
          "anIssuerFirmName dealIssueTranClientFirmName dealIssueUnderwriters dealIssueTran" +
          "Type dealIssueTranSubType dealIssueParticipants dealIssueTranSubType dealIssueTr" +
          "anIssueName dealIssueTranProjectDescription"
    }
    if (type === "documents") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranClientFirmName dealIss" +
          "ueTranType dealIssueTranSubType dealIssueParticipants dealIssueDocuments dealIss" +
          "ueTranClientId dealIssueTranSubType dealIssueTranIssuerFirmName dealIssueTranPro" +
          "jectDescription"
    }
    if (type === "rating") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranType dealIssueTranSubT" +
          "ype dealIssueSeriesDetails.seriesRatings dealIssueSeriesDetails.cepRatings dealI" +
          "ssueParticipants dealIssueTranIssueName dealIssueTranProjectDescription"
    }
    if (type === "contract") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueTradingInformation dealIssu" +
          "eTranSubType dealIssueParticipants dealIssueContractInformation dealIssueReceive" +
          "Leg dealIssuePayLeg dealIssueCompliance dealIssueTranIssueName dealIssueTranProj" +
          "ectDescription"
    }
    if (type === "pricing") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueTranSubType dealIssueSeries" +
          "Details._id dealIssueSeriesDetails.seriesName dealIssueSeriesDetails.description" +
          " dealIssueSeriesDetails.tag dealIssueParticipants dealIssueTranProjectDescriptio" +
          "n dealIssueTranIssueName created_at dealIssueSecurityType "
    }
    if (type === "series" || type === "ratings") {
      select = "dealIssueTranClientId dealIssueTranSubType dealIssueSeriesDetails.seriesRatings " +
          "dealIssueSeriesDetails.cepRatings dealIssueSeriesDetails._id dealIssueSeriesDeta" +
          "ils.seriesName dealIssueSeriesDetails.description dealIssueSeriesDetails.tag dea" +
          "lIssueParticipants dealIssueTranProjectDescription dealIssueTranIssueName"
    }
    if (type === "check-track") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueCostOfIssuance dealIssueTra" +
          "nSubType dealIssueScheduleOfEvents dealIssueCostOfIssuanceNotes dealIssuePartici" +
          "pants dealIssueTranProjectDescription dealIssueTranIssueName dealChecklists"
    }
    if (type === "audit-trail") {
      select = "dealIssueTranClientId dealIssueTranIssuerId"
    }
    const deals = await Deals
      .findById({_id: dealId})
      .select(`${select} dealIssueTranStatus isWon`)
      .populate([
        {
          path: "dealIssueTranAssignedTo",
          model: EntityUser,
          select: "userFirstName userLastName userFirmName entityId userPhone userEmails userAddres" +
              "ses"
        }, {
          path: "dealIssueTranIssuerId",
          model: Entity,
          select: "addresses"
        }
      ])

    let dealTran = {}
    if (deals && deals.dealIssueTranIssuerId && deals.dealIssueTranIssuerId.addresses) {
      let address = deals
        .dealIssueTranIssuerId
        .addresses
        .find(addr => addr.isPrimary)
      address = address || deals.dealIssueTranIssuerId.addresses[0]
      dealTran = {
        ...deals._doc,
        entityAddress: address || {},
        dealIssueTranIssuerId: deals.dealIssueTranIssuerId._id
      }
    }

    // deals.entityAddress = deals.dealIssueTranClientId.addresses
    // deals.dealIssueTranClientId = deals.dealIssueTranClientId._id

    res.json(select
      ? dealTran
      : {})
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateDealTransaction = () => async(req, res) => {
  const {type, dealId} = req.params
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    if (type === "details" || type === "summary") {
      select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranIssuerId dealIssueTra" +
          "nType dealIssueTranName dealIssueTranPurposeOfRequest dealIssueTranOtherTransact" +
          "ionType dealIssueTranState dealIssueTranCounty dealIssueTranPrimarySector dealIs" +
          "sueTranSecondarySector dealIssueTranDateHired dealIssueTranStartDate dealIssueTr" +
          "anExpectedEndDate dealIssueTransNotes dealIssueCostOfIssuanceNotes created_at de" +
          "alIssueBorrowerName dealIssueGuarantorName dealIssueTranStatus dealIssueoffering" +
          "Type dealIssueSecurityType dealIssueBankQualified dealIssueCorpType dealIssuePar" +
          "Amount dealIssuePricingDate dealIssueExpAwardDate dealIssueActAwardDate dealIssu" +
          "eSdlcCreditPerc dealIssueEstimatedRev dealIssueEstimatedRev dealIssueUseOfProcee" +
          "ds dealIssuePlaceholder termsOfIssue tic bbri nic gsp other mgtf exp tkdn"
      await Deals.findByIdAndUpdate({
        _id: dealId
      }, req.body)
    }
    if (type === "documents") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranType dealIssueDocument" +
          "s"
      await Deals.update({
        _id: dealId
      }, {
        $addToSet: {
          dealIssueDocuments: req.body.dealIssueDocuments
        }
      })
    }
    if (type === "contract") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueTradingInformation dealIssu" +
          "eContractInformation dealIssueReceiveLeg dealIssuePayLeg dealIssueCompliance"
      await Deals.update({
        _id: dealId
      }, req.body)
    }
    if (type === "series") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueSeriesDetails.seriesName de" +
          "alIssueSeriesDetails.seriesPricingDetails dealIssueSeriesDetails.seriesPricingDa" +
          "ta dealIssueSeriesDetails.description dealIssueSeriesDetails.tag"
      await Deals.update({
        _id: dealId
      }, {
        $addToSet: {
          dealIssueSeriesDetails: req.body.dealIssueSeriesDetails
        }
      })
    }
    if (type === "check-track") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealChecklists"
      await Deals.update({
        _id: dealId
      }, req.body)
      await manageTasksForProcessChecklists(dealId, req.user)
    }
    const deals = await Deals
      .findOne({_id: dealId})
      .select(select)
    await returnFromES(req.user.tenantId, dealId)
    res.json(select
      ? deals
      : {})
  } catch (error) {
    console.log("*********Deal********updateDealTransaction**************", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const removeParticipantByPartId = () => async(req, res) => {
  const {type, dealId} = req.params
  const {removeFrom} = req.query
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (removeFrom === "dealIssueParticipants") {
      await Deals.update({
        _id: dealId
      }, {
        $pull: {
          dealIssueParticipants: {
            _id: type
          }
        }
      })
      logger.info("Successfully deleted the participants from the transaction")
      logger.info("Successfully updated the eligible participants from the transactions")
    }
    if (removeFrom === "dealIssueUnderwriters") {
      await Deals.update({
        _id: dealId
      }, {
        $pull: {
          dealIssueUnderwriters: {
            _id: type
          }
        }
      })
    }
    const es = returnFromES(req.user.tenantId, dealId)
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const deal = Deals
      .findOne({_id: dealId})
      .select(`${removeFrom}`)
    const [dealDAta] = await Promise.all([deal, es, ent])
    logger.info("Removed Participants successfully")
    res.json(removeFrom
      ? dealDAta
      : "Not found")
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getDealRatings = () => async(req, res) => {
  const {contactId, dealId} = req.params
  const resRequested = [
    {
      resource: "Deals",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let deals = await Deals.aggregate([
      {
        $match: {
          _id: ObjectID(dealId)
        }
      }, {
        $unwind: "$dealIssueSeriesDetails"
      }, {
        $match: {
          "dealIssueSeriesDetails.contactId": ObjectID(contactId)
        }
      }, {
        $project: {
          dealIssueTranClientId: 1,
          "dealIssueSeriesDetails.contactId": 1,
          "dealIssueSeriesDetails.seriesRatings": 1,
          "dealIssueSeriesDetails.cepRatings": 1
        }
      }
    ])
    deals = Array.isArray(deals) && deals.length
      ? deals[0]
      : deals
    res.json(deals)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const removeDealRatingByRatingId = () => async(req, res) => {
  const {contactId, dealId} = req.params
  const {ratingId} = req.query
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  const entitled = await canUserPerformAction(req.user, resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if (!entitled || (isEligible && !isEligible.canEditTran)) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  await Deals
    .findOne({_id: dealId})
    .then(async dealRating => {
      const findIndex = dealRating
        .dealIssueSeriesDetails
        .findIndex(p => p.contactId.toString() === contactId)
      if (findIndex !== -1) {
        dealRating.dealIssueSeriesDetails[findIndex].seriesRatings = dealRating
          .dealIssueSeriesDetails[findIndex]
          .seriesRatings
          .filter(x => x._id.toString() !== ratingId)
        dealRating.dealIssueSeriesDetails[findIndex].cepRatings = dealRating
          .dealIssueSeriesDetails[findIndex]
          .cepRatings
          .filter(x => x._id.toString() !== ratingId)
        await dealRating.save()
        await Promise.all([
          returnFromES(req.user.tenantId, dealId)
        ])
        res.json(dealRating)
      } else {
        res.json("contactId not found")
      }
    })
    .catch(error => {
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const getDealSeriesBySeriesId = () => async(req, res) => {
  const {dealId, seriesId} = req.params
  const resRequested = [
    {
      resource: "Deals",
      access: 1
    }
  ]
  try {
    /* const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(
      req.user,
      dealId
    ) */
    const [entitled,
      isEligible] = await Promise.all([
      canUserPerformAction(req.user, resRequested),
      checkTranEligiblityForLoggedInUser(req.user, dealId)
    ])

    if (!entitled || (isEligible && !isEligible.canViewTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let deals = await Deals.aggregate([
      {
        $match: {
          _id: ObjectID(dealId)
        }
      }, {
        $unwind: "$dealIssueSeriesDetails"
      }, {
        $match: {
          "dealIssueSeriesDetails._id": ObjectID(seriesId)
        }
      }, {
        $project: {
          "dealIssueSeriesDetails._id": 1,
          "dealIssueSeriesDetails.seriesName": 1,
          "dealIssueSeriesDetails.seriesPricingDetails": 1,
          "dealIssueSeriesDetails.seriesPricingData": 1,
          "dealIssueSeriesDetails.description": 1,
          "dealIssueSeriesDetails.tag": 1
        }
      }
    ])
    deals = Array.isArray(deals) && deals.length
      ? deals[0]
      : deals
    res.json(deals)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveDealsPricingBySeriesId = () => async(req, res) => {
  const {seriesId, dealId} = req.params
  const {seriesPricingData, seriesPricingDetails} = req.body
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  const entitled = await canUserPerformAction(req.user, resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if (!entitled || (isEligible && !isEligible.canEditTran)) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  return Deals
    .findOne({_id: dealId})
    .then(async dealRating => {
      const findIndex = dealRating
        .dealIssueSeriesDetails
        .findIndex(p => p._id.toString() === seriesId)
      if (findIndex !== -1) {
        dealRating.dealIssueSeriesDetails[findIndex].seriesPricingDetails = seriesPricingDetails
        dealRating.dealIssueSeriesDetails[findIndex].seriesPricingData = seriesPricingData
        await dealRating.save()
        // await returnFromES(req.user.tenantId,dealId)
        // manageTasksForProcessChecklists(dealId, req.user)
        await Promise.all([
          returnFromES(req.user.tenantId, dealId)
        ])
        res.json(dealRating.dealIssueSeriesDetails[findIndex])
      } else {
        res
          .status(400)
          .json("series not found")
      }
    })
    .catch(error => {
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const delSeriesBySeriesId = () => async(req, res) => {
  const {seriesId, dealId} = req.params
  const resRequested = [
    {
      resource: "Deals",
      access: 1
    }
  ]
  const entitled = await canUserPerformAction(req.user, resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if (!entitled || (isEligible && !isEligible.canEditTran)) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  await Deals
    .findOne({_id: dealId})
    .then(async dealSeries => {
      dealSeries.dealIssueSeriesDetails = dealSeries
        .dealIssueSeriesDetails
        .filter(p => p._id.toString() !== seriesId)
      await dealSeries.save()
      await Promise.all([
        returnFromES(req.user.tenantId, dealId)
      ])
      res.json(dealSeries.dealIssueSeriesDetails)
    })
    .catch(error => {
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const saveDealSeriesBySeriesId = () => async(req, res) => {
  const {seriesId, dealId} = req.params
  const {seriesName, tag, description} = req.body
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  const entitled = await canUserPerformAction(req.user, resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if (!entitled || (isEligible && !isEligible.canEditTran)) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  return Deals
    .findOne({_id: dealId})
    .then(async dealSeries => {
      const findIndex = dealSeries
        .dealIssueSeriesDetails
        .findIndex(p => p._id.toString() === seriesId)
      if (findIndex !== -1) {
        dealSeries.dealIssueSeriesDetails[findIndex].seriesName = seriesName
        dealSeries.dealIssueSeriesDetails[findIndex].tag = tag
        dealSeries.dealIssueSeriesDetails[findIndex].description = description
        await dealSeries.save()
        // await returnFromES(req.user.tenantId,dealId)
        // manageTasksForProcessChecklists(dealId, req.user)
        await Promise.all([
          returnFromES(req.user.tenantId, dealId)
        ])
        res.json(dealSeries.dealIssueSeriesDetails[findIndex])
      } else {
        res
          .status(400)
          .json("series not found")
      }
    })
    .catch(error => {
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const saveDealParticipantsbyPutId = () => async(req, res) => {
  const {type, dealId} = req.params
  const {_id} = req.body
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (type === "underwriters") {
      if (_id) {
        await Deals.updateOne({
          _id: dealId,
          "dealIssueUnderwriters._id": ObjectID(_id)
        }, {
          $set: {
            "dealIssueUnderwriters.$": req.body
          }
        })
      } else {
        await Deals.update({
          _id: dealId
        }, {
          $addToSet: {
            dealIssueUnderwriters: req.body
          }
        })
      }
      // const dealsUnderWriter = await Deals.findById({ _id: dealId }).select(
      // "dealIssueUnderwriters" )
      const [dealsUnderWriter] = await Promise.all([
        Deals
          .findById({_id: dealId})
          .select("dealIssueUnderwriters"),
        updateEligibleIdsForLoggedInUserFirm(req.user)
      ])
      return res.json(dealsUnderWriter)
    }
    if (type === "participants") {
      const isExists = await Deals.findOne({
        _id: ObjectID(dealId)
      }, {
        dealIssueParticipants: {
          $elemMatch: {
            dealPartContactId: ObjectID(req.body.dealPartContactId),
            dealPartFirmId: ObjectID(req.body.dealPartFirmId),
            dealPartType: req.body.dealPartType
          }
        }
      })
      if (isExists && isExists.dealIssueParticipants && isExists.dealIssueParticipants.length) {
        if (isExists.dealIssueParticipants[0]._id.toString() !== (_id && _id.toString())) {
          return res
            .status(422)
            .json({error: "This user was already exists"})
        }
      }
      if (_id) {
        await Deals.updateOne({
          _id: dealId,
          "dealIssueParticipants._id": ObjectID(_id)
        }, {
          $set: {
            "dealIssueParticipants.$": req.body
          }
        })
        const data = await putData("tranagencydeals", dealId, req.body)
      } else {
        await Deals.update({
          _id: dealId
        }, {
          $addToSet: {
            dealIssueParticipants: req.body
          }
        })
        const data = await putData("tranagencydeals", dealId, req.body)
      }
      const dealsPart = await Deals
        .findById({_id: dealId})
        .select("dealIssueParticipants")
      logger.info("Updated Tasks for Checklists")
      await Promise.all([
        updateEligibleIdsForLoggedInUserFirm(req.user),
        returnFromES(req.user.tenantId, dealId)
      ])
      logger.info("Updated Eligible IDs for all the users attached to the tenant")
      return res.json(dealsPart)
    }

    res
      .status(404)
      .json("not found")
  } catch (error) {
    console.log("============", error)
    logger.error(`There is an error - ${JSON.stringify(error, null, 2)}`)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveDealsRatingsByDealId = () => async(req, res) => {
  const {type, dealId} = req.params
  const {oldSeriesId} = req.query
  const {_id, seriesId} = req.body
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let srIndex,
      ratingIndex
    const rating = await Deals
      .findOne({
      _id: dealId
      /* "dealIssueSeriesDetails.seriesName": seriesName,
       "dealIssueSeriesDetails.seriesRatings._id": _id */
    })
      .select("dealIssueSeriesDetails")
    if (_id) {
      if (type === "dealAgencyRatings") {
        srIndex = rating
          .dealIssueSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        ratingIndex = rating
          .dealIssueSeriesDetails[srIndex]
          .seriesRatings
          .findIndex(x => x._id.toString() === _id)

        if(!rating.dealIssueSeriesDetails[srIndex].seriesRatings[ratingIndex]){
          const existSrIndex = rating
            .dealIssueSeriesDetails
            .findIndex(x => x._id.toString() === oldSeriesId)
          const existRatingIndex = rating
            .dealIssueSeriesDetails[srIndex]
            .seriesRatings
            .findIndex(x => x._id.toString() === _id)
          rating.dealIssueSeriesDetails[existSrIndex].seriesRatings.splice(existRatingIndex, 1)
          delete req.body._id

          rating.dealIssueSeriesDetails[srIndex].seriesRatings.push(req.body)
        }else {
          rating.dealIssueSeriesDetails[srIndex].seriesRatings[ratingIndex] = Object.assign({}, rating.dealIssueSeriesDetails[srIndex].seriesRatings[ratingIndex], req.body)
        }
      } else {
        srIndex = rating
          .dealIssueSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        ratingIndex = rating
          .dealIssueSeriesDetails[srIndex]
          .cepRatings
          .findIndex(x => x._id.toString() === _id)
        // rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex] = Object.assign({}, rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex], req.body)
        if(!rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex]){
          const existSrIndex = rating
            .dealIssueSeriesDetails
            .findIndex(x => x._id.toString() === oldSeriesId)
          const existRatingIndex = rating
            .dealIssueSeriesDetails[existSrIndex]
            .cepRatings
            .findIndex(x => x._id.toString() === _id)
          rating.dealIssueSeriesDetails[existSrIndex].cepRatings.splice(existRatingIndex, 1)
          delete req.body._id

          rating.dealIssueSeriesDetails[srIndex].cepRatings.push(req.body)
        }else {
          rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex] = Object.assign({}, rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex], req.body)
        }
      }
      await rating.save()
      const es = returnFromES(req.user.tenantId, dealId)
      const saveRating = Deals
        .findById({_id: dealId})
        .select("dealIssueSeriesDetails")
      const [documentAfterRatingSave] = await Promise.all([saveRating, es])
      res.json(documentAfterRatingSave.dealIssueSeriesDetails)
    } else {
      if (type === "dealAgencyRatings") {
        srIndex = rating
          .dealIssueSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        rating
          .dealIssueSeriesDetails[srIndex]
          .seriesRatings
          .push(req.body)
      } else {
        srIndex = rating
          .dealIssueSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        rating
          .dealIssueSeriesDetails[srIndex]
          .cepRatings
          .push(req.body)
      }
      await rating.save()
      const es = returnFromES(req.user.tenantId, dealId)
      const saveRating = Deals
        .findById({_id: dealId})
        .select("dealIssueSeriesDetails")
      const [documentAfterRatingSave] = await Promise.all([saveRating, es])
      res.json(documentAfterRatingSave.dealIssueSeriesDetails)
    }
  } catch (error) {
    console.log("================", error.message)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const delDealsRatingByRatingId = () => async(req, res) => {
  const {type, dealId} = req.params
  const {seriesId, ratingId} = req.query
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (type === "dealAgencyRatings") {
      await Deals.update({
        _id: ObjectID(dealId),
        "dealIssueSeriesDetails._id": ObjectID(seriesId)
      }, {
        $pull: {
          "dealIssueSeriesDetails.$.seriesRatings": {
            _id: ObjectID(ratingId)
          }
        }
      })
      const data = await deleteData("tranagencydeals", dealId, ratingId)
    } else {
      await Deals.update({
        _id: ObjectID(dealId),
        "dealIssueSeriesDetails._id": ObjectID(seriesId)
      }, {
        $pull: {
          "dealIssueSeriesDetails.$.cepRatings": {
            _id: ObjectID(ratingId)
          }
        }
      })
      const data = await deleteData("tranagencydeals", dealId, ratingId)
    }
    const rating = Deals
      .findById({_id: dealId})
      .select("dealIssueSeriesDetails")
    const es = returnFromES(req.user.tenantId, dealId)
    const [ratingData] = await Promise.all([rating, es])
    res.json(ratingData.dealIssueSeriesDetails)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const delDealsCheckInTrack = () => async(req, res) => {
  const {dealId} = req.params
  const {type, trackId, category} = req.query
  const resRequested = [
    {
      resource: "Deals",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let deals = {}
    if (category === "scheduleOfEvents") {
      deals = await Deals.update({
        _id: dealId,
        "dealIssueScheduleOfEvents.soeCategoryName": type
      }, {
        $pull: {
          "dealIssueScheduleOfEvents.$.soeItems": {
            _id: ObjectID(trackId)
          }
        }
      })
    } else {
      deals = await Deals.update({
        _id: dealId,
        "dealIssueCostOfIssuance.costOfIssCategory": type
      }, {
        $pull: {
          "dealIssueCostOfIssuance.$.costItemDetails": {
            _id: ObjectID(trackId)
          }
        }
      })
    }
    await Promise.all([
      returnFromES(req.user.tenantId, dealId)
    ])
    res
      .status(200)
      .json(deals)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putRelatedTran = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  const {dealId} = req.params
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)
    const select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranClientId dealIssueTra" +
        "nIssuerId dealIssueTranType dealIssueTranName dealIssueTranIssuerFirmName rfpTra" +
        "nIsConduitBorrowerFlag dealIssueTranIssueName dealIssueTranProjectDescription de" +
        "alIssueTranSubType dealIssueParticipants dealIssueTranStatus dealIssueGuarantorN" +
        "ame dealIssueBorrowerName dealIssueSecurityType dealIssueTranRelatedTo"

    if (!entitled || (isEligible && !isEligible.canEditTran)) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await Deals.update({
      _id: dealId
    }, {
      $addToSet: {
        dealIssueTranRelatedTo: req.body
      }
    })

    const dealDetails = await Deals.findById(dealId).select("dealIssueTranIssueName dealIssueTranProjectDescription dealIssueTranIssuerId dealIssueTranIssuerFirmName")
    await RelatedToAssign(dealDetails, req.body, "Bond Issue", "single")

    const [dealData,
      t,
      es] = await Promise.all([
      Deals
        .findById({_id: dealId})
        .select(select),
      returnFromES(req.user.tenantId, dealId)
    ])
    res.json(dealData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export const putTransactionDocStatus = () => async(req, res) => {
  const {tranId} = req.params
  const {tranType, type} = req.query
  const {
    _id,
    docStatus,
    docCategory,
    docSubCategory,
    docType,
    settings
  } = req.body
  try {
    const tranTypes = [
      "Deals",
      "RFP",
      "ActMaRFP",
      "Derivatives",
      "BankLoans",
      "Others"
    ]

    if (tranTypes.indexOf(tranType) !== -1 && tranId && _id) {
      const key = tranType === "Deals"
        ? "dealIssueDocuments"
        : tranType === "ActMaRFP"
          ? "maRfpDocuments"
          : tranType === "Derivatives"
            ? "derivativeDocuments"
            : tranType === "BankLoans"
              ? "bankLoanDocuments"
              : tranType === "RFP"
                ? "rfpBidDocuments"
                : tranType === "Others"
                  ? "actTranDocuments"
                  : ""

      const query1 = `${key}._id`

      const modal = tranType === "Deals"
        ? Deals
        : tranType === "ActMaRFP"
          ? ActMaRFP
          : tranType === "Derivatives"
            ? Derivatives
            : tranType === "BankLoans"
              ? BankLoans
              : tranType === "RFP"
                ? RFP
                : tranType === "Others"
                  ? Others
                  : ""

      let query2 = ""
      if (type === "status") {

        query2 = {
          [`${key}.$.docStatus`]: docStatus
        }
        if (settings) {
          query2[`${key}.$.settings`] = settings
        }
        await modal.updateOne({
          _id: ObjectID(tranId),
          [query1]: ObjectID(_id)
        }, {$set: query2})
      } else {
        query2 = {
          [`${key}.$.docCategory`]: docCategory,
          [`${key}.$.docSubCategory`]: docSubCategory,
          [`${key}.$.docType`]: docType
        }
        req.body.LastUpdatedDate = moment(new Date()).format("MM/DD/YYYY hh:mm A")
        query2 = `${key}.$`
        await modal.updateOne({
          _id: ObjectID(tranId),
          [query1]: ObjectID(_id)
        }, {
          $set: {
            [query2]: req.body
          }
        })
      }
      const [dealData] = await Promise.all([
        modal
          .findById(tranId)
          .select(key),
        returnFromES(req.user.tenantId, tranId)
      ])

      res
        .status(200)
        .json({
          done: true,
          document: (dealData && dealData[key]) || []
        })
    } else {
      res
        .status(422)
        .send({done: false, error: "This type type of transaction doesn't exists."})
    }
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const pullTransactionDoc = () => async(req, res) => {
  const {tranId} = req.params
  const {tranType, docId} = req.query
  try {
    const tranTypes = [
      "Deals",
      "RFP",
      "ActMaRFP",
      "Derivatives",
      "BankLoans",
      "Others"
    ]

    if (tranTypes.indexOf(tranType) !== -1 && tranId && docId) {
      const key = tranType === "Deals"
        ? "dealIssueDocuments"
        : tranType === "ActMaRFP"
          ? "maRfpDocuments"
          : tranType === "Derivatives"
            ? "derivativeDocuments"
            : tranType === "BankLoans"
              ? "bankLoanDocuments"
              : tranType === "RFP"
                ? "rfpBidDocuments"
                : tranType === "Others"
                  ? "actTranDocuments"
                  : ""

      const modal = tranType === "Deals"
        ? Deals
        : tranType === "ActMaRFP"
          ? ActMaRFP
          : tranType === "Derivatives"
            ? Derivatives
            : tranType === "BankLoans"
              ? BankLoans
              : tranType === "RFP"
                ? RFP
                : tranType === "Others"
                  ? Others
                  : ""

      const [document,
        es,
        t] = await Promise.all([
        modal
          .findById(tranId)
          .select(key),
        returnFromES(req.user.tenantId, tranId)
      ])
      res
        .status(200)
        .json(document)
    } else {
      res
        .status(422)
        .send({done: false, error: "This Document doesn't exists."})
    }
  } catch (error) {
    console.log("===========================", error.message)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const postUnderwriterCheckAddToDL = () => async(req, res) => {
  const {type, dealId} = req.params
  try {
    const {addToDL} = req.body

    await Deals.updateOne({
      _id: ObjectID(dealId),
      "dealIssueUnderwriters._id": ObjectID(type)
    }, {
      $set: {
        "dealIssueUnderwriters.$.dealPartContactAddToDL": addToDL
      }
    })

    const underwriter = await Deals
      .findOne({_id: dealId})
      .select("dealIssueUnderwriters")
    let updateUnderwriter = null

    if (underwriter) {
      updateUnderwriter = underwriter
        .dealIssueUnderwriters
        .find(p => p._id.toString() === type)
    }

    await returnFromES(req.user.tenantId, dealId)
    res
      .status(200)
      .json({done: true, updateUnderwriter})
  } catch (error) {
    console.log("===========================", error.message)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

const postDealsNotes = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Deals",
      access: 2
    }
  ]
  const {tranId} = req.params
  const {_id} = req.body
  try {

    if (_id) {
      await Deals.updateOne({
        _id: ObjectID(tranId),
        "tranNotes._id": ObjectID(_id)
      }, {
        $set: {
          "tranNotes.$": req.body
        }
      })
    } else {
      await Deals.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          tranNotes: req.body
        }
      })
    }
    const deals = await Deals.findOne({_id: tranId}).select("tranNotes")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    await Promise.all([deals, ent, es])
    res.json(deals)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export default generateControllers(Deals, {
  getDealTransactionDetails: getDealTransactionDetails(),
  updateDealTransaction: updateDealTransaction(),
  removeParticipantByPartId: removeParticipantByPartId(),
  getDealRatings: getDealRatings(),
  saveDealsRatingsByDealId: saveDealsRatingsByDealId(),
  removeDealRatingByRatingId: removeDealRatingByRatingId(),
  getDealSeriesBySeriesId: getDealSeriesBySeriesId(),
  saveDealsPricingBySeriesId: saveDealsPricingBySeriesId(),
  delSeriesBySeriesId: delSeriesBySeriesId(),
  saveDealSeriesBySeriesId: saveDealSeriesBySeriesId(),
  saveDealParticipantsbyPutId: saveDealParticipantsbyPutId(),
  delDealsRatingByRatingId: delDealsRatingByRatingId(),
  delDealsCheckInTrack: delDealsCheckInTrack(),
  putRelatedTran: putRelatedTran(),
  putTransactionDocStatus: putTransactionDocStatus(),
  pullTransactionDoc: pullTransactionDoc(),
  postDealTransaction: postDealTransaction(),
  postUnderwriterCheckAddToDL: postUnderwriterCheckAddToDL(),
  postDealsNotes: postDealsNotes()
})
