import express from "express"

import dealsController from "./deals.controller"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

import {
  createDealsErrorHandling,
  dealsErrorHandling,
  pricingErrorHandling,
} from "./validation"
import {checkClosedTransaction} from "../commonservices/services.controller"


export const dealsRouter = express.Router()

dealsRouter.param("id", dealsController.findByParam)

dealsRouter.route("/")
  .get(requireAuth, dealsController.getAll)
  .post(createDealsErrorHandling, requireAuth, dealsController.postDealTransaction)

dealsRouter.route("/:id")
  .get(requireAuth, dealsController.getOne)
  .put(requireAuth, checkClosedTransaction, dealsController.updateOne)
  .delete(requireAuth, dealsController.deleteOne)

dealsRouter.route("/related/:dealId")
  .put(requireAuth, checkClosedTransaction, dealsController.putRelatedTran)

dealsRouter.route("/transaction/document/:tranId")
  .put(requireAuth, checkClosedTransaction, dealsController.putTransactionDocStatus)
  .delete(requireAuth, checkClosedTransaction, dealsController.pullTransactionDoc)

dealsRouter.route("/transaction/:type/:dealId")
  .get(requireAuth, dealsController.getDealTransactionDetails)
  .put(dealsErrorHandling,requireAuth, checkClosedTransaction, dealsController.updateDealTransaction)
  .delete(requireAuth, checkClosedTransaction, dealsController.removeParticipantByPartId)

dealsRouter.route("/rating/:type/:dealId")
  // .get(dealsController.getDealRatings)
  .put(requireAuth, checkClosedTransaction, dealsController.saveDealsRatingsByDealId)
  .delete(requireAuth, checkClosedTransaction, dealsController.delDealsRatingByRatingId)

dealsRouter.route("/pricing/:seriesId/:dealId")
  .get(requireAuth, dealsController.getDealSeriesBySeriesId)
  .put(pricingErrorHandling, requireAuth, checkClosedTransaction, dealsController.saveDealsPricingBySeriesId)

dealsRouter.route("/series/:seriesId/:dealId")
  .put(requireAuth, checkClosedTransaction, dealsController.saveDealSeriesBySeriesId)
  .delete(requireAuth, checkClosedTransaction, dealsController.delSeriesBySeriesId)

dealsRouter.route("/participants/:type/:dealId")
  .post(requireAuth, checkClosedTransaction, dealsController.postUnderwriterCheckAddToDL)
  .put(dealsErrorHandling, requireAuth, checkClosedTransaction, dealsController.saveDealParticipantsbyPutId)

dealsRouter.route("/checkInTrack/:dealId")
  .delete(requireAuth, checkClosedTransaction, dealsController.delDealsCheckInTrack)

dealsRouter.route("/notes/:tranId")
  .post(requireAuth, checkClosedTransaction, dealsController.postDealsNotes)
