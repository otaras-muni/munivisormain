import Joi from "joi-browser"

const contractRefSchema = Joi.object().keys({
  contractId: Joi.string().allow("").optional(),
  contractName: Joi.string().allow("").optional(),
  contractType: Joi.string().allow("").optional(),
  endDate: Joi.date().example(new Date("2016-01-01")).optional(),
  startDate: Joi.date().example(new Date("2016-01-01")).optional(),
  _id: Joi.string().allow("").optional()
})

const contractsSchema = Joi.object().keys({
  contractRef: contractRefSchema,
  digitize: Joi.boolean().optional(),
  nonTranFees: Joi.array().optional(),
  retAndEsc: Joi.array().optional(),
  sof: Joi.array().optional(),
  agree: Joi.string().allow("").optional(),
  reviewed: Joi.boolean().optional(),
  _id: Joi.string().allow("").optional()
})

const tranNotesSchema = Joi.object().keys({
  note: Joi.string().allow("").optional(),
  createdAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedByName: Joi.string().allow("").optional(),
  updatedById: Joi.string().allow("").optional(),
  _id: Joi.string().allow("").optional()
})

const tranDealsCreateSchema = Joi.object().keys({
  dealIssueTranName: Joi.string().required().optional(),
  dealIssueTranType: Joi.string().required().optional(),
  dealIssueTranSubType: Joi.string().required().optional(),
  dealIssueTranClientId: Joi.string().required().optional(),
  dealIssueTranClientFirmName: Joi.string().required().optional(),
  dealIssueTranIssuerId: Joi.string().required().optional(),
  dealIssueTranIssuerFirmName: Joi.string().required().optional(),
  dealIssueTranIssuerMSRBType: Joi.string().required().optional(),
  dealIssueTranFirmLeadAdvisorId: Joi.string().allow("").optional(),
  dealIssueTranFirmLeadAdvisorName: Joi.string().allow("").optional(),
  dealIssueTranIsClientMsrbRegistered: Joi.boolean().required().optional(),
  dealIssueTranIsConduitBorrowerFlag: Joi.string().optional(),
  dealIssueTranState: Joi.string().allow("").optional(),
  dealIssueTranCounty: Joi.string().allow("").optional(),
  dealIssueTranIssueName: Joi.string().allow("").optional(),
  dealIssueTranRelatedTo: Joi.array().optional(),
  dealIssueTranPrimarySector: Joi.string().allow("").optional(),
  dealIssueTranSecondarySector: Joi.string().allow("").optional(),
  dealIssueTransNotes: Joi.string().allow("").optional(),
  tranNotes: tranNotesSchema,
  dealIssueTranProjectDescription: Joi.string().allow("").optional(),
  dealIssueTranAssignedTo: Joi.array().min(1).optional(),
  dealIssueTranPurposeOfRequest: Joi.string().allow("").optional(),
  dealIssueTranStatus: Joi.string().required().optional(),
  createdByUser: Joi.string().required(),
  clientActive: Joi.boolean().required().optional(),
  dealIssueParticipants: Joi.array().min(1).required().optional(),
  contracts: contractsSchema,
  opportunity: Joi.object().optional()
})

const transDetailSchema = Joi.object().keys({
  dealIssueTranIssueName: Joi.string().allow(["", null]).optional(),
  dealIssueTranProjectDescription: Joi.string().required().optional(),
  dealIssueTranIssuerId: Joi.string().optional(),
  dealIssueTranClientId: Joi.string().optional(),
  dealIssueTranName: Joi.string().allow("").optional(),
  dealIssueTranType: Joi.string().allow("").optional(),
  dealIssueTranSubType: Joi.string().allow("").optional(),
  dealIssueTranStatus: Joi.string().allow("").optional(),
  dealIssueTranState: Joi.string().allow("").optional(),
  dealIssueTranClientFirmName: Joi.string().allow("").optional(),
  dealIssueTranClientMSRBType: Joi.string().allow("").optional(),
  dealIssueTranIssuerFirmName: Joi.string().allow("").optional(),
  dealIssueTranIssuerMSRBType: Joi.string().allow("").optional(),
  dealIssueBorrowerName: Joi.string().allow("").optional(),
  dealIssueGuarantorName: Joi.string().allow("").optional(),
  dealIssueTranCounty: Joi.string().allow("").optional(),
  dealIssueTranPrimarySector: Joi.string().allow("").optional(),
  dealIssueTranSecondarySector: Joi.string().allow("").optional(),
  dealIssueofferingType: Joi.string().allow("").optional(),
  dealIssueSecurityType: Joi.string().allow("").optional(),
  dealIssueBankQualified: Joi.string().allow("").optional(),
  dealIssueCorpType: Joi.string().allow("").optional(),
  dealIssueParAmount: Joi.number().allow("", null).optional(),
  dealIssuePricingDate: Joi.date().example(new Date("2016-01-01")).optional(),
  dealIssueExpAwardDate: Joi.date().example(new Date("2016-01-01")).optional(),
  dealIssueActAwardDate: Joi.date().example(new Date("2016-01-01")).optional(),
  dealIssueSdlcCreditPerc: Joi.number().allow(null).optional(),
  dealIssueEstimatedRev: Joi.number().allow("", null).optional() ,
  dealIssueUseOfProceeds: Joi.string().allow("").optional(),
  dealIssuePlaceholder: Joi.string().allow("").optional(),
  termsOfIssue: Joi.number().allow(null).optional(),
  tic: Joi.number().allow(null).optional(),
  bbri: Joi.number().allow(null).optional(),
  nic: Joi.number().allow(null).optional(),
  gsp: Joi.number().allow(null).optional(),
  mgtf: Joi.number().allow(null).optional(),
  exp: Joi.number().allow(null).optional(),
  tkdn: Joi.number().allow(null).optional(),
  other: Joi.number().allow(null).optional(),
  updateUser: Joi.string().optional(),
  updateDate: Joi.date().optional(),
  _id: Joi.string().required()
})

const CreateDealsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, tranDealsCreateSchema, {abortEarly: false, stripUnknown: false})

const UpdateDealsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, transDetailSchema, {abortEarly: false, stripUnknown: false})

const dealPartDistSchema = Joi.object().keys({
  dealPartFirmId: Joi.string().required().optional(),
  dealPartType: Joi.string().required(),
  dealPartFirmName: Joi.string().required().optional(),
  dealPartContactId: Joi.string().required().optional(),
  dealPartContactName: Joi.string().required(),
  dealPartContactTitle: Joi.string().required().optional(),
  dealPartContactEmail: Joi.string().email().required(),
  dealPartUserAddress: Joi.string().allow("").optional(),
  dealPartContactAddrLine1: Joi.string().allow("").optional(),
  dealPartContactAddrLine2: Joi.string().allow("").optional(),
  dealPartAddressGoogle: Joi.string().allow("").optional(),
  dealParticipantState: Joi.string().allow("").optional(),
  dealPartContactPhone: Joi.string().allow("").optional(),
  dealPartContactFax: Joi.string().allow("").optional(),
  dealPartUserActive: Joi.string().allow("").optional(),
  addToDL: Joi.boolean().optional(),
  createdDate: Joi.date().example(new Date("2016-01-01")).optional(),
  _id: Joi.string().required().optional()
})

const dealUnderWritersSchema = Joi.object().keys({
  dealPartFirmId: Joi.string().required(),
  dealPartType: Joi.string().required(),
  dealPartFirmName: Joi.string().required(),
  roleInSyndicate: Joi.string().required(),
  liabilityPerc: Joi.number().allow(null, "").optional(),
  managementFeePerc: Joi.number().allow(null, "").optional(),
  dealPartContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional()
})

export const DealsPartUnderWritersValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, dealUnderWritersSchema, {abortEarly: false, stripUnknown: false})

export const DealsParticipantsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, dealPartDistSchema, {abortEarly: false, stripUnknown: false})

const dealsTradingInformationSchema = Joi.object().keys({
  contractID: Joi.string().required(),
  contractStatus: Joi.string().required(),
  portfolio: Joi.string().required(),
  portfolioGroup: Joi.string().required(),
  counterParty: Joi.string().required(),
  securityGroup: Joi.string().required(),
  securityType: Joi.string().required(),
  brokerOrDealer: Joi.string().required(),
  tradeDate: Joi.string().required(),
  settlementDays: Joi.number().min(0).required(),
  settlementDate: Joi.string().required(),
  _id: Joi.string().required().optional()
}).required()

const dealsContractInformationSchema = Joi.object().keys({
  standardContract: Joi.string().required(),
  receive: Joi.string().required(),
  currency: Joi.string().required(),
  notional: Joi.number().min(0).required(),
  fixedRate: Joi.number().min(0).required(),
  referenceRate: Joi.string().required(),
  spread: Joi.number().min(0).required(),
  initialRate: Joi.number().min(0).required(),
  effectiveDate: Joi.string().required(),
  tenor: Joi.number().min(0).required(),
  tenorType: Joi.string().required(),
  _id: Joi.string().required().optional()
}).required()

const dealIssueLegSchema = Joi.object().keys({
  interest: Joi.string().required(),
  frequency: Joi.string().required(),
  maturityDate: Joi.string().required(),
  dayCount: Joi.string().required(),
  businessConvention: Joi.string().required(),
  bankHolidays: Joi.string().required(),
  endOfMonth: Joi.string().required(),
  securityGroup: Joi.string().required(),
  securityType: Joi.string().required(),
  currency: Joi.string().required(),
  notional: Joi.number().min(0).required(),
  fixedRate: Joi.number().min(0).required(),
  referenceRate: Joi.string().required(),
  spread: Joi.number().min(0).required(),
  initialRate: Joi.number().min(0).required(),
  effectiveDate: Joi.string().required(),
  _id: Joi.string().required().optional()
}).required()

const dealIssueComplianceSchema = Joi.object().keys({
  status: Joi.string().required(),
  canRelease: Joi.string().required(),
  _id: Joi.string().required().optional()
}).required()

const dealsContractSchema = Joi.object().keys({
  dealIssueTradingInformation: dealsTradingInformationSchema,
  dealIssueContractInformation: dealsContractInformationSchema,
  dealIssueReceiveLeg: dealIssueLegSchema,
  dealIssuePayLeg: dealIssueLegSchema,
  dealIssueCompliance: dealIssueComplianceSchema,
  _id: Joi.string().required().optional()
})

const TradeContractValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, dealsContractSchema, {abortEarly: false, stripUnknown: false})

const seriesPricingDataSchema = Joi.object().keys({
  term: Joi.number().min(0).max(50).required(),
  maturityDate: Joi.date() /* .min(Joi.ref("callDate")) */.required(),
  amount: Joi.number().min(0).required(),
  coupon: Joi.number().min(0).max(0.15).required(),
  yield: Joi.number().min(0).max(0.15).required(),
  price: Joi.number().min(0).required(),
  YTM: Joi.number().min(0).max(0.15).required(),
  cusip: Joi.string().uppercase().min(6).max(9).required().optional(),
  callDate: Joi.date().required(),
  takeDown: Joi.number().min(0).required(),
  insured: Joi.boolean().required().optional(),
  drop: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional()
})

const seriesPricingDetailsSchema = Joi.object().keys({
  dealSeriesPrincipal: Joi.number().min(0).required(),
  dealSeriesSecurityType: Joi.string().required().optional(),
  dealSeriesSecurity: Joi.string().allow("", null).optional(),
  dealSeriesSecurityDetails: Joi.string().allow("", null).optional(),
  dealSeriesDatedDate: Joi.date().allow(null).optional(),
  dealSeriesSettlementDate: Joi.date().allow(null).optional(),
  dealSeriesFirstCoupon: Joi.date().allow(null).optional(),
  dealSeriesPutDate: Joi.date().allow(null).optional(),
  dealSeriesRecordDate: Joi.date().allow(null).optional(),
  dealSeriesCallDate: Joi.date().allow(null).optional(),
  dealSeriesFedTax: Joi.string().allow("", null).optional(),
  dealSeriesStateTax: Joi.string().allow("", null).optional(),
  dealSeriesPricingAMT: Joi.string().allow("", null).optional(),
  dealSeriesBankQualified: Joi.string().allow("", null).optional(),
  dealSeriesAccrueFrom: Joi.string().allow("", null).optional(),
  dealSeriesPricingForm: Joi.string().allow("", null).optional(),
  dealSeriesCouponFrequency: Joi.string().allow("", null).optional(),
  dealSeriesDayCount: Joi.string().allow("", null).optional(),
  dealSeriesRateType: Joi.string().allow("", null).optional(),
  dealSeriesCallFeature: Joi.string().allow("", null).optional(),
  dealSeriesCallPrice: Joi.number().min(0).allow(null).optional(),
  dealSeriesInsurance: Joi.string().allow("", null).optional(),
  dealSeriesUnderwtiersInventory: Joi.string().allow("", null).optional(),
  dealSeriesMinDenomination: Joi.number().min(0).allow(null).optional(),
  dealSeriesSyndicateStructure: Joi.string().allow("", null).optional(),
  dealSeriesGrossSpread: Joi.number().min(0).allow(null).optional(),
  dealSeriesEstimatedTakeDown: Joi.number().min(0).allow(null).optional(),
  dealSeriesInsuranceFee: Joi.number().min(0).allow(null).optional(),
  _id: Joi.string().required().optional()
})

const dealsPricingSchema = Joi.object().keys({
  seriesPricingDetails: seriesPricingDetailsSchema,
  seriesPricingData: Joi.array().items(seriesPricingDataSchema).optional()
})

const PricingDetailsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, dealsPricingSchema, {abortEarly: false, stripUnknown: false})

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean().required().optional(),
  publicDate: Joi.string().required().optional()
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean().required().optional(),
  emmaSendDate: Joi.string().required().optional()
})

const settings = Joi.object().keys({
  selectLevelType: Joi.string().optional(),
  firms: Joi.array().optional(),
  users: Joi.array().optional()
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().allow("").required(),
  docSubCategory: Joi.string().allow("").required(),
  docType: Joi.string().allow("").required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  docNote: Joi.string().allow("").optional(),
  docStatus: Joi.string().allow("").optional(),
  markedPublic,
  sentToEmma,
  settings,
  docAction: Joi.string().allow("").optional(),
  createdBy: Joi.string().required(),
  LastUpdatedDate: Joi.date().optional(),
  createdUserName: Joi.string().required(),
  timeStamp: Joi.number().optional(),
  _id: Joi.string().required().optional()
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().required(),
  dealIssueDocuments: Joi.array().min(1).items(docListSchema).required()
})

export const DocumentsDealsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, DocDetailsDealsSchema, {abortEarly: false, stripUnknown: false})

export const pricingErrorHandling = (req, res, next) => {
  const errors = PricingDetailsValidate(req.body)
  if (errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const createDealsErrorHandling = (req, res, next) => {
  const errors = CreateDealsValidate(req.body)
  if (errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const dealsErrorHandling = (req, res, next) => {
  const { type } = req.params
  let errors
  if (type === "details") {
    errors = UpdateDealsValidate(req.body)
  }
  if (type === "participants") {
    errors = DealsParticipantsValidate(req.body)
  }
  if (type === "underwriters") {
    errors = DealsPartUnderWritersValidate(req.body)
  }
  if (type === "contract") {
    errors = TradeContractValidate(req.body)
  }
  if (type === "documents") {
    errors = DocumentsDealsValidate(req.body)
  }
  if (errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  next()
}
