import mongoose, { Schema } from "mongoose"
import timestamps from "mongoose-timestamp"
import { Entity } from "../entity/entity.model"
import { EntityUser } from "../entityUser/entityUser.model"
import { checklistsSchema } from "../config/config.model"

// mongoose.Promise = global.Promise

const dealUnderwriterSchema = Schema({
  dealPartFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  dealPartType: String,
  dealPartFirmName: String,
  roleInSyndicate: String,
  liabilityPerc: Number,
  managementFeePerc: Number,
  dealPartContactAddToDL: Boolean
})

const dealPartDistSchema = Schema({
  dealPartFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  dealPartType: String,
  dealPartFirmName: String, // May not need this
  dealPartContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  dealPartContactName: String,
  dealPartContactTitle: String,
  dealPartContactEmail: String, // Same reasoning as above
  dealPartUserAddress: { type: Schema.Types.ObjectId }, // Same reasoning as above
  dealPartContactAddrLine1: String, // Same reasoning as above
  dealPartContactAddrLine2: String, // Same reasoning as above
  dealPartAddressGoogle: String, // Same reasoning as above
  dealParticipantState: String, // Same reasoning as above
  dealPartContactPhone: String,
  dealPartContactFax: String,
  dealPartUserActive: String,
  addToDL: Boolean,
  createdDate: Date
})

const dealSeriesRatingSchema = Schema({
  seriesId: { type: Schema.Types.ObjectId },
  seriesName: String,
  ratingAgencyName: String,
  longTermRating: String,
  longTermOutlook: String,
  shortTermOutlook: String,
  shortTermRating: String
})

const dealCepRatingsSchema = Schema({
  cepName: String,
  seriesId: { type: Schema.Types.ObjectId },
  seriesName: String,
  cepType: String,
  longTermRating: String,
  longTermOutlook: String,
  shortTermOutlook: String,
  shortTermRating: String
})

const dealSeriesPricingSchema = Schema({
  dealSeriesPrincipal: Number,
  dealSeriesSecurityType: String,
  dealSeriesSecurity: String,
  dealSeriesSecurityDetails: String,
  dealSeriesDatedDate: Date,
  dealSeriesSettlementDate: Date,
  dealSeriesFirstCoupon: Date,
  dealSeriesPutDate: Date,
  dealSeriesRecordDate: Date,
  dealSeriesCallDate: Date,
  dealSeriesFedTax: String,
  dealSeriesStateTax: String,
  dealSeriesPricingAMT: String,
  dealSeriesBankQualified: String,
  dealSeriesAccrueFrom: String,
  dealSeriesPricingForm: String,
  dealSeriesCouponFrequency: String,
  dealSeriesDayCount: String,
  dealSeriesRateType: String,
  dealSeriesCallFeature: String,
  dealSeriesCallPrice: Number,
  dealSeriesInsurance: String,
  dealSeriesUnderwtiersInventory: String,
  dealSeriesMinDenomination: Number,
  dealSeriesSyndicateStructure: String,
  dealSeriesGrossSpread: Number,
  dealSeriesEstimatedTakeDown: Number,
  dealSeriesInsuranceFee: Number
})

const dealPricingDataSchema = Schema({
  term: String,
  maturityDate: Date,
  amount: Number,
  coupon: Number,
  yield: Number,
  price: Number,
  YTM: Number,
  cusip: String,
  callDate: Date,
  takeDown: Number,
  insured: Boolean,
  drop: Boolean
})

const dealSeriesInfo = Schema({
  contactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  seriesName: String,
  description: String,
  tag: String,
  seriesRatings: [dealSeriesRatingSchema],
  cepRatings: [dealCepRatingsSchema],
  seriesPricingDetails: dealSeriesPricingSchema,
  seriesPricingData: [dealPricingDataSchema]
})

const dealDocumentsSchema = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: String,
  docFileName: String,
  docNote: String,
  docStatus: String,
  settings: {
    selectLevelType: String,
    firms: Array,
    users: Array
  },
  markedPublic: { publicFlag: Boolean, publicDate: Date },
  sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
  createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const dealSoeSchema = Schema({
  soeCategoryName: String,
  soeItems: [
    {
      eventItem: String,
      responsibleParties: [
        {
          dealPartyUserId: String /*{type: Schema.Types.ObjectId, ref: EntityUser} */,
          dealParticipantType: String
        }
      ],
      eventStartDate: Date,
      eventEndDate: Date,
      eventStatus: String
    }
  ]
})

const dealCostOfIssuanceSchema = Schema({
  costOfIssCategory: String,
  costItemDetails: [
    {
      costItem: String,
      costPerParBond: Number,
      costAmount: Number
    }
  ],
  totalCosts: { costPerParBond: Number, costAmount: Number }
})

const dealIssueTradingInformationSchema = Schema({
  contractID: String,
  contractStatus: String,
  portfolio: String,
  portfolioGroup: String,
  counterParty: String,
  securityGroup: String,
  securityType: String,
  brokerOrDealer: String,
  tradeDate: Date,
  settlementDays: Number,
  settlementDate: Date
})

const dealIssueContractInformationSchema = Schema({
  standardContract: String,
  receive: String,
  currency: String,
  notional: Number,
  fixedRate: Number,
  referenceRate: String,
  spread: Number,
  initialRate: Number,
  effectiveDate: Date,
  tenor: Number,
  tenorType: String
})

const dealIssueLegSchema = Schema({
  currency: String,
  notional: Number,
  interest: String,
  fixedRate: Number,
  referenceRate: String,
  spread: Number,
  frequency: String,
  initialRate: Number,
  effectiveDate: Date,
  maturityDate: Date,
  dayCount: String,
  businessConvention: String,
  bankHolidays: String,
  endOfMonth: String,
  securityGroup: String,
  securityType: String
})

const dealIssueComplianceSchema = Schema({
  status: String,
  canRelease: String
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const tranAgencyDealSchema = Schema({
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  dealIssueTranClientId: { type: Schema.Types.ObjectId, ref: Entity }, // this is the financial advisor who is the client of munivisor
  dealIssueTranIssuerId: { type: Schema.Types.ObjectId, ref: Entity }, // this is the issuer client
  dealIssueTranClientFirmName: String,
  dealIssueTranClientMSRBType: String,
  dealIssueTranIssuerFirmName: String,
  dealIssueTranIssuerMSRBType: String,
  dealIssueBorrowerName: String,
  dealIssueGuarantorName: String,
  dealIssueTranName: String,
  dealIssueTranType: String,
  dealIssueTranPurposeOfRequest: String,
  dealIssueTranAssignedTo: [{ type: Schema.Types.ObjectId, ref: EntityUser }],
  dealIssueTranRelatedTo: [
    Schema({
      relTranId: { type: Schema.Types.ObjectId },
      relType: String,
      relTranIssueName: String,
      relTranProjectName: String,
      relTranClientId: { type: Schema.Types.ObjectId },
      relTranClientName: String,
      tranType: String
    })
  ],
  dealIssueTranState: String,
  dealIssueTranCounty: String,
  dealIssueTranPrimarySector: String,
  dealIssueTranSecondarySector: String,
  dealIssueTranDateHired: Date,
  dealIssueTranStartDate: Date,
  dealIssueTranExpectedEndDate: Date,
  dealIssueTranStatus: String,
  dealIssueTransNotes: String,
  tranNotes: [notesSchema],
  // add new deal //
  dealIssueTranSubType: String,
  dealIssueTranOtherTransactionType: String,
  dealIssueTranFirmLeadAdvisorId: { type: Schema.Types.ObjectId },
  dealIssueTranFirmLeadAdvisorName: String,
  dealIssueTranIsClientMsrbRegistered: Boolean,
  dealIssueTranIsConduitBorrowerFlag: String,
  dealIssueTranIssueName: String,
  dealIssueTranProjectDescription: String,
  // add new deal //

  dealIssueofferingType: String,
  dealIssueSecurityType: String,
  dealIssueBankQualified: String,
  dealIssueCorpType: String,
  dealIssueParAmount: Number,
  dealIssuePricingDate: Date,
  dealIssueExpAwardDate: Date,
  dealIssueActAwardDate: Date,
  dealIssueSdlcCreditPerc: Number,
  dealIssueEstimatedRev: Number,
  dealIssueUseOfProceeds: String,

  termsOfIssue: Number,
  tic: Number,
  bbri: Number,
  nic: Number,
  gsp: Number,
  mgtf: Number,
  exp: Number,
  tkdn: Number,
  other: Number,

  dealIssueUnderwriters: [dealUnderwriterSchema],
  dealIssueParticipants: [dealPartDistSchema],
  dealIssueSeriesDetails: [dealSeriesInfo],
  dealIssueDocuments: [dealDocumentsSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  // dealIssueScheduleOfEvents:[dealSoeSchema],
  // dealIssueCostOfIssuance:[dealCostOfIssuanceSchema],dealMigration:{},
  dealMigration: {},
  dealChecklists: [checklistsSchema],
  dealIssueCostOfIssuanceNotes: String,
  dealIssuePlaceholder: String,
  dealIssueTradingInformation: dealIssueTradingInformationSchema,
  dealIssueContractInformation: dealIssueContractInformationSchema,
  dealIssueReceiveLeg: dealIssueLegSchema,
  dealIssuePayLeg: dealIssueLegSchema,
  dealIssueCompliance: dealIssueComplianceSchema,
  created_at: { type: Date, required: true, default: Date.now },
  updateDate: { type: Date, required: true, default: Date.now },
  updateUser: { type: Schema.Types.ObjectId, ref: EntityUser },
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

tranAgencyDealSchema.index({dealIssueTranClientId:1})
tranAgencyDealSchema.index({"dealIssueParticipants.dealPartFirmId":1})
tranAgencyDealSchema.index({"dealIssueUnderwriters.dealPartFirmId":1})
tranAgencyDealSchema.index({dealIssueTranIssuerId:1})

export const Deals =  mongoose.model("tranagencydeal", tranAgencyDealSchema)
