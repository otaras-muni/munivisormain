import express from "express"
import billingExpenseController from "./billingmanageexpense.controller"
import {requireAuth} from "./../../../authorization/authsessionmanagement/auth.middleware"

export const billingExpenseRouter = express.Router()

billingExpenseRouter.route("/")
  .get(requireAuth,billingExpenseController.getAll)
  .put(requireAuth, billingExpenseController.putManageExpense)

billingExpenseRouter.route("/:entityId/:activityId")
  .get(requireAuth,billingExpenseController.getManageExpense)

billingExpenseRouter.route("/tranexpense")
  .get(requireAuth,billingExpenseController.getExpenseOfSelectedTransactions)

billingExpenseRouter.route("/:entityId")
  .get(requireAuth,billingExpenseController.getManageExpenseByIssueName)

billingExpenseRouter.route("/receipts")
  .get(requireAuth,billingExpenseController.getReceipts)
  .put(requireAuth,billingExpenseController.putReceipts)
  .delete(requireAuth,billingExpenseController.pullReceipts)
