import express from "express"
import billingController from "./billing.controller"
import { requireAuth } from "./../../../authorization/authsessionmanagement/auth.middleware"

export const billingRouter = express.Router()

billingRouter.param("id", billingController.findByParam)

// Get all the existing contracts for the client from the client contract setup
// Search for the contract based on few parameters
// Select the contract that would need to be attached to the Billing Module
// Get the List of Transactions and some high level meta data that can be used to build the billing summary - Done
// Simulate some test contracts and test deals that can be used for testing - Done
// Make the receipts and expenses user specific
// Look at the entitled users and make sure only the logged in user information is accessible to the user
// Display a global firm view in addition to the user view
// What happens if there awre two contracts.

billingRouter
  .route("/")
  .get(requireAuth, billingController.getAll)
  .post(requireAuth, billingController.createOne)

billingRouter.route("/invoice/:id").get(requireAuth, billingController.getOne)

billingRouter
  .route("/managebillingexpensesstate")
  .post(requireAuth, billingController.manageBillingExpensesState)

billingRouter
  .route("/getcontracts/:clientId")
  .get(billingController.getConsolidatedContractsForBilling)

billingRouter
  .route("/search")
  .get(requireAuth, billingController.getAllInvoicesForSearch)

billingRouter
  .route("/dashboard/updateinvoice")
  .post(requireAuth, billingController.updateInvoiceStatus)

billingRouter
  .route("/:invoiceid/alluserexpenses/:userId")
  .get(requireAuth, billingController.getUserExpensesForInvoiceLoggedInUser)
  .post(requireAuth, billingController.postUserExpensesForInvoiceLoggedInUser)

billingRouter
  .route("/:invoiceid/userexpense/:userId")
  .post(requireAuth, billingController.postOneExpenseForInvoiceAndLoggedInUser)
  .delete(requireAuth, billingController.pullExpenseDetails)

billingRouter
  .route("/:invoiceid/expensereceipt/:userId")
  .post(requireAuth, billingController.pushBillingReceiptsForUser)
  .delete(requireAuth, billingController.pullSpecifcReceiptForUser)

billingRouter
  .route("/:invoiceid/alluserexpenses")
  .get(requireAuth, billingController.getAllFirmUserExpenses)
  .post(requireAuth, billingController.postAllFirmUserExpenses)

billingRouter
  .route("/faservicefee")
  .get(requireAuth, billingController.getFAServiceFee)
  .post(requireAuth, billingController.postFAServiceData)
  .put(requireAuth, billingController.putFAServiceFee)

// Use this only for the contract related items
billingRouter
  .route("/:faservid/:feetype/")
  .get(requireAuth, billingController.getFAServiceDataForType)
  .post(requireAuth, billingController.postFAServiceDataForType)

billingRouter
  .route("/feetype/:feetype/:feeId")
  .post(requireAuth, billingController.postFAServiceDataForFeeTypeAndId)

billingRouter
  .route("/transactions")
  .get(requireAuth, billingController.getAllTransactionsForInvoice)

billingRouter
  .route("/createinvoice")
  .post(requireAuth, billingController.putInvoice)

billingRouter
  .route("/revenuepipeline")
  .get(requireAuth, billingController.getRevenuePipelineDetails)
  .post(requireAuth, billingController.filtterRevenuePipelineDetails)
/*
    TODO - Require Authentication on all the routes
    TODO - Require Validation
  */
