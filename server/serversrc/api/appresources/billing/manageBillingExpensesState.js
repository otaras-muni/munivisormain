import {ObjectID} from "mongodb"
import mongoose from "mongoose"
import isEmpty from "lodash/isEmpty"
import  {  Entity, EntityRel, Deals, ActMaRFP, Derivatives, BankLoans, RFP, TenantBilling }  from "./../../appresources/models"
import { getAllAccessAndEntitlementsForLoggedInUser } from "./../../entitlementhelpers"
import { Others } from "../activities";

require("dotenv").config()

// mongoose.Promise = global.Promise
// mongoose.set("useCreateIndex", true)
// mongoose.connect(process.env.DB_URL,{ useNewUrlParser: true })


const fnIsNull = (obj, keys) => keys.reduce ( (ac,k) => ac && (!obj[k] || obj[k] === "" || isEmpty(obj[k])), true)
const fnIsNotNull = (obj, keys) => keys.reduce ( (ac,k) => ac && (obj[k] || obj[k] !== "" || !isEmpty(obj[k])), true)

export const manageAndRefreshBillingDetailsState = async (loggedInUser, stateObj ) => {

  const { relationshipType:RELTYPE, userEntitlement:USERENT} = loggedInUser

  let flagForGlobal = false

  if(RELTYPE === "Self" && USERENT === "global-edit") {
    flagForGlobal = true
  }

  if(RELTYPE !== "Self" ) {
    return {selectedData:{}, dropDownValues:[]}
  }

  console.log("1. ENTERED THE BIG MANAGE EXPENSES FUNCTION")
  const allowedIds = await getAllAccessAndEntitlementsForLoggedInUser(loggedInUser, "nav")
  const edit = (allowedIds && allowedIds.edit) || []
  const allTenantUsers = await EntityRel.aggregate([
    {$match:{entityParty2:ObjectID(loggedInUser.entityId),relationshipType:"Self"}},
    {
      $lookup:{
        from: "entityusers",
        localField: "entityParty2",
        foreignField: "entityId",
        as: "entityusers"
      }
    },
    {
      $unwind:"$entityusers"
    },
    {
      $project:{_id:0,userId:"$entityusers._id",userFirstName:"$entityusers.userFirstName",userLastName:"$entityusers.userLastName"}
    }
  ])

  const allFirmUserIds = allTenantUsers.map( m => m.userId)


  const dealsPipelineGlobal = [
    {
      $match:{dealIssueTranClientId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $lookup:{
        from: "entities",
        localField: "dealIssueTranIssuerId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$dealIssueTranClientId",
        activityTranFirmName:"$dealIssueTranClientFirmName",
        activityTranClientId:"$dealIssueTranIssuerId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$dealIssueTranType",
        activityTranSubType:"$dealIssueTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$dealIssueParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$dealIssueTranAssignedTo"
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$dealIssueParticipants.dealPartContactId",
                    "$dealIssueTranAssignedTo"
                  ]
                },
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$dealIssueTranProjectDescription",
      }
    },
  ]
  const bankLoansPipelineGlobal = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actTranClientId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actTranClientId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actTranType",
        activityTranSubType:"$actTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$bankLoanParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$bankLoanParticipants.partContactId",
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actTranProjectDescription"
      }
    },
  ]
  const derivativeTransactionsGlobal = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actTranClientId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actTranClientId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actTranType",
        activityTranSubType:"$actTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$derivativeParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$derivativeParticipants.partContactId",
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actTranProjectDescription"
      }
    },
  ]
  const marfpTransactionsGlobal = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actIssuerClient",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actIssuerClient",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actType",
        activityTranSubType:"$actSubType",
        activityContract:"$contracts",
        // activityParticipants:"$maRfpParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actLeadFinAdvClientEntityId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$maRfpParticipants.partContactId",
                    ["$actLeadFinAdvClientEntityId"]
                  ]
                },
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actProjectName"
      }
    },
  ]
  const rfpTransactionsGlobal = [
    {
      $match:{rfpTranClientId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $lookup:{
        from: "entities",
        localField: "rfpTranIssuerId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$rfpTranClientId",
        activityTranFirmName:"$rfpTranClientFirmName",
        activityTranClientId:"$rfpTranIssuerId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$rfpTranType",
        activityTranSubType:"$rfpTranSubType",
        activityContract:"$contracts",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$rfpTranAssignedTo"
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        // activityParticipants:"$rfpParticipants",
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$rfpEvaluationTeam.rfpSelEvalContactId",
                    "$rfpProcessContacts.rfpProcessContactId",
                    "$rfpParticipants.rfpParticipantContactId",
                    "$rfpTranAssignedTo"
                  ]
                },
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$rfpTranProjectDescription"
      }
    },
  ]
  const othersTransactionsGlobal = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actTranClientId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actTranClientId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actTranType",
        activityTranSubType:"$actTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$participants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$participants.partContactId",
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actTranProjectDescription",
      }
    },
  ]

  const dealPipelineOtherUserTypes = [
    {
      $match:{dealIssueTranClientId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $match:{_id:{$in:[...edit || []]}}
    },
    {
      $lookup:{
        from: "entities",
        localField: "dealIssueTranIssuerId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$dealIssueTranClientId",
        activityTranFirmName:"$dealIssueTranClientFirmName",
        activityTranClientId:"$dealIssueTranIssuerId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$dealIssueTranType",
        activityTranSubType:"$dealIssueTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$dealIssueParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$dealIssueTranAssignedTo"
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$dealIssueParticipants.dealPartContactId",
                    "$dealIssueTranAssignedTo"
                  ]
                },
                [...edit || []],
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$dealIssueTranProjectDescription",
        /* activityDescription : {
          $cond:{if:{$or:[{"$eq":["$dealIssueTranIssueName",""]},{"$eq":["$dealIssueTranIssueName",null]}]},
          then:"$dealIssueTranProjectDescription",
          else:"$dealIssueTranIssueName"},
        }, */
      }
    },
    // {
    //   $addFields:{
      //     selectedUserDetails:{
    //       "$filter": {
      //         input:"$eligibleUserDetails",
    //         as: "ten",
    //         cond: { $in: [ "$$ten.userId", [updatedSelectedId] ] }
    //       }

    //     }
    //   }
    // }
  ]
  const bankLoansOtherUserTypes = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $match:{_id:{$in:[...edit || []]}}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actTranClientId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actTranClientId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actTranType",
        activityTranSubType:"$actTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$bankLoanParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$bankLoanParticipants.partContactId",
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...edit || []],
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actTranProjectDescription"
        /* activityDescription : {
          $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranProjectDescription",null]}]},
            then:"$actTranProjectDescription",
            else:"$actTranIssueName"},
        }, */
      }
    },
    // {
    //   $addFields:{
    //     selectedUserDetails:{
    //       "$filter": {
    //         input:"$eligibleUserDetails",
    //         as: "ten",
    //         cond: { $in: [ "$$ten.userId", [updatedSelectedId] ] }
    //       }

    //     }
    //   }
    // }

  ]
  const derivativeTransactionsOthers = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $match:{_id:{$in:[...edit ||[]]}}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actTranClientId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actTranClientId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actTranType",
        activityTranSubType:"$actTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$derivativeParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$derivativeParticipants.partContactId",
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...edit || []],
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actTranProjectDescription"
      }
    },
  ]
  const marfpTransactionsOtherTypes = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $match:{_id:{$in:[...edit || []]}}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actIssuerClient",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actIssuerClient",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actType",
        activityTranSubType:"$actSubType",
        activityContract:"$contracts",
        // activityParticipants:"$maRfpParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actLeadFinAdvClientEntityId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$maRfpParticipants.partContactId",
                    ["$actLeadFinAdvClientEntityId"]
                  ]
                },
                [...edit || []],
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actProjectName"
        /* activityDescription : {
          $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actProjectName",null]}]},
            then:"$actProjectName",
            else:"$actIssueName"},
        }, */
      }
    },
    // {
    //   $addFields:{
    //     selectedUserDetails:{
    //       "$filter": {
    //         input:"$eligibleUserDetails",
    //         as: "ten",
    //         cond: { $in: [ "$$ten.userId", [updatedSelectedId] ] }
    //       }

    //     }
    //   }
    // }

  ]
  const rfpTransactionsOtherTypes = [
    {
      $match:{rfpTranClientId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $match:{_id:{$in:[...edit || []]}}
    },
    {
      $lookup:{
        from: "entities",
        localField: "rfpTranIssuerId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$rfpTranClientId",
        activityTranFirmName:"$rfpTranClientFirmName",
        activityTranClientId:"$rfpTranIssuerId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$rfpTranType",
        activityTranSubType:"$rfpTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$rfpParticipants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$rfpTranAssignedTo"
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$rfpEvaluationTeam.rfpSelEvalContactId",
                    "$rfpProcessContacts.rfpProcessContactId",
                    "$rfpParticipants.rfpParticipantContactId",
                    "$rfpTranAssignedTo"
                  ]
                },
                [...edit || []],
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$rfpTranProjectDescription"
      }
    },
  ]
  const othersTransactionsOtherTypes = [
    {
      $match:{actTranFirmId:ObjectID(loggedInUser.tenantId)}
    },
    {
      $match:{_id:{$in:[...edit || []]}}
    },
    {
      $lookup:{
        from: "entities",
        localField: "actTranClientId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:0,
        activityId:"$_id",
        activityTranFirmId:"$actTranFirmId",
        activityTranFirmName:"$actTranFirmName",
        activityTranClientId:"$actTranClientId",
        activityClientName:"$entitydetails.firmName",
        activityTranType:"$actTranType",
        activityTranSubType:"$actTranSubType",
        activityContract:"$contracts",
        // activityParticipants:"$participants",
        activityLeadAdvisor:{
          $arrayElemAt:[{
            "$filter": {
              input:[...allTenantUsers],
              as: "ten",
              cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...allFirmUserIds]
              ]
              } ] }
            }
          },0]
        },
        eligibleUserDetails:{
          "$filter": {
            input:[...allTenantUsers],
            as: "ten",
            cond: { $in: [ "$$ten.userId", { $setIntersection:
              [
                {
                  $setUnion:[
                    "$participants.partContactId",
                    ["$actTranFirmLeadAdvisorId"]
                  ]
                },
                [...(edit || [])],
                [...allFirmUserIds],
                [...edit || []],
                [...allFirmUserIds]
              ]
            } ] }
          }
        },
        activityDescription : "$actTranProjectDescription",
      }
    },
  ]

  const dealTransactions = await Deals.aggregate(flagForGlobal ? dealsPipelineGlobal: dealPipelineOtherUserTypes)
  const bankLoanTransactions = await BankLoans.aggregate(flagForGlobal ? bankLoansPipelineGlobal: bankLoansOtherUserTypes)
  const derivativeTransactions = await Derivatives.aggregate(flagForGlobal ? derivativeTransactionsGlobal: derivativeTransactionsOthers)
  const marfptransactions = await ActMaRFP.aggregate(flagForGlobal ? marfpTransactionsGlobal: marfpTransactionsOtherTypes)
  const rfpTransactions = await RFP.aggregate(flagForGlobal ? rfpTransactionsGlobal: rfpTransactionsOtherTypes)
  const othersTransactions = await Others.aggregate(flagForGlobal ? othersTransactionsGlobal: othersTransactionsOtherTypes)
  const consolidatedData = [...dealTransactions, ...bankLoanTransactions,...rfpTransactions,...derivativeTransactions,...marfptransactions,...othersTransactions ]

  const contracts = await Entity.findOne({_id: stateObj.selectedClient.activityTranClientId}).select("firmName contracts")

  const getClientData = () => {

    const clientData = consolidatedData.reduce( (acc, t) => {
      const { activityClientName,activityTranClientId } = t
      if( !acc[activityTranClientId]) {
        acc[activityTranClientId] = { activityTranClientId,activityClientName }
      }
      return acc
    },{})

    return Object.values(clientData || [])
  }
  /*
  activityId:"$_id",
  activityTranFirmId:"$rfpTranClientId",
  activityTranFirmName:"$rfpTranClientFirmName",
  activityTranClientId:"$rfpTranIssuerId",
  activityClientName:"$entitydetails.firmName",
  activityTranType:"$rfpTranType",
  activityTranSubType:"$rfpTranSubType",
  */

  const getTranForClient = (clientId) => consolidatedData
    .filter( a => a.activityTranClientId.toString() === clientId.toString())
    .map(({activityId,activityLeadAdvisor,activityTranFirmId,activityTranClientId,activityClientName,activityTranFirmName,eligibleUserDetails, activityDescription,activityTranType,activityTranSubType, activityContract, activityParticipants}) => ({activityId, activityLeadAdvisor,activityTranClientId,activityClientName,activityTranFirmId,eligibleUserDetails,activityTranFirmName,activityDescription,activityTranType,activityTranSubType,activityContract, activityParticipants}))

  const getUsersForTran = (tr) => consolidatedData.reduce( (acc, a) => {
    if (a.activityId.toString() === tr.toString())
    {
      return a.eligibleUserDetails.map( k => ({
        ...k,currentUser:k.userId.toString() === loggedInUser._id.toString()
      }))
    }
    return acc
  },[])

  const getInvoiceDetails = async(activityId) => {
    const invoiceDetails = await TenantBilling.aggregate([
      {
        $match:{activityId:ObjectID(activityId)}
      },
      {
        $project:{
          _id:0,
          invoiceId:"$_id",
          invoiceNumber:"$invoiceNumber"
        }
      }
    ])

    return invoiceDetails
  }

  const getExpensesForInvoice = async(invoiceId) => {
    const allUserExpenses = await TenantBilling.aggregate([
      {
        $match:{_id:ObjectID(invoiceId)}
      },
      {
        $unwind:"$outOfPocketUserExpenses"
      },
      {
        $project:{
          userId:"$outOfPocketUserExpenses.tenantUserId",
          activityId:"$activityId",
          activityTranClientId:"$activityTranClientId",
          outOfPocketExpenses:"$outOfPocketUserExpenses.outOfPocketExpenses",
        }
      }
    ])
    return allUserExpenses
  }

  const getExpensesForUser = async(invoiceId, userId) => {

    if (invoiceId && userId)
    {
      const invoiceForUser = await TenantBilling.aggregate([
        {
          $match:{_id:ObjectID(invoiceId)}
        },
        {
          $unwind:"$outOfPocketUserExpenses"
        },
        {
          $match:{"outOfPocketUserExpenses.tenantUserId":ObjectID(userId)}
        },
        {
          $project:{
            userId:"$outOfPocketUserExpenses.tenantUserId",
            outOfPocketExpenses:"$outOfPocketUserExpenses.outOfPocketExpenses",
          }
        }
      ])
      if (invoiceForUser.length > 0){
        return invoiceForUser[0].outOfPocketExpenses
      }
      return invoiceForUser
    }
    return []
  }

  const getReceiptsForUser = async(invoiceId, userId) => {

    if (invoiceId && userId)
    {
      const billingReceiptsForUser = await TenantBilling.aggregate([
        {
          $match:{_id:ObjectID(invoiceId)}
        },
        {
          $unwind:"$outOfPocketUserExpenses"
        },
        {
          $match:{"outOfPocketUserExpenses.tenantUserId":ObjectID(userId)}
        },
        {
          $project:{
            userId:"$outOfPocketUserExpenses.tenantUserId",
            billingReceipts:"$outOfPocketUserExpenses.billingReceipts",
          }
        }
      ])
      if (billingReceiptsForUser.length > 0){
        return billingReceiptsForUser[0].billingReceipts
      }
      return billingReceiptsForUser
    }
    return []
  }


  // is the selected User having Invoices
  // is the current logged in user having invoices
  // do they have only one transaction and one eligible user within the transaction
  // do they have only one user in the transaction


  if (fnIsNull(stateObj,["selectedClient", "selectedActivity","selectedInvoice","selectedUser"]))
  {
    // Everything is Null on the screen, get the client Names
    // get the list of client Names
    const newState = {
      selectedClient:{},
      selectedActivity:{},
      selectedInvoice:{},
      selectedUser:{},
      selectedUserExpenses:[],
      billingReceipts:[]
    }

    let activityDetails = []
    let invoiceDetails = []
    let userDetails = []

    const clientDetails = getClientData()
    if ( clientDetails.length === 1 ) {
      newState.selectedClient = clientDetails[0]
      activityDetails = await getTranForClient(clientDetails[0].activityTranClientId)
      if ( activityDetails.length === 1) {
        newState.selectedActivity = activityDetails[0]
        invoiceDetails = await getInvoiceDetails(activityDetails[0].activityId)
        userDetails = await getUsersForTran(activityDetails[0].activityId)

        if(invoiceDetails.length === 1) {
          getExpensesForInvoice(invoiceDetails[0].invoiceId)
          newState.selectedInvoice = invoiceDetails[0]

          if(userDetails.length === 1) {
            newState.selectedUser = userDetails[0]
            newState.selectedUserExpenses = await getExpensesForUser(invoiceDetails[0].invoiceId,userDetails[0].userId)
            newState.billingReceipts = await getReceiptsForUser(invoiceDetails[0].invoiceId,userDetails[0].userId)
          }
        }
        if(userDetails.length === 1) {
          newState.selectedUser = userDetails[0]
        }
      }
    }

    const dropDownValues = {
      clientDetails,
      activityDetails,
      invoiceDetails,
      userDetails,
      contracts
    }
    return {selectedData:newState, dropDownValues}
  }


  if (fnIsNotNull(stateObj,["selectedClient"]) && fnIsNull(stateObj,["selectedActivity","selectedInvoice","selectedUser"]))
  {
    const newState = {
      selectedClient:stateObj.selectedClient,
      selectedActivity:{},
      selectedInvoice:{},
      selectedUser:{},
      selectedUserExpenses:[],
      billingReceipts:[]

    }

    const clientDetails = await getClientData()
    const activityDetails = await getTranForClient(stateObj.selectedClient.activityTranClientId)
    let userDetails = []
    let invoiceDetails = []

    if ( activityDetails.length === 1) {
      newState.selectedActivity = activityDetails[0]
      invoiceDetails = await getInvoiceDetails(activityDetails[0].activityId)
      userDetails = await getUsersForTran(activityDetails[0].activityId)

      if(invoiceDetails.length === 1) {
        newState.selectedInvoice = invoiceDetails[0]
        if(userDetails.length === 1) {
          newState.selectedUser = userDetails[0]
          newState.selectedUserExpenses = await getExpensesForUser(invoiceDetails[0].invoiceId,userDetails[0].userId)
          newState.billingReceipts = await getReceiptsForUser(invoiceDetails[0].invoiceId,userDetails[0].userId)
        }
      }

      if(userDetails.length === 1) {
        newState.selectedUser = userDetails[0]
      }
    }

    const dropDownValues = {
      clientDetails,
      activityDetails,
      invoiceDetails,
      userDetails,
      contracts
    }
    return {selectedData:newState, dropDownValues}
  }

  if (fnIsNotNull(stateObj,["selectedClient","selectedActivity"]) && fnIsNull(stateObj,["selectedInvoice","selectedUser"]))
  {
    const newState = {
      selectedClient:stateObj.selectedClient,
      selectedActivity:stateObj.selectedActivity,
      selectedInvoice:{},
      selectedUser:{},
      selectedUserExpenses:[],
      billingReceipts:[]

    }

    const clientDetails = await getClientData()
    const activityDetails = await getTranForClient(stateObj.selectedClient.activityTranClientId)
    const userDetails = await getUsersForTran(stateObj.selectedActivity.activityId)
    const invoiceDetails = await getInvoiceDetails(stateObj.selectedActivity.activityId)

    if(invoiceDetails.length === 1) {
      console.log("HELLO ENTERED INVOICE - 1", invoiceDetails )
      newState.selectedInvoice = invoiceDetails[0]

      if(userDetails.length === 1) {
        console.log("HELLO ENTERED INVOICE - 1", invoiceDetails[0], )

        newState.selectedUser = userDetails[0]
        newState.selectedUserExpenses = await getExpensesForUser(invoiceDetails[0].invoiceId,userDetails[0].userId)
        newState.billingReceipts = await getReceiptsForUser(invoiceDetails[0].invoiceId,userDetails[0].userId)
      }
    }

    if(userDetails.length === 1) {
      newState.selectedUser = userDetails[0]
    }

    const dropDownValues = {
      clientDetails,
      activityDetails,
      invoiceDetails,
      userDetails,
      contracts
    }
    return {selectedData:newState, dropDownValues}
  }

  if (fnIsNotNull(stateObj,["selectedClient","selectedActivity","selectedInvoice"]) && fnIsNull(stateObj,["selectedUser"]))
  {
    const newState = {
      selectedClient:stateObj.selectedClient,
      selectedActivity:stateObj.selectedActivity,
      selectedInvoice:stateObj.selectedInvoice,
      selectedUser:{},
      selectedUserExpenses:[],
      billingReceipts:[]
    }

    const clientDetails = await getClientData()
    const activityDetails = await getTranForClient(stateObj.selectedClient.activityTranClientId)
    const userDetails = await getUsersForTran(stateObj.selectedActivity.activityId)
    const invoiceDetails = await getInvoiceDetails(stateObj.selectedActivity.activityId)


    if(userDetails.length === 1) {
      newState.selectedUser = userDetails[0]
      newState.selectedUserExpenses = await getExpensesForUser(stateObj.selectedInvoice.invoiceId,userDetails[0].userId)
      newState.billingReceipts = await getReceiptsForUser(stateObj.selectedInvoice.invoiceId,userDetails[0].userId)
    }
    const dropDownValues = {
      clientDetails,
      activityDetails,
      invoiceDetails,
      userDetails,
      contracts
    }
    return {selectedData:newState, dropDownValues}
  }

  if (fnIsNotNull(stateObj,["selectedClient","selectedActivity","selectedInvoice","selectedUser"]))
  {
    const newState = {
      selectedClient:stateObj.selectedClient,
      selectedActivity:stateObj.selectedActivity,
      selectedInvoice:stateObj.selectedInvoice,
      selectedUser:stateObj.selectedUser,
      selectedUserExpenses:[],
      billingReceipts:[]

    }

    const clientDetails = await getClientData()
    const activityDetails = await getTranForClient(stateObj.selectedClient.activityTranClientId)
    const userDetails = await getUsersForTran(stateObj.selectedActivity.activityId)
    const invoiceDetails = await getInvoiceDetails(stateObj.selectedActivity.activityId)
    newState.selectedUserExpenses = await getExpensesForUser(stateObj.selectedInvoice.invoiceId,stateObj.selectedUser.userId)
    newState.billingReceipts = await getReceiptsForUser(stateObj.selectedInvoice.invoiceId,stateObj.selectedUser.userId)

    const dropDownValues = {
      clientDetails,
      activityDetails,
      invoiceDetails,
      userDetails,
      contracts
    }
    return {selectedData:newState, dropDownValues}
  }

}
