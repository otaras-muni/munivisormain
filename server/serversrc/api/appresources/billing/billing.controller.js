import mongoose from "mongoose"
import { generateControllers } from "./../../modules/query"
import { TenantBilling } from "./billing.model"
import {
  EntityUser,
  EntityRel,
  Deals,
  RFP,
  ActMaRFP,
  Derivatives,
  BankLoans,
  Others,
  Entity
} from "./../models"
import { getAllTransactionDetailsForLoggedInUser } from "./../../commonDbFunctions/getAllDetailedTransactionsForLoggedInUser"
import { manageAndRefreshBillingDetailsState } from "./manageBillingExpensesState"
import { getAllAccessAndEntitlementsForLoggedInUser } from "../../entitlementhelpers"
import { canUserPerformAction } from "../../commonDbFunctions"

const { ObjectID } = require("mongodb")

const manageBillingExpensesState = () => async (req, res) => {
  console.log("Entered Manage Billing Expenses Controller")
  try {
    const { stateObj } = req.body
    const retValue = await manageAndRefreshBillingDetailsState(
      req.user,
      stateObj
    )
    console.log("The returValue", retValue)
    res.status(200).send({ done: true, newstatedetails: retValue })
  } catch (e) {
    console.log(
      "There is an error getting the new state for the billing expenses module",
      e
    )
    res.status(500).send({ done: false, data: [], error: e })
  }
}

const pushBillingReceiptsForUser = () => async (req, res) => {
  console.log(
    "We are in the process of inserting documents for the receipts for the user expenses"
  )
  console.log("PARAMETERS FOR -", { ...req.params, ...req.body })
  const { invoiceid, userId } = req.params
  const { receiptDocuments } = req.body
  console.log("THE PARAMETERS AFTER DESTRUCTURING ARE", {
    invoiceid,
    userId,
    receiptDocuments
  })

  try {
    await TenantBilling.update(
      {
        _id: ObjectID(invoiceid),
        "outOfPocketUserExpenses.tenantUserId": ObjectID(userId)
      },
      {
        $push: {
          "outOfPocketUserExpenses.$.billingReceipts": {
            $each: receiptDocuments.documents
          }
        }
      }
    )

    const invoiceDetails = await TenantBilling.findById({
      _id: invoiceid
    }).select(
      "outOfPocketUserExpenses.tenantUserId outOfPocketUserExpenses._id outOfPocketUserExpenses.billingReceipts"
    )
    const documents = invoiceDetails.outOfPocketUserExpenses.find(
      doc => doc.tenantUserId.toString() === userId.toString()
    )
    res.status(200).json(documents)
  } catch (e) {
    console.log("there is an error in uploading receipt for the users", e)
  }
}

const pullSpecifcReceiptForUser = () => async (req, res) => {
  console.log(
    "We are in the process of inserting documents for the receipts for the user expenses"
  )
  const { invoiceId, userId } = req.params
  const { docId } = req.query
  try {
    await TenantBilling.update(
      {
        _id: ObjectID(invoiceId),
        "outOfPocketUserExpenses.tenantUserId": ObjectID(userId)
      },
      {
        $pull: {
          "outOfPocketUserExpenses.$.billingReceipts": { _id: ObjectID(docId) }
        }
      }
    )

    const invoiceDetails = await TenantBilling.findOne({
      _id: invoiceId
    }).select(
      "outOfPocketUserExpenses.tenantUserId outOfPocketUserExpenses._id outOfPocketUserExpenses.billingReceipts"
    )
    const documents = invoiceDetails.outOfPocketUserExpenses.find(
      doc => doc.tenantUserId.toString() === userId.toString()
    )
    res.status(200).json(documents)
  } catch (e) {
    console.log("There is an error deleting documents for users", e)
  }
}

const getAllTransactionsForTenant = () => async (req, res) => {
  if (!req.user) {
    res.status(500).send({ done: "false", error: "User doesn't exist" })
  }

  const userId = req.user && req.user._id
  const userEntityId = req.user && req.user.entityId
  // Obtained Eligible Users for Logged In Users
  const tranDetails = await getAllTransactionDetailsForLoggedInUser(
    ObjectID(userId)
  )
  const keys = Object.keys(tranDetails.payload)

  // Flatten all transactions
  const allTransactions = keys.reduce(
    (acc, k) => {
      const tranIds = [
        ...acc.tranIds,
        ...tranDetails.payload[k].transactions.map(t => t.activityId)
      ]
      const tranInfo = [...acc.tranInfo, ...tranDetails.payload[k].transactions]
      return { tranIds, tranInfo }
    },
    { tranIds: [], tranInfo: [] }
  )

  // console.log("1.AFTER GETTING ELIGIBLE TRANSACTIONS",JSON.stringify(allTransactions.tranIds,null,2))

  // Get only the invoices that have been added for these transactions
  const invoiceDetails = await EntityRel.aggregate([
    { $match: { entityParty2: ObjectID(userEntityId), relationshipType:{$in:["Client","Self","Prospect","Third Party"]} } },
    { $project: { faEntityId: "$entityParty1", relationshipType: 1 } },
    {
      $lookup: {
        from: "tenantservicebillings",
        localField: "faEntityId",
        foreignField: "activityTranFirmId",
        as: "invoice"
      }
    },
    {
      $unwind: "$invoice"
    },
    {
      $match: { "invoice.activityId": { $in: [...allTransactions.tranIds] } }
    },
    {
      $project: {
        invoice: 1,
        activityId: "$invoice.activityId"
      }
    }
  ])

  const entityDetails = await EntityRel.aggregate([
    { $match: { entityParty2: ObjectID(userEntityId),relationshipType:{$in:["Client","Self","Prospect","Third Party"]} } },
    { $project: { faEntityId: "$entityParty1", relationshipType: 1 } },
    {
      $lookup: {
        from: "entityrels",
        localField: "faEntityId",
        foreignField: "entityParty1",
        as: "relatedentities"
      }
    },
    {
      $unwind: "$relatedentities"
    },
    {
      $project: {
        faEntityId: 1,
        relatedEntityId: "$relatedentities.entityParty2",
        relationshipTye: "$relatedentities.relationshipType"
      }
    },
    {
      $lookup: {
        from: "entities",
        localField: "relatedEntityId",
        foreignField: "_id",
        as: "entitydetails"
      }
    },
    {
      $unwind: "$entitydetails"
    },
    {
      $project: {
        tenantId: "$faEntityId",
        relatedEntityId: 1,
        relationshipTye: 1,
        firmName: {
          $cond: {
            if: { $eq: ["$entitydetails.firmName", null] },
            then: "$entitydetails.firmName",
            else: "$entitydetails.msrbFirmName"
          }
        }
        // firmName:{$cond:{if:{"$entitydetails.msrbFirmName":null},then:"$entitydetails.firmName", else:"$entitydetails.msrbFirmName"}}
      }
    }
  ])
  const newEntityDetails = entityDetails.reduce(
    (acc, a) => ({ ...acc, [a.relatedEntityId]: a }),
    {}
  )

  console.log("----ENTITY DETAILS", JSON.stringify(newEntityDetails, null, 2))

  const { tranInfo } = allTransactions
  const consolidtedTranAndInvoiceDetails = tranInfo.reduce((rtranInfo, t) => {
    const {
      activityId,
      activityType,
      activitySubType,
      activityTranFirmId,
      activityTranClientId,
      activityDescription
    } = t
    const inv = invoiceDetails.reduce((allinv, inv) => {
      if (inv.activityId.toString() === t.activityId.toString()) {
        return [...allinv, inv.invoice]
      }
      return [...allinv]
    }, [])
    const activityTranFirmName =
      newEntityDetails[activityTranFirmId] &&
      newEntityDetails[activityTranFirmId].firmName
    const activityTranClientName =
      newEntityDetails[activityTranClientId] &&
      newEntityDetails[activityTranClientId].firmName

    return [
      ...rtranInfo,
      {
        activityId,
        activityType: activityType || "NA",
        activitySubType: activitySubType || "NA",
        activityTranFirmId,
        activityTranClientId,
        activityDescription: activityDescription || `${activityId}`,
        activityTranClientName,
        activityTranFirmName,
        invoice: inv
      }
    ]
  }, [])

  console.log(
    "3. CONSOLIDATED INFORMATION:",
    JSON.stringify(consolidtedTranAndInvoiceDetails, null, 2)
  )

  res.send({
    done: true,
    message: "successfully fetched transactions and their related invoices",
    data: consolidtedTranAndInvoiceDetails,
    tranInfo: allTransactions.tranInfo
  })
}

export const getBillingSummaryInformationForUser = () => async (
  user,
  invoiceId
) => {
  // Have a method to find if the user has priviledge for this transaction
  // Does the user have admin privileges
  const groupByFields = ["expenseType", "expenseYear", "expenseMonth"]
  const gpFieldObject = groupByFields.reduce(
    (acc, g) => ({ ...acc, [g]: `$${g}` }),
    {}
  )

  console.log(gpFieldObject)

  const billingAggregateForInvoice = TenantBilling.aggregate([
    { $match: { _id: ObjectID(invoiceId) } },
    { $unwind: "$outOfPocketUserExpenses" },
    { $match: { "outOfPocketUserExpenses.tenantUserId": ObjectID(user) } },
    {
      $project: {
        tenantUserId: "$outOfPocketUserExpenses.tenantUserId",
        userName: "$outOfPocketUserExpenses.userFirstName",
        expenses: "$outOfPocketUserExpenses.outOfPocketExpenses",
        issuerClientEntityName: 1,
        activityType: 1,
        activityDescription: 1
      }
    },
    { $unwind: "$expenses" },
    {
      $project: {
        issuerClientEntityName: 1,
        activityType: 1,
        activityDescription: 1,
        tenantUserId: 1,
        invoiceId: "$_id",
        userName: 1,
        expenses: 1,
        expenseType: "$expenses.expenseType",
        expenseAmount: "$expenses.expenseAmount",
        expenseDate: "$expenses.expenseDate",
        expenseYear: { $year: "$expenses.expenseDate" },
        expenseMonth: { $month: "$expenses.expenseDate" }
      }
    },
    {
      $group: {
        // _id:{expenseType:"$expenseType",tenantUserId:"$tenantUserId",invoiceId:"$invoiceId",expenseYear:"$expenseYear",expenseMonth:"$expenseMonth"},
        _id: { ...gpFieldObject },
        totalAmount: { $sum: "$expenseAmount" },
        expenses: { $push: "$expenses" }
      }
    }
  ])

  return billingAggregateForInvoice
}

const getAllBillingDetails = () => async (req, res) => {}

const postFAServiceDataForType = () => async (req, res) => {
  const { faservid, feetype } = req.params
  if (!faservid || !feetype || !req.body) {
    res.send({
      done: false,
      message: `Not all parameters passed to get FeeService config for tran:${faservid}, feeType:${feetype}`,
      data: null
    })
  }
  const findQuery = { _id: ObjectID(faservid) }

  try {
    const returnedDoc = await TenantBilling.findOneAndUpdate(
      { ...findQuery },
      { $set: { [feetype]: req.body } },
      { upsert: true, new: true }
    )
    res.send({
      done: true,
      message: `Updated data for FeeService config :${faservid}, FeeId:${feetype}`,
      data: returnedDoc[feetype]
    })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: `Unable to update Information for FeeService config :${faservid}, FeeId:${feetype}, something went wrong`,
      data: null
    })
  }
}

const getFAServiceDataForType = () => async (req, res) => {
  const { faservid, feetype } = req.params
  if (!faservid || !feetype) {
    res.send({
      done: false,
      message: `Values Passed FeeService config for tran:${faservid}, feeType:${feetype}`,
      data: null
    })
  }
  const findQuery = { _id: ObjectID(faservid) }
  console.log("-----FIND QUERY", findQuery)
  try {
    const returnedDoc = await TenantBilling.findOne({ ...findQuery })
    console.log("----RETURNED DOCUMENT", JSON.stringify(returnedDoc, null, 2))
    res.send({
      done: true,
      message: `Update data Service config for tran:${faservid}, feeType:${feetype}`,
      data: returnedDoc[feetype]
    })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: `Unable to update eeService config for tran:${faservid}, feeType:${feetype}, something went wrong`,
      data: null
    })
  }
}

const postFAServiceDataForFeeTypeAndId = () => async (req, res) => {
  // Check if feetype and feeid are passed as part of the service
  console.log("THE PARAMETERS PASSED", req.params)
  const { feetype, feeId } = req.params

  if (!feetype || !feeId || !req.body) {
    res.send({
      done: false,
      message: `Insufficent Values Passed Feetype:${feetype}, FeeId:${feeId}`,
      data: null
    })
  }

  // Construct the Update Object
  const keys = Object.keys(req.body)
  const updateObject = keys.reduce(
    (acc, k) => ({ ...acc, [`${feetype}.$.${k}`]: req.body[k] }),
    {}
  )
  console.log(JSON.stringify(updateObject, null, 2))

  // Construct the find query
  const findQuery = { [`${feetype}._id`]: ObjectID(feeId) }
  console.log(JSON.stringify(findQuery, null, 2))
  // Update the document for that specific ID.
  try {
    const returnedDoc = await TenantBilling.findOneAndUpdate(
      { ...findQuery },
      { $set: { ...updateObject } },
      { upsert: true, new: true }
    )
    res.send({
      done: true,
      message: `Update data for Feetype:${feetype}, FeeId:${feeId}`,
      data: returnedDoc[feetype]
    })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: `Unable to update Information for Feetype:${feetype}, FeeId:${feeId}, something went wrong`,
      data: null
    })
  }
}

const getFAServiceFee = () => async (req, res) => {
  const { entityId, activityId } = req.query
  if (!entityId || !activityId) {
    res.send({
      done: false,
      message: "Incorrect Information has been passed for Insert",
      data: null
    })
  }
  try {
    const returnedDoc = await TenantBilling.findOne({
      activityTranClientId: ObjectID(entityId),
      activityId: ObjectID(activityId)
    }).select(
      "finAdvisorEntityId activityId invoiceNumber contractRef documents digitize sof nonTranFees retAndEsc notes createdAt"
    )
    res.send({ done: true, message: "Fetch data", data: returnedDoc })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: "Unable to fetch Information, something went wrong",
      data: null
    })
  }
}

const postFAServiceData = () => async (req, res) => {
  console.log(JSON.stringify(req.body, null, 2))
  // get the financial adviosor and activity ID details
  // Identify the pay Load
  const { finAdvisorEntityId, activityId, ...updateData } = req.body
  console.log(JSON.stringify(updateData, null, 2))
  if (!finAdvisorEntityId || !activityId) {
    res.send({
      done: false,
      message: "Incorrect Information has been passed for Insert",
      data: null
    })
  }
  try {
    const returnedDoc = await TenantBilling.findOneAndUpdate(
      {
        finAdvisorEntityId: ObjectID(finAdvisorEntityId),
        activityId: ObjectID(activityId)
      },
      {
        ...updateData
      },
      {
        upsert: true,
        new: true
      }
    )
    res.send({
      done: true,
      message: "Updated / Inserted data",
      data: returnedDoc
    })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: "Unable to update Information, something went wrong",
      data: null
    })
  }
}

const putFAServiceFee = () => async (req, res) => {
  const { entityId, activityId } = req.query
  if (!entityId || !activityId) {
    res.send({
      done: false,
      message: "Incorrect Information has been passed for Insert",
      data: null
    })
  }
  try {
    const returnedDoc = await TenantBilling.findOneAndUpdate(
      {
        activityTranClientId: ObjectID(entityId),
        activityId: ObjectID(activityId)
      },
      {
        ...req.body
      },
      {
        upsert: true,
        new: true
      }
    ).select(
      "finAdvisorEntityId contractRef activityId invoiceNumber documents digitize sof nonTranFees retAndEsc notes createdAt"
    )
    res.send({
      done: true,
      message: "Updated / Inserted data",
      data: returnedDoc
    })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: "Unable to update Information, something went wrong",
      data: null
    })
  }
}

const getConsolidatedActivityDetailsForBilling = () => async (req, res) => {
  const { clientId } = req.params
  const clientDummyDeals = {
    "123": {
      clientName: "City of Tampa",
      deals: [
        {
          tranType: "Deal Issues",
          tranSubType: "Deal Issues",
          securityType: "General Obligation Bonds",
          eligibleBillingAmt: 90000,
          tranStatus: "Open"
        },
        {
          tranType: "Deal Issues",
          tranSubType: "Deal Issues",
          securityType: "Revenue BOnds",
          eligibleBillingAmt: 90000,
          tranStatus: "Closed"
        }
      ]
    },
    "456": {
      clientName: "City of New York",
      deals: [
        {
          tranType: "Deal Issues",
          tranSubType: "Deal Issues",
          securityType: "General Obligation Bonds",
          eligibleBillingAmt: 90000,
          tranStatus: "Open"
        },
        {
          tranType: "Deal Issues",
          tranSubType: "Deal Issues",
          securityType: "Revenue BOnds",
          eligibleBillingAmt: 90000,
          tranStatus: "Open"
        }
      ]
    }
  }
  const allClientTransactions = clientDummyDeals[clientId] || []
  res.send({
    done: true,
    message: "Extracted Transactions For Client",
    trans: allClientTransactions
  })
}

// Tsted and certfi
const getUserExpensesForInvoiceLoggedInUser = () => async (req, res) => {
  const { invoiceid, userId } = req.params
  if (!invoiceid && !userId) {
    res
      .status(204)
      .send({
        done: false,
        message: `Invoice ID - ${invoiceid} or User ID -${userId} has not been passed`
      })
  }
  const findQuery = {
    _id: ObjectID(invoiceid),
    "outOfPocketUserExpenses.tenantUserId": ObjectID(userId)
  }
  console.log("-----FIND QUERY", findQuery)
  try {
    const returnedDoc = await TenantBilling.aggregate([
      { $match: { ...findQuery } },
      { $unwind: "$outOfPocketUserExpenses" },
      { $match: { "outOfPocketUserExpenses.tenantUserId": ObjectID(userId) } },
      { $project: { userData: "$outOfPocketUserExpenses" } }
    ])
    console.log(
      "THE VALUE RETURNED IS",
      JSON.stringify(returnedDoc[0], null, 2)
    )

    if (returnedDoc.length > 0) {
      res
        .status(200)
        .send({
          done: true,
          message: `Found data for invoice ${invoiceid} or User ID -${userId}`,
          data: returnedDoc[0] || {}
        })
    } else {
      res
        .status(200)
        .send({
          done: false,
          message: `No Data Found for ${invoiceid} or User ID -${userId}`,
          data: {}
        })
    }
  } catch (e) {
    console.log(e)
    res
      .status(400)
      .send({
        done: false,
        message: `Unable to process data for invoice ${invoiceid} or User ID -${userId}`,
        data: []
      })
  }
}

const postUserExpensesForInvoiceLoggedInUser = () => async (req, res) => {
  console.log(req.params)

  const { invoiceid, userId } = req.params
  if (!invoiceid && !userId) {
    res
      .status(204)
      .send({
        done: false,
        message: `Invoice ID - ${invoiceid} or User ID -${userId} has not been passed`
      })
  }
  const findQuery = {
    _id: ObjectID(invoiceid),
    "outOfPocketUserExpenses.tenantUserId": ObjectID(userId)
  }
  const findQueryNotExists = {
    _id: ObjectID(invoiceid),
    "outOfPocketUserExpenses.tenantUserId": { $ne: ObjectID(userId) }
  }

  // const findQuery = { _id:ObjectID(invoiceid)}
  console.log("-----FIND QUERY", findQuery)

  // TODO : Get the User details and add information
  const insertObject = {
    tenantUserId: ObjectID(userId),
    userFirstName: "Naveen",
    userLastName: "Balawat",
    outOfPocketExpenses: req.body
  }

  const keys = Object.keys(insertObject)
  const updateObject = keys.reduce(
    (acc, k) => ({
      ...acc,
      [`outOfPocketUserExpenses.$.${k}`]: insertObject[k]
    }),
    {}
  )
  console.log(JSON.stringify(updateObject, null, 2))

  // The assumption is that the OutofPocketExpenses will be available on the object to push, pull and add to set.
  try {
    let returnedDoc
    returnedDoc = await TenantBilling.findOneAndUpdate(
      { ...findQueryNotExists },
      { $push: { outOfPocketUserExpenses: insertObject } },
      { new: true }
    )

    if (!returnedDoc) {
      returnedDoc = await TenantBilling.findOneAndUpdate(
        { ...findQuery },
        { $set: { "outOfPocketUserExpenses.$": insertObject } },
        { new: true }
      )
    }
    console.log("----RETURNED DOCUMENT", JSON.stringify(returnedDoc, null, 2))
    if (returnedDoc) {
      res
        .status(200)
        .send({
          done: true,
          message: `Updated all expenses for invoice - ${invoiceid} - ${userId}`,
          data: returnedDoc
        })
    } else {
      res
        .status(204)
        .send({
          done: false,
          message: `Failed to Update Information - ${invoiceid} - ${userId}`,
          data: []
        })
    }
  } catch (e) {
    console.log(e)
    res
      .status(400)
      .send({
        done: false,
        message: `Unable to process data for invoice ${invoiceid} or User ID -${userId}`,
        data: []
      })
  }
}

const postOneExpenseForInvoiceAndLoggedInUser = () => async (req, res) => {
  // Find the user who is adding expense for the invoice.
  // Get the expenses object
  // check if there is an ID on the expense and try to match
  // check if the array exists. if it does then do something about it.
  // If a match is found then just update that expense.
  // if no match is found then append to the array.

  const { invoiceid, userId } = req.params
  if (!invoiceid || !userId || !req.body) {
    res
      .status(204)
      .send({
        done: false,
        message: `Invoice ID - ${invoiceid} or User ID -${userId} or payload has not been passed`
      })
  }
  const { _id: expenseId } = req.body

  // Get the User Details from the User Collection
  const userDetails = await EntityUser.aggregate([
    { $match: { _id: ObjectID(userId) } },
    {
      $project: {
        tenantUserId: "$_id",
        userFirstName: "$userFirstName",
        userLastName: "$userLastName"
      }
    }
  ])

  const userInfo = userDetails[0]

  const findQuery = {
    _id: ObjectID(invoiceid),
    "outOfPocketUserExpenses.tenantUserId": ObjectID(userId)
  }
  const findQueryNotExists = {
    _id: ObjectID(invoiceid),
    "outOfPocketUserExpenses.tenantUserId": { $ne: ObjectID(userId) }
  }

  try {
    const allUserExpenses = await TenantBilling.aggregate([
      { $match: { ...findQuery } },
      { $unwind: "$outOfPocketUserExpenses" },
      { $match: { "outOfPocketUserExpenses.tenantUserId": ObjectID(userId) } },
      {
        $project: {
          outOfPocketExpenses: "$outOfPocketUserExpenses.outOfPocketExpenses",
          tenantUserId: "$outOfPocketUserExpenses.tenantUserId",
          userFirstName: "$outOfPocketUserExpenses.userFirstName",
          userLastName: "$outOfPocketUserExpenses.userLastName"
        }
      }
    ])

    console.log(JSON.stringify(allUserExpenses[0], null, 2))
    let updateObject

    if (allUserExpenses.length > 0) {
      const { outOfPocketExpenses } = allUserExpenses[0]
      if (outOfPocketExpenses.length > 0 && expenseId) {
        // construct a new expense object
        console.log("---1.EXPENSES EXIST AND PAYLOAD HAS EXPENSE ID")
        updateObject = outOfPocketExpenses.reduce((acc, existingExp) => {
          if (existingExp._id.toString() === ObjectID(expenseId).toString()) {
            return [...acc, req.body]
          }
          return [...acc, existingExp]
        }, [])
        console.log(JSON.stringify(updateObject, null, 2))
      } else if (outOfPocketExpenses.length === 0 && !expenseId) {
        // this is a new array with element that needs to inserted.
        updateObject = [{ ...req.body }]
        console.log("---2.NO EXPENSES AND NO ID IN PAYLOAD")
        console.log(JSON.stringify(updateObject, null, 2))
      } else if (outOfPocketExpenses.length === 0 && expenseId) {
        // this is the same case as above so it can be collapsed
        const { _id, ...otherData } = req.body
        updateObject = [{ ...otherData }]
        console.log("---3.NO EXPENSES AND ID IN PAYLOAD")
        console.log(JSON.stringify(updateObject, null, 2))
      } else if (outOfPocketExpenses.length > 0 && !expenseId) {
        // Just push the document into the array
        updateObject = [...outOfPocketExpenses, req.body]
        console.log("---4. EXPENSES AND NO ID IN PAYLOAD")
        console.log(JSON.stringify(updateObject, null, 2))
      }
      await TenantBilling.findOneAndUpdate(
        { ...findQuery },
        {
          $set: {
            "outOfPocketUserExpenses.$.outOfPocketExpenses": updateObject
          }
        },
        { new: true }
      )
    } else {
      // Nothing exists for the user and we have to insert something for the user
      console.log(
        "WHY IS THAT THE USER OBJECT WAS NOT GENERATED",
        JSON.stringify({ userDetails, userInfo }, null, 2)
      )
      const insertObject = {
        ...userInfo,
        outOfPocketExpenses: [req.body]
      }
      console.log("NEED TO INSERT ", insertObject)

      let returnedDoc = await TenantBilling.findOneAndUpdate(
        { ...findQueryNotExists },
        { $push: { outOfPocketUserExpenses: insertObject } },
        { new: true }
      )

      if (!returnedDoc) {
        returnedDoc = await TenantBilling.findOneAndUpdate(
          { ...findQuery },
          { $set: { "outOfPocketUserExpenses.$": insertObject } },
          { new: true }
        )
      }
    }
    const returnedDoc = await TenantBilling.aggregate([
      { $match: { ...findQuery } }
      // {$unwind:"$outOfPocketUserExpenses"},
      // {$match:{"outOfPocketUserExpenses.tenantUserId":ObjectID(userId)}},
      // {$project:{userData:"$outOfPocketUserExpenses"}}
    ])
    res
      .status(200)
      .send({
        done: true,
        message: `Added / Updated Invoice  - ${invoiceid} - ${userId}`,
        data: returnedDoc[0]
      })
  } catch (e) {
    console.log(e)
    res
      .status(200)
      .send({
        done: false,
        message: `Failed to Upload payload  - ${invoiceid} - ${userId}`,
        data: null
      })
  }
}

const pullExpenseDetails = () => async (req, res) => {
  console.log(req.params, req.query)
  const { invoiceid, userId } = req.params
  const { id } = req.query

  const findQuery = {
    _id: ObjectID(invoiceid),
    "outOfPocketUserExpenses.tenantUserId": ObjectID(userId)
  }

  try {
    await TenantBilling.update(
      { ...findQuery },
      {
        $pull: {
          "outOfPocketUserExpenses.$.outOfPocketExpenses": { _id: ObjectID(id) }
        }
      }
    )
    const billingInfo = await TenantBilling.findOne({ ...findQuery })
    res
      .status(200)
      .send({
        done: true,
        message: "Successfully removed billing items from the chosen user",
        success: "",
        data: billingInfo
      })
  } catch (error) {
    console.log(error)
    res
      .status(200)
      .send({
        done: false,
        message:
          "There is a failure in retrieving information for entitlements",
        success: "",
        data: null
      })
  }
}

const getAllInvoicesForSearch = () => async (req, res) => {
  console.log("ENTERED THE FUNCTION", req.query)
  let { searchterm } = req.query
  const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
  const newSearchString = searchterm.replace(invalid, "")
  searchterm = new RegExp(newSearchString,"i")

  const query = [
    { $match: { activityTranFirmId: ObjectID(req.user.entityId) } },
    {
      $addFields: {
        keySearchDetails: {
          $concat: [
            { $ifNull: ["$activityClientName", ""] },
            { $ifNull: ["$activityTranFirmName", ""] },
            { $ifNull: ["$activityDescription", ""] },
            { $ifNull: ["$invoiceNumber", ""] },
            { $ifNull: ["$activityTranType", ""] },
            { $ifNull: ["$activityTranSubType", ""] },
            { $ifNull: ["$invoiceStatus", ""] }
          ]
        }
      }
    },
    {
      $match: {
        $or: [{ keySearchDetails: { $regex: searchterm, $options: "i" } }]
      }
    }
    /* {
      $unwind: "$outOfPocketUserExpenses"
    },
    {
      $addFields:{"totalUserExpenses":{$sum:"$outOfPocketUserExpenses.outOfPocketExpenses.expenseAmount"}}
    },
    {
      $project:{
        "invoiceNumber": 1,
        "activityId": 1,
        "activityTranType": 1,
        "activityTranSubType": 1,
        "activityTranFirmId": 1,
        "activityTranClientId": 1,
        "activityDescription": 1,
        "activityClientName": 1,
        "invoiceStatus":1,
        userId:"$outOfPocketUserExpenses.tenantUserId",
        userExpenses:"$totalUserExpenses"
      }
    },
    {
      $group:{
        _id:{invoiceDbId:"$_id",
          invoiceNumber:"$invoiceNumber",
          activityId:"$activityId",
          activityDescription: "$activityDescription",
          activityClientName:"$activityClientName",
          activityTranType:"$activityTranType,",
          activityTranSubType:"$activityTranSubType",
          activityTranFirmId:"$activityTranFirmId",
          invoiceStatus:"$invoiceStatus",
          activityTranClientId:"$activityTranClientId"
        },
        totalOutOfPocketExpenses:{$sum:"$userExpenses"}
      }
    },
    {
      $project:{
        _id:"$_id.invoiceDbId",
        invoiceNumber:"$_id.invoiceNumber",
        activityId:"$_id.activityId",
        activityDescription:"$_id.activityDescription",
        activityClientName:"$_id.activityClientName",
        activityTranType:"$_id.activityTranType",
        activityTranSubType:"$_id.activitySubType",
        invoiceStatus:"$_id.invoiceStatus",
        activityTranFirmId:"$_id.activityTranFirmId",
        activityTranClientId:"$_id.activityTranClientId",
        totalOutOfPocketExpenses:1
      }
    } */
  ]

  const finalQuery = [...query]
  if (searchterm.length === 0) {
    finalQuery.splice(2, 1)
  }
  const searchResult1 = await TenantBilling.aggregate(finalQuery)
  // const searchResult = await TenantBilling.find({ activityTranFirmId: req.user.entityId })
  res.send({
    done: true,
    message: "Successfully fetched data for Invoice Search",
    data: searchResult1
  })
}

const updateInvoiceStatus = () => async (req, res) => {
  const { invoiceid, status } = req.body
  if (!invoiceid) {
    res
      .status(204)
      .send({
        done: false,
        message: `Invoice ID - ${invoiceid} has not been passed`
      })
  }

  const findQuery = { _id: ObjectID(invoiceid) }

  try {
    const returnedDoc = await TenantBilling.findOneAndUpdate(
      { ...findQuery },
      { $set: { invoiceStatus: status } },
      { upsert: false, new: true }
    )
    res.send({
      done: true,
      message: `Updated data for Status from Dashboard for invoice - ${invoiceid}`,
      data: returnedDoc
    })
  } catch (e) {
    console.log(e)
    res.send({
      done: false,
      message: `Unable to update Status from Billing dashboard for invoice- ${invoiceid} something went wrong`,
      data: null
    })
  }
}

const getAllFirmUserExpenses = () => async (req, res) => {}

const postAllFirmUserExpenses = () => async (req, res) => {}

const addUserOutofPocketExpense = () => async (req, res) => {}

const modifyOutOfPocketexpense = () => async (req, res) => {}

const getConsolidatedContractsForBilling = () => async (req, res) => {
  const { clientId } = req.params
  const clientDummyContractDetails = {
    123: [
      {
        contractId: "CNTR-NEWREL",
        contractName: "CLIENT CONTRACT 2018",
        contractType: "Schedule Of Fees",
        securityType: "General Obligation Bonds",
        contractStartDate: "2018-01-01",
        contractEndDate: "2018-12-31",
        sof: [
          {
            sofDesc: "Fee Per",
            sofAmt: 10000,
            sofDealType: "Negotiated Sale",
            sofMin: 1,
            sofMax: 1.24
          },
          {
            sofDesc: "First",
            sofAmt: 25000000,
            sofDealType: "Competitive Bid",
            sofMin: 1,
            sofMax: 1.24
          }
        ],
        nonTranFees: [
          {
            role: "Project Manager",
            stdHourlyRate: 120,
            quoatedRate: 150
          }
        ],
        retAndEsc: [
          {
            type: "Retainer",
            hoursCovered: 150,
            effStartDate: "2018-01-01",
            effEndDate: "9999-12-31"
          }
        ]
      },
      {
        contractId: "CNTR-NEWREL-REVISED",
        contractName: "CLIENT CONTRACT 2017",
        contractType: "RETAINER AGREEMENT",
        securityType: "General Obligation Bonds",
        contractStartDate: "2017-01-01",
        contractEndDate: "2011-12-31",
        sof: [
          {
            sofDesc: "Fee Per",
            sofAmt: 10000,
            sofDealType: "Negotiated Sale",
            sofMin: 1.1,
            sofMax: 1.45
          },
          {
            sofDesc: "First",
            sofAmt: 25000000,
            sofDealType: "Competitive Bid",
            sofMin: 1.2,
            sofMax: 1.5
          }
        ],
        nonTranFees: [
          {
            role: "Project Manager",
            stdHourlyRate: 140,
            quoatedRate: 150
          }
        ],
        retAndEsc: [
          {
            type: "Retainer",
            hoursCovered: 150,
            effStartDate: "2018-01-01",
            effEndDate: "9999-12-31"
          }
        ]
      }
    ],

    456: [
      {
        contractId: "CNTR-NEWREL",
        contractName: "CLIENT CONTRACT 2018 - CLIENT 2",
        contractType: "Schedule Of Fees",
        securityType: "General Obligation Bonds",
        contractStartDate: "2018-01-01",
        contractEndDate: "2018-12-31",
        sof: [
          {
            sofDesc: "Fee Per",
            sofAmt: 10000,
            sofDealType: "Negotiated Sale",
            sofMin: 1,
            sofMax: 1.24
          },
          {
            sofDesc: "First",
            sofAmt: 25000000,
            sofDealType: "Competitive Bid",
            sofMin: 1,
            sofMax: 1.24
          }
        ],
        nonTranFees: [
          {
            role: "Project Manager",
            stdHourlyRate: 120,
            quoatedRate: 150
          }
        ],
        retAndEsc: [
          {
            type: "Retainer",
            hoursCovered: 150,
            effStartDate: "2018-01-01",
            effEndDate: "9999-12-31"
          }
        ]
      },
      {
        contractId: "CNTR-NEWREL-REVISED - CLIENT 2",
        contractName: "CLIENT CONTRACT 2017 - CLIENT 2",
        contractType: "RETAINER AGREEMENT",
        securityType: "General Obligation Bonds",
        contractStartDate: "2017-01-01",
        contractEndDate: "2011-12-31",
        sof: [
          {
            sofDesc: "Fee Per",
            sofAmt: 10000,
            sofDealType: "Negotiated Sale",
            sofMin: 1.1,
            sofMax: 1.45
          },
          {
            sofDesc: "First",
            sofAmt: 25000000,
            sofDealType: "Competitive Bid",
            sofMin: 1.2,
            sofMax: 1.5
          }
        ],
        nonTranFees: [
          {
            role: "Project Manager",
            stdHourlyRate: 140,
            quoatedRate: 150
          }
        ],
        retAndEsc: [
          {
            type: "Retainer",
            hoursCovered: 150,
            effStartDate: "2018-01-01",
            effEndDate: "9999-12-31"
          }
        ]
      }
    ]
  }
  const allClientContracts = clientDummyContractDetails[clientId] || []
  res.send({
    done: true,
    message: "Contracts Have been Extracted For Client",
    contracts: allClientContracts
  })
}

const getAllBillingDetailsForInvoice = () => async (req, res) => {}

const postBillingReceipts = () => async (req, res) => {}

const getBillingReceipts = () => async (req, res) => {}

const getAllTransactionsForInvoice = () => async (req, res) => {
  try {
    const { entityId } = req.query
    const clientId = (req.user && req.user.entityId) || ""

    const { relationshipType, userEntitlement} = req.user


    const invoices = await TenantBilling.find({
      activityTranFirmId: req.user.entityId
    })
    const invoiceIds = []
    invoices.forEach(invoice => invoiceIds.push(invoice.activityId))
    let findQuery = []

    if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
      findQuery = [{ _id: { $nin: invoiceIds } }]
    }
    else {
      const { data:{edit} } = await getAllAccessAndEntitlementsForLoggedInUser(
        req.user,
        "nav"
      )
      findQuery= [{ _id: { $in: edit } }, { _id: { $nin: invoiceIds } }]
    }


    const rfpSelect =
      "rfpTranType rfpTranSubType rfpTranClientId rfpTranClientFirmName rfpTranIssuerId rfpTranIssuerFirmName rfpTranIssueName " +
      "rfpTranAssignedTo rfpTranProjectDescription rfpParAmount"
    const dealSelect =
      "dealIssueTranType dealIssueTranSubType dealIssueTranClientId dealIssueTranClientFirmName dealIssueTranIssuerId dealIssueTranIssuerFirmName " +
      "dealIssueTranIssueName dealIssueTranAssignedTo dealIssueTranProjectDescription dealIssueParAmount"
    const loanSelect =
      "actTranType actTranSubType actTranFirmId actTranFirmName actTranClientId actTranClientName actTranIssueName actTranProjectDescription bankLoanTerms.parAmount " +
      "actTranFirmLeadAdvisorId actTranFirmLeadAdvisorName"
    const derivativeSelect =
      "actTranType actTranSubType actTranIssueName actTranFirmId actTranFirmName actTranClientId actTranClientName actTranProjectDescription " +
      " actTranFirmLeadAdvisorId actTranFirmLeadAdvisorName derivativeSummary.tranNotionalAmt"
    const maRfpSelect =
      "actType actSubType actTranFirmId actTranFirmName actIssuerClient actIssuerClientEntityName actIssueName actProjectName " +
      "actLeadFinAdvClientEntityId actLeadFinAdvClientEntityName maRfpSummary.actParAmount"
    const otherSelect =
      "actTranType actTranSubType actTranFirmId actTranFirmName actTranClientId actTranClientName actTranIssueName " +
      "actTranFirmLeadAdvisorId actTranFirmLeadAdvisorName actTranProjectDescription actTranParAmount"

    // let rfp = await RFP.find({ _id: { $nin: invoiceIds } , rfpTranClientId: clientId, rfpTranIssuerId: entityId }).populate({
    let rfp = await RFP.find({
      $and: [...findQuery],
      rfpTranIssuerId: entityId,
      // "opportunity.status": false
    })
      .populate([
        {
          path: "rfpTranAssignedTo",
          model: EntityUser,
          select: "userFirstName userLastName"
        },
        {
          path: "rfpTranIssuerId",
          model: Entity,
          select: "addresses"
        }
      ])
      .select(rfpSelect)
    rfp = rfp.map(r => {
      let address =
        (r.rfpTranIssuerId.addresses &&
          r.rfpTranIssuerId.addresses.find(e => e.isPrimary)) ||
        ""
      address = address || r.rfpTranIssuerId.addresses[0]
      return {
        activityId: r._id,
        activityDescription: r.rfpTranProjectDescription,
        activityTranType: r.rfpTranType,
        activityTranSubType: r.rfpTranSubType,
        activityTranFirmId: r.rfpTranClientId,
        activityTranFirmName: r.rfpTranClientFirmName,
        activityTranClientId: r.rfpTranIssuerId._id,
        activityClientName: r.rfpTranIssuerFirmName,
        activityClientAddress: address,
        activityParAmount: parseInt(r.rfpParAmount || 0, 10),
        activityLeadAdvisorId:
          (r.rfpTranAssignedTo &&
            r.rfpTranAssignedTo.length &&
            r.rfpTranAssignedTo[0]._id) ||
          "",
        activityLeadAdvisorName:
          (r.rfpTranAssignedTo &&
            r.rfpTranAssignedTo.length &&
            `${r.rfpTranAssignedTo[0].userFirstName || ""} ${r
              .rfpTranAssignedTo[0].userLastName || ""}`) ||
          ""
      }
    })

    // let deals = await Deals.find({ _id: { $nin: invoiceIds }, dealIssueTranClientId: clientId, dealIssueTranIssuerId: entityId}).populate({
    let deals = await Deals.find({
      $and: [...findQuery],
      dealIssueTranIssuerId: entityId,
      // "opportunity.status": false
    })
      .populate([
        {
          path: "dealIssueTranAssignedTo",
          model: EntityUser,
          select: "userFirstName userLastName"
        },
        {
          path: "dealIssueTranIssuerId",
          model: Entity,
          select: "addresses"
        }
      ])
      .select(dealSelect)
    deals = deals.map(r => {
      let address =
        (r.dealIssueTranIssuerId.addresses &&
          r.dealIssueTranIssuerId.addresses.find(e => e.isPrimary)) ||
        ""
      address = address || r.dealIssueTranIssuerId.addresses[0]
      return {
        activityId: r._id,
        activityDescription: r.dealIssueTranProjectDescription,
        activityTranType: r.dealIssueTranType,
        activityTranSubType: r.dealIssueTranSubType,
        activityTranFirmId: r.dealIssueTranClientId,
        activityTranFirmName: r.dealIssueTranClientFirmName,
        activityTranClientId: r.dealIssueTranIssuerId._id,
        activityClientName: r.dealIssueTranIssuerFirmName,
        activityClientAddress: address,
        activityParAmount: parseInt(r.dealIssueParAmount || 0, 10),
        activityLeadAdvisorId:
          (r.dealIssueTranAssignedTo &&
            r.dealIssueTranAssignedTo.length &&
            r.dealIssueTranAssignedTo[0]._id) ||
          "",
        activityLeadAdvisorName:
          (r.dealIssueTranAssignedTo &&
            r.dealIssueTranAssignedTo.length &&
            `${r.dealIssueTranAssignedTo[0].userFirstName || ""} ${r
              .dealIssueTranAssignedTo[0].userLastName || ""}`) ||
          ""
      }
    })

    // let loans = await BankLoans.find({ _id: { $nin: invoiceIds }, actTranFirmId: clientId, actTranClientId: entityId})
    let loans = await BankLoans.find({
      $and: [...findQuery],
      actTranClientId: entityId,
      // "opportunity.status": false
    })
      .populate({
        path: "actTranClientId",
        model: Entity,
        select: "addresses"
      })
      .select(loanSelect)
    loans = loans.map(r => {
      let address =
        (r.actTranClientId.addresses &&
          r.actTranClientId.addresses.find(e => e.isPrimary)) ||
        ""
      address = address || r.actTranClientId.addresses[0]
      return {
        activityId: r._id,
        activityDescription: r.actTranProjectDescription,
        activityTranType: r.actTranType,
        activityTranSubType: r.actTranSubType,
        activityTranFirmId: r.actTranFirmId,
        activityTranFirmName: r.actTranFirmName,
        activityTranClientId: r.actTranClientId._id,
        activityClientName: r.actTranClientName,
        activityClientAddress: address,
        activityParAmount: parseInt(
          (r.bankLoanTerms && r.bankLoanTerms.parAmount) || 0,
          10
        ),
        activityLeadAdvisorId:
          r.actTranFirmLeadAdvisorId || r.actLeadFinAdvClientEntityId || "",
        activityLeadAdvisorName:
          r.actTranFirmLeadAdvisorName || r.actLeadFinAdvClientEntityName || ""
      }
    })

    // let derivatives = await Derivatives.find({ _id: { $nin: invoiceIds }, actTranFirmId: clientId, actTranClientId: entityId})
    let derivatives = await Derivatives.find({
      $and: [...findQuery],
      actTranClientId: entityId,
      // "opportunity.status": false
    })
      .populate({
        path: "actTranClientId",
        model: Entity,
        select: "addresses"
      })
      .select(derivativeSelect)
    derivatives = derivatives.map(r => {
      let address =
        (r.actTranClientId.addresses &&
          r.actTranClientId.addresses.find(e => e.isPrimary)) ||
        ""
      address = address || r.actTranClientId.addresses[0]
      return {
        activityId: r._id,
        activityDescription: r.actTranProjectDescription,
        activityTranType: r.actTranType,
        activityTranSubType: r.actTranSubType,
        activityTranFirmId: r.actTranFirmId,
        activityTranFirmName: r.actTranFirmName,
        activityTranClientId: r.actTranClientId._id,
        activityClientName: r.actTranClientName,
        activityClientAddress: address,
        activityParAmount: parseInt(
          (r.derivativeSummary && r.derivativeSummary.tranNotionalAmt) || 0,
          10
        ),
        activityLeadAdvisorId:
          r.actTranFirmLeadAdvisorId || r.actLeadFinAdvClientEntityId || "",
        activityLeadAdvisorName:
          r.actTranFirmLeadAdvisorName || r.actLeadFinAdvClientEntityName || ""
      }
    })

    // let marfps = await ActMaRFP.find({ _id: { $nin: invoiceIds }, actTranFirmId: clientId, actIssuerClient: entityId})
    let marfps = await ActMaRFP.find({
      $and: [...findQuery],
      actIssuerClient: entityId,
      // "opportunity.status": false
    })
      .populate({
        path: "actIssuerClient",
        model: Entity,
        select: "addresses"
      })
      .select(maRfpSelect)
    marfps = marfps.map(r => {
      let address =
        (r.actIssuerClient.addresses &&
          r.actIssuerClient.addresses.find(e => e.isPrimary)) ||
        ""
      address = address || r.actIssuerClient.addresses[0]
      return {
        activityId: r._id,
        activityDescription: r.actProjectName,
        activityTranType: r.actType,
        activityTranSubType: r.actSubType,
        activityTranFirmId: r.actTranFirmId,
        activityTranFirmName: r.actTranFirmName,
        activityTranClientId: r.actIssuerClient._id,
        activityClientName: r.actIssuerClientEntityName,
        activityClientAddress: address,
        activityParAmount: parseInt(
          (r.maRfpSummary && r.maRfpSummary.actParAmount) || 0,
          10
        ),
        activityLeadAdvisorId:
          r.actTranFirmLeadAdvisorId || r.actLeadFinAdvClientEntityId || "",
        activityLeadAdvisorName:
          r.actTranFirmLeadAdvisorName || r.actLeadFinAdvClientEntityName || ""
      }
    })

    // let others = await Others.find({ _id: { $nin: invoiceIds }, actTranFirmId: clientId, actTranClientId: entityId})
    let others = await Others.find({
      $and: [...findQuery],
      actTranClientId: entityId,
      // "opportunity.status": false
    })
      .populate({
        path: "actTranClientId",
        model: Entity,
        select: "addresses"
      })
      .select(otherSelect)
    others = others.map(r => {
      let address =
        (r.actTranClientId.addresses &&
          r.actTranClientId.addresses.find(e => e.isPrimary)) ||
        ""
      address = address || r.actTranClientId.addresses[0]
      return {
        activityId: r._id,
        activityDescription: r.actTranProjectDescription,
        activityTranType: r.actTranType,
        activityTranSubType: r.actTranSubType,
        activityTranFirmId: r.actTranFirmId,
        activityTranFirmName: r.actTranFirmName,
        activityTranClientId: r.actTranClientId._id,
        activityClientName: r.actTranClientName,
        activityClientAddress: address,
        activityParAmount: parseInt(r.actTranParAmount || 0, 10),
        activityLeadAdvisorId:
          r.actTranFirmLeadAdvisorId || r.actLeadFinAdvClientEntityId || "",
        activityLeadAdvisorName:
          r.actTranFirmLeadAdvisorName || r.actLeadFinAdvClientEntityName || ""
      }
    })

    let transactions = rfp.concat(deals)
    transactions = transactions.concat(loans)
    transactions = transactions.concat(derivatives)
    transactions = transactions.concat(marfps)
    transactions = transactions.concat(others)

    res.json({ transactions, invoices: invoices || [] })
  } catch (error) {
    console.log(error)
    res.status(422).send({ done: false, error })
  }
}

const putInvoice = () => async (req, res) => {
  try {
    const { invoiceId } = req.body
    if (invoiceId) {
      await TenantBilling.updateOne({ _id: ObjectID(invoiceId) }, req.body)
    } else {
      if(req.body && req.body.activityId){

        const invoice =  await TenantBilling.findById(req.body.activityId).select("activityId")

        if(!invoice){
          const type = req.body && req.body.activityTranType
          const subType = req.body && req.body.activityTranSubType
          const modal = subType === "Bond Issue" ? Deals :
            type === "MA-RFP" ? ActMaRFP :
              type === "Derivative" ? Derivatives :
                (subType === "Bank Loan" || subType === "Lines and Letter" || subType === "Letter Of Credit" || subType === "Lines Of Credit") ? BankLoans :
                  type === "RFP" ? RFP :
                    type === "Others" ? Others : ""

          const transaction =  await modal.findById(req.body.activityId).select("contracts")
          if(transaction && transaction.contracts && transaction.contracts._id){
            delete transaction.contracts._id
            req.body = {...req.body, ...transaction.contracts}
          }
        } else {
          return res.status(500).send({ done: false, message: "invoice already generated!" })
        }

      }
      const newInvoice = new TenantBilling({
        ...req.body
      })
      await newInvoice.save()
    }

    const invoices = await TenantBilling.find({
      activityTranFirmId: req.user.entityId
    })
    res.json(invoices)
  } catch (error) {
    console.log("************createInvoice***************", error)
    res.status(500).send({ done: false, error })
  }
}

const getRevenuePipelineDetails = () => async (req, res) => {
  const resRequested = [
    { resource: "RFP", access: 1 },
    { resource: "EntityUser", access: 1 },
    { resource: "Deals", access: 1 }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const detailsIds = await getAllAccessAndEntitlementsForLoggedInUser(
      req.user,
      "nav"
    )
    if (!entitled) {
      return res.status(422).send({
        done: false,
        error: "User is not entitled to access the requested services",
        success: "",
        requestedServices: resRequested
      })
    }

    const isEligibleForView = (detailsIds && detailsIds.view) || []

    req.body = {
      "searchTerm":"",
      "status":[
        "In Progress",
        "Lost",
        "Cancel/Dropped",
        "Closed",
        "Executed but not closed",
        "Pre-execution",
        "Pre-Pricing"
      ],
      "client":[],
      "createdAt":"2018-01-01T00:00:00+05:30",
      "type": ["RFP", "Deals", "ActMaRFP", "Derivatives", "BankLoans", "Others"]
    }
    const { type, searchTerm, status, client, createdAt } = req.body
    const clientIds = client.map(id => mongoose.Types.ObjectId(id))
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchTerm.replace(invalid, "")
    const regexValue = new RegExp(newSearchString || "", "i")
    const dealQuery = {
      "trans._id": { $in: isEligibleForView }
    }
    const rfpQuery = {
      "trans._id": { $in: isEligibleForView }
    }
    const bankloanQuery = {
      "trans._id": { $in: isEligibleForView }
    }
    const maRfpQuery = {
      "trans._id": { $in: isEligibleForView }
    }
    const othersQuery = {
      "trans._id": { $in: isEligibleForView }
    }
    const derivativeQuery = {
      "trans._id": { $in: isEligibleForView }
    }
    const facet = {}

    if (type.indexOf("Deals") !== -1) {
      if (searchTerm) {
        dealQuery["trans.dealIssueTranProjectDescription"] = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        dealQuery["trans.dealIssueTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        dealQuery["trans.dealIssueTranIssuerId"] = { $in: clientIds }
      }
      if (createdAt) {
        dealQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }

      facet.deals = [
        {
          $lookup:{
            from: "tranagencydeals",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: dealQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.dealIssueTranStatus",
            activityDescription: "$trans.dealIssueTranProjectDescription",
            activityEstimatedRev: "$trans.dealIssueEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("RFP") !== -1) {
      if (searchTerm) {
        rfpQuery["trans.rfpTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        rfpQuery["trans.rfpTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        rfpQuery["trans.rfpTranIssuerId"] = { $in: clientIds }
      }
      if (createdAt) {
        rfpQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }

      facet.rfps = [
        {
          $lookup:{
            from: "tranagencyrfps",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: rfpQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.rfpTranStatus",
            activityDescription: "$trans.rfpTranProjectDescription",
            activityEstimatedRev: "$trans.rfpEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("ActMaRFP") !== -1) {
      if (searchTerm) {
        maRfpQuery["trans.actProjectName"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        maRfpQuery["trans.actStatus"] = { $in: status }
      }
      if (client && client.length) {
        maRfpQuery["trans.actIssuerClient"] =  { $in: clientIds }
      }
      if (createdAt) {
        maRfpQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      facet.maRfps = [
        {
          $lookup:{
            from: "actmarfps",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: maRfpQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actStatus",
            activityDescription: "$trans.actProjectName",
            activityEstimatedRev: "$trans.maRfpSummary.actEstRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("Derivatives") !== -1) {
      if (searchTerm) {
        derivativeQuery["trans.actTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        derivativeQuery["trans.actTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        derivativeQuery["trans.actTranClientId"] = { $in: clientIds }
      }
      if (createdAt) {
        derivativeQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      facet.derivatives = [
        {
          $lookup:{
            from: "tranderivatives",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: derivativeQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actTranStatus",
            activityDescription: "$trans.actTranProjectDescription",
            activityEstimatedRev: "$trans.derivativeSummary.tranEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("BankLoans") !== -1) {
      if (searchTerm) {
        bankloanQuery["trans.actTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        bankloanQuery["trans.actTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        bankloanQuery["trans.actTranClientId"] = { $in: clientIds }
      }
      if (createdAt) {
        bankloanQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      facet.bankloans = [
        {
          $lookup:{
            from: "tranbankloans",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: bankloanQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actTranStatus",
            activityDescription: "$trans.actTranProjectDescription",
            activityEstimatedRev: "$trans.bankLoanSummary.actTranEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("Others") !== -1) {
      if (searchTerm) {
        othersQuery["trans.actTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        othersQuery["trans.actTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        othersQuery["trans.actTranClientId"] = { $in: clientIds }
      }
      if (createdAt) {
        othersQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      facet.others = [
        {
          $lookup:{
            from: "tranagencyothers",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: othersQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actTranStatus",
            activityDescription: "$trans.actTranProjectDescription",
            activityEstimatedRev: "$trans.actTranEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    let invoices = await TenantBilling.aggregate([
      {
        $facet: facet
      }
    ])

    invoices = invoices.length ? invoices[0] : {}
    invoices = [
      ...(invoices.deals || []),
      ...(invoices.rfps || []),
      ...(invoices.maRfps || []),
      ...(invoices.bankloans || []),
      ...(invoices.derivatives || []),
      ...(invoices.others || [])
    ]

    const inProgress =
      invoices.filter(
        invoice =>
          invoice.invoiceStatus !== "Invoice Sent to Client" &&
          invoice.invoiceStatus !== "Invoice Paid"
      ) || []
    const invoiced =
      invoices.filter(
        invoice => invoice.invoiceStatus === "Invoice Sent to Client"
      ) || []
    const paid =
      invoices.filter(invoice => invoice.invoiceStatus === "Invoice Paid") || []

    let totalInProgress = 0
    let totalInvoiced = 0
    let totalPaid = 0

    inProgress.forEach(inPro => {
      if(inPro.opportunity){
        totalInProgress += parseInt(inPro.totalOverallExpenses || 0, 10)
      }else {
        totalInProgress += parseInt(inPro.activityEstimatedRev || 0, 10)
      }
    })

    invoiced.forEach(inPro => {
      totalInvoiced += parseInt(inPro.totalOverallExpenses || 0, 10)
    })
    paid.forEach(inPro => {
      totalPaid += parseInt(inPro.totalOverallExpenses || 0, 10)
    })

    res.json({
      invoices,
      inProgress,
      invoiced,
      paid,
      totalInProgress,
      totalInvoiced,
      totalPaid,
    })
  } catch (error) {
    console.log("======", error)

    res.status(422).send({
      done: false,
      error: "There is a failure in retrieving information for entitlements",
      success: "",
      requestedServices: resRequested
    })
  }
}

const flyForSearchInvoices = async (modal, query, project) => {
  try {
    const invoices = await modal.aggregate([
      {
        $match: query
      },
      {
        $lookup: {
          from: "tenantservicebillings",
          localField: "_id",
          foreignField: "activityId",
          as: "invoice"
        }
      },
      { $unwind: "$invoice" },
      {
        $project: project
      }
    ])
    return {
      done: true,
      invoices
    }
  } catch (error) {
    return {
      done: false,
      error: error.message
    }
  }
}

const filtterRevenuePipelineDetails = () => async (req, res) => {
  const resRequested = [
    { resource: "RFP", access: 1 },
    { resource: "EntityUser", access: 1 },
    { resource: "Deals", access: 1 }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    const { relationshipType, userEntitlement, tenantId} = req.user
    let isEligibleForView = []
    let spliceQuery = false

    if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
      spliceQuery = true
    }
    else {
      spliceQuery = true
      const { data:{view} } = await getAllAccessAndEntitlementsForLoggedInUser(
        req.user,
        "nav"
      )
      isEligibleForView = view || []
      isEligibleForView = isEligibleForView.map(id => mongoose.Types.ObjectId(id))
    }


    if (!entitled) {
      return res.status(422).send({
        done: false,
        error: "User is not entitled to access the requested services",
        success: "",
        requestedServices: resRequested
      })
    }

    const { searchTerm, status, client, createdAt, leadManager } = req.body
    let { type } = req.body
    if(type.indexOf("All") !== -1){
      type = ["Deals", "BankLoans", "RFP", "ActMaRFP", "Derivatives", "Others"]
    }
    const clientIds = client.map(id => mongoose.Types.ObjectId(id))
    const leadManagerIds = leadManager.map(id => mongoose.Types.ObjectId(id))
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchTerm.replace(invalid, "")
    const regexValue = new RegExp(newSearchString || "", "i")
    let dealQuery = {}
    let rfpQuery = {}
    let bankloanQuery ={}
    let maRfpQuery={}
    let othersQuery={}
    let derivativeQuery={}
    const queryForOppty = []


    if(!spliceQuery) {
      dealQuery = {
        "trans._id": { $in: isEligibleForView }
      }
      rfpQuery = {
        "trans._id": { $in: isEligibleForView }
      }
      bankloanQuery = {
        "trans._id": { $in: isEligibleForView }
      }
      maRfpQuery = {
        "trans._id": { $in: isEligibleForView }
      }
      othersQuery = {
        "trans._id": { $in: isEligibleForView }
      }
      derivativeQuery = {
        "trans._id": { $in: isEligibleForView }
      }
    }
    const facet = {}

    if (type.indexOf("Deals") !== -1) {
      if (searchTerm) {
        dealQuery["trans.dealIssueTranProjectDescription"] = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        dealQuery["trans.dealIssueTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        dealQuery["trans.dealIssueTranIssuerId"] = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        dealQuery["trans.dealIssueTranAssignedTo"] = { $in: leadManagerIds }
      }
      if (createdAt) {
        dealQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      dealQuery["trans.dealIssueTranClientId"] = ObjectID(tenantId)

      facet.deals = [
        {
          $lookup:{
            from: "tranagencydeals",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: dealQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.dealIssueTranStatus",
            activityDescription: "$trans.dealIssueTranProjectDescription",
            activityEstimatedRev: "$trans.dealIssueEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("RFP") !== -1) {
      if (searchTerm) {
        rfpQuery["trans.rfpTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        rfpQuery["trans.rfpTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        rfpQuery["trans.rfpTranIssuerId"] = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        rfpQuery["trans.rfpTranAssignedTo"] = { $in: leadManagerIds }
      }
      if (createdAt) {
        rfpQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      rfpQuery["trans.rfpTranClientId"] = ObjectID(tenantId)

      facet.rfps = [
        {
          $lookup:{
            from: "tranagencyrfps",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: rfpQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.rfpTranStatus",
            activityDescription: "$trans.rfpTranProjectDescription",
            activityEstimatedRev: "$trans.rfpEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("ActMaRFP") !== -1) {
      if (searchTerm) {
        maRfpQuery["trans.actProjectName"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        maRfpQuery["trans.actStatus"] = { $in: status }
      }
      if (client && client.length) {
        maRfpQuery["trans.actIssuerClient"] =  { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        maRfpQuery["trans.actLeadFinAdvClientEntityId"] =  { $in: leadManagerIds }
      }
      if (createdAt) {
        maRfpQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      maRfpQuery["trans.actTranFirmId"] = ObjectID(tenantId)

      facet.maRfps = [
        {
          $lookup:{
            from: "actmarfps",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: maRfpQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actStatus",
            activityDescription: "$trans.actProjectName",
            activityEstimatedRev: "$trans.maRfpSummary.actEstRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]

    }

    if (type.indexOf("Derivatives") !== -1) {
      if (searchTerm) {
        derivativeQuery["trans.actTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        derivativeQuery["trans.actTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        derivativeQuery["trans.actTranClientId"] = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        derivativeQuery["trans.actTranFirmLeadAdvisorId"] = { $in: leadManagerIds }
      }
      if (createdAt) {
        derivativeQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      derivativeQuery["trans.actTranFirmId"] = ObjectID(tenantId)

      facet.derivatives = [
        {
          $lookup:{
            from: "tranderivatives",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: derivativeQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actTranStatus",
            activityDescription: "$trans.actTranProjectDescription",
            activityEstimatedRev: "$trans.derivativeSummary",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("BankLoans") !== -1) {
      if (searchTerm) {
        bankloanQuery["trans.actTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        bankloanQuery["trans.actTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        bankloanQuery["trans.actTranClientId"] = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        bankloanQuery["trans.actTranFirmLeadAdvisorId"] = { $in: leadManagerIds }
      }
      if (createdAt) {
        bankloanQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      bankloanQuery["trans.actTranFirmId"] = ObjectID(tenantId)

      facet.bankloans = [
        {
          $lookup:{
            from: "tranbankloans",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: bankloanQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actTranStatus",
            activityDescription: "$trans.actTranProjectDescription",
            activityEstimatedRev: "$trans.bankLoanSummary.actTranEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    if (type.indexOf("Others") !== -1) {
      if (searchTerm) {
        othersQuery["trans.actTranProjectDescription"] = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        othersQuery["trans.actTranStatus"] = { $in: status }
      }
      if (client && client.length) {
        othersQuery["trans.actTranClientId"] = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        othersQuery["trans.actTranFirmLeadAdvisorId"] = { $in: leadManagerIds }
      }
      if (createdAt) {
        othersQuery["trans.createdAt"] = { $gte: new Date(createdAt) }
      }
      othersQuery["trans.actTranFirmId"] = ObjectID(tenantId)

      facet.others = [
        {
          $lookup:{
            from: "tranagencyothers",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: othersQuery },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityTranFirmId: 1,
            activityTranFirmName: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            totalOverallExpenses: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.actTranStatus",
            activityDescription: "$trans.actTranProjectDescription",
            activityEstimatedRev: "$trans.actTranEstimatedRev",
            opportunity: "$trans.opportunity",
            created_at: "$trans.created_at",
          }
        }
      ]
    }

    let invoices = []

    if(type && type.length){
      invoices = await TenantBilling.aggregate([
        {
          $facet: facet
        }
      ])
    }

    invoices = invoices.length ? invoices[0] : {}
    invoices = [
      ...(invoices.deals || []),
      ...(invoices.rfps || []),
      ...(invoices.maRfps || []),
      ...(invoices.bankloans || []),
      ...(invoices.derivatives || []),
      ...(invoices.others || [])
    ]

    const invoiceIds = []
    let dealOpp = []
    let rfpOpp = []
    let maRfpOpp = []
    let derivativeOpp = []
    let bankloanOpp = []
    let othersOpp = []
    let opportunities = []

    if (invoices) {
      invoices.forEach(invoice => invoiceIds.push(mongoose.Types.ObjectId(invoice.activityId)))
    } else {
      invoices = []
    }
    let findQueryForOpportunity =  []

    if (!spliceQuery){
      findQueryForOpportunity= [
        { _id: { $in: isEligibleForView } },
        { _id: { $nin: invoiceIds } }
      ]
    } else {
      findQueryForOpportunity= [
        { _id: { $nin: invoiceIds } }
      ]
    }


    if (type.indexOf("Deals") !== -1) {
      let dealOppQuery = {
        $and: [
          ...findQueryForOpportunity,
          {"dealIssueTranClientId":ObjectID(req.user.tenantId)}
        ],
        dealIssueTranStatus: { $nin: ["Cancelled"] }
      }
      if (searchTerm) {
        dealOppQuery.dealIssueTranProjectDescription = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        delete dealOppQuery.dealIssueTranStatus
        dealOppQuery = {
          ...dealOppQuery,
          $and: [
            ...dealOppQuery.$and,
            { dealIssueTranStatus: { $in: status } },
            { dealIssueTranStatus: { $nin: ["Cancelled"] } }
          ]
        }
      }
      if (client && client.length) {
        dealOppQuery.dealIssueTranIssuerId = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        dealOppQuery.dealIssueTranAssignedTo = { $in: leadManagerIds }
      }
      if (createdAt) {
        dealOppQuery.createdAt = { $gte: new Date(createdAt) }
      }

      dealOpp = await Deals.aggregate([
        {
          $match: dealOppQuery
        },
        {
          $lookup:{
            from: "entityusers",
            localField: "dealIssueTranAssignedTo",
            foreignField: "_id",
            as: "entityusers"
          }
        },
        {
          $unwind:"$entityusers"
        },
        {
          $project: {
            _id: 0,
            activityId: "$_id",
            activityTranFirmId: "$dealIssueTranClientId",
            activityTranFirmName: "$dealIssueTranClientFirmName",
            activityTranClientId: "$dealIssueTranIssuerId",
            activityClientName: "$dealIssueTranIssuerFirmName",
            activityTranType: "$dealIssueTranType",
            activityTranSubType: "$dealIssueTranSubType",
            activityDescription: "$dealIssueTranProjectDescription",
            activityEstimatedRev: "$dealIssueEstimatedRev",
            opportunity: "$opportunity",
            activityLeadAdvisorId: "$entityusers._id",
            activityLeadAdvisorFirstName: "$entityusers.userFirstName",
            activityLeadAdvisorLastName: "$entityusers.userLastName"
          }
        }
      ])

      opportunities = [...opportunities, ...dealOpp]
    }

    if (type.indexOf("RFP") !== -1) {
      let rfpOppQuery = {
        $and: [
          ...findQueryForOpportunity,
          {"rfpTranClientId":ObjectID(req.user.tenantId)}
        ],
        rfpTranStatus: { $nin: ["Cancelled"] }
      }
      if (searchTerm) {
        rfpOppQuery.rfpTranProjectDescription = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        delete rfpOppQuery.rfpTranStatus
        rfpOppQuery = {
          ...rfpOppQuery,
          $and: [
            ...rfpOppQuery.$and,
            { rfpTranStatus: { $in: status } },
            { rfpTranStatus: { $nin: ["Cancelled"] } }
          ]
        }
      }
      if (client && client.length) {
        rfpOppQuery.rfpTranIssuerId = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        rfpOppQuery.rfpTranAssignedTo = { $in: leadManagerIds }
      }
      if (createdAt) {
        rfpOppQuery.createdAt = { $gte: new Date(createdAt) }
      }

      rfpOpp = await RFP.aggregate([
        {
          $match: rfpOppQuery
        },
        {
          $lookup:{
            from: "entityusers",
            localField: "rfpTranAssignedTo",
            foreignField: "_id",
            as: "entityusers"
          }
        },
        {
          $unwind:"$entityusers"
        },
        {
          $project: {
            _id: 0,
            activityId: "$_id",
            activityTranFirmId: "$rfpTranClientId",
            activityTranFirmName: "$rfpTranClientFirmName",
            activityTranClientId: "$rfpTranIssuerId",
            activityClientName: "$rfpTranIssuerFirmName",
            activityTranType: "$rfpTranType",
            activityTranSubType: "$rfpTranSubType",
            activityDescription: "$rfpTranProjectDescription",
            activityEstimatedRev: "$rfpEstimatedRev",
            opportunity: "$opportunity",
            activityLeadAdvisorId: "$entityusers._id",
            activityLeadAdvisorFirstName: "$entityusers.userFirstName",
            activityLeadAdvisorLastName: "$entityusers.userLastName"
          }
        }
      ])

      opportunities = [...opportunities, ...rfpOpp]
    }

    if (type.indexOf("ActMaRFP") !== -1) {
      let maRfpOppQuery = {
        $and: [
          ...findQueryForOpportunity,
          {"actTranFirmId":ObjectID(req.user.tenantId)}
        ],
        actStatus: { $nin: ["Cancelled"] }
      }
      if (searchTerm) {
        maRfpOppQuery.actProjectName = { $regex: regexValue, $options: "i" }
      }
      if (status && status.length) {
        delete maRfpOppQuery.rfpTranStatus
        maRfpOppQuery = {
          ...maRfpOppQuery,
          $and: [
            ...maRfpOppQuery.$and,
            { actStatus: { $in: status } },
            { actStatus: { $nin: ["Cancelled"] } }
          ]
        }
      }
      if (client && client.length) {
        maRfpOppQuery.actIssuerClient = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        maRfpOppQuery.actLeadFinAdvClientEntityId =  { $in: leadManagerIds }
      }
      if (createdAt) {
        maRfpOppQuery.createdAt = { $gte: new Date(createdAt) }
      }

      maRfpOpp = await ActMaRFP.aggregate([
        {
          $match: maRfpOppQuery
        },
        {
          $project: {
            _id: 0,
            activityId: "$_id",
            activityTranFirmId: "$actTranFirmId",
            activityTranFirmName: "$actTranFirmName",
            activityTranClientId: "$actIssuerClient",
            activityClientName: "$actIssuerClientEntityName",
            activityTranType: "$actType",
            activityTranSubType: "$actSubType",
            activityDescription: "$actProjectName",
            activityEstimatedRev: "$maRfpSummary.actEstRev",
            opportunity: "$opportunity",
            activityLeadAdvisorId: "$actLeadFinAdvClientEntityId",
            activityLeadAdvisorName: "$actLeadFinAdvClientEntityName",
          }
        }
      ])
      opportunities = [...opportunities, ...maRfpOpp]
    }

    if (type.indexOf("Derivatives") !== -1) {
      let derivativeOppQuery = {
        $and: [
          ...findQueryForOpportunity,
          {"actTranFirmId":ObjectID(req.user.tenantId)}
        ],
        actTranStatus: { $nin: ["Cancelled"] }
      }
      if (searchTerm) {
        derivativeOppQuery.actTranProjectDescription = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        delete derivativeOppQuery.actTranStatus
        derivativeOppQuery = {
          ...derivativeOppQuery,
          $and: [
            ...derivativeOppQuery.$and,
            { actTranStatus: { $in: status } },
            { actTranStatus: { $nin: ["Cancelled"] } }
          ]
        }
      }
      if (client && client.length) {
        derivativeOppQuery.actTranClientId = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        derivativeOppQuery.actTranFirmLeadAdvisorId = { $in: leadManagerIds }
      }
      if (createdAt) {
        derivativeOppQuery.createdAt = { $gte: new Date(createdAt) }
      }

      derivativeOpp = await Derivatives.aggregate([
        {
          $match: derivativeOppQuery
        },
        {
          $project: {
            _id: 0,
            activityId: "$_id",
            activityTranFirmId: "$actTranFirmId",
            activityTranFirmName: "$actTranFirmName",
            activityTranClientId: "$actTranClientId",
            activityClientName: "$actTranClientName",
            activityTranType: "$actTranType",
            activityTranSubType: "$actTranSubType",
            activityDescription: "$actTranProjectDescription",
            activityEstimatedRev: "$derivativeSummary.tranEstimatedRev",
            opportunity: "$opportunity",
            activityLeadAdvisorId: "$actTranFirmLeadAdvisorId",
            activityLeadAdvisorName: "$actTranFirmLeadAdvisorName",
          }
        }
      ])
      opportunities = [...opportunities, ...derivativeOpp]
    }

    if (type.indexOf("BankLoans") !== -1) {
      let bankLoanOppQuery = {
        $and: [
          ...findQueryForOpportunity,
          {"actTranFirmId":ObjectID(req.user.tenantId)}
        ],
        actTranStatus: { $nin: ["Cancelled"] }
      }
      if (searchTerm) {
        bankLoanOppQuery.actTranProjectDescription = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        delete bankLoanOppQuery.actTranStatus
        bankLoanOppQuery = {
          ...bankLoanOppQuery,
          $and: [
            ...bankLoanOppQuery.$and,
            { actTranStatus: { $in: status } },
            { actTranStatus: { $nin: ["Cancelled"] } }
          ]
        }
      }
      if (client && client.length) {
        bankLoanOppQuery.actTranClientId = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        bankLoanOppQuery.actTranFirmLeadAdvisorId = { $in: leadManagerIds }
      }
      if (createdAt) {
        bankLoanOppQuery.createdAt = { $gte: new Date(createdAt) }
      }

      bankloanOpp = await BankLoans.aggregate([
        {
          $match: bankLoanOppQuery
        },
        {
          $project: {
            _id: 0,
            activityId: "$_id",
            activityTranFirmId: "$actTranFirmId",
            activityTranFirmName: "$actTranFirmName",
            activityTranClientId: "$actTranClientId",
            activityClientName: "$actTranClientName",
            activityTranType: "$actTranType",
            activityTranSubType: "$actTranSubType",
            activityDescription: "$actTranProjectDescription",
            activityEstimatedRev: "$bankLoanSummary.actTranEstimatedRev",
            opportunity: "$opportunity",
            activityLeadAdvisorId: "$actTranFirmLeadAdvisorId",
            activityLeadAdvisorName: "$actTranFirmLeadAdvisorName",
          }
        }
      ])
      opportunities = [...opportunities, ...bankloanOpp]
    }

    if (type.indexOf("Others") !== -1) {
      let othersOppQuery = {
        $and: [
          ...findQueryForOpportunity,
          {"actTranFirmId":ObjectID(req.user.tenantId)}
        ],
        actTranStatus: { $nin: ["Cancelled"] }
      }
      if (searchTerm) {
        othersOppQuery.actTranProjectDescription = {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (status && status.length) {
        delete othersOppQuery.actTranStatus
        othersOppQuery = {
          ...othersOppQuery,
          $and: [
            ...othersOppQuery.$and,
            { actTranStatus: { $in: status } },
            { actTranStatus: { $nin: ["Cancelled"] } }
          ]
        }
      }
      if (client && client.length) {
        othersOppQuery.actTranClientId = { $in: clientIds }
      }
      if (leadManager && leadManager.length) {
        othersOppQuery.actTranFirmLeadAdvisorId = { $in: leadManagerIds }
      }
      if (createdAt) {
        othersOppQuery.createdAt = { $gte: new Date(createdAt) }
      }

      othersOpp = await Others.aggregate([
        {
          $match: othersOppQuery
        },
        {
          $project: {
            _id: 0,
            activityId: "$_id",
            activityTranFirmId: "$actTranFirmId",
            activityTranFirmName: "$actTranFirmName",
            activityTranClientId: "$actTranClientId",
            activityClientName: "$actTranClientName",
            activityTranType: "$actTranType",
            activityTranSubType: "$actTranSubType",
            activityDescription: "$actTranProjectDescription",
            activityEstimatedRev: "$actTranEstimatedRev",
            opportunity: "$opportunity",
            activityLeadAdvisorId: "$actTranFirmLeadAdvisorId",
            activityLeadAdvisorName: "$actTranFirmLeadAdvisorName",
          }
        }
      ])

      opportunities = [...opportunities, ...othersOpp]
    }

    opportunities = opportunities.map(r => ({
      ...r,
      activityLeadAdvisorName: r.activityLeadAdvisorName || `${r.activityLeadAdvisorFirstName} ${r.activityLeadAdvisorLastName}` || "",
    }))

    const tranDealsInProgress = opportunities.filter(opp => !(opp && opp.opportunity && opp.opportunity.status))
    opportunities = opportunities.filter(opp => opp && opp.opportunity && opp.opportunity.status)

    let inProgress =
      invoices.filter(
        invoice =>
          invoice.invoiceStatus !== "Invoice Sent to Client" &&
          invoice.invoiceStatus !== "Invoice Paid"
      ) || []
    const invoiced =
      invoices.filter(
        invoice => invoice.invoiceStatus === "Invoice Sent to Client"
      ) || []
    const paid =
      invoices.filter(invoice => invoice.invoiceStatus === "Invoice Paid") || []

    let totalInProgress = 0
    let totalInvoiced = 0
    let totalPaid = 0
    let totalOpportunities = 0

    opportunities.forEach(opp => {
      totalOpportunities += parseInt(opp.activityEstimatedRev || 0, 10)
    })

    inProgress = [...inProgress, ...tranDealsInProgress]

    inProgress.forEach(inPro => {
      if(inPro.invoiceStatus){
        totalInProgress += parseInt(inPro.totalOverallExpenses || 0, 10)
      }else {
        totalInProgress += parseInt(inPro.activityEstimatedRev || 0, 10)
      }
    })

    invoiced.forEach(inPro => {
      totalInvoiced += parseInt(inPro.totalOverallExpenses || 0, 10)
    })
    paid.forEach(inPro => {
      totalPaid += parseInt(inPro.totalOverallExpenses || 0, 10)
    })

    res.json({
      invoices,
      opportunities,
      inProgress,
      invoiced,
      paid,
      totalOpportunities,
      totalInProgress,
      totalInvoiced,
      totalPaid,
    })
  } catch (error) {
    console.log("======", error)

    res.status(422).send({
      done: false,
      invoices: []
    })
  }
}

/* const filterInvoices = await TenantBilling.aggregate([
  {
    $match: {
      activityDescription: { $regex: "a", $options:"i"} ,
    }
  },
  {
    $lookup: {
      from: "tranagencydeals",
      localField: "activityId",
      foreignField: "_id",
      as: "activity"
    }
  },
  { $unwind: "$activity" },
  { $match: { "activity.dealIssueTranStatus" : { "$in": [ "Pre-Pricing"] } } },
  {
    $project: {
      activityId: 1,
      activityTranType: 1,
      activityTranSubType: 1,
      activityTranClientId: 1,
      activityClientName: 1,
      activityLeadAdvisorId: 1,
      activityLeadAdvisorName: 1,
      invoiceStatus: 1,
      activityStatus: "$activity.dealIssueTranStatus",
      activityDescription: "$activity.dealIssueTranProjectDescription",
      created_at: "$activity.created_at",
    }
  }
]) */

/* db.tenantservicebillings.aggregate([
  {
    $match:{
      activityDescription: { $regex: "a", $options:"i"} ,
    }
  },
  {
    $facet:{
      "deals":[
        {
          $lookup:{
            from: "tranagencydeals",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: { "trans.dealIssueTranStatus" : { "$in": [ "Pre-Pricing"] } } },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            invoiceStatus: 1,
            activityStatus: "$trans.dealIssueTranStatus",
            activityDescription: "$trans.dealIssueTranProjectDescription",
            created_at: "$trans.created_at",


          }
        }
      ],
      "rfps":[
        {
          $lookup:{
            from: "tranagencyrfps",
            localField: "activityId",
            foreignField: "_id",
            as: "trans"
          }
        },
        {
          $unwind:"$trans"
        },
        { $match: { "trans.rfpTranStatus" : { "$in": [ "Pre-execution"] } } },
        {
          $project:{
            activityId: 1,
            activityTranType: 1,
            activityTranSubType: 1,
            activityTranClientId: 1,
            activityClientName: 1,
            activityLeadAdvisorId: 1,
            activityLeadAdvisorName: 1,
            invoiceStatus: 1,
            rfpTranStatus:"$trans.rfpTranStatus",
          }
        },
      ],
    }
  }
]) */

export default generateControllers(TenantBilling, {
  // Service to get the Billing expeenses and other details
  manageBillingExpensesState: manageBillingExpensesState(),

  // Push and Pull the documents
  pullSpecifcReceiptForUser: pullSpecifcReceiptForUser(),
  pushBillingReceiptsForUser: pushBillingReceiptsForUser(),

  // Get all the transactions for the Tenant
  getAllTransactionsForTenant: getAllTransactionsForTenant(),
  getFAServiceDataForType: getFAServiceDataForType(),
  getAllBillingDetails: getAllBillingDetails(),

  // Lookup services from other modules
  getConsolidatedActivityDetailsForBilling: getConsolidatedActivityDetailsForBilling(),
  getConsolidatedContractsForBilling: getConsolidatedContractsForBilling(),

  // User Expenses - Get and Post all of them
  getUserExpensesForInvoiceLoggedInUser: getUserExpensesForInvoiceLoggedInUser(),
  postUserExpensesForInvoiceLoggedInUser: postUserExpensesForInvoiceLoggedInUser(),

  // Adding and Modifying a given expensse
  postOneExpenseForInvoiceAndLoggedInUser: postOneExpenseForInvoiceAndLoggedInUser(),
  pullExpenseDetails: pullExpenseDetails(),

  // Add and Delete a given expense for a given user
  addUserOutofPocketExpense: addUserOutofPocketExpense(),
  modifyOutOfPocketexpense: modifyOutOfPocketexpense(),

  // Get all Expenses for a Firm and Invoice ID.
  getAllFirmUserExpenses: getAllFirmUserExpenses(),
  postAllFirmUserExpenses: postAllFirmUserExpenses(),

  // Post Services on the Billing Module
  getFAServiceFee: getFAServiceFee(),
  postFAServiceData: postFAServiceData(),
  putFAServiceFee: putFAServiceFee(),
  postFAServiceDataForFeeTypeAndId: postFAServiceDataForFeeTypeAndId(),
  postFAServiceDataForType: postFAServiceDataForType(),
  postBillingReceipts: postBillingReceipts(),

  getBillingReceipts: getBillingReceipts(),
  getAllBillingDetailsForInvoice: getAllBillingDetailsForInvoice(),

  // Dashboard Services
  getAllInvoicesForSearch: getAllInvoicesForSearch(),
  updateInvoiceStatus: updateInvoiceStatus(),

  getAllTransactionsForInvoice: getAllTransactionsForInvoice(),
  putInvoice: putInvoice(),
  getRevenuePipelineDetails: getRevenuePipelineDetails(),
  filtterRevenuePipelineDetails: filtterRevenuePipelineDetails()
})
