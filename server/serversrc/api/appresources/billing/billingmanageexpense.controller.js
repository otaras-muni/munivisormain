import {generateControllers} from "./../../modules/query"
import {BillingManageExpense} from "./billingmanageexpense.model"
import {TenantBilling} from "../billing/billing.model"
import {ActMaRFP, BankLoans, Deals, Derivatives, Others, RFP} from "../models"

const {ObjectID} = require("mongodb")

const putManageExpense = () => async (req, res) => {
  try {
    let manageBillingExpense ={}
    const {_id, nonTranFees} = req.body
    if(_id) {
      await BillingManageExpense.updateOne({ _id }, req.body)
      manageBillingExpense = await BillingManageExpense.findOne({ _id })
      if(nonTranFees && Array.isArray(nonTranFees) && nonTranFees.length){
        await TenantBilling.update({ activityId: manageBillingExpense.activityId }, { nonTranFees })
      }
    }else {
      const billingExpense = new BillingManageExpense({
        ...req.body
      })
      manageBillingExpense = await billingExpense.save()
      const invoices = await TenantBilling.find({activityId: manageBillingExpense.activityId})
      console.log("=======================================>", req.body)
      if (!(invoices && invoices.length)){
        const { activityId, activityDescription, activityTranType, activityTranSubType, activityTranFirmId, activityTranFirmName, activityTranClientId, activityClientName, activityContract, activityLeadAdvisor } = req.body
        const newInvoice = new TenantBilling({
          activityId,
          activityDescription,
          activityTranType,
          activityTranSubType,
          activityTranFirmId,
          activityTranFirmName,
          activityTranClientId,
          activityClientName,
          invoiceNumber: `INVOICE-${Math.random().toString(36).toUpperCase().substring(2, 17)}`,
          nonTranFees: (activityContract && activityContract.nonTranFees && activityContract.nonTranFees.length && activityContract.nonTranFees) || nonTranFees || [],
          retAndEsc: activityContract && activityContract.retAndEsc || [],
          sof: activityContract && activityContract.sof || [],
          contractRef: activityContract && activityContract.contractRef || {},
          digitize: activityContract && activityContract.digitize || false,
          reviewed: activityContract && activityContract.reviewed || false,
          activityLeadAdvisorId: (activityLeadAdvisor && activityLeadAdvisor.userId) || "",
          activityLeadAdvisorName: `${activityLeadAdvisor && activityLeadAdvisor.userFirstName} ${activityLeadAdvisor && activityLeadAdvisor.userLastName}` || "",
          agree: activityContract && activityContract.agree || "",
          invoiceStatus: "In Progress"
        })
        await newInvoice.save()
      } else if(nonTranFees && Array.isArray(nonTranFees) && nonTranFees.length){
        await TenantBilling.update({ activityId: manageBillingExpense.activityId }, { nonTranFees })
      }
    }
    res.json(manageBillingExpense)
  } catch (error) {
    res.status(500).send({done: false,error: "Error saving information on Deals"})
  }
}

const getManageExpense = () => async (req, res) => {
  try {
    const {entityId, activityId} = req.params
    const {userId} = req.query

    if(entityId && activityId && userId) {

      const manageBillingExpense = await BillingManageExpense.findOne({ activityTranClientId: entityId, activityId, userId })

      res.json(manageBillingExpense)

    }else {

      res.status(500).send({done: false,error: "Missing params client or transaction or user"})

    }
  } catch (error) {
    res.status(500).send({done: false,error: "Error saving information on Deals"})
  }
}

const getManageExpenseByIssueName = () => async (req, res) => {
  try {
    const {entityId} = req.params
    const {activityId} = req.query
    console.log("===================52===================>", entityId, activityId)
    if(entityId && activityId ) {

      const manageBillingExpense = await BillingManageExpense.find({ activityTranClientId: entityId, activityId }).select("timeTracker userFirstName roleType outOfPocketExpensesData _id confirmation userId")
      const invoice = await TenantBilling.findOne({ activityId }).select("nonTranFees invoiceStatus")
      const userExpenses = {}
      const userTimeTrackerCount = {}

      manageBillingExpense.forEach(expenses => {
        userExpenses[expenses.userId] = {
          _id: expenses._id,
          userFirstName: expenses.userFirstName,
          roleType: expenses.roleType,
          timeTracker: expenses.timeTracker || [],
          outOfPocketExpensesData: expenses.outOfPocketExpensesData || [],
          confirmation: expenses.confirmation
        }
      })

      console.log("============72=============>", invoice)
      Object.keys(userExpenses).forEach(key => {
        let totalCost = 0
        let totalHours = 0
        let totalAmount = 0
        const tranFee = (invoice && invoice.nonTranFees && invoice.nonTranFees.find(fee => fee.role === userExpenses[key].roleType)) || {}
        console.log("===========76==========>", tranFee)
        userExpenses[key].timeTracker.forEach(time => {
          totalCost += (Number(time.hours || 0) * (tranFee && tranFee.stdHourlyRate) || 0)
          totalHours += (Number(time.hours || 0))
        })
        userExpenses[key].outOfPocketExpensesData.forEach(time => {
          totalAmount += (time.expenseAmount || 0)
        })

        userTimeTrackerCount[key] = {
          roleType: userExpenses[key].roleType,
          totalHours,
          totalAmount,
          rate: tranFee.stdHourlyRate,
          totalCost: Number(totalCost.toFixed(4))
        }
      })
      res.json({userExpenses, userTimeTrackerCount, nonTranFees: (invoice && invoice.nonTranFees) || null, invoiceStatus: (invoice && invoice.invoiceStatus) || ""})

    }else {

      res.status(500).send({done: false,error: "Missing params client or transaction or user"})

    }
  } catch (error) {
    res.status(500).send({done: false,error: "Error saving information on Deals"})
  }
}

const getExpenseOfSelectedTransactions = () => async (req, res) => {
  try {
    const { activityId, type, subType } = req.query

    if(activityId){

      const manageBillingExpense = await BillingManageExpense.find({ activityId }).select("outOfPocketExpensesData userId activityId activityDescription userFirstName userLastName timeTracker roleType")
      const invoice = await TenantBilling.findOne({ activityId }).select("nonTranFees sof invoiceStatus")

      const modal = subType === "Bond Issue" ? Deals :
                      type === "MA-RFP" ? ActMaRFP :
                        type === "Derivative" ? Derivatives :
                          (subType === "Bank Loan" || subType === "Lines and Letter" || subType === "Letter Of Credit" || subType === "Lines Of Credit") ? BankLoans :
                            type === "RFP" ? RFP :
                              type === "Others" ? Others : "";

      const activity = await modal.findOne({ _id: activityId}).select("rfpParAmount dealIssueParAmount bankLoanTerms.parAmount " +
        "derivativeSummary.tranNotionalAmt maRfpSummary.actParAmount actTranParAmount")

      const activityParAmount = subType === "Bond Issue" ? (activity && activity.dealIssueParAmount) || 0 :
        type === "MA-RFP" ? (activity && activity.maRfpSummary && activity.maRfpSummary.actParAmount) || 0 :
          type === "Derivative" ? (activity && activity.derivativeSummary && activity.derivativeSummary.tranNotionalAmt) || 0 :
            (subType === "Bank Loan" || subType === "Lines and Letter" || subType === "Letter Of Credit" || subType === "Lines Of Credit") ? (activity && activity.bankLoanTerms && activity.bankLoanTerms.parAmount) || 0 :
              type === "RFP" ? (activity && activity.rfpParAmount) || 0  :
                type === "Others" ? (activity && activity.actTranParAmount) || 0 : ""

      console.log("===============================")
      console.log(activityParAmount)
      console.log("===============================")

      const pockets = {}
      const pocketCount = {}
      const timeTracker = {}
      const timeTrackerCount = {}
      let totalFinancialAdvisoryFees = 0
      const parAmount = (invoice && Number(activityParAmount || 0)) || 0

      if(invoice && invoice.sof && invoice.sof.length){
        const schedule = invoice.sof[0]
        const feeType = schedule.feesType
        const sofMax = Number(schedule.sofMax || 0)
        const sofMin = Number(schedule.sofMin || 0)

        if(schedule.fees && schedule.fees.length){
          const maxScheduleFee = schedule.fees.filter(fee => fee.sofDesc !== "Amount Over").reduce((prev, current)  => (Number(prev.sofAmt ||0) > Number(current.sofAmt||0)) ? prev : current)
          if(feeType === "feePar"){
            totalFinancialAdvisoryFees = (parAmount * Number(schedule.fees[0].sofCharge || 0)) /  Number(schedule.fees[0].sofAmt || 0)

            totalFinancialAdvisoryFees = totalFinancialAdvisoryFees < sofMin ? sofMin :
              totalFinancialAdvisoryFees > sofMax ? sofMax : totalFinancialAdvisoryFees

          }else {
            schedule.fees.forEach(fee => {
              if(parAmount <= Number(fee.sofAmt||0) && fee.sofDesc !== "Amount Over" && !totalFinancialAdvisoryFees){
                totalFinancialAdvisoryFees = fee.sofCharge
              }
            })

            totalFinancialAdvisoryFees =  parAmount > Number(maxScheduleFee.sofAmt ||0) ? sofMax :
              totalFinancialAdvisoryFees < sofMin ? sofMin :
                totalFinancialAdvisoryFees > sofMax ? sofMax : totalFinancialAdvisoryFees
          }

        }
      }


      manageBillingExpense.forEach(expenses => {
        expenses.outOfPocketExpensesData.forEach(pocket => {
          if(pockets.hasOwnProperty(pocket.expenseType)){
            pockets[pocket.expenseType].push(pocket)
          }else {
            pockets[pocket.expenseType] = [pocket]
          }
        })
      })

      manageBillingExpense.forEach(expenses => {
        expenses.timeTracker.forEach(tracker => {
          const tranFee = (invoice && invoice.nonTranFees && invoice.nonTranFees.find(fee => fee.role === expenses.roleType)) || {}
          const newTracker = {
            fromDate: tracker.fromDate || "",
            toDate: tracker.toDate || "",
            hours: tracker.hours || "",
            rate: (tranFee && tranFee.stdHourlyRate) || 0
          }
          if(timeTracker.hasOwnProperty(expenses.roleType)){
            timeTracker[expenses.roleType].push(newTracker)
          }else {
            timeTracker[expenses.roleType] = [newTracker]
          }
        })
      })


      Object.keys(pockets).forEach(key => {
        pocketCount[key] = 0
        pockets[key].forEach(exp => {
          pocketCount[key] += Number(exp.expenseAmount || 0, 10)
        })
      })

      // console.log(invoice)
      Object.keys(timeTracker).forEach(key => {
        timeTrackerCount[key] = 0
        timeTracker[key].forEach(time => {
          timeTrackerCount[key] += (Number(time.hours || 0, 10) * Number(time.rate))
        })
      })

      res.json( { manageBillingExpense, pockets, pocketCount, timeTracker, timeTrackerCount, totalFinancialAdvisoryFees, activityParAmount, invoiceStatus: (invoice && invoice.invoiceStatus) || ""})

    }else {

      res.status(500).send({done: false,error: "Missing params activity"})

    }
  } catch (error) {
    console.log("************getExpenseOfSelectedTransactions***************",error)
    res.status(500).send({done: false, error})
  }
}

const putReceipts = () => async (req, res) => {
  try {
    const { expenseId, pocketId } = req.query
    await BillingManageExpense.updateOne(
      { _id: ObjectID(expenseId), "outOfPocketExpensesData._id": ObjectID(pocketId) },
      { $addToSet: { "outOfPocketExpensesData.$.documents": req.body.documents } }
    )

    const receipts = await BillingManageExpense.findOne({_id: expenseId}).select("outOfPocketExpensesData")
    // receipts = receipts.outOfPocketExpensesData.find(rec => rec._id.toString() === pocketId.toString())
    res.json(receipts.outOfPocketExpensesData)
  } catch(error) {
    console.log("***************putReceipts*****************",error)
    res.status(422).send({done:false, error})
  }
}

const getReceipts = () => async (req, res) => {
  try {
    const { expenseId, pocketId } = req.query
    let receipts = await BillingManageExpense.findOne({_id: expenseId}).select("outOfPocketExpensesData")
    receipts = receipts.outOfPocketExpensesData.find(rec => rec._id.toString() === pocketId.toString())
    res.json(receipts)

    res.json(receipts)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const pullReceipts = () => async (req, res) => {
  try {
    const { expenseId, pocketId, docId } = req.query
    await BillingManageExpense.update(
      {_id: ObjectID(expenseId), "outOfPocketExpensesData._id" : ObjectID(pocketId)},
      {$pull: {"outOfPocketExpensesData.$.documents": {_id: ObjectID(docId)}}
      })

    const receipts = await BillingManageExpense.findOne({_id: expenseId}).select("outOfPocketExpensesData")
    res.json(receipts.outOfPocketExpensesData)
  } catch(error) {
    console.log("***************pullReceipts*****************",error)
    res.status(422).send({done:false, error})
  }
}


export default generateControllers(BillingManageExpense,
  {
    putManageExpense: putManageExpense(),
    getManageExpense: getManageExpense(),
    getManageExpenseByIssueName: getManageExpenseByIssueName(),
    getExpenseOfSelectedTransactions: getExpenseOfSelectedTransactions(),
    putReceipts: putReceipts(),
    getReceipts: getReceipts(),
    pullReceipts: pullReceipts(),
  })

