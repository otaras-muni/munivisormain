import express from "express"
import {
  generateExcelFile
} from "./excelgen.controller"

import {requireAuth} from "./../../../authorization/authsessionmanagement/auth.middleware"

export const excelGeneratorRouter = express.Router()

excelGeneratorRouter.route("/testexcel")
  .get(requireAuth,generateExcelFile)
