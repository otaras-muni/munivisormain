
const XLSX = require("xlsx")

const sampledata = [["a", "b", "c"], [1, 2, 3]]


const generateExcel = (data, callback) => {
  try {
    const wb = XLSX.utils.book_new()
    const ws = XLSX.utils.aoa_to_sheet(data)
    XLSX.utils.book_append_sheet(wb, ws, "test")
  
    // write options
    const wopts = { bookType: "xlsx", bookSST: false, type: "buffer" }
    const buffer = XLSX.write(wb, wopts)
    callback(buffer)
  } 
  catch(err) {
    console.log("There is an Error generating excel file")
    throw(err)
  }
}

export const generateExcelFile = (req, res) => {
  console.log("Entered the Service - New One for generating excel")
  let fileName = "testfile"
  fileName = `${encodeURIComponent(fileName)}.xlsx`
  console.log("I entered the excel generation service", fileName)

  generateExcel(sampledata, (response) => {
    res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    res.setHeader("Content-disposition", `attachment; filename="${fileName}"`)
    console.log("Before the Response")
    res.send(response) // Buffer data
  })
}

/*

export const generateExcelFile = (req, res) => {
  const wb = XLSX.utils.book_new()
  const table = [["a", "b", "c"], [1, 2, 3]]
  const ws = XLSX.utils.aoa_to_sheet(table)
  XLSX.utils.book_append_sheet(wb, ws, "test")

  // write options
  const wopts = { bookType: "xlsx", bookSST: false, type: "buffer" }
  const buffer = XLSX.write(wb, wopts)
  res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
  res.setHeader("Content-Disposition", "attachment; filename= \"Report.xlsx\"")
  res.send(buffer)
}
*/



