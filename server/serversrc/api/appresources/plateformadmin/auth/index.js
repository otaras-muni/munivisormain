

export const platFormAuthMiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization
    if (token !== process.env.PLATFORM_CALL_FROM_PLATFORM_MANAGEMENT_TOKEN) {
      return res.status(500).send({
        error: "Unauthorized"
      })
    }
    next()
  } catch (err) {
    return res.status(403).send({ error: "Unauthorized" })
  }
}
