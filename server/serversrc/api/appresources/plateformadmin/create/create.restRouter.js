import express from "express"
import {createTenant} from "./create.controller"
import { saveUser } from "../../entityUser/entityUser.controller.new"
import entityUserController from "../../entityUser/entityUser.controller"
import {platFormAuthMiddleware} from "../auth"
export const createRouter = express.Router()

createRouter.route("/tenant")
  .post(platFormAuthMiddleware, createTenant)

createRouter.route("/users")
  .post(platFormAuthMiddleware, entityUserController.signUpSTPUserOnCreateMiddleware, saveUser)
