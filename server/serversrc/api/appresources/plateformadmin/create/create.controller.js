import {
  EntityUser,
  Entity,
  EntityRel
} from "../../models"
import {elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"
import { generateTenantEntity } from "../../../seeddata/revisedseedata/clientonboarding/generateTenant"
import { generatEntityRelData } from "../../../seeddata/revisedseedata/generateEntityRelationshipData"
import { checkDuplicateEmailForTenant } from "../../../commonDbFunctions/checkDuplicateEmailForTenant"
import { doesIdExistsInEntitledUsers, updateEligibleIdsForLoggedInUserFirm } from "../../../entitlementhelpers"
import { generateConfigForEntities } from "../../config/generateConfigForDifferentEntityTypes"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Entity, _id: id, esType: "entities"})

const returnFromESRel = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: EntityRel, _id: id, esType: "entityrels"})

const {ObjectID} = require("mongodb")

export const createTenant = async (req, res) => {

  try {
    const { firmDetails } = req.body

    const regexValue = new RegExp(firmDetails.firmName.trim() || "", "i")
    console.log("regexValue:", regexValue)

    const entities = await EntityRel.aggregate([
      {
        $match:{
          relationshipType: "Self"
        }
      },
      {
        $lookup: {
          from: "entities",
          localField: "entityParty1",
          foreignField: "_id",
          as: "entity"
        }
      },
      {
        $unwind: "$entity"
      },
      {
        $match: {
          "entity.firmName": { $regex: regexValue, $options: "i" }
        }
      },
      {
        $project: {
          entityId: "$entity._id",
          firmName: "$entity.firmName",
        }
      },
      {
        $unwind: "$entityId"
      }
    ])

    const matchData = entities.find(d => d.firmName.toLowerCase() === firmDetails.firmName.toLowerCase())
    if(matchData){
      return res.status(203).send({done:false, message:"Firm Name Is Already Existing Choose Another Name!"})
    }
    const addTenant = (firm) => generateTenantEntity(firm).save()
    const addRel = (entity) => generatEntityRelData(entity, entity,"Self").save()
    const entity = await addTenant(firmDetails)
    await addRel(entity)
    await generateConfigForEntities([entity._id])
    // returnFromES(req.user.tenantId, entity._id)
    // returnFromESRel(req.user.tenantId, entity._id)
    res.json({ done: true, message: "Tenant Entity Create Successfully",  entity })
  }
  catch(err) {
    console.log("Tenant Entity firm data error", err)
    res
      .status(500)
      .send({ done: false, error: "Error saving information on Firm" })
  }
}

export const duplicateEmailCheckMiddlewareForUserUpdate = async (req,res,next) => {
  console.log("1. ENTERED THE DUPLICATE EMAIL CHECK FUNCTION", JSON.stringify(req.body,null,2))
  const userInfo = req && req.body
  const {userId} = req.params
  const user = userInfo && userInfo.userDetails

  if ( !userId ) {
    // The request body seems to be empty let us do something with this.
    next()
  }
  const {_doc:userInfoForCheckingDuplicates} = await EntityUser.findOne({_id:userId}).select({_id:1,userId:1, entityId:1})
  const userEmails = [...user && user.userEmails || []]
  const duplicateInfo = await checkDuplicateEmailForTenant(userInfoForCheckingDuplicates,userEmails,userId)
  console.log("The duplicate information", JSON.stringify(duplicateInfo,null,2))
  req.body = { userDetails:{...user }, duplicateInfo}
  next()
}

export const updateUser = async(req, res) => {
  const {user} = req
  const {userDetails, duplicateInfo} = req.body
  const {userId} = req.params
  const resRequested = [
    {
      resource: "EntityUser",
      access: 2
    }
  ]
  try {
    const { _id }  = user
    let allowedId = (userId && userId.toString() === _id.toString()) || false
    if(userId && !allowedId){
      allowedId = await doesIdExistsInEntitledUsers(req.user,req.params.userId,"edit")
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity Users"})
      }
    }
    /*  const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled && !allowedId) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    } */

    const {userEmails} = userDetails

    if (duplicateInfo) {
      if (duplicateInfo.duplicateExists) {
        let {userEmails} = userDetails
        userEmails = userEmails.map(email => email.emailId)
        const {duplicateEmailIds, duplicateExists} = duplicateInfo
        const duplicateEmailIdsUniq = [...new Set(duplicateEmailIds.map(email => email.emailId))]
        const duplError = []
        userEmails.forEach((email, idx) => {
          if (duplicateEmailIdsUniq.indexOf(email) !== -1)
            duplError.push({email, value: idx, label: `${email} exists already`})
        })
        res
          .status(422)
          .send({
            done: false,
            error: "Duplicates emails are found",
            success: "",
            duplicateExists,
            duplError,
            isPrimaryAddress: true
          })
        return false
      }
    }

    if (userEmails && userEmails.length) {
      const emails = []
      userEmails.forEach(email => {
        if (email.emailPrimary) {
          emails.push(email.emailId)
        }
      })

      const userDetailResult = await EntityUser
        .findById(userId)
        .select({userLoginCredentials: 1})

      const {userLoginCredentials} = userDetailResult
      const {userEmailId, onboardingStatus} = userLoginCredentials
      if (onboardingStatus !== "created" && !emails.includes(userEmailId)) {
        res
          .status(422)
          .send({done: false, error: "Primary emails can not be changed.", success: "", requestedServices: resRequested})
        return false
      }

      if (onboardingStatus === "created") {
        userLoginCredentials.userEmailId = emails[0]
        userDetails.userLoginCredentials = userLoginCredentials
      }
    }

    const entityResult = await Entity.findById(userDetails.entityId).select("primaryContactId")
    if (userDetails && userDetails.userFlags){
      const entityUsers = await EntityUser.find({entityId: userDetails.entityId, userFlags: "Primary Contact"}).select("userFlags")
      if(entityUsers && entityUsers.length){
        const entityUsersIds = entityUsers.map(e => e._id.toString() )
        await EntityUser.updateMany({_id: { $in: entityUsersIds } },   {$pull: {userFlags: "Primary Contact"}})
      }
    }

    const result = await EntityUser.findByIdAndUpdate(userId, {
      $set: {
        ...userDetails
      }
    }, {
      new: true,
      projection: {
        "userLoginCredentials": 0
      }
    })

    if(userDetails && userDetails.userFlags){
      const isPrimaryContact = userDetails && userDetails.userFlags.indexOf("Primary Contact") !== -1
      const primaryContactId = entityResult && entityResult.primaryContactId || ""
      const id = result && result._id || ""
      if(isPrimaryContact){
        await Entity.updateOne({ _id: ObjectID(result.entityId) || ""}, {$set: {primaryContactId: result._id} })
      } else if(primaryContactId.toString() === id.toString() && !isPrimaryContact){
        await Entity.updateOne({ _id: ObjectID(result.entityId) || ""}, {$unset: {primaryContactId: result._id} })
      }
    }

    await returnFromES(req.user.tenantId, result._id)
    await updateEligibleIdsForLoggedInUserFirm(req.user)

    res.json({done: true, error: "", success: result})
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: resRequested})
  }
}
