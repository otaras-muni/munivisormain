import express from "express"
import {
  fetchDataForPlateFormAdminLists,
  fetchTenantUsersList,
  fetchFirmDetails,
  countTenantUsersForPlatform
} from "./plateformadmin.controller"
import {platFormAuthMiddleware} from "../auth"

export const plateFormAdminRouter = express.Router()

plateFormAdminRouter
  .route("/tenants")
  .post(platFormAuthMiddleware, fetchDataForPlateFormAdminLists)

plateFormAdminRouter
  .route("/tenantusers")
  .post(platFormAuthMiddleware, fetchTenantUsersList)

plateFormAdminRouter
  .route("/entities/:entityId")
  .get(platFormAuthMiddleware, fetchFirmDetails)

plateFormAdminRouter
  .route("/tenantusercount")
  .get(platFormAuthMiddleware, countTenantUsersForPlatform)
