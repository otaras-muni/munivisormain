import axios from "axios"
import {
  Entity,
  EntityRel
} from "../../models"
import {getPlateFormAdminDataForSelfEntity} from "./getPlateFormAdminDataForSelfEntity"
import {getUsersListForEntity} from "./getUsersListForEntity"

const {ObjectID} = require("mongodb")

export const fetchDataForPlateFormAdminLists = async (req, res) => {
  const data = await getPlateFormAdminDataForSelfEntity(req.body)
  console.log(data)
  res.status(200).send(data)
}

export const fetchTenantUsersList = async (req, res) => {
  console.log("request body", req.body)
  const data = await getUsersListForEntity(req.body)
  res.status(200).send(data)
}

export const fetchFirmDetails = async(req, res) => {
  const {entityId} = req.params

  if (!entityId) {
    res
      .status(422)
      .send("No entity id provided")
  }
  try {
    const entity = await Entity.aggregate([
      {
        $match: {
          _id: ObjectID(entityId)
        }
      }, {
        $limit: 1
      }, {
        $lookup: {
          from: "entityrels",
          localField: "_id",
          foreignField: "entityParty2",
          as: "rel"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "primaryContactId",
          foreignField: "_id",
          as: "primaryContact"
        }
      }, {
        $project: {
          rel: 0,
          primaryContact: 0
        }
      }
    ])
    res.json(entity)
  } catch (err) {
    res
      .status(422)
      .send({err})
  }
}

export const countTenantUsers = async () => {
  try {
    const result = await EntityRel.aggregate([
      { $match: { relationshipType: "Self" } },
      {
        $lookup: {
          from: "entities",
          localField: "entityParty1",
          foreignField: "_id",
          as: "entitydetails"
        }
      },
      {
        $unwind: "$entitydetails"
      },
      {
        $project : {
          tenantId: "$entitydetails._id",
          firmName: "$entitydetails.firmName",
          createdAt: "$entitydetails.createdAt",
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "tenantId",
          foreignField: "entityId",
          as: "users"
        }
      },
      {
        $unwind:"$users"
      },
      {
        $project: {
          createdAt: 1,
          firmName: 1,
          tenantId: 1,
          userId:"$users._id",
          userFlags:"$users.userFlags",
          userEmailId:"$users.userLoginCredentials.userEmailId",
        }
      },
    ])
    let users = {}
    result.forEach(user => {
      const isSeries50 = user && user.userFlags.indexOf("Series 50") !== -1
      if(users.hasOwnProperty(user.tenantId)){
        users[user.tenantId].allFirmUserEmails.push(user.userEmailId)
        if(isSeries50){
          users[user.tenantId].series50.push(user.userId)
        }else {
          users[user.tenantId].nonSeries50.push(user.userId)
        }
      }else {
        users = {
          ...users,
          [user.tenantId]: {
            primaryContactEmail: user.primaryContactEmail,
            name: user.firmName,
            createdAt: user.createdAt,
            allFirmUserEmails: [user.userEmailId],
            series50: [],
            nonSeries50: [],
          }
        }
        if(isSeries50){
          users[user.tenantId].series50.push(user.userId)
        }else {
          users[user.tenantId].nonSeries50.push(user.userId)
        }
      }
    })
    // `${process.env.PLATFORM_API_URL}/api/billing/compute`
    console.log(users, `${process.env.PLATFORM_API_URL}/service/billingcalculate`)
    const apiRes = await axios.post(`${process.env.PLATFORM_API_URL}/service/billingcalculate`, users, {
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })
    console.log("Invoice calculation complete", apiRes.data)
  }catch (e) {
    console.log("Error getting in invoice calculation.", e)
  }
}

export const countTenantUsersForPlatform = async (req, res) => {
  try {
    const result = await EntityRel.aggregate([
      { $match: { relationshipType: "Self" } },
      {
        $lookup: {
          from: "entities",
          localField: "entityParty1",
          foreignField: "_id",
          as: "entitydetails"
        }
      },
      {
        $unwind: "$entitydetails"
      },
      {
        $project : {
          tenantId: "$entitydetails._id",
          firmName: "$entitydetails.firmName",
          addresses: "$entitydetails.addresses",
          createdAt: "$entitydetails.createdAt",
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "tenantId",
          foreignField: "entityId",
          as: "users"
        }
      },
      {
        $unwind:"$users"
      },
      {
        $project: {
          createdAt: 1,
          firmName: 1,
          tenantId: 1,
          addresses: 1,
          userId: { $ifNull: ["$users._id", ""] },
          userFlags: { $ifNull: ["$users.userFlags", []] },
          userEmailId: { $ifNull: ["$users.userLoginCredentials.userEmailId", ""] }
        }
      },
    ])
    let users = {}
    result.forEach(user => {
      const isSeries50 = user && user.userFlags.indexOf("Series 50") !== -1
      if(users.hasOwnProperty(user.tenantId)){
        users[user.tenantId].allFirmUserEmails.push(user.userEmailId)
        if(isSeries50){
          users[user.tenantId].series50.push(user.userEmailId)
        }else {
          users[user.tenantId].nonSeries50.push(user.userEmailId)
        }
      }else {
        users = {
          ...users,
          [user.tenantId]: {
            primaryContactEmail: user.primaryContactEmail,
            name: user.firmName,
            addresses: user.addresses,
            createdAt: user.createdAt,
            allFirmUserEmails: [user.userEmailId],
            series50: [],
            nonSeries50: [],
          }
        }
        if(user && user.userEmailId){
          if(isSeries50){
            users[user.tenantId].series50.push(user.userEmailId)
          }else {
            users[user.tenantId].nonSeries50.push(user.userEmailId)
          }
        }
      }
    })
    const remainTenants = await EntityRel.aggregate([
      { $match: { entityParty1: { $nin : Object.keys(users || {}).map(id => ObjectID(id)) }, relationshipType: "Self" } },
      {
        $lookup: {
          from: "entities",
          localField: "entityParty1",
          foreignField: "_id",
          as: "entitydetails"
        }
      },
      {
        $unwind: "$entitydetails"
      },
      {
        $project : {
          tenantId: "$entitydetails._id",
          firmName: "$entitydetails.firmName",
          addresses: "$entitydetails.addresses",
          createdAt: "$entitydetails.createdAt",
        }
      }
    ])
    remainTenants.forEach(tenant => {
      users = {
        ...users,
        [tenant.tenantId]: {
          primaryContactEmail: "",
          name: tenant.firmName,
          addresses: tenant.addresses,
          createdAt: tenant.createdAt,
          allFirmUserEmails: [],
          series50: [],
          nonSeries50: [],
        }
      }
    })
    res.status(200).send(users)
  }catch (e) {
    console.log("Error getting in invoice calculation.", e)
    res.status(422).send("Error getting in invoice calculation.")
  }
}
