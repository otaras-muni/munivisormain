import axios from "axios"
import {
  EntityUser,
  EntityRel,
  Deals,
  RFP,
  ActMaRFP,
  Derivatives,
  BankLoans,
  Others,
  ActBusDev
} from "./../../models"
import { Config } from "../../config/config.model"

const { ObjectID } = require("mongodb")


export const getPlateFormAdminPickLists = async (req, res, next) => {
  const { user } = req
  let { entityId, userEntitlement } = user || {}

  const { require, searchTerm} = req.query
  console.log("The query Parameter is ", searchTerm)
  const regexSearchTerm = new RegExp(searchTerm,"i")

  const entities = await EntityRel.aggregate([
    {
      $match:{
        relationshipType: "Self"
      }
    },
    {
      $lookup: {
        from: "entities",
        localField: "entityParty1",
        foreignField: "_id",
        as: "entity"
      }
    },
    {
      $unwind: "$entity"
    },
    {
      $project: {
        entityId: "$entity._id",
        firmName: "$entity.firmName",
      }
    },
    {
      $unwind: "$entityId"
    }
  ])
  const entityID = entities && entities.length ? entities && entities[0] && entities[0].entityId : ""
  entityId = entityID

  let searchQueryPart = []
  if(searchTerm && searchTerm.length > 0) {
    searchQueryPart = [{
      $match:{ "$or": [
        { "picklistTitle": {"$regex":regexSearchTerm} },
        { "picklistItems.label": {"$regex":regexSearchTerm }}
      ]}
    }]
  }

  console.log("The search Query Party", JSON.stringify(searchQueryPart,null,2))

  const pickListPipeline = [
    {
      $match: {entityId:ObjectID(entityId)}
    },
    {
      $unwind:"$picklists"
    },
    {
      $project:{
        entityId:"$entityId",
        picklistId:"$picklists._id",
        picklistTitle:"$picklists.title",
        picklistItems:"$picklists.items",
        picklistMeta:"$picklists.meta"
      }
    },
    {
      $unwind:"$picklistItems"
    },
    ...searchQueryPart,
    {
      $group:{
        _id:{entityId:"$entityId",picklistId:"$picklistId",title:"$picklistTitle",meta:"$picklistMeta"},
        items:{$addToSet:"$picklistItems"}
      }
    },
    {
      $project:{
        _id:0,
        entityId:"$_id.entityId",
        picklistId:"$_id.picklistId",
        title:"$_id.title",
        meta:"$_id.meta",
        // items:1
      }
    }
  ]

  console.log("The overall pipeline is", JSON.stringify(pickListPipeline,null,2))

  // const filter = req.query || {}
  const filter = { entityId }
  let docs
  try {
    switch(require) {
    case "entitlement":
      docs = await Config.find(filter).select("accessPolicy")
      res.json(docs[0].accessPolicy.filter(d => d.role === userEntitlement))
      break
    case "picklistsMeta":
      docs = await Config.findOne(filter).select({"picklists.meta": 1, "picklists.title": 1})
      res.json(docs.picklists)
      break
    case "picklists": {
      const start = new Date()
      const { names } = req.query
      let docs = []
      console.log("names : ", names)
      if(names && names.length) {
        const systemNames = names.split(",")
        console.log("systemNames : ", systemNames)
        docs = await Config.aggregate([
          {
            $match: filter
          },
          {
            $limit: 1
          },
          {
            $project: { picklists: 1 }
          },
          {
            $project: {
              picklists: {
                $filter: {
                  input: "$picklists",
                  as: "picklist",
                  cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
                }
              }
            }
          }
        ])
      } else {
        // docs = await Config.find(filter).select("picklists.meta picklists.title")
        docs = await Config.aggregate(pickListPipeline)
        docs = [{picklists:docs}]
        console.log("The picklist items to be showcased are", JSON.stringify(docs, null, 2))
      }
      console.log("time : ", new Date() - start)
      res.json(docs[0].picklists)
      break
    }
    case "checklists":
      docs = await Config.find(filter).select("checklists")
      res.json(docs[0].checklists)
      break
    case "notifications":
      docs = await Config.find(filter).select("notifications")
      res.json(docs[0].notifications)
      break
    default:
      docs = await Config.find(filter)
      res.json(docs)
    }
  } catch (error) {
    next(error)
  }

}

export const getPlateFormAdminUserAndTransaction = async (req, res) => {

  try {

    const newArray = await EntityRel.aggregate([
      { $match:{ relationshipType: "Self" } },
      { $lookup: { from: "entityusers", localField: "entityParty1", foreignField: "entityId", as: "user" } },
      { $lookup: { from: "tranagencydeals", localField: "entityParty1", foreignField: "dealIssueTranClientId", as: "deal" } },
      { $lookup: { from: "tranbankloans", localField: "entityParty1", foreignField: "actTranFirmId", as: "loan" } },
      { $lookup: { from: "tranderivatives", localField: "entityParty1", foreignField: "actTranFirmId", as: "derivative" } },
      { $lookup: { from: "tranagencyothers", localField: "entityParty1", foreignField: "actTranFirmId", as: "others" } },
      { $lookup: { from: "tranagencyrfps", localField: "entityParty1", foreignField: "rfpTranClientId", as: "rfp" } },
      { $lookup: { from: "actmarfps", localField: "entityParty1", foreignField: "actTranFirmId", as: "marfp" } },
      { $lookup: { from: "actbusdevs", localField: "entityParty1", foreignField: "actTranFirmId", as: "bussdev" } },
      { $lookup: { from: "entities", localField: "entityParty1", foreignField: "_id", as: "entity" } },
      {
        $unwind: "$entity"
      },
      {
        $project : {
          tenantId: "$entity._id",
          firmName: "$entity.firmName",
          userFlags: "$user.userFlags", Deal: { $size: "$deal" }, BankLoan: { $size: "$loan" }, Derivative: { $size: "$derivative" }, Others: { $size: "$others" }, RFP: { $size: "$rfp" } , MaRfp: { $size: "$marfp" } , BusinessDevelopment: { $size: "$bussdev"}
        }
      }
    ])

    const thirdPartyLength = await EntityRel.aggregate([
      { $match:{ relationshipType: "Third Party" } },
      { $group:{_id: "$entityParty1", count:{$sum:1}}},
    ])

    const clientLength = await EntityRel.aggregate([
      { $match:{ relationshipType: "Client" } },
      { $group:{_id: "$entityParty1", count:{$sum:1}}},
    ])

    const prospectLength = await EntityRel.aggregate([
      { $match:{ relationshipType: "Prospect" } },
      { $group:{_id: "$entityParty1", count:{$sum:1}}},
    ])

    const snapShots = []
    newArray.forEach(user => {
      const { Deal, BankLoan, Derivative, Others, RFP, MaRfp, BusinessDevelopment, userFlags, firmName, tenantId } = user
      let series50 = 0
      let nonSeries50 = 0
      if(userFlags && userFlags.length){
        user.userFlags.forEach(u => {
          if(u.includes("Series 50")){
            series50 += 1
          } else {
            nonSeries50 += 1
          }
        })
      }

      const client = clientLength.find(c => c._id.toString() === tenantId.toString())
      const prospect = prospectLength.find(p => p._id.toString() === tenantId.toString())
      const thirdParty = thirdPartyLength.find(t => t._id.toString() === tenantId.toString())

      snapShots.push({
        firmName,
        tenantId,
        users: {
          series50,
          nonSeries50
        },
        transactions: Deal + BankLoan + Derivative + Others + RFP + MaRfp + BusinessDevelopment,
        client: client && client.count || 0,
        prospect: prospect && prospect.count || 0,
        thirdParty: thirdParty && thirdParty.count || 0,
        transactionWise: {
          Deal,
          BankLoan,
          Derivative,
          Others,
          RFP,
          MaRfp,
          BusinessDevelopment
        },
        date: new Date()
      })
    })

    res.json({snapShots})

  } catch (error) {
    console.log({error})
  }

}

export const fetchBillingInvoices = async ( req, res) => {
  const { entityId } = req.params
  try {
    const invoiceList = await axios.post(`${process.env.PLATFORM_API_URL}/service/invoice`, {entityId} ,{
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })
    res.status(200).send(invoiceList.data)
  } catch(e) {
    console.log(e)
    res.status(500).send({done: false,  error:e})
  }

}

export const postMakePayment = async ( req, res) => {

  try {

    const payment = await axios.post(`${process.env.PLATFORM_API_URL}/service/payment`, req.body,{
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })

    res.send(payment && payment.data)

  } catch(err) {
    res.send({done: false,  error: err, message: err.message || ""})
  }

}

export const fetchPlateFormDocs = async ( req, res) => {

  const { docId } = req.params
  try {
    const doc = await axios.get(`${process.env.PLATFORM_API_URL}/service/plateformdocs/${docId}`, {
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })

    res.status(200).send(doc && doc.data && doc.data.doc)
  } catch(e) {
    console.log(e)
    res.status(500).send({done:true, userDetails:[], error:e})
  }

}
