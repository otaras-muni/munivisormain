import express from "express"
import {
  getPlateFormAdminPickLists,
  getPlateFormAdminUserAndTransaction,
  fetchBillingInvoices,
  postMakePayment,
  fetchPlateFormDocs
} from "./commonservices.controller"
import {platFormAuthMiddleware} from "../auth"
import { requireAuth } from "../../../../authorization/authsessionmanagement/auth.middleware"

export const commonservicesRouter = express.Router()

commonservicesRouter
  .route("/snapshots")
  .get( getPlateFormAdminUserAndTransaction)

commonservicesRouter
  .route("/picklists")
  .get(platFormAuthMiddleware, getPlateFormAdminPickLists)

commonservicesRouter
  .route("/billing/:entityId")
  .get(requireAuth, fetchBillingInvoices)

commonservicesRouter
  .route("/payment")
  .post(requireAuth, postMakePayment)

commonservicesRouter
  .route("/plateformdocs/:docId")
  .get(requireAuth, fetchPlateFormDocs)
