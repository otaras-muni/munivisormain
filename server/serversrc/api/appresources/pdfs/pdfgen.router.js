import express from "express"
import {
  generatePDFDoc,
  generateJSONToPDF,
  generateInvoicePDF,
  generateG10PDF,
  generateDistributionPDF
} from "./pdfgen.controller"

import {requireAuth} from "./../../../authorization/authsessionmanagement/auth.middleware"

export const pdfGeneratorRouter = express.Router()

pdfGeneratorRouter.route("/testpdf")
  .get(requireAuth,generatePDFDoc)

pdfGeneratorRouter.route("/jsontopdf")
  .post(requireAuth, generateJSONToPDF)

pdfGeneratorRouter.route("/generateinvoicepdf")
  .post(requireAuth, generateInvoicePDF)

pdfGeneratorRouter.route("/generateg10pdf")
  .post(requireAuth, generateG10PDF)

pdfGeneratorRouter.route("/distributionpdf")
  .post(requireAuth, generateDistributionPDF)
