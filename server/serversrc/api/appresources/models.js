import {Deals} from "./deals"
import {Entity} from "./entity"
import {EntityUser} from "./entityUser"
import {EntityRel} from "./entityRel"
import {RFP} from "./rfp"
import {Tasks} from "./taskmanagement"
import {BankLoans,Derivatives,ActMaRFP,Others,ActBusDev} from "./activities"
import {TenantBilling, BillingManageExpense} from "./billing"
import {DmMapping} from "./dmmapping"
import {DocFolder} from "./docfolder"
import {Docs} from "./docs"
import {Messages} from "./messages"
import {Notifications} from "./notification"

export {Deals} from "./deals"
export {Entity,GlobRefEntity} from "./entity"
export {EntityUser} from "./entityUser"
export {EntityRel} from "./entityRel"
export {RFP} from "./rfp"
export {Tasks} from "./taskmanagement"
export {Config} from "./config"
export {BankLoans,Derivatives,ActMaRFP,Others,ActBusDev} from "./activities"
export {SearchPref} from "./search"
export {TenantBilling, BillingManageExpense} from "./billing"
export {AuditLog} from "./auditlog"
export {Docs} from "./docs"
export {DocFolder} from "./docfolder"
export {DataOnboarding} from "./datamigration"
export {UserEntitlements} from "./entitlements"
export {Controls,ControlsActions} from "./cac"
export {Person} from "./testsubdocuments"
export {Notifications} from "./notification"
export {Messages} from "./messages"
export {DmMapping} from "./dmmapping"

export {

  SuperVisoryObligations,
  ComplSupervisor,
  ProfQualifications,
  PoliticalContributions,
  PoliContributionDetails,
  PoliContributionSummary,
  GiftsAndGratuities,
  GeneralAdmin,
  ComplaintDetails,
  BusConduct

} from "./compliance"

export const modelLookupConfigForElasticSearch = {
  "entities": () => Entity,
  "entityrels": () => EntityRel,
  "entityusers": () => EntityUser,
  "tranagencydeals": () => Deals,
  "tranagencyrfps": () => RFP,
  "mvtasks": () => Tasks,
  "tranbankloans": () => BankLoans,
  "tranderivatives": () => Derivatives,
  "actmarfps": () => ActMaRFP,
  "actbusdevs": () => ActBusDev,
  "tranagencyothers": () => Others,
  "dmmappings": () => DmMapping,
  "docfolders": () => DocFolder,
  "notifications": () => Notifications,
  "messages": () => Messages,
  "docs": () => Docs,
  "billingmanageexpenses": () => BillingManageExpense
}

