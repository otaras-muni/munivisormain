const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// RFP Schema for managing all RFPs

const rfpSchema = Schema({

  rfpName: { type: String, required: true },
  rfpServiceCategory: String,
  rfpStatus: String,

  rfpKeyMilestones: {
    rfpInitiationDate: Date,
    rfpQandADate: Date,
    rfpResponseStartDate: Date,
    rfpResponseEndDate: Date,
    rfpFinalDeadline: Date
  },

  rfpContent: [
    {
      documentType: { type: String },
      documentFileName: String,
      documentVersions: [
        {
          documentVersion: Number,
          documentRepository: String,
          documentUploadDate: Date
        }
      ]
    }
  ],

  rfpMgmtTeam: [
    {
      rfpParticipantId: String, // TODO Link to Participant CRM ID
      rfpParticipantUserList: [
        {
          rfpParticipantId: String,
          rfpMgmtUserId: String
        }
      ]
    }
  ],

  rfpParticipants: [
    {
      rfpParticipantId: String, // TODO Link to CRM Participant ID
      rfpParticipantEmailId: String,
      rfpParticipantName: String,
      rfpParticipantAddress: String,
      rfpParticipantDesignation: String,
      rfpAcknowledgementDate: Date,
      rfpParticipantStatus: String,
      rfpResponse: [
        {
          responseDate: Date,
          responseContentType: String,
          responseContent: [
            {
              responseDocumentName: String,
              responseDocumentRepository: String,
              responseDocumentVersion: Number
            }
          ]
        }
      ],

      rfpInteractions: [
        {
          interactionDate: Date,
          interactionMode: String,
          messages: [
            {
              messagedescription: String,
              messagedate: Date
            }
          ]
        }
      ],

      rfpResponseEval: [
        {
          rfpMgmtTeamUserId: String,
          rfpDecisionCriteria: [
            {
              rfpCriteria: String,
              rfpUserRating: [
                {
                  rating: String,
                  ratingDate: Date
                }
              ]
            }
          ]
        }
      ]
    }
  ],
  rfpTasks: [rfpTaskSchema]
});

const RFPModel = mongoose.model("rfp", rfpSchema);

const rfpTaskSchema = Schema({
  title: String,
  description: String,
  dueDate: Date,
  status: String,
  asignees: [String]
});
