import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
const { Schema } = mongoose

export const eSignSchema = new Schema({
  _id: false,
  envelopeId: String,
  status: String,
  statusDateTime: Date,
  sent: Date,
  signerEmail: String,
  created: Date,
  delivered: Date,
  signed: Date,
  completed: Date,
  fileDate: String
}, {strict: false })

// export const s3VersionSchema = new Schema({
//   _id: false,
//   versionId: String,
//   uploadDate: Date,
//   size: String
// }, {strict: false })

export const docMetaSchema = new Schema({
  _id: false,
  type: String,
  versions: [{}, { strict: false, _id: false }],
  eSign: eSignSchema
}, {strict: false })

const docSchema = new Schema({
  tenantId: String,
  name: String,
  originalName: String,
  folderId: String,
  contextType: String,
  contextId: String,
  meta: docMetaSchema,
  createdDate: Date,
  createdBy: String,
})
docSchema.plugin(timestamps)
docSchema.index({tenantId:1, contextId:1, contextType:1})

export const Docs = mongoose.model("docs", docSchema)
