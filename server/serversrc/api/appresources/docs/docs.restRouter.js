import express from "express"
import docsController from "./docs.controller"
import { Docs } from "./docs.model"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const docsRouter = express.Router()




docsRouter.post("/update-versions", requireAuth,async (req, res) => {
  const { _id, versions, name, originalName, option } = req.body
  const newVersionDetails = (versions || []).map( v => ({
    uploadedByUserId:req.user._id, 
    uploadedByUserTenantId:req.user.tenantId,
    uploadedByUserEntityId:req.user.entityId,
    ...v, 
  }))

  if(!_id || !option || !newVersionDetails || !newVersionDetails.length) {
    return res.status(422).send({ error: "invalid input" })
  }

  try {
    if(option === "add") {
      if(!name || !originalName) {
        return res.status(422).send({ error: "invalid input" })
      }
      const result = await Docs.update({ _id }, {
        $set: { name, originalName },
        $push: { "meta.versions": { $each: newVersionDetails } }
      })
      res.json(result)
    } else if(option === "remove") {
      const result = await Docs.update({ _id }, {
        $pull: { "meta.versions": {
          versionId: { $in: newVersionDetails.filter(e => e) }
        } }
      })
      res.json(result)
    } else {
      return res.status(422).send({ error: "Wrong update option provided" })
    }
  } catch (err) {
    return res.status(422).send({ error: err })
  }
})

docsRouter.param("id", docsController.findByParam)

docsRouter.route("/")
  .get(requireAuth,docsController.getAll)
  .put(requireAuth,docsController.updateFoldersDocs)
  .post(requireAuth,docsController.createOne)

docsRouter.route("/:id")
  .get(requireAuth,docsController.getOne)
  .put(requireAuth,docsController.updateOne)
  .delete(requireAuth,docsController.deleteOne)
