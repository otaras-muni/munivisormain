import mongoose from "mongoose";

const { Schema } = mongoose

export const fruitSchema = new Schema({
  title: String,
  items: [
    {
      fruit: String,
      date: String,
      applicable: Boolean,
      platformConfig: Boolean
    }
  ]
})

const testSchema = new mongoose.Schema({
  fruits: [fruitSchema]
})

export const Test = mongoose.model("test", testSchema)
