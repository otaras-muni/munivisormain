import { generateControllers } from '../../modules/query'
import { Test } from './test.model'

export default generateControllers(Test)
