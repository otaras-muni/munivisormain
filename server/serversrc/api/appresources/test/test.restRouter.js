import express from "express"
import testController from "./test.controller"
import { checkInputDataSecurity } from "./../../../middlewares"

export const testRouter = express.Router()

testRouter.param("id", testController.findByParam)

testRouter.route("/")
  .get(testController.getAll)
  .post(testController.createOne)

testRouter.route("/:id")
  .get(testController.getOne)
  .put(testController.updateOne)
  .delete(testController.createOne)

testRouter.route("/securitytest/")
  .post(checkInputDataSecurity(), ( req, res) => {
    res.status(200).send({message:"POST REQUEST HELLO YOU WERE ABLE TO ACCESS SUCCESSFULLY AFTER THE CONTENTS WERE VALIDATED", data:req.body})
  } )
  .put(checkInputDataSecurity(), ( req, res) => {
    res.status(200).send({message:"HELLO YOU WERE ABLE TO ACCESS SUCCESSFULLY AFTER THE CONTENTS WERE VALIDATED", data:req.body})
  } )
