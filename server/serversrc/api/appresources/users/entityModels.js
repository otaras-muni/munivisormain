import mongoose, {Schema} from "mongoose"

//mongoose.Promise = global.Promise
//mongoose.connect("mongodb://admin:admin123@ds133746.mlab.com:33746/munivisordevsearch")

export const entityAddressSchema = Schema({
  addressName:String,
  isPrimary:Boolean,
  isHeadQuarter:Boolean,
  isActive:Boolean,
  website:String,
  officePhone:[
    {countryCode:String,phoneNumber:String, extension:String}
  ],
  officeFax:[
    { faxNumber:String }
  ],
  officeEmails:[{emailId:String}],
  addressLine1:String,
  addressLine2:String,
  county:String,
  country:String,
  state:String,
  city:String,
  zipCode:{zip1:String, zip2:String}
})

export const firmAddOnsSchema =Schema({
  serviceName:String,
  serviceEnabled:Boolean
})

const entityFlagSchema = Schema({
  marketRole:[String],
  issuerFlags:[String]
})

export const entityLinkCusipSchema = Schema({
  debtType:String,
  associatedCusip6:String,
})

export const entityLinkBorrowersObligorsSchema = Schema({
  borOblRel:String,
  borOblFirmName:String,
  borOblDebtType:String,
  borOblCusip6:String,
  borOblStartDate:Date,
  borOblEndDate:Date
})

// eslint-disable-next-line no-unused-vars
export const entitySchema = Schema({
  _id: { type: Schema.ObjectId, required: true},
  entityFlags:entityFlagSchema,
  isMuniVisorClient:Boolean,
  msrbFirmName:String,
  msrbRegistrantType:String,
  msrbId:String,
  entityAliases:[String],
  firmName:String,
  taxId:String,
  businessStructure:String,
  numEmployees:String,
  annualRevenue:String,
  addresses:[entityAddressSchema],
  firmAddOns:[firmAddOnsSchema],
  entityLinkedCusips:[entityLinkCusipSchema],
  entityBorObl:[entityLinkBorrowersObligorsSchema] 
})

// eslint-disable-next-line no-unused-vars

export const Entity =  mongoose.model("entity", entitySchema)







