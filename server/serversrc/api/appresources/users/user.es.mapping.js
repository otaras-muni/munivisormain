module.exports = {
  "properties": {
    "userId": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "entityId": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userFlags": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userRole": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userEntitlement": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userFirstName": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userLastName": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userEmails": {
      "properties": {
        "emailId": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "emailPrimary": {
          "type": "boolean"
        }
      }
    },
    "userEmployeeID": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userJobTitle": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userManagerEmail": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "userJoiningDate": {
      "type":"date"
    },
    "userExitDate": {
      "type":"date"
    },
    "userCostCenter": {
      "type":"text",
      "fields": {
        "raw":{
          "type":"keyword"
        },
        "searchable":{
          "type":"text",
          "analyzer":"searchableText"
        }
      }
    },
    "addresses": {
      "properties": {
        "addressName": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "addressType": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "addressLine1": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "addressLine2": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "country": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "state": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "city": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "zipCode": {
          "properties": {
            "zip1": {
              "type":"text",
              "fields": {
                "raw":{
                  "type":"keyword"
                },
                "searchable":{
                  "type":"text",
                  "analyzer":"searchableText"
                }
              }
            },
            "zip2": {
              "type":"text",
              "fields": {
                "raw":{
                  "type":"keyword"
                },
                "searchable":{
                  "type":"text",
                  "analyzer":"searchableText"
                }
              }
            }
          }
        }
      }
    },
    "userAddOns": {
      "properties": {
        "serviceType": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "serviceEnabled": {
          "type":"boolean",
        }
      }
    },
    "userLoginCredentials": {
      "properties": {
        "_id": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "userId": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "userEmailId": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "password": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "passwordConf": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "passwordResetString": {
          "type":"text",
          "fields": {
            "raw":{
              "type":"keyword"
            },
            "searchable":{
              "type":"text",
              "analyzer":"searchableText"
            }
          }
        },
        "passwordResetDate": {
          "type": "date"
        }
      }
    },
    "userAddDate": {
      "type": "date"
    },
    "userUpdateDate": {
      "type": "date"
    }
  }
}
