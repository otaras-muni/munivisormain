import mongoose, {Schema} from "mongoose"
import { Entity } from "./entityModels"

// mongoose.Promise = global.Promise

export const userAddressSchema = Schema({
  addressName:String,
  addressType:String, // Office address, headquarter or residence
  addressLine1:String,
  addressLine2:String,
  country:String,
  state:String,
  city:String,
  zipCode:{zip1:String, zip2:String}
})

const userAddonSchema = Schema({
  serviceType:String,
  serviceEnabled:String
})

// eslint-disable-next-line no-unused-vars
const userCredentialsSchema = Schema({
  _id:String,
  userId:String, // Should come from the user Collection
  userEmailId:String, // We can get rid of this if we are able to just use the email ID.
  password:String,  // This is the salted password.
  passwordConf:String,
  passwordResetString:String,
  passwordResetDate:Date
})

// eslint-disable-next-line no-unused-vars
const userLoginSession = Schema({
  _id:String,
  userId:String,
  ipAddress:String,
  sessionStartTime:Date,
  sessionEndTime:Date,
})

const userSchema = Schema({
  _id: { type: Schema.ObjectId, required: true},
  userId:String, // Should we make this the same as the primary email
  entityId: { type: Schema.Types.ObjectId, ref: Entity },
  userFlags: [String], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
  userRole:String, // [ Admin, ReadOnly, Backup ]
  userEntitlement:String, // [ Global, Transaction ]
  userFirstName:String,
  userMiddleName:String,
  userLastName:String,
  userEmails:[{
    emailId:String,
    emailPrimary:Boolean,
  }],
  userEmployeeID:String,
  userJobTitle:String,
  userManagerEmail:String,
  userJoiningDate:Date,
  userExitDate:Date,
  userCostCenter:String,
  addresses:[userAddressSchema],
  userAddOns:[userAddonSchema],
  userLoginCredentials:userCredentialsSchema,
  userAddDate:Date,
  userUpdateDate:Date
})

export const EntityUser =  mongoose.model("entityuser", userSchema)
