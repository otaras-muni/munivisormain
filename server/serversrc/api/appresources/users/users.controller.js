import { generateControllers } from "../../modules/query"
import { EntityUser } from "../entityUser/entityUser.model"

export default generateControllers(EntityUser)
