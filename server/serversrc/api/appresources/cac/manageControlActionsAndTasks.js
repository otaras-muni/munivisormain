import moment from "moment"
import ip from "ip"
import {Controls, Tasks, ControlsActions, AuditLog} from "./../models"
import {PoliContributionDetails} from "../compliance/PoliticalContributions"
import {GiftsAndGratuities} from "../compliance/GiftsGratuities"
import { elasticSearchNormalizationAndBulkInsert } from "./../../elasticsearch/esHelper"

const {ObjectID} = require("mongodb")
const Bson_ObjectID = require("bson-objectid")

const currentYear = parseInt(moment(new Date()).utc().format("YYYY"), 10)
const currentQuarter = moment(new Date()).utc().quarter()

export const returnActionTasksDisclosure = (data, controlId, refId, type, recipients, nextDate, userIds, user, startDate,actionTaskDescription) => {
  console.log("nextDate is:",nextDate)
  const controlActions = []
  const controlTasks = []
  const disclosures = []
  const {topic, name, docIds} = data
  const docs = docIds && docIds.map(item => ({
    name: item.name,
    docId: item.docId,
    date: item.date,
    by: item.by
  }))
  for (const i in userIds) {
    const taskRefId = Bson_ObjectID()
    const payload = {
      _id: taskRefId,
      ...data,
      notes:actionTaskDescription || data.notes,
      refId,
      userId: userIds[i],
      parentId: controlId,
      dueDate: nextDate ? moment(nextDate).format("YYYY-MM-DD") : "",
      docIds: docs || [],
      refRule: (data && data.refRule) || "",
      receivedDate: new Date(),
      status: "due",
      affirmation: type === "affirm",
      tranUrl: data && data.tranUrl || "",
      createdAt: new Date(),
      updatedAt: new Date()
    }
    controlActions.push(payload)

    const matchUser = recipients && recipients.find(alr => alr.id.toString() === userIds[i].toString())
    const payloadTask = {
      taskDetails : {
        parentId: controlId,
        taskFollowing: true,
        taskUnread: true,
        taskAssigneeUserId: userIds[i],
        taskAssigneeName: (matchUser && matchUser.name) || "",
        taskNotes: taskRefId,
        taskDescription: actionTaskDescription || `Compliance task with topic - ${topic} : ${name}`,
        taskStartDate: nextDate ? moment(nextDate).format("YYYY-MM-DD") : "",
        taskEndDate: nextDate ? moment(nextDate).format("YYYY-MM-DD") : "",
        taskType: "Compliance",
        taskStatus: "Open",
        complianceTask: true,
        tranUrl: data && data.tranUrl || ""
      },
      taskHistory: [],
      relatedContacts: [],
      taskRelatedDocuments: [],
      updatedAt: new Date(),
      createdAt: new Date(),
    }
    controlTasks.push(payloadTask)

    if((topic === "Gifts and Gratuities" || topic === "Political Contributions and Prohibitions on Municipal Advisory Business") && type === "affirm"){
      const year = parseInt(moment(startDate).format("YYYY"), 10)
      const quarter = moment(startDate).quarter()

      const isPreApproval = topic === "Gifts and Gratuities" ? (year > currentYear) :
        ((parseInt(year,10) > currentYear) || ((parseInt(quarter,10) > currentQuarter) && (parseInt(year,10) === currentYear)))

      const disclosure = {
        finAdvisorEntityId: user.entityId,
        finAdvisorEntityName: user.userFirmName,
        submitter: `${user.userFirstName} ${user.userLastName}`,
        userOrEntityId: matchUser.id,
        userFirstName: matchUser.name,
        userLastName: "",
        entityName: matchUser.firmName,
        controlName: data.name || "",
        cac: true,
        quarter: topic === "Gifts and Gratuities" ? "All" : moment(nextDate).quarter(),
        year: moment(nextDate).year(),
        controlEffectiveDate:nextDate,
        disclosureFor: userIds[i],
        status: "Pending",
        preApprovalSought: isPreApproval ? "Yes" : "No",
        controlId,
        taskRefId,
        createdAt: new Date(),
        updatedAt: new Date()
      }
      disclosures.push(disclosure)
    }
  }
  return {controlActions, controlTasks, disclosures}
}



export const getAllEligibleControlActionDates = async (payload, entityId, controlId, user,actionTaskDescription) => {
  console.log("===================payload================>", payload)
  try {
    const {
      id,
      refId: parentId,
      name,
      topic,
      type,
      dueDate,
      docIds,
      notification,
      checklist,
      relatedEntity,
      relatedActivity,
      relatedContact,
      subTopic,
      otherTopic,
      refRule,
      notes,
      recipients,
      refId,
      tranUrl
    } = payload

    const data = {
      id,
      parentId,
      name,
      topic,
      dueDate,
      docIds,
      notification,
      checklist,
      relatedEntity,
      relatedActivity,
      relatedContact,
      subTopic,
      otherTopic,
      refRule,
      notes,
      tranUrl
    }

    // console.log("===================payload.notification================>", payload.notification)
    const notificationPayload = (payload && payload.notification) || {}
    // console.log("===================notificationPayload================>", notificationPayload)

    const {
      recurring,
      frequency,
      unit,
      recurringType,
      recurringPattern1,
      recurringPattern2
    } = notificationPayload

    let {startDate, endDate} = notificationPayload
    
    const userIds = recipients && recipients.map(e => e.id)
    
    let preStartDate
    let nextDate
    let day
    let actionDay
    let actionTimestamp
    let numPeriodIterator = 0
    let nextDatePeriodIterator
    let startRefeferenceTimestamp 
    let endReferenceTimestamp
    let currentReferenceTimestamp
    const currentDate = new Date()

    const notificationConfigDates = []
    const controlActionsToBeInserted = []
    const tasksToBeInserted = []

    let controlActions = []
    let controlTasks = []
    let disclosures = []

    const startDateTimeStamp = moment(startDate).startOf("day").unix()
    const endDateTimeStamp = moment(endDate).startOf("day").unix()
    const currentDateTimeStamp = moment(currentDate).startOf("day").unix()

  
    startDate = moment(startDate).format("YYYY-MM-DD")
    endDate = moment(endDate).format("YYYY-MM-DD")

    if ( recurringType === "weekly") {

      preStartDate = moment(startDate)
        .subtract(frequency,unit)
        .day(recurringPattern2)
        .add(frequency,unit)
        .subtract(1,"days")
        .unix()
    
      const referenceTimeStamp = moment(startDate)
        .subtract(frequency,unit)
        .day(recurringPattern2)
        .add(frequency,unit)
        .subtract(1,"days")
        .unix()

      nextDatePeriodIterator = referenceTimeStamp
      startRefeferenceTimestamp = moment(startDate).startOf("week").unix()          
      endReferenceTimestamp = moment(endDate).startOf("week").unix()          
      currentReferenceTimestamp = moment(currentDate).startOf("week").unix()

      console.log("1. PRE START DATE IS CONFIGURED FOR WEEKLY RECURRENCE PATTERN: ", moment(preStartDate).format("dddd, MMMM Do YYYY, h:mm:ss a"))
      // let nextDate = preStartDate.add(frequency, unit).toDate()
      nextDate = preStartDate
      while(nextDate && endDateTimeStamp && ((nextDate <= endDateTimeStamp) || (nextDatePeriodIterator <= endReferenceTimestamp ) )) {
        console.log(`2.${numPeriodIterator} : NEXT DATE IS`, nextDate)
        let actionCreated = "pending"
        /*
        if(nextDate >= startDateTimeStamp && nextDate < currentDateTimeStamp) {
          const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(nextDate).toDate(), userIds, user)
          actionCreated = "created"
          controlActions = [...controlActions,...result.controlActions]
          controlTasks = [...controlTasks, ...result.controlTasks]
          disclosures = [...disclosures,...result.disclosures]
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(nextDate).toDate(),actionDateTimestamp1:moment.unix(nextDate),actionDateTimestamp:nextDate,actionCreated,createdAt:new Date()})
        }
*/
        day = moment.unix(nextDate).format("YYYY-MM-DD")
        actionDay = moment(day).format("dddd")
        if (actionDay === "Sunday") {
          actionTimestamp = moment(day).add(1, "days").unix()
        } else {
          actionTimestamp = nextDate
        }

        if((nextDatePeriodIterator >= startRefeferenceTimestamp) && (nextDatePeriodIterator >= currentReferenceTimestamp)) {
          numPeriodIterator += 1
          if (numPeriodIterator === 1) {
            console.log("Create the task for the first period here")
            const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(actionTimestamp).toDate(), userIds, user)
            controlActions = [...controlActions,...result.controlActions]
            controlTasks = [...controlTasks, ...result.controlTasks]
            disclosures = [...disclosures,...result.disclosures]
            actionCreated = "created"
            controlActionsToBeInserted.push(`NEW CONTROL ACTION ${numPeriodIterator}`)
            tasksToBeInserted.push(`NEW CONTROL RELATED TASK - ${numPeriodIterator}`)
          }
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(actionTimestamp).toDate(),actionDateTimestamp1:moment.unix(actionTimestamp),actionDateTimestamp:actionTimestamp,actionCreated,createdAt:new Date()})
        }
        nextDate = moment.unix(nextDate).add(frequency, `${unit}s`).unix()
        nextDatePeriodIterator = moment.unix(nextDatePeriodIterator).add(frequency, `${unit}s`).unix()

      }
      console.log("3. THE DATES FOR NOTIFICATION ARE", JSON.stringify(notificationConfigDates, null,2))
    }

    if (recurringType === "monthly" || recurringType === "quarterly") {
      preStartDate = moment(startDate)
        .subtract(frequency,unit)
        .date(recurringPattern2)
        .add(frequency,unit)
        .unix()

      let referenceTimeStamp 
      if (recurringType === "monthly") {
        referenceTimeStamp = moment(startDate)
          .subtract(frequency,unit)
          .date(recurringPattern2)
          .add(frequency,unit)
          .startOf("month")
          .unix()
          
        startRefeferenceTimestamp = moment(startDate).startOf("month").unix()          
        endReferenceTimestamp = moment(endDate).startOf("month").unix()
        currentReferenceTimestamp = moment(currentDate).startOf("month").unix()
      }

      if (recurringType === "quarterly") {
        referenceTimeStamp = moment(startDate)
          .subtract(frequency,unit)
          .date(recurringPattern2)
          .add(frequency,unit)
          .startOf("quarter")
          .unix()

        startRefeferenceTimestamp = moment(startDate).startOf("quarter").unix()          
        endReferenceTimestamp = moment(endDate).startOf("quarter").unix()
        currentReferenceTimestamp = moment(currentDate).startOf("quarter").unix()
      }

      // currentReferenceTimestamp = moment(currentDate).startOf("month").unix()

      console.log("1. PRE START DATE IS CONFIGURED FOR MONTHLY RECURRENCE PATTERN: ", moment(preStartDate).format("dddd, MMMM Do YYYY, h:mm:ss a"))
      nextDate = preStartDate
      nextDatePeriodIterator = referenceTimeStamp
      numPeriodIterator = 0

      console.log("===================start============>", startDateTimeStamp)
      console.log("=================end==============>", endDateTimeStamp)

      while(nextDate && endDateTimeStamp && ((nextDate <= endDateTimeStamp) || (nextDatePeriodIterator <= endReferenceTimestamp ) )) {
        let actionCreated = "pending"
        console.log(`2.${numPeriodIterator} : NEXT DATE IS`, nextDate)
        console.log("=================nextdate TimeStamp==============>", moment(nextDate).startOf("day").unix())

        day = moment.unix(nextDate).format("YYYY-MM-DD")
        actionDay = moment(day).format("dddd")
        if (actionDay === "Sunday") {
          actionTimestamp = moment(day).add(1, "days").unix()
        } else {
          actionTimestamp = nextDate
        }

        /*
        if((nextDate >= startDateTimeStamp) && (nextDate < currentDateTimeStamp)) {
          const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(nextDate).toDate(), userIds, user, startDate)
          actionCreated = "created"
          controlActions = [...controlActions,...result.controlActions]
          controlTasks = [...controlTasks, ...result.controlTasks]
          disclosures = [...disclosures,...result.disclosures]
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(nextDate).toDate(),actionDateTimestamp1:moment.unix(nextDate),actionDateTimestamp:nextDate,actionCreated,createdAt:new Date()})
        } 
*/

        if((nextDatePeriodIterator >= startRefeferenceTimestamp) && (nextDatePeriodIterator >= currentReferenceTimestamp)) {

          console.log("The next period iterator is: ",nextDatePeriodIterator)
          numPeriodIterator += 1
          if (numPeriodIterator === 1) {
            actionCreated = "created"
            const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(actionTimestamp).toDate(), userIds, user, startDate)
            controlActions = [...controlActions,...result.controlActions]
            controlTasks = [...controlTasks, ...result.controlTasks]
            disclosures = [...disclosures,...result.disclosures]
            controlActionsToBeInserted.push(`NEW CONTROL ACTION ${numPeriodIterator}`)
            tasksToBeInserted.push(`NEW CONTROL RELATED TASK - ${numPeriodIterator}`)
          }
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(actionTimestamp).toDate(),actionDateTimestamp1:moment.unix(actionTimestamp),actionDateTimestamp:actionTimestamp,actionCreated,createdAt:new Date()})
        }
        nextDate = moment.unix(nextDate).add(frequency, `${unit}s`).unix()
        nextDatePeriodIterator = moment.unix(nextDatePeriodIterator).add(frequency, `${unit}s`).unix()
      }
      console.log("3. THE DATES FOR NOTIFICATION ARE", JSON.stringify(notificationConfigDates, null,2))
    }

    if (recurringType === "yearly") {

      preStartDate = moment(startDate).subtract(1,"year").unix()

      console.log("1. PRE START DATE IS CONFIGURED FOR MONTHLY RECURRENCE PATTERN: ", moment(preStartDate).format("dddd, MMMM Do YYYY, h:mm:ss a"))
      nextDate = preStartDate

      console.log("===================start============>", startDateTimeStamp)
      console.log("=================end==============>", endDateTimeStamp)
      
      const referenceTimeStamp = moment(startDate).subtract(1,"year").unix()

      nextDatePeriodIterator = referenceTimeStamp
      startRefeferenceTimestamp = moment(startDate).startOf("year").unix()          
      endReferenceTimestamp = moment(endDate).startOf("year").unix()          
      currentReferenceTimestamp = moment(currentDate).startOf("year").unix()

 
      numPeriodIterator = 0
      while(nextDate && endDateTimeStamp && ((nextDate <= endDateTimeStamp) || (nextDatePeriodIterator <= endReferenceTimestamp ) )) {
        let actionCreated = "pending"
        console.log(`2.${numPeriodIterator} : NEXT DATE IS`, nextDate)
        console.log("=================nextdate TimeStamp==============>", moment(nextDate).startOf("day").unix())
        /*
        if((nextDate >= startDateTimeStamp) && (nextDate < currentDateTimeStamp)) {
          const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(nextDate).toDate(), userIds, user, startDate)
          actionCreated = "created"
          controlActions = [...controlActions,...result.controlActions]
          controlTasks = [...controlTasks, ...result.controlTasks]
          disclosures = [...disclosures,...result.disclosures]
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(nextDate).toDate(),actionDateTimestamp1:moment.unix(nextDate),actionDateTimestamp:nextDate,actionCreated,createdAt:new Date()})
        }
*/

        day = moment.unix(nextDate).format("YYYY-MM-DD")
        actionDay = moment(day).format("dddd")
        if (actionDay === "Sunday") {
          actionTimestamp = moment(day).add(1, "days").unix()
        } else {
          actionTimestamp = nextDate
        }

        if((nextDatePeriodIterator >= startRefeferenceTimestamp) && (nextDatePeriodIterator >= currentReferenceTimestamp)) {
          numPeriodIterator += 1
          if (numPeriodIterator === 1) {
            actionCreated = "created"
            const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(actionTimestamp).toDate(), userIds, user, startDate)
            controlActions = [...controlActions,...result.controlActions]
            controlTasks = [...controlTasks, ...result.controlTasks]
            disclosures = [...disclosures,...result.disclosures]
            controlActionsToBeInserted.push(`NEW CONTROL ACTION ${numPeriodIterator}`)
            tasksToBeInserted.push(`NEW CONTROL RELATED TASK - ${numPeriodIterator}`)
          }
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(actionTimestamp).toDate(),actionDateTimestamp1:moment.unix(actionTimestamp),actionDateTimestamp:actionTimestamp,actionCreated,createdAt:new Date()})
        }
        nextDate = moment.unix(nextDate).add(1,"y").unix()
        nextDatePeriodIterator = moment.unix(nextDatePeriodIterator).add(1,"y").unix()
      }
      console.log("3. THE DATES FOR NOTIFICATION ARE", JSON.stringify(notificationConfigDates, null,2))

    }
    

    if (recurringType === "daily") {

      preStartDate = moment(startDate).subtract(1,"d").startOf("day").unix()
      const referenceTimeStamp = moment(startDate).subtract(1,"day").startOf("day").unix()

      nextDatePeriodIterator = referenceTimeStamp
      startRefeferenceTimestamp = moment(startDate).startOf("day").unix()          
      endReferenceTimestamp = moment(endDate).startOf("day").unix()          
      currentReferenceTimestamp = moment(currentDate).startOf("day").unix()

      console.log("1. PRE START DATE IS CONFIGURED FOR MONTHLY RECURRENCE PATTERN: ", moment(preStartDate).format("dddd, MMMM Do YYYY, h:mm:ss a"))
      nextDate = preStartDate

      console.log("===================start============>", startDateTimeStamp)
      console.log("=================end==============>", endDateTimeStamp)

      numPeriodIterator = 0
      while(nextDate && endDateTimeStamp && ((nextDate <= endDateTimeStamp) || (nextDatePeriodIterator <= endReferenceTimestamp ) )) {
        let actionCreated = "pending"
        console.log(`2.${numPeriodIterator} : NEXT DATE IS`, nextDate)
        console.log("=================nextdate TimeStamp==============>", moment(nextDate).startOf("day").unix())
        /*
        if((nextDate >= startDateTimeStamp) && (nextDate < currentDateTimeStamp)) {
          const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(nextDate).toDate(), userIds, user)
          actionCreated = "created"
          controlActions = [...controlActions,...result.controlActions]
          controlTasks = [...controlTasks, ...result.controlTasks]
          disclosures = [...disclosures,...result.disclosures]
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(nextDate).toDate(),actionDateTimestamp1:moment.unix(nextDate),actionDateTimestamp:nextDate,actionCreated,createdAt:new Date()})
        } 
*/

        day = moment.unix(nextDate).format("YYYY-MM-DD")
        actionDay = moment(day).format("dddd")
        if (actionDay === "Sunday") {
          actionTimestamp = moment(day).add(1, "days").unix()
        } else {
          actionTimestamp = nextDate
        }

        if((nextDatePeriodIterator >= startRefeferenceTimestamp) && (nextDatePeriodIterator >= currentReferenceTimestamp)) {
          numPeriodIterator += 1
          if (numPeriodIterator === 1) {
            actionCreated = "created"
            const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients, moment.unix(actionTimestamp).toDate(), userIds, user)
            controlActions = [...controlActions,...result.controlActions]
            controlTasks = [...controlTasks, ...result.controlTasks]
            disclosures = [...disclosures,...result.disclosures]
            controlActionsToBeInserted.push(`NEW CONTROL ACTION ${numPeriodIterator}`)
            tasksToBeInserted.push(`NEW CONTROL RELATED TASK - ${numPeriodIterator}`)
          }
          notificationConfigDates.push({notificationConfigId:Bson_ObjectID(),actionDate:moment.unix(actionTimestamp).toDate(),actionDateTimestamp1:moment.unix(actionTimestamp),actionDateTimestamp:actionTimestamp,actionCreated,createdAt:new Date()})
        }
        nextDate = moment.unix(nextDate).add(1,"d").startOf("day").unix()
        nextDatePeriodIterator = moment.unix(nextDatePeriodIterator).add(1,"day").unix()
      }
      console.log("3. THE DATES FOR NOTIFICATION ARE", JSON.stringify(notificationConfigDates, null,2))
    }

    else if (recurringType === "once") {
      const duedate = moment(new Date()).startOf("day")
      const result = returnActionTasksDisclosure(data, controlId, refId, type, recipients,duedate, userIds, user,"",actionTaskDescription)
      controlActions = [...controlActions,...result.controlActions]
      controlTasks = [...controlTasks, ...result.controlTasks]
      disclosures = [...disclosures,...result.disclosures]
      controlActionsToBeInserted.push("NEW CONTROL ACTION")
      tasksToBeInserted.push("NEW CONTROL RELATED TASK")
    }
    console.log("Here are the dates that need to be configured",JSON.stringify(notificationConfigDates,null,2))

    let newInsertedControlActions = []
    let newInsertedTasks = []
    if (controlTasks.length && controlActions.length) {
      newInsertedControlActions = await ControlsActions.insertMany(controlActions)
      newInsertedTasks = await Tasks.insertMany(controlTasks)
      const dataForEs = newInsertedTasks.map(d => d._id)
      const bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType:"mvtasks", ids:dataForEs,tenantId:user.tenantId})

      if(disclosures && disclosures.length){
        const model = topic === "Gifts and Gratuities" ? GiftsAndGratuities : PoliContributionDetails
        const giftsPolitical = await model.insertMany(disclosures)
        const giftsPoliticalLogs = []
        giftsPolitical.forEach(alr => {
          const log = {
            groupId: entityId,
            type: alr._id,
            changeLog: [
              {
                userName: `${user.userFirstName} ${user.userLastName}`,
                log: "created Disclosure from CAC",
                date: new Date(),
                key: "newCompliance",
                ip: ip.address(),
                userId: user._id
              }
            ]
          }
          giftsPoliticalLogs.push(log)
        })
        await AuditLog.insertMany(giftsPoliticalLogs)
      }
    }

    const pendingNotifications = notificationConfigDates
    console.log(`The entity ID - ${entityId} and the control ID - ${controlId}`)
    if(pendingNotifications && pendingNotifications.length) {
      await Controls.updateOne({entityId: ObjectID(entityId), "controls._id": controlId}, {
        // $set: { "controls.$.notificationConfigDates": notificationConfigDates.filter(e => e.actionCreated !== "created") }
        $set: { "controls.$.notificationConfigDates": notificationConfigDates }
      })
    }
    return {
      done: true,
      data: {
        controlActions: newInsertedControlActions,
        tasks: newInsertedTasks,
        notificationConfigDates
      }
    }

  } catch (e) {
    console.log("==getAllEligibleControlActionDates===========", e)
    return {}
  }

}
