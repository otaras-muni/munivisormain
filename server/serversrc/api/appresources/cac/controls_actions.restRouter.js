import express from "express"
import controlsActionsController, {createTaskControls}from "./controls_actions.controller"
import {Tasks, ControlsActions, Controls} from "./../models"

import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const controlsActionsRouter = express.Router()

const {ObjectID} = require("mongodb")

controlsActionsRouter.post("/action-status", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }
  const { taskId } = req.body

  if(!taskId) {
    return res.status(422).send({ error: "Input missing" })
  }
  try {
    const resControl = await ControlsActions.updateOne({_id: taskId},
      { $set: { status: "complete" } }
    )
    const resTask = await Tasks.updateOne({"taskDetails.taskNotes": taskId},
      { $set: { "taskDetails.taskStatus": "complete", status: "complete" } }
    )
    if(resControl && resTask) {
      return res.json({ message: "Status of Control-Action & Task Updated Successfully"})
    }
  } catch (err) {
    return res.status(422).send({ error: "Error in updating status", err })
  }
})

controlsActionsRouter.post("/get-actions-details", requireAuth, async (req, res) => {
  const { filter, projection } = req.body

  if(!filter) {
    return res.status(422).send({ error: "Input missing" })
  }
  try {
    const actions = await ControlsActions.find(filter).select(projection || {})
    return res.json(actions)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting control actions", err })
  }
})

controlsActionsRouter.get("/getControlsActions", requireAuth, async (req, res) => {
  const { cacId } = req.query
  if(!cacId) {
    return res.status(422).send({ error: "Input missing" })
  }
  try {
    const actions = await ControlsActions.find({id: cacId, status: "due"}).select("name")
    return res.json(actions)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting control actions", err })
  }
})

controlsActionsRouter.get("/fetchSpecificControlsActions", requireAuth, async (req, res) => {
  const { id } = req.query

  try {
    console.log({id})
    const control = await ControlsActions.findOne({ _id: id })
    res.json(control)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting Controls" })
  }
})

controlsActionsRouter.param("id", controlsActionsController.findByParam)

controlsActionsRouter.route("/")
  .get(requireAuth, controlsActionsController.getAll)
  .post(requireAuth, controlsActionsController.createOne)

controlsActionsRouter.route("/:id")
  .get(requireAuth, controlsActionsController.getOne)
  .put(requireAuth, controlsActionsController.updateOne)
  .delete(requireAuth, controlsActionsController.createOne)
