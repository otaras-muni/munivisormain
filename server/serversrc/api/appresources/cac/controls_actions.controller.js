import {ObjectID} from "mongodb"
import { generateControllers } from "../../modules/query"
import { ControlsActions } from "./controls_actions.model"
import {
  Controls,
} from "./../models"

import {Tasks} from "../taskmanagement/tasks.model"
import {updateEligibleIdsForLoggedInUserFirm} from "../../entitlementhelpers"
import {elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Tasks, _id: id, esType: "mvtasks"})

export const overrideGetAll = async (req, res, next) => {
  const { user } = req
  const userId = user._id
  if(!user || !userId) {
    return res.status(422).send({ error: "No user or userId found" })
  }
  const filter = { userId, $nor: [{affirmation: true, topic: "Gifts and Gratuities"},
    {affirmation: true, topic: "Political Contributions and Prohibitions on Municipal Advisory Business"}] }

  try {
    // const docs = await ControlsActions.find(filter)

    const docs = await Controls.aggregate([
      {
        $match: {
          entityId: ObjectID(req.user.tenantId)
        }
      },
      {
        $unwind: "$controls"
      },
      {
        $addFields:{
          stateOfTheControl:{$ifNull:["$controls.state", "enabled"]}
        }
      },
      {
        $match: {
          "controls.saveType": "final",
          "controls.status": "open",
          "stateOfTheControl":"enabled"
        }
      }, {
        $project: {
          controlUniqueId: "$controls._id",
          controlId: "$controls.id",
          controlDueDate: "$controls.dueDate",
          controlStatus: "$controls.status",
          controlName: "$controls.name",
          controlTopic: "$controls.topic",
          controlType: "$controls.type",
          controlActionReferenceId: "$controls.refId",
          controlNotificationSentDate: "$controls.notificationSentDate"
        }
      }, {
        $lookup: {
          from: "controlsactions",
          localField: "controlActionReferenceId",
          foreignField: "refId",
          as: "controlActions"
        }
      }, {
        $unwind: "$controlActions"
      },
      {
        $match:{
          "controlActions.userId":userId
        },
      },
      {
        $addFields:{
          donotShowValues:{
            $and:[
              {
                "$in":[
                  "$controlActions.topic",
                  ["Political Contributions and Prohibitions on Municipal Advisory Business","Gifts and Gratuities"]
                ]
              },
              {
                "$eq":["$controlActions.affirmation",true]
              }
            ]
          }
        }
      },{
        $match:{donotShowValues:false}
      },
      {
        $project:{
          _id:"$controlActions._id",
          id:"$controlActions.id",
          name:"$controlActions.name",
          topic:"$controlActions.topic",
          subtopic:"$controlActions.subtopic",
          status:"$controlActions.status",
          dueDate:"$controlActions.dueDate",
        }
      }
    ])
    /* const docs = await ControlsActions.aggregate([
      {
        $match:{
          userId
        },
      },
      {
        $addFields:{
          donotShowValues:{
            $and:[
              {
                "$in":[
                  "$topic",
                  [
                    "Political Contributions and Prohibitions on Municipal Advisory Business",
                    ,   "Gifts and Gratuities"
                  ]
                ]
              },
              {
                "$eq":["$affirmation",true]
              }
            ]
          }
        }
      },{
        $match:{donotShowValues:false}
      }
    ]) */
    res.json(docs)
  } catch (error) {
    next(error)
  }
}

const createControlActions = async (req, res) => {
  console.log("Entered Creating the Control Actions - New Routine")
  try {
    const newControlAction = new ControlsActions({
      ...req.body
    })
    const insertedControlAction = await newControlAction.save()
    res.json(insertedControlAction)
  } catch (error) {
    console.log("There is an Error in creating control Actions", error)
    res.status(500).send({ done: false, error: "Error saving the controls actions" })
  }
}

const updateControlActions = async (req, res) => {
  console.log("Entered Updating the Control Actions - New Routine")
  const {__v, _id, ...restOfControlActions} = req.body
  try {
    const result = await ControlsActions.findOneAndUpdate({_id}, restOfControlActions, {
      new: true,
      upsert: true
    })
    if(req.body.status === "complete"){
      const task = await Tasks.findOne({"taskDetails.taskNotes": _id})
      if(task){
        await Tasks.update({
          _id: task._id
        }, {
          $set: {
            "taskDetails.taskStatus": "Closed"
          }
        })
        await Promise.all([
          updateEligibleIdsForLoggedInUserFirm(req.user),
          returnFromES(req.user.tenantId, task._id)
        ])
      }
    }
    res.json(result)
  } catch (error) {
    console.log("There is an Error in updating controls actions", error)
    res.status(500).send({ done: false, error: "There is a failure in updating controls", success: ""})
  }
}

const overrides = {
  getAll: overrideGetAll,
  createOne:createControlActions,
  updateOne:updateControlActions,
}


export default generateControllers(ControlsActions, overrides)
