import mongoose from "mongoose"

const { Schema } = mongoose

export const controlsActionsSchema = new Schema({
  userId: Schema.Types.ObjectId,
  refId: String,
  id: String,
  parentId: String,
  name: String,
  topic: String,
  subTopic: String,
  otherTopic: String,
  refRule: String,
  receivedDate: Date,
  notification: {
    _id: false,
    recurringType: String,
    recurringPattern1: String,
    recurringPattern2: String,
    recurring: Boolean,
    frequency: Number,
    unit: String,
    startDate: Date,
    endDate: Date
  },
  dueDate: Date,
  relatedEntity: [{}, { strict: false, _id: false }],
  relatedActivity: [{}, { strict: false, _id: false }],
  relatedContact: [{}, { strict: false, _id: false }],
  docIds: [{ _id: false, docId: String, action: String, date: Date, by: String }],
  checklist: [{}, { strict: false, _id: false }],
  comments: String,
  tranUrl: String,
  status: String,
  affirmation: {type:Boolean,default:false},
  meta: { strict: false, _id: false },
  lastUpdated: {
    date: Date,
    by: String
  }
}, { strict: false })

controlsActionsSchema.index({userId:1, parentId:1})


export const ControlsActions = mongoose.model("controlsactions", controlsActionsSchema)
