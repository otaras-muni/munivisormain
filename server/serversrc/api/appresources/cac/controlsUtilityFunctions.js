import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"
import moment from "moment"

import {
  EntityRel,
  Controls,
  ControlsActions,
  Tasks,
  GiftsAndGratuities,
  PoliContributionDetails
} from "./../models"
import {elasticSearchNormalizationAndBulkInsert} from "./../../elasticsearch/esHelper"
import {returnActionTasksDisclosure} from "./manageControlActionsAndTasks"

const Bson_ObjectID = require("bson-objectid")

export const getControlsWithActionsPriorToCurrentDate = async(user) => {

  // Check if the user is a supervisor or not Get all the controls for the tenant
  // Get all the actions that have been generated which is less than today. Get
  // the details of people who are pending vs people who are yet to complete it.

  const currentDate = new Date()

  const controlsWithActionsPriorToCurrentDate = await Controls.aggregate([
    {
      $match: {
        entityId: ObjectID(user.tenantId)
      }
    }, 
    {
      $unwind: "$controls"
    }, 
    {
      $addFields:{
        stateOfTheControl:{$ifNull:["$controls.state", "enabled"]}
      }
    },
    {
      $match: {
        "controls.saveType": "final",
        "controls.status": "open",
        "stateOfTheControl":"enabled"
      }
    }, {
      $project: {
        controlUniqueId: "$controls._id",
        controlId: "$controls.id",
        controlDueDate: "$controls.dueDate",
        controlStatus: "$controls.status",
        controlName: "$controls.name",
        controlTopic: "$controls.topic",
        controlType: "$controls.type",
        controlActionReferenceId: "$controls.refId",
        controlNotificationSentDate: "$controls.notificationSentDate"
      }
    }, {
      $lookup: {
        from: "controlsactions",
        localField: "controlActionReferenceId",
        foreignField: "refId",
        as: "controlActions"
      }
    }, {
      $unwind: "$controlActions"
    }, {
      $project: {
        controlUniqueId: 1,
        controlId: 1,
        controlDueDate: 1,
        controlStatus: 1,
        controlName: 1,
        controlTopic: 1,
        controlActionReferenceId: 1,
        controlType: 1,
        controlNotificationSentDate: 1,
        controlActionUserId: "$controlActions.userId",
        controlActionDueDate: "$controlActions.dueDate",
        controlActionStatus: "$controlActions.status",
      }
    }, {
      $lookup: {
        from: "entityusers",
        localField: "controlActionUserId",
        foreignField: "_id",
        as: "controlActionUsers"
      }
    }, {
      $unwind: "$controlActionUsers"
    }, {
      $project: {
        controlUniqueId: 1,
        controlId: 1,
        controlDueDate: 1,
        controlStatus: 1,
        controlName: 1,
        controlTopic: 1,
        controlActionReferenceId: 1,
        controlType: 1,
        controlNotificationSentDate: 1,
        controlActionUserId: 1,
        controlActionDueDate: 1,
        controlActionStatus: 1,
        controlActionUserEmails: "$controlActionUsers.userLoginCredentials.userEmailId",
        controlActionUserName: {
          $concat: ["$controlActionUsers.userLastName", ", ", "$controlActionUsers.userFirstName"]
        }
      }
    }, {
      $group: {
        _id: {
          controlUniqueId: "$controlUniqueId",
          controlId: "$controlId",
          dueDate: "$controlActionDueDate",
          status: "$status",
          controlName: "$controlName",
          controlTopic: "$controlTopic",
          controlActionReferenceId: "$controlActionReferenceId",
          controlType: "$controlType",
          controlNotificationSentDate: "$controlNotificationSentDate"
        },
        controlActionsPending: {
          $push: {
            $cond: [
              {
                $eq: ["$controlActionStatus", "due"]
              }, {
                controlUniqueId: "$controlUniqueId",
                controlName: "$controlName",
                controlTopic: "$controlTopic",
                controlActionReferenceId: "$controlActionReferenceId",
                controlType: "$controlType",
                controlActionUserId: "$controlActionUserId",
                controlActionDueDate: "$controlActionDueDate",
                controlActionStatus: "$controlActionStatus",
                controlActionUserEmails: "$controlActionUserEmails",
                controlActionUserName: "$controlActionUserName"
              },
              null
            ]
          }
        },
        controlActionsCompleted: {
          $push: {
            $cond: [
              {
                $eq: ["$controlActionStatus", "complete"]
              }, {
                controlUniqueId: "$controlUniqueId",
                controlName: "$controlName",
                controlTopic: "$controlTopic",
                controlActionReferenceId: "$controlActionReferenceId",
                controlType: "$controlType",
                controlActionUserId: "$controlActionUserId",
                controlActionDueDate: "$controlActionDueDate",
                controlActionStatus: "$controlActionStatus",
                controlActionUserEmails: "$controlActionUserEmails",
                controlActionUserName: "$controlActionUserName"
              },
              null
            ]
          }
        }
      }
    }, {
      $project: {
        _id: 1,
        controlActionsPending: {
          $filter: {
            input: "$controlActionsPending",
            as: "pendingactions",
            cond: {
              $ne: ["$$pendingactions", null]
            }
          }
        },
        controlActionsCompleted: {
          $filter: {
            input: "$controlActionsCompleted",
            as: "completedActions",
            cond: {
              $ne: ["$$completedActions", null]
            }
          }
        }
      }
    }, {
      $addFields: {
        controlActionsPendingCount: {
          $size: "$controlActionsPending"
        },
        controlActionsCompletedCount: {
          $size: "$controlActionsCompleted"
        }
      }
    },
    {
      $sort:{
        "_id.controlName":1,
        "_id.dueDate":1
      }
    }
  ])

  return controlsWithActionsPriorToCurrentDate
}

export const updateControlActionsCreationForTenant = async(controlDetails, status) => {
  const {entityId, controlUniqueId,controlActionReferenceId} = controlDetails._id
  const {actionsTobeCreated} = controlDetails
  const notificationIds = actionsTobeCreated.map(({notificationConfigId}) => ObjectID(notificationConfigId))

  try {
    if (!isEmpty(notificationIds)) {

      /*
      const updatedInformation = await Controls.update({
        "entityId": entityId,
        "controls": {
          "$elemMatch": {
            _id: ObjectID(controlUniqueId),
            "notificationConfigDates.notificationConfigId": {
              $in: notificationIds
            }
          }
        }
      }, {
        "$set": {
          "controls.$[outer].notificationConfigDates.$[inner].actionCreated": status,
          "controls.$[outer].notificationConfigDates.$[inner].updatedAt": new Date()
        }
      }, {
        "arrayFilters": [
          {
            "outer._id": ObjectID(controlUniqueId)
          }, {
            "inner.notificationConfigId": {
              $in: notificationIds
            }
          }
        ]
      }) */

      // THIS IS THE OTHER WAY TO UPDATE A NESTED ARRAY
      const updatedInformation = await Promise.all(notificationIds.map(k => {
        console.log("I am trying to Update the Control",JSON.stringify({notificationId:k, entityId, controlUniqueId,controlActionReferenceId},null,2))
        return Controls.update(
          {
            "entityId": entityId,
            "controls": {
              "$elemMatch": {
                // _id:ObjectID(controlUniqueId),
                refId:controlActionReferenceId,
                "notificationConfigDates.notificationConfigId":ObjectID(k)
              }
            }
          },
          {
            "$set": {
              "controls.$[outer].notificationConfigDates.$[inner].actionCreated": status,
              "controls.$[outer].notificationConfigDates.$[inner].updatedAt": new Date(),
            }
          },
          {
            "arrayFilters": [
              { "outer.refId": controlActionReferenceId },
              { "inner.notificationConfigId": ObjectID(k) }
            ]
          }
        )}))

      // console.log("The updated Information is", updatedInformation)
      return {done: true, message: `Successfully Updated the Controls Driver collection for processing - ${status}`, updateIds:updatedInformation}
    }
    return {done: true, message: "Nothing to Update for the Tenant"}
  } catch (e) {
    console.log("There is an Error in updating the status of notification events", e)
    return {done: false, message: "There is an Error Updating notification status"}
  }
}

export const createActionsAndTasksForTenant = async(controlDetails, userDetailsLookupByEntity, userDetailsLookupByEmailId) => {
  const {entityId, controlUniqueId} = controlDetails._id
  const {actionsTobeCreated} = controlDetails

  let controlDetailedInfo

  const allControlInformation = await Controls.aggregate([
    {
      $match: {
        entityId
      }
    }, {
      $unwind: "$controls"
    }, {
      $match: {
        "controls._id": ObjectID(controlUniqueId)
      }
    }
  ])

  if (!isEmpty(allControlInformation)) {

    controlDetailedInfo = allControlInformation[0].controls
    const {
      id,
      name,
      topic,
      type,
      dueDate,
      docIds,
      target,
      notification,
      checklist,
      relatedEntity,
      relatedActivity,
      relatedContact,
      subTopic,
      otherTopic,
      refRule,
      notes,
      recipients,
      refId,
      createdBy
    } = controlDetailedInfo

    const data = {
      id,
      refId,
      name,
      topic,
      dueDate,
      docIds,
      notification,
      checklist,
      relatedEntity,
      relatedActivity,
      relatedContact,
      subTopic,
      otherTopic,
      refRule,
      notes
    }

    let userIds = []
    let newRecipients = []
    const user = userDetailsLookupByEmailId[createdBy.toLowerCase()]
    console.log("The user object to be passed to the function is", JSON.stringify(user, null, 2))

    if (target === "all") {
      const allFirmUserIds = userDetailsLookupByEntity[entityId.toString()]
      userIds = allFirmUserIds && allFirmUserIds.map(e => e.id)
      newRecipients = allFirmUserIds && allFirmUserIds.map(({
        id,
        firmName,
        userFirmName,
        name,
        userFirstName,
        userLastName,
        userFullName,
        userEmailId
      }) => ({
        id,
        firmName,
        userFirmName,
        name,
        userFirstName,
        userLastName,
        userFullName,
        emails: userEmailId
      }))
    } else {
      userIds = recipients && recipients.map(e => e.id)
      newRecipients = recipients
    }

    // Aggregate all the Information and then finally insert all the data into the
    // process

    const allDataToBeInserted = actionsTobeCreated.reduce((acc, {notificationConfigActionDate}) => {
      console.log("The date for which task is being configured is", moment(new Date(notificationConfigActionDate)).startOf("day").toDate())
      const constructObject = returnActionTasksDisclosure(data, controlUniqueId, refId, type, newRecipients, moment(new Date(notificationConfigActionDate)).startOf("day").toDate(), userIds, user)
      return {
        controlActions: [
          ...acc.controlActions,
          ...constructObject.controlActions
        ],
        controlTasks: [
          ...acc.controlTasks,
          ...constructObject.controlTasks
        ],
        disclosures: [
          ...acc.disclosures,
          ...constructObject.disclosures
        ]
      }
    }, {
      controlActions: [],
      controlTasks: [],
      disclosures: []
    })
    // console.log("Created all the control actions and control
    // tasks",JSON.stringify(allDataToBeInserted,null,2))

    let newInsertedControlActions = []
    let newInsertedTasks = []
    let bulkUpdateEs
    if (allDataToBeInserted.controlTasks.length && allDataToBeInserted.controlActions.length) {
      newInsertedControlActions = await ControlsActions.insertMany(allDataToBeInserted.controlActions)
      newInsertedTasks = await Tasks.insertMany(allDataToBeInserted.controlTasks)
      const dataForEs = newInsertedTasks.map(d => d._id)
      bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: entityId})
      if (allDataToBeInserted.disclosures && allDataToBeInserted.disclosures.length) {
        const model = topic === "Gifts and Gratuities"
          ? GiftsAndGratuities
          : PoliContributionDetails
        await model.insertMany(allDataToBeInserted.disclosures)
      }
    }
    return {done: true, message: "All data was inserted properly", allDataToBeInserted, bulkUpdateEs}

  }
  return {done: true, message: "There is no control Information for the given tenant with the specified ID"}
}

export const getGetEligibleActionsTasksAndCreateForallTenants = async() => {

  // Get the control that is currently active
  let userDetailsLookup = []
  let userDetailsLookupByEntity = {}
  let userDetailsLookupByEmailId
  try {

    const getTenantUsers = await EntityRel.aggregate([
      {
        $match: {
          relationshipType: "Self"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "entityParty2",
          foreignField: "entityId",
          as: "userdetails"
        }
      }, {
        $unwind: "$userdetails"
      }, {
        $project: {
          tenantId: "$userdetails.entityId",
          entityId: "$userdetails.entityId",
          userId: "$userdetails._id",
          entityName: "$userdetails.userFirmName",
          userFirmName: "$userdetails.userFirmName",
          userFirstName: "$userdetails.userFirstName",
          userLastName: "$userdetails.userLastName",
          userFullName: {
            "$concat": ["$userdetails.userLastName", ",", "$userdetails.userFirstName"]
          },
          userEmailId: {
            $toLower: "$userdetails.userLoginCredentials.userEmailId"
          },
          isSuperVisor: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $or: [
                      {
                        $in: ["Compliance Officer", "$userdetails.userFlags"]
                      }, {
                        $in: ["Supervisory Principal", "$userdetails.userFlags"]
                      }
                    ]
                  },
                  "then": true
                }
              ],
              "default": false
            }
          }

        }
      }, {
        $addFields: {
          firmName: "$userFirmName",
          name: "$userFullName",
          id: "$userId"
        }
      }
    ])

    userDetailsLookup = getTenantUsers.reduce((acc, userDetails) => ({
      ...acc,
      [userDetails.userId]: {
        ...userDetails
      }
    }), {})
    userDetailsLookupByEmailId = getTenantUsers.reduce((acc, userDetails) => ({
      ...acc,
      [userDetails.userEmailId]: {
        ...userDetails
      }
    }), {})
    userDetailsLookupByEntity = getTenantUsers.reduce((acc, userDetails) => {
      if (!acc[userDetails.tenantId]) {
        return {
          ...acc,
          [userDetails.tenantId]: [userDetails]
        }
      }
      return {
        ...acc,
        [userDetails.tenantId]: [
          ...acc[userDetails.tenantId],
          userDetails
        ]
      }
    }, {})

  } catch (e) {
    console.log("There is an error in getting user Information")
  }

  const controlsThatRequireNewProcessing = await Controls.aggregate([
    // {   $match:{     entityId:ObjectID("5c9ff71ad7eaac1a4ce73150")   } },
    {
      $unwind: "$controls"
    }, {
      $match: {
        "controls.saveType": "final"
      }
    }, {
      $project: {
        entityId: "$entityId",
        controlUniqueId: "$controls._id",
        controlName: "$controls.name",
        controlTopic: "$controls.topic",
        controlType: "$controls.type",
        controlTarget: "$controls.target",
        controlRecipients: {
          "$ifNull": ["$controls.recipients.id", []
          ]
        },
        controlActionReferenceId: "$controls.refId",
        controlNotificationSentDate: "$controls.notificationSentDate",
        controlNotificationUnit: "$controls.notification.unit",
        controlNotificationFrequency: "$controls.notification.frequency",
        controlNotificationConfigDates: "$controls.notificationConfigDates"
      }
    }, {
      $addFields: {
        referenceDate: {
          $switch: {
            branches: [
              {
                case: {
                  $eq: ["$controlNotificationUnit", "day"]
                },
                then: {
                  $add: [
                    moment().utc().startOf("day").unix(), {
                      $multiply: ["$controlNotificationFrequency", 86400]
                    }
                  ]
                }
              }, {
                case: {
                  $in: [
                    "$controlNotificationUnit",
                    [
                      "week",
                      "weeks",
                      "w",
                      "Weeks",
                      "Week",
                      "WEEK",
                      "WEEKS"
                    ]
                  ]
                },
                then: {
                  $add: [
                    moment().utc().startOf("day").unix(), {
                      $multiply: ["$controlNotificationFrequency", 7, 86400]
                    }
                  ]
                }
              }, {
                case: {
                  $in: [
                    "$controlNotificationUnit",
                    [
                      "month",
                      "months",
                      "mon",
                      "Month",
                      "Months",
                      "MONTH",
                      "MONTHS"
                    ]
                  ]
                },
                then: {
                  $add: [
                    moment().utc().startOf("day").unix(), {
                      $multiply: ["$controlNotificationFrequency", 30, 86400]
                    }
                  ]
                }
              }, {
                case: {
                  $in: [
                    "$controlNotificationUnit",
                    ["year", "years", "y"]
                  ]
                },
                then: {
                  $add: [
                    moment().utc().startOf("day").unix(), {
                      $multiply: ["$controlNotificationFrequency", 365, 86400]
                    }
                  ]
                }
              }
            ],
            default: moment().utc().startOf("day").unix()
          }
        }
      }
    }, {
      $unwind: "$controlNotificationConfigDates"
    }, {
      $match: {
        "controlNotificationConfigDates.actionCreated": "pending"
      }
    }, {
      $addFields: {
        notificationConfigDate: "$controlNotificationConfigDates.actionDate",
        notificationConfigId: "$controlNotificationConfigDates.notificationConfigId",
        notificationConfigActionDate: "$controlNotificationConfigDates.actionDate",
        dateCompare1:{$cmp: ["$referenceDate","$controlNotificationConfigDates.actionDateTimestamp"]},
        dateCompare2:{$cmp: [{$add: ["$referenceDate", 86400]},"$controlNotificationConfigDates.actionDateTimestamp"]},
      }
    }, 
    {
      $addFields:{
        shouldBeScheduled: {
          $or: [
            {$eq:["$dateCompare1",1]},
            {$eq:["$dateCompare2",1]},
          ]
        }
      }
    },
    {
      $match: {
        shouldBeScheduled: true
      }
    }, {
      $group: {
        _id: {
          entityId: "$entityId",
          controlUniqueId: "$controlUniqueId",
          controlName: "$controlName",
          controlTopic: "$controlTopic",
          controlType: "$controlType",
          controlTarget: "$controlTarget",
          controlRecipients: "$controlRecipients",
          controlActionReferenceId: "$controlActionReferenceId"
        },
        actionsTobeCreated: {
          $push: {
            notificationConfigDate: "$notificationConfigDate",
            notificationConfigId: "$notificationConfigId",
            notificationConfigActionDate: "$notificationConfigActionDate"
          }
        }
      }
    }
  ])

  // Update the status to Pending so that other processors don't pick this up
  const returnPostUpdateStatusPre = await Promise.all(controlsThatRequireNewProcessing.map(k => updateControlActionsCreationForTenant(k, "processing")))
  // console.log("The revised Status is",
  // JSON.stringify(returnPostUpdateStatusPre,null,2)) Create tasks and control
  // actions

  const returnTaskCreationProcessForAllTenants = await Promise.all(controlsThatRequireNewProcessing.map(k => createActionsAndTasksForTenant(k, userDetailsLookupByEntity, userDetailsLookupByEmailId)))
  // console.log("The revised Status is",
  // JSON.stringify(returnTaskCreationProcessForAllTenants,null,2)) Update the
  // status after sending the information and notification
  const returnPostUpdateStatusPost = await Promise.all(controlsThatRequireNewProcessing.map(k => updateControlActionsCreationForTenant(k, "created")))
  // console.log("The revised Status is",
  // JSON.stringify(returnPostUpdateStatusPost,null,2))

  return {controlsThatRequireNewProcessing, returnPostUpdateStatusPre, returnTaskCreationProcessForAllTenants, returnPostUpdateStatusPost}
}
