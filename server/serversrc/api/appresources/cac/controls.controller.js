import { generateControllers } from "../../modules/query"
import { Controls } from "./controls.model"

export const overrideGetAll = async (req, res, next) => {
  const { user } = req
  const { entityId } = user || {}
  if(!user || !entityId) {
    return res.status(422).send({ error: "No user or entityId found" })
  }
  const filter = { entityId }

  try {
    const docs = await Controls.find(filter)
    res.json(docs[0])    
  } catch (error) {
    next(error)
  }
}

const createControls = async (req, res) => {
  console.log("Entered Creating the Control - New Routine")
  try {
    const newControl = new Controls({
      ...req.body
    })
    const insertedControl = await newControl.save()
    res.json(insertedControl)
  } catch (error) {
    console.log("There is an Error in creating controls", error)
    res.status(500).send({ done: false, error: "Error saving the controls" })
  }
}

const updateControls = async (req, res) => {
  console.log("Entered Updating the Control - New Routine")
  const {__v, _id, ...restOfControl} = req.body
  try {
    const result = await Controls.findOneAndUpdate({_id}, restOfControl, {
      new: true,
      upsert: true
    })
    res.json(result)
  } catch (error) {
    console.log("There is an Error in updating controls", error)
    res.status(500).send({ done: false, error: "There is a failure in updating controls", success: ""})
  }
}

const overrides = {
  getAll: overrideGetAll,
  createOne:createControls,
  updateOne:updateControls
}

export default generateControllers(Controls, overrides)
