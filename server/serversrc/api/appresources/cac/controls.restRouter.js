import express from "express"
import moment from "moment"
import controlsController from "./controls.controller"
import { Controls } from "./controls.model"
import { Entity } from "../entity/entity.model"
import { EntityUser } from "../entityUser/entityUser.model"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import { getAllTransactionDetailsForLoggedInUser } from "../../commonDbFunctions/getAllDetailedTransactionsForLoggedInUser"
import { getAllUsersForFirm } from "../../commonDbFunctions/getAllUsersForFirm"
import {ControlsActions} from "./controls_actions.model"
import {getAllEligibleControlActionDates} from "./manageControlActionsAndTasks"
import {getControlsWithActionsPriorToCurrentDate} from "./controlsUtilityFunctions"
import {GiftsAndGratuities, Tasks} from "../models"
import {PoliContributionDetails} from "../compliance/PoliticalContributions/poliContributionDetails.model"
import {elasticSearchNormalizationAndBulkInsert} from "./../../elasticsearch/esHelper"


const {ObjectID} = require("mongodb")
const BSON_ObjectId = require("bson-objectid")

export const controlsRouter = express.Router()

export const taskStatusChange = async (existControl, controls, tenantId) => {

  const controlId = existControl.controls[0]._id

  const gift = (controls && controls.topic === "Gifts and Gratuities")
  const political = (controls && controls.topic === "Political Contributions and Prohibitions on Municipal Advisory Business")

  const politicalGiftModal = gift ? GiftsAndGratuities : political ? PoliContributionDetails : ""

  const controlActions = await ControlsActions.find({parentId: controlId })

  const tasks = await Tasks.find({"taskDetails.parentId": ObjectID(controlId) } )

  let politicalGift = []
  if(gift || political){
    politicalGift = await politicalGiftModal.find({controlId }) || []
  }

  if(controlActions && controlActions.length){

    for (const i in controlActions) {
      if(controlActions && controlActions[i] && controlActions[i].status && controlActions[i].status !== "complete"){
        await ControlsActions.updateOne({_id: ObjectID(controlActions[i]._id) }, {$set: { status: "Dropped" }})
      }
    }

  }

  if(tasks && tasks.length){
    const dropIds = []
    for (const i in tasks) {
      const array = ["complete", "Closed"]
      if(array.indexOf(tasks && tasks[i] && tasks[i].taskDetails && tasks[i].taskDetails.taskStatus) === -1){
        dropIds.push(tasks[i]._id)
        await Tasks.updateOne({_id: ObjectID(tasks[i]._id) },  { $set: { "taskDetails.taskStatus": "Dropped", status: "Dropped" } } )
      }
    }
    const parameters = {
      ids: dropIds,
      esType: "mvtasks",
      tenantId
    }
    if(dropIds && dropIds.length){
      await elasticSearchNormalizationAndBulkInsert(parameters)
    }
  }

  if(politicalGift && politicalGift.length){

    for (const i in politicalGift) {
      if((politicalGift && politicalGift[i] && politicalGift[i].status !== "Complete")){
        await politicalGiftModal.updateOne({_id: ObjectID(politicalGift[i]._id) },  { $set: { status: "Dropped" } } )
      }
    }

  }

}


controlsRouter.get("/related-info", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  try {
    const result = await getAllTransactionDetailsForLoggedInUser(user)
    return res.json(result)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting related info" })
  }
})

controlsRouter.get("/users", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  try {
    const result = await getAllUsersForFirm(ObjectID(user.entityId))
    return res.json(result.map(e =>({
      id: e._id,
      firmName:e.userFirmName,
      name: `${e.userFirstName} ${e.userMiddleName} ${e.userLastName}`,
      emails: e.userEmails
    })))
  } catch (err) {
    return res.status(422).send({ error: "Error in getting users info" })
  }
})

controlsRouter.get("/check-supervisor", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  const entity = await Entity.findOne({ _id: ObjectID(user.entityId) }).select({ _id: 1 })

  if(!entity) {
    return res.status(422).send({ error: "No entity found for the user" })
  }

  const edits = ["global-edit", "tran-edit", "global-readonly-tran-edit"]

  const result = {
    supervisor: false,
    edit: false,
    isMuniVisorClient: !!entity.isMuniVisorClient,
    firmHasSupervisor: false
  }

  // users.some(e => {
  //   if(e.userFlags && (e.userFlags.includes("Supervisory Principal") ||
  //     e.userFlags.includes("Compliance Office"))) {
  //     result.firmHasSupervisor = true
  //     return true
  //   }
  // })

  if(user.userFlags && (user.userFlags.includes("Supervisory Principal") ||
    user.userFlags.includes("Compliance Officer"))) {
    result.supervisor = true
    result.firmHasSupervisor = true
    result.supervisorUserId = user._id
    result.supervisorUserName = `${user.userFirstName}  ${user.userLastName}`
  } else {
    const supervisorUser = await EntityUser.findOne({
      entityId:  ObjectID(entity._id),
      userFlags: {
        $elemMatch:{
          $in: ["Supervisory Principal", "Compliance Officer"],
          $exists:true
        }
      }
    }).select({ _id: 1, userFirstName: 1, userLastName: 1 })

    // console.log("supervisorUser : ", supervisorUser)

    if(supervisorUser) {
      result.firmHasSupervisor = true
      result.supervisorUserId = supervisorUser._id
      result.supervisorUserName = `${supervisorUser.userFirstName}  ${supervisorUser.userLastName}`
    }
  }

  if(edits.includes(user.role) || edits.includes(user.userEntitlement)) {
    result.edit = true
  }

  res.json(result)
})

controlsRouter.post("/action-status", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  const { parentId, status, period } = req.body

  if(!parentId || !status) {
    return res.status(422).send({ error: "Input missing" })
  }


  try {
    const controls = await Controls.find({ "controls.refId": parentId })
    if(controls && controls[0] && controls[0].controls) {
      // console.log("ctr1 : ", controls[0].controls[0])
      // console.log("ctr2 : ", controls[0].controls[0].refId)
      // console.log("parentId1 : ", parentId)
      // console.log("parentId2 : ", parentId === controls[0].controls[0].refId)
      // const control = controls[0].controls.filter(e => e.refId.valueOf() === parentId)[0]
      const idx = controls[0].controls.findIndex(e => e.refId.valueOf() === parentId)
      if(idx > -1) {
        const control = controls[0].controls[idx]
        // console.log("userId: ", user._id)
        // console.log("controls : ", controls)
        // console.log("control : ", control)
        if(control && control.recipients && control.recipients.length) {
          // console.log("userIds: ", control.recipients.map(e => e.id))
          // console.log("included : ", control.recipients.map(e => e.id).includes(user._id.toHexString()))
          const userIdx = control.recipients.findIndex(e => e.id.toString() === user._id.toString())
          // if(control.recipients.map(e => e.id).includes(user._id.toHexString())) {
          if(userIdx > -1) {
            // const updatedControl = { ...controls[0] }
            const userName = `${user.userFirstName} ${user.userLastName}`
            let actionCompletedBy = []
            if(control.actionCompletedBy && Array.isArray(control.actionCompletedBy)) {
              actionCompletedBy = [ ...control.actionCompletedBy ]
            }
            const statusIdx = actionCompletedBy.find(e => (e &&
              e.userId.toString() === user._id.toString() && e.period === period))
            if(statusIdx > -1) {
              actionCompletedBy[idx] = { ...actionCompletedBy[idx] }
              actionCompletedBy[idx].status = status
            } else {
              actionCompletedBy.push({
                userId: user._id,
                userName,
                status
              })
            }
            // const actionCompletedBy = control.recipients.filter(e =>
            //   (e.id !== user._id.toHexString()
            //     && (e.actionStatus === "complete" || e.actionStatus === "Complete"))).length + 1
            const key1 = `controls.${idx}.actionCompletedBy`
            const key2 = `controls.${idx}.recipients.${userIdx}.actionStatus`
            // console.log("actionCompletedBy : ", actionCompletedBy)
            // updatedControl.controls = [ ...controls[0].controls ]
            // updatedControl.controls[idx] = { ...control, actionCompletedBy }
            const res1 = await Controls.findByIdAndUpdate(
              controls[0]._id,
              { $set: { [key1]: actionCompletedBy, [key2]: status } }
            )
            // console.log("res : ", res1)
          }
        }
      }
    }
    return res.json({ message: "OK" })
  } catch (err) {
    console.log("Error in getting parent control : ", err)
    return res.status(422).send({ error: "Error in getting parent control : " })
  }
})

controlsRouter.post("/saveControl", requireAuth, async (req, res) => {
  const ids = req.query.id
  const {type, flag} = req.query
  const {controls, actionTaskDescription} = req.body
  console.log("----------------action description", actionTaskDescription)
  try {
    if(flag === "isNoActiveContract"){
      console.log({flag})
      const entControls = await Controls.findOne({entityId: ObjectID(ids)})
      let control = null
      if (entControls && entControls.controls && entControls.controls.length) {
        control = entControls.controls.find(ctrl => ctrl.isNoActiveContract)
        if(!control){
          await Controls.updateOne({entityId: ObjectID(ids)}, {
            $addToSet: { controls }
          })
          control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { isNoActiveContract: true }}} )
          control = control.controls[0]
        }
      } else {
        console.log("============================controls==================>", controls)
        control = new Controls({
          entityId: ObjectID(ids),
          controls: [controls]
        })
        control = await control.save()
        control = control.controls[0]
        console.log("==========================================================>", control)
      }
      if (control) {
        controls.refId = control.refId
        await getAllEligibleControlActionDates(controls, ids, control._id, req.user,actionTaskDescription)
      }
      return res.json(control)
    }
    if (ids) {
      const controlDetails = await Controls.findOne({ entityId: ObjectID(ids)}).select("entityId")
      controls && controls.recipients && controls.recipients.forEach(e => { e.id = ObjectID(e.id) })
      if (controlDetails) {
        const existControl = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
        if (existControl && existControl.controls && existControl.controls.length) {
          await Controls.updateOne({entityId: ObjectID(ids), "controls.refId": controls.refId}, {
            $set: { "controls.$": {...controls,_id:existControl.controls[0]._id} }
          })
          const control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
          if (type === "final" && control && control.controls && control.controls.length) {
            await getAllEligibleControlActionDates(controls, ids, control.controls[0]._id, req.user)
          }
          res.json(control)
        } else {
          await Controls.updateOne({entityId: ObjectID(ids)}, {
            $addToSet: { controls: [{
              ...controls,
              _id: BSON_ObjectId()
            }]
            }
          })
          const control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
          if (type === "final" && control && control.controls && control.controls.length) {
            await getAllEligibleControlActionDates(controls, ids, control.controls[0]._id, req.user)
          }
          res.json(control)
        }
      } else {
        controls._id = BSON_ObjectId()
        const newControls = new Controls({
          entityId: ObjectID(ids),
          controls: [controls]
        })
        await newControls.save()
        const control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
        if (type === "final" && control && control.controls && control.controls.length) {
          await getAllEligibleControlActionDates(controls, ids, control.controls[0]._id, req.user)
        }
        res.json(control)
      }
    }
  } catch (err) {
    console.log("=================>", err)
    return res.status(422).send({ error: "Error in getting Controls" })
  }
})

controlsRouter.post("/saveDisableControls", requireAuth, async (req, res) => {
  const ids = req.query.id
  const {type} = req.query
  const {controls} = req.body
  try {
    if (ids) {
      const controlDetails = await Controls.findOne({ entityId: ObjectID(ids)}).select("entityId")
      controls && controls.recipients && controls.recipients.forEach(e => { e.id = ObjectID(e.id) })
      if (controlDetails) {
        const existControl = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
        if (existControl && existControl.controls && existControl.controls.length) {
          await Controls.updateOne({entityId: ObjectID(ids), "controls.refId": controls.refId}, {
            $set: { "controls.$": {...controls,_id:existControl.controls[0]._id} }
          })
          const control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
          if (type === "final" && control && control.controls && control.controls.length) {
            taskStatusChange(existControl, controls, req.user.tenantId)
          }
          res.json(control)
        } else {
          await Controls.updateOne({entityId: ObjectID(ids)}, {
            $addToSet: { controls: [{
              ...controls,
              _id: BSON_ObjectId()
            }]
            }
          })
          const control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
          if (type === "final" && control && control.controls && control.controls.length) {
            taskStatusChange(control, controls, req.user.tenantId)
          }
          res.json(control)
        }
      } else {
        controls._id = BSON_ObjectId()
        const newControls = new Controls({
          entityId: ObjectID(ids),
          controls: [controls]
        })
        await newControls.save()
        const control = await Controls.findOne({entityId: ObjectID(ids)}, {controls: { $elemMatch: { refId: controls.refId }}} )
        if (type === "final" && control && control.controls && control.controls.length) {
          taskStatusChange(control, controls, req.user.tenantId)
        }
        res.json(control)
      }
    }
  } catch (err) {
    return res.status(422).send({ error: "Error in getting Controls" })
  }
})

controlsRouter.get("/getControls", requireAuth, async (req, res) => {
  try {
    const select = "controls.id controls._id controls.name controls.createdBy controls.topic controls.notes controls.lastUpdated controls.status"
    const control = await Controls.find({ entityId: ObjectID(req.user.tenantId)}).select(select)

    const control1 = await Controls.aggregate([
      {
        $match: {
          entityId: ObjectID(req.user.tenantId)
        }
      },
      {
        $unwind: "$controls"
      },
      {
        $addFields:{
          stateOfTheControl:{$ifNull:["$controls.state", "enabled"]}
        }
      },
      {
        $match: {
          "controls.saveType": { $in: ["final", "draft"] },
          "controls.status": "open",
          "stateOfTheControl":"enabled"
        }
      },
      {
        $group:{
          _id:"$_id",
          controls:{"$push": {
            id:"$controls.id",
            _id:"$controls._id",
            name:"$controls.name",
            createdBy:"$controls.createdBy",
            topic:"$controls.topic",
            notes:"$controls.notes",
            lastUpdated:"$controls.lastUpdated",
            status:"$controls.status"
          }}
        }
      }
      // {
      //   $project: {
      //     _id:1,
      //     "controls":{
      //       id:"$controls.id",
      //       _id:"$controls._id",
      //       name:"$controls.name",
      //       createdBy:"$controls.createdBy",
      //       topic:"$controls.topic",
      //       notes:"$controls.notes",
      //       lastUpdated:"$controls.lastUpdated",
      //       status:"$controls.status"
      //     }
      //   }
    ])

    res.json(control1)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting Controls" })
  }
})

controlsRouter.get("/getSpecificControl", requireAuth, async (req, res) => {
  const { id } = req.query
  try {
    const control = await Controls.findOne({entityId: ObjectID(req.user.entityId)}, {controls: { $elemMatch: { id }}} )
    res.json(control)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting Controls" })
  }
})

controlsRouter.get("/fetchControlsMonitor", requireAuth, async (req, res) => {
  try {
    const control = await getControlsWithActionsPriorToCurrentDate(req.user)
    /* let control = await Controls.aggregate([
      { $match: { entityId: ObjectID(req.user.entityId) } },
      { $unwind: "$controls" },
      { $match: { "controls.saveType": "final" } },
      {
        $project: {
          controls: 1
        }
      }
    ])
    control = control.map(item => item.controls)
    // console.log("===============control===============>", control)
    for (const i in control) {
      const actionCompleted  = []
      const actionRemain = []
      let actions = []
      const ctrl = control[i]
      if(ctrl && typeof ctrl === "object" && Object.keys(ctrl).length){
        // console.log("======ControlsActions========>", JSON.stringify(ctrl))
        if(ctrl.refId){
          actions = await ControlsActions.find({parentId: ctrl._id }).select("userId dueDate notification status topic subTopic")
        }
        // console.log("======actions========>", JSON.stringify(actions))
        actions.forEach((item, i) => {
          const notification = (item && item.notification) || {}
          const matchUser = ctrl && ctrl.recipients.filter(data => data.id.toString() === item.userId.toString())
          // console.log("================matchUser===============>", matchUser)
          const data = {
            user: matchUser && matchUser[0] && matchUser[0].name || "",
            topic: item && item.topic,
            subTopic: item && item.subTopic,
            dueDate: item && item.dueDate
          }
          const endOfType = notification && notification.recurringType === "quarterly" ? "quarter"
            : notification && notification.recurringType === "weekly" ? "isoWeek"
              : notification && notification.unit
          const endDate = notification.recurringType === "weekly" ? moment(item && item.dueDate).endOf(endOfType).day(`${notification.recurringPattern2}`).subtract(2, "days").format("MM-DD-YYYY")
            : moment(item && item.dueDate).endOf(endOfType).format("MM-DD-YYYY")
          const dueDate = moment(item && item.dueDate).format("MM-DD-YYYY")
          console.log("==================endDate=============>", endDate)
          console.log("===============dueDate================>", dueDate)
          if (item.status === "complete") {
            actionCompleted.push(data)
          } else if (dueDate <= endDate) {
            actionRemain.push(data)
          } else {
            actionCompleted.push(data)
          }
        })
        control[i].numActions = actionRemain
        control[i].actionCompletedBy = actionCompleted
      }
    } */
    res.json(control)
  } catch (err) {
    console.log("=============>", err)
    return res.status(422).send({ error: "Error in getting Controls" })
  }
})

controlsRouter.post("/updateMonitor", requireAuth, async (req, res) => {
  const {id, type} = req.query
  const {entityId, value} = req.body
  try {
    if (id) {
      if (type === "status") {
        await Controls.updateOne({entityId: ObjectID(entityId), "controls._id": id}, {
          $set: { "controls.$.status": value }
        })
        return res.status(200).send({ done: true, message: "Updated Successfully"})
      } else if (type === "dueDate")
        await Controls.updateOne({entityId: ObjectID(entityId), "controls._id": id}, {
          $set: { "controls.$.dueDate": value }
        })
      return res.status(200).send({ done: true, message: "Updated Successfully"})
    }
  } catch (err) {
    return res.status(422).send({ done: false, message: "Error in getting Controls" })
  }
})


controlsRouter.param("id", controlsController.findByParam)

controlsRouter.route("/")
  .get(requireAuth, controlsController.getAll)
  .post(controlsController.createOne)

controlsRouter.route("/:id")
  .get(controlsController.getOne)
  .put(controlsController.updateOne)
  .delete(controlsController.createOne)
