import mongoose from "mongoose"

const { Schema } = mongoose

export const controlItemsSchema = new Schema({
  _id: false,
  refId: String,
  type: String,
  id: String,
  name: String,
  topic: String,
  target: String,
  subTopic: String,
  otherTopic: String,
  refRule: String,
  notificationSentDate: Date,
  actionCompletedBy: [{}],
  numActions: [{}],
  notificationConfigDates: [],
  dueDate: Date,
  notes: String,
  notification: {
    _id: false,
    recurringType: String,
    recurringPattern1: String,
    recurringPattern2: String,
	  recurring: Boolean,
    frequency: Number,
    unit: String,
    startDate: Date,
    endDate: Date
  },
  recipients: [{}, { strict: false, _id: false }],
  toList: [{}, { strict: false, _id: false }],
  ccList: [{}, { strict: false, _id: false }],
  relatedActivity: [{}, { strict: false, _id: false }],
  relatedEntity: [{}, { strict: false, _id: false }],
  relatedContact: [{}, { strict: false, _id: false }],
  docIds: [{_id: String, name: String }],
  checklist: [{}, { strict: false, _id: false }],
  meta: { strict: false, _id: false },
  checklistApplicable: Boolean,
  isNoActiveContract: Boolean,
  lastUpdated: {
    _id: false,
    date: Date,
    by: String
  },
  status: String,
  state: String,
  saveType: String,
  createdBy: String
})

export const controlsSchema = new Schema({
  entityId: Schema.Types.ObjectId,
  controls: [controlItemsSchema]
})

controlsSchema.index({entityId:1})

export const Controls = mongoose.model("controls", controlsSchema)
