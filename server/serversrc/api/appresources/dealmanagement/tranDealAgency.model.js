import mongoose, {Schema} from "mongoose"

const arrayNotEmpty = (values) => !(values.length > 0)
const startDateLaterThanHired = (startDate) => startDate >= this.tranDateHired

/* Validations

Is the issuer entity ID playing the role of an issuer
 Transaction Name must be atleast 10 characters,
 Assigned to we need to ensure that they belong to the users from the issuer or the financial advisor
*/

const tranCommonSchema = new mongoose.Schema({
  tranIssuerId: {
    type: Schema.Types.ObjectId,
    ref: "Entity",
    required: [true, "Select an issuer for creating a transaction"]
  },
  tranName: {
    type: String,
    uppercase: true,
    required: [true, "Transaction Name is required"]
  },
  tranState: {
    type: String,
    required: [true, "State is required"]
  },
  tranCounty: {
    type: String
  },
  tranPrimarySectory: {
    type: String,
    required: [true, "Primary Sector is required"]
  },
  tranSecondarySectory: {
    type: String,
    required: [true, "Secondary Sector is required"]
  },
  tranType: {
    type: String,
    required: [true, "Select a transaction Type"]
  },
  tranPurposeOfRequest: {
    type: String,
    required: [true, "Purpose of Request is required"]
  },
  tranAssignedTo: [
    {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
      validate: [arrayNotEmpty, "Please select user(s) from issuer / financial advisor"] // this adds custom validation through the function check of notEmpty.
    }
  ],
  tranDateHired: {
    type: Date,
    required: true
  },
  tranDateStartDate: {
    type: Date,
    required: true,
    validate:[startDateLaterThanHired,"Start Date needs to be greater than Hire Date" ]
  },
  tranDateExpEndDate: {
    type: Date,
    required: true
  },
  tranNotes: String
})

const tranDealAgencySchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Transaction Must Have a Description"]
  }
})

export const TranGlobal = mongoose.model("tranglobal", tranCommonSchema)
export const TranAgencyDeal = mongoose.model("tranagencydeal", tranDealAgencySchema)