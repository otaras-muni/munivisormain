import express from "express"
import dealController from "./deal.controller"

export const dealManagementRouter = express.Router()

dealManagementRouter.param("id", dealController.findByParam)

dealManagementRouter.route("/")
  .get(dealController.getAll)
  .post(dealController.createOne)

dealManagementRouter.route("/:id")
  .get(dealController.getOne)
  .put(dealController.updateOne)
  .delete(dealController.createOne)
