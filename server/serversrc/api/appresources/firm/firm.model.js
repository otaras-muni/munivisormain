import mongoose from "mongoose"

import mongooseHidden from "mongoose-hidden"

const mongooseHiddenApp = mongooseHidden()
const { Schema } = mongoose

export const entityAddressSchema = Schema({
  addressName: String,
  isPrimary: Boolean,
  isHeadQuarter: Boolean,
  isActive: Boolean,
  website: String,
  officePhone: [
    { countryCode: String, phoneNumber: String, extension: String }
  ],
  officeFax: [
    { faxNumber: String }
  ],
  officeEmails: [{ emailId: String }],
  addressLine1: String,
  addressLine2: String,
  country: String,
  state: String,
  city: String,
  zipCode: { zip1: String, zip2: String }
})

export const firmAddOnsSchema = Schema({
  serviceName: String,
  serviceEnabled: Boolean
})

const entityFlagSchema = Schema({
  marketRole: [String],
  issuerFlags: [String]
})

export const entityLinkCusipSchema = Schema({
  debtType: String,
  associatedCusip6: String,
})

export const entityLinkBorrowersObligorsSchema = Schema({
  borOblRel: String,
  borOblFirmName: String,
  borOblDebtType: String,
  borOblCusip6: String,
  borOblStartDate: Date,
  borOblEndDate: Date
})

// eslint-disable-next-line no-unused-vars
export const entitySchema = Schema({
  entityFlags: entityFlagSchema,
  isMuniVisorClient: Boolean,
  msrbFirmName: String,
  msrbRegistrantType: String,
  msrbId: String,
  entityAliases: [String],
  firmName: String,
  taxId: String,
  businessStructure: String,
  numEmployees: String,
  annualRevenue: String,
  addresses: [entityAddressSchema],
  firmAddOns: [firmAddOnsSchema],
  entityLinkedCusips: [entityLinkCusipSchema],
  entityBorObl: [entityLinkBorrowersObligorsSchema]
},
{ versionKey: false })

// entitySchema.plugin(mongooseHiddenApp)
// eslint-disable-next-line no-unused-vars

export const Firm = mongoose.model("firm", entitySchema)  
