import express from "express"
import firmController from "./firm.controller"

export const firmRouter = express.Router()

firmRouter.route("/firms")
  .get(firmController.getAllFirmList)
  .post(firmController.saveFirmDetail)


firmRouter.route("/firms/:id")
  .get(firmController.getFirmsDetailById)
  .put(firmController.updateFirmDetailById)

firmRouter.route("/third-parties/:id")
  .get(firmController.getThirdPartyFirmsDetailById)
  .put(firmController.updateThirdPartyFirmDetailById)

firmRouter.route("/third-parties/entityId/:entityId")
  .get(firmController.getAllThirdPartyFirmList)
  .post(firmController.saveThirdPartyFirmDetail)

firmRouter.route("/clients/:id")
  .get(firmController.getClientsFirmsDetailById)
  .put(firmController.updateClientsFirmDetailById)

firmRouter.route("/clients/entityId/:entityId")
  .get(firmController.getAllClientsFirmList)
  .post(firmController.saveClientsFirmDetail)  
 
firmRouter.route("/searchfirm/:firmName")
  .get(firmController.searchFirm)  

/* 
firmRouter.route("/third-parties/:id")
  .get(firmController.getFirmsDetailById)
  .put(firmController.updateFirmDetailById)  
 *//* firmRouter.route('/searchfirm/:firmName')
 .get(firmController.searchFirm)
firmRouter.route('/firms/:userType/:entityId')
  .get(firmController.getAllFirmList)
  .post(firmController.getAllFirmList) */