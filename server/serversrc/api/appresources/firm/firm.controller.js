import {
  generateControllers
} from "../../modules/query"
import {
  Firm
} from "./firm.model"
import {
  EntityRel
} from "../entityRel/entityRes.model"
import {
  EntityUser
} from "../entityUser/entityUser.model"

import {
  validateFirmDetail
} from "../../Validations/firmDetail"

const {ObjectID} = require("mongodb")

const getFirmDetailById = ()=>(req, res, next) => {
  const {
    id
  } = req.params
  return Firm.findById(id)
}

const updateFirmDetailsById = () => async (req, res, next) => {
  try {
    const {
      firmDetails
    } = req.body
    const docToUpdate = req.docFromId
    const {
      _id
    } = docToUpdate
    const errorData = validateFirmDetail(firmDetails)
    if (errorData.error) {     
      res.json(errorData)
    } else {
      const getAllFirmDetails = await getFirmDetailById(req, res)
      if (getAllFirmDetails) {
        const sortedData = firmDetails.addresses.sort((a, b) => a._id > b._id)
        getAllFirmDetails.addresses = getAllFirmDetails.addresses.filter((obj) => obj._id != sortedData[0]._id)
        firmDetails.addresses = [...getAllFirmDetails.addresses, ...
        sortedData
        ]
        const result = await Firm.findByIdAndUpdate(_id, firmDetails, {
          new: true,
          upsert: true
        })
        res.json(result)
      } else {
        const result = await Firm.findByIdAndUpdate(_id, firmDetails, {
          new: true,
          upsert: true
        })
        res.status(201).json(result)
      }
    }
  } catch (ex) {
    console.log("Ex===>>>", ex)
  }
}



/* const saveFirmDetail = () => async (req, res, next) => {
  try {
    const {firmDetails} = req.body
    const errorData = validateFirmDetail(req.body.firmDetails)
    if (errorData.error) {
      throw new Error(errorData.error)
    } else {
      const entity = await Firm.create(firmDetails)
      if(req.body.entityId){
        EntityRel.insert({
          "entityParty1": ObjectId("5b1a63bd471a1919e0ca97d4"),
          "entityParty2": ObjectId(entity._id),
          "relationshipType": req.boody.relType,
        }, (err, result) => {
          if (err)
            throw new Error(err)
          res.json(entity)
        })
      } 
      res.json(entity)       
    }
  } catch (ex) {
    console.log("Ex===>>>>", ex)
  }
} */

const searchFirm = () => async (req, res, next) => {
  const {
    firmName
  } = req.params
  try {
    const result = await Firm.find({
      firmName: {
        $regex: firmName,
        $options: "i"
      }
    }).select("_id, firmName")
    res.json(result)
  } catch (ex) {
    console.log("Error==>>", ex.error)
  }

}
const getAllFirmList = ()=>async(req,res)=>{
  const result = await Firm.find({isMuniVisorClient: true}).select(
    "_id firmName"
  )
  res.json(result)
}

const getFirmsDetailById = ()=>async (req, res,next)=>{
  const {
    id
  } = req.params
  const result = await Firm.findById(id).select("_id entityAliases isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure numEmployees annualRevenue addresses firmAddOns")
  console.log("Result==>>>",result)
  res.json(result)
}

const saveFirmDetail = () => async(req, res, next)=>{
  const {firmDetails} = req.body
  const entity = await Firm.create(firmDetails).select("_id entityAliases isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure numEmployees annualRevenue addresses firmAddOns")
  res.json(entity)
}

const updateFirmDetailById = () => async(req, res, next)=>{
  const {
    firmDetails
  } = req.body
  const {id} = req.params
  try{
    const getAllFirmDetails = await Firm.findById(id)
    const sortedData = firmDetails.addresses.sort((a, b) => a._id > b._id)
    getAllFirmDetails.addresses = getAllFirmDetails.addresses.filter((obj) => obj._id != sortedData[0]._id)
    firmDetails.addresses = [...getAllFirmDetails.addresses, ...
    sortedData
    ]
    const result = await Firm.findByIdAndUpdate(id, firmDetails, {
      new: true,
      upsert: true
    }).select("_id entityAliases isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure numEmployees annualRevenue addresses firmAddOns")  
    console.log("Result==>>>>",result)
    res.json(result)
  }catch(ex){
    console.log("ex===>>",ex)
  }  
}

const getAllThirdPartyFirmList = ()=>(req, res, next)=>{
  const {  
    entityId
  } = req.params
  try{
    EntityRel.aggregate([{
      $match: {
        relationshipType: "Third Party",
        entityParty1: ObjectID("5b1a63bd471a1919e0ca97d4")
      }
    },
    {
      $project: {
        "entityParty1": 1,
        "entityParty2": 1
      }
    },
    {
      "$lookup": {
        "from": "firms",
        "localField": "entityParty2",
        "foreignField": "_id",
        "as": "firmDetails"
      }
    },
    {
      $unwind: "$firmDetails"
    },
    {
      $project: {
        "_id":"$firmDetails._id",
        "firmName":"$firmDetails.firmName"
      }
    }],(err, result) => {
      if (err)
        throw new Error(err)
      console.log("result===>>>",result)
      res.json(result)
    })
  }catch(ex){
    console.log("Ex==>>",ex)
  }
}
const getThirdPartyFirmsDetailById = ()=>async (req, res,next)=>{
  const {
    id
  } = req.params
  const result = await Firm.findById(id).select("_id entityAliases entityFlags.marketRole isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure numEmployees annualRevenue addresses")
  console.log("Result==>>>",result)
  res.json(result)
}

const saveThirdPartyFirmDetail=()=>async(req,res,next)=>{    
  const {entityId} = req.params
  try{
    const entity = await Firm.create(req.body)
    if(entity._id){      
      EntityRel.create({
        "_id":ObjectID(),
        "entityParty1": ObjectID(entityId),
        "entityParty2": ObjectID(entity._id),
        "relationshipType": "Third Party",
      }, (err, result) => {
        if (err)          
          throw new Error(err)
        res.json(entity)
      }) 
    }            
  }catch(ex){
    console.log("===========>>>>>>",JSON.stringify(ex))
  }
}

const updateThirdPartyFirmDetailById = ()=>async (req,res,next)=>{
  const {
    firmDetails
  } = req.body
  const {id} = req.params
  try{
    console.log("Console====>>>")
    const getAllFirmDetails = await Firm.findById(id)
    const sortedData = firmDetails.addresses.sort((a, b) => a._id > b._id)
    getAllFirmDetails.addresses = getAllFirmDetails.addresses.filter((obj) => obj._id != sortedData[0]._id)
    firmDetails.addresses = [...getAllFirmDetails.addresses, ...
    sortedData
    ]
    const result = await Firm.findByIdAndUpdate(id, firmDetails, {
      new: true,
      upsert: true
    }).select("_id entityAliases entityFlags.marketRole isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure numEmployees annualRevenue addresses")
    console.log("Result==>>>>",result)
    res.json(result)
  }catch(ex){
    console.log("ex===>>",ex)
  } 
}


const getAllClientsFirmList = ()=>(req, res, next)=>{
  const {  
    entityId
  } = req.params
  try{
    EntityRel.aggregate([{
      $match: {
        relationshipType: "Client",
        entityParty1: ObjectID("5b1a63bd471a1919e0ca97d4")
      }
    },
    {
      $project: {
        "entityParty1": 1,
        "entityParty2": 1
      }
    },
    {
      "$lookup": {
        "from": "firms",
        "localField": "entityParty2",
        "foreignField": "_id",
        "as": "firmDetails"
      }
    },
    {
      $unwind: "$firmDetails"
    },
    {
      $project: {
        "_id":"$firmDetails._id",
        "firmName":"$firmDetails.firmName"
      }
    }],(err, result) => {
      if (err)
        throw new Error(err)
      console.log("result===>>>",result)
      res.json(result)
    })
  }catch(ex){
    console.log("Ex==>>",ex)
  }
}
const getClientsFirmsDetailById = ()=>async (req, res,next)=>{
  const {
    id
  } = req.params
  const result = await Firm.findById(id).select("_id entityAliases entityFlags.issuerFlags isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure  annualRevenue addresses entityLinkedCusips entityBorObl")
  console.log("Result==>>>",result)
  res.json(result)
}

const saveClientsFirmDetail=()=>async(req,res,next)=>{
  console.log("saveCLientFirmDetail==>>>")
  const {entityId} = req.params
  try{
    const entity = await Firm.create(req.body)
    console.log("Entity==>>",entity)
    if(entity._id){
      EntityRel.create({
        "_id":ObjectID(),
        "entityParty1": ObjectID(entityId),
        "entityParty2": ObjectID(entity._id),
        "relationshipType": "Client",
      }, (err, result) => {
        if (err)          
          throw new Error(err)
        res.json(entity)
      }) 
    }           
  }catch(ex){
    console.log("Errors===========",ex)
  }
}

const updateClientsFirmDetailById = ()=>async (req,res,next)=>{
  const {
    firmDetails
  } = req.body
  const {id} = req.params
  try{
    console.log("Console====>>>")
    const getAllFirmDetails = await Firm.findById(id)
    const sortedAddressesData = firmDetails.addresses.sort((a, b) => a._id > b._id)
    const entityLinkedCusips = firmDetails.entityLinkedCusips.sort((a, b) => a._id > b._id)
    const entityBorObl = firmDetails.entityBorObl.sort((a, b) => a._id > b._id)
    getAllFirmDetails.addresses = getAllFirmDetails.addresses.filter((obj) => obj._id != sortedAddressesData[0]._id)
    getAllFirmDetails.entityLinkedCusips = getAllFirmDetails.entityLinkedCusips.filter((obj) => obj._id != entityLinkedCusips[0]._id)
    getAllFirmDetails.entityBorObl = getAllFirmDetails.entityBorObl.filter((obj) => obj._id != entityBorObl[0]._id)
    firmDetails.addresses = [...getAllFirmDetails.addresses, ...
    sortedAddressesData
    ]
    firmDetails.entityLinkedCusips = [...getAllFirmDetails.addresses, ...
    entityLinkedCusips
    ]
    firmDetails.entityBorObl = [...getAllFirmDetails.addresses, ...
    entityBorObl
    ]
    const result = await Firm.findByIdAndUpdate(id, firmDetails, {
      new: true,
      upsert: true
    }).select("_id entityAliases entityFlags.issuerFlags isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmName taxId businessStructure  annualRevenue addresses entityLinkedCusips entityBorObl")
    console.log("Result==>>>>",result)
    res.json(result)
  }catch(ex){
    console.log("ex===>>",ex)
  } 
}




export default generateControllers(Firm, {
  getAllFirmList: getAllFirmList(),
  updateFirmDetailById: updateFirmDetailById(),
  saveFirmDetail: saveFirmDetail(),
  searchFirm: searchFirm(),
  getFirmsDetailById:getFirmsDetailById(),
  getAllThirdPartyFirmList:getAllThirdPartyFirmList(),
  getThirdPartyFirmsDetailById:getThirdPartyFirmsDetailById(),
  saveThirdPartyFirmDetail:saveThirdPartyFirmDetail(),
  updateThirdPartyFirmDetailById:updateFirmDetailById(),
  getAllClientsFirmList:getAllClientsFirmList(),
  getClientsFirmsDetailById:getClientsFirmsDetailById(),
  saveClientsFirmDetail:saveClientsFirmDetail(),
  updateClientsFirmDetailById:updateClientsFirmDetailById()
})
