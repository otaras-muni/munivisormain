
import { generateControllers } from "../../modules/query"
import { EntityRel } from "./entityRes.model"
import {canUserPerformAction} from "../../commonDbFunctions"
import {getAllEligibleUserIds,getAllEligibleIds} from "./../../entitlementhelpers"

const {ObjectID} = require("mongodb")

export default generateControllers(EntityRel)

export const getRelatedEntitiesForSearch = async (req,res) => {
  const {user} = req
  const {entityId,relationshipType,searchString} = req.body
  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EtityRel",access:1}
  ]
  const regexValue = new RegExp("","i")
  try {
    const allEligibleIds = await getAllEligibleIds(req.user, "all")
    const entitled = await canUserPerformAction(user,resRequested)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const value = await EntityRel.aggregate([
      {
        $match: {
          "entityParty1": ObjectID(entityId),
          "relationshipType": relationshipType
        }
      },
      {
        $project: {
          "firmEntityId":"$entityParty1",
          "relatedEntityId":"$entityParty2",
        }
      },
      {
        $lookup: {
          "from": "entities",
          "localField": "relatedEntityId",
          "foreignField": "_id",
          "as": "relatedEntityInfo"
        }
      },
      { $unwind : "$relatedEntityInfo"},
      {
        $project: {
          "firmEntityId":"$entityParty1",
          "relEntityId":"$relatedEntityInfo._id",
          "relEntMsrbFirmName":"$relatedEntityInfo.msrbFirmName",
          "relEntFirmName":"$relatedEntityInfo.firmName",
          "relEntAliases":"$relatedEntityInfo.entityAliases",
          "relEntMarketRole":"$relatedEntityInfo.entityFlags.marketRole",
          "relEntIssuerFlags":"$relatedEntityInfo.entityFlags.issuerFlags",
          "relEntMsrbRegistrantType":"$relatedEntityInfo.msrbRegistrantType",
          "relEntMsrbId":"$relatedEntityInfo.msrbId",
          "relEntCountry":"$relatedEntityInfo.addresses.country",
          "relEntState":"$relatedEntityInfo.addresses.state",
          "relEntEmails":"$relatedEntityInfo.addresses.officeEmails",
          "relEntborOblFirmName":"$relatedEntityInfo.entityBorObl.borOblFirmName",
          "relEntAssCusip6":"$relatedEntityInfo.entityLinkedCusips.associatedCusip6"
        }
      },
      {$match :{$and:[{$or:[
        { "relEntMsrbFirmName":{$regex:regexValue,$options:"i"}},
        { "relEntFirmName":{$regex:regexValue,$options:"i"}},
        { "relEntAliases":{$regex:regexValue,$options:"i"}},
        { "relEntMarketRole":{$regex:regexValue,$options:"i"}},
        { "relEntIssuerFlags":{$regex:regexValue,$options:"i"}},
        { "relEntMsrbRegistrantType":{$regex:regexValue,$options:"i"}},
        { "relEntMsrbId":{$regex:regexValue,$options:"i"}},
        { "relEntCountry":{$regex:regexValue,$options:"i"}},
        { "relEntState":{$regex:regexValue,$options:"i"}},
        { "relEntEmails":{$regex:regexValue,$options:"i"}},
        { "relEntborOblFirmName":{$regex:regexValue,$options:"i"}},
        { "relEntAssCusip6":{$regex:regexValue,$options:"i"}}
      ]},{"relEntityId": {"$in": [...allEligibleIds]}}]}
      },
      {
        $project: {
          relEntityId:1,
          relEntMsrbFirmName:1,
          relEntFirmName:1,
          relEntMsrbRegistrantType:1
        }
      }
    ])

    res.json(value)
  }
  catch (e) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

export const getEntityUsersForSearch = async (req,res) => {
  const {user} = req
  const {entityId,relationshipType,searchString} = req.body
  const regexValue = new RegExp(searchString,"i")

  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EntityRel",access:1},
    {resource:"EntityUser",access:1}
  ]

  try {
    const allEligibleIds = await getAllEligibleUserIds(req.user, "all")
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const match = relationshipType ==="Client" ?
      {
        $or:[
          {"entityParty1": ObjectID(entityId),
            "relationshipType": "Client"},
          {"entityParty1": ObjectID(entityId),
            "relationshipType": "Prospect"}
        ]
      }
      :{
        "entityParty1": ObjectID(entityId),
        "relationshipType": relationshipType
      }
    const value = await EntityRel.aggregate([
      {
        $match:match
      },
      {
        $project: {
          "firmEntityId":"$entityParty1",
          "relatedEntityId":"$entityParty2",
        }
      },
      {
        $lookup: {
          "from": "entities",
          "localField": "relatedEntityId",
          "foreignField": "_id",
          "as": "relatedEntityInfo"
        }
      },
      { $unwind : "$relatedEntityInfo"},
      {
        $project: {
          "firmEntityId":"$entityParty1",
          "relEntityId":"$relatedEntityInfo._id",
          "relEntMsrbFirmName":"$relatedEntityInfo.msrbFirmName",
          "relEntFirmName":"$relatedEntityInfo.firmName",
          "relEntAliases":"$relatedEntityInfo.entityAliases",
          "relEntMarketRole":"$relatedEntityInfo.entityFlags.marketRole",
          "relEntIssuerFlags":"$relatedEntityInfo.entityFlags.issuerFlags",
          "relEntMsrbRegistrantType":"$relatedEntityInfo.msrbRegistrantType",
          "relEntMsrbId":"$relatedEntityInfo.msrbId",
          "relEntCountry":"$relatedEntityInfo.addresses.country",
          "relEntState":"$relatedEntityInfo.addresses.state",
          "relEntEmails":"$relatedEntityInfo.addresses.officeEmails",
          "relEntborOblFirmName":"$relatedEntityInfo.entityBorObl.borOblFirmName",
          "relEntAssCusip6":"$relatedEntityInfo.entityLinkedCusips.associatedCusip6"
        }
      },
      {
        $lookup: {
          "from": "entityusers",
          "localField": "relEntityId",
          "foreignField": "entityId",
          "as": "relatedentityusers"
        }
      },
      { $unwind : "$relatedentityusers"},
      { $unwind : "$relatedentityusers.userAddresses"},
      { $unwind : "$relatedentityusers.userEmails"},
      {
        $project: {
          "firmEntityId":1,
          "relEntityId":1,
          "relEntMsrbFirmName":1,
          "relEntFirmName":1,
          "relEntAliases":1,
          "relEntMarketRole":1,
          "relEntIssuerFlags":1,
          "relEntMsrbRegistrantType":1,
          "relEntMsrbId":1,
          "relEntUserId":"$relatedentityusers._id",
          "relEntUserFlags":"$relatedentityusers.relEntUserFlags",
          "relEntUserFirstName":"$relatedentityusers.userFirstName",
          "relEntUserLastName":"$relatedentityusers.userLastName",
          "relEntUserEmails":"$relatedentityusers.userEmails.emailId"  ,
          "relEntUserJobTitle":"$relatedentityusers.userJobTitle" ,
          "relEntUserAddressName":"$relatedentityusers.userAddresses.addressName",
          "relEntUserAddrCountry":"$relatedentityusers.userAddresses.country",
          "relEntUserAddrState":"$relatedentityusers.userAddresses.state",
          "relEntUserAddrCity":"$relatedentityusers.userAddresses.city"
        }
      },
      {$match : {$and:[{$or:[
        { "relEntMsrbFirmName":{$regex:regexValue,$options:"i"}},
        { "relEntFirmName":{$regex:regexValue,$options:"i"}},
        { "relEntAliases":{$regex:regexValue,$options:"i"}},
        { "relEntMarketRole":{$regex:regexValue,$options:"i"}},
        { "relEntIssuerFlags":{$regex:regexValue,$options:"i"}},
        { "relEntMsrbRegistrantType":{$regex:regexValue,$options:"i"}},
        { "relEntMsrbId":{$regex:regexValue,$options:"i"}},
        { "relEntUserFirstName":{$regex:regexValue,$options:"i"}},
        { "relEntUserLastName":{$regex:regexValue,$options:"i"}},
        { "relEntUserEmails":{$regex:regexValue,$options:"i"}},
        { "relEntUserAddresses":{$regex:regexValue,$options:"i"}},
        { "relEntAssCusip6":{$regex:regexValue,$options:"i"}},
        { "relEntUserFlags":{$regex:regexValue,$options:"i"}},
        { "relEntUserJobTitle":{$regex:regexValue,$options:"i"}},
      ]},{"relEntUserId": {"$in": [...allEligibleIds]}}]}
      },
      {
        $group: {
          _id: {
            userId: "$relEntUserId",
            entityId: "$relEntityId",
            firstName: "$relEntUserFirstName",
            lastName:"$relEntUserLastName",
            firmName:"$relEntFirmName",
            relEntMsrbRegistrantType:"$relEntMsrbRegistrantType",
            msrbId:"$relEntMsrbId"
          },
          count: {
            $sum: 1
          },
        }
      },
      {
        $project: {
          "_id":0,
          userId:"$_id.userId",
          entityId:"$_id.entityId",
          firstName:"$_id.firstName",
          lastName:"$_id.lastName",
          firmName:"$_id.firmName",
        }
      },
      { $sort : { firstName : 1} }
    ])
    res.json(value)
  }
  catch (e) {
    console.log("====>>>",e)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}
export const getAllEntityUsersList = async(req,res)=>{
  const {entityId, filteredValue} = req.body
  if(filteredValue.groupBy==="entityname")
    filteredValue.groupBy = "firmName"
  if(filteredValue.groupBy==="state")
    filteredValue.groupBy = "userAddressState"
  const {user} = req
  const resRequested = [
    {resource:"Entity",access:1},
    {resource:"EntityRel",access:1},
    {resource:"EntityUser",access:1}
  ]

  try {
    const allEligibleIds = await getAllEligibleUserIds(req.user, "all")
    const searchBy=[{"$or":[{"isPrimary":true},{"isPrimary":false}]},{"userId": {"$in": [...allEligibleIds]}}]
    if(filteredValue!==""){
      if(filteredValue.entitySearch!==""){
        const regexValue = new RegExp(filteredValue.entitySearch,"i")
        searchBy.push({"$or":[
          { "firmName":{$regex:regexValue,$options:"i"}},
          { "firstName":{$regex:regexValue,$options:"i"}},
          { "middleName":{$regex:regexValue,$options:"i"}},
          { "lastName":{$regex:regexValue,$options:"i"}},
          { "userAddressName":{$regex:regexValue,$options:"i"}},
          { "userAddressLine1":{$regex:regexValue,$options:"i"}},
          { "userAddressLine2":{$regex:regexValue,$options:"i"}},
          { "userAddressCity":{$regex:regexValue,$options:"i"}},
          { "userAddressState":{$regex:regexValue,$options:"i"}},
          { "userAddressCountry":{$regex:regexValue,$options:"i"}}
        ]})
      }
      if(filteredValue.entityType!==""){
        if(filteredValue.entityType==="Client" || filteredValue.entityType==="Prospect")
          searchBy.push({"relationshipType":filteredValue.entityType})
        if(filteredValue.entityType==="ClentProspect")
          searchBy.push({"$or":[
            {"relationshipType":"Client"},
            {"relationshipType":"Prospect"}
          ]})
        if(!["Client","Prospect","ClentProspect"].includes(filteredValue.entityType))
          searchBy.push( {"marketRole":{ $in:[filteredValue.entityType]}})
      }

      if(filteredValue.enityState && filteredValue.enityState.length>0)
        searchBy.push({"userAddressState":{ $in:filteredValue.enityState}})
    }
    const entitled = await canUserPerformAction(user,resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE",entitled)
    if( !entitled) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    const value = await EntityRel.aggregate([
      {
        $match: {$or:[
          {"entityParty1": ObjectID(entityId),
            "relationshipType": "Third Party"},
          {"entityParty1": ObjectID(entityId),
            "relationshipType": "Client"},
          {"entityParty1": ObjectID(entityId),
            "relationshipType": "Self"},
          {"entityParty1": ObjectID(entityId),
            "relationshipType": "Prospect"}
        ]
        }
      },
      {
        $project: {
          "relationshipType":"$relationshipType",
          "firmEntityId":"$entityParty1",
          "relatedEntityId":"$entityParty2",
        }
      },
      {
        $lookup: {
          "from": "entities",
          "localField": "relatedEntityId",
          "foreignField": "_id",
          "as": "relatedEntityInfo"
        }
      },
      { $unwind : "$relatedEntityInfo"},
      {
        $project: {
          "firmEntityId":"$entityParty1",
          "relationshipType":"$relationshipType",
          "relEntityId":"$relatedEntityInfo._id",
  
          // "relEntMsrbFirmName":"$relatedEntityInfo.msrbFirmName",
          "relEntFirmName":"$relatedEntityInfo.firmName",
          "relEntAliases":"$relatedEntityInfo.entityAliases",
          "relEntMarketRole":{$ifNull:["$relatedEntityInfo.entityFlags.marketRole",[]]},
          "relEntIssuerFlags":{$ifNull:["$relatedEntityInfo.entityFlags.issuerFlags",[]]},
          "relEntMsrbRegistrantType":{$ifNull:["$relatedEntityInfo.msrbRegistrantType","-"]},
          "relEntMsrbId":{$ifNull:["$relatedEntityInfo.msrbId","NO MSRB ID"]},
          "relEntCountry":{$ifNull:["$relatedEntityInfo.addresses.country","United States"]},
          "relEntState":{$ifNull:["$relatedEntityInfo.addresses.state","NO STATE"]},
          "relEntEmails":"$relatedEntityInfo.addresses.officeEmails",
          "relEntPhone":"$relatedEntityInfo.addresses.userPhone",
          "relEntborOblFirmName":"$relatedEntityInfo.entityBorObl.borOblFirmName",
          "relEntAssCusip6":"$relatedEntityInfo.entityLinkedCusips.associatedCusip6"
        }
      },
      {
        $lookup: {
          "from": "entityusers",
          "localField": "relEntityId",
          "foreignField": "entityId",
          "as": "relatedentityusers"
        }
      },
      {$unwind:{"path":"$relatedentityusers", "preserveNullAndEmptyArrays": true}},
      {$unwind:{"path":"$relatedentityusers.userAddresses", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          "firmEntityId":1,
          "relationshipType":1,
          "relEntityId":1,
          "relEntFirmName":1,
          "relEntAliases":1,
          "relEntMarketRole":1,
          "relEntIssuerFlags":1,
          "relEntMsrbRegistrantType":1,
          "relEntMsrbId":1,
          "relEntUserId":"$relatedentityusers._id",
          "relEntUserFlags":"$relatedentityusers.relEntUserFlags",
          "relEntUserFirstName":"$relatedentityusers.userFirstName",
          "relEntUserMiddleName":"$relatedentityusers.userMiddleName",
          "relEntUserLastName":"$relatedentityusers.userLastName",
          "relEntUserEmails":"$relatedentityusers.userEmails",
          "relEntUserPhone":"$relatedentityusers.userPhone",
          "relEntUserJobTitle":"$relatedentityusers.userJobTitle" ,
  
          "relEntUserAddressType":{$ifNull:["$relatedentityusers.userAddresses.addressType","NO ADDRESS"]},
          "relEntUserIsPrimary":{$ifNull:["$relatedentityusers.userAddresses.isPrimary",false]},
          "relEntUserAddressName":{$ifNull:["$relatedentityusers.userAddresses.addressName","NO ADDRESS"]},
          "relEntUserAddrCountry":{$ifNull:["$relatedentityusers.userAddresses.country","United States"]},
          "relEntUserAddrState":{$ifNull:["$relatedentityusers.userAddresses.state","NO STATE"]},
          "relEntUserAddressLine1":{$ifNull:["$relatedentityusers.userAddresses.addressLine1","NO ADDRESS"]},
          "relEntUserAddressLine2":{$ifNull:["$relatedentityusers.userAddresses.addressLine2","NO ADDRESS"]},
          "relEntUserAddrCity":{$ifNull:["$relatedentityusers.userAddresses.city","NO CITY"]},
        }
      },
      {
        $group: {
          _id: {
            relEntMsrbRegistrantType:"$relEntMsrbRegistrantType",
            relationshipType:"$relationshipType",
            msrbId:"$relEntMsrbId",
            userId: "$relEntUserId",
            firmName:"$relEntFirmName",
            entityId: "$relEntityId",
            marketRole:"$relEntMarketRole",
            firstName: "$relEntUserFirstName",
            middleName:"$relEntUserMiddleName",
            lastName:"$relEntUserLastName",
            userEmails:"$relEntUserEmails",
            userPhone : "$relEntUserPhone",
            // userAddressType:"$relEntUserAddressType",
            isPrimary:"$relEntUserIsPrimary",
            userAddressName:"$relEntUserAddressName",
            userAddressLine1:"$relEntUserAddressLine1",
            userAddressLine2:"$relEntUserAddressLine2",
            userAddressCity:"$relEntUserAddrCity",
            userAddressState:"$relEntUserAddrState",
            userAddressCountry:"$relEntUserAddrCountry"
          },
          count: {
            $sum: 1
          },
        }
      },
      {
        $project: {
          "_id":0,
          relEntMsrbRegistrantType:"$_id.relEntMsrbRegistrantType",
          relationshipType:"$_id.relationshipType",
          msrbId:"$_id.msrbId",
          userId: "$_id.userId",
          firmName:"$_id.firmName",
          entityId: "$_id.entityId",
          marketRole:"$_id.marketRole",
          firstName: "$_id.firstName",
          middleName:"$_id.middleName",
          lastName:"$_id.lastName",
          userEmails:"$_id.userEmails",
          userPhone : "$_id.userPhone",
          // userAddressType:"$_id.userAddressType",
          isPrimary : "$_id.isPrimary",
          userAddressName:"$_id.userAddressName",
          userAddressLine1:"$_id.userAddressLine1",
          userAddressLine2:"$_id.userAddressLine2",
          userAddressCity:"$_id.userAddressCity",
          userAddressState:"$_id.userAddressState",
          userAddressCountry:"$_id.userAddressCountry"
        }
      },
      { $sort : { firmName : 1} },
      {
        $match:{
          $and:searchBy
        }
      },
      {
        "$group": {
          "_id":(filteredValue.groupBy!==""?`$${filteredValue.groupBy}`:""),
          "users": { "$push":{
            relEntMsrbRegistrantType:"$relEntMsrbRegistrantType",
            relationshipType:"$relationshipType",
            msrbId:"$msrbId",
            userId: "$userId",
            firmName:"$firmName",
            entityId: "$entityId",
            marketRole:"$marketRole",
            firstName: "$firstName",
            middleName:"$middleName",
            lastName:"$lastName",
            userEmails:"$userEmails",
            userPhone : "$userPhone",
            userAddressName:"$userAddressName",
            isPrimary:"$isPrimary",
            userAddressLine1:"$userAddressLine1",
            userAddressLine2:"$userAddressLine2",
            userAddressCity:"$userAddressCity",
            userAddressState:"$userAddressState",
            userAddressCountry:"$userAddressCountry"
          }}
        }
      },
      { $project : {_id:0,groupBy:"$_id",users:1}}
    ])
    const result=[]
    value.forEach(element=>{
      const users=[]
      JSON.parse(JSON.stringify(element.users)).filter((item, index, self) =>{
        if(self.findIndex((t)=>(t.entityId === item.entityId && t.userId === item.userId)) === index)
        {
          const data = {}
          data.entityId = item.entityId
          data.userId = item.userId
          data.relationshipType = item.relationshipType ==="Self" ? "Firm" : item.relationshipType
          data.firstName = item.firstName
          data.firmName = item.firmName
          data.middleName = item.middleName
          data.lastName = item.lastName
          data.primaryAddress = `${item.userAddressName}, ${item.userAddressLine1}, ${item.userAddressLine2}, ${item.userAddressCity}, ${item.userAddressState}`
          item.userEmails.map(email=>{
            if(email.emailPrimary){
              data.primaryEmail = email.emailId
            }
          })

          item.userPhone.map(phone=>{
            if(phone.phonePrimary){
              data.phoneNumber = phone.phoneNumber
            }
          })
          users.push(data)}
      })
      result.push({groupBy:element.groupBy,users})
    })
    if(filteredValue==="" && result.length>0){
      res.json(result[0].users)
    }else{
      res.json(result)
    }
  }
  catch (e) {
    console.log("Exception====>>>",e)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}
