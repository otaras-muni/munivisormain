import express from "express"
import entityResController,{getRelatedEntitiesForSearch,getEntityUsersForSearch,getAllEntityUsersList,getUsersListByEntityId} from "./entityRes.controller"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const entityResRouter = express.Router()

entityResRouter.param("id", entityResController.findByParam)

entityResRouter.route("/")
  .get(entityResController.getAll)
  .post(entityResController.createOne)
  
entityResRouter.route("/testsearch")
  .post(requireAuth,getRelatedEntitiesForSearch)

entityResRouter.route("/entityusersearch")
  .post(requireAuth,getEntityUsersForSearch) 
  
entityResRouter.route("/entityUsers")
  .post(requireAuth,getAllEntityUsersList)

entityResRouter.route("/:id")
  .get(entityResController.getOne)
  .put(entityResController.updateOne)
  .delete(entityResController.createOne)

