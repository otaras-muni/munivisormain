
export const constructPipelineForDashboards = (type, {user,alluserdetails,finalQueryPipeline} ) => {
  const transactionPipelineForDashboards = {
    "deals":[
      {
        $lookup:{
          from: "tranagencydeals",
          localField: "tenantId",
          foreignField: "dealIssueTranClientId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.dealIssueTranClientId",
          clientEntityId: "$trans.dealIssueTranIssuerId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] },
                    else: { $ifNull: [["$trans.dealIssueParticipants.dealPartContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.dealIssueTranAssignedTo",[]]} } ] }, 
                  then: { $ifNull: ["$trans.dealIssueTranAssignedTo", []] },
                  else: { $ifNull: [["$trans.dealIssueTranAssignedTo"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranClientId" , []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueTranClientId" , []] },
                    else: { $ifNull: [["$trans.dealIssueTranClientId" ], { $ifNull: ["$trans.dealIssueTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] },
                    else: { $ifNull: [["$trans.dealIssueTranIssuerId"], { $ifNull: ["$trans.dealIssueTranIssuerId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] },
                    else: { $ifNull: [["$trans.dealIssueParticipants.dealPartFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] },
                    else: { $ifNull: [["$trans.dealIssueUnderwriters.dealPartFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
              then:"$trans.dealIssueTranProjectDescription",
              else:"$trans.dealIssueTranIssueName"},
          },
          tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
          tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
          tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
          tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
          tranPrimarySector:"$trans.dealIssueTranPrimarySector",
          tranSecondarySector:"$trans.dealIssueTranSecondarySector",
          tranStatus:{$ifNull:["$trans.dealIssueTranStatus","-"]},
          tranClientName:"$trans.dealIssueTranIssuerFirmName",
          tranFirmName:"$trans.dealIssueTranClientFirmName",
          tranType:"$trans.dealIssueTranType",
          tranSubType:"$trans.dealIssueTranSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          assignee:"$trans.dealIssueTranAssignedTo",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",{$ifNull:["$trans.dealIssueTranAssignedTo",[]]}]}
              }},0]
          },
        },
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
    "rfps":[
      {
        $lookup:{
          from: "tranagencyrfps",
          localField: "tenantId",
          foreignField: "rfpTranClientId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.rfpTranClientId",
          clientEntityId: "$trans.rfpTranIssuerId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: "$trans.rfpProcessContacts.rfpProcessContactId" } ] },
                    then: { $ifNull: ["$trans.rfpProcessContacts.rfpProcessContactId", []] },
                    else: { $ifNull: [["$trans.rfpProcessContacts.rfpProcessContactId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: "$trans.rfpEvaluationTeam.rfpSelEvalContactId" } ] },
                    then: { $ifNull: ["$trans.rfpEvaluationTeam.rfpSelEvalContactId", []] },
                    else: { $ifNull: [["$trans.rfpEvaluationTeam.rfpSelEvalContactId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: "$trans.rfpParticipants.rfpParticipantContactId" } ] },
                    then: { $ifNull: ["$trans.rfpParticipants.rfpParticipantContactId", []] },
                    else: { $ifNull: [["$trans.rfpParticipants.rfpParticipantContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: "$trans.rfpTranAssignedTo" } ] },
                  then: { $ifNull: ["$trans.rfpTranAssignedTo", []] },
                  else: { $ifNull: [["$trans.rfpTranAssignedTo"], []] }
                }
                }
                , []] },
              { $ifNull: [["$trans.createdByUser"], []] }
            ]
          },
          tranEntities:{
            $setUnion:[
              ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
              "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
              "$trans.rfpProcessContacts.rfpProcessFirmId",
              "$trans.rfpParticipants.rfpParticipantFirmId",
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
              then:"$trans.rfpTranProjectDescription",
              else:"$trans.rfpTranIssueName"},
          },
          tranParAmount:"-",
          tranExpectedAwardDate:"-",
          tranActualAwardDate:"-",
          tranPricingDate:"-",
          tranPrimarySector:"$trans.rfpTranPrimarySector",
          tranSecondarySector:"$trans.rfpTranSecondarySector",
          tranStatus:{$ifNull:["$trans.rfpTranStatus","-"]},
          tranClientName:"$trans.rfpTranIssuerFirmName",
          tranFirmName:"$trans.rfpTranClientFirmName",
          tranType:"$trans.rfpTranType",
          tranSubType:"$trans.rfpTranSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId","$trans.rfpTranAssignedTo"]}
              }},0]
          }
        },
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
    "bankloans":[
      {
        $lookup:{
          from: "tranbankloans",
          localField: "tenantId",
          foreignField: "actTranFirmId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.actTranFirmId",
          clientEntityId: "$trans.actTranClientId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] } } ] },
                    then: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] },
                    else: { $ifNull: [["$trans.bankLoanParticipants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                  then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                  else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranFirmId" , []] },
                    else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranClientId" , []] },
                    else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] },
                    else: { $ifNull: [["$trans.bankLoanParticipants.partFirmId"], []] }
                  }
                  }
                  , []] 
              }
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
              then:"$trans.actTranProjectDescription",
              else:"$trans.actTranIssueName"},
          },
          tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
          tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
          tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
          tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
          tranPrimarySector:"$trans.actTranPrimarySector",
          tranSecondarySector:"$trans.actTranSecondarySector",
          tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
          tranClientName:"$trans.actTranClientName",
          tranFirmName:"$trans.actTranFirmName",
          tranType:"$trans.actTranType",
          tranSubType:"$trans.actTranSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
              }},0]
          },
        }
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
    "derivatives":[
      {
        $lookup:{
          from: "tranderivatives",
          localField: "tenantId",
          foreignField: "actTranFirmId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.actTranFirmId",
          clientEntityId: "$trans.actTranClientId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] } } ] },
                    then: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] },
                    else: { $ifNull: [["$trans.derivativeParticipants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                  then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                  else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranFirmId" , []] },
                    else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranClientId" , []] },
                    else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] },
                    else: { $ifNull: [["$trans.derivativeParticipants.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
              then:"$trans.actTranProjectDescription",
              else:"$trans.actTranIssueName"},
          },
          tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
          tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate","-"]},
          tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate","-"]},
          tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate","-"]},
          tranPrimarySector:"$trans.actTranPrimarySector",
          tranSecondarySector:"$trans.actTranSecondarySector",
          tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
          tranClientName:"$trans.actTranClientName",
          tranFirmName:"$trans.actTranFirmName",
          tranType:"$trans.actTranType",
          tranSubType:"$trans.actTranSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
              }},0]
          },
        }
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
    "marfps":[
      {
        $lookup:{
          from: "actmarfps",
          localField: "tenantId",
          foreignField: "actTranFirmId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.actTranFirmId",
          clientEntityId: "$trans.actIssuerClient",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] } } ] },
                    then: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] },
                    else: { $ifNull: [["$trans.maRfpParticipants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] }, 
                  then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                  else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranFirmId" , []] },
                    else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                    then: { $ifNull: ["$trans.actIssuerClient" , []] },
                    else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] },
                    else: { $ifNull: [["$trans.maRfpParticipants.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
              then:"$trans.actProjectName",
              else:"$trans.actIssueName"},
          },
          tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
          tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate","-"]},
          tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate","-"]},
          tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate","-"]},
          tranPrimarySector:"$trans.actPrimarySector",
          tranSecondarySector:"$trans.actSecondarySector",
          tranStatus:{$ifNull:["$trans.actStatus","-"]},
          tranClientName:"$trans.actIssuerClientEntityName",
          tranFirmName:"$trans.actTranFirmName",
          tranType:"$trans.actType",
          tranSubType:"$trans.actSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
              }},0]
          },
        }
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
    "others":[
      {
        $lookup:{
          from: "tranagencyothers",
          localField: "tenantId",
          foreignField: "actTranFirmId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.actTranFirmId",
          clientEntityId: "$trans.actTranClientId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partContactId", []] } } ] },
                    then: { $ifNull: ["$trans.participants.partContactId", []] },
                    else: { $ifNull: [["$trans.participants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                  then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                  else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranFirmId" , []] },
                    else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranClientId" , []] },
                    else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.participants.partFirmId", []] },
                    else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] },
                    else: { $ifNull: [["$trans.actTranUnderwriters.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
              then:"$trans.actTranProjectDescription",
              else:"$trans.actTranIssueName"},
          },
          tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
          tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
          tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
          tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
          tranPrimarySector:"$trans.actTranPrimarySector",
          tranSecondarySector:"$trans.actTranSecondarySector",
          tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
          tranClientName:"$trans.actTranClientName",
          tranFirmName:"$trans.actTranFirmName",
          tranType:"$trans.actTranType",
          tranSubType:"$trans.actTranSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
              }},0]
          },
        }
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
    "busdevs":[
      {
        $lookup:{
          from: "actbusdevs",
          localField: "tenantId",
          foreignField: "actTranFirmId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          firmEntityId:"$trans.actTranFirmId",
          clientEntityId: "$trans.actIssuerClient",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] } } ] },
                    then: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] },
                    else: { $ifNull: [["$trans.participants.rfpParticipantContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] }, 
                  then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                  else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$trans.actTranFirmId" , []] },
                    else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                    then: { $ifNull: ["$trans.actIssuerClient" , []] },
                    else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.participants.partFirmId", []] },
                    else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                  }
                  }
                  , []] 
              }
            ]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
              then:"$trans.actProjectName",
              else:"$trans.actIssueName"},
          },
          tranParAmount:"-",
          tranExpectedAwardDate:"-",
          tranActualAwardDate:"-",
          tranPricingDate:"-",
          tranPrimarySector:"$trans.actPrimarySector",
          tranSecondarySector:"$trans.actSecondarySector",
          tranStatus:{$ifNull:["$trans.actStatus","-"]},
          tranClientName:"$trans.actIssuerClientEntityName",
          tranFirmName:"$trans.actLeadFinAdvClientEntityName",
          tranType:"$trans.actType",
          tranSubType:"$trans.actSubType",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          myFirmUserIds:"$loggedInFirmUserIds",
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
              }},0]
          },
        }
      },
      {
        $addFields:{
          allIds:{
            $setUnion:[
              "$tranUsers",
              "$tranEntities",
              ["$tranId"]
            ]
          },
        }
      },
      ...finalQueryPipeline
    ],
  }
  console.log("The pipeline for dashboards",JSON.stringify(transactionPipelineForDashboards[type],null,2))
  return type === "all" ? transactionPipelineForDashboards : transactionPipelineForDashboards[type]
}

