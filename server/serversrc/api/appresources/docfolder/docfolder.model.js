import mongoose from "mongoose"

const { Schema } = mongoose

export const eSignSchema = new Schema({
  _id: false,
  envelopeId: String,
  status: String,
  statusDateTime: Date,
  sent: Date,
  signerEmail: String,
  created: Date,
  delivered: Date,
  signed: Date,
  completed: Date,
  fileDate: String
}, {strict: false })

// export const s3VersionSchema = new Schema({
//   _id: false,
//   versionId: String,
//   uploadDate: Date,
//   size: String
// }, {strict: false })

export const docFolderMetaSchema = new Schema({
  _id: false,
  type: String,
  versions: [{}, { strict: false, _id: false }],
  eSign: eSignSchema
}, {strict: false })

const shareFolderSchema = Schema({
  id: String,
  name: String,
})

const shareDocumentsSchema = Schema({
  name: String,
  id: String,
  files: Array
})

const docFolderSchema = new Schema({
  folderName: String,
  entityId: String,
  userId: String,
  shareFolder: [shareFolderSchema],
  shareDocuments: [shareDocumentsSchema],
  myDocuments: Boolean,
  createdDate: Date,
  updatedDate: Date
})

docFolderSchema.index({tenantId:1, contextId:1, contextType:1})

export const DocFolder = mongoose.model("docfolder", docFolderSchema)
