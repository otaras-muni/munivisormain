import { generateControllers } from "../../modules/query"
import { DocFolder } from "./docfolder.model"

export default generateControllers(DocFolder)
