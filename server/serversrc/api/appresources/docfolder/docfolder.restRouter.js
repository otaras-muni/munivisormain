import express from "express"
import docFolderController from "./docfolder.controller"
import { DocFolder } from "./docfolder.model"
import {requireAuth} from "./../../../authorization/authsessionmanagement/auth.middleware"


export const docFolderRouter = express.Router()

docFolderRouter.post("/update-versions", async (req, res) => {
  const { _id, versions, name, originalName, option } = req.body
  if(!_id || !option || !versions || !versions.length) {
    return res.status(422).send({ error: "invalid input" })
  }

  try {
    if(option === "add") {
      if(!name || !originalName) {
        return res.status(422).send({ error: "invalid input" })
      }
      const result = await DocFolder.update({ _id }, {
        $set: { name, originalName },
        $push: { "meta.versions": { $each: versions } }
      })
      res.json(result)
    } else if(option === "remove") {
      const result = await DocFolder.update({ _id }, {
        $pull: { "meta.versions": {
          versionId: { $in: versions.filter(e => e) }
        } }
      })
      res.json(result)
    } else {
      return res.status(422).send({ error: "Wrong update option provided" })
    }
  } catch (err) {
    return res.status(422).send({ error: err })
  }
})

docFolderRouter.param("id", docFolderController.findByParam)

docFolderRouter.route("/")
  .get(requireAuth,docFolderController.getAll)
  .post(requireAuth,docFolderController.createOne)

docFolderRouter.route("/:id")
  .get(requireAuth,docFolderController.getOne)
  .put(requireAuth,docFolderController.updateOne)
  .delete(requireAuth,docFolderController.deleteOne)
