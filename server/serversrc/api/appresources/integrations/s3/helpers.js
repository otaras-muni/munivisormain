import axios from "axios"
import config from "../../../../config"

const muniApiBaseURL = `http://localhost:${config.port || 4001}`

export const getS3DownloadURLs = async (docIds, token, expires) => {
  let urls = []
  try {
    const res = await axios.post(`${muniApiBaseURL}/api/s3/get-s3-download-urls`, { docIds, expires }, {headers:{ "Authorization": token}})
    if(res && res.data) {
      urls = res.data
    }
  } catch(err) {
    console.log("err in getting s3 urls : ", err)
  }
  return urls
}

export const getFilesFromS3 = async (docIds) => {
  const len = docIds.length
  const files = []
  for(let i = 0; i < len; i++) {
    try {
      const res = await axios.get(`${muniApiBaseURL}/api/docs/${docIds[i]}`)
      if(res && res.data) {
        const doc = res.data
        const { contextId, contextType, tenantId, name, originalName } = doc
        const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
        console.log("objectName : ", objectName)
        const fileData = await axios.post(`${muniApiBaseURL}/api/s3/get-s3-object`, { objectName })
        files.push({ filename: originalName, content: fileData.data.result.Body })
      }
    } catch(err) {
      console.log("err in getting file : ", err)
    }
  }
  return files
}
