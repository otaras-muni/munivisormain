import {Entity} from "./entity.model"
import {EntityRel} from "../entityRel/entityRes.model"
import {GlobRefEntity} from "./globalRefEntity.model"

import {canUserPerformAction, isUserPlatformAdmin} from "../../commonDbFunctions"
import {getAllEligibleIds, doesIdExistsInEntitledEntityIds, getAllEligibleUserIds, getAllEligibleIDsForSpecificType, updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {elasticSearchUpdateFunction} from "./../../elasticsearch/esHelper"
import {generateConfigForEntities} from "../config/generateConfigForDifferentEntityTypes"
import {EntityUser} from "../entityUser/entityUser.model"

const {ObjectID} = require("mongodb")



const returnFromES = (tenantId,id) => elasticSearchUpdateFunction({tenantId,Model: Entity, _id: id, esType: "entities"})

const returnFromESRel = (tenantId,id) => elasticSearchUpdateFunction({tenantId,Model: EntityRel, _id: id, esType: "entityrels"})

export const getEntities = async(req, res) => {}

export const getEntityById = async(req, res) => {
  const {entityId} = req.params

  if (!entityId) {
    res
      .status(422)
      .send("No entity id provided")
  }

  try {
    const entity = await Entity.aggregate([
      {
        $match: {
          _id: ObjectID(entityId)
        }
      }, {
        $limit: 1
      }, {
        $lookup: {
          from: "entityrels",
          localField: "_id",
          foreignField: "entityParty2",
          as: "rel"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "primaryContactId",
          foreignField: "_id",
          as: "primaryContact"
        }
      }, {
        $replaceRoot: {
          newRoot: {
            $mergeObjects: [
              {
                relationshipType: {
                  $arrayElemAt: ["$rel.relationshipType", 0]
                }
              }, {
                primaryContactFirstName: {
                  $arrayElemAt: ["$primaryContact.userFirstName", 0]
                }
              }, {
                primaryContactLastName: {
                  $arrayElemAt: ["$primaryContact.userLastName", 0]
                }
              }, {
                primaryContactEmail: {
                  $arrayElemAt: ["$primaryContact.userLoginCredentials.userEmailId", 0]
                }
              },
              "$$ROOT"
            ]
          }
        }
      }, {
        $project: {
          rel: 0,
          primaryContact: 0
        }
      }
    ])
    res.json(entity)
  } catch (err) {
    res
      .status(422)
      .send({err})
  }
}

export const saveEntity = async(req, res) => {
  const {user} = req
  const {entityDetails, relationshipType} = req.body

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }, {
      resource: "EntityRel",
      access: 2
    }
  ]

  try {
    const start1 = new Date()
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    const time1 = new Date() - start1
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const start2 = new Date()
    const entity = await Entity.create(entityDetails)
    const time2 = new Date() - start2
    // await updateEligibleIdsForLoggedInUserFirm(req.user)
    let entityParty1
    let entityParty2

    console.log("entity id : ", entity._id)
    if (["Client", "Prospect", "Third Party"].includes(relationshipType)) {
      entityParty1 = ObjectID(req.user.entityId)
      entityParty2 = ObjectID(entity._id)
    }

    const start3 = new Date()
    const rel = await EntityRel.create({entityParty1, entityParty2, relationshipType})
    const time3 = new Date() - start3
    const start4 = new Date()
    console.log("rel id : ", rel._id)
    await Promise.all([
      returnFromES(req.user.tenantId,entity._id),
      updateEligibleIdsForLoggedInUserFirm(req.user)
    ])
    const time4 = new Date() - start4
    // console.log("time1, 2, 3, 4", time1, time2, time3, time4)
    res.json([entity, time1, time2, time3, time4])
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: resRequested})
  }
}

export const updateEntity = async(req, res) => {
  const {user} = req
  const {entityId} = req.params
  const {type} = req.query
  const {entityDetails, relationshipType} = req.body

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }, {
      resource: "EntityRel",
      access: 2
    }
  ]

  try {
    const start1 = new Date()
    const entitled = await canUserPerformAction(user, resRequested)
    const time1 = new Date() - start1
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const start2 = new Date()
    const entity = await Entity.updateOne({
      _id: entityId
    }, {
      $set: {
        ...entityDetails
      }
    })

    const entityUsers = await EntityUser.find({entityId, userFlags: "Primary Contact"}).select("userFlags")

    if(entityUsers && entityUsers.length){
      const entityUsersIds = entityUsers.map(e => e._id.toString() )
      await EntityUser.updateMany({_id: { $in: entityUsersIds } },   {$pull: {userFlags: "Primary Contact"}})
    }
    const isPrimaryContact = entityDetails && entityDetails.primaryContactId
    if(isPrimaryContact){
      await EntityUser.updateOne({ _id: entityDetails.primaryContactId} , {
        $addToSet: { userFlags: "Primary Contact" }
      })
    }

    const time2 = new Date() - start2
    // await updateEligibleIdsForLoggedInUserFirm(req.user)
    let entityParty1
    let entityParty2

    console.log("entity id : ", entity._id)
    const start3 = new Date()
    if (["Client", "Prospect", "Third Party"].includes(relationshipType)) {
      const rel = await EntityRel.findOneAndUpdate({
        entityParty1: ObjectID(req.user.entityId),
        entityParty2: ObjectID(entityId)
      }, {$set: {
        relationshipType
      }})
    }

    console.log("=======type==========", type)
    if(type === "migrated"){
      console.log("=======entityId==========", entityId)
      await generateConfigForEntities([entityId])
    }
    const time3 = new Date() - start3
    const returnValueEs = await returnFromES(req.user.tenantId, entityId)
    console.log("After inserting into ES ------", returnValueEs)

    const start4 = new Date()
    await updateEligibleIdsForLoggedInUserFirm(req.user)
    const time4 = new Date() - start4
    res.json([entity, time1, time2, time3, time4])
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: resRequested})
  }
}

export const lookupBorrowerObligor = async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {type, searchString} = req.body
  try {
    const typeMatch = type
      ? {
        "$eq": ["$$entity.entityType", type]
      }
      : {
        "$in": [
          "$$entity.entityType",
          ["Governmental Entity / Issuer", "501c3 - Obligor"]
        ]
      }
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchString.replace(invalid, "")
    const regexValue = new RegExp(newSearchString, "i")
    const searchStringLower = newSearchString.toLowerCase()
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const relatedEntities = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: req.user.entityId
        }
      }, {
        $lookup: {
          "from": "entityrels",
          "localField": "entityParty1",
          "foreignField": "entityParty1",
          "as": "rels"
        }
      }, {
        $project: {
          rels: {
            $filter: {
              "input": "$rels",
              "as": "rel",
              "cond": {
                "$ne": ["$$rel.relationshipType", "Self"]
              }
            }
          }
        }
      }, {
        $project: {
          entityParty2: "$rels.entityParty2"
        }
      }, {
        $unwind: "$entityParty2"
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "entities"
        }
      }, {
        $project: {
          entities: {
            $filter: {
              "input": "$entities",
              "as": "entity",
              "cond": {
                $and: [
                  typeMatch, {
                    $gt: [
                      {
                        $indexOfCP: [
                          {
                            $toLower: "$$entity.firmName"
                          },
                          searchStringLower
                        ]
                      },
                      -1
                    ]
                  }
                ]
              }
            }
          }
        }
      }, {
        $unwind: "$entities"
      }, {
        $project: {
          _id: "$entities._id",
          firmName: "$entities.firmName",
          entityType: "$entities.entityType"
        }
      }
    ])
    const entityList = relatedEntities.map(r => r.firmName)
    const globIssuer = await GlobRefEntity
      .find({
        participantType: "Governmental Entity / Issuer",
        firmName: {
          $nin: entityList,
          $regex: regexValue,
          $options: "i"
        }
      })
      .select("firmName _id participantType")
      .limit(25)
    let entities = []
    relatedEntities
      .slice(0, 25)
      .forEach(item => {
        const firm = {
          _id: item._id,
          firmName: item.firmName,
          participantType: item.entityType,
          related: true
        }
        entities.push(firm)
      })
    entities = [
      ...entities,
      ...globIssuer
    ]
    res.json(entities)
  } catch (err) {
    res
      .status(422)
      .send({done: false, error: err, success: "", requestedServices: resRequested})
  }
}

export const lookupThirdParties = async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {searchString} = req.body
  const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
  const newSearchString = searchString.replace(invalid, "")
  const regexValue = new RegExp(newSearchString, "i")
  const searchStringLower = newSearchString.toLowerCase()
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const relatedEntities = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: req.user.entityId
        }
      }, {
        $lookup: {
          "from": "entityrels",
          "localField": "entityParty1",
          "foreignField": "entityParty1",
          "as": "rels"
        }
      }, {
        $project: {
          rels: {
            $filter: {
              "input": "$rels",
              "as": "rel",
              "cond": {
                "$eq": ["$$rel.relationshipType", "Third Party"]
              }
            }
          }
        }
      }, {
        $project: {
          entityParty2: "$rels.entityParty2"
        }
      }, {
        $unwind: "$entityParty2"
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "entities"
        }
      }, {
        $project: {
          entities: {
            $filter: {
              "input": "$entities",
              "as": "entity",
              "cond": {
                $gt: [
                  {
                    $indexOfCP: [
                      {
                        $toLower: "$$entity.firmName"
                      },
                      searchStringLower
                    ]
                  },
                  -1
                ]
              }
            }
          }
        }
      }, {
        $unwind: "$entities"
      }, {
        $project: {
          _id: "$entities._id",
          firmName: "$entities.firmName",
          entityType: "$entities.entityType"
        }
      }
    ])
    const entityList = relatedEntities.map(r => r.firmName)
    const globIssuer = await GlobRefEntity
      .find({
        firmType: "Third Party",
        firmName: {
          $nin: entityList,
          $regex: regexValue,
          $options: "i"
        }
      })
      .select("firmName _id participantType")
      .limit(25)
    let entities = []
    relatedEntities
      .slice(0, 25)
      .forEach(item => {
        const firm = {
          _id: item._id,
          firmName: item.firmName,
          participantType: item.entityType,
          related: true
        }
        entities.push(firm)
      })
    entities = [
      ...entities,
      ...globIssuer
    ]
    res.json(entities)
  } catch (err) {
    res
      .status(422)
      .send({done: false, error: err, success: "", requestedServices: resRequested})
  }
}
