import express from "express"
import entityController from "./entity.controller"
import { getEntities, saveEntity, updateEntity, getEntityById,
  lookupBorrowerObligor, lookupThirdParties } from "./entity.controller.new"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const entityRouter = express.Router()

entityRouter.route("/testdatagrid")
  .get(entityController.getAllTenantUsersForTesting)


entityRouter.route("/firms")
  .get(requireAuth,entityController.getAllFirmList)
  .post(requireAuth,entityController.saveFirmDetail)


entityRouter.route("/firms/:id")
  .get(requireAuth,entityController.getFirmsDetailById)
  .post(requireAuth,entityController.updateFirmLogoDetailById)
  .put(requireAuth,entityController.updateFirmDetailById)

entityRouter.route("/third-parties/:id")
  .get(requireAuth,entityController.getThirdPartyFirmsDetailById)
  .put(requireAuth,entityController.updateThirdPartyFirmDetailById)

entityRouter.route("/third-parties/entityId/:entityId")
  .get(requireAuth,entityController.getAllThirdPartyFirmList)
  .post(requireAuth,entityController.saveThirdPartyFirmDetail)

entityRouter.route("/clients/deleteLinkCusipBorrower")
  .post(requireAuth,entityController.deleteLinkCusipBorrower)

entityRouter.route("/clients/:id")
  .get(requireAuth,entityController.getClientsFirmsDetailById)
  .put(requireAuth,entityController.updateClientsFirmDetailById)

entityRouter.route("/clients/entityId/:entityId")
  .get(requireAuth,entityController.getAllClientsFirmList)
  .post(requireAuth,entityController.saveClientsFirmDetail)

entityRouter.route("/entityprospect")
  .post(requireAuth,entityController.getAllClientsPropectList)

entityRouter.route("/entity-third-parties")
  .post(requireAuth,entityController.getAllEntityThirdPartyList)

entityRouter.route("/checkduplicateEntity")
  .post(requireAuth, entityController.checkDuplicateEntity)

entityRouter.route("/searchfirm/:firmName")
  .get(requireAuth,entityController.searchFirm)
entityRouter.route("/business-activity")
  .post(requireAuth, entityController.getAllBusinessActivity)
// entityRouter.param("id", entityController.findByParam)

entityRouter.route("/firmAddOns/:entityId")
  .put(requireAuth,entityController.updateFirmAddOns)

entityRouter.route("/allEnt")
  .get(requireAuth,entityController.getAllEntity)

entityRouter.route("/allTenEnt")
  .get(requireAuth,entityController.getAllFirmList)

entityRouter.route("/getUsersByEnt/:ids")
  .get(requireAuth, entityController.getUsersByEnt)

entityRouter.route("/getSeries50/:ids")
  .get(requireAuth, entityController.getSeries50)

entityRouter.route("/getSeries50ForTesting")
  .get(requireAuth, entityController.getSeries50ForLoadTesting)

entityRouter.route("/getEntByRel/:entId")
  .get(requireAuth, entityController.getEntitiesByRes)

entityRouter.route("/getEntNotEqSelf/:entId")
  .get(requireAuth, entityController.getEntNotEqSelf)

entityRouter.route("/borrowerObligor/:entityId")
  .get(requireAuth, entityController.getBorrowerObligor)

entityRouter.route("/context/:contextname")
  .get(requireAuth,entityController.getAllUserPrefForContext)
  .post(requireAuth,entityController.savePrefrences)
entityRouter.route("/deleteprefsearch")
  .post(requireAuth,entityController.deletePrefSearch)

entityRouter.route("/changeAddressStatus")
  .post(requireAuth,entityController.changeAddressStatus)
entityRouter.route("/docs/:entId")
  .get(requireAuth,entityController.getDocuments)
  .put(requireAuth,entityController.updateDocumentStatus)
  .post(requireAuth,entityController.saveDocuments)
  .delete(requireAuth, entityController.deleteDocuments)

entityRouter.route("/issuers")
  .post(requireAuth, entityController.getGlobRefEntity)
  .put(requireAuth, entityController.postGlobIssuer)

entityRouter.route("/allIssuers")
  .post(requireAuth, entityController.getGlobalRefIssuer)

entityRouter.route("/globissuer")
  .post(requireAuth, entityController.postGlobIssuer)

entityRouter.route("/:id")
  .get(requireAuth, entityController.getOne)
  .put(requireAuth, entityController.updateOne)
  .delete(requireAuth, entityController.createOne)

entityRouter.route("/activecontracts/:entityId")
  .get(requireAuth,entityController.getActiveContracts)

entityRouter.route("/contracts/:entityId")
  .get(requireAuth,entityController.getContracts)
  .put(requireAuth,entityController.putContracts)
  .delete(requireAuth,entityController.deleteContracts)

entityRouter.route("/contracts/document/:entityId")
  .delete(requireAuth,entityController.pullContractsDocument)

entityRouter.route("/auditlog/:contractId")
  .post(requireAuth, entityController.putAuditLog)

entityRouter.route("/entity-look-up")
  .post(requireAuth, entityController.entityLookUp)

entityRouter.route("/entities")
  .get(requireAuth, getEntities)
  .post(requireAuth, saveEntity)

entityRouter.route("/entities/:entityId")
  .get(requireAuth, getEntityById)
  .post(requireAuth, updateEntity)

entityRouter.route("/borrower-look-up")
  .post(requireAuth, lookupBorrowerObligor)

entityRouter.route("/third-party-look-up")
  .post(requireAuth, lookupThirdParties)
