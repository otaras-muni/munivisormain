import {generateControllers} from "../../modules/query"
import {Entity} from "./entity.model"
import {EntityRel} from "../entityRel/entityRes.model"
import {SearchPref} from "./../search/searchpref.model"
import {EntityUser} from "../entityUser/entityUser.model"
import {GlobRefEntity} from "./globalRefEntity.model"
import {canUserPerformAction, isUserPlatformAdmin} from "../../commonDbFunctions"
import {getTransactionsForSpecificEntity} from "./../../commonDbFunctions/getEntityTransactionsForListing"
import {getFaEntityGraphInfo} from "./../../commonDbFunctions/loggedInUserDetails"
import {validateFirmDetail} from "../../Validations/firmDetail"
import {validatClientsDetail} from "../../Validations/clients"
import {validateThirdPartyDetail} from "../../Validations/thirdParty"
import {getAllEntityAndUserDetailsForTenantUser} from "./../../commonDbFunctions/getAllUserDetailsForTenantUser"
import {getAllEligibleIds, doesIdExistsInEntitledEntityIds, getAllEligibleUserIds, getAllEligibleIDsForSpecificType, updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import moment from "moment"
import {Others, RFP} from "../models"
import {putData, getData, elasticSearchUpdateFunction} from "./../../elasticsearch/esHelper"
import {getAllAccessAndEntitlementsForLoggedInUser} from "../../entitlementhelpers"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Entity, _id: id, esType: "entities"})

const returnFromESRel = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: EntityRel, _id: id, esType: "entityrels"})

const fs = require("fs")
const path = require("path")
const {ObjectID} = require("mongodb")

const getFirmDetailById = () => (req, res, next) => {
  try {
    const {id} = req.params
    return Entity.findById(id)

  } catch (error) {
    return (error)
  }
}

const updateFirmDetailsById = () => async(req, res, next) => {
  try {
    const {firmDetails} = req.body
    const docToUpdate = req.docFromId
    const {_id} = docToUpdate
    const errorData = validateFirmDetail(firmDetails)
    if (errorData.error) {
      res.json(errorData)
    } else {
      let allowed = false
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowed = await doesIdExistsInEntitledEntityIds(req.user, req.params.id, "edit")
      } else {
        allowed = true
      }

      if (allowed) {
        const getAllFirmDetails = await getFirmDetailById(req, res)
        if (getAllFirmDetails) {
          const sortedData = firmDetails
            .addresses
            .sort((a, b) => a._id > b._id)
          getAllFirmDetails.addresses = getAllFirmDetails
            .addresses
            .filter((obj) => obj._id != sortedData[0]._id)
          firmDetails.addresses = [
            ...getAllFirmDetails.addresses,
            ...sortedData
          ]
          const result = await Entity.findByIdAndUpdate(_id, firmDetails, {
            new: true,
            upsert: true
          })
          await updateEligibleIdsForLoggedInUserFirm(req.user)
          res.json(result)
        } else {
          const result = await Entity.findByIdAndUpdate(_id, firmDetails, {
            new: true,
            upsert: true
          })
          await updateEligibleIdsForLoggedInUserFirm(req.user)
          res
            .status(201)
            .json(result)
        }
      } else {
        console.log(`NEW ENTITLEMENT SERVICE>>> ALLOWED TO ACCESS ${req.params.id}`)
        res
          .status(500)
          .json({error: "Not eligible to edit firm ID"})
      }
    }
  } catch (ex) {
    console.log("Ex===>>>", ex)
  }
}

const searchFirm = () => async(req, res, next) => {
  const {firmName} = req.params
  try {
    const result = await Entity
      .find({
        firmName: {
          $regex: firmName,
          $options: "i"
        }
      })
      .select("_id, firmName")
    res.json(result)
  } catch (ex) {
    res.send(ex)
  }

}
const getAllFirmList = () => async(req, res) => {
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const pAdmin = await isUserPlatformAdmin(user)
    let result
    if (!pAdmin) {
      const eligibleIds = await getAllEligibleIds(user, "all")
      result = await Entity.find({
        isMuniVisorClient: true,
        _id: {
          $in: [...eligibleIds]
        }
      })
    } else {
      result = await Entity.find({isMuniVisorClient: true})
    }

    result = JSON
      .parse(JSON.stringify(result))
      .filter((elem, index, self) => self.findIndex((t) => (t._id === elem._id)) === index)
    res.json(result)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getFirmsDetailById = () => async(req, res, next) => {
  const {id} = req.params
  const pAdmin = await isUserPlatformAdmin(req.user)
  let allowedAccessId = false
  if (!pAdmin) {
    allowedAccessId = await doesIdExistsInEntitledEntityIds(req.user, req.params.id, "view")
  } else {
    allowedAccessId = true
  }
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    /* if( !entitled || !allowedAccessId) {
      res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    } */
    const result = await Entity
      .findById(id)
      .select("_id entityAliases isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmN" +
          "ame taxId businessStructure numEmployees annualRevenue addresses firmAddOns sett" +
          "ings")
    res.json(result)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveFirmDetail = () => async(req, res, next) => {
  const {user} = req
  const {firmDetails} = req.body
  const {_id} = firmDetails

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }, {
      resource: "EntityRel",
      access: 2
    }
  ]
  try {
    let allowedId = false
    if (_id) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, _id, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    const resRequested = [
      {
        resource: "Entity",
        access: 2
      }, {
        resource: "EntityRel",
        access: 2
      }
    ]
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let {addresses} = firmDetails
    addresses = addresses.filter(item => item.isPrimary)
    const errorData = validateFirmDetail(firmDetails)
    if (errorData.error || addresses.length !== 1) {
      if (addresses.length === 1)
        errorData.isPrimaryAddress = true
      else
        errorData.isPrimaryAddress = false
      await returnFromES(req.user.tenantId, _id)
      res.json(errorData)
    } else {
      firmDetails._id = ObjectID()
      const entity = await Entity.create(firmDetails)
      await returnFromES(req.user.tenantId, _id)

      // await updateEligibleIdsForLoggedInUserFirm(req.user)
      if (entity._id) {
        EntityRel.create({
          "_id": ObjectID(),
          "entityParty1": ObjectID(entity._id),
          "entityParty2": ObjectID(entity._id),
          "relationshipType": "Self"
        }, async(err, result) => {
          if (err) {
            throw new Error(err)
          }
          await returnFromESRel(result._id)
          await updateEligibleIdsForLoggedInUserFirm(req.user)
          res.json(entity)
        })

      }
    }
  } catch (e) {

    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateFirmDetailById = () => async(req, res, next) => {
  const {user} = req
  const {firmDetails, mergeAddresses} = req.body
  const {id} = req.params
  const allowedId = true
  try {
    const resRequested = [
      {
        resource: "Entity",
        access: 2
      }
    ]
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let allowedId = false
    if (id) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, id, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    let {addresses} = firmDetails
    addresses = mergeAddresses.filter(item => item.isPrimary)
    const errorData = validateFirmDetail(firmDetails)
    if (errorData.error || addresses.length !== 1) {
      let isPrimaryAddress
      if (addresses.length === 1)
        isPrimaryAddress = true
      else
        isPrimaryAddress = false

      await returnFromES(req.user.tenantId, id)
      res.json({done: false, error: errorData.error, success: "", isPrimaryAddress, requestedServices: resRequested})
    } else {
      firmDetails.addresses = mergeAddresses
      const result = await Entity
        .findByIdAndUpdate(id, firmDetails, {
          new: true,
          upsert: true
        })
        .select("_id entityAliases isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmN" +
            "ame taxId businessStructure numEmployees annualRevenue addresses")
      await returnFromES(req.user.tenantId, id)
      await updateEligibleIdsForLoggedInUserFirm(req.user)
      res.json({done: true, error: errorData.error, success: result, isPrimaryAddress: true, requestedServices: resRequested})
    }
  } catch (e) {
    console.log("Error", e)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateFirmLogoDetailById = () => async(req, res) => {
  const {user} = req
  const {firmDetails, mergeAddresses} = req.body
  const {id} = req.params
  const allowedId = true
  try {
    const resRequested = [
      {
        resource: "Entity",
        access: 2
      }
    ]
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let allowedId = false
    if (id) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, id, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    const result = await Entity.update({
      _id: id
    }, {$set: req.body}).select("_id entityAliases isMuniVisorClient msrbFirmName msrbRegistrantType msrbId firmN" +
        "ame taxId businessStructure numEmployees annualRevenue addresses settings")
    await returnFromES(req.user.tenantId, id)
    await updateEligibleIdsForLoggedInUserFirm(req.user)

    res.json({done: true, success: result})
  } catch (e) {
    console.log("Error", e)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: ""})
  }
}

const getAllThirdPartyFirmList = () => async(req, res, next) => {
  const {entityId} = req.params
  const {user} = req

  const queryStages = [
    {
      $match: {
        relationshipType: "Third Party",
        entityParty1: ObjectID(entityId)
      }
    }, {
      $project: {
        "entityParty1": 1,
        "entityParty2": 1
      }
    }, {
      "$lookup": {
        "from": "entities",
        "localField": "entityParty2",
        "foreignField": "_id",
        "as": "firmDetails"
      }
    }, {
      $unwind: "$firmDetails"
    }, {
      $project: {
        "_id": "$firmDetails._id",
        "firmName": "$firmDetails.firmName"
      }
    }
  ]

  const pAdmin = await isUserPlatformAdmin(req.user)
  let allowedId = false
  let allEligibleIds = []
  let revisedQueryStages = []

  if (!pAdmin) {
    allowedId = await doesIdExistsInEntitledEntityIds(req.user, entityId, "all")
    allEligibleIds = await getAllEligibleIds(req.user, "all")
    revisedQueryStages = [
      ...queryStages, {
        "$match": {
          "_id": {
            "$in": [...allEligibleIds]
          }
        }
      }
    ]
  } else {
    allowedId = true
    revisedQueryStages = [...queryStages]
  }
  if (!allowedId) {
    res
      .status(500)
      .send({error: "Not Authorized to access Entity"})
  }

  if (!allowedId) {
    res
      .status(500)
      .send({done: false, error: `Not Authorized to access Entity ${entityId}`, success: ""})
  }
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }, {
      resource: "EntityRel",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    EntityRel.aggregate([...revisedQueryStages], (err, result) => {
      if (err)
        throw new Error(err)
      res.json(result)
    })
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getThirdPartyFirmsDetailById = () => async(req, res) => {
  const {user} = req
  const {id} = req.params
  let allowedIdAccess = false
  if (id) {
    const pAdmin = await isUserPlatformAdmin(req.user)
    if (!pAdmin) {
      allowedIdAccess = await doesIdExistsInEntitledEntityIds(req.user, req.params.id, "all")
    } else {
      allowedIdAccess = true
    }
  }

  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    if (!allowedIdAccess) {
      res
        .status(500)
        .send({done: false, error: "Not allowed to access the requested Entity"})
    }
    if (id.length > 24) {
      res.status(422)({done: false, error: "Invalid Entity Id", success: "", requestedServices: resRequested})
    }
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const result = await Entity
      .findById(id)
      .select("_id entityAliases entityFlags.marketRole isMuniVisorClient msrbFirmName msrbRegi" +
          "strantType msrbId firmName taxId businessStructure numEmployees annualRevenue ad" +
          "dresses")
    res.json(result)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}
const saveThirdPartyFirmDetail = () => async(req, res) => {
  const {entityId} = req.params
  const {user} = req
  const {firmDetails, clientType} = req.body
  console.log("=======389===============>", req.body)
  // const resRequested = [   {resource:"Entity",access:2},
  // {resource:"EntityRel",access:2} ]

  try {
    // const entitled = await canUserPerformAction(user,resRequested)
    const {_id} = firmDetails
    let allowedId = false

    if (_id) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, _id, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    console.log("WHAT WAS RETURNED FROM THE SERVICE",
    /* entitled */)
    // if( !entitled) {   res.status(422).send({done:false,error:"User is not
    // entitled to access the requested
    // services",success:"",requestedServices:resRequested}) }
    console.log("=======417===============>", firmDetails)

    // let {addresses} = firmDetails addresses  =
    // addresses.filter(item=>item.isPrimary) const errorData =
    // validateThirdPartyDetail(firmDetails) if (errorData.error ||
    // addresses.length!==1) {   if(addresses.length===1)
    // errorData.isPrimaryAddress=true   else     errorData.isPrimaryAddress=false
    // res.json(errorData) }else{
    firmDetails._id = ObjectID()
    const entity = await Entity.create({
      ...firmDetails
    })
    await returnFromES(req.user.tenantId, entity._id)
    if (entity._id) {
      EntityRel.create({
        "_id": ObjectID(),
        "entityParty1": ObjectID(entityId),
        "entityParty2": ObjectID(entity._id),
        "relationshipType": clientType
      }, async(err, result) => {
        if (err) {
          throw new Error(err)
        }
        console.log("BEFORE UPDATE OF ENTITLEMENTS - THIRD PARTY SAVE")
        await returnFromESRel(result._id)
        await updateEligibleIdsForLoggedInUserFirm(req.user)

        console.log("POST UPDATE OF ENTITLEMENTS - THIRD PARTY SAVE")
        res.json(entity)
      })
    }
    // }
  } catch (e) {
    res
      .status(422)
      .send({
        done: false, error: "There is a failure in retrieving information for entitlements", success: "",
        /* requestedServices: resRequested */
      })
  }
}

const updateThirdPartyFirmDetailById = () => async(req, res, next) => {
  const {user} = req
  const {firmDetails, mergeAddresses} = req.body
  console.log("==========460==", firmDetails, "=================>", mergeAddresses)
  const {id} = req.params

  let allowedIdAccess = false

  if (id) {
    const pAdmin = await isUserPlatformAdmin(req.user)
    if (!pAdmin) {
      allowedIdAccess = await doesIdExistsInEntitledEntityIds(req.user, id, "edit")
    } else {
      allowedIdAccess = true
    }
    if (!allowedIdAccess) {
      res
        .status(500)
        .send({error: "Not Authorized to access third party entity for updates"})
    }
  }

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    if (!allowedIdAccess) {
      res
        .status(500)
        .send({done: false, error: "Not allowed to edit details of the requested Entity"})
    }
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const {addresses} = firmDetails
    // addresses  = mergeAddresses && mergeAddresses.filter(item=>item.isPrimary)
    const errorData = validateThirdPartyDetail(firmDetails)
    if (errorData.error/* || addresses && addresses.length!==1 */) {
      /* let isPrimaryAddress
      if(addresses && addresses.length===1)
        isPrimaryAddress=true
      else
        isPrimaryAddress=false */
      await returnFromES(req.user.tenantId, id)
      res.json({
        done: false, error: errorData.error, success: "",
        /* isPrimaryAddress, */
        requestedServices: resRequested
      })
    } else {
      firmDetails.addresses = mergeAddresses
      const result = await Entity
        .findByIdAndUpdate(id, firmDetails, {
          new: true,
          upsert: true
        })
        .select("_id entityAliases entityFlags.marketRole isMuniVisorClient msrbFirmName msrbRegi" +
            "strantType msrbId firmName taxId businessStructure numEmployees annualRevenue ad" +
            "dresses")
      await updateEligibleIdsForLoggedInUserFirm(req.user)
      await returnFromES(req.user.tenantId, id)
      res.json({done: true, error: errorData.error, success: result, isPrimaryAddress: true, requestedServices: resRequested})
    }
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getAllClientsFirmList = () => async(req, res, next) => {
  const {user} = req
  const {entityId} = req.params
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }, {
      resource: "EntityRel",
      access: 1
    }
  ]
  try {
    const allEligibleIds = await getAllEligibleIds(req.user, "all")
    const entitled = await canUserPerformAction(user, resRequested)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    EntityRel.aggregate([
      {
        $match: {
          relationshipType: "Client",
          entityParty1: ObjectID(entityId)
        }
      }, {
        $project: {
          "entityParty1": 1,
          "entityParty2": 1
        }
      }, {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "firmDetails"
        }
      }, {
        $unwind: "$firmDetails"
      }, {
        $project: {
          "_id": "$firmDetails._id",
          "firmName": "$firmDetails.firmName"
        }
      }, {
        "$match": {
          "_id": {
            "$in": [...allEligibleIds]
          }
        }
      }
    ], (err, result) => {
      if (err)
        throw new Error(err)
      res.json(result)
    })
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getClientsFirmsDetailById = () => async(req, res, next) => {
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const {id} = req.params
    const pAdmin = await isUserPlatformAdmin(req.user)
    let allowedIdAccess = false
    if (!pAdmin) {
      allowedIdAccess = await doesIdExistsInEntitledEntityIds(req.user, id, "all")
    } else {
      allowedIdAccess = true
    }

    if (!allowedIdAccess) {
      res
        .status(500)
        .send({done: false, error: "Not allowed to get details of the Entity"})
    }
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const result = await Entity
      .findById(id)
      .select("_id entityAliases entityFlags.issuerFlags isMuniVisorClient msrbFirmName msrbReg" +
          "istrantType msrbId firmName taxId primarySectors secondarySectors firmLeadAdviso" +
          "r prevAdvisorFirm prevLeadAdvisor primaryContactEmail primaryContactNameInEmma p" +
          "rimaryContactPhone prevAdvisorContractExpire addresses entityLinkedCusips entity" +
          "BorObl entityType")
    res.json(result)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}
const saveClientsFirmDetail = () => async(req, res, next) => {
  const {entityId} = req.params
  const {user} = req
  const {firmDetails, clientType} = req.body
  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }, {
      resource: "EntityRel",
      access: 2
    }
  ]

  try {
    const entitled = await canUserPerformAction(user, resRequested)

    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const {_id} = firmDetails
    let allowedId = false
    if (_id) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, _id, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    // let {addresses} = firmDetails addresses  =
    // addresses.filter(item=>item.isPrimary) const errorData =
    // validatClientsDetail(firmDetails) if (errorData.error ||
    // addresses.length!==1) {   if(addresses.length===1)
    // errorData.isPrimaryAddress=true   else     errorData.isPrimaryAddress=false
    // res.json(errorData) }else{
    firmDetails._id = ObjectID()
    const entity = await Entity.create(firmDetails)
    await returnFromES(req.user.tenantId, entity._id)

    if (entity._id) {
      EntityRel.create({
        "_id": ObjectID(),
        "entityParty1": ObjectID(entityId),
        "entityParty2": ObjectID(entity._id),
        "relationshipType": clientType
      }, async(err, result) => {
        if (err) {
          throw new Error(err)
        }
        console.log("BEFORE UPDATE OF ENTITLEMENTS - CLIENT SAVE")
        await returnFromESRel(result._id)
        await updateEligibleIdsForLoggedInUserFirm(req.user)
        console.log("POST UPDATE OF ENTITLEMENTS - CLIENT SAVE")
        res.json(entity)
      })
    }
    // }
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateClientsFirmDetailById = () => async(req, res, next) => {
  const {user} = req
  const {
    firmDetails, mergeAddresses,
    /* mergeEntityLinkedCusips, mergeEntityBorObl */
  } = req.body
  console.log("=====================firmDetails====================>", firmDetails)
  const {id} = req.params
  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let allowedId = false
    if (id) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, id, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    // let {addresses} = firmDetails addresses  = mergeAddresses &&
    // mergeAddresses.filter(item=>item.isPrimary)
    const errorData = validatClientsDetail(firmDetails)
    if (errorData.error/* || addresses && addresses.length !==1 */) {
      /* let isPrimaryAddress
      if(addresses && addresses.length===1)
        isPrimaryAddress=true
      else
        isPrimaryAddress=false */
      await returnFromES(req.user.tenantId, id)

      res.json({
        done: false, error: errorData.error, success: "",
        /* ,isPrimaryAddress */
        requestedServices: resRequested
      })
    } else {
      firmDetails.addresses = mergeAddresses
      /* firmDetails.entityLinkedCusips = mergeEntityLinkedCusips
      firmDetails.entityBorObl = mergeEntityBorObl */
      const result = await Entity
        .findByIdAndUpdate(id, firmDetails, {
          new: true,
          upsert: true
        })
        .select("_id entityAliases entityFlags.issuerFlags isMuniVisorClient msrbFirmName msrbReg" +
            "istrantType msrbId firmName taxId businessStructure  annualRevenue addresses ent" +
            "ityLinkedCusips entityBorObl")
      console.log("BEFORE UPDATE OF ENTITLEMENTS - CLIENT SAVE")
      await updateEligibleIdsForLoggedInUserFirm(req.user)
      await returnFromES(req.user.tenantId, id)
      console.log("POST UPDATE OF ENTITLEMENTS - CLIENT SAVE")

      res.json({done: true, error: errorData.error, success: result, isPrimaryAddress: true, requestedServices: resRequested})
    }
  } catch (e) {
    console.log("====================e==================>", e)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getEntitiesByRes = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]

  try {
    const {type} = req.query
    const relationshipTypes = ["Client", "Prospect"]
    if (type) {
      relationshipTypes.push(type)
    }

    let matchParams = [{$match:{"alltenantentities.relationshipType": { $in : relationshipTypes }}}]
    if ( req.user.checkEntitlement) {
      const detailsIds = await getAllAccessAndEntitlementsForLoggedInUser(req.user, "nav")
      matchParams = [{
        $match: {
          $and:[
            {"alltenantentities.entityParty1": { $in : ((detailsIds && detailsIds.edit) || []).map(id => ObjectID(id)) }},
            {"alltenantentities.relationshipType": { $in : relationshipTypes }}
          ]
        }
      }]
    }

    const entitled = await canUserPerformAction(req.user,resRequested)
    if( !entitled) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    const issuerEntities = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: ObjectID(req.user.entityId)
        }
      }, {
        $lookup: {
          from: "entityrels",
          localField: "entityParty1",
          foreignField: "entityParty1",
          as: "alltenantentities"
        }
      }, {
        $unwind: "$alltenantentities"
      },
      ...matchParams,
      {
        $project: {
          entityId: "$alltenantentities.entityParty2",
          relType: "$alltenantentities.relationshipType"
        }
      }, {
        $lookup: {
          from: "entities",
          localField: "entityId",
          foreignField: "_id",
          as: "allentitydetails"
        }
      }, {
        $unwind: "$allentitydetails"
      }, {
        $project: {
          _id: "$allentitydetails._id",
          firmName: "$allentitydetails.firmName",
          msrbRegistrantType: "$allentitydetails.msrbRegistrantType",
          msrbFirmName: "$allentitydetails.msrbFirmName",
          primarySector: "$allentitydetails.primarySectors",
          secondarySector: "$allentitydetails.secondarySectors",
          addresses: "$allentitydetails.addresses",
          relType: 1
        }
      }
    ])

    // const resEntity = await EntityRel.find({   entityParty1: entityId,
    // relationshipType: { $in : relationshipTypes } }).select("entityParty1
    // entityParty2 relationshipType") const issuerIds = resEntity.map(p =>
    // p.entityParty2) const issuerEntities = await Entity.find({   _id: {     $in:
    // issuerIds   } }).select("firmName msrbRegistrantType msrbFirmName")
    issuerEntities.forEach(adv => {
      if (adv && adv.addresses && adv.addresses.length) {
        let address = adv.addresses.find(addr => addr.isPrimary)
        address = address || adv.addresses[0]
        adv.addresses = address || {}
      }else {
        adv.addresses = {}
      }
    })
    res.json(issuerEntities)
  } catch (error) {
    console.log("The Error is", error)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const getUsersByEnt = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const {relationshipType} = req.user

    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const ids = req
      .params
      .ids
      .split(",")

    let finalAllEligibleEntities = [...ids.map(k => ObjectID(k))]

    if (relationshipType !== "Self") {
      const allEligibleEntityIds = await getAllEligibleIDsForSpecificType(req.user, "entities", "edit")
      finalAllEligibleEntities = allEligibleEntityIds.reduce((acc, k) => {
        const ret = ids.filter(el => el.toString() === k.toString())
        return [
          ...acc,
          ...ret
        ]
      }, [])
    }

    // console.log("GETTING ALL ELIGIBLE IDS - ", finalAllEligibleEntities) const
    // entitledUserIds = ids.filter( id =>
    // allEligibleEntityIds.includes(ObjectID(id)) )
    const entitiesUsers = await EntityUser
      .find({
        entityId: {
          $in: finalAllEligibleEntities
        }
      })
      .select("userFirstName userMiddleName userLastName userRole userFlags userPhone userEmail" +
          "s entityId userAddresses userFirmName userFax")
    res.json(entitiesUsers)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getSeries50 = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const ids = req
      .params
      .ids
      .split(",")
    const {relationshipType, userEntitlement} = req.user

    let finalAllEligibleEntities = []
    if (relationshipType === "Self" && ["global-edit", "global-readonly", "global-readonly-tran-edit"].includes(userEntitlement)) {
      finalAllEligibleEntities = ids.map(el => ObjectID(el))
    } else {
      const allEligibleEntityIds = await getAllEligibleIDsForSpecificType(req.user, "entities", "edit")
      finalAllEligibleEntities = allEligibleEntityIds.reduce((acc, k) => {
        const ret = ids.filter(el => el.toString() === k.toString())
        return [
          ...acc,
          ...ret
        ]
      }, [])
    }

    const type = ["Series 50"]
    console.log("The entities for which info is being obtained", finalAllEligibleEntities)
    const series50Advisors = await EntityUser.aggregate([
      {
        $match:{
          $and: [
            {
              entityId: {
                $in: finalAllEligibleEntities
              }
            }
          ]
        }
      },
      {
        $unwind:"$userFlags"
      },
      {
        $addFields:{
          userRevisedStatus:{$ifNull:["$userStatus","active"]},
        }
      },
      {
        $match: { userRevisedStatus: "active",userFlags:{$in:type}}
      },
      {
        $project:{
          userFirstName:"$userFirstName",
          userMiddleName:"userMiddleName",
          userLastName:"$userLastName",
          userEmails:"$userEmails",
          entityId :"$entityId",
          userFirmName :"$userFirmName",
          userPhone :"$userPhone",
          userFax :"$userFax",
          userStatus :"$userRevisedStatus",
          userAddresses:"$userAddresses",
          userFlags:"$userFlags"
        }
      }

    ])

    series50Advisors.forEach(adv => {
      if (adv && adv.userAddresses && adv.userAddresses.length) {
        let address = adv.userAddresses.find(addr => addr.isPrimary)
        address = address || adv.userAddresses[0]
        adv.userAddresses = address || {}
      }else {
        adv.userAddresses = {}
      }
    })

    res.json(series50Advisors)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getSeries50ForLoadTesting = () => async(req, res) => {
  try {
    const type = ["Series 50"]
    const series50Users = await EntityUser.find({
      $and: [
        {
          entityId: {
            $in: [req.user.entityId]
          }
        }, {
          userFlags: {
            $in: type
          }
        }
      ]
    }).select("userFirstName userMiddleName userLastName userEmails entityId userFirmName userPhone userAddresses")

    if (series50Users && series50Users.length > 0) {
      res.json(series50Users[Math.floor(Math.random() * (series50Users.length))])
    }
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "Cannot retrieve series 50 information"})
  }
}

const saveEntity = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const {firmDetails} = req.body
    const errorData = validateFirmDetail(firmDetails)
    if (errorData.error) {
      throw new Error(errorData.error)
    } else {
      const entity = await Entity.create(firmDetails)
      const returnValueFromES = await returnFromES(req.user.tenantId, entity._id)
      console.log("The following")

      EntityRel.insert({
        "entityParty1": ObjectID("5b1a63bd471a1919e0ca97d4"),
        "entityParty2": ObjectID(entity._id),
        "relationshipType": "Client"
      }, async(err, result) => {
        if (err)
          throw new Error(err)
        await returnFormESRel(result._id)
        res.json(entity)
      })

    }
  } catch (ex) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const deleteLinkCusipBorrower = () => async(req, res) => {
  const {user} = req

  let allowedId = false
  if (req.body.entityId) {
    const pAdmin = await isUserPlatformAdmin(req.user)
    if (!pAdmin) {
      allowedId = await doesIdExistsInEntitledEntityIds(req.user, req.body.entityId, "edit")
    } else {
      allowedId = true
    }
    if (!allowedId) {
      res
        .status(500)
        .send({error: "Not Authorized to access Entity"})
    }
  }

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let result
    if (req.body.type === "entityBorObl") {
      result = await Entity.update({
        _id: ObjectID(req.body.entityId)
      }, {
        $pull: {
          "entityBorObl": {
            "_id": ObjectID(req.body.docId)
          }
        }
      })
    } else {
      result = await Entity.update({
        _id: ObjectID(req.body.entityId)
      }, {
        $pull: {
          "entityLinkedCusips": {
            "_id": ObjectID(req.body.docId)
          }
        }
      })
    }
    await returnFromES(req.user.tenantId, req.body.entityId)
    res.send(result)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getAllClientsPropectList = () => async(req, res) => {
  const {entityId, filteredValue} = req.body
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }, {
      resource: "EntityRel",
      access: 1
    }
  ]

  const searchBy = [
    {
      "$or": [
        {
          "firmDetails.addresses.isPrimary": true
        }, {
          "firmDetails.addresses.isPrimary": false
        }
      ]
    }
  ]

  const queryStages = (ids, mq) => [
    {
      $match: {
        ...mq
      }
    }, {
      $project: {
        "entityParty1": 1,
        "entityParty2": 1,
        "relationshipType": 1
      }
    }, {
      "$lookup": {
        "from": "entities",
        "localField": "entityParty2",
        "foreignField": "_id",
        "as": "firmDetails"
      }
    }, {
      $unwind: "$firmDetails"
    }, {
      "$match": {
        "firmDetails._id": {
          "$in": [...ids]
        }
      }
    }, {
      $unwind: "$firmDetails.addresses"
    }, {
      $match: {
        $and: searchBy
      }
    }
  ]

  try {
    let allEligibleIds = []
    const matchQueryPlatformAdmin = {
      $or: [
        {
          relationshipType: "Client"
        }, {
          relationshipType: "Prospect"
        }
      ]
    }

    let finalQueryStages = [...queryStages(allEligibleIds, matchQueryPlatformAdmin)]
    const pAdmin = await isUserPlatformAdmin(user)
    if (!pAdmin) {
      console.log("Not platform Admin")
      allEligibleIds = await getAllEligibleIds(req.user, "all")
      console.log("The elgigible IDs are", allEligibleIds)
      const matchQuery = {
        $or: [
          {
            relationshipType: "Client",
            entityParty1: ObjectID(entityId)
          }, {
            relationshipType: "Prospect",
            entityParty1: ObjectID(entityId)
          }
        ]
      }
      finalQueryStages = queryStages(allEligibleIds, matchQuery)
    } else {
      finalQueryStages.splice(4, 1)
    }

    if (filteredValue !== "") {
      if (filteredValue.entitySearch !== "") {
        const regexValue = new RegExp(filteredValue.entitySearch, "i")
        searchBy.push({
          "$or": [
            {
              "relationshipType": {
                $regex: regexValue,
                $options: "i"
              }
            }, {
              "firmDetails.firmName": {
                $regex: regexValue,
                $options: "i"
              }
            }, {
              "firmDetails.addresses.state": {
                $regex: regexValue,
                $options: "i"
              }
            }, {
              "firmDetails.addresses.city": {
                $regex: regexValue,
                $options: "i"
              }
            }, {
              "firmDetails.addresses.addressName": {
                $regex: regexValue,
                $options: "i"
              }
            }, {
              "firmDetails.addresses.officePhone[0].phoneNumber": {
                $regex: regexValue,
                $options: "i"
              }
            }
          ]
        })
      }
      if (filteredValue.entityType !== "ClentProspect" && filteredValue.entityType !== "")
        searchBy.push({"relationshipType": filteredValue.entityType})
      if (filteredValue.enityState && filteredValue.enityState.length > 0)
        searchBy.push({
          "firmDetails.addresses.state": {
            $in: filteredValue.enityState
          }
        })
    }
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    EntityRel.aggregate([...finalQueryStages], (err, result) => {
      if (err)
        throw new Error(err)
      result = result.map(async(item, idx) => {
        const data = {}
        const primaryContactName = await EntityUser.find({
          $and: [
            {
              entityId: item.firmDetails._id
            }, {
              "userFlags": "Primary Contact"
            }
          ]
        }).select("_id userFirstName userMiddleName userLastName userAddresses")
        let primaryAddressDisplay
        let primaryContactUserId
        if (primaryContactName.length > 0) {
          const contactAddresses = primaryContactName[0].userAddresses
          primaryContactUserId = primaryContactName[0]._id
          const filteredPrimaryContactAddress = contactAddresses.filter(a => a.isPrimary)
          const primaryContactAddress = filteredPrimaryContactAddress[0]
          primaryAddressDisplay = `${primaryContactAddress.addressLine1} ${primaryContactAddress.addressLine2}, ${primaryContactAddress.city} ${primaryContactAddress.state}`
        }

        data.firmId = item.firmDetails._id
        data.firmType = item.relationshipType
        data.firmName = item.firmDetails.firmName
        data.state = item.firmDetails.addresses.state
        data.city = item.firmDetails.addresses.city
        data.country = item.firmDetails.addresses.country
        data.primaryContactName = primaryContactName.length > 0
          ? `${primaryContactName[0].userFirstName} ${primaryContactName[0].userMiddleName} ${primaryContactName[0].userLastName}`
          : ""
        data.primaryContactUserId = primaryContactUserId
        data.addressName = primaryContactName.length > 0
          ? `${primaryContactName[0].userFirstName} ${primaryContactName[0].userMiddleName} ${primaryContactName[0].userLastName}`
          : ""
        data.primaryContactAddress = primaryAddressDisplay
        data.primaryContact = item.firmDetails.addresses.officePhone[0].phoneNumber
        if (item.firmDetails.addresses.isPrimary)
          return data
        return data
      })

      Promise
        .all(result)
        .then((res1) => {
          res1 = JSON
            .parse(JSON.stringify(res1))
            .filter((elem, index, self) => self.findIndex((t) => (t.firmId === elem.firmId)) === index)
          res.json(res1)
        })
    })
  } catch (e) {
    console.log("======>>", e.error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getAllEntityThirdPartyList = () => async(req, res) => {
  const {entityId, filteredValue} = req.body
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }, {
      resource: "EntityRel",
      access: 1
    }
  ]
  const searchBy = [
    {
      "$or": [
        {
          "firmDetails.addresses.isPrimary": true
        }, {
          "firmDetails.addresses.isPrimary": false
        }
      ]
    }
  ]

  const queryStages = (ids, mq) => [
    {
      $match: {
        ...mq
      }
    }, {
      $project: {
        "entityParty1": 1,
        "entityParty2": 1,
        "relationshipType": 1
      }
    }, {
      "$lookup": {
        "from": "entities",
        "localField": "entityParty2",
        "foreignField": "_id",
        "as": "firmDetails"
      }
    }, {
      $unwind: "$firmDetails"
    }, {
      "$match": {
        "firmDetails._id": {
          "$in": [...ids]
        }
      }
    }, {
      $unwind: "$firmDetails.addresses"
    }, {
      $match: {
        $and: searchBy
      }
    }
  ]

  if (filteredValue !== "") {
    if (filteredValue.entitySearch !== "") {
      const regexValue = new RegExp(filteredValue.entitySearch, "i")
      searchBy.push({
        "$or": [
          {
            "firmDetails.entityFlags.marketRole": {
              $in: [filteredValue.entitySearch]
            }
          }, {
            "firmDetails.firmName": {
              $regex: regexValue,
              $options: "i"
            }
          }, {
            "firmDetails.addresses.state": {
              $regex: regexValue,
              $options: "i"
            }
          }, {
            "firmDetails.addresses.city": {
              $regex: regexValue,
              $options: "i"
            }
          }, {
            "firmDetails.addresses.addressName": {
              $regex: regexValue,
              $options: "i"
            }
          }, {
            "firmDetails.addresses.officePhone[0].phoneNumber": {
              $regex: regexValue,
              $options: "i"
            }
          }
        ]
      })
    }
    if (filteredValue.entityType !== "")
      searchBy.push({
        "firmDetails.entityFlags.marketRole": {
          $in: [filteredValue.entityType]
        }
      })
    if (filteredValue.enityState && filteredValue.enityState.length > 0)
      searchBy.push({
        "firmDetails.addresses.state": {
          $in: filteredValue.enityState
        }
      })
  }
  try {
    let allEligibleIds = []
    const matchQueryPlatformAdmin = {
      relationshipType: "Third Party"
    }

    let finalQueryStages = [...queryStages(allEligibleIds, matchQueryPlatformAdmin)]
    const pAdmin = await isUserPlatformAdmin(user)
    if (!pAdmin) {
      allEligibleIds = await getAllEligibleIds(req.user, "all")
      const matchQuery = {
        relationshipType: "Third Party",
        entityParty1: ObjectID(entityId)
      }
      finalQueryStages = queryStages(allEligibleIds, matchQuery)
    } else {
      finalQueryStages.splice(4, 1)
    }

    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    EntityRel.aggregate([...finalQueryStages], (err, result) => {
      if (err)
        throw new Error(err)
      result = result.map(async(item, idx) => {
        const data = {}

        let primaryAddressDisplay
        let primaryContactUserId

        const primaryContactName = await EntityUser.find({
          $and: [
            {
              entityId: item.firmDetails._id
            }, {
              "userFlags": "Primary Contact"
            }
          ]
        }).select("_id userFirstName userMiddleName userLastName userAddresses")
        if (primaryContactName.length > 0) {
          const contactAddresses = primaryContactName[0].userAddresses
          const filteredPrimaryContactAddress = contactAddresses.filter(a => a.isPrimary)
          const primaryContactAddress = filteredPrimaryContactAddress[0]
          primaryContactUserId = primaryContactName[0]._id
          primaryAddressDisplay = `${primaryContactAddress.addressLine1}, ${primaryContactAddress.addressLine2}, ${primaryContactAddress.city} ${primaryContactAddress.state}`
        }

        data.firmId = item.firmDetails._id
        data.firmType = item.firmDetails && item.firmDetails.entityFlags && item.firmDetails.entityFlags.marketRole || []
        data.firmName = item.firmDetails.firmName
        data.state = item.firmDetails.addresses.state
        data.city = item.firmDetails.addresses.city
        data.country = item.firmDetails.addresses.country
        data.addressName = item.firmDetails.addresses.addressName
        data.addressName = primaryContactName.length > 0
          ? `${primaryContactName[0].userFirstName} ${primaryContactName[0].userMiddleName} ${primaryContactName[0].userLastName}`
          : ""
        data.primaryContactName = primaryContactName.length > 0
          ? `${primaryContactName[0].userFirstName} ${primaryContactName[0].userMiddleName} ${primaryContactName[0].userLastName}`
          : ""
        data.primaryContactUserId = primaryContactUserId
        data.addressName = primaryContactName.length > 0
          ? `${primaryContactName[0].userFirstName} ${primaryContactName[0].userMiddleName} ${primaryContactName[0].userLastName}`
          : ""
        data.primaryContactAddress = primaryAddressDisplay
        data.primaryContact = item.firmDetails.addresses.officePhone[0].phoneNumber
        if (item.firmDetails.addresses.isPrimary)
          return data
        return data
      })
      Promise
        .all(result)
        .then((res1) => {
          res1 = JSON
            .parse(JSON.stringify(res1))
            .filter((elem, index, self) => self.findIndex((t) => (t.firmId === elem.firmId)) === index)
          res.json(res1)
        })
    })
  } catch (e) {
    console.log("Error=========>>>", e)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getBorrowerObligor = () => async(req, res) => {
  const {entityId} = req.params
  const {user} = req
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    /* let allowedEntityId = false
    if(entityId){
      const pAdmin = await isUserPlatformAdmin(req.user)
      if(!pAdmin) {
        allowedEntityId = await doesIdExistsInEntitledEntityIds(req.user,entityId,"edit")
      } else {
        allowedEntityId = true
      }
      if (!allowedEntityId) {
         res.status(500).send({error:"Not Authorized to access Entity"})
      }
    } */
    const entitled = await canUserPerformAction(user, resRequested)
    if (!entitled/* || !allowedEntityId */) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const entity = await Entity
      .findOne({_id: entityId})
      .select("entityBorObl")
    res.json(entity)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getAllBusinessActivity = () => async(req, res) => {
  const {entityId} = req.body
  try {
    const result = await getTransactionsForSpecificEntity(req.user, entityId)
    res.json(result)
  } catch (error) {
    res
      .status(422)
      .send({error})
  }
}

const checkDuplicateEntity = () => async(req, res) => {
  const {user} = req
  let {entityVal} = req.body
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  entityVal = entityVal.replace(/\s\s+/g, " ")
  entityVal = new RegExp(entityVal, "i")
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const result = await EntityRel.aggregate([
      {
        $match: {
          "entityParty1": ObjectID(user.entityId)
        }
      }, {
        $project: {
          "entityParty1": 1,
          "entityParty2": 1,
          "relationshipType": 1
        }
      }, {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "firmDetails"
        }
      }, {
        $unwind: "$firmDetails"
      }, {
        $match: {
          $or: [
            {
              "firmDetails.msrbFirmName": entityVal
            }, {
              "firmDetails.msrbId": entityVal
            }, {
              "firmDetails.firmName": entityVal
            }
          ]
        }
      }
    ])
    res
      .status(200)
      .send({
        done: true,
        error: "",
        success: result.length > 0
          ? {
            isExist: true
          }
          : {
            isExist: false
          },
        requestedServices: resRequested
      })
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }

}

const getAllEntity = () => async(req, res) => {
  const {entityId} = req.body
  try {
    const result = await getFaEntityGraphInfo(req.user)
    res.json(result)
  } catch (error) {
    res
      .status(422)
      .send({error})
  }
}
const getEntNotEqSelf = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const allEligibleIds = await getAllEligibleIds(req.user, "all")

    const entityId = req.params.entId
    const resEntity = await EntityRel.find({
      entityParty1: entityId,
      relationshipType: {
        $ne: "Self"
      },
      entityParty2: {
        $in: [...allEligibleIds]
      }
    }).select("entityParty1 entityParty2")
    const issuerIds = resEntity.map(p => p.entityParty2)
    const issuerEntities = await Entity
      .find({
        _id: {
          $in: issuerIds
        }
      })
      .select("firmName msrbRegistrantType msrbFirmName")
    res.json(issuerEntities)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateFirmAddOns = () => async(req, res) => {
  const {firmAddOns} = req.body
  const {entityId} = req.params
  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    let allowedId = false
    if (entityId) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, entityId, "edit")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const result = await Entity.findByIdAndUpdate(ObjectID(entityId), {
      "$set": {
        firmAddOns
      }
    }, {upsert: true})

    await returnFromES(req.user.tenantId, entityId)
    res.json(result)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}
const savePrefrences = () => async(req, res) => {
  if (!req.body) {
    res
      .status(422)
      .send({
        status: "Error",
        message: "There is either no user or the payload for updating user preference doesn't exis" +
            "t",
        data: null
      })
  }

  const {_id: entityUserId} = req.user

  const {contextname} = req.params
  try {
    const searchDoc = await SearchPref.update({
      entityUserId,
      searchContext: contextname,
      searchName: req.body.searchname
        ? req.body.searchname
        : ""
    }, {
      entityUserId,
      searchContext: contextname,
      searchPreference: JSON.stringify(req.body.searchPreference)
    }, {
      upsert: true,
      setDefaultsOnInsert: true
    })
    if (!searchDoc) {
      res
        .status(422)
        .send({status: "Error", message: "Could not update/ insert the search preference document", data: null})
    }
    res
      .status(200)
      .send({status: `Success",message:"Successfully updated/inserted for user ${entityUserId}`, data: searchDoc})
  } catch (e) {
    console.log("There is an error in updating the search preference", e)
    res
      .status(422)
      .send({status: "Error", message: "Propblem setting preference for logged in user", data: null})
  }
}

const getAllUserPrefForContext = () => async(req, res) => {
  const {contextname} = req.params
  const {_id: userId} = req.user
  try {
    const searchDoc = await SearchPref.find({entityUserId: userId, searchContext: contextname})
    if (searchDoc) {
      res
        .status(200)
        .send({status: `Success",message:"Retrieved Document for User = ${userId} & context - ${contextname}`, data: searchDoc})
    } else {
      res
        .status(200)
        .send({status: "Error", message: `No information exists for this user = ${userId} & context - ${contextname}`, data: {}})
    }
  } catch (error) {
    console.log("There is an error in updating the search preference", error)
    res
      .status(422)
      .send({status: "Error", message: "Propblem setting preference for logged in user", data: null})
  }
}

const deletePrefSearch = () => async(req, res) => {
  const {prefId} = req.body
  try {
    const deleteDoc = await SearchPref.remove({_id: prefId})
    if (deleteDoc) {
      res
        .status(200)
        .send({status: "Successfully Deleted", data: deleteDoc})
    } else {
      res
        .status(422)
        .send({status: "Error", message: "No information exists for deleting", data: {}})
    }
  } catch (error) {
    console.log("There is an error in updating the search preference", error)
    res
      .status(422)
      .send({status: "Error", message: "Propblem setting preference for logged in user", data: null})
  }
}

const getAllTenantUsersForTesting = () => async(req, res) => {
  const userDetails = await getAllEntityAndUserDetailsForTenantUser({_id: ObjectID("5b957f9d0d78db0cb0481f9b"), entityId: ObjectID("5b957f950d78db0cb0481e21")})
  res.send(userDetails)
}
const changeAddressStatus = () => async(req, res) => {
  const {user} = req

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const fields = `addresses.$.${req.body.field}`
    const result = await Entity.findOneAndUpdate({
      "_id": ObjectID(req.body.entityId),
      "addresses._id": ObjectID(req.body.addressId)
    }, {
      "$set": {
        [fields]: req.body.status
      }
    })
    res.send(result)
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveDocuments = () => async(req, res) => {
  const {entId} = req.params
  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const select = "entityDocuments"
    const docs = req
      .body
      .entityDocuments
      .map(item => {
        item.docStatus = ""
        return item
      })
    await Entity.updateOne({
      _id: entId
    }, {
      $addToSet: {
        entityDocuments: docs
      }
    })
    const entity = await Entity
      .findOne({_id: ObjectID(entId)})
      .select(select)
    await returnFromES(req.user.tenantId, entId)
    res.json(entity)

  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getDocuments = () => async(req, res) => {
  const {entId} = req.params
  const allowedId = true

  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let allowedId = false
    if (entId) {
      const pAdmin = await isUserPlatformAdmin(req.user)
      if (!pAdmin) {
        allowedId = await doesIdExistsInEntitledEntityIds(req.user, entId, "view")
      } else {
        allowedId = true
      }
      if (!allowedId) {
        res
          .status(500)
          .send({error: "Not Authorized to access Entity"})
      }
    }

    const select = "entityDocuments"
    const entity = await Entity
      .find({_id: ObjectID(entId)})
      .select(select)
    res.json(entity)

  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateDocumentStatus = () => async(req, res) => {
  const {entId} = req.params
  const {_id, docStatus} = req.body
  const {type} = req.query
  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const query1 = "entityDocuments._id"

    if (type === "status") {
      await Entity.updateOne({
        _id: ObjectID(entId),
        [query1]: ObjectID(_id)
      }, {
        $set: {
          "entityDocuments.$.docStatus": docStatus
        }
      })
    } else if (type === "meta") {
      req.body.LastUpdatedDate = moment(new Date()).format("MM/DD/YYYY hh:mm A")
      await Entity.updateOne({
        _id: ObjectID(entId),
        [query1]: ObjectID(_id)
      }, {
        $set: {
          "entityDocuments.$": req.body
        }
      })
    }
    const document = await Entity
      .findById(entId)
      .select("entityDocuments")
    await returnFromES(req.user.tenantId, entId)
    res
      .status(200)
      .json({done: true, document})

  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const deleteDocuments = () => async(req, res) => {
  const {entId} = req.params
  const {docId} = req.query
  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const document = await Entity.update({
      _id: ObjectID(entId)
    }, {
      $pull: {
        "entityDocuments": {
          "_id": ObjectID(docId)
        }
      }
    })
    const select = "entityDocuments"
    const entity = await Entity
      .find({_id: ObjectID(entId)})
      .select(select)
    await returnFromES(req.user.tenantId, entId)
    res
      .status(200)
      .json(entity)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}
const getGlobRefEntity = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {inputVal, entityType, participantType} = req.body
  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = inputVal.replace(invalid, "")
    const regexValue = new RegExp(newSearchString, "i")
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const resEntity = await EntityRel
      .find({
        entityParty1: req.user.entityId,
        relationshipType: {
          $ne: "Self"
        }
      })
      .select("entityParty1 entityParty2")
    const issuerIds = resEntity.map(p => p.entityParty2)
    const issuerEntities = await Entity
      .find({
        _id: {
          $in: issuerIds
        }
      })
      .select("-_id firmName")
    const entityList = issuerEntities.map(p => p.firmName)
    let globIssuer = []

    if (entityType === "Third Party") {
      console.log("===================================>", entityType)
      globIssuer = await GlobRefEntity.find({
        $and: [
          {
            firmType: entityType
          }, {
            "firmName": {
              $regex: regexValue,
              $options: "i"
            }
          }, {
            "firmName": {
              $nin: entityList
            }
          }
        ]
      }).select("firmName -_id participantType")
    }

    if (entityType === "Prospects/Clients") {
      // globIssuer = await
      // GlobRefEntity.find({$and:[{participantType},{firmType:entityType}, {
      // "firmName":{$regex:regexValue,$options:"i"}},{"firmName": { $nin: entityList}
      // }]}).select("firmName -_id participantType")
      console.log("==========================>", {
        participantType: participantType.replace(/\//g, "%2F"),
        firmType: entityType
      })
      const match = participantType === "Governmental Entity / Issuer"
        ? /Governmental Entity\/Issuer/
        : participantType
      globIssuer = await GlobRefEntity.aggregate([
        {
          $match: {
            participantType: match,
            firmType: "Prospects/Clients"
          }
        }, {
          $project: {
            "firmName": 1
          }
        }, {
          $sample: {
            size: 50
          }
        }
      ])
    }

    res.json(globIssuer)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getGlobalRefIssuer = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {entityType, participantType} = req.body
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const entityId = await EntityRel
      .findOne({entityParty2: req.user.entityId})
      .select({entityParty1: 1})
    const resEntity = await EntityRel
      .find({
        entityParty1: entityId.entityParty1,
        /* req.user.entityId */
        relationshipType: {
          $ne: "Self"
        }
      })
      .select("entityParty1 entityParty2")
    const issuerIds = resEntity.map(p => p.entityParty2)
    const issuerEntities = await Entity
      .find({
        _id: {
          $in: issuerIds
        }
      })
      .select("_id firmName entityType")
    const entityList = issuerEntities.map(p => p.firmName)
    let globIssuer = []
    const issuerList = []
    if (entityType === "Third Party") {
      globIssuer = await GlobRefEntity
        .find({firmType: entityType})
        .select("firmName _id participantType")
    }

    if (entityType === "Prospects/Clients") {

      if (participantType === "Governmental Entity / Issuer" || participantType === "Municipal Advisors" || participantType === "Corporation") {
        const match = participantType === "Governmental Entity / Issuer"
          ? /Governmental Entity\/Issuer/
          : participantType

        globIssuer = await GlobRefEntity
          .find({participantType: match, firmType: "Prospects/Clients"})
          .select("firmName _id participantType")
          .limit(100)
        const firmType = participantType === "Municipal Advisors"
          ? "Municipal Advisor"
          : participantType === "Corporation"
            ? "Corporation"
            : ""
        console.log("================firmType===================>", firmType)
        if (issuerEntities && Array.isArray(issuerEntities)) {
          console.log("==================issuerEntities===================>", issuerEntities)
          issuerEntities.forEach(item => {
            const firm = {
              firmId: item._id,
              firmName: item.firmName,
              participantType,
              isExist: true
            }
            issuerList.push(firm)
          })
        }
        globIssuer = globIssuer.concat(issuerList)
      }
    }
    res.json({globIssuer})
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const entityLookUpOld = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {entityType, participantType, searchString} = req.body
  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchString.replace(invalid, "")
    const regexValue = new RegExp(newSearchString, "i")
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const start1 = new Date()

    const entityId = await EntityRel
      .findOne({entityParty2: req.user.entityId})
      .select({entityParty1: 1})
    const resEntity = await EntityRel
      .find({
        entityParty1: entityId.entityParty1,
        /* req.user.entityId */
        relationshipType: {
          $ne: "Self"
        }
      })
      .select("entityParty2")
    const time1 = new Date() - start1
    const issuerIds = resEntity.map(p => p.entityParty2)
    const start2 = new Date()
    const issuerEntities = await Entity.find({
      _id: {
        $in: issuerIds
      },
      firmName: {
        $regex: regexValue,
        $options: "i"
      }
    }).select("_id firmName entityType")
    const time2 = new Date() - start2
    const entityList = issuerEntities.map(p => p.firmName)
    let globIssuer = []
    const issuerList = []
    const start3 = new Date()
    let lookupParticipantType = participantType === "501c3 - Obligor"
      ? "Governmental Entity / Issuer"
      : participantType
    lookupParticipantType = lookupParticipantType === "Municipal Advisor"
      ? "Municipal Advisors"
      : lookupParticipantType
    if (entityType === "Third Party") {
      const filters = {
        firmType: entityType,
        firmName: {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (participantType) {
        filters.participantType = lookupParticipantType
      }
      globIssuer = await GlobRefEntity
        .find(filters)
        .select("firmName _id participantType")
        .limit(50)
    }

    if (entityType === "Prospects/Clients") {

      if (["Governmental Entity / Issuer", "501c3 - Obligor", "Municipal Advisors"].includes(lookupParticipantType)) {
        // const match = participantType === "Governmental Entity / Issuer" ?
        // /Governmental Entity\/Issuer/ : participantType
        globIssuer = await GlobRefEntity
          .find({
            participantType: lookupParticipantType,
            firmName: {
              $nin: entityList,
              $regex: regexValue,
              $options: "i"
            }
          })
          .select("firmName _id participantType")
          .limit(50)
      }
    }
    const time3 = new Date() - start3
    issuerEntities.forEach(item => {
      const firm = {
        _id: item._id,
        firmName: item.firmName,
        participantType,
        isExisting: true
      }
      issuerList.push(firm)
    })
    globIssuer = globIssuer.concat(issuerList)
    const time4 = new Date() - start3
    res.json({
      globIssuer,
      times: [time1, time2, time3, time4]
    })
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const entityLookUp = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  try {
    const {entityType, participantType, searchString} = req.body
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchString.replace(invalid, "")
    const regexValue = new RegExp(newSearchString || "", "i")
    const searchStringLower = newSearchString.toLowerCase()
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const start1 = new Date()
    // console.log("start1 : ", start1)
    const issuerEntities = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: req.user.entityId
        }
      }, {
        $lookup: {
          "from": "entityrels",
          "localField": "entityParty1",
          "foreignField": "entityParty1",
          "as": "rels"
        }
      }, {
        $project: {
          rels: {
            $filter: {
              "input": "$rels",
              "as": "rel",
              "cond": {
                "$ne": ["$$rel.relationshipType", "Self"]
              }
            }
          }
        }
      }, {
        $project: {
          entityParty2: "$rels.entityParty2"
        }
      }, {
        $unwind: "$entityParty2"
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "entities"
        }
      }, {
        $project: {
          entities: {
            $filter: {
              "input": "$entities",
              "as": "entity",
              "cond": {
                $gt: [
                  {
                    $indexOfCP: [
                      {
                        $toLower: "$$entity.firmName"
                      },
                      searchStringLower
                    ]
                  },
                  -1
                ]
              }
            }
          }
        }
      }, {
        $unwind: "$entities"
      }, {
        $project: {
          _id: "$entities._id",
          firmName: "$entities.firmName",
          entityType: "$entities.entityType"
        }
      }
    ])

    const time1 = new Date() - start1
    // console.log("time1 : ", time1)
    const entityList = issuerEntities.map(p => p.firmName)
    let globIssuer = []
    const issuerList = []
    const start3 = new Date()
    let lookupParticipantType = participantType === "501c3 - Obligor"
      ? "Governmental Entity / Issuer"
      : participantType
    lookupParticipantType = lookupParticipantType === "Municipal Advisor"
      ? "Municipal Advisors"
      : lookupParticipantType
    if (entityType === "Firms/Tenants") {
      const filters = {
        firmType: entityType,
        firmName: {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (participantType) {
        filters.participantType = lookupParticipantType
      }
      globIssuer = await GlobRefEntity
        .find(filters)
        .select({firmName: 1, _id: 1, participantType: 1, meta: 1})
        .limit(50)
      globIssuer = globIssuer.map(d => ({
        firmName: d.firmName,
        _id: d._id,
        participantType: d.participantType,
        msrbId: (d.meta && d.meta.referenceId) || ""
      }))
    }

    if (entityType === "Third Party") {
      const filters = {
        firmType: entityType,
        firmName: {
          $regex: regexValue,
          $options: "i"
        }
      }
      if (participantType) {
        filters.participantType = lookupParticipantType
      }
      globIssuer = await GlobRefEntity
        .find(filters)
        .select("firmName _id participantType")
        .limit(50)
    }

    if (entityType === "Prospects/Clients") {

      if (["Governmental Entity / Issuer", "501c3 - Obligor", "Municipal Advisors"].includes(lookupParticipantType)) {
        // const match = participantType === "Governmental Entity / Issuer" ?
        // /Governmental Entity\/Issuer/ : participantType
        globIssuer = await GlobRefEntity
          .find({
            participantType: lookupParticipantType,
            firmName: {
              $nin: entityList,
              $regex: regexValue,
              $options: "i"
            }
          })
          .select("firmName _id participantType")
          .limit(50)
      }
    }
    const time3 = new Date() - start3
    issuerEntities.forEach(item => {
      const firm = {
        _id: item._id,
        firmName: item.firmName,
        participantType,
        isExisting: true
      }
      issuerList.push(firm)
    })
    globIssuer = globIssuer.concat(issuerList)
    const time4 = new Date() - start3
    res.json({
      globIssuer,
      times: [time1, time3, time4]
    })
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getContracts = () => async(req, res) => {
  // console.log("===================req=============>", req, res)
  const {entityId} = req.params
  // console.log("===================req=============>", entityId)
  if (!entityId) {
    res.send({done: false, message: "Incorrect Information has been passed for Insert", data: null})
  }
  try {
    const returnedDoc = await Entity
      .findOne({_id: ObjectID(entityId)})
      .select("entityAliases entityFlags _id entityType sof nonTranFees retAndEsc updatedAt con" +
          "tracts createdAt status auditLogs ")
    res.send({done: true, message: "Fetch data", data: returnedDoc})

  } catch (e) {
    console.log(e)
    res.send({done: false, message: "Unable to fetch Information, something went wrong", data: null})
  }
}

const getActiveContracts = () => async(req, res) => {
  const {entityId} = req.params
  console.log("==========entityId================>", entityId)
  if (!entityId) {
    res.send({done: false, message: "Incorrect Information has been passed for Insert", data: null})
  }
  try {
    const returnedDoc = await Entity.aggregate([
      {
        $match: {
          _id: ObjectID(entityId),
          contracts: {
            $elemMatch: {
              "status": "Active"
            }
          }
        }
      }, {
        $unwind: "$contracts"
      }, {
        $match: {
          "contracts.status": "Active"
        }
      }, {
        $group: {
          _id: "$_id",
          contracts: {
            $addToSet: "$contracts"
          }
        }
      }
    ])
    // const contract = returnedDoc && returnedDoc.length && returnedDoc[0]
    //   .contracts
    //   .filter(cont => moment(cont.contractRef.startDate).format("YYYY-MM-DD") <= moment(new Date()).format("YYYY-MM-DD") && moment(cont.contractRef.endDate).format("YYYY-MM-DD") >= moment(new Date()).format("YYYY-MM-DD"))
    res.send({done: true, message: "Fetch data", data: (returnedDoc && returnedDoc.length && returnedDoc[0] && returnedDoc[0].contracts) || []})
  } catch (e) {
    console.log(e)
    res.send({done: false, message: "Unable to fetch Information, something went wrong", data: null})
  }
}

const putContracts = () => async(req, res) => {
  const {entityId} = req.params
  if (!entityId) {
    res.send({done: false, message: "Incorrect Information has been passed for Insert", data: null})
  }
  try {
    const {_id, status} = req.body
    if (req.query.status) {
      await Entity.updateOne({
        _id: entityId,
        "contracts._id": ObjectID(_id)
      }, {
        $set: {
          "contracts.$.status": status
        }
      })
    } else if (_id) {
      req.body.updatedBy = {
        userId: req.user._id || "",
        userFirstName: req.user.userFirstName || "",
        userLastName: req.user.userLastName || "",
        userEntityId: req.user.entityId || "",
        userEntityName: req.user.userFirmName || ""
      }
      await Entity.updateOne({
        _id: entityId,
        "contracts._id": ObjectID(_id)
      }, {
        $set: {
          "contracts.$": req.body
        }
      })
    } else {
      req.body.status = "Active"
      req.body.updatedBy = {
        userId: req.user._id || "",
        userFirstName: req.user.userFirstName || "",
        userLastName: req.user.userLastName || "",
        userEntityId: req.user.entityId || "",
        userEntityName: req.user.userFirmName || ""
      }
      await Entity.update({
        _id: entityId
      }, {
        $addToSet: {
          contracts: req.body
        }
      })
    }
    const returnedDoc = await Entity
      .findOne({_id: entityId})
      .select("entityAliases entityFlags _id entityType sof nonTranFees retAndEsc updatedAt con" +
          "tracts createdAt docSelect status")
    await returnFromES(req.user.tenantId, entityId)
    res.send({done: true, message: "Updated / Inserted data", data: returnedDoc})
  } catch (e) {
    console.log(e)
    res.send({done: false, message: "Unable to update Information, something went wrong", data: null})
  }
}

const deleteContracts = () => async(req, res) => {
  const {entityId} = req.params
  const {contractId} = req.query
  try {
    await Entity.update({
      _id: entityId
    }, {
      $pull: {
        "contracts": {
          _id: contractId
        }
      }
    })
    const document = await Entity
      .findOne({_id: entityId})
      .select("contracts createdAt")
    await returnFromES(req.user.tenantId, entityId)
    res.send({done: true, message: "Removed Data Successfully", data: document})
  } catch (error) {
    console.log("******Others*******removeParticipantByPartId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const postGlobIssuer = () => async(req, res) => {

  const jsonPath = path.join(__dirname, "..", "..", "seeddata", "seedmongo", "globissuer.json")
  console.log("The path of the file is", jsonPath)
  const jsonString = fs.readFileSync(jsonPath, "utf8")

  try {
    const list = []
    const stateName = {
      AL: "Alabama",
      AK: "Alaska",
      AZ: "Arizona",
      AR: "Arkansas",
      CA: "California",
      CO: "Colorado",
      CT: "Connecticut",
      DE: "Delaware",
      FL: "Florida",
      GA: "Georgia",
      HI: "Hawaii",
      ID: "Idaho",
      IL: "Illinois",
      IN: "Indiana",
      IA: "Iowa",
      KS: "Kansas",
      KY: "Kentucky",
      LA: "Louisiana",
      ME: "Maine",
      MD: "Maryland",
      MA: "Massachusetts",
      MI: "Michigan",
      MN: "Minnesota",
      MS: "Mississippi",
      MO: "Missouri",
      MT: "Montana",
      NE: "Nebraska",
      NV: "Nevada",
      NH: "New Hampshire",
      NJ: "New Jersey",
      NM: "New Mexico",
      NY: "New York",
      NC: "North Carolina",
      ND: "North Dakota",
      OH: "Ohio",
      OK: "Oklahoma",
      OR: "Oregon",
      PA: "Pennsylvania",
      RI: "Rhode Island",
      SC: "South Carolina",
      SD: "South Dakota",
      TN: "Tennessee",
      TX: "Texas",
      UT: "Utah",
      VT: "Vermont",
      VA: "Virginia",
      WA: "Washington",
      WV: "West Virginia",
      WI: "Wisconsin"
    }
    Object
      .keys(jsonString)
      .forEach(key => {
        jsonString[key].forEach(issuer => {
          list.push({
            name: issuer.nm,
            state: key,
            stateName: stateName[key] || "",
            type: "Governmental Entity / Issuer",
            subType: issuer.itp,
            meta: {
              itp: issuer.itp,
              referenceId: issuer.id,
              industryCategory: issuer.tp
            }
          })
        })
      })
    GlobRefEntity.insertMany(list, (err, createdList) => {
      if (err)
        return res.status(422).send({done: false, error: err})
      console.log("inserted all the globIssuer into Mongo")
      res.json({done: false, msg: "Successfully updated GlobRefEntity", list: createdList})
    })
  } catch (error) {
    res
      .status(422)
      .send({done: false, error})
  }
}

const pullContractsDocument = () => async(req, res) => {
  const {entityId} = req.params
  const {contractId, docId} = req.query
  try {
    await Entity.update({
      _id: ObjectID(entityId),
      "contracts._id": ObjectID(contractId)
    }, {
      $pull: {
        "contracts.$.documents": {
          _id: ObjectID(docId)
        }
      }
    })
    let contract = await Entity
      .findOne({_id: entityId})
      .select("contracts createdAt")
    contract = contract
      .contracts
      .find(con => con._id.toString() === contractId)
    await returnFromES(req.user.tenantId, entityId)
    res.send({done: true, message: "Removed Data Successfully", documents: contract.documents})
  } catch (error) {
    console.log("******Others*******removeParticipantByPartId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putAuditLog = () => async(req, res) => {
  const {contractId} = req.params
  try {
    await Entity.update({
      _id: ObjectID(contractId)
    }, {
      $addToSet: {
        auditLogs: req.body
      }
    })
    const generalAdmin = await Entity
      .findOne({_id: ObjectID(contractId)})
      .select("auditLogs")
    res.json(generalAdmin)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error})
  }
}

export default generateControllers(Entity, {
  getAllEntity: getAllEntity(),
  getAllFirmList: getAllFirmList(),
  updateFirmDetailById: updateFirmDetailById(),
  updateFirmLogoDetailById: updateFirmLogoDetailById(),
  saveFirmDetail: saveFirmDetail(),
  searchFirm: searchFirm(),
  getFirmsDetailById: getFirmsDetailById(),
  getAllThirdPartyFirmList: getAllThirdPartyFirmList(),
  getThirdPartyFirmsDetailById: getThirdPartyFirmsDetailById(),
  saveThirdPartyFirmDetail: saveThirdPartyFirmDetail(),
  updateThirdPartyFirmDetailById: updateThirdPartyFirmDetailById(),
  getAllClientsFirmList: getAllClientsFirmList(),
  getClientsFirmsDetailById: getClientsFirmsDetailById(),
  saveClientsFirmDetail: saveClientsFirmDetail(),
  updateClientsFirmDetailById: updateClientsFirmDetailById(),
  getEntitiesByRes: getEntitiesByRes(),
  saveEntity: saveEntity(),
  getUsersByEnt: getUsersByEnt(),
  getSeries50: getSeries50(),
  getSeries50ForLoadTesting: getSeries50ForLoadTesting(),
  deleteLinkCusipBorrower: deleteLinkCusipBorrower(),
  getAllClientsPropectList: getAllClientsPropectList(),
  getAllEntityThirdPartyList: getAllEntityThirdPartyList(),
  getBorrowerObligor: getBorrowerObligor(),
  getAllBusinessActivity: getAllBusinessActivity(),
  checkDuplicateEntity: checkDuplicateEntity(),
  getEntNotEqSelf: getEntNotEqSelf(),
  updateFirmAddOns: updateFirmAddOns(),
  savePrefrences: savePrefrences(),
  getAllUserPrefForContext: getAllUserPrefForContext(),
  deletePrefSearch: deletePrefSearch(),
  getAllTenantUsersForTesting: getAllTenantUsersForTesting(),
  changeAddressStatus: changeAddressStatus(),
  saveDocuments: saveDocuments(),
  getDocuments: getDocuments(),
  updateDocumentStatus: updateDocumentStatus(),
  deleteDocuments: deleteDocuments(),
  getGlobRefEntity: getGlobRefEntity(),
  getGlobalRefIssuer: getGlobalRefIssuer(),
  postGlobIssuer: postGlobIssuer(),
  getContracts: getContracts(),
  getActiveContracts: getActiveContracts(),
  putContracts: putContracts(),
  deleteContracts: deleteContracts(),
  pullContractsDocument: pullContractsDocument(),
  entityLookUp: entityLookUp(),
  putAuditLog: putAuditLog()
})
