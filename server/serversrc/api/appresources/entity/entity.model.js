import mongoose from "mongoose"
import mongooseHidden from "mongoose-hidden"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../entityUser/entityUser.model"

const mongooseHiddenApp = mongooseHidden()
const { Schema } = mongoose


export const entityAddressSchema = Schema({
  addressName: String,
  isPrimary: Boolean,
  isHeadQuarter: Boolean,
  /* isActive: Boolean, */
  website: String,
  officePhone: [
    { countryCode: String, phoneNumber: String, extension: String }
  ],
  officeFax: [
    { countryCode: String, faxNumber: String }
  ],
  officeEmails: [{ emailId: String }],
  addressLine1: String,
  addressLine2: String,
  country: String,
  state: String,
  city: String,
  zipCode: { zip1: String, zip2: String },
  formatted_address:String,
  url:String,
  location:{longitude:String,latitude:String}
})

export const firmAddOnsSchema = Schema({
  serviceName: String,
  serviceEnabled: Boolean
})

const entityFlagSchema = Schema({
  marketRole: [String],
  issuerFlags: [String]
})

export const entityLinkCusipSchema = Schema({
  debtType: String,
  debtDescription: String,
  associatedCusip6: String,
})

export const entityLinkBorrowersObligorsSchema = Schema({
  borOblRel: String,
  borOblFirmName: String,
  borOblDebtType: String,
  borOblCusip6: String,
  borOblStartDate: Date,
  borOblEndDate: Date
})

const sofSchema = new Schema({
  securityType: String,
  sofDealType: String,
  feesType: String,
  sofMin: Number,
  sofMax: Number,
  fees: [{
    sofDesc: String,
    sofAmt: Number,
    sofCharge: Number
  }]
})

const nonTranFeesSchema = new Schema({
  role: String ,
  stdHourlyRate: Number ,
  quoatedRate:Number
})

const retAndEscSchema = new Schema({
  type: String,
  hoursCovered: Number ,
  effStartDate: Date ,
  effEndDate: Date
})

const contractReferenceSchema = new Schema({
  contractId:String,
  contractName:String,
  contractType:String,
  startDate: Date,
  endDate: Date,
})

export const entityDocumentsSchema=Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  docNote:String,
  docStatus:String,
  markedPublic:{ publicFlag:Boolean, publicDate:Date},
  sentToEmma: { emmaSendFlag:Boolean, emmaSendDate:Date},
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

export const contractDocumentsSchema=Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  uploadedDate: Date
})

const auditLogs = Schema({
  // superVisorModule: String, // General Admin Activities,
  // superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

// eslint-disable-next-line no-unused-vars
export const entitySchema = Schema({
  entityFlags: entityFlagSchema,
  isMuniVisorClient: Boolean,
  msrbFirmName: String,
  msrbRegistrantType: String,
  entityType:String,
  msrbId: String,
  entityAliases: [String],
  firmName: String,
  taxId: String,
  firmType: String,
  primarySectors: String,
  secondarySectors: String,
  firmLeadAdvisor: String,
  prevLeadAdvisor: String,
  prevAdvisorFirm: String,
  prevAdvisorContractExpire: Date,
  primaryContactId: { type: Schema.Types.ObjectId },
  primaryContactNameInEmma: String,
  primaryContactPhone: String,
  primaryContactEmail: String,
  businessStructure: String,
  settings: Object,
  numEmployees: String,
  annualRevenue: String,
  addresses: [entityAddressSchema],
  organizationType: String,
  firmAddOns: [String],
  contractReference :[contractReferenceSchema],
  contracts : [
    {
      contractRef:contractReferenceSchema,
      sof: [sofSchema],
      documents:[contractDocumentsSchema],
      nonTranFees: [nonTranFeesSchema],
      retAndEsc: [retAndEscSchema],
      notes: String,
      status: String,
      digitize: Boolean,
      updatedBy: {
        userId:{ type: Schema.Types.ObjectId },
        userFirstName:String,
        userLastName:String,
        userEntityId:String,
        userEntityName:String,
      },
      updatedAt: { type: Date, required: true, default: Date.now }
    },
  ],
  entityLinkedCusips: [entityLinkCusipSchema],
  entityBorObl: [entityLinkBorrowersObligorsSchema],
  entityDocuments:[entityDocumentsSchema],
  auditLogs:[auditLogs],
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object,
  notes:[notesSchema],
},
{ versionKey: false })
entitySchema.plugin(timestamps)
entitySchema.index({firmName:"text"})

export const Entity = mongoose.model("entity", entitySchema)
