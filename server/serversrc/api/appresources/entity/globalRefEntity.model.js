import mongoose from "mongoose"
import mongooseHidden from "mongoose-hidden"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

export const globalRefEntitySchema = Schema({
  firmName:String,
  firmType:String,
  participantType:String,
  participantSubType:String,
  state:String,
  meta:{}
}, { _id: false })
globalRefEntitySchema.plugin(timestamps)
globalRefEntitySchema.index({firmName:"text"})

export const GlobRefEntity = mongoose.model("globalrefentity", globalRefEntitySchema)
