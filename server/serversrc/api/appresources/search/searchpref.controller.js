import { generateControllers } from "../../modules/query"
import { SearchPref } from "./searchpref.model"

// Check if this is an object or an array
const isObject = (a)=> (!!a) && (a.constructor === Object)
const isArray = (a)=> (!!a) && (a.constructor === Array)

const upsertAllUserPrefs = ()=>async (req,res,next)=>{
  if( !req.user || !req.body) {
    res.status(422).send({status:"Error",message:"There is either no user or the payload for updating user preference doesn't exist",data:null})
  }

  console.log("GOT PAST INITIAL CHECK", JSON.stringify(req.body,null,2))
  const {_id:userId} = req.user
  try {
    const searchDoc = await SearchPref.findOneAndUpdate({entityUserId:userId},
      {...req.body},
      {
        "new":true,
        "upsert": true,
        "runValidators": true
      })
    console.log("THE DOC POST UPDATE IS", searchDoc )
    if(!searchDoc) {
      res.status(422).send({status:"Error",message:"Could not update/ insert the search preference document",data:null})
    }
    res.status(200).send({status:`Success",message:"Successfully updated/inserted for user ${userId}`,data:searchDoc})
  } catch (e) {
    console.log("There is an error in updating the search preference",e)
    res.status(422).send({status:"Error",message:"Propblem setting preference for logged in user", data:null})
  }
}

const upsertOnePreferenceForContext = ()=>async (req,res,next)=>{
  if( !req.user || !req.body || !req.params) {
    res.status(422).send({status:"Error",message:"There is either no user or the payload for updating user preference doesn't exist",data:null})
  }
  const {_id:userId} = req.user
  const {contextname} = req.params
  console.log("context name", contextname)
  const payload = req.body

  try {
    const searchDocOutput = await SearchPref.findOne({entityUserId:userId,searchContext:contextname}).select({_id:1,searchContext:1,searchPreference:1})
    const searchDoc = (searchDocOutput || {})
    let searchPrefToBeUpdated = null
    if( isObject(payload) ) {
      console.log("THIS IS AN OBJECT")
      const { searchFilterName:updateSearchFilterName,isDefault:defaultFlagUpdate } = payload
      const existingPref = (searchDoc && searchDoc.searchPreference) || []

      if ( existingPref.length > 0 ){
        const foundDoc = existingPref.filter( pref => pref.searchFilterName === updateSearchFilterName)
        const foundFlag = foundDoc.length > 0

        if (foundFlag){
          searchPrefToBeUpdated = existingPref.reduce( (newSearchPref, updPref) => {
            const updObj = {}
            // If the searchFilterName exists then we need to update the default flag properly
            console.log(`${updPref.searchFilterName} = ${updateSearchFilterName}`)

            if( updPref.searchFilterName === updateSearchFilterName) {
              return [...newSearchPref, payload]
            }
            if (defaultFlagUpdate){
              console.log(JSON.stringify(updObj,null, 2))
              return [ ...newSearchPref, {...updPref,isDefault:false}]
            }
            return [ ...newSearchPref, updPref]
          },[])
        } else {
          let newUpdObject = null
          if (defaultFlagUpdate){
            newUpdObject = existingPref.reduce ( (newSearchPref, updPref) =>[ ...newSearchPref, {...updPref,isDefault:false}],[])
          } else {
            newUpdObject = existingPref
          }
          searchPrefToBeUpdated = [...newUpdObject,payload ]
        }

        // console.log("UPDATE AFTER REDUCE",JSON.stringify(searchPrefToBeUpdated,null,2))

        // Update the document
      } else {
        console.log("NOTHING ESISTS", payload)
        searchPrefToBeUpdated = [payload]
      }

    }
    else if( isArray(payload)){ // If the payload is an array then just update the payload

      console.log("THIS IS AN ARRAY")

      searchPrefToBeUpdated=payload
    }

    const updSearchDoc = await SearchPref.findOneAndUpdate({entityUserId:userId,searchContext:contextname},
      {searchPreference:searchPrefToBeUpdated},
      {
        "new":true,
        "upsert": true,
        "runValidators": true
      })

    if(!updSearchDoc) {
      res.status(422).send({status:"Error",message:`Could not updae/insert - user = ${userId} / Context = ${contextname}`,data:null})
    }
    res.status(200).send({status:`Success",message:"Successfully updated/inserted for user = ${userId} & context - ${contextname}`,data:updSearchDoc})

  } catch (e) {
    console.log("There is an error in updating the search preference",e)
    res.status(422).send({status:"Error",message:`There was a problem in updating/inserting data for user = ${userId} & context - ${contextname} & Pref - ${updateSearchFilterName || ""}`, data:null})
  }
}

const getAllUserPrefForContext= ()=>async (req,res,next)=>{
  if( !req.user || !req.params) {
    res.status(422).send({status:"Error",message:"The context or the user doesn't exist",data:null})
  }
  const {_id:userId} = req.user
  const {contextname} = req.params

  try {
    // Find the Search Prefernece
    const searchDoc = await SearchPref.findOne({entityUserId:userId,searchContext:contextname})
    if (searchDoc) {
      const { searchPreference } = searchDoc
      res.status(200).send({status:`Success",message:"Retrieved Document for User = ${userId} & context - ${contextname}`,data: {
        ...searchDoc,
        searchPreference: JSON.parse(searchPreference)
      }})
    } else {
      res.status(200).send({status:"Error",message:`No information exists for this user = ${userId} & context - ${contextname}`, data:{}})
    }
  } catch(e) {
    console.log("There is an error in getting search preferences",e)
    res.status(422).send({status:"Error",message:`There was a problem in getting preferences = ${userId} & context - ${contextname}`, data:{}})
  }
}

const getAllUserPreferences= ()=>async (req,res,next)=>{

  if( !req.user) {
    res.status(422).send({status:"Error",message:"The the user doesn't exist",data:null})
  }
  const {_id:userId} = req.user
  try {
    // Find the Search Prefernece
    const searchDoc = await SearchPref.findOne({entityUserId:userId})
    console.log(searchDoc)
    if (searchDoc) {
      res.status(200).send({status:`Success",message:"Retrieved preferences for User = ${userId} `,data:searchDoc})
    } else {
      res.status(422).send({status:"Error",message:`No preferences exist for the user= ${userId} `, data:{}})
    }
  } catch(e) {
    console.log("There is an error in getting search preferences",e)
    res.status(422).send({status:"Error",message:`There was a problem in getting preferences for = ${userId}`, data:{}})
  }
}

const savePrefrences = () => async(req, res) => {
  if( !req.body) {
    res.status(422).send({status:"Error",message:"There is either no user or the payload for updating user preference doesn't exist",data:null})
  }

  const { _id: entityUserId } = req.user

  const {contextname} = req.params
  try {
    const searchDoc = await SearchPref.update({ entityUserId, searchContext: contextname }, {
      entityUserId,
      searchContext: contextname,
      searchPreference: JSON.stringify(req.body),
    }, {upsert: true, setDefaultsOnInsert: true})
    if(!searchDoc) {
      res.status(422).send({ status:"Error", message:"Could not update/ insert the search preference document", data:null })
    }
    res.status(200).send({ status:`Success",message:"Successfully updated/inserted for user ${entityUserId}`, data: searchDoc })
  } catch (e) {
    console.log("There is an error in updating the search preference",e)
    res.status(422).send({status:"Error",message:"Propblem setting preference for logged in user", data:null})
  }
}

export default generateControllers(SearchPref, {
  getAllUserPreferences:getAllUserPreferences(),
  upsertAllUserPrefs: upsertAllUserPrefs(),
  upsertOnePreferenceForContext: upsertOnePreferenceForContext(),
  getAllUserPrefForContext:getAllUserPrefForContext(),
  savePrefrences: savePrefrences(),
})

