import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

const searchPreferenceSchema = Schema({
  searchFilterName:String,
  isDefault:Boolean,
  groupBy:String,
  activityCategoryFilter:String,
  taskDueFilter:String,        // Tasks that are due in a particular period ( eg., x weeks from now )
  activityPeriodFilter:String, // Was there activity in the last x weeks or months
  taskStatusFilter:String,     // Is the task Open or closed
  clientProspectFilter:String, // Filter by Client or Propect or Both
  activityStateFilter:String,  // The state associated with the transaction
  activityStatusFilter:String, // The status of the transaction
  contactStateFilter:[String], // the state in which the contact is based
  contactCategoryFilter:[String], // Undewriter and different participant type categories
  timeFilter:String,
  resultsPerPage:Number
})

export const userSearchPrefSchema = new Schema({
  entityUserId:{ type: Schema.Types.ObjectId, index: true },
  searchContext: { type: String, index: true },
  searchName: { type: String, index: true,default:"" },
  searchPreference: Object,
})

userSearchPrefSchema.plugin(timestamps)
userSearchPrefSchema.index({entityUserId:1})
userSearchPrefSchema.index({entityUserId:1,searchContext:1})

export const SearchPref = mongoose.model("usersearchprefs", userSearchPrefSchema)
const abc = {
  "entityUserId":"5b4033b22b1c0430142f2b3d",
  "searchContext":"dash-task",
  "searchPreference":[{
    "searchFilterName":"new filter 1",
    "isDefault":true,
    "groupBy":"group by 1",
    "activityCategoryFilter":"activity 1",
    "taskDueFilter":"test 1",        // Tasks that are due in a particular period ( eg., x weeks from now )
    "activityPeriodFilter":"test 1", // Was there activity in the last x weeks or months
    "taskStatusFilter":"test 1" ,    // Is the task Open or closed
    "timeFilter":"Time filter 1",
    "resultsPerPage":25
  }]
}
