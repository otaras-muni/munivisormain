import mongoose, {Schema} from "mongoose"
import timestamps from "mongoose-timestamp"
import { Entity,EntityUser, } from "./../../models"
import { checklistsSchema } from "./../../config/config.model"

// mongoose.Promise = global.Promise

const participaantSchema = Schema({
  partType:String,
  partFirmId: {type: Schema.Types.ObjectId, ref: Entity},
  partFirmName: String, // May not need this
  partContactId: {type: Schema.Types.ObjectId, ref: EntityUser},
  partContactName: String,
  partContactEmail: String, // Same reasoning as above
  partUserAddress: {type: Schema.Types.ObjectId }, // Same reasoning as above
  partContactAddrLine1: String, // Same reasoning as above
  partContactAddrLine2: String, // Same reasoning as above
  participantState: String, // Same reasoning as above
  partContactPhone: String,
  partContactTitle: String,
  partContactAddToDL: Boolean
})

const seriesRatingsSchema = Schema({
  seriesName:String,
  ratingAgencyName: String,
  longTermRating: String,
  longTermOutlook: String,
  shortTermOutlook: String,
  shortTermRating: String,
})

const cepRatingsSchema = Schema({
  seriesName:String,
  cepName: String,
  cepType: String,
  longTermRating: String,
  longTermOutlook: String,
  shortTermOutlook: String,
  shortTermRating: String,
})

const otherTranDocSchema=Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  docNote:String,
  markedPublic:{ publicFlag:Boolean, publicDate:Date},
  sentToEmma: { emmaSendFlag:Boolean, emmaSendDate:Date},
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const tranProjectsSchema = Schema({
  // Attributes from the Create Transaction Page
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  actTranType:String, 
  actTranSubType:String,
  actTranUniqIdentifier:String, // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
  actTranFirmId:Schema.Types.ObjectId, // Entity ID
  actTranFirmName:String,
  actTranClientId:Schema.Types.ObjectId, // Entity ID
  actTranClientName:String, // Name of the Issuer Client
  actTranPrimarySector:String,
  actTranSecondarySector:String,
  actTranFirmLeadAdvisorId:Schema.Types.ObjectId,   // User Id
  actTranFirmLeadAdvisorName:String, // First Name and Last Name
  actTranIssueName:String,
  actTranProjectDescription:String,
  actProjectKeyDetails:{
    actTranSecurityType:String,
    actTranBorrowerName:String,
    actTranObligorName:String,
    actTranState:String,
    actTranCounty:String,
    actTranClosingDate:Date,
    actTranUseOfProceeds:String,
    actPlaceholder1:String,
    actPlaceholder2:String,
    actTranOfferingType:String,
    actTranBankQualified:String,
    actTranCorpType:String,
    actParAmount:Number,
    actPricingDate:Date,
    actExpAwardDate:Date,
    actActAwardDate:Date,
    actEstimatedRevenue:Number,
    actUseOfProceeds:String,
    actPlaceholder3:String,

  },
  actTranRelatedTo: [Schema({
    relTranId: { type: Schema.Types.ObjectId},
    relTranIssueName: String,
    relTranProjectName: String,
    relTranClientId: { type: Schema.Types.ObjectId},
    relTranClientName: String,
    relType: String,
  })],
  actTranNotes:String,
  actTranStatus:String,

  actTranParticipants:[participaantSchema],
  actTranChecklists:[checklistsSchema],
  actSeriesForRatingPresentation:[seriesRatingsSchema],
  actSeriesCEPProviderRatings:[cepRatingsSchema],
  actTranDocuments:[otherTranDocSchema],
}).plugin(timestamps)


export const OtherProjects =  mongoose.model("tranotherprojects", tranProjectsSchema)
