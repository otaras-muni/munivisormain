import Joi from "joi-browser"

const contractRefSchema = Joi.object().keys({
  contractId: Joi.string()
    .allow("")
    .optional(),
  contractName: Joi.string()
    .allow("")
    .optional(),
  contractType: Joi.string()
    .allow("")
    .optional(),
  endDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  startDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const contractsSchema = Joi.object().keys({
  contractRef: contractRefSchema,
  digitize: Joi.boolean().optional(),
  nonTranFees: Joi.array().optional(),
  retAndEsc: Joi.array().optional(),
  sof: Joi.array().optional(),
  agree: Joi.string()
    .allow("")
    .optional(),
  reviewed: Joi.boolean().optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const tranNotesSchema = Joi.object().keys({
  note: Joi.string().allow("").optional(),
  createdAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedByName: Joi.string().allow("").optional(),
  updatedById: Joi.string().allow("").optional(),
  _id: Joi.string().allow("").optional()
})

const tranOthersCreateSchema = Joi.object().keys({
  actTranUniqIdentifier: Joi.string().required().optional(),
  actTranType: Joi.string().required().optional(),
  actTranSubType: Joi.string().required().optional(),
  actTranOtherSubtype: Joi.string().allow("").optional(),
  actTranFirmId: Joi.string().required().optional(),
  actTranFirmName: Joi.string().required().optional(),
  actTranClientId: Joi.string().required().optional(),
  actTranClientName: Joi.string().required().optional(),
  actTranFirmLeadAdvisorId: Joi.string().allow("").optional(),
  actTranFirmLeadAdvisorName: Joi.string().allow("").optional(),
  actTranIsClientMsrbRegistered: Joi.boolean().required().optional(),
  actTranIsConduitBorrowerFlag: Joi.string().optional(),
  actTranState: Joi.string().allow("").optional(),
  actTranCounty: Joi.string().allow("").optional(),
  actTranIssueName: Joi.string().allow("").optional(),
  actTranRelatedTo: Joi.array().optional(),
  actTranPrimarySector: Joi.string().allow("").optional(),
  actTranSecondarySector: Joi.string().allow("").optional(),
  // actTranNotes: Joi.string().allow("").optional(),
  tranNotes: tranNotesSchema,
  actTranProjectDescription: Joi.string().allow("").optional(),
  actTranAssignedTo: Joi.array().min(1).optional(),
  actTranPurposeOfRequest: Joi.string().allow("").optional(),
  actTranStatus: Joi.string().required().optional(),
  createdByUser: Joi.string().required(),
  clientActive: Joi.boolean().required().optional(),
  participants: Joi.array().min(1).required().optional(),
  contracts: contractsSchema,
  opportunity: Joi.object().optional()
})

const transDetailSchema = Joi.object().keys({
  actTranIssueName: Joi.string()
    .allow(["", null])
    .required()
    .optional(),
  actTranProjectDescription: Joi.string()
    .allow(["", null])
    .optional(),
  actTranClientId: Joi.string().required(),
  // actTranIssuerId: Joi.string().required(),
  actTranClientFirmName: Joi.string()
    .required()
    .optional(),
  actTranClientMSRBType: Joi.string()
    .required()
    .optional(),
  actTranIssuerFirmName: Joi.string()
    .required()
    .optional(),
  actTranIssuerMSRBType: Joi.string()
    .required()
    .optional(),
  // actTranName: Joi.string().required(),
  actTranBorrowerName: Joi.string()
    .allow("")
    .optional(),
  actTranGuarantorName: Joi.string()
    .allow("")
    .optional(),
  actTranSubType: Joi.string()
    .required()
    .optional(),
  actTranStatus: Joi.string().required(),
  actTranState: Joi.string()
    .allow("")
    .optional(),
  actTranCounty: Joi.string()
    .allow("")
    .optional(),
  actTranPrimarySector: Joi.string()
    .allow("")
    .optional(),
  actTranSecondarySector: Joi.string()
    .allow("")
    .optional(),
  actTranOfferingType: Joi.string()
    .allow("")
    .optional(),
  actTranSecurityType: Joi.string()
    .allow("")
    .optional(),
  actTranBankQualified: Joi.string()
    .allow("")
    .optional(),
  actTranCorpType: Joi.string()
    .allow("")
    .optional(),
  actTranParAmount: Joi.number().allow(null, "").optional(),
  actTranPricingDate: Joi.date().allow(["", null]).required().optional(),
  actTranExpAwardDate: Joi.date().allow(["", null]).required().optional(),
  actTranActAwardDate: Joi.date().allow(["", null]).required().optional(),
  actTranSdlcCreditPerc: Joi.number().allow(null, "").optional(),
  actTranEstimatedRev: Joi.number().allow(null, "").optional(),
  actTranUseOfProceeds: Joi.string()
    .allow("")
    .required(),
  actTranPlaceholder: Joi.string()
    .allow("")
    .optional(),
  updateUser: Joi.string().optional(),
  updateDate: Joi.date().optional(),
  _id: Joi.string().required()
})

const CreateValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, tranOthersCreateSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const UpdateValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, transDetailSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const PartDistSchema = Joi.object().keys({
  partType: Joi.string()
    .required()
    .optional(),
  partFirmId: Joi.string()
    .required()
    .optional(),
  partFirmName: Joi.string()
    .required()
    .optional(), // May not need this
  partContactTitle: Joi.string()
    .required()
    .optional(),
  partContactId: Joi.string()
    .required()
    .optional(),
  partContactName: Joi.string()
    .required()
    .optional(),
  partUserAddress: Joi.string()
    .allow("")
    .optional(),
  partContactEmail: Joi.string()
    .email()
    .required()
    .optional(), // Same reasoning as above
  partContactAddrLine1: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  partContactAddrLine2: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  participantGoogleAddress: Joi.string().allow("").optional(),
  participantState: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  partContactPhone: Joi.string().allow("").optional(),
  partContactFax: Joi.string().allow("").optional(),
  addToDL: Joi.boolean().optional(),
  createdDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

const UnderWritersSchema = Joi.object().keys({
  partFirmId: Joi.string().required(),
  partType: Joi.string().required(),
  partFirmName: Joi.string().required(),
  roleInSyndicate: Joi.string().required(),
  liabilityPerc: Joi.number().allow(null, "").optional(),
  managementFeePerc: Joi.number().allow(null, "").optional(),
  partContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional()
})

export const PartUnderWritersValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, UnderWritersSchema, {
    abortEarly: false,
    stripUnknown: false
  })

export const ParticipantsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, PartDistSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const seriesPricingDataSchema = Joi.object().keys({
  term: Joi.number()
    .min(0)
    .max(50)
    .required(),
  maturityDate: Joi.date() /* .min(Joi.ref("callDate")) */
    .required(),
  amount: Joi.number()
    .min(0)
    .required(),
  coupon: Joi.number()
    .min(0)
    .max(0.15)
    .required(),
  yield: Joi.number()
    .min(0)
    .max(0.15)
    .required(),
  price: Joi.number()
    .min(0)
    .required(),
  YTM: Joi.number()
    .min(0)
    .max(0.15)
    .required(),
  cusip: Joi.string()
    .uppercase()
    .min(6)
    .max(9)
    .required()
    .optional(),
  callDate: Joi.date().required(),
  takeDown: Joi.number()
    .min(0)
    .required(),
  insured: Joi.boolean()
    .required()
    .optional(),
  drop: Joi.boolean()
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

const seriesPricingDetailsSchema = Joi.object().keys({
  seriesPrincipal: Joi.number()
    .min(0)
    .required(),
  seriesSecurityType: Joi.string()
    .required()
    .optional(),
  seriesSecurity: Joi.string()
    .allow("", null)
    .optional(),
  seriesSecurityDetails: Joi.string()
    .allow("", null)
    .optional(),
  seriesDatedDate: Joi.date()
    .allow(null)
    .optional(),
  seriesSettlementDate: Joi.date()
    .allow(null)
    .optional(),
  seriesFirstCoupon: Joi.date()
    .allow(null)
    .optional(),
  seriesPutDate: Joi.date()
    .allow(null)
    .optional(),
  seriesRecordDate: Joi.date()
    .allow(null)
    .optional(),
  seriesCallDate: Joi.date()
    .allow(null)
    .optional(),
  seriesFedTax: Joi.string()
    .allow("", null)
    .optional(),
  seriesStateTax: Joi.string()
    .allow("", null)
    .optional(),
  seriesPricingAMT: Joi.string()
    .allow("", null)
    .optional(),
  seriesBankQualified: Joi.string()
    .allow("", null)
    .optional(),
  seriesAccrueFrom: Joi.string()
    .allow("", null)
    .optional(),
  seriesPricingForm: Joi.string()
    .allow("", null)
    .optional(),
  seriesCouponFrequency: Joi.string()
    .allow("", null)
    .optional(),
  seriesDayCount: Joi.string()
    .allow("", null)
    .optional(),
  seriesRateType: Joi.string()
    .allow("", null)
    .optional(),
  seriesCallFeature: Joi.string()
    .allow("", null)
    .optional(),
  seriesCallPrice: Joi.number()
    .min(0)
    .allow(null)
    .optional(),
  seriesInsurance: Joi.string()
    .allow("", null)
    .optional(),
  seriesUnderwtiersInventory: Joi.string()
    .allow("", null)
    .optional(),
  seriesMinDenomination: Joi.number()
    .min(0)
    .allow(null)
    .optional(),
  seriesSyndicateStructure: Joi.string()
    .allow("", null)
    .optional(),
  seriesGrossSpread: Joi.number()
    .min(0)
    .allow(null)
    .optional(),
  seriesEstimatedTakeDown: Joi.number()
    .min(0)
    .allow(null)
    .optional(),
  seriesInsuranceFee: Joi.number()
    .min(0)
    .allow(null)
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

const dealsPricingSchema = Joi.object().keys({
  seriesPricingDetails: seriesPricingDetailsSchema,
  seriesPricingData: Joi.array()
    .items(seriesPricingDataSchema)
    .optional()
})

const PricingDetailsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, dealsPricingSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean()
    .required()
    .optional(),
  publicDate: Joi.string()
    .required()
    .optional()
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean()
    .required()
    .optional(),
  emmaSendDate: Joi.string()
    .required()
    .optional()
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().allow("").required(),
  docSubCategory: Joi.string().allow("").required(),
  docType: Joi.string().allow("").required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  docNote: Joi.string().allow("").optional(),
  docStatus: Joi.string().allow("").optional(),
  markedPublic,
  sentToEmma,
  docAction: Joi.string().allow("").optional(),
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  _id: Joi.string().required().optional()
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().required(),
  actTranDocuments: Joi.array()
    .min(1)
    .items(docListSchema)
    .required()
})

export const DocumentsOthersValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, DocDetailsDealsSchema, {
    abortEarly: false,
    stripUnknown: false
  })

export const pricingErrorHandling = (req, res, next) => {
  const errors = PricingDetailsValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const createErrorHandling = (req, res, next) => {
  const errors = CreateValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const OthersErrorHandling = (req, res, next) => {
  const { type } = req.params
  let errors
  if (type === "details") {
    errors = UpdateValidate(req.body)
  }
  if (type === "participants") {
    errors = ParticipantsValidate(req.body)
  }
  if (type === "underwriters") {
    errors = PartUnderWritersValidate(req.body)
  }
  if (type === "documents") {
    errors = DocumentsOthersValidate(req.body)
  }
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}
