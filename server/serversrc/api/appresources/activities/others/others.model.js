import mongoose, { Schema } from "mongoose"
import timestamps from "mongoose-timestamp"
import { Entity } from "../../entity/entity.model"
import { EntityUser } from "../../entityUser/entityUser.model"
import { checklistsSchema } from "../../config/config.model"

// mongoose.Promise = global.Promise

const UnderwriterSchema = Schema({
  partFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  partType: String,
  partFirmName: String,
  roleInSyndicate: String,
  liabilityPerc: Number,
  managementFeePerc: Number,
  partContactAddToDL: Boolean
})

const PartDistSchema = Schema({
  partType: String,
  partFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  partFirmName: String, // May not need this
  partContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  partContactName: String,
  partContactEmail: String, // Same reasoning as above
  partUserAddress: { type: Schema.Types.ObjectId }, // Same reasoning as above
  partContactAddrLine1: String, // Same reasoning as above
  partContactAddrLine2: String, // Same reasoning as above
  participantGoogleAddress: String, // Same reasoning as above
  participantState: String, // Same reasoning as above
  partContactPhone: String,
  partContactFax: String,
  partContactTitle: String,
  addToDL: Boolean,
  createdDate: Date
})

const seriesRatingSchema = Schema({
  seriesId: { type: Schema.Types.ObjectId },
  seriesName: String,
  ratingAgencyName: String,
  longTermRating: String,
  longTermOutlook: String,
  shortTermOutlook: String,
  shortTermRating: String
})

const CepRatingsSchema = Schema({
  cepName: String,
  seriesId: { type: Schema.Types.ObjectId },
  seriesName: String,
  cepType: String,
  longTermRating: String,
  longTermOutlook: String,
  shortTermOutlook: String,
  shortTermRating: String
})

const seriesPricingSchema = Schema({
  seriesPrincipal: Number,
  seriesSecurityType: String,
  seriesSecurity: String,
  seriesSecurityDetails: String,
  seriesDatedDate: Date,
  seriesSettlementDate: Date,
  seriesFirstCoupon: Date,
  seriesPutDate: Date,
  seriesRecordDate: Date,
  seriesCallDate: Date,
  seriesFedTax: String,
  seriesStateTax: String,
  seriesPricingAMT: String,
  seriesBankQualified: String,
  seriesAccrueFrom: String,
  seriesPricingForm: String,
  seriesCouponFrequency: String,
  seriesDayCount: String,
  seriesRateType: String,
  seriesCallFeature: String,
  seriesCallPrice: Number,
  seriesInsurance: String,
  seriesUnderwtiersInventory: String,
  seriesMinDenomination: Number,
  seriesSyndicateStructure: String,
  seriesGrossSpread: Number,
  seriesEstimatedTakeDown: Number,
  seriesInsuranceFee: Number
})

const PricingDataSchema = Schema({
  term: String,
  maturityDate: Date,
  amount: Number,
  coupon: Number,
  yield: Number,
  price: Number,
  YTM: Number,
  cusip: String,
  callDate: Date,
  takeDown: Number,
  insured: Boolean,
  drop: Boolean
})

const seriesInfo = Schema({
  contactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  seriesName: String,
  description: String,
  tag: String,
  seriesRatings: [seriesRatingSchema],
  cepRatings: [CepRatingsSchema],
  seriesPricingDetails: seriesPricingSchema,
  seriesPricingData: [PricingDataSchema]
})

const DocumentsSchema = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: String,
  docFileName: String,
  docNote: String,
  docStatus: String,
  settings: {
    selectLevelType: String,
    firms: Array,
    users: Array
  },
  markedPublic: { publicFlag: Boolean, publicDate: Date },
  sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
  createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const tranAgencyOthersSchema = Schema({
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  actTranFirmId: { type: Schema.Types.ObjectId, ref: Entity }, // this is the financial advisor who is the client of munivisor
  actTranFirmName: String,
  actTranClientId: { type: Schema.Types.ObjectId, ref: Entity }, // this is the issuer client
  actTranClientName: String,
  actTranBorrowerName: String,
  actTranGuarantorName: String,
  actTranOtherSubtype: String,
  actTranUniqIdentifier: String,
  actTranType: String,
  actTranPurposeOfRequest: String,
  actTranRelatedTo: [
    Schema({
      relTranId: { type: Schema.Types.ObjectId },
      relType: String,
      relTranIssueName: String,
      relTranProjectName: String,
      relTranClientId: { type: Schema.Types.ObjectId },
      relTranClientName: String,
      tranType: String
    })
  ],
  actTranState: String,
  actTranCounty: String,
  actTranPrimarySector: String,
  actTranSecondarySector: String,
  actTranDateHired: Date,
  actTranStartDate: Date,
  actTranExpectedEndDate: Date,
  actTranStatus: String,
  actTranNotes: String,
  tranNotes: [notesSchema],
  actTranSubType: String,
  actTranOtherTransactionType: String,
  actTranFirmLeadAdvisorId: { type: Schema.Types.ObjectId },
  actTranFirmLeadAdvisorName: String,
  actTranIsClientMsrbRegistered: Boolean,
  actTranIsConduitBorrowerFlag: String,
  actTranIssueName: String,
  actTranProjectDescription: String,

  actTranOfferingType: String,
  actTranSecurityType: String,
  actTranBankQualified: String,
  actTranCorpType: String,
  actTranParAmount: Number,
  actTranPricingDate: Date,
  actTranExpAwardDate: Date,
  actTranActAwardDate: Date,
  actTranSdlcCreditPerc: Number,
  actTranEstimatedRev: Number,
  actTranUseOfProceeds: String,

  actTranUnderwriters: [UnderwriterSchema],
  participants: [PartDistSchema],
  actTranSeriesDetails: [seriesInfo],
  actTranDocuments: [DocumentsSchema],
  actTranChecklists: [checklistsSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  actTranCostOfIssuanceNotes: String,
  actTranPlaceholder: String,
  created_at: { type: Date, required: true, default: Date.now },
  updateDate: { type: Date, required: true, default: Date.now },
  updateUser: { type: Schema.Types.ObjectId, ref: EntityUser },
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

tranAgencyOthersSchema.index({actTranFirmId:1})
tranAgencyOthersSchema.index({actTranClientId:1})
tranAgencyOthersSchema.index({"participants.partFirmId":1})
tranAgencyOthersSchema.index({"actTranUnderwriters.partFirmId":1})


export const Others =  mongoose.model("tranagencyothers", tranAgencyOthersSchema)
