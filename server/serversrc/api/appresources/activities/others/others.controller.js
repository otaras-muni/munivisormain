import {generateControllers} from "../../../modules/query"
import {Entity, EntityUser, Others} from "./../../models"
import {canUserPerformAction} from "../../../commonDbFunctions"
import {manageTasksForProcessChecklists} from "../../taskmanagement/taskProcessInteraction"
import {checkTranEligiblityForLoggedInUser} from "../../../commonDbFunctions/getEligibleTransactionsForLoggedInUser"
import {putData, getData, elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../../entitlementhelpers"
import {RelatedToAssign} from "../../commonservices/services.controller"

const {ObjectID} = require("mongodb")

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Others, _id: id, esType: "tranagencyothers"})

const postOthersTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]

  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (entitled) {
      const { actTranRelatedTo } = req.body
      const newTran = new Others({
        ...req.body
      })
      const transaction = await newTran.save()

      await RelatedToAssign(transaction, actTranRelatedTo, "Others", "multi")

      const es = returnFromES(req.user.tenantId, transaction._id)
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      await Promise.all([ent, es])
      res.json(transaction)
    } else {
      res
        .status(500)
        .send({done: false, error: "User not entitled to create transaction"})
    }
  } catch (error) {
    console.log("******Others*******postOthersTransaction*********", error)
    res
      .status(500)
      .send({done: false, error: "Error saving information on Others"})
  }
}

const getOthersTransactionDetails = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "Others",
      access: 1
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    // console.log("------CHECKING THE ENTITLEMENTS THAT ARE BEING RRETURNED FROM
    // SERVICE", {entitled,isEligible})

    if (/* !entitled || */
      isEligible && !isEligible.canViewTran) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    if (type === "summary") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranRelatedTo actTranFirmId " +
          "actTranFirmName actTranClientId actTranClientName actTranClientMsrbType actTranI" +
          "sConduitBorrowerFlag actTranPrimarySector actTranSecondarySector actTranFirmLead" +
          "AdvisorId actTranFirmLeadAdvisorName actTranIssueName actTranProjectDescription " +
          "opportunity actTranSecurityType actTranBorrowerName actTranGuarantorName actTran" +
          "Notes actTranOfferingType actTranPricingDate actTranExpAwardDate actTranActAward" +
          "Date actTranSdlcCreditPercactTranEstimatedRev actTranUseOfProceeds actTranPlaceh" +
          "older actTranCorpType actTranParAmount actTranBankQualified actTranCounty actTra" +
          "nState docStatus created_at tranNotes"
    }
    if (type === "details") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranRelatedTo actTranFirmId " +
          "actTranFirmName actTranClientId actTranClientName actTranClientMsrbType actTranI" +
          "sConduitBorrowerFlag actTranPrimarySector actTranSecondarySector actTranFirmLead" +
          "AdvisorId actTranFirmLeadAdvisorName actTranNotes actTranOfferingType actTranPri" +
          "cingDate actTranExpAwardDate actTranActAwardDate actTranSdlcCreditPerc actTranEs" +
          "timatedRev actTranUseOfProceeds actTranPlaceholder actTranCorpType actTranParAmo" +
          "unt actTranBankQualified actTranSecurityType actTranCounty actTranState actTranG" +
          "uarantorName actTranBorrowerName actTranProjectDescription actTranIssueName docS" +
          "tatus  OnBoardingDataMigrationHistorical historicalData created_at tranNotes"
    }
    if (type === "participants") {
      select = "actTranClientId actTranFirmId actTranRelatedTo actTranFirmName actTranFirmName a" +
          "ctTranUnderwriters actTranType participants actTranSubType actTranClientName act" +
          "TranIssueName actTranProjectDescription"
    }
    if (type === "documents") {
      select = "actTranFirmId actTranClientId actTranType actTranIssueName actTranProjectDescrip" +
          "tion actTranChecklists actTranDocuments participants"
    }
    if (type === "rating") {
      select = "actTranFirmId  actTranClientId actTranType actTranSubType actTranIssueName actTr" +
          "anProjectDescription actTranSeriesDetails.seriesRatings actTranSeriesDetails.cep" +
          "Ratings participants"
    }
    if (type === "pricing") {
      select = "actTranFirmId actTranClientId actTranType actTranIssueName actTranProjectDescrip" +
          "tion  actTranSeriesDetails._id actTranSeriesDetails.seriesName actTranSeriesDeta" +
          "ils.description actTranSeriesDetails.tag participants actTranSecurityType "
    }
    if (type === "series" || type === "ratings") {
      select = "actTranFirmId actTranClientId actTranType actTranIssueName actTranProjectDescrip" +
          "tion actTranSeriesDetails.seriesRatings actTranSeriesDetails.cepRatings actTranS" +
          "eriesDetails._id actTranSeriesDetails.seriesName actTranSeriesDetails.descriptio" +
          "n actTranSeriesDetails.tag participants"
    }
    if (type === "check-track") {
      select = "actTranFirmId actTranClientId actTranType actTranIssueName actTranProjectDescrip" +
          "tion actTranChecklists participants"
    }
    if (type === "audit-trail") {
      select = "actTranFirmId actTranClientId actTranType"
    }

    const transaction = await Others
      .findById({_id: tranId})
      .select(`${select} participants actTranStatus`)
      .populate([
        {
          path: "actTranAssignedTo",
          model: EntityUser,
          select: "userFirstName userLastName userFirmName entityId userPhone userEmails userAddres" +
              "ses"
        }, {
          path: "actTranClientId",
          model: Entity,
          select: "addresses"
        }
      ])

    let otherTran = {}
    if (transaction && transaction.actTranClientId && transaction.actTranClientId.addresses) {
      let address = transaction
        .actTranClientId
        .addresses
        .find(addr => addr.isPrimary)
      address = address || transaction.actTranClientId.addresses[0]
      otherTran = {
        ...transaction._doc,
        entityAddress: address || {},
        actTranClientId: transaction.actTranClientId._id
      }
    }
    // transaction.entityAddress = transaction.actTranClientId.addresses
    // transaction.actTranClientId = transaction.actTranClientId._id
    await returnFromES(req.user.tenantId, tranId)
    res.json(select
      ? otherTran
      : {})
  } catch (error) {
    console.log("******Others*******getOthersTransactionDetails*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const updateTransaction = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    let tasks = []
    if (type === "details" || type === "summary") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranRelatedTo actTranFirmId " +
          "actTranFirmName actTranClientId actTranClientName actTranClientMsrbType actTranI" +
          "sConduitBorrowerFlag actTranPrimarySector actTranSecondarySector actTranFirmLead" +
          "AdvisorId actTranNotes actTranFirmLeadAdvisorName actTranIssueName actTranSecuri" +
          "tyType actTranOfferingType actTranProjectDescription actTranStatus actTranBorrow" +
          "erName actTranGuarantorName actTranPricingDate actTranExpAwardDate actTranActAwa" +
          "rdDate actTranSdlcCreditPerc actTranEstimatedRev actTranUseOfProceeds actTranPla" +
          "ceholder actTranCorpType actTranParAmount actTranBankQualified actTranCounty act" +
          "TranState docStatus created_at"
      await Others.findByIdAndUpdate({
        _id: tranId
      }, req.body)
    }
    if (type === "documents") {
      select = "actTranClientId  actTranFirmId actTranType actTranDocuments actTranProjectDescri" +
          "ption actTranIssueName docStatus docCategory docCategory docType docAWSFileLocat" +
          "ion docFileName"
      await Others.update({
        _id: tranId
      }, {
        $addToSet: {
          actTranDocuments: req.body.actTranDocuments
        }
      })
    }
    if (type === "series") {
      select = "actTranClientId  actTranFirmId actTranSeriesDetails.seriesName actTranSeriesDeta" +
          "ils.seriesPricingDetails actTranSeriesDetails.seriesPricingData actTranSeriesDet" +
          "ails.description actTranSeriesDetails.tag actTranProjectDescription actTranIssue" +
          "Name"
      await Others.update({
        _id: tranId
      }, {
        $addToSet: {
          actTranSeriesDetails: req.body.actTranSeriesDetails
        }
      })
    }
    if (type === "check-track") {
      select = "actTranClientId  actTranFirmId actTranChecklists actTranProjectDescription actTr" +
          "anIssueName participants"
      await Others.update({
        _id: tranId
      }, req.body)
      tasks = manageTasksForProcessChecklists(tranId, req.user)
    }
    const transaction = Others
      .findOne({_id: tranId})
      .select(select)
    const es = returnFromES(req.user.tenantId, tranId)
    const [transactionData] = await Promise.all([transaction, tasks, es])
    res.json(select
      ? transactionData
      : {})
  } catch (error) {
    console.log("******Others*******updateTransaction*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const removeParticipantByPartId = () => async(req, res) => {
  const {type, tranId} = req.params
  const {removeFrom} = req.query
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (removeFrom === "participants") {
      await Others.update({
        _id: tranId
      }, {
        $pull: {
          participants: {
            _id: type
          }
        }
      })
    }
    if (removeFrom === "actTranUnderwriters") {
      await Others.update({
        _id: tranId
      }, {
        $pull: {
          actTranUnderwriters: {
            _id: type
          }
        }
      })
    }
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const transaction = Others
      .findOne({_id: tranId})
      .select(`${removeFrom}`)
    const es = returnFromES(req.user.tenantId, tranId)
    const [transactionData] = await Promise.all([transaction, es, ent])
    res.json(removeFrom
      ? transactionData
      : "Not found")
  } catch (error) {
    console.log("******Others*******removeParticipantByPartId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getTranRatings = () => async(req, res) => {
  const {contactId, tranId} = req.params
  const resRequested = [
    {
      resource: "Others",
      access: 1
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canViewTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let transaction = await Others.aggregate([
      {
        $match: {
          _id: ObjectID(tranId)
        }
      }, {
        $unwind: "$actTranSeriesDetails"
      }, {
        $match: {
          "actTranSeriesDetails.contactId": ObjectID(contactId)
        }
      }, {
        $project: {
          actTranFirmId: 1,
          "actTranSeriesDetails.contactId": 1,
          "actTranSeriesDetails.seriesRatings": 1,
          "actTranSeriesDetails.cepRatings": 1
        }
      }
    ])
    transaction = Array.isArray(transaction) && transaction.length
      ? transaction[0]
      : transaction
    res.json(transaction)
  } catch (error) {
    console.log("******Others*******getTranRatings*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getSeriesBySeriesId = () => async(req, res) => {
  const {tranId, seriesId} = req.params
  const resRequested = [
    {
      resource: "Others",
      access: 1
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canViewTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let transaction = await Others.aggregate([
      {
        $match: {
          _id: ObjectID(tranId)
        }
      }, {
        $unwind: "$actTranSeriesDetails"
      }, {
        $match: {
          "actTranSeriesDetails._id": ObjectID(seriesId)
        }
      }, {
        $project: {
          "actTranSeriesDetails._id": 1,
          "actTranSeriesDetails.seriesName": 1,
          "actTranSeriesDetails.seriesPricingDetails": 1,
          "actTranSeriesDetails.seriesPricingData": 1,
          "actTranSeriesDetails.description": 1,
          "actTranSeriesDetails.tag": 1
        }
      }
    ])
    transaction = Array.isArray(transaction) && transaction.length
      ? transaction[0]
      : transaction
    res.json(transaction)
  } catch (error) {
    console.log("******Others*******getSeriesBySeriesId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const savePricingBySeriesId = () => async(req, res) => {
  const {seriesId, tranId} = req.params
  const {seriesPricingData, seriesPricingDetails} = req.body
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  // const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

  if (/* !entitled || */
    isEligible && !isEligible.canEditTran) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  return Others
    .findOne({_id: tranId})
    .then(async rating => {
      const findIndex = rating
        .actTranSeriesDetails
        .findIndex(p => p._id.toString() === seriesId)
      if (findIndex !== -1) {
        rating.actTranSeriesDetails[findIndex].seriesPricingDetails = seriesPricingDetails
        rating.actTranSeriesDetails[findIndex].seriesPricingData = seriesPricingData
        await rating.save()
        const es = returnFromES(req.user.tenantId, tranId)
        await Promise.all([es])
        res.json(rating.actTranSeriesDetails[findIndex])
      } else {
        res
          .status(400)
          .json("series not found")
      }
    })
    .catch(error => {
      console.log("******Others*******savePricingBySeriesId*********", error)
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const delSeriesBySeriesId = () => async(req, res) => {
  const {seriesId, tranId} = req.params
  const resRequested = [
    {
      resource: "Others",
      access: 1
    }
  ]
  // const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

  if (/* !entitled || */
    isEligible && !isEligible.canEditTran) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  await Others
    .findOne({_id: tranId})
    .then(async series => {
      series.actTranSeriesDetails = series
        .actTranSeriesDetails
        .filter(p => p._id.toString() !== seriesId)
      await series.save()
      const es = returnFromES(req.user.tenantId, tranId)
      await Promise.all([es])
      res.json(series.actTranSeriesDetails)
    })
    .catch(error => {
      console.log("******Others*******delSeriesBySeriesId*********", error)
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const saveSeriesBySeriesId = () => async(req, res) => {
  const {seriesId, tranId} = req.params
  const {seriesName, tag, description} = req.body
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  // const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

  if (/* !entitled || */
    isEligible && !isEligible.canEditTran) {
    return res
      .status(422)
      .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
  }
  return Others
    .findOne({_id: tranId})
    .then(async series => {
      const findIndex = series
        .actTranSeriesDetails
        .findIndex(p => p._id.toString() === seriesId)
      if (findIndex !== -1) {
        series.actTranSeriesDetails[findIndex].seriesName = seriesName
        series.actTranSeriesDetails[findIndex].tag = tag
        series.actTranSeriesDetails[findIndex].description = description
        await series.save()
        const es = returnFromES(req.user.tenantId, tranId)
        await Promise.all([es])
        res.json(series.actTranSeriesDetails[findIndex])
      } else {
        res
          .status(400)
          .json("series not found")
      }
    })
    .catch(error => {
      console.log("******Others*******saveSeriesBySeriesId*********", error)
      res
        .status(422)
        .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
    })
}

const saveParticipants = () => async(req, res) => {
  const {type, tranId} = req.params
  const {_id} = req.body
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (type === "underwriters") {
      if (_id) {
        await Others.updateOne({
          _id: tranId,
          "actTranUnderwriters._id": ObjectID(_id)
        }, {
          $set: {
            "actTranUnderwriters.$": req.body
          }
        })
      } else {
        await Others.update({
          _id: tranId
        }, {
          $addToSet: {
            actTranUnderwriters: req.body
          }
        })
      }
      const UnderWriter = Others
        .findById({_id: tranId})
        .select("actTranUnderwriters")
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      const es = returnFromES(req.user.tenantId, tranId)
      const [UnderWriterData] = await Promise.all([UnderWriter, ent, es])
      return res.json(UnderWriterData)
    }
    if (type === "participants") {
      const isExists = await Others.findOne({
        _id: ObjectID(tranId)
      }, {
        participants: {
          $elemMatch: {
            partContactId: ObjectID(req.body.partContactId),
            partFirmId: ObjectID(req.body.partFirmId),
            partType: req.body.partType
          }
        }
      })
      if (isExists && isExists.participants && isExists.participants.length) {
        if (isExists.participants[0]._id.toString() !== (_id && _id.toString())) {
          return res
            .status(422)
            .json({error: "This user was already exists"})
        }
      }
      if (_id) {
        await Others.updateOne({
          _id: tranId,
          "participants._id": ObjectID(_id)
        }, {
          $set: {
            "participants.$": req.body
          }
        })
      } else {
        await Others.update({
          _id: tranId
        }, {
          $addToSet: {
            participants: req.body
          }
        })
      }
      const tranPart = Others
        .findById({_id: tranId})
        .select("participants")

      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      const es = returnFromES(req.user.tenantId, tranId)
      const [tranPartData] = await Promise.all([tranPart, ent, es])
      return res.json(tranPartData)
    }

    res
      .status(404)
      .json("not found")
  } catch (error) {
    console.log("******Others*******saveParticipants*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const saveRatingsByTranId = () => async(req, res) => {
  const {type, tranId} = req.params
  const {oldSeriesId} = req.query
  const {_id, seriesId} = req.body
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  try {
  // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let srIndex,
      ratingIndex
    const rating = await Others
      .findOne({_id: tranId})
      .select("actTranSeriesDetails")
    if (_id) {
      if (type === "otherAgencyRatings") {
        srIndex = rating
          .actTranSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        ratingIndex = rating
          .actTranSeriesDetails[srIndex]
          .seriesRatings
          .findIndex(x => x._id.toString() === _id)

        if(!rating.actTranSeriesDetails[srIndex].seriesRatings[ratingIndex]){
          const existSrIndex = rating
            .actTranSeriesDetails
            .findIndex(x => x._id.toString() === oldSeriesId)
          const existRatingIndex = rating
            .actTranSeriesDetails[existSrIndex]
            .seriesRatings
            .findIndex(x => x._id.toString() === _id)
          rating.actTranSeriesDetails[existSrIndex].seriesRatings.splice(existRatingIndex, 1)
          delete req.body._id

          rating.actTranSeriesDetails[srIndex].seriesRatings.push(req.body)
        }else {
          rating.actTranSeriesDetails[srIndex].seriesRatings[ratingIndex] = Object.assign({}, rating.actTranSeriesDetails[srIndex].seriesRatings[ratingIndex], req.body)
        }
      } else {
        srIndex = rating
          .actTranSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        ratingIndex = rating
          .actTranSeriesDetails[srIndex]
          .cepRatings
          .findIndex(x => x._id.toString() === _id)

        if(!rating.actTranSeriesDetails[srIndex].cepRatings[ratingIndex]){
          const existSrIndex = rating
            .actTranSeriesDetails
            .findIndex(x => x._id.toString() === oldSeriesId)
          const existRatingIndex = rating
            .actTranSeriesDetails[existSrIndex]
            .cepRatings
            .findIndex(x => x._id.toString() === _id)

          rating.actTranSeriesDetails[existSrIndex].cepRatings.splice(existRatingIndex, 1)
          delete req.body._id

          rating.actTranSeriesDetails[srIndex].cepRatings.push(req.body)
        }else {
          rating.actTranSeriesDetails[srIndex].cepRatings[ratingIndex] = Object.assign({}, rating.actTranSeriesDetails[srIndex].cepRatings[ratingIndex], req.body)
        }
      }
      await rating.save()
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      const es = returnFromES(req.user.tenantId, tranId)
      const saveRating = Others
        .findById({_id: tranId})
        .select("actTranSeriesDetails")
      const [ratingInformation] = await Promise.all([saveRating, ent, es])
      res.json(ratingInformation.actTranSeriesDetails)
    } else {
      if (type === "otherAgencyRatings") {
        srIndex = rating
          .actTranSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        rating
          .actTranSeriesDetails[srIndex]
          .seriesRatings
          .push(req.body)
      } else {
        srIndex = rating
          .actTranSeriesDetails
          .findIndex(x => x._id.toString() === seriesId)
        rating
          .actTranSeriesDetails[srIndex]
          .cepRatings
          .push(req.body)
      }
      await rating.save()
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      const es = returnFromES(req.user.tenantId, tranId)
      const saveRating = Others
        .findById({_id: tranId})
        .select("actTranSeriesDetails")
      const [ratingInformation] = await Promise.all([saveRating, ent, es])
      res.json(ratingInformation.actTranSeriesDetails)
    }
  } catch (error) {
    console.log("******Others*******saveRatingsByTranId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const delRatingByRatingId = () => async(req, res) => {
  const {type, tranId} = req.params
  const {seriesId, ratingId} = req.query
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)

    if (/* !entitled || */
      isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (type === "otherAgencyRatings") {
      await Others.update({
        _id: ObjectID(tranId),
        "actTranSeriesDetails._id": ObjectID(seriesId)
      }, {
        $pull: {
          "actTranSeriesDetails.$.seriesRatings": {
            _id: ObjectID(ratingId)
          }
        }
      })
    } else {
      await Others.update({
        _id: ObjectID(tranId),
        "actTranSeriesDetails._id": ObjectID(seriesId)
      }, {
        $pull: {
          "actTranSeriesDetails.$.cepRatings": {
            _id: ObjectID(ratingId)
          }
        }
      })
    }
    const rating = Others
      .findById({_id: tranId})
      .select("actTranSeriesDetails")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [ratingData] = await Promise.all([rating, ent, es])
    res.json(ratingData.actTranSeriesDetails)
  } catch (error) {
    console.log("******Others*******delRatingByRatingId*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putRelatedTran = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  const {tranId} = req.params
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, tranId)
    const select = "actTranType actTranSubType actTranUniqIdentifier actTranRelatedTo actTranFirmId " +
        "actTranFirmName actTranClientId actTranClientName actTranClientMsrbType actTranI" +
        "sConduitBorrowerFlag actTranPrimarySector actTranSecondarySector actTranFirmLead" +
        "AdvisorId actTranFirmLeadAdvisorName actTranIssueName actTranProjectDescription " +
        "actTranBorrowerName actTranSecurityType actTranGuarantorName actTranRelatedTo"

    if (/* !entitled || */
      isEligible && !isEligible.canEditTran) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await Others.update({
      _id: tranId
    }, {
      $addToSet: {
        actTranRelatedTo: req.body
      }
    })

    const othersDetails =  await Others.findById(tranId).select("actTranIssueName actTranProjectDescription actTranClientId actTranClientName")
    await RelatedToAssign(othersDetails, req.body, "Others", "single")

    const otherRelatedTran = Others
      .findById({_id: tranId})
      .select(select)
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [otherRelatedTranData] = await Promise.all([otherRelatedTran, ent, es])
    res.json(otherRelatedTranData)
  } catch (error) {
    console.log("******Others*******putRelatedTran*********", error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export const postUnderwriterCheckAddToDL = () => async(req, res) => {
  const {type, tranId} = req.params
  try {
    const {addToDL} = req.body

    await Others.updateOne({
      _id: ObjectID(tranId),
      "actTranUnderwriters._id": ObjectID(type)
    }, {
      $set: {
        "actTranUnderwriters.$.partContactAddToDL": addToDL
      }
    })

    const underwriter = await Others
      .findOne({_id: tranId})
      .select("actTranUnderwriters")
    let updateUnderwriter = null

    if (underwriter) {
      updateUnderwriter = underwriter
        .actTranUnderwriters
        .find(p => p._id.toString() === type)
    }
    await returnFromES(req.user.tenantId, tranId)
    res
      .status(200)
      .json({done: true, updateUnderwriter})
  } catch (error) {
    console.log("===========================", error.message)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

const postOthersNotes = () => async(req, res) => {
  const {tranId} = req.params
  const {_id} = req.body
  const resRequested = [
    {
      resource: "Others",
      access: 2
    }
  ]
  try {

    if (_id) {
      await Others.updateOne({
        _id: ObjectID(tranId),
        "tranNotes._id": ObjectID(_id)
      }, {
        $set: {
          "tranNotes.$": req.body
        }
      })
    } else {
      await Others.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          tranNotes: req.body
        }
      })
    }
    const others = await Others.findOne({_id: tranId}).select("tranNotes")
    const es = returnFromES(req.user.tenantId, tranId)
    await Promise.all([others, es])
    res.json(others)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export default generateControllers(Others, {
  getOthersTransactionDetails: getOthersTransactionDetails(),
  updateTransaction: updateTransaction(),
  removeParticipantByPartId: removeParticipantByPartId(),
  getTranRatings: getTranRatings(),
  saveRatingsByTranId: saveRatingsByTranId(),
  getSeriesBySeriesId: getSeriesBySeriesId(),
  savePricingBySeriesId: savePricingBySeriesId(),
  delSeriesBySeriesIds: delSeriesBySeriesId(),
  saveSeriesBySeriesId: saveSeriesBySeriesId(),
  // pullDealParticipantsbyPuId: pullDealParticipantsbyPuId(),
  saveParticipants: saveParticipants(),
  // saveDealParticipantsbyPuId: saveDealParticipantsbyPuId(),
  delRatingByRatingId: delRatingByRatingId(),
  putRelatedTran: putRelatedTran(),
  postOthersTransaction: postOthersTransaction(),
  postUnderwriterCheckAddToDL: postUnderwriterCheckAddToDL(),
  postOthersNotes: postOthersNotes()
})
