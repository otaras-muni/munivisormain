import Joi from "joi-browser"

const derivativeSummary = Joi.object().keys({
  tranIssuer: Joi.string()
    .allow("",null)
    .optional(),
  tranProjectDescription: Joi.string()
    .allow("",null)
    .optional(),
  tranType: Joi.string().required(),
  tranStatus: Joi.string()
    .allow("",null)
    .optional(),
  tranBorrowerName: Joi.string()
    .allow("",null)
    .optional(),
  tranGuarantorName: Joi.string()
    .allow("",null)
    .optional(),
  tranNotionalAmt: Joi.number().required(),
  tranNotionalAmtFlag: Joi.boolean()
    .required()
    .optional(),
  tranTradeDate: Joi.date().required(),
  tranEffDate: Joi.date().required(),
  tranEndDate: Joi.date().required(),
  tranCounterpartyClient: Joi.string().required(),
  tranCounterpartyDealer: Joi.string().required(),
  tranCounterpartyType: Joi.string()
    .allow("",null)
    .optional(),
  tranLastMTMValuation: Joi.number()
    .allow("",null)
    .optional(),
  tranEstimatedRev: Joi.number()
    .required(),
  tranLastMTMDate: Joi.date()
    .allow(null)
    .required()
    .optional(),
  tranMTMProvider: Joi.string()
    .allow("",null)
    .optional(),
  createdAt: Joi.date()
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

const derivTradeClientPayLeg = Joi.object().keys({
  tradeDate: Joi.date()
    .label("Trade Date")
    .example(new Date("2016-01-01"))
    .required(),
  effDate: Joi.date()
    .label("Effective Date")
    .example(new Date("2016-01-01"))
    .required(),
  periodEndDate: Joi.date()
    .label("Period End Date")
    .example(new Date("2016-01-01"))
    .allow(null)
    .optional(),
})

const derivTradeDealerPayLeg = Joi.object().keys({
  tradeDate: Joi.date()
    .label("Trade Date")
    .example(new Date("2016-01-01"))
    .required(),
  effDate: Joi.date()
    .label("Effective Date")
    .example(new Date("2016-01-01"))
    .required(),
  periodEndDate: Joi.date()
    .label("Period End Date")
    .example(new Date("2016-01-01"))
    .allow(null)
    .optional(),
})

const derivativesSchema = Joi.object().keys({
  actTranSecondarySector: Joi.string().allow(""),
  actTranPrimarySector: Joi.string().allow(""),
  actTranClientId: Joi.string()
    .required()
    .optional(),
  actTranClientName: Joi.string()
    .required()
    .optional(),
  actTranIssueName: Joi.string()
    .allow("")
    .optional(),
  derivativeSummary,
  derivTradeClientPayLeg,
  derivTradeDealerPayLeg
})

export const DerivativesValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, derivativesSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean()
    .required()
    .optional(),
  publicDate: Joi.string()
    .required()
    .optional()
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean()
    .required()
    .optional(),
  emmaSendDate: Joi.string()
    .required()
    .optional()
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().allow("").required(),
  docSubCategory: Joi.string().allow("").required(),
  docType: Joi.string().allow("").required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  docNote: Joi.string().allow("").optional(),
  docStatus: Joi.string().allow("").optional(),
  markedPublic,
  sentToEmma,
  docAction: Joi.string().allow("").optional(),
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  _id: Joi.string().required().optional()
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().required(),
  derivativeDocuments: Joi.array()
    .min(1)
    .items(docListSchema)
    .required()
})

export const DocumentsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, DocDetailsDealsSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const derivTradeSwapAndCepSchema = Joi.object().keys({
  payor: Joi.string()
    .label("Payor")
    .required(),
  currency: Joi.string()
    .label("Currency")
    .required(),
  paymentType: Joi.string()
    .label("Payment Type")
    .required(),
  upfrontPayment: Joi.number()
    .label("Upfront Payment")
    .required(),
  tradeDate: Joi.date()
    .label("Trade Date")
    .example(new Date("2016-01-01"))
    /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
    .required(),
  effDate: Joi.date()
    .label("Effective Date")
    .example(new Date("2016-01-01"))
    /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
    .required(),
  fixedRate: Joi.number()
    .label("Fixed Rate")
    .allow("", null)
    .optional(),
  floatingRateType: Joi.string()
    .label("Floating Rate Type")
    .allow("", null)
    .optional(),
  floatingRateRatio: Joi.number()
    .label("Floating Rate Ratio")
    .allow("", null)
    .optional(),
  floatingRateSpread: Joi.number()
    .label("Floating Rate Spread")
    .allow("", null).optional(),
  firstPaymentDate: Joi.date()
    .label("First Payment Date")
    .example(new Date("2016-01-01"))
    .allow(null)
    /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
    .required()
    .optional(),
  stub: Joi.string()
    .allow("", null)
    .label("Stub")
    .optional(),
  paymentSchedule: Joi.string()
    .label("Payment Schedule")
    .allow("",null)
    .optional(),
  paymentBusConvention: Joi.string()
    .label("Payment Business Convention")
    .allow("", null)
    .optional(),
  periodEndDate: Joi.date().label("Period End Date").example(new Date("2016-01-01")).required(),
  periodEndDateBusConvention: Joi.string()
    .label("Date Business Convention")
    .allow("", null)
    .optional(),
  initialIndexSetting: Joi.string()
    .label("Initial Index Setting")
    .allow("",null)
    .optional(),
  dayCount: Joi.string()
    .allow("", null)
    .label("Day Count")
    .optional(),
  additionalPayments: Joi.string()
    .label("Additional payments")
    .allow("", null)
    .optional(),
  createdAt: Joi.date()
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional(),
})

const derivativeTrade = Joi.object().keys({
  derivTradeDealerPayLeg: derivTradeSwapAndCepSchema,
  derivTradeClientPayLeg: derivTradeSwapAndCepSchema
})

export const TradeValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, derivativeTrade, {
    abortEarly: false,
    stripUnknown: false
  })

const derivativePartSchema = Joi.object().keys({
  cntrParty: Joi.string().required(),
  cntrPartyFirmName: Joi.string().required(),
  cntrPartyFirmtype: Joi.string().required(),
  cntrPartyMoodysRating: Joi.string()
    .allow("")
    .optional(),
  cntrPartyFitchRating: Joi.string()
    .allow("")
    .optional(),
  cntrPartyKrollRating: Joi.string()
    .allow("")
    .optional(),
  cntrPartySPRating: Joi.string()
    .allow("")
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const DerivativePartValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, derivativePartSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const workingGroupSchema = Joi.object().keys({
  partType: Joi.string()
    .required()
    .optional(),
  partFirmId: Joi.string()
    .required()
    .optional(),
  partFirmName: Joi.string()
    .required()
    .optional(), // May not need this
  partContactTitle: Joi.string()
    .required()
    .optional(),
  partContactId: Joi.string()
    .required()
    .optional(),
  partContactName: Joi.string()
    .required()
    .optional(),
  partUserAddress: Joi.string()
    .allow("")
    .optional(),
  partContactEmail: Joi.string()
    .email()
    .required()
    .optional(), // Same reasoning as above
  partContactAddrLine1: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  partContactAddrLine2: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  participantGoogleAddress: Joi.string().allow("").optional(),
  participantState: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  partContactPhone: Joi.string().allow("").optional(),
  partContactFax: Joi.string().allow("").optional(),
  addToDL: Joi.boolean().optional(),
  createdDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const WorkingGroupValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, workingGroupSchema, {
    abortEarly: false,
    stripUnknown: false
  })

export const errorParticipants = (req, res, next) => {
  const { type } = req.params
  let errors
  if (type === "counterParties") {
    errors = DerivativePartValidate(req.body)
  }
  if (type === "workingGroup") {
    errors = WorkingGroupValidate(req.body)
  }
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}

export const errorHandling = (req, res, next) => {
  const { type } = req.params
  let errors
  if (type === "details") {
    errors = DerivativesValidate(req.body)
  }
  if (type === "documents") {
    errors = DocumentsValidate(req.body)
  }
  if (type === "tradeSwap") {
    errors = TradeValidate(req.body)
  }
  if (type === "tradeCap") {
    errors = TradeValidate(req.body)
  }
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}
