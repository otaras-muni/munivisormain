module.exports = {
  "properties": {
    "actTranType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranSubType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranUniqIdentifier": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranClientId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranClientName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranClientMsrbType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranIsClientMsrbRegistered": {
      "type": "boolean",
    },
    "actTranIsConduitBorrowerFlag": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranPrimarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranSecondarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmLeadAdvisorId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmLeadAdvisorName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranIssueName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranProjectDescription": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranRelatedTo": {
      "properties": {
        "relTranId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranIssueName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranProjectName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranClientId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranClientName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "actTranNotes": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "derivativeSummary": {
      "properties": {
        "tranIssuer": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranProjectDescription": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranPrimarySector": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranSecondaryType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranStatus": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranBorrowerName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranGuarantorName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranNotionalAmt": {
          "type": "float"
        },
        "tranNotionalAmtFlag": {
          "type": "boolean"
        },
        "tranTradeDate": {
          "type": "date"
        },
        "tranEffDate": {
          "type": "date"
        },
        "tranEndDate": {
          "type": "date"
        },
        "tranCounterpartyClient": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranCounterpartyDealer": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranCounterpartyType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tranLastMTMValuation": {
          "type": "float"
        },
        "tranLastMTMDate": {
          "type": "date"
        },
        "tranMTMProvider": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "derivativeCounterparties": {
      "properties": {
        "cntrParty": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "cntrPartyFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "cntrPartyFirmtype": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "cntrPartyMoodysRating": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "cntrPartyFitchRating": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "cntrPartyKrollRating": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "cntrPartySPRating": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "derivativeParticipants": {
      "properties": {
        "partType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddrLine1": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddrLine2": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "participantState": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactTitle": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "derivTradeClientPayLeg": {
      "properties": {
        "payor": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "currency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "upfrontPayment": {
          "type": "float"
        },
        "tradeDate": {
          "type": "date"
        },
        "effDate": {
          "type": "date"
        },
        "fixedRate": {
          "type": "float"
        },
        "floatingRateType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "floatingRateRatio": {
          "type": "float"
        },
        "floatingRateSpread": {
          "type": "float"
        },
        "firstPaymentDate": {
          "type": "date"
        },
        "stub": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentSchedule": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentBusConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "periodEndDate": {
          "type": "date"
        },
        "periodEndDateBusConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "initialIndexSetting": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dayCount": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "additionalPayments": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "derivTradeDealerPayLeg": {
      "properties": {
        "payor": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "currency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "upfrontPayment": {
          "type": "float"
        },
        "tradeDate": {
          "type": "date"
        },
        "effDate": {
          "type": "date"
        },
        "fixedRate": {
          "type": "float"
        },
        "floatingRateType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "floatingRateRatio": {
          "type": "float"
        },
        "floatingRateSpread": {
          "type": "float"
        },
        "firstPaymentDate": {
          "type": "date"
        },
        "stub": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentSchedule": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentBusConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "periodEndDate": {
          "type": "date"
        },
        "periodEndDateBusConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "initialIndexSetting": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dayCount": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "additionalPayments": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "derivAmortTrades": {
      "properties": {
        "reductionDate": {
          "type": "date"
        },
        "prinAmountReduction": {
          "type": "float"
        },
        "reviedPrinAmount": {
          "type": "float"
        }
      }
    },
    "derivativeChecklists": {
      "properties": {
        "type": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "id": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "name": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "attributedTo": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "bestPractice": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "notes": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "lastUpdated": {
          "properties": {
            "date": {
              "type": "date"
            },
            "by": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        },
        "data": {
          "properties": {
            "title": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "headers": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        }
      }
    },
    "derivativeDocuments": {
      "properties": {
        "docCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docSubCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docAWSFileLocation": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docFileName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "markedPublic": {
          "properties": {
            "publicFlag": {
              "type": "boolean"
            },
            "publicDate": {
              "type": "date"
            }
          }
        },
        "sentToEmma": {
          "properties": {
            "emmaSendFlag": {
              "type": "boolean"
            },
            "emmaSendDate": {
              "type": "date"
            }
          }
        },
        "LastUpdatedDate": {
          "type": "date"
        }
      }
    }
  }
}
