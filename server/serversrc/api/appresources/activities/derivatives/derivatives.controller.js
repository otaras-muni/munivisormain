import {generateControllers} from "./../../../modules/query"
import {Derivatives} from "./derivatives.model"
import {putData, getData, elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"
import {canUserPerformAction} from "../../../commonDbFunctions"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../../entitlementhelpers"
import {RelatedToAssign} from "../../commonservices/services.controller"

const {ObjectID} = require("mongodb")

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Derivatives, _id: id, esType: "tranderivatives"})

const postDerivativeTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Derivatives",
      access: 2
    }
  ]

  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (entitled) {
      const { actTranRelatedTo } = req.body
      const newDerivativeTransaction = new Derivatives({
        ...req.body
      })
      const insertedDerivativeTransaction = await newDerivativeTransaction.save()

      await RelatedToAssign(insertedDerivativeTransaction, actTranRelatedTo, "Derivative", "multi")

      const es = returnFromES(req.user.tenantId, insertedDerivativeTransaction._id)
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      await Promise.all([ent, es])
      res.json(insertedDerivativeTransaction)

    } else {
      res
        .status(500)
        .send({done: false, error: "User not entitled to create Derivative Transaction"})
    }
  } catch (error) {
    res
      .status(500)
      .send({done: false, error: "Error saving information on Derivative Transactions"})
  }
}

const postTransaction = () => async(req, res) => {
  try {
    const newDerivatives = new Derivatives({
      ...req.body
    })
    const derivative = await newDerivatives.save()
    // import { putData } from "../../../elasticsearch/esHelper"
    const es = returnFromES(req.user.tenantId, derivative._id)
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    await Promise.all([ent, es])
    res.json(derivative)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements"})
  }
}

const getTransactionDetails = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "Derivatives",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let select = ""
    if (type === "details" || type === "summary") {
      select = "actTranType actTranSubType actTranFirmId actTranClientName actTranClientId actTr" +
          "anClientMsrbType actTranProjectDescription OnBoardingDataMigrationHistorical his" +
          "toricalData actTranUniqIdentifier  actTranIssueName tranBorrowerName tranGuarant" +
          "orName actTranPrimarySector actTranSecondarySector opportunity derivativeSummary" +
          " actTranRelatedTo derivativeParticipants derivativeCounterparties derivTradeClie" +
          "ntPayLeg derivTradeDealerPayLeg actTranFirmLeadAdvisorName actTranNotes tranNotes"
    }
    if (type === "participants") {
      select = "actTranFirmId actTranFirmName actTranSubType actTranUniqIdentifier actTranClient" +
          "Id actTranClientName derivativeCounterparties derivativeParticipants actTranIssu" +
          "eName actTranProjectDescription"
    }
    if (type === "tradeSwap" || type === "tradeCap") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId actTranClient" +
          "Name derivTradeClientPayLeg derivTradeDealerPayLeg derivativeCounterparties deri" +
          "vativeSummary actTranClientName actTranIssueName actTranProjectDescription"
    }
    if (type === "documents") {
      select = "actTranFirmId actTranFirmName actTranIssueName actTranProjectDescription actTran" +
          "ClientId actTranClientName actTranType actTranSubType actTranUniqIdentifier deri" +
          "vativeDocuments derivativeParticipants"
    }
    if (type === "audit-trail") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId actTranClient" +
          "Name"
    }

    const derivatives = await Derivatives
      .findById({_id: tranId})
      .select(`${select} actTranStatus`)
    res.json(select
      ? derivatives
      : {})
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getUserTransactions = () => async(req, res) => {
  const {tranClientId} = req.params
  const resRequested = [
    {
      resource: "Derivatives",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const derivative = await Derivatives
      .find({actTranClientId: tranClientId})
      .select("actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranFirmName a" +
          "ctTranClientId actTranClientName actTranClientMsrbType actTranIsConduitBorrowerF" +
          "lag actTranPrimarySector actTranSecondarySector actTranFirmLeadAdvisorId actTran" +
          "FirmLeadAdvisorName actTranIssueName actTranProjectDescription derivativePartici" +
          "pants")
    res.json(derivative)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Derivatives",
      access: 2
    }
  ]
  const {type, tranId} = req.params
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let select = ""
    if (type === "summary") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranStatus actTranRelatedTo " +
          "actTranFirmId actTranFirmName actTranClientId actTranClientName actTranClientMsr" +
          "bType actTranIsConduitBorrowerFlag tranBorrowerName tranGuarantorName actTranPri" +
          "marySector actTranSecondarySector actTranFirmLeadAdvisorId actTranFirmLeadAdviso" +
          "rName actTranIssueName actTranProjectDescription derivativeSummary derivativeCou" +
          "nterparties derivativeParticipants derivTradeClientPayLeg derivTradeDealerPayLeg" +
          " actTranFirmLeadAdvisorName actTranNotes actTranStatus"
      await Derivatives.update({
        _id: tranId
      }, {
        $addToSet: {
          actTranRelatedTo: req.body
        }
      })

      const derivativeDetails =  await Derivatives.findById(tranId).select("actTranIssueName actTranProjectDescription actTranClientId actTranClientName")
      await RelatedToAssign(derivativeDetails, req.body, "Derivative", "single")

    }
    if (type === "details" || type === "status") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranRelatedTo actTranFirmId " +
          "actTranFirmName actTranClientId actTranClientName actTranClientMsrbType actTranI" +
          "sConduitBorrowerFlag actTranPrimarySector actTranSecondarySector actTranFirmLead" +
          "AdvisorId actTranFirmLeadAdvisorName actTranIssueName actTranProjectDescription " +
          "derivativeSummary actTranProjectDescription actTranStatus"
      await Derivatives.update({
        _id: tranId
      }, req.body)
    }
    if (type === "tradeSwap" || type === "tradeCap") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranClientId d" +
          "erivTradeClientPayLeg derivTradeDealerPayLeg actTranClientName"
      await Derivatives.update({
        _id: tranId
      }, req.body)
    }
    if (type === "documents") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranClientId d" +
          "erivativeDocuments derivativeParticipants"
      await Derivatives.update({
        _id: tranId
      }, {
        $addToSet: {
          derivativeDocuments: req.body.derivativeDocuments
        }
      })
    }
    const derivative = Derivatives
      .findOne({_id: tranId})
      .select(select)
    const es = returnFromES(req.user.tenantId, tranId)
    const [derivativeData] = await Promise.all([derivative, es])
    res.json(derivativeData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putParticipantsDetails = () => async(req, res) => {
  const resRequested = [
    {
      resource: "Derivatives",
      access: 2
    }
  ]
  const {tranId, type} = req.params
  const {_id} = req.body
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    /* if(type && (type.toString() !== "counterParties" || type.toString() !== "workingGroup")) {
      return res.status(422).json({done:false,error:"Not Found"})
    } */
    if (_id) {
      if (type === "counterParties") {
        await Derivatives.updateOne({
          _id: ObjectID(tranId),
          "derivativeCounterparties._id": ObjectID(_id)
        }, {
          $set: {
            "derivativeCounterparties.$": req.body
          }
        })
      } else {
        await Derivatives.updateOne({
          _id: ObjectID(tranId),
          "derivativeParticipants._id": ObjectID(_id)
        }, {
          $set: {
            "derivativeParticipants.$": req.body
          }
        })
      }
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      const derivative = Derivatives
        .findOne({_id: tranId})
        .select("derivativeParticipants derivativeCounterparties")
      const es = returnFromES(req.user.tenantId, tranId)

      const [derivativeData] = await Promise.all([derivative, ent, es])
      res.json(derivativeData)
    } else {
      if (type === "counterParties") {
        await Derivatives.updateOne({
          _id: ObjectID(tranId)
        }, {
          $addToSet: {
            derivativeCounterparties: req.body
          }
        })
      } else {
        await Derivatives.updateOne({
          _id: ObjectID(tranId)
        }, {
          $addToSet: {
            derivativeParticipants: req.body
          }
        })
      }
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      const derivative = Derivatives
        .findOne({_id: tranId})
        .select("derivativeParticipants derivativeCounterparties")
      const es = returnFromES(req.user.tenantId, tranId)
      const [derivativeData] = await Promise.all([derivative, ent, es])
      res.json(derivativeData)
    }
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const pullParticipantsDetails = () => async(req, res) => {
  const {tranId, type} = req.params
  const {id} = req.query
  const resRequested = [
    {
      resource: "Derivatives",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    /* if(type !== "counterParties" || type !== "workingGroup") {
      return res.status(422).json({done:false,error:"Not Found"})
    } */
    if (type === "counterParties") {
      await Derivatives.update({
        _id: tranId
      }, {
        $pull: {
          derivativeCounterparties: {
            _id: id
          }
        }
      })
    } else {
      await Derivatives.update({
        _id: tranId
      }, {
        $pull: {
          derivativeParticipants: {
            _id: id
          }
        }
      })
    }
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const derivative = Derivatives
      .findOne({_id: tranId})
      .select("derivativeParticipants derivativeCounterparties")
    const es = returnFromES(req.user.tenantId, tranId)
    const [derivativeData] = await Promise.all([derivative, ent, es])
    res.json(derivativeData)

    res.json(derivative)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const postDerivativeNotes = () => async(req, res) => {
  const {tranId} = req.params
  const {_id} = req.body
  const resRequested = [
    {
      resource: "Derivatives",
      access: 2
    }
  ]
  try {

    if (_id) {
      await Derivatives.updateOne({
        _id: ObjectID(tranId),
        "tranNotes._id": ObjectID(_id)
      }, {
        $set: {
          "tranNotes.$": req.body
        }
      })
    } else {
      await Derivatives.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          tranNotes: req.body
        }
      })
    }
    const derivative = await Derivatives.findOne({_id: tranId}).select("tranNotes")
    const es = returnFromES(req.user.tenantId, tranId)
    await Promise.all([derivative, es])
    res.json(derivative)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}


export default generateControllers(Derivatives, {
  postDerivativeTransaction: postDerivativeTransaction(),
  postTransaction: postTransaction(),
  getTransactionDetails: getTransactionDetails(),
  getUserTransactions: getUserTransactions(),
  putTransaction: putTransaction(),
  putParticipantsDetails: putParticipantsDetails(),
  pullParticipantsDetails: pullParticipantsDetails(),
  postDerivativeNotes: postDerivativeNotes()
})
