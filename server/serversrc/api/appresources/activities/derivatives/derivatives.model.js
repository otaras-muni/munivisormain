import mongoose, { Schema } from "mongoose"
import timestamps from "mongoose-timestamp"
import { Entity, EntityUser } from "./../../models"
import { checklistsSchema } from "./../../config/config.model"

// mongoose.Promise = global.Promise

const derivWorkingGroupSchema = Schema({
  partType: String,
  partFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  partFirmName: String, // May not need this
  partContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  partContactName: String,
  partContactEmail: String, // Same reasoning as above
  partUserAddress: { type: Schema.Types.ObjectId }, // Same reasoning as above
  partContactAddrLine1: String, // Same reasoning as above
  partContactAddrLine2: String, // Same reasoning as above
  participantGoogleAddress: String, // Same reasoning as above
  participantState: String, // Same reasoning as above
  partContactPhone: String,
  partContactFax: String,
  partContactTitle: String,
  addToDL: Boolean,
  createdDate: Date
})

const amortSchema = Schema({
  reductionDate: Date,
  prinAmountReduction: Number,
  reviedPrinAmount: Number
})

const derivCouterpartiesSchema = Schema({
  cntrParty: String,
  cntrPartyFirmName: String,
  cntrPartyFirmtype: String,
  cntrPartyMoodysRating: String,
  cntrPartyFitchRating: String,
  cntrPartyKrollRating: String,
  cntrPartySPRating: String
})

const derivativeDocumentSchema = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: String,
  docFileName: String,
  docNote: String,
  docStatus: String,
  settings: {
    selectLevelType: String,
    firms: Array,
    users: Array
  },
  markedPublic: { publicFlag: Boolean, publicDate: Date },
  sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
  createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const tranDerivatives = Schema({
  // Attributes from the Create Transaction Page
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  actTranType: String, // This will be Bank Loan
  actTranSubType: String,
  actTranUniqIdentifier: String, // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
  actTranFirmId: Schema.Types.ObjectId, // Entity ID
  actTranFirmName: String,
  actTranClientId: Schema.Types.ObjectId, // Entity ID
  actTranClientName: String, // Name of the Issuer Client
  actTranClientMsrbType: String,
  actTranIsClientMsrbRegistered: Boolean,
  actTranIsConduitBorrowerFlag: String,
  actTranPrimarySector: String,
  actTranSecondarySector: String,
  tranBorrowerName: String,
  tranGuarantorName: String,
  actTranOtherSubtype: String,
  actTranFirmLeadAdvisorId: Schema.Types.ObjectId, // User Id
  actTranFirmLeadAdvisorName: String, // First Name and Last Name
  actTranIssueName: String,
  actTranProjectDescription: String,
  actTranRelatedTo: [
    Schema({
      relTranId: { type: Schema.Types.ObjectId },
      relTranIssueName: String,
      relTranProjectName: String,
      relTranClientId: { type: Schema.Types.ObjectId },
      relTranClientName: String,
      relType: String,
      tranType: String
    })
  ],
  actTranNotes: String,
  tranNotes: [notesSchema],
  actTranStatus: String,

  derivativeSummary: {
    tranIssuer: String,
    tranProjectDescription: String,
    tranType: String,
    tranBorrowerName: String,
    tranGuarantorName: String,
    tranNotionalAmt: Number,
    tranNotionalAmtFlag: Boolean,
    tranTradeDate: Date,
    tranEffDate: Date,
    tranEndDate: Date,
    tranCounterpartyClient: String,
    tranCounterpartyDealer: String,
    tranCounterpartyType: String,
    tranLastMTMValuation: Number,
    tranEstimatedRev: Number,
    tranLastMTMDate: Date,
    tranMTMProvider: String,
    createdAt: { type: Date, required: true, default: Date.now }
  },
  derivativeCounterparties: [derivCouterpartiesSchema],
  derivativeParticipants: [derivWorkingGroupSchema],

  derivTradeClientPayLeg: {
    payor: String, // Should be a drop down of issuers, dealers, obligors
    currency: String,
    paymentType: String,
    upfrontPayment: Number,
    tradeDate: Date,
    effDate: Date,
    fixedRate: Number,
    floatingRateType: String,
    floatingRateRatio: Number,
    floatingRateSpread: Number,
    firstPaymentDate: Date,
    stub: String,
    paymentSchedule: String,
    paymentBusConvention: String,
    periodEndDate: Date, // save the end date for cap here
    periodEndDateBusConvention: String,
    initialIndexSetting: String,
    dayCount: String,
    additionalPayments: String, // applicable for cap
    createdAt: { type: Date, required: true, default: Date.now }
  },
  derivTradeDealerPayLeg: {
    payor: String, // Should be a drop down of issuers, dealers, obligors
    currency: String,
    paymentType: String,
    upfrontPayment: Number,
    tradeDate: Date,
    effDate: Date,
    fixedRate: Number,
    floatingRateType: String,
    floatingRateRatio: Number,
    floatingRateSpread: Number,
    firstPaymentDate: Date,
    stub: String,
    paymentSchedule: String,
    paymentBusConvention: String,
    periodEndDate: Date, // save the end date for cap here
    periodEndDateBusConvention: String,
    initialIndexSetting: String,
    dayCount: String,
    additionalPayments: String,
    createdAt: { type: Date, required: true, default: Date.now }
  },
  derivAmortTrades: [amortSchema],
  derivativeChecklists: [checklistsSchema],
  derivativeDocuments: [derivativeDocumentSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

tranDerivatives.index({actTranFirmId:1})
tranDerivatives.index({actTranClientId:1})
tranDerivatives.index({"derivativeParticipants.partFirmId":1})

export const Derivatives =  mongoose.model("tranderivatives", tranDerivatives)
