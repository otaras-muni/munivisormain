import {generateControllers} from "../../../modules/query"
import {ActBusDev} from "./busdev.model"
import {elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"
import {canUserPerformAction} from "../../../commonDbFunctions"
import {Entity, Tasks} from "../../../seeddata/revisedseedata/models"
import {updateEligibleIdsForLoggedInUserFirm} from "../../../entitlementhelpers"
import {RelatedToAssign} from "../../commonservices/services.controller";

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: ActBusDev, _id: id, esType: "actbusdevs"})

const {ObjectID} = require("mongodb")

/* const postBUSDEVTransaction = () => async (req, res) => {

  const resRequested = [
    {resource:"ActBusDev",access:2},
  ]
  console.log("==============================================>",req.body)
  try {
    // const entitled = await canUserPerformAction(req.user,resRequested)
    if(entitled) {
      const newBusDevTransaction = new ActBusDev({
        ...req.body
      })

      const insertedBusDevTransaction = await newBusDevTransaction.save()
      /!* const data = await putData("actmarfps", insertedMaRfpTransaction._id, insertedMaRfpTransaction._doc)
      console.log("The return data from elastic search", data)
      console.log("==================ES SYNCH ISSUE CHECKS====================================")
      const dataFromES = await getData("actmarfps",insertedMaRfpTransaction._id)
      console.log("Validating whether the data is getting saved in ES", dataFromES) *!/

      res.json(insertedBusDevTransaction)
    }
    else {
      res.status(500).send({done:false,error:"User not entitled to create BUSDEV Transaction"})
    }
  } catch (error) {
    res.status(500).send({done: false,error: "Error saving information on BUSDEV Transactions"})
  }

} */

const postBusDevTransaction = () => async(req, res) => {

  const resRequested = [
    {
      resource: "ActBusDev",
      access: 2
    }
  ]
  try {
    const { actRelTrans } = req.body
    const newBusDevTransaction = new ActBusDev({
      ...req.body
    })
    const insertedBusDevTransaction = await newBusDevTransaction.save()
    await RelatedToAssign(insertedBusDevTransaction, actRelTrans, "BusinessDevelopment", "multi")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, insertedBusDevTransaction._id)
    await Promise.all([ent, es])
    res.json(insertedBusDevTransaction)
  } catch (error) {
    console.log(error)
    res
      .status(500)
      .send({done: false, error: "Error saving information on BUSDEV Transactions"})
  }
}

const searchTaskByEntity = () => async(req, res) => {
  try {
    const {text} = req.query
    // const tranTasks = await ActBusDev.find( {"actIssuerClientEntityName":
    // {$regex: text || "",$options: "i" }} )
    const tranTasks = await ActBusDev.find({
      actTranFirmId: req.user.entityId,
      "$or": [
        {
          actIssuerClientEntityName: {
            $regex: text || "",
            $options: "i"
          }
        }, {
            actProjectName: {
              $regex: text || "",
              $options: "i"
            }
          }
        ]
    })
      .select("tasks actTranFirmId actIssuerClient actProjectName actIssuerClientEntityName act" +
        "Type")
      .populate({path: "tasks", model: Tasks})
    let tasks = []
    const taskGroupBy = {}
    let taskDetails = {}
    if (tranTasks && tranTasks.length) {

      tranTasks.forEach(taskList => {
        if (taskList.tasks) {
          tasks = tasks.concat(taskList.tasks)
        }
      })

      tasks.forEach(task => {
        if (taskGroupBy.hasOwnProperty(`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`)) {
          taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`].push(task)
        } else {
          taskGroupBy[`${task.relatedActivityDetails.activityProjectName} (${task.relatedEntityDetails.entityName})`] = [task]
        }
      })

      tranTasks.forEach(emptyTask => {
        if (!taskGroupBy.hasOwnProperty(`${emptyTask.actProjectName} (${emptyTask.actIssuerClientEntityName})`)) {
          taskGroupBy[`${emptyTask.actProjectName} (${emptyTask.actIssuerClientEntityName})`] = []
        }
      })

      tranTasks.forEach(task => {
        if (!taskDetails.hasOwnProperty(`${task.actIssuerClient} (${task.actIssuerClientEntityName})`)) {
          taskDetails[`${task.actProjectName} (${task.actIssuerClientEntityName})`] = {
            activityId: `${task._id}`,
            activityProjectName: `${task.actProjectName}`,
            activitySubType: "businessDevelopment",
            activityType: "BusinessDevelopment",
            activityIssuerClientId: `${task.actIssuerClient}`,
            activityIssuerClientName: `${task.actIssuerClientEntityName}`
          }

        }
      })
    }
    res.json({tasks, taskGroupBy, taskDetails})
  } catch (error) {
    res
      .status(500)
      .send({done: false, error: "Error saving information on BUSDEV Transactions"})
  }
}

export default generateControllers(ActBusDev, {
  postBusDevTransaction: postBusDevTransaction(),
  searchTaskByEntity: searchTaskByEntity()
})
