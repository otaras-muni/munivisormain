import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

import { Entity } from "../../entity"
import { EntityUser } from "../../entityUser"
import { Tasks } from "../../taskmanagement"
const { Schema } = mongoose

const relatedTransSchema = new Schema({
  /* relationshipType:String,
  relatedTrans:Schema.Types.ObjectId */
  relTranId: { type: Schema.Types.ObjectId },
  relTranIssueName: String,
  relTranProjectName: String,
  relTranClientId: { type: Schema.Types.ObjectId },
  relTranClientName: String,
  relType: String,
  tranType: String
})

const activityParticipantsSchema = new Schema({
  partType: String,
  partFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  partFirmName: String, // May not need this
  partContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  partContactName: String,
  partContactEmail: String, // Same reasoning as above
  partUserAddress: { type: Schema.Types.ObjectId }, // Same reasoning as above
  partContactAddrLine1: String, // Same reasoning as above
  partContactAddrLine2: String, // Same reasoning as above
  participantState: String, // Same reasoning as above
  partContactPhone: String,
  partContactTitle: String,
  partContactAddToDL: Boolean
})

const busDevSchema = new Schema({
  createdByUser: Schema.Types.ObjectId,
  actType: String,
  actSubType: String,
  actOtherSubtype: String,
  actStatus: String,
  actUniqIdentifier: String,
  actTranFirmId: Schema.Types.ObjectId,
  actLeadFinAdvClientEntityId: Schema.Types.ObjectId,
  actLeadFinAdvClientEntityName: String,
  actIssuerClient: Schema.Types.ObjectId,
  actIssuerClientEntityName: String,
  isClientBorrower: Boolean,
  isClientConduit: Boolean,
  clientActive: Boolean,
  actIssueName: String,
  actProjectName: String,
  actPrimarySector: String,
  actSecondarySector: String,
  actNotes: String,
  actRelTrans: [relatedTransSchema],
  participants: [activityParticipantsSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  tasks: [{ type: Schema.Types.ObjectId, ref: Tasks }],
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

busDevSchema.index({actTranFirmId:1,actIssuerClient:1})
busDevSchema.index({actTranFirmId:1})
busDevSchema.index({"participants.partFirmId":1})

export const ActBusDev = mongoose.model("actbusdev", busDevSchema)
