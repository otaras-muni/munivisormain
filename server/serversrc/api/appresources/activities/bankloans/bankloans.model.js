import mongoose, { Schema } from "mongoose"
import timestamps from "mongoose-timestamp"
import { Entity, EntityUser } from "./../../models"
import { checklistsSchema } from "./../../config/config.model"
// mongoose.Promise = global.Promise

const participaantSchema = Schema({
  partType: String,
  partFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  partFirmName: String, // May not need this
  partContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  partContactName: String,
  partContactEmail: String, // Same reasoning as above
  partUserAddress: { type: Schema.Types.ObjectId }, // Same reasoning as above
  partContactAddrLine1: String, // Same reasoning as above
  partContactAddrLine2: String, // Same reasoning as above
  participantGoogleAddress: String, // Same reasoning as above
  participantState: String, // Same reasoning as above
  partContactPhone: String,
  partContactFax: String,
  partContactTitle: String,
  addToDL: Boolean,
  createdDate: Date
})

const linkCusipSchema = Schema({
  cusip: String, // Needs to be 6 characters
  description: String,
  tag: String
})

const amortSchema = Schema({
  reductionDate: Date,
  prinAmountReduction: Number,
  reviedPrinAmount: Number
})

const dealDocumentsSchema = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: String,
  docFileName: String,
  docNote: String,
  docStatus: String,
  settings: {
    selectLevelType: String,
    firms: Array,
    users: Array
  },
  markedPublic: { publicFlag: Boolean, publicDate: Date },
  sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
  createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const tranBankLoans = Schema({
  // Attributes from the Create Transaction Page
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  actTranType: String, // This will be Bank Loan
  actTranSubType: String,
  actTranUniqIdentifier: String, // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
  actTranFirmId: Schema.Types.ObjectId, // Entity ID
  actTranFirmName: String,
  actTranClientId: Schema.Types.ObjectId, // Entity ID
  actTranClientName: String, // Name of the Issuer Client
  actTranClientMsrbType: String,
  actTranIsClientMsrbRegistered: Boolean,
  actTranIsConduitBorrowerFlag: String,
  actTranPrimarySector: String,
  actTranSecondarySector: String,
  actTranBorrowerName: String,
  actTranObligorName: String,
  actTranSecurityType: String,
  actTranFirmLeadAdvisorId: Schema.Types.ObjectId, // User Id
  actTranFirmLeadAdvisorName: String, // First Name and Last Name
  actTranIssueName: String,
  actTranProjectDescription: String,
  actTranRelatedTo: [
    Schema({
      relTranId: { type: Schema.Types.ObjectId },
      relTranIssueName: String,
      relTranProjectName: String,
      relTranClientId: { type: Schema.Types.ObjectId },
      relTranClientName: String,
      relType: String,
      tranType: String
    })
  ],
  actTranNotes: String,
  tranNotes: [notesSchema],
  actTranStatus: String,

  bankLoanCompSolicitation: Boolean,

  // Process Specific Attributes
  bankLoanSummary: {
    actTranSecurityType: String,
    actTenorMaturities: String, // will be read from the deal details
    actTranBorrowerName: String,
    actTranObligorName: String,
    actTranState: String,
    actTranCounty: String,
    actTranClosingDate: Date,
    actTranUseOfProceeds: String,
    actTranEstimatedRev: Number,
    actTranType: String
  },

  bankLoanTerms: {
    stateTax: String,
    bankQualified: String,
    parAmount: Number,
    fedTax: String,
    paymentType: String,
    prinPaymentFreq: String,
    prinPaymentStartDate: Date,
    prinPaymentDay: Number,

    intPaymentFreq: String,
    intPaymentStartDate: Date,
    intPaymentDay: Number,

    fixedRate: Number,
    floatingRateRef: String,
    floatingRateRatio: Number, // Needs to be a percentage
    floatingRateSpread: Number, // Needs to be a percentage

    dayCount: String,
    busConvention: String,
    bankHolidays: String,
    prePaymentTerms: String
  },

  bankLoanLinkCusips: [linkCusipSchema],
  bankLoanParticipants: [participaantSchema],
  bankLoanChecklists: [checklistsSchema],
  bankLoanAmort: [amortSchema],
  bankLoanDocuments: [dealDocumentsSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

tranBankLoans.index({actTranClientId:1})
tranBankLoans.index({actTranFirmId:1})
tranBankLoans.index({"bankLoanParticipants.partFirmId":1})


export const BankLoans =  mongoose.model("tranbankloans", tranBankLoans)
