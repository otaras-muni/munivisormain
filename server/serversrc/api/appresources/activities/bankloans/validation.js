import Joi from "joi-browser"

const bankLoanSummarySchema = Joi.object().keys({
  actTranIssuer: Joi.string()
    .required()
    .optional(),
  actTransactionIssueName: Joi.string().optional(),
  actTranStatus: Joi.string()
    .allow("")
    .optional(),
  actTranType: Joi.string()
    .allow("")
    .required()
    .optional(),
  actTenorMaturities: Joi.string()
    .allow("")
    .optional(),
  actTranBorrowerName: Joi.string()
    .allow("")
    .required()
    .optional(),
  actTranObligorName: Joi.string()
    .allow("")
    .required()
    .optional(),
  actTranSecurityType: Joi.string()
    .allow("")
    .optional(),
  actTranState: Joi.string()
    .allow("")
    .optional(),
  actTranCounty: Joi.string()
    .allow("")
    .required()
    .optional(),
  actTranClosingDate: Joi.date()
    .required()
    .optional(),
  actTranUseOfProceeds: Joi.string()
    .allow("")
    .required()
    .optional(),
  actTranEstimatedRev: Joi.number()
    .allow(null)
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

const bankLoanTermsSchema = Joi.object().keys({
  parAmount: Joi.number()
    .allow(null)
    .optional(),
  fedTax: Joi.string()
    .allow("")
    .optional(),
  stateTax: Joi.string()
    .allow("")
    .optional(),
  bankQualified: Joi.string()
    .allow("")
    .optional(),
  paymentType: Joi.string()
    .allow("")
    .optional(),
  prinPaymentFreq: Joi.string()
    .allow("")
    .optional(),
  prinPaymentStartDate: Joi.date()
    .allow(null)
    .optional(),
  prinPaymentDay: Joi.number()
    .allow(null)
    .optional(),
  intPaymentFreq: Joi.string()
    .allow("")
    .optional(),
  intPaymentStartDate: Joi.date()
    .allow(null)
    .optional(),
  intPaymentDay: Joi.number()
    .allow(null)
    .optional(),
  fixedRate: Joi.number()
    .max(100)
    .allow("")
    .optional(),
  floatingRateRef: Joi.string()
    .allow("")
    .optional(),
  floatingRateRatio: Joi.number()
    .max(100)
    .allow(null)
    .optional(),
  floatingRateSpread: Joi.number()
    .max(100)
    .allow(null)
    .optional(),
  dayCount: Joi.string()
    .allow("")
    .optional(),
  busConvention: Joi.string()
    .allow("")
    .optional(),
  bankHolidays: Joi.string()
    .allow("")
    .optional(),
  prePaymentTerms: Joi.string()
    .allow("")
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

const loanDetailsSchema = Joi.object().keys({
  actTranClientId: Joi.string().required(),
  actTranClientName: Joi.string().required(),
  actTranClientMsrbType: Joi.string()
    .allow("")
    .optional(),
  actTranSecondarySector: Joi.string()
    .allow("")
    .optional(),
  actTranPrimarySector: Joi.string()
    .allow("")
    .optional(),
  actTranIssueName: Joi.string()
    .allow("")
    .optional(),
  actTranProjectDescription: Joi.string()
    .allow(["", null])
    .optional(),
  bankLoanSummary: bankLoanSummarySchema,
  bankLoanTerms: bankLoanTermsSchema
})

export const LoanDetailsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, loanDetailsSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const loanCusIpSchema = Joi.object().keys({
  cusip: Joi.string()
    .alphanum()
    .min(6)
    .max(9)
    .required()
    .optional(),
  description: Joi.string()
    .allow("")
    .required()
    .optional(),
  tag: Joi.string()
    .allow("")
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const LoanCUSIPValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, loanCusIpSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const loanPartDistSchema = Joi.object().keys({
  partFirmId: Joi.string()
    .required()
    .optional(),
  partType: Joi.string().required(),
  partFirmName: Joi.string()
    .required()
    .optional(),
  partContactId: Joi.string()
    .required()
    .optional(),
  partContactName: Joi.string().required(),
  partContactTitle: Joi.string()
    .allow("")
    .optional(),
  partContactEmail: Joi.string()
    .email()
    .required(),
  partUserAddress: Joi.string()
    .allow("")
    .optional(),
  partContactAddrLine1: Joi.string().allow("").optional(),
  partContactAddrLine2: Joi.string()
    .allow("")
    .optional(),
  participantGoogleAddress: Joi.string().allow("").optional(),
  participantState: Joi.string().allow("").optional(),
  partContactPhone: Joi.string().allow("").optional(),
  partContactFax: Joi.string().allow("").optional(),
  createdDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  addToDL: Joi.boolean().optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const LoanParticipantsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, loanPartDistSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const amortSchema = Joi.object().keys({
  reductionDate: Joi.date()
    .required()
    .optional(),
  prinAmountReduction: Joi.number()
    .required()
    .optional(),
  reviedPrinAmount: Joi.number()
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const AmortValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, amortSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean()
    .required()
    .optional(),
  publicDate: Joi.string()
    .required()
    .optional()
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean()
    .required()
    .optional(),
  emmaSendDate: Joi.string()
    .required()
    .optional()
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().allow("").required(),
  docSubCategory: Joi.string().allow("").required(),
  docType: Joi.string().allow("").required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  docNote: Joi.string().allow("").optional(),
  docStatus: Joi.string().allow("").optional(),
  markedPublic,
  sentToEmma,
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  docAction: Joi.string().allow("").optional(),
  _id: Joi.string().required().optional()
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().required(),
  bankLoanDocuments: Joi.array()
    .min(1)
    .items(docListSchema)
    .required()
})

export const DocumentsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, DocDetailsDealsSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const contractRefSchema = Joi.object().keys({
  contractId: Joi.string()
    .allow("")
    .optional(),
  contractName: Joi.string()
    .allow("")
    .optional(),
  contractType: Joi.string()
    .allow("")
    .optional(),
  endDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  startDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const contractsSchema = Joi.object().keys({
  contractRef: contractRefSchema,
  digitize: Joi.boolean().optional(),
  nonTranFees: Joi.array().optional(),
  retAndEsc: Joi.array().optional(),
  sof: Joi.array().optional(),
  agree: Joi.string()
    .allow("")
    .optional(),
  reviewed: Joi.boolean().optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const derivativeSummary = Joi.object().keys({
  tranCounterpartyClient: Joi.string().required(),
})

const tranNotesSchema = Joi.object().keys({
  note: Joi.string().allow("").optional(),
  createdAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedByName: Joi.string().allow("").optional(),
  updatedById: Joi.string().allow("").optional(),
  _id: Joi.string().allow("").optional()
})

const transDetailSchema = Joi.object().keys({
  actTranType: Joi.string()
    .required()
    .optional(),
  actTranSubType: Joi.string()
    .required()
    .optional(),
  actTranOtherSubtype: Joi.string()
    .allow("")
    .optional(),
  actTranUniqIdentifier: Joi.string()
    .required()
    .optional(), // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
  actTranFirmId: Joi.string()
    .required()
    .optional(), // Entity ID
  actTranFirmName: Joi.string()
    .required()
    .optional(),
  actTranClientId: Joi.string()
    .required()
    .optional(), // Entity ID
  actTranClientName: Joi.string()
    .required()
    .optional(), // Name of the Issuer Client
  actTranClientMsrbType: Joi.string()
    .required()
    .optional(),
  actTranIsConduitBorrowerFlag: Joi.string().optional(),
  actTranPrimarySector: Joi.string()
    .allow("")
    .optional(),
  bankLoanSummary: Joi.object().allow("").optional(),
  actTranSecondarySector: Joi.string()
    .allow("")
    .optional(),
  actTranFirmLeadAdvisorId: Joi.string()
    .required()
    .optional(), // User Id
  actTranFirmLeadAdvisorName: Joi.string()
    .required()
    .optional(), // First Name and Last Name
  actTranIssueName: Joi.string()
    .allow("")
    .optional(),
  actTranProjectDescription: Joi.string()
    .allow(["", null])
    .optional(),
  actTranRelatedTo: Joi.array().optional(),
  // actTranNotes: Joi.string().allow("").optional(),
  tranNotes: tranNotesSchema,
  actTranRelatedType: Joi.string().allow("").optional(),
  createdByUser: Joi.string().required(),
  clientActive: Joi.boolean().required().optional(),
  actTranStatus: Joi.string().required(),
  bankLoanParticipants: Joi.array().min(1).required().optional(),
  contracts: contractsSchema,
  derivativeParticipants: Joi.array().min(1).required().optional(),
  opportunity: Joi.object().optional(),
  derivativeSummary,
})

export const CreateTransValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, transDetailSchema, {
    abortEarly: false,
    stripUnknown: false
  })

export const createErrorHandling = (req, res, next) => {
  const errors = CreateTransValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

export const loanCUSIPErrorHandling = (req, res, next) => {
  const errors = LoanCUSIPValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

export const participantsErrorHandling = (req, res, next) => {
  const errors = LoanParticipantsValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

export const amortErrorHandling = (req, res, next) => {
  const errors = AmortValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

export const errorHandling = (req, res, next) => {
  const { type } = req.params
  let errors
  if (type === "details") {
    errors = LoanDetailsValidate(req.body)
  }
  if (type === "documents") {
    errors = DocumentsValidate(req.body)
  }
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
