import {generateControllers} from "./../../../modules/query"
import {manageTasksForProcessChecklists} from "../../taskmanagement/taskProcessInteraction"
import {BankLoans} from "./bankloans.model"
import {canUserPerformAction} from "../../../commonDbFunctions"
import {putData, getData, elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"
import {EntityUser} from "../../entityUser"
import {Entity} from "../../entity"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../../entitlementhelpers"
import { RelatedToAssign } from "../../commonservices/services.controller"

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: BankLoans, _id: id, esType: "tranbankloans"})

const {ObjectID} = require("mongodb")

const postBankloansTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]

  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (entitled) {
      const { actTranRelatedTo } = req.body
      const newBankLoans = new BankLoans({
        ...req.body
      })
      const bankLoanSavedTransaction = await newBankLoans.save()

      await RelatedToAssign(bankLoanSavedTransaction, actTranRelatedTo, "Bank Loan", "multi")

      await Promise.all([
        updateEligibleIdsForLoggedInUserFirm(req.user),
        returnFromES(req.user.tenantId, bankLoanSavedTransaction._id)
      ])
      res.json(bankLoanSavedTransaction)

    } else {
      res
        .status(500)
        .send({done: false, error: "User not entitled to create bankloans transaction"})
    }
  } catch (error) {
    res
      .status(500)
      .send({done: false, error: "Error saving information on Deals"})
  }
}

const getUserTransactions = () => async(req, res) => {
  const {tranClientId} = req.params
  const resRequested = [
    {
      resource: "BankLoans",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const loans = await BankLoans
      .find({actTranClientId: tranClientId})
      .select("actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranFirmName a" +
          "ctTranClientId actTranClientName actTranClientMsrbType actTranIsConduitBorrowerF" +
          "lag actTranPrimarySector actTranSecondarySector actTranFirmLeadAdvisorId actTran" +
          "FirmLeadAdvisorName actTranIssueName actTranProjectDescription")

    res.json(loans)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getTransactionDetails = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "BankLoans",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    let select = ""
    if (type === "details") {
      select = "actTranType actTranSubType actTranFirmId actTranStatus actTranClientName actTran" +
          "ClientId actTranClientMsrbType actTranIsConduitBorrowerFlag actTranProjectDescri" +
          "ption actTranUniqIdentifier  actTranPrimarySector  actTranSecondarySector  actTr" +
          "anFirmLeadAdvisorId  actTranFirmLeadAdvisorName  actTranIssueName actTranRelated" +
          "To actTranClientId bankLoanSummary bankLoanTerms bankLoanLinkCusips  OnBoardingD" +
          "ataMigrationHistorical historicalData createdAt actTranNotes tranNotes"
    }
    if (type === "summary") {
      select = "actTranFirmId actTranSubType actTranBorrowerName actTranObligorName actTranClien" +
          "tId actTranClientMsrbType actTranClientName actTranFirmLeadAdvisorName actTranIs" +
          "sueName actTranUniqIdentifier actTranRelatedTo actTranClientId actTranProjectDes" +
          "cription bankLoanSummary actTranStatus actTranSecurityType bankLoanTerms.parAmou" +
          "nt bankLoanTerms.paymentType bankLoanParticipants actTranNotes opportunity tranNotes"
    }
    if (type === "participants") {
      select = "actTranFirmId actTranFirmName actTranClientId actTranClientName actTranSubType a" +
          "ctTranUniqIdentifier bankLoanParticipants actTranIssueName actTranProjectDescrip" +
          "tion"
    }
    if (type === "amortization") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId bankLoanAmort" +
          " createdAt"
    }
    if (type === "rfp") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId"
    }
    if (type === "documents") {
      select = "actTranFirmId actTranFirmName actTranIssueName actTranProjectDescription actTran" +
          "ClientId actTranClientName actTranSubType actTranUniqIdentifier bankLoanDocument" +
          "s bankLoanParticipants"
    }
    if (type === "check-track") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId bankLoanCheck" +
          "lists bankLoanParticipants actTranFirmLeadAdvisorId actTranIssueName actTranProj" +
          "ectDescription actTranFirmLeadAdvisorName"
    }
    if (type === "audit-trail") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId"
    }

    const loans = await BankLoans
      .findById({_id: tranId})
      .select(`${select} actTranStatus`)
      .populate([
        {
          path: "actTranFirmLeadAdvisorId",
          model: EntityUser,
          select: "userFirstName userLastName userFirmName entityId userPhone userEmails userAddres" +
              "ses"
        }, {
          path: "actTranClientId",
          model: Entity,
          select: "addresses"
        }
      ])

    let loanTran = {}
    if (loans && loans.actTranClientId && loans.actTranClientId.addresses) {
      let address = loans
        .actTranClientId
        .addresses
        .find(addr => addr.isPrimary)
      address = address || loans.actTranClientId.addresses[0]
      loanTran = {
        ...loans._doc,
        entityAddress: address || {},
        actTranClientId: loans.actTranClientId._id
      }
    }
    // loans.entityAddress = loans.actTranClientId.addresses loans.actTranClientId =
    // loans.actTranClientId._id

    res.json(select
      ? loanTran
      : {})
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putLoanTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  const {type, tranId} = req.params
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let select = ""
    let tasks = []
    if (type === "summary") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranBorrowerName actTranObli" +
          "gorName actTranRelatedTo actTranFirmId actTranFirmName actTranClientId actTranCl" +
          "ientName actTranClientMsrbType actTranIsConduitBorrowerFlag actTranPrimarySector" +
          " actTranSecondarySector actTranFirmLeadAdvisorId actTranFirmLeadAdvisorName actT" +
          "ranIssueName actTranSecurityType actTranProjectDescription"
      await BankLoans.update({
        _id: tranId
      }, {
        $addToSet: {
          actTranRelatedTo: req.body
        }
      })

      const bankDetails =  await BankLoans.findById(tranId).select("actTranIssueName actTranProjectDescription actTranClientId actTranClientName")
      await RelatedToAssign(bankDetails, req.body, "Bank Loan", "single")

    }
    if (type === "details") {
      select = "actTranClientId actTranClientName actTranClientMsrbType actTranPrimarySector act" +
          "TranSecondarySector actTranIssueName actTranType actTranSubType actTranUniqIdent" +
          "ifier actTranClientId bankLoanSummary actTranStatus bankLoanTerms bankLoanLinkCu" +
          "sips"
      await BankLoans.update({
        _id: tranId
      }, req.body)
    }
    if (type === "documents") {
      select = "actTranType actTranSubType actTranUniqIdentifier actTranClientId bankLoanDocumen" +
          "ts"
      await BankLoans.update({
        _id: tranId
      }, {
        $addToSet: {
          bankLoanDocuments: req.body.bankLoanDocuments
        }
      })
    }
    if (type === "check-track") {
      select = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId bankLoanCheck" +
          "lists"
      await BankLoans.update({
        _id: tranId
      }, req.body)
      tasks = manageTasksForProcessChecklists(tranId, req.user)
    }
    const loan = BankLoans
      .findOne({_id: tranId})
      .select(select)
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, tasks, es])
    res.json(loanData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putCusIpsDetails = () => async(req, res) => {
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  const {tranId} = req.params
  const {_id} = req.body
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (_id) {
      await BankLoans.updateOne({
        _id: ObjectID(tranId),
        "bankLoanLinkCusips._id": ObjectID(_id)
      }, {
        $set: {
          "bankLoanLinkCusips.$": req.body
        }
      })
    } else {
      await BankLoans.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          bankLoanLinkCusips: req.body
        }
      })
    }

    const loan = BankLoans
      .findOne({_id: tranId})
      .select("bankLoanLinkCusips")
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, es])
    res.json(loanData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const pullLinkCucIps = () => async(req, res) => {
  const {tranId} = req.params
  const {cusIpId} = req.query
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await BankLoans.update({
      _id: tranId
    }, {
      $pull: {
        bankLoanLinkCusips: {
          _id: cusIpId
        }
      }
    })
    const loan = BankLoans
      .findOne({_id: tranId})
      .select("bankLoanLinkCusips")
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, es])

    res.json(loanData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putParticipantsDetails = () => async(req, res) => {
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  const {tranId} = req.params
  const {_id} = req.body
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (_id) {
      await BankLoans.updateOne({
        _id: ObjectID(tranId),
        "bankLoanParticipants._id": ObjectID(_id)
      }, {
        $set: {
          "bankLoanParticipants.$": req.body
        }
      })
    } else {
      await BankLoans.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          bankLoanParticipants: req.body
        }
      })
    }
    const loan = BankLoans
      .findOne({_id: tranId})
      .select("bankLoanParticipants")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, ent, es])
    res.json(loanData)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const pullParticipantsDetails = () => async(req, res) => {
  const {tranId} = req.params
  const {id} = req.query
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await BankLoans.update({
      _id: tranId
    }, {
      $pull: {
        bankLoanParticipants: {
          _id: id
        }
      }
    })
    const loan = BankLoans
      .findOne({_id: tranId})
      .select("bankLoanParticipants")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, ent, es])
    res.json(loanData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putAmortizationDetails = () => async(req, res) => {
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  const {tranId} = req.params
  const {_id} = req.body
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (_id) {
      await BankLoans.updateOne({
        _id: ObjectID(tranId),
        "bankLoanAmort._id": ObjectID(_id)
      }, {
        $set: {
          "bankLoanAmort.$": req.body
        }
      })
    } else {
      await BankLoans.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          bankLoanAmort: req.body
        }
      })
    }
    const loan = BankLoans
      .findOne({_id: tranId})
      .select("bankLoanAmort")
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, es])
    res.json(loanData)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const pullAmortizationDetails = () => async(req, res) => {
  const {tranId} = req.params
  const {id} = req.query
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await BankLoans.update({
      _id: tranId
    }, {
      $pull: {
        bankLoanAmort: {
          _id: id
        }
      }
    })
    const loan = BankLoans
      .findOne({_id: tranId})
      .select("bankLoanAmort")
    const es = returnFromES(req.user.tenantId, tranId)
    const [loanData] = await Promise.all([loan, es])

    res.json(loanData)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const postLoanNotes = () => async(req, res) => {
  const {tranId} = req.params
  const {_id} = req.body
  const resRequested = [
    {
      resource: "BankLoans",
      access: 2
    }
  ]
  try {

    if (_id) {
      await BankLoans.updateOne({
        _id: ObjectID(tranId),
        "tranNotes._id": ObjectID(_id)
      }, {
        $set: {
          "tranNotes.$": req.body
        }
      })
    } else {
      await BankLoans.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          tranNotes: req.body
        }
      })
    }
    const loan = await BankLoans.findOne({_id: tranId}).select("tranNotes")
    const es = returnFromES(req.user.tenantId, tranId)
    await Promise.all([loan, es])
    res.json(loan)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export default generateControllers(BankLoans, {
  getUserTransactions: getUserTransactions(),
  getTransactionDetails: getTransactionDetails(),
  putLoanTransaction: putLoanTransaction(),
  putCusIpsDetails: putCusIpsDetails(),
  pullLinkCucIps: pullLinkCucIps(),
  putParticipantsDetails: putParticipantsDetails(),
  pullParticipantsDetails: pullParticipantsDetails(),
  putAmortizationDetails: putAmortizationDetails(),
  pullAmortizationDetails: pullAmortizationDetails(),
  postBankloansTransaction: postBankloansTransaction(),
  postLoanNotes: postLoanNotes()
})
