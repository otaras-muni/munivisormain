import express from "express"
import bankLoanController from "./bankloans.controller"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"
import {
  createErrorHandling,
  errorHandling,
  loanCUSIPErrorHandling,
  participantsErrorHandling,
  amortErrorHandling
} from "./validation"
import {checkClosedTransaction} from "../../commonservices/services.controller"

export const bankLoanRouter = express.Router()

bankLoanRouter.param("id", bankLoanController.findByParam)

bankLoanRouter.route("/")
  .get(requireAuth,bankLoanController.getAll)
  .post(createErrorHandling, requireAuth, bankLoanController.postBankloansTransaction)

bankLoanRouter.route("/:id")
  .get(requireAuth,bankLoanController.getOne)
  .put(requireAuth, checkClosedTransaction, bankLoanController.updateOne)
  .delete(requireAuth, checkClosedTransaction, bankLoanController.createOne)

bankLoanRouter.route("/transactions/:tranClientId")
  .get(requireAuth, bankLoanController.getUserTransactions)

bankLoanRouter.route("/transaction/:type/:tranId")
  .get(requireAuth, bankLoanController.getTransactionDetails)
  .put(errorHandling, requireAuth, checkClosedTransaction, bankLoanController.putLoanTransaction)

bankLoanRouter.route("/cusIps/:tranId")
  .put(loanCUSIPErrorHandling, requireAuth, bankLoanController.putCusIpsDetails)
  .delete(requireAuth, checkClosedTransaction, bankLoanController.pullLinkCucIps)

bankLoanRouter.route("/participants/:tranId")
  .put(participantsErrorHandling, requireAuth, checkClosedTransaction, bankLoanController.putParticipantsDetails)
  .delete(requireAuth, checkClosedTransaction, bankLoanController.pullParticipantsDetails)

bankLoanRouter.route("/amortization/:tranId")
  .put(amortErrorHandling, requireAuth, checkClosedTransaction, bankLoanController.putAmortizationDetails)
  .delete(requireAuth, checkClosedTransaction, bankLoanController.pullAmortizationDetails)

bankLoanRouter.route("/notes/:tranId")
  .post(requireAuth, checkClosedTransaction, bankLoanController.postLoanNotes)
