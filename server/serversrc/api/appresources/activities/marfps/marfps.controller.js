import {generateControllers} from "../../../modules/query"
import {ActMaRFP} from "./marfps.model"
import {putData, getData, elasticSearchUpdateFunction} from "../../../elasticsearch/esHelper"
import {canUserPerformAction} from "../../../commonDbFunctions"
import {EntityUser} from "../../entityUser"
import {Entity} from "../../entity"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../../entitlementhelpers"
import {RelatedToAssign} from "../../commonservices/services.controller"

const {ObjectID} = require("mongodb")

const returnFromES = (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: ActMaRFP, _id: id, esType: "actmarfps"})

const postMARFPTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 2
    }
  ]

  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (entitled) {
      const { actRelTrans } = req.body
      const newMaRfpTransaction = new ActMaRFP({
        ...req.body
      })
      const insertedMaRfpTransaction = await newMaRfpTransaction.save()

      await RelatedToAssign(insertedMaRfpTransaction, actRelTrans, "MA-RFP", "multi")

      const es = returnFromES(req.user.tenantId, insertedMaRfpTransaction._id)
      const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
      await Promise.all([es, ent])

      res.json(insertedMaRfpTransaction)
    } else {
      res
        .status(500)
        .send({done: false, error: "User not entitled to create MARFP Transaction"})
    }
  } catch (error) {
    res
      .status(500)
      .send({done: false, error: "Error saving information on MARFP Transactions"})
  }
}

const getMaRfpTransactionDetails = () => async(req, res) => {
  const {type, tranId} = req.params
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let select = ""
    if (type === "details" || type === "summary") {
      select = "actLeadFinAdvClientEntityId actNotes actTranFirmId maRfpResult actProjectName ac" +
          "tPrimarySector actSaleType actSecondarySector actUniqIdentifier opportunity OnBo" +
          "ardingDataMigrationHistorical historicalData actLeadFinAdvClientEntityName actIs" +
          "sueName actRelTrans maRfpParticipants actIssuerClientMsrbType actIssuerClient ac" +
          "tIssuerClientEntityName maRfpSummary actTranNotes tranNotes"
    }
    if (type === "participants") {
      select = "actLeadFinAdvClientEntityId actTranFirmId actTranFirmName actUniqIdentifier actL" +
          "eadFinAdvClientEntityName actPrimarySector actSecondarySector actIssueName actRe" +
          "lTrans maRfpParticipants actIssuerClientMsrbType actIssuerClient actIssuerClient" +
          "EntityName maRfpSummary actTranNotes actProjectName"
    }
    if (type === "documents") {
      select = "actTranType actTranSubType actUniqIdentifier actIssueName actTranFirmId  actTran" +
          "FirmName actIssuerClient actIssuerClientEntityName actTranClientId actPrimarySec" +
          "tor actSecondarySector maRfpDocuments actIssuerClientMsrbType actIssuerClient ac" +
          "tIssuerClientEntityName maRfpSummary actTranNotes actProjectName actType maRfpPa" +
          "rticipants"
    }
    if (type === "audit-trail") {
      select = "actTranFirmId actTranSubType actTranFirmId actUniqIdentifier actTranClientId act" +
          "PrimarySector actSecondarySector actTranClientName actIssuerClientMsrbType actIs" +
          "suerClient actIssuerClientEntityName maRfpSummary"
    }

    const marfp = await ActMaRFP
      .findById({_id: tranId})
      .select(`${select} actStatus`)
      .populate([
        {
          path: "actLeadFinAdvClientEntityId",
          model: EntityUser,
          select: "userFirstName userLastName userFirmName entityId userPhone userEmails userAddres" +
              "ses"
        }, {
          path: "actIssuerClient",
          model: Entity,
          select: "addresses"
        }
      ])

    let maRfpTran = {}
    if (marfp && marfp.actIssuerClient && marfp.actIssuerClient.addresses) {
      let address = marfp
        .actIssuerClient
        .addresses
        .find(addr => addr.isPrimary)
      address = address || marfp.actIssuerClient.addresses[0]
      maRfpTran = {
        ...marfp._doc,
        entityAddress: address || {},
        actIssuerClient: marfp.actIssuerClient._id
      }
    }
    // marfp.entityAddress = marfp.actIssuerClient.addresses marfp.actIssuerClient =
    // marfp.actIssuerClient._id
    res.json(select
      ? maRfpTran
      : {})
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putMaRfpTransaction = () => async(req, res) => {
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 2
    }
  ]
  const {type, tranId} = req.params
  const {subType} = req.query
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    let select = ""
    if (type === "summary") {
      if (subType === "related") {
        select = "actType actLeadFinAdvClientEntityId actNotes maRfpResult actSaleType actStatus a" +
            "ctProjectName actPrimarySector actSecondarySector actUniqIdentifier actLeadFinAd" +
            "vClientEntityName actIssuerClientMsrbType actIssuerClient actIssuerClientEntityN" +
            "ame actIssueName actRelTrans maRfpSummary maRfpParticipants"
        await ActMaRFP.update({
          _id: tranId
        }, {
          $addToSet: {
            actRelTrans: req.body
          }
        })

        const marfpDetails = await ActMaRFP.findById(tranId).select("actIssueName actProjectName actIssuerClient actIssuerClientEntityName")
        await RelatedToAssign(marfpDetails, req.body, "MA-RFP", "single")

      }
      if (subType === "result") {
        select = "actType actLeadFinAdvClientEntityId actTranFirmId actNotes maRfpResult actUniqId" +
            "entifier actPrimarySector actSecondarySector actLeadFinAdvClientEntityName actIs" +
            "suerClientMsrbType actIssuerClient actIssuerClientEntityName actIssueName actRel" +
            "Trans maRfpSummary maRfpParticipants"
        await ActMaRFP.update({
          _id: tranId
        }, req.body)
      }
    }
    if (type === "details") {
      select = "actType actLeadFinAdvClientEntityId actUniqIdentifier actLeadFinAdvClientEntityN" +
          "ame actPrimarySector actSecondarySector actIssueName actRelTrans actIssuerClient" +
          "MsrbType actIssuerClient actIssuerClientEntityName maRfpParticipants maRfpSummar" +
          "y actTranNotes"
      await ActMaRFP.update({
        _id: tranId
      }, req.body)
    }

    if (type === "documents") {
      select = "actTranType actTranSubType actUniqIdentifier actTranFirmId actTranClientId actIs" +
          "suerClientMsrbType actPrimarySector actSecondarySector actIssuerClient actIssuer" +
          "ClientEntityName maRfpDocuments maRfpSummary maRfpParticipants"
      await ActMaRFP.update({
        _id: tranId
      }, {
        $addToSet: {
          maRfpDocuments: req.body.maRfpDocuments
        }
      })
    }
    const marfp = ActMaRFP
      .findOne({_id: tranId})
      .select(select)
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [marfpdata] = await Promise.all([marfp, ent, es])
    res.json(marfpdata)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const putMaRfpParticipantsDetails = () => async(req, res) => {
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 2
    }
  ]
  const {tranId} = req.params
  const {_id} = req.body
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    if (_id) {
      await ActMaRFP.updateOne({
        _id: ObjectID(tranId),
        "maRfpParticipants._id": ObjectID(_id)
      }, {
        $set: {
          "maRfpParticipants.$": req.body
        }
      })
      const marfp = await ActMaRFP
        .findOne({_id: tranId})
        .select("maRfpParticipants")
      res.json(marfp)
    } else {
      await ActMaRFP.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          maRfpParticipants: req.body
        }
      })
    }
    const marfp = ActMaRFP
      .findOne({_id: tranId})
      .select("maRfpParticipants")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [marfpdata] = await Promise.all([marfp, ent, es])
    res.json(marfpdata)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const getMaRfpUserTransactions = () => async(req, res) => {
  const {tranClientId} = req.params
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 1
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const marfp = await ActMaRFP
      .find({actTranClientId: tranClientId})
      .select("actTranType actTranSubType actUniqIdentifier actTranFirmId actTranFirmName actTr" +
          "anClientId actTranClientName actTranClientMsrbType maRfpParticipants")
    res
      .json(marfp)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const pullMaRfpParticipantsDetails = () => async(req, res) => {
  const {tranId} = req.params
  const {id} = req.query
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 2
    }
  ]
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(500)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    await ActMaRFP.update({
      _id: tranId
    }, {
      $pull: {
        maRfpParticipants: {
          _id: id
        }
      }
    })
    const marfp = ActMaRFP
      .findOne({_id: tranId})
      .select("maRfpParticipants")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    const [marfpdata] = await Promise.all([marfp, ent, es])
    res.json(marfpdata)
  } catch (error) {
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

const postMARFPNotes = () => async(req, res) => {
  const resRequested = [
    {
      resource: "ActMaRFP",
      access: 2
    }
  ]
  const {tranId} = req.params
  const {_id} = req.body
  try {

    if (_id) {
      await ActMaRFP.updateOne({
        _id: ObjectID(tranId),
        "tranNotes._id": ObjectID(_id)
      }, {
        $set: {
          "tranNotes.$": req.body
        }
      })
    } else {
      await ActMaRFP.updateOne({
        _id: ObjectID(tranId)
      }, {
        $addToSet: {
          tranNotes: req.body
        }
      })
    }
    const marfp = await ActMaRFP.findOne({_id: tranId}).select("tranNotes")
    const ent = updateEligibleIdsForLoggedInUserFirm(req.user)
    const es = returnFromES(req.user.tenantId, tranId)
    await Promise.all([marfp, ent, es])
    res.json(marfp)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving information for entitlements", success: "", requestedServices: resRequested})
  }
}

export default generateControllers(ActMaRFP, {
  postMARFPTransaction: postMARFPTransaction(),
  getMaRfpTransactionDetails: getMaRfpTransactionDetails(),
  putMaRfpTransaction: putMaRfpTransaction(),
  getMaRfpUserTransactions: getMaRfpUserTransactions(),
  putMaRfpParticipantsDetails: putMaRfpParticipantsDetails(),
  pullMaRfpParticipantsDetails: pullMaRfpParticipantsDetails(),
  postMARFPNotes: postMARFPNotes()
})
