import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

import { Entity, EntityUser } from "../../models"

const { Schema } = mongoose

const relatedTransSchema = new Schema({
  /* relationshipType:String,
  relatedTrans:Schema.Types.ObjectId */
  relTranId: { type: Schema.Types.ObjectId },
  relTranIssueName: String,
  relTranProjectName: String,
  relTranClientId: { type: Schema.Types.ObjectId },
  relTranClientName: String,
  relType: String,
  tranType: String
})

const activityParticipantsSchema = new Schema({
  partType: String,
  partFirmId: { type: Schema.Types.ObjectId, ref: Entity },
  partFirmName: String, // May not need this
  partContactId: { type: Schema.Types.ObjectId, ref: EntityUser },
  partContactName: String,
  partContactEmail: String, // Same reasoning as above
  partUserAddress: { type: Schema.Types.ObjectId }, // Same reasoning as above
  partContactAddrLine1: String, // Same reasoning as above
  partContactAddrLine2: String, // Same reasoning as above
  participantGoogleAddress: String, // Same reasoning as above
  participantState: String, // Same reasoning as above
  partContactPhone: String,
  partContactFax: String,
  partContactTitle: String,
  addToDL: Boolean,
  createdDate: Date
})

const MarfpDocumentSchema = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: String,
  docFileName: String,
  docNote: String,
  docStatus: String,
  settings: {
    selectLevelType: String,
    firms: Array,
    users: Array
  },
  markedPublic: { publicFlag: Boolean, publicDate: Date },
  sentToEmma: { emmaSendFlag: Boolean, emmaSendDate: Date },
  createdBy: { type: Schema.Types.ObjectId, ref: EntityUser },
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const notesSchema = Schema({
  note:String,
  createdAt: Date,
  updatedAt: Date,
  updatedByName: String,
  updatedById: String,
})

const maRfpSchema = new Schema({
  clientActive: Boolean,
  createdByUser: Schema.Types.ObjectId,
  actType: String,
  actSubType: String,
  actOtherSubtype: String,
  actStatus: String,
  actSaleType: String,
  actUniqIdentifier: String,
  actTranFirmId: Schema.Types.ObjectId,
  actTranFirmName: String,
  actLeadFinAdvClientEntityId: Schema.Types.ObjectId,
  actTranUniqIdentifier: String,
  actLeadFinAdvClientEntityName: String,
  actIssuerClient: Schema.Types.ObjectId,
  actIssuerClientEntityName: String,
  actIssuerClientMsrbType: String,
  actIssueName: String,
  actProjectName: String,
  actPrimarySector: String,
  actSecondarySector: String,
  actNotes: String,
  tranNotes: [notesSchema],
  actTranNotes: String,
  actIssuer: String,
  actOppName: String,
  actOppType: String,
  actOppStatus: String,
  actRelTrans: [relatedTransSchema],

  maRfpResult: {
    actResult: String,
    actClosingNotes: String
  },

  maRfpSummary: {
    actState: String,
    actCounty: String,
    actPlaceholder1: String,
    actPlaceholder2: String,
    actPlaceholder3: String,
    actOfferingType: String,
    actSecurityType: String,
    actBankQualified: String,
    actCorpType: String,
    actParAmount: Number,
    actPricingDate: Date,
    actExpAwaredDate: Date,
    actActAwardDate: Date,
    actEstRev: Number,
    actUseOfProceeds: String,
    actIssuer: String,
    actOppName: String,
    actOppType: String,
    actOppStatus: String,
    createdAt: { type: Date, required: true, default: Date.now }
  },

  maRfpParticipants: [activityParticipantsSchema],
  maRfpDocuments: [MarfpDocumentSchema],
  contracts: {
    contractRef: Object,
    digitize: Boolean,
    nonTranFees: Array,
    retAndEsc: Array,
    sof: Array,
    agree: String,
    reviewed: Boolean,
    _id: { type: Schema.Types.ObjectId }
  },
  opportunity: Object,
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}).plugin(timestamps)

maRfpSchema.index({actTranFirmId:1,actIssuerClient:1})
maRfpSchema.index({actTranFirmId:1})
maRfpSchema.index({"maRfpParticipants.partFirmId":1})

export const ActMaRFP = mongoose.model("actmarfp", maRfpSchema)
