import express from "express"
import marfpsController from "./marfps.controller"
import {
  CreateErrorHandling,
  errorParticipants,
  errorHandling
} from "./validation"
import {requireAuth} from "../../../../authorization/authsessionmanagement/auth.middleware"
import {checkClosedTransaction} from "../../commonservices/services.controller"

export const marfpsRouter = express.Router()

marfpsRouter.param("id", marfpsController.findByParam)

marfpsRouter.route("/")
  .get(requireAuth, marfpsController.getAll)
  .post(requireAuth, CreateErrorHandling, marfpsController.postMARFPTransaction)

marfpsRouter.route("/:id")
  .get(requireAuth, marfpsController.getOne)
  .put(requireAuth, checkClosedTransaction, marfpsController.updateOne)
  .delete(requireAuth, checkClosedTransaction, marfpsController.deleteOne)

marfpsRouter.route("/transactions/:tranClientId")
  .get(requireAuth, marfpsController.getMaRfpUserTransactions)

marfpsRouter.route("/transaction/:type/:tranId")
  .get(requireAuth, marfpsController.getMaRfpTransactionDetails)
  .put(requireAuth, errorHandling, checkClosedTransaction, marfpsController.putMaRfpTransaction)

marfpsRouter.route("/participants/:tranId")
  .put(requireAuth, errorParticipants, checkClosedTransaction, marfpsController.putMaRfpParticipantsDetails)
  .delete(requireAuth, checkClosedTransaction, marfpsController.pullMaRfpParticipantsDetails)

marfpsRouter.route("/notes/:tranId")
  .post(requireAuth, checkClosedTransaction, marfpsController.postMARFPNotes)
