import Joi from "joi-browser"

const contractRefSchema = Joi.object().keys({
  contractId: Joi.string()
    .allow("")
    .optional(),
  contractName: Joi.string()
    .allow("")
    .optional(),
  contractType: Joi.string()
    .allow("")
    .optional(),
  endDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  startDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const contractsSchema = Joi.object().keys({
  contractRef: contractRefSchema,
  digitize: Joi.boolean().optional(),
  nonTranFees: Joi.array().optional(),
  retAndEsc: Joi.array().optional(),
  sof: Joi.array().optional(),
  agree: Joi.string()
    .allow("")
    .optional(),
  reviewed: Joi.boolean().optional(),
  _id: Joi.string()
    .allow("")
    .optional()
})

const tranNotesSchema = Joi.object().keys({
  note: Joi.string().allow("").optional(),
  createdAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedAt: Joi.date().example(new Date("2016-01-01")).optional(),
  updatedByName: Joi.string().allow("").optional(),
  updatedById: Joi.string().allow("").optional(),
  _id: Joi.string().allow("").optional()
})

const maRfpSchema = Joi.object().keys({
  actType: Joi.string().required().optional(),
  actSubType: Joi.string().required().optional(),
  actOtherSubtype: Joi.string().allow("").optional(),
  actTranFirmId: Joi.string().required(),
  actTranFirmName: Joi.string().required(),
  actStatus: Joi.string().allow("").required().optional(),
  actUniqIdentifier: Joi.string().required(),
  actLeadFinAdvClientEntityId: Joi.string().required().optional(),
  actLeadFinAdvClientEntityName: Joi.string().required().optional(),
  actIssuerClientMsrbType: Joi.string().allow("").optional(),
  actIssuerClient: Joi.string().required().optional(),
  actIssuerClientEntityName: Joi.string().required().optional(),
  maRfpSummary: Joi.object().optional(),
  actIssueName: Joi.string().allow("").required().optional(),
  actProjectName: Joi.string().required().optional(),
  actPrimarySector: Joi.string().allow("").optional(),
  actSecondarySector: Joi.string().allow("").optional(),
  // actNotes: Joi.string().allow("").required().optional(),
  tranNotes: tranNotesSchema,
  actTranNotes: Joi.string().allow("").optional(),
  actRelTrans: Joi.array().optional(),
  contracts: contractsSchema,
  createdByUser: Joi.string().required(),
  clientActive: Joi.boolean().required().optional(),
  maRfpParticipants: Joi.array().min(1).required().optional(),
  opportunity: Joi.object().optional()
})

export const CreateMARFPValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, maRfpSchema, {
    abortEarly: false,
    stripUnknown: false
  })

export const CreateErrorHandling = (req, res, next) => {
  const errors = CreateMARFPValidate(req.body)

  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

const maRfpSummary = Joi.object().keys({
  actState: Joi.string().required(),
  actCounty: Joi.string()
    .allow("")
    .optional(),
  actPlaceholder1: Joi.string()
    .allow("")
    .optional(),
  actPlaceholder2: Joi.string()
    .allow("")
    .optional(),
  actPlaceholder3: Joi.string()
    .allow("")
    .optional(),
  actOfferingType: Joi.string()
    .allow("")
    .optional(),
  actSecurityType: Joi.string()
    .allow("")
    .optional(),
  actBankQualified: Joi.string()
    .allow("")
    .optional(),
  actCorpType: Joi.string()
    .allow("")
    .optional(),
  actParAmount: Joi.number().allow("", null).required().optional(),
  actPricingDate: Joi.date().allow(["", null]).required().optional(),
  actExpAwaredDate: Joi.date().allow(["", null]).required().optional(),
  actActAwardDate: Joi.date().allow(["", null]).required().optional(),
  actEstRev: Joi.number().allow("", null).required().optional(),
  actUseOfProceeds: Joi.string().allow("").optional(),
  actOppName: Joi.string().allow("").optional(),
  actOppType: Joi.string().allow("").optional(),
  actOppStatus: Joi.string().allow("").optional(),
  actPrimarySector: Joi.string().allow("").optional(),
  actSecondarySector: Joi.string().allow("").optional(),
  createdAt: Joi.date().required().optional(),
  _id: Joi.string().required().optional()
})

const marfpSchema = Joi.object().keys({
  actIssuerClientEntityName: Joi.string().required().optional(),
  actIssuerClient: Joi.string().required().optional(),
  actIssuerClientMsrbType: Joi.string().required().optional(),
  actSecondarySector: Joi.string().allow("").optional(),
  actPrimarySector: Joi.string().allow("").optional(),
  actIssueName: Joi.string().allow("").optional(),
  actProjectName: Joi.string().required(),
  maRfpSummary
})

export const DetailsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, marfpSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean()
    .required()
    .optional(),
  publicDate: Joi.string()
    .required()
    .optional()
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean()
    .required()
    .optional(),
  emmaSendDate: Joi.string()
    .required()
    .optional()
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().allow("").required(),
  docSubCategory: Joi.string().allow("").required(),
  docType: Joi.string().allow("").required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  docNote: Joi.string().allow("").optional(),
  docStatus: Joi.string().allow("").optional(),
  markedPublic,
  sentToEmma,
  docAction: Joi.string().allow("").optional(),
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  _id: Joi.string().required().optional()
})

const DocDetailsMarfpSchema = Joi.object().keys({
  _id: Joi.string().required(),
  maRfpDocuments: Joi.array()
    .min(1)
    .items(docListSchema)
    .required()
})

export const DocumentsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, DocDetailsMarfpSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const ParticipantsSchema = Joi.object().keys({
  partType: Joi.string()
    .required()
    .optional(),
  partFirmId: Joi.string()
    .required()
    .optional(),
  partFirmName: Joi.string()
    .required()
    .optional(), // May not need this
  partContactTitle: Joi.string()
    .required()
    .optional(),
  partContactId: Joi.string()
    .required()
    .optional(),
  partContactName: Joi.string()
    .required()
    .optional(),
  partUserAddress: Joi.string()
    .allow("")
    .optional(),
  partContactEmail: Joi.string()
    .email()
    .required()
    .optional(), // Same reasoning as above
  partContactAddrLine1: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  partContactAddrLine2: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  participantGoogleAddress: Joi.string().allow("").optional(),
  participantState: Joi.string()
    .allow("")
    .optional(), // Same reasoning as above
  partContactPhone: Joi.string().allow("").optional(),
  partContactFax: Joi.string().allow("").optional(),
  addToDL: Joi.boolean().optional(),
  createdDate: Joi.date()
    .example(new Date("2016-01-01"))
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const activityParticipantsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, ParticipantsSchema, {
    abortEarly: false,
    stripUnknown: false
  })

export const errorParticipants = (req, res, next) => {
  const errors = activityParticipantsValidate(req.body)
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

export const errorHandling = (req, res, next) => {
  const { type } = req.params
  let errors
  if (type === "details") {
    errors = DetailsValidate(req.body)
  }
  if (type === "documents") {
    errors = DocumentsValidate(req.body)
  }
  if (
    errors &&
    errors.error &&
    Array.isArray(errors.error.details) &&
    errors.error.details.length
  ) {
    return res.status(422).json(errors.error.details)
  }
  next()
}
