module.exports = {
  "properties": {
    "actType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actSubType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actStatus": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actUniqIdentifier": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actLeadFinAdvClientEntityId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranUniqIdentifier": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actLeadFinAdvClientEntityName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actIssuerClient": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actIssuerClientEntityName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actIssuerClientMsrbType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "isClientBorrower": {
      "type": "boolean"
    },
    "isClientConduit": {
      "type": "boolean"
    },
    "actIssueName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actProjectName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actPrimarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actSecondarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actNotes": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actIssuer": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actOppName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actOppType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actOppStatus": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actRelTrans": {
      "properties": {
        "relTranId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranIssueName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranProjectName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranClientId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranClientName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "maRfpResult": {
      "properties": {
        "actResult": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actClosingNotes": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "maRfpSummary": {
      "properties": {
        "actState": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actCounty": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actPlaceholder1": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actPlaceholder2": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actPlaceholder3": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actOfferingType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actSecurityType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actBankQualified": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actCorpType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actParAmount": {
          "type": "float"
        },
        "actPricingDate": {
          "type": "date"
        },
        "actExpAwaredDate": {
          "type": "date"
        },
        "actActAwardDate": {
          "type": "date"
        },
        "actEstRev": {
          "type": "float"
        },
        "actUseOfProceeds": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actIssuer": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actOppName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actOppType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actOppStatus": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actPrimarySector": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actSecondarySector": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "maRfpParticipants": {
      "properties": {
        "partType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddrLine1": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddrLine2": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "participantState": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactTitle": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "maRfpDocuments": {
      "properties": {
        "docCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docSubCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docAWSFileLocation": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docFileName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "markedPublic": {
          "properties": {
            "publicFlag": {
              "type": "boolean"
            },
            "publicDate": {
              "type": "date"
            }
          }
        },
        "sentToEmma": {
          "properties": {
            "emmaSendFlag": {
              "type": "boolean"
            },
            "emmaSendDate": {
              "type": "date"
            }
          }
        },
        "LastUpdatedDate": {
          "type": "date"
        }
      }
    }
  }
}
