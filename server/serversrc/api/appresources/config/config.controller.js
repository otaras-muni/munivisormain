import { generateControllers } from "../../modules/query"
import { Config } from "./config.model"
import { EntityRel } from "../entityRel"

const { ObjectID } = require("mongodb")

export const overrideGetAll = async (req, res, next) => {
  const { user } = req
  let { entityId, userEntitlement } = user || {}

  // console.log("entityId : ", entityId)

  if(!user || !entityId) {
    return res.status(422).send({ error: "No user or entityId found" })
  }
  const { require, searchTerm} = req.query
  console.log("The query Parameter is ", searchTerm)
  const regexSearchTerm = new RegExp(searchTerm,"i")

  if(require && (require !== "entitlement")) {
    const rels = await EntityRel.find({ entityParty2: entityId })
    if(rels && rels[0] && ["Client", "Prospect", "Third Party"].includes(rels[0].relationshipType)) {
      entityId = rels[0].entityParty1
    }
  }

  let searchQueryPart = []
  if(searchTerm && searchTerm.length > 0) {
    searchQueryPart = [{
      $match:{ "$or": [
        { "picklistTitle": {"$regex":regexSearchTerm} }, 
        { "picklistItems.label": {"$regex":regexSearchTerm}},
        { "picklistMeta.systemName": {"$regex":regexSearchTerm}}
      ]}
    }]
  }

  console.log("The search Query Party", JSON.stringify(searchQueryPart,null,2))

  const pickListPipeline = [
    {
      $match: {entityId:ObjectID(entityId)}
    },
    {
      $unwind:"$picklists"
    },
    {
      $project:{
        entityId:"$entityId",
        picklistId:"$picklists._id",
        picklistTitle:"$picklists.title",
        picklistItems:"$picklists.items",
        picklistMeta:"$picklists.meta"
      }
    },
    {
      $unwind:"$picklistItems"
    },
    ...searchQueryPart,
    {
      $group:{
        _id:{entityId:"$entityId",picklistId:"$picklistId",title:"$picklistTitle",meta:"$picklistMeta"},
        items:{$addToSet:"$picklistItems"}
      }
    },
    {
      $project:{
        _id:0,
        entityId:"$_id.entityId",
        picklistId:"$_id.picklistId",
        title:"$_id.title",
        meta:"$_id.meta",
        // items:1
      }
    }
  ]

  console.log("The overall pipeline is", JSON.stringify(pickListPipeline,null,2))


  // const filter = req.query || {}
  const filter = { entityId }
  let docs
  try {
    switch(require) {
    case "entitlement":
      docs = await Config.find(filter).select("accessPolicy")
      res.json(docs[0].accessPolicy.filter(d => d.role === userEntitlement))
      break
    case "picklistsMeta":
      docs = await Config.findOne(filter).select({"picklists.meta": 1, "picklists.title": 1})
      res.json(docs.picklists)
      break
    case "picklists": {
      const start = new Date()
      const { names } = req.query
      let docs = []
      console.log("names : ", names)
      if(names && names.length) {
        const systemNames = names.split(",")
        console.log("systemNames : ", systemNames)
        docs = await Config.aggregate([
          {
            $match: filter
          },
          {
            $limit: 1
          },
          {
            $project: { picklists: 1 }
          },
          {
            $project: {
              picklists: {
                $filter: {
                  input: "$picklists",
                  as: "picklist",
                  cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
                }
              }
            }
          }
        ])
      } else {
        // docs = await Config.find(filter).select("picklists.meta picklists.title")
        docs = await Config.aggregate(pickListPipeline)
        docs = [{picklists:docs}]
        console.log("The picklist items to be showcased are", JSON.stringify(docs, null, 2))
      }
      console.log("time : ", new Date() - start)
      res.json(docs[0].picklists)
      break
    }
    case "checklists":
      docs = await Config.find(filter).select("checklists")
      res.json(docs[0].checklists)
      break
    case "notifications":
      docs = await Config.find(filter).select("notifications")
      res.json(docs[0].notifications)
      break
    default:
      docs = await Config.find(filter)
      res.json(docs)
    }
  } catch (error) {
    next(error)
  }

  // Config.find(filter)
  //   .then(docs => res.json(docs))
  //   .catch(error => next(error))
}

export const getSpecific = async (req, res, next) => {
  const { user } = req
  let { entityId } = user || {}

  if(!user || !entityId) {
    return res.status(422).send({ error: "No user or entityId found" })
  }

  const rels = await EntityRel.find({ entityParty2: entityId })
  if(rels && rels[0] && ["Client", "Prospect", "Third Party"].includes(rels[0].relationshipType)) {
    entityId = rels[0].entityParty1
  }

  const filter = { entityId }
  let docs = []
  try {
    const names = decodeURIComponent(req.query.names || "")
    console.log("names : ", names)
    if(names && names.length) {
      const systemNames = names.split(",")
      console.log("systemNames : ", systemNames)
      docs = await Config.aggregate([
        {
          $match: filter
        },
        {
          $limit: 1
        },
        {
          $project: { picklists: 1 }
        },
        {
          $project: {
            picklists: {
              $filter: {
                input: "$picklists",
                as: "picklist",
                cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
              }
            }
          }
        }
      ])
    }
    res.json(docs)
  } catch (error) {
    next(error)
  }
}

export const postSpecific = async (req, res, next) => {
  const { user } = req

  if(!user) {
    return res.status(422).send({ error: "No auth user found" })
  }

  const { entityId } = user
  console.log("entityId : ", entityId)
  let docs = []
  try {
    const pickList = req.body
    console.log("===========pickList===========>", pickList)
    const { id } = req.query
    const items = pickList && pickList.items
    const title = pickList && pickList.title
    const meta = pickList && pickList.meta
    const filter = { entityId }
    if(entityId) {
      if(id){
        await Config.updateOne(
          { entityId: ObjectID(entityId), "picklists._id": id },
          { $set: { "picklists.$.items": items, "picklists.$.meta": meta, "picklists.$.title": title } }
        )
      }else {
        docs = await Config.update(
          { entityId },
          { $addToSet: { picklists: [pickList] } }
        )
      }
    }
    const names = pickList.meta.systemName
    console.log("names : ", names)
    if(names && names.length) {
      const systemNames = names.split(",")
      console.log("systemNames : ", systemNames)
      docs = await Config.aggregate([
        {
          $match: filter
        },
        {
          $limit: 1
        },
        {
          $project: { picklists: 1 }
        },
        {
          $project: {
            picklists: {
              $filter: {
                input: "$picklists",
                as: "picklist",
                cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
              }
            }
          }
        }
      ])
    }
    res.json(docs)

  } catch (error) {
    next(error)
  }
}

export const postNotifications = async (req, res, next) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No auth user found" })
  }
  const { entityId } = user
  try {
    if(entityId) {
      await Config.updateOne(
        { entityId: ObjectID(entityId)},
        { $set: { notifications: req.body.body } }
      )
    }
    const resData = await Config.find({entityId: ObjectID(entityId)}).select("notifications")
    console.log("resData",resData)
    res.json(resData)
  } catch (error) {
    next(error)
  }
}

export const deleteSpecific = async (req, res, next) => {
  const { user } = req
  const { id } = req.query
  if(!user) {
    return res.status(422).send({ error: "No auth user found" })
  }
  const { entityId } = user
  try {
    if(entityId) {
      await Config.update({
        entityId: ObjectID(entityId)
      }, {
        $pull: { picklists: { _id: id } }
      })
    }
    const resData = await Config.find({entityId: ObjectID(entityId)}).select("notifications")
    console.log("resData",resData)
    res.json(resData)
  } catch (error) {
    next(error)
  }
}

export const getNotifications = async (req, res, next) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No auth user found" })
  }
  const { entityId } = user
  try {
    if(entityId) {
      const resData = await Config.findOne({entityId: ObjectID(entityId)}).select("notifications")
      console.log("resData",resData)
      res.json(resData)
    }
  } catch (error) {
    next(error)
  }
}

const overrides = {
  getAll: overrideGetAll,
  getSpecific,
  postSpecific,
  postNotifications,
  getNotifications,
  deleteSpecific,
}

export default generateControllers(Config, overrides)
