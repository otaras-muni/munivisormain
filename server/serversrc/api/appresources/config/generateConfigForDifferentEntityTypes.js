import path from "path"
import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"

import {
  Config,
  Entity
} from "./../models"

import { generateUIAccressObject } from "./../../seeddata/revisedseedata/config/ui-access"
import { generateAccessPolicyData } from "./../../seeddata/revisedseedata/config/access-policy"
import { generatePicklists } from "./../../seeddata/revisedseedata/config/picklists"
import { generateChecklists } from "./../../seeddata/revisedseedata/config/checklists"

export const generateConfigForEntities = async (entityIds) => {
  
  const ROLES = ["global-edit", "global-readonly", "tran-edit", "tran-readonly",
    "global-readonly-tran-edit"]
  
  const uiaccessfilepath =  path.join(__dirname,"..","..","seeddata","revisedseedata","uiaccess", "uiaccess.csv")
  const editablePicklistPath = path.join(__dirname,"..","..","seeddata","revisedseedata", "picklists", "editable.csv")
  const notEditablePicklistPath = path.join(__dirname,"..","..","seeddata","revisedseedata", "picklists", "notEditable.csv")
  const checklistfilepath = path.join(__dirname,"..","..","seeddata","revisedseedata", "checklists", "test.csv")
  
  let uiAccessModel = {}
  let picklists = []
  let checklists = []
  
  const start = new Date()
  console.log("start generating config for all")
  const accessPolicy = generateAccessPolicyData(ROLES)
  console.log("got access policy data for roles : ", accessPolicy.length)

  const entities = await Entity.aggregate([
    {
      $match:{
        _id:{ $in: entityIds.map( e => ObjectID(e)) }
      }
    },
    {
      $lookup: {
        from: "entityrels",
        localField: "_id",
        foreignField: "entityParty2",
        as: "rel"
      }
    },
    {
      $project: {
        relType: "$rel.relationshipType",
      }
    },
    {
      $unwind: "$relType"
    }
  ])

  console.log("The entity details are as follows", JSON.stringify(entities))

  try {
    uiAccessModel = await generateUIAccressObject(ROLES, uiaccessfilepath)
    console.log("got ui access data")
  } catch (err) {
    console.log("err in getting ui access data : ", err)
  }

  
  try {
    await Config.insertMany(entities.map(e => ({
      entityId: e._id,
      accessPolicy: [],
      checklists: [],
      picklists: [],
      notifications: {}
    })))
    console.log("configs inserted ok for all entities. Please wait till it gets updated with data.")
  } catch (err) {
    console.log("err in inserting configs : ", err)
  }

  const tenants = entities.filter(e => ["PlatformAdmin", "Self"].includes(e.relType))
  const clients = entities.filter(e => (e.relType === "Client"))
  const prospects = entities.filter(e => (e.relType === "Prospect"))
  const thirdParties = entities.filter(e => (e.relType === "Third Party"))

  console.log(JSON.stringify({tenants,clients,prospects,thirdParties},null,2))
  
  
  if(!isEmpty(tenants))
  {
    try {
      picklists = await generatePicklists(editablePicklistPath, notEditablePicklistPath)
      console.log("got picklists data")
    } catch (err) {
      console.log("err in getting picklists data : ", err)
    }
    
    try {
      checklists = await generateChecklists(checklistfilepath)
      console.log("got checklists data")
    } catch (err) {
      console.log("err in getting checklists data : ", err)
    }
    
    try {
      const tenantAccessPolicy = [ ...accessPolicy ]
      tenantAccessPolicy.forEach((p, i) => {
        tenantAccessPolicy[i] = { ...tenantAccessPolicy[i] }
        tenantAccessPolicy[i].UIAccess = uiAccessModel.Self[p.role]
      })
      
      await Config.updateMany(
        { entityId: { $in: tenants.map(e => e._id) } },
        { $set: { picklists, checklists, accessPolicy: tenantAccessPolicy, notifications: { self : true } }},
        {"new":true, "upsert": false, "runValidators": true}
      )
      console.log("config updated ok for all tenants")
    } catch (err) {
      console.log("err in updating config for tenants : ", err)
    }
  }
    
  if( !isEmpty(clients)) {
    try {
      const clientsAccessPolicy = [ ...accessPolicy ]
      clientsAccessPolicy.forEach((p, i) => {
        clientsAccessPolicy[i] = { ...clientsAccessPolicy[i] }
        clientsAccessPolicy[i].UIAccess = uiAccessModel.Client[p.role]
      })
      await Config.updateMany(
        { entityId: { $in: clients.map(e => e._id) } },
        { $set: { accessPolicy: clientsAccessPolicy }},
        {"new":true, "upsert": false, "runValidators": true}
      )
      console.log("config updated ok for all clients")
    } catch (err) {
      console.log("err in updating config for clients : ", err)
    }
  }
  
  if( !isEmpty(prospects)) {
    try {
      const prospectsAccessPolicy = [ ...accessPolicy ]
      prospectsAccessPolicy.forEach((p, i) => {
        prospectsAccessPolicy[i] = { ...prospectsAccessPolicy[i] }
        prospectsAccessPolicy[i].UIAccess = uiAccessModel.Prospect[p.role]
      })
      await Config.updateMany(
        { entityId: { $in: prospects.map(e => e._id) } },
        { $set: { accessPolicy: prospectsAccessPolicy }},
        {"new":true, "upsert": false, "runValidators": true}
      )
      console.log("config updated ok for all prospects")
    } catch (err) {
      console.log("err in updating config for prospects : ", err)
    }
  }
  
  if( !isEmpty(thirdParties)) {
    try {
      const thirdPartiesAccessPolicy = [ ...accessPolicy ]
      thirdPartiesAccessPolicy.forEach((p, i) => {
        thirdPartiesAccessPolicy[i] = { ...thirdPartiesAccessPolicy[i] }
        thirdPartiesAccessPolicy[i].UIAccess = uiAccessModel["Third Party"][p.role]
      })
      await Config.updateMany(
        { entityId: { $in: thirdParties.map(e => e._id) } },
        { $set: { accessPolicy: thirdPartiesAccessPolicy }},
        {"new":true, "upsert": false, "runValidators": true}
      )
      console.log("config updated ok for all third parties")
    } catch (err) {
      console.log("err in updating config for third parties : ", err)
    }
  }
  console.log("all config done in time(ms) : ", new Date() - start)
  return true
}
