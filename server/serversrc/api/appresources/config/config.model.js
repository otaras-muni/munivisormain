import mongoose from "mongoose"
import { Entity } from "../entity/entity.model"

const { Schema } = mongoose

// export const checklistItemSchema = new Schema({itemId: Schema.Types.ObjectId }, { strict: false, _id: false })

export const checklistSchema = new Schema({
  title: String,
  headers: [String],
  items: [{}]
}, {_id: false})

export const checklistsSchema = new Schema({
  type: String,
  id: String,
  name: String,
  attributedTo: String,
  bestPractice: String,
  notes: String,
  status: String,
  lastUpdated: {
    date: Date,
    by: String
  },
  data: [checklistSchema]
}, { _id: false, strict: false })

export const resourceAccessSchema = new Schema({
  resource: String,
  access: Number
}, { _id: false })

export const navAccessSchema = new Schema({ _id: false })

navAccessSchema.add({
  path: String,
  access: Boolean,
  subpath: [navAccessSchema]
})

export const UIAccessSchema = new Schema({
  primaryNav: [navAccessSchema]
}, { _id: false, strict: false })

export const accessPolicySchema = new Schema({
  role: String,
  resourceAccess: [resourceAccessSchema],
  UIAccess: UIAccessSchema
}, { _id: false, strict: false })

export const picklistMetaSchema = new Schema({
  key: String,
  systemName: String,
  subListLevel2: Boolean,
  subListLevel3: Boolean,
  systemConfig: Boolean,
  restrictedList: Boolean,
  externalList: Boolean,
  bestPractice: Boolean,
  editable: Boolean
}, { _id: false })

export const picklistItemSchema = new Schema({ _id: false })

picklistItemSchema.add({
  label: String,
  included: Boolean,
  visible: Boolean,
  items: [picklistItemSchema]
})


export const picklistsSchema = new Schema({
  title: String,
  meta: picklistMetaSchema,
  items: [picklistItemSchema]
}/* , { _id: false } */)

export const notificationsConfigSchema = new Schema({
  self: Boolean,
  client: Boolean,
  thirdParty: Boolean,
}, { _id: false })

const configSchema = new Schema({
  entityId: { 
    type: Schema.Types.ObjectId,
    index: true,
    unique: true,
    required: true,
    ref: Entity},
  accessPolicy: [accessPolicySchema],
  checklists: [checklistsSchema],
  picklists: [picklistsSchema],
  notifications: notificationsConfigSchema
})

configSchema.index({entityId:1})

export const Config = mongoose.model("config", configSchema)
