import express from "express"
import configController from "./config.controller"
import {Entity} from "../entity"
import {EntityRel} from "../entityRel"
import {Config} from "./config.model"
import {generateConfigForEntities} from "./generateConfigForDifferentEntityTypes"

import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const configRouter = express.Router()

// configRouter.get("/", (req, res, next) => {   const { user } = req   const {
// entityId } = user || {}
//
//   console.log("entityId : ", entityId)
//
//   if(!user || !entityId) {     return res.status(422).send({ error: "No user
// or entityId found" })   }
//
//   return res.json(user) })

configRouter.get("/get-config-id", requireAuth, async(req, res) => {
  const {user} = req

  if (!user) {
    return res
      .status(422)
      .send({error: "No auth user found"})
  }

  let config

  try {
    const {entityId} = user
    console.log("entityId : ", entityId)
    config = await Config
      .findOne({entityId})
      .select({_id: 1})

    if (!config) {
      return res
        .status(422)
        .send({error: "No config found"})
    }
    res.json(config)
  } catch (err) {
    return res
      .status(422)
      .send({error: "Error in config"})
  }
})

configRouter.post("/update-checklists", requireAuth, async(req, res) => {
  const {user} = req
  const {checklists} = req.body

  if (!user) {
    return res
      .status(422)
      .send({error: "No auth user found"})
  }

  if (!checklists) {
    return res
      .status(422)
      .send({error: "No checklists data provided"})
  }

  const {entityId} = user
  console.log("entityId : ", entityId)

  const config = await Config
    .findOne({entityId})
    .select({_id: 1})

  if (!config) {
    return res
      .status(422)
      .send({error: "No config for the user found"})
  }

  const {_id} = config

  const updateRes = await Config.update({
    _id
  }, {$set: {
    checklists
  }})

  res.json({updateRes})

})

configRouter.post("/update-picklists", requireAuth, async(req, res) => {
  const {user} = req

  if (!user) {
    return res
      .status(422)
      .send({error: "No auth user found"})
  }

  const {entityId} = user
  console.log("entityId : ", entityId)

  try {
    const {_id, newPicklists, changedPicklists, removed} = req.body
    if (!newPicklists.length && !changedPicklists.length && !removed.length) {
      return res
        .status(200)
        .send({ok: "no change"})
    }
    console.log("_id : ", _id)
    const config = await Config
      .findOne({_id})
      .select({_id: 1, "picklists.meta.key": 1})

    if (!config) {
      return res
        .status(422)
        .send({error: "No config for the user found"})
    }

    let insertRes
    let updateRes
    let removeRes
    if (newPicklists.length) {
      insertRes = await Config.update({
        _id
      }, {
        $push: {
          picklists: {
            $each: newPicklists
          }
        }
      })
    }

    if (changedPicklists.length) {
      const updateValues = {}
      changedPicklists.forEach(e => {
        const idx = config
          .picklists
          .findIndex(d => d.meta.key === e.meta.key)
        if (idx > -1) {
          const key = `picklists.${idx}`
          updateValues[key] = {
            ...e
          }
        }
      })
      updateRes = await Config.update({
        _id
      }, {
        $set: {
          ...updateValues
        }
      })
    }

    if (removed.length) {
      removeRes = await Config.update({
        _id
      }, {
        $pull: {
          "picklists": {
            "meta.key": {
              $in: removed
            }
          }
        }
      })
    }

    res.json({
      insert: insertRes && insertRes.ok,
      update: updateRes && updateRes.ok,
      remove: removeRes && removeRes.ok
    })
  } catch (err) {
    return res
      .status(422)
      .send({error: "Error in updating config", err})
  }
})

configRouter.post("/inherit", requireAuth, async(req, res) => {
  const {user} = req
  const {entityId} = req.body
  if (!user) {
    return res
      .status(422)
      .send({error: "No auth user found"})
  }

  const existingConfig = await Config.find({entityId})
  if (existingConfig && existingConfig[0]) {
    return res
      .status(422)
      .send({error: "Config already exists for the given entityId"})
  }

  const platfromAdminEntity = await Entity.find({firmName: "Otaras,Inc"})
  let platfromAdminEntityId
  if (platfromAdminEntity && platfromAdminEntity[0]) {
    platfromAdminEntityId = platfromAdminEntity[0]._id
  } else {
    return res
      .status(422)
      .send({error: "No platform admin found to inherit"})
  }

  // console.log("platfromAdminEntityId : ", platfromAdminEntityId)
  const platfromAdminConfig = await Config.find({entityId: platfromAdminEntityId})
  if (platfromAdminConfig && platfromAdminConfig[0]) {
    // console.log("platfromAdminConfig id : ", platfromAdminConfig[0]._id)
    // console.log("platfromAdminConfig 1 : ",
    // platfromAdminConfig[0].accessPolicy.length) console.log("platfromAdminConfig
    // 2 : ", platfromAdminConfig[0].checklists.length)
    // console.log("platfromAdminConfig 3 : ",
    // platfromAdminConfig[0].picklists.length)
    try {
      const config = new Config({
        entityId,
        accessPolicy: [...platfromAdminConfig[0].accessPolicy],
        checklists: [...platfromAdminConfig[0].checklists],
        picklists: [...platfromAdminConfig[0].picklists]
      })
      await config.save()
    } catch (err) {
      console.log("err : ", err)
      return res
        .status(422)
        .send({error: "Error in inheriting admin config"})
    }
  } else {
    return res
      .status(422)
      .send({error: "No platform admin config found to inherit"})
  }
  res.send({message: "OK"})
})

/*
configRouter.post("/create-ctp-config", requireAuth, async (req, res) => {
  const { user } = req
  const { entityId, type } = req.body
  if(!user) {
    return res.status(422).send({ error: "No auth user found" })
  }

  const existingConfig = await Config.find({ entityId })
  if(existingConfig && existingConfig[0]) {
    return res.status(422).send({ error: "Config already exists for the given entityId" })
  }

  const rel = await EntityRel.find({ relationshipType: type }).select({ entityParty2: 1 })

  if(rel && rel.length) {
    const ids = rel.map(e => e.entityParty2)
    const modelConfig = await Config.findOne({ entityId: {$in: ids } })
    if(modelConfig) {
      try {
        const config =  new Config({
          entityId,
          accessPolicy: [ ...modelConfig.accessPolicy ],
          checklists: [],
          picklists: []
        })
        await config.save()
      } catch(err) {
        console.log("err : ", err)
        return res.status(422).send({ error: "Error in creating config" })
      }
    } else {
      return res.status(422).send({ error: "No model config found" })
    }
  } else {
    return res.status(422).send({ error: "No model entity relationship found" })
  }

  res.send({ message: "OK" })
})
*/

configRouter.post("/create-ctp-config", requireAuth, async(req, res) => {
  const {user} = req
  const {entityId, type} = req.body
  if (!user) {
    return res
      .status(422)
      .send({error: "No auth user found"})
  }

  const existingConfig = await Config.find({entityId})
  if (existingConfig && existingConfig[0]) {
    return res
      .status(422)
      .send({error: "Config already exists for the given entityId"})
  }

  try {
    const configReturnData = await generateConfigForEntities([entityId])
    console.log("The Config Information", configReturnData)
    res.send({message: "OK"})
  } catch (e) {
    console.log("There is a problem with inserting configuration for entityId")
    return res
      .status(422)
      .send({error: "There was error in inserting configuration information"})
  }

})

// configRouter.get("/test123", async (req, res) => {   const client = await
// MongoClient.connect("mongodb://52.91.222.27:27017")   const db = await
// client.db("munivisor_dev")   const coll = await db.collection("configs")
// const count = await coll.find({entityId:
// ObjectID("5b6064d88c4edb1624d87f77")}).count()   console.log("count : ",
// count)   const start = new Date()   const docsCursor = await coll.aggregate([
//     {       $match: {         entityId: ObjectID("5b6064d88c4edb1624d87f77")
//     },     },     {       $project: {         picklists: { $filter: {
//     input: "$picklists",             as: "picklist",       cond: { $in: [
// "$$picklist.meta.systemName", ["LKUPCOUNTRY"] ] }    }         }       }
// }   ])   const docs = await docsCursor.toArray() console.log("time : ", new
// Date() - start)   res.json(docs) })
configRouter.param("id", configController.findByParam)

configRouter
  .route("/")
  .get(requireAuth, configController.getAll)
  .post(configController.createOne)

configRouter
  .route("/notification")
  .get(requireAuth, configController.getNotifications)
  .post(requireAuth, configController.postNotifications)

configRouter
  .route("/:name")
  .get(requireAuth, configController.getSpecific)

configRouter
  .route("/update-specific")
  .post(requireAuth, configController.postSpecific)
  .delete(requireAuth, configController.deleteSpecific)

configRouter
  .route("/:id")
  .get(configController.getOne)
  .put(configController.updateOne)
  .delete(configController.createOne)
