import express from "express"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import {  
  getDataForProjectDashboard,
  getDataForProjectDashboardEnhanced,
  fetchDataForTaskDashboard,
  fetchDataForTaskDashboardTransacitionBased,
  fetchDataForEntityMasterLists,
  fetchDataForUserMasterLists,
  fetchDataForTaskDashboardComplianceAndOtherTypes,
  fetchDataForDashboardCalendar
} from "./dashboard.controller"

export const dashboardRouter = express.Router()

dashboardRouter
  .route("/projects/")
  .get(requireAuth, getDataForProjectDashboard)

dashboardRouter
  .route("/projectsnew/")
  .post(requireAuth, getDataForProjectDashboardEnhanced)

dashboardRouter
  .route("/tasks")
  .post(requireAuth, fetchDataForDashboardCalendar)

dashboardRouter
  .route("/dash-tasks")
  .post(requireAuth, fetchDataForDashboardCalendar)

dashboardRouter
  .route("/taskstransactions")
  .post(requireAuth, fetchDataForTaskDashboardTransacitionBased)

dashboardRouter
  .route("/tasksothers")
  .post(requireAuth, fetchDataForTaskDashboardComplianceAndOtherTypes)

dashboardRouter
  .route("/entitylist")
  .post(requireAuth, fetchDataForEntityMasterLists)

dashboardRouter
  .route("/userlist")
  .post(requireAuth, fetchDataForUserMasterLists)
