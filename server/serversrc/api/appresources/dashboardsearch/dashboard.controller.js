import { getDataForProjectSearch,getDataForProjectSearchEnhanced } from "./getDataForProjectDashboard"
import { getDataForTaskDashboard,getDataForTaskDashboardTransactionBased,getDataForDashboardCalendar,getDataForTaskDashboardComplianceAndOtherTypes } from "./getDataForTasksDashboard"
import { getEntityDetailsForMasterLists } from "./getDataForEntityMasterLists"
import { getUserDetailsForMasterLists } from "./getDataForUserMasterLists"

export const getDataForProjectDashboard = async (req, res) => {
  console.log("THE SEARCH PARAMETERS SENT FOR DASHBOARD ARE", req.query)
  const { searchterm } = req.query
  const data = await getDataForProjectSearch(req.user, searchterm)
  res.status(200).send(data)
}

export const getDataForProjectDashboardEnhanced = async (req, res) => {
  console.log("THE SEARCH PARAMETERS SENT FOR DASHBOARD ARE", JSON.stringify(req.body))
  const data = await getDataForProjectSearchEnhanced(req.user, req.body)
  res.status(200).send(data)
}

export const fetchDataForDashboardCalendar = async (req, res) => {
  console.log("request body", req.body)
  const data = await getDataForDashboardCalendar(req.user, req.body)
  res.status(200).send(data)
}


export const fetchDataForTaskDashboard = async (req, res) => {
  console.log("request body", req.body)
  const data = await getDataForTaskDashboard(req.user, req.body)
  res.status(200).send(data)
}

export const fetchDataForTaskDashboardTransacitionBased = async (req, res) => {
  console.log("Transaction based dashboard results - request body", req.body)
  const data = await getDataForTaskDashboardTransactionBased(req.user, req.body)
  res.status(200).send(data)
}

export const fetchDataForTaskDashboardComplianceAndOtherTypes = async (req, res) => {
  console.log("Compliance and other dashboard results - request body", req.body)
  const data = await getDataForTaskDashboardComplianceAndOtherTypes(req.user, req.body)
  res.status(200).send(data)
}

export const fetchDataForEntityMasterLists = async (req, res) => {
  console.log("request body", req.body)
  const data = await getEntityDetailsForMasterLists(req.user, req.body)
  res.status(200).send(data)
}

export const fetchDataForUserMasterLists = async (req, res) => {
  console.log("request body", req.body)
  const data = await getUserDetailsForMasterLists(req.user, req.body)
  res.status(200).send(data)
}
