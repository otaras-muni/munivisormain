import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"

import { EntityRel } from "./../models"
import { urlGenerator } from "./../commonservices/urlGeneratorHelper"
import { getAllEntityAndUserInformationForTenant } from "./../commonservices/getAllUserAndEntityDetails"
import { getDateString} from "./../../../helpers"

export const getDataForProjectSearch = async(user, searchterm) => {

  const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
  const newSearchString = searchterm.replace(invalid, "")
  searchterm = new RegExp(newSearchString,"i")

  const alluserdetails = await getAllEntityAndUserInformationForTenant(user)

  const commonPipelineStages = [
    {
      $addFields:{
        currentUserInTran:{
          $in:[ObjectID(user._id), {$ifNull:["$tranUsers",[]]}]
        },
        currentUserFirmInTran:{
          $in:[ObjectID(user.entityId),{$ifNull:["$tranEntities",[]]}]
        }
      }
    },
    {
      $addFields:{
        editTran:{
          $or:[
            {$and: [
              {$eq:["$userEntitlement","global-edit"]},
              {$or:[
                {$eq:["$currentUserInTran",true]},
                {$eq:["$currentUserFirmInTran",true]}
              ]},
            ]},
            {
              $and:[
                {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                {$eq:["$currentUserInTran",true]},
              ]
            }
          ]
        },
        viewTran:{
          $or:[
            {$and: [
              {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
              {$or:[
                {$eq:["$currentUserInTran",true]},
                {$eq:["$currentUserFirmInTran",true]}
              ]},
            ]},
            {
              $and:[
                {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                {$eq:["$currentUserInTran",true]},
              ]
            }
          ]
        }
      }
    },
    {
      $addFields:{
        myUserDetails: { $setIntersection:["$tranUsers",[user._id] ]},
        myFirmUserDetails: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                  ]},
                "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
              },
              {
                "case": {$and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {
                  $and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                  ]},
                "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
              },
              {
                "case": {$and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },

            ],
            "default": [user._id]
          }
        },
        myDealTeamUserDetails: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                  ]},
                "then": "$tranUsers"
              },
              {
                "case": {$and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["tran-edit"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {$and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {
                  $and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                  ]},
                "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
              },
              {
                "case": {$and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {$and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
            ],
            "default": { $setIntersection:["$tranUsers",[user._id] ]}
          }
        },
      }
    },
    {
      $addFields:{

        selectTran:{$or:[
          {$eq:["$editTran",true]},
          {$eq:["$viewTran",true]}
        ]},
        keySearchDetails: { $concat: [ 
          {$ifNull:["$tranStatus",""]},
          {$ifNull:["$activityDescription",""]},
          {$ifNull:["$tranClientName",""]},
          {$ifNull:["$tranAssigneeDetails.userFullName",""]},
          {$ifNull:["$tranFirmName",""]},
          {$ifNull:["$tranType",""]},
          {$ifNull:["$tranSubType",""]},
          {$ifNull:["$tranPrimarySector",""]},
          {$ifNull:["$tranSecondarySector",""]},
        ] 
        }
      },
    },
    {
      $match:{"selectTran":true}
    },
    {
      $match : {
        $or:[
          { "keySearchDetails":{$regex:searchterm,$options:"i"}}
        ]
      }
    },
  ]

  const finalQueryPipeline = [...commonPipelineStages]
  if(searchterm.length === 0 ) {
    finalQueryPipeline.splice(-1,1)
  }

  // console.log("THE FINAL QUERY PIPELINE IS", JSON.stringify(finalQueryPipeline,null,2))


 
  const allTransactionDetails = await EntityRel.aggregate([
    {
      $match:{
        entityParty2:ObjectID(user.entityId)
      }
    },
    {
      $project:{
        tenantId:"$entityParty1",
        loggedInUserEntityId:"$entityParty2",
        relationshipType:"$relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "loggedInUserEntityId",
        foreignField: "entityId",
        as: "myFirmUsers"
      }
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:"$myFirmUsers._id",
      }
    },
    {
      $facet:{
        "deals":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "tenantId",
              foreignField: "dealIssueTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.dealIssueTranClientId",
              clientEntityId: "$trans.dealIssueTranIssuerId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] },
                        else: { $ifNull: [["$trans.dealIssueParticipants.dealPartContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.dealIssueTranAssignedTo",[]]} } ] }, 
                      then: { $ifNull: ["$trans.dealIssueTranAssignedTo", []] },
                      else: { $ifNull: [["$trans.dealIssueTranAssignedTo"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueTranClientId" , []] },
                        else: { $ifNull: [["$trans.dealIssueTranClientId" ], { $ifNull: ["$trans.dealIssueTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] },
                        else: { $ifNull: [["$trans.dealIssueTranIssuerId"], { $ifNull: ["$trans.dealIssueTranIssuerId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] },
                        else: { $ifNull: [["$trans.dealIssueParticipants.dealPartFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] },
                        else: { $ifNull: [["$trans.dealIssueUnderwriters.dealPartFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                  then:"$trans.dealIssueTranProjectDescription",
                  else:"$trans.dealIssueTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
              tranPrimarySector:"$trans.dealIssueTranPrimarySector",
              tranSecondarySector:"$trans.dealIssueTranSecondarySector",
              tranStatus:{$ifNull:["$trans.dealIssueTranStatus","-"]},
              tranClientName:"$trans.dealIssueTranIssuerFirmName",
              tranFirmName:"$trans.dealIssueTranClientFirmName",
              tranType:"$trans.dealIssueTranType",
              // tranType:"Bond Issue",
              tranSubType:"$trans.dealIssueTranSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              assignee:"$trans.dealIssueTranAssignedTo",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",{$ifNull:["$trans.dealIssueTranAssignedTo",[]]}]}
                  }},0]
              },
            },
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "rfps":[
          {
            $lookup:{
              from: "tranagencyrfps",
              localField: "tenantId",
              foreignField: "rfpTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.rfpTranClientId",
              clientEntityId: "$trans.rfpTranIssuerId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpProcessContacts.rfpProcessContactId" } ] },
                        then: { $ifNull: ["$trans.rfpProcessContacts.rfpProcessContactId", []] },
                        else: { $ifNull: [["$trans.rfpProcessContacts.rfpProcessContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpEvaluationTeam.rfpSelEvalContactId" } ] },
                        then: { $ifNull: ["$trans.rfpEvaluationTeam.rfpSelEvalContactId", []] },
                        else: { $ifNull: [["$trans.rfpEvaluationTeam.rfpSelEvalContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpParticipants.rfpParticipantContactId" } ] },
                        then: { $ifNull: ["$trans.rfpParticipants.rfpParticipantContactId", []] },
                        else: { $ifNull: [["$trans.rfpParticipants.rfpParticipantContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: "$trans.rfpTranAssignedTo" } ] },
                      then: { $ifNull: ["$trans.rfpTranAssignedTo", []] },
                      else: { $ifNull: [["$trans.rfpTranAssignedTo"], []] }
                    }
                    }
                    , []] },
                  { $ifNull: [["$trans.createdByUser"], []] }
                ]
              },
              tranEntities:{
                $setUnion:[
                  ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
                  "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
                  "$trans.rfpProcessContacts.rfpProcessFirmId",
                  "$trans.rfpParticipants.rfpParticipantFirmId",
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
                  then:"$trans.rfpTranProjectDescription",
                  else:"$trans.rfpTranIssueName"},
              },
              tranParAmount:"-",
              tranExpectedAwardDate:"-",
              tranActualAwardDate:"-",
              tranPricingDate:"-",
              tranPrimarySector:"$trans.rfpTranPrimarySector",
              tranSecondarySector:"$trans.rfpTranSecondarySector",
              tranStatus:{$ifNull:["$trans.rfpTranStatus","-"]},
              tranClientName:"$trans.rfpTranIssuerFirmName",
              tranFirmName:"$trans.rfpTranClientFirmName",
              tranType:"$trans.rfpTranType",
              tranSubType:"$trans.rfpTranSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId","$trans.rfpTranAssignedTo"]}
                  }},0]
              }
            },
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "bankloans":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] },
                        else: { $ifNull: [["$trans.bankLoanParticipants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                      else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranClientId" , []] },
                        else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] },
                        else: { $ifNull: [["$trans.bankLoanParticipants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  }
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranClientName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
                  }},0]
              },
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "derivatives":[
          {
            $lookup:{
              from: "tranderivatives",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] },
                        else: { $ifNull: [["$trans.derivativeParticipants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                      else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranClientId" , []] },
                        else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] },
                        else: { $ifNull: [["$trans.derivativeParticipants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate","-"]},
              tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranClientName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
                  }},0]
              },
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "marfps":[
          {
            $lookup:{
              from: "actmarfps",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] },
                        else: { $ifNull: [["$trans.maRfpParticipants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                      else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                        then: { $ifNull: ["$trans.actIssuerClient" , []] },
                        else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] },
                        else: { $ifNull: [["$trans.maRfpParticipants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actProjectName",
                  else:"$trans.actIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate","-"]},
              tranPrimarySector:"$trans.actPrimarySector",
              tranSecondarySector:"$trans.actSecondarySector",
              tranStatus:{$ifNull:["$trans.actStatus","-"]},
              tranClientName:"$trans.actIssuerClientEntityName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
                  }},0]
              },
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "others":[
          {
            $lookup:{
              from: "tranagencyothers",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.partContactId", []] },
                        else: { $ifNull: [["$trans.participants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                      else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranClientId" , []] },
                        else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.partFirmId", []] },
                        else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] },
                        else: { $ifNull: [["$trans.actTranUnderwriters.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranClientName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
                  }},0]
              },
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
       
        "busdevs":[
          {
            $lookup:{
              from: "actbusdevs",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] },
                        else: { $ifNull: [["$trans.participants.rfpParticipantContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                      else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                        then: { $ifNull: ["$trans.actIssuerClient" , []] },
                        else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.partFirmId", []] },
                        else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  }
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actProjectName",
                  else:"$trans.actIssueName"},
              },
              tranParAmount:"-",
              tranExpectedAwardDate:"-",
              tranActualAwardDate:"-",
              tranPricingDate:"-",
              tranPrimarySector:"$trans.actPrimarySector",
              tranSecondarySector:"$trans.actSecondarySector",
              tranStatus:{$ifNull:["$trans.actStatus","-"]},
              tranClientName:"$trans.actIssuerClientEntityName",
              tranFirmName:"$trans.actLeadFinAdvClientEntityName",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
                  }},0]
              },
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
      }
    },
  ])

  const finalMap = {
    busdevs:"Projects",
    others:"Projects",
    rfps:"Projects",
    marfps:"MA Request For Proposals",
    deals:"Transactions",
    bankloans:"Transactions",
    derivatives:"Transactions"
  }

  const retData = isEmpty(allTransactionDetails[0]) ? [] : allTransactionDetails[0]

  
  const transRetData = Object.keys(retData).reduce( (revTranData, tranCategory ) => {
    const revCategory = finalMap[tranCategory]
    if(revTranData[revCategory]) {
      return {...revTranData, [revCategory]:[...revTranData[revCategory],...Object.values(retData[tranCategory])]} 
    } 
    return {...revTranData, [revCategory]:Object.values(retData[tranCategory]) }
  },{})


  // console.log(JSON.stringify(retData, null, 2))

  /*  const flattenedIds = Object.keys(retData).reduce ( (acc, k) => {
    const arrData = retData[k].reduce( (allTranIds, tr) => {
      // console.log("WHAT HAS GONE WRONG",k, tr.allIds)
      const allIdsToString = tr.allIds.map( aa => aa.toString())
      return [ ...new Set([...allTranIds, ...allIdsToString])]
    },[])
    return [ ...new Set([...acc, ...arrData])]
  },[]) */

  const flattenedIds = Object.keys(retData).reduce ( (acc, k) => {
    const arrData = retData[k].reduce( (allTranIds, tr) => {
      // console.log("WHAT HAS GONE WRONG",k, tr.allIds)
      const allIdsToString = (tr.allIds || []).map( aa => {
        if( aa) {
          return aa.toString()
        } 
        return ObjectID()
      })
      return [ ...new Set([...allTranIds, ...allIdsToString])]
    },[])
    return [ ...new Set([...acc, ...arrData])]
  },[])


  const newFlattened = [... new Set(flattenedIds)]
  const allUrls = await urlGenerator(user, [... new Set(flattenedIds)])
  return { allData:transRetData, allflattenedIds:newFlattened, urls:allUrls, retData }
}


const defaultTypes = [ "Client","Prospect","Third Party", "Undefined"]

export const getDataForProjectSearchEnhanced = async(user, payload,) => {

  const alluserdetails = await getAllEntityAndUserInformationForTenant(user)
  const { freeTextSearchTerm, tranCreatePeriod, tranPricingPeriod, serverPerformPagination, tranStatus, entityTypes } = payload

  let newEntityTypes = defaultTypes
  if ( entityTypes ) {
    newEntityTypes = entityTypes
  }
  
  const matchQueryNew = []

  if (!isEmpty(tranCreatePeriod)) {
    matchQueryNew.push(getDateString("createdAt",tranCreatePeriod))
  }

  if (!isEmpty(tranPricingPeriod)) {
    matchQueryNew.push(getDateString("tranPricingDate",tranPricingPeriod))
  }

  if (!isEmpty(tranStatus)) {
    matchQueryNew.push({ "tranStatus": {$in:[...tranStatus.map( s => s.toUpperCase())]} })
  }

  if (!isEmpty(freeTextSearchTerm)) {
    matchQueryNew.push({ "keySearchDetails":{$regex:freeTextSearchTerm,$options:"i"}})
  }

  const mapper = {
    transactions : "Transactions",
    projects : "Projects",
    marfps : "MARFPS"
  }

  const { groups } = payload

  const serverSidePaginationParameters = Object.keys(payload.groups).reduce (( acc, grp) => {
    if(serverPerformPagination) {
      const dataObject = {
        [`${grp}-data`]:[
          {$match:{displayCategory:mapper[grp]}},
          {$sort: { ...groups[grp].sortFields}},
          {$skip: groups[grp].currentPage * groups[grp].size},
          {$limit:groups[grp].size}
        ]
      }
      const metaObject = {
        [`${grp}-meta`]:[
          {$match:{displayCategory:mapper[grp]}},
          { $count: "total" }, 
          { $addFields: { pages: { $ceil: { $divide: ["$total", groups[grp].size] } } } }
        ]
      }
      return {...acc, ...dataObject, ...metaObject }
    }
    const dataObject = {
      [`${grp}-data`]:[
        {$match:{displayCategory:mapper[grp]}},
      ]
    }
    return {...acc, ...dataObject }
       
  },{})

  console.log("constructed payload", JSON.stringify(matchQueryNew, null, 2))
  console.log("Server Side Pagination", JSON.stringify(serverSidePaginationParameters, null, 2))

  const commonPipelineStages = [
    {
      $addFields:{
        currentUserInTran:{
          $in:[ObjectID(user._id), {$ifNull:["$tranUsers",[]]}]
        },
        currentUserFirmInTran:{
          $in:[ObjectID(user.entityId),{$ifNull:["$tranEntities",[]]}]
        }
      }
    },
    {
      $addFields:{
        editTran:{
          $or:[
            {$and: [
              {$eq:["$userEntitlement","global-edit"]},
              {$or:[
                {$eq:["$currentUserInTran",true]},
                {$eq:["$currentUserFirmInTran",true]}
              ]},
            ]},
            {
              $and:[
                {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                {$eq:["$currentUserInTran",true]},
              ]
            }
          ]
        },
        viewTran:{
          $or:[
            {$and: [
              {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
              {$or:[
                {$eq:["$currentUserInTran",true]},
                {$eq:["$currentUserFirmInTran",true]}
              ]},
            ]},
            {
              $and:[
                {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                {$eq:["$currentUserInTran",true]},
              ]
            }
          ]
        }
      }
    },
    {
      $addFields:{
        myUserDetails: { $setIntersection:["$tranUsers",[user._id] ]},
        myFirmUserDetails: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                  ]},
                "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
              },
              {
                "case": {$and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {
                  $and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                  ]},
                "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
              },
              {
                "case": {$and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },

            ],
            "default": [user._id]
          }
        },
        myDealTeamUserDetails: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                  ]},
                "then": "$tranUsers"
              },
              {
                "case": {$and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["tran-edit"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {$and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {
                  $and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                  ]},
                "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
              },
              {
                "case": {$and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
              {
                "case": {$and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["tran-readonly"]]},
                ]},
                "then": { $setIntersection:["$tranUsers",[user._id] ]}
              },
            ],
            "default": { $setIntersection:["$tranUsers",[user._id] ]}
          }
        },
      }
    },
    {
      $addFields:{
        selectTran:{$or:[
          {$eq:["$editTran",true]},
          {$eq:["$viewTran",true]}
        ]},
        keySearchDetails: { $concat: [ 
          {$ifNull:["$tranStatus",""]},
          {$ifNull:["$activityDescription",""]},
          {$ifNull:["$tranClientName",""]},
          {$ifNull:["$tranAssigneeDetails.userFullName",""]},
          {$ifNull:["$tranFirmName",""]},
          {$ifNull:["$tranType",""]},
          {$ifNull:["$tranSubType",""]},
          {$ifNull:["$tranPrimarySector",""]},
          {$ifNull:["$tranSecondarySector",""]},
        ] 
        }
      },
    },
    {
      $match:{"selectTran":true}
    },
    {
      $match : {
        $and:[
          ...matchQueryNew
        ]
      }
    },
  ]

  const finalQueryPipeline = [...commonPipelineStages]
  if(isEmpty(matchQueryNew) ) {
    finalQueryPipeline.splice(-1,1)
  }

  // console.log("THE FINAL QUERY PIPELINE IS", JSON.stringify(finalQueryPipeline,null,2))
 
  const allTransactionDetails = await EntityRel.aggregate([
    {
      $match:{
        entityParty2:ObjectID(user.entityId)
      }
    },
    {
      $project:{
        tenantId:"$entityParty1",
        loggedInUserEntityId:"$entityParty2",
        relationshipType:"$relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "loggedInUserEntityId",
        foreignField: "entityId",
        as: "myFirmUsers"
      }
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:"$myFirmUsers._id",
      }
    },
    {
      $facet:{
        "deals":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "tenantId",
              foreignField: "dealIssueTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.dealIssueTranClientId",
              clientEntityId: "$trans.dealIssueTranIssuerId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] },
                        else: { $ifNull: [["$trans.dealIssueParticipants.dealPartContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.dealIssueTranAssignedTo",[]]} } ] }, 
                      then: { $ifNull: ["$trans.dealIssueTranAssignedTo", []] },
                      else: { $ifNull: [["$trans.dealIssueTranAssignedTo"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueTranClientId" , []] },
                        else: { $ifNull: [["$trans.dealIssueTranClientId" ], { $ifNull: ["$trans.dealIssueTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] },
                        else: { $ifNull: [["$trans.dealIssueTranIssuerId"], { $ifNull: ["$trans.dealIssueTranIssuerId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] },
                        else: { $ifNull: [["$trans.dealIssueParticipants.dealPartFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] },
                        else: { $ifNull: [["$trans.dealIssueUnderwriters.dealPartFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                  then:{$toUpper:"$trans.dealIssueTranProjectDescription"},
                  else:{$toUpper:"$trans.dealIssueTranIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.dealIssueTranProjectDescription"," "]},
              tranIssueName:{$ifNull:["$trans.dealIssueTranIssueName"," "]},
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate",new Date(8640000000000000)]},
              tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate",new Date(8640000000000000)]},
              tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate",new Date(8640000000000000)]},
              tranPrimarySector:{$toUpper:"$trans.dealIssueTranPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.dealIssueTranSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.dealIssueTranStatus","-"]}},
              tranClientName:{ $toUpper: "$trans.dealIssueTranIssuerFirmName" },
              tranFirmName:{$toUpper:"$trans.dealIssueTranClientFirmName"},
              tranType:{$toUpper:"$trans.dealIssueTranType"},
              tranSubType:{$toUpper:"$trans.dealIssueTranSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              assignee:"$trans.dealIssueTranAssignedTo",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",{$ifNull:["$trans.dealIssueTranAssignedTo",[]]}]}
                  }},0]
              },
              displayCategory:"Transactions",
              createdAt :"$trans.createdAt"
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "rfps":[
          {
            $lookup:{
              from: "tranagencyrfps",
              localField: "tenantId",
              foreignField: "rfpTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.rfpTranClientId",
              clientEntityId: "$trans.rfpTranIssuerId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpProcessContacts.rfpProcessContactId" } ] },
                        then: { $ifNull: ["$trans.rfpProcessContacts.rfpProcessContactId", []] },
                        else: { $ifNull: [["$trans.rfpProcessContacts.rfpProcessContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpEvaluationTeam.rfpSelEvalContactId" } ] },
                        then: { $ifNull: ["$trans.rfpEvaluationTeam.rfpSelEvalContactId", []] },
                        else: { $ifNull: [["$trans.rfpEvaluationTeam.rfpSelEvalContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpParticipants.rfpParticipantContactId" } ] },
                        then: { $ifNull: ["$trans.rfpParticipants.rfpParticipantContactId", []] },
                        else: { $ifNull: [["$trans.rfpParticipants.rfpParticipantContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: "$trans.rfpTranAssignedTo" } ] },
                      then: { $ifNull: ["$trans.rfpTranAssignedTo", []] },
                      else: { $ifNull: [["$trans.rfpTranAssignedTo"], []] }
                    }
                    }
                    , []] },
                  { $ifNull: [["$trans.createdByUser"], []] }
                ]
              },
              tranEntities:{
                $setUnion:[
                  ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
                  "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
                  "$trans.rfpProcessContacts.rfpProcessFirmId",
                  "$trans.rfpParticipants.rfpParticipantFirmId",
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
                  then:{$toUpper:"$trans.rfpTranProjectDescription"},
                  else:{$toUpper:"$trans.rfpTranIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.rfpTranProjectDescription",""]},
              tranIssueName:{$ifNull:["$trans.rfpTranIssueName"," "]},
              tranParAmount:"-",
              tranExpectedAwardDate:new Date(8640000000000000),
              tranActualAwardDate:new Date(8640000000000000),
              tranPricingDate:new Date(8640000000000000),
              tranPrimarySector:{$toUpper:"$trans.rfpTranPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.rfpTranSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.rfpTranStatus","-"]}},
              tranClientName:{ $toUpper: "$trans.rfpTranIssuerFirmName"},
              tranFirmName:{$toUpper:"$trans.rfpTranClientFirmName"},
              tranType:{$toUpper:"$trans.rfpTranType"},
              tranSubType:{$toUpper:"$trans.rfpTranSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId","$trans.rfpTranAssignedTo"]}
                  }},0]
              },
              displayCategory:"Projects",
              createdAt :"$trans.createdAt"
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "bankloans":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] },
                        else: { $ifNull: [["$trans.bankLoanParticipants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                      else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranClientId" , []] },
                        else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] },
                        else: { $ifNull: [["$trans.bankLoanParticipants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  }
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:{$toUpper:"$trans.actTranProjectDescription"},
                  else:{$toUpper:"$trans.actTranIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.actTranProjectDescription"," "]},
              tranIssueName:{$ifNull:["$trans.actTranIssueName"," "]},
              tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate",new Date(8640000000000000)]},
              tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate",new Date(8640000000000000)]},
              tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate",new Date(8640000000000000)]},
              tranPrimarySector:{$toUpper:"$trans.actTranPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.actTranSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.actTranStatus","-"]}},
              tranClientName:{ $toUpper: "$trans.actTranClientName"},
              tranFirmName:{$toUpper:"$trans.actTranFirmName"},
              tranType:{$toUpper:"$trans.actTranType"},
              tranSubType:{$toUpper:"$trans.actTranSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
                  }},0]
              },
              displayCategory:"Transactions",
              createdAt :"$trans.createdAt"
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "derivatives":[
          {
            $lookup:{
              from: "tranderivatives",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] },
                        else: { $ifNull: [["$trans.derivativeParticipants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                      else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranClientId" , []] },
                        else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] },
                        else: { $ifNull: [["$trans.derivativeParticipants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:{$toUpper:"$trans.actTranProjectDescription"},
                  else:{$toUpper:"$trans.actTranIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.actTranProjectDescription"," "]},
              tranIssueName:{$ifNull:["$trans.actTranIssueName"," "]},
              tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate",new Date(8640000000000000)]},
              tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate",new Date(8640000000000000)]},
              tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate",new Date(8640000000000000)]},
              tranPrimarySector:{$toUpper:"$trans.actTranPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.actTranSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.actTranStatus","-"]}},
              tranClientName:{ $toUpper: "$trans.actTranClientName"},
              tranFirmName:{$toUpper:"$trans.actTranFirmName"},
              tranType:{$toUpper:"$trans.actTranType"},
              tranSubType:{$toUpper:"$trans.actTranSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
                  }},0]
              },
              displayCategory:"Transactions",
              createdAt :"$trans.createdAt"

            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "marfps":[
          {
            $lookup:{
              from: "actmarfps",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] },
                        else: { $ifNull: [["$trans.maRfpParticipants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                      else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                        then: { $ifNull: ["$trans.actIssuerClient" , []] },
                        else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] },
                        else: { $ifNull: [["$trans.maRfpParticipants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:{$toUpper:"$trans.actProjectName"},
                  else:{$toUpper:"$trans.actIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.actProjectName"," "]},
              tranIssueName:{$ifNull:["$trans.actIssueName"," "]},
              tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate",new Date(8640000000000000)]},
              tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate",new Date(8640000000000000)]},
              tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate",new Date(8640000000000000)]},
              tranPrimarySector:{$toUpper:"$trans.actPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.actSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.actStatus","-"]}},
              tranClientName:{ $toUpper: "$trans.actIssuerClientEntityName" },
              tranFirmName:{$toUpper:"$trans.actTranFirmName"},
              tranType:{$toUpper:"$trans.actType"},
              tranSubType:{$toUpper:"$trans.actSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
                  }},0]
              },
              displayCategory:"MARFPS",
              createdAt :"$trans.createdAt"
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
        "others":[
          {
            $lookup:{
              from: "tranagencyothers",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partContactId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.partContactId", []] },
                        else: { $ifNull: [["$trans.participants.partContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                      else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranClientId" , []] },
                        else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.partFirmId", []] },
                        else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] },
                        else: { $ifNull: [["$trans.actTranUnderwriters.partFirmId"], []] }
                      }
                      }
                      , []] 
                  },
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:{$toUpper:"$trans.actTranProjectDescription"},
                  else:{$toUpper:"$trans.actTranIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.actTranProjectDescription"," "]},
              tranIssueName:{$ifNull:["$trans.actTranIssueName"," "]},
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate",new Date(8640000000000000)]},
              tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate",new Date(8640000000000000)]},
              tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate",new Date(8640000000000000)]},
              tranPrimarySector:{$toUpper:"$trans.actTranPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.actTranSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.actTranStatus","-"]}},
              tranClientName:{$toUpper:"$trans.actTranClientName"},
              tranFirmName:{$toUpper:"$trans.actTranFirmName"},
              tranType:{$toUpper:"$trans.actTranType"},
              tranSubType:{$toUpper:"$trans.actTranSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
                  }},0]
              },
              displayCategory:"Projects",
              createdAt :"$trans.createdAt"
            },
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },

          ...finalQueryPipeline
        ],
        "busdevs":[
          {
            $lookup:{
              from: "actbusdevs",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              tranUsers:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] },
                        else: { $ifNull: [["$trans.participants.rfpParticipantContactId"], []] }
                      }
                      }
                      , []] 
                  },
                  { $ifNull: [    
                    { $cond:{
                      if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] }, 
                      then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                      else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                    }
                    }
                    , []] 
                  },
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                        then: { $ifNull: ["$trans.createdByUser", []] },
                        else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                      }
                      }
                      , []] 
                  }, 
                ]
              },
              tranEntities:{
                $setUnion:[
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                        then: { $ifNull: ["$trans.actTranFirmId" , []] },
                        else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                        then: { $ifNull: ["$trans.actIssuerClient" , []] },
                        else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                      }
                      }
                      , []] 
                  }, 
                  { 
                    $ifNull: [    
                      { $cond:{
                        if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                        then: { $ifNull: ["$trans.participants.partFirmId", []] },
                        else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                      }
                      }
                      , []] 
                  }
                ]
              },
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:{$toUpper:"$trans.actProjectName"},
                  else:{$toUpper:"$trans.actIssueName"}},
              },
              tranProjectName:{$ifNull:["$trans.actProjectName"," "]},
              tranIssueName:{$ifNull:["$trans.actIssueName"," "]},
              tranParAmount:"-",
              tranExpectedAwardDate:new Date(8640000000000000),
              tranActualAwardDate:new Date(8640000000000000),
              tranPricingDate:"-",
              tranPrimarySector:{$toUpper:"$trans.actPrimarySector"},
              tranSecondarySector:{$toUpper:"$trans.actSecondarySector"},
              tranStatus:{$toUpper:{$ifNull:["$trans.actStatus","-"]}},
              tranClientName:{$toUpper:"$trans.actIssuerClientEntityName"},
              tranFirmName:{$toUpper:"$trans.actLeadFinAdvClientEntityName"},
              tranType:{$toUpper:"$trans.actType"},
              tranSubType:{$toUpper:"$trans.actSubType"},
              userEntitlement:user.userEntitlement,
              relationshipType:"$relationshipType",
              myFirmUserIds:"$loggedInFirmUserIds",
              tranAssigneeDetails:{
                $arrayElemAt:[
                  {$filter:{
                    input:alluserdetails,
                    as:"alltendetails",
                    cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
                  }},0]
              },
              displayCategory:"Projects",
              createdAt :"$trans.createdAt"
            }
          },
          {
            $addFields:{
              allIds:{
                $setUnion:[
                  "$tranUsers",
                  "$tranEntities",
                  ["$tranId"]
                ]
              },
            }
          },
          ...finalQueryPipeline
        ],
      }
    },
    { 
      "$project": {
        "data": { "$concatArrays": ["$deals","$bankloans","$derivatives","$rfps","$busdevs","$others","$marfps"] },
      }
    },
    { "$unwind": "$data" },
    { "$replaceRoot": { "newRoot": "$data" } },
    {
      $facet: {
        ...serverSidePaginationParameters,
        "allids":[
          {$unwind:"$allIds"},
          {$group:{_id:"$allIds"}}
        ]
      }
    }
  ])

  const retData = allTransactionDetails[0]
  // const allUniqueIds = retData.allids.reduce ( (acc,  idobj) => [...acc,idobj._id ],[])
  // const allUrls = await urlGenerator(user, [...allUniqueIds])
  return { allData:retData, allflattenedIds:[], urls:{}, retData }
  // return { allData:retData }
}
