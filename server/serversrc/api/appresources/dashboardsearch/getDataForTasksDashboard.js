import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"
import moment from "moment"
import {EntityRel,Tasks} from "./../models"
import {urlGenerator} from "./../commonservices/urlGeneratorHelper"
import { getDateString} from "./../../../helpers"

export const getDataForTaskDashboard = async(user, {groupBy, activityCategory, activityTime, taskStatus, searchTerm}) => {

  // const refDate = new Date(new Date().getTime()+(0*24*60*60*1000)) const
  // refDate = new Date()

  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchTerm.replace(invalid, "")
    searchTerm = newSearchString

    // searchTerm = new RegExp(newSearchString,"i")

    let matchQuery = {}
    let otherMatchQuery = {}

    if (activityCategory === "myfirm") {
      matchQuery = {
        ...matchQuery,
        tasksMyFirmTranActivities: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksForMyFirmUsers: true
      }
    } else if (activityCategory === "dealteam") {
      matchQuery = {
        ...matchQuery,
        tasksMyDealTeamTranActivities: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksForMyFirmUsers: false
      }
    } else if (activityCategory === "self") {
      matchQuery = {
        ...matchQuery,
        tasksMyTranActivities: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksForMyFirmUsers: true,
        tasksForMe: true
      }
    } else {
      matchQuery = {
        ...matchQuery,
        tasksMyDealTeamTranActivities: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksForMyFirmUsers: true
      }
    }

    if (activityTime === "day") {
      matchQuery = {
        ...matchQuery,
        tasksDueToday: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksDueToday: true
      }
    } else if (activityTime === "week") {
      matchQuery = {
        ...matchQuery,
        tasksDueThisWeek: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksDueThisWeek: true
      }
    } else if (activityTime === "month") {
      matchQuery = {
        ...matchQuery,
        tasksDueThisMonth: true
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        tasksDueThisMonth: true
      }
    }

    if (taskStatus === "openactive") {
      matchQuery = {
        ...matchQuery,
        taskStatus: {
          $in: ["Open", "Active"]
        }
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        taskStatus: {
          $in: ["Open", "Active"]
        }
      }

    } else if (taskStatus === "closedcancelled") {
      matchQuery = {
        ...matchQuery,
        taskStatus: {
          $in: ["Closed", "Cancelled", "Complete", "Completed", "Dropped"]
        }
      }
      otherMatchQuery = {
        ...otherMatchQuery,
        taskStatus: {
          $in: ["Closed", "Cancelled", "Complete", "Completed", "Dropped"]
        }
      }
    }

    console.log("MATCH QUERY", matchQuery)

    const today = new Date()
    const localoffset = -(today.getTimezoneOffset() / 60)
    const refDate = new Date(new Date().getTime() + localoffset * 3600 * 1000)

    const originalCommonPipelineStages = [
      {
        $addFields: {
          currentUserInTran: {
            $in: [
              ObjectID(user._id), {
                $ifNull: ["$tranUsers", []
                ]
              }
            ]
          },
          currentUserFirmInTran: {
            $in: [
              ObjectID(user.entityId), {
                $ifNull: ["$tranEntities", []
                ]
              }
            ]
          }
        }
      }, {
        $addFields: {
          editTran: {
            $or: [
              {
                $and: [
                  {
                    $eq: ["$userEntitlement", "global-edit"]
                  }, {
                    $or: [
                      {
                        $eq: ["$currentUserInTran", true]
                      }, {
                        $eq: ["$currentUserFirmInTran", true]
                      }
                    ]
                  }
                ]
              }, {
                $and: [
                  {
                    $in: [
                      "$userEntitlement",
                      ["tran-edit", "global-readonly-tran-edit"]
                    ]
                  }, {
                    $eq: ["$currentUserInTran", true]
                  }
                ]
              }
            ]
          },
          viewTran: {
            $or: [
              {
                $and: [
                  {
                    $in: [
                      "$userEntitlement",
                      ["global-edit", "global-readonly", "global-readonly-tran-edit"]
                    ]
                  }, {
                    $or: [
                      {
                        $eq: ["$currentUserInTran", true]
                      }, {
                        $eq: ["$currentUserFirmInTran", true]
                      }
                    ]
                  }
                ]
              }, {
                $and: [
                  {
                    $in: [
                      "$userEntitlement",
                      ["tran-edit", "tran-readonly"]
                    ]
                  }, {
                    $eq: ["$currentUserInTran", true]
                  }
                ]
              }
            ]
          }
        }
      },
      /*      {
        $addFields:{
          myUserDetails: { $setIntersection:["$tranUsers",[user._id] ]},
          myFirmUserDetails: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                    ]},
                  "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
                },
                {
                  "case": {$and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                  ]},
                  "then": { $setIntersection:["$tranUsers",[user._id] ]}
                },
                {
                  "case": {
                    $and:[
                      {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                      {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                    ]},
                  "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
                },
                {
                  "case": {$and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                  ]},
                  "then": { $setIntersection:["$tranUsers",[user._id] ]}
                },

              ],
              "default": [user._id]
            }
          },
          myDealTeamUserDetails: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                    ]},
                  "then": "$tranUsers"
                },
                {
                  "case": {$and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["tran-edit"]]},
                  ]},
                  "then": { $setIntersection:["$tranUsers",[user._id] ]}
                },
                {
                  "case": {$and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["tran-readonly"]]},
                  ]},
                  "then": { $setIntersection:["$tranUsers",[user._id] ]}
                },
                {
                  "case": {
                    $and:[
                      {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                      {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                    ]},
                  "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
                },
                {
                  "case": {$and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                  ]},
                  "then": { $setIntersection:["$tranUsers",[user._id] ]}
                },
                {
                  "case": {$and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["tran-readonly"]]},
                  ]},
                  "then": { $setIntersection:["$tranUsers",[user._id] ]}
                },
              ],
              "default": { $setIntersection:["$tranUsers",[user._id] ]}
            }
          },
        }
      }, */

      {
        $addFields: {
          selectTran: {
            $or: [
              {
                $eq: ["$editTran", true]
              },
              // {$eq:["$viewTran",true]}
            ]
          },
          myUserDetails: {
            $setIntersection: [
              "$tranUsers",
              [user._id]
            ]
          },
          myFirmUserDetails: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and: [
                      {
                        "$eq": ["$relationshipType", "Self"]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["global-edit", "global-readonly", "global-readonly-tran-edit"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setUnion: [
                      "$myFirmUserIds",
                      [ObjectID(user.entityId)]
                    ]
                  }
                  // "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
                }, {
                  "case": {
                    $and: [
                      {
                        "$eq": ["$relationshipType", "Self"]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["tran-edit", "tran-readonly"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setIntersection: [
                      "$tranUsers",
                      [user._id]
                    ]
                  }
                }, {
                  "case": {
                    $and: [
                      {
                        $in: [
                          "$relationshipType",
                          ["Client", "Prospect", "Third Party"]
                        ]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["global-edit", "global-readonly", "global-readonly-tran-edit"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setUnion: [
                      "$myFirmUserIds",
                      [ObjectID(user.entityId)]
                    ]
                  }
                  // "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
                }, {
                  "case": {
                    $and: [
                      {
                        $in: [
                          "$relationshipType",
                          ["Client", "Prospect", "Third Party"]
                        ]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["tran-edit", "tran-readonly"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setIntersection: [
                      "$tranUsers",
                      [user._id]
                    ]
                  }
                }
              ],
              "default": [user._id]
            }
          },
          myDealTeamUserDetails: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and: [
                      {
                        "$eq": ["$relationshipType", "Self"]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["global-edit", "global-readonly", "global-readonly-tran-edit"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setUnion: [
                      "$tranUsers",
                      "$myFirmUserIds",
                      "$tranEntities",
                      [ObjectID(user.entityId)]
                    ]
                  }
                }, {
                  "case": {
                    $and: [
                      {
                        "$eq": ["$relationshipType", "Self"]
                      }, {
                        $in: ["$userEntitlement", ["tran-edit"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setIntersection: [
                      "$tranUsers",
                      [user._id]
                    ]
                  }
                }, {
                  "case": {
                    $and: [
                      {
                        "$eq": ["$relationshipType", "Self"]
                      }, {
                        $in: ["$userEntitlement", ["tran-readonly"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setIntersection: [
                      "$tranUsers",
                      [user._id]
                    ]
                  }
                }, {
                  "case": {
                    $and: [
                      {
                        $in: [
                          "$relationshipType",
                          ["Client", "Prospect", "Third Party"]
                        ]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["global-edit", "global-readonly"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setUnion: [
                      {
                        $setIntersection: ["$tranUsers", "$myFirmUserIds"]
                      },
                      "$tranEntities",
                      [ObjectID(user.entityId)]
                    ]
                  }
                }, {
                  "case": {
                    $and: [
                      {
                        $in: [
                          "$relationshipType",
                          ["Client", "Prospect", "Third Party"]
                        ]
                      }, {
                        $in: [
                          "$userEntitlement",
                          ["tran-edit", "global-readonly-tran-edit"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setIntersection: [
                      "$tranUsers",
                      [user._id]
                    ]
                  }
                }, {
                  "case": {
                    $and: [
                      {
                        $in: [
                          "$relationshipType",
                          ["Client", "Prospect", "Third Party"]
                        ]
                      }, {
                        $in: ["$userEntitlement", ["tran-readonly"]
                        ]
                      }
                    ]
                  },
                  "then": {
                    $setIntersection: [
                      "$tranUsers",
                      [user._id]
                    ]
                  }
                }
              ],
              "default": {
                $setIntersection: [
                  "$tranUsers",
                  [user._id]
                ]
              }
            }
          }
        }
      }, {
        $lookup: {
          from: "mvtasks",
          localField: "tranId",
          foreignField: "relatedActivityDetails.activityId",
          as: "relatedTasks"
        }
      }, {
        $unwind: {
          "path": "$relatedTasks",
          "preserveNullAndEmptyArrays": false
        }
      }, {
        $addFields: {
          taskId: "$relatedTasks._id",
          taskDescription: "$relatedTasks.taskDetails.taskDescription",
          taskActivityContext: "$relatedTasks.relatedActivityDetails.activityContext",
          taskEndDate: "$relatedTasks.taskDetails.taskEndDate",
          taskStatus: "$relatedTasks.taskDetails.taskStatus",
          taskAssigneeId: "$relatedTasks.taskDetails.taskAssigneeUserId",
          taskAssigneeName: "$relatedTasks.taskDetails.taskAssigneeName",
          tasksMyFirmTranActivities: {
            $and: [
              {
                $eq: ["$relatedTasks.relatedActivityDetails.activityId", "$tranId"]
              }, {
                $in: ["$relatedTasks.taskDetails.taskAssigneeUserId", "$myFirmUserDetails"]
              }
            ]
          },
          tasksMyTranActivities: {
            $and: [
              {
                $eq: ["$relatedTasks.relatedActivityDetails.activityId", "$tranId"]
              }, {
                $eq: ["$relatedTasks.taskDetails.taskAssigneeUserId", user._id]
              }
            ]
          },
          tasksMyDealTeamTranActivities: {
            $and: [
              {
                $eq: ["$relatedTasks.relatedActivityDetails.activityId", "$tranId"]
              }, {
                $in: ["$relatedTasks.taskDetails.taskAssigneeUserId", "$myDealTeamUserDetails"]
              }
            ]
          },
          "taskDueYear": {
            "$year": "$relatedTasks.taskDetails.taskEndDate"
          },
          "taskDueMonth": {
            "$month": "$relatedTasks.taskDetails.taskEndDate"
          },
          "taskDueWeek": {
            "$week": "$relatedTasks.taskDetails.taskEndDate"
          },
          "taskDueDay": {
            "$dayOfMonth": "$relatedTasks.taskDetails.taskEndDate"
          },
          "refDateYear": {
            $year: refDate
          },
          "refDateMonth": {
            $month: refDate
          },
          "refDateWeek": {
            $week: refDate
          },
          "refDateDay": {
            $dayOfMonth: refDate
          }
        }
      }, {
        $project: {
          "relatedTasks": 0
        }
      }, {
        $addFields: {
          tasksDueToday: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }, {
                $eq: ["$taskDueDay", "$refDateDay"]
              }
            ]
          },
          tasksDueThisWeek: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }, {
                $eq: ["$taskDueWeek", "$refDateWeek"]
              }
            ]
          },
          tasksDueThisMonth: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }
            ]
          },
          /*
                          activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                    then:"$trans.dealIssueTranProjectDescription",
                    else:"$trans.dealIssueTranIssueName"},
                },
                tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
                tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
                tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
                tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
                tranPrimarySector:"$trans.dealIssueTranPrimarySector",
                tranSecondarySector:"$trans.dealIssueTranSecondarySector",
                tranStatus:{$ifNull:["$trans.dealIssueTranStatus","-"]},
                tranClientName:"$trans.dealIssueTranIssuerFirmName",
                tranFirmName:"$trans.dealIssueTranClientFirmName",
                tranType:"$trans.dealIssueTranType",
                tranSubType:"$trans.dealIssueTranSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                allIds:{
                  $setUnion:[
                    ["$trans.dealIssueTranClientId","$trans.dealIssueTranIssuerId"],
                    "$trans.dealIssueParticipants.dealPartFirmId",
                    "$trans.dealIssueUnderwriters.dealPartFirmId",
                    "$trans.dealIssueParticipants.dealPartContactId",
                    "$trans.dealIssueTranAssignedTo",
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }

          */
          keySearchDetails: {
            $concat: [
              {
                $ifNull: ["$tranStatus", ""]
              }, {
                $ifNull: ["$activityDescription", ""]
              }, {
                $ifNull: ["$tranClientName", ""]
              }, {
                $ifNull: ["$tranFirmName", ""]
              }, {
                $ifNull: ["$tranType", ""]
              }, {
                $ifNull: ["$tranSubType", ""]
              }, {
                $ifNull: ["$tranPrimarySector", ""]
              }, {
                $ifNull: ["$tranSecondarySector", ""]
              }, {
                $ifNull: ["$taskDescription", ""]
              }, {
                $ifNull: ["$taskAssigneeName", ""]
              }
            ]
          }

        }
      }, {
        $match: {
          ...matchQuery,
          selectTran: true
        }
      }, {
        $match: {
          $or: [
            {
              "keySearchDetails": {
                $regex: searchTerm,
                $options: "i"
              }
            }
          ]
        }
      }
    ]

    const originalOtherTasksPipeline = [
      {
        $unwind: "$loggedInFirmUserIds"
      }, {
        $lookup: {
          from: "mvtasks",
          localField: "loggedInFirmUserIds",
          foreignField: "taskDetails.taskAssigneeUserId",
          as: "othertasks"
        }
      }, {
        $unwind: "$othertasks"
      }, {
        $match: {
          $and: [
            {
              "othertasks.taskDetails.taskType": {
                $in: ["general", "Compliance", "Other"]
              }
            }, {
              "othertasks.relatedActivityDetails.activityId": {
                $exists: false
              }
            }
          ]
        }
      }, {
        $project: {
          taskId: "$othertasks._id",
          // tranClientName:"Not Applicable",
          tranClientName: {
            $switch: {
              branches: [
                {
                  case: {
                    $eq: ["$othertasks.taskDetails.taskType", "Compliance"]
                  },
                  then: "Compliance Action Center"
                }, {
                  case: {
                    $eq: ["$othertasks.taskDetails.taskType", "general"]
                  },
                  then: "General Tasks"
                }
              ],
              default: "Not Applicable"
            }
          },
          tranType: "General / Compliance",
          tranSubType: "$othertasks.taskDetails.taskType",
          taskDescription: "$othertasks.taskDetails.taskDescription",
          taskNotes: "$othertasks.taskDetails.taskNotes",
          complianceTask: "$othertasks.taskDetails.complianceTask",
          taskActivityContext: "Non Transaction Tasks",
          relatedTranId: "$othertasks.relatedActivityDetails.activityId",
          taskEndDate: "$othertasks.taskDetails.taskEndDate",
          taskStatus: "$othertasks.taskDetails.taskStatus",
          taskAssigneeId: "$othertasks.taskDetails.taskAssigneeUserId",
          taskAssigneeName: "$othertasks.taskDetails.taskAssigneeName",
          tasksForMyFirmUsers: true,
          allIds: [
            "$othertasks._id", "$othertasks.taskDetails.taskAssigneeUserId"
          ],
          tasksForMe: {
            $and: [
              {
                $eq: ["$othertasks.taskDetails.taskAssigneeUserId", user._id]
              }
            ]
          },
          "taskDueYear": {
            "$year": "$othertasks.taskDetails.taskEndDate"
          },
          "taskDueMonth": {
            "$month": "$othertasks.taskDetails.taskEndDate"
          },
          "taskDueWeek": {
            "$week": "$othertasks.taskDetails.taskEndDate"
          },
          "taskDueDay": {
            "$dayOfMonth": "$othertasks.taskDetails.taskEndDate"
          },
          "refDateYear": {
            $year: refDate
          },
          "refDateMonth": {
            $month: refDate
          },
          "refDateWeek": {
            $week: refDate
          },
          "refDateDay": {
            $dayOfMonth: refDate
          }
        }
      }, {
        $project: {
          "othertasks": 0
        }
      }, {
        $addFields: {
          tasksDueToday: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }, {
                $eq: ["$taskDueDay", "$refDateDay"]
              }
            ]
          },
          tasksDueThisWeek: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }, {
                $eq: ["$taskDueWeek", "$refDateWeek"]
              }
            ]
          },
          tasksDueThisMonth: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }
            ]
          },
          /*
                          tranClientName:{
            $switch: {
              branches: [
                { case: {$eq:["$othertasks.taskDetails.taskType","Compliance"]}, then: "Compliance Action Center" },
                { case: {$eq:["$othertasks.taskDetails.taskType","general"]}, then: "General Tasks" },
              ],
              default: "Not Applicable"
            }
          },
          tranType:"General / Compliance",
          tranSubType:"$othertasks.taskDetails.taskType",
          taskDescription:"$othertasks.taskDetails.taskDescription",
          taskNotes:"$othertasks.taskDetails.taskNotes",
          taskActivityContext:"Non Transaction Tasks",
          taskEndDate:"$othertasks.taskDetails.taskEndDate",
          taskStatus:"$othertasks.taskDetails.taskStatus",
          taskAssigneeId:"$othertasks.taskDetails.taskAssigneeUserId",
          taskAssigneeName:"$othertasks.taskDetails.taskAssigneeName",
          tasksForMyFirmUsers:true,

          */
          keySearchDetails: {
            $concat: [
              {
                $ifNull: ["$tranStatus", ""]
              }, {
                $ifNull: ["$activityDescription", ""]
              }, {
                $ifNull: ["$tranClientName", ""]
              }, {
                $ifNull: ["$tranType", ""]
              }, {
                $ifNull: ["$tranSubType", ""]
              }, {
                $ifNull: ["$taskDescription", ""]
              }, {
                $ifNull: ["$taskNotes", ""]
              }, {
                $ifNull: ["$taskAssigneeName", ""]
              }
            ]
          },

          tasksForMyFirmUsers: true
        }
      }, {
        $match: {
          ...otherMatchQuery
        }
      }, {
        $match: {
          $or: [
            {
              "keySearchDetails": {
                $regex: searchTerm,
                $options: "i"
              }
            }
          ]
        }
      }
    ]

    const commonPipelineStages = [...originalCommonPipelineStages]
    const otherTasksPipeline = [...originalOtherTasksPipeline]
    if ((searchTerm && searchTerm.length === 0) || isEmpty(searchTerm)) {
      commonPipelineStages.splice(-1, 1)
      otherTasksPipeline.splice(-1, 1)
    }

    const allTransactionDetails = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: ObjectID(user.entityId)
        }
      }, {
        $project: {
          tenantId: "$entityParty1",
          loggedInUserEntityId: "$entityParty2",
          relationshipType: "$relationshipType"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "loggedInUserEntityId",
          foreignField: "entityId",
          as: "myFirmUsers"
        }
      }, {
        $project: {
          tenantId: 1,
          loggedInUserEntityId: 1,
          relationshipType: 1,
          loggedInFirmUserIds: "$myFirmUsers._id"
        }
      }, {
        $facet: {
          "deals": [
            {
              $lookup: {
                from: "tranagencydeals",
                localField: "tenantId",
                foreignField: "dealIssueTranClientId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.dealIssueTranClientId",
                clientEntityId: "$trans.dealIssueTranIssuerId",
                tranUsers: {
                  $setUnion: ["$trans.dealIssueParticipants.dealPartContactId", "$trans.dealIssueTranAssignedTo", ["$trans.createdByUser"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.dealIssueTranClientId", "$trans.dealIssueTranIssuerId"
                    ],
                    "$trans.dealIssueParticipants.dealPartFirmId",
                    "$trans.dealIssueUnderwriters.dealPartFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.dealIssueTranIssueName", ""]
                        }, {
                          "$eq": ["$trans.dealIssueTranIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.dealIssueTranProjectDescription",
                    else : "$trans.dealIssueTranIssueName"
                  }
                },
                tranParAmount: {
                  $ifNull: ["$trans.dealIssueParAmount", "-"]
                },
                tranExpectedAwardDate: {
                  $ifNull: ["$trans.dealIssueExpAwardDate", "-"]
                },
                tranActualAwardDate: {
                  $ifNull: ["$trans.dealIssueActAwardDate", "-"]
                },
                tranPricingDate: {
                  $ifNull: ["$trans.dealIssuePricingDate", "-"]
                },
                tranPrimarySector: "$trans.dealIssueTranPrimarySector",
                tranSecondarySector: "$trans.dealIssueTranSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.dealIssueTranStatus", "-"]
                },
                tranClientName: "$trans.dealIssueTranIssuerFirmName",
                tranFirmName: "$trans.dealIssueTranClientFirmName",
                tranType: "$trans.dealIssueTranType",
                tranSubType: "$trans.dealIssueTranSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                assignee: {
                  $ifNull: ["$trans.dealIssueTranAssignedTo", []
                  ]
                },
                allIds: {
                  $setUnion: [
                    [
                      "$trans.dealIssueTranClientId", "$trans.dealIssueTranIssuerId"
                    ],
                    "$trans.dealIssueParticipants.dealPartFirmId",
                    "$trans.dealIssueUnderwriters.dealPartFirmId",
                    "$trans.dealIssueParticipants.dealPartContactId",
                    "$trans.dealIssueTranAssignedTo",
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ],
          "bankloans": [
            {
              $lookup: {
                from: "tranbankloans",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.actTranFirmId",
                clientEntityId: "$trans.actTranClientId",
                tranUsers: {
                  $setUnion: [
                    "$trans.bankLoanParticipants.partContactId",
                    ["$trans.createdByUser", "$trans.actTranFirmLeadAdvisorId"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actTranClientId"
                    ],
                    "$trans.bankLoanParticipants.partFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.actTranIssueName", ""]
                        }, {
                          "$eq": ["$trans.actTranIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.actTranProjectDescription",
                    else : "$trans.actTranIssueName"
                  }
                },
                tranParAmount: {
                  $ifNull: ["$trans.bankLoanTerms.parAmount", "-"]
                },
                tranExpectedAwardDate: {
                  $ifNull: ["$trans.bankLoanSummary.actTranClosingDate", "-"]
                },
                tranActualAwardDate: {
                  $ifNull: ["$trans.bankLoanSummary.actTranClosingDate", "-"]
                },
                tranPricingDate: {
                  $ifNull: ["$trans.bankLoanSummary.actTranClosingDate", "-"]
                },
                tranPrimarySector: "$trans.actTranPrimarySector",
                tranSecondarySector: "$trans.actTranSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.actTranStatus", "-"]
                },
                tranClientName: "$trans.actTranClientName",
                tranFirmName: "$trans.actTranFirmName",
                tranType: "$trans.actTranType",
                tranSubType: "$trans.actTranSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                allIds: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actTranClientId"
                    ],
                    "$trans.bankLoanParticipants.partFirmId",
                    "$trans.bankLoanParticipants.partContactId",
                    ["$trans.actTranFirmLeadAdvisorId"],
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ],
          "derivatives": [
            {
              $lookup: {
                from: "tranderivatives",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.actTranFirmId",
                clientEntityId: "$trans.actTranClientId",
                tranUsers: {
                  $setUnion: [
                    "$trans.derivativeParticipants.partContactId",
                    ["$trans.createdByUser", "$trans.actTranFirmLeadAdvisorId"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actTranClientId"
                    ],
                    "$trans.derivativeParticipants.partFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.actTranIssueName", ""]
                        }, {
                          "$eq": ["$trans.actTranIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.actTranProjectDescription",
                    else : "$trans.actTranIssueName"
                  }
                },
                tranParAmount: {
                  $ifNull: ["$trans.derivativeSummary.tranNotionalAmt", "-"]
                },
                tranExpectedAwardDate: {
                  $ifNull: ["$trans.derivativeSummary.tranEffDate", "-"]
                },
                tranActualAwardDate: {
                  $ifNull: ["$trans.derivativeSummary.tranEndDate", "-"]
                },
                tranPricingDate: {
                  $ifNull: ["$trans.derivativeSummary.tranTradeDate", "-"]
                },
                tranPrimarySector: "$trans.actTranPrimarySector",
                tranSecondarySector: "$trans.actTranSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.actTranStatus", "-"]
                },
                tranClientName: "$trans.actTranFirmName",
                tranFirmName: "$trans.actTranClientName",
                tranType: "$trans.actTranType",
                tranSubType: "$trans.actTranSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                allIds: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actTranClientId"
                    ],
                    "$trans.derivativeParticipants.partFirmId",
                    "$trans.derivativeParticipants.partContactId",
                    ["$trans.actTranFirmLeadAdvisorId"],
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ],
          "marfps": [
            {
              $lookup: {
                from: "actmarfps",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.actTranFirmId",
                clientEntityId: "$trans.actIssuerClient",
                tranUsers: {
                  $setUnion: [
                    "$trans.maRfpParticipants.partContactId",
                    ["$trans.createdByUser", "$trans.actLeadFinAdvClientEntityId"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actIssuerClient"
                    ],
                    "$trans.maRfpParticipants.partFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.actIssueName", ""]
                        }, {
                          "$eq": ["$trans.actIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.actProjectName",
                    else : "$trans.actIssueName"
                  }
                },
                tranParAmount: {
                  $ifNull: ["$trans.maRfpSummary.actParAmount", "-"]
                },
                tranExpectedAwardDate: {
                  $ifNull: ["$trans.maRfpSummary.actExpAwaredDate", "-"]
                },
                tranActualAwardDate: {
                  $ifNull: ["$trans.maRfpSummary.actActAwardDate", "-"]
                },
                tranPricingDate: {
                  $ifNull: ["$trans.maRfpSummary.actPricingDate", "-"]
                },
                tranPrimarySector: "$trans.actPrimarySector",
                tranSecondarySector: "$trans.actSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.actStatus", "-"]
                },
                tranClientName: "$trans.actIssuerClientEntityName",
                tranFirmName: "$trans.actTranFirmName",
                tranType: "$trans.actType",
                tranSubType: "$trans.actSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                allIds: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actIssuerClient"
                    ],
                    "$trans.maRfpParticipants.partFirmId",
                    "$trans.maRfpParticipants.partContactId",
                    ["$trans.actLeadFinAdvClientEntityId"],
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ],
          "others": [
            {
              $lookup: {
                from: "tranagencyothers",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.actTranFirmId",
                clientEntityId: "$trans.actTranClientId",
                tranUsers: {
                  $setUnion: ["$trans.participants.partContactId", ["$trans.actTranFirmLeadAdvisorId"], ["$trans.createdByUser"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actTranClientId"
                    ],
                    "$trans.participants.partFirmId",
                    "$trans.actTranUnderwriters.partFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.actTranIssueName", ""]
                        }, {
                          "$eq": ["$trans.actTranIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.actTranProjectDescription",
                    else : "$trans.actTranIssueName"
                  }
                },
                tranParAmount: {
                  $ifNull: ["$trans.dealIssueParAmount", "-"]
                },
                tranExpectedAwardDate: {
                  $ifNull: ["$trans.dealIssueExpAwardDate", "-"]
                },
                tranActualAwardDate: {
                  $ifNull: ["$trans.dealIssueActAwardDate", "-"]
                },
                tranPricingDate: {
                  $ifNull: ["$trans.dealIssuePricingDate", "-"]
                },
                tranPrimarySector: "$trans.actTranPrimarySector",
                tranSecondarySector: "$trans.actTranSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.actTranStatus", "-"]
                },
                tranClientName: "$trans.actTranIssuerFirmName",
                tranFirmName: "$trans.actTranClientFirmName",
                tranType: "$trans.actTranType",
                tranSubType: "$trans.actTranSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                allIds: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actTranClientId"
                    ],
                    "$trans.participants.partFirmId",
                    "$trans.actTranUnderwriters.partFirmId",
                    "$trans.participants.partContactId",
                    ["$trans.actTranFirmLeadAdvisorId"],
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ],
          "rfps": [
            {
              $lookup: {
                from: "tranagencyrfps",
                localField: "tenantId",
                foreignField: "rfpTranClientId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.rfpTranClientId",
                clientEntityId: "$trans.rfpTranIssuerId",
                tranUsers: {
                  $setUnion: ["$trans.rfpProcessContacts.rfpProcessContactId", "$trans.rfpEvaluationTeam.rfpSelEvalContactId", "$trans.rfpParticipants.rfpParticipantContactId", "$trans.rfpTranAssignedTo", ["$trans.createdByUser"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.rfpTranClientId", "$trans.rfpTranIssuerId"
                    ],
                    "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
                    "$trans.rfpProcessContacts.rfpProcessFirmId",
                    "$trans.rfpParticipants.rfpParticipantFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.rfpTranIssueName", ""]
                        }, {
                          "$eq": ["$trans.rfpTranIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.rfpTranProjectDescription",
                    else : "$trans.rfpTranIssueName"
                  }
                },
                tranParAmount: "-",
                tranExpectedAwardDate: "-",
                tranActualAwardDate: "-",
                tranPricingDate: "-",
                tranPrimarySector: "$trans.rfpTranPrimarySector",
                tranSecondarySector: "$trans.rfpTranSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.rfpTranStatus", "-"]
                },
                tranClientName: "$trans.rfpTranIssuerFirmName",
                tranFirmName: "$trans.rfpTranClientFirmName",
                tranType: "$trans.rfpTranType",
                tranSubType: "$trans.rfpTranSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                allIds: {
                  $setUnion: [
                    [
                      "$trans.rfpTranClientId", "$trans.rfpTranIssuerId"
                    ],
                    "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
                    "$trans.rfpProcessContacts.rfpProcessFirmId",
                    "$trans.rfpParticipants.rfpParticipantFirmId",
                    "$trans.rfpTranAssignedTo",
                    ["$trans.createdByUser"],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ],
          "busdevs": [
            {
              $lookup: {
                from: "actbusdevs",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            }, {
              $unwind: "$trans"
            }, {
              $project: {
                tranId: "$trans._id",
                firmEntityId: "$trans.actTranFirmId",
                clientEntityId: "$trans.actIssuerClient",
                tranUsers: {
                  $setUnion: [
                    "$trans.participants.rfpParticipantContactId",
                    ["$trans.createdByUser", "$trans.actLeadFinAdvClientEntityId"]
                  ]
                },
                tranEntities: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actIssuerClient"
                    ],
                    "$trans.participants.partFirmId"
                  ]
                },
                activityDescription: {
                  $cond: {
                    if: {
                      $or: [
                        {
                          "$eq": ["$trans.actIssueName", ""]
                        }, {
                          "$eq": ["$trans.actIssueName", null]
                        }
                      ]
                    },
                    then: "$trans.actProjectName",
                    else : "$trans.actIssueName"
                  }
                },
                tranParAmount: "-",
                tranExpectedAwardDate: "-",
                tranActualAwardDate: "-",
                tranPricingDate: "-",
                tranPrimarySector: "$trans.actPrimarySector",
                tranSecondarySector: "$trans.actSecondarySector",
                tranStatus: {
                  $ifNull: ["$trans.actStatus", "-"]
                },
                tranClientName: "$trans.actIssuerClientEntityName",
                tranFirmName: "$trans.actProjectName",
                tranType: "$trans.actType",
                tranSubType: "$trans.actSubType",
                userEntitlement: user.userEntitlement,
                relationshipType: "$relationshipType",
                myFirmUserIds: "$loggedInFirmUserIds",
                allIds: {
                  $setUnion: [
                    [
                      "$trans.actTranFirmId", "$trans.actIssuerClient"
                    ],
                    "$trans.participants.partFirmId",
                    ["$trans.createdByUser", ["$trans.actLeadFinAdvClientEntityId"]
                    ],
                    ["$trans._id"]
                  ]
                }
              }
            },
            ...commonPipelineStages
          ]

        }
      }
    ])

    const allOtherTasks = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: ObjectID(user.entityId)
        }
      }, {
        $project: {
          tenantId: "$entityParty1",
          loggedInUserEntityId: "$entityParty2",
          relationshipType: "$relationshipType"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "loggedInUserEntityId",
          foreignField: "entityId",
          as: "myFirmUsers"
        }
      }, {
        $project: {
          tenantId: 1,
          loggedInUserEntityId: 1,
          relationshipType: 1,
          loggedInFirmUserIds: "$myFirmUsers._id"
        }
      }, {
        $facet: {
          catchalltasks: [...otherTasksPipeline]
        }
      }
    ])

    let retData = isEmpty(allTransactionDetails[0])
      ? []
      : allTransactionDetails[0]
    const otherData = isEmpty(allOtherTasks[0])
      ? []
      : allOtherTasks[0]

    retData = {
      ...retData,
      ...otherData
    }

    console.log("retData : ", JSON.stringify(retData, null, 2))

    const flattenedIds = Object
      .keys(retData)
      .reduce((acc, k) => {
        const arrData = retData[k].reduce((allTranIds, tr) => {
          const allIdsToString = (tr.allIds || []).map(aa => {
            if (aa) {
              return aa.toString()
            }
            return ObjectID()
          })
          return [...new Set([
            ...allTranIds,
            ...allIdsToString
          ])]
        }, [])
        return [...new Set([
          ...acc,
          ...arrData
        ])]
      }, [])

    const newFlattened = [...new Set(flattenedIds)]
    return {allData: retData, allflattenedIds: newFlattened}

  } catch (e) {
    console.log("There is an Error", e)
    return {allData: {}, allflattenedIds: [], error: e}

  }
}

const buildDynamicPipeline = ( {payload,groupByFields}) => {

  const {groupBy} = payload
  const {serverPerformPagination,currentPage, size, sortFields} = payload.pagination
  const defaultSize = 25

  const groupByParameters = [ ...groupByFields ]
  const groupByAllMetaData = groupByParameters.reduce (( acc, g) => ({ ...acc, [g]:[
    {$group: {_id:`$${g}`,  count:{$sum:1}}},
    { $sort: { _id: 1 } },
    {
      "$project": {
        _id:1,
        count:1,
        pages: { $ceil: { $divide: ["$count", size] } }
      }
    }

  ]}), {})
  // console.log("All possible groups", JSON.stringify(groupByAllMetaData,null, 2))

  // if there is a group by parameter then we group by that value and get the data

  let groupByDataWithoutSpecificGroupsSelected = {}
  let groupByPipelineStages = {}

  if( !isEmpty(groupBy)) {

    groupByDataWithoutSpecificGroupsSelected =  {[groupBy]:[
      {$sort: { [groupBy]:1 } },
      {$group: {
        _id:`$${groupBy}`,
        count:{$sum:1},
        data: { $push: "$$ROOT" }
      }
      },
      { $sort: { _id: 1 } },
      {
        "$project": {
          _id:1,
          count:1,
          pages: { $ceil: { $divide: ["$count", (size || defaultSize)] } },
          data: { "$slice": ["$data", 0, (size || defaultSize)] }
        }
      }
    ]}
    console.log("No specific groups have been selected.", JSON.stringify(groupByDataWithoutSpecificGroupsSelected,null, 2))

    // Cases where we have grouped and also specified only certain groups for pagination
    groupByPipelineStages = ((payload && payload.groups) || []).reduce ( (acc, {value, size, currentPage, sortFields}) => {
      const stageDetails = {
        [value]:[
          {
            $match:{[groupBy]:value}
          },
          { $sort: { ...sortFields } },
          { $skip: (size || defaultSize) * currentPage },
          { $limit: (size || defaultSize) }
        ]
      }
      return {...acc, ...stageDetails }
    },{})
    console.log("The dynamic group by is", JSON.stringify(groupByPipelineStages,null,2))
  }

  const pipelineStageToExtractAllIds = {
    "allids":[
      {$unwind:"$allIds"},
      {$group:{_id:"$allIds"}}
    ]
  }

  let paginationPipelineStages = []

  if (serverPerformPagination) {

    if (isEmpty(groupBy) && isEmpty(payload.groups)) {
      console.log("Entered 1")
      paginationPipelineStages = [
        {
          $facet: {
            metadata: [{ $count: "count" }, { $addFields: { pages: { $ceil: { $divide: ["$count", (size || defaultSize)] } } } }],
            data: [{ $sort: { ...sortFields } }, { $skip: (size || defaultSize) * currentPage }, { $limit: (size || defaultSize) }],
            ...groupByAllMetaData,// add projection here wish you re-shape the docs
            ...pipelineStageToExtractAllIds
          }
        }
      ]

    } else if (!isEmpty(groupBy) && !isEmpty(payload.groups)) {
      console.log("Entered 2")

      paginationPipelineStages = [
        {
          $facet: {
            ...groupByAllMetaData,// add projection here wish you re-shape the docs
            ...groupByPipelineStages,
            ...pipelineStageToExtractAllIds
          }
        }
      ]
    }
    else if (!isEmpty(groupBy) && isEmpty(payload.groups)) {
      console.log("Entered 3")

      paginationPipelineStages = [
        {
          $facet: {
            ...groupByDataWithoutSpecificGroupsSelected,
            ...pipelineStageToExtractAllIds
          }
        }
      ]
    } else if (isEmpty(groupBy) && !isEmpty(payload.groups)) {
      console.log("Entered 4")
      paginationPipelineStages = [
        {
          $facet: {
            metadata: [{ $count: "count" }, { $addFields: { pages: { $ceil: { $divide: ["$count", size] } } } }],
            data: [{ $sort: { ...sortFields } }, { $skip: (size || defaultSize) * currentPage }, { $limit: (size || defaultSize) }],
            ...groupByAllMetaData,// add projection here wish you re-shape the docs
            ...pipelineStageToExtractAllIds
          }
        }
      ]

    }
  } else {
    paginationPipelineStages = [
      {
        $facet: {
          data:[{$skip:0}],
          ...pipelineStageToExtractAllIds
        }
      }
    ]
  }

  return paginationPipelineStages

}

const generateDashboardResultSet = async ({pipelineResults,user, payload}) => {

  const resultsData = isEmpty(pipelineResults[0]) ? {} : pipelineResults[0]
  const allUniqueIds = resultsData.allids.reduce ( (acc,  idobj) => [...acc,idobj._id ],[])

  if(payload.pagination.serverPerformPagination) {
    if ( !isEmpty(payload.groupBy) && !isEmpty((payload && payload.groups))) {
      const groupByMetaData = resultsData[payload.groupBy]
      const selectedGroups = payload.groups.reduce ( (acc, a) => ({ ...acc, [a.value]:a}),{})
      // const groupdMetaDataFormattedResults = groupByMetaData.reduce ( (acc, {_id:id, count}) => ({ ...acc,  [id]: { count, pages: selectedGroups[id] ? Math.ceil(count / selectedGroups[id].size) : 0, data: resultsData[id] || [] } }),{} )
      const groupdMetaDataFormattedResults = groupByMetaData.reduce ( (acc, {_id:id, count}) => ([ ...acc,  {_id:id, count, pages: selectedGroups[id] ? Math.ceil(count / selectedGroups[id].size) : 0, data: resultsData[id] || [] } ]),[] )
      return { taskData:{[payload.groupBy]:groupdMetaDataFormattedResults}, allflattenedIds:[allUniqueIds] }
    }
    else if (!isEmpty(payload.groupBy) && isEmpty(payload.groups)) {
      return { taskData:{...resultsData}, allflattenedIds:[allUniqueIds] }
    }

    const {metadata, ...rest} = resultsData
    return { taskData:{...metadata[0],...rest}, allflattenedIds:[allUniqueIds] }
  }
  return { taskData:{...resultsData}, allflattenedIds:[allUniqueIds] }
}

export const getDataForTaskDashboardTransactionBased = async(user, payload ) => {

  const {activityCategory,activityTime,taskStatus,taskReadUnread,calendarStart,calendarEnd} = payload
  let {freeTextSearchTerm} = payload

  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = freeTextSearchTerm.replace(invalid, "")
    freeTextSearchTerm = new RegExp(newSearchString,"i")
    const  matchQuery = []

    if(activityCategory === "myfirm" ) {
      matchQuery.push({tasksMyFirmTranActivities:true})
    }
    else if(activityCategory === "dealteam" ) {
      matchQuery.push({tasksMyDealTeamTranActivities:true})
    }
    else if(activityCategory === "self" ) {
      matchQuery.push({tasksMyTranActivities:true})
    }
    else {
      matchQuery.push({tasksMyDealTeamTranActivities:true})
    }

    if(activityTime === "day" ) {
      matchQuery.push(getDateString("taskEndDate","TODAY"))
    }
    else if(activityTime === "week" ) {
      matchQuery.push(getDateString("taskEndDate","CURRENTWEEK"))
    }
    else if(activityTime === "month" ) {
      matchQuery.push(getDateString("taskEndDate","CURRENTMONTH"))
    }

    if(calendarStart && calendarEnd) {
      matchQuery.push({"taskEndDate":{
        $gte: new Date(moment(calendarStart).utc().toDate()),
        $lte:new Date(moment(calendarEnd).utc().toDate())
      }})
    }

    if(taskStatus === "openactive" ) {
      matchQuery.push({taskStatus:{$in:["Open","Active"]}})
    }
    else if(taskStatus === "closedcancelled" ) {
      matchQuery.push({taskStatus:{$in:["Closed","Cancelled","Complete","Completed", "Dropped"]}})
    }

    if(taskReadUnread === "Unread" ) {
      matchQuery.push({taskUnreadStatus:true})
    }
    else if(taskReadUnread === "Read") {
      matchQuery.push({taskUnreadStatus:false})
    }

    console.log("MATCH QUERY", matchQuery)

    const today = new Date()
    const localoffset = -(today.getTimezoneOffset()/60)
    const refDate = new Date( new Date().getTime() + localoffset * 3600 * 1000)

    const originalCommonPipelineStages = [
      {
        $addFields:{
          currentUserInTran:{
            $in:[ObjectID(user._id), {$ifNull:["$tranUsers",[]]}]
          },
          currentUserFirmInTran:{
            $in:[ObjectID(user.entityId),{$ifNull:["$tranEntities",[]]}]
          }
        }
      },
      {
        $addFields:{
          editTran:{
            $or:[
              {$and: [
                {$eq:["$userEntitlement","global-edit"]},
                {$or:[
                  {$eq:["$currentUserInTran",true]},
                  {$eq:["$currentUserFirmInTran",true]}
                ]},
              ]},
              {
                $and:[
                  {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                  {$eq:["$currentUserInTran",true]},
                ]
              }
            ]
          },
          viewTran:{
            $or:[
              {$and: [
                {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                {$or:[
                  {$eq:["$currentUserInTran",true]},
                  {$eq:["$currentUserFirmInTran",true]}
                ]},
              ]},
              {
                $and:[
                  {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                  {$eq:["$currentUserInTran",true]},
                ]
              }
            ]
          }
        }
      },
      {
        $addFields:{
          selectTran:{$or:[
            {$eq:["$editTran",true]},
            {$eq:["$viewTran",true]}
          ]},
          myUserDetails: { $setIntersection:["$tranUsers",[ObjectID(user._id)] ]},
          myFirmUserDetails: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                    ]},
                  "then": {$setUnion: ["$myFirmUserIds", [ObjectID(user.entityId)]]}
                  // "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
                },
                {
                  "case": {$and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                  ]},
                  "then": [ObjectID(user._id),ObjectID(user.entityId)]
                },
                {
                  "case": {
                    $and:[
                      {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                      {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                    ]},
                  "then": {$setUnion: ["$myFirmUserIds", [ObjectID(user.entityId)]]}
                },
                {
                  "case": {$and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                  ]},
                  // "then": { $setUnion:["$tranUsers",[ObjectID(user._id),ObjectID(user.entityId)] ]}
                  "then": { $setIntersection:[{$setUnion:["$tranUsers","$tranEntities"]},[ObjectID(user._id),ObjectID(user.entityId)] ]}
                },

              ],
              "default": [ObjectID(user._id)]
            }
          },
          myDealTeamUserDetails: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                    ]},
                  "then": {$setUnion:["$tranUsers","$myFirmUserIds", "$tranEntities", [ObjectID(user.entityId)]]}
                },
                {
                  "case": {$and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["tran-edit"]]},
                  ]},
                  "then": { $setIntersection:[{$setUnion:["$tranUsers","$tranEntities"]},[ObjectID(user._id),ObjectID(user.entityId)] ]}
                },
                {
                  "case": {$and:[
                    {"$eq":["$relationshipType","Self"]},
                    {$in:["$userEntitlement",["tran-readonly"]]},
                  ]},
                  "then": { $setIntersection:[{$setUnion:["$tranUsers","$tranEntities"]},[ObjectID(user._id),ObjectID(user.entityId)] ]}
                },
                {
                  "case": {
                    $and:[
                      {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                      {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                    ]},
                  "then": {$setUnion: [{ $setIntersection:["$tranUsers","$myFirmUserIds" ]}, "$tranEntities", [ObjectID(user.entityId)]]}
                },
                {
                  "case": {$and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                  ]},
                  "then": { $setIntersection:[{$setUnion:["$tranUsers","$tranEntities"]},[ObjectID(user._id),ObjectID(user.entityId)] ]}
                },
                {
                  "case": {$and:[
                    {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                    {$in:["$userEntitlement",["tran-readonly"]]},
                  ]},
                  "then": { $setIntersection:[{$setUnion:["$tranUsers","$tranEntities"]},[ObjectID(user._id),ObjectID(user.entityId)] ]}
                },
              ],
              "default": { $setIntersection:["$tranUsers",[ObjectID(user._id)] ]}
            }
          },
        }
      },
      {
        $lookup:{
          from: "mvtasks",
          localField: "tranId",
          foreignField: "relatedActivityDetails.activityId",
          as: "relatedTasks"
        }
      },
      {$unwind:{"path":"$relatedTasks", "preserveNullAndEmptyArrays": false}},
      {
        $addFields: {
          taskId:"$relatedTasks._id",
          taskDescription:"$relatedTasks.taskDetails.taskDescription",
          taskActivityContext:"$relatedTasks.relatedActivityDetails.activityContext",
          taskAssigneeType:{"$ifNull":["$relatedTasks.taskDetails.taskAssigneeType","user"]},
          taskEndDate:"$relatedTasks.taskDetails.taskEndDate",
          taskStatus:"$relatedTasks.taskDetails.taskStatus",
          taskAssigneeId:"$relatedTasks.taskDetails.taskAssigneeUserId",
          taskAssigneeName:"$relatedTasks.taskDetails.taskAssigneeName",
          taskUnreadStatus : {$ifNull:["$relatedTasks.taskDetails.taskUnread",true]},
          tasksMyFirmTranActivities:{
            $and:[
              {$eq:["$relatedTasks.relatedActivityDetails.activityId","$tranId"]},
              {$in:["$relatedTasks.taskDetails.taskAssigneeUserId","$myFirmUserDetails"]},
            ]
          },
          tasksMyTranActivities:{
            $or:[
              {
                $and:[
                  {$eq:["$relatedTasks.relatedActivityDetails.activityId","$tranId"]},
                  {$eq:["$relatedTasks.taskDetails.taskAssigneeUserId",ObjectID(user._id)]},
                ]
              },
              {
                $and:[
                  {$eq:["$relatedTasks.relatedActivityDetails.activityId","$tranId"]},
                  {$eq:["$relatedTasks.taskDetails.taskAssigneeUserId",ObjectID(user.entityId)]},
                  {$eq:["$currentUserInTran",true]},
                ]
              }
            ]
          },
          tasksMyDealTeamTranActivities:{
            $and:[
              {$eq:["$relatedTasks.relatedActivityDetails.activityId","$tranId"]},
              {$in:["$relatedTasks.taskDetails.taskAssigneeUserId","$myDealTeamUserDetails"]},
            ]
          },
          "taskDueYear": {
            "$year": "$relatedTasks.taskDetails.taskEndDate"
          },
          "taskDueMonth": {
            "$month": "$relatedTasks.taskDetails.taskEndDate"
          },
          "taskDueWeek": {
            "$week": "$relatedTasks.taskDetails.taskEndDate"
          },
          "taskDueDay": {
            "$dayOfMonth": "$relatedTasks.taskDetails.taskEndDate"
          },
          "refDateYear": {
            $year:refDate
          },
          "refDateMonth": {
            $month:refDate
          },
          "refDateWeek": {
            $week:refDate
          },
          "refDateDay": {
            $dayOfMonth:refDate
          }
        },
      },
      {
        $project:{"relatedTasks":0}
      },
      {
        $addFields: {
          tasksDueToday:{
            $and:[
              {$eq:["$taskDueYear","$refDateYear"]},
              {$eq:["$taskDueMonth","$refDateMonth"]},
              {$eq:["$taskDueDay","$refDateDay"]},
            ]
          },
          tasksDueThisWeek:{
            $and:[
              {$eq:["$taskDueYear","$refDateYear"]},
              {$eq:["$taskDueMonth","$refDateMonth"]},
              {$eq:["$taskDueWeek","$refDateWeek"]},
            ]
          },
          tasksDueThisMonth:{
            $and:[
              {$eq:["$taskDueYear","$refDateYear"]},
              {$eq:["$taskDueMonth","$refDateMonth"]},
            ]
          },
          keySearchDetails: { $concat: [
            {$ifNull:["$tranStatus",""]},
            {$ifNull:["$activityDescription",""]},
            {$ifNull:["$tranClientName",""]},
            {$ifNull:["$tranFirmName",""]},
            {$ifNull:["$tranType",""]},
            {$ifNull:["$tranSubType",""]},
            {$ifNull:["$tranPrimarySector",""]},
            {$ifNull:["$tranSecondarySector",""]},
            {$ifNull:["$taskDescription",""]},
            {$ifNull:["$taskAssigneeName",""]},
          ]
          }

        }
      },
      {
        $match:{
          $and:[
            ...matchQuery,
            {selectTran:true}
          ]
        }
      },
      {
        $match : {
          $or:[
            { "keySearchDetails":{$regex:freeTextSearchTerm,$options:"i"}}
          ]
        }
      },
    ]

    const commonPipelineStages = [...originalCommonPipelineStages]
    if((freeTextSearchTerm && freeTextSearchTerm.length === 0) || isEmpty(freeTextSearchTerm)) {
      commonPipelineStages.splice(-1,1)
    }

    // Get the meta data for all the possible groups so that this can be shown on the UI

    const groupByFields = [ "activityDescription", "tranStatus", "tranClientName", "tranType","tranSubType","taskUnreadStatus"]
    const consolidatedPipelineStages = [
      {
        $match:{
          entityParty2:ObjectID(user.entityId)
        }
      },
      {
        $project:{
          tenantId:"$entityParty1",
          loggedInUserEntityId:"$entityParty2",
          relationshipType:"$relationshipType"
        }
      },
      {
        $lookup:{
          from: "entityusers",
          localField: "loggedInUserEntityId",
          foreignField: "entityId",
          as: "myFirmUsers"
        }
      },
      {
        $project:{
          tenantId:1,
          loggedInUserEntityId:1,
          relationshipType:1,
          loggedInFirmUserIds:"$myFirmUsers._id",
        }
      },
      {
        $facet:{
          "deals":[
            {
              $lookup:{
                from: "tranagencydeals",
                localField: "tenantId",
                foreignField: "dealIssueTranClientId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                firmEntityId:"$trans.dealIssueTranClientId",
                calendarType:"deals",
                clientEntityId: "$trans.dealIssueTranIssuerId",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] } } ] },
                          then: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] },
                          else: { $ifNull: [["$trans.dealIssueParticipants.dealPartContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: {$ifNull:["$trans.dealIssueTranAssignedTo",[]]} } ] },
                        then: { $ifNull: ["$trans.dealIssueTranAssignedTo", []] },
                        else: { $ifNull: [["$trans.dealIssueTranAssignedTo"], []] }
                      }
                      }
                      , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                          then: { $ifNull: ["$trans.createdByUser", []] },
                          else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranClientId" , []] } } ] },
                          then: { $ifNull: ["$trans.dealIssueTranClientId" , []] },
                          else: { $ifNull: [["$trans.dealIssueTranClientId" ], { $ifNull: ["$trans.dealIssueTranClientId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] } } ] },
                          then: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] },
                          else: { $ifNull: [["$trans.dealIssueTranIssuerId"], { $ifNull: ["$trans.dealIssueTranIssuerId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] },
                          else: { $ifNull: [["$trans.dealIssueParticipants.dealPartFirmId"], []] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] },
                          else: { $ifNull: [["$trans.dealIssueUnderwriters.dealPartFirmId"], []] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                    then:"$trans.dealIssueTranProjectDescription",
                    else:"$trans.dealIssueTranIssueName"},
                },
                tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
                tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
                tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
                tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
                tranPrimarySector:"$trans.dealIssueTranPrimarySector",
                tranSecondarySector:"$trans.dealIssueTranSecondarySector",
                tranStatus:{$ifNull:["$trans.dealIssueTranStatus","-"]},
                tranClientName:"$trans.dealIssueTranIssuerFirmName",
                tranFirmName:"$trans.dealIssueTranClientFirmName",
                tranType:"$trans.dealIssueTranType",
                tranSubType:"$trans.dealIssueTranSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                assignee:{$ifNull:["$trans.dealIssueTranAssignedTo",[]]},
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },
            ...commonPipelineStages
          ],
          "bankloans":[
            {
              $lookup:{
                from: "tranbankloans",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                calendarType:"bankloans",
                firmEntityId:"$trans.actTranFirmId",
                clientEntityId: "$trans.actTranClientId",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] } } ] },
                          then: { $ifNull: ["$trans.bankLoanParticipants.partContactId", []] },
                          else: { $ifNull: [["$trans.bankLoanParticipants.partContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] },
                        then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                        else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                      }
                      }
                      , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                          then: { $ifNull: ["$trans.createdByUser", []] },
                          else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranFirmId" , []] },
                          else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranClientId" , []] },
                          else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.bankLoanParticipants.partFirmId", []] },
                          else: { $ifNull: [["$trans.bankLoanParticipants.partFirmId"], []] }
                        }
                        }
                        , []]
                    }
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                    then:"$trans.actTranProjectDescription",
                    else:"$trans.actTranIssueName"},
                },
                tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
                tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
                tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
                tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
                tranPrimarySector:"$trans.actTranPrimarySector",
                tranSecondarySector:"$trans.actTranSecondarySector",
                tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
                tranClientName:"$trans.actTranClientName",
                tranFirmName:"$trans.actTranFirmName",
                tranType:"$trans.actTranType",
                tranSubType:"$trans.actTranSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                // allIds:{
                //   $setUnion:[
                //     ["$trans.actTranFirmId","$trans.actTranClientId"],
                //     "$trans.bankLoanParticipants.partFirmId",
                //     "$trans.bankLoanParticipants.partContactId",
                //     ["$trans.actTranFirmLeadAdvisorId"],
                //     ["$trans.createdByUser"],
                //     ["$trans._id"]
                //   ]
                // }
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },
            ...commonPipelineStages
          ],
          "derivatives":[
            {
              $lookup:{
                from: "tranderivatives",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                calendarType:"derivatives",
                firmEntityId:"$trans.actTranFirmId",
                clientEntityId: "$trans.actTranClientId",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] } } ] },
                          then: { $ifNull: ["$trans.derivativeParticipants.partContactId", []] },
                          else: { $ifNull: [["$trans.derivativeParticipants.partContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] },
                        then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                        else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                      }
                      }
                      , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                          then: { $ifNull: ["$trans.createdByUser", []] },
                          else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranFirmId" , []] },
                          else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranClientId" , []] },
                          else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.derivativeParticipants.partFirmId", []] },
                          else: { $ifNull: [["$trans.derivativeParticipants.partFirmId"], []] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                    then:"$trans.actTranProjectDescription",
                    else:"$trans.actTranIssueName"},
                },
                tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
                tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate","-"]},
                tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate","-"]},
                tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate","-"]},
                tranPrimarySector:"$trans.actTranPrimarySector",
                tranSecondarySector:"$trans.actTranSecondarySector",
                tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
                tranClientName:"$trans.actTranFirmName",
                tranFirmName:"$trans.actTranClientName",
                tranType:"$trans.actTranType",
                tranSubType:"$trans.actTranSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                // allIds:{
                //   $setUnion:[
                //     ["$trans.actTranFirmId","$trans.actTranClientId"],
                //     "$trans.derivativeParticipants.partFirmId",
                //     "$trans.derivativeParticipants.partContactId",
                //     ["$trans.actTranFirmLeadAdvisorId"],
                //     ["$trans.createdByUser"],
                //     ["$trans._id"]
                //   ]
                // }
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },
            ...commonPipelineStages
          ],
          "marfps":[
            {
              $lookup:{
                from: "actmarfps",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                calendarType:"marfps",
                firmEntityId:"$trans.actTranFirmId",
                clientEntityId: "$trans.actIssuerClient",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] } } ] },
                          then: { $ifNull: ["$trans.maRfpParticipants.partContactId", []] },
                          else: { $ifNull: [["$trans.maRfpParticipants.partContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] },
                        then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                        else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                      }
                      }
                      , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                          then: { $ifNull: ["$trans.createdByUser", []] },
                          else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranFirmId" , []] },
                          else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                          then: { $ifNull: ["$trans.actIssuerClient" , []] },
                          else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.maRfpParticipants.partFirmId", []] },
                          else: { $ifNull: [["$trans.maRfpParticipants.partFirmId"], []] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                    then:"$trans.actProjectName",
                    else:"$trans.actIssueName"},
                },
                tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
                tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate","-"]},
                tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate","-"]},
                tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate","-"]},
                tranPrimarySector:"$trans.actPrimarySector",
                tranSecondarySector:"$trans.actSecondarySector",
                tranStatus:{$ifNull:["$trans.actStatus","-"]},
                tranClientName:"$trans.actIssuerClientEntityName",
                tranFirmName:"$trans.actTranFirmName",
                tranType:"$trans.actType",
                tranSubType:"$trans.actSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                // allIds:{
                //   $setUnion:[
                //     ["$trans.actTranFirmId","$trans.actIssuerClient"],
                //     "$trans.maRfpParticipants.partFirmId",
                //     "$trans.maRfpParticipants.partContactId",
                //     ["$trans.actLeadFinAdvClientEntityId"],
                //     ["$trans.createdByUser"],
                //     ["$trans._id"]
                //   ]
                // }
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },

            ...commonPipelineStages
          ],
          "others":[
            {
              $lookup:{
                from: "tranagencyothers",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                calendarType:"others",
                firmEntityId:"$trans.actTranFirmId",
                clientEntityId: "$trans.actTranClientId",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partContactId", []] } } ] },
                          then: { $ifNull: ["$trans.participants.partContactId", []] },
                          else: { $ifNull: [["$trans.participants.partContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: {$ifNull:["$trans.actTranFirmLeadAdvisorId",[]]} } ] },
                        then: { $ifNull: ["$trans.actTranFirmLeadAdvisorId", []] },
                        else: { $ifNull: [["$trans.actTranFirmLeadAdvisorId"], []] }
                      }
                      }
                      , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                          then: { $ifNull: ["$trans.createdByUser", []] },
                          else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranFirmId" , []] },
                          else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranClientId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranClientId" , []] },
                          else: { $ifNull: [["$trans.actTranClientId"], { $ifNull: ["$trans.actTranClientId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.participants.partFirmId", []] },
                          else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.actTranUnderwriters.partFirmId", []] },
                          else: { $ifNull: [["$trans.actTranUnderwriters.partFirmId"], []] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                    then:"$trans.actTranProjectDescription",
                    else:"$trans.actTranIssueName"},
                },
                tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
                tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
                tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
                tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
                tranPrimarySector:"$trans.actTranPrimarySector",
                tranSecondarySector:"$trans.actTranSecondarySector",
                tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
                tranClientName:"$trans.actTranIssuerFirmName",
                tranFirmName:"$trans.actTranClientFirmName",
                tranType:"$trans.actTranType",
                tranSubType:"$trans.actTranSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                // allIds:{
                //   $setUnion:[
                //     ["$trans.actTranFirmId","$trans.actTranClientId"],
                //     "$trans.participants.partFirmId",
                //     "$trans.actTranUnderwriters.partFirmId",
                //     "$trans.participants.partContactId",
                //     ["$trans.actTranFirmLeadAdvisorId"],
                //     ["$trans.createdByUser"],
                //     ["$trans._id"]
                //   ]
                // }
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },
            ...commonPipelineStages
          ],
          "rfps":[
            {
              $lookup:{
                from: "tranagencyrfps",
                localField: "tenantId",
                foreignField: "rfpTranClientId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                calendarType:"rfps",
                firmEntityId:"$trans.rfpTranClientId",
                clientEntityId: "$trans.rfpTranIssuerId",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: "$trans.rfpProcessContacts.rfpProcessContactId" } ] },
                          then: { $ifNull: ["$trans.rfpProcessContacts.rfpProcessContactId", []] },
                          else: { $ifNull: [["$trans.rfpProcessContacts.rfpProcessContactId"], []] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: "$trans.rfpEvaluationTeam.rfpSelEvalContactId" } ] },
                          then: { $ifNull: ["$trans.rfpEvaluationTeam.rfpSelEvalContactId", []] },
                          else: { $ifNull: [["$trans.rfpEvaluationTeam.rfpSelEvalContactId"], []] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: "$trans.rfpParticipants.rfpParticipantContactId" } ] },
                          then: { $ifNull: ["$trans.rfpParticipants.rfpParticipantContactId", []] },
                          else: { $ifNull: [["$trans.rfpParticipants.rfpParticipantContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: "$trans.rfpTranAssignedTo" } ] },
                        then: { $ifNull: ["$trans.rfpTranAssignedTo", []] },
                        else: { $ifNull: [["$trans.rfpTranAssignedTo"], []] }
                      }
                      }
                      , []] },
                    { $ifNull: [["$trans.createdByUser"], []] }
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
                    "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
                    "$trans.rfpProcessContacts.rfpProcessFirmId",
                    "$trans.rfpParticipants.rfpParticipantFirmId",
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
                    then:"$trans.rfpTranProjectDescription",
                    else:"$trans.rfpTranIssueName"},
                },
                tranParAmount:"-",
                tranExpectedAwardDate:"-",
                tranActualAwardDate:"-",
                tranPricingDate:"-",
                tranPrimarySector:"$trans.rfpTranPrimarySector",
                tranSecondarySector:"$trans.rfpTranSecondarySector",
                tranStatus:{$ifNull:["$trans.rfpTranStatus","-"]},
                tranClientName:"$trans.rfpTranIssuerFirmName",
                tranFirmName:"$trans.rfpTranClientFirmName",
                tranType:"$trans.rfpTranType",
                tranSubType:"$trans.rfpTranSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },
            ...commonPipelineStages
          ],
          "busdevs":[
            {
              $lookup:{
                from: "actbusdevs",
                localField: "tenantId",
                foreignField: "actTranFirmId",
                as: "trans"
              }
            },
            {
              $unwind:"$trans"
            },
            {
              $project:{
                tranId:"$trans._id",
                calendarType:"busdevs",
                firmEntityId:"$trans.actTranFirmId",
                clientEntityId: "$trans.actIssuerClient",
                tranUsers:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] } } ] },
                          then: { $ifNull: ["$trans.participants.rfpParticipantContactId", []] },
                          else: { $ifNull: [["$trans.participants.rfpParticipantContactId"], []] }
                        }
                        }
                        , []]
                    },
                    { $ifNull: [
                      { $cond:{
                        if: { $and: [ { $isArray: {$ifNull:["$trans.actLeadFinAdvClientEntityId",[]]} } ] },
                        then: { $ifNull: ["$trans.actLeadFinAdvClientEntityId", []] },
                        else: { $ifNull: [["$trans.actLeadFinAdvClientEntityId"], []] }
                      }
                      }
                      , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                          then: { $ifNull: ["$trans.createdByUser", []] },
                          else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                        }
                        }
                        , []]
                    },
                  ]
                },
                tranEntities:{
                  $setUnion:[
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actTranFirmId" , []] } } ] },
                          then: { $ifNull: ["$trans.actTranFirmId" , []] },
                          else: { $ifNull: [["$trans.actTranFirmId" ], { $ifNull: ["$trans.actTranFirmId" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.actIssuerClient" , []] } } ] },
                          then: { $ifNull: ["$trans.actIssuerClient" , []] },
                          else: { $ifNull: [["$trans.actIssuerClient"], { $ifNull: ["$trans.actIssuerClient" , []] }] }
                        }
                        }
                        , []]
                    },
                    {
                      $ifNull: [
                        { $cond:{
                          if: { $and: [ { $isArray: { $ifNull: ["$trans.participants.partFirmId", []] } } ] },
                          then: { $ifNull: ["$trans.participants.partFirmId", []] },
                          else: { $ifNull: [["$trans.participants.partFirmId"], []] }
                        }
                        }
                        , []]
                    }
                  ]
                },
                activityDescription : {
                  $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                    then:"$trans.actProjectName",
                    else:"$trans.actIssueName"},
                },
                tranParAmount:"-",
                tranExpectedAwardDate:"-",
                tranActualAwardDate:"-",
                tranPricingDate:"-",
                tranPrimarySector:"$trans.actPrimarySector",
                tranSecondarySector:"$trans.actSecondarySector",
                tranStatus:{$ifNull:["$trans.actStatus","-"]},
                tranClientName:"$trans.actIssuerClientEntityName",
                tranFirmName:"$trans.actProjectName",
                tranType:"$trans.actType",
                tranSubType:"$trans.actSubType",
                userEntitlement:user.userEntitlement,
                relationshipType:"$relationshipType",
                myFirmUserIds:"$loggedInFirmUserIds",
                // allIds:{
                //   $setUnion:[
                //     ["$trans.actTranFirmId","$trans.actIssuerClient"],
                //     "$trans.participants.partFirmId",
                //     ["$trans.createdByUser","$trans.actLeadFinAdvClientEntityId" ],
                //     ["$trans._id"]
                //   ]
                // }
              }
            },
            {
              $addFields:{
                allIds:{
                  $setUnion:[
                    "$tranUsers",
                    "$tranEntities",
                    ["$tranId"]
                  ]
                },
              }
            },
            ...commonPipelineStages
          ]
        }
      },
      {
        "$project": {
          "data": { "$concatArrays": ["$deals","$bankloans","$derivatives","$rfps","$busdevs","$others","$marfps"] },
        }
      },
      { "$unwind": "$data" },
      { "$replaceRoot": { "newRoot": "$data" } },
    ]

    const paginationPipelineStages = buildDynamicPipeline( {payload,groupByFields})


    const pipelineResults = await EntityRel.aggregate( [...consolidatedPipelineStages,...paginationPipelineStages] )
    const resultSet = await generateDashboardResultSet({pipelineResults,user, payload})
    return resultSet

  }

  catch(e) {
    console.log("There is an Error", e)
    return {taskData: {}, allflattenedIds: [], urls: {}, error: "There is an error in fetching Other / Compliance / General tasks for the user"}
  }
}

export const getDataForTaskDashboardComplianceAndOtherTypesOldLatest = async(user, payload) => {
  const {activityCategory,activityTime,taskStatus,taskReadUnread,calendarStart,calendarEnd} = payload
  let {freeTextSearchTerm} = payload
  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = freeTextSearchTerm.replace(invalid, "")
    // freeTextSearchTerm = new RegExp(newSearchString,"i")
    freeTextSearchTerm = newSearchString
    const otherMatchQuery = []

    if(taskReadUnread === "Unread" ) {
      otherMatchQuery.push({taskUnreadStatus:true})
    }
    else if(taskReadUnread === "Read") {
      otherMatchQuery.push({taskUnreadStatus:false})
    }

    if (activityTime === "day") {
      console.log(getDateString("taskEndDate","TODAY"))
      otherMatchQuery.push(getDateString("taskEndDate","TODAY"))
      // otherMatchQuery.push({tasksDueToday:false})
    } else if (activityTime === "week") {
      otherMatchQuery.push(getDateString("taskEndDate","CURRENTWEEK"))
      // otherMatchQuery.push({tasksDueThisWeek:false})
    } else if (activityTime === "month") {
      otherMatchQuery.push(getDateString("taskEndDate","CURRENTMONTH"))
      // otherMatchQuery.push({tasksDueThisMonth:false})
    }

    if(calendarStart && calendarEnd) {
      otherMatchQuery.push({"taskEndDate":{
        $gte: new Date(moment(calendarStart).utc().toDate()),
        $lte:new Date(moment(calendarEnd).utc().toDate())
      }})
    }


    if (taskStatus === "openactive") {
      otherMatchQuery.push({taskStatus:{$in: ["Open", "Active"]}})

    } else if (taskStatus === "closedcancelled") {
      otherMatchQuery.push({taskStatus:{$in: ["Closed", "Cancelled", "Complete","complete", "Completed", "Dropped"]}})
    }

    if (activityCategory === "myfirm") {
      otherMatchQuery.push({ $or: [{ tasksForMyFirmUsers: true }, { tasksForMe: true }]})
    } else if (activityCategory === "dealteam") {
      otherMatchQuery.push({ $or: [{ tasksForMyFirmUsers: true }, { tasksForMe: true }]})
    } else if (activityCategory === "self") {
      otherMatchQuery.push({tasksForMe:true})
    } else {
      otherMatchQuery.push({ $or: [{ tasksForMyFirmUsers: true }, { tasksForMe: true }]})
    }

    console.log("Here is the match query", JSON.stringify(otherMatchQuery,null,2))

    const today = new Date()
    const localoffset = -(today.getTimezoneOffset() / 60)
    const refDate = new Date(new Date().getTime() + localoffset * 3600 * 1000)

    const originalOtherTasksPipeline = [
      {
        $lookup: {
          from: "mvtasks",
          localField: "loggedInFirmUserIds",
          foreignField: "taskDetails.taskAssigneeUserId",
          as: "othertasks"
        }
      }, {
        $unwind: "$othertasks"
      }, {
        $match: {
          $and: [
            {
              "othertasks.relatedActivityDetails.activityId": {
                $exists: false
              }
            }
          ]
        }
      }, {
        $project: {
          taskId: "$othertasks._id",
          tenantId: 1,
          loggedInUserEntityId: 1,
          relationshipType: 1,
          loggedInFirmUserIds: 1,
          userEntitlement:1,
          isUserSupervisor:1,
          eligibleToSeeFirmTasks:1,
          taskCategory: {
            $switch: {
              branches: [
                {
                  case: {
                    $eq: ["$othertasks.taskDetails.taskType", "Compliance"]
                  },
                  then: "Compliance Tasks"
                }, {
                  case: {
                    $eq: ["$othertasks.taskDetails.taskType", "general"]
                  },
                  then: "General Tasks"
                }
              ],
              default: "Other Tasks"
            }
          },
          taskType: "$othertasks.taskDetails.taskType",
          taskDescription: "$othertasks.taskDetails.taskDescription",
          taskUnreadStatus : {$ifNull:["$othertasks.taskDetails.taskUnread",true]},
          taskNotes: "$othertasks.taskDetails.taskNotes",
          complianceTask: "$othertasks.taskDetails.complianceTask",
          taskActivityContext: "Non Transaction Tasks",
          taskEndDate: "$othertasks.taskDetails.taskEndDate",
          taskStatus: "$othertasks.taskDetails.taskStatus",
          taskAssigneeId: "$othertasks.taskDetails.taskAssigneeUserId",
          taskAssigneeName: "$othertasks.taskDetails.taskAssigneeName",
          allIds: [
            "$othertasks._id", "$othertasks.taskDetails.taskAssigneeUserId"
          ],
          tasksForMe: {
            $and: [
              {
                $eq: ["$othertasks.taskDetails.taskAssigneeUserId", ObjectID(user._id)]
              }
            ]
          },
          tasksForMyFirmUsers: {
            $and: [
              {
                $eq: ["$eligibleToSeeFirmTasks", true]
              },

            ]
          },

          "taskDueYear": {
            "$year": "$othertasks.taskDetails.taskEndDate"
          },
          "taskDueMonth": {
            "$month": "$othertasks.taskDetails.taskEndDate"
          },
          "taskDueWeek": {
            "$week": "$othertasks.taskDetails.taskEndDate"
          },
          "taskDueDay": {
            "$dayOfMonth": "$othertasks.taskDetails.taskEndDate"
          },
          "refDateYear": {
            $year: refDate
          },
          "refDateMonth": {
            $month: refDate
          },
          "refDateWeek": {
            $week: refDate
          },
          "refDateDay": {
            $dayOfMonth: refDate
          }
        }
      }, {
        $project: {
          "othertasks": 0
        }
      }, {
        $addFields: {
          tasksDueToday: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }, {
                $eq: ["$taskDueDay", "$refDateDay"]
              }
            ]
          },
          tasksDueThisWeek: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }, {
                $eq: ["$taskDueWeek", "$refDateWeek"]
              }
            ]
          },
          tasksDueThisMonth: {
            $and: [
              {
                $eq: ["$taskDueYear", "$refDateYear"]
              }, {
                $eq: ["$taskDueMonth", "$refDateMonth"]
              }
            ]
          },
          keySearchDetails: {
            $concat: [
              {
                $ifNull: ["$tranStatus", ""]
              }, {
                $ifNull: ["$activityDescription", ""]
              }, {
                $ifNull: ["$tranClientName", ""]
              }, {
                $ifNull: ["$tranType", ""]
              }, {
                $ifNull: ["$tranSubType", ""]
              }, {
                $ifNull: ["$taskDescription", ""]
              }, {
                $ifNull: ["$taskNotes", ""]
              }, {
                $ifNull: ["$taskAssigneeName", ""]
              }
            ]
          },
        }
      }, {
        $match: {
          $and:[...otherMatchQuery]
        }
      },
      {
        $match: {
          $or: [
            {
              "keySearchDetails": {
                $regex: freeTextSearchTerm,
                $options: "i"
              }
            }
          ]
        }
      }
    ]

    const otherTasksPipeline = [...originalOtherTasksPipeline]
    if ((freeTextSearchTerm && freeTextSearchTerm.length === 0) || isEmpty(freeTextSearchTerm)) {
      otherTasksPipeline.splice(-1, 1)
    }

    const consolidatedOtherPipeline = [
      {
        $match: {
          entityParty2: ObjectID(user.entityId)
        }
      }, {
        $project: {
          tenantId: "$entityParty1",
          loggedInUserEntityId: "$entityParty2",
          relationshipType: "$relationshipType"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "loggedInUserEntityId",
          foreignField: "entityId",
          as: "myFirmUsers"
        }
      },
      {
        $unwind:"$myFirmUsers"
      },
      {
        $project: {
          tenantId: "$tenantId",
          loggedInUserEntityId: "$loggedInUserEntityId",
          relationshipType: user.relationshipType,
          loggedInFirmUserIds: "$myFirmUsers._id",
          userEntitlement:user.userEntitlement,
          isUserSupervisor:{$or:[{$in:["Compliance Officer",user.userFlags || []]},{$in:["Supervisory Principal",user.userFlags || []]}]}
        }
      },
      {
        $addFields:{
          eligibleToSeeFirmTasks: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit"]]},
                      {
                        $eq: ["$loggedInUserEntityId", ObjectID(user.entityId)]
                      }
                    ]
                  },
                  "then": true
                },
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$eq:["$isUserSupervisor",true]},
                      {
                        $eq: ["$loggedInUserEntityId", ObjectID(user.entityId)]
                      }
                    ]
                  },
                  "then": true
                },
              ],
              "default": false
            }
          },
        }
      },
      ...otherTasksPipeline
    ]

    const groupByFields = [ "taskCategory","taskUnreadStatus","taskStatus"]
    const paginationPipeline = buildDynamicPipeline( {payload,groupByFields})
    console.log("Other tasks - The pagination pipeline", JSON.stringify(paginationPipeline,null,2))

    const pipelineResults = await EntityRel.aggregate([...consolidatedOtherPipeline,...paginationPipeline])
    const resultSet = await generateDashboardResultSet({pipelineResults,user, payload})
    return resultSet

  } catch (e) {
    console.log("There is an Error", e)
    return {taskData: {}, allflattenedIds: [], urls: {}, error: "There is an error in fetching Other / Compliance / General tasks for the user"}
  }
}

export const getDataForTaskDashboardComplianceAndOtherTypes = async(user, payload) => {
  const {activityCategory,activityTime,taskStatus,taskReadUnread,calendarStart,calendarEnd} = payload
  let {freeTextSearchTerm} = payload
  try {
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = freeTextSearchTerm.replace(invalid, "")
    // freeTextSearchTerm = new RegExp(newSearchString,"i")
    freeTextSearchTerm = newSearchString
    const otherMatchQuery = []

    if(taskReadUnread === "Unread" ) {
      otherMatchQuery.push({taskUnreadStatus:true})
    }
    else if(taskReadUnread === "Read") {
      otherMatchQuery.push({taskUnreadStatus:false})
    }

    if (activityTime === "day") {
      console.log(getDateString("taskEndDate","TODAY"))
      otherMatchQuery.push(getDateString("taskEndDate","TODAY"))
      // otherMatchQuery.push({tasksDueToday:false})
    } else if (activityTime === "week") {
      otherMatchQuery.push(getDateString("taskEndDate","CURRENTWEEK"))
      // otherMatchQuery.push({tasksDueThisWeek:false})
    } else if (activityTime === "month") {
      otherMatchQuery.push(getDateString("taskEndDate","CURRENTMONTH"))
      // otherMatchQuery.push({tasksDueThisMonth:false})
    }

    if(calendarStart && calendarEnd) {
      otherMatchQuery.push({"taskEndDate":{
        $gte: new Date(moment(calendarStart).utc().toDate()),
        $lte:new Date(moment(calendarEnd).utc().toDate())
      }})
    }


    if (taskStatus === "openactive") {
      otherMatchQuery.push({taskStatus:{$in: ["Open", "Active"]}})

    } else if (taskStatus === "closedcancelled") {
      otherMatchQuery.push({taskStatus:{$in: ["Closed", "Cancelled", "Complete","complete", "Completed", "Dropped"]}})
    }

    const matchQueryForEntities = [ {entityParty1:ObjectID(user.tenantId)},{relationshipType:{$in:["Self","Client","Prospect","Third Party"]}} ]

    let idsToBeConsidered
    if(user.relationshipType === "Self" && ["global-edit","global-readonly"].includes(user.userEntitlement)) {
      console.log("FIRM GLOBAL EDIT CASE : Nothing to be Done Here this is global roles they would have access to everything")
      if (activityCategory === "myfirm") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)],"$firmUserIds"]}
      } else if (activityCategory === "dealteam") {
        idsToBeConsidered = {$setUnion:["$userIds","$entityIds"]}
      } else if (activityCategory === "self") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user._id)]]}
      } else {
        idsToBeConsidered = {$setUnion:["$userIds","$entityIds"]}
      }
    } else if ( !["global-edit","global-readonly"].includes(user.userEntitlement) && user.isSuperVisor ) {
      console.log("FIRM SUPERVISOR CASE : Hey this is a supervisor of the firm")
      if (activityCategory === "myfirm") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)],"$firmUserIds"]}
      } else if (activityCategory === "dealteam") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)],"$firmUserIds"]}
      } else if (activityCategory === "self") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user._id)]]}
      } else {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)],"$firmUserIds"]}
      }
    }
    else {
      console.log("ALL OTHER ROLES : Need to ensure that only details of the current user and their firms are obtained")
      matchQueryForEntities.push({entityParty2:ObjectID(user.entityId)})
      if (activityCategory === "myfirm") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)]]}
      } else if (activityCategory === "dealteam") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)]]}
      } else if (activityCategory === "self") {
        idsToBeConsidered = {$setUnion:[[ObjectID(user._id)]]}
      } else {
        idsToBeConsidered = {$setUnion:[[ObjectID(user.entityId),ObjectID(user._id)]]}
      }
    }
    console.log("Here is the match query", JSON.stringify({otherMatchQuery,matchQueryForEntities},null,2))

    const userIdsForTasks = await EntityRel.aggregate([
      {
        $match:{
          $and:[
            ...matchQueryForEntities
          ]
        }
      },
      {
        $lookup:{
          from: "entityusers",
          localField: "entityParty2",
          foreignField: "entityId",
          as: "userDetails"
        }
      },
      {$unwind:{"path":"$userDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project:{
          userId:"$userDetails._id",
          entityId:"$userDetails.entityId",
          firmUser:{"$eq":[ObjectID(user.entityId), "$userDetails.entityId"]}
        }
      },
      {
        $group:{
          _id:null,
          userIds:{
            $addToSet:"$userId"
          },
          entityIds:{
            $addToSet:"$entityId"
          },
          firmUserIds:{
            $push:{$cond:[{$eq:["$firmUser",true]},"$userId",null]}
          }
        }
      },
      {
        $project: {
          _id: 1,
          userIds:1,
          entityIds:1,
          firmUserIds: {
            $filter: {
              input: "$firmUserIds",
              as: "uids",
              cond: {
                $ne: ["$$uids", null]
              }
            }
          }
        }
      },
      {
        $project:{
          _id:0,
          allUniqueIds:{
            ...idsToBeConsidered
          },
        }
      },
    ])

    console.log("Here are the IDs for which data is being Obtained", JSON.stringify(userIdsForTasks,null,2))

    const originalOtherTasksPipeline = [
      {
        $match: {
          $and: [
            {
              "relatedActivityDetails.activityId": {
                $exists: false
              }
            },
            {
              "taskDetails.taskAssigneeUserId":{$in:[...((userIdsForTasks[0] && userIdsForTasks[0].allUniqueIds) || [])]}
            }
          ]
        }
      }, {
        $project: {
          taskId: "$_id",
          tenantId: 1,
          taskCategory: {
            $switch: {
              branches: [
                {
                  case: {
                    $eq: ["$taskDetails.taskType", "Compliance"]
                  },
                  then: "Compliance Tasks"
                }, {
                  case: {
                    $eq: ["$taskDetails.taskType", "general"]
                  },
                  then: "General Tasks"
                }
              ],
              default: "Other Tasks"
            }
          },
          taskType: "$taskDetails.taskType",
          taskDescription: "$taskDetails.taskDescription",
          taskUnreadStatus : {$ifNull:["$taskDetails.taskUnread",true]},
          taskNotes: "$taskDetails.taskNotes",
          complianceTask: "$taskDetails.complianceTask",
          taskActivityContext: "Non Transaction Tasks",
          taskEndDate: "$taskDetails.taskEndDate",
          taskStatus: "$taskDetails.taskStatus",
          taskAssigneeId: "$taskDetails.taskAssigneeUserId",
          taskAssigneeName: "$taskDetails.taskAssigneeName",
          allIds: [
            "$_id", "$taskDetails.taskAssigneeUserId"
          ],
        }
      },
      {
        $addFields: {
          keySearchDetails: {
            $concat: [
              {
                $ifNull: ["$tranStatus", ""]
              }, {
                $ifNull: ["$activityDescription", ""]
              }, {
                $ifNull: ["$tranClientName", ""]
              }, {
                $ifNull: ["$tranType", ""]
              }, {
                $ifNull: ["$tranSubType", ""]
              }, {
                $ifNull: ["$taskDescription", ""]
              }, {
                $ifNull: ["$taskNotes", ""]
              }, {
                $ifNull: ["$taskAssigneeName", ""]
              }
            ]
          },
        }
      },
      {
        $match: {
          $and:[...otherMatchQuery]
        }
      },
      {
        $match: {
          $or: [
            {
              "keySearchDetails": {
                $regex: freeTextSearchTerm,
                $options: "i"
              }
            }
          ]
        }
      }
    ]

    const otherTasksPipeline = [...originalOtherTasksPipeline]
    if(isEmpty(otherMatchQuery)){
      otherTasksPipeline.splice(-2, 1)
    }
    if ((freeTextSearchTerm && freeTextSearchTerm.length === 0) || isEmpty(freeTextSearchTerm)) {
      otherTasksPipeline.splice(-1, 1)
    }

    const groupByFields = [ "taskCategory","taskUnreadStatus","taskStatus"]
    const paginationPipeline = buildDynamicPipeline( {payload,groupByFields})
    console.log("Other tasks - The pagination pipeline", JSON.stringify(paginationPipeline,null,2))

    const pipelineResults = await Tasks.aggregate([...otherTasksPipeline,...paginationPipeline])
    const resultSet = await generateDashboardResultSet({pipelineResults,user, payload})
    return resultSet

  } catch (e) {
    console.log("There is an Error", e)
    return {taskData: {}, allflattenedIds: [], urls: {}, error: "There is an error in fetching Other / Compliance / General tasks for the user"}
  }
}


export const getDataForDashboardCalendar = async(user, payload) => {
  // {"allData":{"deals":[],"bankloans":[],"derivatives":[],"marfps":[],"others":[],"rfps":[],"busdevs":[],"catchalltasks":[]},"allflattenedIds":[],"urls":{"defaultUrls":{},"allurls":{}}}
  // {"groupBy":"","activityCategory":"self","activityTime":"","taskStatus":"","searchTerm":""}
  const taskDashboardPayload = {"groupBy":"","freeTextSearchTerm":"","activityTime":"","activityCategory":"self","taskReadUnread":"","taskStatus":"","pagination":{"serverPerformPagination":false,"size":25,"currentPage":0,"sortFields":{"taskCategory":1}}}
  const newTaskDashboardPayload = {...taskDashboardPayload, ...payload}
  // Get details from the dashboard
  const types = [ "deals", "bankloans","derivatives", "marfps", "others",  "rfps", "busdevs"]

  const {taskData:{data}} = await getDataForTaskDashboardTransactionBased(user,newTaskDashboardPayload)
  const getComplianceTasksData = await getDataForTaskDashboardComplianceAndOtherTypes(user,newTaskDashboardPayload)
  const defaultObject = types.reduce ( (acc, k) => ({...acc,[k]:[]}),{})
  const tranReducedData = (data || []).reduce( (acc, t) => {
    const {calendarType} = t
    if( acc[calendarType]) {
      return {...acc, [calendarType]:[...acc[calendarType],t]}
    }
    return {...acc, [calendarType]:[t]}
  },{})

  const allData = {...defaultObject,...tranReducedData, catchalltasks:getComplianceTasksData.taskData.data}
  return { allData }
}
