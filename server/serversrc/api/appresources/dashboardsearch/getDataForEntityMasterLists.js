import { ObjectID } from "mongodb"
import isEmpty from "lodash/isEmpty"
import { EntityRel, UserEntitlements } from "./../../appresources/models"

export const getEntityDetailsForMasterLists = async (user, searchbody) => {

  // Get current user
  // Get all the entities that he has access to - Refer to the entitlementCollections - Done
  // Get all the users that he has access to - Refer to the entitlementCollections - Done
  // Store that information from the entitlement service - Done
  // Query the entities collection - Done
  // Get the primary address associated with the entity - Done
  // Filter the entities that he has access to - Done
  // Get the users attached to the entities - Done
  // Get the primary user attached to the entity - Done
  // Accumulate all the details that one could search on.
  // Do the facets - get the total number of records - Done
  // Do the facets - get the number of pages  - Done
  // Do the facets - get the data that they are eligible to see - Done
  // Write Proper Logs to ensure that we are able to debug this Properly - Done

  // const body = {
  //   filters: {
  //     entityTypes:[],
  //     userContactTypes:[],
  //     entityMarketRoleFlags:[],
  //     entityIssuerFlags:[],
  //     entityPrimaryAddressStates:[],
  //     freeTextSearchTerm:"NAVEEN"
  //   },
  //   pagination:{
  //     serverPerformPagination:false,
  //     currentPage:2,
  //     size:5,
  //     sortFields:{ entityName:1, userFirstName:1, userLastName:1 }
  //   }
  // }


  console.log(JSON.stringify(searchbody, null, 2))

  const { entityId, entityTypes, userContactTypes, entityMarketRoleFlags, entityIssuerFlags, entityPrimaryAddressStates, freeTextSearchTerm,activeFlag } = searchbody.filters
  const { serverPerformPagination, currentPage, size, sortFields } = searchbody.pagination

  const matchQueryNew = []
  const matchQueryNoUserCase = []
  const entityTypesRevised = isEmpty(entityTypes) ? ["Client", "Prospect"] : [...entityTypes]
  const userContactTypesRevised = [...userContactTypes, "Primary Contact"]

  const userExistsQuery = [{$eq:["$userExistsForEntityNew",true]},{ $eq:["$userRevisedStatus","active"] }]

  if (!isEmpty(entityMarketRoleFlags)) {
    matchQueryNew.push({ "matchOnEntityMarketRole": true })
    matchQueryNoUserCase.push({ "matchOnEntityMarketRole": true })
  }

  if (!isEmpty(entityIssuerFlags)) {
    matchQueryNew.push({ "matchOnIssuerTypeFlags": true })
    matchQueryNoUserCase.push({ "matchOnIssuerTypeFlags": true })
  }

  /*
  if (activeFlag === "active") {
    matchQueryNew.push({ "userRevisedStatus": "active" })
    matchQueryNoUserCase.push({ "userRevisedStatus": "active" })
    userExistsQuery.push({ $eq:["$userRevisedStatus","active"] })
  }
  else if (activeFlag === "inactive") {
    matchQueryNew.push({ "userRevisedStatus": "inactive" })
    matchQueryNoUserCase.push({ "userRevisedStatus": "inactive" })
    userExistsQuery.push({ $eq:["$userRevisedStatus","inactive"] })
  }
  */

  if (!isEmpty(entityPrimaryAddressStates)) {
    matchQueryNew.push({ "matchOnStates": true })
    matchQueryNoUserCase.push({ "matchOnStates": true })
  }

  if (!isEmpty(freeTextSearchTerm)) {
    matchQueryNew.push({ "keySearchDetails": { $regex: freeTextSearchTerm, $options: "i" } })
    matchQueryNoUserCase.push({ "keySearchDetails": { $regex: freeTextSearchTerm, $options: "i" } })
  }

  // const paginationParameter = 5
  const recordstoskip = size * currentPage
  // Get all the eligible Users and Entities

  try {
    let streamLineQuery = false
    let entityList = []
    let usersList = []

    const { relationshipType, userEntitlement} = user

    if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
      streamLineQuery = true
      if (!isEmpty(userContactTypesRevised)) {
        matchQueryNew.push({ "matchOnUserContactType": true })
      }
    } else {
      streamLineQuery = false
      const eligibleUsersAndEntities = await UserEntitlements.findOne({ userId: user._id }).select({ "accessInfo.entities": 1, "accessInfo.users": 1 })
      entityList = [...new Set(eligibleUsersAndEntities.accessInfo.entities.all || []),...entityList]
      usersList = eligibleUsersAndEntities.accessInfo.users.all || []
      matchQueryNew.push({ "accessibleUsers": true })
      matchQueryNoUserCase.push({ "accessibleUsers": false })
    }

    if(!isEmpty(entityId)) {
      entityList =[...entityList, ObjectID(entityId)]
    }

    let matchingQueryStage = {
      matchOnUserContactType: { $and: [{ $isArray: "$userHavingContactFlags" }, { $gt: [{ $size: "$userHavingContactFlags" }, 0] }] },
      matchOnEntityMarketRole: { $and: [{ $isArray: "$entityMarketRoleFlags" }, { $gt: [{ $size: "$entityMarketRoleFlags" }, 0] }] },
      matchOnIssuerTypeFlags: { $and: [{ $isArray: "$entityIssuerTypeFlags" }, { $gt: [{ $size: "$entityIssuerTypeFlags" }, 0] }] },
      userPrimaryPhoneNumber: { $ifNull: ["$userPrimaryPhoneNumber.phoneNumber", "-"] },
      matchOnStates: { $in: ["$entityPrimaryAddressState", [...entityPrimaryAddressStates]] },
      keySearchDetails: {
        $concat: [
          { $ifNull: ["$entityName", ""] },
          { $ifNull: ["$userFirstName", ""] },
          { $ifNull: ["$userLastName", ""] },
          { $ifNull: ["$userPrimaryEmail", ""] },
          { $ifNull: ["$entityPrimaryAddressState", "-"] },
          { $ifNull: ["$entityPrimaryAddressCity", "-"] },
          { $ifNull: ["$entityPrimaryAddressLine1", "-"] },
          { $ifNull: ["$entityPrimaryAddressLine2", "-"] },
          { $ifNull: ["$entityPrimaryAddressName", "-"] },
          { $ifNull: ["$associatedCusipsAggregated", "-"] },
          { $ifNull: ["$entityFlagsAggregated", "-"] },
          { $ifNull: ["$issuerFlagsAggregated", "-"] },
        ]
      },
    }

    if( !streamLineQuery) {
      matchingQueryStage = {...matchingQueryStage, accessibleUsers: { $in: ["$userId", [...new Set(usersList)]] }}
    }

    const queryPipeline = [
      { $match: { entityParty2: ObjectID(user.entityId) } },
      {
        $lookup: {
          from: "entityrels",
          localField: "entityParty1",
          foreignField: "entityParty1",
          as: "alltenantentities"
        }
      },
      {
        $unwind: { path: "$alltenantentities", preserveNullAndEmptyArrays: true }
      },
      {
        $project: {
          entityId: "$alltenantentities.entityParty2",
          relationshipType: "$alltenantentities.relationshipType",
          tenantId: "$alltenantentities.entityParty1"
        }
      },
      {
        $match: { "relationshipType": { $in: [...entityTypesRevised] } }
      },
      {
        $lookup: {
          from: "entities",
          localField: "entityId",
          foreignField: "_id",
          as: "entitydetails"
        }
      },
      {
        $unwind: { path: "$entitydetails", preserveNullAndEmptyArrays: true }
      },
      {
        $match: { "entitydetails._id": { $in: [...entityList] } }
      },
      {
        $project: {
          entityId: 1,
          relationshipType: 1,
          entityName: { $ifNull: ["$entitydetails.firmName", "-"] },
          entityPrimaryAddress: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$entitydetails.addresses",
                  as: "entityaddresses",
                  cond: { $eq: ["$$entityaddresses.isPrimary", true] }
                }
              }, 0]
          },
          entityFlags: { $ifNull: ["$entitydetails.entityFlags", []] },
          entityLinkedCusips: { $ifNull: ["$entitydetails.entityLinkedCusips", []] }
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "entityId",
          foreignField: "entityId",
          as: "allentityusers"
        }
      },
      {
        $unwind: {
          path: "$allentityusers", preserveNullAndEmptyArrays: true
        }
      },
      {
        $project: {
          _id: "$entityId",
          entityId: 1,
          userId: "$allentityusers._id",
          entityRelationshipType: "$relationshipType",
          userRevisedStatus:{$ifNull:["$allentityusers.userStatus","active"]},
          entityName: { $toUpper: { $ifNull: ["$entityName", ""] } },
          userFirstName: { $toUpper: { $ifNull: ["$allentityusers.userFirstName", ""] } },
          userLastName: { $toUpper: { $ifNull: ["$allentityusers.userLastName", ""] } },
          userFullName: { $toUpper: { $concat: [{ $ifNull: ["$allentityusers.userLastName", ""] }, " ", { $ifNull: ["$allentityusers.userFirstName", ""] }] } },
          userPrimaryEmail: { $ifNull: ["$allentityusers.userLoginCredentials.userEmailId", ""] },
          userPrimaryPhoneNumber: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$allentityusers.userPhone",
                  as: "userPhoneNumbers",
                  cond: { $eq: ["$$userPhoneNumbers.phonePrimary", true] }
                }
              }, 0]
          },
          entityPrimaryAddressState: { $ifNull: ["$entityPrimaryAddress.state", "-"] },
          entityPrimaryAddressCity: { $ifNull: ["$entityPrimaryAddress.city", "-"] },
          entityPrimaryAddressLine1: { $ifNull: ["$entityPrimaryAddress.addressline1", "-"] },
          entityPrimaryAddressLine2: { $ifNull: ["$entityPrimaryAddress.addressline2", "-"] },
          entityPrimaryAddressName: { $ifNull: ["$entityPrimaryAddress.addressName", "-"] },
          entityPrimaryAddressGoogle: {
            $ifNull: ["$userPrimaryAddress.formatted_address", {
              $concat: [
                { $ifNull: ["$entityPrimaryAddress.addressline1", ""] },
                { $cond: [{ $eq: [{ $ifNull: ["$entityPrimaryAddress.addressline1", ""] }, ""] }, "", " "] },
                { $ifNull: ["$entityPrimaryAddress.state", ""] },
                { $cond: [{ $eq: [{ $ifNull: ["$entityPrimaryAddress.state", ""] }, ""] }, "", " "] },
                { $ifNull: ["$entityPrimaryAddress.city", ""] },
                { $cond: [{ $eq: [{ $ifNull: ["$entityPrimaryAddress.city", ""] }, ""] }, "", " "] },
                { $ifNull: ["$entityPrimaryAddress.zipCode.zip1", ""] }
              ]
            }]
          },

          entityPrimaryAddressGoogleUrl: { $ifNull: ["$entityPrimaryAddress.url", "-"] },
          userFlagsAggregated: {
            $reduce: {
              input: "$allentityusers.userFlags",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          associatedCusipsAggregated: {
            $reduce: {
              input: "$entityLinkedCusips.associatedCusip6",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          entityFlagsAggregated: {
            $reduce: {
              input: "$entityFlags.marketRole",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          issuerFlagsAggregated: {
            $reduce: {
              input: "$entityFlags.issuerFlags",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          userHavingContactFlags: {
            $setIntersection: [userContactTypesRevised, { $ifNull: ["$allentityusers.userFlags", []] }]
          },
          entityMarketRoleFlags: {
            $setIntersection: [entityMarketRoleFlags, { $ifNull: ["$entityFlags.marketRole", []] }]
          },
          entityIssuerTypeFlags: {
            $setIntersection: [entityIssuerFlags, { $ifNull: ["$entityFlags.issuerFlags", []] }]
          },
          userExistsForEntityNew: { $cond: [{ $gt: ["$allentityusers._id", null] }, true, false] },
        }
      },
      {
        $match: { userRevisedStatus: "active" }
      },      
      {
        $addFields:{
          userExistsForEntityNewWithStatus: {
            $and:[
              ...userExistsQuery
            ]
          }
        }
      },
      {
        $addFields: {
          ...matchingQueryStage
        }
      },
      {
        $facet: {
          condition1: [
            {
              $match: {
                $or: [
                  {
                    $and: [
                      { userExistsForEntityNewWithStatus: false },
                      ...matchQueryNoUserCase
                    ]
                  },
                  {
                    $and: [
                      ...matchQueryNew
                    ]
                  }
                ]
              }
            },
            {
              "$group": {
                "_id": "$entityId",
                count: { $sum: 1 },
                data: { $push: "$$ROOT" }
              }
            },
            {
              "$project": {
                results: { "$slice": ["$data", 1] }
              }
            },
            { "$unwind": "$results" },
            { "$replaceRoot": { "newRoot": "$results" } }
          ],
          condition2: [
            {
              "$group": {
                "_id": "$entityId",
                isThereAMatchWithPrimary: { $sum: { $cond: ["$matchOnUserContactType", 1, 0] } },
                data: { $push: "$$ROOT" }
              }
            },
            {
              $match: { isThereAMatchWithPrimary: 0 }
            },
            {
              "$project": {
                results: { "$slice": ["$data", 1] }
              }
            },
            { "$unwind": "$results" },
            { "$replaceRoot": { "newRoot": "$results" } },
            {
              $match: {
                $and: [
                  ...matchQueryNoUserCase,
                  { userExistsForEntityNewWithStatus: true },
                  { matchOnUserContactType: false },
                ]
              }
            },

          ]
        },
      },
      {
        "$project": {
          // "data": { "$concatArrays": ["$condition1", "$condition2"] }
          "data": { "$setUnion": ["$condition1", "$condition2"] }
        }
      },
      { "$unwind": "$data" },
      { "$replaceRoot": { "newRoot": "$data" } },
      {
        $facet: {
          metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", size] } } } }],
          data: [{ $sort: { ...sortFields } }, { $skip: recordstoskip }, { $limit: size }] // add projection here wish you re-shape the docs
        }
      }
    ]

    if(streamLineQuery) {
      queryPipeline.splice(7,1)
    }
    if (!serverPerformPagination) {
      queryPipeline.splice(-1, 1)
    }
    const pipeLineQueryResults = await EntityRel.aggregate([
      ...queryPipeline
    ])
    return { pipeLineQueryResults }
  } catch (e) {
    console.log(e)
    return { pipeLineQueryResults: [] }
  }
}

// getEntityDetailsForMasterLists({_id:ObjectID("5bf782cd9d1d1b20b40de56e"), entityId:ObjectID("5bf782cc9d1d1b20b40de52d")}).then( a=> console.log(JSON.stringify(a, null,2)))
