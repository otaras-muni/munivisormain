import { ObjectID } from "mongodb"
import isEmpty from "lodash/isEmpty"
import { EntityRel, UserEntitlements } from "./../../appresources/models"

const multiSplice = (array, ...indexes) => {
  indexes.sort((a, b) => a - b)
  indexes.forEach((item, index) => array.splice(item - index, 1))
}



export const getUserDetailsForMasterLists = async (user, searchbody) => {

  // const body = {
  //   filters: {
  //     userContactTypes:[],
  //     entityMarketRoleFlags:[],
  //     entityIssuerFlags:[],
  //     userPrimaryAddressStates:[],
  //     freeTextSearchTerm:"NAVEEN"
  //   },
  //   pagination:{
  //     serverPerformPagination:false,
  //     currentPage:2,
  //     size:5,
  //     sortFields:{ entityName:1, userFirstName:1, userLastName:1 }
  //   }
  // }

  console.log(JSON.stringify(searchbody, null, 2))

  const { entityId, userContactTypes, entityMarketRoleFlags, entityIssuerFlags, userPrimaryAddressStates, freeTextSearchTerm, entityTypes = ["Client","Prospect","Third Party","Self"], activeFlag } = searchbody.filters
  const { serverPerformPagination, currentPage, size, sortFields } = searchbody.pagination

  const matchQueryNew = []
  const userContactTypesRevised = [...userContactTypes]


  
  if (!isEmpty(userContactTypesRevised)) {
    matchQueryNew.push({ "matchOnUserContactType": true })
  }

  if (!isEmpty(entityMarketRoleFlags)) {
    matchQueryNew.push({ "matchOnMarketRoleOrEntityType": true })
  }

  if (!isEmpty(entityIssuerFlags)) {
    matchQueryNew.push({ "matchOnIssuerTypeFlags": true })
  }

  if (!isEmpty(userPrimaryAddressStates)) {
    matchQueryNew.push({ "matchOnStates": true })
  }

  if (activeFlag === "active") {
    matchQueryNew.push({ "userRevisedStatus": "active" })
  }
  else if (activeFlag === "inactive") {
    matchQueryNew.push({ "userRevisedStatus": "inactive" })
  }

  if (!isEmpty(freeTextSearchTerm)) {
    matchQueryNew.push({ "keySearchDetails": { $regex: freeTextSearchTerm, $options: "i" } })
  }

  if(isEmpty(entityId)){
    if (!isEmpty(entityTypes)) {
      matchQueryNew.push({ "entityRelationshipType": { $in: [...entityTypes] } })
    }
  }

  matchQueryNew.push({"userExistsForEntityNew":true})

  console.log("The Match Query is", JSON.stringify(matchQueryNew, null, 2) )

  // const paginationParameter = 5
  const recordstoskip = size * currentPage



  // Get all the eligible Users and Entities
  try {

    let streamLineQuery = false
    let entityList = []
    let usersList = []

    const { relationshipType, userEntitlement} = user

    if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
      streamLineQuery = true
    } else {
      streamLineQuery = false
      const eligibleUsersAndEntities = await UserEntitlements.findOne({ userId: user._id }).select({ "accessInfo.entities": 1, "accessInfo.users": 1 })
      entityList = [...eligibleUsersAndEntities.accessInfo.entities.all || [],...entityList]
      usersList = eligibleUsersAndEntities.accessInfo.users.all || []
    }

    let newEntityList = []
    let spliceFields = []

    if (!isEmpty(entityId)) {
      if(streamLineQuery) {
        newEntityList = [ObjectID(entityId)]
        spliceFields = [...spliceFields, 10]
      } else {
        newEntityList = entityList.filter( e => ObjectID(e).toString() === ObjectID(entityId).toString())
      }
    } else if(streamLineQuery){
      spliceFields = [...spliceFields, 6,10]
    } else {
      newEntityList = entityList
    }

    console.log("This the new entity List", newEntityList )

    const queryPipeline = [
      { $match: { entityParty2: ObjectID(user.entityId) } },
      {
        $lookup: {
          from: "entityrels",
          localField: "entityParty1",
          foreignField: "entityParty1",
          as: "alltenantentities"
        }
      },
      {
        $unwind: { path: "$alltenantentities", preserveNullAndEmptyArrays: true }
      },
      {
        $project: {
          entityId: "$alltenantentities.entityParty2",
          relationshipType: "$alltenantentities.relationshipType",
          tenantId: "$alltenantentities.entityParty1"
        }
      },
      {
        $lookup: {
          from: "entities",
          localField: "entityId",
          foreignField: "_id",
          as: "entitydetails"
        }
      },
      {
        $unwind: { path: "$entitydetails", preserveNullAndEmptyArrays: true }
      },
      {
        $match: { "entitydetails._id": { $in: [...newEntityList] } }
      },
      {
        $project: {
          entityId: 1,
          relationshipType: 1,
          entityName: { $ifNull: ["$entitydetails.firmName", ""] },
          entityPrimaryAddress: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$entitydetails.addresses",
                  as: "entityaddresses",
                  cond: { $eq: ["$$entityaddresses.isPrimary", true] }
                }
              }, 0]
          },
          entityFlags: { $ifNull: ["$entitydetails.entityFlags", []] },
          entityLinkedCusips: { $ifNull: ["$entitydetails.entityLinkedCusips", []] }
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "entityId",
          foreignField: "entityId",
          as: "allentityusers"
        }
      },
      {
        $unwind: {
          path: "$allentityusers", preserveNullAndEmptyArrays: true
        }
      },
      {
        $match: { "allentityusers._id": { $in: [...usersList] } }
      },
      {
        $project: {
          entityId: 1,
          userId:"$allentityusers._id",
          userRevisedStatus:{$ifNull:["$allentityusers.userStatus","active"]},
          entityRelationshipType: "$relationshipType",
          entityName: {$toUpper:{ $ifNull: ["$entityName", ""] }},
          userFirstName: {$toUpper:{ $ifNull: ["$allentityusers.userFirstName", ""] }},
          userLastName: {$toUpper:{ $ifNull: ["$allentityusers.userLastName", ""] }},
          userJobTitle: {$toUpper:{ $ifNull: ["$allentityusers.userJobTitle", ""] }},
          userJoiningDate: {$toUpper:{ $ifNull: ["$allentityusers.userJoiningDate", ""] }},
          userExitDate: {$toUpper:{ $ifNull: ["$allentityusers.userExitDate", ""] }},
          userFullName: {$toUpper:{ $concat: [{ $ifNull: ["$allentityusers.userLastName", ""] }, " ", { $ifNull: ["$allentityusers.userFirstName", ""] }] }},
          userPrimaryEmail: {$toLower:{ $ifNull: ["$allentityusers.userLoginCredentials.userEmailId", ""] }},
          userPrimaryPhoneNumber: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$allentityusers.userPhone",
                  as: "userPhoneNumbers",
                  cond: { $eq: ["$$userPhoneNumbers.phonePrimary", true] }
                }
              }, 0]
          },
          userPrimaryAddress: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$allentityusers.userAddresses",
                  as: "userAddresses",
                  cond: { $eq: ["$$userAddresses.isPrimary", true] }
                }
              }, 0]
          },
          entityPrimaryAddressState: { $ifNull: ["$entityPrimaryAddress.state", ""] },
          entityPrimaryAddressCity: { $ifNull: ["$entityPrimaryAddress.city", ""] },
          entityPrimaryAddressLine1: { $ifNull: ["$entityPrimaryAddress.addressLine1", ""] },
          entityPrimaryAddressLine2: { $ifNull: ["$entityPrimaryAddress.addressLine2", ""] },
          entityPrimaryAddressName: { $ifNull: ["$entityPrimaryAddress.addressName", ""] },
          entityPrimaryAddressGoogle: { $ifNull: ["$entityPrimaryAddress.formatted_address", ""] },
          entityPrimaryAddressGoogleUrl: { $ifNull: ["$entityPrimaryAddress.url", ""] },
          userFlagsAggregated: {
            $reduce: {
              input: "$allentityusers.userFlags",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          associatedCusipsAggregated: {
            $reduce: {
              input: "$entityLinkedCusips.associatedCusip6",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          entityFlagsAggregated: {
            $reduce: {
              input: "$entityFlags.marketRole",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          issuerFlagsAggregated: {
            $reduce: {
              input: "$entityFlags.issuerFlags",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          userHavingContactFlags: {
            $setIntersection: [userContactTypesRevised, { $ifNull: ["$allentityusers.userFlags", []] }]
          },
          entityMarketRoleFlags: {
            $setIntersection: [entityMarketRoleFlags, { $ifNull: ["$entityFlags.marketRole", []] }]
          },
          entityIssuerTypeFlags: {
            $setIntersection: [entityIssuerFlags, { $ifNull: ["$entityFlags.issuerFlags", []] }]
          },
          userExistsForEntityNew: { $cond: [{ $gt: ["$allentityusers._id", null] }, true, false] },
        }
      },
      {
        $addFields:{
          userPrimaryAddressState: { $ifNull: ["$userPrimaryAddress.state", ""] },
          userPrimaryAddressCity: { $ifNull: ["$userPrimaryAddress.city", ""] },
          userPrimaryAddressLine1: { $ifNull: ["$userPrimaryAddress.addressLine1", ""] },
          userPrimaryAddressLine2: { $ifNull: ["$userPrimaryAddress.addressLine2", ""] },
          userPrimaryAddressName: { $ifNull: ["$userPrimaryAddress.addressName", ""] },
          userPrimaryAddressGoogle:"$userPrimaryAddress.formatted_address",
          userPrimaryAddressZipCode:{ $ifNull: ["$userPrimaryAddress.zipCode.zip1", ""] },
          userPrimaryAddressGoogleUrl:{ $ifNull: ["$userPrimaryAddress.url", ""] }
        }
      },
      {
        $addFields:{
          userPrimaryAddressState: { $ifNull: ["$userPrimaryAddressState", ""] },
          userPrimaryAddressCity: { $ifNull: ["$userPrimaryAddressCity", ""] },
          userPrimaryAddressLine1: { $ifNull: ["$userPrimaryAddressLine1", ""] },
          userPrimaryAddressLine2: { $ifNull: ["$userPrimaryAddressLine2", ""] },
          userPrimaryAddressName: { $ifNull: ["$userPrimaryAddressName", ""] },
          userPrimaryAddressGoogle: { $ifNull: ["$userPrimaryAddressGoogle", {
            $concat:[
              { $ifNull: ["$userPrimaryAddressLine1", ""] },
              { $cond:[{$eq:[{ $ifNull: ["$userPrimaryAddressLine1", ""] },""]},""," "]},
              { $ifNull: ["$userPrimaryAddressCity", ""] },
              { $cond:[{$eq:[{ $ifNull: ["$userPrimaryAddressCity", ""] },""]},""," "]},
              { $ifNull: ["$userPrimaryAddressState", ""] },
              { $cond:[{$eq:[{ $ifNull: ["$userPrimaryAddressState", ""] },""]},""," "]},
              { $ifNull: ["$userPrimaryAddressZipCode", ""] }
            ]}]},
          userPrimaryAddressGoogleUrl: { $ifNull: ["$userPrimaryAddressGoogleUrl" ,""]}
        }
      },
      {
        $addFields: {
          matchOnUserContactType: { $and: [{ $isArray: "$userHavingContactFlags" }, { $gt: [{ $size: "$userHavingContactFlags" }, 0] }] },
          matchOnMarketRoleOrEntityType: {
            $or:
            [
              { $in: ["$entityRelationshipType", [...entityMarketRoleFlags]] },
              {
                $and: [{ $isArray: "$entityMarketRoleFlags" }, { $gt: [{ $size: "$entityMarketRoleFlags" }, 0] }]
              }
            ]
          },
          matchOnIssuerTypeFlags: { $and: [{ $isArray: "$entityIssuerTypeFlags" }, { $gt: [{ $size: "$entityIssuerTypeFlags" }, 0] }] },
          userPrimaryPhoneNumber: { $ifNull: ["$userPrimaryPhoneNumber.phoneNumber", "-"] },
          matchOnStates: { $in: ["$userPrimaryAddress.state", [...userPrimaryAddressStates]] },
          keySearchDetails: {
            $concat: [
              { $ifNull: ["$entityName", ""] },
              { $ifNull: ["$userFirstName", ""] },
              { $ifNull: ["$userLastName", ""] },
              { $ifNull: ["$userPrimaryEmail", ""] },
              // { $ifNull: ["$entityPrimaryAddress.state", "-"] },
              // { $ifNull: ["$entityPrimaryAddress.city", "-"] },
              // { $ifNull: ["$entityPrimaryAddress.addressline1", "-"] },
              // { $ifNull: ["$entityPrimaryAddress.addressline2", "-"] },
              // { $ifNull: ["$entityPrimaryAddress.addressName", "-"] },
              { $ifNull: ["$userPrimaryAddress.state", ""] },
              { $ifNull: ["$userPrimaryAddress.city", ""] },
              { $ifNull: ["$userPrimaryAddress.addressLine1", ""] },
              { $ifNull: ["$userPrimaryAddress.addressLine2", ""] },
              { $ifNull: ["$userPrimaryAddress.addressName", ""] },
              { $ifNull: ["$associatedCusipsAggregated", ""] },
              { $ifNull: ["$entityFlagsAggregated", ""] },
              { $ifNull: ["$issuerFlagsAggregated", ""] },
            ]
          },
        }
      },
      {
        $match: {
          $or: [
            {
              $and: [
                ...matchQueryNew
              ]
            }
          ]
        }
      },
      {
        $facet: {
          metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", size] } } } }],
          data: [{ $sort: { ...sortFields } }, { $skip: recordstoskip }, { $limit: size }] // add projection here wish you re-shape the docs
        }
      }
    ]

    if(streamLineQuery) {
      multiSplice(queryPipeline,...spliceFields)
    }

    if (!serverPerformPagination && isEmpty(matchQueryNew)) {
      queryPipeline.splice(-1, 2)
    }

    if (!serverPerformPagination && !isEmpty(matchQueryNew)) {
      queryPipeline.splice(-1, 1)
    }


    if (serverPerformPagination && isEmpty(matchQueryNew)) {
      queryPipeline.splice(-2, 1)
    }


    const pipeLineQueryResults = await EntityRel.aggregate([
      ...queryPipeline
    ])

    // console.log("pipeLineQueryresults",JSON.stringify(pipeLineQueryResults,null,2))

    return { pipeLineQueryResults }
  } catch (e) {
    console.log(e)
    return { pipeLineQueryResults: [] }
  }
}

