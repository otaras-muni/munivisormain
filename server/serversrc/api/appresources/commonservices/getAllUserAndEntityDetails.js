import {EntityUser, EntityRel} from "./../models"
import {ObjectID} from "mongodb"
const relationshipTypes = ["Client","Self","Prospect","Third Party","Undefined"]

export const getEntityAndUserDetails = async(user, queryRelationshipTypes = [...relationshipTypes]) => {

  const {relationshipType} = await EntityRel
    .findOne({
      entityParty2: ObjectID(user.entityId),
      relationshipType:{$in:[...queryRelationshipTypes]}
    })
    .select({"relationshipType": 1, _id: 0})
    
  const lkupConfig = {
    "Self": {
      lkup: [
        {
          $lookup: {
            from: "entityrels",
            localField: "entityParty1",
            foreignField: "entityParty1",
            as: "alltenantentities"
          }
        }, {
          $unwind: "$alltenantentities"
        }, 
        {
          $match:{ "$alltenantentities.relationshipType":{$in:[...queryRelationshipTypes]}}
        },
        {
          $project: {
            entityId: "$alltenantentities.entityParty2",
            relType: "$alltenantentities.relationshipType"
          }
        }
      ]
    },
    "Third Party": {
      lkup: [
        {
          $project: {
            entityId: "$entityParty2",
            relType: "$relationshipType"
          }
        }
      ]
    },
    "Client": {
      lkup: [
        {
          $project: {
            entityId: "$entityParty2",
            relType: "$relationshipType"
          }
        }
      ]
    }
  }

  const allEntitiesAndUsers = await EntityRel.aggregate([
    {
      $match: {
        entityParty2: ObjectID(user.entityId),
        relationshipType:{$in:[...queryRelationshipTypes]}
      }
    },
    ...lkupConfig[relationshipType].lkup, {
      $lookup: {
        from: "entityusers",
        localField: "entityId",
        foreignField: "entityId",
        as: "alltenantentityusers"
      }
    }, {
      $unwind: "$alltenantentityusers"
    }, {
      $project: {
        _id: 0,
        entityId: 1,
        userId: "$alltenantentityusers._id",
        userFullName: {
          $concat: ["$alltenantentityusers.userFirstName", " ", "$alltenantentityusers.userLastName"]
        },
        userPrimaryEmail: "$alltenantentityusers.userLoginCredentials.userEmailId"
      }
    }
  ])
  return allEntitiesAndUsers
}

export const getAllEntityAndUserInformationForTenant = async(user,queryRelationshipTypes=[...relationshipTypes]) => {

  const allEntitiesAndUsers = await EntityRel.aggregate([
    {
      $match: {
        entityParty2: ObjectID(user.entityId),
        relationshipType:{$in:[...queryRelationshipTypes]}
      }
    }, {
      $lookup: {
        from: "entityrels",
        localField: "entityParty1",
        foreignField: "entityParty1",
        as: "alltenantentities"
      }
    }, {
      $unwind: "$alltenantentities"
    }, 
    {
      $match:{ "alltenantentities.relationshipType":{$in:[...queryRelationshipTypes]}}
    },
    {
      $project: {
        entityId: "$alltenantentities.entityParty2",
        relType: "$alltenantentities.relationshipType"
      }
    }, {
      $lookup: {
        from: "entityusers",
        localField: "entityId",
        foreignField: "entityId",
        as: "alltenantentityusers"
      }
    }, {
      $unwind: "$alltenantentityusers"
    }, {
      $project: {
        _id: 0,
        entityId: 1,
        userId: "$alltenantentityusers._id",
        userFullName: {
          $concat: ["$alltenantentityusers.userFirstName", " ", "$alltenantentityusers.userLastName"]
        },
        userPrimaryEmail: "$alltenantentityusers.userLoginCredentials.userEmailId"
      }
    }, {
      $lookup: {
        from: "entities",
        localField: "entityId",
        foreignField: "_id",
        as: "entitydetails"
      }
    }, {
      $unwind: "$entitydetails"
    }, {
      $addFields: {
        entityName: "$entitydetails.firmName"
      }
    }, {
      $project: {
        entitydetails: 0
      }
    }

  ])
  return allEntitiesAndUsers
}

export const getAllUsersAcrossAllTenants = async(queryRelationshipTypes=[...relationshipTypes]) => {

  const allUsersForAllTenantsAndTheirEntities = await EntityRel.aggregate([
    {
      $match: {
        relationshipType: "Self"
      }
    }, {
      $lookup: {
        from: "entityrels",
        localField: "entityParty1",
        foreignField: "entityParty1",
        as: "alltenantentities"
      }
    }, {
      $unwind: "$alltenantentities"
    }, 
    {
      $match:{ "alltenantentities.relationshipType":{$in:[...queryRelationshipTypes]}}
    },
    {
      $project: {
        tenantId:"$entityParty1",
        relationshipToTenant:"$alltenantentities.relationshipType",
        entityId: "$alltenantentities.entityParty2",
        relType: "$alltenantentities.relationshipType"
      }
    }, {
      $lookup: {
        from: "entityusers",
        localField: "entityId",
        foreignField: "entityId",
        as: "alltenantentityusers"
      }
    }, {
      $unwind: "$alltenantentityusers"
    }, {
      $project: {
        _id: 0,
        entityId: 1,
        tenantId:1,
        relationshipToTenant:1,
        userId: "$alltenantentityusers._id",
        userEntitlement: "$alltenantentityusers.userEntitlement",
        userFullName: {
          $concat: ["$alltenantentityusers.userFirstName", " ", "$alltenantentityusers.userLastName"]
        },
        userPrimaryEmail: "$alltenantentityusers.userLoginCredentials.userEmailId",
        stpEligible: {$ifNull:["$alltenantentityusers.userLoginCredentials.isUserSTPEligible",false]},
      }
    }, {
      $lookup: {
        from: "entities",
        localField: "entityId",
        foreignField: "_id",
        as: "entitydetails"
      }
    }, {
      $unwind: "$entitydetails"
    }, {
      $addFields: {
        entityName: "$entitydetails.firmName"
      }
    }, {
      $project: {
        entitydetails: 0
      }
    },
    {
      $lookup: {
        from: "entities",
        localField: "tenantId",
        foreignField: "_id",
        as: "tenantDetails"
      }
    }, 
    {
      $unwind: "$tenantDetails"
    }, 
    {
      $project: {
        tenantId:1,
        tenantFirmName:"$tenantDetails.firmName",
        entityId:1,
        entityName:1,
        relationshipToTenant:1,
        userId: 1,
        userEntitlement: 1,
        userFullName: 1,
        userPrimaryEmail: 1,
        stpEligible: 1
      }
    },
    { $sort: { tenantId:1, entityId:1 } }

  ])
  return allUsersForAllTenantsAndTheirEntities
}




export const getAllEntitiesForTenant = async(user,queryRelationshipTypes=[...relationshipTypes]) => {

  const allEntityInformation = await EntityRel.aggregate([
    {
      $match: {
        entityParty2: ObjectID(user.entityId),
        relationshipType:{$in:[...queryRelationshipTypes]}
      }
    }, {
      $lookup: {
        from: "entityrels",
        localField: "entityParty1",
        foreignField: "entityParty1",
        as: "alltenantentities"
      }
    }, {
      $unwind: "$alltenantentities"
    }, 
    {
      $match:{ "alltenantentities.relationshipType":{$in:[...queryRelationshipTypes]}}
    },
    {
      $project: {
        entityId: "$alltenantentities.entityParty2",
        relType: "$alltenantentities.relationshipType"
      }
    }, {
      $lookup: {
        from: "entities",
        localField: "entityId",
        foreignField: "_id",
        as: "alltenantentities"
      }
    }, {
      $unwind: "$alltenantentities"
    }, {
      $project: {
        _id: 0,
        entityId: 1,
        relType:1,
        name:"$alltenantentities.firmName"
      }
    }, 

  ])
  return allEntityInformation
}

export const getAllUsersForLoggedInUsersFirm = async(user) => {

  const allUserInformationForLoggedInUserFirm = await EntityUser.aggregate([
    {
      $match: {
        entityId: ObjectID(user.entityId)
      }
    }, 
    {
      $project: {
        _id:0,
        userId: "$_id",
        entityId: 1,
        userFullName: {
          $concat: ["$userFirstName", " ", "$userLastName"]
        },
        userPrimaryEmail: "$userLoginCredentials.userEmailId"
      }
    }
  ])
  return allUserInformationForLoggedInUserFirm

}

