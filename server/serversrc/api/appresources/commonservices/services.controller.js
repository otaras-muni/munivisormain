import {ObjectID} from "mongodb"
import axios from "axios"
import XLSX from "xlsx"
import  { EntityRel, Entity, EntityUser,Deals,RFP,ActMaRFP,Derivatives,BankLoans, Others,ActBusDev }  from "./../models"
import { canUserPerformAction, getSupervisorDetails } from "./../../commonDbFunctions"
import { urlGenerator} from "./urlGeneratorHelper"
import { tenantUrlGenerator } from "./urlGeneratorForTenant"
import { getAllEntitiesForTenant,getAllUsersAcrossAllTenants} from "./getAllUserAndEntityDetails"
import {
  getConsolidatedAccessAndEntitlementsForLoggedInUser,
  getAllAccessAndEntitlementsForLoggedInUser,
  updateEligibleIdsForLoggedInUserFirm
} from "../../entitlementhelpers"
import { getTransactionInformationForLoggedInUser} from "./getTransactionsForLoggedInUser"
import {transactionsTabsKeys, returnFromESObject} from "./constants"


const commonQuerySections = [
  {
    "$unwind":"$participantIds"
  },
  {
    "$lookup":{
      from: "entityusers",
      localField: "participantIds",
      foreignField: "_id",
      as: "userdetails"
    },
  },
  {
    "$unwind":"$userdetails"
  },
  {
    $project:{
      userId:"$userdetails._id",
      userFirstName:"$userdetails.userFirstName",
      userLastName:"$userdetails.userLastName",
      userEmails:"$userdetails.userEmails",
      entityId:"$userdetails.entityId",
      userFirmName:"$userdetails.userFirmName",
      participantEntities:1
    }
  },
  {
    "$unwind":"$userEmails"
  },
  {
    $match:{"userEmails.emailPrimary":true}
  },
  {
    $project:{
      "userId":1,
      "userFirstName":1,
      "userLastName":1,
      "entityId":1,
      "sendEmailTo":"$userEmails.emailId",
      "userFirmName":1,
      participantEntities:1
    }
  }
]


export const getEligibleUserDetailsForLoggedInUser = async (user) => {

  let idsToCheck = []

  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
    idsToCheck = []
  } else {
    try {
      const detailsIds = await getAllAccessAndEntitlementsForLoggedInUser(user,"nav")
      idsToCheck = [...((detailsIds && detailsIds.all) || [])]
    } catch (e) {
      idsToCheck = []
    }
  }

  const tenantArray = [
    {
      $lookup:
          {
            from: "entityrels",
            localField: "entityParty1",
            foreignField: "entityParty1",
            as: "alltenantentities"
          }
    },
    { $unwind : "$alltenantentities"},
    { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}},
    { $match: {
      relType:{$in:["Self","Client","Third Party","Prospect"]}
    }}
  ]

  const otherArray = [
    { $project: {entityId:"$entityParty2", relType:"$relationshipType"}}
  ]

  const mapper = {
    "Self":tenantArray,
    "Client":otherArray,
    "Third Party":otherArray,
    "Undefined":otherArray
  }

  const checkIds = idsToCheck.length > 0 ? [
    {$match:{"alltenantentityusers._id":{"$in":[...idsToCheck]}}}
  ] : []

  const pipeline = [
    {$match:{entityParty2:ObjectID(user.entityId)}},
    ...mapper[user.relationshipType],
    {
      $lookup:
            {
              from: "entityusers",
              localField: "entityId",
              foreignField: "entityId",
              as: "alltenantentityusers"
            }
    },
    {
      $unwind:"$alltenantentityusers"
    },
    ...checkIds,
    {
      $project:{
        entityId:1,
        relType:1,
        entitlement:"$alltenantentityusers.userEntitlement",
        userId:"$alltenantentityusers._id",
        userFirstName:"$alltenantentityusers.userFirstName",
        userLastName:"$alltenantentityusers.userLastName",
        loginEmail:{$ifNull:["$alltenantentityusers.userLoginCredentials.userEmailId",""]},
        stpEligible:{$ifNull:["$alltenantentityusers.userLoginCredentials.isUserSTPEligible",false]},
        password:"password"
      }
    },
    {
      $lookup:
            {
              from: "entities",
              localField: "entityId",
              foreignField: "_id",
              as: "entitydetails"
            }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        _id:"$userId",
        entityId:1,
        firmName:"$entitydetails.firmName",
        relType:1,
        entitlement:1,
        userId:1,
        userFirstName:1,
        userLastName:1,
        loginEmail:1,
        stpEligible:1,
      }
    }

  ]

  let allTenantUsersPrimaryEmailId = []
  try {
    allTenantUsersPrimaryEmailId = await EntityRel.aggregate(pipeline)
    console.log("1.EXTRACTED ALL EMAILS",allTenantUsersPrimaryEmailId[0])
    return {message:"Successfully extracted all emails from ", data:allTenantUsersPrimaryEmailId}

  }  catch (e) {
    console.log(e)
    return {error:"Error in extracting all the users with primary email",message:"", data:[]}
  }
}

export const getEligibleEntityDetailsForLoggedInUser = async (user) => {

  let idsToCheck = []

  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
    idsToCheck = []
  } else {
    try {
      const {all} = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,"nav")
      idsToCheck = [...all]
    } catch (e) {
      idsToCheck = []
    }
  }

  const tenantArray = [
    {
      $lookup:
          {
            from: "entityrels",
            localField: "entityParty1",
            foreignField: "entityParty1",
            as: "alltenantentities"
          }
    },
    { $unwind : "$alltenantentities"},
    { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}},
    { $match: {
      relType:{$in:["Self","Client","Third Party","Prospect"]}
    }}    
  ]

  const otherArray = [
    { $project: {entityId:"$entityParty2", relType:"$relationshipType"}}
  ]

  const mapper = {
    "Self":tenantArray,
    "Client":otherArray,
    "Third Party":otherArray,
    "Undefined":otherArray
  }

  const checkIds = idsToCheck.length > 0 ? [
    {$match:{"allentitydetails._id":{"$in":[...idsToCheck]}}}
  ] : []

  const pipeline = [
    {$match:{entityParty2:ObjectID(user.entityId)}},
    ...mapper[user.relationshipType],
    {
      $lookup:
            {
              from: "entities",
              localField: "entityId",
              foreignField: "_id",
              as: "allentitydetails"
            }
    },
    {
      $unwind:"$allentitydetails"
    },
    ...checkIds,
    {
      $project:{
        _id:"$allentitydetails._id",
        entityId:"$allentitydetails._id",
        firmName:"$allentitydetails.firmName",
        relType:1,
      }
    }
  ]

  let allTenantEntityDetails = []
  try {
    allTenantEntityDetails = await EntityRel.aggregate(pipeline)
    console.log("1.EXTRACTED ALL EMAILS",allTenantEntityDetails[0])
    return {message:"Successfully extracted all entity Details for logged in user ", data:allTenantEntityDetails}
  }  catch (e) {
    console.log(e)
    return {error:"Error in extracting all the users with primary email",message:"", data:[]}
  }
}

export const fetchEligibleUserDetailsForLoggedInUser = async(req,res) => {
  try {
    const retValues =  await getEligibleUserDetailsForLoggedInUser(req.user)
    return res.status(200).send(retValues)
  } catch(e) {
    console.log(e)
  }
}

export const fetchEligibleEntityDetailsForLoggedInUser = async(req,res) => {
  try {
    const retValues =  await getEligibleEntityDetailsForLoggedInUser(req.user)
    return res.status(200).send(retValues)
  } catch(e) {
    console.log(e)
  }
}

export const fetchEligibleTransactionDetailsForLoggedInUser = async(req,res) => {
  try {
    const retValues =  await getEligibleEntityDetailsForLoggedInUser(req.user)
    return res.status(200).send(retValues)
  } catch(e) {
    console.log(e)
  }
}

export const fetchSuperVisorDetailsForLoggedInUser = async(req, res) => {
  try {
    const supervisorDetails = await getSupervisorDetails(req.user)
    res.status(200).send({done:true,data:supervisorDetails})
  } catch (e) {
    console.log("Error", e)
  }
}


export const getUsesForTranDocsModal = async( req, res) => {

  // get the ID from the params
  // get the participant list
  // get the primary email IDs
  // get the list of tenant users that are not the same as the participant list
  // send the response back to the users

  try {

    const { tranId } = req.params
    let allParticipants = []

    const dealParticipants = await Deals.aggregate([
      { $match:{_id:ObjectID(tranId)}},
      {
        $project: {
          participantIds: {$setUnion:[
            "$dealIssueTranAssignedTo",
            "$dealIssueParticipants.dealPartContactId",
            ["$createdByUser"]
          ]},
          participantEntities: {$setUnion:[
            "$dealIssueUnderwriters.dealPartFirmId",
            "$dealIssueParticipants.dealPartFirmId",
            ["$dealIssueTranClientId","$dealIssueTranIssuerId"]
          ]}

        }
      },
      ...commonQuerySections
    ])

    allParticipants = [...allParticipants, ...dealParticipants ]

    if (allParticipants.length === 0)
    {
      const otherParticipants = await Others.aggregate([
        { $match:{_id:ObjectID(tranId)}},
        {
          $project: {
            participantIds: {$setUnion:[
              "$participants.partContactId",
              ["$createdByUser","$actTranFirmLeadAdvisorId"]
            ]},
            participantEntities: {$setUnion:[
              "$actTranUnderwriters.partFirmId",
              "$participants.partFirmId",
              ["$actTranClientId","$actTranFirmId"]
            ]}
          }
        },
        ...commonQuerySections
      ])
      allParticipants = [...allParticipants, ...otherParticipants ]
    }


    if (allParticipants.length === 0)
    {
      const maRfpParticipants = await ActMaRFP.aggregate([
        { $match:{_id:ObjectID(tranId)}},
        {
          $project: {
            participantIds: {$setUnion:[
              ["$actLeadFinAdvClientEntityId"],
              "$maRfpParticipants.dealPartContactId",
              ["$createdByUser"]
            ]},
            participantEntities: {$setUnion:[
              "$maRfpParticipants.partFirmId",
              ["$actTranFirmId","$actIssuerClient"]
            ]}

          }
        },
        ...commonQuerySections
      ])
      allParticipants = [...allParticipants, ...maRfpParticipants ]
    }

    if (allParticipants.length === 0)
    {
      const derivativesParticipants = await Derivatives.aggregate([
        { $match:{_id:ObjectID(tranId)}},
        {
          $project: {
            participantIds: {$setUnion:[
              ["$actTranFirmLeadAdvisorId"],
              "$derivativeParticipants.partContactId",
              ["$createdByUser"]
            ]},
            participantEntities: {$setUnion:[
              "$derivativeParticipants.partFirmId",
              ["$actTranFirmId","$actTranClientId"]
            ]}

          }
        },
        ...commonQuerySections
      ])

      allParticipants = [...allParticipants, ...derivativesParticipants ]
    }

    if (allParticipants.length === 0)
    {
      const bankLoanParticipants = await BankLoans.aggregate([
        { $match:{_id:ObjectID(tranId)}},
        {
          $project: {
            participantIds: {$setUnion:[
              ["$actTranFirmLeadAdvisorId"],
              "$bankLoanParticipants.partContactId",
              ["$createdByUser"]
            ]},
            participantEntities: {$setUnion:[
              "$bankLoanParticipants.partFirmId",
              ["$actTranFirmId","$actTranClientId"]
            ]}
          }
        },
        ...commonQuerySections
      ])

      allParticipants = [...allParticipants, ...bankLoanParticipants ]
    }

    if (allParticipants.length === 0)
    {
      const rfpParticipants = await RFP.aggregate([
        { $match:{_id:ObjectID(tranId)}},
        {
          $project: {
            participantIds: {$setUnion:[
              "$rfpTranAssignedTo",
              "$rfpEvaluationTeam.rfpSelEvalContactId",
              "$rfpProcessContacts.rfpProcessContactId",
              "$rfpParticipants.rfpParticipantContactId",
              ["$createdByUser"]
            ]},
            participantEntities: {$setUnion:[
              "$rfpEvaluationTeam.rfpSelEvalFirmId",
              "$rfpProcessContacts.rfpProcessFirmId",
              "$rfpParticipants.rfpParticipantFirmId",
              ["$rfpTranClientId","$rfpTranIssuerId"]
            ]}

          }
        },
        ...commonQuerySections
      ])
      allParticipants = [...allParticipants, ...rfpParticipants ]
    }

    if (allParticipants.length === 0)
    {
      const busDevParticipants = await ActBusDev.aggregate([
        { $match:{_id:ObjectID(tranId)}},
        {
          $project: {
            participantIds: {$setUnion:[
              "$participants.partContactId",
              ["$createdByUser","$actLeadFinAdvClientEntityId"]
            ]},
            participantEntities: {$setUnion:[
              "$participants.partFirmId",
              ["$actTranFirmId","$actIssuerClient"]
            ]}

          }
        },
        ...commonQuerySections
      ])
      allParticipants = [...allParticipants, ...busDevParticipants ]
    }

    const flattenedEntities = allParticipants.reduce ( (acc, k) => {
      const { participantEntities } = k
      const stringifiedParticipants = participantEntities.map( aa => {
        if (aa) {
          return aa.toString()
        }
        return ObjectID()
      })
      return [ ...new Set([...acc, ...stringifiedParticipants])]
    },[])


    const allEntities = flattenedEntities.map( a => ObjectID(a) )
    const allParticipantUsers = allParticipants.map( a => ObjectID(a.userId) )

    // get the distinct set of users who are not in this list
    let allUserDetails = []

    if ( allParticipants.length > 0)
    {
      allUserDetails = await EntityUser.aggregate([
        {
          $match:{
            "$and":
            [
              {entityId:{"$in":[...allEntities]}},
              {_id:{"$nin":[...allParticipantUsers]}}
            ]
          }
        },
        {
          "$unwind":"$userEmails"
        },
        {
          $match:{"userEmails.emailPrimary":true}
        },
        {
          $project:{
            "userId":"$_id",
            "userFirstName":1,
            "userLastName":1,
            "entityId":1,
            "sendEmailTo":"$userEmails.emailId",
            "userFirmName":1
          }
        }
      ])
    }
    res.status(200).send({participants:allParticipants, flattenedEntities, allparticipants:allUserDetails, error:null,message:"successfully fetched users for doc modal"})
  } catch (e) {
    console.log("There is an Error in getting users for document service", e)
    res.status(500).send({participants:[], allparticipants:[], error:"Unable to get users for document service",message:""})
  }

}

export const getAllTenantUserDetailsByUser = async (user) => {

  const information = await EntityUser.aggregate([
    {
      $match:{_id:ObjectID(user._id)}
    },
    {
      "$lookup":{
        from: "entityrels",
        localField: "entityId",
        foreignField: "entityParty2",
        as: "tenantgraph"
      },
    },
    {
      $unwind:"$tenantgraph"
    },
    {
      $project:{
        tenantId:"$tenantgraph.entityParty1",
      }
    },
    {
      "$lookup":{
        from: "entityrels",
        localField: "tenantId",
        foreignField: "entityParty1",
        as: "alltenantentities"
      },
    },
    {
      $unwind:"$alltenantentities"
    },
    {
      $project:{
        tenantId:1,
        relationshipType:"$alltenantentities.relationshipType",
        entityId:"$alltenantentities.entityParty2"
      }
    },
    {
      "$lookup":{
        from: "entities",
        localField: "entityId",
        foreignField: "_id",
        as: "allentitydetails"
      },
    },
    {
      $unwind:"$allentitydetails"
    },
    {
      $project:{
        tenantId:1,
        relationshipType:1,
        entityId:"$allentitydetails._id",
        entityName:"$allentitydetails.firmName"
      }
    },
    {
      "$lookup":{
        from: "entityusers",
        localField: "entityId",
        foreignField: "entityId",
        as: "allusers"
      },
    },
    {
      $unwind:"$allusers"
    },
    {
      $project:{
        _id:"$allusers._id",
        "tenantId":1,
        "entityId":1,
        "relationshipType":1,
        "entityName":1,
        "userId":"$allusers._id",
        "userFirstName":"$allusers.userFirstName",
        "userLastName":"$allusers.userLastName",
        "sendEmailTo":"$allusers.userLoginCredentials.userEmailId"
      }
    }
  ])

  return information

}

export const getAllUsersForAllFirmsAcrossTenant = async (req, res) => {

  // get the distinct set of users who are not in this list
  try {
    const allTenantUserDetails = await EntityUser.aggregate([
      {
        $match:{_id:ObjectID(req.user._id)}
      },
      {
        "$lookup":{
          from: "entityrels",
          localField: "entityId",
          foreignField: "entityParty2",
          as: "tenantgraph"
        },
      },
      {
        $unwind:"$tenantgraph"
      },
      {
        $project:{
          tenantId:"$tenantgraph.entityParty1",
        }
      },
      {
        "$lookup":{
          from: "entityrels",
          localField: "tenantId",
          foreignField: "entityParty1",
          as: "alltenantentities"
        },
      },
      {
        $unwind:"$alltenantentities"
      },
      {
        $project:{
          tenantId:1,
          relationshipType:"$alltenantentities.relationshipType",
          entityId:"$alltenantentities.entityParty2"
        }
      },
      {
        "$lookup":{
          from: "entities",
          localField: "entityId",
          foreignField: "_id",
          as: "allentitydetails"
        },
      },
      {
        $unwind:"$allentitydetails"
      },
      {
        $project:{
          tenantId:1,
          relationshipType:1,
          entityId:"$allentitydetails._id",
          entityName:"$allentitydetails.firmName"
        }
      },
      {
        "$lookup":{
          from: "entityusers",
          localField: "entityId",
          foreignField: "entityId",
          as: "allusers"
        },
      },
      {
        $unwind:"$allusers"
      },
      {
        $project:{
          _id:"$allusers._id",
          "tenantId":1,
          "entityId":1,
          "relationshipType":1,
          "entityName":1,
          "userId":"$allusers._id",
          "userFirstName":"$allusers.userFirstName",
          "userLastName":"$allusers.userLastName",
          "sendEmailTo":"$allusers.userLoginCredentials.userEmailId"
        }
      },
    ])
    res.status(200).send({alltenantusers:allTenantUserDetails, error:null,message:"successfully fetched users for tenant"})
  } catch (e) {
    console.log("There is an Error in getting users for document service", e)
    res.status(500).send({alltenantusers:[], error:"Unable to get all users for tenant",message:""})
  }
}

export const getAllEntitiesUnderTenant = async ( req, res) => {

  try {
    const entityDetails = await getAllEntitiesForTenant(req.user)
    res.status(200).send({done:true, entities:entityDetails})
  } catch(e) {
    console.log(e)
    res.status(500).send({done:false, entities:[], error:e})
  }

}

export const fetchAllUsersAcrossAllTenants = async ( req, res) => {

  try {
    const allEntityDetails = await getAllUsersAcrossAllTenants(req.user)
    res.status(200).send({done:true, userDetails:allEntityDetails})
  } catch(e) {
    console.log(e)
    res.status(500).send({done:true, userDetails:[], error:e})
  }

}

export const fetchAllEligibleTransactionsForLoggedInUser = async ( req, res) => {

  try {
    const {entityId} = req.query
    const allTransactionDetails = await getTransactionInformationForLoggedInUser(req.user, entityId)
    res.status(200).send({done:true, ...allTransactionDetails})
  } catch(e) {
    console.log(e)
    res.status(500).send({done:false, status:"error",message:"Failed to get Information", data:[]})
  }

}

export const fetchUrlsForSpecificIDs = async (req,res) => {
  console.log("THIS IS THE REQUEST BODY", req.body, req.user)
  const {ids} = req.body

  if ( !req.user ) {
    res.status(500).send({done:false, urlObject:{}})
  }

  try {
    const urlObject = await urlGenerator(req.user, ids)
    res.status(200).send({done:true, urlObject})
  } catch (e) {
    console.log("There is a problem in fetching URLs for IDs",e)
    res.status(500).send({done:false, urlObject:{}})
  }
}

export const fetchAllUrlsForTenant = async (req,res) => {
  console.log("THIS IS THE REQUEST BODY")

  if ( !req.user )
  {
    res.status(500).send({done:false, urlObject:{}})
  }

  const {tenantId} = req.params
  try {
    const urlObject = await tenantUrlGenerator(tenantId)
    res.status(200).send({done:true, urlObject})
  } catch (e) {
    console.log("There is a problem in fetching all URLs for the tenant",e)
    res.status(500).send({done:false, urlObject:{}})
  }
}

export const postGeneralDocuments = async (req,res) => {
  if ( !req.user )
  {
    res.status(500).send({done:false, urlObject:{}})
  }

  const {type, documents} = req.body
  const cmnField = {
    markedPublic: {
      publicFlag: false,
      publicDate: new Date().toString()
    },
    sentToEmma: {
      emmaSendFlag: false,
      emmaSendDate: new Date().toString()
    },
    createdBy: req.user._id,
    createdUserName: `${req.user.userFirstName} ${req.user.userLastName}`
  }

  try {
    if(type){
      if(type === "firm") {

        Object.keys(documents).forEach(async key => {

          const newDocuments = []

          documents[key].forEach(doc => {
            newDocuments.push({ ...cmnField, ...doc})
          })

          await Entity.updateOne({ _id: key || ""}, {$addToSet: {entityDocuments: newDocuments} })
        })

        res.status(200).send({done:true, message:"documents inserted or updated successfully"})

      }else if(type === "individual") {

        Object.keys(documents).forEach(async key => {

          const newDocuments = []

          documents[key].forEach(doc => {
            newDocuments.push({ ...cmnField, ...doc})
          })

          await EntityUser.updateOne({ _id: key || ""}, {$addToSet: {userDocuments: newDocuments} })
        })

        res.status(200).send({done:true, message:"documents inserted or updated successfully"})

      }else if(type === "transaction") {

        Object.keys(documents).forEach(async docKey => {

          const newDocuments = []
          const projectType = documents[docKey] && documents[docKey].length ? documents[docKey][0].type : ""
          documents[docKey].forEach(doc => {
            newDocuments.push({ ...cmnField, ...doc})
          })

          const key = projectType === "Deals" ? "dealIssueDocuments" : projectType === "ActMaRFP" ? "maRfpDocuments" : projectType === "Derivatives" ? "derivativeDocuments"
            : projectType === "BankLoans" ? "bankLoanDocuments" :  projectType === "RFP" ? "rfpBidDocuments" : projectType === "Others" ? "actTranDocuments" : ""

          const modal = projectType === "Deals" ? Deals : projectType === "ActMaRFP" ? ActMaRFP : projectType === "Derivatives" ? Derivatives
            : projectType === "BankLoans" ? BankLoans :  projectType === "RFP" ? RFP : projectType === "Others" ? Others : ""

          await modal.updateOne(
            { _id: ObjectID(docKey || "")},
            { $addToSet: { [key] : newDocuments } }
          )

        })

        res.status(200).send({done:true, message:"documents inserted or updated successfully"})

      }else {
        res.status(500).send({done:false, message:"type should be firm, individual and transaction"})
      }
    }else {
      res.status(500).send({done:false, message:"please pass the type, type should not be blank"})
    }
  } catch (e) {
    console.log("There is a problem in fetching all URLs for the tenant",e)
    res.status(500).send({done:false, urlObject:{}})
  }
}

export const postCheckedAddToDL = async (req,res) => {
  try {
    const { type, tranId} = req.params
    const { checkId } = req.params
    const {_id, addToDL} = req.body
    const tranTypes = ["Deals","RFP","ActMaRFP","Derivatives","BankLoans","Others"]
    if(tranTypes.indexOf(type) !== -1 && _id) {

      const key = type === "Deals" ? "dealIssueParticipants" : type === "ActMaRFP" ? "maRfpParticipants" : type === "Derivatives" ? "derivativeParticipants"
        : type === "BankLoans" ? "bankLoanParticipants" :  type === "RFP" ? "rfpParticipants" : type === "Others" ? "participants" : ""

      const query1 = `${key}._id`

      const query2 = `${key}.$.addToDL`

      const modal = type === "Deals" ? Deals : type === "ActMaRFP" ? ActMaRFP : type === "Derivatives" ? Derivatives
        : type === "BankLoans" ? BankLoans :  type === "RFP" ? RFP : type === "Others" ? Others : ""

      await modal.updateOne(
        { _id: ObjectID(tranId), [query1]: ObjectID(_id)},
        { $set: { [query2] : addToDL } }
      )

      const participant = await modal.findOne({_id: tranId}).select(key)
      let updateParticipant = null
      if(participant) {
        updateParticipant = participant[key].find(p => p._id.toString() === _id)
      }
      res.status(200).json({done: true, updateParticipant})
    }else {
      res.status(200).send({done:true, message:"check item or id not found"})
    }
  } catch (e) {
    console.log("There is a problem in fetching all URLs for the tenant",e)
    res.status(500).send({done:false, urlObject:{}})
  }
}

export const getFirmDetailsForUsers = async (req, res) => {
  const { userIds } = req.body
  if(!userIds || !userIds.length) {
    return res.status(422).send({ error: "No userIds provided" })
  }
  try {
    const users = await EntityUser.find({ _id: {$in: userIds }}).select({ entityId: 1, userFirmName:1 })
    res.json(users)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const getTransactions = async (req, res) => {
  const resRequested = [
    {resource:"RFP",access:1},
    {resource:"EntityUser",access:1},
    {resource:"Deals",access:1},
  ]
  try {
    const {type} = req.params
    const {searchTerm, entityId, tranId} = req.query
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchTerm.replace(invalid, "")
    const regexValue = new RegExp(newSearchString || "", "i")
    const entitled = await canUserPerformAction(req.user,resRequested)
    const detailsIds = await getTransactionInformationForLoggedInUser(req.user, entityId)
    const view = (detailsIds && detailsIds.data && detailsIds.data.map(tran => tran.tranId.toString())) || []
    if( !entitled) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    const rfpSelect = "rfpTranIssueName rfpTranIssueName rfpTranProjectDescription rfpTranIssuerId rfpTranIssuerFirmName rfpTranClientId created_at rfpTranIssuerId rfpTranType rfpTranClientFirmName rfpTranSubType rfpTranIssuerFirmName"

    const dealSelect = "dealIssueTranIssueName dealIssueTranProjectDescription dealIssueTranClientId dealIssueTranClientFirmName dealIssueTranIssuerFirmName dealIssueTranIssuerId dealIssueTranType dealIssueTranSubType created_at"

    const loanSelect = "actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranFirmName actTranClientId actTranClientName actTranIssueName actTranClientName actTranProjectDescription"

    const derivativeSelect = "actTranFirmId actTranSubType actTranUniqIdentifier actTranClientId actTranType actTranClientName actTranIssueName actTranProjectDescription actTranClientName"

    const maRfpSelect = "actTranFirmId actType actSubType actUniqIdentifier actLeadFinAdvClientEntityName actIssuerClient actIssuerClientEntityName actIssueName actProjectName actIssuer actIssuerClientEntityName"

    const otherSelect = "actTranType actTranSubType actTranUniqIdentifier actTranFirmId actTranFirmName actTranClientId actTranClientName actTranIssueName actTranProjectDescription actTranClientName"

    const busDevSelect = "actTranFirmId actType actSubType actUniqIdentifier actIssuerClient actIssuerClientEntityName actIssueName actProjectName actIssuer actIssuerClientEntityName"


    const select = type === "Deals" ? dealSelect : type === "ActMaRFP" ? maRfpSelect : type === "Derivatives" ? derivativeSelect
      : type === "BankLoans" ? loanSelect : type === "RFP" ? rfpSelect : type === "Others" ? otherSelect : type === "ActBusDev" ? busDevSelect : ""

    const searchField = type === "Deals" ? "dealIssueTranProjectDescription" : type === "ActMaRFP" ? "actProjectName" : type === "Derivatives" ? "actTranProjectDescription"
      : type === "BankLoans" ? "actTranProjectDescription" : type === "RFP" ? "rfpTranProjectDescription" : type === "Others" ? "actTranProjectDescription" : type === "ActBusDev" ? "actProjectName" : ""

    const modal = type === "Deals" ? Deals : type === "ActMaRFP" ? ActMaRFP : type === "Derivatives" ? Derivatives
      : type === "BankLoans" ? BankLoans : type === "RFP" ? RFP : type === "Others" ? Others : type === "ActBusDev" ? ActBusDev :""

    const selectedField = {}
    select.split(" ").forEach(t=>{
      selectedField[t] = 1
    })
    if(tranId){
      const tranIndex = view.indexOf(tranId.toString())
      if(tranIndex !== -1) {
        view.splice(tranIndex, 1)
      }
    }
    const query = [
      { $match: { _id:  {$in : view.map(id => ObjectID(id)) } } },
      {
        $addFields: {
          keySearchDetails: {
            $concat: [
              { $ifNull: [`$${searchField}`, ""] },
            ]
          }
        }
      },
      {
        $match: {
          $or: [{ keySearchDetails: { $regex: regexValue, $options: "i" } }]
        }
      },
      {
        $project: selectedField
      },
    ]

    if(!searchTerm){
      query.push({ $limit : 5 })
    }

    let transactions = await modal.aggregate(query)

    transactions = transactions.map(r => ({
      activityId: r._id,
      activityDescription: r.rfpTranProjectDescription || r.dealIssueTranProjectDescription || r.actTranProjectDescription || r.actProjectName || "",
      activityTranType: r.rfpTranType|| r.dealIssueTranType || r.actTranType || r.actType || "",
      activityTranSubType: r.rfpTranSubType || r.dealIssueTranSubType || r.actTranSubType || r.actSubType || "" ,
      activityTranFirmId: r.rfpTranClientId || r.dealIssueTranClientId || r.actTranFirmId || r.actTranFirmId || "",
      activityTranFirmName: r.rfpTranClientFirmName || r.dealIssueTranClientFirmName || r.actTranFirmName || r.actTranFirmName,
      activityTranClientId: r.rfpTranIssuerId || r.dealIssueTranIssuerId || r.actTranClientId || r.actIssuerClient || "",
      activityClientName: r.rfpTranIssuerFirmName || r.dealIssueTranIssuerFirmName || r.actTranClientName || r.actIssuerClientEntityName || "",
      activityIssueName: r.rfpTranIssueName || r.dealIssueTranIssueName || r.actTranIssueName || r.actIssueName || "",
    }))

    res.json(transactions)
  } catch(error) {
    console.log("======", error)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

export const pullRelatedTransaction = async (req, res) => {
  const { type } = req.params
  const { tranId, relatedId, relTranId, relatedType, relType } = req.query
  const resRequested = [
    {resource:"RFP",access:1},
    {resource:"Deals",access:1},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    if( !entitled) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    const tranTypes = ["Deals", "RFP", "ActMaRFP", "Derivatives", "BankLoans", "Others"]

    if (tranTypes.indexOf(type) !== -1 && tranId && relatedId) {

      const key = type === "Deals" ? "dealIssueTranRelatedTo" : type === "ActMaRFP" ? "actRelTrans" : type === "Derivatives" ? "actTranRelatedTo"
        : type === "BankLoans" ? "actTranRelatedTo" : type === "RFP" ? "rfpTranRelatedTo" : type === "Others" ? "actTranRelatedTo" : ""

      const relatedKey =
        relatedType === "Bond Issue" ? "dealIssueTranRelatedTo" :
          (relatedType === "Bank Loan" || relatedType === "Others" || relatedType === "Derivative") ? "actTranRelatedTo" :
            (relatedType === "MA-RFP" || relatedType === "BusinessDevelopment") ?  "actRelTrans" :
              relatedType === "RFP" ? "rfpTranRelatedTo" : ""

      const modal = type === "Deals" ? Deals : type === "ActMaRFP" ? ActMaRFP : type === "Derivatives" ? Derivatives
        : type === "BankLoans" ? BankLoans : type === "RFP" ? RFP : type === "Others" ? Others : ""

      const relatedModel = relatedType === "Bond Issue" ? Deals : relatedType === "MA-RFP" ? ActMaRFP : relatedType === "Derivative" ? Derivatives
        : relatedType === "Bank Loan" ? BankLoans : relatedType === "RFP" ? RFP : relatedType === "Others" ? Others : relatedType === "BusinessDevelopment" ? ActBusDev : ""

      await modal.update({ _id: tranId }, { $pull: { [key]: { _id: relatedId } } })

      await relatedModel.update({ _id: relTranId }, { $pull: { [relatedKey]: { relTranId: tranId, relType } } })

      const relatedTransaction = await modal.findById(tranId).select(key)

      res.status(200).json({relatedTransaction: (relatedTransaction && relatedTransaction[key] || []), done: true})
    } else {

      res.status(422).send({ done: false, error: "This type type of transaction doesn't exists." })

    }
  } catch (err) {
    console.log("*********pullRelatedTransaction***********",err)
    return res.status(422).send({ done: false, error: "Error in getting details", err })
  }
}

export const  RelatedToAssign = async (tranDetail, actTranRelatedTo, tranType, single) => {

  const Deal = tranType === "Bond Issue"
  const BankLoan = tranType === "Bank Loan"
  const Derivative = tranType === "Derivative"
  const Other = tranType === "Others"
  const RFPS = tranType === "RFP"
  const MARFPS = tranType === "MA-RFP"
  const BusDev = tranType === "BusinessDevelopment"

  const issueName =
    Deal ? "dealIssueTranIssueName" :
      ( BankLoan || Derivative || Other ) ? "actTranIssueName" :
        RFPS ? "rfpTranIssueName" :
          ( MARFPS || BusDev ) ? "actIssueName" : ""

  const projectDescription =
    Deal ? "dealIssueTranProjectDescription" :
      ( BankLoan || Derivative || Other ) ? "actTranProjectDescription" :
        RFPS ? "rfpTranProjectDescription" :
          ( MARFPS || BusDev ) ? "actProjectName" : ""

  const clientId =
    Deal ? "dealIssueTranIssuerId" :
      ( BankLoan || Derivative || Other ) ? "actTranClientId" :
        RFPS ? "rfpTranIssuerId" :
          ( MARFPS || BusDev ) ? "actIssuerClient" : ""

  const clientName =
    Deal ? "dealIssueTranIssuerFirmName" :
      ( BankLoan || Derivative || Other ) ? "actTranClientName" :
        RFPS ? "rfpTranIssuerFirmName" :
          ( MARFPS || BusDev ) ? "actIssuerClientEntityName" : ""

  if(single && single === "multi"){
    for (const i in actTranRelatedTo) {

      const deal = actTranRelatedTo[i].tranType === "Bond Issue"
      const bankLoan = (actTranRelatedTo[i].tranType === "Bank Loan" || actTranRelatedTo[i].tranType === "Lines and Letter" || actTranRelatedTo[i].tranType === "Letter Of Credit" || actTranRelatedTo[i].tranType === "Lines Of Credit")
      const derivative = actTranRelatedTo[i].tranType === "Derivative"
      const rfp = actTranRelatedTo[i].tranType === "RFP"
      const others = actTranRelatedTo[i].tranType === "Others"
      const businessDev = actTranRelatedTo[i].tranType === "BusinessDevelopment"
      const marfp = actTranRelatedTo[i].tranType === "MA-RFP"

      const modal =
        deal ? Deals :
          bankLoan ? BankLoans :
            derivative ? Derivatives :
              rfp ? RFP :
                others ? Others :
                  businessDev ? ActBusDev :
                    marfp ? ActMaRFP : ""

      const relatedName =
        deal ? "dealIssueTranRelatedTo" :
          (others || derivative || bankLoan) ? "actTranRelatedTo" :
            (marfp || businessDev) ?  "actRelTrans" :
              rfp ? "rfpTranRelatedTo" : ""

      const payload = {
        relTranId: tranDetail && tranDetail._id || "",
        relTranIssueName: tranDetail && tranDetail[issueName] || "",
        relTranProjectName: tranDetail && tranDetail[projectDescription] || "",
        relTranClientId: tranDetail && tranDetail[clientId] || "",
        relTranClientName: tranDetail && tranDetail[clientName] || "",
        relType: actTranRelatedTo[i] && actTranRelatedTo[i].relType || "",
        tranType,
      }

      if(payload && payload.tranType){
        await modal.updateOne({_id: ObjectID(actTranRelatedTo[i].relTranId)}, {$addToSet: {[relatedName]: payload}})
      }
    }
  } else {
    const deal = actTranRelatedTo.tranType === "Bond Issue"
    const bankLoan = (actTranRelatedTo.tranType === "Bank Loan" || actTranRelatedTo.tranType === "Lines and Letter" || actTranRelatedTo.tranType === "Letter Of Credit" || actTranRelatedTo.tranType === "Lines Of Credit")
    const derivative = actTranRelatedTo.tranType === "Derivative"
    const rfp = actTranRelatedTo.tranType === "RFP"
    const others = actTranRelatedTo.tranType === "Others"
    const businessDev = actTranRelatedTo.tranType === "BusinessDevelopment"
    const marfp = actTranRelatedTo.tranType === "MA-RFP"

    const modal =
      deal ? Deals :
        bankLoan ? BankLoans :
          derivative ? Derivatives :
            rfp ? RFP :
              others ? Others :
                businessDev ? ActBusDev :
                  marfp ? ActMaRFP : ""

    const relatedName =
      deal ? "dealIssueTranRelatedTo" :
        (others || derivative || bankLoan) ? "actTranRelatedTo" :
          (marfp || businessDev) ?  "actRelTrans" :
            rfp ? "rfpTranRelatedTo" : ""

    const payload = {
      relTranId: tranDetail && tranDetail._id || "",
      relTranIssueName: tranDetail && tranDetail[issueName] || "",
      relTranProjectName: tranDetail && tranDetail[projectDescription] || "",
      relTranClientId: tranDetail && tranDetail[clientId] || "",
      relTranClientName: tranDetail && tranDetail[clientName] || "",
      relType: actTranRelatedTo && actTranRelatedTo.relType || "",
      tranType,
    }
    if(payload && payload.tranType){
      await modal.updateOne({_id: ObjectID(actTranRelatedTo.relTranId)}, {$addToSet: {[relatedName]: payload}})
    }
  }
}

const generateExcel = (jsonData, callback) => {
  try {

    const workbook = { Sheets: {}, SheetNames: [] }
    jsonData.forEach(e => {
      let { name, data, headers } = e
      name = name.replace(/[^\w\s]/gi, " ").substring(0, 31)
      workbook.Sheets[name] = XLSX.utils.json_to_sheet(data, {header: headers})
      workbook.SheetNames.push(name)
    })
    const excelBuffer = XLSX.write(workbook, { bookType: "xlsx", type: "buffer" })
    callback(excelBuffer)
  }
  catch(err) {
    console.log("There is an Error generating excel file")
    throw(err)
  }
}

export const fetchJSONToBlobData = ( req, res ) => {

  const { dataArray } = req.body

  if(!dataArray || !dataArray.length) {
    return res.status(422).send({ error: "No data provided" })
  }

  try {
    let fileName = "testfile"
    fileName = `${encodeURIComponent(fileName)}.xlsx`
    console.log("I entered the excel generation service", fileName)
    generateExcel(dataArray, (response) => {
      res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
      res.setHeader("Content-disposition", `attachment; filename="${fileName}"`)
      console.log("Before the Response")
      res.send(response)
    })
  } catch (err) {
    console.log(err)
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const checkExcelFiles = ( req, res ) => {

  const { data, jsonSheet }  = req.body

  try {
    const excelData = []
    const workbook = XLSX.read(data, {
      type: "binary",
      cellStyles: true
    })

    workbook.SheetNames.forEach(sheetName => {

      const xlsxRowObject = XLSX.utils.sheet_to_row_object_array(
        workbook.Sheets[sheetName]
      )

      for (let i = 0; i < xlsxRowObject.length; i++) {
        excelData.push(xlsxRowObject[i]["your column name"])
      }
      const jsonObject = JSON.stringify(xlsxRowObject)

      if (jsonObject && JSON.parse(jsonObject)) {
        let parseJson = JSON.parse(jsonObject)

        parseJson = parseJson.map(obj => {
          if (obj.callDate) {
            obj.callDate = typeof obj.callDate === "number" ? XLSX.SSF.format("m/d/yy", obj.callDate) : obj.callDate
          }
          if (obj.maturityDate) {
            obj.maturityDate = typeof obj.maturityDate === "number" ? XLSX.SSF.format("m/d/yy", obj.maturityDate) : obj.maturityDate
          }
          return obj
        })

        const jsonKeys = Object.keys(parseJson[0])
        const sampleKeys = Object.keys(jsonSheet[0])
        const validKey = []
        if(sampleKeys && sampleKeys.length){
          sampleKeys.forEach(key => {
            if(jsonKeys.indexOf(key) !== -1){
              validKey.push(key)
            }
          })
        }

        if((sampleKeys && sampleKeys.length === validKey.length) || !sampleKeys.length){
          res.send(parseJson)
        }else {
          return res.send("Invalid data format")
        }
      }
    })

  } catch (err) {
    console.log(err)
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const removeWhiteSpaces = value => {
  if (typeof value === "string") {
    return value.trim().replace(/\s+/g, " ") || ""
  }
  return value
}

export const readDMExcel = ( req, res ) => {

  const { bstr, hasHeaders } = req.body

  try {

    const wb = XLSX.read(bstr, { type: "binary", cellStyles: true })

    const wsname = wb.SheetNames[0]
    const ws = wb.Sheets[wsname]
    let columnLength = 0
    if (!hasHeaders) {
      columnLength = XLSX.utils.sheet_to_json(ws, { header: 1, defval: "" })[0].length
    }
    const gridData = XLSX.utils.sheet_to_json(ws, { header: columnLength ? [...Array(columnLength)].map((x, i) => `Column ${i}`) : 0, defval: "" })

    Object.keys(gridData).forEach((rowKey) => {
      const rowData = gridData[rowKey]
      Object.keys(rowData).forEach(key => {
        if(key && key.toLowerCase().includes("date") && typeof rowData[key] === "number"){
          rowData[key] = XLSX.SSF.format("mm/dd/yyyy", rowData[key])
        }else {
          rowData[key] = removeWhiteSpaces(rowData && typeof rowData[key] === "string" ? rowData[key].trim() : rowData[key] || "")
        }
      })
    })

    res.send(gridData)
  } catch (err) {
    console.log(err)
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const makeCopyOfTransaction = async ( req, res ) => {
  const { user } = req
  const { tranId, tranType, tranSubType, issueName, projectName, tabs } = req.body
  tabs.push("default")
  try {
    if(!tranId){
      res.status(422).send({ done: false, error: "Please pass transaction id in body" })
    }

    const transactionType =
      tranSubType === "BOND ISSUE" ? "BOND ISSUE" :
        tranSubType === "BANK LOAN" ? "BANK LOAN" :
          tranType === "DERIVATIVE" ? "DERIVATIVE" :
            tranType === "RFP" ? "RFP" :
              tranType === "OTHERS" ? "OTHERS" :
                tranType === "BUSINESSDEVELOPMENT" ? "BUSINESSDEVELOPMENT" :
                  tranType === "MA-RFP" ? "MA-RFP" : "BANK LOAN"

    const Model =
      tranSubType === "BOND ISSUE" ? Deals :
        tranSubType === "BANK LOAN" ? BankLoans :
          tranType === "DERIVATIVE" ? Derivatives :
            tranType === "RFP" ? RFP :
              tranType === "OTHERS" ? Others :
                tranType === "BUSINESSDEVELOPMENT" ? ActBusDev :
                  tranType === "MA-RFP" ? ActMaRFP : BankLoans

    const copyFrom = await Model.findById(tranId)

    console.log("*********Transaction Type*********", transactionType)
    let newTran = {
      createdByUser: user._id
    }
    if(transactionType === "BOND ISSUE"){
      newTran.dealIssueTranIssueName = issueName
      newTran.dealIssueTranProjectDescription = projectName
      newTran.dealIssueTranStatus = "Pre-Pricing"
    }
    if(transactionType === "BANK LOAN" || transactionType === "DERIVATIVE" || transactionType === "OTHERS"){
      newTran.actTranIssueName = issueName
      newTran.actTranProjectDescription = projectName
      newTran.actTranStatus = "Pre-Pricing"
    }
    if(transactionType === "BUSINESSDEVELOPMENT" || transactionType === "MA-RFP" ){
      newTran.actIssueName = issueName
      newTran.actProjectName = projectName
      newTran.actStatus = "Pre-Pricing"
    }

    if(copyFrom && tabs){
      tabs.forEach(tabName => {
        if(transactionsTabsKeys && transactionsTabsKeys[transactionType] && transactionsTabsKeys[transactionType][tabName]){
          transactionsTabsKeys[transactionType][tabName].forEach(key => {
            newTran = {
              ...newTran,
              [key]: copyFrom[key]
            }
          })
        }
      })
    }

    let newTransaction = new Model(newTran)
    newTransaction = await newTransaction.save()

    await Promise.all([
      updateEligibleIdsForLoggedInUserFirm(req.user),
      returnFromESObject[transactionType](req.user.tenantId, newTransaction._id)
    ])
    res.send({ done: true, message: "Transaction Created Successfully.", newTransaction})
  } catch (err) {
    console.log(err)
    res.status(422).send({ done: false, error: "Error in getting details", err })
  }
}

export const getTransactionStatus = async ( tranType, tranId ) => {
  const statusKey =
    tranType === "deals" ? "dealIssueTranStatus" :
      tranType === "bankloans" || tranType === "derivatives" || tranType === "others" ? "actTranStatus" :
        tranType === "rfp" ? "rfpTranStatus" :
          tranType === "busdev" ? "actStatus" :
            tranType === "marfps" ? "actStatus" : ""

  const Model =
    tranType === "deals" ? Deals :
      tranType === "bankloans" ? BankLoans :
        tranType === "derivatives" ? Derivatives :
          tranType === "rfp" ? RFP :
            tranType === "others" ? Others :
              tranType === "busdev" ? ActBusDev :
                tranType === "marfps" ? ActMaRFP : BankLoans

  const transaction = await Model.findOne({ _id: tranId }).select(statusKey)
  console.log("=========> transaction", transaction)
  return {
    tranId: (transaction && transaction._id) || "",
    tranStatus: (transaction && transaction[statusKey]) || ""
  }
}

export const checkClosedTransaction = async ( req, res, next ) => {
  try {
    const pathname =  req.originalUrl.split("/") || []
    const transactionId = req.query.tranId
    const transactionType = req.query.tranType
    const key = transactionType === "Deals"
      ? "deals"
      : transactionType === "ActMaRFP"
        ? "marfps"
        : transactionType === "Derivatives"
          ? "derivatives"
          : transactionType === "BankLoans"
            ? "bankloans"
            : transactionType === "RFP"
              ? "rfp"
              : transactionType === "Others"
                ? "others"
                : ""
    const {_id} = req.body
    const status = ["Closed"]
    const tranType = pathname[2]
    let tranId = pathname[pathname.length-1]
    tranId = tranId.split("?")
    tranId = (tranId && tranId[0].length === 24 && tranId[0]) || _id || transactionId || ""

    console.log("Service Path ", tranType, tranId)
    const transaction = tranId ? await getTransactionStatus(key || tranType, tranId) : {}
    if ( transaction && status.indexOf(transaction.tranStatus) === -1) {
      console.log("get transaction status", transaction)
      if(transaction.tranId){
        next()
      }else {
        return res.status(500).send({error:"Transaction not found"})
      }
    } else {
      return res.status(500).send({error:"This transaction was closed. so you can not update this transaction."})
    }
  } catch (err) {
    console.log(err)
    res.status(422).send({ done: false, error: "Error in getting details", err })
  }
}
