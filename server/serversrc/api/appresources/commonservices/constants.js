import ObjectID from "mongodb"
import {elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"
import  { Deals,RFP,ActMaRFP,Derivatives,BankLoans, Others,ActBusDev }  from "./../models"

export const URLMAPPER = {
  deals: ["deals", "summary"],
  rfps: ["rfp", "summary"],
  marfps: ["marfp", "summary"],
  derivatives: ["derivative", "summary"],
  bankloans: ["loan", "summary"],
  others: ["others", "summary"],
  busdevs: ["bus-development"],

  tenantusers: ["admin-users", "users"],
  clientusers: ["contacts", "cltprops"],
  prospectusers: ["contacts", "cltprops"],
  thirdpartyusers: ["contacts", "thirdparties"],
  migratedusers:["contacts", "migratedentities"],
  firms: ["admin-firms", "firms"],
  clients: ["clients-propects", "entity"],
  prospects: ["clients-propects", "entity"],
  thirdparties: ["thirdparties", "entity"],
  migratedentities:["migratedentities", "entity"],

}

export const ALLURLS = {
  deals: { l1: "deals", otherlevels: ["summary", "details", "participants", "pricing", "ratings", "documents", "check-track", "audit-trail"] },
  bankloans: { l1: "loan", otherlevels: ["summary", "details", "participants", "documents", "check-track", "audit-trail"] },
  marfps: { l1: "marfp", otherlevels: ["summary", "participants", "documents", "check-track", "audit-trail"] },
  others: { l1: "others", otherlevels: ["summary", "details", "participants", "pricing", "ratings", "documents", "check-track", "audit-trail"] },
  derivatives: { l1: "derivative", otherlevels: ["summary", "participants", "tradeSwap", "documents", "check-track", "audit-trail"] },
  rfps: { l1: "rfp", otherlevels: ["summary", "distribute", "manageresponse", "check-track", "audit-trail"] },
  busdevs: { l1: "bus-development", otherlevels: [] },

  tenantusers: { l1: "admin-users", otherlevels: ["users"] },
  clientusers: { l1: "contacts", otherlevels: ["cltprops"] },
  prospectusers: { l1: "contacts", otherlevels: ["cltprops"] },
  thirdpartyusers: { l1: "contacts", otherlevels: ["thirdparties"] },
  migratedusers: { l1: "contacts", otherlevels: ["migratedentities"] },
  firms: { l1: "admin-firms", "otherlevels": ["firms"] },
  clients: { l1: "clients-propects", "otherlevels": ["entity", "business-activity", "contacts", "contracts", "documents"] },
  prospects: { l1: "clients-propects", "otherlevels": ["entity", "business-activity", "contacts", "contracts", "documents"] },
  thirdparties: { l1: "thirdparties", "otherlevels": ["entity", "business-activity", "contacts", "contracts", "documents"] },
  migratedentities: { l1: "migratedentities", "otherlevels": ["entity", "business-activity", "contacts", "contracts", "documents"] },
}

export const passwordStrength =
  process.env.PASSWORD_STRENGTH === "" ? "false" : process.env.PASSWORD_STRENGTH

const pipelineLoggedInUserInfo = (user) => [
  {
    $match:{
      entityParty2:ObjectID(user.entityId)
    }
  },
  {
    $project:{
      tenantId:"$entityParty1",
      loggedInUserEntityId:"$entityParty2",
      relationshipType:"$relationshipType"
    }
  },
  {
    $lookup:{
      from: "entityusers",
      localField: "loggedInUserEntityId",
      foreignField: "entityId",
      as: "myFirmUsers"
    }
  },
  {
    $project:{
      tenantId:1,
      loggedInUserEntityId:1,
      relationshipType:1,
      loggedInFirmUserIds:"$myFirmUsers._id",
    }
  },
]

const pipelineGetDealIssueDetails = (user, alluserdetails) => [
  {
    $lookup:{
      from: "tranagencydeals",
      localField: "tenantId",
      foreignField: "dealIssueTranClientId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.dealIssueTranClientId",
      clientEntityId: "$trans.dealIssueTranIssuerId",
      tranUsers:{
        $setUnion:[
          "$trans.dealIssueParticipants.dealPartContactId",
          "$trans.dealIssueTranAssignedTo",
          ["$trans.createdByUser"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.dealIssueTranClientId","$trans.dealIssueTranIssuerId"],
          "$trans.dealIssueParticipants.dealPartFirmId",
          "$trans.dealIssueUnderwriters.dealPartFirmId"
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
          then:{$toUpper:"$trans.dealIssueTranProjectDescription"},
          else:{$toUpper:"$trans.dealIssueTranIssueName"}},
      },
      tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
      tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate",new Date(8640000000000000)]},
      tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate",new Date(8640000000000000)]},
      tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate",new Date(8640000000000000)]},
      tranPrimarySector:{$toUpper:"$trans.dealIssueTranPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.dealIssueTranSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.dealIssueTranStatus","-"]}},
      tranClientName:{ $toUpper: "$trans.dealIssueTranIssuerFirmName" },
      tranFirmName:{$toUpper:"$trans.dealIssueTranClientFirmName"},
      tranType:{$toUpper:"$trans.dealIssueTranType"},
      tranSubType:{$toUpper:"$trans.dealIssueTranSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      assignee:"$trans.dealIssueTranAssignedTo",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId",{$ifNull:["$trans.dealIssueTranAssignedTo",[]]}]}
          }},0]
      },
      allIds:{
        $setUnion:[
          ["$trans.dealIssueTranClientId","$trans.dealIssueTranIssuerId"],
          "$trans.dealIssueParticipants.dealPartFirmId",
          "$trans.dealIssueUnderwriters.dealPartFirmId",
          "$trans.dealIssueParticipants.dealPartContactId",
          "$trans.dealIssueTranAssignedTo",
          ["$trans.createdByUser"],
          ["$trans._id"]
        ]
      },
      displayCategory:"Transactions",
      createdAt :"$trans.createdAt"
    }
  }
]

export const pipelineGetManageRFPIssueDetails = (user, alluserdetails) => [
  {
    $lookup:{
      from: "tranagencyrfps",
      localField: "tenantId",
      foreignField: "rfpTranClientId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.rfpTranClientId",
      clientEntityId: "$trans.rfpTranIssuerId",
      tranUsers:{
        $setUnion:[
          "$trans.rfpProcessContacts.rfpProcessContactId",
          "$trans.rfpEvaluationTeam.rfpSelEvalContactId",
          "$trans.rfpParticipants.rfpParticipantContactId",
          "$trans.rfpTranAssignedTo",
          ["$trans.createdByUser"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
          "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
          "$trans.rfpProcessContacts.rfpProcessFirmId",
          "$trans.rfpParticipants.rfpParticipantFirmId",
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
          then:{$toUpper:"$trans.rfpTranProjectDescription"},
          else:{$toUpper:"$trans.rfpTranIssueName"}},
      },
      tranParAmount:"-",
      tranExpectedAwardDate:new Date(8640000000000000),
      tranActualAwardDate:new Date(8640000000000000),
      tranPricingDate:new Date(8640000000000000),
      tranPrimarySector:{$toUpper:"$trans.rfpTranPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.rfpTranSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.rfpTranStatus","-"]}},
      tranClientName:{ $toUpper: "$trans.rfpTranIssuerFirmName"},
      tranFirmName:{$toUpper:"$trans.rfpTranClientFirmName"},
      tranType:{$toUpper:"$trans.rfpTranType"},
      tranSubType:{$toUpper:"$trans.rfpTranSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId","$trans.rfpTranAssignedTo"]}
          }},0]
      },
      allIds:{
        $setUnion:[
          ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
          "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
          "$trans.rfpProcessContacts.rfpProcessFirmId",
          "$trans.rfpParticipants.rfpParticipantFirmId",
          "$trans.rfpTranAssignedTo",
          ["$trans.createdByUser"],
          ["$trans._id"]
        ]
      },
      displayCategory:"Projects",
      createdAt :"$trans.createdAt"
    }
  }
]

export const pipelineGetBankLoansDetails = (user, alluserdetails)=> [
  {
    $lookup:{
      from: "tranbankloans",
      localField: "tenantId",
      foreignField: "actTranFirmId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.actTranFirmId",
      clientEntityId: "$trans.actTranClientId",
      tranUsers:{
        $setUnion:[
          "$trans.bankLoanParticipants.partContactId",
          ["$trans.createdByUser","$trans.actTranFirmLeadAdvisorId"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actTranClientId"],
          "$trans.bankLoanParticipants.partFirmId",
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
          then:{$toUpper:"$trans.actTranProjectDescription"},
          else:{$toUpper:"$trans.actTranIssueName"}},
      },
      tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
      tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate",new Date(8640000000000000)]},
      tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate",new Date(8640000000000000)]},
      tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate",new Date(8640000000000000)]},
      tranPrimarySector:{$toUpper:"$trans.actTranPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.actTranSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.actTranStatus","-"]}},
      tranClientName:{ $toUpper: "$trans.actTranClientName"},
      tranFirmName:{$toUpper:"$trans.actTranFirmName"},
      tranType:{$toUpper:"$trans.actTranType"},
      tranSubType:{$toUpper:"$trans.actTranSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
          }},0]
      },

      allIds:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actTranClientId"],
          "$trans.bankLoanParticipants.partFirmId",
          "$trans.bankLoanParticipants.partContactId",
          ["$trans.actTranFirmLeadAdvisorId"],
          ["$trans.createdByUser"],
          ["$trans._id"]
        ]
      },
      displayCategory:"Transactions",
      createdAt :"$trans.createdAt"
    }
  }
]

export const pipelineGetDerivativesDetails = (user, alluserdetails)=> [
  {
    $lookup:{
      from: "tranderivatives",
      localField: "tenantId",
      foreignField: "actTranFirmId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.actTranFirmId",
      clientEntityId: "$trans.actTranClientId",
      tranUsers:{
        $setUnion:[
          "$trans.derivativeParticipants.partContactId",
          ["$trans.createdByUser","$trans.actTranFirmLeadAdvisorId"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actTranClientId"],
          "$trans.derivativeParticipants.partFirmId",
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
          then:{$toUpper:"$trans.actTranProjectDescription"},
          else:{$toUpper:"$trans.actTranIssueName"}},
      },
      tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
      tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate",new Date(8640000000000000)]},
      tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate",new Date(8640000000000000)]},
      tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate",new Date(8640000000000000)]},
      tranPrimarySector:{$toUpper:"$trans.actTranPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.actTranSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.actTranStatus","-"]}},
      tranClientName:{ $toUpper: "$trans.actTranClientName"},
      tranFirmName:{$toUpper:"$trans.actTranFirmName"},
      tranType:{$toUpper:"$trans.actTranType"},
      tranSubType:{$toUpper:"$trans.actTranSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
          }},0]
      },
      allIds:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actTranClientId"],
          "$trans.derivativeParticipants.partFirmId",
          "$trans.derivativeParticipants.partContactId",
          ["$trans.actTranFirmLeadAdvisorId"],
          ["$trans.createdByUser"],
          ["$trans._id"]
        ]
      },
      displayCategory:"Transactions",
      createdAt :"$trans.createdAt"

    }
  }
]

export const pipelineGetMarfpDetails= (user,alluserdetails) => [
  {
    $lookup:{
      from: "actmarfps",
      localField: "tenantId",
      foreignField: "actTranFirmId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.actTranFirmId",
      clientEntityId: "$trans.actIssuerClient",
      tranUsers:{
        $setUnion:[
          "$trans.maRfpParticipants.partContactId",
          ["$trans.createdByUser","$trans.actLeadFinAdvClientEntityId"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actIssuerClient"],
          "$trans.maRfpParticipants.partFirmId",
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
          then:{$toUpper:"$trans.actProjectName"},
          else:{$toUpper:"$trans.actIssueName"}},
      },
      tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
      tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate",new Date(8640000000000000)]},
      tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate",new Date(8640000000000000)]},
      tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate",new Date(8640000000000000)]},
      tranPrimarySector:{$toUpper:"$trans.actPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.actSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.actStatus","-"]}},
      tranClientName:{ $toUpper: "$trans.actIssuerClientEntityName" },
      tranFirmName:{$toUpper:"$trans.actTranFirmName"},
      tranType:{$toUpper:"$trans.actType"},
      tranSubType:{$toUpper:"$trans.actSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
          }},0]
      },

      allIds:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actIssuerClient"],
          "$trans.maRfpParticipants.partFirmId",
          "$trans.maRfpParticipants.partContactId",
          ["$trans.actLeadFinAdvClientEntityId"],
          ["$trans.createdByUser"],
          ["$trans._id"]
        ]
      },
      displayCategory:"MARFPS",
      createdAt :"$trans.createdAt"
    }
  }
]

export const pipelineGetOtherTransactionDetails = (user,alluserdetails) => [
  {
    $lookup:{
      from: "tranagencyothers",
      localField: "tenantId",
      foreignField: "actTranFirmId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.actTranFirmId",
      clientEntityId: "$trans.actTranClientId",
      tranUsers:{
        $setUnion:[
          "$trans.participants.partContactId",
          ["$trans.createdByUser","$trans.actTranFirmLeadAdvisorId"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actTranClientId"],
          "$trans.participants.partFirmId",
          "$trans.actTranUnderwriters.partFirmId"
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
          then:{$toUpper:"$trans.actTranProjectDescription"},
          else:{$toUpper:"$trans.actTranIssueName"}},
      },
      tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
      tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate",new Date(8640000000000000)]},
      tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate",new Date(8640000000000000)]},
      tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate",new Date(8640000000000000)]},
      tranPrimarySector:{$toUpper:"$trans.actTranPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.actTranSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.actTranStatus","-"]}},
      tranClientName:{$toUpper:"$trans.actTranClientName"},
      tranFirmName:{$toUpper:"$trans.actTranFirmName"},
      tranType:{$toUpper:"$trans.actTranType"},
      tranSubType:{$toUpper:"$trans.actTranSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId",["$trans.actTranFirmLeadAdvisorId"]]}
          }},0]
      },

      allIds:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actTranClientId"],
          "$trans.participants.partFirmId",
          "$trans.actTranUnderwriters.partFirmId",
          "$trans.participants.partContactId",
          ["$trans.actTranFirmLeadAdvisorId"],
          ["$trans.createdByUser"],
          ["$trans._id"]
        ]
      },
      displayCategory:"Projects",
      createdAt :"$trans.createdAt"

    }
  }
]

export const pipelineGetBusDevDetails = (user,alluserdetails) => [
  {
    $lookup:{
      from: "actbusdevs",
      localField: "tenantId",
      foreignField: "actTranFirmId", 
      as: "trans"
    }
  },
  {
    $unwind:"$trans"
  },
  {
    $project:{
      tranId:"$trans._id",
      firmEntityId:"$trans.actTranFirmId",
      clientEntityId: "$trans.actIssuerClient",
      tranUsers:{
        $setUnion:[
          "$trans.participants.rfpParticipantContactId",
          ["$trans.createdByUser","$trans.actLeadFinAdvClientEntityId"]
        ]
      },
      tranEntities:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actIssuerClient"],
          "$trans.participants.partFirmId",
        ]
      },
      activityDescription : {
        $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
          then:{$toUpper:"$trans.actProjectName"},
          else:{$toUpper:"$trans.actIssueName"}},
      },
      tranParAmount:new Date(8640000000000000),
      tranExpectedAwardDate:new Date(8640000000000000),
      tranActualAwardDate:new Date(8640000000000000),
      tranPricingDate:"-",
      tranPrimarySector:{$toUpper:"$trans.actPrimarySector"},
      tranSecondarySector:{$toUpper:"$trans.actSecondarySector"},
      tranStatus:{$toUpper:{$ifNull:["$trans.actStatus","-"]}},
      tranClientName:{$toUpper:"$trans.actIssuerClientEntityName"},
      tranFirmName:{$toUpper:"$trans.actLeadFinAdvClientEntityName"},
      tranType:{$toUpper:"$trans.actType"},
      tranSubType:{$toUpper:"$trans.actSubType"},
      userEntitlement:user.userEntitlement,
      relationshipType:"$relationshipType",
      myFirmUserIds:"$loggedInFirmUserIds",
      tranAssigneeDetails:{
        $arrayElemAt:[
          {$filter:{
            input:alluserdetails,
            as:"alltendetails",
            cond:{$in:["$$alltendetails.userId",["$trans.actLeadFinAdvClientEntityId"]]}
          }},0]
      },
      allIds:{
        $setUnion:[
          ["$trans.actTranFirmId","$trans.actIssuerClient"],
          "$trans.participants.partFirmId",
          ["$trans.createdByUser", "$trans.actLeadFinAdvClientEntityId" ],
          ["$trans._id"]
        ]
      },
      displayCategory:"Projects",
      createdAt :"$trans.createdAt"
    }
  }
]

export const commonPipelineStages = (user) => [
  {
    $addFields:{
      currentUserInTran:{
        $in:[ObjectID(user._id), {$ifNull:["$tranUsers",[]]}]
      },
      currentUserFirmInTran:{
        $in:[ObjectID(user.entityId),{$ifNull:["$tranEntities",[]]}]
      }
    }
  },
  {
    $addFields:{
      editTran:{
        $or:[
          {$and: [
            {$eq:["$userEntitlement","global-edit"]},
            {$or:[
              {$eq:["$currentUserInTran",true]},
              {$eq:["$currentUserFirmInTran",true]}
            ]},
          ]},
          {
            $and:[
              {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
              {$eq:["$currentUserInTran",true]},
            ]
          }
        ]
      },
      viewTran:{
        $or:[
          {$and: [
            {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
            {$or:[
              {$eq:["$currentUserInTran",true]},
              {$eq:["$currentUserFirmInTran",true]}
            ]},
          ]},
          {
            $and:[
              {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
              {$eq:["$currentUserInTran",true]},
            ]
          }
        ]
      }
    }
  },
  {
    $addFields:{
      myUserDetails: { $setIntersection:["$tranUsers",[user._id] ]},
      myFirmUserDetails: {
        "$switch": {
          "branches": [
            {
              "case": {
                $and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                ]},
              "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
            },
            {
              "case": {$and:[
                {"$eq":["$relationshipType","Self"]},
                {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
              ]},
              "then": { $setIntersection:["$tranUsers",[user._id] ]}
            },
            {
              "case": {
                $and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                ]},
              "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
            },
            {
              "case": {$and:[
                {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
              ]},
              "then": { $setIntersection:["$tranUsers",[user._id] ]}
            },

          ],
          "default": [user._id]
        }
      },
      myDealTeamUserDetails: {
        "$switch": {
          "branches": [
            {
              "case": {
                $and:[
                  {"$eq":["$relationshipType","Self"]},
                  {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
                ]},
              "then": "$tranUsers"
            },
            {
              "case": {$and:[
                {"$eq":["$relationshipType","Self"]},
                {$in:["$userEntitlement",["tran-edit"]]},
              ]},
              "then": { $setIntersection:["$tranUsers",[user._id] ]}
            },
            {
              "case": {$and:[
                {"$eq":["$relationshipType","Self"]},
                {$in:["$userEntitlement",["tran-readonly"]]},
              ]},
              "then": { $setIntersection:["$tranUsers",[user._id] ]}
            },
            {
              "case": {
                $and:[
                  {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                  {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                ]},
              "then": { $setIntersection:["$tranUsers","$myFirmUserIds" ]}
            },
            {
              "case": {$and:[
                {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
              ]},
              "then": { $setIntersection:["$tranUsers",[user._id] ]}
            },
            {
              "case": {$and:[
                {$in:["$relationshipType",["Client","Prospect","Third Party"]]},
                {$in:["$userEntitlement",["tran-readonly"]]},
              ]},
              "then": { $setIntersection:["$tranUsers",[user._id] ]}
            },
          ],
          "default": { $setIntersection:["$tranUsers",[user._id] ]}
        }
      },
    }
  }
]

export const tranSearchConcatenatedFields = () => [
  {
    $addFields:{
      selectTran:{$or:[
        {$eq:["$editTran",true]},
        {$eq:["$viewTran",true]}
      ]},
      keySearchDetails: { $concat: [
        {$ifNull:["$tranStatus",""]},
        {$ifNull:["$activityDescription",""]},
        {$ifNull:["$tranClientName",""]},
        {$ifNull:["$tranAssigneeDetails.userFullName",""]},
        {$ifNull:["$tranFirmName",""]},
        {$ifNull:["$tranType",""]},
        {$ifNull:["$tranSubType",""]},
        {$ifNull:["$tranPrimarySector",""]},
        {$ifNull:["$tranSecondarySector",""]},
      ]
      }
    },
  }
]

export const transactionsTabsKeys = {
  "BOND ISSUE": {
    default: [
      "dealIssueTranClientId",
      "dealIssueTranIssuerId",
      "dealIssueTranClientFirmName",
      "dealIssueTranClientMSRBType",
      "dealIssueTranIssuerFirmName",
      "dealIssueTranIssuerMSRBType",
      "dealIssueTranPurposeOfRequest",
      "dealIssueTranAssignedTo",
      "dealIssueTranType",
      "dealIssueTranSubType",
      "contracts"
    ],
    summary: [
      "dealIssueBorrowerName",
      "dealIssueGuarantorName",
      "dealIssueSecurityType"
    ],
    details: [
      "dealIssueTranState",
      "dealIssueTranCounty",
      "dealIssueTranPrimarySector",
      "dealIssueTranSecondarySector",
      "dealIssueUseOfProceeds",
      "dealIssueofferingType",
      "dealIssueBankQualified",
      "dealIssueCorpType",
      "dealIssueParAmount",
      "dealIssuePricingDate",
      "dealIssueExpAwardDate",
      "dealIssueActAwardDate",
      "dealIssueSdlcCreditPerc",
      "dealIssueEstimatedRev",
      "termsOfIssue",
      "tic",
      "bbri",
      "nic",
      "gsp",
      "mgtf",
      "exp",
      "tkdn",
      "other",
    ],
    participants: [
      "dealIssueParticipants",
      "dealIssueUnderwriters"
    ],
    pricing: [
      "dealIssueSeriesDetails"
    ],
    "check-track": [
      "dealChecklists"
    ]
  },
  "BANK LOAN": {
    default: [
      "actTranType",
      "actTranSubType",
      "actTranFirmId",
      "actTranFirmName",
      "actTranClientId",
      "actTranClientName",
      "actTranClientMsrbType",
      "actTranFirmLeadAdvisorId",
      "actTranFirmLeadAdvisorName",
      "contracts"
    ],
    summary: [
      "actTranBorrowerName",
      "actTranObligorName",
      "actTranSecurityType"
    ],
    details: [
      "bankLoanSummary",
      "bankLoanTerms",
      "bankLoanLinkCusips",
      "actTranPrimarySector",
      "actTranSecondarySector"
    ],
    participants: [
      "bankLoanParticipants"
    ],
    "check-track": [
      "bankLoanChecklists"
    ]
  },
  "DERIVATIVE": {
    default: [
      "actTranType",
      "actTranSubType",
      "actTranFirmId",
      "actTranFirmName",
      "actTranClientId",
      "actTranClientName",
      "actTranClientMsrbType",
      "actTranOtherSubtype",
      "actTranFirmLeadAdvisorId",
      "actTranFirmLeadAdvisorName",
      "contracts"
    ],
    summary: [
      "tranBorrowerName",
      "tranGuarantorName",
    ],
    details: [
      "actTranPrimarySector",
      "actTranSecondarySector",
      "derivativeSummary",
    ],
    participants: [
      "derivativeCounterparties",
      "derivativeParticipants"
    ],
    swapCap: [
      "derivTradeClientPayLeg",
      "derivTradeDealerPayLeg"
    ],
    "check-track": [
      "derivativeChecklists"
    ]
  },
  "RFP": {
    default: [
      "rfpTranClientId",
      "rfpTranIssuerId",
      "rfpTranClientFirmName",
      "rfpTranClientMSRBType",
      "rfpTranIssuerFirmName",
      "rfpTranIssuerMSRBType",
      "rfpTranName",
      "rfpTranType",
      "rfpTranSubType",
      "rfpTranPurposeOfRequest",
      "rfpTranOtherSubtype",
      "rfpTranAssignedTo",
      "contracts"
    ],
    summary: [
      "rfpTranIsConduitBorrowerFlag",
      "rfpEstimatedRev",
      "rfpParAmount",
      "rfpTranSecurityType",
      "rfpTranState",
      "rfpTranCounty"
    ],
    distribute: [
      "rfpEvaluationTeam",
      "rfpProcessContacts",
      "rfpParticipants",
    ],
    manage: [
      "rfpParticipantQuestions",
      "rfpMemberEvaluations",
      "rfpFinalEvaluations",
      "rfpFinalSelections",
      "rfpSupplierResponses",
    ],
    "check-track": [
      "rfpBidCheckList"
    ]
  },
  "OTHERS": {
    default: [
      "actTranFirmId",
      "actTranFirmName",
      "actTranClientId",
      "actTranClientName",
      "actTranOtherSubtype",
      "actTranType",
      "actTranSubType",
      "actTranPurposeOfRequest",
      "actTranFirmLeadAdvisorId",
      "actTranFirmLeadAdvisorName",
      "contracts"
    ],
    summary: [
      "actTranBorrowerName",
      "actTranGuarantorName",
      "actTranSecurityType"
    ],
    details: [
      "actTranState",
      "actTranCounty",
      "actTranPrimarySector",
      "actTranSecondarySector",
      "actTranUseOfProceeds",
      "actTranOfferingType",
      "actTranBankQualified",
      "actTranCorpType",
      "actTranParAmount",
      "actTranPricingDate",
      "actTranExpAwardDate",
      "actTranActAwardDate",
      "actTranSdlcCreditPerc",
      "actTranEstimatedRev",
    ],
    participants: [
      "participants",
      "actTranUnderwriters"
    ],
    pricing: [
      "actTranSeriesDetails"
    ],
    "check-track": [
      "actTranChecklists"
    ]
  },
  "MA-RFP": {
    default: [
      "actType",
      "actSubType",
      "actOtherSubtype",
      "actTranFirmId",
      "actTranFirmName",
      "actLeadFinAdvClientEntityId",
      "actLeadFinAdvClientEntityName",
      "actIssuerClient",
      "actIssuerClientEntityName",
      "actIssuerClientMsrbType",
      "actIssuer",
      "contracts"
    ],
    summary: [
      "maRfpResult",
      "actSaleType"
    ],
    details: [
      "maRfpSummary",
      "actPrimarySector",
      "actSecondarySector"
    ],
    participants: [
      "maRfpParticipants"
    ]
  },
  "BUSINESSDEVELOPMENT": {
    default: [
      "actType",
      "actSubType",
      "actOtherSubtype",
      "actTranFirmId",
      "actLeadFinAdvClientEntityId",
      "actLeadFinAdvClientEntityName",
      "actIssuerClient",
      "actIssuerClientEntityName",
      "actPrimarySector",
      "actSecondarySector",
    ]
  },
}

export const returnFromESObject ={
  "BOND ISSUE": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Deals, _id: id, esType: "tranagencydeals"}),
  "BANK LOAN": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: BankLoans, _id: id, esType: "tranbankloans"}),
  "DERIVATIVE": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Derivatives, _id: id, esType: "tranderivatives"}),
  "OTHERS": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Others, _id: id, esType: "tranagencyothers"}),
  "RFP": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: RFP, _id: id, esType: "tranagencyrfps"}),
  "MA-RFP": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: ActMaRFP, _id: id, esType: "actmarfps"}),
  "BUSINESSDEVELOPMENT": (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: ActBusDev, _id: id, esType: "actbusdevs"})
}
