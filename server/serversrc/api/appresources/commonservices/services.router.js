import express from "express"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import { getUsesForTranDocsModal,
  getAllUsersForAllFirmsAcrossTenant,
  fetchSuperVisorDetailsForLoggedInUser,
  fetchUrlsForSpecificIDs,
  fetchAllUrlsForTenant,
  postGeneralDocuments,
  getAllEntitiesUnderTenant,
  postCheckedAddToDL,
  getFirmDetailsForUsers,
  getTransactions,
  pullRelatedTransaction,
  fetchAllUsersAcrossAllTenants,
  fetchEligibleUserDetailsForLoggedInUser,
  fetchEligibleEntityDetailsForLoggedInUser,
  fetchAllEligibleTransactionsForLoggedInUser,
  fetchJSONToBlobData,
  checkExcelFiles,
  readDMExcel,
  makeCopyOfTransaction,
  fetchBillingInvoices
} from "./services.controller"

export const commonServicesRouter = express.Router()

commonServicesRouter
  .route("/getusersfortrandocs/:tranId")
  .get(requireAuth, getUsesForTranDocsModal)

commonServicesRouter
  .route("/getalltenantuserdetails")
  .get(requireAuth, getAllUsersForAllFirmsAcrossTenant)

commonServicesRouter
  .route("/getalltenantentitydetails")
  .get(requireAuth, getAllEntitiesUnderTenant)

commonServicesRouter
  .route("/geteligibleusersforloggedinuser")
  .get(requireAuth, fetchEligibleUserDetailsForLoggedInUser)

commonServicesRouter
  .route("/geteligiblentitiesforloggedinuser")
  .get(requireAuth, fetchEligibleEntityDetailsForLoggedInUser)

commonServicesRouter
  .route("/geteligibletransforloggedinuser")
  .get(requireAuth, fetchAllEligibleTransactionsForLoggedInUser)


commonServicesRouter
  .route("/getsupervisordetails")
  .get(requireAuth, fetchSuperVisorDetailsForLoggedInUser)

commonServicesRouter
  .route("/constructurlsforids")
  .post(requireAuth, fetchUrlsForSpecificIDs)

commonServicesRouter
  .route("/geturlsfortenant/:tenantId")
  .get(requireAuth, fetchAllUrlsForTenant)

commonServicesRouter
  .route("/postgeneraldocs")
  .post(requireAuth, postGeneralDocuments)

commonServicesRouter
  .route("/checkedaddtodl/:type/:tranId")
  .post(requireAuth, postCheckedAddToDL)
commonServicesRouter
  .post("/users-firms-details", requireAuth, getFirmDetailsForUsers)

commonServicesRouter
  .route("/transactions/:type")
  .get(requireAuth, getTransactions)
  .delete(requireAuth, pullRelatedTransaction)

commonServicesRouter
  .route("/allusersforalltenants")
  .get(requireAuth, fetchAllUsersAcrossAllTenants)

commonServicesRouter
  .route("/jsontoblob")
  .post(requireAuth, fetchJSONToBlobData)

commonServicesRouter
  .route("/checkexcelfiles")
  .post(requireAuth, checkExcelFiles)

commonServicesRouter
  .route("/readdmexcel")
  .post(requireAuth, readDMExcel)

commonServicesRouter
  .route("/makecopy")
  .post(requireAuth, makeCopyOfTransaction)


