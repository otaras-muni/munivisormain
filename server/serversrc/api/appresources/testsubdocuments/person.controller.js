import { generateControllers } from "../../modules/query"
import { Person } from "./person.model"

export default generateControllers(Person)