import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { putData, getData, deleteData } from "../../elasticsearch/esHelper"



const arrayNotEmpty = (values) => (values.length > 0)
const valuePositive = (value) => (value > 0)

// do a proper named function definition to ensure that "this" points to the current document

function dependencyTest(num2){
  console.log("The value of Num1:",this.num1)
  return (num2 <= this.num1)
}

const personAddressSchema = new mongoose.Schema ({
  addressLine1:String,
  addressLine2:String,
  country:String
})

const personSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "The First Name is required"]
  },
  lastName: {
    type:String,
    required:[true, "Last Name is required"]
  },
  addresses:[{type:personAddressSchema, validate:[arrayNotEmpty,"There need to test one two 3 to be atleast one address"]}],
  num1:{type:Number,required:[true, "Number 1 is required"]},
  num2:{ type:Number,validate:[{validator:dependencyTest,message:"NUM2 cannot be greater than Num1"},{validator:valuePositive,message:"The value has to greater than 0"}] }
})

personSchema.plugin(timestamps)

// const callUpdateFunction = async function(result, next) {
//   console.log("1. Get the Query", this.getQuery())
//   const query = this.getQuery()
//   console.log("2. Get the Update Query", this.getUpdate())
//   const {_id} = this.getQuery()

//   const doc = await Person.findOne({_id})

//   console.log("3. The document returned based on the update is",JSON.stringify(doc, null, 2) )

//   const data = await putData(
//     "personschema",
//     _id,
//     doc
//   )
//   console.log("The return data from inserting data in elastic search", JSON.stringify(data,null,2))
//   next()
// }


// personSchema.post("findOneAndUpdate", callUpdateFunction)
// personSchema.post("findByIdAndUpdate", callUpdateFunction)
// personSchema.post("updateOne", callUpdateFunction)

export const Person = mongoose.model("junktest", personSchema)

