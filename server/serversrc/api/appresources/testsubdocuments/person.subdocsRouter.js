import express from "express"
import {personAddressController} from "./person.subdocs.controller"

export const personAddressRouter = express.Router({mergeParams: true})

/* Define all the routes */
personAddressRouter.route("/")
  .get(personAddressController.getAllSubDocs)
  .post(personAddressController.insertSingleSubDocument)

personAddressRouter.route("/all")
  .get(personAddressController.getAllSubDocs)
  .post(personAddressController.insertUpdateAllSubDocuments)

personAddressRouter.route("/:id2")
  .get(personAddressController.getSingleSubDocument)
  .put(personAddressController.putSingleSubDocument)
  .post(personAddressController.putSingleSubDocument)  
  .delete(personAddressController.deleteSingleDocument)


  


