const mongoose = require("mongoose")

const {Schema} = mongoose

// eslint-disable-next-line no-unused-vars
const crmSchema = Schema({
  tenantId: String,
  uid: String,
  createdAt: Number,
  updatedAt: Number,
  updatedBy: String,
  entities: [
    {
      type: String,
      entityId: String,
      active: String,
      name: String,
      aliases: [String],
      keyParams: {
        sector: String,
        state: String,
        taxID: String,
        contractStatus: String,
        engagedSince: Date,
        numberOfEmployees: String,
        annualRevenue: String,
        businessStructure: String,
        ticker: String,
        websiteURL: String,
        notes: String
      },
      linkedBorrowersOrObligors: [
        {
          relationship: String,
          entity: String,
          engagedSince: Date,
          engagedTill: Date
        }
      ],
      linkedCusips: [
        {
          debtType: String,
          associatedCUSIP6: String
        }
      ],
      addresses: [
        {
          active: String,
          default: String,
          primaryOffice: String,
          addressType: String,
          addressLine: String,
          city: String,
          state: String,
          zip: String,
          phoneType: String,
          phoneNumber: String,
          fax: String					
        }
      ]
    }
  ],
  contacts: [
    {
      entityId: String,
      contactType: String,
      isActive: String,
      isDefault: String,
      prefix: String,
      firstName: String,
      middle: String,
      lastName: String,
      suffix: String,
      titleDesignation: String,
      addressType: String,
      addressLine: String,
      city: String,
      state: String,
      zip: String,
      phoneType: String,
      phoneNumber: String,
      fax: String,
      email: String
    }
  ],
  contracts: [
    {
      entityId: String,
      disclosures: [
        {
          appraisalDistricts: String,
          marketingRestrictions: String,
          filingResponsibility: String,
          negativeFilingConsent: String,
          notes: String
        }
      ],
      exemptions: [
        {
          exemptionType: String,
          startDate: String,
          endDate: Date,
          notes: Date
        }
      ]
    }
  ],
  feeDetails: {
    entityId: String,
    feeSchedules: [
      {
        scheduleName: String,
        scheduleStart: Date,
        scheduleEnd: Date,
        baseFee: Number,
        contractStatus: String,
        contractRenewalType: String				
      }
    ],
    rateBasis: [
      {
        fAFeeContractType: String,
        value: Number,
        tierAmount: Number,
        notes: String,
      }
    ],
    hourlyFees: [
      {
        roleWithClient: String,
        hourlyRate: Number,
        notes: String,
      }
    ],
    addendums: [
      {
        feeName: String,
        feeAmount: Number,
        isItReimburseable: String
      }
    ]
  },
  activities: [
    {
      entityId: String,
      dealId: String,
      transactionName: String,
      transactionID: String,
      activityType: String,
      name: String,
      roleWithClient: String,
      taskStart: Date,
      taskEnd: Date,
      hours: Number,
      billable: String,
      notes: String
    }
  ]
})

export const CRMModel = mongoose.model("crm",crmSchema )


