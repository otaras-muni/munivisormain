const schema = require('./RFP');
const mongoose = require('mongoose');
const dburl = 'mongodb://localhost:27017/munivisor_seed';

mongoose.connect(dburl);
mongoose.connection.on('open', (connectionError) => {
    if (connectionError) throw connectionError;

});