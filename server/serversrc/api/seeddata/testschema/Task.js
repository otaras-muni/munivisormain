const mongoose = require("mongoose")

const {Schema} = mongoose

// Tenant Schema that will hold good for every tenant 
const tenantSchema = Schema({
  name: String,
  type: String
})
const Tenant = mongoose.model("Tenant", tenantSchema)

// Custom User Schema. this needs to be replaced with other schema as we harden the application
const userSchema = Schema({
  name: String,
  email: String,
  password: String,
  tenant: { type: Schema.Types.ObjectId, ref: "Tenant" }
})
const UserRevised = mongoose.model("UserNew", userSchema)

// this is a custom deal schema for testing the the tasks module

const dealSchema = Schema({
  dealName: String,
  createdByTenantUser: { type: Schema.Types.ObjectId, ref: "UserNew" }
})
const Deal = mongoose.model("DealNew", dealSchema)

// This is the task schema that has relationships to User, tenant and Deal objects

const subTaskSchema = Schema({
  title: String,
  description: String,
  user: { type: Schema.Types.ObjectId, ref: "UserNew" },
  deal: { type: Schema.Types.ObjectId, ref: "DealNew" },
  dueDate: Date,
  status: String,
  asignees: [String],
  childTasks: [this]
})

const taskSchema = Schema({
  title: String,
  description: String,
  user: { type: Schema.Types.ObjectId, ref: "UserNew" },
  deal: { type: Schema.Types.ObjectId, ref: "DealNew" },
  dueDate: Date,
  status: String,
  asignees: [String],
  subTasks: [subTaskSchema]
})

exports.Tenant = Tenant
exports.UserRevised = UserRevised 
exports.Deal = Deal 
exports.Task = mongoose.model("Task", taskSchema)

/* 
	const seedAllDataForTesting = async () => {
	const Tenant1 = new Tenant({
		name: 'Financial Adivisor 1',
		type: 'Municipal Advisor'
	});

	const User1 = new UserRevised({
		name: 'Naveen',
		email: 'naveen.balawat@gmail.com',
		password: 'asdkflskdjfd',
		tenant: Tenant1
	});

	const Deal1 = new Deal({
		dealName: 'This is a test Deal',
		createdByTenantUser: User1
	});

	const SubTask3 = {
		title: 'this is the child 3',
		descirption: 'This is a new parent',
		user: User1,
		deal: Deal1,
		dueDate: Date.now(),
		status: 'Open'
	};

	const SubTask1 = {
		title: 'this is the child 1',
		descirption: 'This is a new parent',
		user: User1,
		deal: Deal1,
		dueDate: Date.now(),
		status: 'Open',
		childTasks: [SubTask3]
	};

	const SubTask2 = {
		title: 'this is the child 2',
		descirption: 'This is a new parent',
		user: User1,
		deal: Deal1,
		dueDate: Date.now(),
		status: 'Open'
	};

	const TaskParent = new Task({
		title: 'Task 1 - Parent',
		descirption: 'This is a new parent',
		user: User1,
		deal: Deal1,
		dueDate: Date.now(),
		status: 'Open',
		subTasks: [SubTask1, SubTask2]
	});

	try {
		await Promise.all([
			Task.remove(),
			Tenant.remove(),
			UserRevised.remove(),
			Deal.remove()
		]);

		await Promise.all([
			Tenant1.save(),
			User1.save(),
			Deal1.save(),
			TaskParent.save()
		]);
		return 'all well and this is great';
	} catch (err) {
		console.log('there is an error', err);
		throw Error(err);
	}
};

seedAllDataForTesting().then(val =>
	console.log('The value of return value is', val)
).catch((err) => console.log(err)) 
*/
