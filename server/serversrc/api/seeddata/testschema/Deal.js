// load the things we need
const mongoose = require("mongoose")

// eslint-disable-next-line no-unused-vars
const dealSchemaTest = mongoose.Schema({
  dealId:String,
  dealAmount:Number
})
// define the schema for our user model
const dealSchema = mongoose.Schema({
  _id: String,
  uid: String,
  collaborators: Array[String],
  dealName: String,
  dealKeyInfo: {
    issuerName: String,
    borrowerOrObligorName: String,
    guarantorName: String,
    transaction: String,
    transactionID: String,
    corpType: String,
    transactionType: String,
    startDate: Date,
    state: String,
    transactionStatus: String,
    dateHired: Date,
    county: String,
    moreIdentifiersParAmount: Number,
    sector: String,
    expectedAwardDate: Date,
    sDCCreditPercentage: Number,
    actualAwardDateTime: Date,
    pricingDate: Date,
    estimatedRevenue: Number,
    useOfProceeds: String,
    placeholder: String
  },
  dealComplianceChecks: {
    goodFaithAmount: Number,
    accountName: String,
    relatedNotes: String,
    goodFaithDate: Date,
    accountNumber: String,
    goodFaithInstructions: String,
    moneyTransferMethod: String,
    aBANumber: String,
    placeholder: String,
    advancedRefunding: String,
    formG36ARDSubmissionDateTime: Date,
    fOSReceivedDateTime: Date,
    g32SubmissionDateTime: Date
  },
  mAExemptions:
		Array[
		  {
		    mAExemptionType: String,
		    mAExemptionDate: Date,
		    mAExemptionNotes: String
		  }
		],
  g17Exemptions: [
    {
      g17Type: String,
      g17SentDate: Date,
      g17AcknowledgementDate: Date,
      g17Notes: String
    }
  ],
  miscComplianceApprovals: [
    {
      name: String,
      approvalDateTime: Date
    }
  ],
  underwritingTeam: [
    {
      firmName: String,
      roleInSyndicate: String,
      liabilityPercentage: Number,
      managementFeePercentage: Number
    }
  ],
  dealParticipants: [
    {
      participantType: String,
      firmName: String,
      contactName: String,
      contactTitle: String,
      primaryOfficeAddress1: String,
      primaryOfficeAddress2: String,
      primaryOfficeState: String,
      phone: String,
      email: String,
      notes: String,
      addToDL: Boolean
    }
  ],
  issuerUnderlyingRatings: [
    {
      series: String,
      seriesCode: String,
      longTermRating: String,
      longTermOutlook: String,
      shortTermRating: String,
      shortTermOutlook: String,
      notes: String
    }
  ],
  insuredEnhancedRatings: [
    {
      creditEnhancementProvider: String,
      creditEnhancementType: String,
      longTermRating: String,
      longTermOutlook: String,
      shortTermEnhancedRating: String,
      shortTermOutlook: String,
      notes: String
    }
  ],
  dealKeyParameters: {
    seriesCode: String,
    pricingParAmount: Number,
    securityType: String,
    security: String,
    securityDetails: String,
    principalFrequency: String,
    firstMaturityDate: Date,
    finalMaturityDate: Date,
    datedDate: Date,
    settlementDate: Date,
    firstCouponDate: Date,
    putDateMandatoryTender: Date,
    callFeature: String,
    callDate: Date,
    callPrice: Number,
    recordDate: Date,
    federalTax: String,
    stateTax: String,
    AMT: Number,
    bankQualified: String,
    couponFrequency: String,
    dayCount: String,
    rateType: String,
    firmInventory: String,
    minimumDenomination: Number,
    grossSpreadUSDPer1000: Number,
    estAvgTakedownUSDPer1000: Number,
    insuranceFee: Number,
    pricingStatus: Number,
    keyNotes: Number,
    otherRelatedNotes: String
  },
  pricingData: [
    {
      term: Number,
      maturityDate: Date,
      averageLife: Number,
      amount: Number,
      coupon: Number,
      yield: Number,
      price: Number,
      YTM: Number,
      cusip: String
    }
  ],
  dealDocuments: [
    {
      documentType: String,
      accessEntitlement: String,
      documentStatus: String,
      docId: String,
      fileName: String,
      action: String,
      tag: String
    }
  ],
  costsOfIssuance: [
    {
      standardCost: String,
      amount: Number,
      notes: String
    }
  ],
  additionalCostOfIssuance: [
    {
      costName: String,
      amount: Number,
      notes: String
    }
  ],
  scheduleofEvents: [
    {
      taskOrEventName: String,
      applicableToTransaction: String,
      assignedTo: String,
      dueDate: Date,
      status: String
    }
  ],
  additionalScheduleOfEvents: [
    {
      taskOrEventName: String,
      dueDate: Date,
      assignedTo: String,
      status: String,
      notes: String
    }
  ]
})


export default mongoose.model("Deal", dealSchema)
