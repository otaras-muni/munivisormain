const multiLevelPickList = [
  {
    pickListId:"123",
    level1Value:"A",
    level2Value:"A3"
  },
  {
    pickListId:"123",
    level1Value:"A",
    level2Value:"A1"
  },
  {
    pickListId:"123",
    level1Value:"B",
    level2Value:"B2"
  },
  {
    pickListId:"123",
    level1Value:"A",
    level2Value:"A2"
  },
  {
    pickListId:"123",
    level1Value:"B",
    level2Value:"B1"
  },
  {
    pickListId:"123",
    level1Value:"B",
    level2Value:"B3"
  },
  {
    pickListId:"ABC",
    level1Value:"BB",
    level2Value:"BB1"
  },
  {
    pickListId:"ABC",
    level1Value:"BB",
    level2Value:"BB3"
  }
]


const multiLevelFormattedObject =  multiLevelPickList.reduce( (finalInsertObject, vals) => {
  const {pickListId, level1Value, level2Value} = vals
  
  if(finalInsertObject[pickListId]) {
    
    if(finalInsertObject[pickListId][level1Value]) {
      finalInsertObject[pickListId][level1Value].items.push(
        {
          "label" : level2Value,
          "included" : true,
          "visible" : true,  
          "items":[]  
        }
      )
    }
    else {
      finalInsertObject[pickListId][level1Value] = {
        "label" : level1Value,
        "included" : true,
        "visible" : true,
        "items" : [{
          "label" : level2Value,
          "included" : true,
          "visible" : true,  
          "items":[]  
        }]
      }
    }
  } else {
    finalInsertObject[pickListId] = {
      title:pickListId,
      "meta" : {
        "systemName" : pickListId,
        "subListLevel2" : false,
        "subListLevel3" : false,
        "systemConfig" : true,
        "restrictedList" : false,
        "externalList" : false,
        "bestPractice" : true
      },
      [level1Value]:{
        "label" : level1Value,
        "included" : true,
        "visible" : true,
        "items" : [{
          "label" : level2Value,
          "included" : true,
          "visible" : true,  
          "items":[]  
        }]
      },
    }

  }
  return finalInsertObject
},{})

const finalMultiLevelObject = Object.keys(multiLevelFormattedObject).reduce ( (finalMultiLevelObject, pickList) => {
  const {title,meta,...alllevel1items} = multiLevelFormattedObject[pickList]
  const pickListObject = { title,meta, items:Object.values(alllevel1items)}
  return [...finalMultiLevelObject,pickListObject]
},[])

console.log(JSON.stringify(finalMultiLevelObject,null,2))