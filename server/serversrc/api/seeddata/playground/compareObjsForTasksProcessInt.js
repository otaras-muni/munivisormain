
const  crypto = require("crypto")

const checklists = [
  {
    data:[
      {
        headers:["test1","something else"],
        items:[
          {
            _id:"1",
            label:"ITEM 1",
            assignedTo:[{_id:"u1",name:"naveen",type:"bond counsel"},{_id:"u2",name:"naveen",type:"bond counsel"}],
            //            startDate:"20180101",
            endDate:"20180131",
            resolved:false,
            priority:"low"
          },
          {
            _id:"2",
            label:"ITEM 2",
            assignedTo:[{_id:"u1",name:"naveen",type:"bond counsel"}],
            startDate:"20180101",
            endDate:"20180131" ,
            resolved:false ,
            priority:"high"        
          }
        ],
        title:"Manage Deal Items"
      },
      {
        headers:["test1","something else"],
        items:[
          {
            _id:"3",
            label:"ITEM 11",
            assignedTo:[{_id:"u1",name:"naveen",type:"bond counsel"},{_id:"u2",name:"deepak",type:"bond counsel"}],
            startDate:"20180101",
            endDate:"20180131",
            resolved:false,
            priority:"low"
          },
          {
            _id:"4",
            label:"ITEM 21",
            assignedTo:[{_id:"u1",name:"naveen",type:"bond counsel"}],
            startDate:"20180101",
            endDate:"20180131" ,
            resolved:false ,
            priority:"high"        
          }
        ],
        title:"Manage Deal Items"
      }
    ],
    id:"CL1",
    type:"template4"
  }
]

const existingTasksInDb =   [
  // The content of the information has changed
  {
    "taskUniqueIdentifier": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "taskAssigneeUserId": "u2",
      "activityContextTaskId": "3"
    },
    "taskDetails": {
      "taskAssigneeUserId": "u2",
      "taskAssigneeName": "deepak",
      "taskAssigneeType": "bond counsel",
      "taskDescription": "ITEM 11",
      "taskStartDate": "20180101",
      "taskEndDate": "20180131",
      "taskPriority": "Medium",
      "taskStatus": "open"
    },
    "relatedActivityDetails": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "activityContextSection": "Manage Deal Items",
      "activityContextTaskId": "3"
    },
  },

  // this is the same there is no change
  {
    "taskUniqueIdentifier": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "taskAssigneeUserId": "u1",
      "activityContextTaskId": "4"
    },
    "taskDetails": {
      "taskAssigneeUserId": "u1",
      "taskAssigneeName": "naveen",
      "taskAssigneeType": "bond counsel",
      "taskDescription": "ITEM 21",
      "taskStartDate": "20180101",
      "taskEndDate": "20180131",
      "taskPriority": "high",
      "taskStatus": "open"
    },
    "relatedActivityDetails": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "activityContextSection": "Manage Deal Items",
      "activityContextTaskId": "4"
    }
  },
  // This one doesn't exist in the list so this needs to be deleted.
  {
    "taskUniqueIdentifier": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "taskAssigneeUserId": "u1",
      "activityContextTaskId": "5"
    },
    "taskDetails": {
      "taskAssigneeUserId": "u1",
      "taskAssigneeName": "naveen",
      "taskAssigneeType": "bond counsel",
      "taskDescription": "ITEM 21",
      "taskStartDate": "20180101",
      "taskEndDate": "20180131",
      "taskPriority": "high",
      "taskStatus": "open"
    },
    "relatedActivityDetails": {
      "activityId": "deal 123",
      "activityContext": "CL1",
      "activityContextSection": "Manage Deal Items",
      "activityContextTaskId": "5"
    }
  }
]

// For a given ID get the following details
/* 
  - checklists
  - ID, type, subtype
  - clientId
  - clientName
*/
// For the given ID get all the tasks - store that Information in a variable


// Destructure the checklists into a variable.
const processContext = "deal 123"
const processType = "Transactions"
const processSubType = "Deal Issue"
const processClientId = "123"
const processClientName = "City of Tampa"

// Get only those checklists that are of a specific type
const taskEligibleChecklists = (checklists || []).filter ( ({type}) => type === "template4" || type === "template1" )

// Flatten the checklists to 
const mappedTasksFromCheckLists = (taskEligibleChecklists || []).reduce ( (allCheckListFlatItems, chklist) => {
  const {id:chkListContext, data: checkListData} = chklist
  const allTaskData = (checkListData ||[]).reduce ( (allFlatAccordions, chkListAccordion ) => {
    const {items:checkListDataItems, title} = chkListAccordion
    const allFlatItems = (checkListDataItems || []).reduce((unwindItems, item) => {
      const {assignedTo,_id:itemId, label, startDate, endDate, resolved,priority} = item
      // Make sure that the start date and end date are populated
      if( assignedTo && startDate && endDate ) {
        const modifiedAssignees = (assignedTo || []).map( ({_id,name,type}) => {
          const retObject = { 
            taskUniqueIdentifier:{
              activityId:processContext,
              activityContext:chkListContext,
              taskAssigneeUserId:_id,
              activityContextTaskId:itemId
            },
            taskDetails:{
              taskAssigneeUserId:_id,
              taskAssigneeName:name,
              taskAssigneeType:type,
              taskDescription:label,
              taskStartDate:startDate,
              taskEndDate:endDate,
              taskPriority:priority || "low",
              taskStatus: resolved ? "closed" : "open"
            },
            relatedActivityDetails:{
              activityId:processContext,
              activityType:processType,
              activitySubType:processSubType, 
              activityIssuerClientId:processClientId,
              activityUserClientName:processClientName,             
              activityContext:chkListContext,
              activityContextSection:title,
              activityContextTaskId:itemId,
            }
          }
          // Generate a unique String Information
          const taskIdentifier = crypto.createHash("md5").update(JSON.stringify(retObject.taskUniqueIdentifier)).digest("hex")
          const taskContentHash = crypto.createHash("md5").update(JSON.stringify(retObject.taskDetails)).digest("hex")
          return {...retObject,taskIdentifier,taskContentHash}
        }
        )
        return [...unwindItems,...modifiedAssignees]
      }
      return unwindItems
    },[])
    return [...allFlatAccordions,...allFlatItems]
  },[])
  return [...allCheckListFlatItems,...allTaskData]
},[])

// Get the tasks associated with the deal ID
// Run a loop to compare the information from the checklist and the deal ID

const tasksForComparison = existingTasksInDb.reduce ( (overallTasks, oldTask) => {
  const taskIdentifier = crypto.createHash("md5").update(JSON.stringify(oldTask.taskUniqueIdentifier)).digest("hex")
  const taskContentHash = crypto.createHash("md5").update(JSON.stringify(oldTask.taskDetails)).digest("hex")
  return {...overallTasks,[taskIdentifier]:{taskIdentifier,taskContentHash}}
},{})

// Get IDs from existing and new set of tasks

const existingTasksUniqueIds = Object.keys(tasksForComparison)
const newTaskUniqueIds = mappedTasksFromCheckLists.map( ({taskIdentifier}) => taskIdentifier)

// Capture all the new tasks that need to be inserted

const listNewAndUpdatedTasks = mappedTasksFromCheckLists.reduce ( (finalStatus, newTask) => {
  const {addTask,modifyTask,noaction} = finalStatus
  const {taskIdentifier,taskContentHash} = newTask
  const matchStatus = existingTasksUniqueIds.includes(taskIdentifier)

  let sameContent = false
  if(matchStatus) {
    sameContent = tasksForComparison[taskIdentifier].taskContentHash === taskContentHash
  }
  console.log({taskIdentifier,taskContentHash,matchStatus,sameContent})

  if(matchStatus && !sameContent) {
    return {...finalStatus,...{modifyTask:[...modifyTask,newTask]}}
  }
  if(!matchStatus && !sameContent) {  
    return {...finalStatus,...{addTask:[...addTask,newTask]}}
  }
  if(!matchStatus && sameContent) {
    return {...finalStatus,...{addTask:[...addTask,newTask]}}
  }
  if(matchStatus && sameContent) {
    return {...finalStatus,...{noaction:[...noaction,newTask]}}
  }
}, {"addTask":[],"modifyTask":[],"noaction":[]})

// Find Tasks that need to be deleted
const tasksToBeDeleted = existingTasksUniqueIds.reduce( (deletedTasks,oldTaskIdentifier)=>{
  const {deleteTask} = deletedTasks
  const matchStatus = newTaskUniqueIds.includes(oldTaskIdentifier)
  if( !matchStatus) {
    return {...deletedTasks,...{deleteTask:[...deleteTask,oldTaskIdentifier]}}
  }
  return deletedTasks
},{deleteTask:[]})

// Consolidate all the items into one Object
const consolidatedTaskAction = {...listNewAndUpdatedTasks,...tasksToBeDeleted}
// Make modifications in Mongo DB for the tasks

// push notifications based on items into the DB.
console.log(JSON.stringify(consolidatedTaskAction,null,2))




 
