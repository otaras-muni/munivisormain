import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"


import {EntityRel, Deals, Tasks, RFP, Others, BankLoans, ActMaRFP} from "./../../appresources/models"
import {elasticSearchNormalizationAndBulkInsert, deleteData, deleteDataInBulkForTenant} from "./../../elasticsearch/esHelper"

export const getCheckListDataAndCompareTasks = async (id, user) => {

  const commonTaskPipeline = [
    {
      $unwind:"$checklists"
    },
    {
      $match:{"checklists.status":"active","checklists.type":{$in:["template1","template4"]}}
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:"$checklists.name",
        checklistTemplateType:"$checklists.type",
        checklistData:"$checklists.data",
      }
    },
    {
      $unwind:"$checklistData"
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:1,
        checklistTaskCategoryTitle:"$checklistData.title",
        checklistTemplateType:1,
        checklistItems:"$checklistData.items"
      }
    },
    {
      $unwind:"$checklistItems"
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:1,
        checklistTaskCategoryTitle:1,
        checklistTemplateType:1,
        checklistItemTaskAssignees:"$checklistItems.assignedTo",
        checklistItemTaskLinkId:"$checklistItems._id",
        checklistItemTaskDesc: "$checklistItems.label",
        checklistItemTaskStartDate:"$checklistItems.startDate",
        checklistItemTaskDueDate:"$checklistItems.endDate",
        checklistItemStatus:"$checklistItems.eventStatus"
      }
    },
    {
      $unwind:"$checklistItemTaskAssignees"
    },
    {
      $project:{
        _id:1,
        tranId:1,
        tranType:1,
        tranSubType:1,
        tranClientEntityId: 1,
        tranActivityDescription :1,
        checklistName:1,
        checklistTemplateType:1,
        checklistTaskCategoryTitle:1,
        checklistItemTaskLinkId:1,
        checklistItemTaskDesc: 1,
        checklistItemTaskStartDate:1,
        checklistItemTaskDueDate:1,
        checklistItemTransactionStatus:1,
        checklistItemAssigneeId:"$checklistItemTaskAssignees._id",
        checklistItemAssigneeName:"$checklistItemTaskAssignees.name",
        checklistItemAssigneeType:"$checklistItemTaskAssignees.type",
      }
    },
    {
      $addFields:{
        checklistItemTaskUniqueId:{ $concat: [ 
          {
            $toString: "$tranId"
          }, "|", 
          {
            $toString:"$checklistItemTaskLinkId"
          },"|",
          {
            $toString:"$checklistItemAssigneeId" 
          }
        ]
        }
      }
    },
    {
      $addFields:{
        taskUniqueIdentifier:{
          _id:"$checklistItemTaskUniqueId",
          activityId:"$tranId",
          activityContext:"$checklistItemTaskUniqueId",
          taskAssigneeUserId:"$checklistItemAssigneeId",
          activityContextTaskId:"$checklistItemTaskUniqueId"
        },
        taskDetails:{
          taskAssigneeUserId:"$checklistItemAssigneeId",
          taskAssigneeName:"$checklistItemAssigneeName",
          taskAssigneeType:"$checklistItemAssigneeType",
          taskDescription:"$checklistItemTaskDesc",
          taskStartDate:"$checklistItemTaskStartDate",
          taskEndDate:"$checklistItemTaskDueDate",
          taskType:"$tranType",
          taskInsertContext:"Transactions/Projects"
        },
        relatedActivityDetails:{
          activityId:"$tranId",
          activityType:"$tranType",
          activitySubType:"$tranSubType",
          activityProjectName: "$tranActivityDescription",
          activityIssuerClientId:"$tranClientEntityId",
          activityIssuerClientName:"$tranActivityDescription",
          activityContext:"$checklistTemplateType",
          activityContextSection:"$checklistName",
          activityContextTaskId:"$checklistItemTaskUniqueId",
        },
      }
    }
  ]
  
  let chklist = []

  if(isEmpty(chklist)){
    chklist = await Deals.aggregate([
      {
        $match:{_id:ObjectID(id)}
      },
      {
        $project:{
          tranId:"$_id",
          tranType:"$dealIssueTranType",
          tranSubType:"$dealIssueTranSubType",
          tranClientEntityId: "$dealIssueTranIssuerId",
          tranActivityDescription : {
            $cond:{if:{$or:[{"$eq":["$dealIssueTranIssueName",""]},{"$eq":["$dealIssueTranIssueName",null]}]},
              then:"$dealIssueTranProjectDescription",
              else:"$dealIssueTranIssueName"},
          },
          checklists:{$ifNull:["$dealChecklists",[]]},
        }
      },
      ...commonTaskPipeline
    ])
  }

  if(isEmpty(chklist)){
    chklist = await RFP.aggregate([
      {
        $match:{_id:ObjectID(id)}
      },
      {
        $project:{
          tranId:"$_id",
          tranType:"$rfpTranType",
          tranSubType:"$rfpTranSubType",
          tranClientEntityId: "$rfpTranIssuerId",
          tranActivityDescription : {
            $cond:{if:{$or:[{"$eq":["$rfpTranIssueName",""]},{"$eq":["$rfpTranIssueName",null]}]},
              then:"$rfpTranProjectDescription",
              else:"$rfpTranIssueName"},
          },
          checklists:{$ifNull:["$rfpBidCheckList",[]]},
        }
      },
      ...commonTaskPipeline
    ])
  }
  if(isEmpty(chklist)){

    chklist = await Others.aggregate([
      {
        $match:{_id:ObjectID(id)}
      },
      {
        $project:{
          tranId:"$_id",
          tranType:"$actTranType",
          tranSubType:"$actTranSubType",
          tranClientEntityId: "$actTranClientId",
          tranActivityDescription : {
            $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
              then:"$actTranProjectDescription",
              else:"$actTranIssueName"},
          },
          checklists:{$ifNull:["$actTranChecklists",[]]},
        }
      },
      ...commonTaskPipeline
    ])
  }

  if(isEmpty(chklist)){
    chklist = await BankLoans.aggregate([
      {
        $match:{_id:ObjectID(id)}
      },
      {
        $project:{
          tranId:"$_id",
          tranType:"$actTranType",
          tranSubType:"$actTranSubType",
          tranClientEntityId: "$actTranClientId",
          tranActivityDescription : {
            $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
              then:"$actTranProjectDescription",
              else:"$actTranIssueName"},
          },
          checklists:{$ifNull:["$bankLoanChecklists",[]]},
        }
      },
      ...commonTaskPipeline
    ])
  }

  if(isEmpty(chklist)){
    chklist = await ActMaRFP.aggregate([
      {
        $match:{_id:ObjectID(id)}
      },
      {
        $project:{
          tranId:"$_id",
          tranType:"$actType",
          tranSubType:"$actSubType",
          tranClientEntityId: "$actIssuerClient",
          tranActivityDescription : {
            $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actIssueName",null]}]},
              then:"$actProjectName",
              else:"$actIssueName"},
          },
          checklists:{$ifNull:["$marfpChecklists",[]]},
        }
      },
      ...commonTaskPipeline
    ])
  }



  // Get the unique task IDs for comparison with existing tasks
  const uniqueTaskIds = chklist.map( a => a.checklistItemTaskUniqueId)
  const type = chklist.reduce( (acc,{tranType}) => tranType,null)

  const convertToObject = chklist.reduce( ( acc, {checklistItemTaskUniqueId,taskUniqueIdentifier,taskDetails,relatedActivityDetails}) => ({...acc,
    [checklistItemTaskUniqueId]:{
      taskUniqueIdentifier,
      taskDetails,
      relatedActivityDetails
    }
  }),{})

  const getExistingTasksForProcess = await Tasks.aggregate([
    {
      $match:{"relatedActivityDetails.activityId":ObjectID(id),"taskDetails.taskInsertContext":"Transactions/Projects"}
    },
    {
      $addFields:{
        "taskUniqueIdentifier":{$ifNull:["$taskUniqueIdentifier._id",""]}
      }
    }
  ])

  // Get unique Ids for comparison

  const tasksExistingInGlobalForProcessContext = getExistingTasksForProcess.map( ({taskUniqueIdentifier}) => taskUniqueIdentifier)
  

  // Get the case where the tasks already exist. Just 
  const processTasksUpdate = await Promise.all(getExistingTasksForProcess
    .filter(({taskUniqueIdentifier}) => uniqueTaskIds.includes(taskUniqueIdentifier))
    .map(async ({_id,taskUniqueIdentifier,taskDetails,relatedActivityDetails,createdAt,...rest}) => {
      const newTaskDetails = convertToObject[taskUniqueIdentifier].taskDetails
      const newRelatedActivityDetails = convertToObject[taskUniqueIdentifier].relatedActivityDetails
      return Tasks.findOneAndUpdate({_id},{
        taskDetails:{...taskDetails,...newTaskDetails},
        relatedActivityDetails:{...relatedActivityDetails,...newRelatedActivityDetails},
        ...rest
      },{
        upsert:false,
        new:true
      })
    }))

  const createdDate = new Date()
  const updatedDate = createdDate

  const processTasksInsert = chklist
    .filter(({checklistItemTaskUniqueId}) => !tasksExistingInGlobalForProcessContext.includes(checklistItemTaskUniqueId) )
    .map( ({taskUniqueIdentifier,taskDetails,relatedActivityDetails}) =>({
      taskDetails:{...taskDetails,taskStatus:"Open",taskUnread:true},
      taskUniqueIdentifier,
      relatedActivityDetails,
      updatedAt:updatedDate,
      createdAt:createdDate
    }))
      
  const processTasksDelete = await Promise.all(getExistingTasksForProcess
    .filter( x => !uniqueTaskIds.includes(x.taskUniqueIdentifier) )
    .map( async ( {_id}) => Tasks.findOneAndRemove({_id})))
      
  const esTaskInsertStatus =[]
  if(!isEmpty(processTasksInsert)){
    const insertedData = await Tasks.insertMany(processTasksInsert)
    const dataForEs = insertedData.map(d => d._id)
    const bulkEsInsertStatus = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: user.tenantId})
    esTaskInsertStatus.push({transactionId:id,transactionType:type,operation:"Insert Tasks",esOutput:bulkEsInsertStatus})  
  }
    
  if(!isEmpty(processTasksUpdate)) {
    const dataForEs = processTasksUpdate.map(d => d._id)
    const bulkUpdateEs = await elasticSearchNormalizationAndBulkInsert({esType: "mvtasks", ids: dataForEs, tenantId: user.tenantId})
    esTaskInsertStatus.push({transactionId:id,transactionType:type,operation:"Update",esOutput:bulkUpdateEs})  
  }

  if(!isEmpty(processTasksDelete)){
    await deleteDataInBulkForTenant({
      tenantId: user.tenantId,
      ids: processTasksDelete.map(({_id}) => _id)
    })
    esTaskInsertStatus.push({transactionId:id,transactionType:type,operation:"Delete Tasks",esOutput:"done with es delete"})  
  }
  return {processTasksUpdate,processTasksInsert,processTasksDelete,esTaskInsertStatus}
}

const user = {
  _id:ObjectID("5ca403bc5b6b8b4c74bdb15e"),
  tenantId:ObjectID("5ca403bc5b6b8b4c74bdace9"),
  entityId:ObjectID("5ca403bc5b6b8b4c74bdace9"),
  userEntitlement:"global-edit",
  relationshipType:"Self",
  isSuperVisor:true
}

getCheckListDataAndCompareTasks("5ca559542ed2861fecf4d8e8",user).then ( k => console.log(JSON.stringify(k,null,2)))
