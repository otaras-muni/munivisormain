import {ObjectID} from "mongodb"
import mongoose from "mongoose"
import isEmpty from "lodash/isEmpty"
import { Entity,EntityRel,Tasks,EntityUser,  PoliticalContributions,  PoliContributionDetails, UserEntitlements } from "./../../appresources/models"
import { urlGenerator } from "./../../appresources/commonservices/urlGeneratorHelper"
import { getAllEntityAndUserInformationForTenant , getAllUsersForLoggedInUsersFirm} from "./../../appresources/commonservices/getAllUserAndEntityDetails"
import { getAllKeysForDocumentIndexService } from "./../../elasticsearch/getAllTenantInfoForDocSearch"

require("dotenv").config()

mongoose.Promise = global.Promise
mongoose.set("useCreateIndex", true)
mongoose.connect(process.env.DB_URL,{ useNewUrlParser: true })

const testingPipelineConcatenation = async (user) => {

  const allTransactionDetails = await EntityRel.aggregate([
    {
      $match:{
        entityParty2:ObjectID(user.entityId)
      }
    },
    {
      $project:{
        tenantId:"$entityParty1",
        loggedInUserEntityId:"$entityParty2",
        relationshipType:"$relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "loggedInUserEntityId",
        foreignField: "entityId",
        as: "myFirmUsers"
      }
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:"$myFirmUsers._id",
      }
    },
    {
      $facet:{
        "deals":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "tenantId",
              foreignField: "dealIssueTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.dealIssueTranClientId",
              clientEntityId: "$trans.dealIssueTranIssuerId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                  then:"$trans.dealIssueTranProjectDescription",
                  else:"$trans.dealIssueTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranType:"$trans.dealIssueTranType",
              tranSubType:"$trans.dealIssueTranSubType",
              createdDate:"$trans.createdAt"
            }
          }
        ],
        "bankloans":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              createdDate:"$trans.createdAt"
            }
          }
        ]
      }
    },
    { 
      "$project": {
        "data": { "$concatArrays": ["$deals", "$bankloans"] }
      }},
    { "$unwind": "$data" },
    { "$replaceRoot": { "newRoot": "$data" } },
    {
      $addFields: {
        "CreatedMonth" : {
          "$month" : "$createdDate"
        }, 
        "CreatedYear" : {
          "$year" : "$createdDate"
        }, 
        "CreatedYearMonth" : {
          "$dateToString" : {
            "format" : "%m:%Y", 
            "date" : "$createdDate"
          }
        }
      }
    },
    {
      "$group" : {
        "_id" : "$tranSubType", 
        count:{$sum:1},
        data: { $push : "$$ROOT" }
      }
    },
    { "$project": {
      count:1,
      pages: { $ceil: { $divide: ["$count", 5] }},      
      results: { "$slice": [ "$data", 0, 2 ] }
    }}
    // {
    //   $facet: {
    //     metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", 5] } } } }],
    //     data: [ { $skip: 0 }, { $limit: 3 }] // add projection here wish you re-shape the docs
    //   }
    // }
  ]
  )
  return allTransactionDetails

  // Need to appy the match criteria, group by should be dynamic

}


export const getEntityDetailsForMasterLists = async (user, searchbody) => {

  // Get current user
  // Get all the entities that he has access to - Refer to the entitlementCollections - Done
  // Get all the users that he has access to - Refer to the entitlementCollections - Done
  // Store that information from the entitlement service - Done
  // Query the entities collection - Done
  // Get the primary address associated with the entity - Done
  // Filter the entities that he has access to - Done
  // Get the users attached to the entities - Done
  // Get the primary user attached to the entity - Done
  // Accumulate all the details that one could search on.
  // Do the facets - get the total number of records - Done
  // Do the facets - get the number of pages  - Done
  // Do the facets - get the data that they are eligible to see - Done
  // Write Proper Logs to ensure that we are able to debug this Properly - Done

  // const body = {
  //   filters: {
  //     entityTypes:[],
  //     userContactTypes:[],
  //     entityMarketRoleFlags:[],
  //     entityIssuerFlags:[],
  //     entityPrimaryAddressStates:[],
  //     freeTextSearchTerm:"NAVEEN"
  //   },
  //   pagination:{
  //     serverPerformPagination:false,
  //     currentPage:2,
  //     size:5,
  //     sortFields:{ entityName:1, userFirstName:1, userLastName:1 }
  //   }
  // }

  console.log(JSON.stringify(searchbody, null, 2))

  const { entityId, entityTypes, userContactTypes, entityMarketRoleFlags, entityIssuerFlags, entityPrimaryAddressStates, freeTextSearchTerm } = searchbody.filters
  const { serverPerformPagination, currentPage, size, sortFields } = searchbody.pagination

  const matchQueryNew = []
  const matchQueryNoUserCase = []
  const entityTypesRevised = isEmpty(entityTypes) ? ["Client", "Prospect"] : [...entityTypes]
  const userContactTypesRevised = [...userContactTypes, "Primary Contact"]


  if (!isEmpty(userContactTypesRevised)) {
    matchQueryNew.push({ "matchOnUserContactType": true })
  }

  if (!isEmpty(entityMarketRoleFlags)) {
    matchQueryNew.push({ "matchOnEntityMarketRole": true })
    matchQueryNoUserCase.push({ "matchOnEntityMarketRole": true })
  }

  if (!isEmpty(entityIssuerFlags)) {
    matchQueryNew.push({ "matchOnIssuerTypeFlags": true })
    matchQueryNoUserCase.push({ "matchOnIssuerTypeFlags": true })
  }

  if (!isEmpty(entityPrimaryAddressStates)) {
    matchQueryNew.push({ "matchOnStates": true })
    matchQueryNoUserCase.push({ "matchOnStates": true })
  }

  if (!isEmpty(freeTextSearchTerm)) {
    matchQueryNew.push({ "keySearchDetails": { $regex: freeTextSearchTerm, $options: "i" } })
    matchQueryNoUserCase.push({ "keySearchDetails": { $regex: freeTextSearchTerm, $options: "i" } })
  }

  // const paginationParameter = 5
  const recordstoskip = size * currentPage

  // Get all the eligible Users and Entities

  try {

    const eligibleUsersAndEntities = await UserEntitlements.findOne({ userId: user._id }).select({ "accessInfo.entities": 1, "accessInfo.users": 1 })
    console.log("the eligible users are",eligibleUsersAndEntities )
    const entityList = eligibleUsersAndEntities.accessInfo.entities.all || []
    const usersList = eligibleUsersAndEntities.accessInfo.users.all || []

    console.log("The main match query is",JSON.stringify(matchQueryNew,null,2) )
    console.log("The alternate match query is",JSON.stringify(matchQueryNoUserCase,null,2), )


    const newEntityList = isEmpty(entityId)? entityList : [ObjectID(entityId)]
    matchQueryNew.push({ "accessibleUsers": true })

    const queryPipeline = [
      { $match: { entityParty2: ObjectID(user.entityId) } },
      {
        $lookup: {
          from: "entityrels",
          localField: "entityparty1",
          foreignField: "entityparty1",
          as: "alltenantentities"
        }
      },
      {
        $unwind: { path: "$alltenantentities", preserveNullAndEmptyArrays: true }
      },
      {
        $project: {
          entityId: "$alltenantentities.entityParty2",
          relationshipType: "$alltenantentities.relationshipType",
          tenantId: "$alltenantentities.entityParty1"
        }
      },
      {
        $match: { "relationshipType": { $in: [...entityTypesRevised] } }
      },
      {
        $lookup: {
          from: "entities",
          localField: "entityId",
          foreignField: "_id",
          as: "entitydetails"
        }
      },
      {
        $unwind: { path: "$entitydetails", preserveNullAndEmptyArrays: true }
      },
      {
        $match: { "entitydetails._id": { $in: [ObjectID("5c2c766bdba832075ca1fa14")] } }
      },
      {
        $project: {
          entityId: 1,
          relationshipType: 1,
          entityName: { $ifNull: ["$entitydetails.firmName", "-"] },
          entityPrimaryAddress: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$entitydetails.addresses",
                  as: "entityaddresses",
                  cond: { $eq: ["$$entityaddresses.isPrimary", true] }
                }
              }, 0]
          },
          entityFlags: { $ifNull: ["$entitydetails.entityFlags", []] },
          entityLinkedCusips: { $ifNull: ["$entitydetails.entityLinkedCusips", []] }
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "entityId",
          foreignField: "entityId",
          as: "allentityusers"
        }
      },
      {
        $unwind: {
          path: "$allentityusers", preserveNullAndEmptyArrays: true
        }
      },
      {
        $project: {
          entityId: 1,
          userId:"$allentityusers._id",
          entityRelationshipType: "$relationshipType",
          entityName: { $ifNull: ["$entityName", ""] },
          userFirstName: { $ifNull: ["$allentityusers.userFirstName", ""] },
          userLastName: { $ifNull: ["$allentityusers.userLastName", ""] },
          userFullName: { $concat: [{ $ifNull: ["$allentityusers.userLastName", ""] }, " ", { $ifNull: ["$allentityusers.userFirstName", ""] }] },
          userPrimaryEmail: { $ifNull: ["$allentityusers.userLoginCredentials.userEmailId", ""] },
          userPrimaryPhoneNumber: {
            $arrayElemAt: [
              {
                $filter: {
                  input: "$allentityusers.userPhone",
                  as: "userPhoneNumbers",
                  cond: { $eq: ["$$userPhoneNumbers.phonePrimary", true] }
                }
              }, 0]
          },
          entityPrimaryAddressState: { $ifNull: ["$entityPrimaryAddress.state", "-"] },
          entityPrimaryAddressCity: { $ifNull: ["$entityPrimaryAddress.city", "-"] },
          entityPrimaryAddressLine1: { $ifNull: ["$entityPrimaryAddress.addressline1", "-"] },
          entityPrimaryAddressLine2: { $ifNull: ["$entityPrimaryAddress.addressline2", "-"] },
          entityPrimaryAddressName: { $ifNull: ["$entityPrimaryAddress.addressName", "-"] },
          entityPrimaryAddressGoogle: { $ifNull: ["$userPrimaryAddress.formatted_address", {
            $concat:[
              { $ifNull: ["$entityPrimaryAddress.addressline1", ""] },
              { $cond:[{$eq:[{ $ifNull: ["$entityPrimaryAddress.addressline1", ""] },""]},""," "]},
              { $ifNull: ["$entityPrimaryAddress.state", ""] },
              { $cond:[{$eq:[{ $ifNull: ["$entityPrimaryAddress.state", ""] },""]},""," "]},
              { $ifNull: ["$entityPrimaryAddress.city", ""] },
              { $cond:[{$eq:[{ $ifNull: ["$entityPrimaryAddress.city", ""] },""]},""," "]},
              { $ifNull: ["$entityPrimaryAddress.zipCode.zip1", ""] }
            ]}]},

          entityPrimaryAddressGoogleUrl: { $ifNull: ["$entityPrimaryAddress.url", "-"] },
          userFlagsAggregated: {
            $reduce: {
              input: "$allentityusers.userFlags",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          associatedCusipsAggregated: {
            $reduce: {
              input: "$entityLinkedCusips.associatedCusip6",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          entityFlagsAggregated: {
            $reduce: {
              input: "$entityFlags.marketRole",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          issuerFlagsAggregated: {
            $reduce: {
              input: "$entityFlags.issuerFlags",
              initialValue: "",
              in: {
                $concat: [
                  "$$value",
                  {
                    $cond: {
                      if: { $eq: ["$$value", ""] },
                      then: " ",
                      else: ", "
                    }
                  },
                  "$$this"
                ]
              }
            }
          },
          userHavingContactFlags: {
            $setIntersection: [userContactTypesRevised, { $ifNull: ["$allentityusers.userFlags", []] }]
          },
          entityMarketRoleFlags: {
            $setIntersection: [entityMarketRoleFlags, { $ifNull: ["$entityFlags.marketRole", []] }]
          },
          entityIssuerTypeFlags: {
            $setIntersection: [entityIssuerFlags, { $ifNull: ["$entityFlags.issuerFlags", []] }]
          },
          userExistsForEntityNew: { $cond: [{ $gt: ["$allentityusers._id", null] }, true, false] },
        }
      },
      {
        $addFields: {
          accessibleUsers: { $in:["$userId",[...usersList]] },
          matchOnUserContactType: { $and: [{ $isArray: "$userHavingContactFlags" }, { $gt: [{ $size: "$userHavingContactFlags" }, 0] }] },
          matchOnEntityMarketRole: { $and: [{ $isArray: "$entityMarketRoleFlags" }, { $gt: [{ $size: "$entityMarketRoleFlags" }, 0] }] },
          matchOnIssuerTypeFlags: { $and: [{ $isArray: "$entityIssuerTypeFlags" }, { $gt: [{ $size: "$entityIssuerTypeFlags" }, 0] }] },
          userPrimaryPhoneNumber: { $ifNull: ["$userPrimaryPhoneNumber.phoneNumber", "-"] },
          matchOnStates: { $in: ["$entityPrimaryAddressState", [...entityPrimaryAddressStates]] },
          keySearchDetails: {
            $concat: [
              { $ifNull: ["$entityName", ""] },
              { $ifNull: ["$userFirstName", ""] },
              { $ifNull: ["$userLastName", ""] },
              { $ifNull: ["$userPrimaryEmail", ""] },
              { $ifNull: ["$entityPrimaryAddress.state", "-"] },
              { $ifNull: ["$entityPrimaryAddress.city", "-"] },
              { $ifNull: ["$entityPrimaryAddress.addressline1", "-"] },
              { $ifNull: ["$entityPrimaryAddress.addressline2", "-"] },
              { $ifNull: ["$entityPrimaryAddress.addressName", "-"] },
              { $ifNull: ["$associatedCusipsAggregated", "-"] },
              { $ifNull: ["$entityFlagsAggregated", "-"] },
              { $ifNull: ["$issuerFlagsAggregated", "-"] },
            ]
          },
        }
      },
      {
        $facet:{
          condition1:[
            {
              $match: {
                $or: [
                  {
                    $and: [
                      { userExistsForEntityNew: false },
                      ...matchQueryNoUserCase
                    ]
                  },
                  {
                    $and: [
                      ...matchQueryNew
                    ]
                  }
                ]
              }
            },
            {
              "$group" : {
                "_id" : "$entityId", 
                count:{$sum:1},
                data: { $push : "$$ROOT" }
              }
            },
            { "$project": {
              results: { "$slice": [ "$data", 1 ] }
            }},
            { "$unwind": "$results" },
            { "$replaceRoot": { "newRoot": "$results" } }
          ],
          condition2:[
            {
              "$group" : {
                "_id" : "$entityId", 
                isThereAMatchWithPrimary: { $sum: {$cond : [ "$matchOnUserContactType", 1, 0 ] }},
                data: { $push : "$$ROOT" }
              }
            },
            {
              $match:{isThereAMatchWithPrimary:0}
            },
            { "$project": {
              results: { "$slice": [ "$data", 1 ] }
            }},
            { "$unwind": "$results" },
            { "$replaceRoot": { "newRoot": "$results" } },
            {
              $match: {
                $and: [
                  ...matchQueryNoUserCase,
                  { userExistsForEntityNew: true },
                  { matchOnUserContactType:false} ,                 
                ]
              }
            },

          ],

        },
      },
      // { 
      //   "$project": {
      //     "data": { "$concatArrays": ["$condition1","$condition2"] }
      //   }},
      // { "$unwind": "$data" },
      // { "$replaceRoot": { "newRoot": "$data" } },
      // {
      //   $facet: {
      //     metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", size] } } } }],
      //     data: [{ $sort: { ...sortFields } }, { $skip: recordstoskip }, { $limit: size }] // add projection here wish you re-shape the docs
      //   }
      // }
    ]
    // if (!serverPerformPagination) {
    //   queryPipeline.splice(-1, 1)
    // }

    const pipeLineQueryResults = await EntityRel.aggregate([
      ...queryPipeline
    ])

    return { pipeLineQueryResults }
  } catch (e) {
    console.log(e)
    return { pipeLineQueryResults: [] }
  }
}

// getEntityDetailsForMasterLists({_id:ObjectID("5bf782cd9d1d1b20b40de56e"), entityId:ObjectID("5bf782cc9d1d1b20b40de52d")}).then( a=> console.log(JSON.stringify(a, null,2)))
// testingPipelineConcatenation({_id:ObjectID("5c224dde039040400ca16f6d"), entityId:ObjectID("5c224dd7039040400ca04e9b")}).then( a=> console.log(JSON.stringify(a, null,2)))

getEntityDetailsForMasterLists(
  {
    _id:ObjectID("5c2c7674dba832075ca32ef5"),
    entityId:ObjectID("5c2c766adba832075ca1f81b")
  },
  {
    "filters": {
      "entityTypes": [
        "Client","Prospect"
      ],
      "userContactTypes": [],
      "entityMarketRoleFlags": [],
      "entityIssuerFlags": [],
      "entityPrimaryAddressStates": [],
      "freeTextSearchTerm": ""
    },
    "pagination": {
      "serverPerformPagination": true,
      "currentPage": 0,
      "size": 10,
      "sortFields": {
        "entityName": 1
      }
    }
  }
).then(a => console.log(JSON.stringify(a, null, 2)))