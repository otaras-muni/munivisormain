import mongoose from "mongoose"
import {ObjectID} from "mongodb"

mongoose.Promise = global.Promise
mongoose.connect("mongodb://admin:admin123@ds133746.mlab.com:33746/munivisordevsearch")
// mongoose.connect("mongodb://admin:admin123@ds139929.mlab.com:39929/mvtrainingdev")
// mongoose.connect("mongodb://127.0.0.1:27017/mvtrainingdev")

import  {
  Deals,RFP,BankLoans,Derivatives, Entitiy, EntityUser, EntityRel
}  from "./../../appresources/models"


const getDataOrganizedByMarketRole = async (faEntityId) => {

  const ret = await EntityRel.aggregate([
    {
      $match:{entityParty1:faEntityId,relationshipType:"Third Party"}
    },
    {  
      $lookup:
      {
        from: "entities",
        localField: "entityParty2",
        foreignField: "_id",
        as: "thirdPartyDetails"
      }
    },
    {
      "$unwind":"$thirdPartyDetails"
    },
    {
      $project:{
        faEntityId:"$entityParty1",
        thirdPartyEntityId:"$thirdPartyDetails._id",
        thirdPartyMsrbFirmName:"$thirdPartyDetails.msrbFirmName",
        thidPartyFirmName:"$thirdPartyDetails.firmName",
        thidPartyFlags:"$thirdPartyDetails.entityFlags.marketRole",
      }
    },
    {
      "$unwind":"$thidPartyFlags"
    },
    {
      $lookup:
        {
          from: "entityusers",
          localField: "thirdPartyEntityId",
          foreignField: "entityId",
          as: "thirdPartyUserDetails"
        }
    },
    {
      $project:{
        faEntityId:1,
        thirdPartyEntityId:1,
        thirdPartyMsrbFirmName:1,
        thidPartyFirmName:1,
        thidPartyFlags:1,
        "thirdPartyUserDetails._id":1,
        "thirdPartyUserDetails.userFirstName":1,
        "thirdPartyUserDetails.userLastName":1,
        "thirdPartyUserDetails.userEmails":1,
        "thirdPartyUserDetails.userJobTitle":1,
        "thirdPartyUserDetails.userPhone":1,
        "thirdPartyUserDetails.userAddresses":1
      }
    },
    {
      $group:{
        _id:{thidPartyFlag:"$thidPartyFlags", faEntityId:"$faEntityId"},
        data:{$push:{
          thirdPartyEntityId:"$thirdPartyEntityId",
          thirdPartyMsrbFirmName:"$thirdPartyMsrbFirmName",
          thidPartyFirmName:"$thidPartyFirmName",
          thirdPartyUserDetails:"$thirdPartyUserDetails"
        }}
      }
    }
  ])

  return ret

}

getDataOrganizedByMarketRole(ObjectID("5b3e3803e93d941192965b06")).then( a=> console.log(JSON.stringify(a,null,2)))



