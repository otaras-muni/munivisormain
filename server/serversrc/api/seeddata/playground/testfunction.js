const dealParticipants = [{entityId:123,entityName:"New"},{entityId:123,entityName:"New"},{entityId:124,entityName:"New 1"}]

const findUniqueValues = dealParticipants.reduce( (uniqueValues, ent, index) => {
  if( index === 0 ) {
    return {[ent.entityId]:ent}
  } 
  if ( uniqueValues[ent.entityId] ) {
    return uniqueValues
  } 
  return {...uniqueValues,...{[ent.entityId]:ent} }
},{})

console.log(JSON.stringify(Object.values(findUniqueValues),null,2))

