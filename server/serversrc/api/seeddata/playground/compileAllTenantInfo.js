db.entityrels.aggregate([
  {"$match":{"entityParty1":ObjectId("5b1781dc609c022b98e2d549")}},
  {
    $group: {
      _id: "$entityParty1",
      thirdParties: {
        $push: {
          $cond: { 
            if: { $eq: [ "$relationshipType", "Third Party" ] }, 
            then: "$entityParty2" ,
            else: null,
          }
        }
      },
      clients: {
        $push: {
          $cond: { 
            if: { $eq: [ "$relationshipType", "Client" ] }, 
            then: "$entityParty2" ,
            else: null,
          }
        }
      }
    }
  },
  {
    $project: {
      "thirdParties": {
        "$filter": {
          input: "$thirdParties",
          as: "tp",
          cond: { $ne: [ "$$tp", null ] } 
        } 
      },
      "clients": {
        "$filter": {
          input: "$clients",
          as: "clt",
          cond: { $ne: [ "$$clt", null ] } 
        } 
      },         
  
    }
  }
  
])