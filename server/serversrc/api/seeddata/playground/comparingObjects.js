// Check if this is an object or an array
const isObject = (a)=> (!!a) && (a.constructor === Object)
const isArray = (a)=> (!!a) && (a.constructor === Array)

// Compare two arrays that have objects in them.
const compareTwoObjectArrays = (x,y,keyForChange) => {
  const finalResults = x.reduce(  (xacc,xobj, whichxobject) => {
    const diffs = y.reduce ( (yacc, yobj) => {
      const ykeys = Object.keys(yobj)
      const matches = ykeys.reduce ((accyval, ykey) => {
        const m = yobj[ykey] === xobj[ykey]
        return {...accyval,...{some:accyval.some || m, all:accyval.all && m}}
      },{some:false,all:true} )
      const { some, all} = matches
      let allmatch
      let somematch

      if( all ) {
        allmatch = yobj

      }
      else if ( some && !all) {
        somematch = yobj
      }
      else if ( !some && !all) {
        allmatch = null
        somematch = null
      }

      return {...yacc,...{
        some:yacc.some || matches.some,
        all:yacc.all || matches.all,
        allmatch: yacc.allmatch ? yacc.allmatch : allmatch,
        somematch:yacc.somematch ? yacc.somematch : somematch
      }}
    },{})

    // if all is true then don't have to include
    if (!diffs.all && diffs.some) {
      return {...xacc, ...{updated:[...xacc.updated,{ind:whichxobject,newValue:xobj,oldValue:diffs.somematch}]}}
    }
    else if ( !diffs.all && !diffs.some) {
      return {...xacc,
        ...{[keyForChange]:[...xacc[keyForChange],{ind:whichxobject,newValue:keyForChange==="added"? xobj:null,oldValue:keyForChange==="added"? null:xobj}]}}
    }
    return {...xacc}
  },{updated:[],added:[],deleted:[]})
  // console.log(JSON.stringify(finalResults,null,2))
  return finalResults
}

const arrayObjectComparison = (x,y) => {
  const res1 = compareTwoObjectArrays(x,y,"added")
  const res2 = compareTwoObjectArrays(y,x,"deleted")
  return{...res1,...{deleted:res2.deleted}}
}

// if this is a basic array of primitives then compare the values here
const basicArrayComparison =( x,y) => {
  const diffxy = x.reduce ((acc,xval) => {
    const {added} = acc
    if( !y.includes(xval)) {
      return {...acc,...{added:[...added,xval]}}
    }
    return {...acc}
  },{deleted:[], added:[]})

  const finalDiff = y.reduce ((acc,yval) => {
    const {deleted} = acc
    if( !x.includes(yval)) {
      return {...acc,...{deleted:[...deleted,yval]}}
    }
    return {...acc}
  },{...diffxy})
  return finalDiff
}


const compareObjectsAndCreateAuditLogs =(newObj,oldObj) => {
  const consolidatedChanges = []
  let parentKeys = ""
  
  const compareObjects = (newObj, oldObj) => {
    const keysNewObj = Object.keys(newObj)
    const keysOldObj = Object.keys(oldObj)
  
    // Get new keys that have been added
    // Get keys that have been modified
    // get keys that are the same and would need to be drilled into.
    const addKeys = keysNewObj.reduce ( (acc,v1) => {
      if( !keysOldObj.includes(v1) ) {
        consolidatedChanges.push({key: parentKeys ? `${parentKeys}.${v1}` : v1,operation:"added",oldValue:"",newValue:JSON.stringify(newObj[v1])})
        return  {...acc,...{[v1]:`Added value - ${newObj[v1]}`}}
      } 
      return acc
    },{})
    const remKeys = keysOldObj.reduce ( (acc,v1) => {
      if(!keysNewObj.includes(v1)) {
        consolidatedChanges.push({key: parentKeys ? `${parentKeys}.${v1}` : v1,operation:"removed",oldValue:JSON.stringify(oldObj[v1]),newValue:"",})
        return {...acc,...{[v1]:`Removed value - ${oldObj[v1]}`}}
      } 
      return acc
    },{})
      
    const sameKeys = keysOldObj.reduce ( (acc,v1) => keysNewObj.includes(v1) ? [...acc,v1] :[...acc],[])
  
    const loopKey = parentKeys
    const sameKeyDiffs = sameKeys.reduce( (accDiffs, sameKey) => {
      const newVal = newObj[sameKey]
      const oldVal = oldObj[sameKey]
  
      let diffValue = null
      let nonEmptyDifferences = null
      // case where the old and the new values are plain arrays with primitive types
      if( isArray(oldVal || []) && isArray(newVal || [])) {
        const isArrayOfObjects = oldVal.reduce( (arrOfObj,v) => isObject(v) && arrOfObj,true)
        if(isArrayOfObjects) {
          diffValue = arrayObjectComparison(newVal,oldVal)
  
          nonEmptyDifferences = Object.keys(diffValue).reduce( (nonEmpty, v) => {
            if(diffValue[v].length > 0) {
              const diffArray = diffValue[v]
              if(v === "added" ) {
                diffArray.forEach (da => {
                  consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"added",oldValue:"",newValue:JSON.stringify(da.newValue)})
                })
              }
              if(v=== "deleted") {
                diffArray.forEach (da => {
                  consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"removed",oldValue:JSON.stringify(da.oldValue),newValue:""})
                })            }
              if(v === "updated") {
                diffArray.forEach (da => {
                  consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"updated",oldValue:JSON.stringify(da.oldValue),newValue:JSON.stringify(da.newValue)})
                })            }
              return {...nonEmpty,...{[v]:diffValue[v]}}
            }
            return nonEmpty
          },{})
        }
        else {
          diffValue = basicArrayComparison(newVal,oldVal)
          nonEmptyDifferences = Object.keys(diffValue).reduce( (nonEmpty, v) => {
            if(diffValue[v].length > 0) {
              if(v === "added" ) {
                consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"added",oldValue:"",newValue:JSON.stringify(diffValue[v])})
              }
              if(v=== "deleted") {
                consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"removed",oldValue:JSON.stringify(diffValue[v]),newValue:""})
              }
              return {...nonEmpty,...{[v]:diffValue[v]}}
            }
            return nonEmpty
          },{})
        }
        const isEmptyDiffs = Object.keys(diffValue).reduce( (isEmpty, v) => diffValue[v].length === 0 && isEmpty, true)
  
        // if there are no differences then return the previous value.
        if( !isEmptyDiffs ) {
  
          const log = {[sameKey]: nonEmptyDifferences}
          return { ...accDiffs, ...log}
        }
        return { ...accDiffs}
      }
      // case where the old and the new values are objects
      if( isObject(oldVal || {}) && isObject(newVal || {})) {
        parentKeys = parentKeys ? `${parentKeys}.${sameKey}` : sameKey
        diffValue = compareObjects(newVal, oldVal)
        return { ...accDiffs, ...{[sameKey]: diffValue}}
      }
  
      // if it doesn't meet any of the conditions then it will fall into the default case
      if(newVal !== oldVal) {
        const log = {[sameKey]: `Changed value from ${oldVal} to ${newVal}`}
        consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"updated",oldValue:oldVal,newValue:newVal})
        return { ...accDiffs, ...log}
      }
      // if none of the conditions is met then just return the previous accumulator
      return accDiffs
  
    },{})
    return {added:addKeys, deleted:remKeys, updated:sameKeyDiffs}
  }
  const consolidatedDetailedLog = compareObjects(newObj,oldObj)
  return {logs:consolidatedChanges, detailedLogs:consolidatedDetailedLog}

}

const obj1 = { firstName:"Nave", lastName:"Balawat",testobj:{ a:1,c:{name:{f:"kapil"}},b:[4,3,45]}, salary:[1,2,4],testfield:[{x:1,y:2,z:3}],address:"address 1", toomuch:"asdkfjlasdkf"}
const obj2 = { firstName:"Naveen", lastName:"Bal",testobj:{ a:2,c:{name:{f:"adas"}},b:[4,3]}, salary:[2,4,6,7,8], testfield:[{x:1,y:2,z:4},{x:2,y:4}],address:"address 1", zipcode:"sajflkd", anotherKey:"asdjflkasdjf"}

console.log(JSON.stringify(compareObjectsAndCreateAuditLogs(obj1, obj2),null,2))
