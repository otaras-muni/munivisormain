import mongoose from "mongoose"
import {ObjectID} from "mongodb"
import  {
  Deals,RFP,BankLoans,Derivatives,EntityUser,Entity
}  from "./../../appresources/models"

mongoose.Promise = global.Promise
// mongoose.connect("mongodb://admin:admin123@ds133746.mlab.com:33746/munivisordevsearch")
// mongoose.connect("mongodb://admin:admin123@ds139929.mlab.com:39929/mvtrainingdev")
mongoose.connect("mongodb://52.91.222.27:27017/munivisor_dev")
// mongoose.connect("mongodb://127.0.0.1:27017/mvtrainingdev")


// Get Eligible Transactions for Logged In User
// For each Transaction accumulate a list of Entities that are part of that Transaction
// Find only those transactions where the entity is involved
// Identify attributes required for each type of transaction, get details after consulting design
// Attach the View / Edit flag for each eligible transaction using Javascript
// Provide result set for display

const queryCommonSectionsMapper =(tranType) => {
  const queryReference = {
    deals:[
      {
        $lookup:
              {
                from: "tranagencydeals",
                localField: "faEntityId",
                foreignField: "dealIssueTranClientId",
                as: "deals"
              }
      },
      {$unwind:{"path":"$deals", "preserveNullAndEmptyArrays": true}},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          dealTranId:"$deals._id",
          dealAssignees:"$deals.dealIssueTranAssignedTo",
          dealParticipants:"$deals.dealIssueParticipants"
        }
      },
      {$unwind:{"path":"$dealAssignees", "preserveNullAndEmptyArrays": true}},
      { $group:{
        _id:{userId:"$userId",tranId:"$dealTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" },
        "assignees": { $addToSet:  "$dealAssignees" },
        "dealParticipants":{"$addToSet":"$dealParticipants.dealPartContactId"},
      }},
      {$unwind:{"path":"$dealParticipants", "preserveNullAndEmptyArrays": true}},
      { $project: { _id:"$_id", users:{ $setUnion: [ "$assignees","$dealParticipants" ] } } }
    ],
    rfps: [
      {
        $lookup:
              {
                from: "tranagencyrfps",
                localField: "faEntityId",
                foreignField: "rfpTranClientId",
                as: "rfps"
              }
      },
      {$unwind:{"path":"$rfps", "preserveNullAndEmptyArrays": true}},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          rfpTranId:"$rfps._id",
          rfpAssignees:"$rfps.rfpTranAssignedTo",
          rfpEvaluationTeam:"$rfps.rfpEvaluationTeam",
          rfpProcessContacts:"$rfps.rfpEvaluationTeam",
          rfpParticipants:"$rfps.rfpParticipants"
        }
      },
      {$unwind:{"path":"$rfpAssignees", "preserveNullAndEmptyArrays": true}},
     
      { $group:{
        _id:{userId:"$userId",tranId:"$rfpTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" },
        "assignees": { $addToSet:  "$rfpAssignees" },
        "evalTeam":{"$addToSet":"$rfpEvaluationTeam.rfpSelEvalContactId"},
        "processTeam":{"$addToSet":"$rfpProcessContacts.rfpProcessContactId"},
        "participants":{"$addToSet":"$rfpParticipants.rfpParticipantContactId"},
      }},
      {$unwind:{"path":"$evalTeam", "preserveNullAndEmptyArrays": true}},
      {$unwind:{"path":"$processTeam", "preserveNullAndEmptyArrays": true}},
      {$unwind:{"path":"$participants", "preserveNullAndEmptyArrays": true}},
      { $project: { _id:"$_id", users:{ $setUnion: [ "$assignees","$evalTeam", "$processTeam","$participants" ] } } }
    ],
    bankloans: [
      {
        $lookup:
          {
            from: "tranbankloans",
            localField: "faEntityId",
            foreignField: "actTranFirmId",
            as: "bankloans"
          }
      },
      {$unwind:"$bankloans"},
      {$project:
          {
            userId:1,
            userRole:1,
            userEntitlement:1,
            entityId:1,
            faEntityId:1,
            "bankLoansTranId":"$bankloans._id",
            "bankLoansLeadAdvisor":"$bankloans.actTranFirmLeadAdvisorId",
            "bankLoansEvaluationTeam":"$bankloans.banLoanRfpEvaluationTeam", 
            "bankLoanParticipants":"$bankloans.bankLoanParticipants",
            "bankLoansProcessContactsTeam":"$bankloans.banLoanRfpProcessContacts",       
            "bankLoansRfpParticipants":"$bankloans.banLoanRfpParticipants"       
          }
      },
      {$unwind:{"path":"$bankLoansEvaluationTeam", "preserveNullAndEmptyArrays": true}},
      {$unwind:{"path":"$bankLoansProcessContactsTeam", "preserveNullAndEmptyArrays": true}},
      {$unwind:{"path":"$bankLoanParticipants", "preserveNullAndEmptyArrays": true}},
      {$unwind:{"path":"$bankLoansRfpParticipants", "preserveNullAndEmptyArrays": true}},
      { $group:{
        _id:{userId:"$userId",tranId:"$bankLoansTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement",bankLoansLeadAdvisor:"$bankLoansLeadAdvisor" },
        "blRfpEvalTeam":{"$addToSet":"$bankLoansEvaluationTeam.rfpSelEvalContactId"},
        "blRfpProcessTeam":{"$addToSet":"$bankLoansProcessContactsTeam.rfpProcessContactId"},
        "blRfpParticipants":{"$addToSet":"$bankLoansRfpParticipants.rfpParticipantContactId"},
        "blParticipants":{"$addToSet":"$bankLoanParticipants.partContactId"}
      }},
      { $project: { _id:"$_id", users:{ $setUnion: [ ["$_id.bankLoansLeadAdvisor"],"$blRfpEvalTeam","$blRfpProcessTeam", "$blRfpParticipants","$blParticipants" ] } } }
    ],
    derivatives:[    
      {
        $lookup:
          {
            from: "tranderivatives",
            localField: "faEntityId",
            foreignField: "actTranFirmId",
            as: "derivatives"
          }
      },
      {$unwind:{"path":"$derivatives", "preserveNullAndEmptyArrays": true}},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          "derivativesTranId":"$derivatives._id",
          "derivativesLeadAdvisor":"$derivatives.actTranFirmLeadAdvisorId",
          "derivativeParticipants":"$derivatives.derivativeParticipants.partContactId", 
        }
      },
      { $project: { _id:{loggedInUserId:"$userId",tranId:"$derivativesTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , users:{ $setUnion: [ ["$derivativesLeadAdvisor"],"$derivativeParticipants" ] } } }
    ],
    marfps:[
      {
        $lookup:
          {
            from: "actmarfps",
            localField: "faEntityId",
            foreignField: "actTranFirmId",
            as: "marfps"
          }
      },
      {$unwind:"$marfps"},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          "maRfpTranId":"$marfps._id",
          "maRfpLeadAdvisor":"$marfps.actLeadFinAdvClientEntityId",
          "maRfpParticipants":"$marfps.maRfpParticipants.partContactId", 
        }
      },
      { $project: { _id:{loggedInUserId:"$userId",tranId:"$maRfpTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , users:{ $setUnion: [ ["$maRfpLeadAdvisor"],"$maRfpParticipants" ] } } }
    ]
  }
  return queryReference[tranType]
}

// this is the new function that streamlines the way we get transactions for eligible users
export const getEligibleTransactionsByTypeForLoggedInUser = async(user, tranType) => {
  const customQueryPipelineStages = queryCommonSectionsMapper(tranType)
  console.log("....the user ID is", user)
  console.log("....the tran type is", tranType)
  console.log("...the mapped query is ", JSON.stringify(customQueryPipelineStages,null,2))
  const transactions = await EntityUser.aggregate([
    // Get the user
    {$match:{_id:ObjectID(user._id)}},
    // get the associated FA for the user
    {
      $lookup:
            {
              from: "entityrels",
              localField: "entityId",
              foreignField: "entityParty2",
              as: "matchedEntity"
            }
    },
    { $unwind : "$matchedEntity"},
    { $project : { userId:"$_id",userRole:1, userEntitlement:1, entityId:1, faEntityId:"$matchedEntity.entityParty1"}},
    ...customQueryPipelineStages,
    {$unwind:{"path":"$users", "preserveNullAndEmptyArrays": true}},
    {
      $lookup:
            {
              from: "entityusers",
              localField: "users",
              foreignField: "_id",
              as: "participantDetails"
            }
    },
    {$unwind:{"path":"$participantDetails", "preserveNullAndEmptyArrays": true}},
    {
      $project:{
        _id:1,users:1,
        loggedInUserFirmTran: {$cond: [ {"$eq": ["$participantDetails.entityId", "$_id.loggedInUserEntityId"]}, true, false ]},
        loggedInUserTran:{$cond: [ {"$eq": ["$participantDetails._id", "$_id.userId"]}, true, false ]},
        "userDetails.loggedInUserFirmTran": {$cond: [ {"$eq": ["$participantDetails.entityId", "$_id.loggedInUserEntityId"]}, true, false ]},
        "userDetails.loggedInUserTran": {$cond: [ {"$eq": ["$participantDetails._id", "$_id.userId"]}, true, false ]},
        "userDetails.participantUser": "$participantDetails._id",
        "userDetails.participantUserEntityId": "$participantDetails.entityId"
      }
    },
    // global-readonly ; global-edit ; tran-edit ; global-readonly-tran-edit ; tran-readonly
    {
      $project:{
        _id:1,
        loggedInUserFirmTran:1,
        loggedInUserTran:1,
        users:1,
        userDetails:1,
        canEditTran:{
          $switch:
            {
              branches: [
                {
                  case: { $and : [ { $eq : [ "$_id.userEntitlement","global-edit" ] },
                    { $eq:["$loggedInUserFirmTran",true]} ] },
                  then: true
                },
                {
                  case: { $and : [ { $eq : [ "$_id.userEntitlement","tran-edit" ] },
                    { $eq:["$loggedInUserTran",true]} ] },
                  then: true
                },
                {
                  case: { $and : [ { $eq : [ "$_id.userEntitlement","global-read-tran-edit" ] },
                    { $eq:["$loggedInUserTran",true]} ] },
                  then: true
                },
              ],
              default: false
            }
        },
        canViewTran:{
          $switch:
                {
                  branches: [
                    {
                      case: { $and : [ { $eq : [ "$_id.userEntitlement","global-edit" ] },
                        { $eq:["$loggedInUserFirmTran",true]} ] },
                      then: true
                    },
                    {
                      case: { $and : [ { $eq : [ "$_id.userEntitlement","tran-edit" ] },
                        { $eq:["$loggedInUserTran",true]} ] },
                      then: true
                    },
                    {
                      case: { $and : [ { $eq : [ "$_id.userEntitlement","global-read-tran-edit" ] },
                        { $eq:["$loggedInUserTran",true]} ] },
                      then: true
                    },
                    {
                      case: { $and : [ { $eq : [ "$_id.userEntitlement","global-readonly" ] },
                        { $eq:["$loggedInUserFirmTran",true]} ] },
                      then: true
                    },
                    {
                      case: { $and : [ { $eq : [ "$_id.userEntitlement","tran-readonly" ] },
                        { $eq:["$loggedInUserTran",true]} ] },
                      then: true
                    },
                  ],
                  default: false
                }
        }
      }
    },
    { $group:{
      _id:"$_id",
      canEditTran:{$addToSet:"$canEditTran"},
      canViewTran:{$addToSet:"$canViewTran"},
    }},
    {
      $project:{
        _id:1,
        loggedInUserId:"$_id.userId",
        loggedInUserEntityId:"$_id.loggedInUserEntityId",
        tranId:"$_id.tranId",
        canEditTran:{$anyElementTrue:"$canEditTran"},
        canViewTran:{$anyElementTrue:"$canViewTran"},
        filterRows:{ $or:[ {"$eq":[{$anyElementTrue:"$canViewTran"}, true]},{"$eq":[{$anyElementTrue:"$canEditTran"}, true]}] }
      }
    },
    // select only those rows where the flag is true
    { $match : { filterRows:true }},
    { $group:{
      _id:{loggedInUserId:"$loggedInUserId",loggedInUserEntityId:"$loggedInUserEntityId"},
      transactions:{ $push:{tranId:"$tranId",canEditTran:"$canEditTran",canViewTran:"$canViewTran"}}
    }},
    {
      $project:{
        loggedInUserId:"$_id.loggedInUserId",
        loggedInUserEntityId:"$_id.loggedInUserEntityId",
        transactions:1,
        _id:0
      }
    }
  ])

  return transactions[0]
}

export const getEligibleTransactionsForLoggedInUserRevised = async(user) => {
  const {_id:loggedInUserId,entityId:loggedInUserEntityId} = user

  try {
    // const tranTypes = ["deals","rfps", "bankloans", "derivatives", "marfps"]
    const tranTypes = ["deals","marfps"]
    const allOverallTranData = await (tranTypes || []).reduce( async (allEligibleTranTypes,tranType,ind) => {
      const tranData = await getEligibleTransactionsByTypeForLoggedInUser(user, tranType)
      const {transactions} = await tranData || {transactions:[]}
      const accumulatedTranData = await allEligibleTranTypes
      const allFlatTrans = accumulatedTranData.allTrans || []
      // console.log(JSON.stringify({tranType,accumulatedTranData},null,2))
      if(ind === 0) {
        return Promise.resolve({loggedInUserId,loggedInUserEntityId,...{[tranType]:transactions,allTrans:[...allFlatTrans,...transactions]}})
      }
      return Promise.resolve({...accumulatedTranData,...{[tranType]:transactions,allTrans:[...allFlatTrans,...transactions]}})
    },Promise.resolve({}))
  
    return {status:"success",message:`Successfully got entitled transactions for user  - ${user._id}`, data:allOverallTranData}
  } catch (e) 
  {
    console.log( "Error while fetch eligible transactions for users", e)
    return {status:"fail",message:`there was an error fetching data for user - ${user._id}`, data:undefined}
  }
}

export const checkTranEligiblityForLoggedInUser = async(user, lookupTranId) => {
  console.log("I ENTERED THE FUNCTION",user,lookupTranId)
  try {
    const {data} = await getEligibleTransactionsForLoggedInUserRevised(user)
    const {allTrans} = data
    const tranExists = (allTrans || []).filter( ({tranId}) => tranId.toString() === lookupTranId.toString() )
    console.log("======tranExists",tranExists)
  
    if( tranExists.length > 0) {
      return {
        loggedInUserId: user._id,
        loggedInUserEntityId: user.entityId,
        ...tranExists[0]
      }
    }
  }  catch (error) {
    console.log(error)
    return {
      loggedInUserId: user._id,
      loggedInUserEntityId: user.entityId,
      tranId:lookupTranId,
      canEditTran:false,
      canViwTran:false
    } 
  }
  return {
    loggedInUserId: user._id,
    loggedInUserEntityId: user.entityId,
    tranId:lookupTranId,
    canEditTran:false,
    canViwTran:false
  }  
}

getEligibleTransactionsForLoggedInUserRevised({_id:"5b6064db8c4edb1624d8832d",entityId:"5b6064d88c4edb1624d87f77"})
  .then( (d) => console.log(JSON.stringify(d,null,2)))
// getEligibleTransactionsByTypeForLoggedInUser({_id:"5b6064db8c4edb1624d8832d"},"marfps")
//   .then( (d) => console.log(JSON.stringify(d,null,2)))

