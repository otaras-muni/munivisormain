import _ from "lodash"
import DROPDOWN from "./globaldropdown"


const randomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const getRandomLookup = lookupKey => {
  const len = DROPDOWN[lookupKey].length
  return DROPDOWN[lookupKey][randomInt(0, len - 1)].value
}

const getRandomLookupArray = lookupArray => {
  const [a] = _.sampleSize(lookupArray, 1)
  return a
}

exports.randomInt = randomInt
exports.getRandomLookup = getRandomLookup
exports.getRandomLookupArray = getRandomLookupArray
