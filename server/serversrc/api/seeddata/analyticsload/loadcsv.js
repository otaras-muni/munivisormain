import mongoose, {Schema} from "mongoose"

const csv = require("fast-csv")
const path = require("path")



mongoose.connect("mongodb://admin:admin123@ds147030.mlab.com:47030/munivisoranalytics")

// eslint-disable-next-line no-unused-vars
export const debtStructureSchema = Schema({
  _id: { type: Schema.ObjectId, required: true},
  index:Number,
  delDate:String,
  matDate:String,
  callDate:String,
  coupon:Number,
  yield:Number
})

// eslint-disable-next-line no-unused-vars
const DebtStructure =  mongoose.model("debtstructure", debtStructureSchema)

// Define the Model
// create connection
// Insert the data one by one into MongoDB
// Send the details of the connection to Bob
const debtstructure = []
 
csv
  .fromPath(path.resolve("./server/serversrc/api/seeddata/analyticsload/debtStructureFunctionInput.csv"),{headers: true})
  .on("data", (data) => {
    console.log(data)
    data._id = new mongoose.Types.ObjectId()
    debtstructure.push(data)
  })
  .on("end", async () => {
    try {
      console.log("done", debtstructure)
      await DebtStructure.create(debtstructure)
    } catch (error) {
      console.log("There is an error",error)
    }
  })