import faker from "faker"
import { isNullOrUndefined } from "util"
import _ from "lodash"
import mongoose from "mongoose"
import {ObjectID} from "mongodb"
import helpers from "./../testdatareference/testhelpers"
import {
  EntityRel,
  Entity,
  EntityUser,
  RFP
} from "./models"

import {rfpBidCheckList, rfpEvaluationChecklist} from "./checklistreference"


const yesNoRef = [true, false]
const tranPurposeOfRequest = [ "Underwriting Services","Arbitrage Services","Bond Counsel"]
const docStatus = [ "Submitted for Review","Finalized Document","Not reviewed"]


const getServiceProviderEmails = async (serviceProviderList) => {

  const emailsToBeReturnd = await Promise.all (serviceProviderList.map( async ({entityParty2}) => {
    const {_id:thirdPartyId} = entityParty2
    // User Emails for the third party entities
    const emailsResponse = await EntityUser.find({entityId:thirdPartyId}).select({ userEmails: 1 })
    // Select some random set of emails for the selected entities
    const selectedUserEmails = _.sampleSize(emailsResponse[0].userEmails, Math.ceil(Math.random() * emailsResponse[0].userEmails.length))
    // Get a random set of emails from this List
    return { thirdParty:entityParty2,thirdPartyEmails:selectedUserEmails}
  }))

  return emailsToBeReturnd
}

// Ideally this should take two entities, the MSRB participant type and return a something that can be used else where.
const findReferenceEntities = async () => {
  let returnObject
  try {
    // Get all the financil advisors
    const financialAdvisors = await  Entity.find({ msrbRegistrantType: "Municipal Advisor", isMuniVisorClient: true}).select({ msrbRegistrantType: 1, msrbFirmName: 1 })
    // Choose one finfinancialAdvisorsancial advisor randomly

    const {_id:selFinAdvisor} = helpers.getRandomLookupArray(financialAdvisors)
    // Now Find the clients attached to the financial advisor
    const issuerClients = await  EntityRel.find({ entityParty1: selFinAdvisor, relationshipType:"Third Party"}).select({ entityParty1: 1, entityParty2: 1 })
    // Pick an issuer Client randomly
    const {entityParty2:selIssuerClient} = helpers.getRandomLookupArray(issuerClients)
    // Pick entities that provide a specific type of service and those that are related to the third party
    // The registrant type will be an input to the query
    const matchedEntities = await EntityRel
      .find({ "entityParty1": mongoose.Types.ObjectId(selFinAdvisor),"relationshipType":"Third Party"})
      .populate({ path: "entityParty1", model: Entity,select:"msrbFirmName" })
      .populate({ path: "entityParty2", model: Entity,select:"msrbFirmName businessStructure" })

    // Filter out those that don't satisfy the join query. this will be the list of firms that provide the desired set of services
    const filteredServiceProviders = matchedEntities.filter( ent => !isNullOrUndefined(ent.entityParty2))

    // Now find the Users of the Financial Advisor
    const faUsers = await EntityUser.find({entityId:selFinAdvisor})

    // Find a FA User Randomly and get his email ID
    const selectedFaUsers = helpers.getRandomLookupArray(faUsers)
    // Find the Users of the Issuer Entity
    const faReturnObject = {finAdvisor:selFinAdvisor,finAdvUserEmails:selectedFaUsers.userEmails}

    const issuerUsers = await EntityUser.find({entityId:selIssuerClient})
    // Find the Users of each available service provider
    const selectedIssuerUsers = helpers.getRandomLookupArray(issuerUsers)

    const issuerReturnObject = {issuer:selIssuerClient,issuerUserEmails:selectedIssuerUsers.userEmails}

    const serviceProviderUsers = await getServiceProviderEmails(filteredServiceProviders)

    returnObject = {...faReturnObject,...issuerReturnObject,serviceProviderUsers}

  } catch (e) {
    console.log(e)
  }
  return returnObject
}

const getClientDetailsForFinAdvisor = async (finAdvisor) => {
  let aggReturn
  try{
    aggReturn = await EntityRel.aggregate([
      { $match: { relationshipType:"Client",entityParty1:ObjectID(finAdvisor)} },
      { $project : {"entityParty1":1,"entityParty2":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty1",
          "foreignField": "_id",
          "as": "faDetails"
        }
      },
      { $unwind : "$faDetails"},
      { $project : {"entityParty2":1,"faDetails.msrbFirmName":1,"faDetails._id":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "clientDetails"
        }
      },
      { $project : {"faDetails.msrbFirmName":1,"faDetails._id":1,"clientDetails.msrbFirmName":1,"clientDetails._id":1,"clientDetails.entityFlags":1}},
      // { $match : { "clientDetails.entityFlags.marketRole":"Rebate Services"}},
      { $unwind : "$clientDetails"},
      { "$group": {
        "_id": "$faDetails",
        "clientDetails": { "$push": "$clientDetails" }
      }},
      { $project : {_id:0,faDetails:"$_id",clientDetails:1}}
    ])
  } catch(e) {
    console.log("There is an error",e)
  }
  return aggReturn[0]
}

const getThirdPartiesOfSpecificType = async (finAdvisorId, serviceType) => {
  let aggReturn
  try{
    aggReturn = await EntityRel.aggregate([
      { $match: { relationshipType:"Third Party",entityParty1:ObjectID(finAdvisorId)} },
      { $project : {"entityParty1":1,"entityParty2":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty1",
          "foreignField": "_id",
          "as": "faDetails"
        }
      },
      { $unwind : "$faDetails"},
      { $project : {"entityParty2":1,"faDetails.msrbFirmName":1,"faDetails._id":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "thirdPartyDetails"
        }
      },
      { $project : {"faDetails.msrbFirmName":1,"faDetails._id":1,"thirdPartyDetails.msrbFirmName":1,"thirdPartyDetails._id":1,"thirdPartyDetails.entityFlags":1}},
      { $match : { "thirdPartyDetails.entityFlags.marketRole":serviceType}},
      { $project : {"thirdPartyDetails.entityFlags":0}},
      { $unwind : "$thirdPartyDetails"},
      { "$group": {
        "_id": "$faDetails",
        "thirdPartyDetails": { "$push": "$thirdPartyDetails" }
      }},
      { $project : {_id:0,faDetails:"$_id",thirdPartyDetails:1}}
    ])
  } catch(e) {
    console.log("There is an error",e)
  }
  return aggReturn[0]
}

export const generateRFPData = async () => {
  let rfpObject
  try {
    // Get all the financil advisors
    const financialAdvisors = await  Entity.find({ msrbRegistrantType: "Municipal Advisor", isMuniVisorClient: true}).select({ msrbRegistrantType: 1, msrbFirmName: 1 })
    // Choose one finfinancialAdvisorsancial advisor randomly
    const {_id:selFinAdvisor} = helpers.getRandomLookupArray(financialAdvisors)
    // Get the Users for the financial Advisor
    const faUsers = await EntityUser.find({entityId:selFinAdvisor}).select({ userFirstName: 1, userLastName: 1,userEmails:1, entityId:1 })
    // Get the Issuer and the Users of the Issuer
    const issuerClients = await  EntityRel.find({ entityParty1: selFinAdvisor, relationshipType:"Client"}).select({ entityParty1: 1, entityParty2: 1 })
    // Pick an issuer Client randomly
    const {entityParty2:selIssuerClient} = helpers.getRandomLookupArray(issuerClients)
    // Pick the Issuer Users
    const issuerUsers = await EntityUser.find({entityId:selIssuerClient}).select({ userFirstName: 1, userLastName: 1,userEmails:1, entityId:1 })
    // Selection committee user List
    const faIssuerConsolidatedUsers = [...issuerUsers,...faUsers]


    const reducedEvalUsers = faIssuerConsolidatedUsers.reduce( (finalArray, user) => {
      const {_id:userId, entityId} = user
      const {emailId} = helpers.getRandomLookupArray(user.userEmails)

      const userObject = {
        rfpSelEvalMemberEntityId:entityId,
        rfpSelEvalContactId:userId,
        rfpSelEvalRole:"Need to finalize this",
        rfpSelEvalContactName: "user.userFirstName - user.userLastName",
        rfpSelEvalEmail: emailId,
        rfpSelEvalPhone: "123-456-9999",
        rfpSelEvalAddToDL: helpers.getRandomLookupArray(yesNoRef),
      }

      return [...finalArray, userObject]
    },[])

    const rfpEvalUsers = _.sampleSize(reducedEvalUsers, Math.ceil(Math.random() * reducedEvalUsers.length))
    // Select a subset of users for the evaluation committee
    const reducedProcessUsers = faIssuerConsolidatedUsers.reduce( (finalArray, user) => {
      const {_id:userId, entityId} = user
      const {emailId} = helpers.getRandomLookupArray(user.userEmails)
      const userObject = {
        rfpProcessMemberEntityId:entityId,
        rfpProcessContactId:userId,
        rfpProcessRole:"Need to finalize this",
        rfpProcessContactName: "user.userFirstName - user.userLastName",
        rfpProcessEmail: emailId,
        rfpProcessPhone: "123-456-9999",
        rfpProcessAddToDL: helpers.getRandomLookupArray(yesNoRef),
      }
      return [...finalArray, userObject]
    },[])

    // Generate a sample set of Process Users
    const rfpProcessUsers = _.sampleSize(reducedProcessUsers, Math.ceil(Math.random() * reducedProcessUsers.length))

    // Get the supplier List from Mongo DB
    const rfpSupplierList = await getThirdPartiesOfSpecificType(selFinAdvisor,"Rebate Services")

    // Loop through the third party list
    const  rfpSupplierUserDetails = await rfpSupplierList.thirdPartyDetails.reduce ( async (thirdPartyUserList, thirdParty) => {
      const { _id:entityId,msrbFirmName} = thirdParty
      const users = await EntityUser.find({entityId}).select({ userEmails: 1, entityId:1,_id:1,userFirstName:1,userLastName:1  })
      const selectedUser = helpers.getRandomLookupArray(users)
      const selectedEmail = helpers.getRandomLookupArray(selectedUser.userEmails)
      const {_id:userId} = selectedUser
      const thirdPartyRfpDetails = {
        rfpParticipantFirmId:entityId,
        rfpParticipantFirmName: msrbFirmName, // May not need this
        rfpParticipantContactId:userId ,
        rfpParticipantContactName: `${selectedUser.userFirstName} - ${selectedUser.userLastName}`,
        rfpParticipantContactEmail: selectedEmail.emailId, // Same reasoning as above
        rfpParticipantContactPhone: "123-456-9999",
        rfpParticipantContactAddToDL: helpers.getRandomLookupArray(yesNoRef)
      }

      const resolvedThirdPartyList = await thirdPartyUserList
      return [ ...resolvedThirdPartyList,thirdPartyRfpDetails]

    },Promise.resolve([]))

    const generateBidPacketInfo = (n) => (_.times(n).map(() => ({
      rfpBidDocName: faker.system.commonFileName(),
      rfpBidDocType: "RFP",
      rfpBidDocStatus: helpers.getRandomLookupArray(docStatus),
      rfpBidDocAction: [
        { actionType:String, user:helpers.getRandomLookupArray(rfpEvalUsers.map(({rfpSelEvalMemberEntityId}) => rfpSelEvalMemberEntityId)), date:faker.date.past(0.2)},
      ],
      rfpBidDocUploadUser:helpers.getRandomLookupArray(rfpEvalUsers.map(({rfpSelEvalMemberEntityId}) => rfpSelEvalMemberEntityId))
    })))


    // Need to check with Kapil on the best way to model this
    const rfpBidCheckListDetails = rfpBidCheckList.map ((item) => {
      const checkListItems = item.checkListItems.map ( (itemDesc) => ({
        rfpListItemDetails: itemDesc,
        rfpListItemConsider: helpers.getRandomLookupArray(yesNoRef),
        rfpListItemPriority: helpers.getRandomLookupArray(["High","Med","Low"]),
        rfpListItemEndDate: faker.date.past(0.2),
        rfpListItemResolved: helpers.getRandomLookupArray(yesNoRef),
        rfpListItemAssignees: [
          {
            rfpTeamAssigneeId: helpers.getRandomLookupArray(rfpEvalUsers.map(({rfpSelEvalMemberEntityId}) => rfpSelEvalMemberEntityId))
          }
        ]
      }))
      return {
        checkListCategoryId:item.checkListCategoryId,
        checkListItems
      }
    })

    const rfpEvaluateDetails = rfpSupplierUserDetails.map( ({rfpParticipantFirmId}) => rfpEvalUsers.map( (mem) => ({
      rfpParticipantFirmId,
      rfpEvaluationCommitteeMemberId: mem.rfpSelEvalContactId,
      rfpEvaluationCategories:rfpEvaluationChecklist.map ((item) => {
        const categoryItemsDetails = item.categoryItems.map ( (itemDesc) => ({
          evalItem: itemDesc,
          evalPriority: helpers.getRandomLookupArray(["High","Med","Low"]),
          evalRating:  helpers.getRandomLookupArray([1,2,3,4,5])
        }))
        return {
          categoryName:item.categoryName,
          categoryItems:categoryItemsDetails
        }
      })
    })))

    const rfpEvaluationAggregations = rfpSupplierUserDetails.map(({
      rfpParticipantFirmId
    }) => ({
      rfpParticipantFirmId,
      rfpEvaluationCategories: rfpEvaluationChecklist.map((item) => ({
        categoryName: item.categoryName,
        categoryItems: Math.random() * 150
      }))
    }))

    const rfpFinalSelectionDetails = ({
      rfpParticipantFirmId:helpers.getRandomLookupArray(rfpSupplierUserDetails.map(({rfpParticipantFirmId}) => rfpParticipantFirmId)),
      rfpFinalContractDocuments: [{
        rfpFinalContractName:"Final RFP Contract",
        rfpFinalContractLocation:"AWS:123",
        rfpFinalContractDocType:"Contract"
      }]
    })

    const rfpTransaction = {
      rfpTransactionId:new ObjectID(),
      rfpTranClientId:selFinAdvisor , // this is the financial advisor who is the client of munivisor
      rfpTranIssuerId: selIssuerClient, // this is the issuer client
      rfpTranName:`TXRFP${Math.random()*10000000}`,
      rfpTranType: "Manage RFP for Client",
      rfpTranPurposeOfRequest: helpers.getRandomLookupArray(tranPurposeOfRequest),
      rfpTranAssignedTo: helpers.getRandomLookupArray(faUsers.map( ({_id:userId}) => userId)), // There can be a combination of users from financial advisors and issuer users
      rfpTranState: helpers.getRandomLookupArray(["NJ","FL","NY"]),
      rfpTranCounty: helpers.getRandomLookupArray(["Middlesex","Monmouth","Bergen","Mercer"]),
      rfpTranPrimarySector: helpers.getRandomLookupArray(["General Obligation","Education","Infrastructure"]),
      rfpTranSecondarySector: helpers.getRandomLookupArray(["Tax","Parks","Charter Schools","Toll ways","Upkeep"]),
      rfpTranDateHired: faker.date.past(0.5),
      rfpTranStartDate: faker.date.past(0.3),
      rfpTranExpectedEndDate: faker.date.past(0.1),
      rfpTranStatus:"New",
      // rfpEvaluationTeam:rfpEvalUsers,
      // rfpProcessContacts:rfpProcessUsers,
      // rfpParticipants:rfpSupplierUserDetails,
      // rfpBidDocuments:generateBidPacketInfo(3),
      // rfpBidCheckList:rfpBidCheckListDetails,
      // // rfpParticipantQuestions:[rfpParticipantQuestionsSchema],
      // rfpMemberEvaluations:rfpEvaluateDetails,
      // rfpFinalEvaluations:rfpEvaluationAggregations,
      // rfpFinalSelections:rfpFinalSelectionDetails
    }

    rfpObject = rfpTransaction
  }

  catch(err) {
    console.log("There was an error")
  }

  return new RFP(rfpObject)
}


export const generateRFPDataWithParameters = async ({fa,issuer,serviceType}) => {
  let rfpObject
  try {
    // Choose one finfinancialAdvisorsancial advisor randomly
    const {_id:selFinAdvisor} = fa
    // Get the Users for the financial Advisor
    const faUsers = await EntityUser.find({entityId:selFinAdvisor}).select({ userFirstName: 1, userLastName: 1,userEmails:1, entityId:1 })
    // Get the Issuer and the Users of the Issuer
    const {_id:selIssuerClient} = issuer
    // Pick the Issuer Users
    const issuerUsers = await EntityUser.find({entityId:selIssuerClient}).select({ userFirstName: 1, userLastName: 1,userEmails:1, entityId:1 })
    // Selection committee user List
    const faIssuerConsolidatedUsers = [...issuerUsers,...faUsers]


    const reducedEvalUsers = faIssuerConsolidatedUsers.reduce( (finalArray, user) => {
      const {_id:userId, entityId} = user
      const {emailId} = helpers.getRandomLookupArray(user.userEmails)

      const userObject = {
        rfpSelEvalMemberEntityId:entityId,
        rfpSelEvalContactId:userId,
        rfpSelEvalRole:"Need to finalize this",
        rfpSelEvalContactName: "user.userFirstName - user.userLastName",
        rfpSelEvalEmail: emailId,
        rfpSelEvalPhone: "123-456-9999",
        rfpSelEvalAddToDL: helpers.getRandomLookupArray(yesNoRef),
      }
      return [...finalArray, userObject]
    },[])

    const rfpEvalUsers = _.sampleSize(reducedEvalUsers, Math.ceil(Math.random() * reducedEvalUsers.length))
    // Select a subset of users for the evaluation committee
    const reducedProcessUsers = faIssuerConsolidatedUsers.reduce( (finalArray, user) => {
      const {_id:userId, entityId} = user
      const {emailId} = helpers.getRandomLookupArray(user.userEmails)
      const userObject = {
        rfpProcessMemberEntityId:entityId,
        rfpProcessContactId:userId,
        rfpProcessRole:"Need to finalize this",
        rfpProcessContactName: "user.userFirstName - user.userLastName",
        rfpProcessEmail: emailId,
        rfpProcessPhone: "123-456-9999",
        rfpProcessAddToDL: helpers.getRandomLookupArray(yesNoRef),
      }
      return [...finalArray, userObject]
    },[])

    // Generate a sample set of Process Users
    const rfpProcessUsers = _.sampleSize(reducedProcessUsers, Math.ceil(Math.random() * reducedProcessUsers.length))

    // Get the supplier List from Mongo DB
    const rfpSupplierList = await getThirdPartiesOfSpecificType(selFinAdvisor,serviceType)

    // Loop through the third party list
    const  rfpSupplierUserDetails = await rfpSupplierList.thirdPartyDetails.reduce ( async (thirdPartyUserList, thirdParty) => {
      const { _id:entityId,msrbFirmName} = thirdParty
      const users = await EntityUser.find({entityId}).select({ userEmails: 1, entityId:1,_id:1,userFirstName:1,userLastName:1  })
      const selectedUser = helpers.getRandomLookupArray(users)
      const selectedEmail = helpers.getRandomLookupArray(selectedUser.userEmails)
      const {_id:userId} = selectedUser
      const thirdPartyRfpDetails = {
        rfpParticipantFirmId:entityId,
        rfpParticipantFirmName: msrbFirmName, // May not need this
        rfpParticipantContactId:userId ,
        rfpParticipantContactName: `${selectedUser.userFirstName} - ${selectedUser.userLastName}`,
        rfpParticipantContactEmail: selectedEmail.emailId, // Same reasoning as above
        rfpParticipantContactPhone: "123-456-9999",
        rfpParticipantContactAddToDL: helpers.getRandomLookupArray(yesNoRef)
      }

      const resolvedThirdPartyList = await thirdPartyUserList
      return [ ...resolvedThirdPartyList,thirdPartyRfpDetails]

    },Promise.resolve([]))

    const generateBidPacketInfo = (n) => (_.times(n).map(() => ({
      rfpBidDocName: faker.system.commonFileName(),
      rfpBidDocType: "RFP",
      rfpBidDocStatus: helpers.getRandomLookupArray(docStatus),
      rfpBidDocAction: [
        { actionType:String, user:helpers.getRandomLookupArray(rfpEvalUsers.map(({rfpSelEvalMemberEntityId}) => rfpSelEvalMemberEntityId)), date:faker.date.past(0.2)},
      ],
      rfpBidDocUploadUser:helpers.getRandomLookupArray(rfpEvalUsers.map(({rfpSelEvalMemberEntityId}) => rfpSelEvalMemberEntityId))
    })))


    // Need to check with Kapil on the best way to model this
    const rfpBidCheckListDetails = rfpBidCheckList.map ((item) => {
      const checkListItems = item.checkListItems.map ( (itemDesc) => ({
        rfpListItemDetails: itemDesc,
        rfpListItemConsider: helpers.getRandomLookupArray(yesNoRef),
        rfpListItemPriority: helpers.getRandomLookupArray(["High","Med","Low"]),
        rfpListItemEndDate: faker.date.past(0.2),
        rfpListItemResolved: helpers.getRandomLookupArray(yesNoRef),
        rfpListItemAssignees: [
          {
            rfpTeamAssigneeId: helpers.getRandomLookupArray(rfpEvalUsers.map(({rfpSelEvalMemberEntityId}) => rfpSelEvalMemberEntityId))
          }
        ]
      }))
      return {
        checkListCategoryId:item.checkListCategoryId,
        checkListItems
      }
    })

    const rfpEvaluateDetails = rfpSupplierUserDetails.map( ({rfpParticipantFirmId}) => rfpEvalUsers.map( (mem) => ({
      rfpParticipantFirmId,
      rfpEvaluationCommitteeMemberId: mem.rfpSelEvalContactId,
      rfpEvaluationCategories:rfpEvaluationChecklist.map ((item) => {
        const categoryItemsDetails = item.categoryItems.map ( (itemDesc) => ({
          evalItem: itemDesc,
          evalPriority: helpers.getRandomLookupArray(["High","Med","Low"]),
          evalRating:  helpers.getRandomLookupArray([1,2,3,4,5])
        }))
        return {
          categoryName:item.categoryName,
          categoryItems:categoryItemsDetails
        }
      })
    })))

    const rfpEvaluationAggregations = rfpSupplierUserDetails.map(({
      rfpParticipantFirmId
    }) => ({
      rfpParticipantFirmId,
      rfpEvaluationCategories: rfpEvaluationChecklist.map((item) => ({
        categoryName: item.categoryName,
        categoryItems: Math.random() * 150
      }))
    }))

    const rfpFinalSelectionDetails = ({
      rfpParticipantFirmId:helpers.getRandomLookupArray(rfpSupplierUserDetails.map(({rfpParticipantFirmId}) => rfpParticipantFirmId)),
      rfpFinalContractDocuments: [{
        rfpFinalContractName:"Final RFP Contract",
        rfpFinalContractLocation:"AWS:123",
        rfpFinalContractDocType:"Contract"
      }]
    })

    const rfpTransaction = {
      rfpTransactionId:new ObjectID(),
      rfpTranClientId:selFinAdvisor , // this is the financial advisor who is the client of munivisor
      rfpTranIssuerId: selIssuerClient, // this is the issuer client
      rfpTranName:`RFP-${Math.random()*10000000}`,
      rfpTranType: "RFP Transaction",
      rfpTranPurposeOfRequest: helpers.getRandomLookupArray(tranPurposeOfRequest),
      rfpTranOtherTransactionType: String,
      rfpTranAssignedTo: helpers.getRandomLookupArray(faUsers.map( ({_id:userId}) => userId)), // There can be a combination of users from financial advisors and issuer users
      rfpTranState: helpers.getRandomLookupArray(["NJ","FL","NY"]),
      rfpTranCounty: helpers.getRandomLookupArray(["Middlesex","Monmouth","Bergen","Mercer"]),
      rfpTranPrimarySector: helpers.getRandomLookupArray(["Middlesex","Monmouth","Bergen","Mercer"]),
      rfpTranSecondarySector: helpers.getRandomLookupArray(["Middlesex","Monmouth","Bergen","Mercer"]),
      rfpTranDateHired: faker.date.past(0.5),
      rfpTranStartDate: faker.date.past(0.3),
      rfpTranExpectedEndDate: faker.date.past(0.1),
      rfpTranStatus:"New",
      rfpEvaluationTeam:rfpEvalUsers,
      rfpProcessContacts:rfpProcessUsers,
      rfpParticipants:rfpSupplierUserDetails,
      rfpBidDocuments:generateBidPacketInfo(3),
      rfpBidCheckList:rfpBidCheckListDetails,
      rfpMemberEvaluations:rfpEvaluateDetails,
      rfpFinalEvaluations:rfpEvaluationAggregations,
      rfpFinalSelections:rfpFinalSelectionDetails
    }

    rfpObject = rfpTransaction
  }

  catch(err) {
    console.log("There was an error rfp",err)
  }

  return new RFP(rfpObject)
}
// getClientDetailsForFinAdvisor("5b048c5aa9ef2a0cd45e1ce4").then(a => console.log(JSON.stringify(a,null,2)))
// getThirdPartiesOfSpecificType("5b048c5aa9ef2a0cd45e1ce4","Rebate Services").then(a =>  console.log(JSON.stringify(a, null, 2)))
// findReferenceEntities().then( fa => console.log(JSON.stringify(fa, null, 2)))

// generateRFPData().then(rfp => {
//   console.log(JSON.stringify(rfp,null,2))
//   rfp.save()
// })

// // _.times(10).forEach( async (ind) => {
// //   console.log("RFP Created - ", ind)
// //   const rfpData = await generateRFPData()
// //   await rfpData.save()
// // })
