import faker from "faker"
import _ from "lodash"
import {ObjectID} from "mongodb"
import helpers from "./../testdatareference/testhelpers"
import {
  EntityRel,
  Entity,
  EntityUser,
  Deals
} from "./models"

import {getThirdPartiesOfSpecificType,getThirdPartyDetails} from "./commonDbFunctions/findEntities"

const yesNoRef = [true, false]
const tranPurposeOfRequest = [ "Underwriting Services","Arbitrage Services","Bond Counsel"]
const docStatus = [ "Submitted for Review","Finalized Document","Not reviewed"]

export const generateDealData = async () => {
  let dealObject
  try {
    // Get all the financial  advisors
    const financialAdvisors = await  Entity.find({ msrbRegistrantType: "Municipal Advisor", isMuniVisorClient: true}).select({ msrbRegistrantType: 1, msrbFirmName: 1 })
    // Choose one finfinancialAdvisorsancial advisor randomly
    const {_id:selFinAdvisor} = helpers.getRandomLookupArray(financialAdvisors)
    // Get the Users for the financial Advisor
    const faUsers = await EntityUser.find({entityId:selFinAdvisor}).select({ userFirstName: 1, userLastName: 1,userEmails:1, entityId:1 })
    // Get the Issuer and the Users of the Issuer
    const issuerClients = await  EntityRel.find({ entityParty1: selFinAdvisor, relationshipType:"Client"}).select({ entityParty1: 1, entityParty2: 1 })
    // Pick an issuer Client randomly
    const {entityParty2:selIssuerClient} = helpers.getRandomLookupArray(issuerClients)
    // Pick the Issuer Users
    const issuerUsers = await EntityUser.find({entityId:selIssuerClient}).select({ userFirstName: 1, userLastName: 1,userEmails:1, entityId:1 })
    // Selection committee user List

    const dealUnderwriters = await getThirdPartyDetails(selFinAdvisor,"Underwriting Services")

    const  dealUnderWriterInfo = await dealUnderwriters.thirdPartyDetails.reduce ( async (thirdPartyUserList, thirdParty) => {
      const { _id:entityId,msrbFirmName} = thirdParty
      const dealUnderwritersDetails = {
        dealPartFirmId:entityId,
        dealPartType:"Underwriter",
        dealPartFirmName: msrbFirmName,
        roleInSyndicate:helpers.getRandomLookup("LKUPUNDERWRITERROLE"),
        liabilityPerc:Math.random() / 10,
        managementFeePerc:Math.random() / 10,
        dealPartContactAddToDL:helpers.getRandomLookupArray(yesNoRef),
      }
      const resolvedThirdPartyList = await thirdPartyUserList
      return [ ...resolvedThirdPartyList,dealUnderwritersDetails]

    },Promise.resolve([]))

    const dealParticipantList = await getThirdPartiesOfSpecificType(selFinAdvisor)
    const  dealParticipantUserDetails = await dealParticipantList.thirdPartyDetails.reduce ( async (thirdPartyUserList, thirdParty) => {
      const { _id:entityId,msrbFirmName,entityFlags} = thirdParty
      const users = await EntityUser.find({entityId}).select({ userEmails: 1, entityId:1,_id:1,userFirstName:1,userLastName:1  })
      const selectedUser = helpers.getRandomLookupArray(users)
      const selectedEmail = helpers.getRandomLookupArray(selectedUser.userEmails)
      const {_id:userId} = selectedUser
      const dealParticipantDetails = {
        dealPartFirmId: entityId,
        dealPartType:helpers.getRandomLookupArray(entityFlags.marketRole),
        dealPartFirmName: msrbFirmName, // May not need this
        dealPartContactId: userId,
        dealPartContactName: `${selectedUser.userFirstName} - ${selectedUser.userLastName}`,
        dealPartContactEmail: selectedEmail.emailId, // Same reasoning as above
        dealPartContactAddrLine1: faker.address.streetAddress(), // Same reasoning as above
        dealPartContactAddrLine2: faker.address.streetAddress(), // Same reasoning as above
        dealParticipantState: faker.address.state(), // Same reasoning as above
        dealPartContactPhone: faker.phone.phoneNumber(),
        dealPartContactAddToDL: helpers.getRandomLookupArray(yesNoRef)
      }
      const resolvedThirdPartyList = await thirdPartyUserList
      return [ ...resolvedThirdPartyList,dealParticipantDetails]

    },Promise.resolve([]))

    const generateDealRatingInfo = (n) => (_.times(n).map(() => ({
      seriesName:"SERIES NAME TO/DO",
      ratingAgencyName: helpers.getRandomLookupArray(["Moodys","Fitch","S & P"]),
      longTermRating: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
      longTermOutlook: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
      shortTermOutlook: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
      shortTermRating: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
    })))

    const generateCEPRatingInfo = (n) => (_.times(n).map(() => ({
      cepName: helpers.getRandomLookupArray(["Ambac","Assured Guaranty"]),
      cepType: helpers.getRandomLookupArray(["Enhancement Provider","Collateral"]),
      longTermRating: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
      longTermOutlook: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
      shortTermOutlook: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"]),
      shortTermRating: helpers.getRandomLookupArray(["A","AA","AAA","BBB","B","B+"])
    })))

    const generatePricingKeyInfo = (n=1) => (_.times(n).map(() => ({
      dealSeriesPrincipal: Math.random()*10000000/10,
      dealSeriesSecurityType: helpers.getRandomLookupArray(["REV","REFI"]),
      dealSeriesSecurity: _.sampleSize("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 9).join(""),
      dealSeriesSecurityDetails: `Security - ${_.sampleSize("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 9).join("")}`,
      dealSeriesDatedDate: faker.date.future(0.25),
      dealSeriesSettlementDate: faker.date.future(0.27),
      dealSeriesFirstCoupon: faker.date.future(0.5),
      dealSeriesPutDate: faker.date.future(6),
      dealSeriesRecordDate: faker.date.future(0.28),
      dealSeriesCallDate: faker.date.future(8),
      dealSeriesFedTax: "Tax Exempt",
      dealSeriesStateTax: "Tax Exempt",
      dealSeriesPricingAMT: helpers.getRandomLookupArray(yesNoRef),
      dealSeriesBankQualified: helpers.getRandomLookupArray(yesNoRef),
      dealSeriesAccrueFrom: "Settlement date",
      dealSeriesPricingForm: "Bearer",
      dealSeriesCouponFrequency: "Bi-annual",
      dealSeriesDayCount: "30/360",
      dealSeriesDateType: helpers.getRandomLookupArray(["Fixed","Floating"]),
      dealSeriesCallFeature: helpers.getRandomLookupArray(["Callable","Non Callable Option"]),
      dealSeriesCallPrice: (1-(Math.random() / 10))*100,
      dealSeriesInsurance: helpers.getRandomLookupArray(["Partial","Full"]),
      dealSeriesUnderwtiersInventory: helpers.getRandomLookupArray(yesNoRef),
      dealSeriesMinDenomination: Number,
      dealSeriesSyndicateStructure: "Not Applicable",
      dealSeriesGrossSpread: Math.random()*10000,
      dealSeriesEstimatedTakeDown: Math.random()*10000,
      dealSeriesInsuranceFee: Math.random()*10000,
    })))

    const generatePricingInformation = (n) => (_.times(n).map(() => ({
      term: helpers.getRandomLookupArray(["10 year fixed","Floating","30 Year Callable"]),
      maturityDate: faker.date.future(10),
      amount: Math.random()*10000000,
      coupon: Math.random() / 10,
      yield: Math.random() / 10,
      price: (1-(Math.random() / 10))*100,
      YTM: Math.random() / 10,
      cusip: _.sampleSize("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 9).join(""),
      callDate: faker.date.future(7),
      takeDown: Math.random() / 10,
      insured: helpers.getRandomLookupArray(yesNoRef),
      drop: helpers.getRandomLookupArray(yesNoRef)
    })))

    const generateSeriesInfo = (n) => (_.times(n).map(() => ({
      contactId:ObjectID(),
      seriesName: _.sampleSize("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 6).join(""),
      description:"Series Information",
      tag :"series tag",
      seriesRatings: generateDealRatingInfo(n),
      cepRatings: generateCEPRatingInfo(n),
      seriesPricingDetails: generatePricingKeyInfo(),
      seriesPricingData: generatePricingInformation(12)
    })))

    const generateDealDocumentInfo = (n) => (_.times(n).map(() => ({
      docCategory:helpers.getRandomLookupArray(["Correspondence","Disclosure"]),
      docSubCategory:helpers.getRandomLookupArray(["Sale","Legal"]),
      docType:helpers.getRandomLookupArray(["Official statement","Finale OS","CAFR","Financial Statement","Bio"]),
      docAWSFileLocation:`AWS:S3:${faker.system.commonFileName()}`,
      docFileName:faker.system.commonFileName(),
      markedPublic:{ publicFlag:true, publicDate:faker.date.recent()},
      sentToEmma: { emmaSendFlag:false, emmaSendDate:faker.date.recent()},
      LastUpdatedDate:Date.now()
    })))

    const dealTransaction = {
      dealIssueTranClientId:selFinAdvisor , // this is the financial advisor who is the client of munivisor
      dealIssueTranIssuerId: selIssuerClient, // this is the issuer client
      dealIssueBorrowerName: "Some Issuer",
      dealIssueGuarantorName: "Some Borrower",
      dealIssueTranName:`TXDEAL${Math.random()*10000000}`,
      dealIssueTranType: "Manage Deal/Issue for Client",
      dealIssueTranPurposeOfRequest: helpers.getRandomLookupArray(tranPurposeOfRequest),
      dealIssueTranAssignedTo: helpers.getRandomLookupArray(faUsers.map( ({_id:userId}) => userId)), // There can be a combination of users from financial advisors and issuer users
      dealIssueTranState: helpers.getRandomLookupArray(["NJ","FL","NY"]),
      dealIssueTranCounty: helpers.getRandomLookupArray(["Middlesex","Monmouth","Bergen","Mercer"]),
      dealIssueTranPrimarySector: helpers.getRandomLookupArray(["General Obligation","Education","Infrastructure"]),
      dealIssueTranSecondarySector: helpers.getRandomLookupArray(["Tax","Parks","Charter Schools","Toll ways","Upkeep"]),
      dealIssueTranDateHired: faker.date.past(0.5),
      dealIssueTranStartDate: faker.date.past(0.3),
      dealIssueTranExpectedEndDate: faker.date.past(0.1),
      dealIssueTranStatus:"New",

      dealIssueofferingType:helpers.getRandomLookupArray(["Negotiated","Competitive"]),
      dealIssueSecurityType:helpers.getRandomLookupArray(["Bond","Loan","Pooled Loan"]),
      dealIssueBankQualified:helpers.getRandomLookupArray(yesNoRef),
      dealIssueCorpType:helpers.getRandomLookupArray(["Municipal","Other"]),
      dealIssueParAmount:Math.random()*100000000,
      dealIssuePricingDate:faker.date.past(0.5),
      dealIssueExpAwardDate:faker.date.past(0.4),
      dealIssueActAwardDate:faker.date.past(0.35),
      dealIssueSdlcCreditPerc:Math.random()/10,
      dealIssueEstimatedRev:Math.random()*100000000 / 100,
      dealIssueUseOfProceeds:helpers.getRandomLookupArray(["New Money","Refunding"]),

      dealIssueUnderwriters:dealUnderWriterInfo,
      dealIssueParticipants:dealParticipantUserDetails,
      dealIssueSeriesDetails:generateSeriesInfo(4),
      dealIssueDocuments:generateDealDocumentInfo(5),
    }
    dealObject = dealTransaction
  }

  catch(err) {
    console.log("There was an error ", err)
  }

  return new Deals(dealObject)
}

// generateDealData().then(deal => {
//   console.log(JSON.stringify(deal,null,2))
//   deal.save()
// })
