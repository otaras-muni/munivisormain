import { loadChecklistsForAllEntities} from "./../generateAndLoadChecklists"

const loadChecklists = async () => {
  try {
    const returnedValue = await loadChecklistsForAllEntities()
    console.log("After Loading Checklists", returnedValue)
    return returnedValue
  }
  catch (e) {
    console.log("Unable to load picklists", e)
  }
  finally {
    console.log("end")
  }
}

loadChecklists().then(a => {
  console.log(a)
  process.exit(0)}
)
// loadPickValuesForMuniVisorClientsLatest().then( a=> console.log(a)).then( () => console.log("done"))

