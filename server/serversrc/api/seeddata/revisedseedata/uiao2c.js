const fs = require("fs")
const Papa = require("papaparse")

const input = require("./uiaccess/uiao.json")

const headers = ["entity-type","role","L0","L0val","L1","L1val","L2","L2val","L3","L3val"]

const processInput = (input) => {
  const outFile = `${__dirname  }/uiaccess/uiaccess.csv`
  fs.writeFileSync(outFile, Papa.unparse([headers]), "utf8")
  const l0Keys = Object.keys(input)
  l0Keys.forEach(k0 => {
    const l1Keys = Object.keys(input[k0].level1)
    if(l1Keys.length) {
      l1Keys.forEach(k1 => {
        const l2Keys = Object.keys(input[k0].level1[k1].level2)
        if(l2Keys.length) {
          l2Keys.forEach(k2 => {
            const l3Keys = Object.keys(input[k0].level1[k1].level2[k2].level3)
            if(l3Keys.length) {
              const l3Keys = Object.keys(input[k0].level1[k1].level2[k2].level3)
              if(l3Keys.length) {
                // console.log("found l3keys : ", l3Keys)
                l3Keys.forEach(k3 => {
                  const entityTypes3 = Object.keys(input[k0].level1[k1].level2[k2].level3[k3].entityTypes)
                  const rows3 = []
                  entityTypes3.forEach(t3 => {
                    const rows = input[k0].level1[k1].level2[k2].level3[k3].entityTypes[t3].map(d => [t3, d, k0, "1", k1, "1", k2, "1", k3, "1"])
                    rows3.push(...rows)
                  })
                  fs.appendFileSync(outFile, `\r\n${  Papa.unparse(rows3)}`, "utf8")
                })
              }
            } else {
              // console.log("only l2 : ", k2)
              const entityTypes2 = Object.keys(input[k0].level1[k1].level2[k2].entityTypes)
              // console.log("entityTypes2 : ", entityTypes2)
              const rows2 = []
              entityTypes2.forEach(t2 => {
                // console.log("entityTypes2 vals : ", t2, input[k0].level1[k1].level2[k2].entityTypes[t2])
                const rows = input[k0].level1[k1].level2[k2].entityTypes[t2].map(d => [t2, d, k0, "1", k1, "1", k2, "1", "", ""])
                // console.log("rows : ", rows)
                rows2.push(...rows)
              })
              // console.log("rows2 : ", rows2)
              fs.appendFileSync(outFile, `\r\n${  Papa.unparse(rows2)}`, "utf8")
            }
          })
        } else {
          // console.log("only l1 : ", k1)
          const entityTypes1 = Object.keys(input[k0].level1[k1].entityTypes)
          // console.log("entityTypes1 : ", entityTypes1)
          const rows1 = []
          entityTypes1.forEach(t1 => {
            // console.log("entityTypes1 vals : ", t1, input[k0].level1[k1].entityTypes[t1])
            const rows = input[k0].level1[k1].entityTypes[t1].map(d => [t1, d, k0, "1", k1, "1", "", "", "", ""])
            // console.log("rows : ", rows)
            rows1.push(...rows)
          })
          // console.log("rows1 : ", rows1)
          fs.appendFileSync(outFile, `\r\n${  Papa.unparse(rows1)}`, "utf8")
        }
      })
    } else {
      // console.log("only l0 : ", k0)
      const entityTypes0 = Object.keys(input[k0].entityTypes)
      // console.log("entityTypes0 : ", entityTypes0)
      const rows0 = []
      entityTypes0.forEach(t0 => {
        // console.log("entityTypes0 vals : ", t0, input[k0].entityTypes[t0])
        const rows = input[k0].entityTypes[t0].map(d => [t0, d, k0, "1", "", "", "", "", "", ""])
        // console.log("rows : ", rows)
        rows0.push(...rows)
      })
      // console.log("rows0 : ", rows0)
      fs.appendFileSync(outFile, `\r\n${  Papa.unparse(rows0)}`, "utf8")
    }
  })
}

processInput(input)
