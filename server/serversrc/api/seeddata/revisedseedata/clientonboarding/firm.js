export const firm = {
  firmName: "Ford And Associates",
  address: {
    "zipCode" : {
      "zip1" : "33609",
      "zip2" : ""
    },
    "addressLine1" : "109 S. MacDill Avenue",
    "formatted_address" : "109 S. MacDill Avenue, Tampa, Florida, 33609",
    "addressLine2" : "",
    "country" : "United States",
    "state" : "Florida",
    "city" : "Tampa"
  },
  phones: [
    {
      countryCode: "+1",
      phoneNumber: "317-208-9125",
      extension: "298820"
    }
  ],
  faxes: [
    {
      faxNumber: "(676) 003-0778 x091"
    }
  ],
  emails: [
    {
      emailId: "admin1@fordassocinc.com"
    }
  ],
  addressName : "Corporate Head Quarters"
}
