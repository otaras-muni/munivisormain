import mongoose from "mongoose"
import {
  Entity
} from "../models"

mongoose.Promise = global.Promise

const defaultParams = {
  firmAddOns: [],
  msrbRegistrantType: "Municipal Advisor",
  isMuniVisorClient: true,
  entityLinkedCusips:[],
  entityBorObl:[],
  contractReference: [],
  contracts: [],
  entityDocuments: [],
  entityAliases: []
}

export const generateEntity = (data) => {
  const { firmName, address, phones, faxes, emails, addressName, settings} = data
  const entityData = {
    firmName,
    msrbFirmName: firmName,
    settings,
    addresses: [{...address, isPrimary: true, isActive: true,
      officePhone: phones, officeFax: faxes, officeEmails: emails,addressName}]
  }
  const entity = new Entity({ ...entityData,
    ...defaultParams
  })
  return entity
}

export const generateTenantEntity = (data) => {
  const { firmName } = data

  data.msrbFirmName = firmName
  const entity = new Entity({ ...data,
    ...defaultParams
  })
  return entity
}
