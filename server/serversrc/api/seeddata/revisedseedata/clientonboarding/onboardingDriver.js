import { generateEntity } from "./generateTenant"
import { generateEntityUserData } from "./generateUser"
import { generatEntityRelData } from "../generateEntityRelationshipData"
import { generateConfigData } from "../generateConfigData"
import { loadPickValuesForOneFirm } from "../generateAndLoadPicklists1"
import { loadChecklistForOneFirm } from "../generateAndLoadChecklists1"
import { loadUIAccessInformationForOne } from "../generateAndLoadUIAccess"
import {sendOnboardingEmailForNewPlatformUser} from "../../../appresources/emails/email.controller"
import {createTenantIndex,elasticSearchNormalizationAndBulkInsert} from "../../../elasticsearch/esHelper"
import {generateConfigForEntities} from "./../../../appresources/config/generateConfigForDifferentEntityTypes"


export const onboardTenant = async (firm, users) => {
  const statusMessages = []
  
  try {

    const addTenant = (firm) => generateEntity(firm).save()
    const addRel = (entity) => generatEntityRelData(entity, entity,"Self").save()
    const addConfig = (entity) => generateConfigData(entity).save()
    const addUsers = async (entityId, firm, users) => {
      const len = users.length
      const insertedUsers = []
      const { firmName, address, phones, faxes } = firm
      for(let i = 0; i < len; i++) {
        const { userFirstName, userMiddleName, userLastName,
          userEntitlement, emails,userFlags } = users[i]
        const data = {
          entityId,
          userFirmName: firmName,
          userFirstName,
          userMiddleName,
          userLastName,
          userRole: userEntitlement,
          userEntitlement,
          address,
          phones,
          faxes,
          emails,
          userFlags
        }
        const user = await generateEntityUserData(data).save()
        insertedUsers.push(user)
      }
      return insertedUsers
    }

    console.log("*********In Onboarding************", JSON.stringify(users))
    const entity = await addTenant(firm)
    console.log("entity added", entity)
    statusMessages.push({"Entity Creation":"Success", entityId:entity._id})
    await addRel(entity)
    console.log("entity relationship added")
    statusMessages.push({"Entity Relationship Creation":"Success", entityId:entity._id})
    const configReturnData = await generateConfigForEntities([entity._id])
    statusMessages.push({"Configs - checklists, picklists creation":"Success"})

/*    await addConfig(entity)
    console.log("config added")
    statusMessages.push({"Entity Configuration - Resource access":"Success", entityId:entity._id})
    await loadPickValuesForOneFirm(entity)
    console.log("picklists added")
    statusMessages.push({"Entity Configuration - Picklist Addition":"Success", entityId:entity._id})
    await loadChecklistForOneFirm(entity)
    console.log("checklists added")
    statusMessages.push({"Entity Configuration - Checklists Addition":"Success", entityId:entity._id})
    await loadUIAccessInformationForOne(entity._id)
    console.log("ui access added")
    statusMessages.push({"Entity Configuration - UI access for tenant":"Success", entityId:entity._id})*/
    const insertedUsers = await addUsers(entity._id, firm, users)
   
    for (const i in insertedUsers) {
      const user = insertedUsers[i]
      const emailResponse = await sendOnboardingEmailForNewPlatformUser(user)
      console.log("Email Response", emailResponse.message)
      statusMessages.push({"User Creation":"Success","message":`Onboarding Email Sent for User - ${user._doc.userFirstName} - ${user._doc.userLastName}`, entityId:entity._id})
    }
    await createTenantIndex(entity._id)
    console.log("Creating the elastic search index for the tenant")
    statusMessages.push({"Elastic Search Index Creation for tenant":"Success", entityId:entity._id})
    // const { ids , esType, tenantId } = parameters

    await elasticSearchNormalizationAndBulkInsert({ids:[entity._id],esType:"entities", tenantId:entity._id})
    await elasticSearchNormalizationAndBulkInsert({ids:[...insertedUsers.map( ({_id}) => _id)],esType:"entityusers", tenantId:entity._id})

    statusMessages.push({"User and Entity Data Inserted into Elastic Search Index":"Success"})

    if (global.gc) {
      console.log("The garbage collector is enabled")
      global.gc()
    } else {
      console.warn("No GC hook! Start your program as `node --expose-gc file.js`.")
    }
    console.log("all users for the tenant have been added")
    return statusMessages
    // dbCon.close()
  } catch (err) {
    console.log("err in onboardTenant : ", err)
  }
}

// onboardTenant(firm, users)
