import {
  initAndReseedDataForAllTenants,
  initAndReseedDataOnlyForTenant
} from "./../../../elasticsearch/esHelper"

export const elasticSearchInit = async (tenantId) => {
  let initTenantInfo
  try {
    if( tenantId) {
      initTenantInfo = await initAndReseedDataOnlyForTenant(tenantId)
      return initTenantInfo
    } 
    initTenantInfo = await initAndReseedDataForAllTenants()
    return initTenantInfo
    
  } catch (e) {
    console.log("there is an error initiating the tenant Indexes", e)
    return false
  }
}