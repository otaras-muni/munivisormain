const csv = require("fast-csv")
const path = require("path")

export const getFileData = filePath => new Promise(( resolve, reject ) => {
  const rows = []
  csv.fromPath(path.resolve(filePath),
    {headers: true, objectMode: true})
    .on("data", (data) => {
      // console.log(data)
      rows.push(data)
    })
    .on("end", () => {
      resolve(rows)
    })
    .on("error", (err) => {
      console.log("Error in getting file data : ", err)
      reject(new Error("Error in getting file data"))
    })
})
