const eval1 = [
  {
    rfpParticipantFirmId:1,
    rfpEvaluationCommitteeMemberId: 1,
    rfpEvaluationCategories: [{
      categoryName:"Cat1",
      categoryItems: [
        {
          evalItem: "Cat1 - Item 1",
          evalPriority: "High",
          evalRating: 1
        },
        {
          evalItem: "Cat1 - Item 2",
          evalPriority: "Medium",
          evalRating: 2
        },
      ]
    }]
  },
  {
    rfpParticipantFirmId:1,
    rfpEvaluationCommitteeMemberId: 2,
    rfpEvaluationCategories: [{
      categoryName:"Cat1",
      categoryItems: [
        {
          evalItem: "Cat1 - Item 1",
          evalPriority: "High",
          evalRating: 2
        },
        {
          evalItem: "Cat1 - Item 2",
          evalPriority: "Medium",
          evalRating: 3
        },
      ]
    }]
  },
  {
    rfpParticipantFirmId:2,
    rfpEvaluationCommitteeMemberId: 1,
    rfpEvaluationCategories: [{
      categoryName:"Cat1",
      categoryItems: [
        {
          evalItem: "Cat1 - Item 1",
          evalPriority: "High",
          evalRating: 4
        },
        {
          evalItem: "Cat1 - Item 2",
          evalPriority: "Medium",
          evalRating: 3
        },
      ]
    }]
  },
  {
    rfpParticipantFirmId:2,
    rfpEvaluationCommitteeMemberId: 2,
    rfpEvaluationCategories: [{
      categoryName:"Cat1",
      categoryItems: [
        {
          evalItem: "Cat1 - Item 1",
          evalPriority: "High",
          evalRating: 4
        },
        {
          evalItem: "Cat1 - Item 2",
          evalPriority: "Medium",
          evalRating: 1
        },
      ]
    }]
  }
]

console.log(JSON.stringify(eval1,null,2))

db.tranagencyrfps.aggregate([
  {$match: {   _id: ObjectId("5b1a80a9e2280768657d370c") }},
  {$project: {"rfpMemberEvaluations" : 1}},
  {
        $unwind:"$rfpMemberEvaluations"

  },
  {
    $project: {
      "rfpParticipantFirmId": "$rfpMemberEvaluations.rfpParticipantFirmId",
      "rfpEvaluationCommitteeMemberId": "$rfpMemberEvaluations.rfpEvaluationCommitteeMemberId",
      "rfpEvaluationCategories": "$rfpMemberEvaluations.rfpEvaluationCategories"
    }
  },
   {
    $unwind: "$rfpEvaluationCategories"
  },
  {
    $project: {
      "rfpEvaluationCategories.categoryItems": 1,
      "rfpEvaluationCategories.categoryName": 1,
      "rfpParticipantFirmId": 1,
      "rfpEvaluationCommitteeMemberId": 1
    }
  },
 {
    $unwind: "$rfpEvaluationCategories.categoryItems"
  },
  {
    $addFields: {
      categoryItem: "$rfpEvaluationCategories.categoryItems.evalItem",
      categoryName: "$rfpEvaluationCategories.categoryName",
      categoryPriority: "$rfpEvaluationCategories.categoryItems.evalPriority",
      categoryItemRating: "$rfpEvaluationCategories.categoryItems.evalRating",
      revisedRating: {
        "$switch": {
          "branches": [{
              "case": {
                "$eq": ["High",
                  "$rfpEvaluationCategories.categoryItems.evalPriority"
                ]
              },
              "then": 3
            },
            {
              "case": {
                "$eq": ["Medium",
                  "$rfpEvaluationCategories.categoryItems.evalPriority"
                ]
              },
              "then": 2
            },
            {
              "case": {
                "$eq": ["Low",
                  "$rfpEvaluationCategories.categoryItems.evalPriority"
                ]
              },
              "then": 1
            },

          ],
          "default": -1
        }
      }
    }
  },
  {
    $project: {
      _id: 1,
      categoryItem: 1,
      rfpParticipantFirmId: 1,
      categoryName: 1,
      categoryPriority: 1,
      rfpEvaluationCommitteeMemberId: 1,
      categoryItemRating: 1,
      revisedRating: 1,
      consolidatedRating: {
        $multiply: ["$revisedRating", "$categoryItemRating"]
      }
    }
  },
  {
    $group: {
      _id: {
        participant: "$rfpParticipantFirmId",
        category: "$categoryName",
        consolidatedRating: "$consolidatedRating"
      },
      count: {
        $sum: 1
      },
    }
  },
  {
    $project: {
      participant: "$_id.participant",
      category: "$_id.category",
      consolidatedRating: "$_id.consolidatedRating",
      count: 1
    }
  },
  {
    $group: {
      _id: {
        participant: "$participant",
        category: "$category"
      }, // build any group key ypo need
      numerator: {
        $sum: {
          $multiply: ["$consolidatedRating", "$count"]
        }
      },
      denominator: {
        $sum: "$count"
      }
    }
  }, {
    $project: {
      participant: "$_id.participant",
      category: "$_id.category",
      average: {
        $divide: ["$numerator", "$denominator"]
      }
    }
  }, {
    $group: {
      _id: "$participant",
      categoryScores: {
        $push: {
          category: "$category",
          averageScore: "$average"
        }
      }
    }
  },
  {
    $addFields: {
      totalRfpScore: {
        $sum: "$categoryScores.averageScore"
      },
    }
  }
  ])

