import {_} from "lodash"

const a = {prop1: {prop1_1: "text 1", prop1_2: "text 2", prop1_3: [1, 2, 3]}, prop2: 2, prop3: 3}
const b = {prop1: {prop1_1: "text 1", prop1_3: [1, 2]}, prop2: 2, prop3: 4}

const c={
  entityAliases: [],
  isMuniVisorClient: true,
  msrbFirmName: "",
  msrbRegistrantType: "",
  msrbId: "",
  firmName: "",
  taxId: "",
  businessStructure: "",
  numEmployees: "",
  annualRevenue: "",
  addresses: [{
    addressName: "",
    isPrimary: true,
    isHeadQuarter:false,
    isActive:true,
    website: "",
    officePhone: [{
      countryCode: "",
      phoneNumber: "",
      extension: ""
    }],
    officeFax: [{
      faxNumber: ""
    }],
    officeEmails: [{
      emailId: ""
    }],
    addressLine1: "",
    addressLine2: "",
    country: "",
    state: "",
    city: "",
    zipCode: {
      zip1: "",
      zip2: ""
    }
  }]
}

export const diff = (obj1, obj2) => _.reduce(obj1, (result, value, key) => {
  if (_.isPlainObject(value)) {
    result[key] = diff(value, obj2[key])
  } else if (!_.isEqual(value, obj2[key])) {
    result[key] = value
  }
  return result
}, {})

export const checkEmptyElObject =(obj)=>{ 
  let isEmpty = true   
  const isEmptyFunc = (obj)=>{
    Object.keys(obj).forEach(item => {
      if(_.isObject(obj[item])){
        isEmptyFunc(obj[item])
      }else if(obj[item]!=="" && !_.isBoolean(obj[item]))
        isEmpty = false      
    })
    return isEmpty
  }  
  return isEmptyFunc(obj)
}