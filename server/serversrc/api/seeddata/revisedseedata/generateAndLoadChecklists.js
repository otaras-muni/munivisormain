import  {
  Entity, Config
}  from "./models"

const fs = require("fs")
const csv = require("fast-csv")
const path = require("path")
const {ObjectID} = require("mongodb")

const rows = []
const checklists = []
// const checklistFilePath = "./server/serversrc/api/seeddata/revisedseedata/checklists/test.csv"
const checklistFilePath = path.join(__dirname, "checklists", "test.csv")


const populateChecklist = (id, rows) => {
  const { name, type, bestPractice, notes } = rows[0]
  const lastUpdated = { date: new Date(), by: "system" }
  const data = []
  let oldTitle = ""
  let list = {}
  rows.forEach(r => {
    if(r.title !== oldTitle) {
      if(oldTitle) {
        data.push(list)
      }
      oldTitle = r.title
      list = { title: r.title, headers: r.headers.split("|"), items: [] }
      const item = { _id: (new ObjectID()).toHexString(), label: r["item label"] }
      if(["template1", "template4"].includes(type)) {
        item.assignedToTypes = r["item assignedToTypes"].split("|")
      }
      list.items.push(item)
    } else {
      const item = { _id: (new ObjectID()).toHexString(), label: r["item label"] }
      if(["template1", "template4"].includes(type)) {
        item.assignedToTypes = r["item assignedToTypes"].split("|")
      }
      list.items.push(item)
    }
  })
  if(rows.length) {
    data.push(list)
  }
  const checklist = {id, name, type, bestPractice, notes, lastUpdated, data }
  return checklist
}

const populateChecklists = (rows) => {
  const checklistIds = [ ...new Set(rows.map(e => e.id)) ]
  checklistIds.forEach(id => {
    const values = rows.filter(e => e.id === id)
    checklists.push(populateChecklist(id, values))
  })
  // console.log(JSON.stringify(checklists, null, 2))
}

const updateConfigNew = async () => {
  const tenants = await Entity.find({isMuniVisorClient:true }).select({_id:1, firmName:1, msrbFirmName:1})
  // console.log(tenants)

  const data = [...checklists]

  try {
    await (tenants || []).reduce(async(acc, tenant, ind) => {
      const {_id:tenantId, firmName, msrbFirmName} = tenant
      const configDoc = await Config.findOne({entityId:tenantId}, {_id: 1, entityId: 1})
      console.log(`Processing Data for Tenant : ${tenantId} - ${firmName} -# ${ind}`)
  
      const newData = data.map( list => ({...list,attributedTo:(firmName || msrbFirmName)}))
  
      await Config.update({ _id: configDoc._id }, {
        checklists: newData
      })
  
      return Promise.resolve({
        ...acc,
        ...configDoc
      })
    }, Promise.resolve({}))
  } catch (e) {
    console.log("The error is as follows", e)
  }
}


const updateConfig = async () => {
  await Config.find({}, {_id: 1, entityId: 1}).then(docs => {
    console.log("num configs : ", docs.length)
    docs.forEach((d, i) => {
      Entity.findOne({ _id: d.entityId }).then(entity =>  {
        const data = [...checklists]
        data.forEach(e => {
          e.attributedTo = (entity.firmName || entity.msrbFirmName)
        })
        Config.update({ _id: d._id }, {
          checklists: data
        }).then(res => console.log("res in update: ", i, d.entityId, entity.firmName, res.ok))
      }).catch(err => console.log("err in update: ", i, d.entityId, err))
    })
  })
}

const updateConfigForEntity = async (entityId) => {
  const data = [...checklists]
  await Config.update({ entityId }, {
    checklists: data
  })
}

export const loadCheckListsForEntity = async (chkListPath, entityId) =>new Promise (( resolve,reject ) => {
  csv
    .fromPath(path.resolve(chkListPath),
      {headers: true, objectMode: true})
    .on("data", (data) => {
      rows.push(data)
    })
    .on("end", async () => {
      await populateChecklists(rows)
      await updateConfigForEntity(entityId)
      resolve(`All Checklists Were successfully loaded for the entity - ${entityId}`)
    })
})

export const loadChecklistsForAllEntities = async () =>new Promise (( resolve,reject ) => {
  csv
    .fromPath(path.resolve(checklistFilePath),
      {headers: true, objectMode: true})
    .on("data", (data) => {
      // console.log(data)
      rows.push(data)
    })
    .on("end", async () => {
      // console.log("1 Populate Checklists", rows)
      await populateChecklists(rows)
      // await updateConfig()
      await updateConfigNew()
      console.log("2 Updated Config and Checklists")
      resolve("All Checklists Were successfully loaded for all entities")
    })
})

export const loadChecklistsForAllEntitiesWithPath = async (filePath) =>new Promise (( resolve,reject ) => {
  csv
    .fromPath(path.resolve(filePath),
      {headers: true, objectMode: true})
    .on("data", (data) => {
      // console.log(data)
      rows.push(data)
    })
    .on("end", async () => {
      // console.log("1 Populate Checklists", rows)
      await populateChecklists(rows)
      // await updateConfig()
      await updateConfigNew()
      console.log("2 Updated Config and Checklists")
      resolve("All Checklists Were successfully loaded for all entities")
    })
})
