import mongoose from "mongoose"
import path from "path"

import {
  Config,
  Entity
} from "./models"

import { generateUIAccressObject } from "./config/ui-access"
import { generateAccessPolicyData } from "./config/access-policy"
import { generatePicklists } from "./config/picklists"
import { generateChecklists } from "./config/checklists"
import { readJsonfileAndCopyToTarget } from "./controls/loadControlsDataIntoCurrentDB"
import upsertTemplates from "../../../emailTemplateMangement/templateManagement"

mongoose.Promise = global.Promise
const {ObjectID} = require("mongodb")

const ROLES = ["global-edit", "global-readonly", "tran-edit", "tran-readonly",
  "global-readonly-tran-edit"]

let uiAccessModel = {}
let picklists = []
let checklists = []
const uiaccessfilepath =  path.join(__dirname, "uiaccess", "uiaccess.csv")
const editablePicklistPath = path.join(__dirname, "picklists", "editable.csv")
const notEditablePicklistPath = path.join(__dirname, "picklists", "notEditable.csv")
const checklistfilepath = path.join(__dirname, "checklists", "test.csv")

export const generateAllConfig = async () => {
  const start = new Date()
  const args = process.argv.slice(2)
  // console.log("===========args============>", args)
  // const entityId = (args && args.length) ? args[0] : ""
  // const pickId = (args && args.length) ? args[0] : ""
  const key = (args && args.length) ? args[0] : ""
  let accessPolicy = []

  if (!key) {
    console.log("start generating config for all")
    accessPolicy = generateAccessPolicyData(ROLES)
    console.log("got access policy data for roles : ", accessPolicy.length)
    try {
      uiAccessModel = await generateUIAccressObject(ROLES, uiaccessfilepath)
      console.log("got ui access data")
    } catch (err) {
      console.log("err in getting ui access data : ", err)
    }

    try {
      checklists = await generateChecklists(checklistfilepath)
      console.log("got checklists data")
    } catch (err) {
      console.log("err in getting checklists data : ", err)
    }
  }

  try {
    picklists = await generatePicklists(editablePicklistPath, notEditablePicklistPath, key)
    console.log("got picklists data")
  } catch (err) {
    console.log("err in getting picklists data : ", err)
  }

  const entities = await Entity.aggregate([
    {
      $lookup: {
        from: "entityrels",
        localField: "_id",
        foreignField: "entityParty2",
        as: "rel"
      }
    },
    {
      $project: {
        relType: "$rel.relationshipType",
      }
    },
    {
      $unwind: "$relType"
    }
  ])

  console.log("got entities with relationships : ", entities.length)

  if (!key) {
    try {
      await Config.remove({})
      console.log("removed existing configs ok")
    } catch(err) {
      console.log("error in removing existing configs. Exiting now !!!")
      throw err
    }

    try {
      await Config.insertMany(entities.map(e => ({
        entityId: e._id,
        accessPolicy: [],
        checklists: [],
        picklists: [],
        notifications: []
      })))
      console.log("configs inserted ok for all entities. Please wait till it gets updated with data.")
    } catch (err) {
      console.log("err in inserting configs : ", err)
    }
  }

  const tenants = entities.filter(e => ["PlatformAdmin", "Self"].includes(e.relType))
  const clients = entities.filter(e => (e.relType === "Client"))
  const prospects = entities.filter(e => (e.relType === "Prospect"))
  const thirdParties = entities.filter(e => (e.relType === "Third Party"))

  const tenantAccessPolicy = [ ...accessPolicy ]
  tenantAccessPolicy.forEach((p, i) => {
    tenantAccessPolicy[i] = { ...tenantAccessPolicy[i] }
    tenantAccessPolicy[i].UIAccess = uiAccessModel.Self[p.role]
  })
  const clientsAccessPolicy = [ ...accessPolicy ]
  clientsAccessPolicy.forEach((p, i) => {
    clientsAccessPolicy[i] = { ...clientsAccessPolicy[i] }
    clientsAccessPolicy[i].UIAccess = uiAccessModel.Client[p.role]
  })
  const prospectsAccessPolicy = [ ...accessPolicy ]
  prospectsAccessPolicy.forEach((p, i) => {
    prospectsAccessPolicy[i] = { ...prospectsAccessPolicy[i] }
    prospectsAccessPolicy[i].UIAccess = uiAccessModel.Prospect[p.role]
  })
  const thirdPartiesAccessPolicy = [ ...accessPolicy ]
  thirdPartiesAccessPolicy.forEach((p, i) => {
    thirdPartiesAccessPolicy[i] = { ...thirdPartiesAccessPolicy[i] }
    thirdPartiesAccessPolicy[i].UIAccess = uiAccessModel["Third Party"][p.role]
  })

  try {
    if (key) {
      const data = await Config.aggregate([
        { $match: {"picklists.meta.systemName" : key }},
        { $unwind: "$picklists" },
        { $match: { "picklists.meta.systemName": key }},
      ])

      data.forEach(i => {
        const level2 = i.picklists.meta.subListLevel2
        const level3 = i.picklists.meta.subListLevel3
        i.picklists.items.forEach(k => {
          const labelLevel1 = i && i.picklists && i.picklists.items && i.picklists.items.map(i => i.label) || []
          picklists[0].items.forEach(alr => {
            if (!labelLevel1.includes(alr.label)) {
              i.picklists.items.push(alr)
            }
          })

          if (level2) {
            const labelLevel2 = k && k.items && k.items.map(i => i.label) || []
            picklists[0].items.forEach(alr => {
              alr.items.forEach(d => {
                if ((labelLevel2.indexOf(d.label) === -1) && (alr.label === k.label)) {
                  k.items.push(d)
                }
              })
            })
          }

          if (level3) {
            picklists[0].items.forEach(alr => {
              alr.items.forEach(d => {
                d.items.forEach(g => {
                  k.items.forEach(s => {
                    const third = s && s.items && s.items.map(w => w.label) || []
                    if ((third.indexOf(g.label) === -1) && (d.label === s.label)) {
                      s.items.push(g)
                    }
                  })
                })
              })
            })
          }

        })
      })

      const tenantsUpdate = tenants.map(e => e._id) || []
      for (const i in tenantsUpdate) {
        const keyUpdate = data.find(f => f.entityId.toString() === tenantsUpdate[i].toString()) || {}
        await Config.updateOne({
          entityId: ObjectID(tenantsUpdate[i]), "picklists.meta.systemName": key
        },
        { $set: { "picklists.$": keyUpdate.picklists }
        })
      }
    } else {
      await Config.updateMany(
        { entityId: { $in: tenants.map(e => e._id) } },
        { $set: { picklists, checklists, accessPolicy: tenantAccessPolicy, notifications: { self:true, client:true, thirdParty:true} }},
      )
    }
    console.log("config updated ok for all tenants")
  } catch (err) {
    console.log("err in updating config for tenants : ", err)
  }

  if (!key) {
    try {
      await Config.updateMany(
        { entityId: { $in: clients.map(e => e._id) } },
        { $set: { accessPolicy: clientsAccessPolicy }},
        {"new":true, "upsert": true, "runValidators": true}
      )
      console.log("config updated ok for all clients")
    } catch (err) {
      console.log("err in updating config for clients : ", err)
    }
    try {
      await Config.updateMany(
        { entityId: { $in: prospects.map(e => e._id) } },
        { $set: { accessPolicy: prospectsAccessPolicy }},
        {"new":true, "upsert": true, "runValidators": true}
      )
      console.log("config updated ok for all prospects")
    } catch (err) {
      console.log("err in updating config for prospects : ", err)
    }
    try {
      await Config.updateMany(
        { entityId: { $in: thirdParties.map(e => e._id) } },
        { $set: { accessPolicy: thirdPartiesAccessPolicy }},
        {"new":true, "upsert": true, "runValidators": true}
      )
      console.log("config updated ok for all third parties")
    } catch (err) {
      console.log("err in updating config for third parties : ", err)
    }

    try {
      console.log("Adding the controls information")
      await readJsonfileAndCopyToTarget("controls.json")
    } catch(e) {
      console.log("Error While Loading Controls For All Tenants",e)
    }
    try{
      console.log("Updating the Templates for Sending Email")
      await upsertTemplates()
    } catch(err) {
      console.log(err)
    }
  }

  console.log("all config done in time(ms) : ", new Date() - start)
  console.log("Exiting")
  process.exit()
}

generateAllConfig()
