import { generateEntity } from "./generateEntityData"
import { generateEntityUserData } from "./generateEntityUserData"
import { generatEntityRelData } from "./generateEntityRelationshipData"
import { generateConfigData } from "./generateConfigData"

const generatePlatformAdminDetails = async() => {

  console.log("Generating Platform Details")
  const platformAdminEntity = await generateEntity({entityType:"Platform", entityName:"Otaras,Inc"}).save()
  console.log("---PLATFORM ADMIN CREATED - ", platformAdminEntity.firmName)
  
  const rel = await generatEntityRelData(platformAdminEntity, platformAdminEntity,"PlatformAdmin").save()
  console.log("---RELATIONSHIP CREATED FOR ALL THE PLATFORM ADMINS - ", rel.relationshipType)
  
  const user = await generateEntityUserData(platformAdminEntity,"munivisorproddev@otaras.com","platformadmin").save()
  console.log("---PLATFORM ADMIN USER CREATED - ", user.userFirstName)

  await generateConfigData(platformAdminEntity).save()
  console.log("---PLATFORM CONFIGURATION CREATED - ")
}

generatePlatformAdminDetails().then(a=> console.log(JSON.stringify(a,null,2)))