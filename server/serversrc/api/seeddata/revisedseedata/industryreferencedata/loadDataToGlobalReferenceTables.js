import { GlobRefEntity } from "./../models"

const fs = require("fs")
const path = require("path")
const csv = require("fast-csv")

/*
export const globalRefEntitySchema = Schema({
  firmName:String,
  firmType:String,
  participantType:String,
  participantSubType:String,
  state:String,
  meta:{}
}, { _id: false })
*/

const loadThirdPartyInformation = async () =>new Promise (( resolve,reject ) => {
  const rows = []
  const filePath=  path.resolve(path.join(__dirname, "tenantandthirdparties.csv"))
  console.log("The Path for Loading Third Parties file:",filePath)
  csv
    // .fromPath(path.resolve("./server/serversrc/api/seeddata/revisedseedata/industryreferencedata/tenantandthirdparties.csv"), {headers: true, objectMode: true})
    .fromPath(path.resolve(filePath), {headers: true, objectMode: true})
    .on("data", (data) => {
    // // // console.log(data);
      rows.push({
        "firmName" : data.firmName,
        "firmType" : data.category,
        "participantType" : data.subcategory,
        "participantSubType" : "",
        state:""
      })

    })
    .on("end", async () => {
      try {
        const a = await GlobRefEntity.insertMany(rows)
        resolve(`All Third Parties Have been  successfully loaded - ${a.length}`)
      } catch(e) {
        console.log("Rejected Information", e)
        reject(new Error(e))
      }
    })
}
)
const loadTenantsInformation = async () =>new Promise (( resolve,reject ) => {
  const rows = []
  const filePath=  path.resolve(path.join(__dirname, "tenants.csv"))
  console.log("The Path for Loading tenants file:",filePath)

  csv
    // .fromPath(path.resolve("./server/serversrc/api/seeddata/revisedseedata/industryreferencedata/tenants.csv"), {headers: true, objectMode: true})
    .fromPath(path.resolve(filePath), {headers: true, objectMode: true})
    .on("data", (data) => {
    // // // console.log(data);
      rows.push({
        "firmName" : data.firmName,
        "firmType" : "Firms/Tenants",
        "participantType" : "Municipal Advisors",
        "participantSubType" : "",
        state:"",
        meta : {
          referenceId : data.msrbid,
          subCategory : data.subcategory
        }
      })

    })
    .on("end", async () => {
      try {
        const a = await GlobRefEntity.insertMany(rows)
        resolve(`All Tenants Have been  successfully loaded - ${a.length}`)
      } catch(e) {
        console.log("Rejected Information", e)
        reject(new Error(e))
      }
    })
})


const readAndLoadIssuerReferenceData = async () => {
  // const jsonPath = path.resolve("./server/serversrc/api/seeddata/revisedseedata/industryreferencedata/EMMA_Issuers.json")

  const jsonPath = path.resolve(path.join(__dirname, "EMMA_Issuers.json"))
  console.log("The Path:",jsonPath)
  // const jsonPath = path.join(__dirname, "globissuer.json")
  const jsonString = fs.readFileSync(jsonPath, "utf8")
  const parsedJsonFile = JSON.parse(jsonString)
  console.log("The path of the file is",Object.keys(parsedJsonFile) )

  try {
    const stateName = {
      AL: "Alabama",AK: "Alaska",AZ: "Arizona",
      AR: "Arkansas",CA: "California",CO: "Colorado",
      CT: "Connecticut",DE: "Delaware",FL: "Florida",
      GA: "Georgia",HI: "Hawaii",ID: "Idaho",
      IL: "Illinois",IN: "Indiana",IA: "Iowa",
      KS: "Kansas",KY: "Kentucky",LA: "Louisiana",
      ME: "Maine",MD: "Maryland",MA: "Massachusetts",
      MI: "Michigan",MN: "Minnesota",MS: "Mississippi",
      MO: "Missouri",MT: "Montana",NE: "Nebraska",
      NV: "Nevada",NH: "New Hampshire",NJ: "New Jersey",
      NM: "New Mexico",NY: "New York",NC: "North Carolina",
      ND: "North Dakota",OH: "Ohio",OK: "Oklahoma",
      OR: "Oregon",PA: "Pennsylvania",RI: "Rhode Island",
      SC: "South Carolina",SD: "South Dakota",TN: "Tennessee",
      TX: "Texas",UT: "Utah",VT: "Vermont",VA: "Virginia",
      WA: "Washington",WV: "West Virginia",WI: "Wisconsin",
    }
    const list = []
    await Object.keys(parsedJsonFile).forEach( async key => {
      await parsedJsonFile[key].forEach( async issuer => {
        list.push({
          "firmName" : issuer.nm,
          "firmType" : "Prospects/Clients",
          "participantType" : "Governmental Entity / Issuer",
          "participantSubType" : issuer.itp,
          state:stateName[key] ||"",
          meta : {
            itp: issuer.itp,
            referenceId : issuer.id,
            industryCategory : issuer.tp,
            stateCode:key
          }
        })
      })
    })
    // console.log("Here is the data to be loaded", JSON.stringify(list,null,2))
    const a = await GlobRefEntity.insertMany(list)
    return a.length

  } catch (error) {
    console.log("There are issues loading the information",error)
  }
}

export const loadGloabalReferenceTables = async () => {
  try {
    console.log("1. Deleting existing Global Reference")
    await GlobRefEntity.deleteMany({})
    console.log("2. Successfully Deleted Global Reference DAta. Loading Issuer Data")
    const returnedValue = await readAndLoadIssuerReferenceData()
    console.log("3. After Loading the Issuer Information - Total Loaded. Now Loading Third Party Information", returnedValue )
    const thirdPartiesLoadInformation = await loadThirdPartyInformation()
    console.log("4. AFter Loading Third Parties", thirdPartiesLoadInformation )
    const tenantsInformation = await loadTenantsInformation()
    console.log("4. AFter Loading Tenants", tenantsInformation )
    return "success"
  }
  catch (e) {
    console.log("Unable to load industry reference data", e)
    return "error"
  }
  finally {
    console.log("end")
  }
}

// loadGloabalReferenceTables().then(a => {
//   console.log("Message:",a)
//   process.exit(0)
// })
