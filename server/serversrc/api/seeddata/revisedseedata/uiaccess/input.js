export const ROLES = ["global-edit", "global-readonly", "tran-edit", "tran-readonly", "global-readonly-tran-edit"]

export const ENTITY_TYPES = ["Self", "Client", "Third Party", "Prospect", "PlatformAdmin"]

export const ENTITY_TYPES_MATCH = {
  "Self": "firm",
  "PlatformAdmin": "firm",
  "Client": "client/prospect",
  "Prospect": "client/prospect",
  "Third Party": "third-parties"
}

export const input = {
  admin: {
    entityTypes: {
      firm: ["global-edit"],
      "client/prospect": ["global-edit"],
      "third-parties": ["global-edit"]
    },
    level1: {
      "admin-firms": {
        entityTypes: {
          firm: ["global-edit"],
          "client/prospect": ["global-edit"],
          "third-parties": ["global-edit"]
        },
        level2: {
          
        }
      },
      "admin-firmusers": {
        entityTypes: {
          firm: ["global-edit" ,"read", "tran"],
          "client/prospect": ["global-edit" , "tran"],
          "third-parties": ["global-edit" ,"read", "tran"]
        },
        level2: {

        }
      },
    }
  },
  userprofile: {
    entityTypes: {
      firm: ["global-edit" ,"read", "tran"],
      "client/prospect": ["global-edit" , "tran"],
      "third-parties": ["global-edit" ,"read", "tran"]
    }
  }

}
