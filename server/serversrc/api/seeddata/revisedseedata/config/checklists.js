import  { getFileData } from "../utils"

const {ObjectID} = require("mongodb")

const checklists = []

const populateChecklist = (id, rows) => {
  const { name, type, bestPractice, notes } = rows[0]
  const lastUpdated = { date: new Date(), by: "system" }
  const data = []
  let oldTitle = ""
  let list = {}
  rows.forEach(r => {
    if(r.title !== oldTitle) {
      if(oldTitle) {
        data.push(list)
      }
      oldTitle = r.title
      list = { title: r.title, headers: r.headers.split("|"), items: [] }
      const item = { _id: (new ObjectID()).toHexString(), label: r["item label"] }
      if(["template1", "template4"].includes(type)) {
        item.assignedToTypes = r["item assignedToTypes"].split("|")
      }
      list.items.push(item)
    } else {
      const item = { _id: (new ObjectID()).toHexString(), label: r["item label"] }
      if(["template1", "template4"].includes(type)) {
        item.assignedToTypes = r["item assignedToTypes"].split("|")
      }
      list.items.push(item)
    }
  })
  if(rows.length) {
    data.push(list)
  }
  const checklist = {id, name, type, bestPractice, notes, lastUpdated, data }
  return checklist
}

const populateChecklists = (rows) => {
  const checklistIds = [ ...new Set(rows.map(e => e.id)) ]
  checklistIds.forEach(id => {
    const values = rows.filter(e => e.id === id)
    checklists.push(populateChecklist(id, values))
  })
  // console.log(JSON.stringify(checklists, null, 2))
}

export const generateChecklists = async (filePath) => {
  // const filePath = "./server/serversrc/api/seeddata/revisedseedata/checklists/test.csv"
  try {
    const rows = await getFileData(filePath)
    populateChecklists(rows)
    console.log("population of checklists done")
  } catch (err) {
    console.log("err in population of checklists : ", err)
  }
  return checklists
}
