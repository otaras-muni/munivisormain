import  { getFileData } from "../utils"

const uiAccess = {}

const ENTITY_TYPES = ["Self", "Client", "Third Party", "Prospect", "PlatformAdmin"]

export const ENTITY_TYPES_MATCH = {
  "Self": "firm",
  "PlatformAdmin": "firm",
  "Client": "client/prospect",
  "Prospect": "client/prospect",
  "Third Party": "third-parties"
}

const populateAccessPolicy = (role, rows) => {
  const primaryNav = []
  let oldL0 = ""
  let oldL1 = ""
  let UIAccess = { primaryNav: [] }
  let item = {}
  let L1item
  let L2item
  rows.forEach(r => {
    // console.log(r.L0, r.L1, r.L2, oldL0, oldL1, JSON.stringify(item))
    if(r.L0 !== oldL0) {
      // console.log("L0 changed : ", r.L0, oldL0)
      if(oldL0) {
        primaryNav.push(item)
      }
      oldL0 = r.L0
      item = { path: r.L0, access: !!+r.L0val, subpath: [] }
      if(r.L1) {
        oldL1 = r.L1
        L1item = { path: r.L1, access: !!+r.L1val, subpath: [] }
        if(r.L2){
          L2item = { path: r.L2, access: !!+r.L2val, subpath: [] }
          if(r.L3) {
            L2item.subpath.push({ path: r.L3, access: !!+r.L3val, subpath: [] })
          }
          // console.log("pushing level2Item : ", r.L2)
          L1item.subpath.push(L2item)
        }
        // console.log("pushing level1Item : ", r.L1)
        item.subpath.push(L1item)
      }
    } else if(r.L1) {
      if(r.L1 !== oldL1) {
        // console.log("L1 changed : ", r.L1, oldL1)
        oldL1 = r.L1
        L1item = { path: r.L1, access: !!+r.L1val, subpath: [] }
        if(r.L2){
          L2item = { path: r.L2, access: !!+r.L2val, subpath: [] }
          if(r.L3) {
            L2item.subpath.push({ path: r.L3, access: !!+r.L3val, subpath: [] })
          }
          // console.log("pushing level2Item : ", r.L2)
          L1item.subpath.push(L2item)
        }
        // console.log("pushing level1Item : ", r.L1)
        item.subpath.push(L1item)
      } else if(r.L2){
        L2item = { path: r.L2, access: !!+r.L2val, subpath: [] }
        if(r.L3) {
          L2item.subpath.push({ path: r.L3, access: !!+r.L3val, subpath: [] })
        }
        // console.log("pushing level2Item : ", r.L2)
        L1item.subpath.push(L2item)
      }
    }
  })
  if(rows.length) {
    // console.log("pushing last item : ", JSON.stringify(item))
    primaryNav.push(item)
    UIAccess = { primaryNav }
  }
  return UIAccess
}

const populateUIAccess = (rows, roles) => {
  ENTITY_TYPES.forEach(t => {
    uiAccess[t] = {}
    roles.forEach(role => {
      const values = rows.filter(e => e["entity-type"] === ENTITY_TYPES_MATCH[t]
        && e.role === role)
      uiAccess[t][role] = populateAccessPolicy(role, values)
    })
  })
  // console.log(JSON.stringify(uiAccess, null, 2))
}

export const generateUIAccressObject = async (roles, filePath) => {
  // const filePath = "./server/serversrc/api/seeddata/revisedseedata/uiaccess/uiaccess.csv"
  
  try {
    const rows = await getFileData(filePath)
    populateUIAccess(rows, roles)
    console.log("generateUIAccressObject done ok")
  } catch (err) {
    console.log("err in generateUIAccressObject : ", err)
  }
  return uiAccess
}
