const ACCESS = {
  "global-edit": {Entity: 2, EntityUser: 2,TenantBilling:2,Docs:2,Controls:2,
    Notifications:2, Deals: 2,Others:2,ActBusDev:2, EntityRel: 2, RFP: 2,
    Tasks: 2, Config:2, BankLoans:2,Derivatives:2,ActMaRFP:2 ,BusConduct:2,
    ComplaintDetails:2,GeneralAdmin:2,PoliticalContributions:2,
    ProfQualifications:2,ComplSupervisor:2,SuperVisoryObligations:2,
    GiftsAndGratuities:2 },
  "global-readonly": {Entity: 1, EntityUser: 1, TenantBilling:1, Docs:1,
    Controls:1,Notifications:1,Deals: 1, Others:1,ActBusDev:1,EntityRel: 1, RFP: 1,
    Tasks: 1, Config:1,BankLoans:1,Derivatives:1,ActMaRFP:1,BusConduct:1,
    ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,
    GiftsAndGratuities:1},
  "tran-edit": {Entity: 1, EntityUser: 1, Deals: 2, Others:2,ActBusDev:2,
    TenantBilling:1,Docs:1,Controls:1,Notifications:1,EntityRel: 1, RFP: 2,
    Tasks: 2, Config:1,BankLoans:2,Derivatives:2,ActMaRFP:2,BusConduct:1,
    ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,
    GiftsAndGratuities:1},
  "tran-readonly": {Entity: 1, EntityUser: 1, TenantBilling:1,Deals: 1,Others:1,
    ActBusDev:1,Docs:1,Controls:1,Notifications:1,EntityRel: 1, RFP: 1,
    Tasks: 1, Config:1,BankLoans:1,Derivatives:1,ActMaRFP:1,BusConduct:1,
    ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,
    GiftsAndGratuities:1},
  "global-readonly-tran-edit": {Entity: 1, EntityUser: 1, TenantBilling:1,Docs:1,
    Controls:1,Config:1, Notifications:1, EntityRel: 1,
    Tasks:2,Deals: 2, RFP: 2,BankLoans:2,Derivatives:2,ActMaRFP:2,Others:2,
    ActBusDev:2,BusConduct:1, ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,GiftsAndGratuities:1}
}

export const RESOURCES = ["Entity","EntityUser","TenantBilling",
  "Deals","EntityRel","RFP","Tasks","Config", "BankLoans","Derivatives","ActMaRFP","Docs","Others","ActBusDev",
  "Controls","Notifications",
  "BusConduct","ComplaintDetails","GeneralAdmin","PoliticalContributions","ProfQualifications","ComplSupervisor","SuperVisoryObligations","GiftsAndGratuities"
]

export const getResourceAccess = (resource, role) => ({
  resource,
  access: ACCESS[role][resource]
})
