import { RESOURCES, getResourceAccess } from "./resource-access"

export const getAccessPolicyForRole = role => ({
  role,
  resourceAccess: RESOURCES.map(resource => getResourceAccess(resource, role)),
  UIAccess: { primaryNav: [] }
})

export const generateAccessPolicyData = roles => roles.map(r => getAccessPolicyForRole(r))
