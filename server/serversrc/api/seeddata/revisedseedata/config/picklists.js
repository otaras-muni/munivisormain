const csv = require("fast-csv")
const path = require("path")
const {ObjectID} = require("mongodb")

const picklists = []
const rows = []
const notEditableRows = []

const picklistMeta = {
  systemName: "",
  subListLevel2:false,
  subListLevel3:false,
  systemConfig:true,
  restrictedList:false,
  externalList:false,
  bestPractice:true
}

const checkLevels = (values) => {
  let result = 1
  values.some(e => {
    if(e.level3) {
      result = 3
      return true
    }
  })
  if(result !== 3) {
    values.some(e => {
      if(e.level2) {
        result = 2
        return true
      }
    })
  }
  return result
}

const addLevel1 = (level, values) => {
  const item = {}
  item.label = level
  item.included = !!(values["level1 included"] === "TRUE" || values["level1 included"] === "")
  item.visible = !!(values["level1 visible"] === "TRUE" || values["level1 visible"] === "")
  item.items = []
  return item
}

const addLevel2 = (level, values) => {
  const item = {}
  item.label = level
  item.included = !!(values["level2 included"] === "TRUE" || values["level2 included"] === "")
  item.visible = !!(values["level2 visible"] === "TRUE" || values["level2 visible"] === "")
  item.items = []
  return item
}

const addLevel3 = (level, values) => {
  const item = {}
  item.label = level
  item.included = !!(values["level3 included"] === "TRUE" || values["level3 included"] === "")
  item.visible = !!(values["level3 visible"] === "TRUE" || values["level3 visible"] === "")
  item.items = []
  return item
}

const populatePicklist = (title, name, values, editable) => {
  const last = values && values.length && values[values.length-1]
  const lastItem = {
    label: last.level2,
    included: true,
    visible: true,
    items: []
  }

  const picklist = {}
  picklist.items = []
  picklist.title = title
  picklist.meta = { ...picklistMeta, systemName: name, key: (new ObjectID()).toHexString(), editable }
  const levels = checkLevels(values)

  if(levels === 3) {
    picklist.meta.subListLevel2 = true
    picklist.meta.subListLevel3 = true
  } else if( levels === 2) {
    picklist.meta.subListLevel2 = true
  }
  let oldLevel1
  let oldLevel2
  let item
  let level2Item
  let newLevel2Items

  values.forEach(e => {
    if(e.level1) {
      // console.log(e.level1, e.level2, e.level3, oldLevel1, oldLevel2, JSON.stringify(item))
      if(oldLevel1 !== e.level1) {
        // console.log("level1 changed")
        if(item) {
          if(item && item.items && level2Item) {
            // console.log("pushing level2Item : ", level2Item)
            item.items.push({ ...level2Item})
          }
          picklist.items.push({ ...item })
          level2Item = null
        }
        item = { ...addLevel1(e.level1, e) }
        if(e.level2 && item.items) {
          newLevel2Items = addLevel2(e.level2, e)
          if(e.level3 && newLevel2Items.items) {
            newLevel2Items.items.push({ ...addLevel3(e.level3, e) })
          }
          item.items.push(newLevel2Items)
        }
        oldLevel1 = e.level1
        oldLevel2 = e.level2
      } else if(e.level2) {
        // console.log("level2Item : ", level2Item)
        if(oldLevel2 !== e.level2) {
          // console.log("level2 changed")
          if(item && item.items && level2Item) {
            // console.log("pushing level2Item")
            item.items.push({ ...level2Item })
          }
          level2Item = { ...addLevel2(e.level2, e) }
          // console.log("new level2Item : ", level2Item)
          if(e.level3 && level2Item.items) {
            level2Item.items.push({ ...addLevel3(e.level3, e) })
          }
          oldLevel2 = e.level2
        } else if(e.level3) {
          // console.log("adding level3 for old level2")
          if(level2Item && level2Item.items) {
            level2Item.items.push({ ...addLevel3(e.level3, e) })
          } else {
            newLevel2Items.items.push({ ...addLevel3(e.level3, e) })
          }
        }
      }
    }
  })
  // console.log("last item : ", item)
  if(item) {
    if(picklist.meta && picklist.meta.subListLevel3 && level2Item) {
      item.items.push({ ...level2Item })
    }
    if(picklist.meta && picklist.meta.subListLevel2) {
      item.items.push({ ...lastItem })
    }
    if(picklist.meta && picklist.meta.subListLevel3) {
      item.items.splice(-1, 1)
    }
    picklist.items.push({ ...item })
  }
  return picklist
}

const populatePicklists = (editableRows, notEditableRows, key) => {
  let rows = []

  notEditableRows.forEach(s => {
    // const isEditable = editableRows.find(p => s.name === p.name && p.isEditable)
    rows.push({...s, isEditable: false})
  })
  editableRows.forEach(s => {
    const isEditable = notEditableRows.find(p => s.name === p.name)
    if (!isEditable) {
      rows.push(s)
    }
  })

  if (key) {
    rows = rows.filter(e => e.name === key)
  }

  const uniqueValues = rows.reduce( (uniqueCombinations, r) => {
    const isEditable = rows.find(val => val.name === r.name && val.isEditable)
    if(uniqueCombinations[r.name]) {
      return {
        ...uniqueCombinations,
        [r.name]: {
          ...uniqueCombinations[r.name],
          name: r.name,
          title: r.title,
          editable: !!isEditable
        }
      }
    }
    return {...uniqueCombinations, [r.name]: {name: r.name, title: r.title, editable: !!isEditable}}
  },{})

  Object.keys(uniqueValues).forEach(n => {
    const values = rows.filter(e => e.name === n)
    // console.log(n, uniqueValues[n])
    // console.log(values)
    picklists.push(populatePicklist(uniqueValues[n].title, uniqueValues[n].name, values, uniqueValues[n].editable))
  })
}

export const loadEditableCSV = (editablePath) => {
  return new Promise (async ( resolve, reject ) => {
    csv
      .fromPath(path.resolve(editablePath), {headers: true, objectMode: true})
      .on("data", (data) => {
        rows.push({...data, isEditable: true})
      })
      .on("end", async () => {
        resolve()
      })
      .on("error", (err) => {
        console.log("Error in getting file data : ", err)
        reject(new Error("Error in getting file data"))
      })
  })
}

export const loadNotEditableCSV = (notEditablePath) => {
  return new Promise (async ( resolve, reject ) => {
    csv
      .fromPath(path.resolve(notEditablePath), {headers: true, objectMode: true})
      .on("data", (data) => {
        notEditableRows.push({...data, isEditable: false})
      })
      .on("end", async () => {
        resolve()
      })
      .on("error", (err) => {
        console.log("Error in getting file data : ", err)
        reject(new Error("Error in getting file data"))
      })
  })
}

export const generatePicklists = async (editablePath, notEditablePath, key) => {
  try {
    await loadNotEditableCSV(notEditablePath)
    await loadEditableCSV(editablePath)
    populatePicklists(rows, notEditableRows, key)
    console.log("population of picklists done")
  } catch (err) {
    console.log("err in population of picklists : ", err)
  }
  return picklists
}
