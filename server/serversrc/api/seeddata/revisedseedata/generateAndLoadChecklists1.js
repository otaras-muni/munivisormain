import  {
  Entity, Config
}  from "./models"

import  { getFileData } from "./utils"

const fs = require("fs")
const csv = require("fast-csv")
const path = require("path")
const {ObjectID} = require("mongodb")

const rows = []
const checklists = []

const populateChecklist = (id, rows) => {
  const { name, type, bestPractice, notes } = rows[0]
  const lastUpdated = { date: new Date(), by: "system" }
  const data = []
  let oldTitle = ""
  let list = {}
  rows.forEach(r => {
    if(r.title !== oldTitle) {
      if(oldTitle) {
        data.push(list)
      }
      oldTitle = r.title
      list = { title: r.title, headers: r.headers.split("|"), items: [] }
      const item = { _id: (new ObjectID()).toHexString(), label: r["item label"] }
      if(["template1", "template4"].includes(type)) {
        item.assignedToTypes = r["item assignedToTypes"].split("|")
      }
      list.items.push(item)
    } else {
      const item = { _id: (new ObjectID()).toHexString(), label: r["item label"] }
      if(["template1", "template4"].includes(type)) {
        item.assignedToTypes = r["item assignedToTypes"].split("|")
      }
      list.items.push(item)
    }
  })
  if(rows.length) {
    data.push(list)
  }
  const checklist = {id, name, type, bestPractice, notes, lastUpdated, data }
  return checklist
}

const populateChecklists = (rows) => {
  const checklistIds = [ ...new Set(rows.map(e => e.id)) ]
  checklistIds.forEach(id => {
    const values = rows.filter(e => e.id === id)
    checklists.push(populateChecklist(id, values))
  })
  // console.log(JSON.stringify(checklists, null, 2))
}

const updateConfig = async (entity) => {
  try {
    const data = [...checklists]
    data.forEach(e => {
      e.attributedTo = (entity.firmName || entity.msrbFirmName)
    })
    const result =  await Config.update({ entityId: entity._id }, {
      checklists: data
    })
    console.log("result : ", result && result.ok)
    // For each MuniVisor Client Insert the picklists values
  } catch (error) {
    console.log("There is an error",error)
  }
}

export const loadChecklistForOneFirmOld = async (entity) => {
  await csv
    .fromPath(path.join(__dirname, "checklists", "test.csv"),
      {headers: true, objectMode: true})
    .on("data", (data) => {
      // console.log(data)
      rows.push(data)
    })
    .on("end", async () => {
      // console.log("done : ", rows)
      populateChecklists(rows)
      // fs.writeFileSync("./server/serversrc/api/seeddata/seedmongo/clout.json", JSON.stringify(checklists, null, 2), "utf8")
      await updateConfig(entity)
      console.log("ok")
    })
}

export const loadChecklistForOneFirm = async (entity) => {
  const filePath = path.join(__dirname, "checklists", "test.csv")
  try {
    const rows = await getFileData(filePath)
    populateChecklists(rows)
    await updateConfig(entity)
    console.log("loadChecklistForOneFirm done ok")
  } catch (err) {
    console.log("err in loadChecklistForOneFirm : ", err)
  }
}
