import mongoose from "mongoose"

import {
  Controls,
  Entity
} from "./models"

const {ObjectID, MongoClient} = require("mongodb")

const sourceDBURL = "mongodb://munivisordev:TCVeSVBJeJF6zMCi@munivisortrial-shard-00-00-qatoy.mongodb.net:27017,munivisortrial-shard-00-01-qatoy.mongodb.net:27017,munivisortrial-shard-00-02-qatoy.mongodb.net:27017/munivisor_confi_demo?ssl=true&replicaSet=munivisortrial-shard-0&authSource=admin&retryWrites=true"
const sourceDBName = "munivisor_confi_demo"

mongoose.connect(process.env.DB_URL,{ useNewUrlParser: true })

const getSourceCACData = async (dbUrl, dbName) => {
  let client
  try {
    client = await MongoClient.connect(dbUrl)
    const db = await client.db(dbName)
    console.log("connected to source DB ok")
    const controlsCollection = await db.collection("controls")
    const controls = await controlsCollection.find({}).toArray()
    if(!controls.length) {
      console.log("no controls found in source")
      return []
    }
    const sortedControls = [ ...controls ]
    sortedControls.forEach((e, i) => {
      sortedControls[i] = { ...e }
      console.log("controls length original : ", e.controls.length)
      sortedControls[i].controls = e.controls.filter(c => !(/^CTRLTTACC/.test(c.id)))
      console.log("controls length after filter : ", sortedControls[i].controls.length)
    })
    console.log("sortedControls length : ", sortedControls.length)
    sortedControls.sort((a,b) => b.controls.length - a.controls.length)
    const sourceControl = sortedControls[0]
    console.log("selecting source control with numbers : ", sourceControl.controls.length)
    client.close()
    return sourceControl.controls
  } catch (err) {
    console.log("err in getSourceCACData : ", err)
    if(client) {
      client.close()
    }
  }
}

const getSourceCopy = (sourceControls) => {
  const controls = []
  sourceControls.forEach(e => {
    const control = {
      ...e,
      refId: ObjectID(),
      notificationSentDate: null,
      actionCompletedBy: 0,
      numActions: 0,
      dueDate: null,
      notification: {
        ...e.notification,
        startDate: e.notification.startDate ?
          new Date(e.notification.startDate) : null,
        endDate: e.notification.endDate ?
          new Date(e.notification.endDate) : null,
      },
      state: "enabled",
      status: "open",
      saveType: "draft",
      recipients: [],
      toList: [],
      ccList: [],
      relatedActivity: [],
      relatedEntity: [],
      relatedContact: [],
      docIds: [],
      checklist: e.checklist ? e.checklist.map(e => ({ ...e })) : [],
      createdBy: "admin@otaras.com",
      lastUpdated: {
        date: new Date(),
        by: "admin@otaras.com"
      }
    }
    controls.push(control)
  })
  return controls
}

const copyToTarget = async (sourceDBURL, sourceDBName) => {
  const sourceControls = await getSourceCACData(sourceDBURL, sourceDBName)
  if(sourceControls.length) {
    const entities = await Entity.find({ isMuniVisorClient: true }).select({ _id: 1, msrbFirmName: 1 })
    console.log("found tenants : ", entities.length)
    for(let i = 0; i < entities.length; i++) {
      const { _id, msrbFirmName } = entities[i]
      const control = await Controls.findOne({ entityId: _id })
      if(!control) {
        const controls = getSourceCopy(sourceControls)
        console.log("copying controls numbers : ", controls.length)
        try {
          await Controls.create({ entityId: _id, controls })
          console.log("inserted control ok for entity : ", _id, msrbFirmName)
        } catch (err) {
          console.log("err in inserting control for entity : ", _id, msrbFirmName, err)
        }
      } else {
        console.log("controls already present for entity : ", _id, msrbFirmName)
      }
    }
  } else {
    console.log("no controls found to copy")
  }
  process.exit()
}

copyToTarget(sourceDBURL, sourceDBName)
