import _ from "lodash"
import dotenv from "dotenv"

import { generateEntity } from "./generateEntityData"
import { generateEntityUserData } from "./generateEntityUserData"
import { generatEntityRelData } from "./generateEntityRelationshipData"
import { cleanAllCollections } from "./cleanDbData"
import { generateConfigData } from "./generateConfigData"
import { loadPickValuesForMuniVisorClientsLatest } from "./generateAndLoadPicklists1"
import { loadUIAccessInformation } from "./generateAndLoadUIAccess"
import { loadChecklistsForAllEntitiesWithPath } from "./generateAndLoadChecklists"
import { loadGloabalReferenceTables } from "./industryreferencedata/loadDataToGlobalReferenceTables"
import { initandreseeddata, clearIndex,indexExistsPinger } from "./../../elasticsearch/esHelper"
import isEmpty from "lodash/isEmpty"



import { Entity, EntityUser, Config, EntityRel } from "./models"

const path = require("path")

dotenv.config()

let munivisorFinAdvisorClients = []
let issuerList = []
let prospectList = []
let undList = []
let genThirdParties = []

let munivisorFinAdvisorClientsEmailDomains = []
let issuerListEmailDomains = []
let prospectListEmailDomains = []
let undListEmailDomains = []
let genThirdPartiesEmailDomains = []

const { fas, issuers, prospects, unds, tps, numusers } = {
  fas: 5,
  issuers: 400,
  prospects: 50,
  unds: 50,
  bcs: 50,
  numusers: 5
}

if (process.env.PERFTEST && process.env.PERFTEST === "Yes") {
  console.log("Seeding Data to Cover for Performance")

  munivisorFinAdvisorClients = _.times(fas).map(i => `Financial Advisor-${i}`)
  issuerList = _.times(issuers).map(i => `Issuer-${i}`)
  prospectList = _.times(prospects).map(i => `Prospect-${i}`)
  undList = _.times(unds).map(i => `Underwriter-${i}`)
  genThirdParties = _.times(tps).map(i => `General Third Party-${i}`)

  munivisorFinAdvisorClientsEmailDomains = _.times(fas).map(
    i => `finadvisor${i}.com`
  )
  issuerListEmailDomains = _.times(issuers).map(i => `issuer${i}.com`)
  prospectListEmailDomains = _.times(prospects).map(i => `prospect${i}.com`)
  undListEmailDomains = _.times(unds).map(i => `underwriter${i}.com`)
  genThirdPartiesEmailDomains = _.times(tps).map(i => `thirdparty${i}.com`)
} else {
  munivisorFinAdvisorClients = [
    "Financial Advisor 1",
    "Financial Advisor 2",
    "Financial Advisor 3"
  ]
  issuerList = [
    "State of New Jersey",
    "State of California",
    "State of Texas",
    "State of New York"
  ]
  prospectList = ["Pennsylvania State", "Florida State", "Colorado State"]
  undList = [
    "Goldman Sachs",
    "Wells Fargo",
    "Morgan Stanley",
    "Stifel",
    "Bank of America",
    "PNC Capital Markets"
  ]
  genThirdParties = ["Orrick", "Bond Counsel 1", "Bond Counsel 2"]

  munivisorFinAdvisorClientsEmailDomains = [
    "finadvisor1.com",
    "finadvisor2.com",
    "finadvisor3.com"
  ]
  issuerListEmailDomains = ["nj.com", "cal.com", "texas.com", "ny.com"]
  prospectListEmailDomains = ["penn.com", "flordiastate.com", "colorado.com"]
  undListEmailDomains = [
    "goldman.com",
    "wellsfargo.com",
    "morganstanley.com",
    "stifel.com",
    "bofa.com",
    "pnc.com"
  ]
  genThirdPartiesEmailDomains = ["orric.com", "bc1.com", "bc2.com"]
}

const namesArray = [
  ...munivisorFinAdvisorClients,
  ...issuerList,
  ...prospectList,
  ...undList,
  ...genThirdParties
]
const domainsArray = [
  ...munivisorFinAdvisorClientsEmailDomains,
  ...issuerListEmailDomains,
  ...prospectListEmailDomains,
  ...undListEmailDomains,
  ...genThirdPartiesEmailDomains
]

const nameDomainMapping = namesArray.reduce((zippedObj, entName, index) => {
  const rootVal = domainsArray[index].split(".")[0]
  return {
    ...zippedObj,
    ...{
      [entName]: {
        entityName: entName,
        domain: domainsArray[index],
        domainUserRoot: rootVal
      }
    }
  }
}, {})

// console.log(nameDomainMapping)

let finAdvisorsList = []
const allEntities = []

const createAllTenants = async () => {
  console.log("1. Creating all the financial Advisors ")
  const financialAdvisors = []
  munivisorFinAdvisorClients.forEach(mv =>
    financialAdvisors.push(generateEntity({ entityType: "FA", entityName: mv }))
  )
  finAdvisorsList = await Entity.insertMany(financialAdvisors)
  console.log("2. All financial advisors have been created")
}

// Create all the users for the financial advisors
const createAllTenantRelatedEntities = async () => {
  await Promise.all(
    finAdvisorsList.map(async fa => {
      const relationships = []
      const entityUsers = []

      const issuerArrayFortenant = await Promise.all(
        issuerList.map(iss =>
          generateEntity({ entityType: "C", entityName: iss })
        )
      )
      const prospectArrayForTenant = await Promise.all(
        prospectList.map(prospect =>
          generateEntity({ entityType: "P", entityName: prospect })
        )
      )
      const underwriterArrayForTenant = await Promise.all(
        undList.map(UND => generateEntity({ entityType: "U", entityName: UND }))
      )
      const thirdPartiesArrayForTenant = await Promise.all(
        genThirdParties.map(thirdParty =>
          generateEntity({ entityType: "BC", entityName: thirdParty })
        )
      )

      console.log(`3. Creating Issuers for the tenant - ${fa.firmName}`)
      const insertedIssuersForTenant = await Entity.insertMany(
        issuerArrayFortenant
      )
      console.log(
        "4. Completed creation of issuers for tenant",
        insertedIssuersForTenant.length
      )

      console.log(`5. Creating Prospects for the tenant - ${fa.firmName}`)
      const insertedProspectsForTenant = await Entity.insertMany(
        prospectArrayForTenant
      )
      console.log(
        "6. Completed creation of prospects for tenant",
        insertedProspectsForTenant.length
      )

      console.log(`7. Creating Underwriters for the tenant - ${fa.firmName}`)
      const insertedUnderwriters = await Entity.insertMany(
        underwriterArrayForTenant
      )
      console.log(
        "8. Completed creation of Underwriters for tenant",
        insertedUnderwriters.length
      )

      console.log(`9. Creating Third Parties for the tenant - ${fa.firmName}`)
      const insertedThirdParties = await Entity.insertMany(
        thirdPartiesArrayForTenant
      )
      console.log(
        "10. Completed creation of Third Parties for tenant",
        insertedThirdParties.length
      )

      console.log(`11. Writing relationships and adding users - ${fa.firmName}`)

      // Capture relationships and also create the users for Clients

      insertedIssuersForTenant.map(async iss => {
        relationships.push(generatEntityRelData(fa, iss, "Client"))
        _.times(numusers).map((ind, index) => {
          const { domain } = nameDomainMapping[iss.firmName]
          const { domainUserRoot } = nameDomainMapping[fa.firmName]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          entityUsers.push(
            generateEntityUserData(iss, userEmailId, "STP", "CP", index)
          )
        })
        return "done"
      })

      // Capture relationships and also create the users for prospects

      insertedProspectsForTenant.map(async prospect => {
        relationships.push(generatEntityRelData(fa, prospect, "Prospect"))
        _.times(numusers).map((ind, index) => {
          const { domain } = nameDomainMapping[prospect.firmName]
          const { domainUserRoot } = nameDomainMapping[fa.firmName]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          entityUsers.push(
            generateEntityUserData(prospect, userEmailId, "STP", "CP", index)
          )
        })
        return "done"
      })

      // Capture relationships and also create the users for Underwriters

      insertedUnderwriters.map(async und => {
        // const rel = generatEntityRelData(fa, und,"Third Party")
        relationships.push(generatEntityRelData(fa, und, "Third Party"))
        _.times(numusers).map((ind, index) => {
          const { domain } = nameDomainMapping[und.firmName]
          const { domainUserRoot } = nameDomainMapping[fa.firmName]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          entityUsers.push(
            generateEntityUserData(und, userEmailId, "STP", "TP", index)
          )
        })
        return "done"
      })

      // Capture relationships and also create the users for Third Parties

      insertedThirdParties.map(async thirdParty => {
        relationships.push(generatEntityRelData(fa, thirdParty, "Third Party"))
        _.times(numusers).map((ind, index) => {
          const { domain } = nameDomainMapping[thirdParty.firmName]
          const { domainUserRoot } = nameDomainMapping[fa.firmName]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          entityUsers.push(
            generateEntityUserData(thirdParty, userEmailId, "STP", "TP", index)
          )
        })
        return "done"
      })

      // Capture relationships and users for the tenant

      relationships.push(generatEntityRelData(fa, fa, "Self"))

      _.times(numusers).map((ind, index) => {
        const { domain } = nameDomainMapping[fa.firmName]
        const userEmailId = `user${ind}@${domain}`
        entityUsers.push(generateEntityUserData(fa, userEmailId, "NSTP", "FA", index))
      })

      await EntityRel.insertMany(relationships)
      await EntityUser.insertMany(entityUsers)

      console.log(
        `12. Completed writing relationships and Users - ${fa.firmName}`
      )
      return "done for tenant"
    })
  )
}

const createConfigForEntities = async () => {
  const configPromises = []
  allEntities.forEach(async ent => {
    configPromises.push(generateConfigData(ent).save())
  })
  await Promise.all(configPromises)
  console.log("9. Completed loading configuration information")
}

const clearOldIndexes = async () => {
  try {
    console.log("=> Deleting Dangling Indexes")
    const tenants = await Entity.find({ isMuniVisorClient: true }).select({
      _id: 1
    })
    console.log("The elastic search indexes for for the tenants will be deleted ", tenants.length)

    if(!isEmpty(tenants)) {
      const tenantIds = tenants.map(t => t._id)
      const tenantDataIndexes = tenantIds.map(t => `tenant_data_${t}`)
      const tenantDocumentIndexes = tenantIds.map(t => `tenant_docs_${t}`)
      const indexesExistsStatus = await Promise.all([...tenantDataIndexes,...tenantDocumentIndexes].map((tenIndex) => indexExistsPinger(tenIndex)))
     
      console.log(`The status of indexes that exists : ${JSON.stringify(indexesExistsStatus)}`)
      const indexListToBeDeleted = indexesExistsStatus.reduce( (acc, item) => {
        if(item.status) {
          return [...acc,item.index]
        }
        return [...acc]
      },[])
      console.log("Indexes to be deleted", JSON.stringify(indexListToBeDeleted,null,2))
      if (!isEmpty(indexListToBeDeleted)) {
        const deleteRes = await clearIndex(indexListToBeDeleted.join())
        console.log(
          "=> Delete Dangling Indexes Res",
          JSON.stringify(deleteRes, null, 2)
        )
        return {done:true, status:`Indexes Deleted - ${indexListToBeDeleted}`}
      }
      return {done:true, status:"Tenant IDs exist but there are no dangling indexes for them"}
    }
    return {done:true, status:"Nothing to delete in elastic search...all clear"}
  } catch (err) {
    console.error(err)
    throw err
  }
}

const buildAllData = async () => {
  const checklistPath = path.join(__dirname, "checklists", "test.csv")
  try {
    // clear dangling indexes
    await clearOldIndexes()
    await cleanAllCollections()
    await createAllTenants()
    await createAllTenantRelatedEntities()
    // await initandreseeddata()
    // await createConfigForEntities()
    // await loadPickValuesForMuniVisorClientsLatest()
    // await loadUIAccessInformation()
    // await loadChecklistsForAllEntitiesWithPath(checklistPath)
    // await loadGloabalReferenceTables()
  } catch (e) {
    console.log("Error Generating graph", e)
  }
}

buildAllData()
  .then(() => {
    console.log("success")
    process.exit(0)
  })
  .catch(e => console.log(e))
