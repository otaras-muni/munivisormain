import faker from "faker"
import _ from "lodash"
import bcrypt from "bcrypt-nodejs"
import mongoose from "mongoose"
import crypto from "crypto"
import helpers from "./../testdatareference/testhelpers"
import {
  EntityUser
} from "./models"


const userAddressRef = ["office 1","office 2"]
const serviceTypeRef = ["Office 365", "Docusign", "EMMA", "Analytics", "Expense Revenue"]
const userTitleRef = [ "VP","ED","Director","Principal"]
const userAddressNameRef = [ "Corporate Office 1", "Corporate Office 2","Corporate Office 3"]
const userFlagsRef =  ["Series 50","MSRB Contact", "EMMA Contact", "Supervisory Principal", "Compliance Officer", "MFP", "Primary Contact", "Procurement Contact", "Finance Contact", "Dist Member" ]
const userFlagsIssuer =  [ "Primary Contact", "Financial Controller", "CFO", "Mayor" ]
const userFlagsThirdParty =  [ "Primary Contact" ]
const yesNoRef = [true, false]
const cntrystate = {"United States":["Pennsylvania", "Rhode Island"]}
const statecity =  {"Pennsylvania":["Youngsville","Yukon","Zionville"],"Rhode Island":["Ashaway","Bradford","Bristol"]}

const useFlagsClientsFirm = [
  "Series 50",
  "Compliance Officer",
  "Municipal Finance Professional (MFP)",
  "Supervisory Principal",
  "Other"
]

const useFlagsClientsTP = [
  "Dist Member",
  "Municipal Finance Professional (MFP)",
  "EMMA Contact",
  "MSRB Contact",
  "Finance Contact",
  "Procurement Contact",
  "Other"
]


faker.locale = "en_US"

export const generateUserAddress = (n) => (_.times(n).map(() => {

  const cntry = "United States"
  const state = helpers.getRandomLookupArray(cntrystate[cntry])
  const city = helpers.getRandomLookupArray(statecity[state])
  const addr1 = faker.address.streetName()
  const addr2 = faker.address.streetName()
  const zip1 = faker.address.zipCode()
  const zip2 = faker.address.zipCode()
  const formattedAddress = `${addr1} ${addr2} ${city} ${state} ${zip1}-${zip2}`

  return {
    addressName: helpers.getRandomLookupArray(userAddressNameRef),
    addressType:helpers.getRandomLookupArray(userAddressRef),
    isPrimary:helpers.getRandomLookupArray([true,false]),
    addressLine1: addr1,
    addressLine2: addr2,
    formattedAddress,
    country: cntry,
    state,
    // county: faker.address.county(),
    city,
    zipCode: {
      zip1,
      zip2
    }
  }}))

export const generateUserAddOns = (n) => (_.times(n).map(() => (helpers.getRandomLookupArray(serviceTypeRef))))

export const generateUserCredentials = (emailId,stp, flag) => {

  const authSTPPassword = "HumHongeKamyab"
  const authSTPToken = crypto.randomBytes(40).toString("hex")

  // const salt = bcrypt.genSaltSync(1)
  // const stpHashedPassword = bcrypt.hashSync(authSTPPassword, salt )
  const stpHashedPassword = "$2a$10$5x.s17pSFL14W8PjskBhwe39ltfyvO77ehAsVWNPjotvqYpWlEM4y"

  return {
    userId:emailId, // Should come from the user Collection
    userEmailId:emailId, // We can get rid of this if we are able to just use the email ID.
    password:stp ? stpHashedPassword : "HumHongeKamyab",  // This is the salted password.
    onboardingStatus:stp?"Done":"created",
    userEmailConfirmString:"",
    userEmailConfirmExpiry:null,
    passwordConfirmString:"",
    passwordConfirmExpiry:null,
    passwordResetIteration:0,
    passwordResetStatus:"",
    passwordResetDate:faker.date.recent(),
    isUserSTPEligible:flag === "NSTP" ? false : stp,
    authSTPToken:(flag === "STP" && stp) ? authSTPToken : null,
    authSTPPassword:(flag === "STP" && stp) ? authSTPPassword : null,
  }
}

export const generateUserEmailsRevised = (n, emailId) => {

  const primaryEmail = {
    emailId,
    emailPrimary:true
  }

  const otherEmails = _.times(n).map(() => ({
    emailId:faker.internet.email(),
    emailPrimary:false
  }))

  return [primaryEmail, ...otherEmails]
}


export const generateUserEmails = (n) => (_.times(n).map(() => ({
  emailId:faker.internet.email(),
  emailPrimary:helpers.getRandomLookupArray(yesNoRef)
})))

const generatePlatformAdminConfig =(emailId) => {
  const salt = bcrypt.genSaltSync(10)
  const hashedpassword = bcrypt.hashSync("PeopleFromIndia", salt )

  return {
    userId:emailId, // Should come from the user Collection
    userEmailId:emailId, // We can get rid of this if we are able to just use the email ID.
    password:hashedpassword,  // This is the salted password.
    onboardingStatus:"Done",
    userEmailConfirmString:"",
    userEmailConfirmExpiry:null,
    passwordConfirmString:"",
    passwordConfirmExpiry:null,
    passwordResetIteration:0,
    passwordResetStatus:"",
    passwordResetDate:faker.date.recent(),
    isUserSTPEligible:false,
    authSTPToken:null,
    authSTPPassword:null
  }
}

export const generateEntityUserData = (ent, emailId, entityType, relToTenant, index ) => {

  let userDetails = {}

  switch (entityType) {
  case "platformadmin":
    userDetails = generatePlatformAdminConfig(emailId)
    break
  case "STP":
    userDetails = generateUserCredentials(emailId,true,"STP")
    break
  case "NSTP":
    userDetails = generateUserCredentials(emailId,true,"NSTP")
    break
  default:
    userDetails = generateUserCredentials(emailId,false,"NOTHING")
  }

  let userFlags = []
  const isPrimaryContact = index === 0 ? "Primary Contact" : ""
  switch (relToTenant) {
  case "FA":
    userFlags = _.sampleSize(useFlagsClientsFirm, Math.floor(Math.random() * useFlagsClientsFirm.length))
    userFlags = _.uniq([...userFlags,"Series 50" ])
    userFlags = _.compact([...userFlags, isPrimaryContact ])
    break
  case "CP":
    userFlags = _.sampleSize(useFlagsClientsTP, Math.floor(Math.random() * useFlagsClientsTP.length))
    userFlags = _.compact([...userFlags, isPrimaryContact ])
    break
  case "TP":
    userFlags = _.sampleSize(useFlagsClientsTP, Math.floor(Math.random() * useFlagsClientsTP.length))
    userFlags = _.compact([...userFlags, isPrimaryContact ])
    break
  default:
    userFlags = _.sampleSize(useFlagsClientsTP, Math.floor(Math.random() * useFlagsClientsTP.length))
  }

  let userEntitlement = null
  
  switch (relToTenant) {
  case "FA":
    userEntitlement = helpers.getRandomLookupArray(["global-edit"]) // [ Global, Transaction ]
    break
  case "CP":
    userEntitlement = helpers.getRandomLookupArray(["tran-edit", "tran-readonly"]) // [ Global, Transaction ]
    break
  case "TP":
    userEntitlement = helpers.getRandomLookupArray(["tran-edit", "tran-readonly"]) // [ Global, Transaction ]
    break
  default:
    userEntitlement = helpers.getRandomLookupArray(["global-edit", "global-readonly", "tran-edit", "tran-readonly", "global-readonly-tran-edit"]) // [ Global, Transaction ]
  }
  

  // console.log("HERE IS THE LOGIN DETAILS",JSON.stringify(userDetails,null,2))

  const userTestData = {
    _id: mongoose.Types.ObjectId(),
    userId:"",
    entityId:ent, // Needs to Link to the entity
    // userFlags: _.sampleSize(userFlagsRef, Math.floor(Math.random() * userFlagsRef.length)), // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
    userFlags, // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
    // userRole:helpers.getRandomLookupArray(["global-edit", "global-readonly", "tran-edit", "tran-readonly", "global-readonly-tran-edit"]), // [ Admin, ReadOnly, Backup ]
    userRole:userEntitlement,
    userEntitlement,
    userFirmName:ent.firmName,
    userFirstName:faker.name.firstName(),
    userMiddleName:"",
    userLastName:faker.name.firstName(),
    userPhone: _.times(_.random(1, 2)).map(() => ({
      countryCode: "+1",
      phoneNumber: `+1 ${faker.phone.phoneNumberFormat(1)}`,
      extension: "",
      phonePrimary:helpers.getRandomLookupArray(yesNoRef)
    })).map( (k,i) => {
      if(i === 0) {
        return {...k,phonePrimary:true}
      }
      return {...k,phonePrimary:false}
    }),
    userFax: _.times(_.random(1, 2)).map(() => ({
      faxNumber: `+1 ${faker.phone.phoneNumberFormat(1)}`,
      faxPrimary: helpers.getRandomLookupArray(yesNoRef)
    })).map( (k,i) => {
      if(i === 0) {
        return {...k,faxPrimary:true}
      }
      return {...k,faxPrimary:false}
    }),
    userEmails:generateUserEmailsRevised(_.random(1, 3), emailId),
    userEmployeeID:_.sampleSize("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 10).join(""),
    userJobTitle:helpers.getRandomLookupArray(userTitleRef),
    userManagerEmail:faker.internet.email(),
    userJoiningDate:faker.date.past(5),
    userExitDate:"",
    userCostCenter:"employee",
    userAddresses:generateUserAddress(1).map( (k,i) => {
      if(i === 0) {
        return {...k,isPrimary:true}
      }
      return {...k,isPrimary:false}
    }),
    userAddOns:generateUserAddOns(1).map( a => a.serviceName),
    userLoginCredentials:userDetails,
    userAddDate:faker.date.past(0.5),
    userUpdateDate:faker.date.past(0.25),
    createdAt:new Date(),
    updatedAt:new Date()
  }

  // console.log(JSON.stringify(userTestData,null,2))
  // const EntityUserSample = new EntityUser(userTestData)
  const EntityUserSample = userTestData
  return EntityUserSample
}
