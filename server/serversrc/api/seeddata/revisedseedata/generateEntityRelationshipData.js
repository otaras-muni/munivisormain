import mongoose from "mongoose"
import {
  EntityRel
} from "./models"

mongoose.Promise = global.Promise

// export const generatEntityRelData = (ent1, ent2,relType) => new EntityRel({
//   _id: mongoose.Types.ObjectId(),
//   entityParty1:ent1,
//   entityParty2:ent2,
//   relationshipType:relType
// })

export const generatEntityRelData = (ent1, ent2,relType) => {
  return new EntityRel({
    _id: mongoose.Types.ObjectId(),
    entityParty1: ent1,
    entityParty2: ent2,
    relationshipType: relType
  })
}
