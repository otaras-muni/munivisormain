import mongoose from "mongoose"

import {
  Config,
  Entity
} from "./models"

const {ObjectID} = require("mongodb")

const DROPDOWN =  require("../testdatareference/globaldropdown")

mongoose.Promise = global.Promise

const ROLES = ["global-edit", "global-readonly", "tran-edit", "tran-readonly",
  "global-readonly-tran-edit"]
const ACCESS = {
  "global-edit": {Entity: 2, EntityUser: 2,TenantBilling:2,Docs:2,Controls:2,
    Notifications:2, Deals: 2,Others:2,ActBusDev:2, EntityRel: 2, RFP: 2,
    Tasks: 2, Config:2, BankLoans:2,Derivatives:2,ActMaRFP:2 ,BusConduct:2,
    ComplaintDetails:2,GeneralAdmin:2,PoliticalContributions:2,
    ProfQualifications:2,ComplSupervisor:2,SuperVisoryObligations:2,
    GiftsAndGratuities:2 },
  "global-readonly": {Entity: 1, EntityUser: 1, TenantBilling:1, Docs:1,
    Controls:1,Notifications:1,Deals: 1, Others:1,ActBusDev:1,EntityRel: 1, RFP: 1,
    Tasks: 1, Config:1,BankLoans:1,Derivatives:1,ActMaRFP:1,BusConduct:1,
    ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,
    GiftsAndGratuities:1},
  "tran-edit": {Entity: 1, EntityUser: 1, Deals: 2, Others:2,ActBusDev:2,
    TenantBilling:1,Docs:1,Controls:1,Notifications:1,EntityRel: 1, RFP: 2,
    Tasks: 2, Config:1,BankLoans:2,Derivatives:2,ActMaRFP:2,BusConduct:1,
    ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,
    GiftsAndGratuities:1},
  "tran-readonly": {Entity: 1, EntityUser: 1, TenantBilling:1,Deals: 1,Others:1,
    ActBusDev:1,Docs:1,Controls:1,Notifications:1,EntityRel: 1, RFP: 1,
    Tasks: 1, Config:1,BankLoans:1,Derivatives:1,ActMaRFP:1,BusConduct:1,
    ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,
    GiftsAndGratuities:1},
  "global-readonly-tran-edit": {Entity: 1, EntityUser: 1, TenantBilling:1,Docs:1,
    Controls:1,Config:1, Notifications:1, EntityRel: 1,
    Tasks:2,Deals: 2, RFP: 2,BankLoans:2,Derivatives:2,ActMaRFP:2,Others:2,
    ActBusDev:2,BusConduct:1, ComplaintDetails:1,GeneralAdmin:1,PoliticalContributions:1,
    ProfQualifications:1,ComplSupervisor:1,SuperVisoryObligations:1,GiftsAndGratuities:1}
}

const RESOURCES = ["Entity","EntityUser","TenantBilling",
  "Deals","EntityRel","RFP","Tasks","Config", "BankLoans","Derivatives","ActMaRFP","Docs","Others","ActBusDev",
  "Controls","Notifications",
  "BusConduct","ComplaintDetails","GeneralAdmin","PoliticalContributions","ProfQualifications","ComplSupervisor","SuperVisoryObligations","GiftsAndGratuities"
]

export const getResourceAccess = (resource, role) => ({
  resource,
  access: ACCESS[role][resource]
})

const navLevels = {
  dash: ["dash-task","dash-projs","dash-cltprosp","dash-cal","dash-busdev"],
  addnew:["addnew-entity","addnew-activity","addnew-contact","addnew-task"],
  mast:["mast-cltprosp","mast-thirdparty","mast-allcontacts"],
  comp:["comp-msrbsecfil","comp-polandproc","comp-orgdocuments","comp-stbusentfilandlicenses","comp-engletandforms","comp-custcomp"],
  tools:["tools-analytics","tools-revpipe","tools-billing","tools-chat","tools-docs","tools-emma","tools-globalsearch"],
  admin:["admin-firms","admin-firmusers","admin-clients","admin-thirdparties","admin-config","admin-migtools","admin-usage","admin-billing"],
}


export const getUIAccess = role => {
  const topLevelKeys = Object.keys(navLevels)
  const navObject = topLevelKeys.reduce( (finalObject, topKey) => {
    const adminValue = topKey === "admin" && role === "global-edit"
    const supathObj = navLevels[topKey].map( sub => ({
      path:sub,
      access:topKey === "admin" ? adminValue : true,
      subpath:[]
    }))
    const retObj = {  path : topKey, access : topKey === "admin" ? adminValue : true,    subpath : supathObj }
    return [...finalObject,retObj]

  },[])
  return {primaryNav: navObject}
}

// export const getUIAccess = role => ({
//   primaryNav: [
//     {
//       path: "process",
//       access: true,
//       subpath: [
//         {
//           path: "dashboard",
//           access: true,
//           subpath: []
//         },
//         {
//           path: "transactions",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "opportunities",
//           access: true,
//           subpath: []
//         }
//       ]
//     },
//     {

//       path: "data",
//       access: true,
//       subpath: [
//         {

//           path: "documents",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "emma-content",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "relationships",
//           access: true,
//           subpath: []
//         }
//       ]
//     },
//     {

//       path: "tools",
//       access: true,
//       subpath: [
//         {

//           path: "chat",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "search",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "billing",
//           access: true,
//           subpath: []
//         }
//       ]
//     },
//     {

//       path: "analytics",
//       access: true,
//       subpath: [
//         {

//           path: "calculators",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "arbitrage",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "pricingStructuring",
//           access: true,
//           subpath: []
//         },
//         {

//           path: "allocations",
//           access: true,
//           subpath: []
//         }
//       ]
//     },
//     {

//       path: "admin",
//       access: role === "global-edit",
//       subpath: [
//         {

//           path: "firms",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "users",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "clients",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "third-parties",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "configuration",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "migration-tools",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "usage",
//           access: role === "global-edit",
//           subpath: []
//         },
//         {

//           path: "billing",
//           access: role === "global-edit",
//           subpath: []
//         }
//       ]
//     }
//   ]
// })

export const getAccessPolicyForRole = role => ({

  role,
  resourceAccess: RESOURCES.map(resource => getResourceAccess(resource, role)),
  UIAccess: getUIAccess(role)
})

export const generateAccessPolicyData = () => ROLES.map(r => getAccessPolicyForRole(r))

export const generateChecklistsData = () => []



export const generatePicklistsData = () => {
  const keys = Object.keys(DROPDOWN)
  const picklists = {}
  const picklistsMeta = {}

  const picklistMetaItem = {

  	systemName: "",
  	subListLevel2:false,
  	subListLevel3:false,
  	systemConfig:true,
  	restrictedList:false,
  	externalList:false,
  	bestPractice:true
  }

  keys.forEach(k => {
  	picklistsMeta[k] = { ...picklistMetaItem, systemName: k }
  	const items = DROPDOWN[k].map(e => e.label).filter(v => v)
  	picklists[k] = items.map(e => ({

  			label: e,
  			included: true,
  			visible: true,
  			items: []
  		}))
  })

  const  mapAccordionsObjectToArray = (data, otherData={}) => Object.keys(data).map(k => ({

    title: k,
    ...otherData[k],
    items: data[k]
  }))

  const otherData = {}
  Object.keys(picklistsMeta).forEach(k => {
  	otherData[k] = {_id: mongoose.Types.ObjectId()}
  	otherData[k].meta = picklistsMeta[k]
  })

  return mapAccordionsObjectToArray(picklists, otherData)
}

export const generateConfigData = entityId => {
  console.log("generating config (No Picklists) for : ", entityId._id)
  try {
    const config =  new Config({
      entityId,
      accessPolicy: generateAccessPolicyData(),
      checklists: generateChecklistsData(),
      picklists: {},
      notifications: { self:true, client:false, thirdParty:false}
    })
    console.log("config created ok for : ", entityId._id)
    return config
  } catch (err) {
    console.log("error in generating config for : ", entityId._id)
    throw err
  }
}

export const updateAccessPolicyDataForAllTenants = async () => {

  console.log("Updating Access Policy for all Munivisor Clients")
  const tenantEntities = await Entity.aggregate([
    {$match:{isMuniVisorClient:true}},
    {$project:{_id:1,msrbFirmName:1,firmName:1}}
  ])

  console.log(JSON.stringify(tenantEntities,null,2))

  const retVals = await tenantEntities.forEach( async (t) => {
    try {
      const updatedConfig = await Config.findOneAndUpdate(
        {entityId:ObjectID(t._id)},
        {$set:{
          accessPolicy: generateAccessPolicyData(),
        }},
        {
          upsert:true,
          new:true
        }
      )

      if(updatedConfig && updatedConfig.entityId) {
        console.log(`SUCCESSFULLY UPDATED ACCESS POLICY FOR TENANT - ${t._id} - ${t.firmName}`)
      }
      else {
        console.log("SOMETHING WENT WRONG IN UPDATING ACCESS POLICY, PLEASE REVIEW")
      }
    } catch(e) {
      console.log(e)
    }
    return `SUCCESSFULLY UPDATED ACCESS POLICY FOR TENANT - ${t._id} - ${t.firmName}`
  }
  )
  return retVals
}
