const fs = require("fs")
const Papa = require("papaparse")

const parseInput = (file) => {
  Papa.parse(file, {
    header: true,
    skipEmptyLines: true,
    delimiter: ",",
    newline: "\r\n",
    complete(results) {
      // data = results
      // console.log(data)
      const data = {}
      console.log("num rows : ", results.data.length)
      const allL0Items = [ ...new Set(results.data.map(e => e.L0).filter(e => e)) ]
      // console.log("allL0Items : ", allL0Items);
      allL0Items.forEach(e0 => {
        data[e0] = { entityTypes: {}, level1: {} }
        const l0Entities = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L0val === "1")).map(e => e["entity-type"]).filter(e => e)) ]
        l0Entities.forEach(en0 => {
          data[e0].entityTypes[en0] = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e["entity-type"] === en0) && (e.L0val === "1")).map(e => e.role).filter(e => e)) ]
        })
        const l1Items = [ ...new Set(results.data.filter(e => e.L0 === e0).map(e => e.L1).filter(e => e)) ]
        l1Items.forEach(e1 => {
          data[e0].level1[e1] = { entityTypes: {}, level2: {} }
          const l1Entities = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1) && (e.L0val === "1") && (e.L1val === "1")).map(e => e["entity-type"]).filter(e => e)) ]
          l1Entities.forEach(en1 => {
            data[e0].level1[e1].entityTypes[en1] = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1) && (e["entity-type"] === en1) && (e.L0val === "1") && (e.L1val === "1")).map(e => e.role).filter(e => e)) ]
          })
          const l2Items = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1)).map(e => e.L2).filter(e => e)) ]
          l2Items.forEach(e2 => {
            data[e0].level1[e1].level2[e2] = { entityTypes: {}, level3: {} }
            const l2Entities = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1) && (e.L2 === e2) && (e.L0val === "1") && (e.L1val === "1") && (e.L2val === "1")).map(e => e["entity-type"]).filter(e => e)) ]
            l2Entities.forEach(en2 => {
              data[e0].level1[e1].level2[e2].entityTypes[en2] =
							[ ...new Set(
							  results.data.filter(e =>
							    (e.L0 === e0) && (e.L1 === e1) && (e.L2 === e2) && (e["entity-type"] === en2) && (e.L0val === "1") && (e.L1val === "1") && (e.L2val === "1")).map(e => e.role).filter(e => e)
							)
							]
            })
            const l3Items = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1) && (e.L2 === e2)).map(e => e.L3).filter(e => e)) ]
            l3Items.forEach(e3 => {
              data[e0].level1[e1].level2[e2].level3[e3] = { entityTypes: {} }
              const l3Entities = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1) && (e.L2 === e2) && (e.L3 === e3) && (e.L0val === "1") && (e.L1val === "1") && (e.L2val === "1") && (e.L3val === "1")).map(e => e["entity-type"]).filter(e => e)) ]
              l3Entities.forEach(en3 => {
                data[e0].level1[e1].level2[e2].level3[e3].entityTypes[en3] = [ ...new Set(results.data.filter(e => (e.L0 === e0) && (e.L1 === e1) && (e.L2 === e2) && (e.L3 === e3) && (e["entity-type"] === en3) && (e.L0val === "1") && (e.L1val === "1") && (e.L2val === "1") && (e.L3val === "1")).map(e => e.role).filter(e => e)) ]
              })
            })
          })
        })
      })
      // console.log(JSON.stringify(data, null, 2))
      fs.writeFileSync(`${__dirname  }/uiaccess/uiao.json`, JSON.stringify(data, null, 2), "utf8")
    }
  })
}

const filePath = `${__dirname  }/uiaccess/uiaccess.csv`
console.log("filePath : ", filePath)
const file = fs.createReadStream(filePath)
parseInput(file)
