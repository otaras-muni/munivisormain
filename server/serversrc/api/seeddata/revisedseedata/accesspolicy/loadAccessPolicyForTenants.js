import { updateAccessPolicyDataForAllTenants} from "./../generateConfigData"

const loadAccessPolicyForAllTenants = async () => {
  try {
    const responseFromUpdate = await updateAccessPolicyDataForAllTenants()
    console.log("Message from Update Access Policy for tenants", responseFromUpdate)
    return "success"
  }
  catch (e) {
    console.log("Unable to load Ui Acess and Entitlements", e)
    return "fail"
  }
}

loadAccessPolicyForAllTenants()
  .then( a => {
    console.log(a)
    process.exit(0)
  })
  .catch( err => {
    console.log("Issues with Loading Access Policy for All Tenants",err)
    process.exit(-1)  
  })

