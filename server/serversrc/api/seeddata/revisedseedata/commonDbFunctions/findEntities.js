import {ObjectID} from "mongodb"
import {
  EntityRel,

} from "./../models"

export const getClientDetailsForFinAdvisor = async (finAdvisor) => {
  let aggReturn
  try{
    aggReturn = await EntityRel.aggregate([
      { $match: { relationshipType:"Client",entityParty1:ObjectID(finAdvisor)} },
      { $project : {"entityParty1":1,"entityParty2":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty1",
          "foreignField": "_id",
          "as": "faDetails"
        }
      },
      { $unwind : "$faDetails"}, 
      { $project : {"entityParty2":1,"faDetails.msrbFirmName":1,"faDetails._id":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "clientDetails"
        }
      },  
      { $project : {"faDetails.msrbFirmName":1,"faDetails._id":1,"clientDetails.msrbFirmName":1,"clientDetails._id":1,"clientDetails.entityFlags":1}},
      // { $match : { "clientDetails.entityFlags.marketRole":"Rebate Services"}},      
      { $unwind : "$clientDetails"},      
      { "$group": {
        "_id": "$faDetails",
        "clientDetails": { "$push": "$clientDetails" }
      }},
      { $project : {_id:0,faDetails:"$_id",clientDetails:1}}
    ])
  } catch(e) {
    console.log("There is an error",e)
  }
  return aggReturn[0]
}

export const getThirdPartiesOfSpecificType = async (finAdvisorId) => {
  let aggReturn
  try{
    aggReturn = await EntityRel.aggregate([
      { $match: { relationshipType:"Third Party",entityParty1:ObjectID(finAdvisorId)} },
      { $project : {"entityParty1":1,"entityParty2":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty1",
          "foreignField": "_id",
          "as": "faDetails"
        }
      },
      { $unwind : "$faDetails"}, 
      { $project : {"entityParty2":1,"faDetails.msrbFirmName":1,"faDetails._id":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "thirdPartyDetails"
        }
      },  
      { $project : {"faDetails.msrbFirmName":1,"faDetails._id":1,"thirdPartyDetails.msrbFirmName":1,"thirdPartyDetails._id":1,"thirdPartyDetails.entityFlags":1}},
      //      { $match : { "thirdPartyDetails.entityFlags.marketRole":serviceType}},  
      // { $project : {"thirdPartyDetails.entityFlags":0}},
      { $unwind : "$thirdPartyDetails"},      
      { "$group": {
        "_id": "$faDetails",
        "thirdPartyDetails": { "$push": "$thirdPartyDetails" }
      }},
      { $project : {_id:0,faDetails:"$_id",thirdPartyDetails:1}}
    ])
  } catch(e) {
    console.log("There is an error",e)
  }
  return aggReturn[0]
}

export const getThirdPartyDetails = async (finAdvisorId,marketRoleType) => {
  let aggReturn
  try{
    aggReturn = await EntityRel.aggregate([
      { $match: { relationshipType:"Third Party",entityParty1:ObjectID(finAdvisorId)} },
      { $project : {"entityParty1":1,"entityParty2":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty1",
          "foreignField": "_id",
          "as": "faDetails"
        }
      },
      { $unwind : "$faDetails"}, 
      { $project : {"entityParty2":1,"faDetails.msrbFirmName":1,"faDetails._id":1}},
      {
        "$lookup": {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "thirdPartyDetails"
        }
      },  
      { $project : {"faDetails.msrbFirmName":1,"faDetails._id":1,"thirdPartyDetails.msrbFirmName":1,"thirdPartyDetails._id":1,"thirdPartyDetails.entityFlags":1}},
      { $match : { "thirdPartyDetails.entityFlags.marketRole":marketRoleType}},  
      { $project : {"thirdPartyDetails.entityFlags":0}},
      { $unwind : "$thirdPartyDetails"},      
      { "$group": {
        "_id": "$faDetails",
        "thirdPartyDetails": { "$push": "$thirdPartyDetails" }
      }},
      { $project : {_id:0,faDetails:"$_id",thirdPartyDetails:1}}
    ])
  } catch(e) {
    console.log("There is an error",e)
  }
  return aggReturn[0]
}