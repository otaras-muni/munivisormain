import  {
  Entity, EntityRel, Config, dbCon
}  from "./models"

import  { getFileData } from "./utils"
import { generateUIAccressObject } from "./config/ui-access"
// const fs = require("fs")
const csv = require("fast-csv")
const path = require("path")

const rows = []
const uiAccess = {}
let uiAccessModel = {}

const ROLES = ["global-edit", "global-readonly", "tran-edit", "tran-readonly", "global-readonly-tran-edit"]
const ENTITY_TYPES = ["Self", "Client", "Third Party", "Prospect", "PlatformAdmin"]
const ENTITY_TYPES_MATCH = {
  "Self": "firm",
  "PlatformAdmin": "firm",
  "Client": "client/prospect",
  "Prospect": "client/prospect",
  "Third Party": "third-parties"
}

const uiaccessfilepath =  path.join(__dirname, "uiaccess", "uiaccess.csv")


const populateAccessPolicy = (role, rows) => {
  const primaryNav = []
  let oldL0 = ""
  let oldL1 = ""
  let UIAccess = { primaryNav: [] }
  let item = {}
  let L1item
  let L2item
  rows.forEach(r => {
    // console.log(r.L0, r.L1, r.L2, oldL0, oldL1, JSON.stringify(item))
    if(r.L0 !== oldL0) {
      // console.log("L0 changed : ", r.L0, oldL0)
      if(oldL0) {
        primaryNav.push(item)
      }
      oldL0 = r.L0
      item = { path: r.L0, access: !!+r.L0val, subpath: [] }
      if(r.L1) {
        oldL1 = r.L1
        L1item = { path: r.L1, access: !!+r.L1val, subpath: [] }
        if(r.L2){
          L2item = { path: r.L2, access: !!+r.L2val, subpath: [] }
          if(r.L3) {
            L2item.subpath.push({ path: r.L3, access: !!+r.L3val, subpath: [] })
          }
          // console.log("pushing level2Item : ", r.L2)
          L1item.subpath.push(L2item)
        }
        // console.log("pushing level1Item : ", r.L1)
        item.subpath.push(L1item)
      }
    } else if(r.L1) {
      if(r.L1 !== oldL1) {
        // console.log("L1 changed : ", r.L1, oldL1)
        oldL1 = r.L1
        L1item = { path: r.L1, access: !!+r.L1val, subpath: [] }
        if(r.L2){
          L2item = { path: r.L2, access: !!+r.L2val, subpath: [] }
          if(r.L3) {
            L2item.subpath.push({ path: r.L3, access: !!+r.L3val, subpath: [] })
          }
          // console.log("pushing level2Item : ", r.L2)
          L1item.subpath.push(L2item)
        }
        // console.log("pushing level1Item : ", r.L1)
        item.subpath.push(L1item)
      } else if(r.L2){
        L2item = { path: r.L2, access: !!+r.L2val, subpath: [] }
        if(r.L3) {
          L2item.subpath.push({ path: r.L3, access: !!+r.L3val, subpath: [] })
        }
        // console.log("pushing level2Item : ", r.L2)
        L1item.subpath.push(L2item)
      }
    }
  })
  if(rows.length) {
    // console.log("pushing last item : ", JSON.stringify(item))
    primaryNav.push(item)
    UIAccess = { primaryNav }
  }
  return UIAccess
}

const populateUIAccess = (rows) => {
  ENTITY_TYPES.forEach(t => {
    uiAccess[t] = {}
    ROLES.forEach(role => {
      const values = rows.filter(e => e["entity-type"] === ENTITY_TYPES_MATCH[t]
        && e.role === role)
      uiAccess[t][role] = populateAccessPolicy(role, values)
    })
  })
  // console.log(JSON.stringify(uiAccess, null, 2))
}

const updateConfig = async () => {
  try {
    const configs = await Config.find({}, {_id: 1, entityId: 1, accessPolicy: 1})
    const len1 = configs.length
    for(let i = 0; i < len1; i++) {
      const entity = await Entity.findOne({ _id: configs[i].entityId })
      const rel = await EntityRel.findOne({ entityParty2: configs[i].entityId })
      const type = rel.relationshipType
      if((type === "Self") !== entity.isMuniVisorClient) {
        console.log("wrong rel type for : ", configs[i].entityId, type, entity.isMuniVisorClient)
      }
      if(uiAccess[type]) {
        const accessPolicy = [...configs[i].accessPolicy]
        accessPolicy.forEach((p, i) => {
          if(uiAccess[type][p.role]) {
            // console.log("uiAccess found for type, role: ", type, p.role)
            accessPolicy[i].UIAccess = uiAccess[type][p.role]
          } else {
            console.log("No uiAccess found for type, role: ", type, p.role)
          }
        })
        await Config.update({ _id: configs[i]._id }, {
          accessPolicy
        })
        console.log("uiAccess updated for : ", configs[i].entityId, entity.firmName, type)
      } else {
        console.log("No uiAccess found for : ", type)
      }
    }
  } catch (err) {
    console.log("err in updateConfig uiAccess : ", err)
  }
}

// const checkWaitingUIAccessUpdate = () => {
//   const len = (global.waitingUIAccessUpdate).length
//   console.log("waitingUIAccessUpdate length : ", len)
//   if(len) {
//     console.log("exiting in : ", new Date() - start)
//     process.exit()
//   }
// }

export const loadUIAccessInformation = async () => {
  console.log("starting loadUIAccessInformation")
  const start = new Date()

  uiAccessModel = await generateUIAccressObject(ROLES,uiaccessfilepath )
  console.log("got ui access data in (ms) : ", new Date() - start)

  const entities = await Entity.aggregate([
    {
      $lookup: {
        from: "entityrels",
        localField: "_id",
        foreignField: "entityParty2",
        as: "rel"
      }
    },
    {
      $project: {
        firmName: 1,
        relType: "$rel.relationshipType",
      }
    },
    {
      $unwind: "$relType"
    },
    {
      $lookup: {
        from: "configs",
        localField: "_id",
        foreignField: "entityId",
        as: "config"
      }
    },
    {
      $unwind: "$config"
    },
    {
      $project: {
        firmName: 1,
        relType: "$relType",
        accessPolicy: "$config.accessPolicy"
      }
    }
  ])

  // console.log("got all entities with accessPolicy in (ms) : ", new Date() - start)

  // const configs = await Config.find({
  //   entityId: { $in: entities.map(e => e._id) }
  // }).select({ _id: 1, entityId: 1, accessPolicy: 1 })

  let updates = []
  // global.waitingUIAccessUpdate = entities.map(e => e._id)

  let subArr = []

  const doneUpdateArr = []
  const errUpdateArr = []

  const len = entities.length
  const partLen = 100
  const iter = parseInt(len/partLen)

  const promisifyUpdate = async (subArr, batchNum) => {
    const batchStart = new Date()
    subArr.forEach((e, i) => {
      const type = e.relType
      if(uiAccessModel[type]) {
        const accessPolicy = [...subArr[i].accessPolicy]
        accessPolicy.forEach((p, j) => {
          if(uiAccessModel[type][p.role]) {
            // console.log("uiAccess found for type, role: ", type, p.role)
            accessPolicy[j].UIAccess = { ...uiAccessModel[type][p.role] }
          } else {
            console.log("No uiAccess found for type, role: ", type, p.role)
            errUpdateArr.push((subArr[i]._id).toString())
          }
        })
        const update = Config.update({ entityId: subArr[i]._id }, {
          accessPolicy
        }).then((res) => {
          doneUpdateArr.push((subArr[i]._id).toString())
          console.log("updated for : ", i, subArr[i]._id, subArr[i].firmName, type, new Date() - start, res)
          // const idx = global.waitingUIAccessUpdate.findIndex(d => d === entities[i]._id)
          // global.waitingUIAccessUpdate.splice(idx, 1)
        }).catch(err => {
          console.log("err in updating for : ", i, subArr[i]._id, subArr[i].firmName, type, err)
          errUpdateArr.push((subArr[i]._id).toString())
        })
        updates.push(update)
        console.log("update promised for : ", i, subArr[i]._id, subArr[i].firmName, type)
      } else {
        console.log("No uiAccess found for : ", type)
        errUpdateArr.push((subArr[i]._id).toString())
      }
    })
    await Promise.all(updates)
    console.log("updates done for batch and batch time(ms) : ", batchNum, new Date() - batchStart)
    updates = []
  }
  for(let i = 0; i < iter; i++) {
    const si = i*partLen
    const ei = (i+1)*partLen
    console.log("doing range : ", si, ei)
    subArr = entities.slice(si, ei)
    await promisifyUpdate(subArr, i)
  }

  console.log("doing last range : ", iter*partLen, len)
  subArr = entities.slice(iter*partLen, len)
  await promisifyUpdate(subArr, iter)

  // subArr = entities.slice(0, 2)
  // await promisifyUpdate(subArr, 1)


  // await Promise.all(updates)

  // setInterval(checkWaitingUIAccessUpdate, 5000)

  console.log("all config done for : ", doneUpdateArr.length, [...new Set(doneUpdateArr)].length)
  console.log("err config in : ", errUpdateArr.length, [...new Set(errUpdateArr)].length)
  console.log("done in time(ms) : ", new Date() - start)
  console.log("Exiting")
  process.exit()
}

export const loadUIAccessInformationOld = () => {
  csv
    // .fromPath(path.resolve("./server/serversrc/api/seeddata/revisedseedata/uiaccess/uiaccess.csv"),
    .fromPath(path.resolve( path.join(__dirname, "uiaccess", "uiaccess.csv")),{headers: true, objectMode: true})
    .on("data", (data) => {
    // console.log(data)
      rows.push(data)
    })
    .on("end", async () => {
    // console.log("done : ", rows)
      populateUIAccess(rows)
      try {
        await updateConfig()
        console.log("done update uiAccess")
      } catch (err) {
        console.log("error in update uiAccess : ", err)
      }
      dbCon.close()
      // process.exit()
      // resolve("Done Loading UI Access")
      // fs.writeFileSync("./server/serversrc/api/seeddata/seedmongo/uiaccess.json", JSON.stringify(uiAccess, null, 2), "utf8")
    })
    .on("error", async () => {
      // reject("There was Error Loading Information")
      console.log("There was Error Loading Information")
    })
}

const updateConfigForOne = async (entityId) => {
  try {
    const configs = await Config.find({entityId}, {_id: 1, entityId: 1, accessPolicy: 1})
    const len1 = configs.length
    for(let i = 0; i < len1; i++) {
      const entity = await Entity.findOne({ _id: configs[i].entityId })
      const rel = await EntityRel.findOne({ entityParty2: configs[i].entityId })
      const type = rel.relationshipType
      if((type === "Self") !== entity.isMuniVisorClient) {
        console.log("wrong rel type for : ", configs[i].entityId, type, entity.isMuniVisorClient)
      }
      // const type = "Self"
      if(uiAccess[type]) {
        const accessPolicy = [...configs[i].accessPolicy]
        accessPolicy.forEach((p, i) => {
          if(uiAccess[type][p.role]) {
            // console.log("uiAccess found for type, role: ", type, p.role)
            accessPolicy[i].UIAccess = uiAccess[type][p.role]
          } else {
            console.log("No uiAccess found for type, role: ", type, p.role)
          }
        })
        await Config.update({ _id: configs[i]._id }, {
          accessPolicy
        })
        console.log("uiAccess updated for : ", configs[i].entityId)
      } else {
        console.log("No uiAccess found for : ", type)
      }
    }
  } catch (err) {
    console.log("err in updateConfig uiAccess : ", err)
  }
}

export const loadUIAccessInformationForOneOld = (entityId) => {
  csv
    // .fromPath(path.resolve("./server/serversrc/api/seeddata/revisedseedata/uiaccess/uiaccess.csv"),
    .fromPath(path.resolve( path.join(__dirname, "uiaccess", "uiaccess.csv")),{headers: true, objectMode: true})
    .on("data", (data) => {
    // console.log(data)
      rows.push(data)
    })
    .on("end", async () => {
    // console.log("done : ", rows)
      populateUIAccess(rows)
      try {
        await updateConfigForOne(entityId)
        console.log("done update uiAccess")
      } catch (err) {
        console.log("error in update uiAccess : ", err)
      }
      dbCon.close()
      // process.exit()
      // resolve("Done Loading UI Access")
      // fs.writeFileSync("./server/serversrc/api/seeddata/seedmongo/uiaccess.json", JSON.stringify(uiAccess, null, 2), "utf8")
    })
    .on("error", async () => {
      // reject("There was Error Loading Information")
      console.log("There was Error Loading Information")
    })
}

export const loadUIAccessInformationForOne = async (entityId) => {
  const filePath = path.join(__dirname, "uiaccess", "uiaccess.csv")
  // const filePath = "./server/serversrc/api/seeddata/revisedseedata/uiaccess/uiaccess.csv"
  try {
    const rows = await getFileData(filePath)
    populateUIAccess(rows)
    await updateConfigForOne(entityId)
    console.log("loadUIAccessInformationForOne done ok")
  } catch (err) {
    console.log("err in loadUIAccessInformationForOne : ", err)
  }
}
