import _ from "lodash"
import { generateEntity } from "./generateEntityData"
import { generateEntityUserData } from "./generateEntityUserData"
import { generatEntityRelData } from "./generateEntityRelationshipData"
import { cleanAllCollections } from "./cleanDbData"
import { generateConfigData } from "./generateConfigData"
import { generateTasksForAllProcesses } from "./generateTaskData"
// import { lookUpCountryStateCity } from "./country"
import { loadPickValuesForMuniVisorClientsLatest } from "./generateAndLoadPicklists1"
import { loadUIAccessInformation } from "./generateAndLoadUIAccess"
import { loadChecklistsForAllEntitiesWithPath } from "./generateAndLoadChecklists"
import { loadGloabalReferenceTables } from "./industryreferencedata/loadDataToGlobalReferenceTables"

const path = require("path")

let munivisorFinAdvisorClients = ["Financial Advisor 1", "Financial Advisor 2", "Financial Advisor 3"]
let issuerList = []
let prospectList = []
let issuerListEmailDomains = []
let prospectListEmailDomains = []
const issuerListMaster = ["City of Tampa", "Florida State", "Hillsborough County Schools", "Other Reference"]
const prospectListMaster = ["City of Princeton", "State of California", "State of Texas", "City of New York"]
const undList = ["Goldman Sachs", "Wells Fargo", "Morgan Stanley", "Stifel", "Bank of America", "PNC Capital Markets"]
const bondCounselList = ["Orrick", "Bond Counsel 1", "Bond Counsel 2"]

let munivisorFinAdvisorClientsEmailDomains = ["finadvisor1.com", "finadvisor2.com", "finadvisor3.com"]
const issuerListEmailDomainsMaster = ["tampa", "flordiastate", "hillsborough", "other"]
const prospectListEmailDomainsMaster = ["princeton", "calif", "texas", "cny"]
const undListEmailDomains = ["goldman.com", "wellsfargo.com", "morganstanley.com", "stifel.com", "bofa.com", "pnc.com"]
const bondCounselListEmailDomains = ["orric.com", "bc1.com", "bc2.com"]

_.times(20).map(indi => {
  munivisorFinAdvisorClients.push("Financial Avdisor " + (indi + 3))
  munivisorFinAdvisorClientsEmailDomains.push("finadvisor" + (indi + 3) + ".com")
})

_.times(50).map((i) => {
  issuerListMaster.map((issuer, j) => issuerList.push(issuer + " " + i))
  prospectListMaster.map((issuer, j) => prospectList.push(issuer + " " + i))
  issuerListEmailDomainsMaster.map((issuer, j) => issuerListEmailDomains.push(issuer + i + ".com"))
  prospectListEmailDomainsMaster.map((issuer, j) => prospectListEmailDomains.push(issuer + i + ".com"))
})

const namesArray = [...munivisorFinAdvisorClients, ...issuerList, ...prospectList, ...undList, ...bondCounselList]
const domainsArray = [...munivisorFinAdvisorClientsEmailDomains, ...issuerListEmailDomains, ...prospectListEmailDomains, ...undListEmailDomains, ...bondCounselListEmailDomains]

const nameDomainMapping = namesArray.reduce((zippedObj, entName, index) => {
  const rootVal = domainsArray[index].split(".")[0]
  return { ...zippedObj, ...{ [entName]: { entityName: entName, domain: domainsArray[index], domainUserRoot: rootVal } } }
}, {})

console.log(nameDomainMapping)


const buildGraph = async () => {

  // await cleanAllCollections()
  console.log("Generating Platform Details")

  const platformAdminEntity = await generateEntity({ entityType: "Platform", entityName: "Otaras,Inc" }).save()
  await generatEntityRelData(platformAdminEntity, platformAdminEntity, "PlatformAdmin").save()
  await generateEntityUserData(platformAdminEntity, "munivisorproddev@otaras.com", "platformadmin").save()
  await generateConfigData(platformAdminEntity).save()

  // await generateEntityUserData(platformAdminEntity,"munivisorproddev@otaras.com","NSTP").save()


  await Promise.all(munivisorFinAdvisorClients.map(async (fa, i) => {
    try {
      const mvEntity = await generateEntity({ entityType: "FA", entityName: fa }).save()
      await generatEntityRelData(mvEntity, mvEntity, "Self").save()

      await generateConfigData(mvEntity).save()

      await Promise.all(_.times(30).map(async (ind) => {
        console.log(`${ind} - USER GENERATION PROCESS - ${mvEntity.firmName}`)
        const { domain } = nameDomainMapping[fa]
        const userEmailId = `user${ind}@${domain}`
        await generateEntityUserData(mvEntity, userEmailId, "NSTP").save()
      }))

      await Promise.all((issuerList.map(async (iss) => {
        const issEntity = await generateEntity({ entityType: "C", entityName: iss }).save()
        await Promise.all(_.times(3).map(async (ind) => {
          console.log(`${ind} - USER GENERATION PROCESS - ${issEntity.firmName}`)
          const { domain } = nameDomainMapping[iss]
          const { domainUserRoot } = nameDomainMapping[fa]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          await generateEntityUserData(issEntity, userEmailId, "STP").save()
        }))
        await generateConfigData(issEntity).save()
        await generatEntityRelData(mvEntity, issEntity, "Client").save()
      })))

      await Promise.all((prospectList.map(async (prospect) => {
        const prospectEntity = await generateEntity({ entityType: "P", entityName: prospect }).save()
        await Promise.all(_.times(30).map(async (ind) => {
          console.log(`${ind} - USER GENERATION PROCESS - ${prospectEntity.firmName}`)
          const { domain } = nameDomainMapping[prospect]
          const { domainUserRoot } = nameDomainMapping[fa]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          await generateEntityUserData(prospectEntity, userEmailId, "STP").save()
        }))
        await generateConfigData(prospectEntity).save()
        await generatEntityRelData(mvEntity, prospectEntity, "Prospect").save()
      })))


      await Promise.all((undList.map(async (und) => {
        const undEntity = await generateEntity({ entityType: "U", entityName: und }).save()
        await Promise.all(_.times(3).map(async (ind) => {
          console.log(`${ind} - USER GENERATION PROCESS - ${undEntity.firmName}`)
          const { domain } = nameDomainMapping[und]
          const { domainUserRoot } = nameDomainMapping[fa]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          await generateEntityUserData(undEntity, userEmailId, "STP").save()
        }))
        await generateConfigData(undEntity).save()
        await generatEntityRelData(mvEntity, undEntity, "Third Party").save()
      })))

      await Promise.all((bondCounselList.map(async (bc) => {
        const bcEntity = await generateEntity({ entityType: "BC", entityName: bc }).save()
        await Promise.all(_.times(3).map(async (ind) => {
          console.log(`${ind} - USER GENERATION PROCESS - ${bcEntity.firmName}`)
          const { domain } = nameDomainMapping[bc]
          const { domainUserRoot } = nameDomainMapping[fa]
          const userEmailId = `user${ind}_${domainUserRoot}@${domain}`
          await generateEntityUserData(bcEntity, userEmailId, "STP").save()
        }))
        await generateConfigData(bcEntity).save()
        await generatEntityRelData(mvEntity, bcEntity, "Third Party").save()
      })))
      console.log(`All entity graphs successfully created for ${fa}`)
    }
    catch (e) {
      console.log("There is an error  in the Entity creation process", e)
    }
  }))
}

const buildAllData = async () => {
  const checklistPath = path.join(__dirname, "checklists", "test.csv")
  try {
    await buildGraph()
    await loadPickValuesForMuniVisorClientsLatest()
    await loadUIAccessInformation()
    await loadChecklistsForAllEntitiesWithPath(checklistPath)
    await loadGloabalReferenceTables()
  }
  catch (e) {
    console.log("Error Generating graph", e)
  }
}

buildAllData().then(() => console.log("success")).catch((e) => console.log(e))


