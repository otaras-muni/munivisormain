import mongoose from "mongoose"
import {Controls, Entity} from "./../models"

const fs = require("fs")
const path = require("path")
const csv = require("fast-csv")
const {ObjectID, MongoClient} = require("mongodb")

const sourceDBURL = "mongodb://munivisor_staging:HSEoRRx68qRXxSxM@munivisor-dev-test-shard-00-00-ymfrh.mongodb.net:27017,munivisor-dev-test-shard-00-01-ymfrh.mongodb.net:27017,munivisor-dev-test-shard-00-02-ymfrh.mongodb.net:27017/munivisor_staging?ssl=true&replicaSet=munivisor-dev-test-shard-0&authSource=admin"
const sourceDBName = "munivisor_staging"

mongoose.connect(process.env.DB_URL, {useNewUrlParser: true})

const getSourceCACData = async(dbUrl, dbName) => {
  let client
  try {
    client = await MongoClient.connect(dbUrl)
    const db = await client.db(dbName)
    console.log("connected to source DB ok")
    const controlsCollection = await db.collection("controls")
    const controls = await controlsCollection
      .find({})
      .toArray()
    if (!controls.length) {
      console.log("no controls found in source")
      return []
    }
    const sortedControls = [...controls]
    sortedControls.forEach((e, i) => {
      sortedControls[i] = {
        ...e
      }
      console.log("controls length original : ", e.controls.length)
      sortedControls[i].controls = e
        .controls
        .filter(c => !(/^CTRLTTACC/.test(c.id)))
      console.log("controls length after filter : ", sortedControls[i].controls.length)
    })
    console.log("sortedControls length : ", sortedControls.length)
    sortedControls.sort((a, b) => b.controls.length - a.controls.length)
    const sourceControl = sortedControls[0]
    console.log("selecting source control with numbers : ", sourceControl.controls.length)
    client.close()
    return sourceControl.controls
  
  } catch (err) {
    console.log("err in getSourceCACData : ", err)
    if (client) {
      client.close()
    }
  }
}

const getSourceCopy = (sourceControls) => {
  const controls = []
  sourceControls.forEach(e => {
    const control = {
      ...e,
      refId: ObjectID(),
      notificationSentDate: null,
      actionCompletedBy: 0,
      numActions: 0,
      dueDate: null,
      notification: {
        ...e.notification,
        startDate: e.notification.startDate
          ? new Date(e.notification.startDate)
          : null,
        endDate: e.notification.endDate
          ? new Date(e.notification.endDate)
          : null
      },
      state: "enabled",
      status: "open",
      saveType: "draft",
      recipients: [],
      target:"others",
      toList: [],
      ccList: [],
      relatedActivity: [],
      relatedEntity: [],
      relatedContact: [],
      docIds: [],
      checklist: e.checklist
        ? e
          .checklist
          .map(e => ({
            ...e
          }))
        : [],
      createdBy: "admin@otaras.com",
      lastUpdated: {
        date: new Date(),
        by: "admin@otaras.com"
      }
    }
    controls.push(control)
  })
  return controls
}

const createJsonFile = async(sourceDBURL, sourceDBName) => {
  try {
    console.log("The JSON file to be downloaded")
    const sourceControls = await getSourceCACData(sourceDBURL, sourceDBName)
    console.log(Object.keys(sourceControls))
    await new Promise(((resolve, reject) => {
      fs.writeFile("./controls.json", JSON.stringify(sourceControls,null,4), "utf-8", (err) => {
        if (err) reject(err)
        else resolve(sourceControls)
      })
    }))
    return true
  } catch (e) {
    console.log("There is an error getting the data from JSON file", e)
    return false
  }
}

const readJsonfileAndCopyToTargetProcess = async (fileName) => {
  // const jsonPath = path.resolve(path.join(fileName))
  const jsonPath=  path.resolve(path.join(__dirname, fileName))

  console.log("The Path:",jsonPath)
  // const jsonPath = path.join(__dirname, "globissuer.json")
  const jsonString = fs.readFileSync(jsonPath, "utf8")
  const sourceControls = JSON.parse(jsonString)
  console.log("The path of the file is",Object.keys(sourceControls) )
  if (sourceControls.length) {
    const entities = await Entity
      .find({isMuniVisorClient: true})
      .select({_id: 1, firmName: 1})
    console.log("found tenants : ", entities.length)
    for (let i = 0; i < entities.length; i++) {
      const {_id, firmName} = entities[i]
      const control = await Controls.findOne({entityId: _id})
      if (!control) {
        const controls = getSourceCopy(sourceControls)
        console.log("copying controls numbers : ", controls.length)
        try {
          await Controls.create({entityId: _id, controls})
          console.log("inserted control ok for entity : ", _id, firmName)
        } catch (err) {
          console.log("err in inserting control for entity : ", _id, firmName, err)
        }
      } else {
        console.log("controls already present for entity : ", _id, firmName)
      }
    }
  } else {
    console.log("no controls found to copy")
  }
  return true
}
export const readJsonfileAndCopyToTargetStandAlone = async (fileName) => {
  await readJsonfileAndCopyToTargetProcess(fileName)
  process.exit()
}

export const readJsonfileAndCopyToTarget = async (fileName) => {
  try {
    await readJsonfileAndCopyToTargetProcess(fileName)
  }
  catch (e) {
    console.log("There is an Error in Loading Information", e)
  }
}



const copyToTarget = async(sourceDBURL, sourceDBName) => {

  const sourceControls = await getSourceCACData(sourceDBURL, sourceDBName)
  if (sourceControls.length) {
    const entities = await Entity
      .find({isMuniVisorClient: true})
      .select({_id: 1, msrbFirmName: 1})
    console.log("found tenants : ", entities.length)
    for (let i = 0; i < entities.length; i++) {
      const {_id, msrbFirmName} = entities[i]
      const control = await Controls.findOne({entityId: _id})
      if (!control) {
        const controls = getSourceCopy(sourceControls)
        console.log("copying controls numbers : ", controls.length)
        try {
          await Controls.create({entityId: _id, controls})
          console.log("inserted control ok for entity : ", _id, msrbFirmName)
        } catch (err) {
          console.log("err in inserting control for entity : ", _id, msrbFirmName, err)
        }
      } else {
        console.log("controls already present for entity : ", _id, msrbFirmName)
      }
    }
  } else {
    console.log("no controls found to copy")
  }
  process.exit()
}

// createJsonFile(sourceDBURL, sourceDBName).then(a => {
//   readJsonfileAndCopyToTarget("controls.json")
//   console.log(a)
// })

// readJsonfileAndCopyToTargetStandAlone("controls.json").then( a => console.log( JSON.stringify( a, null, 2)))
