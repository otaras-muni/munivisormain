import faker from "faker"
import  crypto  from "crypto"
import {ObjectID} from "mongodb"

import {
  EntityRel,
  Entity,
  Tasks,Deals,RFP, BankLoans,Derivatives
} from "./models"

import helpers from "./../testdatareference/testhelpers"

const getProcessDetails = async() =>{
  const existsInDeal = await Deals.aggregate([
    {
      $project: {
        processContext : "$_id",
        processType : "Debt",
        processTenantId : "$dealIssueTranClientId",
        processSubType : "Bond Issue",
        processClientId : "$dealIssueTranIssuerId",
        processProjectDesc : "$dealIssueTranName",
        processUserIds:{ $setUnion: [ "$dealIssueTranAssignedTo","$dealIssueParticipants.dealPartContactId" ] }
      }
    },
    {
      $lookup: {
        "from": "entities",
        "localField": "processClientId",
        "foreignField": "_id",
        "as": "issuerDetails"
      }
    },
    {$unwind:"$issuerDetails"},
    {
      $project: {
        processContext : 1,
        processType : 1,
        processTenantId : 1,
        processSubType : 1,
        processClientId : 1,
        processClientName : "$issuerDetails.firmName",
        processProjectDesc : 1,
        processUserIds:1
      }
    },

  ])

  const existsInRFP = await RFP.aggregate([
    {
      $project: {
        processContext : "$_id",
        processTenantId : "$rfpTranClientId",
        processType : "Managed Client RFP",
        processSubType : "$rfpTranPurposeOfRequest",
        processClientId : "$rfpTranClientId",
        processProjectDesc : "$rfpTranName",
        processUserIds:{ $setUnion: [ "$rfpTranAssignedTo","$rfpEvaluationTeam.rfpSelEvalContactId","$rfpProcessContacts.rfpProcessContactId","$rfpParticipants.rfpParticipantContactId" ] }
      }
    },
    {
      $lookup: {
        "from": "entities",
        "localField": "processClientId",
        "foreignField": "_id",
        "as": "issuerDetails"
      }
    },
    {$unwind:"$issuerDetails"},

    {
      $project: {
        processContext : 1,
        processType : 1,
        processTenantId : 1,
        processSubType : 1,
        processClientId : 1,
        processClientName : "$issuerDetails.firmName",
        processProjectDesc : 1,
        processUserIds:1
      }
    }
    
  ])

  const existsInBankLoans = await BankLoans.aggregate([
    {
      $project: {
        processContext : "$_id",
        processTenantId : "$actTranFirmId",
        processType : "$actTranType",
        processSubType : "$actTranSubType",
        processClientId : "$actTranClientId",
        processProjectDesc : "$actTranProjectDescription",
        processUserIds:{ $setUnion: [ ["$actTranFirmLeadAdvisorId"],"$bankLoanParticipants.partContactId"] }
      }
    },
    {
      $lookup: {
        "from": "entities",
        "localField": "processClientId",
        "foreignField": "_id",
        "as": "issuerDetails"
      }
    },
    {$unwind:"$issuerDetails"},

    {
      $project: {
        processContext : 1,
        processType : 1,
        processTenantId : 1,
        processSubType : 1,
        processClientId : 1,
        processClientName : "$issuerDetails.firmName",
        processProjectDesc : 1,
        processUserIds:1
      }
    }
  ])

  const existsInDerivatives = await Derivatives.aggregate([
    {
      $project: {
        processContext : "$_id",
        processTenantId : "$actTranFirmId",
        processType : "$actTranType",
        processSubType : "$actTranSubType",
        processClientId : "$actTranClientId",
        processProjectDesc : "$actTranProjectDescription",
        processUserIds:{ $setUnion: [ ["$actTranFirmLeadAdvisorId"],"$derivativeParticipants.partContactId"] }
      }
    },
    {
      $lookup: {
        "from": "entities",
        "localField": "processClientId",
        "foreignField": "_id",
        "as": "issuerDetails"
      }
    },
    {$unwind:"$issuerDetails"},
    {
      $project: {
        processContext : 1,
        processType : 1,
        processTenantId : 1,
        processSubType : 1,
        processClientId : 1,
        processClientName : "$issuerDetails.firmName",
        processProjectDesc : 1,
        processUserIds:1

      }
    }
  ])
  const allprocessInfo = [...existsInDeal,...existsInDerivatives,...existsInRFP,...existsInBankLoans]
  // console.log( "-----PROCESS INFO",JSON.stringify([...existsInDeal],null,2))
  // const allprocessInfo = [...existsInDeal]
  return allprocessInfo
}

const getTenantUsers = async () => {
  
  const tenantUsers = await Entity.aggregate(
    [
      {
        $match :{isMuniVisorClient:true}
      },
      {
        $project:{firmName:1,msrbFirmName:1,entityId:"$_id"}
      },
      {
        $lookup: {
          "from": "entityusers",
          "localField": "entityId",
          "foreignField": "entityId",
          "as": "tenantUserInfo"
        }
      },
      {
        $unwind:"$tenantUserInfo"
      },
      {
        $project:{
          tenantId:"$entityId",
          userId:"$tenantUserInfo._id",
          userFirstName:"$tenantUserInfo.userFirstName",
          userLastName:"$tenantUserInfo.userLastName",
          userEntityId:"$entityId",
          userEntityName:"$msrbFirmName"          
        }
      }
    ]
  )

  return tenantUsers
}

const getAllUserDetails = async () => {
  const tenantUsers = await EntityRel.aggregate(
    [
      {
        $project:{tenantId:"$entityParty1",entityId:"$entityParty2"}
      },
      {
        $lookup: {
          "from": "entities",
          "localField": "entityId",
          "foreignField": "_id",
          "as": "entityInfo"
        }
      },   
      {
        $unwind:"$entityInfo"
      },         
      {
        $project:{tenantId:1,entityId:1,firmName:"$entityInfo.firmName",isMuniVisorClient:"$entityInfo.isMuniVisorClient"}
      },
      // {
      //   $match:{entityId:ObjectID("5b6064d88c4edb1624d87f77")}
      // } ,     
      {
        $lookup: {
          "from": "entityusers",
          "localField": "entityId",
          "foreignField": "entityId",
          "as": "otherUserInfo"
        }
      },
      {
        $unwind:"$otherUserInfo"
      },
      {
        $project:{
          _id:0,
          tenantId:1,
          userId:"$otherUserInfo._id",
          userFirstName:"$otherUserInfo.userFirstName",
          userLastName:"$otherUserInfo.userLastName",
          userEntityId:"$otherUserInfo.entityId",
          userEntityName:"$firmName",
          isMuniVisorClient:1          
        }
      },
      // {
      //   "$group": {
      //     "_id": "$tenantId",
      //     "userDetails": {
      //       "$push": {
      //         tenantId:"$tenantId",
      //         userId:"$userId",
      //         userFirstName:"$userFirstName",
      //         userLastName:"$userLastName",
      //         userEntityId:"$userEntityId",
      //         userEntityName:"$userEntityName",
      //         isMuniVisorClient:"$isMuniVisorClient"   
      //       }
      //     }
      //   }
      // }
    ]
  )

  const allTenantUsers = tenantUsers.filter( f => f.isMuniVisorClient ).reduce( (acc, u) => ({ ...acc,[u.userId]:u}),{})  
  const allUsers = tenantUsers.reduce( (acc, u) => ({ ...acc,[u.userId]:u}),{})  
  const allTenantUserUpdated = tenantUsers.filter( f => f.isMuniVisorClient ).reduce( (acc, u) => {
    const {tenantId} = u
    if( acc[tenantId]) {
      acc[tenantId].push(u)
    } else {
      acc[tenantId] = [u]
    }
    return acc
  },{}) 
  
  // console.log(JSON.stringify(allTenantUserUpdated,null,2))
  return {allTenantUsers:allTenantUserUpdated,allUsers}
}

export const generateTasksForAllProcesses = async () => {

  // Get some random Test data
  const processContexts = ["SOE","CHK1","CHK2","CHK3","CHK4","CHK5"]
  const taskDescriptions = ["Financing Plan","Review Docs", "Ratings done","Type of Service"] 
  const taskAssigneeType = ["Fin Adv","Bond Counsel","Underwriter","Other"]
  const contextHeader = ["Manage Deals","SOE Section 1", "SOE Section 2", "SOE Section 3", "SOE Section 4", "TRANSACTION DETAILS 1"]

  const processInfo = await getProcessDetails()
  const allUserInfo = await getAllUserDetails()

  // Find all the Users
  const {allTenantUsers,allUsers} = allUserInfo
  // console.log(JSON.stringify(processInfo,null,2))
  
  const tasksToInsert = processInfo.reduce ( (acc,p) => {
    const { processUserIds  } = p
    

    const tasksForAllTenantUsers = allTenantUsers[p.processTenantId].reduce( ( accTenUser, tenUser) =>     {
      const {tenantId} = tenUser
      const rtenantuser = helpers.getRandomLookupArray(allTenantUsers[tenantId])
      const {tenantId:t1,isMuniVisorClient:ign1,...userInfo} = tenUser
      const {tenantId:t2,isMuniVisorClient:ign2,...tenantUserInfo} = rtenantuser

      const taskObjectsToInsert = taskDescriptions.map( td => {
        const obj = {
          taskUniqueIdentifier:{
            activityId:p.processContext,
            activityContext:helpers.getRandomLookupArray(processContexts),
            taskAssigneeUserId:tenUser.userId,
            activityContextTaskId:ObjectID()
          },
          taskDetails:{
            taskAssigneeUserId:ObjectID(tenUser.userId),
            taskAssigneeName:tenUser.userFirstName,
            taskAssigneeType:helpers.getRandomLookupArray(taskAssigneeType),
            taskDescription:td,
            taskStartDate:faker.date.past(0.2),
            taskEndDate:faker.date.past(0.1),
            taskPriority:helpers.getRandomLookupArray(["Low","Medium","High"]),
            taskStatus: helpers.getRandomLookupArray(["Open","Closed"]),
            taskFollowing:true
          },
          relatedActivityDetails:{
            activityId:p.processContext,
            activityType:p.processType,
            activitySubType:p.processSubType,
            activityProjectName: p.processProjectDesc,
            activityIssuerClientId:p.processClientId,
            activityIssuerClientName:p.processClientName,             
            activityContext:helpers.getRandomLookupArray(processContexts),
            activityContextSection:helpers.getRandomLookupArray(contextHeader),
            activityContextTaskId:ObjectID(),
          },
          relatedEntityDetails:{
          },
          relatedUserDetails:{
          },
          taskAssigneeUserDetails:{
            ...userInfo
          },
          taskCreatedBy:{
            ...tenantUserInfo
          }
        }
        const {taskDetails} = obj
        const {taskFollowing,...coreTaskInfo} = taskDetails
        // Generate a unique String Information
        const taskIdentifier = crypto.createHash("md5").update(JSON.stringify(obj.taskUniqueIdentifier)).digest("hex")
        const taskContentHash = crypto.createHash("md5").update(JSON.stringify(coreTaskInfo)).digest("hex")
  
        return {...obj,taskIdentifier,taskContentHash}
      })
      
      return [...accTenUser,...taskObjectsToInsert]
    },[])


    const tasksForAllProcessUsers = processUserIds.reduce( (accprUser, prUser) => 
    {
      const ruser = allUsers[prUser]
      const {tenantId} = ruser
      const rtenantuser = helpers.getRandomLookupArray(allTenantUsers[tenantId])
      const {tenantId:t1,isMuniVisorClient:ign1,...userInfo} = ruser
      const {tenantId:t2,isMuniVisorClient:ign2,...tenantUserInfo} = rtenantuser

      const taskObjectsToInsert = taskDescriptions.map( td => {
        const obj = {
          taskUniqueIdentifier:{
            activityId:p.processContext,
            activityContext:helpers.getRandomLookupArray(processContexts),
            taskAssigneeUserId:ruser.userId,
            activityContextTaskId:ObjectID()
          },
          taskDetails:{
            taskAssigneeUserId:ObjectID(ruser.userId),
            taskAssigneeName:ruser.userFirstName,
            taskAssigneeType:helpers.getRandomLookupArray(taskAssigneeType),
            taskDescription:td,
            taskStartDate:faker.date.past(0.2),
            taskEndDate:faker.date.past(0.1),
            taskPriority:helpers.getRandomLookupArray(["Low","Medium","High"]),
            taskStatus: helpers.getRandomLookupArray(["Open","Closed"]),
            taskFollowing:true
          },
          relatedActivityDetails:{
            activityId:p.processContext,
            activityType:p.processType,
            activitySubType:p.processSubType,
            activityProjectName: p.processProjectDesc,
            activityIssuerClientId:p.processClientId,
            activityIssuerClientName:p.processClientName,             
            activityContext:helpers.getRandomLookupArray(processContexts),
            activityContextSection:helpers.getRandomLookupArray(contextHeader),
            activityContextTaskId:ObjectID(),
          },
          relatedEntityDetails:{
          },
          relatedUserDetails:{
          },
          taskAssigneeUserDetails:{
            ...userInfo
          },
          taskCreatedBy:{
            ...tenantUserInfo
          }
        }
        const {taskDetails} = obj
        const {taskFollowing,...coreTaskInfo} = taskDetails
        // Generate a unique String Information
        const taskIdentifier = crypto.createHash("md5").update(JSON.stringify(obj.taskUniqueIdentifier)).digest("hex")
        const taskContentHash = crypto.createHash("md5").update(JSON.stringify(coreTaskInfo)).digest("hex")
  
        return {...obj,taskIdentifier,taskContentHash}
      })
      
      return [...accprUser,...taskObjectsToInsert]
    },[])





    return [...acc,...tasksForAllProcessUsers,...tasksForAllTenantUsers]
  },[])
  console.log("NUMBER OF TASKS TO BE INSERTED",tasksToInsert.length)

  try {
    const allRemoved = await Tasks.remove({})
    console.log("Number of tasks removed", allRemoved.n)
    const docsInserted = await Tasks.insertMany(tasksToInsert)
    console.log("NUMBER OF TASKS INSERTED",docsInserted.length)
    return docsInserted.length
  } catch (e) {
    console.log(e)
  }
}
// generateTasksForAllProcesses().then( a => console.log(`successfully inserted tasks - ${a}`))

