import faker from "faker"
import _ from "lodash"
import mongoose from "mongoose"
import crypto from "crypto"
import helpers from "./../testdatareference/testhelpers"
import {Entity} from "./models"

mongoose.Promise = global.Promise

const entityAddressRef = ["headquarters", "satellite office", "secondary", "other"]
const serviceTypeRef = ["Office 365", "Docusign", "EMMA", "Analytics", "Expense Revenue"]

const marketRoleRefLookup = {
  "BC": [
    "Bank Counsel",
    "Bond Counsel",
    "Disclosure Counsel",
    "General/Local Counsel",
    "Trustee Counsel",
    "UW Counsel",
  ],
  "U": [
    "Banking Services",
    "Bank Loans / Lease",
    "Underwriting Services",
  ],
  "C": [],
  "P": [],
  "FA": [
    "Swap Advisor",
    "Municipal Advisor",
    "Financial Advisor"
  ],
  "Other":[
    "Credit Enhancement Provider (CEP)",
    "Non-Bank Purchaser",
    "Paying Agent",
    "Placement Agent",
    "Printer",
    "Rating Agency",
    "Registrar",
    "Trustee",
    "Verification Agent"
  ]
}

/*

LKUPPARTICIPANTTYPE,Participant Type (Working Group),Banking Services,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Bank Loans / Lease,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Bank Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Bond Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Bond Insurer,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Credit Enhancement Provider (CEP),,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Disclosure Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Financial Advisor,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),General or Local Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Non-Bank Purchaser,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Paying Agent,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Placement Agent,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Printer,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Registrar,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Swap Advisor,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Trustee,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Trustee Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Underwriting Services,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),UW Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Verification Agent,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Rating Agency,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Disclosure Dissemination Agent,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),District's Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Issuer Counsel,,
LKUPPARTICIPANTTYPE,Participant Type (Working Group),Not Configured,,

*/


const marketRoleRef = [
  "Bank or Lender",
  "Bank Counsel",
  "Bond Counsel",
  "Bond Insurer",
  "Credit Enhancer",
  "Disclosure Counsel",
  "Financial Advisor",
  "General/Local Counsel",
  "Non-Bank Purchaser",
  "Paying Agent",
  "Placement Agent",
  "Printer",
  "Rating Agencies",
  "Registrar",
  "Swap Advisor",
  "Trustee",
  "Trustee Counsel",
  "Underwriting Services",
  "UW Counsel",
  "Verification Agent"
]

/*
LKUPISSUERFLAG,Issuer Type, City Issuer,,
LKUPISSUERFLAG,Issuer Type, County Issuer,,
LKUPISSUERFLAG,Issuer Type, Local Authority,,
LKUPISSUERFLAG,Issuer Type, Locality,,
LKUPISSUERFLAG,Issuer Type, School District,,
LKUPISSUERFLAG,Issuer Type, State Authority,,
LKUPISSUERFLAG,Issuer Type, State Issuer,,
LKUPISSUERFLAG,Issuer Type, Airport,,
LKUPISSUERFLAG,Issuer Type, Seaport,,
LKUPISSUERFLAG,Issuer Type, Special District,,
LKUPISSUERFLAG,Issuer Type, Municipal Utility,,
LKUPISSUERFLAG,Issuer Type, Joint Action Agency,,
LKUPISSUERFLAG,Issuer Type, Other,,
*/

const issuerTypeRole = [
  "State Issuer",
  "State Authority ",
  "City Issuer",
  "County Issuer",
  "Local Authority",
  "Locality",
  "Other"
]
const yesNoRef = [true, false]
const conduitBorrowerRef = ["conduit", "borrower"]
const msrbRegCat = ["Issuer", "Broker Dealer Only", "Municipal Securities Dealer", "Municipal Advisor Only", "Broker and Municipal Advisor"]
const emplCat = ["< 5", "5 to 20", "20 +"]
const revRef = ["Less than 5 M", "5M to 20M", "20M +"]

const primarySecondaryRef = {
  "HC-HealthCare": [
    "Hospital equipment loans", "Hospitals", "Lifecare/retirement centers", "Nursing homes", "Other Health Care"
  ],
  "HSG-Housing": [
    "CMO-backed housing",
    "Multi-family housing",
    "New public housing",
    "Other housing",
    "Senior Hsg Independent Living",
    "Single/multi-family housing",
    "Single-family housing",
    "Student Housing"
  ],
  "IND-Industrial Development": [
    "Economic development",
    "Hotel",
    "Industrial development",
    "Malls/shopping centers",
    "Office buildings/limited partner",
    "Other industrial development",
    "Pollution Control",
    "Private human service provider",
    "Secured airlines",
    "Solid waste/res. recovery",
    "Unsecured airlines"
  ]
}
const cntrystate = {
  "United States": ["Pennsylvania", "Rhode Island"]
}
const statecity = {
  "Pennsylvania": [
    "Youngsville", "Yukon", "Zionville"
  ],
  "Rhode Island": ["Ashaway", "Bradford", "Bristol"]
}
const entityCategories = ["Governmental Entity / Issuer"]

faker.locale = "en_US"
// Generate Entity Address Data

export const generateEntityAddress = (n) => {

  const cntry = "United States"
  const state = helpers.getRandomLookupArray(cntrystate[cntry])
  const city = helpers.getRandomLookupArray(statecity[state])
  const addr1 = faker
    .address
    .streetName()
  const addr2 = faker
    .address
    .streetName()
  const zip1 = faker
    .address
    .zipCode()
  const zip2 = faker
    .address
    .zipCode()

  const formattedAddress = `${addr1} ${addr2} ${city} ${state} ${zip1}-${zip2}`

  const primaryAddress = {
    addressName: "primary",
    isPrimary: true,
    // isHeadQuarter: helpers.getRandomLookupArray(yesNoRef),
    isActive: true,
    website: faker.company.website,
    officePhone: _
      .times(_.random(1, 2))
      .map(() => ({
        countryCode: "+1",
        phoneNumber: `+1 ${faker
          .phone
          .phoneNumberFormat(1)}`,
        extension: ""
      })),
    officeFax: _
      .times(_.random(1, 2))
      .map(() => ({
        faxNumber: `+1 ${faker
          .phone
          .phoneNumberFormat(1)}`
      })),
    officeEmails: _
      .times(_.random(1, 3))
      .map(() => ({
        emailId: faker
          .internet
          .email()
      })),
    addressLine1: addr1,
    formatted_address: formattedAddress,
    addressLine2: addr2,
    country: cntry,
    state,
    city,
    zipCode: {
      zip1,
      zip2
    }
  }

  const nonPrimaryAddresses = _
    .times(1)
    .map(() => {

      const cntry = "United States"
      const state = helpers.getRandomLookupArray(cntrystate[cntry])
      const city = helpers.getRandomLookupArray(statecity[state])
      const addr1 = faker
        .address
        .streetName()
      const addr2 = faker
        .address
        .streetName()
      const zip1 = faker
        .address
        .zipCode()
      const zip2 = faker
        .address
        .zipCode()

      const formattedAddress = `${addr1} ${addr2} ${city} ${state} ${zip1}-${zip2}`
      return {
        addressName: helpers.getRandomLookupArray(entityAddressRef),
        isPrimary: false,
        // isHeadQuarter: helpers.getRandomLookupArray(yesNoRef),
        isActive: true,
        website: faker.company.website,
        officePhone: _
          .times(_.random(1, 2))
          .map(() => ({
            countryCode: "+1",
            phoneNumber: faker
              .phone
              .phoneNumberFormat(),
            extension: ""
          })),
        officeFax: _
          .times(_.random(1, 2))
          .map(() => ({
            faxNumber: faker
              .phone
              .phoneNumberFormat()
          })),
        officeEmails: _
          .times(_.random(1, 3))
          .map(() => ({
            emailId: faker
              .internet
              .email()
          })),
        formatted_address: formattedAddress,
        addressLine1: addr1,
        addressLine2: addr2,
        country: cntry,
        state,
        city,
        zipCode: {
          zip1,
          zip2
        }
      }
    })

  return [
    primaryAddress, ...nonPrimaryAddresses
  ]
}

export const generateEntityAddOns = (n) => (_.times(n).map(() => ({
  serviceName: helpers.getRandomLookupArray(serviceTypeRef),
  serviceEnabled: helpers.getRandomLookupArray(yesNoRef)
})))

export const generateLinkedCusip = (n) => (_.times(n).map(() => ({
  debtType: helpers.getRandomLookup("LKUPDEBTTYPE"),
  associatedCusip6: crypto
    .randomBytes(3)
    .toString("hex")
    .toLocaleUpperCase()
})))

export const generateLinkedBorrowerOrConduitUser = (n) => (_.times(n).map(() => ({
  borOblRel: helpers.getRandomLookupArray(conduitBorrowerRef),
  borOblFirmName: helpers.getRandomLookup("LKUPBORROWER"),
  borOblDebtType: helpers.getRandomLookup("LKUPDEBTTYPE"),
  borOblCusip6: crypto
    .randomBytes(3)
    .toString("hex")
    .toLocaleUpperCase(),
  borOblStartDate: faker
    .date
    .past(5),
  borOblEndDate: faker
    .date
    .past(3)
})))

export const generateEntity = ({entityType, entityName}) => {
  const fname = entityName
  const firmAddOns = generateEntityAddOns(_.random(1, 4)).map(a => a.serviceName)

  let defaultParams = {}

  switch (entityType) {
  case "FA":
    defaultParams = {
      firmAddOns,
      msrbRegistrantType: "Municipal Advisor",
      isMuniVisorClient: true,
      entityLinkedCusips: [],
      entityBorObl: [],
      entityFlags: {
        marketRole: _.sampleSize(marketRoleRefLookup[entityType], Math.ceil(Math.random() * marketRoleRefLookup[entityType].length)),
        issuerFlags: []
      },
      settings: {
        auditFlag : "no"
      }
    }
    break
  case "C":
    defaultParams = {
      firmAddOns: [],
      msrbRegistrantType: "Issuer",
      isMuniVisorClient: false,
      entityFlags: {
        marketRole: [],
        issuerFlags: ["State Issuer"]
      }
    }
    break
  case "P":
    defaultParams = {
      firmAddOns: [],
      msrbRegistrantType: "Issuer",
      isMuniVisorClient: false,
      entityFlags: {
        marketRole: [],
        issuerFlags: ["State Issuer"]
      }
    }
    break

  case "U":
    defaultParams = {
      firmAddOns: [],
      msrbRegistrantType: "Municipal Securities Dealer",
      isMuniVisorClient: false,
      entityLinkedCusips: [],
      entityBorObl: [],
      entityFlags: {
        marketRole: _.sampleSize(marketRoleRefLookup[entityType], Math.ceil(Math.random() * marketRoleRefLookup[entityType].length)),
        issuerFlags: []
      }
    }
    break
  case "BC":
    defaultParams = {
      msrbRegistrantType: "Bond Counsel",
      firmAddOns: [],
      isMuniVisorClient: false,
      entityLinkedCusips: [],
      entityBorObl: [],
      entityFlags: {
        marketRole: _.sampleSize(marketRoleRefLookup[entityType], Math.ceil(Math.random() * marketRoleRefLookup[entityType].length)),
        issuerFlags: []
      }
    }
    break
  case "EA":
    defaultParams = {
      msrbRegistrantType: "Escrow Agent",
      firmAddOns: [],
      isMuniVisorClient: false,
      entityLinkedCusips: [],
      entityBorObl: []
    }
    break
  case "PA":
    defaultParams = {
      msrbRegistrantType: "Paying Agent",
      firmAddOns: [],
      isMuniVisorClient: false,
      entityLinkedCusips: [],
      entityBorObl: [],
      entityFlags: {
        marketRole: _.sampleSize(marketRoleRefLookup.Other, Math.ceil(Math.random() * marketRoleRefLookup.Other.length)),
        issuerFlags: []
      }
    }
    break
  case "Platform":
    defaultParams = {
      msrbRegistrantType: "",
      firmAddOns: [],
      isMuniVisorClient: false,
      entityLinkedCusips: [],
      entityBorObl: [],
      entityFlags: {
        marketRole: _.sampleSize(marketRoleRefLookup.Other, Math.ceil(Math.random() * marketRoleRefLookup.Other.length)),
        issuerFlags: []
      }
    }
    break
  default:
    defaultParams = {
      msrbRegistrantType: entityType,
      firmAddOns: [],
      isMuniVisorClient: false,
      entityLinkedCusips: [],
      entityBorObl: [],
      entityFlags: {
        marketRole: _.sampleSize(marketRoleRefLookup.Other, Math.ceil(Math.random() * marketRoleRefLookup.Other.length)),
        issuerFlags: []
      }

    }
  }

  const primaryRef = helpers.getRandomLookupArray(Object.keys(primarySecondaryRef))
  const secondaryRef = helpers.getRandomLookupArray(primarySecondaryRef[primaryRef])

  // console.log("FIRM ADDONS", firmAddOns)

  const entityTestData = {
    _id: mongoose
      .Types
      .ObjectId(),
    // entityFlags: generateEntityFlags(entityType),
    entityType: helpers.getRandomLookupArray(entityCategories),
    isMuniVisorClient: helpers.getRandomLookupArray(yesNoRef),
    msrbFirmName: fname,
    msrbRegistrantType: helpers.getRandomLookupArray(msrbRegCat),
    msrbId: crypto
      .randomBytes(2)
      .toString("hex")
      .toLocaleUpperCase(),
    entityAliases: _.times(Math.floor(Math.random() * 5)).map(() => faker.lorem.word()),
    firmName: fname,
    taxId:Math.random().toString().slice(2,11),
    // taxId: crypto
    //   .randomBytes(4)
    //   .toString("hex")
    //   .toLocaleUpperCase(),
    businessStructure: helpers.getRandomLookup("LKUPBUSSTRUCT"),
    numEmployees: helpers.getRandomLookupArray(emplCat),
    annualRevenue: helpers.getRandomLookupArray(revRef),
    primarySectors: primaryRef,
    secondarySectors: secondaryRef,
    addresses: generateEntityAddress(_.random(1, 3)),
    firmAddOns,
    // entityLinkedCusips: generateLinkedCusip(_.random(1, 4)),
    // entityBorObl: generateLinkedBorrowerOrConduitUser(_.random(1, 5)),
    entityLinkedCusips: [],
    entityBorObl: [],
    createdAt:new Date(),
    updatedAt:new Date()
  }

  // const EntitySample = new Entity({ ...entityTestData,   ...defaultParams })

  const EntitySample = {
    ...entityTestData,
    ...defaultParams
  }

  return EntitySample
}
