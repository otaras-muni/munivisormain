const {MongoClient} = require("mongodb")
const _ = require("lodash")
const assert = require("assert")
const DROPDOWN = require("./../testdatareference/globaldropdown")
const faker = require("faker")
const uuidv1 = require("uuid/v1")
const {ObjectID} = require("mongodb")

const elasticsearch = require("elasticsearch")

const url = " PLEASE SPECIFIY THE MOGO DB URL HERE "

/* Connect to Mongo DB */
// eslint-disable-next-line no-unused-vars
const mclient = new MongoClient(url)
const dbName = "munivisor_dev"

const uids = _.times(5, () => uuidv1())
const randomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min



const numUids = uids.length

const getRandomUids = () => {
  const randNum = randomInt(1, numUids - 1)
  const randUids = []
  for (let i = randNum; i >= 0; i-=1) {
    randUids.push(uids[i])
  }
  return randUids
}

let iter


const getRandomLookup = lookupKey => {
  const len = DROPDOWN[lookupKey].length
  return DROPDOWN[lookupKey][randomInt(0, len - 1)].value
}

const generateDeal = dealCounter => {
  const randUids = getRandomUids()
  const deal = {}
  const [a] = randUids
  deal.uid = a
  deal.collaborators = randUids.slice(1)
  deal.dealKeyInfo = {}
  deal.dealKeyInfo.dealName = `Deal ${dealCounter}`
  deal.dealKeyInfo.issuerName = getRandomLookup("LKUPISSUER")
  deal.dealKeyInfo.borrowerOrObligorName = getRandomLookup("LKUPBORROWER")
  deal.dealKeyInfo.guarantorName = getRandomLookup("LKUPGUARANTOR")
  deal.dealKeyInfo.transaction = getRandomLookup("LKUPTRANSACTION")
  deal.dealKeyInfo.transactionID = `Txn ${dealCounter}`
  deal.dealKeyInfo.corpType = getRandomLookup("LKUPCORPTYPE")
  deal.dealKeyInfo.transactionType = getRandomLookup("LKUPTRANSACTIONTYPE")
  deal.dealKeyInfo.startDate = faker.date.past(2)
  deal.dealKeyInfo.state = getRandomLookup("LKUPSTATE")
  deal.dealKeyInfo.transactionStatus = getRandomLookup("LKUPTRANSACTIONSTATUS")
  deal.dealKeyInfo.dateHired = faker.date.past(2)
  deal.dealKeyInfo.county = getRandomLookup("LKUPCOUNTY")
  deal.dealKeyInfo.parAmount = Math.ceil(Math.random() * 10000)
  deal.dealKeyInfo.sector = getRandomLookup("LKUPSECTORS")
  deal.dealKeyInfo.expectedAwardDate = faker.date.past(1)
  deal.dealKeyInfo.sDCCreditPercentage = Math.ceil(Math.random() * 100)
  deal.dealKeyInfo.actualAwardDateTime = faker.date.past(1)
  deal.dealKeyInfo.pricingDate = faker.date.past(1)
  deal.dealKeyInfo.estimatedRevenue = Math.ceil(Math.random() * 100000)
  deal.dealKeyInfo.useOfProceeds = getRandomLookup("LKUPUSEOFPROCEED")
  deal.dealKeyInfo.placeholder = faker.lorem.sentence()
  deal.dealComplianceChecks = {}
  deal.dealComplianceChecks.goodFaithAmount = Math.ceil(Math.random() * 100000)
  deal.dealComplianceChecks.accountName = faker.company.companyName()
  deal.dealComplianceChecks.relatedNotes = faker.lorem.sentence()
  deal.dealComplianceChecks.goodFaithDate = faker.date.past(1)
  deal.dealComplianceChecks.accountNumber = faker.finance.account()
  deal.dealComplianceChecks.goodFaithInstructions = faker.lorem.sentence()
  deal.dealComplianceChecks.moneyTransferMethod = getRandomLookup(
    "LKUPMONEYTRANSFERTYPE"
  )
  deal.dealComplianceChecks.aBANumber = Math.ceil(Math.random() * 10000000000)
    .toString()
    .slice(0, 9)
  deal.dealComplianceChecks.placeholder = faker.lorem.sentence()
  deal.dealComplianceChecks.advancedRefunding = getRandomLookup("LKUPYESNO")
  deal.dealComplianceChecks.formG36ARDSubmissionDateTime = faker.date.past(1)
  deal.dealComplianceChecks.fOSReceivedDateTime = faker.date.past(1)
  deal.dealComplianceChecks.g32SubmissionDateTime = faker.date.past(1)

  deal.mAExemptions = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.mAExemptionType = getRandomLookup("LKUPMAEXEMPTION")
    item.mAExemptionDate = faker.date.past(1)
    item.mAExemptionNotes = faker.lorem.sentence()
    deal.mAExemptions.push(item)
  }

  deal.g17Exemptions = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.g17Type = getRandomLookup("LKUPG17TYPE")
    item.g17SentDate = faker.date.past(1)
    item.g17AcknowledgementDate = faker.date.past(1)
    item.g17Notes = faker.lorem.sentence()
    deal.g17Exemptions.push(item)
  }

  deal.miscComplianceApprovals = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.name = faker.lorem.words()
    item.approvalDateTime = faker.date.past(1)
    deal.miscComplianceApprovals.push(item)
  }

  deal.underwritingTeam = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.firmName = getRandomLookup("LKUPUNDERWRITER")
    item.roleInSyndicate = getRandomLookup("LKUPUNDERWRITERROLE")
    item.liabilityPercentage = Math.ceil(Math.random() * 100)
    item.managementFeePercentage = Math.ceil(Math.random() * 100)
    deal.underwritingTeam.push(item)
  }

  deal.dealParticipants = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.participantType = getRandomLookup("LKUPPARTICIPANTTYPE")
    item.firmName = getRandomLookup("LKUPFIRMNAME")
    item.contactName = faker.name.findName()
    item.contactTitle = faker.name.jobTitle()
    item.primaryOfficeAddress1 = faker.address.streetAddress()
    item.primaryOfficeAddress2 = faker.address.secondaryAddress()
    item.primaryOfficeState = getRandomLookup("LKUPSTATE")
    item.phone = faker.phone.phoneNumberFormat()
    item.email = faker.internet.email()
    item.addToDL = getRandomLookup("LKUPYESNO")
    deal.dealParticipants.push(item)
  }

  deal.issuerUnderlyingRatings = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.series = faker.lorem.word()
    item.seriesCode = faker.lorem.word()
    item.longTermRating = getRandomLookup("LKUPRATING")
    item.longTermOutlook = getRandomLookup("LKUPOUTLOOK")
    item.shortTermRating = getRandomLookup("LKUPRATING")
    item.shortTermOutlook = getRandomLookup("LKUPOUTLOOK")
    item.notes = faker.lorem.sentence()
    deal.issuerUnderlyingRatings.push(item)
  }

  deal.insuredEnhancedRatings = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.creditEnhancementProvider = getRandomLookup("LKUPCEP")
    item.creditEnhancementType = getRandomLookup("LKUPCREDITENHANCEMENTTYPE")
    item.longTermEnhancedRating = getRandomLookup("LKUPRATING")
    item.longTermOutlook = getRandomLookup("LKUPOUTLOOK")
    item.shortTermEnhancedRating = getRandomLookup("LKUPRATING")
    item.shortTermOutlook = getRandomLookup("LKUPOUTLOOK")
    item.notes = faker.lorem.sentence()
    deal.insuredEnhancedRatings.push(item)
  }

  deal.dealKeyParameters = {}
  deal.dealKeyParameters.seriesCode = faker.lorem.word()
  deal.dealKeyParameters.parAmount = Math.ceil(Math.random() * 100000)
  deal.dealKeyParameters.securityType = getRandomLookup("LKUPSECTYPEGENERAL")
  deal.dealKeyParameters.security = faker.lorem.word()
  deal.dealKeyParameters.securityDetails = faker.lorem.sentence()
  deal.dealKeyParameters.principalFrequency = getRandomLookup(
    "LKUPCOUPONFREQUENCY"
  )
  deal.dealKeyParameters.firstMaturityDate = faker.date.past(1)
  deal.dealKeyParameters.finalMaturityDate = faker.date.past(1)
  deal.dealKeyParameters.datedDate = faker.date.past(1)
  deal.dealKeyParameters.settlementDate = faker.date.past(1)
  deal.dealKeyParameters.firstCouponDate = faker.date.past(1)
  deal.dealKeyParameters.putDateMandatoryTender = faker.date.past(1)
  deal.dealKeyParameters.callFeature = getRandomLookup("LKUPCALLFEATURE")
  deal.dealKeyParameters.callDate = faker.date.past(1)
  deal.dealKeyParameters.callPrice = Math.ceil(Math.random() * 1000)
  deal.dealKeyParameters.recordDate = faker.date.past(1)
  deal.dealKeyParameters.federalTax = getRandomLookup("LKUPFEDTAX")
  deal.dealKeyParameters.stateTax = getRandomLookup("LKUPSTATETAX")
  deal.dealKeyParameters.AMT = getRandomLookup("LKUPYESNO")
  deal.dealKeyParameters.bankQualified = getRandomLookup("LKUPYESNO")
  deal.dealKeyParameters.accrueFrom = getRandomLookup("LKUPACCRUEFROM")
  deal.dealKeyParameters.form = getRandomLookup("LKUPFORM")
  deal.dealKeyParameters.couponFrequency = getRandomLookup(
    "LKUPCOUPONFREQUENCY"
  )
  deal.dealKeyParameters.dayCount = getRandomLookup("LKUPDAYCOUNT")
  deal.dealKeyParameters.rateType = getRandomLookup("LKUPRATETYPE")
  deal.dealKeyParameters.firmInventory = faker.lorem.word()
  deal.dealKeyParameters.minimumDenomination = Math.ceil(Math.random() * 100)
  deal.dealKeyParameters.grossSpreadUSDPer1000 = Math.ceil(Math.random() * 10)
  deal.dealKeyParameters.estAvgTakedownUSDPer1000 = Math.ceil(
    Math.random() * 10
  )
  deal.dealKeyParameters.insuranceFee = Math.ceil(Math.random() * 10000)
  deal.dealKeyParameters.pricingStatus = getRandomLookup("LKUPPRICINGSTATUS")
  deal.dealKeyParameters.keyNotes = faker.lorem.sentence()
  deal.dealKeyParameters.otherRelatedNotes = faker.lorem.sentence()

  deal.pricingData = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.term = Math.ceil(Math.random() * 10)
    item.maturityDate = faker.date.future(5)
    item.averageLife = Math.ceil(Math.random() * 10)
    item.amount = Math.ceil(Math.random() * 1000)
    item.coupon = Math.ceil(Math.random() * 10)
    item.yield = Math.ceil(Math.random() * 10)
    item.price = Math.ceil(Math.random() * 1000)
    item.YTM = Math.ceil(Math.random() * 10)
    item.cusip = faker.finance.account
    deal.pricingData.push(item)
  }

  deal.dealDocuments = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.documentType = getRandomLookup("LKUPDOCTYPE")
    item.accessEntitlement = getRandomLookup("LKUPDOCSHARESTATUS")
    item.documentStatus = getRandomLookup("LKUPDOCSTATUS")
    item.docId = new ObjectID()
    item.fileName = faker.system.fileName()
    item.action = getRandomLookup("LKUPDOCACTION")
    item.tag = faker.lorem.word()
    deal.dealDocuments.push(item)
  }

  deal.costsOfIssuance = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.standardCost = getRandomLookup("LKUPCOSTSOFISSUANCE")
    item.amount = Math.ceil(Math.random() * 1000)
    item.notes = faker.lorem.sentence()
    deal.costsOfIssuance.push(item)
  }

  deal.additionalCostOfIssuance = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.costName = getRandomLookup("LKUPCOSTSOFISSUANCE")
    item.amount = Math.ceil(Math.random() * 1000)
    item.notes = faker.lorem.sentence()
    deal.additionalCostOfIssuance.push(item)
  }

  deal.scheduleofEvents = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.taskOrEventName = getRandomLookup("LKUPSCHEDULEOFEVENTS")
    item.applicableToTransaction = getRandomLookup("LKUPYESNO")
    item.assignedTo = getRandomLookup("LKUPPARTICIPANTTYPE")
    item.dueDate = faker.date.future(1)
    item.status = getRandomLookup("LKUPTRANSACTIONSTATUS")
    deal.scheduleofEvents.push(item)
  }

  deal.additionalScheduleOfEvents = []
  iter = randomInt(1, 4)
  for (let i = 0; i < iter; i+=1) {
    const item = {}
    item.taskOrEventName = faker.lorem.words()
    item.assignedTo = getRandomLookup("LKUPPARTICIPANTTYPE")
    item.dueDate = faker.date.future(1)
    item.status = getRandomLookup("LKUPTRANSACTIONSTATUS")
    item.notes = faker.lorem.sentence()
    deal.additionalScheduleOfEvents.push(item)
  }
  return deal
}

const generateDeals = numDeals => {
  const deals = []
  for (let i = 1; i <= numDeals; i+=1) {
    deals.push(generateDeal(i))
  }
  return deals
}

/* Insert into Mongo DB all the details of the deal */
const deals = generateDeals(500)

/* Construct the String Query for Elastic Search */

const initialString = "{\"index\":{}}"
const finalElasticSearchLoadingObject = deals
  .map(elem => JSON.stringify(elem))
  .join(`\n${initialString}\n`)

const elasticLoadFinal = `${initialString}\n${finalElasticSearchLoadingObject}`
/* Insert Data into Mongo DB */

MongoClient.connect(url, (err, mongoclient) => {
  assert.equal(null, err)
  console.log("Connected correctly to server")
  const db = mongoclient.db(dbName)
  db.collection("deals_dev").deleteMany({}, (err) => {
    assert.equal(null, err)
    console.log("deleted all the documents from Mongo")
    mongoclient.close()

    MongoClient.connect(url, (err, mongoclient) => {
      assert.equal(null, err)
      console.log("Connected correctly to server")
      const db = mongoclient.db(dbName)
      db.collection("deals_dev").insertMany(deals, (err) => {
        assert.equal(null, err)
        console.log("inserted all the documents into Mongo")
        mongoclient.close()
      })
    })
  })
})

const elasticclient = new elasticsearch.Client({
  // host: 'localhost:9200' // Note that fa is the index and deals is the document
  // host:'https://search-munivisor-6sxxstd3jxvu4e4udr6lghn5ue.us-east-1.es.amazonaws.com'
  host:"PROVIDE ELASTIC SEARCH URL"
})

const elasticClientBulk = new elasticsearch.Client({
  // host: 'localhost:9200/fa/deals' // Note that fa is the index and deals is the document
// host:'https://search-munivisor-6sxxstd3jxvu4e4udr6lghn5ue.us-east-1.es.amazonaws.com/fa/deals'
  host:"PROVIDE ELASTIC SEARCH URL"

})

/* Delete Information from Elastic Search */
elasticclient.deleteByQuery(
  {
    index: "fa",
    type: "deals",
    body: {
      query: {
        match_all: {}
      }
    }
  },
  (error, ) => {
    assert.equal(null, error)
    console.log("All data was deleted from the Elastic Index")
    elasticclient.close()

    elasticClientBulk.bulk(
      {
        body: [elasticLoadFinal]
      },
      (err) => {
        assert.equal(null, err)
        console.log("Bulk Inserted all data into Elastic Index")
        elasticClientBulk.close()
      }
    )
  }
)

// Bulk insert into Elastic Search
