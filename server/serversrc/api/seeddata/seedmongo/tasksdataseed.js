const mongoose = require("mongoose")
const faker = require("faker")
const _ = require("lodash")

const TaskModels = require("./../testschema/Task")
const helpers = require("./../testdatareference/testhelpers")

mongoose.Promise = global.Promise
mongoose.connect("PROVIDE MONGO DB URL")

/* Destructure the objects from the exported variables */
const { Task, UserRevised, Tenant, Deal } = TaskModels

// Clean Up the Database
const cleanAllCollections = async () => {
  try {
    await Promise.all([
      Task.remove(),
      Tenant.remove(),
      UserRevised.remove(),
      Deal.remove()
    ])

    return "All collections have been removed"
  } catch (err) {
    console.log("there is an error", err)
    throw Error(err)
  }
}

// Create Functions to generate Random Data
const createTenant = () => new Tenant({
  name: faker.company.companyName(),
  type: helpers.getRandomLookupArray([
    "Financial Advisors",
    "Issuers",
    "Underwriting Services",
    "Law Firms"
  ])
})

const createUser = allTenants => new UserRevised({
  name: faker.name.firstName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
  tenant: helpers.getRandomLookupArray(allTenants)
})

const createDeal = (allTenants, allUsers) => new Deal({
  dealName: `deal${faker.random.uuid()}`,
  createdByTenantUser: helpers.getRandomLookupArray(allUsers)
})

const createTask = (allUsers, allDeals) => {
  const task = {}
  task.title = faker.lorem.words()
  task.descirption = faker.lorem.words()
  task.user = helpers.getRandomLookupArray(allUsers)
  task.deal = helpers.getRandomLookupArray(allDeals)
  task.dueDate = Date.now()
  task.status = helpers.getRandomLookupArray(["Open", "Pending", "Closed"])
  task.subTasks = []
  const iter = helpers.randomInt(1, 10)
  for (let i = 0; i < iter; i+=1) {
    const subTask = {}
    subTask.title = faker.lorem.words()
    subTask.descirption = faker.lorem.words()
    subTask.user = helpers.getRandomLookupArray(allUsers)
    subTask.deal = helpers.getRandomLookupArray(allDeals)
    subTask.dueDate = faker.date.past(1)
    subTask.status = helpers.getRandomLookupArray([
      "Open",
      "Pending",
      "Closed"
    ])
    task.subTasks.push(subTask)
  }
  return new Task(task)
}

// Save Information in Database
const createTenantsInDb = async Tenant => {
  try {
    const savedTenant = await Tenant.save()
    return savedTenant
  } catch (err) {
    console.log("there is an error in saving tenant", err)
    throw Error(err)
  }
}

const createUsersInDb = async User => {
  try {
    const savedUser = await User.save()
    return savedUser
  } catch (err) {
    console.log("there is an error in saving user", err)
    throw Error(err)
  }
}

const createDealsInDb = async Deal => {
  try {
    const savedDeal = await Deal.save()
    return savedDeal
  } catch (err) {
    console.log("there is an error in saving deal", err)
    throw Error(err)
  }
}

const createTasksInDb = async Task => {
  try {
    const savedTask = await Task.save()
    return savedTask
  } catch (err) {
    console.log("there is an error in saving Task", err)
    throw Error(err)
  }
}

const executeTestDataCreation = async () => {
  try {

    // Clean the Database
    const dbClean = await cleanAllCollections()
    console.log("Database Successfully Clean", dbClean)

    // Create Tenants on the Platform
    const allTenants = _.times(25, () => createTenant())

    // Iteratively Insert data into the database
    await allTenants.map(tenant => {
      createTenantsInDb(tenant).then(mongotenant => mongotenant)
      return null
    })

    console.log("Tenants inserted successfully")

    // Create Users in the database
    const allUsers = _.times(200, () => createUser(allTenants))
    // Iteratively Insert all the Users into the Database
    await allUsers.map(user => {
      createUsersInDb(user).then(mongouser => mongouser)
      return null
    })

    console.log("Users inserted successfully")

    // Create TransactionDeals in the database
    const allDeals = _.times(400, () => createDeal(allTenants, allUsers))
    // Iteratively Insert all the TransactionDeals into the Database
    await allDeals.map(deal => {
      createDealsInDb(deal).then(mongodeal => mongodeal)
      return null
    })

    console.log("TransactionDeals inserted successfully")

    // Create TransactionDeals in the database
    const allTasks = _.times(2000, () => createTask(allUsers, allDeals))
    // Iteratively Insert all the TransactionDeals into the Database
    await allTasks.map(task => {
      createTasksInDb(task).then(mongotask => mongotask)
      return null
    })

    console.log("Tasks inserted successfully")

    return "Successfully completed all tasks"
  } catch (e) {
    throw Error("Something went wrong in this process")
  }
}

executeTestDataCreation()
  .then(n => console.log(n))
  .catch(err => console.log(err))
