import {ObjectID} from "mongodb"
import { Docs, Deals, BankLoans, ActBusDev, Others, RFP, Derivatives, ActMaRFP,EntityRel, Tasks,EntityUser } from "./../appresources/models" 
import { putBulkDataRevised} from "./esHelper"

export const dataNormalizerHelper = ({type, docId, versionDetails,alluserdetails}) => {


  // console.log("All the users for all the tenants", JSON.stringify(alluserdetails,null,4))


  const tranFieldNames = {
    users:[
      "tranId",
      "firmEntityId",
      "clientEntityId",
      "tranEntities",
      "tranAssigneeDetails",
      "activityDescription",
      "tranParAmount",
      "tranExpectedAwardDate",
      "tranActualAwardDate",
      "tranPricingDate",
      "tranPrimarySector",
      "tranSecondarySector",
      "tranStatus",
      "tranClientName",
      "tranFirmName",
      "tranType",
      "tranSubType",
      "docDetails",
      "tranUsersAll",
      "tranEntitiesAll",
      "historicalData",
      "allIds",
      "createdAt",
      "updatedAt"
    ],
    entities:[
      "tranId",
      "firmEntityId",
      "clientEntityId",
      "tranUsers",
      "tranAssigneeDetails",
      "userDetails",
      "activityDescription",
      "tranParAmount",
      "tranExpectedAwardDate",
      "tranActualAwardDate",
      "tranPricingDate",
      "tranPrimarySector",
      "tranSecondarySector",
      "tranStatus",
      "tranClientName",
      "tranFirmName",
      "tranType",
      "tranSubType",
      "docDetails",
      "tranUsersAll",
      "tranEntitiesAll",
      "historicalData",
      "allIds",
      "createdAt",
      "updatedAt"
    ]
  }

  const tranFieldNamesForGroupByUsers = tranFieldNames.users.reduce ( (acc, k) => ({"group":{ ...acc.group, [k]:`$${k}`},"select":{...acc.select,[k]:1},"groupSelect":{...acc.groupSelect,[k]:`$_id.${k}`}}), {})
  const tranFieldNamesForGroupByEntities = tranFieldNames.entities.reduce ( (acc, k) => ({"group":{ ...acc.group, [k]:`$${k}`},"select":{...acc.select,[k]:1},"groupSelect":{...acc.groupSelect,[k]:`$_id.${k}`}}), {})

  const docPropertyMappoer = {
    deals:"$dealIssueDocuments",
    rfps:"$rfpBidDocuments",
    bankloans:"$bankLoanDocuments",
    derivatives:"$derivativeDocuments",
    marfps:"$maRfpDocuments",
    others:"$actTranDocuments",
    busdevs:"$actTranDocuments"
  }
  

  const docDetailsMapperAll = {
    $setUnion:[
      {$map: {
        input: docPropertyMappoer[type],
        as: "documents",
        in: {
          docId:"$$documents._id",
          fileName:"$$documents.docFileName",
          docCategory:"$$documents.docCategory",
          docSubCategory:"$$documents.docSubCategory",
          docType:"$$documents.docType",
          docAWSReference:"$$documents.docAWSFileLocation",
          uploadedBy:"$$documents.createdUserName",
          docUploadUserId:"$$documents.createdBy",
          uploadDate:"$$documents.LastUpdatedDate"
        }
      },
      }]
  }
 

  const docDetailsMapperSpecificDoc = {
    $filter: {
      input:{
        $map: {
          input: docPropertyMappoer[type],
          as: "documents",
          in: {
            docId:"$$documents._id",
            fileName:"$$documents.docFileName",
            docCategory:"$$documents.docCategory",
            docSubCategory:"$$documents.docSubCategory",
            docType:"$$documents.docType",
            docAWSReference:"$$documents.docAWSFileLocation",
            uploadedBy:"$$documents.createdUserName",
            docUploadUserId:"$$documents.createdBy",
            uploadDate:"$$documents.LastUpdatedDate",
            versionDetails:[...versionDetails || [] ]
          }
        },
      },
      as: "documents",
      cond:{$eq:["$$documents.docAWSReference",(docId || "").toString()]}
    }
  }
  const getDocMapperInfo = docId ? docDetailsMapperSpecificDoc : docDetailsMapperAll

  const commonGroupByPipeline = [
    {
      $addFields:{
        allIds:{
          $setUnion:["$tranUsers","$tranEntities"]
        },
        tranUsersAll:"$tranUsers",
        tranEntitiesAll:"$tranEntities"        
      }
    },
    // get the entity details
    {
      $unwind: {
        "path": "$tranUsers",
        "preserveNullAndEmptyArrays": true
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "tranUsers",
        foreignField: "_id", 
        as: "userDetails"
      }
    },
    {
      $unwind: {
        "path": "$userDetails",
        "preserveNullAndEmptyArrays": true
      }
    },
    {
      $project: {
        // tranId:1,
        // tranClientName:1,
        // tranEntities:1,
        ...tranFieldNamesForGroupByUsers.select,
        userId:"$userDetails._id",
        userName:"$userDetails.userFirstName",
        userLastName:"$userDetails.userLastName",
        userFlags:"$userDetails.userFlags",
        userAddresses:"$userDetails.userAddresses"
      
      }
    },
    {
      $group:{
        // _id:{tranId:"$tranId", tranClientName:"$tranClientName", tranEntities:"$tranEntities"},
        _id:{...tranFieldNamesForGroupByUsers.group},
        userDetails:{
          $push:{
            userId:"$userId",
            userName:"$userName",
            userLastName:"$userLastName",
            userFlags:"$userFlags",
            userAddresses:"$userAddresses"
          }
        }
      }
    },
    {
      $project:{
        _id:0,
        ...tranFieldNamesForGroupByUsers.groupSelect,
        userDetails:1
      }
    },
    // get the entityDetails details

    {
      $unwind: {
        "path": "$tranEntities",
        "preserveNullAndEmptyArrays": true
      }
    },
    {
      $lookup:{
        from: "entities",
        localField: "tranEntities",
        foreignField: "_id", 
        as: "entityDetails"
      }
    },
    {
      $unwind: {
        "path": "$entityDetails",
        "preserveNullAndEmptyArrays": true
      }
    },
    {
      $project: {
        ...tranFieldNamesForGroupByEntities.select,
        entityId:"$entityDetails._id",
        entityName:"$entityDetails.firmName",
        entityAliases:"$entityDetails.entityAliases",
        entityType:"$entityDetails.entityType",
        entityFlags:"$entityDetails.entityFlags"
      }
    },
    {
      $group:{
        _id:{...tranFieldNamesForGroupByEntities.group },
        entityDetails:{
          $push:{
            entityId:"$entityId",
            entityName:"$entityName",
            entityAliases:"$entityAliases",
            entityType:"$entityType",
            entityFlags:"$entityFlags"
          }
        }
      }
    },
    {
      $project:{
        _id:"$_id.tranId",
        ...tranFieldNamesForGroupByEntities.groupSelect,
        entityDetails:1
      }
    }
  ]

  const pipelineDetails = {
    "deals":[
      {
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$dealIssueTranClientId",
          clientEntityId: "$dealIssueTranIssuerId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$dealIssueParticipants.dealPartContactId", []] } } ] },
                    then: { $ifNull: ["$dealIssueParticipants.dealPartContactId", []] },
                    else: { $ifNull: [["$dealIssueParticipants.dealPartContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$dealIssueTranAssignedTo",[]]} } ] }, 
                  then: { $ifNull: ["$dealIssueTranAssignedTo", []] },
                  else: { $ifNull: [["$dealIssueTranAssignedTo"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$dealIssueTranClientId" , []] } } ] },
                    then: { $ifNull: ["$dealIssueTranClientId" , []] },
                    else: { $ifNull: [["$dealIssueTranClientId" ], { $ifNull: ["$dealIssueTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$dealIssueTranIssuerId" , []] } } ] },
                    then: { $ifNull: ["$dealIssueTranIssuerId" , []] },
                    else: { $ifNull: [["$dealIssueTranIssuerId"], { $ifNull: ["$dealIssueTranIssuerId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$dealIssueParticipants.dealPartFirmId", []] } } ] },
                    then: { $ifNull: ["$dealIssueParticipants.dealPartFirmId", []] },
                    else: { $ifNull: [["$dealIssueParticipants.dealPartFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$dealIssueUnderwriters.dealPartFirmId", []] } } ] },
                    then: { $ifNull: ["$dealIssueUnderwriters.dealPartFirmId", []] },
                    else: { $ifNull: [["$dealIssueUnderwriters.dealPartFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",{$ifNull:["$dealIssueTranAssignedTo",[]]}]}
              }},0]
          },

          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$dealIssueTranIssueName",""]},{"$eq":["$dealIssueTranIssueName",null]}]},
              then:"$dealIssueTranProjectDescription",
              else:"$dealIssueTranIssueName"},
          },
          tranParAmount:"$dealIssueParAmount", 
          tranExpectedAwardDate:"$dealIssueExpAwardDate"    ,
          tranActualAwardDate:"$dealIssueActAwardDate",
          tranPricingDate:"$dealIssuePricingDate",
          tranPrimarySector:"$dealIssueTranPrimarySector",
          tranSecondarySector:"$dealIssueTranSecondarySector",
          tranStatus:"$dealIssueTranStatus",
          tranClientName:"$dealIssueTranIssuerFirmName",
          tranFirmName:"$dealIssueTranClientFirmName",
          tranType:"$dealIssueTranType",
          tranSubType:"$dealIssueTranSubType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },
        }
      },
      ...commonGroupByPipeline
    ],
    "rfps":[
      { 
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$rfpTranClientId",
          clientEntityId: "$rfpTranIssuerId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpProcessContacts.rfpProcessContactId", []] } } ] },
                    then: { $ifNull: ["$rfpProcessContacts.rfpProcessContactId", []] },
                    else: { $ifNull: [["$rfpProcessContacts.rfpProcessContactId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpEvaluationTeam.rfpSelEvalContactId", []] } } ] },
                    then: { $ifNull: ["$rfpEvaluationTeam.rfpSelEvalContactId", []] },
                    else: { $ifNull: [["$rfpEvaluationTeam.rfpSelEvalContactId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpParticipants.rfpParticipantContactId", []] } } ] },
                    then: { $ifNull: ["$rfpParticipants.rfpParticipantContactId", []] },
                    else: { $ifNull: [["$rfpParticipants.rfpParticipantContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$rfpTranAssignedTo",[]]} } ] }, 
                  then: { $ifNull: ["$rfpTranAssignedTo", []] },
                  else: { $ifNull: [["$rfpTranAssignedTo"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpTranIssuerId" , []] } } ] },
                    then: { $ifNull: ["$rfpTranIssuerId" , []] },
                    else: { $ifNull: [["$rfpTranIssuerId" ], { $ifNull: ["$rfpTranIssuerId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpTranClientId" , []] } } ] },
                    then: { $ifNull: ["$rfpTranClientId" , []] },
                    else: { $ifNull: [["$rfpTranClientId"], { $ifNull: ["$rfpTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpProcessContacts.rfpProcessFirmId", []] } } ] },
                    then: { $ifNull: ["$rfpProcessContacts.rfpProcessFirmId", []] },
                    else: { $ifNull: [["$rfpProcessContacts.rfpProcessFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpEvaluationTeam.rfpSelEvalFirmId", []] } } ] },
                    then: { $ifNull: ["$rfpEvaluationTeam.rfpSelEvalFirmId", []] },
                    else: { $ifNull: [["$rfpEvaluationTeam.rfpSelEvalFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$rfpParticipants.rfpParticipantFirmId", []] } } ] },
                    then: { $ifNull: ["$rfpParticipants.rfpParticipantFirmId", []] },
                    else: { $ifNull: [["$rfpParticipants.rfpParticipantFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId","$rfpTranAssignedTo"]}
              }},0]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$rfpTranIssueName",""]},{"$eq":["$rfpTranIssueName",null]}]},
              then:"$rfpTranProjectDescription",
              else:"$rfpTranIssueName"},
          },
          tranParAmount:"$tranParAmount",
          tranExpectedAwardDate:"$tranExpectedAwardDate",
          tranActualAwardDate:"$tranExpectedAwardDate",
          tranPricingDate:"$tranPricingDate",
          tranPrimarySector:"$rfpTranPrimarySector",
          tranSecondarySector:"$rfpTranSecondarySector",
          tranStatus:{$ifNull:["$rfpTranStatus","-"]},
          tranClientName:"$rfpTranIssuerFirmName",
          tranFirmName:"$rfpTranClientFirmName",
          tranType:"$rfpTranType",
          tranSubType:"$rfpTranSubType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },

        }
      },
      ...commonGroupByPipeline
    ],
    "bankloans":[
      {
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$actTranFirmId",
          clientEntityId: "$actTranClientId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$bankLoanParticipants.partContactId", []] } } ] },
                    then: { $ifNull: ["$bankLoanParticipants.partContactId", []] },
                    else: { $ifNull: [["$bankLoanParticipants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$actTranFirmLeadAdvisorId",[]]} } ] }, 
                  then: { $ifNull: ["$actTranFirmLeadAdvisorId", []] },
                  else: { $ifNull: [["$actTranFirmLeadAdvisorId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$actTranFirmId" , []] },
                    else: { $ifNull: [["$actTranFirmId" ], { $ifNull: ["$actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranClientId" , []] } } ] },
                    then: { $ifNull: ["$actTranClientId" , []] },
                    else: { $ifNull: [["$actTranClientId"], { $ifNull: ["$actTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$bankLoanParticipants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$bankLoanParticipants.partFirmId", []] },
                    else: { $ifNull: [["$bankLoanParticipants.partFirmId"], []] }
                  }
                  }
                  , []] 
              }
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$actTranFirmLeadAdvisorId"]]}
              }},0]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
              then:"$actTranProjectDescription",
              else:"$actTranIssueName"},
          },
          tranParAmount:"$bankLoanTerms.parAmount",
          tranExpectedAwardDate:"$bankLoanSummary.actTranClosingDate",
          tranActualAwardDate:"$bankLoanSummary.actTranClosingDate",
          tranPricingDate:"$bankLoanSummary.actTranClosingDate",
          tranPrimarySector:"$actTranPrimarySector",
          tranSecondarySector:"$actTranSecondarySector",
          tranStatus:"$actTranStatus",
          tranClientName:"$actTranClientName",
          tranFirmName:"$actTranFirmName",
          tranType:"$actTranType",
          tranSubType:"$actTranSubType",
          relationshipType:"$relationshipType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },
        }
      },
      ...commonGroupByPipeline
    ],
    "derivatives":[
      {
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$actTranFirmId",
          clientEntityId: "$actTranClientId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$derivativeParticipants.partContactId", []] } } ] },
                    then: { $ifNull: ["$derivativeParticipants.partContactId", []] },
                    else: { $ifNull: [["$derivativeParticipants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$actTranFirmLeadAdvisorId",[]]} } ] }, 
                  then: { $ifNull: ["$actTranFirmLeadAdvisorId", []] },
                  else: { $ifNull: [["$actTranFirmLeadAdvisorId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$actTranFirmId" , []] },
                    else: { $ifNull: [["$actTranFirmId" ], { $ifNull: ["$actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranClientId" , []] } } ] },
                    then: { $ifNull: ["$actTranClientId" , []] },
                    else: { $ifNull: [["$actTranClientId"], { $ifNull: ["$actTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$derivativeParticipants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$derivativeParticipants.partFirmId", []] },
                    else: { $ifNull: [["$derivativeParticipants.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$actTranFirmLeadAdvisorId"]]}
              }},0]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
              then:"$actTranProjectDescription",
              else:"$actTranIssueName"},
          },
          tranParAmount:"$derivativeSummary.tranNotionalAmt",
          tranExpectedAwardDate:"$derivativeSummary.tranEffDate",
          tranActualAwardDate:"$derivativeSummary.tranEndDate",
          tranPricingDate:"$derivativeSummary.tranTradeDate",
          tranPrimarySector:"$actTranPrimarySector",
          tranSecondarySector:"$actTranSecondarySector",
          tranStatus:{$ifNull:["$actTranStatus","-"]},
          tranClientName:"$actTranClientName",
          tranFirmName:"$actTranFirmName",
          tranType:"$actTranType",
          tranSubType:"$actTranSubType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },

        }
      },
      ...commonGroupByPipeline
    ],
    "marfps":[
      {
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$actTranFirmId",
          clientEntityId: "$actIssuerClient",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$maRfpParticipants.partContactId", []] } } ] },
                    then: { $ifNull: ["$maRfpParticipants.partContactId", []] },
                    else: { $ifNull: [["$maRfpParticipants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$actLeadFinAdvClientEntityId",[]]} } ] }, 
                  then: { $ifNull: ["$actLeadFinAdvClientEntityId", []] },
                  else: { $ifNull: [["$actLeadFinAdvClientEntityId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$actTranFirmId" , []] },
                    else: { $ifNull: [["$actTranFirmId" ], { $ifNull: ["$actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actIssuerClient" , []] } } ] },
                    then: { $ifNull: ["$actIssuerClient" , []] },
                    else: { $ifNull: [["$actIssuerClient"], { $ifNull: ["$actIssuerClient" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$maRfpParticipants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$maRfpParticipants.partFirmId", []] },
                    else: { $ifNull: [["$maRfpParticipants.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$actLeadFinAdvClientEntityId"]]}
              }},0]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actIssueName",null]}]},
              then:"$actProjectName",
              else:"$actIssueName"},
          },
          tranParAmount:"$maRfpSummary.actParAmount",
          tranExpectedAwardDate:"$maRfpSummary.actExpAwaredDate",
          tranActualAwardDate:"$maRfpSummary.actActAwardDate",
          tranPricingDate:"$maRfpSummary.actPricingDate",
          tranPrimarySector:"$actPrimarySector",
          tranSecondarySector:"$actSecondarySector",
          tranStatus:{$ifNull:["$actStatus","-"]},
          tranClientName:"$actIssuerClientEntityName",
          tranFirmName:"$actTranFirmName",
          tranType:"$actType",
          tranSubType:"$actSubType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },
        }
      },
      ...commonGroupByPipeline
    ],
    "others":[
      {
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$actTranFirmId",
          clientEntityId: "$actTranClientId",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$participants.partContactId", []] } } ] },
                    then: { $ifNull: ["$participants.partContactId", []] },
                    else: { $ifNull: [["$participants.partContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$actTranFirmLeadAdvisorId",[]]} } ] }, 
                  then: { $ifNull: ["$actTranFirmLeadAdvisorId", []] },
                  else: { $ifNull: [["$actTranFirmLeadAdvisorId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$actTranFirmId" , []] },
                    else: { $ifNull: [["$actTranFirmId" ], { $ifNull: ["$actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranClientId" , []] } } ] },
                    then: { $ifNull: ["$actTranClientId" , []] },
                    else: { $ifNull: [["$actTranClientId"], { $ifNull: ["$actTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$participants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$participants.partFirmId", []] },
                    else: { $ifNull: [["$participants.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranUnderwriters.partFirmId", []] } } ] },
                    then: { $ifNull: ["$actTranUnderwriters.partFirmId", []] },
                    else: { $ifNull: [["$actTranUnderwriters.partFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$actTranFirmLeadAdvisorId"]]}
              }},0]
          },

          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$actTranIssueName",""]},{"$eq":["$actTranIssueName",null]}]},
              then:"$actTranProjectDescription",
              else:"$actTranIssueName"},
          },
          tranParAmount:"$actTranParAmount",
          tranExpectedAwardDate:"$dealIssueExpAwardDate",
          tranActualAwardDate:"$actTranExpAwardDate",
          tranPricingDate:"$actTranPricingDate",
          tranPrimarySector:"$actTranPrimarySector",
          tranSecondarySector:"$actTranSecondarySector",
          tranStatus:{$ifNull:["$actTranStatus","-"]},
          tranClientName:"$actTranClientName",
          tranFirmName:"$actTranFirmName",
          tranType:"$actTranType",
          tranSubType:"$actTranSubType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },

        }  
      },
      ...commonGroupByPipeline
  
    ],
    "busdevs":[
      {
        $project:{
          _id:"$_id",
          tranId:"$_id",
          firmEntityId:"$actTranFirmId",
          clientEntityId: "$actIssuerClient",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$participants.rfpParticipantContactId", []] } } ] },
                    then: { $ifNull: ["$participants.rfpParticipantContactId", []] },
                    else: { $ifNull: [["$participants.rfpParticipantContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$actLeadFinAdvClientEntityId",[]]} } ] }, 
                  then: { $ifNull: ["$actLeadFinAdvClientEntityId", []] },
                  else: { $ifNull: [["$actLeadFinAdvClientEntityId"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$createdByUser", []] } } ] },
                    then: { $ifNull: ["$createdByUser", []] },
                    else: { $ifNull: [["$createdByUser"], { $ifNull: ["$createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actTranFirmId" , []] } } ] },
                    then: { $ifNull: ["$actTranFirmId" , []] },
                    else: { $ifNull: [["$actTranFirmId" ], { $ifNull: ["$actTranFirmId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$actIssuerClient" , []] } } ] },
                    then: { $ifNull: ["$actIssuerClient" , []] },
                    else: { $ifNull: [["$actIssuerClient"], { $ifNull: ["$actIssuerClient" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$participants.partFirmId", []] } } ] },
                    then: { $ifNull: ["$participants.partFirmId", []] },
                    else: { $ifNull: [["$participants.partFirmId"], []] }
                  }
                  }
                  , []] 
              }
            ]
          },
          tranAssigneeDetails:{
            $arrayElemAt:[
              {$filter:{
                input:alluserdetails,
                as:"alltendetails",
                cond:{$in:["$$alltendetails.userId",["$actLeadFinAdvClientEntityId"]]}
              }},0]
          },
          activityDescription : {
            $cond:{if:{$or:[{"$eq":["$actIssueName",""]},{"$eq":["$actIssueName",null]}]},
              then:"$actProjectName",
              else:"$actIssueName"},
          },
          tranParAmount:"$tranParAmount",
          tranExpectedAwardDate:"$tranExpectedAwardDate",
          tranActualAwardDate:"$tranActualAwardDate",
          tranPricingDate:"$tranPricingDate",
          tranPrimarySector:"$actPrimarySector",
          tranSecondarySector:"$actSecondarySector",
          tranStatus:{$ifNull:["$actStatus","-"]},
          tranClientName:"$actIssuerClientEntityName",
          tranFirmName:"$actLeadFinAdvClientEntityName",
          tranType:"$actType",
          tranSubType:"$actSubType",
          createdAt:"$createdAt",
          updatedAt:"$updatedAt",
          historicalData:"$historicalData",
          docDetails: { 
            ...getDocMapperInfo
          },
        }  
      },
      ...commonGroupByPipeline
    
    ]
  }

  // console.log("The Pipeline return parameters", JSON.stringify(pipelineDetails[type],null,4))
  return pipelineDetails[type]
}

const getEntityInformationForNormalization = async (docId,ids,versionDetails) => {

  const entityRelatedColumns = [
    "tenantId",
    "relationshipToTenant",
    "entityId",
    "entityName",
    "entityAliases",
    "entityType",
    "entityFlags",
    "entityAddresses",
    "entityLinkedCusips",
    "entityDocuments",
    "createdAt",
    "updatedAt",
    "docDetails",
    "historicalData",
    "entityPrimaryAddress"
  ]
  const entityRelatedColumnsGroupByInfo = entityRelatedColumns.reduce ( (acc, k) => ({"group":{ ...acc.group, [k]:`$${k}`},"select":{...acc.select,[k]:1},"groupSelect":{...acc.groupSelect,[k]:`$_id.${k}`}}), {})

  const docDetailsMapperAll = {
    $setUnion:[
      {$map: {
        input: "$entityRelatedInformation.entityDocuments",
        as: "documents",
        in: {
          docId:"$$documents._id",
          fileName:"$$documents.docFileName",
          docCategory:"$$documents.docCategory",
          docSubCategory:"$$documents.docSubCategory",
          docType:"$$documents.docType",
          docAWSReference:"$$documents.docAWSFileLocation",
          uploadedBy:"$$documents.createdUserName",
          docUploadUserId:"$$documents.createdBy",
          uploadDate:"$$documents.LastUpdatedDate"
        }
      },
      }]
  }
 

  const docDetailsMapperSpecificDoc = {
    $filter: {
      input:{
        $map: {
          input: "$entityRelatedInformation.entityDocuments",
          as: "documents",
          in: {
            docId:"$$documents._id",
            fileName:"$$documents.docFileName",
            docCategory:"$$documents.docCategory",
            docSubCategory:"$$documents.docSubCategory",
            docType:"$$documents.docType",
            docAWSReference:"$$documents.docAWSFileLocation",
            uploadedBy:"$$documents.createdUserName",
            docUploadUserId:"$$documents.createdBy",
            uploadDate:"$$documents.LastUpdatedDate",
            versionDetails:[...versionDetails || [] ]
          }
        },
      },
      as: "documents",
      cond:{$eq:["$$documents.docAWSReference",(docId || "").toString()]}
    }
  }
  const getDocMapperInfo = docId ? docDetailsMapperSpecificDoc : docDetailsMapperAll


  return EntityRel.aggregate([
    {
      $match:{
        // entityParty1:ObjectID(tenantId),
        entityParty2:{$in:[...ids.map(k => ObjectID(k))]}
      }
    },
    {
      $project: {
        tenantId:"$entityPart1",
        tenantRelatedEntityId:"$entityParty2",
        relationshipToTenant:"$relationshipType"
      }
    },
    {
      $lookup:{
        from: "entities",
        localField: "tenantRelatedEntityId",
        foreignField: "_id",
        as: "entityRelatedInformation"
      }
    },
    {
      $unwind: {
        "path": "$entityRelatedInformation",
        "preserveNullAndEmptyArrays": true
      }
    },
    {
      $project: {
        _id:"$entityRelatedInformation._id",
        tenantId:1,
        relationshipToTenant:1,
        entityId:"$entityRelatedInformation._id",
        entityName:"$entityRelatedInformation.firmName",
        entityAliases:"$entityRelatedInformation.entityAliases",
        entityType:"$entityRelatedInformation.entityType",
        entityFlags:"$entityRelatedInformation.entityFlags",
        entityAddresses:"$entityRelatedInformation.addresses",
        entityLinkedCusips:"$entityRelatedInformation.entityLinkedCusips",
        entityDocuments:"$entityRelatedInformation.entityDocuments",
        createdAt:"$entityRelatedInformation.createdAt",
        updatedAt:"$entityRelatedInformation.updatedAt",
        entityPrimaryAddress: {
          $arrayElemAt: [
            {
              $filter: {
                input: "$entityRelatedInformation.addresses",
                as: "entityaddresses",
                cond: { $eq: ["$$entityaddresses.isPrimary", true] }
              }
            }, 0]
        },
        historicalData:"$entityRelatedInformation.historicalData",
        docDetails: { 
          ...getDocMapperInfo
        },
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "entityId",
        foreignField: "entityId", 
        as: "userDetails"
      }
    },
    {
      $unwind: {
        "path": "$userDetails",
        "preserveNullAndEmptyArrays": true
      }
    },
    {
      $project: {
        ...entityRelatedColumnsGroupByInfo.select,
        userId:"$userDetails._id",
        userFirstName:"$userDetails.userFirstName",
        userLastName:"$userDetails.userLastName",
        userFlags:"$userDetails.userFlags",
        userAddresses:"$userDetails.userAddresses"
      }
    },
    {
      $group:{
        _id:{
          ...entityRelatedColumnsGroupByInfo.group,
        },
        userDetails:{
          $push:{
            userId:"$userId",
            userFirstName:"$userFirstName",
            userLastName:"$userLastName",
            userFlags:"$userFlags",
            userAddresses:"$userAddresses"
          }
        }
      }
    },
    {
      $project:{
        ...entityRelatedColumnsGroupByInfo.groupSelect,
        _id:"$_id.entityId",
        userDetails:1
      }
    },
  ])
}

const getEntityUserInformationForNormalization = async (ids) => EntityUser.aggregate([
  {$match:{_id:{"$in":[...ids.map( i => ObjectID(i))]}}},
  { 
    $project: {
      _id:1,
      userFirstName:"$userFirstName",
      userLastName:"$userLastName",
      historicalData:"$historicalData",
      userPrimaryPhone: {
        $arrayElemAt: [
          {
            $filter: {
              input: "$userPhone",
              as: "phones",
              cond: { $eq: ["$$phones.phonePrimary", true] }
            }
          }, 0]
      },      
      userPrimaryAddress: {
        $arrayElemAt: [
          {
            $filter: {
              input: "$userAddresses",
              as: "useraddress",
              cond: { $eq: ["$$useraddress.isPrimary", true] }
            }
          }, 0]
      },
      userCreatedAt:"$createdAt",
      userUpdatedAt:"$updatedAt",
      userEntityId:"$entityId",
      userFlags:"$userFlags"
    }
  },
  {
    $lookup:{
      from: "entityrels",
      localField: "userEntityId",
      foreignField: "entityParty2", 
      as: "entityRelationship"
    },
  },
  {
    $unwind:"$entityRelationship"
  },
  {
    $addFields:{
      tenantId:"$entityRelationship.entityParty1",
      entityRelationshipToTenant:"$entityRelationship.relationshipType"
    }
  },
  {
    $lookup:{
      from: "entities",
      localField: "userEntityId",
      foreignField: "_id", 
      as: "entitydetails"
    },
  },
  {
    $unwind:"$entitydetails"
  },
  {
    $project:{
      _id:1,
      userFirstName:1,
      userLastName:1,
      userPrimaryPhone: 1,      
      userPrimaryAddress: 1,
      userCreatedAt:1,
      userUpdatedAt:1,
      userEntityId:1,
      userFlags:1,
      tenantId:1,
      historicalData:1,
      entityRelationshipToTenant:1,
      userFirmName:"$entitydetails.firmName"
    }
  },
])

const getTaskDetailsForSearchOld = async(docId,tasks,versionDetails) => {

  // console.log("The details of the Tasks", docId,tasks,versionDetails)

  const docDetailsMapperAll = {
    $setUnion:[
      {$map: {
        input: "$taskRelatedDocuments",
        as: "documents",
        in: {
          docId:"$$documents._id",
          fileName:"$$documents.docFileName",
          docCategory:{"$ifNull":["$$documents.docCategory","Tasks"]},
          docSubCategory:{"$ifNull":["$$documents.docSubCategory","Task Documents"]},
          docType:{"$ifNull":["$$documents.docType","Task Documents"]},
          docAWSReference:"$$documents.docId",
          uploadedBy:"$$documents.createdUserName",
          docUploadUserId:"$$documents.createdBy",
          uploadDate:"$$documents.LastUpdatedDate",
          versionDetails:[...versionDetails || [] ]
        }
      },
      }]
  }
 

  const docDetailsMapperSpecificDoc = {
    $filter: {
      input:{
        $map: {
          input: "$taskRelatedDocuments",
          as: "documents",
          in: {
            docId:"$$documents._id",
            fileName:"$$documents.docFileName",
            docCategory:{"$ifNull":["$$documents.docCategory","Tasks"]},
            docSubCategory:{"$ifNull":["$$documents.docSubCategory","Task Documents"]},
            docType:{"$ifNull":["$$documents.docType","Task Documents"]},
            docAWSReference:"$$documents.docId",
            uploadedBy:"$$documents.createdUserName",
            docUploadUserId:"$$documents.createdBy",
            uploadDate:"$$documents.LastUpdatedDate",
            versionDetails:[...versionDetails || [] ]
          }
        },
      },
      as: "documents",
      cond:{$eq:["$$documents.docAWSReference",(docId || "").toString()]}
    }
  }
  const getDocMapperInfo = docId ? docDetailsMapperSpecificDoc : docDetailsMapperAll


  const taskDetails = await Tasks.aggregate([
    {$match:{$or:[{_id:{$in:[...tasks.map( k => ObjectID(k))]}},{taskRefId:{$in:[...tasks.map( k => k.toString())]}}]}},
    {
      $project:{
        docId:"$_id",
        taskDetails:1,
        createdAt:1,
        updatedAt:1,
        relatedActivityId:"$relatedActivityDetails.activityId",
        docDetails:{
          ...getDocMapperInfo
        }
      }
    },
    {
      $facet:{
        "deals":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.dealIssueTranClientId",
              clientEntityId: "$trans.dealIssueTranIssuerId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                  then:"$trans.dealIssueTranProjectDescription",
                  else:"$trans.dealIssueTranIssueName"},
              },
              tranPrimarySector:"$trans.dealIssueTranPrimarySector",
              tranSecondarySector:"$trans.dealIssueTranSecondarySector",
              tranStatus:{$ifNull:["$trans.dealIssueTranStatus","-"]},
              tranClientName:"$trans.dealIssueTranIssuerFirmName",
              tranFirmName:"$trans.dealIssueTranClientFirmName",
              tranType:"$trans.dealIssueTranType",
              tranSubType:"$trans.dealIssueTranSubType",
            }
          }
        ],
        "bankloans":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranClientName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
            }
          },
        ],
        "derivatives":[
          {
            $lookup:{
              from: "tranderivatives",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate","-"]},
              tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranFirmName",
              tranFirmName:"$trans.actTranClientName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
            }
          },
        ],
        "marfps":[
          {
            $lookup:{
              from: "actmarfps",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actProjectName",
                  else:"$trans.actIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate","-"]},
              tranPrimarySector:"$trans.actPrimarySector",
              tranSecondarySector:"$trans.actSecondarySector",
              tranStatus:{$ifNull:["$trans.actStatus","-"]},
              tranClientName:"$trans.actIssuerClientEntityName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
            }
          },
        ],
        "others":[
          {
            $lookup:{
              from: "tranagencyothers",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranIssuerFirmName",
              tranFirmName:"$trans.actTranClientFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
            }
          },
        ],
        "rfps":[
          {
            $lookup:{
              from: "tranagencyrfps",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.rfpTranClientId",
              clientEntityId: "$trans.rfpTranIssuerId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
                  then:"$trans.rfpTranProjectDescription",
                  else:"$trans.rfpTranIssueName"},
              },
              tranParAmount:"-",
              tranExpectedAwardDate:"-",
              tranActualAwardDate:"-",
              tranPricingDate:"-",
              tranPrimarySector:"$trans.rfpTranPrimarySector",
              tranSecondarySector:"$trans.rfpTranSecondarySector",
              tranStatus:{$ifNull:["$trans.rfpTranStatus","-"]},
              tranClientName:"$trans.rfpTranIssuerFirmName",
              tranFirmName:"$trans.rfpTranClientFirmName",
              tranType:"$trans.rfpTranType",
              tranSubType:"$trans.rfpTranSubType",
            }
          },
        ],
        "busdevs":[
          {
            $lookup:{
              from: "actbusdevs",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actProjectName",
                  else:"$trans.actIssueName"},
              },
              tranParAmount:"-",
              tranExpectedAwardDate:"-",
              tranActualAwardDate:"-",
              tranPricingDate:"-",
              tranPrimarySector:"$trans.actPrimarySector",
              tranSecondarySector:"$trans.actSecondarySector",
              tranStatus:{$ifNull:["$trans.actStatus","-"]},
              tranClientName:"$trans.actIssuerClientEntityName",
              tranFirmName:"$trans.actProjectName",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
            }
          }
        ],
        "generalandother":[
          {
            $match: {
              $and: [
                {
                  "relatedActivityId": {
                    $exists: false
                  }
                }
              ]
            }
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              taskDetails:1,
              taskCategory: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$taskDetails.taskType", "Compliance"]
                      },
                      then: "Compliance Tasks"
                    }, {
                      case: {
                        $eq: ["$taskDetails.taskType", "general"]
                      },
                      then: "General Tasks"
                    }
                  ],
                  default: "Transaction Related Tasks"
                }
              },
            
            }
          }
        ],
        "taskUserDetails":[
          {
            $lookup:{
              from: "entityusers",
              localField: "taskDetails.taskAssigneeUserId",
              foreignField: "_id",
              as: "userInfo"
            }
          },
          {
            $unwind:"$userInfo"
          },
          {
            $project:{
              docId:1,
              userId:"$userInfo._id",
              userFirstName:"$userInfo.userFirstName",
              userLastName:"$userInfo.userLastName",
            }
          }
        ],
        "taskEntityDetails":[
          {
            $lookup:{
              from: "entities",
              localField: "taskDetails.taskAssigneeUserId",
              foreignField: "_id",
              as: "entityDetails"
            }
          },
          {
            $unwind:"$entityDetails"
          },
          {
            $project:{
              docId:1,
              entityId:"$entityDetails._id",
              entityName:"$entityDetails.firmName",
              entityAliases:"$entityDetails.entityAliases",
              entityType:"$entityDetails.entityType",
              entityFlags:"$entityDetails.entityFlags"
            }
          }
        ]
      }
    },
    { 
      "$project": {
        "taskCoreData": { "$concatArrays": ["$deals","$bankloans","$derivatives","$marfps","$others","$rfps","$busdevs","$generalandother"] },
        "taskAssigneeDetails":{"$concatArrays":["$taskUserDetails","$taskEntityDetails"]}
      }
    },
  ])

  console.log("The task details",taskDetails)

  let retValue = []
  if ( taskDetails.length > 0) {
    const {taskCoreData, taskAssigneeDetails} = taskDetails[0]
    retValue = taskCoreData.map( t => {
      const tad = taskAssigneeDetails.filter( f => f._id.toString() === t._id.toString())
      return {...t, assigneeDetails:tad.length >0 ? tad[0]:{}}
    })
  }

  return retValue

}


const getTaskDetailsForSearch = async(docId,tasks,versionDetails,docContext) => {

  // console.log("The details of the Tasks", docId,tasks,versionDetails)

  const docDetailsMapperAll = {
    $setUnion:[
      {$map: {
        input: "$taskRelatedDocuments",
        as: "documents",
        in: {
          docId:"$$documents._id",
          fileName:"$$documents.docFileName",
          docCategory:{"$ifNull":["$$documents.docCategory","Tasks"]},
          docSubCategory:{"$ifNull":["$$documents.docSubCategory","Task Documents"]},
          docType:{"$ifNull":["$$documents.docType","Task Documents"]},
          docAWSReference:"$$documents.docId",
          uploadedBy:"$$documents.createdUserName",
          docUploadUserId:"$$documents.createdBy",
          uploadDate:"$$documents.LastUpdatedDate",
          versionDetails:[...versionDetails || [] ]
        }
      },
      }]
  }
 

  const docDetailsMapperSpecificDoc = {
    $filter: {
      input:{
        $map: {
          input: "$taskRelatedDocuments",
          as: "documents",
          in: {
            docId:"$$documents._id",
            fileName:"$$documents.docFileName",
            docCategory:{"$ifNull":["$$documents.docCategory","Tasks"]},
            docSubCategory:{"$ifNull":["$$documents.docSubCategory","Task Documents"]},
            docType:{"$ifNull":["$$documents.docType","Task Documents"]},
            docAWSReference:"$$documents.docId",
            uploadedBy:"$$documents.createdUserName",
            docUploadUserId:"$$documents.createdBy",
            uploadDate:"$$documents.LastUpdatedDate",
            versionDetails:[...versionDetails || [] ]
          }
        },
      },
      as: "documents",
      cond:{$eq:["$$documents.docAWSReference",(docId || "").toString()]}
    }
  }
  const getDocMapperInfo = docId ? docDetailsMapperSpecificDoc : docDetailsMapperAll


  const taskDetails = await Tasks.aggregate([
    {$match:{$or:[{_id:{$in:[...tasks.map( k => ObjectID(k))]}},{taskRefId:{$in:[...tasks.map( k => k.toString())]}}]}},
    {
      $project:{
        // docId:{ $cond: [ {$in:["$taskRefId",[...tasks.map( k => k.toString())]]}, "$taskRefId", "$_id" ] },
        docId:"$_id",
        taskDetails:1,
        taskRefId:1,
        createdAt:1,
        updatedAt:1,
        relatedActivityId:"$relatedActivityDetails.activityId",
        relatedActivityDetails:1,
        taskRelatedDocuments:1,
        docDetails:{
          ...getDocMapperInfo
        }
      }
    },
    {
      $facet:{
        "deals":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              relatedActivityDetails:1,
              taskRelatedDocuments:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.dealIssueTranClientId",
              clientEntityId: "$trans.dealIssueTranIssuerId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                  then:"$trans.dealIssueTranProjectDescription",
                  else:"$trans.dealIssueTranIssueName"},
              },
              tranPrimarySector:"$trans.dealIssueTranPrimarySector",
              tranSecondarySector:"$trans.dealIssueTranSecondarySector",
              tranStatus:{$ifNull:["$trans.dealIssueTranStatus","-"]},
              tranClientName:"$trans.dealIssueTranIssuerFirmName",
              tranFirmName:"$trans.dealIssueTranClientFirmName",
              tranType:"$trans.dealIssueTranType",
              tranSubType:"$trans.dealIssueTranSubType",
            }
          }
        ],
        "bankloans":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.bankLoanTerms.parAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranPricingDate:{$ifNull:["$trans.bankLoanSummary.actTranClosingDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranClientName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
            }
          },
        ],
        "derivatives":[
          {
            $lookup:{
              from: "tranderivatives",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.derivativeSummary.tranNotionalAmt","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.derivativeSummary.tranEffDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.derivativeSummary.tranEndDate","-"]},
              tranPricingDate:{$ifNull:["$trans.derivativeSummary.tranTradeDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranFirmName",
              tranFirmName:"$trans.actTranClientName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
            }
          },
        ],
        "marfps":[
          {
            $lookup:{
              from: "actmarfps",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actProjectName",
                  else:"$trans.actIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.maRfpSummary.actParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.maRfpSummary.actExpAwaredDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.maRfpSummary.actActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.maRfpSummary.actPricingDate","-"]},
              tranPrimarySector:"$trans.actPrimarySector",
              tranSecondarySector:"$trans.actSecondarySector",
              tranStatus:{$ifNull:["$trans.actStatus","-"]},
              tranClientName:"$trans.actIssuerClientEntityName",
              tranFirmName:"$trans.actTranFirmName",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
            }
          },
        ],
        "others":[
          {
            $lookup:{
              from: "tranagencyothers",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actTranClientId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              tranParAmount:{$ifNull:["$trans.dealIssueParAmount","-"]},
              tranExpectedAwardDate:{$ifNull:["$trans.dealIssueExpAwardDate","-"]},
              tranActualAwardDate:{$ifNull:["$trans.dealIssueActAwardDate","-"]},
              tranPricingDate:{$ifNull:["$trans.dealIssuePricingDate","-"]},
              tranPrimarySector:"$trans.actTranPrimarySector",
              tranSecondarySector:"$trans.actTranSecondarySector",
              tranStatus:{$ifNull:["$trans.actTranStatus","-"]},
              tranClientName:"$trans.actTranIssuerFirmName",
              tranFirmName:"$trans.actTranClientFirmName",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
            }
          },
        ],
        "rfps":[
          {
            $lookup:{
              from: "tranagencyrfps",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.rfpTranClientId",
              clientEntityId: "$trans.rfpTranIssuerId",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
                  then:"$trans.rfpTranProjectDescription",
                  else:"$trans.rfpTranIssueName"},
              },
              tranParAmount:"-",
              tranExpectedAwardDate:"-",
              tranActualAwardDate:"-",
              tranPricingDate:"-",
              tranPrimarySector:"$trans.rfpTranPrimarySector",
              tranSecondarySector:"$trans.rfpTranSecondarySector",
              tranStatus:{$ifNull:["$trans.rfpTranStatus","-"]},
              tranClientName:"$trans.rfpTranIssuerFirmName",
              tranFirmName:"$trans.rfpTranClientFirmName",
              tranType:"$trans.rfpTranType",
              tranSubType:"$trans.rfpTranSubType",
            }
          },
        ],
        "busdevs":[
          {
            $lookup:{
              from: "actbusdevs",
              localField: "relatedActivityId",
              foreignField: "_id",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              createdAt:1,
              updatedAt:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskCategory:"Transaction Related Tasks",
              tranId:"$trans._id",
              firmEntityId:"$trans.actTranFirmId",
              clientEntityId: "$trans.actIssuerClient",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actProjectName",
                  else:"$trans.actIssueName"},
              },
              tranParAmount:"-",
              tranExpectedAwardDate:"-",
              tranActualAwardDate:"-",
              tranPricingDate:"-",
              tranPrimarySector:"$trans.actPrimarySector",
              tranSecondarySector:"$trans.actSecondarySector",
              tranStatus:{$ifNull:["$trans.actStatus","-"]},
              tranClientName:"$trans.actIssuerClientEntityName",
              tranFirmName:"$trans.actProjectName",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
            }
          }
        ],
        "generalandother":[
          {
            $match: {
              $and: [
                {
                  "relatedActivityId": {
                    $exists: false
                  }
                }
              ]
            }
          },
          {
            $project:{
              _id:"$docId",
              docId:1,
              taskDetails:1,
              relatedActivityDetails:1,
              docDetails:1,
              taskRelatedDocuments:1,
              taskCategory: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$taskDetails.taskType", "Compliance"]
                      },
                      then: "Compliance Tasks"
                    }, {
                      case: {
                        $eq: ["$taskDetails.taskType", "general"]
                      },
                      then: "General Tasks"
                    }
                  ],
                  default: "Transaction Related Tasks"
                }
              },
            
            }
          }
        ],
        "taskUserDetails":[
          {
            $lookup:{
              from: "entityusers",
              localField: "taskDetails.taskAssigneeUserId",
              foreignField: "_id",
              as: "userInfo"
            }
          },
          {
            $unwind:"$userInfo"
          },
          {
            $project:{
              docId:1,
              userId:"$userInfo._id",
              userFirstName:"$userInfo.userFirstName",
              userLastName:"$userInfo.userLastName",
            }
          }
        ],
        "taskEntityDetails":[
          {
            $lookup:{
              from: "entities",
              localField: "taskDetails.taskAssigneeUserId",
              foreignField: "_id",
              as: "entityDetails"
            }
          },
          {
            $unwind:"$entityDetails"
          },
          {
            $project:{
              docId:1,
              entityId:"$entityDetails._id",
              entityName:"$entityDetails.firmName",
              entityAliases:"$entityDetails.entityAliases",
              entityType:"$entityDetails.entityType",
              entityFlags:"$entityDetails.entityFlags"
            }
          }
        ]
      }
    },
    { 
      "$project": {
        "taskCoreData": { "$concatArrays": ["$deals","$bankloans","$derivatives","$marfps","$others","$rfps","$busdevs","$generalandother"] },
        "taskAssigneeDetails":{"$concatArrays":["$taskUserDetails","$taskEntityDetails"]}
      }
    }
  ])


  let retValue = []
  if ( taskDetails.length > 0) {
    const {taskCoreData, taskAssigneeDetails} = taskDetails[0]
    retValue = taskCoreData.map( t => {
      const tad = taskAssigneeDetails.filter( f => f._id.toString() === t._id.toString())
      return {...t, assigneeDetails:tad.length >0 ? tad[0]:{}}
    })
  }
  return retValue
}


export const getAllTenantUsers = async() => EntityRel.aggregate([
  {
    $match:{relationshipType:"Self"}
  },
  {
    $lookup:{
      from: "entityusers",
      localField: "entityParty2",
      foreignField: "entityId", 
      as: "alltenantentityusers"
    }
  },
  {
    $unwind: "$alltenantentityusers"
  }, {
    $project: {
      _id: 0,
      entityId: 1,
      userId: "$alltenantentityusers._id",
      userFullName: {
        $concat: ["$alltenantentityusers.userFirstName", " ", "$alltenantentityusers.userLastName"]
      },
      userPrimaryEmail: "$alltenantentityusers.userLoginCredentials.userEmailId"
    }
  }  
])


export const dataDocNormalizer = async (docContext, getIdDetails, docId,docVersionDetails,otherparams) =>{
  
  const globalMetaData = {
    context:docContext,
    docId,
    versions:docVersionDetails,
    ...otherparams
  }

  try {

    const tenantUsers = await getAllTenantUsers()

    // ({type, docId, versionDetails,alluserdetails})
    const mappedFunctions = {
      "DEALS": (tid, docId,docVersionDetails,tenantUsers) => Deals.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"deals",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "RFPS": (tid, docId,docVersionDetails,tenantUsers) => RFP.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"rfps",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "BANKLOANS": (tid, docId,docVersionDetails,tenantUsers) => BankLoans.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"bankloans",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "BUSDEV": (tid, docId,docVersionDetails,tenantUsers) => ActBusDev.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"busdev",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "OTHERS": (tid, docId,docVersionDetails,tenantUsers) => Others.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"others",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "DERIVATIVES": (tid, docId,docVersionDetails,tenantUsers) => Derivatives.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"derivatives",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "MARFPS": (tid, docId,docVersionDetails,tenantUsers) => ActMaRFP.aggregate([
        {$match:{_id:ObjectID(tid)}},
        ...dataNormalizerHelper({type:"marfps",docId,versionDetails:docVersionDetails,alluserdetails:tenantUsers})
      ]),
      "ENTITYDOCS": (id, docId, docVersionDetails) => getEntityInformationForNormalization(docId, [id], docVersionDetails ),
      "THIRDPARTYDOCS": (id, docId, docVersionDetails) => getEntityInformationForNormalization(docId, [id], docVersionDetails ),
      "CLIENTPROSPECTDOCS": (id, docId, docVersionDetails) => getEntityInformationForNormalization(docId, [id], docVersionDetails ),
      "TASKS": (id, docId, docVersionDetails) => getTaskDetailsForSearch(docId, [id], docVersionDetails,docContext )
    }


    const functionToRun = mappedFunctions[docContext] || ((id, docId, ver) => ({
      [docId]:{...globalMetaData}
    }))
    console.log("The funciton to run", functionToRun)
    const retData = await functionToRun(getIdDetails, docId,docVersionDetails,tenantUsers)

    if (retData && retData.length > 0) {
      return {[docId]:{...retData[0],...globalMetaData}}
    }
    return {[docId]:{...globalMetaData}}
  } catch (e) {
    console.log("there is an error getting information for doc parsing", e)
    return {[docId]:{...globalMetaData}}
  }
}
/*
entity: "ENTITYDOCS" - DONE,
rfp: "RFPS" - DONE,
deal: "DEALS" - DONE,
loan: "BANKLOANS" - DONE,
maRfp: "MARFPS" - DONE,
derivative: "DERIVATIVES" - DONE,
others: "OTHERS" - DONE,
supervisor: {
  svAndObligation: "SVOBLIGATIONS",
  svProQualification: "SVPROFQUALIFICATIONS",
  svAdmin: "SVGENADMIN",
  svComplaints: "SVCLIENTCOMPLAINTS",
  businessConduct: "SVBUSINESSCONDUCT",
  poliProducers: "POLICIESPROCEDURES",
  msrbSubmissions: "SVMSRBSUBMISSIONS",
  poliContributeRelated: "SVPOLICONTRELATEDDOCS",
  giftsAndGrautities: "SVGIFTSGRATUITIES"
},
billing: {
  receipts: "BILLINGEXPENSERECEIPTS"
},
busDev: "BUSDEV" - DONE,
contract: "ENTITYCONTRACT" - DONE,
cac: "CONTROLS",
thirdParty: "THIRDPARTYDOCS" - DONE,
generalDoc: "GENERALDOCS",
user: "USERDOCS",
tasks: "TASKS" - DONE,
entityLogo: "ENTITYLOGO",
migrationTools: "MIGRATIONTOOLS"
*/
/*
URL Mapping
=====================
entity: "ENTITYDOCS":/clients-propects/5c80cd71a7e1e71e655ab7b5/documents,
rfp: "RFPS":/rfp/__TRAN_ID/documents,
deal: "DEALS":/deals/__TRANID__/documents,
loan: "BANKLOANS":/loan/__TRANID__/documents,
maRfp: "MARFPS":/marfps/__TRANID__/documents,
derivative: "DERIVATIVES":/derivatives/__TRANID__/documents,
others: "OTHERS":/others/__TRANID__/documents,
busDev: "BUSDEV":/bus-development/5c6671ebfc6262160794f32f,
contract: "ENTITYCONTRACT":/clients-propects/5c80cd71a7e1e71e655ab7b5/contracts,
thirdParty: "THIRDPARTYDOCS":/thirdparties/5c04717979475a4344ab73e1/documents,
tasks: "TASKS":/tasks/5c66f08bfc6262160794f4c9

The below are only accessible to Tenant Users Only:
=========================================================

poliProducers: "POLICIESPROCEDURES":/comp-polandproc,
svAdmin: "SVGENADMIN":/compliance/cmp-sup-general/record-keeping,
cac: "CONTROLS":/cac/controls-list/controls-lists,
user: "USERDOCS":tools-docs/manage-doc,
migrationTools: "MIGRATIONTOOLS":/admin-migtools
entityLogo: "ENTITYLOGO":/admin-firms/5c5e54faaf6ffc17a93dc60a/firms,
svAndObligation: "SVOBLIGATIONS":/compliance/cmp-sup-compobligations/recordkeeping,
receipts: "BILLINGEXPENSERECEIPTS":/tools-billing
svComplaints: "SVCLIENTCOMPLAINTS":/compliance/cmp-sup-complaints/complaints,
businessConduct: "SVBUSINESSCONDUCT":/compliance/cmp-sup-busconduct/business-conduct,
svProQualification: "SVPROFQUALIFICATIONS":/compliance/cmp-sup-prof/associate-persons,
poliContributeRelated: "SVPOLICONTRELATEDDOCS":/compliance/cmp-sup-political/instructions,
giftsAndGrautities: "SVGIFTSGRATUITIES":compliance/cmp-sup-gifts/instructions
*/

export const getDataForNormalization = async (context,mongoIds) => {
  const tenantUsers = await getAllTenantUsers()

  try {
    const mappedFunctions = {
      "DEALS": (tids,tenantUsers) => Deals.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"deals",alluserdetails:tenantUsers})
      ]),
      "RFPS": (tids) => RFP.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"rfps",alluserdetails:tenantUsers})
      ]),
      "BANKLOANS": (tids) => BankLoans.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"bankloans",alluserdetails:tenantUsers})
      ]),
      "BUSDEV": (tids) => ActBusDev.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"busdevs",alluserdetails:tenantUsers})
      ]),
      "OTHERS": (tids) => Others.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"others",alluserdetails:tenantUsers})
      ]),
      "DERIVATIVES": (tids) => Derivatives.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"derivatives",alluserdetails:tenantUsers})
      ]),
      "MARFPS": (tids) => ActMaRFP.aggregate([
        {$match:{_id:{$in:[...tids.map( m => ObjectID(m))]}}},
        ...dataNormalizerHelper({type:"marfps",alluserdetails:tenantUsers})
      ]),
      "ENTITYINFO": (ids) => getEntityInformationForNormalization(undefined, ids, undefined ),
      "USERINFO":(ids) => getEntityUserInformationForNormalization(ids),
      "TASKSINFO": (ids) => getTaskDetailsForSearch(undefined, ids, undefined )
    }
    const functionToRun = mappedFunctions[context] || ((mongoIds) => ([]))
    const retData = await functionToRun(mongoIds,tenantUsers)
    const newretData = retData.map( ({historicalData,...otherdata}) => ({...otherdata,historicalData:JSON.stringify(historicalData || {})}))
    return newretData
  } catch (e) {
    console.log("there is an error getting information", e)
    return []
  }
}

export const getDataForGranularInformation = async ( docIds) => {

  const allDocIds = docIds.map( d => ObjectID(d))
  
  const docInfo = await Docs.aggregate([
    {
      $match:{_id:{$in:[...allDocIds]}}
    },
    {
      $project:{
        docId:"$_id",
        tenantId:1,
        contextType:1,
        contextId:1,
        category:{$ifNull:["$meta.category","N/A"]},
        subcategory:{$ifNull:["$meta.subCategory","N/A"]},
        type:{$ifNull:["$meta.type","N/A"]},
        versions:"$meta.versions"
      }
    },
    {
      $unwind:"$versions"
    },
    {
      $project:{
        docId:1,
        tenantId:1,
        contextType:1,
        contextId:1,
        category:1,
        subcategory:1,
        type:1,
        versionId:"$versions.versionId",
        fileName:"$versions.name",
        fileObjectPath:{ $concat: [ "$tenantId", "/", "$contextType","/","$contextType","_","$contextId","/","$versions.name" ] }
      }
    },
    {
      $group:{
        _id:{
          contextId:"$contextId",
          contextType:"$contextType",
          docId:"$docId",
          category:"$category",
          subcategory:"$subcategory",
          type:"$type"
        },
        docVersionDetails:{ 
          $addToSet: {
            versionId:"$versionId",
            fileName:"$fileName",
            filePath:"$fileObjectPath"
          }
        }
      }
    },
    {
      $project:{
        _id:0,
        contextId:"$_id.contextId",
        contextType:"$_id.contextType",
        docId:"$_id.docId",
        category:"$_id.category",
        subcategory:"$_id.subcategory",
        type:"$_id.type",
        docVersionDetails:1
      }
    }
  ])

  return docInfo
}

export const getDetailsForDocParsr = async (docIds) => {
  try {
    const docDetails = await getDataForGranularInformation(docIds)
    console.log(docDetails)
    return Promise.all(docDetails.map( async ({contextId, contextType, docId,docVersionDetails,...otherparams}) => dataDocNormalizer(contextType, contextId, docId,docVersionDetails,otherparams)))
  }
  catch(e) {
    console.log("There is an error", e)
  }
}


export const normalizeAllConfiguredDataForTenant = async( tenantId) => {

  try {
    // await clearIndex(`tenant_data_${tenantId}`)
    // await setupTenantIndex(tenantId)
    const entityDetails = EntityRel.aggregate([
      {
        $match:{entityParty1:ObjectID(tenantId)}
      },
      {
        $project:{
          _id:"$entityParty2"
        }
      }
    ])
    const userDetails = EntityRel.aggregate([
      {
        $match:{entityParty1:ObjectID(tenantId)}
      },
      {
        $project:{
          _id:"$entityParty2"
        }
      },
      {
        $lookup:{
          from: "entityusers",
          localField: "_id",
          foreignField: "entityId",
          as: "userInfo"
        }
      },
      {
        $unwind:"$userInfo"
      },
      {
        $project:{
          _id:"$userInfo._id"
        }    
      }
    ])
    const [entityIds, userIds] = await Promise.all([entityDetails, userDetails])
   
    // Get all the eligible ids for the tenant
    const[dids,rids,marfpids,blids,derids,busdevids,others,tids] = await Promise.all([
      Deals.find({dealIssueTranClientId:ObjectID(tenantId)}).select({_id:1}),
      RFP.find({rfpTranClientId:ObjectID(tenantId)}).select({_id:1}),
      ActMaRFP.find({actTranFirmId:ObjectID(tenantId)}).select({_id:1}),
      BankLoans.find({actTranFirmId:ObjectID(tenantId)}).select({_id:1}),
      Derivatives.find({actTranFirmId:ObjectID(tenantId)}).select({_id:1}),
      ActBusDev.find({actTranFirmId:ObjectID(tenantId)}).select({_id:1}),
      Others.find({actTranFirmId:ObjectID(tenantId)}).select({_id:1}),
      Tasks.find({"taskDetails.taskAssigneeUserId":{$in:[...entityIds.map(k => k._id),...userIds.map(k=>k._id)]}}).select({_id:1})
    ])
    
    // Get all the normalized data for the eligible IDs
    const [didsN,ridsN,marfpidsN,blidsN,deridsN,busdevidsN,othersN,tidsN,entitiesN,usersN] = await Promise.all([
      getDataForNormalization("DEALS",dids.map(k => k._id)),
      getDataForNormalization("RFPS",rids.map(k => k._id)),
      getDataForNormalization("MARFPS",marfpids.map(k => k._id)),
      getDataForNormalization("BANKLOANS",blids.map(k => k._id)),
      getDataForNormalization("DERIVATIVES",derids.map(k => k._id)),
      getDataForNormalization("BUSDEV",busdevids.map(k => k._id)),
      getDataForNormalization("OTHERS",others.map(k => k._id)),
      getDataForNormalization("TASKSINFO",tids.map(k => k._id)),
      getDataForNormalization("ENTITYINFO",entityIds.map(k => k._id)),
      getDataForNormalization("USERINFO",userIds.map(k => k._id)),
    ])

    const categoryString = [ "Deals", "Manage Rfps", "MA RFPs", "Bank Loans", "Derivatives", "Business Development Activities", "Other Transactions","Task Info", "Entity Details","User Details"]
    const allDocsToBeInserted = [didsN,ridsN,marfpidsN,blidsN,deridsN,busdevidsN,othersN,tidsN,entitiesN,usersN].reduce( (a,k, i) => [...a, ...k.map( l => ({tenantId,_id:l._id,category:categoryString[i],data:{...l}}))] ,[])
    // console.log(JSON.stringify(allDocsToBeInserted,null,2))
    const esInsert = await putBulkDataRevised(tenantId,allDocsToBeInserted )
    console.log("This is what was inserted into the index", JSON.stringify(esInsert,null,2))
    
    return {
      "DEALS":didsN.length,
      "RFPS":ridsN.length,
      "MARFPS":marfpidsN.length,
      "BANKLOANS":blidsN.length,
      "DERIVATIVES":deridsN.length,
      "BUSDEV":busdevidsN.length,
      "OTHERS":othersN.length,
      "TASKSINFO":tidsN.length,
      "ENTITYINFO":entitiesN.length,
      "USERINFO":usersN.length,
      "CONSOLIDATEDINFO":allDocsToBeInserted.length
    } 
  } catch (error) {
    console.log(`There is an error getting normalized data for the tenant ${tenantId}`, error)   
  }
}


