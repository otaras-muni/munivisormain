import express from "express"
import axios from "axios"
import config from "./config"
import {
  getHeaders,
  deleteTagsForTenant,
  initandreseeddata,
  setupTenantIndex,
  initAndReseedDataForAllTenants,
  initAndReseedDataOnlyForTenant
} from "./esHelper"
import {
  getDetailsForDocParsr,
  getDataForNormalization,
  normalizeAllConfiguredDataForTenant
} from "./elasticsearch.controller"
import {
  getDocs,
  deleteDocs,
  updateState,
  dropMetaIndex,
  checkMetaIndex
} from "./metaIndexHelper"

require("dotenv").config()

const appUrl = `${process.env.ES_HOST}${process.env.ES_INDEX}`

export const esRouter = express.Router()

esRouter.get("/init", async (req, res) => {
  try {
    const apiRes = await axios.put(`${appUrl}`, config, {
      headers: getHeaders()
    })
    res.send(apiRes.data)
  } catch (err) {
    throw err
  }
})

esRouter.post("/getdocsmetadata", async (req, res) => {
  try {
    const { docIds } = req.body
    const normalizedDataForDocParser = await getDetailsForDocParsr(docIds)
    res.send(normalizedDataForDocParser)
  } catch (err) {
    throw err
  }
})

esRouter.post("/getmetadataforagivencontext", async (req, res) => {
  try {
    const { context, ids } = req.body
    const normalizedDataForDocParser = await getDataForNormalization(
      context,
      ids
    )
    res.send(normalizedDataForDocParser)
  } catch (err) {
    throw err
  }
})

esRouter.post("/initandseed", async (req, res) => {
  try {
    const apiRes = await initandreseeddata()
    res.send(apiRes)
  } catch (err) {
    throw err
  }
})

esRouter.get("/clear", async (req, res) => {
  try {
    const apiRes = await axios.delete(`${appUrl}`, { headers: getHeaders() })
    res.send(apiRes.data)
  } catch (err) {
    throw err
  }
})

esRouter.post("/cleartags/:tenantId", async (req, res) => {
  const { tenantId } = req.params
  try {
    const apiRes = await deleteTagsForTenant(tenantId)
    res.send(apiRes)
  } catch (err) {
    throw err
  }
})

esRouter.post("/init-tenant", async (req, res) => {
  const { tenantId } = req.body
  try {
    const apiRes = await setupTenantIndex(tenantId)
    res.send(apiRes)
  } catch (err) {
    throw err
  }
})

esRouter.post("/init-reseed-alltenants", async (req, res) => {
  try {
    const { tenantIds } = req.body
    const apiRes = await initAndReseedDataForAllTenants(tenantIds)
    res.send(apiRes)
  } catch (err) {
    throw err
  }
})

esRouter.post("/init-reseed-tenant", async (req, res) => {
  const { tenantId } = req.body
  try {
    const apiRes = await initAndReseedDataOnlyForTenant(tenantId)
    res.send(apiRes)
  } catch (err) {
    throw err
  }
})

esRouter.post("/index-data", async (req, res) => {
  const { tenantId } = req.body
  try {
    const apiRes = await normalizeAllConfiguredDataForTenant(tenantId)
    res.send(apiRes)
  } catch (err) {
    throw err
  }
})

esRouter.post("/get-meta-docs", async (req, res) => {
  try {
    const apiRes = await getDocs(req.body)
    res.send(apiRes)
  } catch (err) {
    console.log(err)
    res.status(400).send(err)
  }
})

esRouter.post("/delete-meta-docs", async (req, res) => {
  try {
    const apiRes = await deleteDocs(req.body)
    res.send(apiRes)
  } catch (err) {
    res.status(400).send(err)
  }
})

esRouter.post("/update-docs-state", async (req, res) => {
  try {
    const apiRes = await updateState(req.body)
    res.send(apiRes)
  } catch (err) {
    res.status(400).send(err)
  }
})

esRouter.delete("/metaindex", async (req, res) => {
  try {
    const apiRes = await dropMetaIndex()
    res.send(apiRes)
  } catch (error) {
    res.status(400).send(error)
  }
})

esRouter.get("/metaindex", async (req, res) => {
  try {
    const apiRes = await checkMetaIndex()
    res.send(apiRes)
  } catch (error) {
    res.status(400).send(error)
  }
})
