const config = {
  template: "tenant_data_*",
  settings: {
    "index.mapping.total_fields.limit": 2048,
    analysis: {
      analyzer: {
        autosuggest_analyzer: {
          filter: ["lowercase", "asciifolding", "autosuggest_filter"],
          tokenizer: "standard",
          type: "custom"
        },
        ngram_analyzer: {
          filter: ["lowercase", "asciifolding", "ngram_filter"],
          tokenizer: "standard",
          type: "custom"
        }
      },
      filter: {
        autosuggest_filter: {
          max_gram: "20",
          min_gram: "1",
          token_chars: ["letter", "digit", "punctuation", "symbol"],
          type: "edge_ngram"
        },
        ngram_filter: {
          max_gram: "9",
          min_gram: "2",
          token_chars: ["letter", "digit", "punctuation", "symbol"],
          type: "ngram"
        }
      }
    }
  },
  mappings: {
    doc: {
      dynamic_templates: [
        {
          string_as_float: {
            match_mapping_type: "string",
            path_match: "data.tranParAmount",
            mapping: {
              type: "float"
            }
          }
        },
        {
          long_as_float: {
            match_mapping_type: "long",
            path_match: "data.tranParAmount",
            mapping: {
              type: "float"
            }
          }
        },
        {
          strings: {
            match_mapping_type: "string",
            unmatch: "doc_content*",
            mapping: {
              type: "text",
              fields: {
                autosuggest: {
                  type: "text",
                  analyzer: "autosuggest_analyzer",
                  search_analyzer: "simple"
                },
                keyword: {
                  type: "keyword",
                  ignore_above: 256
                },
                search: {
                  type: "text",
                  analyzer: "ngram_analyzer",
                  search_analyzer: "simple"
                }
              },
              analyzer: "standard"
            }
          }
        }
      ],
      dynamic: true,
      _source: {
        excludes: ["doc_content", "doc_content.search"]
      },
      properties: {
        doc_content: {
          type: "text",
          term_vector: "with_positions_offsets",
          store: true,
          fields: {
            search: {
              type: "text",
              analyzer: "ngram_analyzer",
              search_analyzer: "simple",
              term_vector: "with_positions_offsets",
              store: true
            }
          }
        }
      }
    }
  }
}

export const metaDataConfig = {
  template: "metadata",
  settings: {
    "index.mapping.total_fields.limit": 2048,
    analysis: {
      analyzer: {
        autosuggest_analyzer: {
          filter: ["lowercase", "asciifolding", "autosuggest_filter"],
          tokenizer: "standard",
          type: "custom"
        },
        ngram_analyzer: {
          filter: ["lowercase", "asciifolding", "ngram_filter"],
          tokenizer: "standard",
          type: "custom"
        }
      },
      filter: {
        autosuggest_filter: {
          max_gram: "20",
          min_gram: "1",
          token_chars: ["letter", "digit", "punctuation", "symbol"],
          type: "edge_ngram"
        },
        ngram_filter: {
          max_gram: "9",
          min_gram: "2",
          token_chars: ["letter", "digit", "punctuation", "symbol"],
          type: "ngram"
        }
      }
    }
  },
  mappings: {
    doc: {
      dynamic_templates: [
        {
          strings: {
            match_mapping_type: "string",
            unmatch: "doc_content",
            mapping: {
              type: "text",
              fields: {
                autosuggest: {
                  type: "text",
                  analyzer: "autosuggest_analyzer",
                  search_analyzer: "simple"
                },
                keyword: {
                  type: "keyword",
                  ignore_above: 256
                },
                search: {
                  type: "text",
                  analyzer: "ngram_analyzer",
                  search_analyzer: "simple"
                }
              },
              analyzer: "standard"
            }
          }
        }
      ],
      dynamic: true
    }
  }
}

export default config
