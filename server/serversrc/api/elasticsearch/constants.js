require("dotenv").config()

export const TENANT_DOC_ORGANIZATION = {
  DEALS: "DEALS",
  RFPS: "RFPS",
  BANKLOANS: "BANKLOANS",
  DERIVATIVES: "DERIVATIVES",
  MARFPS: "MARFPS",
  OTHERS: "OTHERS",
  ENTITYDOCS: "ENTITYDOCS"
}

export const TRANSACTION_TYPES_KEYS = [
  "deals",
  "rfps",
  "bankloans",
  "derivatives",
  "marfps"
]

export const ContextTypeForDocumentSearch = {
  "ENTITYDOCS":"Clients / Prospects - Documents",
  "RFPS":"Manage RFPs for Clients",
  "BANKLOANS":"Debt Issue - Bank Loans",
  "MARFPS":"MA RFPs",
  "DERIVATIVES":"Derivatives",
  "OTHERS":"Other Transactions",
  "SVOBLIGATIONS":"Supervisory Compliance Obligations",
  "SVPROFQUALIFICATIONS":"Compliance - Professional Qualifications",
  "SVGENADMIN":"Compliance - General Administration",
  "SVCLIENTCOMPLAINTS":"Compliance - Client Education & Research - Compliants",
  "SVBUSINESSCONDUCT":"Compliance Business Conduct",
  "POLICIESPROCEDURES":"Compliance - Policies & Procedures",
  "SVMSRBSUBMISSIONS" :"Compliance - MSRB Submissions",
  "SVPOLICONTRELATEDDOCS":"Compliance - Political Contributions",
  "SVGIFTSGRATUITIES":"Compliance - Gifts and Gratuities",
  "BILLINGEXPENSERECEIPTS":"Billing & Exepenses Receipts",
  "BUSDEV":"MA Business Development Activities",
  "ENTITYCONTRACT":"Entity Contract",
  "CONTROLS":"Controls",
  "THIRDPARTYDOCS":"Third Party - Documents",
  "GENERALDOCS":"General - Documents",
  "USERDOCS":"User - Documents",
  "TASKS":"Tasks - Documents",
  "ENTITYLOGO":"Firm - Entity Logo",
  "MIGRATIONTOOLS":"Firm - Migration and Onboarding Files"
}


export const AWS_ACCESS_KEY = "AKIAJ3FYOKRTTRMON5FQ"
export const AWS_SECRET = "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"
export const ELASTIC_SEARCH_DOC_INDEX = process.env.ES_INDEX
export const ELASTIC_SEARCH_DOC_URL = process.env.ES_HOST
export const ALLOWED_FILE_TYPES = [
  "doc",
  "docx",
  "ppt",
  "pdf",
  "xls",
  "xlsx",
  "pptx",
  "rtf",
  "odt",
  "ods",
  "txt",
  "csv",
  "sxw",
  "ods",
  "sxc",
  "odp",
  "sxi"
]
