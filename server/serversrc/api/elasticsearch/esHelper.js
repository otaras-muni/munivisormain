import axios from "axios"
import btoa from "btoa"
import { ObjectID } from "mongodb"
import AWS from "aws-sdk"
import _ from "lodash"
import config, { metaDataConfig } from "./config"

import {
  modelLookupConfigForElasticSearch,
  EntityUser,
  Entity
} from "./../../api/appresources/models"
import { getAllDocumentUrlsForTenant } from "./../../api/appresources/docs"
import {
  normalizeAllConfiguredDataForTenant,
  getDataForNormalization
} from "./elasticsearch.controller"
import { deleteDocs, checkMetaIndex } from "./metaIndexHelper"

require("dotenv").config()

const clusterUrl = process.env.ES_HOST
const appUrl = `${process.env.ES_HOST}${process.env.ES_INDEX}`
const indexName = process.env.ES_INDEX
const metaIndex = "metadata"
const metaUrl = `${clusterUrl}${metaIndex}`

export const getHeaders = () => {
  const headers = {
    "Content-Type": "application/json"
  }

  const credentials = process.env.ES_CREDENTIALS

  if (credentials) {
    headers.Authorization = `Basic ${btoa(credentials)}`
  }
  return headers
}

export const initTemplate = async () => {
  try {
    const apiRes = await axios.put(
      `${clusterUrl}_template/tenant_data_template`,
      config,
      { headers: getHeaders() }
    )
    return apiRes.data
  } catch (err) {
    throw err
  }
}

export const initMetaDataTemplate = async () => {
  try {
    const apiRes = await axios.put(
      `${clusterUrl}_template/meta_data_template`,
      metaDataConfig,
      { headers: getHeaders() }
    )
    return apiRes.data
  } catch (err) {
    throw err
  }
}

export const initIndex = async indexName => {
  try {
    const apiRes = await axios.put(`${clusterUrl}${indexName}`, config, {
      headers: getHeaders()
    })
    return apiRes.data
  } catch (err) {
    throw err
  }
}

export const clearIndex = async indexName => {
  try {
    const apiRes = await axios.delete(`${clusterUrl}${indexName}`, {
      headers: getHeaders()
    })
    return apiRes.data
  } catch (err) {
    throw err
  }
}

export const initMetaDataIndex = async indexName => {
  try {
    const apiRes = await axios.put(
      `${clusterUrl}${indexName}`,
      metaDataConfig,
      { headers: getHeaders() }
    )
    return apiRes.data
  } catch (err) {
    throw err
  }
}

export const clearMetaDataIndex = async indexName => {
  try {
    const apiRes = await axios.delete(`${clusterUrl}${indexName}`, {
      headers: getHeaders()
    })
    return apiRes.data
  } catch (err) {
    throw err
  }
}

export const putData = async (schemaName, id, reqestData) => {
  try {
    const tmpData = {
      ...reqestData
    }
    tmpData.tid = tmpData._id
    delete tmpData._id

    const res = await axios.put(
      `${appUrl}/${schemaName}/${id}?refresh=true`,
      tmpData,
      {
        timeout: 60000000,
        headers: getHeaders()
      }
    )
    const data = await res.data
    return data
  } catch (e) {
    return e
  }
}

export const putBulkData = async (schemaName, bulkRequestData, chunkSize) => {
  try {
    const CHUNK_SIZE = chunkSize || 500
    console.log("=> Indexing bulk data")
    console.log("=> Total data", bulkRequestData.length)
    console.log("=> Creating chunks of size", CHUNK_SIZE)
    const chunks = _.chunk(bulkRequestData, CHUNK_SIZE)
    console.log("=> Total chunks", chunks.length)
    const promises = chunks.map(async chunk => {
      const finalElasticSearchLoadingObject = chunk
        .map(
          ({ _id, ...rest }) =>
            `${JSON.stringify({
              index: {
                _id
              }
            })}\n${JSON.stringify({
              ...rest,
              tid: _id
            })}\n`
        )
        .join("")
      const res = await axios.put(
        `${appUrl}/${schemaName}/_bulk`,
        finalElasticSearchLoadingObject,
        {
          timeout: 60000000,
          headers: {
            "Content-Type": "text/plain"
          }
        }
      )
      const data = await res.data
      if (data.errors) {
        return {
          done: false,
          message: `Error inserting / updating rows for ${appUrl}/${schemaName}. Total Documents - ${
            bulkRequestData.length
          }`
        }
      }
      return {
        done: true,
        message: `Successfully inserted / updated rows for ${appUrl}/${schemaName}. Total Documents - ${
          bulkRequestData.length
        }`
      }
    })

    return await Promise.all(promises)
  } catch (e) {
    return e
  }
}

export const deleteData = async (schemaName, id) => {
  try {
    const res = await axios.delete(
      `${appUrl}/${schemaName}/${id}?refresh=true`,
      { headers: getHeaders() }
    )
    const data = await res.data
    return data
  } catch (e) {
    return e
  }
}

export const deleteAllIndexes = async () => {
  try {
    console.log(
      `Deleting all the indexes on the elastic search server - ${clusterUrl}`
    )
    const res = await axios.delete(`${clusterUrl}_all`, {
      headers: getHeaders()
    })
    const data = await res.data
    console.log("After deleting the index", data)
    return data
  } catch (e) {
    return e
  }
}

export const deleteDataInBulkForTenant = async ({ tenantId, ids }) => {
  try {
    const query = {
      query: {
        ids: {
          values: [...ids]
        }
      }
    }
    const res = await axios.put(
      `${clusterUrl}tenant_data_${tenantId}/_delete_by_query`,
      query,
      {
        timeout: 60000000,
        headers: {
          "Content-Type": "text/plain"
        }
      }
    )
    const data = await res.data
    return data
  } catch (e) {
    return e
  }
}

export const getData = async (schemaName, id) => {
  try {
    const res = await axios.get(`${appUrl}/${schemaName}/${id}`, {
      headers: getHeaders()
    })
    const data = await res.data
    return data
  } catch (e) {
    return e
  }
}

export const elasticSearchUpdateFunctionOld = async parameters => {
  const { _id, esType, Model } = parameters
  try {
    const data = await Model.findOne({ _id: ObjectID(_id) })

    if (data) {
      await putData(esType, data._id, data._doc)
      return true
    }
    console.log(`${esType}|${_id} - There were no documents to update`)
    return false
  } catch (e) {
    console.log(`${esType}|${_id} - There were an error updating documents`)
    return false
  }
}

const getModelData = async indexType => {
  const Model = modelLookupConfigForElasticSearch[indexType]
  const dataFromMongo = await Model().find({})
  const dataParsed = dataFromMongo.map(d => d._doc)
  await putBulkData(indexType, dataParsed)
  return {
    [indexType]: (dataFromMongo || []).length
  }
}

export const initandreseeddataOld = async () => {
  try {
    const indexExists = await axios.get(`${appUrl}`, { headers: getHeaders() })
    const indexData = await indexExists.data
    let indexExistsStatus = false
    if (indexData[indexName]) {
      indexExistsStatus = true
      await clearIndex()
      await axios.put(`${appUrl}`, config, { headers: getHeaders() })
    } else {
      return {
        indexExistsStatus: false,
        esReturnValues: "There is a problem please check"
      }
    }
    const schemas = Object.keys(modelLookupConfigForElasticSearch)
    const esReturnValues = await Promise.all(
      schemas.map(sch => {
        console.log(
          `Reading Data from Mongo db collect - ${sch} - and inserting into Elastic Search`
        )
        return getModelData(sch)
      })
    )
    return { indexExistsStatus, esReturnValues }
  } catch (err) {
    const {
      error: { type }
    } = (err && err.response && err.response.data) || {
      error: {
        type: "Unknown Error"
      }
    }
    console.log("There is an error", type)
    if (type === "index_not_found_exception") {
      console.log(
        "There is no index exists with the specified name...going through the intializati" +
          "on route"
      )
      const indexExistsStatus = false
      await axios.put(`${appUrl}`, config, { headers: getHeaders() })
      const schemas = Object.keys(modelLookupConfigForElasticSearch)
      const esReturnValues = await Promise.all(
        schemas.map(sch => {
          console.log(
            `Reading Data from Mongo db collect - ${sch} - and inserting into Elastic Search`
          )
          return getModelData(sch)
        })
      )
      return { indexExistsStatus, esReturnValues }
    }
    return {
      indexExistsStatus: false,
      esReturnValues: "The server is not accessible"
    }
  }
}

export const deleteTags = async fileList => {
  const s3 = new AWS.S3({
    accessKeyId: process.env.S3ACCESSKEYID,
    secretAccessKey: process.env.S3SECRETKEY
  })
  const bucketName = process.env.S3BUCKET

  const promises = fileList.map(
    ({ fileName, versionId, fileObjectPath }) =>
      new Promise(resolve => {
        s3.deleteObjectTagging(
          {
            Bucket: bucketName,
            Key: fileObjectPath,
            VersionId: versionId
          },
          (err, data) => {
            console.log(fileName)
            console.log(data)
            resolve({
              ...data,
              fileDeleted: fileName,
              fileObjectPath
            })
          }
        )
      })
  )

  return Promise.all(promises)
}

export const deleteTagsForTenant = async tenantId => {
  try {
    const docsToDeleteTag = await getAllDocumentUrlsForTenant(tenantId)
    console.log(
      "The Number of Files For which tags will be reset",
      docsToDeleteTag
    )
    const retData = await deleteTags(docsToDeleteTag)
    return retData
  } catch (e) {
    console.log("There is an error in return data", e)
    return "There is a problem deleting tags."
  }
}

export const setupTenantIndex = async tenantId => {
  try {
    const dataIndex = `tenant_data_${tenantId}`
    const docIndex = `tenant_docs_${tenantId}`

    const dataIndexResponse = await initIndex(dataIndex)
    const docIndexResponse = await initIndex(docIndex)

    return { dataIndexResponse, docIndexResponse }
  } catch (err) {
    console.error(
      "The error Response",
      JSON.stringify(err && err.response && err.response.data, null, 2)
    )
    throw err
  }
}

export const deleteTenantIndex = async tenantId => {
  try {
    const dataIndex = `tenant_data_${tenantId}`
    const docIndex = `tenant_docs_${tenantId}`

    const dataIndexResponse = await clearIndex(dataIndex)
    const docIndexResponse = await clearIndex(docIndex)

    return { dataIndexResponse, docIndexResponse }
  } catch (err) {
    console.error(
      "The error Response",
      err && err.response && err.response.data
    )
    throw err
  }
}

export const createTenantIndex = async tenantId => {
  try {
    const createRes = await setupTenantIndex(tenantId)
    console.log("Create Index Res", createRes)
  } catch (err) {
    console.error("Err", err)
    console.error(
      "The error Response",
      err && err.response && err.response.data
    )
  }
}

export const putBulkDataRevised = async (
  tenantId,
  bulkRequestData,
  chunkSize
) => {
  try {
    const CHUNK_SIZE = chunkSize || 500
    console.log("=> Bulk indexing data for", tenantId)
    console.log("=> Total data", bulkRequestData.length)
    console.log("=> Creating chunks of size", CHUNK_SIZE)
    const chunks = _.chunk(bulkRequestData, CHUNK_SIZE)
    console.log("=> Total chunks", chunks.length)
    const promises = chunks.map(async chunk => {
      const finalElasticSearchLoadingObject = chunk
        .map(
          ({ _id, ...rest }) =>
            `${JSON.stringify({
              index: {
                _id
              }
            })}\n${JSON.stringify({
              ...rest
            })}\n`
        )
        .join("")

      // console.log(   "The final Data to be inserted is",
      // finalElasticSearchLoadingObject )
      const res = await axios.put(
        `${clusterUrl}tenant_data_${tenantId}/doc/_bulk`,
        finalElasticSearchLoadingObject,
        {
          timeout: 60000000,
          headers: {
            "Content-Type": "text/plain"
          }
        }
      )
      const data = await res.data
      if (data.errors) {
        return {
          done: false,
          message: `Error inserting / updating rows for ${clusterUrl}tenant_data_${tenantId}. Total Documents - ${
            chunk.length
          }`
        }
      }
      return {
        done: true,
        message: `Successfully inserted / updated rows for ${clusterUrl}tenant_data_${tenantId}. Total Documents - ${
          chunk.length
        }`
      }
    })

    return await Promise.all(promises)
  } catch (e) {
    return e
  }
}
export const indexExistsPinger = async indexToBeChecked => {
  try {
    const indexExists = await axios.get(`${clusterUrl}${indexToBeChecked}`, {
      headers: getHeaders()
    })
    const indexData = await indexExists.data

    if (indexData[indexToBeChecked]) {
      console.log(`Index - ${indexToBeChecked} exists`)
      return {index:indexToBeChecked,status:true}
    }
  } catch (err) {
    const {
      error: { type }
    } = (err && err.response && err.response.data) || {
      error: {
        type: "Unknown Error"
      }
    }
    console.log("There is an error", type)
    if (type === "index_not_found_exception") {
      return {index:indexToBeChecked,status:false}
    } else if (type === "index_already_exists_exception") {
      return {index:indexToBeChecked,status:true}
    }
  }
}
export const checkExistanceOfIndex = async indexToBeChecked => {
  try {
    const indexExists = await axios.get(`${clusterUrl}${indexToBeChecked}`, {
      headers: getHeaders()
    })
    const indexData = await indexExists.data

    if (indexData[indexToBeChecked]) {
      console.log(`Tenant Index - ${indexToBeChecked} exists`)
      await clearIndex(indexToBeChecked)
      await initIndex(indexToBeChecked)
      return `Tenant Index - ${indexToBeChecked} exists`
    }
  } catch (err) {
    const {
      error: { type }
    } = (err && err.response && err.response.data) || {
      error: {
        type: "Unknown Error"
      }
    }
    console.log("There is an error", type)
    if (type === "index_not_found_exception") {
      console.log(`Tenant Index - ${indexToBeChecked} created`)
      await initIndex(indexToBeChecked)
      return `Tenant Index - ${indexToBeChecked} created`
    } else if (type === "index_already_exists_exception") {
      console.log(`Tenant Index - ${indexToBeChecked} already exists`)
      await clearIndex(indexToBeChecked)
      await initIndex(indexToBeChecked)
      return `Tenant Index - ${indexToBeChecked} already exists`
    }
  }
}

export const checkExistanceOfMetaDataIndexAndCreate = async indexToBeChecked => {
  try {
    const indexExists = await axios.get(`${clusterUrl}${indexToBeChecked}`, {
      headers: getHeaders()
    })
    const indexData = await indexExists.data

    if (indexData[indexToBeChecked]) {
      console.log(`Data Exists in  - ${indexToBeChecked} exists`)
      await clearMetaDataIndex(indexToBeChecked)
      await initMetaDataIndex(indexToBeChecked)
      return `Meta Data Index - ${indexToBeChecked} exists`
    }
  } catch (err) {
    const {
      error: { type }
    } = (err && err.response && err.response.data) || {
      error: {
        type: "Unknown Error"
      }
    }
    console.log("There is an error", type)
    if (type === "index_not_found_exception") {
      console.log(`Meta Data Index - ${indexToBeChecked} does not exist`)
      await initMetaDataIndex(indexToBeChecked)
      console.log(`Metadata Index - ${indexToBeChecked} has been created`)
      return `Metadata Index - ${indexToBeChecked} has been created`
    } else if (type === "index_already_exists_exception") {
      console.log(`Meta Index - ${indexToBeChecked} already exists`)
      await clearMetaDataIndex(indexToBeChecked)
      await initMetaDataIndex(indexToBeChecked)
      return `Metadata Index - ${indexToBeChecked} already exists`
    }
  }
}

export const initAndReseedDataOnlyForTenantOld = async tenantId => {
  try {
    const indexExists = await axios.get(
      `${clusterUrl}tenant_data_${tenantId}`,
      { headers: getHeaders() }
    )
    const indexData = await indexExists.data

    // console.log("The data in the index are as follows",
    // JSON.stringify(indexData,null,2))
    let indexExistsStatus = false
    if (indexData[`tenant_data_${tenantId}`]) {
      indexExistsStatus = true
      await deleteTenantIndex(tenantId)
      await createTenantIndex(tenantId)
    } else {
      return {
        tenantId,
        indexExistsStatus: false,
        esReturnValues: "There is a problem please check"
      }
    }
    const esReturnValues = await normalizeAllConfiguredDataForTenant(tenantId)
    return { tenantId, indexExistsStatus, esReturnValues }
  } catch (err) {
    const {
      error: { type }
    } = (err && err.response && err.response.data) || {
      error: {
        type: "Unknown Error"
      }
    }
    console.log("There is an error", type)
    if (type === "index_not_found_exception") {
      console.log(
        "There is no index exists with the specified name...going through the intializati" +
          "on route"
      )
      await createTenantIndex(tenantId)
      const indexExistsStatus = false
      const esReturnValues = await normalizeAllConfiguredDataForTenant(tenantId)
      return { indexExistsStatus, esReturnValues }
    }
    return {
      tenantId,
      indexExistsStatus: false,
      esReturnValues: "The server is not accessible"
    }
  }
}

export const initAndReseedDataOnlyForTenant = async tenantId => {
  try {
    const dataIndex = `tenant_data_${tenantId}`
    const docsIndex = `tenant_docs_${tenantId}`

    const dataIndexRes = await checkExistanceOfIndex(dataIndex)
    const docsIndexRes = await checkExistanceOfIndex(docsIndex)

    const esReturnValues = await normalizeAllConfiguredDataForTenant(tenantId)
    return {
      tenantId,
      response: {
        dataIndex: dataIndexRes,
        docsIndex: docsIndexRes
      },
      esReturnValues
    }
  } catch (err) {
    console.log("There is an Error", err)
  }
}

export const initAndReseedDataForAllTenants = async tenantIds => {
  try {
    // await deleteAllIndexes()
    let tids = []
    if (!tenantIds) {
      const tenants = await Entity.find({ isMuniVisorClient: true }).select({
        _id: 1
      })
      tids = [...tenants.map(t => t._id)]
    } else {
      tids = tenantIds
    }

    const { success: metaIndexExists } = await checkMetaIndex()
    let deleteMetaDocsRes = null
    if (metaIndexExists) {
      deleteMetaDocsRes = await deleteDocs({
        tenantIds: tids
      })
      console.log("=>deleted docs", deleteMetaDocsRes)
    } else {
      const returnVal = await checkExistanceOfMetaDataIndexAndCreate("metadata")
      console.log(returnVal)
    }

    const allTenantIndexReseedData = await Promise.all(
      tids.map(t => initAndReseedDataOnlyForTenant(t))
    )
    return allTenantIndexReseedData
  } catch (e) {
    console.log("There are errors", e)
  }
}

export const putMongoData = async ({ tenantId, id, requestData, category }) => {
  try {
    const res = await axios.put(
      `${clusterUrl}tenant_data_${tenantId}/doc/${id}?refresh=true`,
      {
        tenantId,
        category: category || "Catch All",
        data: requestData
      },
      {
        timeout: 60000000,
        headers: getHeaders()
      }
    )
    const data = await res.data
    return { done: true, data }
  } catch (e) {
    return { done: false, data: {}, error: e }
  }
}

export const elasticSearchNormalizationAndBulkInsert = async parameters => {
  // console.log("ELASTIC SEARCH UPDATE : ", JSON.stringify(parameters, null, 4))
  const { ids, esType, tenantId } = parameters
  // Get the correct ID for normalization

  const idsForNormalization = (ids || []).map(id => ObjectID(id))
  // if (esType === "entityusers") {   const entityDetails = await
  // EntityUser.aggregate([     {$match:{_id:{$in:idsForNormalization}}},
  // {$group:{       _id:{entityId:"$entityId"}     }},     {
  // $project:{entityId:"$_id.entityId"}     }   ])   idsForNormalization =
  // entityDetails.map( ({entityId}) => ObjectID(entityId)) }

  const mappingCategory = {
    deals: "Deals",
    rfps: "Manage Rfps",
    mvtasks: "Task Info",
    marfps: "MA RFPs",
    busDev: "Business Development Activities",
    others: "Other Transactions",
    bankloans: "Bank Loans",
    derivatives: "Derivatives",
    entityusers: "User Details",
    entities: "Entity Details"
  }

  // Get the correct ID for normalization - this is the normalizer

  const normalizer = {
    deals: () => getDataForNormalization("DEALS", idsForNormalization),
    rfps: () => getDataForNormalization("RFPS", idsForNormalization),
    mvtasks: () => getDataForNormalization("TASKSINFO", idsForNormalization),
    marfps: () => getDataForNormalization("MARFPS", idsForNormalization),
    busDev: () => getDataForNormalization("BUSDEV", idsForNormalization),
    others: () => getDataForNormalization("OTHERS", idsForNormalization),
    bankloans: () => getDataForNormalization("BANKLOANS", idsForNormalization),
    derivatives: () =>
      getDataForNormalization("DERIVATIVES", idsForNormalization),
    entityusers: () => getDataForNormalization("USERINFO", idsForNormalization),
    entities: () => getDataForNormalization("ENTITYINFO", idsForNormalization)
  }

  const mappedCategory = mappingCategory[esType]
  try {
    const functionToRun = normalizer[esType]
    const normalizedData = await functionToRun()
    const allDocsToBeInserted = normalizedData.map(l => ({
      tenantId,
      _id: l._id,
      category: mappedCategory,
      data: {
        ...l
      }
    }))
    if (allDocsToBeInserted && allDocsToBeInserted.length > 0) {
      const esInsert = await putBulkDataRevised(tenantId, allDocsToBeInserted)
      /* console.log(
        "This is what was inserted into the index",
        JSON.stringify(esInsert, null, 2)
      ) */
      return {
        done: true,
        message: `Successfully normalized and pushed ${mappedCategory} into the the tenant data index`,
        data: esInsert
      }
    }
    return {
      done: true,
      message: `No data to be inserted for ${mappedCategory} into the the tenant data index`,
      data: []
    }
  } catch (e) {
    return {
      done: false,
      message: `Successfully normalized and pushed ${mappedCategory} into the the tenant data index`,
      data: {}
    }
  }
}

export const elasticSearchUpdateFunction = async parameters => {
  // console.log("ELASTIC SEARCH UPDATE : ", JSON.stringify(parameters, null, 4))
  const { _id, esType, tenantId } = parameters
  // Get the correct ID for normalization

  const idForNomralization = _id
  // if (esType === "entityusers") {   const entityDetails = await
  // EntityUser.findOne({ _id }).select({     entityId: 1   })
  // idForNomralization = entityDetails.entityId }

  const mappingCategory = {
    tranagencydeals: "Deals",
    tranagencyrfps: "Manage Rfps",
    mvtasks: "Task Info",
    actmarfps: "MA RFPs",
    actbusdevs: "Business Development Activities",
    tranagencyothers: "Other Transactions",
    tranbankloans: "Bank Loans",
    tranderivatives: "Derivatives",
    entityusers: "User Details",
    entities: "Entity Details"
  }

  // Get the correct ID for normalization - this is the normalizer
  const normalizer = {
    tranagencydeals: () =>
      getDataForNormalization("DEALS", [idForNomralization]),
    tranagencyrfps: () => getDataForNormalization("RFPS", [idForNomralization]),
    mvtasks: () => getDataForNormalization("TASKSINFO", [idForNomralization]),
    actmarfps: () => getDataForNormalization("MARFPS", [idForNomralization]),
    actbusdevs: () => getDataForNormalization("BUSDEV", [idForNomralization]),
    tranagencyothers: () =>
      getDataForNormalization("OTHERS", [idForNomralization]),
    tranbankloans: () =>
      getDataForNormalization("BANKLOANS", [idForNomralization]),
    tranderivatives: () =>
      getDataForNormalization("DERIVATIVES", [idForNomralization]),
    entityusers: () =>
      getDataForNormalization("USERINFO", [idForNomralization]),
    entities: () => getDataForNormalization("ENTITYINFO", [idForNomralization])
  }

  const mappedCategory = mappingCategory[esType]
  try {
    const functionToRun = normalizer[esType]
    const normalizedData = await functionToRun()
    if (normalizedData.length > 0) {
      const putParameters = {
        tenantId,
        id: idForNomralization,
        requestData: normalizedData[0],
        category: mappedCategory
      }
      const data = await putMongoData(putParameters)
      if (data.done) {
        console.log(
          `${esType}|${mappedCategory}|${idForNomralization} - document successfully updated`,
          data
        )
        return true
      }
      console.log(
        `${esType}|${mappedCategory}|${idForNomralization} - document - failed to update document into the index`,
        data
      )
      return false
    }
    console.log(
      `${esType}|${mappedCategory}|${idForNomralization} - There were no documents to update`
    )
    return false
  } catch (e) {
    console.log(
      `${esType}|${mappedCategory}|${idForNomralization} - There were an error updating documents`
    )
    return false
  }
}
