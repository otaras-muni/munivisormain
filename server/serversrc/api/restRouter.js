import express from "express"
import {transactionRouter} from "./appresources/transactions"
import {testRouter} from "./appresources/test"
import {configRouter} from "./appresources/config"
import {auditLogRouter} from "./appresources/auditlog"
import {personRouter} from "./appresources/testsubdocuments"
import {rfpRouter} from "./appresources/rfp"
import {docsRouter} from "./appresources/docs"
import {docFolderRouter} from "./appresources/docfolder"
import {s3Router} from "./appresources/integrations"
import {esRouter} from "./elasticsearch"
import {usersRouter} from "./appresources/users"
import {entityRouter} from "./appresources/entity"
import {entityResRouter} from "./appresources/entityRel"
import {entityUserRouter} from "./appresources/entityUser"
import {dealsRouter} from "./appresources/deals"
import {firmRouter} from "./appresources/firm"
import {tasksRouter} from "./appresources/taskmanagement"
import {bankLoanRouter, derivativesRouter, marfpsRouter, othersRouter, busdevRouter} from "./appresources/activities"
import {
  businessConductRouter,
  generalAdminRouter,
  profQualificationsRouter,
  poliContributionsRouter,
  superVisoryObligationsRouter,
  supervisorRouter,
  complaintDetailsRouter,
  giftsGratuitiesRouter,
  poliContributionSummaryRouter,
  clientEducationG10Router
} from "./appresources/compliance"
import {searchPrefRouter} from "./appresources/search"
import {billingExpenseRouter, billingRouter} from "./appresources/billing"
import {controlsRouter, controlsActionsRouter} from "./appresources/cac"
import {notificationsRouter} from "./appresources/notification"
import {messagesRouter} from "./appresources/messages"
import {sendEmailRouter} from "./appresources/emails"
import {entitlementRouter} from "./appresources/entitlements"
import {commonServicesRouter} from "./appresources/commonservices"
import {dashboardRouter} from "./appresources/dashboardsearch"
import {dataMigrationRouter} from "./appresources/datamigration"
import {pdfGeneratorRouter} from "./appresources/pdfs"
import {excelGeneratorRouter} from "./appresources/excel"
import {securityRouter} from "./appresources/security"
import {onBoardinTenantRouter} from "./appresources/tenantonboarding"
import {dmMappingRouter} from "./appresources/dmmapping"
import {plateFormAdminRouter} from "./appresources/plateformadmin/dashboard"
import {createRouter, plateformEntityRouter} from "./appresources/plateformadmin/create"
import {commonservicesRouter} from "./appresources/plateformadmin/commonservices"

import {apiErrorHandler} from "./modules/errorHandler"
import docAuthMiddleware from "../middlewares/docInfoAuth"
export const restRouter = express.Router()
export const authorizationRouter = express.Router()

// The authorization routes go here Api Routs
restRouter.use("/transaction", transactionRouter)
restRouter.use("/adminfirm", firmRouter)
restRouter.use("/test", testRouter)
restRouter.use("/configs", configRouter)
restRouter.use("/auditlog", auditLogRouter)
restRouter.use("/person", personRouter)
restRouter.use("/rfp", rfpRouter)
restRouter.use("/docs", docsRouter)
restRouter.use("/docfolder", docFolderRouter)
restRouter.use("/s3", s3Router)
restRouter.use("/es", [docAuthMiddleware], esRouter)
restRouter.use("/users", usersRouter)
restRouter.use("/entity", entityRouter)
restRouter.use("/entityRel", entityResRouter)
restRouter.use("/entityUser", entityUserRouter)
restRouter.use("/deals", dealsRouter)
restRouter.use("/tasks", tasksRouter)
restRouter.use("/bankloans", bankLoanRouter)
restRouter.use("/searchpref", searchPrefRouter)
restRouter.use("/derivatives", derivativesRouter)
restRouter.use("/marfps", marfpsRouter)
restRouter.use("/others", othersRouter)
restRouter.use("/busdev", busdevRouter)
restRouter.use("/businessconduct", businessConductRouter)
restRouter.use("/generaladmin", generalAdminRouter)
restRouter.use("/profqualifications", profQualificationsRouter)
restRouter.use("/policontributions", poliContributionsRouter)
restRouter.use("/policontriSummary", poliContributionSummaryRouter)
restRouter.use("/giftsgratuities", giftsGratuitiesRouter)
restRouter.use("/svandobligations", superVisoryObligationsRouter)
restRouter.use("/supervisor", supervisorRouter)
restRouter.use("/svcomplaintdetails", complaintDetailsRouter)
restRouter.use("/clientEducationG10", clientEducationG10Router)
restRouter.use("/billing", billingRouter)
restRouter.use("/billingmanageExpense", billingExpenseRouter)
restRouter.use("/controls", controlsRouter)
restRouter.use("/notifications", notificationsRouter)
restRouter.use("/messages", messagesRouter)
restRouter.use("/controls-actions", controlsActionsRouter)
restRouter.use("/emails", sendEmailRouter)
restRouter.use("/entitlements", entitlementRouter)
restRouter.use("/common", commonServicesRouter)
restRouter.use("/dashboard", dashboardRouter)
restRouter.use("/migration", dataMigrationRouter)
restRouter.use("/pdfs", pdfGeneratorRouter)
restRouter.use("/excel", excelGeneratorRouter)
restRouter.use("/security", securityRouter)
restRouter.use("/tenantonboarding",[docAuthMiddleware], onBoardinTenantRouter)
restRouter.use("/dmMapping", dmMappingRouter)


// Plate form admin services
restRouter.use("/plateformadmin", plateFormAdminRouter)
restRouter.use("/plateformadmin/entity", plateFormAdminRouter)
restRouter.use("/plateformadmin/create", createRouter)
restRouter.use("/plateformadmincommon", commonservicesRouter)

// This will have all the default error handling information
restRouter.use(apiErrorHandler)
