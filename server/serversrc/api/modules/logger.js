const { createLogger, format, transports } = require("winston")
const DailyRotateFile = require("winston-daily-rotate-file")

const fs = require("fs")
const path = require("path")

const env = process.env.NODE_ENV || "development"
const logDir = "logs"

// Create the log directory if it does not exist

/*
const loggingOptions = {
  file: {
    level: "info",
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
  },
  console: {
    level: "debug",
    handleExceptions: true,
    json: false,
    colorize: true,
  },
}
*/

// Rotate the log files daily 
const dailyRotateCombinedLogFile = {
  level:process.env.LOG_LEVEL || "info",
  filename: `${logDir}/%DATE%-combined.log`,
  datePattern: "YYYY-MM-DD",
  zippedArchive:true,
  maxSize: "20m",
  handleExceptions: true,
  maxFiles:"15d"
}

const dailyRotateErrorLogFile = {
  level:process.env.LOG_LEVEL || "info",
  filename: `${logDir}/%DATE%-error.log`,
  datePattern: "YYYY-MM",
  zippedArchive:true,
  handleExceptions: true,
  maxFiles:"15d",
  maxSize: "20m",
}

// If Production ensure that you log information based on the LOG_LEVEL

let logger 

if (process.env.NODE_ENV === "production") {
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir)
  }
  logger = createLogger({
    level: "info",
    format: format.combine(
  
      // Use this function to create a label for additional text to display
      format.label({ label: path.basename(module.parent.filename) }),
      format.colorize(),
      format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
      format.printf(
        // We display the label text between square brackets using ${info.label} on the next line
        info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
      )
    ),
    transports: [
      new transports.Console({
        level: process.env.LOG_LEVEL || "info",     
        handleExceptions: true,
      }),
      new DailyRotateFile(dailyRotateCombinedLogFile),
      new DailyRotateFile(dailyRotateErrorLogFile),
    ],
    exitOnError: false, // do not exit on handled exceptions
  })
} else {
  logger = createLogger({
    level: "debug",
    format: format.combine(
  
      // Use this function to create a label for additional text to display
      format.label({ label: path.basename(module.parent.filename) }),
      format.colorize(),
      format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
      format.printf(
        // We display the label text between square brackets using ${info.label} on the next line
        info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
      )
    ),
    transports: [
      new transports.Console({
        level: process.env.LOG_LEVEL || "debug",     
        handleExceptions: true,
      }),
    ],
    exitOnError: false, // do not exit on handled exceptions
  })
}

logger.stream = {
  write(message, encoding) {
    logger.info(message)
  },
}


const exportableLogger = logger

export default exportableLogger
