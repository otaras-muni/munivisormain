import  AWS from "aws-sdk"
import dotenv from "dotenv"

const nodemailer = require("nodemailer")

dotenv.config()


/* SESREGION=us-east-1
SESACCESSKEYID=AKIAI4WVM6IDL4B6T7LQ
SESSECRETKEY=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
*/

console.log("SESACCESSKEYID",process.env.SESACCESSKEYID)
console.log("SESSECRETKEY",process.env.SESACCESSKEYID)


AWS.config.update({region: process.env.SESREGION,
  accessKeyId:process.env.SESACCESSKEYID,
  secretAccessKey:process.env.SESSECRETKEY
})

const templateEnv = process.env.NODE_ENV === "development" ? "DEV_" : ""

// Create sendEmail params
export const sendAWSEmail = async (emailConfig) => {

  console.log(JSON.stringify(emailConfig, null,2))

  const {to,cc,bodytemplate,subject,replyto} = emailConfig
  const params = {
    Destination: { /* required */
      CcAddresses: [...cc],
      ToAddresses: [...to]
    },
    Message: { /* required */
      Body: { /* required */
        Html: {
          Charset: "UTF-8",
          Data: bodytemplate
        },
        Text: {
          Charset: "UTF-8",
          Data: "test email"
        }
      },
      Subject: {
        Charset: "UTF-8",
        Data: subject
      }
    },
    Source: process.env.REPLYTOEMAIL, /* required */
    ReplyToAddresses: [...replyto],
  }

  // Create the promise and SES service object
  const sendPromise = new AWS.SES({apiVersion: "2010-12-01"}).sendEmail(params).promise()

  // Handle promise's fulfilled/rejected states
  // sendPromise.then(
  //   (data) => {
  //     console.log(data.MessageId)
  //   }).catch(
  //   (err) => {
  //     console.error(err, err.stack)
  //   })
  let returnStatus
  try {
    const data = await sendPromise
    returnStatus = {status:"success",error:"", data}
    console.log("RETURN STATUS AFTER SENDING EMAILS:", returnStatus)
  } catch (e) {
    console.log(e)
    returnStatus = {status:"fail",error:"Error Sending email"}
  }

  return returnStatus
}

export const sendSESEmail = async (emailConfig, templateInfo) => {

  const {to,cc,replyto} = emailConfig
  const {templateData, templateName} = templateInfo

  const params = {
    // Destinations: [ /* required */
    //   {
        Destination: { /* required */
          CcAddresses: [...cc],
          ToAddresses: [...to]
        },
    //     ReplacementTemplateData: JSON.stringify(templateData)
    //   }
    // ],
    Source: process.env.REPLYTOEMAIL, /* required */
    Template:templateEnv+templateName, /* required */
    TemplateData: JSON.stringify(templateData),
    ReplyToAddresses: [...replyto]
  }
  console.log(params)
  // Create the promise and SES service object
  const sendPromise = new AWS.SES({apiVersion: "2010-12-01"}).sendTemplatedEmail(params).promise()
  let returnStatus
  try {
    const data = await sendPromise
    returnStatus = {status:"success",error:"", data}
    console.log("RETURN STATUS AFTER SENDING EMAILS:", returnStatus)
  } catch (e) {
    console.log(e)
    returnStatus = {status:"fail",error:"Error Sending email"}
  }

  return returnStatus
}

export const sendAWSEmailWithAttachment = async (emailConfig) => {
  const {to,cc,bodytemplate,subject,replyto, attachments} = emailConfig
  const transporter = nodemailer.createTransport({
    SES: new AWS.SES({
      apiVersion: "2010-12-01"
    })
  })
  let returnStatus
  console.log("attachments : ", JSON.stringify(attachments))
  try {
    const data = await transporter.sendMail({
      from: replyto,
      subject,
      to,
      cc,
      html: bodytemplate,
      attachments: attachments.map(a => ({
        filename: a.filename,
        content: Buffer.from(a.content.data)
      }))
    })
    returnStatus = {status:"success",error:"", data}
    console.log("RETURN STATUS AFTER SENDING EMAILS:", returnStatus)
  } catch (err) {
    console.log("err in sendAWSEmailWithAttachment : ", err)
    returnStatus = {status:"fail",error:"Error Sending email"}
  }
}
