import Joi from "joi-browser"
import dateFormat from "dateformat"

const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
const urlRegex = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/
const officePhone = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  countryCode: Joi.string().allow("").label("Country Code").optional(),
  phoneNumber: Joi.string().allow("").label("Phone Number").optional(),
  extension: Joi.string().allow("").label("Extension").optional()
})

const officeFax = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  faxNumber: Joi.string().allow("").label("Fax Number").optional()
})

const officeEmails = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  emailId: Joi.string().email().allow("").label("Email Id").optional()
})

const entityAddressSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  addressName: Joi.string().allow("").label("Address Name").optional(),
  addressType: Joi.string().allow("").label("Address Type").optional(),
  isPrimary: Joi.boolean().required().label("Is Primary").optional(),
  isHeadQuarter: Joi.boolean().required().label("Is HeadQuarter").optional(),
  /* isActive: Joi.boolean().required().label("Is Active").optional(), */
  website: Joi.string().allow("").label("WebSite").optional(),
  officePhone: Joi.array().items(officePhone),
  officeFax: Joi.array().items(officeFax),
  officeEmails: Joi.array().items(officeEmails),
  addressLine1: Joi.string().label("Address Line 1").optional(),
  addressLine2: Joi.string().allow("").label("Address Line 2").optional(),
  country: Joi.string().label("Country").optional(),
  county: Joi.string().label("County").optional(),
  state: Joi.string().label("State").optional(),
  city: Joi.string().label("City").optional(),
  zipCode: {
    _id: Joi.string().allow("").optional(),
    zip1: Joi.string().allow("").label("Zip Code 1").optional(),
    zip2: Joi.string().allow("").label("Zip Code 2").optional()
  },
  formatted_address: Joi.string().allow("").label("Formatted Address").optional(),
  url: Joi.string().allow("").label("URL").optional(),
  location: {
    _id: Joi.string().allow("").optional(),
    longitude: Joi.string().allow("").label("Longitude").optional(),
    latitude: Joi.string().allow("").label("Latitude").optional()
  }
})

const marketRole = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  label: Joi.string().label("Market Role").optional(),
  value: Joi.string().label("Market Role").optional()
})

const entityFlagSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  marketRole: Joi.array().items(marketRole).required().label("Market Role").optional(),
  issuerFlags: Joi.array().items(Joi.string().required().optional()).min(0).unique().required().label("Issuer Flag").optional()
})

const entityLinkCusipSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  debtType: Joi.string().allow("").label("Debt Type").optional(),
  associatedCusip6: Joi.string().uppercase().min(6).allow("").label("Associated").optional(),
})

const entityLinkBorrowersObligorsSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  borOblRel: Joi.string().required().label("Borrower Relationship").optional(),
  borOblFirmName: Joi.string().required().label("Borrower Firm Name").optional(),
  borOblDebtType: Joi.string().required().label("Borrower Debt Type").optional(),
  borOblCusip6: Joi.string().required().label("Borrower Link Cusip").optional(),
  borOblStartDate: Joi.date().example(new Date("2005-01-01")).required().label("Start Date").optional(),
  borOblEndDate: Joi.date().example(new Date("2016-01-01")).min(Joi.ref("borOblStartDate")).required().label("End Date").optional(), // switched order of min and example

})

// eslint-disable-next-line no-unused-vars
const firmDetailSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  entityFlags: entityFlagSchema,
  isMuniVisorClient: Joi.boolean().required().optional(),
  msrbFirmName: Joi.string().allow("").label("MSRB Firm Name").optional(),
  msrbRegistrantType: Joi.string().allow("").label("MSRB Registrant Type").optional(),
  msrbId: Joi.string().allow("").label("MSRB Id").optional(),
  entityAliases: Joi.array().items(Joi.string().label("Aliases").optional()).optional(),
  entityType: Joi.string().required().label("Entity Type").optional(),
  firmName: Joi.string().required().label("Firm Name").optional(),
  firmType: Joi.string().allow("").label("Firm Type").optional(),
  taxId: Joi.string().allow("").label("Tax Id").optional(),
  primarySectors: Joi.string().allow("").label("Primary Sectors").optional(),
  secondarySectors: Joi.string().allow("").label("Secondary Sectors").optional(),
  firmLeadAdvisor: Joi.string().allow("").label("Firm Lead Advisor").optional(),
  prevLeadAdvisor: Joi.string().allow([null, ""]).label("Previous Lead Advisor").optional(),
  prevAdvisorFirm: Joi.string().allow("").label("Previous Advisor Firm").optional(),
  prevAdvisorContractExpire: Joi.string().allow("").label("Previous Advisor Contract Expire").optional(),
  primaryContactNameInEmma: Joi.string().allow("").label("Primary Contact Name In Emma").optional(),
  primaryContactPhone: Joi.string().allow("").label("Primary Contact Phone").optional(),
  primaryContactEmail: Joi.string().allow("").required().label("Primary Contact Email").optional(),
  businessStructure: Joi.string().required().label("Business Structure").optional(),
  numEmployees: Joi.string().allow("").label("Number Of Employees").optional(),
  annualRevenue: Joi.string().allow("").label("Annual Revenue").optional(),
  addresses: Joi.array().items(entityAddressSchema).optional(),
  firmAddOns: Joi.array().items(Joi.string()).optional(),
  entityLinkedCusips: Joi.array().items(entityLinkCusipSchema).optional(),
  entityBorObl: Joi.array().items(entityLinkBorrowersObligorsSchema).required().optional(),
  __v: Joi.number().integer().optional()
})

export const validatClientsDetail = (inputFirmDetail) => {
  const result = Joi.validate(inputFirmDetail, firmDetailSchema, { convert: true, abortEarly: false, stripUnknown: false })
  return result
}
