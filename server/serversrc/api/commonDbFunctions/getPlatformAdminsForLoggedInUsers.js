import {ObjectID} from "mongodb"
import  {  EntityRel }  from "./../appresources/models"

export const getSupervisorDetails = async(user) => {
  const a = await EntityRel.aggregate([
    {$match:{entityParty2:ObjectID(user.entityId)}},
    {
      $lookup:{
        from: "entityrels",
        localField: "entityParty1",
        foreignField: "entityParty1",
        as: "tenantdetails"
      }
    },
    {
      $unwind:"$tenantdetails"
    },
    {
      $match:{"tenantdetails.relationshipType":"Self"}
    },
    {
      $project:{
        tenantId:"$tenantdetails.entityParty1"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "tenantId",
        foreignField: "entityId",
        as: "tenantusers"
      }
    },
    {
      $unwind:"$tenantusers"
    },
    {
      $project:{
        userFirstName:"$tenantusers.userFirstName",
        userLastName:"$tenantusers.userLastName",
        userFlags:"$tenantusers.userFlags",
        userEmails:"$tenantusers.userEmails"
      }
    },
    {
      $match:{
        "userFlags": {
          $elemMatch:{
            $in: ["Supervisory Principal", "Compliance Officer"],
            $exists:true
          }
        }
      }
    },
    {
      $unwind:"$userEmails"
    },
    {
      $match:{"userEmails.emailPrimary":true}
    },
    {
      $project:{
        userFirstName:1,
        userLastName:1,
        userFlags:1,
        emailId:"$userEmails.emailId"
      }
    }
  ])
  return a
}
