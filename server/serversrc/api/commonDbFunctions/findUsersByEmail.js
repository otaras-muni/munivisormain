import {
  EntityUser
} from "./../appresources/entityUser/entityUser.model"

import { tenantUrlGenerator } from "./../appresources/commonservices/urlGeneratorForTenant"


export const getMappedFinAdvisorForLoggedInNonFAUser = async (email) => {
  let returnObject

  const regexemail = new RegExp(email,"i")

  try{
    returnObject = await EntityUser.aggregate([{
      $match: {
        "userLoginCredentials.userEmailId":  {$regex:regexemail,$options:"i"}
      }
    },
    {
      $project: {
        "userId":"$_id",
        "entityId":1,
        "userFirstName":1,
        "userLastName":1,
        "loginEmailId":{$toLower:"$userLoginCredentials.userEmailId"},
      }
    },
    {
      $lookup: {
        "from": "entities",
        "localField": "entityId",
        "foreignField": "_id",
        "as": "entityInfo"
      }
    },
    { $unwind : "$entityInfo"}, 
    
    {
      $project: {
        "userId":1,
        "entityId":1,
        "userFirstName":1,
        "userLastName":1,
        "loginEmailId":1,
        "entityInfo.userEntityId":"$entityId",
        "entityInfo.msrbName":"$entityInfo.msrbFirmName",
        "entityInfo.msrbType":"$entityInfo.msrbRegistrantType",
        "entityInfo.firmName":"$entityInfo.firmName",
        "entityInfo.munivisorClient":{$ifNull:["$entityInfo.isMuniVisorClient",false]},
        "entityInfo.settings":"$entityInfo.settings"
      }

    },
    {
      $match: {
        "entityInfo.munivisorClient": false
      }
    },

    {
      $lookup: {
        "from": "entityrels",
        "localField": "entityId",
        "foreignField": "entityParty2",
        "as": "relatedFas"
      }
    },
    { $unwind : "$relatedFas"},
    {
      $project: {
        "userId":1,
        "entityId":1,
        "userFirstName":1,
        "userLastName":1,
        "loginEmailId":1,
        "entityInfo":1,
        "relatedFas.relId":"$relatedFas._id",
        "relatedFas.faEntityId:":"$relatedFas.entityParty1",
        "joinFaEntityId":"$relatedFas.entityParty1",
        "relationshipToTenant":"$relatedFas.relationshipType"

      }},
    {
      $lookup: {
        "from": "entities",
        "localField": "joinFaEntityId",
        "foreignField": "_id",
        "as": "relatedFaDetails"
      }
    },
    { $unwind : "$relatedFaDetails"},
    
    {
      $project: {
        "userId":1,
        "userFirstName":1,
        "userLastName":1,
        "loginEmailId":1,
        "entityInfo":1,
        "relationshipToTenant":1,
        "relatedFas.faEntityId":"$joinFaEntityId",
        "relatedFas.msrbName":"$relatedFaDetails.msrbFirmName",
        "relatedFas.msrbType":"$relatedFaDetails.msrbRegistrantType",
        "relatedFas.firmName":"$relatedFaDetails.firmName",
        "relatedFas.munivisorClient":"$relatedFaDetails.isMuniVisorClient"

      }        
        
    },
    {
      $group: {
        _id: "$loginEmailId",
        userEntities: {
          $addToSet: {
            userId: "$userId",
            userFirstName: "$userFirstName",
            userLastName: "$userLastName",
            msrbFirmName: "$entityInfo.msrbName",
            relationshipToTenant:"$relationshipToTenant",
            firmName:"$entityInfo.firmName",
            msrbType:"$entityInfo.msrbType",
            entityId:"$entityInfo.userEntityId",
            settings:"$entityInfo.settings"
          }
        },
        relatedFaEntities: {
          $addToSet: {
            msrbFirmName: "$relatedFas.msrbName",
            msrbType:"$relatedFas.msrbType",
            firmName:"$relatedFas.firmName",
            entityId:"$relatedFas.faEntityId"
          }
        }
      }
    }          
    ])
  }
  catch(err) {
    console.log("There is an error")
  }
  return returnObject[0]
}
export const getMappedFinAdvisorForLoggedInFAUser = async (email) => {
  let returnObject
  const regexemail = new RegExp(email,"i")

  try{
    returnObject = await EntityUser.aggregate([{
      $match: {
        "userLoginCredentials.userEmailId": {$regex:regexemail,$options:"i"}
      }
    },
    {
      $project: {
        "userId":"$_id",
        "entityId":1,
        "userFirstName":1,
        "userLastName":1,
        "userEntitlement":1,
        "loginEmailId":{$toLower:"$userLoginCredentials.userEmailId"},
      }
    },
    {
      $lookup: {
        "from": "entities",
        "localField": "entityId",
        "foreignField": "_id",
        "as": "entityInfo"
      }
    },
    { $unwind : "$entityInfo"}, 
        
    {
      $project: {
        "userId":1,
        "entityId":1,
        "userFirstName":1,
        "userLastName":1,
        "loginEmailId":1,
        "userEntitlement":1,
        "entityInfo.msrbName":"$entityInfo.msrbFirmName",
        "entityInfo.msrbType":"$entityInfo.msrbRegistrantType",
        "entityInfo.firmName":"$entityInfo.firmName",
        "entityInfo.munivisorClient":{$ifNull:["$entityInfo.isMuniVisorClient",false]},
        "entityInfo.settings":"$entityInfo.settings"
      }      
    },
    {
      $match: {
        "entityInfo.munivisorClient": true
      }
    },
    {
      $group: {
        _id: "$loginEmailId",
        userEntities: {
          $addToSet: {
            userId: "$userId",
            userFirstName: "$userFirstName",
            userLastName: "$userLastName",
            userEntitlement: "$userEntitlement",
            msrbFirmName: "$entityInfo.msrbName",
            relationshipToTenant:"Self",
            firmName:"$entityInfo.firmName",
            msrbType:"$entityInfo.msrbType",
            entityId:"$entityId",
            settings:"$entityInfo.settings"
          }
        },
        relatedFaEntities: {
          $addToSet: {
            msrbFirmName: "$entityInfo.msrbName",
            msrbType:"$entityInfo.msrbType",
            firmName:"$entityInfo.firmName",
            entityId:"$entityId"
          }
        }
      }
    }      
    ])
  }
  catch(err) {
    console.log("There is an error")
  }
  return returnObject[0]
}

export const getUrlsForTenantBasedOnLoggedInUserAtSignIn = async (email) => {
  const regexemail = new RegExp(email,"i")
  const tenantDetails = await EntityUser.aggregate([{
    $match: {
      "userLoginCredentials.userEmailId": {$regex:regexemail,$options:"i"}
    }
  },
  {
    $project: {
      "entityId":1,
    }
  },
  {
    $lookup:{
      from: "entityrels",
      localField: "entityId",
      foreignField: "entityParty2",
      as: "tenantdetails"
    }
  },
  {
    $unwind:"$tenantdetails"
  },
  {
    $project: {
      entityId:1,
      tenantId:"$tenantdetails.entityParty1"
    }
  }
  ])

  let tenantUrlDetails = {}
  if(tenantDetails.length > 0) {
    tenantUrlDetails = await tenantUrlGenerator(tenantDetails[0].tenantId)
  }
  return tenantUrlDetails
}

export const getMappedFinancialAdvisor = async (email) => {
  const fasForLoggedInFaUser = await getMappedFinAdvisorForLoggedInFAUser(email)
  const fasForLoggedInNonFaUser = await getMappedFinAdvisorForLoggedInNonFAUser(email)
  return fasForLoggedInFaUser || fasForLoggedInNonFaUser
}
