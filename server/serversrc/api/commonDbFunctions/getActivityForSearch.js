import {ObjectID} from "mongodb"
import {getEligibleDealsForLoggedInUser,getEligibleRFPsForLoggedInUser,getEligibleTransactionsForLoggedInUserRevised } from "./getEligibleTransactionsForLoggedInUser"
import  {  EntityUser }  from "./../appresources/models"

const queryMapperFunction = (tranIds, tranType ) => {
  const queryReference = {
    deals:[
      {
        $lookup:
                {
                  from: "tranagencydeals",
                  localField: "faEntityId",
                  foreignField: "dealIssueTranClientId",
                  as: "deals"
                }
      },
      {$unwind:"$deals"},
      {$match:{"deals._id":{"$in":[...tranIds]}}},
      {$project:
          {
            userId:1,
            userRole:1,
            userEntitlement:1,
            entityId:1,
            faEntityId:1,
            "dealTranId":"$deals._id",
            "dealAssignees":"$deals.dealIssueTranAssignedTo",
            "dealParticipants":"$deals.dealIssueParticipants",
          }
      },
      { $project: { _id:{loggedInUserId:"$userId",tranId:"$dealTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , participantUser:{ $setUnion: [ "$dealAssignees","$dealParticipants" ] } } }
    ],

    rfps:[
      {
        $lookup:
                {
                  from: "tranagencyrfps",
                  localField: "faEntityId",
                  foreignField: "rfpTranClientId",
                  as: "rfps"
                }
      },
      {$unwind:"$rfps"},
      {$match:{"rfps._id":{"$in":[...tranIds]}}},
      {$project:
          {
            userId:1,
            userRole:1,
            userEntitlement:1,
            entityId:1,
            faEntityId:1,
            "rfpTranId":"$rfps._id",
            "rfpAssignees":"$rfps.rfpTranAssignedTo",
            "rfpEvaluationTeam":"$rfps.rfpEvaluationTeam.rfpSelEvalContactId",
            "rfpProcessContacts":"$rfps.rfpProcessContacts.rfpProcessContactId",
            "rfpParticipants":"$rfps.rfpParticipants.rfpParticipantContactId"
          }
      },

      { $project: { _id:{loggedInUserId:"$userId",tranId:"$rfpTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , participantUser:{ $setUnion: [ "$rfpAssignees","$rfpEvaluationTeam","$rfpProcessContacts","$rfpParticipants" ] } } }
    ],
    
    bankloans:[    
      {
        $lookup:
          {
            from: "tranbankloans",
            localField: "faEntityId",
            foreignField: "actTranFirmId",
            as: "bankloans"
          }
      },
      {$unwind:"$bankloans"},
      {$match:{"bankloans._id":{"$in":[...tranIds]}}},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          "bankLoansTranId":"$bankloans._id",
          "bankLoansLeadAdvisor":"$bankloans.actTranFirmLeadAdvisorId",
          "bankLoansEvaluationTeam":"$bankloans.banLoanRfpEvaluationTeam.rfpSelEvalContactId", 
          "bankLoanParticipants":"$bankloans.bankLoanParticipants.partContactId",
          "bankLoansProcessContactsTeam":"$bankloans.banLoanRfpProcessContacts.rfpProcessContactId",       
          "bankLoansRfpParticipants":"$bankloans.banLoanRfpParticipants.rfpParticipantContactId"       
        }
      },
      { $project: { _id:{loggedInUserId:"$userId",tranId:"$bankLoansTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , participantUser:{ $setUnion: [ ["$bankLoansLeadAdvisor"],"$bankLoansEvaluationTeam","$bankLoanParticipants","$bankLoansProcessContactsTeam","$bankLoansRfpParticipants" ] } } }
   
    ],

    derivatives:[    
      {
        $lookup:
          {
            from: "tranderivatives",
            localField: "faEntityId",
            foreignField: "actTranFirmId",
            as: "derivatives"
          }
      },
      {$unwind:"$derivatives"},
      {$match:{"derivatives._id":{"$in":[...tranIds]}}},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          "derivativesTranId":"$derivatives._id",
          "derivativesLeadAdvisor":"$derivatives.actTranFirmLeadAdvisorId",
          "derivativeParticipants":"$derivatives.derivativeParticipants.partContactId", 
        }
      },
      { $project: { _id:{loggedInUserId:"$userId",tranId:"$derivativesTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , participantUser:{ $setUnion: [ ["$derivativesLeadAdvisor"],"$derivativeParticipants" ] } } }
    ],
    
    marfps:[
      {
        $lookup:
          {
            from: "actmarfps",
            localField: "faEntityId",
            foreignField: "actTranFirmId",
            as: "marfps"
          }
      },
      {$unwind:"$marfps"},
      {$match:{"marfps._id":{"$in":[...tranIds]}}},
      {$project:
        {
          userId:1,
          userRole:1,
          userEntitlement:1,
          entityId:1,
          faEntityId:1,
          "maRfpTranId":"$marfps._id",
          "maRfpLeadAdvisor":"$marfps.actLeadFinAdvClientEntityId",
          "maRfpParticipants":"$marfps.maRfpParticipants.partContactId", 
        }
      },
      { $project: { _id:{loggedInUserId:"$userId",tranId:"$maRfpTranId",loggedInUserEntityId:"$entityId",userEntitlement:"$userEntitlement" }
        , participantUser:{ $setUnion: [ ["$maRfpLeadAdvisor"],"$maRfpParticipants" ] } } }
    ]
  }
  return queryReference[tranType]
}

export const getSearchTransactions = async (user,tranids,tranType) => {
  const customTranQuery = queryMapperFunction(tranids ,tranType || "deals")
  const transactions= await EntityUser.aggregate([
    // Get the user
    {$match:{_id:ObjectID(user._id)}},
    // get the associated FA for the user
    {
      $lookup:
            {
              from: "entityrels",
              localField: "entityId",
              foreignField: "entityParty2",
              as: "matchedEntity"
            }
    },
    { $unwind : "$matchedEntity"},
    { $project : { userId:"$_id",userRole:1, userEntitlement:1, entityId:1, faEntityId:"$matchedEntity.entityParty1"}},
    ...customTranQuery,
    {$unwind:"$participantUser"},
    {  $lookup:
            {
              from: "entityusers",
              localField: "participantUser",
              foreignField: "_id",
              as: "participantDetails"
            }
    },    
    {$unwind:"$participantDetails"},
    {
      $project:{
        _id:1,
        loggedInUserEntityId:"$_id.loggedInUserEntityId",
        participantUser:1,
        participantUserEntityId:"$participantDetails.entityId",
        myTran:{$cond: [ {"$eq": ["$participantDetails._id", "$_id.userId"]}, true, false ]},
        myFirmTran: {$cond: [ {"$eq": ["$participantDetails.entityId", "$_id.loggedInUserEntityId"]}, true,false ]},
      }
    },
    {  $lookup:
      {
        from: "entities",
        localField: "loggedInUserEntityId",
        foreignField: "_id",
        as: "participantEntityInfo"
      }
    },
    {$unwind:"$participantEntityInfo"},
    {
      $project:{
        _id:1,
        participantUser:1,
        participantUserEntityId:1,
        loggedInUserEntityMvClient:"$participantEntityInfo.isMuniVisorClient",
        myTran:1,
        myFirmTran: 1,
      }
    },
    {
      $project:{
        _id:1,
        participantUser:1,
        participantUserEntityId:1,
        loggedInUserEntityMvClient:1,
        myTran:1,
        myFirmTran: 1,
        viewClass:{
          $switch:
            {
              branches: [
                {
                  case: { $eq : [ "$myTran",true ] },
                  then: "My Tran"
                },
                {
                  case: { $and : [ { $eq : [ "$myTran",false ] },
                    { $eq:["$myFirmTran",true]} ] },
                  then: "My Firm"
                },
                {
                  case: { $and : [ { $eq : [ "$myTran",false ] },
                    { $eq:["$myFirmTran",false]},{ $eq:["$loggedInUserEntityMvClient",true]} ] },
                  then: "My Deal Team"
                },                
              ],
              default: "No Team"
            }
        },      
      }
    }, 
    {
      $match: { viewClass:{"$ne":"No Team"} }
    },
    {
      $group: {
        _id:{
          loggedInUserId:"$_id.loggedInUserId",
          dealTranId:"$_id.tranId",
          loggedInUserEntityId:"$_id.loggedInUserEntityId",
          userEntitlement:"$_id.userEntitlement" ,
          loggedInUserEntityMvClient:"$loggedInUserEntityMvClient",
        },
        myTrans: {
          $addToSet: {
            $cond: { 
              if: { $eq: [ "$viewClass", "My Tran" ] }, 
              then: {
                "participantUser": "$participantUser",
                "participantUserEntityId": "$participantUserEntityId",
              } ,
              else: null,
            }
          }
        },
        myFirmTeam: {
          $addToSet: {
            $cond: { 
              if: { $eq: [ "$viewClass", "My Firm" ] }, 
              then: {
                "participantUser": "$participantUser",
                "participantUserEntityId": "$participantUserEntityId",
              } ,
              else: null,
            }
          }
        },
        myTranTeam: {
          $addToSet: {
            $cond: { 
              if: { $eq: [ "$viewClass", "My Deal Team" ] }, 
              then: {
                "participantUser": "$participantUser",
                "participantUserEntityId": "$participantUserEntityId",
              } ,
              else: null,
            }
          }
        }        
      }
    }, 
    {
      $project: {
        _id:0,
        loggedInUserId:"$_id.loggedInUserId",
        loggedInUserEntityId:"$_id.loggedInUserEntityId",
        loggedInUserEntitlement:"$_id.userEntitlement" ,
        loggedInUserEntityMvClient:"$_id.loggedInUserEntityMvClient",
        transactionId:"$_id.dealTranId",
        "myTrans": {
          "$filter": {
            input: "$myTrans",
            as: "ip",
            cond: { $ne: [ "$$ip", null ] } 
          } 
        },
        "myFirmTeam": {
          "$filter": {
            input: "$myFirmTeam",
            as: "ip",
            cond: { $ne: [ "$$ip", null ] } 
          } 
        },  
        "myTranTeam": {
          "$filter": {
            input: "$myTranTeam",
            as: "ip",
            cond: { $ne: [ "$$ip", null ] } 
          } 
        }               
      }
    },  
    {  
      $lookup:
      {
        from: "tasks",
        localField: "transactionId",
        foreignField: "relatedActivityDetails.activityId",
        as: "tasks"
      }
    },                 
    { $sort : { _id:1 } }   
  ])

  // console.log(JSON.stringify(transactions,null,2))
  return {[tranType]:transactions}
}

export const getAllEligibleActivityForSearch = async (user) => {
  const tranTypes = [ "deals", "rfps", "bankloans", "derivatives", "marfps"]
  // const tranTypes = [ "derivatives"]
  try {
    const {data} = await getEligibleTransactionsForLoggedInUserRevised(user)
    // console.log(JSON.stringify(data,null,2))
    const overallSearchData = await (tranTypes || []).reduce( async (allEligibleSearchTranDetails,tranType) => {
      const processData = data && data[tranType]
      
      const tranObjectIds = (processData || []).map( d => ObjectID(d.tranId))
      console.log(`TRAN OBJECT IDS - ${tranType}`,JSON.stringify(tranObjectIds,null,2))
      
      const searchEligibleData = await getSearchTransactions(user, tranObjectIds,tranType)
      // console.log(JSON.stringify(searchEligibleData,null,2))
      const allEligibleSearchTranDetailsAcc = await allEligibleSearchTranDetails
      return Promise.resolve({...allEligibleSearchTranDetailsAcc,...searchEligibleData})

    },Promise.resolve({}))

    // console.log(JSON.stringify(overallSearchData,null,2))
    
    return {status:"success",message:`Obtained all Search Eligible Transactions  - ${user._id} -${tranTypes}`, payload:overallSearchData}
  } catch (e) 
  {
    console.log( "Error while fetch eligible transactions for users", e)
    return {status:"fail",message:`Error Fetching Data for Search Eligible Transactions - ${user._id} - ${tranTypes}`, payload:undefined}
  }
}

