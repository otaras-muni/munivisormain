import {ObjectID} from "mongodb"
import  {  EntityRel }  from "./../appresources/models"
import { getConsolidatedAccessAndEntitlementsForLoggedInUser } from "./../entitlementhelpers"

export const getAllEntityAndUserDetailsForTenantUser = async (user) => {

  let idsToCheck = []
  
  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
    idsToCheck = []
  } else {
    const {all} = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,"nav")
    idsToCheck = [...all]
  }
  const tenantArray = [      
    {
      $lookup:
          {
            from: "entityrels",
            localField: "entityParty1",
            foreignField: "entityParty1",
            as: "alltenantentities"
          }
    },
    { $unwind : "$alltenantentities"},
    { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}}
  ]

  const otherArray = [
    { $project: {entityId:"$entityParty2", relType:"$relationshipType"}}
  ]

  const mapper = {
    "Self":tenantArray,
    "Client":otherArray,
    "Third Party":otherArray,
    "Undefined":otherArray
  }

  const checkIds = idsToCheck.length > 0 ? [
    {$match:{"alltenantentityusers._id":{"$in":[...idsToCheck]}}}
  ] : []

  const pipeline = [
    {$match:{entityParty2:ObjectID(user.entityId)}},
    ...mapper[user.relationshipType],
    {
      $lookup:
            {
              from: "entityusers",
              localField: "entityId",
              foreignField: "entityId",
              as: "alltenantentityusers"
            }
    },
    {
      $unwind:"$alltenantentityusers"
    },
    ...checkIds,
    {
      $project:{
        entityId:1,
        relType:1,
        entitlement:"$alltenantentityusers.userEntitlement",
        userId:"$alltenantentityusers._id",
        userFirstName:"$alltenantentityusers.userFirstName",
        userLastName:"$alltenantentityusers.userLastName",
        loginEmail:{$ifNull:["$alltenantentityusers.userLoginCredentials.userEmailId",""]},
        stpEligible:{$ifNull:["$alltenantentityusers.userLoginCredentials.isUserSTPEligible",false]},
        password:"password"
      }
    },
    {
      $lookup:
            {
              from: "entities",
              localField: "entityId",
              foreignField: "_id",
              as: "entitydetails"
            }
    },
    {
      $unwind:"$entitydetails"
    },
    {
      $project:{
        entityId:1,
        firmName:"$entitydetails.firmName",
        relType:1,
        entitlement:1,
        userId:1,
        userFirstName:1,
        userLastName:1,
        loginEmail:1,
        stpEligible:1,
      }      
    }

  ]


  let allTenantUsersPrimaryEmailId = []
  try {
    allTenantUsersPrimaryEmailId = await EntityRel.aggregate(pipeline)
    console.log("1.EXTRACTED ALL EMAILS",allTenantUsersPrimaryEmailId[0])
    return {message:"Successfully extracted all emails from ", emailDetails:allTenantUsersPrimaryEmailId}

  }  catch (e) {
    console.log(e)
    return {error:"Error in extracting all the users with primary email",message:"", emailDetails:[]}
  }
}