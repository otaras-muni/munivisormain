import {ObjectID} from "mongodb"

import  {
  EntityUser
}  from "./../appresources/models"

export const getFaEntityGraphInfo = async (user) => {
  
  const entityGraphInfo= await EntityUser.aggregate([
    
    // Get the user
    {$match:{_id:ObjectID(user._id)}},
    // get the associated FA for the user
    {
      $lookup:
              {
                from: "entityrels",
                localField: "entityId",
                foreignField: "entityParty2",
                as: "matchedEntity"
              }
    },
    { $unwind : "$matchedEntity"},
    { $project : { faEntityId:"$matchedEntity.entityParty1", relatedEntityId:"$matchedEntity.entityParty2",relationshipType:"$matchedEntity.relationshipType"}},
    {
      $lookup:
              {
                from: "entityrels",
                localField: "faEntityId",
                foreignField: "entityParty1",
                as: "fagraphentities"
              }
    }, 
    { $unwind : "$fagraphentities"},
    { $project : { faEntityId:1, 
      relatedEntityId:"$fagraphentities.entityParty2",
      relationshipType:{$cond:{if:{$eq:["$fagraphentities.relationshipType","Self"]},then:"Firm",else:"$fagraphentities.relationshipType"}}
    }},
    { $match:{"relationshipType":{$in:["Client","Self","Firm","Prospect","Third Party"]}}},
    {
      $lookup:
              {
                from: "entities",
                localField: "relatedEntityId",
                foreignField: "_id",
                as: "relatedentitydetails"
              }
    },   
    { $unwind : "$relatedentitydetails"},
    { $project : { 
      faEntityId:1, 
      relatedEntityId:1,
      relationshipType:1,
      firmName:"$relatedentitydetails.firmName"}},
    { $sort : { relationshipType : 1, firmName: 1 } }
  ])

  return entityGraphInfo
}