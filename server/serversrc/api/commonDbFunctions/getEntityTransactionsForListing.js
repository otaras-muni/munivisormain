import {ObjectID} from "mongodb"

import  {
  RFP, Deals, BankLoans, Derivatives, ActMaRFP,Others,ActBusDev
}  from "./../appresources/models"

import { getEligibleTransactionsByTypeForLoggedInUser } from "./getEligibleTransactionsForLoggedInUser"
import { getAllAccessAndEntitlementsForLoggedInUser } from "./../entitlementhelpers"
import { fetchUrlForTransactions } from "./../../helpers"

const transRelatedToEntitiesLookup = async (trantype,tranIds = [],refEntity,tenantId,applyEntitlement) => {

  let matchQuery = []

  const referenceQuery = {
    deals:[{"$match":{dealIssueTranClientId:ObjectID(tenantId)}}],
    rfps:[{"$match":{rfpTranClientId:ObjectID(tenantId)}}],
    bankloans:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    derivatives:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    marfps:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    others:[{"$match":{actTranClientId:ObjectID(tenantId)}}],
    busdevs:[{"$match":{actTranFirmId:ObjectID(tenantId)}}]
  }

  if(applyEntitlement) {
    matchQuery = [{ $match:{"_id":{"$in":[...tranIds || []]}}}]
    matchQuery = [...matchQuery, ...referenceQuery[trantype]]
  } else {
    matchQuery = referenceQuery[trantype]
  }

  if ( trantype === "deals") {
    return Deals.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          dealIssueTranClientId:1,
          dealIssueTranIssuerId:1,
          dealParticipantFirms:{$setUnion:["$dealIssueUnderwriters.dealPartFirmId","$dealIssueParticipants.dealPartFirmId"]},
          dealParticipantUsers:{$setUnion:["$dealIssueParticipants.dealPartContactId","$dealIssueTranAssignedTo",["$createdByUser"]]},
          tranAttributes:{
            issueName:"$dealIssueTranIssueName",
            projectDescription:"$dealIssueTranProjectDescription",
            type:"$dealIssueTranType",
            subType:"$dealIssueTranSubType",
            status:"$dealIssueTranStatus",
            parAmount:"$dealIssueParAmount"
          }
        },
      },
      {$unwind:{"path":"$dealParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "dealParticipantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          dealIssueTranClientId:1,
          dealIssueTranIssuerId:1,
          dealParticipantFirms:1,
          dealParticipantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",dealIssueTranClientId:"$dealIssueTranClientId",dealIssueTranIssuerId:"$dealIssueTranIssuerId", tranAttributes:"$tranAttributes",dealParticipantFirms:"$dealParticipantFirms"},
          dealIssueTranClientId:{$addToSet:"$dealIssueTranClientId"},
          dealIssueTranIssuerId:{$addToSet:"$dealIssueTranIssuerId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$dealParticipantUsers"}
        }
      },
      { $project: 
        { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", tranUsers:1,
          allTranEntities:{ $setUnion: [ "$_id.dealParticipantFirms","$dealIssueTranClientId","$dealIssueTranIssuerId","$tranEntities" ] },
        } 
      },
      { 
        $match:{"allTranEntities":refEntity}
      }
    ])
  }

  if ( trantype === "rfps") {
    return RFP.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          rfpTranClientId:1,
          rfpTranIssuerId:1,
          rfpParticipantUsers:{$setUnion:["$rfpEvaluationTeam.rfpSelEvalContactId","$rfpProcessContacts.rfpProcessContactId", "$rfpParticipants.rfpParticipantContactId", "$rfpTranAssignedTo", ["$createdByUser"]]},
          tranAttributes:{
            issueName:"$rfpTranIssueName",
            projectDescription:"$rfpTranProjectDescription",
            type:"$rfpTranType",
            subType:"$rfpTranSubType",
            status:"$rfpTranStatus",
          }
        },
      },
      {$unwind:{"path":"$rfpParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "rfpParticipantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          rfpTranClientId:1,
          rfpTranIssuerId:1,
          rfpParticipantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",rfpTranClientId:"$rfpTranClientId",rfpTranIssuerId:"$rfpTranIssuerId", tranAttributes:"$tranAttributes"},
          rfpTranClientId:{$addToSet:"$rfpTranClientId"},
          rfpTranIssuerId:{$addToSet:"$rfpTranIssuerId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$rfpParticipantUsers"}
        }
      },
      { $project: { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", tranUsers:1, allTranEntities:{ $setUnion: [ "$tranEntities","$rfpTranClientId","$rfpTranIssuerId" ] } } },
      { 
        $match:{"allTranEntities":refEntity}
      }
    ])
  }

  if ( trantype === "bankloans") {
    return BankLoans.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          bankLoansFirmId:"$actTranFirmId",
          bankLoansClientId:"$actTranClientId",
          bankLoansParticipantUsers:{$setUnion:["$banLoanRfpEvaluationTeam.rfpSelEvalContactId","$banLoanRfpProcessContacts.rfpProcessContactId", "$banLoanRfpParticipants.rfpParticipantContactId", ["$actTranFirmLeadAdvisorId"],["$createdByUser"]]},
          tranAttributes:{
            issueName:"$dealIssueTranIssueName",
            projectDescription:"$actTranProjectDescription",
            type:"$actTranType",
            subType:"$actTranSubType",
            status:"$bankLoanSummary.actTranStatus"
          }
        },
      },
      {$unwind:{"path":"$bankLoansParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "bankLoansParticipantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          bankLoansFirmId:1,
          bankLoansClientId:1,
          bankLoansParticipantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },  
      {
        $group:{
          _id:{tranId:"$_id",bankLoansFirmId:"$bankLoansFirmId",bankLoansClientId:"$bankLoansClientId", tranAttributes:"$tranAttributes"},
          bankLoansFirmId:{$addToSet:"$bankLoansFirmId"},
          bankLoansClientId:{$addToSet:"$bankLoansClientId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$bankLoansParticipantUsers"}
        }
      },
      { 
        $project: 
        { _id:0, tranId:"$_id.tranId",
          tranAttributes:"$_id.tranAttributes", 
          tranUsers:1, 
          allTranEntities:{ $setUnion: [ "$tranEntities","$bankLoansFirmId","$bankLoansClientId" ] } 
        } 
      },
      { 
        $match:{"allTranEntities":refEntity}
      }
        
    ])
  }
  if( trantype === "derivatives") {
    return Derivatives.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          derivativesFirmId:"$actTranFirmId",
          derivativesClientId:"$actTranClientId",
          derivativesParticipantUsers:{$setUnion:["$derivativeParticipants.partContactId", ["$actTranFirmLeadAdvisorId"],["$createdByUser"]]},
          tranAttributes:{
            issueName:"$actTranIssueName",
            projectDescription:"$actTranProjectDescription",
            type:"$actTranType",
            subType:"$actTranSubType",
            status:"$actTranStatus"
          }
        },
      },
      {$unwind:{"path":"$derivativesParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "derivativesParticipantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          derivativesFirmId:1,
          derivativesClientId:1,
          derivativesParticipantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },  
      {
        $group:{
          _id:{tranId:"$_id",derivativesFirmId:"$derivativesFirmId",derivativesClientId:"$derivativesClientId", tranAttributes:"$tranAttributes"},
          derivativesFirmId:{$addToSet:"$derivativesFirmId"},
          derivativesClientId:{$addToSet:"$derivativesClientId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$derivativesParticipantUsers"}
        }
      },
      { 
        $project: 
        { _id:0, tranId:"$_id.tranId",
          tranAttributes:"$_id.tranAttributes", 
          tranUsers:1, 
          allTranEntities:{ $setUnion: [ "$tranEntities","$derivativesFirmId","$derivativesClientId" ] } 
        } 
      },
      { 
        $match:{"allTranEntities":refEntity}
      }
        
    ])
  }
  
  if(trantype ==="marfps") {
    return ActMaRFP.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          marfpFirmId:"$actTranFirmId",
          marfpClientId:"$actIssuerClient",
          marfpParticipantUsers:{$setUnion:["$maRfpParticipants.partContactId", ["$actLeadFinAdvClientEntityId"],["$createdByUser"]]},
          tranAttributes:{
            issueName:"$actIssueName",
            projectDescription:"$actProjectName",
            type:"$actTranType",
            subType:"$actSubType",
            status:"$actStatus"
          }
        },
      },
      {$unwind:{"path":"$marfpParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "marfpParticipantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          marfpFirmId:1,
          marfpClientId:1,
          marfpParticipantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },  
      {
        $group:{
          _id:{tranId:"$_id",marfpFirmId:"$marfpFirmId",marfpClientId:"$marfpClientId", tranAttributes:"$tranAttributes"},
          marfpFirmId:{$addToSet:"$marfpFirmId"},
          marfpClientId:{$addToSet:"$marfpClientId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$marfpParticipantUsers"}
        }
      },
      { 
        $project: 
        { _id:0, tranId:"$_id.tranId",
          tranAttributes:"$_id.tranAttributes", 
          tranUsers:1, 
          allTranEntities:{ $setUnion: [ "$tranEntities","$marfpFirmId","$marfpClientId" ] } 
        } 
      },
      { 
        $match:{"allTranEntities":refEntity}
      }
        
    ])
  }

  if( trantype ===  "others" ) {
    return Others.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          actTranClientId:1,
          actTranIssuerId:1,
          participantFirms:{$setUnion:["$actTranUnderwriters.partFirmId","$participants.partFirmId"]},
          participantUsers:{$setUnion:["$participants.partContactId","$actTranAssignedTo",["$createdByUser"]]},
          tranAttributes:{
            issueName:"$actTranIssueName",
            projectDescription:"$actTranProjectDescription",
            type:"$actTranType",
            subType:"$actTranSubType",
            status:"$actTranStatus",
            parAmount:"$actTranParAmount"
          }
        },
      },
      {$unwind:{"path":"$participantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "participantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          actTranClientId:1,
          actTranIssuerId:1,
          participantFirms:1,
          participantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",actTranClientId:"$actTranClientId",actTranIssuerId:"$actTranIssuerId", tranAttributes:"$tranAttributes",participantFirms:"$participantFirms"},
          actTranClientId:{$addToSet:"$actTranClientId"},
          actTranIssuerId:{$addToSet:"$actTranIssuerId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$participantUsers"}
        }
      },
      { $project: 
        { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", tranUsers:1,
          allTranEntities:{ $setUnion: [ "$_id.participantFirms","$actTranClientId","$actTranIssuerId","$tranEntities" ] },
        } 
      },
      { 
        $match:{"allTranEntities":refEntity}
      }
    ])
  }

  if (trantype === "busdevs") {
    return ActBusDev.aggregate([
      ...matchQuery,
      { 
        $project: { 
          _id:"$_id",
          actTranFirmId:1,
          actIssuerClient:1,
          participantFirms:{$setUnion:["$participants.partFirmId",["$actIssuerClient"]]},
          participantUsers:{$setUnion:["$participants.partContactId",["$createdByUser","$actLeadFinAdvClientEntityId"]]},
          tranAttributes:{
            issueName:"$actIssueName",
            projectDescription:"$actProjectName",
            type:"$actType",
            subType:"$actSubType",
            status:"$actStatus",
            parAmount:""
          }
        },
      },
      {$unwind:{"path":"$participantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
                {
                  from: "entityusers",
                  localField: "participantUsers",
                  foreignField: "_id",
                  as: "participantUserDetails"
                }
      }, 
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      { 
        $project: { 
          _id:1,
          actTranClientId:1,
          actTranIssuerId:1,
          participantFirms:1,
          participantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",actTranClientId:"$actTranClientId",actTranIssuerId:"$actTranIssuerId", tranAttributes:"$tranAttributes",participantFirms:"$participantFirms"},
          actTranClientId:{$addToSet:"$actTranClientId"},
          actTranIssuerId:{$addToSet:"$actTranIssuerId"},
          tranEntities:{$push:"$tranEntities"},
          tranUsers:{$push:"$participantUsers"}
        }
      },
      { $project: 
        { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", tranUsers:1,
          allTranEntities:{ $setUnion: [ "$_id.participantFirms","$actTranClientId","$actTranIssuerId","$tranEntities" ] },
        } 
      },
      { 
        $match:{"allTranEntities":refEntity}
      }
    ])    
  }
}

export const getTransactionsForSpecificEntity = async (user,refEntity) => {
  
  try {
    const{checkEntitlement, tenantId} = user
    const tranTypes = ["deals","rfps","bankloans","derivatives","marfps","others","busdevs"]
    // Get access to the transactions that the user is supposed to access

    let mongoTranIds =[]

    if (checkEntitlement) {

      const allEntitledData = await getAllAccessAndEntitlementsForLoggedInUser(user, "consolidated")
      const tranIds = (allEntitledData && allEntitledData.tranEntitlements && Object.keys(allEntitledData.tranEntitlements)) || []
      mongoTranIds = (tranIds || []).map( tranId => ObjectID(tranId) )
    }

    /* const allOverallTranData = await (tranTypes || []).reduce( async (allEligibleTranTypes,tranType) => {
      // console.log(`----THESE ARE ALL THE ELIGIBLE TRANSACTIONS FOR THE USER -${tranType}`, JSON.stringify(tranData,null,2))
      const tranFunctionToRun = transRelatedToEntitiesLookup(tranType, mongoTranIds || [], ObjectID(refEntity, tenantId,checkEntitlement))
      // Get the eligible Entities
      const entityListingDataForType  = await tranFunctionToRun()
      // Need await the accumulator
      const revisedEntityListingData = entityListingDataForType.map( a => ({...a, url:fetchUrlForTransactions(tranType,"summary",a.tranId)}))
      const allEligibleTranTypesAcc = await allEligibleTranTypes
      // Return the accumulator
      return Promise.resolve({ ...allEligibleTranTypesAcc,...{[tranType]:revisedEntityListingData}})
    },Promise.resolve({})) */

    const allDataToRun = await Promise.all((tranTypes || []).map( async (tranType) => 
      transRelatedToEntitiesLookup(tranType, mongoTranIds || [], ObjectID(refEntity), tenantId,checkEntitlement)
    ))

    const dataToReturn = ((tranTypes || []).reduce((acc, tranType, ind) => ({...acc, [tranType]:allDataToRun[ind].map( a => ({...a, url:fetchUrlForTransactions(tranType,"summary",a.tranId)}))}),{}))
    console.log("New Console Setup", JSON.stringify(dataToReturn,null,2))
    return {status:"success",message:`Successfully fetched transactions for entity - ${refEntity}`, data:dataToReturn}
  } catch (e) 
  {
    console.log( "Error while fetch eligible transactions for users", e)
    return {status:"fail",message:`there was an error fetching data for entity - ${refEntity}`, data:undefined}
  }
}


export const getTransactionsForSpecificEntityOld = async (user,refEntity) => {
  try {
    const tranTypes = ["deals","rfps","bankloans"]
    const allOverallTranData = await (tranTypes || []).reduce( async (allEligibleTranTypes,tranType) => {
      const tranData = await getEligibleTransactionsByTypeForLoggedInUser(user, tranType)
      // console.log(`----THESE ARE ALL THE ELIGIBLE TRANSACTIONS FOR THE USER -${tranType}`, JSON.stringify(tranData,null,2))
      const {transactions} = await tranData || {}
      const tranids = (transactions || []).map( ({tranId}) => ObjectID(tranId) )
      // console.log("---Transactions that need to be checked", JSON.stringify(tranids,null,2))
      // Returns a function to be executed
      const tranFunctionToruun = transRelatedToEntitiesLookup(tranType, tranids, ObjectID(refEntity))

      // Get the eligible Entities
      const entityListingDataForType  = await tranFunctionToruun()

      // Need await the accumulator
      const allEligibleTranTypesAcc = await allEligibleTranTypes

      // Return the accumulator
      return Promise.resolve({ ...allEligibleTranTypesAcc,...{[tranType]:entityListingDataForType}})
    },Promise.resolve({}))
  
    return {status:"success",message:"Successfully fetch transactions", data:allOverallTranData}
  } catch (e) 
  {
    console.log( "Error while fetch eligible transactions for users", e)
    return {status:"fail",message:`there was an error fetching data for entity - ${refEntity}`, data:undefined}
  }
}
