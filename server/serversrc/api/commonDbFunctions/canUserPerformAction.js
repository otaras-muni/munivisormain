import { EntityUser } from "./../../api/appresources/entityUser/entityUser.model"

const {ObjectID} = require("mongodb")

export const canUserPerformAction = async(user,resourceAccess) => {
  // From the user get his entity ID and the entitlement and his role
  const {_id} = user
  const userResourceEntitlements = await EntityUser.aggregate([
    {"$match":{_id:ObjectID(_id)}},
    {"$project":{"_id":1,"entityId":1,"userFirstName":1,"userEntitlement":1,"userRole":1,"userLastName":1}},
    {
      $lookup: {
        "from": "configs",
        "localField": "entityId",
        "foreignField": "entityId",
        "as": "configInfo"
      }
    },
    {$unwind:"$configInfo"},
    {$project:
            {
              "_id":1,"entityId":1,"userFirstName":1,"userEntitlement":1,"userRole":1,"userLastName":1,
              "accessPolicy":"$configInfo.accessPolicy"
            }
    },
    {$unwind:"$accessPolicy"},
    {$project:
            {
              "_id":1,"entityId":1,"userFirstName":1,"userEntitlement":1,"userRole":1,"userLastName":1,
              "resourcesAccess":"$accessPolicy.resourceAccess",
              rolesEqual: { $eq: [ "$accessPolicy.role", "$userEntitlement" ] },
            }
    },
    {$match:{rolesEqual:true}},
    {$unwind:"$resourcesAccess"},
    {$addFields:{"resource": "$resourcesAccess.resource","access": "$resourcesAccess.access"}},
    {$project:{resourcesAccess:0}}
  ])

  // console.log("THE USER ENTITLEMENTS FROM THE BACKEND ARE", userResourceEntitlements)

  const accessPolicyPermissions = userResourceEntitlements.reduce( (entObject, ent) => {
    const {resource, access} = ent
    return {...entObject,...{[resource]:access}}
  }, {})

  // console.log("THE TRANSFORMED ENTITLEMENT ACCESS", accessPolicyPermissions)
  // console.log("REQUESTED PERMISSIONS",resourceAccess)

  const permission = resourceAccess.reduce( (finalEntitlement, resRequested) => {
    const {resource,access:accessRequested} = resRequested
    const entitled = accessRequested <= accessPolicyPermissions[resource]
    // console.log("THE ENTITLEMENT CHECK", entitled)
    return entitled && finalEntitlement
  },true)

  console.log("FINAL PERMISSION", permission)

  return permission
}
