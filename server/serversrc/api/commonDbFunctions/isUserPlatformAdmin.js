import { Entity } from "./../../api/appresources/entity/entity.model"

const {ObjectID} = require("mongodb")

export const isUserPlatformAdmin = async(user) => {
  
  if (!user.entityId) {
    console.log("THE ENTITY IS NOT PASSED IN THE REQUEST")
    return false
  }  

  const regexname = new RegExp("otaras","i")
  const val = await Entity.findOne(
    {_id:ObjectID(user.entityId), firmName: {$regex:regexname,$options:"i"}}
  ).select("_id firmName")
  console.log("Found Data", JSON.stringify(val, null,2))  
  if( val ) {
    return true
  } 
  return false
}

