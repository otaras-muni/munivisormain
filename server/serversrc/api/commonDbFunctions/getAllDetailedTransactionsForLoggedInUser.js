import {ObjectID} from "mongodb"
import {getEligibleTransactionsForLoggedInUserRevised } from "./getEligibleTransactionsForLoggedInUser"
import  {
  EntityUser
}  from "../appresources/models"

const typeSpecificConfig = {
  "deals": {
    lkup: {
      $lookup: {
        from: "tranagencydeals",
        localField: "faEntityId",
        foreignField: "dealIssueTranClientId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.dealIssueTranClientId"], ["$trans.dealIssueTranIssuerId"], "$trans.dealIssueUnderwriters.dealPartFirmId", "$trans.dealIssueParticipants.dealPartFirmId"
    ],
    partUsers: [
      "$trans.dealIssueTranAssignedTo", "$trans.dealIssueParticipants"
    ],
    typeInfo: {
      tranType: "debt",
      tranSubType: "deal issue",
      tranDesc: "$trans.dealIssueTranProjectDescription",
      tranStatus: "$trans.dealIssueTranStatus",
      tranFirmId:"$trans.dealIssueTranClientId",
      tranFirmName:"$trans.dealIssueTranClientFirmName",
      tranClientId:"$trans.dealIssueTranIssuerId",
      tranClientName:"$trans.dealIssueTranIssuerFirmName"
    }
  },
  "rfps": {
    lkup: {
      $lookup: {
        from: "tranagencyrfps",
        localField: "faEntityId",
        foreignField: "rfpTranClientId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.rfpTranClientId"], ["$trans.rfpTranIssuerId"], "$trans.rfpParticipants.rfpParticipantFirmId"
    ],
    partUsers: [
      "$trans.rfpEvaluationTeam.rfpSelEvalContactId", "$trans.rfpProcessContacts.rfpProcessContactId", "$trans.rfpParticipants.rfpParticipantContactId", "$trans.rfpTranAssignedTo"
    ],
    typeInfo: {
      tranType: "$trans.rfpTranType",
      tranSubType: "$trans.rfpTranSubType",
      tranDesc: "$trans.rfpTranProjectDescription",
      tranStatus: "$trans.rfpTranStatus",

      tranFirmId:"$trans.rfpTranClientId",
      tranFirmName:"$trans.rfpTranClientFirmName",
      tranClientId:"$trans.rfpTranIssuerId",
      tranClientName:"$trans.rfpTranIssuerFirmName"
    }
  },
  "bankloans": {
    lkup: {
      $lookup: {
        from: "tranbankloans",
        localField: "faEntityId",
        foreignField: "actTranFirmId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.actTranFirmId"], ["$trans.actTranClientId"], "$trans.bankLoanParticipants.partFirmId"
    ],
    partUsers: ["$trans.banLoanRfpEvaluationTeam.rfpSelEvalContactId", "$trans.banLoanRfpProcessContacts.rfpProcessContactId", "$trans.banLoanRfpParticipants.rfpParticipantContactId", ["$trans.actTranFirmLeadAdvisorId"]
    ],
    typeInfo: {
      tranType: "$trans.actTranType",
      tranSubType: "$trans.actTranSubType",
      tranDesc: "$trans.actTranProjectDescription",
      tranStatus: "$trans.bankLoanSummary.actTranStatus",

      tranFirmId:"$trans.actTranFirmId",
      tranFirmName:"$trans.actTranFirmName",
      tranClientId:"$trans.actTranClientId",
      tranClientName:"$trans.actTranClientName"
    }

  },
  "derivatives": {
    lkup: {
      $lookup: {
        from: "tranderivatives",
        localField: "faEntityId",
        foreignField: "actTranFirmId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.actTranFirmId"], ["$trans.actTranClientId"], "$trans.derivativeParticipants.partFirmId"
    ],
    partUsers: ["$trans.derivativeParticipants.partContactId", ["$trans.actTranFirmLeadAdvisorId"]
    ],
    typeInfo: {
      tranType: "$trans.actTranType",
      tranSubType: "$trans.actTranSubType",
      tranDesc: "$trans.actTranProjectDescription",
      tranStatus: "$trans.derivativeSummary.tranStatus",

      tranFirmId:"$trans.actTranFirmId",
      tranFirmName:"$trans.actTranFirmName",
      tranClientId:"$trans.actTranClientId",
      tranClientName:"$trans.actTranClientName"

    }
  },
  "marfps": {
    lkup: {
      $lookup: {
        from: "actmarfps",
        localField: "faEntityId",
        foreignField: "actTranFirmId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.actTranFirmId"], ["$trans.actIssuerClient"], "$trans.maRfpParticipants.partFirmId"
    ],
    partUsers: ["$trans.maRfpParticipants.partContactId", ["$trans.actLeadFinAdvClientEntityId"]
    ],
    typeInfo: {
      tranType: "$trans.actType",
      tranSubType: "$trans.actSubType",
      tranDesc: "$trans.actProjectName",
      tranStatus: "$trans.actStatus",

      tranFirmId:"$trans.actTranFirmId",
      tranFirmName:"$trans.actTranFirmName",
      tranClientId:"$trans.actIssuerClient",
      tranClientName:"$trans.actIssuerClientEntityName"
    }
  },
  "others": {
    lkup: {
      $lookup: {
        from: "tranagencyothers",
        localField: "faEntityId",
        foreignField: "actTranFirmId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.actTranFirmId"], ["$trans.actTranClientId"], "$trans.participants.partFirmId","$trans.actTranUnderwriters.partFirmId"
    ],
    partUsers: ["$trans.participants.partContactId", ["$trans.actTranFirmLeadAdvisorId"]
    ],
    typeInfo: {
      tranType: "$trans.actTranType",
      tranSubType: "$trans.actTranSubType",
      tranDesc: "$trans.actTranProjectDescription",
      tranStatus: "$trans.actTranStatus",

      tranFirmId:"$trans.actTranFirmId",
      tranFirmName:"$trans.actTranFirmName",
      tranClientId:"$trans.actTranClientId",
      tranClientName:"$trans.actTranClientName"
    }
  },
  "busdev": {
    lkup: {
      $lookup: {
        from: "actbusdevs",
        localField: "faEntityId",
        foreignField: "actTranFirmId",
        as: "trans"
      }
    },
    partEntities: [
      ["$trans.actTranFirmId"], ["$trans.actIssuerClient"], "$trans.participants.partFirmId" ],
    partUsers: [ ["$trans.actLeadFinAdvClientEntityId","$trans.createdByUser"],"$trans.participants.partContactId"],
    typeInfo: {
      tranType: "$trans.actType",
      tranSubType: "$trans.actSubType",
      tranDesc: "$trans.actProjectName",
      tranStatus: "$trans.actStatus",

      tranFirmId:"$trans.actTranFirmId",
      tranFirmName:"-",
      tranClientId:"$trans.actIssuerClient",
      tranClientName:"$trans.actIssuerClientEntityName"
    }
  }
}

export const getAllKeyTransactionDetailsForUser = async(user, tranType, tranIds,entitlementInfo) => {

  const {lkup, partEntities, partUsers, typeInfo} = typeSpecificConfig[tranType]
  const transactions = await EntityUser.aggregate([
    // Get the user
    {
      $match: {
        _id: ObjectID(user._id)
      }
    }, {
      $lookup: {
        from: "entityrels",
        localField: "entityId",
        foreignField: "entityParty2",
        as: "matchedEntity"
      }
    }, {
      $unwind: "$matchedEntity"
    }, {
      $project: {
        loggedInUserId: "$_id",
        loggedInUserEntityId: "$entityId",
        faEntityId: "$matchedEntity.entityParty1"
      }
    }, {
      ...lkup
    }, {
      $unwind: "$trans"
    }, {
      $match: {
        "trans._id": {
          "$in": [...tranIds]
        }
      }
    }, {
      $project: {
        loggedInUserId: 1,
        userRole: 1,
        userEntitlement: 1,
        loggedInUserEntityId: 1,
        faEntityId: 1,
        tranId: "$trans._id",
        ...typeInfo,
        participantEntities: {
          $setUnion: [...partEntities]
        },
        participantUsers: {
          $setUnion: [...partUsers]
        }
      }
    }, {
      $unwind: "$participantUsers"
    }, {
      $lookup: {
        from: "entityusers",
        localField: "participantUsers",
        foreignField: "_id",
        as: "userDetails"
      }
    }, {
      $unwind: "$userDetails"
    }, {
      $project: {
        loggedInUserId: 1,
        userRole: 1,
        userEntitlement: 1,
        loggedInUserEntityId: 1,
        faEntityId: 1,
        tranId: 1,
        tranType: 1,
        tranSubType: 1,
        tranDesc: 1,
        tranStatus: 1,

        tranFirmId:1,
        tranFirmName:1,
        tranClientId:1,
        tranClientName:1,
        
        participantEntities: 1,
        tranUserId: "$userDetails._id",
        tranUserFirstName: "$userDetails.userFirstName",
        tranUserLastName: "$userDetails.userLastName",
        tranUserEntityId: "$userDetails.entityId",
        tranUserFirmName: "$userDetails.userFirmName"
      }
    }, {
      $group: {
        _id: {
          loggedInUserId: "$loggedInUserId",
          loggedInUserEntityId: "$loggedInUserEntityId",
          faEntityId: "$faEntityId",
          tranId: "$tranId",
          tranType: "$tranType",
          tranSubType: "$tranSubType",
          tranDesc: "$tranDesc",
          tranStatus: "$tranStatus",

          tranFirmId:"$tranFirmId",
          tranFirmName:"$tranFirmName",
          tranClientId:"$tranClientId",
          tranClientName:"$tranClientName",
          
          participantEntities: "$participantEntities"
        },
        "participantUsers": {
          $addToSet: {
            tranId: "$tranId",
            tranUserId: "$tranUserId",
            tranUserFirstName: "$tranUserFirstName",
            tranUserLastName: "$tranUserLastName",
            tranUserEntityId: "$tranUserEntityId",
            tranUserFirmName:"$tranUserFirmName"

          }
        },
        "participantUniqueEntities": {
          $addToSet: "$tranUserEntityId"
        }
      }
    }, {
      $project: {
        _id: 0,
        loggedInUserId: "$_id.loggedInUserId",
        loggedInUserEntityId: "$_id.loggedInUserEntityId",
        faEntityId: "$_id.faEntityId",
        tranId: "$_id.tranId",
        tranType: "$_id.tranType",
        tranSubType: "$_id.tranSubType",
        tranDesc: "$_id.tranDesc",
        tranStatus: "$_id.tranStatus",
        tranFirmId:"$_id.tranFirmId",
        tranFirmName:"$_id.tranFirmName",
        tranClientId:"$_id.tranClientId",
        tranClientName:"$_id.tranClientName",
        participantUsers: 1,
        participantEntities: {
          $setUnion: ["$participantUniqueEntities", "$_id.participantEntities"]
        }
      }
    }, {
      $unwind: "$participantEntities"
    },
    // {$match:{"participantEntities":ObjectID("5b1a63bd471a1919e0ca97c8")}}
    {
      $lookup: {
        from: "entities",
        localField: "participantEntities",
        foreignField: "_id",
        as: "participantEntityDetails"
      }
    }, {
      $unwind: "$participantEntityDetails"
    }, {
      $group: {
        _id: {
          loggedInUserId: "$loggedInUserId",
          loggedInUserEntityId: "$loggedInUserEntityId",
          faEntityId: "$faEntityId",
          tranId: "$tranId",
          tranType: "$tranType",
          tranSubType: "$tranSubType",
          tranDesc: "$tranDesc",
          tranStatus: "$tranStatus",
          tranFirmId:"$tranFirmId",
          tranFirmName:"$tranFirmName",
          tranClientId:"$tranClientId",
          tranClientName:"$tranClientName",          
          participantUsers: "$participantUsers"
        },
        "participantEntityDetails": {
          $addToSet: {
            tranId: "$tranId",
            participantEntityId: "$participantEntityDetails._id",
            msrbFirmName: "$participantEntityDetails.msrbFirmName",
            firmName: "$participantEntityDetails.firmName"
          }
        }
      }
    }, {
      $project: {
        _id: 0,
        loggedInUserId: "$_id.loggedInUserId",
        loggedInUserEntityId: "$_id.loggedInUserEntityId",
        faEntityId: "$_id.faEntityId",
        activityId: "$_id.tranId",
        activityType: "$_id.tranType",
        activitySubType: "$_id.tranSubType",
        activityDescription: "$_id.tranDesc",
        activityStatus: "$_id.tranStatus",
        activityTranFirmId:"$_id.tranFirmId",
        activityTranFirmName:"$_id.tranFirmName",
        activityTranClientId:"$_id.tranClientId",
        activityTranClientName:"$_id.tranClientName",        
        participantUsers: "$_id.participantUsers",
        participantEntityDetails: 1
      }
    }
  ])

  const revisedRawTransformedTransactions = transactions.reduce( (acc, t) => {
    const {tranId,...restEntitlementInfo} = entitlementInfo[t.activityId]
    // console.log(JSON.stringify(restEntitlementInfo,null,2))
    return [...acc, {...t, ...restEntitlementInfo}]
  },[])

  const transformedTrans = transactions.reduce((consTranDetails, tran) => {
    const {
      participantEntityDetails,
      participantUsers,
      ...restParams
    } = tran

    const revisedPartEntDetails = (participantEntityDetails || []).reduce((allPartDetails, partEntity) => {
      const { participantEntityId } = partEntity
      return { ...allPartDetails,[participantEntityId]:partEntity}
    }, {})
    // console.log(JSON.stringify({revisedPartEntDetails}, null, 2))

    const findUniqueUsers = (participantUsers || []).reduce((uniqueValues, partUser) => {
      const {tranUserFirstName, tranUserId, tranUserLastName, tranUserEntityId,tranUserFirmName} = partUser
      if (uniqueValues[tranUserEntityId]) {
        const users = uniqueValues[tranUserEntityId]
        if (!users[tranUserId]) {
          users[tranUserId] = {
            userId: tranUserId,
            userFirstName: tranUserFirstName,
            userLastName: tranUserLastName,
            entityId: tranUserEntityId,
            userEntityName: tranUserFirmName
          }
        }
        return {
          ...uniqueValues,
          ...{
            [tranUserEntityId]: users
          }
        }
      }
      return {
        ...uniqueValues,
        ...{
          [tranUserEntityId]: {
            [tranUserId]: {
              userId: tranUserId,
              userFirstName: tranUserFirstName,
              userLastName: tranUserLastName,
              entityId: tranUserEntityId,
              userEntityName: tranUserFirmName
            }
          }
        }
      }
    }, {})

    // console.log(JSON.stringify({findUniqueUsers}, null, 2))

    const {tranInfo, tranRelatedEntities, tranRelatedUsers} = consTranDetails
    tranInfo[tran.activityId] = {
      ...restParams,
      ...entitlementInfo[tran.activityId]
    }
    tranRelatedEntities[tran.activityId] = revisedPartEntDetails
    tranRelatedUsers[tran.activityId] = findUniqueUsers
    return {tranInfo, tranRelatedEntities, tranRelatedUsers}
  }, {
    tranInfo: {},
    tranRelatedEntities: {},
    tranRelatedUsers: {}
  })

  // console.log("TOTAL OBJECTS", transactions.length)
  return {
    [tranType]: {
      transactions:revisedRawTransformedTransactions,
      transformedTrans
    }
  }
}

export const getAllTransactionDetailsForLoggedInUser = async(user) => {
  const tranTypes = ["deals", "rfps", "bankloans", "derivatives", "marfps","others","busdev"]
  // const tranTypes = [ "derivatives"]
  try {
    const {data} = await getEligibleTransactionsForLoggedInUserRevised(user)
    // console.log(JSON.stringify(data,null,2))
    const overallTaskInputDetailedData = await(tranTypes || []).reduce(async(allTaskInputDetails, tranType) => {
      const processData = data && data[tranType]

      const tranObjectIds = (processData || []).map(d => ObjectID(d.tranId))
      const tranlkupObjects = (processData || []).reduce((acc,d) => {
        return {...acc,[ObjectID(d.tranId)]:d}
      },{})

      // console.log(JSON.stringify(tranlkupObjects,null,2))
      // console.log(`TRAN OBJECT IDS - ${tranType}`, JSON.stringify(tranObjectIds, null, 2))

      const taskRelatedData = await getAllKeyTransactionDetailsForUser(user, tranType, tranObjectIds,tranlkupObjects)
      // console.log(JSON.stringify(searchEligibleData,null,2))
      const allTaskInputDetailsAcc = await allTaskInputDetails
      return Promise.resolve({
        ...allTaskInputDetailsAcc,
        ...taskRelatedData
      })

    }, Promise.resolve({}))

    // console.log(JSON.stringify(overallTaskInputDetailedData, null, 2))
    return {status: "success", message: `Obtained all Task Related Transactions  - ${user._id} -${tranTypes}`, payload: overallTaskInputDetailedData}
  } catch (e) {
    console.log("Error while fetch eligible transactions for users", e)
    return {status: "fail", message: `Error fetching transaction details for tasks - ${user._id} - ${tranTypes}`, payload: undefined}
  }
}
