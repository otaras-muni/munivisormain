import { EntityRel } from "./../../api/appresources/models"

const {ObjectID} = require("mongodb")

export const checkDuplicateEmailForTenant = async(user, emailsToValidate, userObjectId) => {

  console.log("1. ENTERED DUPLICATE CHECK PROCESS",{user,emailsToValidate,userObjectId})

  // Pass the Logged In User Details
  // Get the Logged In User
  // Find th related Entity
  // Find the tenant from the Entity
  // Get all entities for the tenant
  // Get all users for all entities
  // Validate if the emailsSubmittedAre in the List

  const allEmails = emailsToValidate.reduce( (allEmails,eml) => {
    const {emailId} = eml
    return [...allEmails,emailId.toLowerCase()]
  },[])

  console.log("2. ALL EMAIL DETAILS",allEmails, user)


  console.log(allEmails)

  const dynamicQueryPipeline = [
    {$match:{entityParty2:ObjectID(user.entityId)}},
    {
      $lookup:
            {
              from: "entityrels",
              localField: "entityParty1",
              foreignField: "entityParty1",
              as: "alltenantentities"
            }
    },
    { $unwind : "$alltenantentities"},
    { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}},
    {
      $lookup:
            {
              from: "entityusers",
              localField: "entityId",
              foreignField: "entityId",
              as: "alltenantentityusers"
            }
    },
    {
      $unwind:"$alltenantentityusers"
    },
    {
      $match:{"alltenantentityusers._id":{$ne:ObjectID(userObjectId)}}
    },
    {
      $project:{
        entityId:1,
        userId:"$alltenantentityusers._id",
        userEmails:"$alltenantentityusers.userEmails.emailId",
        userLoginEmail:"$alltenantentityusers.userLoginCredentials.userEmailId",
      }
    },
    {
      $addFields:{allUniqueEmails:{ $setUnion: [ "$userEmails",["$userLoginEmail" ]] }}
    },
    {
      $unwind:"$allUniqueEmails"
    },
    {
      $project:{entityId:1,userId:1,emailId:{ $toLower: "$allUniqueEmails" }}
    },
    {"$match": {"emailId": {"$in": [...allEmails]}}},
  ]

  let finalQuery = null
  if (userObjectId && (userObjectId !== "")) {
    finalQuery = [...dynamicQueryPipeline]
  } else {
    dynamicQueryPipeline.splice(6,1)
    finalQuery = [...dynamicQueryPipeline]

  }

  console.log("3. FINAL QUERY AFTER DOING ALL THE ELIMINATIONS",finalQuery)

 
  const uniqueEmails = await EntityRel.aggregate([...finalQuery])

  const dupEmailsInEmailsObject = uniqueEmails.reduce ( (dupEmailsAcc,eml) => {
    const dupEmails = emailsToValidate.filter( f => f.emailId.toLowerCase() === eml.emailId.toLowerCase())
    return [...dupEmailsAcc,...dupEmails]
  },[])
  console.log("THE DUPLICATE EMAILS RETURNED FROM DB SERVICE",JSON.stringify(uniqueEmails,null,2))
  console.log("DUPLICATE EMAILS IN INPUT OBJECT",JSON.stringify(dupEmailsInEmailsObject,null,2))

  return {duplicateEmailIds:dupEmailsInEmailsObject, duplicateExists:uniqueEmails.length > 0}
}

export const canEditPrimaryLoginEmailId = () => {
  // does the user have the permission to edit the primary address
  // is the user eligible to edit the user ID or user details
  // 
}

// checkDuplicateEmailForTenant({
//   _id:ObjectID("5b6064da8c4edb1624d88276"),
//   entityId:ObjectID("5b6064d88c4edb1624d87f82")
// },
// [{
//   emailId:"user6_prag@bc222.com"
//   isPrimary:"false"
// }]
// ).then( a => console.log(JSON.stringify(a,null,2)))
