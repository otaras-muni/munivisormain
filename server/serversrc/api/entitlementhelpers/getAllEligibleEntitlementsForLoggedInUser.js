import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"
import  {  EntityUser, EntityRel, UserEntitlements,Tasks }  from "./../appresources/models"
import { manageEligibleIdsForLoggedInUser} from "./entitlementUtilities"
import { getAllEntityAndUserInformationForTenant , getAllUsersForLoggedInUsersFirm} from "./../appresources/commonservices/getAllUserAndEntityDetails"


const findIntersection = (arr1, arr2) => arr1.filter((n) => arr2.indexOf(n) > -1)
const findUniqueIds = (arr1, arr2) => [...arr1,...arr2].reduce( (acc, id) =>  acc.includes(id) ? [...acc]:[...acc,id],[])

const getEntityAndUserDetails =async (user) => {

  const {relationshipType} = await EntityRel.findOne({entityParty2:ObjectID(user.entityId)}).select({"relationshipType":1,_id:0})

  const lkupConfig = {
    "Self":{
      lkup:[
        {
          $lookup:
              {
                from: "entityrels",
                localField: "entityParty1",
                foreignField: "entityParty1",
                as: "alltenantentities"
              }
        },
        { $unwind : "$alltenantentities"},
        { $project: {entityId:"$alltenantentities.entityParty2", relType:"$alltenantentities.relationshipType"}},
      ]
    },
    "Third Party":{
      lkup:[
        { $project: {entityId:"$entityParty2", relType:"$relationshipType"}},
      ]
    },
    "Client":{
      lkup:[
        { $project: {entityId:"$entityParty2", relType:"$relationshipType"}},
      ]
    },
    "Prospect":{
      lkup:[
        { $project: {entityId:"$entityParty2", relType:"$relationshipType"}},
      ]
    },
    "Undefined":{
      lkup:[
        { $project: {entityId:"$entityParty2", relType:"$relationshipType"}},
      ]
    }
  }

  const allEntitiesAndUsers = await EntityRel.aggregate([
    {$match:{entityParty2:ObjectID(user.entityId), relationshipType:{$in:["Client","Prospect","Third Party","Self"]}}},
    ...lkupConfig[relationshipType].lkup,
    {
      $lookup:
          {
            from: "entityusers",
            localField: "entityId",
            foreignField: "entityId",
            as: "alltenantentityusers"
          }
    },
    {
      $project:{
        _id:0,
        entityId:1,
        userIds:"$alltenantentityusers._id",
      }
    },
  ])

  const entities = allEntitiesAndUsers.map( ({entityId}) => entityId )
  const entityUsers = allEntitiesAndUsers.reduce( (acc,{entityId,userIds}) => ({...acc, [entityId]:userIds}),{})
  const allUsers = allEntitiesAndUsers.reduce( (acc, {userIds}) => [...acc, ...userIds],[])
  return {allEntitiesAndUsers,entities, entityUsers,allUsers}
}

export const configurationParameters = async (user, allFirmUsersArray,configObject) => {

  // console.log("values passed",{user, allFirmUsersArray,configObject})
  const {type, ent} = configObject

  const baseConfig = {
    "global-edit":{
      "TranUserSelection": {$match: { $or: [{"tranAllUsers": {"$in": [ObjectID(user._id)]}},{tranAllFirms:{"$in":[ObjectID(user.entityId)]}}]} },
      "TranMyFirmUsers":{tranMyFirmUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      "TranMyDealTeamUsers":{tranMyDealTeamUsers:"$tranAllUsers"},
      "TranCanEdit":{canEdit:true},
      "TranCanView":{canView:true},
    },
    "global-readonly":{
      "TranUserSelection": {$match: { $or: [{"tranAllUsers": {"$in": [ObjectID(user._id)]}},{tranAllFirms:{"$in":[ObjectID(user.entityId)]}}]} },
      "TranMyFirmUsers":{tranMyFirmUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      "TranMyDealTeamUsers":{tranMyDealTeamUsers:"$tranAllUsers"},
      "TranCanEdit":{canEdit:false},
      "TranCanView":{canView:true},
    },
    "tran-edit":{
      "TranUserSelection": {$match: {"tranAllUsers": {"$in": [ObjectID(user._id)]}}},
      "TranMyFirmUsers":{tranMyFirmUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      "TranMyDealTeamUsers":{tranMyDealTeamUsers:"$tranAllUsers"},
      "TranCanEdit":{canEdit:true},
      "TranCanView":{canView:true},

    },
    "tran-readonly":{
      "TranUserSelection": {$match: {"tranAllUsers": {"$in": [ObjectID(user._id)]}}},
      "TranMyFirmUsers":{tranMyFirmUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      "TranMyDealTeamUsers":{tranMyDealTeamUsers:"$tranAllUsers"},
      "TranCanEdit":{canEdit:false},
      "TranCanView":{canView:true},

    },
    "global-readonly-tran-edit":{
      "TranUserSelection": {$match: { $or: [{"tranAllUsers": {"$in": [ObjectID(user._id)]}},{tranAllFirms:{"$in":[ObjectID(user.entityId)]}}]} },
      "TranMyFirmUsers":{tranMyFirmUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      "TranMyDealTeamUsers":{tranMyDealTeamUsers:"$tranAllUsers"},
      "TranCanEdit":{canEdit: {"$in": [ObjectID(user._id),"$tranAllUsers"]}},
      "TranCanView":true,

    }
  }

  const CONFIG ={
    "Client":{
      ...baseConfig
    },
    "Prospect":{
      ...baseConfig
    },
    "Third Party":{
      "global-edit":{
        ...baseConfig["global-edit"],
        "TranMyDealTeamUsers":{tranMyDealTeamUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      },
      "global-readonly":{
        ...baseConfig["global-readonly"],
        "TranMyDealTeamUsers":{tranMyDealTeamUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      },
      "tran-edit":{
        ...baseConfig["tran-edit"],
        "TranMyDealTeamUsers":{tranMyDealTeamUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      },
      "tran-readonly":{
        ...baseConfig["tran-readonly"],
        "TranMyDealTeamUsers":{tranMyDealTeamUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      },
      "global-readonly-tran-edit":{
        ...baseConfig["global-readonly-tran-edit"],
        "TranMyDealTeamUsers":{tranMyDealTeamUsers:{ $setIntersection: [ "$tranAllUsers", allFirmUsersArray ] }},
      }

    },
    "Self":{
      ...baseConfig
    }
  }
  // console.log("OVERALL PIPELINE", JSON.stringify(CONFIG[type][ent],null,2))
  return CONFIG[type][ent]

}

export const transactionQueryParameters = (type) => {

  const tranQueryConfig = {
    deals:{
      lkup:{
        $lookup:{
          from: "tranagencydeals",
          localField: "entityParty1",
          foreignField: "dealIssueTranClientId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.dealIssueTranIssuerId",
      tranAllFirms:[
        ["$transactions.dealIssueTranClientId","$transactions.dealIssueTranIssuerId"],
        { $ifNull: ["$transactions.dealIssueParticipants.dealPartFirmId", []] },
        { $ifNull: ["$transactions.dealIssueUnderwriters.dealPartFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: ["$transactions.dealIssueParticipants.dealPartContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.dealIssueTranAssignedTo" } ] },
            then: { $ifNull: ["$transactions.dealIssueTranAssignedTo", []] },
            else: { $ifNull: [["$transactions.dealIssueTranAssignedTo"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
    rfps: {
      lkup:{
        $lookup:{
          from: "tranagencyrfps",
          localField: "entityParty1",
          foreignField: "rfpTranClientId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.rfpTranClientId",
      tranAllFirms:[
        ["$transactions.rfpTranClientId","$transactions.rfpTranIssuerId"],
        { $ifNull: ["$transactions.rfpEvaluationTeam.rfpSelEvalFirmId", []] },
        { $ifNull: ["$transactions.rfpProcessContacts.rfpProcessFirmId", []] },
        { $ifNull: ["$transactions.rfpParticipants.rfpParticipantFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: [ "$transactions.rfpParticipants.rfpParticipantContactId", []] },
        { $ifNull: [ "$transactions.rfpEvaluationTeam.rfpSelEvalContactId", []] },
        { $ifNull: [ "$transactions.rfpProcessContacts.rfpProcessContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.rfpTranAssignedTo" } ] },
            then: { $ifNull: ["$transactions.rfpTranAssignedTo", []] },
            else: { $ifNull: [["$transactions.rfpTranAssignedTo"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
    bankloans: {
      lkup:{
        $lookup:{
          from: "tranbankloans",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.actTranFirmId",
      tranAllFirms:[
        ["$transactions.actTranFirmId","$transactions.actTranClientId"],
        { $ifNull: ["$transactions.bankLoanParticipants.partFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: [ "$transactions.bankLoanParticipants.partContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.actTranFirmLeadAdvisorId" } ] },
            then: { $ifNull: ["$transactions.actTranFirmLeadAdvisorId", []] },
            else: { $ifNull: [["$transactions.actTranFirmLeadAdvisorId"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
    derivatives: {
      lkup:{
        $lookup:{
          from: "tranderivatives",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.actTranFirmId",
      tranAllFirms:[
        ["$transactions.actTranFirmId","$transactions.actTranClientId"],
        { $ifNull: ["$transactions.derivativeParticipants.partFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: [ "$transactions.derivativeParticipants.partContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.actTranFirmLeadAdvisorId" } ] },
            then: { $ifNull: ["$transactions.actTranFirmLeadAdvisorId", []] },
            else: { $ifNull: [["$transactions.actTranFirmLeadAdvisorId"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
    marfps: {
      lkup:{
        $lookup:{
          from: "actmarfps",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.actTranFirmId",
      tranAllFirms:[
        ["$transactions.actTranFirmId","$transactions.actIssuerClient"],
        { $ifNull: ["$transactions.maRfpParticipants.partFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: [ "$transactions.maRfpParticipants.partContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.actLeadFinAdvClientEntityId" } ] },
            then: { $ifNull: ["$transactions.actLeadFinAdvClientEntityId", []] },
            else: { $ifNull: [["$transactions.actLeadFinAdvClientEntityId"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
    other: {
      lkup:{
        $lookup:{
          from: "tranagencyothers",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.actTranFirmId",
      tranAllFirms:[
        ["$transactions.actTranFirmId","$transactions.actTranClientId"],
        { $ifNull: ["$transactions.participants.partFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: [ "$transactions.participants.partContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.actTranFirmLeadAdvisorId" } ] },
            then: { $ifNull: ["$transactions.actTranFirmLeadAdvisorId", []] },
            else: { $ifNull: [["$transactions.actTranFirmLeadAdvisorId"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
    busdev: {
      lkup:{
        $lookup:{
          from: "actbusdevs",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "transactions"
        }
      },
      tranTenantId:"$transactions.actTranFirmId",

      tranAllFirms:[
        ["$transactions.actTranFirmId","$transactions.actIssuerClient"],
        { $ifNull: ["$transactions.participants.partFirmId", []] },
      ],
      tranAllUsers:[
        { $ifNull: [ "$transactions.participants.partContactId", []] },
        { $ifNull: [
          { $cond:{
            if: { $and: [ { $isArray: "$transactions.actLeadFinAdvClientEntityId" } ] },
            then: { $ifNull: ["$transactions.actLeadFinAdvClientEntityId", []] },
            else: { $ifNull: [["$transactions.actLeadFinAdvClientEntityId"], []] }
          }
          }
          , []] },
        { $ifNull: [["$transactions.createdByUser"], []] },
      ]
    },
  }

  return tranQueryConfig[type]
}

export const coreTransactionEntitlementMapper = async (user,type,config) => {
  const {lkup,tranTenantId,tranAllFirms,tranAllUsers} = transactionQueryParameters(type)
  const trans = await EntityRel.aggregate([
    {$match:{entityParty2:ObjectID(user.entityId)}},
    {
      ...lkup
    },
    {$unwind:"$transactions"},
    {
      $project: {
        _id:0,
        loggedInUserId:user._id,
        loggedInUserFirmId:user.entityId,
        relationshipToTenant:"$relationshipType",
        tranId:"$transactions._id",
        tranTenantId,
        tranAllFirms:{
          $setUnion: [
            ...tranAllFirms
          ]
        },
        tranAllUsers:{
          $setUnion: [
            ...tranAllUsers
          ]
        },
      }
    },
    {
      $addFields:{
        ...config.TranMyFirmUsers,
        ...config.TranMyDealTeamUsers,
        ...config.TranCanEdit,
        ...config.TranCanView
      }
    },
    {
      ...config.TranUserSelection
    },
  ])

  // console.log(JSON.stringify(trans,null,2))
  return trans

}

export  const getTasksForLoggedInUser = async (user, eligibleUserEntities, eligibleTransactions) => {

  const {editentities} = eligibleUserEntities
  
  const editTranIds = (eligibleTransactions && eligibleTransactions.tranEditIds || []).map( a => ObjectID(a))
  const editEntityIds = (editentities || []).map(a => ObjectID(a))
  
  try {
    const { userEntitlement,relationshipType,userFlags } = user
    const isSuperVisor = (userFlags || []).includes("Compliance Officer") || (userFlags || []).includes("Supervisory Principal")
    const getFirmTasks =  (relationshipType === "Self" && userEntitlement === "global-edit") || 
                          (relationshipType === "Self" && isSuperVisor ) ||
                          (relationshipType === "Client" && userEntitlement === "global-edit") || 
                          (relationshipType === "Third Party" && userEntitlement === "global-edit")
                          
    let tasksForAllTenantUsers = []
    let tasksForMyFirmUsers = []
    
    if ( (relationshipType === "Self" && userEntitlement === "global-edit") || (relationshipType === "Self" && isSuperVisor ) )
    {
      const allTenantUsers = await getAllEntityAndUserInformationForTenant(user)
      const allTenantUserIds = allTenantUsers.map( t => t.userId )
      tasksForAllTenantUsers = await Tasks.aggregate([
        {
          $match: {
            "taskDetails.taskAssigneeUserId":{$in:allTenantUserIds}
          }
        },
        {
          $project:{
            _id:0,
            taskId:"$_id",
            taskRefId:1,
            taskUserId:"$taskDetails.taskAssigneeUserId",
            taskStatus:"$taskDetails.taskStatus"
          }
        }
      ])
    }
    
    if (getFirmTasks) {
      const allFirmUsers = await getAllUsersForLoggedInUsersFirm(user)
      const allFirmUserIds = allFirmUsers.map( t => ObjectID(t.userId) )
      tasksForMyFirmUsers = await Tasks.aggregate([
        {
          $match: {
            "taskDetails.taskAssigneeUserId":{$in:allFirmUserIds}
          }
        },
        {
          $project:{
            _id:0,
            taskId:"$_id",
            taskRefId:1,
            taskUserId:"$taskDetails.taskAssigneeUserId",
            taskStatus:"$taskDetails.taskStatus"
          }
        }
      ])
    }

    const tasksForMe = await Tasks.aggregate([
      {
        $match: {
          "taskDetails.taskAssigneeUserId":ObjectID(user._id)
        }
      },
      {
        $project:{
          _id:0,
          taskId:"$_id",
          taskRefId:1,
          taskUserId:"$taskDetails.taskAssigneeUserId",
          taskStatus:"$taskDetails.taskStatus"
        }
      }
    ])

    const tasksAssignedToParticipantFirmsThatUserCanAccess = await Tasks.aggregate([
      {
        $match: {
          $and:[
            {"taskDetails.taskAssigneeUserId":{$in:editEntityIds}},
            {"relatedActivityDetails.activityId":{$in:editTranIds}}
          ]
        }
      },
      {
        $project:{
          _id:0,
          taskId:"$_id",
          taskRefId:1,
          taskUserId:"$taskDetails.taskAssigneeUserId",
          taskStatus:"$taskDetails.taskStatus"
        }
      }
    ])


    const allTaskIdsForTenant = [
      ...tasksForAllTenantUsers.map( a => a.taskId ),
      ...tasksForAllTenantUsers.reduce( (acc,a) => {
        if( a.taskId && a.taskId.length > 0) {
          return [...acc,a.taskRefId]
        }
        return [...acc]
      },[])]
    const allTaskIdsForFirm = [
      ...tasksForMyFirmUsers.map( a => a.taskId ),
      ...tasksForMyFirmUsers.reduce( (acc,a) => {
        if( a.taskId && a.taskId.length > 0) {
          return [...acc,a.taskRefId]
        }
        return [...acc]
      },[])]
    const allTaskIdsForMe = [
      ...tasksForMe.map( a => a.taskId ),
      ...tasksForMe.reduce( (acc,a) => {
        if( a.taskId && a.taskId.length > 0) {
          return [...acc,a.taskRefId]
        }
        return [...acc]
      },[])]
 
    const allTaskIdsForFirmAssignments = [
      ...tasksAssignedToParticipantFirmsThatUserCanAccess.map ( a => a.taskId),
      ...tasksAssignedToParticipantFirmsThatUserCanAccess.reduce( (acc,a) => {
        if( a.taskId && a.taskId.length > 0) {
          return [...acc,a.taskRefId]
        }
        return [...acc]
      },[])]
      

    const resultMapper = {
      "Self":{
        "global-edit": {"edit": findUniqueIds(allTaskIdsForTenant,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForTenant,allTaskIdsForFirmAssignments)},
        "global-readonly": {"edit":[],"view":findUniqueIds(allTaskIdsForTenant,allTaskIdsForFirmAssignments)},
        "tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "tran-readonly": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "global-readonly-tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)}
      },

      "Client":{
        "global-edit": {"edit":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments)},
        "global-readonly": {"edit":[],"view":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments)},
        "tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "tran-readonly": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "global-readonly-tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)}
      },
      "Prospect":{
        "global-edit": {"edit":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments)},
        "global-readonly": {"edit":[],"view":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments)},
        "tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "tran-readonly": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "global-readonly-tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)}
      },
      "Third Party":{
        "global-edit": {"edit":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments)},
        "global-readonly": {"edit":[],"view":findUniqueIds(allTaskIdsForFirm,allTaskIdsForFirmAssignments)},
        "tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "tran-readonly": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)},
        "global-readonly-tran-edit": {"edit":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments),"view":findUniqueIds(allTaskIdsForMe,allTaskIdsForFirmAssignments)}
      },
    }
    // console.log("The Eligible Tasks are as follows", resultMapper[relationshipType][userEntitlement])
    return resultMapper[relationshipType][userEntitlement]
  } catch (e) {
    console.log("There is an error", e)
    return { edit:[], view:[]}
  }
}


export const getEligibleEntitiesAndUsers = async (allConsolidated, user, configObject) => {
  const loggedInUser = user._id

  const tranAllFirms = (allConsolidated && allConsolidated.tranAllFirms) || []
  const tranAllUsers = (allConsolidated && allConsolidated.tranAllUsers) || []

  // const {tranAllFirms,tranAllUsers,tranMyDealTeamUsers} = allConsolidated
  const {entities, entityUsers, allUsers} = await getEntityAndUserDetails(user)
  const { type, ent} = configObject

  const entityUserDataMapper = {
    "Self":{
      "global-edit": {"editentities":findUniqueIds(tranAllFirms,entities),"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":allUsers,"viewusers":allUsers},
      "global-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[loggedInUser],"viewusers":allUsers},
      "tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[loggedInUser],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[loggedInUser],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly-tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[loggedInUser],"viewusers":findUniqueIds(allUsers,tranAllUsers)}
    },
    "Client":{
      "global-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly-tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)}
    },
    "Prospect":{
      "global-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly-tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)}
    },
    "Third Party":{
      "global-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "tran-readonly": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)},
      "global-readonly-tran-edit": {"editentities":[],"viewentities":findUniqueIds(tranAllFirms,entities),"editusers":[],"viewusers":findUniqueIds(allUsers,tranAllUsers)}
    },
  }
  return entityUserDataMapper[type][ent]
}

export const getConsolidatedAccessAndEntitlementsForLoggedInUserDetailed = async (user, reqtype) => {
  // deals, rfps, derivatives, users, entities, nav, tasks
  // Get all Users of the User of the Current Firm

  try {

    const allFirmUsers = await EntityUser.aggregate([
      {$match:{entityId:ObjectID(user.entityId)}},
      {$project:{_id:1}}
    ])
    const allFirmUsersArray = allFirmUsers.map( f => f._id)
    // Get the relationship Type
    const {relationshipType} = await EntityRel.findOne({entityParty2:ObjectID(user.entityId)}).select({"relationshipType":1,_id:0})
    // get entitlement of user
    if ( relationshipType === "Undefined") {
      return {}
    }

    const ent = user && user.userEntitlement
    // Get Configuration details
    const configObject = {type:relationshipType,ent}
    const config = await configurationParameters(user,allFirmUsersArray,configObject)

    // console.log("1. THE CONFIGURATION PARAMETERS ARE", JSON.stringify(config,null,2))
    /* const deals = await coreTransactionEntitlementMapper(user,"deals",config)
    const rfps = await coreTransactionEntitlementMapper(user,"rfps",config)
    const bankloans = await coreTransactionEntitlementMapper(user,"bankloans",config)
    const derivatives = await coreTransactionEntitlementMapper(user,"derivatives",config)
    const marfps = await coreTransactionEntitlementMapper(user,"marfps",config)
    const others = await coreTransactionEntitlementMapper(user,"other",config)
    const busdev = await coreTransactionEntitlementMapper(user,"busdev",config) */

    const start1 = new Date()

    const [ deals, rfps, bankloans, derivatives, marfps, others, busdev] = await Promise.all([
      coreTransactionEntitlementMapper(user,"deals",config),
      coreTransactionEntitlementMapper(user,"rfps",config),
      coreTransactionEntitlementMapper(user,"bankloans",config) ,
      coreTransactionEntitlementMapper(user,"derivatives",config),
      coreTransactionEntitlementMapper(user,"marfps",config),
      coreTransactionEntitlementMapper(user,"other",config),
      coreTransactionEntitlementMapper(user,"busdev",config)
    ])

    const time1 = new Date() - start1


    const allTrans = [...(deals || []),...(rfps || []),...(bankloans || []),...(derivatives || []),...(marfps || []), ...(others || []),...(busdev || [])]
    // console.log("3. ALL CONSOLIDATED TRANSACTIONS", JSON.stringify({allTrans},null,2))

    const start2 = new Date()

    const allConsolidated = (allTrans || []).reduce( (acc,tr) => {
      const {tranAllFirms,tranAllUsers, tranMyFirmUsers,tranMyDealTeamUsers,tranId,canEdit,canView} = tr
      // console.log( JSON.stringify(tranAllFirms,null,2))
      return {
        tranEntitlements:{...(acc && acc.tranEntitlements || []),[tranId]:{canEdit,canView}},
        tranEditIds:canEdit ? [...(acc && acc.tranEditIds || []),tranId] : [...(acc && acc.tranEditIds || [])],
        tranViewIds:canView ? [...(acc && acc.tranViewIds || []),tranId] : [...(acc && acc.tranViewIds || [])],
        tranAllFirms:[...new Set([...(acc && acc.tranAllFirms || []),...(tranAllFirms || [])])],
        tranAllUsers:[...new Set([...(acc && acc.tranAllUsers || []),...(tranAllUsers || [])])],
        tranMyFirmUsers:[...new Set([...(acc && acc.tranMyFirmUsers || []),...(tranMyFirmUsers || [])])],
        tranMyDealTeamUsers:[...new Set([...(acc && acc.tranMyDealTeamUsers || []),...(tranMyDealTeamUsers || [])])],
      }
    },{})

    const time2 = new Date() - start2


    // console.log("4.  POST AGGREGATING THROUGH REDUCE", JSON.stringify({allConsolidated},null,2))

    const start3 = new Date()
    const entityUserEntitlementObject = await getEligibleEntitiesAndUsers(allConsolidated,user,configObject)
    const {editentities,viewentities,editusers,viewusers} = entityUserEntitlementObject
    const uniqEntityIds = [...editentities || [],...viewentities || []].reduce( (acc, id) =>  acc.includes(id) ? [...acc]:[...acc,id],[])
    const uniqUserIds = [...editusers || [],...viewusers || []].reduce( (acc, id) =>  acc.includes(id) ? [...acc]:[...acc,id],[])
    const time3 = new Date() - start3

    const start4 = new Date()
    const eligibleTasks = await getTasksForLoggedInUser(user,entityUserEntitlementObject,allConsolidated)
    const time4 = new Date() - start4
    // console.log("The tasks from the service - eligibleTasks", eligibleTasks)

    // console.log("5.  GETTING ELIGIBLE ENTITIES AND USERS", JSON.stringify({entityUserEntitlementObject},null,2))


    // Construct Edit
    const entities = {
      edit:editentities || [],
      view:viewentities || [],
      all:uniqEntityIds || []
    }

    // Construct users Object
    const users = {
      edit:editusers || [],
      view:viewusers || [],
      all:uniqUserIds || []
    }

    // console.log("6.  SETTING SOME EFAULTS", JSON.stringify({entities, users},null,2))


    // Get Edit Ids
    const editIds = [...(allConsolidated && allConsolidated.tranEditIds || []),...editentities || [],...editusers ||[],...eligibleTasks.edit]
    // Get View IDs
    const viewIds = [...(allConsolidated && allConsolidated.tranViewIds || []),...viewentities || [],...viewusers || [],...eligibleTasks.view]
    // Get Unique Ids
    const uniqIds = [...editIds,...viewIds].reduce( (acc, id) =>  acc.includes(id) ? [...acc]:[...acc,id],[])

    // console.log("7.  FINAL FRONTIER", JSON.stringify({entities, users},null,2))
    const consolidatedEditViewFlagForNavMain = {
      edit:[...editIds],
      view:[...viewIds],
      all: [...uniqIds]
    }
    console.log( {time1,time2,time3,time4} )
    const returnObject = {deals,rfps,bankloans, derivatives, marfps,others,busdev,tasks:eligibleTasks,consolidated:allConsolidated,entities,users,nav:consolidatedEditViewFlagForNavMain}
    // console.log("8. HERE IS THE RETURN OBJECT", JSON.stringify({returnObject},null,2))
    return reqtype==="all" ? returnObject : returnObject[reqtype]
  } catch (e) {
    console.log("There was an error getting the entitlement details", e)
    return {}
  }

}

export const getConsolidatedAccessAndEntitlementsForLoggedInUser = async (user, reqtype) => {
  // console.log("THE USER IS", user, reqtype)

  const { relationshipType, userEntitlement} = user

  if ( relationshipType === "Self" && ["global-edit"].includes(userEntitlement)) {
    return {data:{},checkIdForExistance:false,ent:{ edit:true, view:true}}
  }
  if ( relationshipType === "Self" && ["global-readonly"].includes(userEntitlement)) {
    return {data:{},checkIdForExistance:false,ent:{ edit:false, view:true}}
  }
  const entitlementInfo  = await UserEntitlements.findOne({entityId:ObjectID(user.entityId), userId:ObjectID(user._id)})
  const accessInfo = entitlementInfo && entitlementInfo.accessInfo || {}

  if( isEmpty(accessInfo) ) {
    await manageEligibleIdsForLoggedInUser(user)
    const newAccessInfo = await UserEntitlements.findOne({entityId:ObjectID(user.entityId), userId:ObjectID(user._id)})
    const retValue =  reqtype==="all" ? newAccessInfo.accessInfo : newAccessInfo.accessInfo[reqtype]
    return {data:retValue,checkIdForExistance:true,...retValue }
  }
  // console.log("The accessInfo", accessInfo)
  const retValue = reqtype==="all" ? accessInfo : accessInfo[reqtype]
  return {data:retValue,checkIdForExistance:true,...retValue }

}

export const getAllAccessAndEntitlementsForLoggedInUser = async (user, reqtype) => {
  // console.log("THE USER IS", user, reqtype)
  const entitlementInfo  = await UserEntitlements.findOne({entityId:ObjectID(user.entityId), userId:ObjectID(user._id)})
  const accessInfo = entitlementInfo && entitlementInfo.accessInfo || {}

  if( isEmpty(accessInfo) ) {
    await manageEligibleIdsForLoggedInUser(user)
    const newAccessInfo = await UserEntitlements.findOne({entityId:ObjectID(user.entityId), userId:ObjectID(user._id)})
    return reqtype==="all" ? newAccessInfo && newAccessInfo.accessInfo : newAccessInfo && newAccessInfo.accessInfo[reqtype]
  }
  // console.log("The accessInfo", accessInfo)
  return reqtype==="all" ? accessInfo : accessInfo[reqtype]
}

export const getEntitlementDetailsForIds = async ( user, ids) => {

  const {_id:userId, userEntitlement,relationshipType} = user

  if( userEntitlement === "global-edit" && relationshipType === "Self"){
    return (ids || []).reduce( (a,k) => ({...a,[k]:{edit:true, view:true}}),{})
  }

  if( userEntitlement === "global-readonly" && relationshipType === "Self"){
    return (ids || []).reduce( (a,k) => ({...a,[k]:{edit:false, view:true}}),{})
  }

  const entitlementDetails = await Promise.all(ids.map(k =>
    UserEntitlements.aggregate([
      {
        $match:{ userId:ObjectID(userId)}
      },
      {
        $project:{
          _id:0,
          idToCheck:ObjectID(k),
          edit:{$in:[ObjectID(k), "$accessInfo.nav.edit"]},
          view:{
            "$or":[
              {$in:[ObjectID(k), "$accessInfo.nav.edit"]},
              {$in:[ObjectID(k), "$accessInfo.nav.view"]},
            ]
          }
        }
      }
    ])))

  return entitlementDetails.reduce( (a, k) => [...a,...k],[]).reduce ( (a,k) => ({...a,[k.idToCheck]:k}),{})
}
