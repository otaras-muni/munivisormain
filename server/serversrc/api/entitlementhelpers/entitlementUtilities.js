import {ObjectID} from "mongodb"
import { 
  getConsolidatedAccessAndEntitlementsForLoggedInUserDetailed,
  getConsolidatedAccessAndEntitlementsForLoggedInUser
} from "./getAllEligibleEntitlementsForLoggedInUser"
import { UserEntitlements, EntityRel } from "./../../api/appresources/models"
import  logger from "./../../api/modules/logger"


export const doesIdExistsInEntitledEntityIds = async (user, checkId, perm="view") => {
 
  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly","global-readonly-tran-edit"].includes(userEntitlement)) {
    return true
  }
 
  const validIds = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,"entities")
  let ids = (validIds && validIds[perm]) || []  
  ids = ids.map(id=>id.toString())  
  return ids.includes(checkId)
}

export const doesIdExistsInEntitledUsers = async (user, checkId, perm="view") => {

  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly","global-readonly-tran-edit"].includes(userEntitlement)) {
    return true
  }

  const validIds = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,"users")
  let ids = (validIds && validIds[perm]) || []
  ids = ids.map(id=>id.toString()) 
  return ids.includes(checkId)
}

export const getAllEligibleIds = async (user,perm="view") => {
  
  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly","global-readonly-tran-edit"].includes(userEntitlement)) {
    return true
  }
  
  const validIds= await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,"entities")
  let ids = (validIds && validIds[perm]) || []
  ids = ids.map(id=>id.toString())  
  return ids.map( i => ObjectID(i))
}

export const getAllEligibleUserIds = async (user,perm="view") => {
  const { relationshipType, userEntitlement} = user
  if ( relationshipType === "Self" && ["global-edit","global-readonly","global-readonly-tran-edit"].includes(userEntitlement)) {
    return true
  }

  const validIds = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,"users")
  const ids = (validIds && validIds[perm]) || [] 
  return ids.map( i => ObjectID(i))
}

export const manageEligibleIdsForLoggedInUser = async(user) => {
  
  try {
    const userId = user._id
    const {entityId,tenantId,relationshipType,userEntitlement} = user
    
    if ( relationshipType === "Self" && ["global-edit","global-readonly"].includes(userEntitlement)) {
      return {done:true, message:"Not writing entitlements for global roles", userDetails:{}}
    }
    
    const accessAndEntitlementInformation = await getConsolidatedAccessAndEntitlementsForLoggedInUserDetailed(user, "all")
    // console.log("The keys for entitlements", Object.keys(accessAndEntitlementInformation))
    /*    const tenantDetails = await EntityRel.aggregate([
      {
        $match:{entityParty2:ObjectID(entityId)}
      },
      {
        $project:{
          entityParty1:1
        }
      }
    ])
    const tenantId = tenantDetails[0].entityParty1 */

    const updatedUser = await UserEntitlements.findOneAndUpdate({
      tenantId,
      userId,
      entityId
    },
    {
      "$set": {
        accessInfo: accessAndEntitlementInformation || {}
      }
    },
    { upsert: true, new: true })

    return {done:true, message:"Successfully updated entitlements for the user", userDetails:updatedUser}
  } 
  catch(e) {
    console.log("Here is the error", e)
    return {done:false, message:"There was an error updating the entitlements for the user"}
  }
}

export const updateEligibleIdsForLoggedInUserFirm = async(user) => {
  // Get all the Users who are already existing in the entitlement
  try {
    const tenantId = ObjectID(user.tenantId)
    const entitledUsersFromUserFirm = await UserEntitlements.aggregate([
      {
        $match:{tenantId}
      },
      {
        $lookup:{
          from: "entityusers",
          localField: "userId",
          foreignField: "_id",
          as: "entititledusers"
        }
      },
      {
        $unwind:"$entititledusers"
      },
      {
        $project:{
          entityId:1,
          tenantId:1,
          _id:"$userId",
          userEntitlement:"$entititledusers.userEntitlement",
        }
      },
      {
        $lookup:{
          from: "entityrels",
          localField: "entityId",
          foreignField: "entityParty2",
          as: "obtainrelationshiptype"
        }
      },
      {
        $unwind:"$obtainrelationshiptype"
      },
      {
        $project:{
          entityId:1,
          tenantId:1,
          _id:1,
          userEntitlement:1,
          relationshipType:"$obtainrelationshiptype.relationshipType"
        }
      },
      {
        $addFields:{
          writeEntitlement: {
            "$switch": {
              "branches": [
                {
                  "case": {
                    $and:[
                      {"$eq":["$relationshipType","Self"]},
                      {$in:["$userEntitlement",["global-edit","global-readonly"]]},
                    ]},
                  "then": false
                } ],
              "default": true
            }
          }
        }
      },
      {
        $match:{writeEntitlement:true}
      }
    ])
    // logger.debug(`Here are the users of the Tenant whole entitlements will be impacted - ${JSON.stringify(entitledUsersFromUserFirm,null,2)}`)

    // For each User call the entitlement service and Update the entitlement Information
    /*
    const allUserDataUpdated = await (entitledUsersFromUserFirm || []).reduce(async (allretvalues, entitledUser) => {
      const newallretvalues = await allretvalues
      const {_id, entityId, userEntitlement} = entitledUser
      const retValue = await manageEligibleIdsForLoggedInUser({_id, entityId, userEntitlement})
      return Promise.resolve([...newallretvalues, retValue.done])
    },Promise.resolve([]))
    */

    // console.log(entitledUsersFromUserFirm)

    
    const allUserDataUpdated = await Promise.all((entitledUsersFromUserFirm || []).map(async(entitledUser) => {
      const {_id, entityId, userEntitlement,relationshipType} = entitledUser
      manageEligibleIdsForLoggedInUser({_id, entityId, userEntitlement,relationshipType})
    }))

    // logger.debug(`Post Update of Information- ${JSON.stringify(allUserDataUpdated,null,2)}`)
    // console.log("Post Update", allUserDataUpdated)

    if(allUserDataUpdated.includes(false)) {
      return {done:false, message:"Unable to update the entitlements for all the Users of the Firm"}
    } 
    return {done:true, message:`Successfully updated entitlement information for ${JSON.stringify(entitledUsersFromUserFirm.map(a => a._id))}`}
  } catch(e) {
    console.log("There is an Error in updating the User Data", e)
  }
}



export const updateEligibleIdsForLoggedInUserFirm_NEW = async(user) => {
  // Get all the Users who are already existing in the entitlement
  try {
    const { _id, entityId, userEntitlement, relationshipType} = user 

    if ( relationshipType === "Self" && ["global-edit","global-readonly","global-readonly-tran-edit"].includes(userEntitlement)) {
      return {done:true, message:`Not writing cache information for the logged in user : ${user.userFirstName}`}
    }
    
    const retValue = await manageEligibleIdsForLoggedInUser({_id, entityId, userEntitlement,relationshipType})

    if(retValue && retValue.done) {
      return {done:true, message:`Successfully updated entitlement information for ${user.userFirstName}`}
    } 
    return {done:false, message:"Unable to update the entitlements for the logged inuser"}
  } catch(e) {
    console.log("There is an Error in updating the User Data", e)
    return {done:false, message:"Unable to update the entitlements for all the Users of the Firm"}
  }
}

