import {ObjectID} from "mongodb"
import { getConsolidatedAccessAndEntitlementsForLoggedInUser } from "./getAllEligibleEntitlementsForLoggedInUser"

export const isCurrentUserEntitledToAccessID = async (user, checkId, type="entities", perm="view") => {
  try {

    const { relationshipType, userEntitlement} = user
    if ( relationshipType === "Self" && ["global-edit","global-readonly","global-readonly-tran-edit"].includes(userEntitlement)) {
      return true
    }
  
    const {data} = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,type)
    const ids = (data && data[perm]) || []
    return ids.reduce ( ( exists, entityId) => exists || entityId.toString() === checkId.toString(), false)
  } catch (e) {
    console.log("There is an error fetching entitlement flags", e)
    return false
  }
}

export const getAllEligibleIDsForSpecificType = async (user,type="entities", perm="view") => {
  try {
    const {data} = await getConsolidatedAccessAndEntitlementsForLoggedInUser(user,type)
    const ids = (data && data[perm]) || []
    return ids.map( i => ObjectID(i))
  }
  catch(e) {
    console.log("There is an error fetching entitlement flags", e)
    return false
  }
}

/* Example Usage

// Check if the entity exists and can I view the entity
isCurrentUserEntitledToAccessID(user,"5ba7c1c030580f39ec3b9b1a","entities","view")
  .then(a => console.log("I can view entity ", a))

// Check if the entity exists and can I edit the entity
isCurrentUserEntitledToAccessID(user,"5ba7c1c030580f39ec3b9b1a","entities","edit")
  .then(a => console.log("I can edit entity", a))

// Check if the entity exists and can I view the user
isCurrentUserEntitledToAccessID(user,"5ba7c1c130580f39ec3b9b8e","users","view")
  .then(a => console.log("I can view user", a))

// Check if the entity exists and can I view the user
isCurrentUserEntitledToAccessID(user,"5ba7c1c130580f39ec3b9b8e","users","edit")
  .then(a => console.log("I can edit user", a))


// Get all entitiies that I can view
getAllEligibleIDsForSpecificType(user,"entities","view")
  .then(a => console.log("entities that I can view", a))

// Get all entitiies that I can view
getAllEligibleIDsForSpecificType(user,"entities","edit")
  .then(a => console.log("entities that I can edit ", a))

// Get all users that I can view
getAllEligibleIDsForSpecificType(user,"users","view")
  .then(a => console.log("users I can view", a))

// Get all the users that I can edit
getAllEligibleIDsForSpecificType(user,"users","edit")
  .then(a => console.log("users I can edit", a))

  */
