
import {ObjectID} from "mongodb"
import { EntityRel } from "./../appresources/models" 

/* 
Get the transactions
Make sure it works for migrated data.
Consolidate all the users and entities who have access
Dynamically find the users and entities that the user is entitled to.
Get the tasks assoiated with the transactions that the user has access to.
Get the documents associated with the transactions
*/

const getTransactionTransformationQueries = (user, tranType) => {
 
  const queries = {
    deals: [
      {
        $lookup:{
          from: "tranagencydeals",
          localField: "tenantId",
          foreignField: "dealIssueTranClientId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueParticipants.dealPartContactId", []] },
                    else: { $ifNull: [["$trans.dealIssueParticipants.dealPartContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: {$ifNull:["$trans.dealIssueTranAssignedTo",[]]} } ] }, 
                  then: { $ifNull: ["$trans.dealIssueTranAssignedTo", []] },
                  else: { $ifNull: [["$trans.dealIssueTranAssignedTo"], []] }
                }
                }
                , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.createdByUser", []] } } ] },
                    then: { $ifNull: ["$trans.createdByUser", []] },
                    else: { $ifNull: [["$trans.createdByUser"], { $ifNull: ["$trans.createdByUser", []] }] }
                  }
                  }
                  , []] 
              }, 
            ]
          },
          tranEntities:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranClientId" , []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueTranClientId" , []] },
                    else: { $ifNull: [["$trans.dealIssueTranClientId" ], { $ifNull: ["$trans.dealIssueTranClientId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueTranIssuerId" , []] },
                    else: { $ifNull: [["$trans.dealIssueTranIssuerId"], { $ifNull: ["$trans.dealIssueTranIssuerId" , []] }] }
                  }
                  }
                  , []] 
              }, 
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueParticipants.dealPartFirmId", []] },
                    else: { $ifNull: [["$trans.dealIssueParticipants.dealPartFirmId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] } } ] },
                    then: { $ifNull: ["$trans.dealIssueUnderwriters.dealPartFirmId", []] },
                    else: { $ifNull: [["$trans.dealIssueUnderwriters.dealPartFirmId"], []] }
                  }
                  }
                  , []] 
              },
            ]
          },
          myFirmUserIds:"$loggedInFirmUserIds",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          allEntitiesAssociatedWithTenant:1,
          allUsersAssociatedWithTenant:1
        }
      }
    ],
    rfps:[
      {
        $lookup:{
          from: "tranagencyrfps",
          localField: "tenantId",
          foreignField: "rfpTranClientId", 
          as: "trans"
        }
      },
      {
        $unwind:"$trans"
      },
      {
        $project:{
          tranId:"$trans._id",
          tranUsers:{
            $setUnion:[
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: "$trans.rfpProcessContacts.rfpProcessContactId" } ] },
                    then: { $ifNull: ["$trans.rfpProcessContacts.rfpProcessContactId", []] },
                    else: { $ifNull: [["$trans.rfpProcessContacts.rfpProcessContactId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: "$trans.rfpEvaluationTeam.rfpSelEvalContactId" } ] },
                    then: { $ifNull: ["$trans.rfpEvaluationTeam.rfpSelEvalContactId", []] },
                    else: { $ifNull: [["$trans.rfpEvaluationTeam.rfpSelEvalContactId"], []] }
                  }
                  }
                  , []] 
              },
              { 
                $ifNull: [    
                  { $cond:{
                    if: { $and: [ { $isArray: "$trans.rfpParticipants.rfpParticipantContactId" } ] },
                    then: { $ifNull: ["$trans.rfpParticipants.rfpParticipantContactId", []] },
                    else: { $ifNull: [["$trans.rfpParticipants.rfpParticipantContactId"], []] }
                  }
                  }
                  , []] 
              },
              { $ifNull: [    
                { $cond:{
                  if: { $and: [ { $isArray: "$trans.rfpTranAssignedTo" } ] },
                  then: { $ifNull: ["$trans.rfpTranAssignedTo", []] },
                  else: { $ifNull: [["$trans.rfpTranAssignedTo"], []] }
                }
                }
                , []] },
              { $ifNull: [["$trans.createdByUser"], []] }
            ]
          },
          tranEntities:{
            $setUnion:[
              ["$trans.rfpTranClientId","$trans.rfpTranIssuerId"],
              "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
              "$trans.rfpProcessContacts.rfpProcessFirmId",
              "$trans.rfpParticipants.rfpParticipantFirmId",
            ]
          },
          myFirmUserIds:"$loggedInFirmUserIds",
          userEntitlement:user.userEntitlement,
          relationshipType:"$relationshipType",
          allEntitiesAssociatedWithTenant:1,
          allUsersAssociatedWithTenant:1
        }
      }
    ]
  }
  return queries[tranType]
}

export const getEntitlementForTransaction = async(user) => {

  console.log("ENTERED THE NEW ENTITLEMENT HELPER", JSON.stringify(user,null,2))

  const selfQuery = [
    {
      $match:{
        entityParty2:ObjectID(user.entityId),
        relationshipType:user.relationshipType
      }
    },
    {
      $project:{
        tenantId:"$entityParty1",
        loggedInUserEntityId:"$entityParty2",
        relationshipType:"$relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "loggedInUserEntityId",
        foreignField: "entityId",
        as: "myFirmUsers"
      }
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:"$myFirmUsers._id",
      }
    },
    {
      $lookup:{
        from: "entityrels",
        localField: "tenantId",
        foreignField: "entityParty1",
        as: "allEntitiesForTenant"
      }
    },
    {
      $unwind: "$allEntitiesForTenant"
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:1,
        tenantRelatedEntityId:"$allEntitiesForTenant.entityParty2",
        relationshipToTenant:"$allEntitiesForTenant.relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "tenantRelatedEntityId",
        foreignField: "entityId",
        as: "allMyTenantUsers"
      }
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:1,
        tenantRelatedEntityId:1,
        relationshipToTenant:1,
        allUsersAssociatedWithTenantOfLoggedInUser:"$allMyTenantUsers._id",
      }
    },
    {
      $group:{
        _id:{
          tenantId:"$tenantId",
          loggedInUserEntityId:"$loggedInUserEntityId",
          relationshipType:"$relationshipType",
          loggedInFirmUserIds:"$loggedInFirmUserIds"
        },
        userData:{"$push":"$allUsersAssociatedWithTenantOfLoggedInUser"},
        allEntitiesAssociatedWithTenant:{"$push":"$tenantRelatedEntityId"}
      }
    },
    {
      "$project": {
        _id:0,
        tenantId:"$_id.tenantId",
        loggedInUserEntityId:"$_id.loggedInUserEntityId",
        relationshipType:"$_id.relationshipType",
        loggedInFirmUserIds:"$_id.loggedInFirmUserIds",
        allEntitiesAssociatedWithTenant:1,
        allUsersAssociatedWithTenant: {
          "$reduce": {
            "input": "$userData",
            "initialValue": [],
            "in": { "$setUnion": ["$$value", "$$this"] }
          }
        }
      }
    }
  ]

  const otherQuery = [
    {
      $match:{
        entityParty2:ObjectID(user.entityId),
        relationshipType:user.relationshipType
      }
    },
    {
      $project:{
        tenantId:"$entityParty1",
        loggedInUserEntityId:"$entityParty2",
        relationshipType:"$relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "loggedInUserEntityId",
        foreignField: "entityId",
        as: "myFirmUsers"
      }
    },
    {
      $project:{
        tenantId:1,
        loggedInUserEntityId:1,
        relationshipType:1,
        loggedInFirmUserIds:"$myFirmUsers._id",
        allEntitiesAssociatedWithTenant:["$loggedInUserEntityId"],
        allUsersAssociatedWithTenant:"$myFirmUsers._id"
      }
    },
  ]
 
  const finalQueryPipeline = [
    {
      $addFields:{
        currentUserInTran:{
          $in:[ObjectID(user._id), {$ifNull:["$tranUsers",[]]}]
        },
        currentUserFirmInTran:{
          $in:[ObjectID(user.entityId),{$ifNull:["$tranEntities",[]]}]
        }
      }
    },
    {
      $match: { $or: [{ currentUserInTran: true }, { currentUserFirmInTran: true }]}
    },
    {
      $addFields:{
        editTran:{
          $or:[
            {$and: [
              {$eq:["$userEntitlement","global-edit"]},
              {$or:[
                {$eq:["$currentUserInTran",true]},
                {$eq:["$currentUserFirmInTran",true]}
              ]},
            ]},
            {
              $and:[
                {$in:["$userEntitlement",["tran-edit","global-readonly-tran-edit"]]},
                {$eq:["$currentUserInTran",true]},
              ]
            }
          ]
        },
        viewTran:{
          $or:[
            {$and: [
              {$in:["$userEntitlement",["global-edit","global-readonly","global-readonly-tran-edit"]]},
              {$or:[
                {$eq:["$currentUserInTran",true]},
                {$eq:["$currentUserFirmInTran",true]}
              ]},
            ]},
            {
              $and:[
                {$in:["$userEntitlement",["tran-edit","tran-readonly"]]},
                {$eq:["$currentUserInTran",true]},
              ]
            }
          ]
        }
      }
    },
    {
      $addFields:{
        entitiesCurrentUserCanEdit: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and: [
                    {
                      "$eq": ["$relationshipType", "Self"]
                    }, {
                      $in: [
                        "$userEntitlement",
                        ["global-edit"]
                      ]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$tranEntities",
                    "$allEntitiesAssociatedWithTenant",
                    [ObjectID(user.entityId)]
                  ]
                }
              }, 
              {
                "case": {
                  $and: [
                    {
                      $in: [
                        "$relationshipType",
                        ["Client", "Prospect", "Third Party"]
                      ]
                    }, 
                    {
                      $in: [
                        "$userEntitlement",
                        ["global-edit"]
                      ]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$allEntitiesAssociatedWithTenant",
                    [ObjectID(user.entityId)]
                  ]
                }
              }, 
            ],
            "default": []
          },
        },
        usersCurrentUserCanEdit: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and: [
                    {
                      "$eq": ["$relationshipType", "Self"]
                    }, {
                      $in: [
                        "$userEntitlement",
                        ["global-edit"]
                      ]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$tranUsers",
                    "$allUsersAssociatedWithTenant",
                    [ObjectID(user._id)]
                  ]
                }
              }, 
              {
                "case": {
                  $and: [
                    {
                      $in: [
                        "$relationshipType",
                        ["Client", "Prospect", "Third Party"]
                      ]
                    }, 
                    {
                      $in: [
                        "$userEntitlement",
                        ["global-edit"]
                      ]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$allUsersAssociatedWithTenant",
                    [ObjectID(user._id)]
                  ]
                }
              }, 
            ],
            "default": [user._id]
          },
        },
        entitiesCurrentUserCanView: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and: [
                    {
                      "$eq": ["$relationshipType", "Self"]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$tranEntities",
                    "$allEntitiesAssociatedWithTenant",
                    [ObjectID(user.entityId)]
                  ]
                }
              }, 
              {
                "case": {
                  $and: [
                    {
                      $in: [
                        "$relationshipType",
                        ["Client", "Prospect", "Third Party"]
                      ]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$tranEntities",
                    "$allEntitiesAssociatedWithTenant",
                    [ObjectID(user.entityId)]
                  ]
                }
              }, 
            ],
            "default": [user.entityId]
          },
        },
        usersCurrentUserCanView: {
          "$switch": {
            "branches": [
              {
                "case": {
                  $and: [
                    {
                      "$eq": ["$relationshipType", "Self"]
                    }
                  ]
                },
                "then": {
                  $setUnion: [
                    "$tranUsers",
                    "$allUsersAssociatedWithTenant",
                    [ObjectID(user._id)]
                  ]
                }
              }, 
              {
                "case": {
                  $and: [
                    {
                      $in: [
                        "$relationshipType",
                        ["Client", "Prospect", "Third Party"]
                      ]
                    } 
                  ]
                },
                "then": {
                  $setUnion: [
                    "$tranUsers",
                    "$allUsersAssociatedWithTenant",
                    [ObjectID(user._id)]
                  ]
                }
              }, 
            ],
            "default": [user._id]
          },
        },
      },
    },
    {
      $addFields:{
        editEntitiesAndUsers:{$setUnion:["$entitiesCurrentUserCanEdit","$usersCurrentUserCanEdit"]},
        viewEntitiesAndUsers:{$setUnion:["$entitiesCurrentUserCanView","$usersCurrentUserCanView"]}
      }
    },
  ]

  const initialQuery = user.relationshipType === "Self" ? selfQuery : otherQuery
 
  const allTransactionDetails = await EntityRel.aggregate([
    ...initialQuery,
    {
      $facet:{
        deals:[
          ...getTransactionTransformationQueries(user,"deals"),
          ...finalQueryPipeline
        ],
        // rfps:[
        //   ...getTransactionTransformationQueries(user,"rfps"),
        //   ...finalQueryPipeline
        // ]
      }
    },
    { 
      "$project": {
        "data": { "$concatArrays": ["$deals"] },
      }
    },
    { "$unwind": "$data" },
    { "$replaceRoot": { "newRoot": "$data" } },
    {
      $facet:{
        "editTran":[
          {
            $match:{editTran:true}
          },
          {
            $group:{_id:"$editTran",tranIds:{"$addToSet":"$tranId"}},
          },
          {
            $project:{_id:0, ids:"$tranIds"}
          }
        ],
        "viewTran":[
          {
            $match:{viewTran:true}
          },
          {
            $group:{_id:"$viewTran",tranIds:{"$addToSet":"$tranId"}},
          },
          {
            $project:{_id:0, ids:"$tranIds"}
          }
        ],
        "editEntitiesAndUsers":[
          {
            $unwind:"$editEntitiesAndUsers"
          },
          {
            $group:{_id:null,data:{$push:"$editEntitiesAndUsers"}},
          },
          {
            $project:{_id:0, ids:"$data"}
          }
        ],
        "viewEntitiesAndUsers":[
          {
            $unwind:"$viewEntitiesAndUsers"
          },
          {
            $group:{_id:null,data:{$push:"$viewEntitiesAndUsers"}},
          },
          {
            $project:{_id:0, ids:"$data"}
          }
        ],
        "editTasksWithActivity":[
          {
            $match:{editTran:true}
          },
          {
            $lookup: {
              from: "mvtasks",
              localField: "tranId",
              foreignField: "relatedActivityDetails.activityId",
              as: "relatedTasks"
            }
          }, 
          {
            $group:{_id:null, ids:{$addToSet:"$relatedTasks._id"}}
          },
          {
            $project:{
              _id:0,
              ids: {
                "$reduce": {
                  "input": "$ids",
                  "initialValue": [],
                  "in": { "$setUnion": ["$$value", "$$this"] }
                }
              }
            }
          }
        ],
        "editTasksWithUsers":[
          {
            $unwind:"$editEntitiesAndUsers"
          },
          {
            $lookup: {
              from: "mvtasks",
              localField: "editEntitiesAndUsers",
              foreignField: "taskDetails.taskAssigneeUserId",
              as: "relatedTasks"
            }
          }, 
          {
            $group:{_id:null, ids:{$addToSet:"$relatedTasks._id"}}
          },
          {
            $project:{
              _id:0,
              ids: {"$reduce": {
                "input": "$ids",
                "initialValue": [],
                "in": { "$setUnion": ["$$value", "$$this"] }
              }
              }
            }
          }
        ]
      },
    },
    { 
      "$project": {
        "editdata": { "$concatArrays": ["$editTran","$editEntitiesAndUsers","$editTasksWithActivity","$editTasksWithUsers"] },
        "viewdata": { "$concatArrays": ["$viewTran","$viewEntitiesAndUsers","$editTasksWithActivity","$editTasksWithUsers"] },
        "alldata": { "$concatArrays": ["$editTran","$editEntitiesAndUsers","$editTasksWithActivity","$editTasksWithUsers","$viewTran","$viewEntitiesAndUsers"] },
      }
    },
    {
      $facet:{
        edit:[
          { "$unwind": "$editdata" },
          { "$replaceRoot": { "newRoot": "$editdata" } },
          { "$unwind":"$ids"},
          { "$group":{_id:null, ids:{$addToSet:"$ids"}}},
          { "$project":{_id:0, ids:1}}
        ],
        view:[
          { "$unwind": "$viewdata" },
          { "$replaceRoot": { "newRoot": "$viewdata" } },
          { "$unwind":"$ids"},
          { "$group":{_id:null, ids:{$addToSet:"$ids"}}},
          { "$project":{_id:0, ids:1}}
        ],
        all:[
          { "$unwind": "$alldata" },
          { "$replaceRoot": { "newRoot": "$alldata" } },
          { "$unwind":"$ids"},
          { "$group":{_id:null, ids:{$addToSet:"$ids"}}},
          { "$project":{_id:0, ids:1}}
        ],
      }
    },
    {
      $unwind:"$edit"
    },
    {
      $unwind:"$view"
    },
    {
      $unwind:"$all"
    },
    {
      $project:{
        edit:"$edit.ids",
        view:"$view.ids",
        all:"$all.ids",
      }
    }
  ])
  
  return allTransactionDetails
}
