// require("appdynamics").profile({
//   controllerHostName: "marie2019022710332115.saas.appdynamics.com",
//   controllerPort: 443,

//   // If SSL, be sure to enable the next line
//   controllerSslEnabled: true,
//   accountName: "marie2019022710332115",
//   accountAccessKey: "4i8xk52s9oif",
//   applicationName: "testmunivisor",
//   tierName: "webapp",
//   nodeName: "process" // The controller will automatically append the node name with a unique number
// })

import http from "http"
import numeral from "numeral"

import app from "./server"

import { processNotifications } from "./api/appresources/notification/helpers"
import { getGetEligibleActionsTasksAndCreateForallTenants } from "./api/appresources/cac/controlsUtilityFunctions"
import { getS3DownloadURLs } from "./api/appresources/integrations/s3/helpers"
import {
  initTemplate,
  initMetaDataTemplate
} from "./api/elasticsearch/esHelper"
import { initMetaIndex } from "./api/elasticsearch/metaIndexHelper"
import { getPlateFormAdminUserAndTransaction } from "./api/appresources/plateformadmin/commonservices/commonservices.controller"
import {countTenantUsers} from "./api/appresources/plateformadmin/dashboard/plateformadmin.controller"

require("dotenv").config()

let server = null
let currentApp = app

if (process.env.NODE_ENV === "development") {
  server = http.createServer(app)
} else {
  console.log("This is the production environment")
  server = app
}
const timeouts = []

const clearTimeouts = () => {
  // console.log("num timeouts : ", timeouts.length)
  const timeout = timeouts.pop()
  clearTimeout(timeout)
}


const reviewCACControlsAndCreatedTasksAndActions = async () => {
  console.log("----------------------------------")
  console.log("Checking for CAC Notifications - Running every 5 minutes")
  const retValue = await getGetEligibleActionsTasksAndCreateForallTenants()
  console.log("The results of CAC Updates", JSON.stringify(retValue, null, 2))
  console.log("----------------------------------")
  clearTimeouts()
  const timeout = setTimeout(reviewCACControlsAndCreatedTasksAndActions, 10000)
  timeouts.push(timeout)
}

reviewCACControlsAndCreatedTasksAndActions()


setInterval(() => {
  const { rss, heapTotal } = process.memoryUsage()
  console.log(
    "rss",
    numeral(rss).format("0.0 ib"),
    "heapTotal",
    numeral(heapTotal).format("0.0 ib")
  )
}, 5000)

const PORT = process.env.PORT || 8081
server.listen(PORT, async () => {
  try {
    console.log(`Server listening on port ${PORT}`)
    const templateInitialized = await initTemplate()
    // const templateInitializedMetaData = await initMetaDataTemplate() // not required because we only have one metaindex
    const metaDataIndexInitialize = await initMetaIndex()
    console.log(
      `Elasticsearch Template initialized ${JSON.stringify({
        templateInitialized,
        metaDataIndexInitialize
      })}`
    )
  } catch (err) {
    console.log("Server init error", err)
  }
})

console.log(process.env.NODE_ENV)

if (module.hot && process.env.NODE_ENV === "development") {
  module.hot.accept(["./server"], () => {
    server.removeListener("request", currentApp)
    server.on("request", app)
    currentApp = app
  })
}
