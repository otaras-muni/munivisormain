// eslint-disable-next-line no-unused-vars
const globalTransaction = {
  transactionId: 12345,
  tranClientId: "THIS ID SHOULD BE RELATED TO THE ISSUER CLIENT",
  tranName: "NAME OF THE TRANSACTION",
  tranType: "TYPE OF THE TRANSACTION",
  tranPurposeOfRequest: "PURPOSE OF REQUEST",
  tranAssignedTo: [
    {}
  ],
  tranState: "THE STATE OF THE TRANSACTIONS",
  tranCounty: "THE COUNTY WITHIN THE STATE",
  tranPrimarySector: "THE SECTOR OF THE TRANSACTION",
  tranSecondarySector: "THE SECONDARY SECTOR ATTACHED TO THE TRANSACTION",
  tranDateHired: "HIRE DATE",
  tranStartDate: "START DATE OF THE RELATIONSHIP",
  tranExpectedEndDate: "EXPECTED END DATE OF THE TRANSACTIONS"
}

// eslint-disable-next-line no-unused-vars
const rfpDistributeSchema = {
  transactionId: 12345,
  rfpSelEvalCommittee: [
    {
      rfpSelEvalContactId: "THIS IS THE ID OF THE FIRM OR THE ISSUER CLIENT",
      rfpSelEvalRole: "ROLE",
      rfpSelEvalContactName: "THIS IS THE NAME CORRESPONDING TO THE ID",
      rfpSelEvalEmail: "EMAIL OF THE CONTACT",
      rfpSelEvalPhone: "PHONE NUMBER OF THE CONTACT",
      rfpSelEvalAddToDL: false
    }, {
      rfpSelEvalContactId: "THIS IS THE ID OF THE FIRM OR THE ISSUER CLIENT",
      rfpSelEvalRole: "ROLE",
      rfpSelEvalContactName: "THIS IS THE NAME CORRESPONDING TO THE ID",
      rfpSelEvalEmail: "EMAIL OF THE CONTACT",
      rfpSelEvalPhone: "PHONE NUMBER OF THE CONTACT",
      rfpSelEvalAddToDL: false
    }, {
      rfpSelEvalContactId: "THIS IS THE ID OF THE FIRM OR THE ISSUER CLIENT",
      rfpSelEvalRole: "ROLE",
      rfpSelEvalContactName: "THIS IS THE NAME CORRESPONDING TO THE ID",
      rfpSelEvalEmail: "EMAIL OF THE CONTACT",
      rfpSelEvalPhone: "PHONE NUMBER OF THE CONTACT",
      rfpSelEvalAddToDL: false
    }
  ],

  rfpProcessContacts: [
    {
      rfpContactId: "THIS IS THE ID OF THE FIRM OR THE ISSUER CLIENT",
      rfpContactFor: "ROLE",
      rfpContactName: "THIS IS THE NAME CORRESPONDING TO THE ID",
      rfpContactEmail: "EMAIL OF THE CONTACT",
      rfpContactPhone: "PHONE NUMBER OF THE CONTACT",
      rfpContactAddToDL: false
    }
  ],

  rfpParticipantDist: [
    {
      rfpParticipantId: "THIS WILL COME FROM THE 3RD PARTY DATABASE",
      rfpParticipantFirmName: "ROLE",
      rfpParticipantContactName: "THIS IS THE NAME CORRESPONDING TO THE ID",
      rfpParticipantContactEmail: "EMAIL OF THE SUPPLIER CONTACT",
      rfpParticipantContactPhone: "PHONE NUMBER OF THE CONTACT",
      rfpParticipantContactAddToDL: false
    }, {
      rfpParticipantId: "THIS WILL COME FROM THE 3RD PARTY DATABASE",
      rfpParticipantFirmName: "ROLE",
      rfpParticipantContactName: "THIS IS THE NAME CORRESPONDING TO THE ID",
      rfpParticipantContactEmail: "EMAIL OF THE SUPPLIER CONTACT",
      rfpParticipantContactPhone: "PHONE NUMBER OF THE CONTACT",
      rfpParticipantContactAddToDL: false
    }
  ],

  rfpBidPacket: [
    {
      rfpBidDocId: "A unique identifier of the Document that was created",
      rfpBidDocName: "The name of the document",
      rfpBidDocType: "The type of the document",
      rfpBidDocStatus: "The status of the document",
      rfpBidDocAction: "The Document Action",
      rfpBidDocUploadUser: "The User who uploaded the document",
      rfpBidPacketUploadDate: "MM/DD/YYYY"
    }, {
      rfpBidDocId: "A unique identifier of the Document that was created",
      rfpBidDocName: "The name of the document",
      rfpBidDocType: "The type of the document",
      rfpBidDocStatus: "The status of the document",
      rfpBidDocAction: [
        { actionType:"submitted", user:"stiffel", date:"akdsflk"},
        { actionType:"rejected", user:"ford", date:"akdsflk"},
        { actionType:"submitted", user:"stiffel", date:"akdsflk"},
        { actionType:"finalized", user:"ford", date:"akdsflk"}
      ],
      rfpBidDocUploadUser: "The User who uploaded the document",
      rfpBidPacketUploadDate: "MM/DD/YYYY"
    }
  ],

  // Need to check with Kapil on the best way to model this
  rfpBidPacketChecklist: [
    {
      checkListCategoryId: "THE ID OF THE CHECKLIST FROM THE CHECKLIST CONFIG",
      checkListItems: [
        {
          rfpListItemDetails: "ITEM",
          rfpListItemConsider: "yes consider",
          rfpListItemPriority: "HIGH / MED LOW",
          rfpListItemEndDate: "MM/DD/YYYYY",
          rfpListItemResolved: false,
          rfpListItemAssignees: [
            {
              rfpTeamAssigneeId: 12341
            }, {
              rfpTeamAssigneeId: 12342
            }
          ]
        }
      ]
    }
  ]
}

// eslint-disable-next-line no-unused-vars
const rfpManageSchema = {
  transactionId: "THIS IS THE OVERALL TRANSACTION ID",
  rfpSubmissionDocuments: [
    {
      rfpParticipantId: "THIS WILL REFER TO THE PARTICIPANT WHO RECEIVED THE INFORMATION",
      rfpDocSubUploadDate: "MM/DD/YYYY",
      rfpDocSubAWSLocation: "THE LOCATION OF THE DOCUMENT ON AWS",
      rfpDocSubComments: "THE USER WHO IS ENTITLED TO PARTICIPATE IN THE RFP PROCESS",
      rfpDocSubActions: [
        {
          rfpDocSubActionType: "SHARE / UPDATE / DELETE ETC",
          rfpActionDate: "MM/DD/YYYY"
        }
      ]
    }
  ],

  // Need to check with Kapil on how to make this similar to the Reddit
  rfpParticipantQuestions: [
    {
      rfpParticipantId: "THIS WILL REFER TO THE PARTICIPANT WHO RECEIVED THE INFORMATION",
      rfpPartContactId: "The Person from the organization who is uploading the ",
      rfpPartQuestionId: "MM/DD/YYYY",
      rfpPartQuestDocAWSId: "The location where the document is available on AWS",
      rfpPartQuestDetails: "The question submitted by the participant contact",
      rfpPartQuestPosts: [
        {
          postId: "Unique ID of the post",
          postDetails: "The details of the post",
          postDate: "MM/DD/YYYY",
          postVisibilityFilter:"resitricted/public",
          postFirmId: "This can be either the ",
          postContactId: "The ID of the participant user",
          postDocument:"AWS ID",
          postParentId: 1,
          postVisibilityFlag: "All or only the Participant who posted the Post",
          postChildren: [{}, {}, {}, {}]
        }
      ]
    }
  ]

}

// eslint-disable-next-line no-unused-vars
const rfpEvaluateSchema = {
  transactionId: 12345,
  rfpEvaluations: [
    {
      rfpParticipantFirmId: "The ID of the firm that is being rated",
      rfpEvaluationCommitteeMemberId: "The ID of the Member Evaluating the RFP",
      rfpEvaluationCategories: {
        costOfService: [
          {
            evalItem: "Cost of Service",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }, {
            evalItem: "Evaluation Item 123",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }
        ],
        qualifications: [
          {
            evalItem: "Cost of Service",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }, {
            evalItem: "Evaluation Item 123",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }
        ],
        financingApproach: [
          {
            evalItem: "FinancingApproach",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }, {
            evalItem: "Financing Approach 1",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }
        ],
        LegalIssues: [
          {
            evalItem: "Legal Issues",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }, {
            evalItem: "Legal Issues 2",
            evalPriority: "High/Medium/Low",
            evalRating: 3
          }
        ],
        evalNotes: "THIS WILL HAVE ALL THE NOTES FROM A GIVEN USER"
      }
    }, {}
  ],
  rfpFinalValuations: [
    {
      evalCategory: "The category heads from above",
      finalRatings: [
        {
          rfpParticipantId: "THE ID OF THE RFP PARTICIPANT",
          rfpParticipantFinalRating: 33
        }
      ]
    }, {
      evalCategory: "One of the categories from above",
      finalRatings: [
        {
          rfpParticipantId: "THE ID OF THE RFP PARTICIPANT",
          rfpParticipantFinalRating: 33
        }
      ]
    }
  ],
  rfpFinalWinner: {
    rfpWinnerId: "Participant ID of the Winner",
    rfpWinnerBidAwardDate: "MM/DD/YYYY",
    rfpFinalClosingComments: "THIS IS THE FINAL COMMENTS"
  }
}

// eslint-disable-next-line no-unused-vars
const rfpDiscussionSchema = {}

// Check with Kapil on how to integrate the Chat Schema 