import {normalize, schema} from "normalizr"

// Define the participant Schema
const partFirmSchema = new schema.Entity("rfpPartFirms", {}, {idAttribute: "firmId"})

// Define the User Schema
const rfpManageSchema = new schema.Entity("rfpAssignees", {
  firmId: partFirmSchema
}, {idAttribute: "id"})

// Participant User schema
const partUserSchema = new schema.Entity("rfpPartUsers", {
  firmId: partFirmSchema
}, {idAttribute: "pid"})

// RFP submission document details
const partSubmissionDocDetails = new schema.Entity("rfpSubmissionDocs", {}, {idAttribute: "docId"})

// Define the Submission Schema
const rfpSubmissionSchema = new schema.Entity("rfpSubmission", {
  participantId: partUserSchema,
  submission: partSubmissionDocDetails
}, {idAttribute: "submissionId"})

// Define the RFP Schema
const rfpSchema = new schema.Entity("rfps", {
  rfpAssignees: [rfpManageSchema],
  rfpParticipants: [partUserSchema],
  rfpContacts: [rfpManageSchema],
  rfpResponses: [rfpSubmissionSchema]
})

const rfpOverallSchema = [rfpSchema]

const rfpdata = [
  {
    id: 123,
    rfpAssignees: [
      {
        id: 1,
        firmId: 1,
        firstName: "naveen",
        lastName: "balawat"
      }, {
        id: 2,
        firmId: 2,
        firstName: "deepak",
        lastName: "adinarayana"
      }
    ],
    rfpParticipants: [
      {
        pid: 4,
        firmId: 3,
        firstName: "Part1 - fname",
        lastName: "Part1 - Last Name"
      }, {
        pid: 5,
        firmId: 4,
        firstName: "Part2 - fname",
        lastName: "Part2 - Last Name"
      }, {
        pid: 6,
        firmId: 4,
        firstName: "Part2 - fname",
        lastName: "Part2 - Last Name"
      }
    ],
    rfpContacts: [
      {
        id: 2.1,
        firmId: 1,
        firstName: "rfpcont 1",
        lastName: "rfpcontact 1 lname"
      }, {
        id: 2,
        firmId: 2,
        firstName: "rfpcont 2",
        lastName: "rfpcontact 2 lname"
      }
    ],
    rfpBidPacketDetails: [],
    rfpResponses: [
      {
        submissionId: 1,
        participantId: {
          pid: 111,
          firmId: 4
        },
        submission: {
          docId: 123,
          docName: "asdkfsadf",
          awsLocation: "aws:123/////"
        }
      }, {
        submissionId: 2,
        participantId: {
          pid: 5,
          firmId: 4
        },
        submission: {
          docId: 124,
          docName: "asdkfsadf",
          awsLocation: "aws:124/////"
        }
      }
    ]
  }
]


const normalizedData = normalize(rfpdata, rfpOverallSchema)
console.log(JSON.stringify(normalizedData, null, 2))

