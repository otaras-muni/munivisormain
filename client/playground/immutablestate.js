import { Map,fromJS } from "immutable"

// This is the test object that is used to make updates
const dealObj = fromJS({firstName:"Naveen",lastName:"Balawat",sal:[10,10,20,30]})

// Update the Salary and change the first index value
// eslint-disable-next-line no-unused-vars
const newDealObj = dealObj.updateIn(["sal",2], x => 25)
console.log("EXAMPLE 1", newDealObj.toJS())

// Insert a new property
const newDealObj1 = dealObj.set("salnew",Map({a:"amazing", b:"magnificient"}))
console.log("EXAMPLE 2", newDealObj1.toJS())

// Update a new property that doesn't exist -- Need to provide a call back
// eslint-disable-next-line no-unused-vars
const newDealObj2 = dealObj.updateIn(["salnew"],(x) => Map({a:"amazing", b:"magnificient"}))
console.log("EXAMPLE 3", newDealObj2.toJS())

// Update all values of an array
const newDealObj3 = dealObj.updateIn(["sal"], x => x.map(x1 => x1+5))
console.log("EXAMPLE 4", newDealObj3.toJS())

// Filter and set only some values that fit the criteria
const newDealObj4 = dealObj.updateIn(["sal"], x => x.filter(x1 => x1>10))
console.log("EXAMPLE 5", newDealObj4.toJS())

// Find all instances where the value matches a criteria and then update the value
const newDealObj5 = dealObj.updateIn(["sal"], ((list) => {
  const newlist = list.reduce( (f,a,i) =>  a === 10 ? [...f,list.get(i) + 20] : [...f,list.get(i)],[])
  return newlist
} ))
console.log("EXAMPLE 6", newDealObj5.toJS())

// Find an index and insert a value at that index
const newDealObj6 = dealObj.updateIn(["sal"], ((list) => {
  const idx = list.findIndex( a => a === 10)
  return list.insert(idx+1,45)
} ))
console.log("EXAMPLE 7", newDealObj6.toJS())

// Append a value at the end of the list
const newDealObj7 = dealObj.updateIn(["sal"], ((list) => list.insert(list.size,45) ))
console.log("EXAMPLE 8", newDealObj7.toJS())

// Delete a value at a particular position and return the data
const newDealObj8 = dealObj.updateIn(["sal"], ((list) => {
  const idx = list.findIndex( a => a === 10)
  return list.delete(idx)
} ))
console.log("EXAMPLE 9", newDealObj8.toJS())

// splice few values and return some
const newDealObj9 = dealObj.updateIn(["sal"], ((list) => {
  const idx = list.findIndex( a => a === 10)
  return list.splice(idx,2)
} ))
console.log("EXAMPLE 10", newDealObj9.toJS())












// Add a new value in the array and get a new object created