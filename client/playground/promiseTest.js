import { fromJS } from "immutable"
// Get the config information from the configuration object

const soeChecklist = {
  _id : "5af3a70095a24220f0e6700b",
  entityId : "1234",
  checkLists : {
    deal : {
      "Cost of Issuance" : [ 
        {
          "title" : "Bond Rating",
          "items" : [ 
            "Bond Rating:Fitch", 
            "Bond Rating:kroll", 
            "Bond Rating:Moodys"
          ]
        }, 
        {
          "title" : "Credit Facility",
          "items" : [ 
            "Management Fee", 
            "Spread expenses", 
            "Takedown", 
            "Underwriter Discount"
          ]
        }, 
        {
          "title" : "Others",
          "items" : [ 
            "Accounting CPA", 
            "Attorney's fees", 
            "Bond Call Fees", 
            "Bond Counsel"
          ]
        }
      ]
    }
  }
}
// Tenant ID will be the entity ID of the tenant
// The configPath is the path object that will get the checklist 
// Once you get the checklist make sure that the checklist is stored in the redux store so that it can be used

const getConfigForTenant = (tenantId, configPath) => new Promise((resolve) => setTimeout(()=> {
  // Walk the path and find the Object corresponding to the config Path
  // find the configuration for the entity that is in question
  const immutableCheckList = fromJS(soeChecklist)
  resolve(immutableCheckList.getIn(configPath))
},10000))


const getReturnValue = async () => {
  const retValue = await getConfigForTenant("1234", ["checkLists","deal"])
  console.log("This is the return value from asyc:",retValue)
  return retValue
}

// This returns a promise that resolves after 10 seconds or whatever timeinterval 
// The second async call happens after the first one ends and there is a sequence of calls in the promise chain.
const returnValue = getConfigForTenant("1234", ["checkLists","deal"])
returnValue.then( (value) => {
  console.log("FIRST",Object.values(value.toJS()))
  // this syncrhonous code that
  const asyncReturnValue = getReturnValue()
  asyncReturnValue.then( (v) => console.log("THIRD", v.toJS())).catch( (e) => console.log(e))
}).catch( () => console.log("No Error"))

// this method runs in in parallel with the above call and resolves the promise as soon as the first one ends.
const asyncReturnValue = getReturnValue()
asyncReturnValue.then( (v) => console.log("SECOND", v.toJS())).catch( (e) => console.log(e))






