import "react-widgets/dist/css/react-widgets.css"
import "react-select/dist/react-select.css"

import React, { Component, Suspense } from "react"
import { Link, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { toast } from "react-toastify"

import CONST, { navLabels, PUBLIC_ROUTES } from "GlobalUtils/consts"


import GlobalSignUp from "Auth/GlobalSignUp"
import GlobalSignIn from "Auth/GlobalSignIn"
import {
  arraysWithSameElements,
  checkPlatformAdmin,
  checkNavAccessEntitlrment
} from "GlobalUtils/helpers"
import STPLogin from "Global/STPLogin"
import { AccordionGlobalAlt } from "Global/AccordionGlobalAlernate"
import AttachChecklist from "Global/AttachChecklist"
import ProcessChecklistTest from "Global/ProcessChecklistTest"
import AuditLog from "Global/AuditLog"
import Messages from "Global/Messages"
import UserProfile from "Global/UserProfile"
import BulmaModalCard from "Global/BulmaModal"
import PicklistsTest from "Global/PicklistsTest"
import UpdateTagsTest from "Global/UpdateTagsTest"
import NotAvailable from "Global/NotAvailable"
import EventsCalender from "Task/components/EventsCalender"
import TaskMain from "Task/TaskMain"

import Firms from "Admin/Firms/FirmsMain"
import Users from "Admin/Users/UsersMain"
import AdminUsersListTabsView from "Admin/Users/containers/AdminUsersListTabsView"
import AdminClientFirmTabsView from "Admin/Clients/containers/AdminClientFirmTabsView"
import AdminThirdPartyTabsView from "Admin/Third-Party/containers/AdminThirdPartyTabsView"


import {
  getNavOptions,
  checkAuth,
  signOut
} from "./app/StateManagement/actions"
import "./public/images/munivisor.png"

import ClientsProspects from "./app/FunctionalComponents/ClientsProspects/ClientsProspects"
import ThirdPartyNew from "./app/FunctionalComponents/ThirdParty/ThirdParty"
import UsersNew from "./app/FunctionalComponents/Users/Users"

import ChooseActivity from "./app/FunctionalComponents/BusinessProcesses/components/ChooseActivity"
import ChooseEntity from "./app/FunctionalComponents/BusinessProcesses/components/ChooseEntity"
import ChooseAdmin from "./app/FunctionalComponents/BusinessProcesses/components/ChooseAdmin"


import DocsMain from "./app/FunctionalComponents/docs/DocsMain"
import DocUpload from "./app/FunctionalComponents/docs/DocUpload"


import ProjectDashboard from "./app/FunctionalComponents/DashAlt/ProjectDashboard"

import ClientProspectDashboard from "./app/FunctionalComponents/Dashboard/components/ClientsProspectsDashboard"

import RFP from "./app/FunctionalComponents/RFP"
import DealsMain from "./app/FunctionalComponents/TransactionDeals/DealsMain"

import BankLoan from "./app/FunctionalComponents/BankLoan"
import Others from "./app/FunctionalComponents/Others"
import Derivative from "./app/FunctionalComponents/Derivative"
import Marfp from "./app/FunctionalComponents/Marfp"

import { DevMainContainer } from "./DevMainContainer"
import DealRatingDetails from "./app/FunctionalComponents/TransactionDeals/Component/DealSeriesRatings"
import ConfigMain from "./app/FunctionalComponents/PlatformManagement/ConfigMain"

import MunivisorSplash from "./app/LoginAuthentication/MunivisorSplash"
import ForgotPassword from "./app/LoginAuthentication/ForgotPassword"
import RequestToken from "./app/LoginAuthentication/RequestToken"

import SeriesDealRatings from "./app/FunctionalComponents/TransactionDeals/Component/Ratings/DealRatings"
import Loader from "./app/GlobalComponents/Loader"
import DealCostOfIssuance from "./app/FunctionalComponents/TransactionDeals/Component/CheckAndTrack/DealCostOfIssuance"
import DealScheduleOfEvents from "./app/FunctionalComponents/TransactionDeals/Component/CheckAndTrack/DealScheduleOfEvents"
import ClientFirmTabsView from "./app/FunctionalComponents/EntityManagement/clients/containers/ClientFirmTabsView"
import ThirdPartyFirmTabsView from "./app/FunctionalComponents/EntityManagement/third-party/containers/ThirdPartyFirmTabsView"
import UsersListTabsView from "./app/FunctionalComponents/EntityManagement/users/containers/UsersListTabsView"

import ClientsProspect from "./app/FunctionalComponents/EntityManagement/clients/ClientsMain"
import MigratedEntitiesMain from "./app/FunctionalComponents/EntityManagement/clients/MigratedEntitiesMain"
import ThirdParties from "./app/FunctionalComponents/EntityManagement/third-party/ClientsMain"
import BusinessDev from "./app/FunctionalComponents/BusinessDevelopment/index"

import Document from "./app/FunctionalComponents/Document/components/ManageDocuments/index.js"


import ContentDocs from "./app/FunctionalComponents/ContentDocs"
import MainContacts from "./app/FunctionalComponents/EntityManagement/contacts/ContactsMain"
import ChangeForgotPassword from "./app/LoginAuthentication/ChangeForgotPassword"
import NavSearch from "./app/FunctionalComponents/Search/NavigationSearch"
import FormsLinks from "./app/FunctionalComponents/Compliance1/Component/FormsLinks"

const CACMain = React.lazy(() => import("./app/FunctionalComponents/CAC/CACMain"))
const createTran = React.lazy(() => import("./app/FunctionalComponents/CreateTran"))
const BillingMain = React.lazy(() => import("./app/FunctionalComponents/Billing"))
const TaskDashboard = React.lazy(() => import("./app/FunctionalComponents/DashAlt/TaskDashboard"))
const BigCalendarComponent = React.lazy(() => import("./app/FunctionalComponents/TaskManagement/components/BigCalendarComponent"))
const PoliciesProcedures = React.lazy(() => import("./app/FunctionalComponents/Compliance1/Component/PoliciesProcedures"))
const NewCompliance = React.lazy(() => import("./app/FunctionalComponents/NewCompliance"))
const RevenuePipeline = React.lazy(() => import("./app/FunctionalComponents/RevenuePipeline"))
const GlobalSearch = React.lazy(() => import("./app/FunctionalComponents/Search/GlobalSearch"))
const MigrationTools1 = React.lazy(() => import("Admin/MigrationTools1"))
const DocSearch = React.lazy(() => import("./app/FunctionalComponents/Search/DocSearch"))
const AdminBilling = React.lazy(() => import("./app/FunctionalComponents/AdminBilling"))

const routesMap = {
  "dev": DevMainContainer,
  "dealRatings": DealRatingDetails,
  "seriesratings": SeriesDealRatings,
  "costofissuance": DealCostOfIssuance,
  "scheduleofevents": DealScheduleOfEvents,
  "dashboard": TaskDashboard,
  "forgotpass": ForgotPassword,
  "requesttoken": RequestToken,
  "changeforgotpass": ChangeForgotPassword,
  "deals": DealsMain,
  "signup": GlobalSignUp,
  "signin": GlobalSignIn,
  "splash": MunivisorSplash,
  "tasks": TaskMain,
  "auditlog": AuditLog,
  "modal": BulmaModalCard,
  "calendar": EventsCalender,
  "docs": DocsMain,
  "upload": DocUpload,
  "rfp": RFP,
  "configuration": ConfigMain,
  "picklisttest": PicklistsTest,
  "firms": Firms,
  "third-parties": AdminThirdPartyTabsView,
  "users": Users,
  "clients": AdminClientFirmTabsView,
  "createTran": createTran,
  "update-tags": UpdateTagsTest,
  "attach-checklist": AttachChecklist,
  "process-checklist-test": ProcessChecklistTest,

  "addnew": TaskDashboard,
  "addnew-entity": ChooseEntity,
  "addnew-activity": ChooseActivity,
  "addnew-task": TaskMain,

  "dash": TaskDashboard,
  "dash-alt": TaskDashboard,
  "dash-task": TaskDashboard,
  "dash-projects": ProjectDashboard,
  "dash-projs": BusinessDev,
  "bus-development": BusinessDev,
  "dash-cltprosp": ClientProspectDashboard,
  "dash-cal": EventsCalender,
  "dash-cal1": BigCalendarComponent,
  "dash-busdev": BusinessDev,

  "mast": TaskDashboard,
  "mast-cltprosp": ClientFirmTabsView,
  "mast-thirdparty": ThirdPartyFirmTabsView,
  "mast-allcontacts": UsersListTabsView,

  "comp": TaskDashboard,
  "compliance": NewCompliance,
  "new-compliance": NewCompliance,
  "comp-msrbsecfil": TaskDashboard,
  "comp-polandproc": PoliciesProcedures,
  "comp-formLinks": FormsLinks,
  "comp-stbusentfilandlicenses": TaskDashboard,
  "comp-orgdocuments": TaskDashboard,
  "comp-engletandforms": TaskDashboard,
  "comp-custcomp": TaskDashboard,

  "tools": TaskDashboard,
  "tools-analytics": TaskDashboard,
  "tools-revpipe": RevenuePipeline,
  "tools-billing": BillingMain,
  "tools-chat": TaskDashboard,
  "tools-docs": Document,
  "tools-docsearch": DocSearch,
  "contentDocs": ContentDocs,
  "tools-emma": TaskDashboard,
  "tools-globalsearch": GlobalSearch,

  "contacts": MainContacts,
  "admin": ChooseAdmin,
  "admin-config": ConfigMain,
  "admin-migtools": MigrationTools1,
  "admin-usage": TaskDashboard,
  "admin-billing": AdminBilling,

  "loan": BankLoan,
  "derivative": Derivative,
  "marfp": Marfp,
  "others": Others,
  "clients-propects": ClientsProspect,
  "migratedentities": MigratedEntitiesMain,

  "addnew-client": ClientsProspects,
  "addnew-prospect": ClientsProspects,
  "addnew-thirdparty": ThirdPartyNew,
  "addnew-contact": UsersNew,
  "admin-cltprosp": AdminClientFirmTabsView,
  "admin-thirdparty": AdminThirdPartyTabsView,
  "admin-firms": Firms,
  "admin-firmusers": Users,
  "admin-users": AdminUsersListTabsView,
  "thirdparties": ThirdParties,
  "cac": CACMain,
  "messages": Messages,
  "dash-projects-alt": ProjectDashboard,
  "not-available": NotAvailable,
  "userprofile": UserProfile,
  "stp-redirect": STPLogin
}

const componentProps = {
  docs: {
    rowCells: [
      "name",
      "uploadDate",
      "download",
      "docuSign",
      "share",
      "details"
    ],
    showUploadButton: true
  },
  upload: {
    tenantId: "tenant1",
    contextType: "rfp",
    contextId: "rfp123",
    tags: {
      docType: "type1",
      docCategory: "cat1"
    }
  },
  dealRatings: {
    multiple: false
  },
  "update-tags": {
    docId: "5b20b103483cc2f19c0b0f5a",
    tags: {
      docType: "type2",
      docCategory: "cat1"
    }
  },
  "attach-checklist": {
    checklistId: "CL3"
  }
}

class NavMain extends Component {
  constructor(props) {
    super(props)
    this.state = {
      navOptions: {},
      menuExpanded: false,
      allowedIds: "",
      waiting: false
    }
    this.protectRoutes = this.protectRoutes.bind(this)
    this.signOut = this.signOut.bind(this)
    this.expandMenu = this.expandMenu.bind(this)
    this.handleNotAllowed = this.handleNotAllowed.bind(this)
    this.setExpiryTimeout = this.setExpiryTimeout.bind(this)
    this.expireAuth = this.expireAuth.bind(this)
    // this.showAuthExpiryWarning = this.showAuthExpiryWarning.bind(this)
  }

  async componentDidMount() {
    this.unmount = false
    const { nav1, authenticated, exp } = this.props
    if (exp) {
      this.setExpiryTimeout(exp)
    }
    if (!nav1 && !authenticated) {
      // console.log("moving to splash")
      this.props.history.push("/splash")
    } else if (authenticated) {
      // console.log("calling checkAllowedNavs 1")
      await this.checkAllowedNavs(this.props)
    }
  }

  async componentWillReceiveProps(nextProps) {
    // console.log("in componentWillReceiveProps navmain")
    if (nextProps.exp && this.props.exp !== nextProps.exp) {
      this.setExpiryTimeout(nextProps.exp)
    }
    if (
      (this.props.authenticated !== nextProps.authenticated ||
        this.props.token !== nextProps.token) &&
      nextProps.nav1 !== "stp-redirect"
    ) {
      // console.log("this props : ", this.props)
      // console.log("nextProps : ", nextProps)
      // console.log("count : ", this.count)
      await this.props.checkAuth(nextProps.token)
      const nextUserEntities =
        (nextProps.loginDetails && nextProps.loginDetails.userEntities) || []
      // console.log(" nextUserEntities : ", nextUserEntities)
      if (nextUserEntities[0]) {
        await this.props.getNavOptions(nextProps.token)
      }
    } else if (nextProps.authenticated) {
      if (!Object.keys(nextProps.nav).length) {
        // console.log(" setting nav")
        this.setNavOptions(nextProps.nav)
      } else {
        await this.protectRoutes(nextProps)
      }
      // const prevUserEntities = this.props.loginDetails.userEntities || []
      // const nextUserEntities = nextProps.loginDetails.userEntities || []
      // console.log(" prevUserEntities : ", prevUserEntities)
      // console.log(" nextUserEntities : ", nextUserEntities)
      // if(nextUserEntities[0] && (nextUserEntities[0].userId !== prevUserEntities[0].userId)) {
      //   await this.props.getNavOptions(nextUserEntities[0].userId)
      //   this.setNavOptions(nextProps.nav)
      // }
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.props.token === nextProps.token &&
  //     this.props.authenticated === nextProps.authenticated &&
  //     ((this.props.loginDetails && this.props.loginDetails.userEntities &&
  //       this.props.loginDetails.userEntities[0] &&
  //       this.props.loginDetails.userEntities[0].userId) === (
  //       nextProps.loginDetails && nextProps.loginDetails.userEntities &&
  //         nextProps.loginDetails.userEntities[0] &&
  //         nextProps.loginDetails.userEntities[0].userId)) &&
  //     !(this.navChanged(nextProps) && !this.allowedIdsChanged(nextState))) {
  //     console.log("no update")
  //     return false
  //   }
  //   console.log("update")
  //   return true
  // }

  componentWillUnmount() {
    this.unmount = true
  }

  setNavOptions(navOptions) {
    // console.log("navOptions : ", navOptions)
    navOptions = navOptions || {}
    if (!this.unmount) {
      this.setState({ navOptions })
    }
  }

  setExpiryTimeout(exp) {
    const tokenExpiry = new Date(exp * 1000) - new Date()
    console.log("token expires at : ", new Date(exp * 1000), tokenExpiry, exp)

    const oneDayInterval = 1 * 24 * 60 * 60 * 1000
    const fiveMinutesInterval = 5 * 60 * 1000
    if (
      !window.mvAuthWarningTimeout5 &&
      tokenExpiry < oneDayInterval &&
      tokenExpiry > fiveMinutesInterval
    ) {
      console.log("setting fiveMinutesInterval")
      window.mvAuthWarningTimeout5 = window.setTimeout(
        this.showAuthExpiryWarning.bind(this, 5),
        tokenExpiry - fiveMinutesInterval
      )
    }

    const oneMinuteInterval = 1 * 60 * 1000
    if (
      !window.mvAuthWarningTimeout1 &&
      tokenExpiry < oneDayInterval &&
      tokenExpiry > oneMinuteInterval
    ) {
      console.log("setting oneMinuteInterval")
      window.mvAuthWarningTimeout1 = window.setTimeout(
        this.showAuthExpiryWarning.bind(this, 1),
        tokenExpiry - oneMinuteInterval
      )
    }

    if (!window.mvAuthTimeout && tokenExpiry < oneDayInterval) {
      window.mvAuthTimeout = window.setTimeout(this.expireAuth, tokenExpiry)
    }
  }

  showAuthExpiryWarning(minutes) {
    console.log(
      "authWarningTimeout : ",
      minutes,
      window[`mvAuthWarningTimeout${minutes}`],
      new Date()
    )
    if (this[`mvAuthWarningTimeout${minutes}`]) {
      window.clearTimeout(this[`mvAuthWarningTimeout${minutes}`])
    }
    window[`mvAuthWarningTimeout${minutes}`] = null
    toast(
      `Your session will expire in ${minutes} minutes. You will be logged out automatically for security reasons. You will need to login again. Please save any unsaved work.`,
      {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      }
    )
  }

  expireAuth() {
    console.log("in expireAuth")
    if (window.mvAuthTimeout) {
      window.clearTimeout(window.mvAuthTimeout)
    }
    window.mvAuthTimeout = null
    toast(
      "Your session has expired. Please sign in again for security reasons.",
      {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      }
    )
    this.props.signOut()
  }

  allowedIdsChanged(nextState) {
    // console.log("this state : ", this.state)
    // console.log("next state : ", nextState)
    if (!this.state.allowedIds && nextState.allowedIds) {
      // console.log("allowedIds changed")
      return true
    }
    // console.log("allowedIds not changed")
    return false
  }

  navChanged(nextProps) {
    if (!this.props.nav || !nextProps.nav) {
      return true
    }
    if (
      !arraysWithSameElements(
        Object.keys(this.props.nav),
        Object.keys(nextProps.nav)
      )
    ) {
      return true
    }
    const navKeys1 = Object.keys(this.props)
      .filter(k => /^nav(?=[1-9])/.test(k))
      .sort((a, b) => a.split("nav")[1] - b.split("nav")[1])
    const navKeys2 = Object.keys(nextProps)
      .filter(k => /^nav(?=[1-9])/.test(k))
      .sort((a, b) => a.split("nav")[1] - b.split("nav")[1])
    if (!arraysWithSameElements(navKeys1, navKeys2)) {
      return true
    }
    return false
  }

  expandMenu() {
    if (!this.unmount) {
      this.setState(prevState => ({ menuExpanded: !prevState.menuExpanded }))
    }
  }

  handleNotAllowed() {
    toast("Selected functionality is not available to you. Redirecting to dashboard.", {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.WARNING
    })
    this.timeout = window.setTimeout(() => {
      this.props.history.push("/dashboard")
    }, CONST.ToastTimeout)
  }

  async checkAllowedNavs(props) {
    const { nav1, nav, token } = props
    if (token !== localStorage.getItem("token")) {
      toast(
        "You are being logged out for security reasons. Please login again.",
        {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.WARNING
        }
      )
      this.props.signOut()
    }
    if (token) {
      const isPlatformAdmin = false // await checkPlatformAdmin(token)
      if (isPlatformAdmin) {
        if (!this.unmount) {
          this.setState({ allowedIds: {} })
        }
        return true
      }
    }
    const navKeys = Object.keys(props)
      .filter(k => /^nav(?=[1-9])/.test(k))
      .sort((a, b) => a.split("nav")[1] - b.split("nav")[1])
    if (
      token &&
      nav1 &&
      nav1 !== "signin" &&
      !PUBLIC_ROUTES.includes(nav1) &&
      nav
    ) {
      // const idsData = await getNavIds(token)
      // const ids = idsData.data || {}
      let ids = {}
      const regExp = /[^a-zA-Z0-9]/
      const { nav2 } = props
      if (nav2 && nav2.length === 24 && !regExp.test(nav2)) {
        ids = await checkNavAccessEntitlrment(nav2)
      }
      // console.log("updating state")

      if (!this.unmount) {
        this.setState({ allowedIds: ids })
      }
      // console.log("ids  ", ids)
      // console.log("nav  ", nav)
      // console.log("navKeys  ", navKeys)
      let val = ""
      let allowed = true
      const allNavL1Keys = Object.keys(nav)
      navKeys.some((k, i) => {
        if (i === 0) {
          // console.log(" k, i : ", k, i)
          if (allNavL1Keys.includes(props[k])) {
            val = nav[props[k]]
            // console.log(" val00 : ", val)
          } else {
            let foundL2 = false
            allNavL1Keys.some(k1 => {
              // console.log(" k1 : ", k1, nav[k1])
              if (nav[k1] && nav[k1][props[k]]) {
                foundL2 = true
                val = nav[k1][props[k]]
                // console.log(" val01 : ", val)
                return true
              }
            })
            if (!foundL2) {
              allowed = false
              return true
            }
          }
          // console.log(" k, i val1 : ", k, i, val)
          if (val === true) {
            // return true
          } else if (!val) {
            allowed = false
            return true
          }
        } else if (
          val &&
          Object.getPrototypeOf(val).constructor.name === "Object"
        ) {
          // let val1 = val[props[k]] || ids.all.includes(props[k])
          // console.log(" k, i, val2 : ", k, i, val, props[k])
          // if(val1 === true) {
          //   // return true
          // } else if(!val1) {
          //   allowed = false
          //   return true
          // }
          if (val[props[k]]) {
            val = val[props[k]]
          } else if (!ids.view) {
            allowed = false
            return true
          } else if (!val[props[k]] && k === "nav3") {
            allowed = false
            return true
          }
        } else if (val === true) {
          // console.log(" k, i val3 : ", k, i, val)
          // return true
        } else if (!val && !ids.view) {
          // console.log(" k, i val4 : ", k, i, val)
          allowed = false
          return true
        }
      })
      console.log("allowed : ", allowed)
      // allowed = true
      if (!allowed && nav1 !== "dashboard") {
        // console.log("Not Authorized - moving")
        this.setState({ waiting: true }, this.handleNotAllowed)
      }
      // if(!navOptionsFlat[nav1] && nav1 !== "dashboard") {
      //   console.log("Not Authorized - moving")
      //   // this.props.history.push("/dashboard")
      // }
    }
  }

  async protectRoutes(props) {
    // console.log("in protectRoutes ", props)
    const { nav1, loginDetails, exp } = props
    if (exp) {
      this.setExpiryTimeout(exp)
    }
    // const { navOptions } = this.state
    if (
      loginDetails &&
      loginDetails.userEntities &&
      loginDetails.userEntities[0] &&
      loginDetails.userEntities[0].userId
    ) {
      if (!nav1 || nav1 === "signin") {
        // console.log("moving to dashboard after setting nav")
        this.props.history.push("/dashboard")
      } else {
        // console.log("calling checkAllowedNavs 2")
        await this.checkAllowedNavs(props)
      }
    }
  }

  signOut() {
    // console.log("signing out")
    this.props.signOut()
  }

  renderNavs(options) {
    // console.log("options : ", options)
    let { navOption } = this.props
    navOption = navOption || "dashboard"
    const navItems = []
    const sortedOptions = Object.keys(options).sort(
      (a, b) =>
        (navLabels[a] ? navLabels[a].order : 0) -
        (navLabels[b] ? navLabels[b].order : 0)
    )
    sortedOptions.forEach((e, i) => {
      if (options[e]) {
        // console.log("e : ", e)
        if (Object.getPrototypeOf(options[e]).constructor.name === "Object") {
          const subItems = []
          const nestedRoutes = { ...options[e] }
          const sortedNestedKeys = Object.keys(nestedRoutes).sort(
            (a, b) =>
              (navLabels[a] ? navLabels[a].order : 0) -
              (navLabels[b] ? navLabels[b].order : 0)
          )
          sortedNestedKeys.forEach((k, j) => {
            if (options[e][k]) {
              // console.log("k : ", k)
              const itemClass =
                navOption === k ? "navbar-item is-active" : "navbar-item"
              if (navLabels[k]) {
                subItems.push(
                  <Link
                    key={e + k + i + j}
                    className={itemClass}
                    to={`/${navLabels[k].route || k}`}
                  >
                    {navLabels[k].label}
                  </Link>
                )
              }
            }
          })
          // navItems.push(
          //   <div key={`${e}div${i}`} className="navbar-item has-dropdown is-hoverable">
          //     {e === "admin" ?
          //       <Link to="/admin">Admin</Link>
          //       :
          //       <a className="navbar-link" disabled selected>
          //         {navLabels[e] || e}
          //       </a>
          //     }
          //     {e === !"admin" ?
          //       <div className="navbar-dropdown ">
          //         {subItems}
          //       </div>
          //       : undefined
          //     }
          //   </div>
          // )
          if (navLabels[e]) {
            navItems.push(
              <div
                key={`${e}div${i}`}
                className="navbar-item has-dropdown is-hoverable"
              >
                <a className="navbar-link" disabled selected>
                  {navLabels[e].label}
                </a>
                <div className="navbar-dropdown ">{subItems}</div>
              </div>
            )
          }
        } else {
          const itemClass =
            navOption === e ? "navbar-item is-active" : "navbar-item"
          // console.log("e : ", e)
          if (navLabels[e]) {
            navItems.push(
              <Link
                key={e + i}
                to={`/${navLabels[e].route || e}`}
                className={itemClass}
              >
                {navLabels[e].label}
              </Link>
            )
          }
        }
      }
    })
    navItems.push(
      <div key="logout-touch-div" className="navbar-item is-hoverable is-touch">
        {" "}
        <a key="privacy-policy- touch" target="_blank"
                href="https://www.munivisor.com/privacy.php" className="navbar-item">
         Privacy Policy
       </a>
        <a key="logout - touch" onClick={this.signOut} className="navbar-item">
          Logout
        </a>
      </div>
    )
    // console.log("navItems : ", navItems)
    return navItems
  }

  renderNavBar(navOptions, menuExpanded) {
    const { _id, userEntities } = this.props.loginDetails
    const { userFirstName, userLastName, firmName } = userEntities[0]
    // console.log(this.props)
    return (
      <nav id="navbar" className="navbar is-gray is-fixed-top">
        {/* <BreadCrumb/> */}
        <div id="specialShadow" className="bd-special-shadow" />
        <div className="navbar-brand">
          {/* <Link to="/dashboard" className="navbar-item"> */}
          {/* <img src="/images/munivisor.png" alt="MuniVisor" /> */}
          {/* </Link> */}

          <div
            id="navbarBurger"
            className={
              menuExpanded
                ? "navbar-burger burger is-active"
                : "navbar-burger burger"
            }
            onClick={this.expandMenu}
            data-target="navMenuDocumentation"
          >
            <span />
            <span />
            <span />
          </div>
        </div>
        <div
          id="navMenuDocumentation"
          className={menuExpanded ? "navbar-menu is-active" : "navbar-menu"}
        >
          <div className="navbar-start">{this.renderNavs(navOptions)}</div>

          <div className="navbar-end is-hidden-touch ipad-nav">
            {/*
              <div className="navbar-item is-hidden-touch">
                <h4>{`${userFirstName} - ${userLastName} -${_id}`}</h4>
              </div>
            */}

            <NavSearch
              loginDetails={this.props.loginDetails}
              allowedIds={this.state.allowedIds}
            />
            {/* <a href="/dash-alt" className="navbar-item">
              <span className="icon is-small">
                <i className="fas fa-exclamation is-solid" />
              </span>
            </a>
            <a href="" className="navbar-item">
              <span className="icon is-small">
                <i className="fa fa-bell is-solid" />
              </span>
    </a> */}
            {/* <a href="/messages" className="navbar-item">
              <Envelope token={this.props.token} />
            </a> */}
            {/* <a href="" className="navbar-item">
              <span className="icon is-small">
                <i className="fa fa-sliders-h" />
              </span>
  </a> */}
            {/* <Link to="/messages" className="navbar-item">
              <Envelope token={this.props.token} />
            </Link> */}
            {this.props.nav && Object.keys(this.props.nav).includes("admin") ? (
              <Link to="/admin" className="navbar-item">
                <span className="icon is-small">
                  <i className="fab fa-adn" />
                </span>
              </Link>
            ) : (
              undefined
            )}

            <div className="navbar-item navbar-item-config is-hoverable has-dropdown is-hidden-touch">
              <a className="navbar-link">
                <span className="icon is-small">
                  <i className="fas fa-cog" />
                </span>
              </a>
              <div className="navbar-dropdown is-right">
                <Link to="/userprofile" className="navbar-item is-hidden-touch">
                  <span className="icon is-small">
                    <i className="fa fa-user" />
                  </span>
                  User Profile
                </Link>
                {/* <a className="navbar-item is-hidden-touch" href="/userprofile">
                  <span className="icon">
                    <i className="fa fa-user" />
                  </span>
                  User Profile
                </a> */}
                <hr className="navbar-divider" />
                <a
                className="navbar-item is-hidden-touch"
                target="_blank"
                href="https://www.munivisor.com/privacy.php">
                <span className="icon">
                    <i className="fa fa-lock" />
                  </span>
                Privacy Policy
                </a>
                <hr className="navbar-divider" />
                <a
                  className="navbar-item is-hidden-touch"
                  onClick={this.signOut}
                >
                  <span className="icon">
                    <i className="fa fa-power-off" />
                  </span>
                  Logout
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }

  renderPrivateComponent(nav1) {
    if (nav1 && routesMap.hasOwnProperty(nav1)) {
      const { allowedIds } = this.state
      const { loginDetails } = this.props
      if (
        !allowedIds ||
        !(
          loginDetails &&
          loginDetails.userEntities &&
          loginDetails.userEntities[0] &&
          loginDetails.userEntities[0].userId
        )
      ) {
        return <Loader />
      }
      const RouteComponent = withRouter(routesMap[nav1])
      return (
        <Suspense fallback={<Loader />}><RouteComponent
          {...this.props}
          {...componentProps[nav1]}
          allowedIds={allowedIds}
        /></Suspense>
      )
    }
    return <h4>404 - Page not found</h4>
  }

  renderPublicComponent(nav1) {
    if (routesMap.hasOwnProperty(nav1)) {
      const PublicComponent = withRouter(routesMap[nav1])
      return  <Suspense fallback={<Loader />}><PublicComponent /></Suspense>
    }
    return <h4>404 - Page not found</h4>
  }

  render() {
    const navOptions = this.props.nav
    const { nav1 } = this.props
    const { menuExpanded, allowedIds, waiting } = this.state

    // console.log("nav1 : ", nav1, allowedIds)

    if (waiting) {
      return <Loader />
    }

    // console.log(PUBLIC_ROUTES)
    if (nav1 && PUBLIC_ROUTES.includes(nav1)) {
      return this.renderPublicComponent(nav1)
    }

    if (this.props.authenticated) {
      if (Object.keys(navOptions).length) {
        return (
          <div>
            {this.renderNavBar(navOptions, menuExpanded)}
            <div style={{ marginTop: 50 }}>
              {this.renderPrivateComponent(nav1 || "dashboard")}
            </div>
            {/* <a href="/dashboard" className="float">
              <i className="fa fa-home my-float" />
            </a> */}
            <div className="label-container">
              <div className="label-text">Dashboard</div>
              <i className="fa fa-play label-arrow" />
            </div>
          </div>
        )
      }
      return <Loader />
    }
    const SignIn = withRouter(GlobalSignIn)
    // console.log("render signin")
    return <SignIn />
  }
}

const mapStateToProps = ({
  nav,
  auth: { token, authenticated, loginDetails, exp },
  urls
}) => ({
  nav,
  token,
  authenticated,
  loginDetails,
  exp,
  allurls: urls ? urls.allurls : {}
})

export default connect(
  mapStateToProps,
  { getNavOptions, checkAuth, signOut }
)(NavMain)
