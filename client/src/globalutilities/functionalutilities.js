/* Library of functions to enable us to perform extensive set of complex operations */
import { isUndefined, isNull } from "util"

/* Partially Applied Functions */
export const partialApply = (fn, ...argset1) => (...argset2) =>
  fn(...argset1, ...argset2)

/* Compose all the functions where the output of one function is used as input to the other */
export const composeAllFuncs = (...fns) => x =>
  fns.reduce((acc, fn) => fn(acc), x)

/* Return the first value of an array */

export const getFirstObjectFromArray = arr => arr[0]
/* Returns true if all the predicates are true */

export const allTrue = (...fns) => fns.reduce((retBoolVal, fn) => fn() && retBoolVal, true)

/* Returns true if any of the predicates is true */
export const anyTrue = (...fns) => fns.reduce((retBoolVal, fn) => fn() || retBoolVal, false)

/* Return functions based on the predicate */
export const ifElse = (predicate, fn1, fn2) => (predicate() ? fn1 : fn2)

/* Return the same function */
export const identity = fn => fn

/* Return true if the passed fields have values */
export const hasValues = (...values) =>
  values.reduce((retBoolVal, val) => {
    const v =
      retBoolVal && !isNull(val) && !isUndefined(val) && (!!val)
    return v
  }, true)

/* Computes and checks if all the values are greater than the first value */
export const valGreaterThanAllValues = (firstVal, restValues) => restValues.reduce((acc, y) => (isUndefined(y) || isNull(y) ? false : firstVal > y) && acc, true)

/* Returns an array of strings if they match a particular in an array of values */
// The first field contains the string to be searched. The rest are the values that will be searched on.
export const findMatchingFields = (searchString, ...valuesArray) => {
  const patt = new RegExp(searchString, "i")
  return valuesArray.reduce(
    (filteredValues, y) =>
      patt.test(y) ? [...filteredValues, y] : [...filteredValues],
    []
  )
}

/* Generic Utilities that can be used with any picklist that has a dependency structure */

export const getLevelOneValues = (LIST, keyname) => LIST.reduce((aggregatedList, listItem) => [...aggregatedList, listItem[keyname]], [])

export const getLevelTwoValuesForLevelOne = (
  LIST,
  level1KeyName,
  level1KeyValue,
  level2KeyName
) => LIST.reduce((aggregatedLevelTwoList, listItem) => {
  if (listItem[level1KeyName] === level1KeyValue) {
    return [...aggregatedLevelTwoList, ...listItem[level2KeyName]]
  }
  return aggregatedLevelTwoList
}, [])

export const getFlatNestedArrayDetails = (
  LIST,
  pickName = "id",
  keyValues,
  consolidationKey,
  transformKey = "type",
  valueKey = "value"
) => LIST.reduce((flattenedArrayElements, listItem) => {
  const listValues = keyValues.reduce((extractListValues, keyVal) => {
    if (listItem[pickName] === keyVal) {
      return [...extractListValues, ...listItem[consolidationKey]].map(
        val => ({ [transformKey]: keyVal, [valueKey]: val })
      )
    }
    return [...extractListValues]
  }, [])
  return [...flattenedArrayElements, ...listValues]
}, [])

// Number With Commas
export const numberWithCommas = (x) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

/* This regex is used to validate email IDs on the platform */
export const validateEmail = (...valuesArray) => {
  // eslint-disable-next-line no-useless-escape
  const regexString = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const patt = new RegExp(regexString)
  return valuesArray.reduce(
    (filteredValues, y) => [...filteredValues, patt.test(y)],
    []
  )
}

export const createMergedObjectArray = (keys, values) => {
  const keysArray = [...keys]
  const valuesArray = [...values]
  return keysArray.reduce((finalArray, key, index) => [...finalArray, { [key]: valuesArray[index] }], [])
}

const formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2
})

export const formatCurrency = n => formatter.format(n)

export const makeActionCreator = (type, ...argNames) => function(...args) {
  const action = { type }
  argNames.forEach((arg, index) => {
    action[argNames[index]] = args[index]
  })
  return action
}

// Check if this is an object or an array
export const isObject = (a)=> (!!a) && (a.constructor === Object)
export const isArray = (a)=> (!!a) && (a.constructor === Array)

// Compare two arrays that have objects in them.
const compareTwoObjectArrays = (x,y,keyForChange) => {
  const finalResults = x.reduce(  (xacc,xobj, whichxobject) => {
    const diffs = y.reduce ( (yacc, yobj) => {
      const ykeys = Object.keys(yobj)
      const matches = ykeys.reduce ((accyval, ykey) => {
        const m = yobj[ykey] === xobj[ykey]
        return {...accyval,...{some:accyval.some || m, all:accyval.all && m}}
      },{some:false,all:true} )
      const { some, all} = matches
      let allmatch
      let somematch

      if( all ) {
        allmatch = yobj

      }
      else if ( some && !all) {
        somematch = yobj
      }
      else if ( !some && !all) {
        allmatch = null
        somematch = null
      }

      return {...yacc,...{
        some:yacc.some || matches.some,
        all:yacc.all || matches.all,
        allmatch: yacc.allmatch ? yacc.allmatch : allmatch,
        somematch:yacc.somematch ? yacc.somematch : somematch
      }}
    },{})

    // if all is true then don't have to include
    if (!diffs.all && diffs.some) {
      return {...xacc, ...{updated:[...xacc.updated,{ind:whichxobject,newValue:xobj,oldValue:diffs.somematch}]}}
    }
    else if ( !diffs.all && !diffs.some) {
      return {...xacc,
        ...{[keyForChange]:[...xacc[keyForChange],{ind:whichxobject,newValue:keyForChange==="added"? xobj:null,oldValue:keyForChange==="added"? null:xobj}]}}
    }
    return {...xacc}
  },{updated:[],added:[],deleted:[]})
  // console.log(JSON.stringify(finalResults,null,2))
  return finalResults
}

const arrayObjectComparison = (x,y) => {
  const res1 = compareTwoObjectArrays(x,y,"added")
  const res2 = compareTwoObjectArrays(y,x,"deleted")
  return{...res1,...{deleted:res2.deleted}}
}

// if this is a basic array of primitives then compare the values here
const basicArrayComparison =( x,y) => {
  const diffxy = x.reduce ((acc,xval) => {
    const {added} = acc
    if( !y.includes(xval)) {
      return {...acc,...{added:[...added,xval]}}
    }
    return {...acc}
  },{deleted:[], added:[]})

  const finalDiff = y.reduce ((acc,yval) => {
    const {deleted} = acc
    if( !x.includes(yval)) {
      return {...acc,...{deleted:[...deleted,yval]}}
    }
    return {...acc}
  },{...diffxy})
  return finalDiff
}

export const compareObjectsAndCreateAuditLogs =(newObj,oldObj) => {
  const consolidatedChanges = []
  let parentKeys = ""

  const compareObjects = (newObj, oldObj) => {
    const keysNewObj = Object.keys(newObj)
    const keysOldObj = Object.keys(oldObj)
    console.log("keysOldObj==>>>",keysOldObj)

    // Get new keys that have been added
    // Get keys that have been modified
    // get keys that are the same and would need to be drilled into.
    const addKeys = keysNewObj.reduce ( (acc,v1) => {
      if( !keysOldObj.includes(v1) ) {
        consolidatedChanges.push({key: parentKeys ? `${parentKeys}.${v1}` : v1,operation:"added",oldValue:"",newValue:JSON.stringify(newObj[v1])})
        return  {...acc,...{[v1]:`Added value - ${newObj[v1]}`}}
      }
      return acc
    },{})
    const remKeys = keysOldObj.reduce ( (acc,v1) => {
      if(!keysNewObj.includes(v1)) {
        consolidatedChanges.push({key: parentKeys ? `${parentKeys}.${v1}` : v1,operation:"removed",oldValue:JSON.stringify(oldObj[v1]),newValue:"",})
        return {...acc,...{[v1]:`Removed value - ${oldObj[v1]}`}}
      }
      return acc
    },{})

    const sameKeys = keysOldObj.reduce ( (acc,v1) => keysNewObj.includes(v1) ? [...acc,v1] :[...acc],[])

    const loopKey = parentKeys
    const sameKeyDiffs = sameKeys.reduce( (accDiffs, sameKey) => {
      const newVal = newObj[sameKey]
      const oldVal = oldObj[sameKey]

      let diffValue = null
      let nonEmptyDifferences = null
      // case where the old and the new values are plain arrays with primitive types
      if( isArray(oldVal || []) && isArray(newVal || [])) {
        const isArrayOfObjects = oldVal.reduce( (arrOfObj,v) => isObject(v) && arrOfObj,true)
        if(isArrayOfObjects) {
          diffValue = arrayObjectComparison(newVal,oldVal)

          nonEmptyDifferences = Object.keys(diffValue).reduce( (nonEmpty, v) => {
            if(diffValue[v].length > 0) {
              const diffArray = diffValue[v]
              if(v === "added" ) {
                diffArray.forEach (da => {
                  consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"added",oldValue:"",newValue:JSON.stringify(da.newValue)})
                })
              }
              if(v=== "deleted") {
                diffArray.forEach (da => {
                  consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"removed",oldValue:JSON.stringify(da.oldValue),newValue:""})
                })            }
              if(v === "updated") {
                diffArray.forEach (da => {
                  consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"updated",oldValue:JSON.stringify(da.oldValue),newValue:JSON.stringify(da.newValue)})
                })            }
              return {...nonEmpty,...{[v]:diffValue[v]}}
            }
            return nonEmpty
          },{})
        }
        else {
          diffValue = basicArrayComparison(newVal,oldVal)
          nonEmptyDifferences = Object.keys(diffValue).reduce( (nonEmpty, v) => {
            if(diffValue[v].length > 0) {
              if(v === "added" ) {
                consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"added",oldValue:"",newValue:JSON.stringify(diffValue[v])})
              }
              if(v=== "deleted") {
                consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"removed",oldValue:JSON.stringify(diffValue[v]),newValue:""})
              }
              return {...nonEmpty,...{[v]:diffValue[v]}}
            }
            return nonEmpty
          },{})
        }
        const isEmptyDiffs = Object.keys(diffValue).reduce( (isEmpty, v) => diffValue[v].length === 0 && isEmpty, true)

        // if there are no differences then return the previous value.
        if( !isEmptyDiffs ) {

          const log = {[sameKey]: nonEmptyDifferences}
          return { ...accDiffs, ...log}
        }
        return { ...accDiffs}
      }
      // case where the old and the new values are objects
      if( isObject(oldVal || {}) && isObject(newVal || {})) {
        parentKeys = parentKeys ? `${parentKeys}.${sameKey}` : sameKey
        diffValue = compareObjects(newVal, oldVal)
        return { ...accDiffs, ...{[sameKey]: diffValue}}
      }

      // if it doesn't meet any of the conditions then it will fall into the default case
      if(newVal !== oldVal) {
        const log = {[sameKey]: `Changed value from ${oldVal} to ${newVal}`}
        consolidatedChanges.push({key: loopKey ? `${loopKey}.${sameKey}` : sameKey,operation:"updated",oldValue:oldVal,newValue:newVal})
        return { ...accDiffs, ...log}
      }
      // if none of the conditions is met then just return the previous accumulator
      return accDiffs

    },{})
    return {added:addKeys, deleted:remKeys, updated:sameKeyDiffs}
  }
  const consolidatedDetailedLog = compareObjects(newObj,oldObj)
  return {logs:consolidatedChanges, detailedLogs:consolidatedDetailedLog}

}




// ///////////////////////////////////////////////////////////////////////////
/* Testing of functions */
// //////////////////////////////////////////////////////////////////////////

/* Testing partial apply function */
// const newfunc = partialApply(Math.max)
// const finalvalue = newfunc(100, 200, 300)

// console.log(newfunc)
// console.log(finalvalue)

// /* testing compose all functions */
// const state = {}
// const dealObject = [
//   {
//     _id: "123",
//     dealName: "deal1",
//     dealType: "negotiate",
//     participants: [
//       { pid: 1, type: "issuer", name: "city of princeton" },
//       { pid: 2, type: "bondcounsel", name: "Bond Lawyers of America" }
//     ]
//   },
//   {
//     _id: "124",
//     dealName: "deal2",
//     dealType: "negotiate",
//     participants: [
//       { pid: 1, type: "issuer", name: "city of princeton" },
//       { pid: 2, type: "bondcounsel", name: "Bond Lawyers of America" }
//     ]
//   }
// ]
// state.deals = dealObject
// state.crm = dealObject

// const valToBeModified1 = composeAllFuncs(
//   x => x + 1,
//   x => x * 5,
//   y => y * 20,
//   y1 => y1
// )(45)

// // const valToBeModified2 = composeAllFuncs(
// //   // eslint-disable-next-line no-underscore-dangle
// //   R.prop(R.__, state),
// //   R.filter(({ _id }) => _id === "124"),
// //   R.head,
// //   R.prop("participants"),
// //   R.filter(({ pid }) => pid === 1),
// //   R.head
// // )("deals")

// // console.log(valToBeModified1)
// // console.log(valToBeModified2)

// /* Test Any and All True */

// console.log(anyTrue(() => false, () => false))
// console.log(anyTrue(() => true, () => false))
// console.log(anyTrue(() => true, () => true))
// console.log(anyTrue(() => false, () => true))

// console.log(allTrue(() => false, () => false))
// console.log(allTrue(() => true, () => false))
// console.log(allTrue(() => true, () => true))
// console.log(allTrue(() => false, () => true))

// /* Testing ifElse */
// let retFunction = ifElse(() => true, Math.max, Math.min)
// console.log("The return function from the IfElse", retFunction(1, 2, 3, 4, 5))

// /* Testing Identify Function */
// retFunction = identity(Math.max)
// console.log(
//   "The value returned from the identify function",
//   retFunction(1, 2, 3, 4, 5)
// )

// /* Testing the Has Values Function */
// retFunction = hasValues(1, 2, 3, "", 34)
// console.log("Do all the values contain valid values", retFunction)

// /* Test the Search String in the list of values */

// retFunction = findMatchingFields(
//   "VEEN",
//   "naveen",
//   "balawat",
//   "soujanya",
//   "vishnav"
// )
// console.log("The values returned from the match function is", retFunction)

// retFunction = validateEmail(
//   "naveen@otaras.com",
//   "naveen",
//   "this@this.com",
//   "awesome"
// )

// console.log("The email validation of fields", retFunction)

// retFunction = createMergedObjectArray(
//   ["fname", "lastname"],
//   ["naveen", "balawat"]
// )

// console.log("The Merged Object Array is", retFunction)
export const getHeaders = () => {
  const headers = {
    "Content-Type": "application/json"
  }

  const credentials = process.env.ES_CREDENTIALS

  if (credentials) {
    headers.Authorization = `Basic ${btoa(credentials)}`
  }
  return headers
}
