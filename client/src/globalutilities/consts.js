import Joi from "joi-browser";

const getApiEndPoint = () => "http://localhost:3004/api"

// export const muniApiBaseURL = "/api/" export const muniAuthBaseURL = "/auth/"

export const MAX_NAV_LEVELS = 10
export const PUBLIC_ROUTES = [
  "stp-redirect",
  "splash",
  "signup",
  "not-available",
  "forgotpass",
  "changeforgotpass",
  "requesttoken"
]
export const muniApiBaseURL = process.env.API_URL
  ? `${process.env.API_URL}/api/`
  : "/api/"
export const muniAuthBaseURL = process.env.API_URL
  ? `${process.env.API_URL}/auth/`
  : "/auth/"

export const FRONTENDURL = process.env.FRONTEND_URL
  ? `${process.env.FRONTEND_URL}`
  : `${process.env.FRONTEND_URL}`

export const passwordStrength = process.env.PASSWORD_STRENGTH === ""
  ? "false"
  : process.env.PASSWORD_STRENGTH

export const token = localStorage.getItem("token") || ""

export const NOTIFICATIONS_MODES = ["app", "email", "sms"]

export const STRIPE_CLIENT_PUBLIC_KEY= "pk_test_7L7OC6QD9MKlgDpZhCy6dJM800sZ8BD4Ku"

export const TIME_UNITS = [
  "minute",
  "hour",
  "day",
  "week",
  "month",
  "year"
]

export const COPYTABS = {
  "BOND ISSUE": [
    {name: "Summary", key: "summary"},
    {name: "Details", key: "details"},
    {name: "Participants", key: "participants"},
    {name: "Pricing And Rating", key: "pricing"},
    {name: "Check-n-Track", key: "check-track"},
  ],
  "BANK LOAN": [
    {name: "Summary", key: "summary"},
    {name: "Details", key: "details"},
    {name: "Participants", key: "participants"},
    {name: "Check-n-Track", key: "check-track"},
  ],
  "DERIVATIVE":[
    {name: "Summary", key: "summary"},
    {name: "Details", key: "details"},
    {name: "Participants", key: "participants"},
    {name: "Trade(Swap) OR Trade(Cap)", key: "swapCap"}
  ],
  "RFP": [
    {name: "Summary", key: "summary"},
    {name: "Distribute", key: "distribute"},
    {name: "Manage", key: "manage"},
    {name: "Check-n-Track", key: "check-track"},
  ],
  "OTHERS": [
    {name: "Summary", key: "summary"},
    {name: "Details", key: "details"},
    {name: "Participants", key: "participants"},
    {name: "Pricing And Rating", key: "pricing"},
    {name: "Check-n-Track", key: "check-track"},
  ],
  "MA-RFP": [
    {name: "Summary", key: "summary"},
    {name: "Details", key: "details"},
    {name: "Participants", key: "participants"},
  ],
  "BUSINESSDEVELOPMENT": [],
}

export const DAY_WEEK_MONTHS = {
  weekly: [
    {
      label: "Sun",
      value: 1
    }, {
      label: "Mon",
      value: 2
    }, {
      label: "Tue",
      value: 3
    }, {
      label: "Wed",
      value: 4
    }, {
      label: "Thu",
      value: 5
    }, {
      label: "Fri",
      value: 6
    }, {
      label: "Sat",
      value: 7
    }
  ],
  monthly: [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ].map(e => ({ label: `${e}`, value: e }))
}

export const RFP_ASSIGNED_TO = ["Evaluation Team", "RFP Contacts", "RFP Assignees"]

export const oppMsg = {
  won: "The opportunity will be transitioned to a transaction/project. The status will b" +
    "e defaulted to Pre-pricing/Open. Are you sure you want to proceed?",
  lost: "You have lost this opportunity. Conversion to transaction/project not possible. " +
    "Are you sure you want to proceed?",
  cancel: "You have chosen not to proceed with this opportunity. Conversion to transaction/" +
    "project not possible. Are you sure you want to proceed?"
}

export const primaryPicklists = [
  "LKUPACCRUEFROM",
  "LKUPADDRESSTYPE",
  "LKUPAMT",
  "LKUPBANKQUALIFIED",
  "LKUPBORROWER",
  "LKUPBUSSTRUCT",
  "LKUPCALLFEATURE",
  "LKUPCDCEVENTNOTICE",
  "LKUPCDCFILINGRESPONSIBILITY",
  "LKUPCDCVOLADDTIONAL",
  "LKUPCDCVOLFINANCIALINFO",
  "LKUPCDCVOLOTHERINFO",
  "LKUPCEP",
  "LKUPCLIENTCONTRACTTYPE",
  "LKUPCONFLICTOFINTEREST",
  "LKUPCONTACTTYPE",
  "LKUPCONTRACTRENEWAL",
  "LKUPCONTRACTSTATUS",
  "LKUPCORPTYPE",
  "LKUPCORRESPONDENCEDOCS",
  "LKUPCOSTSOFISSUANCE",
  "LKUPCOUPONFREQUENCY",
  "LKUPCREDITENHANCEMENTTYPE",
  "LKUPCRMBORROWER",
  "LKUPDAYCOUNT",
  "LKUPDEALFILECHECKLIST",
  "LKUPDEALOFFERING",
  "LKUPDELIVERYDOCS",
  "LKUPDERIVATES",
  "LKUPDISCLOSERDOCUMENTS",
  "LKUPDOCACTION",
  "LKUPDOCSHARESTATUS",
  "LKUPDOCSTATUS",
  "LKUPDOCTYPE",
  "LKUPDOLLARRANGE",
  "LKUPDUEDILIGENCEDOCUMENTS",
  "LKUPENGAGEMENTREVIEWDOCUMENTS",
  "LKUPFAACTIVITYTYPE",
  "LKUPFAENGAGEDINDEAL",
  "LKUPFAFEECONTRACTTYPE",
  "LKUPFEDTAX",
  "LKUPFIRMNAME",
  "LKUPFORM",
  "LKUPG17DISCLOSURES",
  "LKUPG17TYPE",
  "LKUPGUARANTOR",
  "LKUPINSURANCE",
  "LKUPISSUER",
  "LKUPLEGALDOCS",
  "LKUPLEGALOPINIONS",
  "LKUPMAEXEMPTION",
  "LKUPMARKETINGMATERIALS",
  "LKUPMONEYTRANSFERTYPE",
  "LKUPNAMEPREFIX",
  "LKUPNAMESUFFIX",
  "LKUPNUMOFPEOPLE",
  "LKUPOUTLOOK",
  "LKUPPARTICIPANTTYPE",
  "LKUPPHONETYPE",
  "LKUPPRESALEDOCTYPE",
  "LKUPPRICINGSTATUS",
  "LKUPRAINSURERDOCS",
  "LKUPRATETYPE",
  "LKUPRATINGTERM",
  "LKUPREGISTRANTTYPE",
  "LKUPDEBTTYPE",
  "LKUPRETAILORDERPERIODRELATED",
  "LKUPROLEWITHENTITY",
  "LKUPSALEPRICINGDOCS",
  "LKUPSECTORS",
  "LKUPSECTYPEGENERAL",
  "LKUPSTATE",
  "LKUPTASKTYPE",
  "LKUPSTATETAX",
  "LKUPTAXSTATUS",
  "LKUPTRANSACTION",
  "LKUPTRANSACTIONSTATUS",
  "LKUPTRANSACTIONTYPE",
  "LKUPUNDERWRITER",
  "LKUPUNDERWRITERROLE",
  "LKUPUSERENTITLEMENT",
  "LKUPUSEOFPROCEED",
  "LKUPUWCOMMITTMENTDOCS",
  "LKUPUWCOUNSELDOCS",
  "LKUPUWREVENUEDOCS",
  "LKUPYESNO",
  "LKUPSCHEDULEOFEVENTS",
  "TEST123"
]

export const navLabels = {
  new: {
    label: "New",
    order: 1
  },
  master: {
    label: "Master Lists",
    order: 2
  },
  compliance: {
    label: "Compliance",
    order: 3
  },
  tools: {
    label: "Tools",
    order: 4
  },
  "addnew-activity": {
    label: "Transaction/Project",
    order: 5
  },
  "addnew-entity": {
    label: "Entity",
    order: 6
  },
  "addnew-contact": {
    label: "Contact",
    order: 7
  },
  "addnew-task": {
    label: "Task",
    order: 8
  },
  "mast-cltprosp": {
    label: "Entity - Clients / Prospects",
    order: 9
  },
  "mast-thirdparty": {
    label: "Entity - 3rd Party",
    order: 10
  },
  "mast-allcontacts": {
    label: "All Contacts",
    order: 11
  },
  "comp-polandproc": {
    label: "Policies & Procedures",
    order: 12,
  },
  // "comp-formLinks": {
  //   label: "Forms & Links",
  //   order: 13,
  // },
  supervisor: {
    label: "Compliance Action Center (CAC)",
    order: 14,
    route: "compliance"
  },
  "tools-billing": {
    label: "Billing",
    order: 15
  },
  "tools-docs": {
    label: "Manage Documents",
    order: 16
  },
  "tools-revpipe": {
    label: "Revenue Pipeline",
    order: 17
  },
  "tools-docsearch": {
    label: "Documents Search",
    order: 18
  },
  "tools-globalsearch": {
    label: "Global Search",
    order: 19
  }
}

export const navLabelsOld = {
  addnew: "New",
  "addnew-entity": "Entity",
  "addnew-activity": "Activity",
  "addnew-contact": "Contact",
  "addnew-task": "Task",
  dash: "User Dashboard",
  "dash-task": "Tasks",
  "dash-projs": "Projects",
  "dash-cltprosp": "Clients & Prospects",
  "dash-cal": "Calendar",
  "dash-busdev": "Bus Dev",
  mast: "Master Lists",
  "mast-cltprosp": "Entity Clients and Propsects",
  "mast-thirdparty": "Third Parties",
  "mast-allcontacts": "All Contacts",
  comp: "Compliance",
  "comp-msrbsecfil": "MSRB & SEC filings",
  "comp-polandproc": "Policies & Procedures",
  "comp-formLinks": "Form & Links",
  "comp-stbusentfilandlicenses": "State Business Entity Filings & Licenses",
  "comp-orgdocuments": "Compliance Organization Documents",
  "comp-engletandforms": "Compliance Engagement Letter and Forms",
  "comp-custcomp": "Compliance Customer Complaints",

  tools: "Tools",
  "tools-analytics": "Analytics",
  "tools-revpipe": "Revenue Pipeline",
  "tools-billing": "Billing",
  "tools-chat": "Chat",
  "tools-docs": "Documents",
  "tools-emma": "Emma Content",
  "tools-globalsearch": "Global Search",
  admin: "Admin",
  "admin-firms": "Firm",
  "admin-firmusers": "Firm Users",
  "admin-clients": "Clients",
  "admin-thirdparties": "Third Parties",
  "admin-config": "Configuration",
  "admin-migtools": "Migration",
  "admin-usage": "Usage",
  "admin-billing": "Billing",
  // admin: "Admin",
  firms: "Firms",
  users: "Users",
  clients: "Clients",
  "third-parties": "3rd Parties",
  configuration: "Configuration",
  "migration-tools": "Migration Tools",
  usage: "Usage",
  billing: "Billing",
  process: "Process",
  transactions: "Transactions",
  opportunities: "Opportunities",
  dashboard: "User Dashboard",
  data: "Data",
  documents: "Documents",
  "emma-content": "EMMA Content",
  relationships: "Relationships",
  //  tools: "Tools",
  chat: "Chat",
  search: "Search",
  analytics: "Analytics",
  calculators: "Calculators",
  arbitrage: "Arbitrage",
  pricingStructuring: "Pricing Structuring",
  allocations: "Allocations"
}

export const navLabelsOrig = {
  admin: "Admin",
  firms: "Firms",
  users: "Users",
  clients: "Clients",
  "third-parties": "3rd Parties",
  configuration: "Configuration",
  "migration-tools": "Migration Tools",
  usage: "Usage",
  billing: "Billing",
  process: "Process",
  transactions: "Transactions",
  opportunities: "Opportunities",
  dashboard: "User Dashboard",
  data: "Data",
  documents: "Documents",
  "emma-content": "EMMA Content",
  relationships: "Relationships",
  tools: "Tools",
  chat: "Chat",
  search: "Search",
  analytics: "Analytics",
  calculators: "Calculators",
  arbitrage: "Arbitrage",
  pricingStructuring: "Pricing Structuring",
  allocations: "Allocations"
}

export const docTableHeaders = [
  {
    name: "name",
    label: "File Name"
  }, {
    name: "uploadDate",
    label: "Uploaded At"
  }, {
    name: "download",
    label: "Download"
  }, {
    name: "docuSign",
    label: "E-Sign"
  }, {
    name: "share",
    label: "Share"
  }, {
    name: "details",
    label: "More"
  }
]

export const eSignTableHeaders = [
  {
    name: "signerEmail",
    label: "Signer Email"
  }, {
    name: "sent",
    label: "Sent"
  }, {
    name: "delivered",
    label: "Delivered"
  }, {
    name: "signed",
    label: "Signed"
  }
]

export const secondarySector = [
  "LKUPEDUSECSECTOR",
  "LKUPHCSECSECTOR",
  "LKUPHSGSECSECTOR",
  "LKUPINDSECSECTOR",
  "LKUPPSSECSECTOR",
  "LKUPRECRSECSECTOR",
  "LKUPTRANSSECSECTOR",
  "LKUPUTILSECSECTOR",
  "LKUPOTHERSECSECTOR",
  "Charter Schools"
]

export const states = [
  {
    countryName: "United States",
    states: [
      {
        stateShortName: "NJ",
        stateFullName: "New Jersey",
        counties: ["Middlesex", "Monmouth"]
      }, {
        stateShortName: "FL",
        stateFullName: "Florida",
        counties: ["Baker", "Bradford"]
      }
    ]
  }
]

const leg = {
  currency: "",
  notional: "",
  interest: "",
  fixedRate: "",
  referenceRate: "",
  spread: "",
  frequency: "",
  initialRate: "",
  effectiveDate: "",
  maturityDate: "",
  dayCount: "",
  businessConvention: "",
  bankHolidays: "",
  endOfMonth: "",
  securityGroup: "",
  securityType: ""
}

export const ContextType = {
  entity: "ENTITYDOCS",
  rfp: "RFPS",
  deal: "DEALS",
  loan: "BANKLOANS",
  maRfp: "MARFPS",
  derivative: "DERIVATIVES",
  others: "OTHERS",
  supervisor: {
    svAndObligation: "SVOBLIGATIONS",
    svProQualification: "SVPROFQUALIFICATIONS",
    svAdmin: "SVGENADMIN",
    svComplaints: "SVCLIENTCOMPLAINTS",
    businessConduct: "SVBUSINESSCONDUCT",
    poliProducers: "POLICIESPROCEDURES",
    msrbSubmissions: "SVMSRBSUBMISSIONS",
    poliContributeRelated: "SVPOLICONTRELATEDDOCS",
    giftsAndGrautities: "SVGIFTSGRATUITIES"
  },
  billing: {
    receipts: "BILLINGEXPENSERECEIPTS"
  },
  busDev: "BUSDEV",
  contract: "ENTITYCONTRACT",
  cac: "CONTROLS",
  thirdParty: "THIRDPARTYDOCS",
  generalDoc: "GENERALDOCS",
  user: "USERDOCS",
  usersDocs: "USERSDOCS",
  shareDocs: "SHAREDOCS",
  tasks: "TASKS",
  entityLogo: "ENTITYLOGO",
  migrationTools: "MIGRATIONTOOLS"
}

export const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

export const sortNameStateList = {
  AL: "Alabama",AK: "Alaska",AZ: "Arizona",
  AR: "Arkansas",CA: "California",CO: "Colorado",
  CT: "Connecticut",DE: "Delaware", DC: "District of Columbia",
  FL: "Florida",GA: "Georgia",HI: "Hawaii",ID: "Idaho",
  IL: "Illinois",IN: "Indiana",IA: "Iowa",
  KS: "Kansas",KY: "Kentucky",LA: "Louisiana",
  ME: "Maine",MD: "Maryland",MA: "Massachusetts",
  MI: "Michigan",MN: "Minnesota",MS: "Mississippi",
  MO: "Missouri",MT: "Montana",NE: "Nebraska",
  NV: "Nevada",NH: "New Hampshire",NJ: "New Jersey",
  NM: "New Mexico",NY: "New York",NC: "North Carolina",
  ND: "North Dakota",OH: "Ohio",OK: "Oklahoma",
  OR: "Oregon",PA: "Pennsylvania",RI: "Rhode Island",
  SC: "South Carolina",SD: "South Dakota",TN: "Tennessee",
  TX: "Texas",UT: "Utah",VT: "Vermont",VA: "Virginia",
  WA: "Washington",WV: "West Virginia",WI: "Wisconsin",
  WY: "Wyoming",PR: "Puerto Rico"
}

const CONST = {
  periodData: [
    {
      key: "Select All",
      value: "",
    },
    {
      key: "Month to Date",
      value: "MTD",
    },
    {
      key: "Quarter to Date",
      value: "QTD",
    },
    {
      key: "Year to Date",
      value: "YTD",
    },
    {
      key: "Previous Year",
      value: "PREVYEAR",
    },
    {
      key: "Previous 5 Years",
      value: "PREV5YEARS",
    },
    {
      key: "Previous 10 Years",
      value: "PREV10YEARS",
    }
  ],
  rfpMemberEvaluations: {
    rfpParticipantFirmId: "",
    rfpEvaluationCommitteeMemberId: "",
    rfpParticipantFirmName: "",
    rfpParticipantMSRBType: "",
    rfpCommitteeMemberFirstName: "",
    rfpCommitteeMemberLastName: "",
    rfpCommitteeMemberEmailId: "",
    evalNotes: "",
    rfpEvaluationCategories: [
      {
        categoryName: "costOfService",
        categoryItems: [
          {
            evalItem: "Cost of Service"
          }
        ]
      }, {
        categoryName: "qualifications",
        categoryItems: [
          {
            evalItem: "Size / Organization structure"
          }, {
            evalItem: "Experience with similar transactions"
          }, {
            evalItem: "Level and types of insurance"
          }, {
            evalItem: "Net capital and excess net capital"
          }, {
            evalItem: "Qualifications of key personnel"
          }, {
            evalItem: "Reference for key personnel"
          }, {
            evalItem: "Analytic capabilities"
          }, {
            evalItem: "Affirmative action"
          }
        ]
      }, {
        categoryName: "financingApproach",
        categoryItems: [
          {
            evalItem: "General financing approach"
          }, {
            evalItem: "Market for the bonds"
          }, {
            evalItem: "Strategies for issuance"
          }, {
            evalItem: "Credit objectives"
          }, {
            evalItem: "Recommendation on co-managers"
          }, {
            evalItem: "Transaction-specific issues"
          }
        ]
      }, {
        categoryName: "legalIssues",
        categoryItems: [
          {
            evalItem: "No violations of laws or regulations"
          }, {
            evalItem: "No pending or current litigation"
          }, {
            evalItem: "No conflicts of interest"
          }
        ]
      }
    ]
  },
  rfpEvaluations: {
    rfpEvaluationCategories: {
      costOfService: [
        {
          evalItem: "Cost of Service"
        }
      ],
      qualifications: [
        {
          evalItem: "Size / Organization structure"
        }, {
          evalItem: "Experience with similar transactions"
        }, {
          evalItem: "Level and types of insurance"
        }, {
          evalItem: "Net capital and excess net capital"
        }, {
          evalItem: "Qualifications of key personnel"
        }, {
          evalItem: "Reference for key personnel"
        }, {
          evalItem: "Analytic capabilities"
        }, {
          evalItem: "Affirmative action"
        }
      ],
      financingApproach: [
        {
          evalItem: "General financing approach"
        }, {
          evalItem: "Market for the bonds"
        }, {
          evalItem: "Strategies for issuance"
        }, {
          evalItem: "Credit objectives"
        }, {
          evalItem: "Recommendation on co-managers"
        }, {
          evalItem: "Transaction-specific issues"
        }
      ],
      legalIssues: [
        {
          evalItem: "No violations of laws or regulations"
        }, {
          evalItem: "No pending or current litigation"
        }, {
          evalItem: "No conflicts of interest"
        }
      ]
    },
    rfpParticipantFirmId: "",
    rfpEvaluationCommitteeMemberId: "",
    evalNotes: ""
  },
  EvalCategories: [
    {
      name: "Cost of Service",
      key: "costOfService",
      listItems: [
        {
          evalItem: "Cost of Service"
        }
      ]
    }, {
      name: "Qualifications of firm and key personnel",
      key: "qualifications",
      listItems: [
        {
          evalItem: "Size / Organization structure"
        }, {
          evalItem: "Experience with similar transactions"
        }, {
          evalItem: "Level and types of insurance"
        }, {
          evalItem: "Net capital and excess net capital"
        }, {
          evalItem: "Qualifications of key personnel"
        }, {
          evalItem: "Reference for key personnel"
        }, {
          evalItem: "Analytic capabilities"
        }, {
          evalItem: "Affirmative action"
        }
      ]
    }, {
      name: "Financing approach",
      key: "financingApproach",
      listItems: [
        {
          evalItem: "General financing approach"
        }, {
          evalItem: "Market for the bonds"
        }, {
          evalItem: "Strategies for issuance"
        }, {
          evalItem: "Credit objectives"
        }, {
          evalItem: "Recommendation on co-managers"
        }, {
          evalItem: "Transaction-specific issues"
        }
      ]
    }, {
      name: "Legal issues / conflicts of interest",
      key: "legalIssues",
      listItems: [
        {
          evalItem: "No violations of laws or regulations"
        }, {
          evalItem: "No pending or current litigation"
        }, {
          evalItem: "No conflicts of interest"
        }
      ]
    }
  ],
  CheckInTrack: [
    {
      key: "purposeOfRequest",
      name: "Name of Issuer and Purpose of Request",
      checkListCategoryId: "purposeOfRequest",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Name and brief description of the issuer",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Type of service being requested",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }, {
      key: "relevantInformation",
      checkListCategoryId: "relevantInformation",
      name: "Description of the issuer and summary of relevant information",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Information pertaining to financing plans",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Overview of the organization, purpose, administrative functions, etc.",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Legal authority of issue debt",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Capital plans/projects to be financed",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Planned issuances / refunding",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Outstanding debt, maturity schedules, interest rates, debt service payments",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Ratings",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }, {
      key: "scopeOfService",
      checkListCategoryId: "scopeOfService",
      name: "Scope of Services",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Specific services desired",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Duration of contract and options for renewal",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }, {
      key: "selectionCriteria",
      checkListCategoryId: "selectionCriteria",
      name: "Selection Criteria",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Criteria used to evaluate proposals",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Weighting of each criteria",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }, {
      key: "timetableAward",
      checkListCategoryId: "timetableAward",
      name: "Timetable to award contract",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Date of pre-bid conference",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Deadline for proposal submissions",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Schedule for proposal review",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Date(s) for oral interviews",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Date for final decision/legislative action",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }, {
      key: "instructionsProposals",
      checkListCategoryId: "instructionsProposals",
      name: "Instructions for submitting proposals",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Date and time due",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Deadline for proposal submissions",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Office/individual to whom to address proposals",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Contact to whom questions may be addressed and any limitations",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Special instructions, if any",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }, {
      key: "miscellaneous",
      name: "Miscellaneous",
      checkListCategoryId: "miscellaneous",
      checkListItems: [
        {
          rfpListItemConsider: true,
          rfpListItemDetails: "Terms and conditions",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }, {
          rfpListItemConsider: true,
          rfpListItemDetails: "Documents to conform with state / local procurement law or policy",
          rfpListItemPriority: "",
          rfpListItemAssignees: [],
          rfpListItemEndDate: "",
          rfpListItemResolved: false
        }
      ]
    }
  ],
  actTranUnderwriters: {
    partFirmId: "",
    partType: "Underwriters",
    partFirmName: "",
    roleInSyndicate: "",
    liabilityPerc: 0,
    managementFeePerc: 0,
    partContactAddToDL: false,
    isNew: true
  },
  OtherRating: {
    otherCepRatings: {
      cepName: "",
      seriesId: "",
      seriesName: "",
      cepType: "",
      longTermRating: "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: ""
    },
    otherAgencyRatings: {
      seriesId: "",
      seriesName: "",
      ratingAgencyName: "",
      longTermRating: "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: ""
    }
  },
  dealIssueParticipants: {
    dealPartFirmId: "",
    dealPartType: "",
    dealPartFirmName: "",
    dealPartContactId: "",
    dealPartContactName: "",
    dealPartContactEmail: "",
    dealPartContactAddrLine1: "",
    dealPartContactAddrLine2: "",
    dealParticipantState: "",
    dealPartContactPhone: "",
    addToDL: false,
    isNew: true
  },
  dealIssueUnderwriters: {
    dealPartFirmId: "",
    dealPartType: "Underwriters",
    dealPartFirmName: "",
    roleInSyndicate: "",
    liabilityPerc: 0,
    managementFeePerc: 0,
    dealPartContactAddToDL: false,
    isNew: true
  },
  dealIssueDocuments: {
    docCategory: "",
    docSubCategory: "",
    docType: "",
    docAWSFileLocation: "",
    docFileName: "",
    markedPublic: {
      publicFlag: false,
      publicDate: new Date()
    },
    sentToEmma: {
      emmaSendFlag: false,
      emmaSendDate: new Date()
    }
  },
  DealRating: {
    dealCepRatings: {
      cepName: "",
      seriesId: "",
      seriesName: "",
      cepType: "",
      longTermRating: "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: ""
    },
    dealAgencyRatings: {
      seriesId: "",
      seriesName: "",
      ratingAgencyName: "",
      longTermRating: "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: ""
    }
  },
  contract: {
    dealIssueTradingInformation: {
      contractID: "",
      contractStatus: "",
      portfolio: "",
      portfolioGroup: "",
      counterParty: "",
      securityGroup: "",
      securityType: "",
      brokerOrDealer: "",
      tradeDate: "",
      settlementDays: "",
      settlementDate: ""
    },
    dealIssueContractInformation: {
      standardContract: "",
      receive: "",
      currency: "",
      notional: "",
      fixedRate: "",
      referenceRate: "",
      spread: "",
      initialRate: "",
      effectiveDate: "",
      tenor: "",
      tenorType: ""
    },
    dealIssueReceiveLeg: {
      ...leg
    },
    dealIssuePayLeg: {
      ...leg
    },
    dealIssueCompliance: {
      status: "",
      canRelease: ""
    }
  },
  dealIssueSeriesDetails: {
    seriesName: "",
    description: "",
    tag: ""
  },
  actTranSeriesDetails: {
    seriesName: "",
    description: "",
    tag: ""
  },
  OtherSeriesPricingDetails: {
    seriesPrincipal: "",
    seriesSecurityType: "",
    seriesSecurity: "",
    seriesSecurityDetails: "",
    seriesDatedDate: null,
    seriesSettlementDate: null,
    seriesFirstCoupon: null,
    seriesPutDate: null,
    seriesRecordDate: null,
    seriesCallDate: null,
    seriesFedTax: "",
    seriesStateTax: "",
    seriesPricingAMT: "",
    seriesBankQualified: "",
    seriesAccrueFrom: "",
    seriesPricingForm: "",
    seriesCouponFrequency: "",
    seriesDayCount: "",
    seriesRateType: "",
    seriesCallFeature: "",
    seriesCallPrice: 0,
    seriesInsurance: "",
    seriesUnderwtiersInventory: "",
    seriesMinDenomination: 0,
    seriesSyndicateStructure: "",
    seriesGrossSpread: 0,
    seriesEstimatedTakeDown: 0,
    seriesInsuranceFee: 0
  },
  seriesPricingDetails: {
    dealSeriesPrincipal: "",
    dealSeriesSecurityType: "",
    dealSeriesSecurity: "",
    dealSeriesSecurityDetails: "",
    dealSeriesDatedDate: null,
    dealSeriesSettlementDate: null,
    dealSeriesFirstCoupon: null,
    dealSeriesPutDate: null,
    dealSeriesRecordDate: null,
    dealSeriesCallDate: null,
    dealSeriesFedTax: "",
    dealSeriesStateTax: "",
    dealSeriesPricingAMT: "",
    dealSeriesBankQualified: "",
    dealSeriesAccrueFrom: "",
    dealSeriesPricingForm: "",
    dealSeriesCouponFrequency: "",
    dealSeriesDayCount: "",
    dealSeriesRateType: "",
    dealSeriesCallFeature: "",
    dealSeriesCallPrice: 0,
    dealSeriesInsurance: "",
    dealSeriesUnderwtiersInventory: "",
    dealSeriesMinDenomination: 0,
    dealSeriesSyndicateStructure: "",
    dealSeriesGrossSpread: 0,
    dealSeriesEstimatedTakeDown: 0,
    dealSeriesInsuranceFee: 0
  },
  seriesPricingData: {
    term: "",
    maturityDate: "",
    amount: "",
    coupon: "",
    yield: "",
    price: "",
    YTM: "",
    cusip: "",
    callDate: "",
    takeDown: "",
    insured: false,
    drop: false
  },
  Loan: {
    bankLoanSummary: {
      actTranType: "Bank Loan",
      actTenorMaturities: "",
      actTranBorrowerName: "",
      actTranObligorName: "",
      actTranState: "",
      actTranCounty: "",
      actTranClosingDate: "",
      actTranUseOfProceeds: "",
      actTranEstimatedRev: null
    },
    bankLoanTerms: {
      parAmount: null,
      fedTax: "",
      /* optional */
      stateTax: "",
      bankQualified: "",
      /* optional */
      paymentType: "",
      prinPaymentFreq: "",
      prinPaymentStartDate: null,
      prinPaymentDay: null,
      intPaymentFreq: "",
      intPaymentStartDate: null,
      intPaymentDay: null,
      fixedRate: 0,
      floatingRateRef: "",
      floatingRateRatio: null,
      floatingRateSpread: null,
      dayCount: "",
      busConvention: "",
      bankHolidays: "",
      prePaymentTerms: ""
    },
    bankLoanLinkCusips: {
      cusip: "",
      description: "",
      tag: ""
    },
    bankLoanAmort: {
      reductionDate: "",
      prinAmountReduction: "",
      reviedPrinAmount: "",
      isNew: true
    }
  },
  Derivative: {
    derivativeSummary: {
      tranIssuer: "",
      tranProjectDescription: "",
      tranType: "",
      tranBorrowerName: "",
      tranGuarantorName: "",
      tranNotionalAmt: "",
      tranNotionalAmtFlag: false,
      tranTradeDate: "",
      tranEffDate: "",
      tranEndDate: "",
      tranCounterpartyClient: "",
      tranCounterpartyDealer: "",
      tranCounterpartyType: "",
      tranLastMTMValuation: "",
      tranLastMTMDate: "",
      tranMTMProvider: "",
      tranEstimatedRev: null
    },
    derivativeCounterparties: {
      cntrParty: "11",
      cntrPartyFirmName: "",
      cntrPartyFirmtype: "",
      cntrPartyMoodysRating: "",
      cntrPartyFitchRating: "",
      cntrPartyKrollRating: "",
      cntrPartySPRating: ""
    },
    derivativeParticipants: {
      partType: "",
      partFirmId: "",
      partFirmName: "", // May not need this
      partContactId: "",
      partContactName: "",
      partContactEmail: "", // Same reasoning as above
      partContactAddrLine1: "", // Same reasoning as above
      partContactAddrLine2: "", // Same reasoning as above
      participantState: "", // Same reasoning as above
      partContactPhone: "",
      partContactAddToDL: false
    },
    TradeSwap: {
      payLeg: {
        payor: "", // Should be a drop down of issuers, dealers, obligors
        currency: "",
        paymentType: "",
        upfrontPayment: "",
        tradeDate: "",
        effDate: "",
        fixedRate: "",
        floatingRateType: "",
        floatingRateRatio: "",
        floatingRateSpread: "",
        firstPaymentDate: "",
        stub: "",
        paymentSchedule: "",
        paymentBusConvention: "",
        periodEndDate: "", // save the end date for cap here
        periodEndDateBusConvention: "",
        initialIndexSetting: "",
        dayCount: "",
        additionalPayments: "" // applicable for cap
      }
    },
    TradeCap: {
      derivTradeClientPayLeg: {
        payor: "", // Should be a drop down of issuers, dealers, obligors
        currency: "",
        paymentType: "",
        upfrontPayment: "",
        fixedRate: "",
        floatingRateType: "",
        floatingRateRatio: "",
        floatingRateSpread: "",
        tradeDate: "",
        effDate: "",
        periodEndDate: "",
        additionalPayments: "" // applicable for cap
      }
    }
  },
  Marfp: {
    maRfpSummary: {
      actState: "",
      actCounty: "",
      actPlaceholder1: "",
      actPlaceholder2: "",
      actPlaceholder3: "",
      actOfferingType: "",
      actSecurityType: "",
      actBankQualified: "",
      actCorpType: "",
      actParAmount: "",
      actPricingDate: "",
      actExpAwaredDate: " ",
      actActAwardDate: "",
      actEstRev: "",
      actUseOfProceeds: "",
      actOppName: "",
      actOppType: "",
      actOppStatus: ""
    }
  },
  CREATE: {
    loan: {
      actTranType: "",
      actTranSubType: "",
      actTranOtherSubtype: "",
      actTranUniqIdentifier: "", // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
      actTranFirmId: "", // Entity ID
      actTranFirmName: "",
      actTranClientId: "", // Entity ID
      actTranClientName: "", // Name of the Issuer Client
      actTranPrimarySector: "",
      actTranSecondarySector: "",
      actTranFirmLeadAdvisorId: "", // User Id
      actTranFirmLeadAdvisorName: "", // First Name and Last Name
      actTranIssueName: "",
      actTranProjectDescription: "",
      actTranRelatedTo: [],
      actTranNotes: "",
      actTranRelatedType: "",
      opportunity: false
    },
    MARFP: {
      actType: "",
      actSubType: "",
      actOtherSubtype: "",
      actStatus: "",
      actLeadFinAdvClientEntityId: "",
      actLeadFinAdvClientEntityName: "",
      actIssuerClient: "",
      actIssuerClientEntityName: "",
      actIssueName: "",
      actProjectName: "",
      actPrimarySector: "",
      actSecondarySector: "",
      actTranNotes: "",
      actNotes: "",
      actRelatedType: "",
      actRelTrans: [],
      opportunity: false
    },
    DocCommunicationDetails: {
      docCategory: "Compliance",
      docSubCategory: "Policy & Procedures",
      docType: "",
      docAWSFileLocation: "",
      docFileName: "",
      status: "",
      subject: "",
      notes: "",
      reviewer: "",
      reviewerRole: "",
      lastReviewedOn: null,
      lastUpdateBy: "",
      // docStatus: "", // WIP, send for review and other flags
      /* docActions:{
        actionType: "",
        actionDate: new Date().toString()
      }, */
      docCommunication: {
        owners: [],
        toRecipients: [],
        ccRecipients: [],
        // communicationSubject: "", communicationNotes: ""
      }
    }
  },
  Participants: {
    isNew: true,
    partType: "",
    partFirmId: "",
    partFirmName: "",
    partContactId: "",
    partContactName: "",
    partContactEmail: "",
    partUserAddress: "",
    partContactAddrLine1: "",
    partContactAddrLine2: "",
    participantState: "",
    partContactPhone: "",
    partContactAddToDL: false
  },
  Documents: {
    docCategory: "",
    docSubCategory: "",
    docType: "",
    docAWSFileLocation: "",
    docAction: "",
    docFileName: "",
    markedPublic: {
      publicFlag: false,
      publicDate: new Date().toString()
    },
    sentToEmma: {
      emmaSendFlag: false,
      emmaSendDate: new Date().toString()
    },
    createdBy: "",
    docNote: "",
    createdUserName: ""
  },
  SuperVisor: {
    bcaContraDetails: {
      /* userId: "",
      userFirstName: "",
      userLastName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      violataionDate: "", */
      violationType: "",
      violationNotes: "",
      recordDate: "",
      closeDate: "",
      assPersonName: "",
      docAWSFileLocation: "",
      docFileName: "",
    },
    pqQualifiedPersons: {
      qualification: "",
      userId: "",
      userFirstName: "",
      userLastName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      // userOrgRole: "",
      profFeePaidOn: "",
      userSeries50PassedOnDate: "",
      userSeries50ValidEndDate: ""
    },
    pqTrainingPrograms: {
      trgProgramName: "",
      trgOrganizationDate: "",
      trgConductedBy: "",
      trgAttendees: "",
      trgNotes: ""
    },
    gaDesignateFirmContacts: {
      userFirstName: "",
      userLastName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      userAddress: "",
      userContactType: ""
    },
    gaFirmRegInformation: {
      registeringBody: "",
      regFeePaidOn: "",
      feeAmount: "",
      regValidTill: "",
    },
    gaQualifiedAssPersons: {
      qualification: "",
      userFirstName: "",
      userLastName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      profFeePaidOn: "",
      series50PassDate: "",
      series50ValidityEndDate: ""
    },
    businessActivities: {
      activity: "",
      subActivity: "",
      notes: "",
    },
    gaGeneralFirmInfo: {
      field: "",
      notes: "",
      information: "",
    },
    trainingDetail: {
      topic: "",
      programName: "",
      trainingDate: "",
      conductedBy: "",
      conductedOn: "",
      AWSFileLocation: "",
      fileName: "",
      attendees: [],
      notes: ""
    },
    responsibleSupervision: {
      userFirstName: "",
      userEntityId: "",
      userLastName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      userRoleFirm: "",
      userComplianceDesignation: "",
      /* userActive: false, */
      startDate: "",
      endDate: ""
    },
    annualCertification: {
      certifiedOn: "",
      certifiedBy: {
        userId: "",
        userFirstName: "",
        userEntityId: "",
        userLastName: "",
        userPrimaryEmailId: ""
      },
      certifierDesignation: "",
      docCategory: "", // Default to "Supervisory Obligations"
      docSubCategory: "", // Default to "Annual Certification"
      docAWSFileLocation: "",
      docFileName: "",
      docNotes: ""
    },
    complainantAssocPersonDetails: {
      userId: "",
      userFirstName: "",
      userMiddleName: "",
      userLastName: "",
      userEntityId: "",
      userEntityName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      userPrimaryAddress: ""
    },
    contributorDisclosure: {
      userId: "",
      userFirstName: "",
      userMiddleName: "",
      userLastName: "",
      userFirmName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      userPrimaryFax: "",
      userAddressName: "",
      userAddressType: "",
      userAddressLine1: "",
      userAddressLine2: "",
      country: "",
      state: "",
      city: "",
      zip1: "",
      zip2: ""
    },
    contribMuniEntityByState: {
      // userId: "", userName: "",
      userAddressConsolidated: "",
      state: "",
      // contributionReceipientDetails: {   userId: "",   userFirstName: "",
      // userLastName: "",   userAddressConsolidated: "" },
      contributorCategory: "",
      contributionAmount: "",
      contributionDate: "",
      exemptionG37RuleExemption: ""
    },
    paymentsToPartiesByState: {
      state: "",
      politicalPartyDetails: {
        partyName: "",
        partyAddress: ""
      },
      contributorCategory: "",
      paymentAmount: "",
      paymentDate: ""
    },
    ballotContribByState: {
      userId: "",
      state: "",
      ballotDetail: "",
      contributorCategory: "",
      contributionAmount: "",
      contributionDate: ""
    },
    reimbursements: {
      state: "",
      ballotDetail: "",
      userAddressConsolidated: "",
      /* thirdPartyDetails:{
        userId: "",
        userFirstName: "",
        userLastName: "",
        userAddressConsolidated: ""
      }, */
      thirdPartyCategory: "",
      reimbursementAmt: "",
      reimbursementDate: ""
    },
    ballotApprovedOfferings: {
      entityId: "",
      municipalEntityName: "",
      processDescription: "",
      reportablesaleDate: ""
    },
    disclosureSection1WithNegativeAnswers: {
      sectionHeader: "",
      sectionItems: {
        sectionItemDesc: "",
        sectionAffirm: false
      }
    },
    disclosureSectionWithPositiveAnswers: {
      sectionHeader: "",
      sectionItems: {
        sectionItemDesc: "",
        sectionAffirm: false
      }
    },
    disclosureInfo: {
      year: "", // Store this as an Integer
      quarter: "",
      disclosureFor: "",
      G37Obligation: false,
      affirmationNotes: ""
    },
    giftDisclosureInfo: {
      year: "", // Store this as an Integer
      quarter: "",
      disclosureFor: "",
      G20Obligation: false,
      affirmationNotes: ""
    },
    giftsToRecipients: {
      recipientEntityId: "",
      recipientEntityName: "",
      recipientUserId: "",
      recipientUserFirstName: "",
      recipientUserLastName: "",
      giftValue: "",
      giftDate: ""
    }
  },
  NewCompliance: {
    politicalContributions: {
      contribMuniEntityByState: {
        nameOfMuniEntity: "",
        titleOfMuniEntity: "",
        state: "",
        city: "",
        county: "",
        otherPoliticalSubdiv: "",
        contributorCategory : "",
        contributionAmount : "",
        contributionDate : "",
        exempted : false,
        checkNone : false,
        dateOfExemption : "",
      },
      paymentsToPartiesByState: {
        state: "",
        city: "",
        county: "",
        politicalParty: "",
        contributorCategory: "",
        contributionAmount: "",
        contributionDate: "",
        checkNone : false
      },
      ballotContribByState: {
        nameOfBondBallot: "",
        nameOfIssueMuniEntity: "",
        state: "",
        city: "",
        county: "",
        otherPoliticalSubdiv: "",
        contributorCategory: "",
        contributionAmount: "",
        contributionDate: "",
        reimbursements: "",
        reimbursementsAmount: "",
        checkNone : false
      },
    },
    clientComplaint: {
      administration: {
        field: "",
        information: "",
      },
    }
  },
  CAC: {
    busConduct: [
      {
        title: "You are able to send periodic automated notifications to associated persons with" +
          " custom checklists for them to affirm. e.g.",
        list: [
          "* No municipal advisor nor an associated person of a municipal advisor shall eng" +
          "age in municipal advisory activities in contravention of restrictions imposed on" +
          " the municipal advisor by the SEC pursuant to the Exchange Act (e.g., suspension" +
          ", revocation of registration or bar).",
          "* A municipal advisor shall deal fairly with all persons and shall not engage in" +
          " any deceptive, dishonest or unfair practice "
        ]
      }
    ],
    polContribution: [
      {
        title: "You are able to send periodic automated notifications to associated persons with" +
          " custom checklists for them to affirm. e.g.",
        list: ["* Submit to the MSRB, by the last day of the month, following the end of each ca" +
          "lendar quarter, the information requested on Form G-37."]
      }
    ],
    genAdmin: [
      {
        title: "",
        list: [
          "* Register with the SEC", "* Register with the MSRB", "* Notify a registered securities association or similar of intent to engage as M" +
          "A",
          "* Filing Form A-12",
          "* Paying the applicable fees",
          "* Form A-12 must be amended within 30 days of information becoming inaccurate",
          "* The annual affirmation of Form A-12 must be completed within 17 business days " +
          "after January 1 of each calendar year",
          "* Promptly amend Form MA: At least annually, within 90 days of the end of a muni" +
          "cipal advisor’s fiscal year or the end of the calendar year for a sole proprieto" +
          "r",
          "* Records are maintained/accessible to permit independent examination",
          "* Municipal advisor is responsible for the annual registration fee that is due b" +
          "y October 31 each year",
          "* Annual professional fee for each of its qualified associated persons that is d" +
          "ue by April 30 each year",
          "* Municipal advisor must designate firm contacts via Form A-12 and is required t" +
          "o amend Form A-12 to update the designated contacts as changes occur",
          "* Municipal advisor must maintain a copy of the email received from the MSRB con" +
          "firming submission and acceptance of Form A-12"
        ]
      }
    ],
    profQulifications: [
      {
        title: "",
        list: [
          "* Municipal advisor may not engage in municipal advisory activities unless it is" +
          " properly qualified",
          "* An associated person must pass the Municipal Advisor Representative Qualificat" +
          "ion Examination (Series 50) prior to engaging in municipal advisory activities.",
          "* Each associated person directly engaged in the management, direction or superv" +
          "ision of the activities of the municipal advisor and that of its associated pers" +
          "ons must pass the Series 50.",
          "* Municipal advisor must have at least one principal",
          "* A list or other record of the names, titles and business and residence address" +
          "es of all persons associated with the municipal advisor",
          "* Record of the names of persons who have been, in the past five years, associat" +
          "ed with the municipal advisor.",
          "* Annually, evaluate and prioritize the training needs for its municipal advisor" +
          " representatives and municipal advisor principals",
          "* Annually, develop a written training plan",
          "* Conduct training on applicable municipal advisory activities for such covered " +
          "persons, which shall include, at a minimum: fiduciary obligation that a municipa" +
          "l advisor owes to municipal entity clients; Applicable regulatory requirements",
          "* Copies of the firm’s needs analysis",
          "* Copies of the firm’s written training plan",
          "* Documentation of the content of the training programs",
          "* Documentation that each municipal advisor representative and municipal advisor" +
          " principal completed the applicable training programs.",
          "* Municipal advisor must maintain these records for at least five years"
        ]
      }
    ],
    svcoHeadStart: [
      {
        title: "Supervisory System",
        list: [
          "* A municipal advisor shall establish, implement and maintain a system to superv" +
          "ise the municipal advisory activities of the municipal advisor and that of its a" +
          "ssociated persons.",
          "* The supervisory system shall be reasonably designed to achieve compliance with" +
          " applicable securities laws and regulations.",
          "* WSPs are reasonably designed to ensure that the firm’s municipal advisory acti" +
          "vities are conducted in compliance with applicable rules, taking into considerat" +
          "ion, among other things, the firm’s size, organizational structure and scope of " +
          "municipal advisory activities.",
          "* WSPs are promptly amended to reflect changes in applicable rules and as change" +
          "s occur in the firm’s business and supervisory system. Any changes to a municipa" +
          "l advisor’s WSPs should be promptly communicated to applicable associated person" +
          "s.",
          "* Designation of one or more municipal advisor principals who are vested with th" +
          "e authority to carry out the supervision they are responsible for and have suffi" +
          "cient knowledge, experience and training to effectively discharge their responsi" +
          "bilities.",
          "* In the case where an associated person is supervising his or her own activitie" +
          "s, the firm’s WSPs must address the manner in which, in the absence of separate " +
          "supervisory personnel, the procedures are reasonably designed to achieve complia" +
          "nce with applicable rules."
        ]
      }, {
        title: "Compliance Processes",
        list: [
          "* A municipal advisor shall have in place and implement processes to establish, " +
          "maintain, review, test and modify the firm’s written compliance policies and WSP" +
          "s to ensure they are reasonably designed to achieve compliance with applicable r" +
          "ules.",
          "* No less than annually, a municipal advisor must conduct a review of the firm’s" +
          " compliance policies and WSPs. The review should, at a minimum, consider any com" +
          "pliance matters that arose since the previous review, any changes in the firm’s " +
          "municipal advisory activities and any changes in applicable rules."
        ]
      }, {
        title: "Chief Compliance Officer",
        list: [
          "* A municipal advisor shall designate one individual to serve as its chief compl" +
          "iance officer (CCO). The CCO shall have significant interaction with senior mana" +
          "gement regarding the firm’s comprehensive compliance program.",
          "* Gaining an understanding of the services and activities that need to be the su" +
          "bject of written compliance policies and WSPs",
          "* Identifying the applicable rules and standards of conduct pertaining to such s" +
          "ervices and activities based on experience and/or consultation with others;",
          "* Developing, or advising other persons charged with the obligation to develop, " +
          "policies and procedures that are reasonably designed to achieve compliance with " +
          "applicable rules and standards of conduct;",
          "* Developing programs to test compliance with the municipal advisor’s policies a" +
          "nd procedures",
          "* The CCO may be a principal of the firm or a non-employee of the firm.",
          "* If a non-employee, then the person designated as CCO must have the competence " +
          "described above and the municipal advisor shall retain ultimate responsibility f" +
          "or its compliance obligations.",
          "* An employee designated as the CCO may hold other positions within the firm, in" +
          "cluding serving in any position in senior management or being designated as a su" +
          "pervisory principal, provided the person can discharge the duties of CCO conside" +
          "ring all the responsibilities of any other position(s)."
        ]
      }, {
        title: "Annual Certification",
        list: ["* A municipal advisor shall have its chief executive officer(s) or an equivalent" +
          " officer annually certify in writing that the firm has the compliance processes " +
          "required under Rule G-44(b) in place."]
      }, {
        title: "Recordkeeping Requirements",
        list: [
          "* A copy of the firm’s policies and procedures", "* A copy of any policies and procedures that were in effect during the previous " +
          "five years",
          "* Records of the reviews of written compliance policies and WSPs",
          "* The annual certifications that the firm has in place processes to establish, m" +
          "aintain, review, test and modify written compliance policies and written supervi" +
          "sory procedures  ",
          "* Any certification made as to substantially equivalent supervisory and complian" +
          "ce obligations and books and records requirements with respect to exempted feder" +
          "ally regulated banks.",
          "* Such records are to be retained for a period of five years",
          "* A record of all designations of persons responsible for supervision",
          "* A record of all designations of persons as CCO.",
          "* Records reflecting such designations must be retained for the period of design" +
          "ation of each person and for at least six years following any change in designat" +
          "ion."
        ]
      }
    ],
    svcoPeople: [
      {
        title: "Annual Certification",
        list: ["* A municipal advisor shall have its chief executive officer(s) or an equivalent" +
          " officer annually certify in writing that the firm has the compliance processes " +
          "required under Rule G-44(b) in place."]
      }
    ],
    svComplaintDetails: [
      {
        title: "",
        list: [
          "- In addition to the recordkeeping requirements under the rule, a municipal advi" +
          "sor may be required to promptly report complaints to other appropriate regulator" +
          "y authorities, such as complaints alleging theft, misappropriation of funds or s" +
          "ecurities, or forgery.",
          "- In addition to the recordkeeping requirements under the rule, a municipal advi" +
          "sor may be required to promptly report complaints to other appropriate regulator" +
          "y authorities, such as complaints alleging theft, misappropriation of funds or s" +
          "ecurities, or forgery."
        ]
      }
    ],
    giftsGrautities: [
      {
        title: "You are able to send periodic automated notifications to associated persons with" +
          " custom checklists for them to affirm. e.g.",
        list: ["* Municipal advisor should aggregate all gifts given by the firm and each of its" +
          " associated persons to recipients over the course of a year."]
      }
    ]
  },
  complaint: {
    keyDetails: {
      complaintsName: "",
      organizationName: "",
      assPersonName: "",
      productCode: "",
      problemCode: "",
      complaintStatus: "",
      dateReceived: "",
      dateOfActivity: "",
      activityDetails: [],
      complaintDescription: "",
      actionAgainstComplaint: "",
      docAWSFileLocation: "",
      docFileName: "",
    },
    complainantDetails: {
      entityId: "",
      entityName: "",
      userId: "",
      userFirstName: "",
      userMiddleName: "",
      userLastName: "",
      userPrimaryEmailId: "",
      userPrimaryPhone: "",
      userFax: "",
      userAddressConsolidated: {
        country: "",
        state: "",
        city: "",
        zipCode: {
          zip1: "",
          zip2: ""
        },
        addressName: "",
        addressType: "",
        addressLine1: "",
        addressLine2: ""
      }
    },
    complainantAssocPersonDetails: []
  },
  Section: {
    check: [
      {
        title: "Contribution by MA or MA-controlled PAC to Candidate (Issuer Official with MA Se" +
          "lection Influence)",
        list: ["Jurisdiction Where MA Can NOT Vote Including Client or Potential Client", "Jurisdiction Where MA Can NOT Vote and Is NOT Client or Potential Client"]
      }, {
        title: "Contribution by MA or MA-controlled PAC to State or Local Political Party (Need " +
          "to determine ultimate beneficiary -- if not benefitting particular issuer offici" +
          "als then only the disclosure rules apply)",
        list: [
          "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote Including Client or " +
          "Potential Client",
          "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
          "ential Client"
        ]
      }, {
        title: "Contribution by MA or MA-controlled PAC to National Political Party (Need to det" +
          "ermine ultimate beneficiary -- if not benefitting particular issuer officials th" +
          "en only the disclosure rules apply)",
        list: [
          "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote Including Client or " +
          "Potential Client",
          "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
          "ential Client"
        ]
      }, {
        title: "Contribution to Charitable Organization (Even With Ties to Client/Potential Clie" +
          "nt)",
        list: ["Pass through to candidate"]
      }, {
        title: "Contribution by MA to PACs not controlled by MA (need to determine ultimate bene" +
          "ficiary)",
        list: [
          "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote Including Client-Pot" +
          "ential Client",
          "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
          "ential Client"
        ]
      }, {
        title: "Contribution by MA or MA controlled PAC to Elected Official's Inaugural Expenses",
        list: ["Jurisdiction Where MA Can NOT Vote Including Client or Potential Client", "Jurisdiction Where MA Can NOT Vote and Is NOT Client or Potential Client"]
      }, {
        title: "Soliciting or Coordinating Contributions for PAC, Political Party or Candidate (" +
          "including as part of volunteer work)",
        list: ["Benefitting Candidate Who is Client/Potential Client of MA"]
      }
    ],
    negativeAnswers: [
      {
        sectionHeader: "Contribution by MA or MA-controlled PAC to Candidate (Issuer Official with MA Se" +
          "lection Influence)",
        sectionItems: [
          {
            sectionItemDesc: "Jurisdiction Where MA Can Vote Including Client or Potential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Contribution is less than $250/election",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA-controlled PAC to State or Local Political Party (Need " +
          "to determine ultimate beneficiary -- if not benefitting particular issuer offici" +
          "als then only the disclosure rules apply)",
        sectionItems: [
          {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can Vote Including Client or Pote" +
              "ntial Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Not Benefitting Candidate",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Contribution is less than $250/year",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA-controlled PAC to National Political Party (Need to det" +
          "ermine ultimate beneficiary -- if not benefitting particular issuer officials th" +
          "en only the disclosure rules apply)",
        sectionItems: [
          {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can Vote Including Client or Pote" +
              "ntial Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Not Benefitting Candidate",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Contribution is less than $250/year",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution to Charitable Organization (Even With Ties to Client/Potential Clie" +
          "nt)",
        sectionItems: [
          {
            sectionItemDesc: "Not pass through to candidate",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA to PACs not controlled by MA (need to determine ultimate bene" +
          "ficiary)",
        sectionItems: [
          {
            sectionItemDesc: "Contribution by MA to PACs not controlled by MA (need to determine ultimate bene" +
              "ficiary)",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
              "ential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Not Benefitting Candidate",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Contribution is less than $250/year",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA controlled PAC to Bond Ballot Initiatives",
        sectionItems: [
          {
            sectionItemDesc: "In State/Local Jurisdiction Where MA Can Vote",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Contribution is less than $250/year",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA controlled PAC to Elected Official's Inaugural Expenses",
        sectionItems: [
          {
            sectionItemDesc: "Jurisdiction Where MA Can Vote Including Client or Potential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Contribution is less than $250/year",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Candidates for Federal Office (unless candidate is currently serving in state/lo" +
          "cal capacity as a client)",
        sectionItems: [],
        sectionAffirm: ""
      }, {
        sectionHeader: "Candidates for Federal Office (unless candidate is currently serving in state/lo" +
          "cal capacity as a client)",
        sectionItems: [],
        sectionAffirm: ""
      }
    ],
    positiveAnswers: [
      {
        sectionHeader: "Contribution by MA or MA-controlled PAC to Candidate (Issuer Official with MA Se" +
          "lection Influence)",
        sectionItems: [
          {
            sectionItemDesc: "Jurisdiction Where MA Can NOT Vote Including Client or Potential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Jurisdiction Where MA Can NOT Vote and Is NOT Client or Potential Client",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA-controlled PAC to State or Local Political Party (Need " +
          "to determine ultimate beneficiary -- if not benefitting particular issuer offici" +
          "als then only the disclosure rules apply)",
        sectionItems: [
          {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote Including Client or " +
              "Potential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
              "ential Client",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA-controlled PAC to National Political Party (Need to det" +
          "ermine ultimate beneficiary -- if not benefitting particular issuer officials th" +
          "en only the disclosure rules apply)",
        sectionItems: [
          {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote Including Client or " +
              "Potential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
              "ential Client",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution to Charitable Organization (Even With Ties to Client/Potential Clie" +
          "nt)",
        sectionItems: [
          {
            sectionItemDesc: "Pass through to candidate",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA to PACs not controlled by MA (need to determine ultimate bene" +
          "ficiary)",
        sectionItems: [
          {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote Including Client-Pot" +
              "ential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Benefitting Candidate in Jurisdiction Where MA Can NOT Vote But No Client or Pot" +
              "ential Client",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Contribution by MA or MA controlled PAC to Elected Official's Inaugural Expenses",
        sectionItems: [
          {
            sectionItemDesc: "Jurisdiction Where MA Can NOT Vote Including Client or Potential Client",
            sectionAffirm: ""
          }, {
            sectionItemDesc: "Jurisdiction Where MA Can NOT Vote and Is NOT Client or Potential Client",
            sectionAffirm: ""
          }
        ]
      }, {
        sectionHeader: "Soliciting or Coordinating Contributions for PAC, Political Party or Candidate (" +
          "including as part of volunteer work)",
        sectionItems: [
          {
            sectionItemDesc: "Benefitting Candidate Who is Client/Potential Client of MA",
            sectionAffirm: ""
          }
        ]
      }
    ]
  },
  confirmAlert: {
    title: "Are you sure?",
    text: "You want to delete this ?",
    buttons: true,
    dangerMode: true
  },
  politicalConfirmAlert: {
    text: "",
    buttons: ["Cancel", "Save Disclosure"],
    dangerMode: false
  },
  FAServiceFee: {
    contractRef: {
      contractId: "",
      contractName: "",
      contractType: "Schedule of Fees",
      startDate: "",
      endDate: ""
    },
    sof: {
      securityType: "",
      sofDealType: "",
      sofMin: "",
      sofMax: "",
      feesType: "",
      fees: [
        {
          sofDesc: "First",
          sofAmt: "",
          sofCharge: ""
        }, {
          sofDesc: "Next",
          sofAmt: "",
          sofCharge: ""
        }, {
          sofDesc: "Next",
          sofAmt: "",
          sofCharge: ""
        }, {
          sofDesc: "Amount Over",
          sofAmt: "",
          sofCharge: ""
        }
      ],
      feePar: [
        {
          sofDesc: "",
          sofAmt: "",
          sofCharge: ""
        }
      ]
    },
    nonTranFees: {
      role: "",
      stdHourlyRate: "",
      quoatedRate: ""
    },
    retAndEsc: {
      type: "",
      hoursCovered: "",
      effStartDate: "",
      effEndDate: ""
    },
    documents: {
      docCategory: "",
      docSubCategory: "",
      docType: "",
      docAWSFileLocation: "",
      docFileName: "",
      createdBy: "",
      createdUserName: "",
      uploadedDate: new Date()
    }
  },

  activityTask: {
    taskDetails: {
      taskAssigneeUserId: "",
      taskAssigneeName: "",
      taskNotes: "",
      taskDescription: "",
      taskStartDate: "",
      taskEndDate: "",
      taskStatus: "",
      taskType: ""
    },
    relatedActivityDetails: {
      activityId: "",
      activityType: "",
      activitySubType: "",
      activityProjectName: "",
      activityIssuerClientId: "",
      activityIssuerClientName: ""
    },
    taskHistory: {
      userId: "",
      userFirstName: "",
      userLastName: "",
      taskNotes: "",
      taskDescription: ""
    },
    taskAssigneeUserDetails: {
      userId: "",
      userFirstName: "",
      userLastName: "",
      userEntityId: "",
      userEntityName: ""
    },
    relatedEntityDetails: {
      entityId: "",
      entityName: ""
    },
    taskCreatedBy: {
      userId: "",
      userFirstName: "",
      userLastName: "",
      userEntityId: "",
      userEntityName: ""
    },
    relatedContacts: [],
    taskRelatedDocuments: []
  },

  ToastTimeout: 3000,
  API: {
    RFP_TRANSACTION: ` ${getApiEndPoint} / rfp `
  },

  bucketName: process.env.S3BUCKET,
  keyInformation: {
    proposalDate: "",
    tenorLoan: "",
    averageLife: "",
    paymentType: "",
    fixedRate: 0,
    floatingRateType: "",
    floatingRateRatio: 0,
    floatingRateSpread: 0,
    underlyingCredit: "",
    sectorCode: "",
    state: "",
    bankLender: "",
    provisionType: ""
  },
  keyInterest: {
    rateType: "",
    loanAmountFrom: 0,
    loanAmountTo: 0,
    maturities: "",
    rate: 0
  },
  outOfPocketExpenses: {
    airfare: "",
    internetCon: "",
    airportParking: "",
    carRental: "",
    carRentalGas: "",
    conferenceCall: "",
    dataTechService: "",
    faxOrCopy: "",
    lodging: "",
    meals: "",
    mileadge: "",
    tolls: "",
    transportation: "",
    totalOutOfPocketExpenses: ""
  },
  opportunityStatus: [
    "In Progress", "Won", "Lost", "Cancel/Dropped"
  ],
  tranStatus: [
    "Cancelled",
    "Closed",
    "Executed but not closed",
    "Pre-execution",
    "Pre-Pricing",
    "Pre-Munivisor"
  ],
  oppDecline: ["Lost", "Cancel/Dropped", "Cancelled", "Closed"]
}

export default CONST

export const TASK_RELATED_DOCUMENT = {
  docId: "",
  taskDocumentName: "",
  taskDocumentId: "",
  taskDocumentUploadDate: "",
  taskDocumentUploadUser: "" // Logged in user in auth
}
