import React from "react"
import axios from "axios"
import fileSize from "file-size"
import { muniVisorApplicationStore } from "AppState/store"
import moment from "moment/moment"
import {toast} from "react-toastify"
import CONST, { muniApiBaseURL } from "./consts"
import { getDocDetails, generateJSONToPDF, generateInvoicePDF, generateG10PDF, getToken } from "GlobalUtils/helpers"
import { getSignedUrl } from "../app/StateManagement/actions/docs_actions"
import { putG10RecordKeeping, fetchG10RecordKeeping } from "../app/StateManagement/actions/Supervisor"
import {sendComplianceEmail} from "../app/StateManagement/actions/Transaction"

const htmlToPdfMake = require("html-to-pdfmake")

export const pdfInvoiceDownload = async (invoiceData, fileName) => {
  await generateInvoicePDF(invoiceData, fileName)
}

export const getLogoBase64 = async (docId) => {
  let imageURL = ""
  const res = await getDocDetails(docId || "")
  console.log("res : ", res)
  const [error, doc] = res
  if (error) {
    this.setState({error})
  } else {
    const {contextId, contextType, tenantId, name} = doc
    if (tenantId && contextId && contextType && name) {
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      imageURL = `${process.env.API_URL}/api/s3/get-s3-object-as-stream?objectName=${encodeURIComponent(objectName)}`
      return imageURL
    }
  }
}

export const pdfTableDownload = async (title, data=[], fileName, logo, position) => {
  fileName = fileName || "document.pdf"
  const state = muniVisorApplicationStore.getState() || {}

  logo = logo || (state && state.logo && state.logo.firmLogo) || ""

  const location = (position && position.location && position.location.toLowerCase()) ||
    (state && state.auth && state.auth.userEntities &&
      state.auth.userEntities.settings && state.auth.userEntities.settings.location &&
      state.auth.userEntities.settings.location.toLowerCase())
    || "header"

  const align = (position && position.alignment && position.alignment.toLowerCase()) ||
    (state && state.auth &&
      state.auth.userEntities && state.auth.userEntities.settings &&
      state.auth.userEntities.settings.alignment && state.auth.userEntities.settings.alignment.toLowerCase())
    || "center"

  const firmTitle = (position && position.title) ||
    (state && state.auth &&
      state.auth.userEntities && state.auth.userEntities.settings &&
      state.auth.userEntities.settings.title)
    || ""

  const contentData = [
    { text: (firmTitle && `${firmTitle} \n\n`)  || "", style: {margin: [10, 10, 10, 20],
      alignment: align || "center",
      bold: true} }
  ]

  await generateJSONToPDF({contentData, location, align, data, fileName, logo})
}

export const sendEmailTo = async (payload) => {
  await sendComplianceEmail(payload)
}

export const updateDocsDB = async (bucketName, file, fileName, userDetails, getPDFResponse, entityDetails, docId) => {
  const {
    contextId,
    contextType,
    tenantId,
    docMeta,
    versionMeta,
    userName
  } = userDetails

  const {
    entityName,
    userPrimaryEmail,
    _id,
    userFullName
  } = entityDetails

  const meta = { ...docMeta }
  const name = userFullName && userFullName.replace(/\s+/g, "")

  const filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
  axios
    .post(`${muniApiBaseURL}/s3/get-s3-object-versions`, {
      bucketName,
      fileName: filePath
    },{headers:{"Authorization":getToken()}})
    .then(res => {
      if (res.data.error) {
        console.log("err in getting version : ", res.error)
        this.setState({
          message: "",
          error: "Error in getting S3 versions",
          inProgress: false
        })
      } else {
        console.log("got versions : ", res)
        // let latestVersion = res.Versions.filter(v => v.IsLatest)[0];
        let versionId
        let uploadDate
        let size
        res.data.result.Versions.some(v => {
          if (v.IsLatest) {
            versionId = v.VersionId
            uploadDate = v.LastModified
            size = fileSize(v.Size).human("jedec")
            return true
          }
        })
        if (versionId) {
          console.log("docId : ", docId)
          if (docId) {
            axios
              .get(`${muniApiBaseURL}/docs`, { params: { _id: docId },headers:{"Authorization":getToken()} })
              .then(res => {
                console.log("res : ", res)
                const doc = (res && res.data[0]) || {}
                const { _id } = doc
                if (_id) {
                  axios
                    .post(`${muniApiBaseURL}/docs/update-versions`, {
                      _id,
                      versions: [
                        {
                          versionId,
                          name: fileName,
                          originalName: file.name,
                          uploadDate,
                          uploadedBy: userName,
                          size,
                          // type,
                          ...versionMeta
                        }
                      ],
                      name: fileName,
                      originalName: file.name,
                      option: "add"
                    },{headers:{"Authorization":getToken()}})
                    .then(res => {
                      console.log("updated versions ok in docs db : ", res)

                      fetchG10RecordKeeping( `?id=${_id}`, (response) => {
                        response.deliveryAttemptDate = new Date()
                        response.deliveryStatus = "Failure"
                        response.entityName = entityName
                        response.recipientEmail = userPrimaryEmail
                        if(name && entityName && userPrimaryEmail){
                          const regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
                          if(regexEmail.test(userPrimaryEmail)){
                            response.deliveryStatus = "Success"
                          }
                        }
                        putG10RecordKeeping( `?type=recordKeeping&_id=${response._id}`, response, (g10Response)=> {
                          toast(`${entityName} auto generated successfully`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
                          const emailPayload = {
                            type: "cmp-sup-complaints",
                            sendEmailUserChoice: true,
                            emailParams:{
                              url: "compliance/cmp-sup-complaints/recordkeeping",
                              subject: "",
                              category: "custom",
                              message: "",
                              sendDocEmailLinks: true,
                              docIds: [_id || ""],
                              sendEmailTo: [{sendEmailTo: userPrimaryEmail}]
                            }
                          }
                          if(response.deliveryStatus === "Success"){
                            sendEmailTo(emailPayload)
                          } else {
                            toast("Something went wrong! Email not send", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
                          }
                          getPDFResponse.getPDFResponse(res)
                        })
                      })
                    })
                    .catch(err => {
                      console.log(
                        "err in updating versions in docs db : ",
                        err
                      )
                    })
                } else {
                  meta.versions = [
                    {
                      versionId,
                      name: fileName,
                      originalName: file.name,
                      uploadDate,
                      uploadedBy: userName,
                      size,
                      // type,
                    }
                  ]
                  const doc = {
                    name: fileName,
                    originalName: file.name,
                    meta,
                    contextType,
                    contextId,
                    tenantId
                  }
                  axios
                    .post(`${muniApiBaseURL}/docs`, doc,{headers:{"Authorization":getToken()}})
                    .then(res => {
                      console.log("inserted ok in docs db : ", res)
                      toast(`${entityName} auto generated successfully`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
                      getPDFResponse.getPDFResponse(res)
                    })
                    .catch(err => {
                      console.log("err in inserting in docs db : ", err)
                    })
                }
              })
              .catch(err => {
                console.log("err in getting docs : ", err)
              })
          } else {
            meta.versions = [
              {
                versionId,
                name: fileName,
                originalName: file.name,
                uploadDate: new Date(),
                uploadedBy: userName,
                size,
                ...versionMeta
              }
            ]
            const doc = {
              name: fileName,
              originalName: file.name,
              meta,
              contextType,
              contextId,
              tenantId
            }

            axios
              .post(`${muniApiBaseURL}/docs`, doc,{headers:{"Authorization":getToken()}})
              .then(res => {
                console.log("inserted ok in docs db : ", res)
                // this.setState({ message: "", error: "", inProgress: false })
                const payload = {
                  entityName,
                  entityId: _id || "",
                  fileName: res && res.data && res.data.originalName || "",
                  fileAWSId: res && res.data && res.data._id || "",
                  recordKeepingType: "G-10 disclosure",
                  recipientEmail: userPrimaryEmail || "",
                  deliveryAttemptDate: new Date(),
                  deliveryStatus: "Failure",
                }

                if(name && entityName && userPrimaryEmail){
                  const regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
                  if(regexEmail.test(userPrimaryEmail)){
                    payload.deliveryStatus = "Success"
                  }
                }

                putG10RecordKeeping( "?type=recordKeeping", payload, (response)=> {

                  if(response.data){

                    toast(`${entityName} auto generated successfully`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
                    const emailPayload = {
                      type: "cmp-sup-complaints",
                      sendEmailUserChoice: true,
                      emailParams:{
                        url: "compliance/cmp-sup-complaints/recordkeeping",
                        subject: "",
                        category: "custom",
                        message: "",
                        sendDocEmailLinks: true,
                        docIds: [res && res.data && res.data._id || ""],
                        sendEmailTo: [{sendEmailTo: userPrimaryEmail}]
                      }
                    }
                    if(payload.deliveryStatus === "Success"){
                      sendEmailTo(emailPayload)
                    } else {
                      toast("Something went wrong! Email not send", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
                    }
                    getPDFResponse.getPDFResponse(res)
                  }
                })
              })
              .catch(err => {
                console.log("err in inserting in docs db : ", err)
              })
          }
        } else {
          console.log("No version error")
        }
      }
    })

}

export const uploadWithSignedUrl = (bucketName, signedS3Url, file, fileName, tags, userDetails, getPDFResponse, entityDetails, docId) => {
  console.log(
    "in uploadWithSignedUrl : ",
    signedS3Url,
    bucketName,
    fileName,
    file.type
  )

  const xhr = new XMLHttpRequest()
  xhr.open("PUT", signedS3Url, true)
  xhr.setRequestHeader("Content-Type", file.type)
  if (tags) {
    console.log("no tagging")
    // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
  }
  xhr.onload = () => {
    console.log("readyState : ", xhr.readyState)
    if (xhr.status === 200) {
      console.log("file uploaded ok")
      console.log(`Last upload successful for "${fileName}" at ${new Date()}`)
      updateDocsDB(bucketName, file, fileName, userDetails, getPDFResponse, entityDetails, docId)
    }
  }
  xhr.onerror = err => {
    console.log("error in file uploaded", xhr.status, xhr.responseText, err)
  }
  xhr.send(file)
}

export const g10PDFPreview = async (entity, adminList, logo, position, disclosureMessage, type, userDetails, getPDFResponse, docId) => {

  let fileNames =  ""
  if(entity && entity.entityName){
    fileNames = entity.entityName.split(/\s+/).slice(0,4).join(" ")
    fileNames = fileNames.replace(/[^a-zA-Z ]/g, "")
    fileNames = fileNames.replace(/\s+/g, "")
  }

  const state = muniVisorApplicationStore.getState() || {}

  const address = entity && !(entity.entityPrimaryAddressGoogle) || entity && entity.entityPrimaryAddressGoogle === "-"
  const phoneNumber = entity && !(entity.userPrimaryPhoneNumber) || entity && entity.userPrimaryPhoneNumber === "-"
  const fax = entity && !(entity.fax) || entity && entity.fax === "-"

  console.log({address, phoneNumber, fax})

  logo = logo || (state && state.logo && state.logo.firmLogo) || ""

  const header = []
  const body = []
  adminList.forEach(admin => {
    header.push(admin.field || "-")
    body.push(admin.information || "-")
  })
  const contentData = [

    {
      columns: [
        {},
        {},
        {},
        {},
        {
          stack: [
            { text: `Date : ${moment(new Date()).format("MM-DD-YYYY") }` },
          ],
        },
      ]
    },

    {
      columns: [
        {
          stack: [
            { text: `${entity && entity.userFullName || ""}` ,  margin: [ 0, 18, 0, 0 ], fontSize: 13},
            { text: `${entity && entity.entityName || ""}`,  fontSize: 13},
            { text: `${entity && entity.entityPrimaryAddressGoogle || ""}`, fontSize: 13},
          ],
        },
      ]
    },
    {
      columns: [
        {style: "bigger",
          stack: [
            { text: "Subject: MSRB Rule G-10 Customer Complaint Required Disclosure", margin:[ 0, 10, 0, 0 ]},
          ],
        },
      ]
    },
    {
      columns: [
        {style: "bigger",
          stack: [
            { text: `Dear ${entity && entity.userFirstName || ""},` , margin:[ 0, 10, 0, 0 ]},
          ],
        },
      ]
    },
    {
      columns: [
        {style: "bigger",
          stack: [
            { text: "This disclosure pertains to the firm (“we”, “our”, “us”) having the below registration details.", margin:[ 0, 10, 0, 10 ]},
          ],
        },
      ]
    },

    {
      style: "tableExample",
      table: {
        widths: ["50%","50%"],
        body: [...header.map((s, i) => [`${s}:`, body[i]])]
      },
    },

    {
      marginTop:15,
      text: [ "" ]
    },

    htmlToPdfMake(disclosureMessage),

    {
      style: "bigger",
      columns: [
        { text: `${entity && entity.userFullName || ""}` ,  marginTop:20, fontSize: 13},
      ]
    },
    {
      style: "bigger",
      columns: [
        {text: `${entity && entity.entityPrimaryAddressGoogle ? `${address ? "" : entity.entityPrimaryAddressGoogle} ` : ""} ${phoneNumber ? "" : "•"} ${ phoneNumber ? "" : entity.userPrimaryPhoneNumber} ${ fax ? "" : "•" } ${fax ? "" : entity.fax }`,bold:true,  margin:[100,120,0,0]},
      ]
    }
  ]

  const location = (position && position.location && position.location.toLowerCase()) ||
    (state && state.auth && state.auth.userEntities &&
      state.auth.userEntities.settings && state.auth.userEntities.settings.location &&
      state.auth.userEntities.settings.location.toLowerCase())
    || "header"

  const align = (position && position.alignment && position.alignment.toLowerCase()) ||
    (state && state.auth &&
      state.auth.userEntities && state.auth.userEntities.settings &&
      state.auth.userEntities.settings.alignment && state.auth.userEntities.settings.alignment.toLowerCase())
    || "center"

  const entityName = entity && entity.entityName || ""

  const file = await generateG10PDF({contentData, location, align, logo, entityName}, type)

  if(type === "generate"){
    file.name = `${fileNames}.pdf`
    const tags = {
      status : "fail"
    }
    let fileName = file.name
    const bucketName = "munivisor-docs-dev"
    const extnIdx = fileName.lastIndexOf(".")
    if (extnIdx > -1) {
      fileName = `${fileName.substr(
        0,
        extnIdx
      )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
    }
    const contentType = file.type
    if (!fileName) {
      return console.log("No file name provided")
    }

    const { tenantId, contextType, contextId, userName } = userDetails && userDetails.userDetails
    const { entityName, userPrimaryEmail, _id, userFullName } = entity
    const versionMeta = {}
    versionMeta.uploadedBy = userName

    const filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`

    const opType = "upload"
    const options = { contentType, tags }

    const res = await getSignedUrl({  // eslint-disable-line
      opType,
      bucketName,
      fileName: filePath,
      options
    })
    if (res) {
      console.log("res : ", res)
      uploadWithSignedUrl(bucketName, res.data.url, file, fileName, tags, {tenantId, contextType, contextId, userName, versionMeta}, getPDFResponse, {entityName, userPrimaryEmail, _id, userFullName}, docId)
    }else {
      console.log("err : ", res)
    }
  }

}
