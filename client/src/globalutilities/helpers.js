import React from "react"
import { Link } from "react-router-dom"
import axios from "axios"
import moment from "moment"
import FileSaver from "file-saver"
import { toast } from "react-toastify"
import { muniVisorApplicationStore } from "AppState/store"
import {
  AUDIT_UPDATE,
  GET_ELIGIBLE_TRANSACTIONS,
  PDF_LOGO,
  SET_NAV_PERMISSION, SET_PLATFORMADMIN
} from "../app/StateManagement/actions/types"
import CONST, { muniApiBaseURL, muniAuthBaseURL, token } from "./consts"
import { checkSupervisorControls } from "../app/StateManagement/actions/CreateTransaction"
import { getLogoBase64 } from "GlobalUtils/pdfutils"
const ObjectID = require("bson-objectid")
const publicIp = require("public-ip")

export const getToken = () => localStorage.getItem("token")

export const getIpAddress = async () => {
  const res = await axios.get(`${muniApiBaseURL}rfp/ipAddress`, {
    headers: { Authorization: getToken() }
  })
  if (res && res.data && res.data.connect) {
    return res.data.connect
  }
  console.log("err in getting ip address")
}

export const getTranEntityUrl = (entityType, entityName, entityId, search) => {
  let url = ""
  switch (entityType) {
  case "Issuer":
    url = `/clients-propects/${entityId || ""}/entity`
    break
  case "Municipal Advisor":
    url = `/admin-firms/${entityId || ""}/firms`
    break
  default:
    url = `/thirdparties/${entityId || ""}/entity`
    break
  }
  if(!entityName){
    return <small/>
  }
  return (
    <Link to={url}>
      <small
        dangerouslySetInnerHTML={{
          __html: highlightSearch(entityName || "-", search)
        }}
      />
    </Link>
  )
}

export const getTranUserUrl = (userType, userName, userId, search, userStatus) => {
  let url = ""
  switch (userType) {
  case "Issuer":
    url = `/contacts/${userId || ""}/cltprops`
    break
  case "Municipal Advisor":
    url = `/admin-users/${userId || ""}/users`
    break
  default:
    url = `/contacts/${userId || ""}/thirdparties`
    break
  }

  if(!userName){
    return <small/>
  }
  return (
    <Link to={url}>
      <small style={{color: userStatus && userStatus === "inactive" ? "red" : ""}} title={userStatus && userStatus === "inactive" ? "Inactive User" : "Active User"}
        dangerouslySetInnerHTML={{
          __html: highlightSearch(userName || "-", search)
        }}
      />
    </Link>
  )
}

export const getDocumentList = (documentsList, loginUser) => {
  console.log("===================25===============>", documentsList, loginUser)
  const userUpdateDoc = []
  documentsList.forEach(d => {
    if (d.createdBy === loginUser.userId) {
      userUpdateDoc.push(d)
    }
  })
  if (userUpdateDoc.length > 0) {
    userUpdateDoc.forEach(u => {
      const index = documentsList.findIndex(d => d._id === u._id)
      documentsList.splice(index, 1)
    })
  }
  const document = documentsList.filter(doc => doc.docStatus === "Public")
  const docPublic = document.filter(
    doc => doc.settings.firms.length === 0 && doc.settings.users.length === 0
  )
  const ss = []
  document.forEach(s => {
    const settings = s.settings
    if (settings.selectLevelType === "user") {
      const users = settings.users
      const user = users.find(u => u.contactId === loginUser.userId)
      if (user) {
        ss.push(s)
      }
    } else if (settings.selectLevelType === "firms") {
      const firms = settings.firms
      const firm = firms.find(f => f.firmId === loginUser.entityId)
      if (firm) {
        ss.push(s)
      }
    }
  })
  const withoutPublicDoc = documentsList.filter(
    doc =>
      doc.docStatus !== "Public" &&
      doc.settings.firms.length === 0 &&
      doc.settings.users.length === 0
  )
  const filterDocumentList =
    ss.length === 0 ? withoutPublicDoc : ss.concat(withoutPublicDoc)
  const finalDoc =
    docPublic.length > 0
      ? docPublic.concat(filterDocumentList)
      : filterDocumentList
  const filterDoc = finalDoc.filter(t => t.docStatus === "Public")
  return userUpdateDoc.length === 0
    ? filterDoc
    : filterDoc.concat(userUpdateDoc)
}

export const getPublicIP = async () => {
  try {
    const ip4 = await publicIp.v4()
    if (ip4) {
      return ip4
    }
    const ip6 = await publicIp.v6()
    if (ip6) {
      return ip6
    }
    console.log("Did not get IP in  getPublicIP")
  } catch (err) {
    console.log("err in getPublicIP : ", err)
  }
}

export const setDateFormat = (value, format) => {
  Date.prototype.isValid = function() {
    return this.getTime() === this.getTime()
  }
  return value && new Date(value).isValid()
    ? moment(new Date(value)).format(format || "YYYY-MM-DD")
    : ""
}

export const setTableCache = (name, value) => {
  const tableCache =
    (localStorage.getItem("tableCache") &&
      JSON.parse(localStorage.getItem("tableCache"))) ||
    {}
  if (name && value && Object.keys(value).length) {
    if (tableCache.hasOwnProperty(name)) {
      tableCache[name] = {
        ...tableCache[name],
        ...value
      }
    } else {
      tableCache[name] = value
    }
    localStorage.setItem("tableCache", JSON.stringify(tableCache))
  }
}

export const getTableCache = name => {
  const tableCache =
    (localStorage.getItem("tableCache") &&
      JSON.parse(localStorage.getItem("tableCache"))) ||
    {}
  return (
    (tableCache && Object.keys(tableCache).length && tableCache[name]) || {}
  )
}

export const mapAccordionsArrayToObject = (data, otherDataRequired) => {
  const result = {}
  const otherData = {}
  data.forEach(e => {
    const key = e.title
    if (!result.hasOwnProperty(key)) {
      result[key] = e.items
      if (otherDataRequired) {
        otherData[key] = {}
        const otherKeys = Object.keys(e).filter(
          k => k !== "title" || k !== "items"
        )
        otherKeys.forEach(k => {
          otherData[key][k] = e[k]
        })
      }
    }
  })
  if (otherDataRequired) {
    return [result, otherData]
  }
  return result
}

export const fetchEligibleTransactionAction = async tranId => {
  try {
    const state = muniVisorApplicationStore.getState()
    let eligibleTrans = []
    let performIdCheckForExistance = true
    let entitlements = { edit: false, view: false }
    if (
      state.transaction &&
      state.transaction.eligibleTrans &&
      state.transaction.eligibleTrans.length
    ) {
      eligibleTrans = state.transaction.eligibleTrans || []
    } else {
      /* const res = await axios.get(`${muniApiBaseURL}rfp/eligibleAction`, {headers:{"Authorization":getToken()}})
      if (res && res.data) {
        eligibleTrans = res.data */
      const res = await axios.get(`${muniApiBaseURL}entitlements/all`, {
        headers: { Authorization: getToken() }
      })
      const { data, checkIdForExistance, ent } = res.data
      performIdCheckForExistance = checkIdForExistance
      entitlements = ent
      if (data && checkIdForExistance) {
        eligibleTrans =
          (res.data.data.consolidated &&
            res.data.data.consolidated.tranEntitlements) ||
          {}
        muniVisorApplicationStore.dispatch({
          type: GET_ELIGIBLE_TRANSACTIONS,
          payload: res.data
        })
      } else {
        console.log("err in getting picklists")
      }
    }
    if (tranId) {
      let tranCan = {}
      if (!performIdCheckForExistance) {
        tranCan = {
          canEditTran: entitlements.edit,
          canViewTran: entitlements.view,
          tranId
        }
        return tranCan
      }
      Object.keys(eligibleTrans).forEach(key => {
        if (key === tranId) {
          tranCan = {
            canEditTran: eligibleTrans[key].canEdit,
            canViewTran: eligibleTrans[key].canView,
            tranId: key
          }
        }
      })
      return tranCan
    }
    return {}
  } catch (error) {
    console.log(error)
  }
}

export const checkNavAccessEntitlrment = async (ids, transaction) => {
  try {
    const navIds = typeof ids === "string" ? [ids] : ids
    const state = muniVisorApplicationStore.getState()
    let res = {}
    if (
      state &&
      state.nav &&
      state.navPermission &&
      Object.keys(state.navPermission).length &&
      state.navPermission[ids]
    ) {
      res = state.navPermission
    } else {
      res = await axios.post(
        `${muniApiBaseURL}entitlements/getentitlementforids`,
        { ids: navIds },
        { headers: { Authorization: getToken() } }
      )
      res = res.data
      muniVisorApplicationStore.dispatch({
        type: SET_NAV_PERMISSION,
        payload: res
      })
    }
    const eligibleTrans = res || {}

    /*
    const res = await axios.post(
      `${muniApiBaseURL}entitlements/getentitlementforids`,
      { ids: navIds },
      { headers: { Authorization: getToken() } }
    )

    const eligibleTrans = res.data || {}
   */

    if (navIds) {
      if (typeof ids === "string") {
        let tranCan = {}
        Object.keys(eligibleTrans).forEach(key => {
          if (key === ids) {
            if (transaction) {
              tranCan = {
                canEditTran: eligibleTrans[key].edit,
                canViewTran: eligibleTrans[key].view,
                tranId: key
              }
            } else {
              tranCan = eligibleTrans[key]
            }
          }
        })
        return tranCan
      }
      return eligibleTrans
    }
    return {}
  } catch (error) {
    console.log(error)
    return {}
  }
}

export const makeEligibleTabView = async (
  user,
  transId,
  TABS,
  allEligibleNav,
  tranClientId,
  tranIssuerId
) => {
  const logInFirmId = (user && user.entityId) || ""
  const logInUserId = (user && user.userId) || ""

  const tranAction = await checkNavAccessEntitlrment(transId, true)
  const eligibleTab = {
    name: "",
    view: [],
    edit: []
  }

  let allowTabs = TABS
  const state = muniVisorApplicationStore.getState()
  const isSuperVisor = await checkSupervisorControls()
  const submitAudit =
    state.auth &&
    state.auth.userEntities &&
    state.auth.userEntities.settings &&
    state.auth.userEntities.settings.auditFlag === "no"
      ? false
      : state.auth &&
        state.auth.userEntities &&
        state.auth.userEntities.settings &&
        state.auth.userEntities.settings.auditFlag === "currentState"
      ? true
      : (state.auth &&
          state.auth.userEntities &&
          state.auth.userEntities.settings &&
          state.auth.userEntities.settings.auditFlag === "supervisor" &&
          (isSuperVisor && isSuperVisor.supervisor)) ||
        false
  if (!submitAudit) {
    allowTabs = TABS.filter(col => col.path !== "audit-trail")
  }

  if (allEligibleNav && allEligibleNav.length) {
    eligibleTab.view = allEligibleNav
  }
  const tabs = []
  if (eligibleTab) {
    allowTabs.forEach(tab => {
      if (eligibleTab.view.indexOf(tab.path) !== -1) {
        tabs.push(tab)
      }
    })
  }

  tranAction.canClient = tranClientId === logInFirmId
  tranAction.canEditDocument = tranAction.canEditTran

  if (tranAction.canEditTran) {
    tranAction.canEditTran =
      tranAction.canEditTran && tranIssuerId === logInFirmId
  }

  return {
    tranAction: {
      ...tranAction,
      ...eligibleTab
    },
    tabs
  }
}

export const makeEligibleTabViewForRFP = async (
  user,
  transaction,
  nav3,
  allEligibleNav,
  tranFirmId
) => {
  const logInFirmId = (user && user.entityId) || ""
  const tranAction = await checkNavAccessEntitlrment(transaction._id, transaction) //eslint-disable-line

  const canViewEdit = {
    name: "",
    view: [],
    edit: [],
    canManageEdit: false
  }

  let allowTabs = allEligibleNav
  const state = muniVisorApplicationStore.getState()
  const isSuperVisor = await checkSupervisorControls()
  const submitAudit =
    state.auth &&
    state.auth.userEntities &&
    state.auth.userEntities.settings &&
    state.auth.userEntities.settings.auditFlag === "no"
      ? false
      : state.auth &&
        state.auth.userEntities &&
        state.auth.userEntities.settings &&
        state.auth.userEntities.settings.auditFlag === "currentState"
      ? true
      : (state.auth &&
          state.auth.userEntities &&
          state.auth.userEntities.settings &&
          state.auth.userEntities.settings.auditFlag === "supervisor" &&
          (isSuperVisor && isSuperVisor.supervisor)) ||
        false
  if (!submitAudit) {
    allowTabs = allEligibleNav.filter(col => col !== "audit-trail")
  }

  if (allowTabs && allowTabs.length) {
    canViewEdit.edit = allowTabs
  }

  if(allowTabs && allowTabs.length) {
    canViewEdit.view = allowTabs
  }

  tranAction.canEditDocument =  tranAction.canEditTran

  const entTypes = ["Client", "Prospect", "Third Party", "Self"]
  if (
    entTypes.indexOf(user.relationshipToTenant) !== -1 &&
    tranAction.canEditTran
  ) {
    canViewEdit.canManageEdit = true
  }

  if (tranAction.canEditTran) {
    tranAction.canEditTran =
      tranAction.canEditTran && transaction.rfpTranClientId === logInFirmId
  }

  return {
    ...canViewEdit,
    ...tranAction,
    canClientEdit: transaction.rfpTranIssuerId === logInFirmId
  }
}

export const mapAccordionsObjectToArray = (data, otherData = {}) => {
  // console.log("data : ", data)
  // console.log("otherData : ", otherData)
  const result = Object.keys(data).map(k => ({
    title: k,
    ...otherData[k],
    items: data[k]
  }))
  // console.log("result : ", result)
  return result
}

export const updateDocDetails = async (docId, payload)=> {
  let error
  let result
  if (!docId) {
    error = "No docId provided"
  }
  try {
    if(payload.meta && payload.meta.hasOwnProperty("docCategory")){
      payload.meta.category = payload.meta.docCategory || ""
      delete payload.meta.docCategory
    }
    if(payload.meta && payload.meta.hasOwnProperty("docSubCategory")){
      payload.meta.subCategory = payload.meta.docSubCategory || ""
      delete payload.meta.docSubCategory
    }
    const res = await axios.put(`${muniApiBaseURL}docs/${docId}`, payload, {
      headers: { Authorization: getToken() }
    })
    console.log("res : ", res)
    if (res && res.data) {
      result = res.data
    } else {
      error = "No document found with the docId"
    }
  } catch (err) {
    console.log("error in getting doc : ", err)
    error = "Error in getting document"
  }
  return [error, result]
}

export const updateS3DocTags = async (docId, tags, meta) => {
  if (!docId || !tags) {
    throw Error("No docId or tags provided")
  }
  try {
    const res1 = await axios.get(`${muniApiBaseURL}docs/${docId}`, {
      headers: { Authorization: getToken() }
    })
    console.log("res1 : ", res1)
    if (res1 && res1.data) {
      const doc = res1.data
      const { tenantId, contextId, contextType, name } = doc
      if(meta && Object.keys(meta).length){
        meta.versions = (doc && doc.meta && doc.meta.versions) || []
        await updateDocDetails(docId, { meta: {...document.meta, ...meta} })
      }
      let fileName = name
      if (tenantId && contextId && contextType) {
        fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
      }
      const bucketName = CONST.bucketName
      const res2 = await axios.post(
        `${muniApiBaseURL}s3/put-s3-object-tagging`,
        { bucketName, fileName, tags },
        { headers: { Authorization: getToken() } }
      )
      console.log("res2 : ", res2)
      if (res2 && res2.status >= 200 && res2.status < 300) {
        console.log("doc tags updated ok")
      } else {
        throw Error("Error in updating tags")
      }
    } else {
      throw Error("No document found with the docId")
    }
  } catch (err) {
    throw err
  }
}

export const getChecklist = async checklistId => {
  const token = localStorage.getItem("token")
  // console.log("id : ", checklistId)
  const res = await axios.get(`${muniApiBaseURL}configs`, {
    headers: { Authorization: getToken() },
    params: { require: "checklists" }
  })
  if (res && res.data) {
    // console.log("res : ", res)
    const checklist = res.data.filter(e => e.id === checklistId)[0] || {}
    // console.log("checklist : ", checklist)
    const { data, type, name } = checklist

    if (data) {
      const [itemData, otherData] = mapAccordionsArrayToObject(data, true)
      const itemHeaders = {}
      Object.keys(otherData).forEach(k => {
        itemHeaders[k] = otherData[k].headers
      })
      return [itemData, itemHeaders, type, name]
    }
    // console.log("checklists : ", checklists)
  }
  console.log("err in getting checklists")
  return {}
}

export const getAllTenantUserDetails = async () => {
  try {
    const res = await axios.get(
      `${muniApiBaseURL}common/getalltenantuserdetails`,
      { headers: { Authorization: getToken() } }
    )
    const participants = []
    if (res && res.data) {
      res.data.alltenantusers.forEach(part => {
        part.id = part.userId
        part.name = `${part.userFirstName} ${part.userLastName}`
        part.group = "Firm"
        participants.push(part)
      })
    } else {
      console.log("err in getting participants")
    }
    return participants
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
    return err
  }
}

export const getAllTenantUserEmails = async () => {
  try {
    const result = await getAllTenantUserDetails()
    return result.map(res => res.sendEmailTo)
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
    return err
  }
}

export const getAllTenantEntityNames = async () => {
  try {
    const res = await axios.get(
      `${muniApiBaseURL}common/getalltenantentitydetails`,
      { headers: { Authorization: getToken() } }
    )
    let tenantEntityNames = []
    if (res && res.data) {
      tenantEntityNames = res.data.entities.map(entity => entity.name)
    } else {
      console.log("err in getting Tenant Entity Names")
    }
    return tenantEntityNames
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
    return err
  }
}

/* export const getPicklistValues = async (picklistName) => {
  const token = localStorage.getItem("token")
  const res = await axios.get(`${muniApiBaseURL}configs`, {
    headers:{"Authorization":token},
    params: { require: "picklists" }
  })

  let picklists
  if(res && res.data) {
    picklists = res.data
    console.log("picklists : ", picklists)
  } else {
    console.log("err in getting picklists")
  }

  const populateResult = (picklistName) => {
    let levels = 1
    let primaryValues = []
    const secondaryValues = {}
    const tertiaryValues = {}
    const picklist = picklists.filter(p => (p.meta && p.meta.systemName === picklistName))[0] || {}
    primaryValues = picklist.items ? picklist.items.map(e => e.label) : []
    if(picklist.meta) {
      if(picklist.meta.subListLevel2) {
        levels = 2
      } else if(picklist.meta.subListLevel3) {
        levels = 3
      }
      if(picklist.meta.subListLevel2) {
        picklist.items.forEach(e => {
          secondaryValues[e.label] = e.items.map(f => f.label).filter(d => d !== "Add a label")
          if(picklist.meta.subListLevel3) {
            tertiaryValues[e.label] = {}
            e.items.forEach(f => {
              tertiaryValues[e.label][f.label] = f.items.map(g => g.label).filter(d => d !== "Add a label")
            })
          }
        })
      }
    }
    return [levels, primaryValues, secondaryValues, tertiaryValues]
  }
  // console.log("picklists : ", picklists)
  if(picklists && picklists.length) {
    if(Array.isArray(picklistName) && picklistName.length) {
      return picklistName.map(d => populateResult(d))
    }
    console.log("wrong type of or empty arugments or old format argument passed to getPicklistValues")
    return populateResult(picklistName)

  }
} */
export const getPicklistValues = async picklistName => {
  const token = localStorage.getItem("token")
  const res = await axios.get(`${muniApiBaseURL}configs`, {
    headers: { Authorization: token || getToken() },
    params: { require: "picklists", names: picklistName.toString() }
  })

  let picklists
  if (res && res.data) {
    picklists = res.data
    console.log("picklists : ", picklists)
  } else {
    console.log("err in getting picklists")
  }

  const populateResult = picklistName => {
    let levels = 1
    let primaryValues = []
    const secondaryValues = {}
    const tertiaryValues = {}
    const picklist =
      picklists.filter(p => p.meta && p.meta.systemName === picklistName)[0] ||
      {}
    let isNum = (picklist.items || []).map(e => Number(e.label)).includes(NaN)

    primaryValues = isNum ? (picklist.items || []).filter(e => e.visible).map(e => ({label: e.label, included: e.included})).sort() :
      (picklist.items || []).filter(e => e.visible).map(e => ({label: e.label, included: e.included})).sort((a,b) =>{return a - b;})

    if (picklist.meta) {
      if (picklist.meta.subListLevel2) {
        levels = 2
      } else if (picklist.meta.subListLevel3) {
        levels = 3
      }
      if (picklist.meta.subListLevel2) {
        picklist.items.forEach(e => {
          secondaryValues[e.label] = e.items.filter(e => e.visible)
            .map(f => ({label: f.label, included: f.included}))
            .filter(d => (d !== "Add a label" || d !== ""))
            .sort()
          if (picklist.meta.subListLevel3) {
            tertiaryValues[e.label] = {}
            e.items.forEach(f => {
              tertiaryValues[e.label][f.label] = f.items
                .map(g => g.label)
                .filter(d => d !== "Add a label")
                .sort()
            })
          }
        })
      }
    }
    return [levels, primaryValues, secondaryValues, tertiaryValues]
  }
  // console.log("picklists : ", picklists)
  if (picklists && picklists.length) {
    if (Array.isArray(picklistName) && picklistName.length) {
      return picklistName.map(d => populateResult(d))
    }
    console.log(
      "wrong type of or empty arugments or old format argument passed to getPicklistValues"
    )
    return populateResult(picklistName)
  }
  console.log("picklists===>>>", picklists)
  return picklists
}

export const getPicklistByPicklistName = async picklistName => {
  const token = localStorage.getItem("token")
  let picklists = []
  let levels = 1
  let primaryValues = []
  const secondaryValues = {}
  const tertiaryValues = {}
  const state = muniVisorApplicationStore.getState()
  const localPicklists =
    (localStorage.getItem("picklists") &&
      JSON.parse(localStorage.getItem("picklists"))) ||
    []

  /* if(state.picklists && state.picklists.length) {
    picklists = state.picklists || []
  }else if(localPicklists && localPicklists.length){
    picklists = localPicklists
  }else {
    const res = await axios.get(`${muniApiBaseURL}configs`, {
      headers: {"Authorization": token},
      params: { require: "picklists", names: picklistName.toString()}
    })
    if (res && res.data) {
      picklists = res.data
      localStorage.setItem("picklists", JSON.stringify(picklists))
      muniVisorApplicationStore.dispatch({type: GET_PICKLISTS, payload: res.data})
    } else {
      console.log("err in getting picklists")
    }
  } */

  const res = await axios.get(`${muniApiBaseURL}configs`, {
    headers: { Authorization: token || getToken() },
    params: { require: "picklists", names: picklistName.toString() }
  })
  if (res && res.data) {
    picklists = res.data
  } else {
    console.log("err in getting picklists")
  }

  if (picklists && picklists.length) {
    if (Array.isArray(picklistName) && picklistName.length) {
      const picklist = picklists.filter(
        p =>
          p.meta &&
          p.meta.systemName &&
          picklistName.indexOf(p.meta.systemName) !== -1
      )
      primaryValues = {}
      picklist.map(p => {
        let isNum = (p.items || []).map(e => Number(e.label)).includes(NaN)
        primaryValues = {
          ...primaryValues,
          [p.meta.systemName]: isNum ? (p.items || []).filter(e => e.visible).map(e => ({label: e.label, included: e.included})).sort() :
            (p.items || []).filter(e => e.visible).map(e => ({label: e.label, included: e.included})).sort((a,b) =>{return a - b;})
        }
        if (p.meta) {
          if (p.meta.subListLevel2) {
            levels = 2
          } else if (p.meta.subListLevel3) {
            levels = 3
          }

          if (p.meta.subListLevel2) {
            p.items.forEach(e => {
              secondaryValues[p.meta.systemName] = {
                ...secondaryValues[p.meta.systemName],
                [e.label]: e.items.filter(e => e.visible)
                  .map(f => ({label: f.label, included: f.included}))
                  .filter(d => (d !== "Add a label" || d !== ""))
                  .sort()
              }
              if (p.meta.subListLevel3) {
                let tertiary = {}
                Object.keys(secondaryValues[p.meta.systemName]).forEach(key => {
                  tertiary = {
                    ...tertiary,
                    [key]: {}
                  }
                  secondaryValues[p.meta.systemName][key].forEach(val => {
                    tertiary[key] = {
                      ...tertiary[key],
                      [val]: []
                    }
                  })
                })
                tertiaryValues[p.meta.systemName] = tertiary
                e.items.forEach(f => {
                  if (p.meta.systemName && e.label) {
                    tertiaryValues[p.meta.systemName][e.label][
                      f.label
                    ] = f.items
                      .map(g => g.label)
                      .filter(d => d !== "Add a label")
                      .sort()
                  }
                })
              }
            })
          }
        }
      })
    }
  }
  return [levels, primaryValues, secondaryValues, tertiaryValues]
}

export const getUserDetails = async userId => {
  const res = await axios.get(`${muniApiBaseURL}users/${userId}`)
  if (res && res.data) {
    return res.data
  }
  console.log("err in getting user details")
}

export const getAccessPolicy = async token => {
  console.log("token : ", token)
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}configs`, {
      headers: { Authorization: token || getToken() },
      params: { require: "entitlement" }
    })
  } catch (err) {
    console.log("err : ", err)
  }

  // console.log("res : ", res)
  let userPolicy
  if (res && res.data && res.data[0]) {
    userPolicy = res.data[0]
  } else {
    console.log("err in getting accessPolicy")
  }
  return userPolicy
}

export const getDeals = async () => {
  const res = await axios.get(`${muniApiBaseURL}deals`, {
    headers: { Authorization: token || getToken() }
  })
  if (res && res.data) {
    return res.data
  }
  console.log("err in getting all deals")
}

export const updateAuditLog = async (type, changeLog) => {
  const token = localStorage.getItem("token")
  const state = muniVisorApplicationStore.getState()
  let ip = state.ip || null
  const userId = state && state.auth && state.auth.userEntities && state.auth.userEntities.userId || null
  const isSuperVisor = await checkSupervisorControls()
  const submitAudit = type === "Data Migration" ||
    (state.auth &&
    state.auth.userEntities &&
    state.auth.userEntities.settings &&
    state.auth.userEntities.settings.auditFlag === "no"
      ? false
      : state.auth &&
        state.auth.userEntities &&
        state.auth.userEntities.settings &&
        state.auth.userEntities.settings.auditFlag === "currentState"
      ? true
      : (state.auth &&
          state.auth.userEntities &&
          state.auth.userEntities.settings &&
          state.auth.userEntities.settings.auditFlag === "supervisor" &&
          (isSuperVisor && isSuperVisor.supervisor)) ||
        false)
  if (submitAudit) {
    if (!ip) {
      ip = await getIpAddress()
    }

    changeLog.forEach((e, i) => {
      if (!e.hasOwnProperty(ip) || !e.ip || !!e.hasOwnProperty(userId) || !e.userId ) {
        changeLog[i] = { ...e, ip, userId }
      }
    })
    const auditData = {
      groupId: "",
      type,
      changeLog
    }
    const res = await axios.post(`${muniApiBaseURL}auditlog/`, auditData, {
      headers: { Authorization: token || getToken() }
    })
    console.log("res : ", res)
    if (res && res.status >= 200 && res.status < 300) {
      console.log("updated audit log ok")
    } else {
      console.log("err in updating audit log")
    }
  }
}

// export const mapNestedAccordionsArrayToObject = (data) => {
//   const result = {}
//   data.forEach(e => {
//     const key = e.title
//     if(!result.hasOwnProperty(key)) {
//       result[key] = e.items
//     }
//     if(e.items.items && e.items.items.length) {
//
//     }
//   })
//   return result
// }
//
// export const  mapNestedAccordionsObjectToArray = (data) => Object.keys(data).map(k => ({
//   title: k,
//   items: data[k]
// }))

// export const mapPrimaryNavArrayToObject = (data) => {
//   const result = {}
//   data.forEach(e => {
//     const key = e.path
//     if(e.access) {
//       if(e.subpath && e.subpath.length) {
//         result[key] = {}
//         e.subpath.forEach(s => {
//           if(s.access) {
//             result[key][s.path] = true
//           }
//         })
//       } else {
//         result[key] = true
//       }
//     }
//   })
//   return result
// }

export const mapPrimaryNavArrayToObject = data => {
  const result = {}
  data.forEach(e => {
    const key = e.path
    if (e.access) {
      if (e.subpath && e.subpath.length) {
        result[key] = mapPrimaryNavArrayToObject(e.subpath)
      } else {
        result[key] = true
      }
    }
  })
  return result
}

export const flattenNavObject = (nav, result = {}) => {
  Object.keys(nav).forEach(k => {
    if (Object.getPrototypeOf(nav[k]).constructor.name === "Object") {
      result[k] = true
      flattenNavObject(nav[k], result)
    } else {
      result[k] = nav[k]
    }
  })
}

export const getEmailIdsByParticipantType = async participantType => {
  let mappedResponse = null
  try {
    const response = await axios.get(
      `${muniAuthBaseURL}/signupusers?type=${participantType}`
    )
    mappedResponse = response.data.map(obj => ({
      label: obj.loginEmailId,
      value: obj
    }))
  } catch (error) {
    console.error(error)
  }
  return mappedResponse
}

export const checkExistanceOfEmail = async emailId => {
  let mappedResponse = null
  try {
    const response = await axios.get(
      `${muniAuthBaseURL}/checkemailexists?email=${emailId}`
    )
    mappedResponse = response.data
  } catch (error) {
    mappedResponse = { foundemail: false, error: "Error fetching email" }
  }
  return mappedResponse
}

export const customObj = (result, firmDetail) => {
  const items = {}
  Object.keys(firmDetail).map((item, idx) => {
    if (result.hasOwnProperty(item)) {
      items[item] = result[item]
      if (typeof item === Object) {
        Object.keys(item).map((item1, idx) => {
          if (result.hasOwnProperty(item1)) {
            items[item][item1] = result[item][item1]
          }
        })
      }
    }
  })
  return items
}

export const copyDeepObject = Obj => JSON.parse(JSON.stringify(Obj))

export const validateData = (userDetails, initialState) => {
  const objData = {}
  Object.keys(initialState).forEach(item => {
    objData[item] = userDetails[item]
  })
  return objData
}
export const checkEmptyElObject = obj => {
  let isEmpty = true
  const isEmptyFunc = obj => {
    Object.keys(obj).forEach(item => {
      if (_.isObject(obj[item])) {
        isEmptyFunc(obj[item])
      } else if (obj[item] !== "" && !_.isBoolean(obj[item])) isEmpty = false
    })
    return isEmpty
  }
  return isEmptyFunc(obj)
}
export const convertError = (errorFirmDetail, errorData) => {
  errorData.error.details.forEach(item => {
    switch (item.path.length) {
    case 1:
      errorFirmDetail[item.path[0]] = `${item.context.label} Required.`
      break
    case 2:
      errorFirmDetail[item.path[0]][item.path[1]] = `${
        item.context.label
      } Required.`
      break
    case 3:
      errorFirmDetail[item.path[0]][item.path[1]][item.path[2]] = `${
        item.context.label
      } Required.`
      break
    case 4:
      errorFirmDetail[item.path[0]][item.path[1]][item.path[2]][
        item.path[3]
      ] = `${item.context.label} Required.`
      break
    case 5:
      errorFirmDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]][
        item.path[4]
      ] = `${item.context.label} Required.`
      break
    default:
      console.log("Test")
    }
  })
  return errorFirmDetail
}

const dateCompare = (startDate, endDate) => {
  if (startDate > endDate) return 1
  if (startDate < endDate) return -1
  return 0
}

export const numberWithCommas = x => {
  if(!x){
    return ""
  }
  if( x && x === "-") {
    return x
  }
  return (x && x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) || ""
}

export const getNotificationDescription = (name, options, meta) => {
  switch (name) {
  case "NOTIFY_COMPLIANCE_CONTROL_ASSIGNED": {
    const { contextType, topic } = meta
    const description = `${
      contextType ? `${contextType} ` : ""
    }notification - ${topic ? `topic: ${topic} ` : ""}`
    return description
  }
  case "NOTIFY_TASK_ASSIGNED": {
    const { contextType, contextId, contextName } = meta
    const {
      taskStartDate,
      taskEndDate,
      taskDescription,
      taskPriority,
      taskStatus
    } = options
    const description = `A new taks ${taskDescription} is assigned for ${
      contextType ? `${contextType} ` : ""
    }${contextId ? `${contextId} ` : ""}${
      contextName ? `${contextName} ` : ""
    }}.
  Start date: ${taskStartDate}, End date: ${taskEndDate}, Priority: ${taskPriority}, Status: ${taskStatus}`
    return description
  }
  default:
    return "No description"
  }
}

export const createMessage = data => {
  console.log("in createMessage : ", data)
  let {
    userId,
    from,
    sentDate,
    description,
    sourceType,
    sourceId,
    docIds
  } = data
  from = from || "system"
  const payload = {
    userId,
    from,
    sentDate,
    description,
    sourceType,
    sourceId,
    docIds
  }
  axios
    .post(`${muniApiBaseURL}messages/`, payload)
    .then(res => console.log("ok in createMessage: ", res.status))
    .catch(err => console.log("err : ", err))
}

export const regexTestsForEmail = (matchString) => {
  matchString = matchString.toString().toLowerCase().trim()
  matchString.replace(/(\r\n|\n|\r|\s|\t)/gm, "")
  console.log(`${matchString}----`)
  const regexString = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const regex = new RegExp(regexString)
  return regex.test(matchString)
}

export const regexTestsForPhoneFax = (matchString) => {
  matchString = matchString.toString()
  const regexString = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *\s*[x|\||\;|,|:|\s]\s*(\d+))?\s*$/
  const regex = new RegExp(regexString)
  const isThereAMatch = regex.test(matchString)
  if( isThereAMatch) {
    // console.log("======matchString========", matchString)
    const matches = matchString.match(regex)
    const countryCode = matches[1] ? `+${matches[1]}` : ""
    const phoneNumber = `(${matches[2]})-${matches[3]}-${matches[4]}`
    const extension = matches[5] || ""
    return { valid:true, data:{countryCode, phoneNumber, extension}}
  }
  return { valid:false,data:{}}
}

export const regexTextsForZipCode = (matchString) => {
  matchString = matchString.toString()
  const regexString = /^\s*(\d{4,5}|[A-Za-z]+|\d{5,10})(?: *[x|\||;|,|:|\-|\s](\d+|[A-Za-z]+))?\s*$/
  const regex = new RegExp(regexString)
  const isThereAMatch = regex.test(matchString)

  if(isThereAMatch) {
    // console.log("======matchString========", matchString)
    const matches = matchString.match(regex)
    const zipCode1 = matches[1] || ""
    const zipCode2 = matches[2] || ""
    return { valid:true, data:{zip1: zipCode1, zip2: zipCode2}}
  }
  return {valid:false, data:{}}
}


export const createNotification = (
  token,
  name,
  userIds,
  options,
  meta = {}
) => {
  if (!token || !name || !userIds || !userIds.length || !options) {
    console.log("Input missing")
    return
  }
  console.log(
    "token, name, userIds, options, meta : ",
    token,
    name,
    userIds,
    options,
    meta
  )
  const {
    modes,
    recurring,
    startDate,
    endDate,
    frequency,
    unit,
    limit,
    docIds
  } = options
  const lastSent = options.lastSent || new Date(startDate)
  console.log("lastSent client  : ", lastSent)
  const description = getNotificationDescription(name, options, meta)
  userIds.forEach(userId => {
    if (recurring) {
      const payload = {
        userId,
        name,
        modes,
        description,
        recurring,
        meta,
        startDate,
        endDate,
        frequency: { value: frequency, unit },
        limit,
        docIds,
        lastSent
      }
      console.log("payload : ", payload)
      axios
        .post(`${muniApiBaseURL}notifications/`, payload)
        .then(res => {
          console.log("res : ", res)
          const sourceId =
            res && res.data && res.data._id ? res.data._id : "CAC_daily_yearly"
          if (unit === "day" || unit === "year") {
            createMessage({
              userId,
              from: "system",
              sentDate: new Date(),
              description,
              sourceType: "system",
              sourceId,
              docIds
            })
          } else if (unit === "week") {
            const nextDate = moment(new Date(lastSent))
              .add(frequency, `${unit}s`)
              .toDate()
            console.log("nextDate : ", nextDate)
            if (nextDate > new Date()) {
              createMessage({
                userId,
                from: "system",
                sentDate: new Date(),
                description,
                sourceType: "system",
                sourceId,
                docIds
              })
            }
          }
        })
        .catch(err => console.log("err : ", err))
    } else {
      createMessage({
        userId,
        from: "system",
        sentDate: new Date(),
        description,
        sourceType: "system",
        sourceId: "CAC_one_off",
        docIds
      })
    }
  })
}

export const createDashboardTask = (token, data) => {
  const {
    userId,
    startDate,
    endDate,
    description,
    taskType,
    taskNotes,
    taskAssigneeName,
    priority
  } = data
  console.log("data, taskType : ", data, taskType)
  const taskDetails = {
    taskAssigneeUserId: userId,
    taskAssigneeName,
    taskNotes,
    taskDescription: description,
    taskStartDate: startDate,
    taskEndDate: endDate,
    taskPriority: priority,
    taskType,
    taskStatus: "Open"
  }
  console.log("taskDetails : ", taskDetails)
  axios
    .post(
      `${muniApiBaseURL}tasks/save-simple-task`,
      { taskDetails },
      {
        headers: {
          Authorization: token
        }
      }
    )
    .then(res => console.log("task created ok : ", res.status))
    .catch(err => console.log("error in creating task : ", err))

  const emailPayload = {
    to: "munivisorproddev@otaras.com",
    cc: "munivisorproddev@otaras.com",
    replyto: "munivisorproddev@otaras.com",
    bodytemplate: `${description}. Click ${
      window.location.origin
    }/cac/view-control/view-detail?id=${taskNotes} to access it.`,
    subject: "Compliance Control Action Assigned"
  }
  axios
    .post(`${muniApiBaseURL}emails/sendmail`, emailPayload, {
      headers: {
        Authorization: token
      }
    })
    .then(res => console.log("email sent ok : ", res.status))
    .catch(err => console.log("error in sending email : ", err))
}

export const getPeriodsBetweenDate = (startDate, endDate, unit) => {
  const result = []
  if (!startDate || !endDate) {
    console.log("missing input in getPeriodsBetweenDate")
    return result
  }

  startDate = new Date(startDate)
  endDate = new Date(endDate)

  console.log("getPeriodsBetweenDate : ", startDate, endDate)
  if (endDate < startDate) {
    console.log(
      "endDate found to be less than the startDate in getQuartersBetweenDates"
    )
    return result
  }
}

export const getQuartersBetweenDates = (startDate, endDate) => {
  const result = []
  if (!startDate || !endDate) {
    console.log("missing input in getQuartersBetweenDates")
    return result
  }

  startDate = new Date(startDate)
  endDate = new Date(endDate)

  console.log("getQuartersBetweenDates : ", startDate, endDate)

  if (endDate < startDate) {
    console.log(
      "endDate found to be less than the startDate in getQuartersBetweenDates"
    )
    return result
  }
  try {
    const currentQuarter = moment(startDate).quarter()
    const currentYear = moment(startDate).year()
    result.push([currentQuarter, currentYear])
    const currentQuarterEndDate = moment(startDate)
      .endOf("quarter")
      .startOf("day")
      .toDate()
    const lastQuarterEndDate = moment(endDate)
      .endOf("quarter")
      .startOf("day")
      .toDate()
    if (endDate > currentQuarterEndDate) {
      let more = true
      let i = 1
      while (more) {
        const nextQuarterEndDate = moment(startDate)
          .add(i, "quarter")
          .endOf("quarter")
          .startOf("day")
          .toDate()
        if (nextQuarterEndDate <= lastQuarterEndDate) {
          i += 1
          const quarter = moment(nextQuarterEndDate).quarter()
          const year = moment(nextQuarterEndDate).year()
          result.push([quarter, year])
        } else {
          more = false
        }
      }
    }
  } catch (err) {
    console.log("error in getQuartersBetweenDates : ", err)
  }
  console.log("result in getQuartersBetweenDates : ", result)
  return result
}

export const createControlActions = (token, data = {}, userNames = []) => {
  console.log("data : ", data)
  const { id, name, topic, docIds, dueDate, checklist, userIds } = data
  if (!id) {
    console.log("controlId missing")
    return
  }
  let docsActions = []
  if (docIds && docIds.length) {
    docsActions = docIds.map(e => ({
      docId: e._id,
      action: "",
      name: e.name,
      date: new Date(),
      by: "system"
    }))
  }
  userIds.forEach((userId, i) => {
    const payload = {
      userId,
      ...data,
      docIds: docsActions,
      receivedDate: new Date(),
      status: "due"
    }
    axios
      .post(`${muniApiBaseURL}controls-actions/`, payload)
      .then(res => {
        console.log("control action created ok : ", res.status)
        createDashboardTask(token, {
          userId,
          taskAssigneeName: userNames[i] || "",
          taskNotes: res.data._id,
          startDate: new Date(),
          endDate: dueDate || null,
          description: `Compliance task with topic -${topic} : ${name}`,
          taskType: "Compliance"
        })
      })
      .catch(err => console.log("err in creating control action: ", err))
  })
}

export const createControlActionsTask = async (token, data = {}) => {
  console.log("data : ", data)
  const { id } = data
  if (!id) {
    console.log("controlId missing")
    return
  }

  try {
    const res = await axios.post(`${muniApiBaseURL}controls-actions/controlActions-task`, {data}, {headers:{"Authorization":getToken()}})
    if (res && res.data) {
      return res.data
    }
  } catch (error) {
    return error
  }
}

export const createCACTaskAction = (token, control) => {
  const {
    refId,
    id,
    name,
    topic,
    recipients,
    dueDate,
    docIds,
    checklist,
    relatedEntity,
    relatedActivity,
    relatedContact,
    subTopic,
    refRule,
    otherTopic,
    notes,
    meta,
    users
  } = control
  const userIds = users || recipients.map(e => e.id)
  const names = {}
  recipients.forEach(e => {
    names[e.id] = e.name
  })
  const userNames = userIds.map(e => names[e])
  const data = {
    userIds,
    id,
    parentId: refId,
    name,
    topic:
      meta && meta.year && meta.quarter
        ? `${topic} - ${meta.year} Q${meta.quarter}`
        : topic,
    dueDate,
    docIds,
    checklist,
    relatedEntity,
    relatedActivity,
    relatedContact,
    subTopic,
    otherTopic,
    refRule,
    meta,
    notes
  }
  return createControlActions(token, data, userNames)
}

export const createCACTask = async (
  token,
  entityId,
  recipient,
  meta,
  createdBy,
  actionTaskDescription,
  tranUrl
) => {
  token = token || getToken()
  console.log("in createCACTask")
  console.log("token", token)
  const saveType = "final"
  const newListId = `CTRL-${Math.random().toString(36).toUpperCase().substring(2, 17)}`
  const controls = {
    _id: ObjectID(),
    refId: ObjectID(),
    id: newListId,
    name: "No active or eligible contracts",
    type: "task",
    target: "self",
    // topic: topic || "Supervisory & Compliance Obligations",
    topic: "Supervisory & Compliance Obligations",
    subTopic: "Recordkeeping Requirements",
    otherTopic: "",
    refRule: "",
    notificationSentDate: new Date(),
    actionCompletedBy: 0,
    numActions: 1,
    dueDate: new Date(),
    notification: {
      recurringType: "once",
      recurringPattern1: 0,
      recurringPattern2: 0,
      recurring: false,
      frequency: 0,
      unit: "",
      startDate: null,
      endDate: null
    },
    recipients: recipient && [recipient[0]],
    saveType,
    tranUrl,
    toList: [],
    ccList: [],
    relatedActivity: [],
    relatedEntity: [],
    relatedContact: [],
    docIds: [],
    checklist: [],
    checklistApplicable: false,
    isNoActiveContract: true,
    // meta,
    state: "enabled",
    status: "open",
    createdBy,
    lastUpdated: {
      date: new Date(),
      by: createdBy
    }
  }
  await axios.post(`${muniApiBaseURL}controls/saveControl?id=${entityId}&type=${saveType}&flag=isNoActiveContract`, {controls,actionTaskDescription}, {headers:{"Authorization":token}})
  /* axios
    .get(`${muniApiBaseURL}controls`, {
      headers: { Authorization: token || getToken() }
    })
    .then(res => {
      if (
        res &&
        res.status >= 200 &&
        res.status < 300 &&
        res.data &&
        res.data._id
      ) {
        const controlsObj = { ...res.data }
        controlsObj.controls = [...res.data.controls]
        const ids = controlsObj.controls.map(c => +c.id.split("CTRLTTACC")[1])
        ids.sort((a, b) => b - a)
        const controlId = `CTRLTTACC${(ids[0] || 0) + 1}`
        controlsObj.controls.push({ ...control, id: controlId })
        axios
          .put(`${muniApiBaseURL}controls/${res.data._id}`, controlsObj, {
            headers: { Authorization: token || getToken() }
          })
          .then(() => createCACTaskAction(token, { ...control, id: controlId }))
          .then(() =>
            createMessage({
              userId: recipient.id,
              from: "system",
              sentDate: new Date(),
              description: "CTRLTTACC",
              sourceType: "system",
              sourceId: "CAC_one_off",
              docIds: []
            })
          )
          .catch(err => {
            console.log("err in createCACTask updating control : ", err)
          })
      } else {
        const controlsObj = {
          entityId,
          controls: [{ ...control, id: "CTRLTTACC1" }]
        }
        axios
          .post(`${muniApiBaseURL}controls/`, controlsObj, {
            headers: { Authorization: token || getToken() }
          })
          .then(() =>
            createCACTaskAction(token, { ...control, id: "CTRLTTACC1" })
          )
          .then(() =>
            createMessage({
              userId: recipient.id,
              from: "system",
              sentDate: new Date(),
              description: "CTRLTTACC",
              sourceType: "system",
              sourceId: "CAC_one_off",
              docIds: []
            })
          )
          .catch(err => {
            console.log("err in createCACTask creating control : ", err)
          })
      }
    })
    .catch(err => {
      console.log("err in createCACTask getting controls : ", err)
    }) */
}

export const getPreApprovalListForCAC = async token => {
  const result = []
  try {
    const res1 = await axios.get(
      `${muniApiBaseURL}policontributions/preapproval?type=supervisor`,
      { headers: { Authorization: token || getToken() } }
    )
    console.log("res1 : ", res1)
    if (res1 && res1.data) {
      result.push(...res1.data)
    }
  } catch (err1) {
    console.log("err1 : ", err1)
  }

  try {
    const res2 = await axios.get(
      `${muniApiBaseURL}giftsgratuities/preapproval?type=supervisor`,
      { headers: { Authorization: token || getToken() } }
    )
    console.log("res2 : ", res2)
    if (res2 && res2.data) {
      result.push(...res2.data)
    }
  } catch (err2) {
    console.log("err2 : ", err2)
  }
  return result
}

export const updateControlTaskStatus = async (token, taskId) => {
  if (!token) {
    throw new Error("no token provided")
  }
  if (!taskId) {
    throw new Error("no id provided")
  }

  const res1 = await axios.post(
    `${muniApiBaseURL}controls-actions/action-status`, { taskId },
    { headers: { Authorization: token || getToken() } }
  )
  console.log("res1 : ", res1)
}

export const updateCACPoliticalActionStatus = async (token, data, status) => {
  console.log("data : ", data)
  if (!token || !data) {
    throw new Error("no token or data provided")
  }
  const { year, quarter, type } = data
  if (!year || !quarter) {
    throw new Error("no year or quarter provided")
  }
  let sMonth = 0
  let eMonth = 0
  let filter = {}
  if (quarter === 1) {
    sMonth = 1
    eMonth = 3
  } else if (quarter === 2) {
    sMonth = 4
    eMonth = 6
  } else if (quarter === 3) {
    sMonth = 7
    eMonth = 9
  } else if (quarter === 4) {
    sMonth = 10
    eMonth = 12
  }

  if (type === "political"){
    filter = {
      type: "Political Contributions and Prohibitions on Municipal Advisory Business",
      sMonth,
      eMonth
    }
  }
  try {
    const res1 = await axios.post(
      `${muniApiBaseURL}controls-actions/action-status-political`,
      { filter, status },
      { headers: { Authorization: token || getToken() } }
    )
    console.log("res1 : ", res1)
  } catch (error) {
    return error
  }
}

export const checkExistingQuarterlyAffirmationsAction = async (
  refId
) => {
  const res = await axios.get(`${muniApiBaseURL}controls-actions`, {
    headers: { Authorization: getToken() }
  })
  if (res && res.status === 200 && res.data) {
    const idx = res.data.findIndex(e => e.parentId === refId)
    console.log("idx, refId, res : ", idx, refId, res)
    if (idx > -1) {
      return true
    }
  }
  return false
}

export const createQuarterlyAffirmationActions = async (token, control) => {
  const quarterlyAffirmations = [
    "Political Contributions and Prohibitions on Municipal Advisory Business",
    "Gifts and Gratuities"
  ]
  let error
  const result = []
  if (!token || !control || !control.notification) {
    error = "No token or control information provided"
    console.log("error in createQuarterlyAffirmationActions : ", error)
    return [error]
  }
  if (
    !quarterlyAffirmations.includes(control.topic) ||
    control.type !== "affirm"
  ) {
    error = "Wrong control type"
    console.log("error in createQuarterlyAffirmationActions : ", error)
    return [error]
  }
  const querters = getQuartersBetweenDates(
    control.notification.startDate,
    control.notification.endDate
  )

  const startYear = moment(new Date(control.notification.startDate)).year()
  const endYear = moment(control.notification.endDate).year()
  const yearList = []
  for (let i = startYear; i <= endYear; i++) {
    yearList.push(i)
  }

  if (!querters || !querters.length) {
    error = "Wrong control dates"
    console.log("error in createQuarterlyAffirmationActions : ", error)
    return [error]
  }
  console.log("querters : ", querters)
  const isPoliticalContributionTopic =
    control.topic ===
    "Political Contributions and Prohibitions on Municipal Advisory Business"
  const isGiftsTopic = control.topic === "Gifts and Gratuities"
  const userIds = control.recipients.map(e => e.id)
  const len1 = userIds.length
  const len2 = isPoliticalContributionTopic
    ? querters.length
    : isGiftsTopic
    ? yearList.length
    : 0

  for (let i = 0; i < len1; i++) {
    for (let j = 0; j < len2; j++) {
      console.log("i , j : ", i, j)
      if (isPoliticalContributionTopic || isGiftsTopic) {
        const payload = {
          quarter: isGiftsTopic ? "All" : querters[j][0],
          year: isGiftsTopic ? yearList[j] : querters[j][1],
          disclosureFor: userIds[i]
        }
        const apiUrl = isPoliticalContributionTopic
          ? `${muniApiBaseURL}policontributions/statedetails`
          : `${muniApiBaseURL}giftsgratuities/statedetails`
        try {
          const res = await axios.put(apiUrl, payload, {
            headers: { Authorization: token || getToken() }
          })
          // console.log("res : ",res)
          const { contributeId, _id } = isPoliticalContributionTopic
            ? res.data.politicalContDetails
            : res.data.details
          await createCACTaskAction(token, {
            ...control,
            dueDate: moment()
              .year(querters[j][1])
              .quarter(querters[j][0])
              .endOf("quarter")
              .toDate(),
            users: [userIds[i]],
            meta: {
              contributeId,
              userContributionId: _id || null,
              quarter: isGiftsTopic ? "All" : querters[j][0],
              year: isGiftsTopic ? yearList[j] : querters[j][1],
              type: isPoliticalContributionTopic ? "political" : "gifts"
            }
          })
          result.push(res.data)
        } catch (err) {
          console.log(
            "error in createQuarterlyAffirmationActions : ",
            `Error in creating policontributions/action for user ${
              userIds[i]
            }, Q: ${querters[j][0]}, Y: ${querters[j][1]}`
          )
        }
      } else {
        try {
          await createCACTaskAction(token, {
            ...control,
            dueDate: moment()
              .year(querters[j][1])
              .quarter(querters[j][0])
              .endOf("quarter")
              .toDate(),
            users: [userIds[i]],
            meta: {
              quarter: isGiftsTopic ? "All" : querters[j][0],
              year: isGiftsTopic ? yearList[j] : querters[j][1]
            }
          })
        } catch (err) {
          console.log(
            "error in createQuarterlyAffirmationActions : ",
            `Error in creating action for user ${userIds[i]}, Q: ${
              querters[j][0]
            }, Y: ${querters[j][1]}`
          )
        }
      }
    }
  }
  return [error, result]
}

export const inheritPlatformConfig = async (entityId, token) => {
  token = token || localStorage.getItem("token")
  if (!token || !entityId) {
    console.log("No token or entityId found")
    return
  }
  const bodyObj = { entityId }
  const res = await axios.post(`${muniApiBaseURL}configs/inherit`, bodyObj, {
    headers: { Authorization: token || getToken() }
  })
  // console.log("res : ", res)
  if (res && res.status >= 200 && res.status < 300) {
    console.log("config inherited log ok")
  } else {
    console.log("err in inheriting config")
  }
}

export const getNavIds = async token => {
  console.log("token : ", token)
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}entitlements/nav`, {
      headers: { Authorization: token || getToken() }
    })
  } catch (err) {
    console.log("err in getNavIds : ", err)
  }

  // console.log("res : ", res)
  return (res && res.data) || {}
}

export const checkComplianceEntitlement = async token => {
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}controls/check-supervisor`, {
      headers: { Authorization: token || getToken() }
    })
  } catch (err) {
    console.log("err in getNavIds : ", err)
  }
  return (res && res.data) || {}
}

export const getConfigPicklistMeta = async token => {
  console.log("in action getConfigPicklistMeta")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}configs/`, {
      headers: { Authorization: token || getToken() },
      params: { require: "picklistsMeta" }
    })
    console.log("res : ", res)
    if (res && res.status === 200 && res.data && res.data.length) {
      const picklistsMeta = {}
      res.data.forEach(e => {
        if (e.title) {
          picklistsMeta[e.title] = e.meta
        }
      })
      console.log("picklistsMeta : ", picklistsMeta)
      return picklistsMeta
    }
    console.log("no data in getConfigPicklist: ")
    return {}
  } catch (err) {
    console.log("err in getConfigPicklist: ", err)
    return {}
  }
}

export const arraysWithSameElements = (arr1, arr2) => {
  // console.log("arr1 : ", arr1);
  // console.log("arr2 : ", arr2);
  if (!arr1 || !arr2) {
    return false
  }
  if (arr1.length !== arr2.length) {
    return false
  }

  if (!arr1.length && !arr2.length) {
    return true
  }

  let res = true
  arr1.some(e => {
    if (!arr2.includes(e)) {
      res = false
      return true
    }
  })
  // console.log("res : ", res);
  return res
}

export const getDocDetails = async docId => {
  let error
  let result
  if (!docId) {
    error = "No docId provided"
  }
  try {
    const res = await axios.get(`${muniApiBaseURL}docs/${docId}`, {
      headers: { Authorization: getToken() }
    })
    console.log("res : ", res)
    if (res && res.data) {
      result = res.data
    } else {
      error = "No document found with the docId"
    }
  } catch (err) {
    console.log("error in getting doc : ", err)
    error = "Error in getting document"
  }
  return [error, result]
}

export const getS3SignedURLDownload = async doc => {
  let error
  let result
  const { contextId, contextType, tenantId, name } = doc
  let fileName
  if (tenantId && contextId && contextType && name) {
    fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
  } else {
    error = "invalid doc details provided"
  }
  const opType = "download"
  try {
    const res = await axios.post(
      `${muniApiBaseURL}s3/get-signed-url`,
      {
        opType,
        fileName
      },
      { headers: { Authorization: getToken() } }
    )
    console.log("res : ", res)
    if (res && res.data && res.data.url) {
      result = res.data.url
    } else {
      error = "No signed url to download"
    }
  } catch (err) {
    console.log("Error in getting signed url to download : ", err)
    error = "Error in getting signed url to download"
  }
  return [error, result]
}

export const downloadFromS3AsStream = doc => {
  const { contextId, contextType, tenantId, name } = doc
  let objectName
  if (tenantId && contextId && contextType && name) {
    objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
  } else {
    console.log("invalid doc details provided")
  }

  try {
    axios
      .post(
        `${muniApiBaseURL}s3/get-s3-object-as-stream`,
        { objectName, fileName: name },
        {
          responseType: "arraybuffer",
          headers: { Authorization: token || getToken() }
        }
      )
      .then(res => {
        console.log("ok")
      })
      .catch(err => {
        console.log("err : ", err)
      })
    // console.log("res : ", res)
  } catch (err) {
    console.log("Error in streaming object to download : ", err)
  }
}

export const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

export const getDashboardData = async (token, payload) => {
  let result
  try {
    const res = await axios.post(`${muniApiBaseURL}dashboard/tasks`, payload, {
      headers: { Authorization: token || getToken() }
    })
    console.log("res : ", res)
    if (res && res.data) {
      result = res && res.data
    }
  } catch (err) {
    console.log("err in getDashboardData : ", err)
  }
  return result
}

export const getCurrentQuarter = () => {
  const currentDate = new Date()
  const currentQuarter = moment(currentDate).quarter()
  const currentYear = moment(currentDate).year()
  return [currentQuarter, currentYear]
}

/**
 * Save Interim Data
 * @param {['userdata', 'entitydata', 'trandata']} infotype
 * @param {any} payload
 */
export const saveInterimData = async (infotype, payload) => {
  let result
  if (infotype) {
    try {
      result = await axios.post(
        `${muniApiBaseURL}migration/interiminformation/${infotype}`,
        payload,
        { headers: { Authorization: getToken() } }
      )
    } catch (err) {
      console.log("err in saveInterimData : ", err)
    }
  } else {
    console.log("No infotype provided")
  }
  return result
}

/**
 * Get Interim data by infotype
 * @param {['userdata', 'entitydata', 'trandata']} infotype
 */
export const getInterimData = async infotype => {
  let result
  if (infotype) {
    try {
      const res = await axios.get(
        `${muniApiBaseURL}migration/interiminformation/${infotype}`,
        { headers: { Authorization: getToken() } }
      )
      if (res && res.data && res.data.data.data) {
        result = res.data.data.data[0]
      }
    } catch (err) {
      // console.log("error in getting interim data : ", err);
    }
  } else {
    console.log("No infotype provided")
  }
  return result
}

/**
 * Delete interim data
 * @param {['userdata', 'entitydata', 'trandata']} infotype
 */
export const deleteInterimData = async infotype => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}migration/interiminformation/${infotype}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log("error while deleting interim data : ", err)
  }
}

const setTransactionObject = (data, type, user, mappedColumn, partTypes, UWTypes) => {
  const participants = []
  const dealIssueUnderwriters = []
  const partKeys = Object.keys(data).filter(
    key => partTypes.indexOf(key) !== -1 && data[key]
  )
  const UWKeys = Object.keys(data).filter(
    key => UWTypes.indexOf(key) !== -1 && data[key]
  )
  if (partKeys && partKeys.length) {
    partKeys.forEach(key => {
      const findIndex = Object.values(mappedColumn).indexOf(key)
      const pType = Object.keys(mappedColumn)[findIndex]
      if (type.groupBy === "Debt" && type.name === "Bond Issue") {
        participants.push({
          dealPartType: pType || "",
          dealPartFirmId: "",
          dealPartFirmName: data[key] || "",
          addToDL: false,
          createdDate: new Date()
        })
      } else if (
        (type.groupBy === "Debt" && type.name !== "Bond Issue") ||
        type.groupBy === "Derivative" ||
        type.groupBy === "Others"
      ) {
        participants.push({
          partType: pType || "",
          partFirmId: "",
          partFirmName: data[key] || "",
          addToDL: false,
          createdDate: new Date()
        })
      }
    })
  }

  if(UWKeys && UWKeys.length){
    UWKeys.forEach(key => {
      const findIndex = Object.values(mappedColumn).indexOf(key)
      const pType = Object.keys(mappedColumn)[findIndex]
      if (type.groupBy === "Debt" && type.name === "Bond Issue") {
        dealIssueUnderwriters.push({
          dealPartContactAddToDL: false,
          dealPartType: pType || "",
          dealPartFirmId: "",
          roleInSyndicate: data[`dealSyndicate${key}`] || "",
          dealPartFirmName: data[key] || "",
          addToDL: false,
          createdDate: new Date()
        })
      }
    })
  }
  const serRatingObj = (seriesName, agency, rating) => ({
    // seriesId: "",
    seriesName: seriesName || "",
    ratingAgencyName: agency,
    longTermRating: rating || "",
    longTermOutlook: rating || "",
    shortTermOutlook: rating || "",
    shortTermRating: rating || ""
  })
  const dealIssueSeriesDetails = []

  if (type.groupBy === "Debt" && type.name === "Bond Issue") {
    if (data.seriesName) {
      dealIssueSeriesDetails.push({
        seriesName: data.seriesName || "",
        description: "",
        tag: "",
        seriesRatings: [
          serRatingObj(data.seriesName, "S&P", data.dealRatingSP),
          serRatingObj(data.seriesName, "Fitch", data.dealRatingFitch),
          serRatingObj(data.seriesName, "Kroll", data.dealRatingKroll),
          serRatingObj(data.seriesName, "Moodys", data.dealRatingMoodys)
        ],
        cepRatings: [],
        seriesPricingData: [],
        seriesPricingDetails: {
          dealSeriesPrincipal: data.dealSeriesPrincipal || 0,
          dealSeriesSecurityType: data.dealSeriesSecurityType || ""
        }
      })
    }
    if (data.seriesName2) {
      dealIssueSeriesDetails.push({
        seriesName: data.seriesName2 || "",
        description: "",
        tag: "",
        seriesRatings: [
          serRatingObj(data.seriesName2, "S&P", data.dealRatingSP2),
          serRatingObj(data.seriesName2, "Fitch", data.dealRatingFitch2),
          serRatingObj(data.seriesName2, "Kroll", data.dealRatingKroll2),
          serRatingObj(data.seriesName2, "Moodys", data.dealRatingMoodys2)
        ],
        cepRatings: [],
        seriesPricingData: [],
        seriesPricingDetails: {
          dealSeriesPrincipal: data.dealSeriesPrincipal2 || 0,
          dealSeriesSecurityType: data.dealSeriesSecurityType2 || ""
        }
      })
    }
    return {
      dealIssueTranClientId: user.entityId || "",
      dealIssueTranClientFirmName: user.firmName || "",
      dealIssueTranIssuerId: "",
      dealIssueTranIssuerFirmName: data.dealIssueTranClientFirmName || "",
      dealIssueTranProjectDescription: data.dealIssueTranProjectDescription || "",
      dealIssueTranIssueName: data.dealIssueTranProjectDescription || "",
      dealIssueTranStatus: data.dealIssueTranStatus || "Pre-Munivisor",
      dealIssueofferingType: data.dealIssueofferingType || "",
      dealIssueParAmount: data.dealIssueParAmount || null,
      dealIssueTranPrimarySector: data.dealIssueTranPrimarySector || "",
      dealIssueTranSecondarySector: data.dealIssueTranSecondarySector || "",
      dealIssuePricingDate: data.dealIssuePricingDate || "",
      dealIssueActAwardDate: data.dealIssueActAwardDate || "",
      termsOfIssue: data.termsOfIssue || "",
      dealIssueSecurityType: data.dealIssueSecurityType || "",
      dealIssueTranState: data.dealIssueTranState || "",
      dealIssueUseOfProceeds: data.dealIssueUseOfProceeds || "",
      dealIssueTranType: type.groupBy || "",
      dealIssueTranSubType: type.name || "",
      dealIssueSeriesDetails,
      dealIssueParticipants: participants,
      dealIssueUnderwriters,
      OnBoardingDataMigrationHistorical: true,
      historicalData: data._otherData,
      createdByUser: user.userId || "",
      tic: data.tic || null,
      bbri: data.bbri || null,
      nic: data.nic || null,
      gsp: data.gsp || null,
      mgtf: data.mgtf || null,
      exp: data.exp || null,
      tkdn: data.tkdn || null,
      other: data.other || null,
      updatedAt: new Date(),
      createdAt: new Date()
    }
  }
  if (type.groupBy === "RFP") {
    return {
      rfpTranClientId: user.entityId || "",
      rfpTranClientFirmName: user.firmName || "",
      rfpTranIssuerId: "",
      rfpTranIssuerFirmName: data.dealIssueTranClientFirmName || "",
      rfpTranStatus: data.dealIssueTranStatus || "Pre-Munivisor",
      rfpTranType: type.groupBy || "",
      rfpTranSubType: type.name || "",
      rfpTranState: data.dealIssueTranState || "",
      rfpTranPrimarySector: data.dealIssueTranPrimarySector || "",
      rfpTranSecurityType: data.dealIssueSecurityType || "",
      rfpParAmount: data.dealIssueParAmount || null,
      rfpTranProjectDescription: data.dealIssueTranProjectDescription || "",
      rfpTranIssueName: data.dealIssueTranProjectDescription || "",
      rfpParticipants: participants,
      OnBoardingDataMigrationHistorical: true,
      historicalData: data._otherData,
      createdByUser: user.userId || "",
      updatedAt: new Date(),
      createdAt: new Date()
    }
  }
  if (
    (type.groupBy === "Debt" && type.name !== "Bond Issue") ||
    type.groupBy === "Derivative" ||
    type.groupBy === "Others"
  ) {
    const object = {
      actTranFirmId: user.entityId || "",
      actTranFirmName: user.firmName || "",
      actTranClientId: "",
      actTranClientName: data.dealIssueTranClientFirmName || "",
      actTranUseOfProceeds: data.dealIssueUseOfProceeds || "",
      actTranPricingDate: data.dealIssuePricingDate || "",
      actTranBorrowerName: data.dealIssueBorrowerName || "",
      actTranStatus: data.dealIssueTranStatus || "Pre-Munivisor",
      actTranType: type.groupBy || "",
      actTranSubType: type.name || "",
      actTranPrimarySector: data.dealIssueTranPrimarySector || "",
      actTranSecondarySector: data.dealIssueTranSecondarySector || "",
      actTranProjectDescription: data.dealIssueTranProjectDescription || "",
      actTranIssueName: data.dealIssueTranProjectDescription || "",
      OnBoardingDataMigrationHistorical: true,
      historicalData: data._otherData,
      createdByUser: user.userId || "",
      updatedAt: new Date(),
      createdAt: new Date()
    }
    if (type.groupBy === "Debt" && type.name !== "Bond Issue") {
      return {
        ...object,
        bankLoanParticipants: participants,
        bankLoanSummary: {
          actTranSecurityType: data.dealIssueSecurityType || "",
          actTranState: data.dealIssueTranState || "",
          actTranClosingDate: data.dealIssueActAwardDate || ""
        },
        bankLoanTerms: {
          parAmount: data.dealIssueParAmount || null
        }
      }
    }
    if (type.groupBy === "Derivative") {
      return {
        ...object,
        derivativeParticipants: participants,
        derivativeSummary: {
          tranNotionalAmt: data.dealIssueParAmount || null,
          tranProjectDescription: data.dealIssueTranProjectDescription || "",
          tranType: type.name || ""
        }
      }
    }
    if (type.groupBy === "Others") {
      return {
        ...object,
        actTranState: data.dealIssueTranState || "",
        actTranOfferingType: data.dealIssueofferingType || "",
        actTranParAmount: data.dealIssueParAmount || null,
        actTranSecurityType: data.dealIssueSecurityType || "",
        actTranActAwardDate: data.dealIssueActAwardDate || "",
        actTranPricingDate: data.dealIssuePricingDate || "",
        participants,
        actTranSeriesDetails: dealIssueSeriesDetails
      }
    }
  }
  if (type.groupBy === "Business Development" || type.groupBy === "MA-RFP") {
    const busDevMarfp = {
      actTranFirmId: user.entityId || "",
      actTranFirmName: user.firmName || "",
      actIssuerClient: "",
      actIssuerClientEntityName: data.dealIssueTranClientFirmName || "",
      actStatus: data.dealIssueTranStatus || "Pre-Munivisor",
      actType: type.groupBy || "",
      actSubType: type.name || "",
      actPrimarySector: data.dealIssueTranPrimarySector || "",
      actProjectName: data.dealIssueTranProjectDescription || "",
      actIssueName: data.dealIssueTranProjectDescription || "",
      OnBoardingDataMigrationHistorical: true,
      historicalData: data._otherData,
      createdByUser: user.userId || "",
      updatedAt: new Date(),
      createdAt: new Date()
    }
    if (type.groupBy === "Business Development") {
      return {
        ...busDevMarfp,
        actType: "BusinessDevelopment",
        participants
      }
    }
    if (type.groupBy === "MA-RFP") {
      return {
        ...busDevMarfp,
        actSubType: "RFP",
        maRfpSummary: {
          actState: data.dealIssueTranState || "",
          actOfferingType: data.dealIssueofferingType || "",
          actSecurityType: data.dealIssueSecurityType || "",
          actParAmount: data.dealIssueParAmount || null,
          actPricingDate: data.dealIssuePricingDate || "",
          actActAwardDate: data.dealIssueActAwardDate || "",
        },
        maRfpParticipants: participants
      }
    }
  }
}

export const setDataClarity = async (
  type,
  list,
  userAddress,
  mappedColumn,
  allTranTypes,
  offeringTypes,
  user,
  partTypes,
  UWTypes
) => {
  const users = []
  const entities = []
  const tempList = []
  let returnObj = {}
  const phones1 = ["officePhone", "officePhone1", "officePhone2", "officePhone3", "officePhone4", "officePhone5"]
  const phones2 = ["address2officePhone", "address2officePhone1", "address2officePhone2", "address2officePhone3", "address2officePhone4", "address2officePhone5"]
  const address1Keys = [
    "firmAddressLine1",
    "firmAddressLine2",
    "firmState",
    "firmCity",
    "firmZipCode",
    "officeFax",
    "firmEmailId",
    "officePhone",
    "firmWebsite",
    ...phones1
  ]
  const address2Keys = [
    "address2AddressLine1",
    "address2AddressLine2",
    "address2State",
    "address2City",
    "address2ZipCode",
    "address2Website",
    "address2officePhone",
    "address2officeFax",
    "address2EmailId",
    ...phones2
  ]
  const userAddressKeys = [
    "userAddressLine1",
    "userAddressLine2",
    "userCountry",
    "userState",
    "userCity",
    "userZipCode"
  ]

  if (type === "Users/Entities") {
    list = Object.keys(list).forEach(rowKey => {
      const data = list[rowKey]
      const phones = ["userPhone", "userPhone1", "userPhone2", "userPhone3", "userPhone4"]
      const user = {
        userFirstName: data.userFirstName,
        userLastName: data.userLastName,
        userMiddleName: data.userMiddleName,
        userEmails: [
          {
            emailPrimary: true,
            emailId: data.emailId || ""
          }
        ],
        userRole: "tran-edit",
        userEntitlement: "tran-edit",
        userFirmName: data.firmName,
        userJobTitle: data.userJobTitle || "",
        userDepartment: data.userDepartment || "",
        OnBoardingDataMigrationHistorical: true,
        isMuniVisorClient: false,
        userPhone: [],
        userLoginCredentials: {
          userEmailId: data.emailId,
          password: "",
          onboardingStatus: "created",
          userEmailConfirmString: "",
          userEmailConfirmExpiry: "",
          passwordConfirmString: "",
          passwordConfirmExpiry: "",
          isUserSTPEligible: false,
          authSTPToken: "",
          authSTPPassword: "",
          passwordResetIteration: 0,
          passwordResetStatus: 0,
          passwordResetDate: ""
        },
        historicalData: data._otherData || {},
        updatedAt: new Date(),
        createdAt: new Date()
      }

      if(data && data.userEmail1){
        user.userEmails.push({
          emailPrimary: false,
          emailId: data.userEmail1 || ""
        })
      }
      if(data && data.userEmail2){
        user.userEmails.push({
          emailPrimary: false,
          emailId: data.userEmail2 || ""
        })
      }
      phones.forEach(pk => {
        if(data[pk]) {
          const validPhone = {
            ...data[pk]
          }
          if(data[pk].countryCode){
            validPhone.phoneNumber = `+${data[pk].countryCode} ${data[pk].phoneNumber}`
          }else {
            validPhone.phoneNumber = `+1 ${data[pk].phoneNumber}`
          }
          if(pk === "userPhone"){
            user.userPhone.push({ ...validPhone, phonePrimary: true})
          }else {
            user.userPhone.push(validPhone)
          }
        }
      })
      let address1 = {}
      let address2 = {}
      let address3 = {}
      const isAnyAddress1 = address1Keys.some(s => data[s])
      const isAnyAddress2 = address2Keys.some(s => data[s])
      const isAnyAddress3 = userAddressKeys.some(s => data[s])
      if (isAnyAddress1) {
        address1 = {
          isPrimary: true,
          website: data.firmWebsite || "",
          officePhone: [],
          officeFax: [{ faxNumber: (data.officeFax && data.officeFax.phoneNumber) || "" }],
          officeEmails: [{ emailId: data.firmEmailId || "" }],
          addressName: "Primary" || "",
          addressLine1: data.firmAddressLine1 || "",
          addressLine2: data.firmAddressLine2 || "",
          country: data.firmCountry || "",
          state: data.firmState || "",
          city: data.firmCity || "",
          zipCode: data.firmZipCode || {}
        }
        phones1.forEach(pk => {
          if(data[pk]){
            const validPhone = {
              ...data[pk]
            }
            if(data[pk].countryCode){
              validPhone.phoneNumber = `+${data[pk].countryCode} ${data[pk].phoneNumber}`
            }else {
              validPhone.phoneNumber = `+1 ${data[pk].phoneNumber}`
            }
            if(pk === "officePhone"){
              address1.officePhone.push({ ...validPhone, isPrimary: true})
            }else {
              address1.officePhone.push(validPhone)
            }
          }
        })
      }
      if (isAnyAddress2) {
        address2 = {
          isPrimary: !isAnyAddress1,
          officePhone: [],
          website: data.address2Website || "",
          officeFax: [{ faxNumber: (data.address2officeFax && data.address2officeFax.phoneNumber) || "" }],
          officeEmails: [{ emailId: data.address2EmailId || "" }],
          addressName: isAnyAddress1 ? "Secondary" : "Primary",
          addressLine1: data.address2AddressLine1 || "",
          addressLine2: data.address2AddressLine2 || "",
          country:  data.address2Country || "",
          state: data.address2State || "",
          city: data.address2City || "",
          zipCode: data.address2ZipCode || {}
        }
        phones2.forEach(pk => {
          if(data[pk]){
            const validPhone = {
              ...data[pk]
            }
            if(data[pk].countryCode){
              validPhone.phoneNumber = `+${data[pk].countryCode} ${data[pk].phoneNumber}`
            }else {
              validPhone.phoneNumber = `+1 ${data[pk].phoneNumber}`
            }
            if(pk === "address2officePhone"){
              address2.officePhone.push({ ...validPhone, isPrimary: true})
            }else {
              address2.officePhone.push(validPhone)
            }
          }
        })
      }
      if (isAnyAddress3){
        address3 = {
          isPrimary: true,
          addressName: "Primary" || "",
          addressLine1: data.userAddressLine1 || "",
          addressLine2: data.userAddressLine2 || "",
          country: data.userCountry || "",
          state: data.userState || "",
          city: data.userCity || "",
          zipCode: data.userZipCode || {}
        }
      }
      if (tempList.indexOf(data.firmName.toLowerCase()) === -1) {
        const entity = {
          firmName: data.firmName || "",
          msrbFirmName: data.firmName || "",
          addresses: [],
          OnBoardingDataMigrationHistorical: true,
          historicalData: data._otherData || {},
          updatedAt: new Date(),
          createdAt: new Date()
        }
        if (data.firmAddressLine1) {
          entity.addresses.push(address1)
        }
        if (data.address2AddressLine1) {
          entity.addresses.push(address2)
        }
        tempList.push(data.firmName.toLowerCase())
        entities.push(entity)
      }

      if(isAnyAddress3){
        user.userAddresses = [address3]
      }
      if (userAddress === "address1") {
        address1.isPrimary = !isAnyAddress3
        address1.addressName = isAnyAddress3 ? "Secondary" : "Primary"
        isAnyAddress3 ? user.userAddresses.push(address1) : user.userAddresses = [address1]
      }
      if (userAddress === "address2") {
        address1.isPrimary = !isAnyAddress3
        address1.addressName = isAnyAddress3 ? "Secondary" : "Primary"
        isAnyAddress3 ? user.userAddresses.push(address2) : user.userAddresses = [address2]
      }
      users.push(user)
    })
    returnObj = {
      users,
      entities
    }
  }
  if (type === "Entities") {
    list = Object.keys(list).forEach(rowKey => {
      const data = list[rowKey]
      let address1 = {}
      let address2 = {}
      const isAnyAddress1 = address1Keys.some(s => data[s])
      const isAnyAddress2 = address2Keys.some(s => data[s])
      if (isAnyAddress1) {
        address1 = {
          isPrimary: true,
          website: data.firmWebsite || "",
          officePhone: [],
          officeFax: [{ faxNumber: (data.officeFax && data.officeFax.phoneNumber) || "" }],
          officeEmails: [{ emailId: data.firmEmailId || "" }],
          addressName: "Primary" || "",
          addressLine1: data.firmAddressLine1 || "",
          addressLine2: data.firmAddressLine2 || "",
          country:  data.firmCountry || "",
          state: data.firmState || "",
          city: data.firmCity || "",
          zipCode: data.firmZipCode || {}
        }
        phones1.forEach(pk => {
          if(data[pk]){
            const validPhone = {
              ...data[pk]
            }
            if(data[pk].countryCode){
              validPhone.phoneNumber = `+${data[pk].countryCode} ${data[pk].phoneNumber}`
            }else {
              validPhone.phoneNumber = `+1 ${data[pk].phoneNumber}`
            }
            if(pk === "officePhone"){
              address1.officePhone.push({ ...validPhone, isPrimary: true})
            }else {
              address1.officePhone.push(validPhone)
            }
          }
        })
      }
      if (isAnyAddress2) {
        address2 = {
          isPrimary: !isAnyAddress1,
          officePhone: [],
          website: data.address2Website || "",
          officeFax: [{ faxNumber: (data.address2officeFax || data.address2officeFax.phoneNumber) || "" }],
          officeEmails: [{ emailId: data.address2EmailId || "" }],
          addressName: isAnyAddress1 ? "Secondary" : "Primary",
          addressLine1: data.address2AddressLine1 || "",
          addressLine2: data.address2AddressLine2 || "",
          country:  data.address2Country || "",
          state: data.address2State || "",
          city: data.address2City || "",
          zipCode: data.address2ZipCode || {}
        }
        phones2.forEach(pk => {
          const validPhone = {
            ...data[pk]
          }
          if(data[pk].countryCode){
            validPhone.phoneNumber = `+${data[pk].countryCode} ${data[pk].phoneNumber}`
          }else {
            validPhone.phoneNumber = `+1 ${data[pk].phoneNumber}`
          }
          if(pk === "address2officePhone"){
            address2.officePhone.push({ ...validPhone, isPrimary: true})
          }else {
            address2.officePhone.push(validPhone)
          }
        })
      }
      if (tempList.indexOf(data.firmName.toLowerCase()) === -1) {
        const entity = {
          firmName: data.firmName || "",
          msrbFirmName: data.firmName || "",
          addresses: [],
          OnBoardingDataMigrationHistorical: true,
          historicalData: data._otherData || {},
          updatedAt: new Date(),
          createdAt: new Date()
        }
        if (data.firmAddressLine1) {
          entity.addresses.push(address1)
        }
        if (data.address2AddressLine1) {
          entity.addresses.push(address2)
        }
        tempList.push(data.firmName.toLowerCase())
        entities.push(entity)
      }
    })
    returnObj = {
      entities
    }
  }
  if (type === "Users") {
    list = Object.keys(list).forEach(rowKey => {
      const data = list[rowKey]
      const user = {
        userFirstName: data.userFirstName,
        userLastName: data.userLastName,
        userMiddleName: data.userMiddleName,
        userEmails: [
          {
            emailPrimary: true,
            emailId: data.emailId || ""
          }
        ],
        userFirmName: data.firmName,
        userJobTitle: data.userJobTitle || "",
        userRole: "tran-edit",
        userEntitlement: "tran-edit",
        userDepartment: data.userDepartment || "",
        userLoginCredentials: {
          userEmailId: data.emailId,
          password: "",
          onboardingStatus: "created",
          userEmailConfirmString: "",
          userEmailConfirmExpiry: "",
          passwordConfirmString: "",
          passwordConfirmExpiry: "",
          isUserSTPEligible: false,
          authSTPToken: "",
          authSTPPassword: "",
          passwordResetIteration: 0,
          passwordResetStatus: 0,
          passwordResetDate: ""
        },
        isMuniVisorClient: false,
        OnBoardingDataMigrationHistorical: true,
        historicalData: data._otherData || {},
        updatedAt: new Date(),
        createdAt: new Date()
      }
      if(data && data.userEmail1){
        user.userEmails.push({
          emailPrimary: false,
          emailId: data.userEmail1 || ""
        })
      }
      if(data && data.userEmail2){
        user.userEmails.push({
          emailPrimary: false,
          emailId: data.userEmail2 || ""
        })
      }
      let address1 = {}
      const isAnyAddress1 = address1Keys.some(s => data[s])
      if (isAnyAddress1) {
        address1 = {
          isPrimary: true,
          website: data.firmWebsite || "",
          officePhone: [],
          officeFax: [{ faxNumber: (data.officeFax && data.officeFax.phoneNumber) || "" }],
          officeEmails: [{ emailId: data.firmEmailId || "" }],
          addressName: "Primary" || "",
          addressLine1: data.firmAddressLine1 || "",
          addressLine2: data.firmAddressLine2 || "",
          country:  data.firmCountry || "",
          state: data.firmState || "",
          city: data.firmCity || "",
          zipCode: data.firmZipCode || {}
        }
        phones1.forEach(pk => {
          if(data[pk]){
            const validPhone = {
              ...data[pk]
            }
            if(data[pk].countryCode){
              validPhone.phoneNumber = `+${data[pk].countryCode} ${data[pk].phoneNumber}`
            }else {
              validPhone.phoneNumber = `+1 ${data[pk].phoneNumber}`
            }
            if(pk === "officePhone"){
              address1.officePhone.push({ ...validPhone, isPrimary: true})
            }else {
              address1.officePhone.push(validPhone)
            }
          }
        })
        user.userAddresses = [address1]
      }
      users.push(user)
    })
    returnObj = {
      users
    }
  }
  if (type === "Deals") {
    const deals = []
    const rfps = []
    const bankloans = []
    const others = []
    const marfps = []
    const busDev = []
    const derivatives = []

    for (const rowKey in Object.keys(list)) {
      const data = list[rowKey] || {}
      const type = allTranTypes.find(
        type => data.dealIssueTranSubType && typeof data.dealIssueTranSubType === "string" && type.name.toLowerCase() === data.dealIssueTranSubType.trim().toLowerCase() // offeringTypes[data.dealIssueTranSubType]
      ) || {}

      const transaction = await setTransactionObject(
        data,
        type,
        user,
        mappedColumn,
        partTypes,
        UWTypes
      ) || {}
      if(data && data.dealIssueTranAssignedTo){
        transaction.dealIssueTranAssignedTo = data.dealIssueTranAssignedTo
      }
      if(data && data.dealIssueTranAssigneeId){
        transaction.dealIssueTranAssigneeId = data.dealIssueTranAssigneeId
      }
      if (type.groupBy === "Debt" && type.name === "Bond Issue") {
        if(data.dealIssueTranAssigneeId){
          transaction.dealIssueTranAssignedTo = [data.dealIssueTranAssigneeId]
        }
        deals.push(transaction)
      }
      /* if (type.groupBy === "RFP") {
        if(transaction.dealIssueTranAssigneeId){
          transaction.rfpTranAssignedTo = [data.dealIssueTranAssigneeId]
        }
        rfps.push(transaction)
      } */
      if (type.groupBy === "Debt" && type.name !== "Bond Issue") {
        if(transaction.dealIssueTranAssigneeId){
          transaction.actTranFirmLeadAdvisorId = data.dealIssueTranAssigneeId
          transaction.actTranFirmLeadAdvisorName = data.dealIssueTranAssignedTo
          transaction.actTranBorrowerName = data.dealIssueBorrowerName
        }
        bankloans.push(transaction)
      }
      if (type.groupBy === "Others") {
        if(transaction.dealIssueTranAssigneeId){
          transaction.actTranFirmLeadAdvisorId = data.dealIssueTranAssigneeId
          transaction.actTranFirmLeadAdvisorName = data.dealIssueTranAssignedTo
        }
        others.push(transaction)
      }
      /* if (type.groupBy === "Derivative") {
        if(transaction.dealIssueTranAssigneeId){
          transaction.actTranFirmLeadAdvisorId = data.dealIssueTranAssigneeId
          transaction.actTranFirmLeadAdvisorName = data.dealIssueTranAssignedTo
        }
        derivatives.push(transaction)
      }
      if (type.groupBy === "Business Development") {
        if(transaction.dealIssueTranAssigneeId){
          transaction.actLeadFinAdvClientEntityId = data.dealIssueTranAssigneeId
          transaction.actLeadFinAdvClientEntityName = data.dealIssueTranAssignedTo
        }
        busDev.push(transaction)
      }
      if (type.groupBy === "MA-RFP") {
        if(transaction.dealIssueTranAssigneeId){
          transaction.actLeadFinAdvClientEntityId = data.dealIssueTranAssigneeId
          transaction.actLeadFinAdvClientEntityName = data.dealIssueTranAssignedTo
        }
        marfps.push(transaction)
      } */
    }
    returnObj = { deals, bankloans, derivatives, rfps, others, marfps, busDev }
  }
  console.log("22. returnObj", returnObj)
  return returnObj
}

export const uploadMigrationData = async (infotype, payload) => {
  infotype = infotype === "Users/Entities" ? "UsersAndEntities" : infotype
  if (infotype === "Deals") {
    return await axios.post(
      `${muniApiBaseURL}migration/saveData/deals`,
      payload,
      { headers: { Authorization: getToken() } }
    )
  }
  return await axios.post(
    `${muniApiBaseURL}migration/saveData/${infotype}`,
    payload,
    { headers: { Authorization: getToken() } }
  )
}

export const getAuditLog = async (infotype, payload) => {
  infotype = infotype === "Users/Entities" ? "UsersAndEntities" : infotype
  return await axios.post(
    `${muniApiBaseURL}migration/saveData/${infotype}`,
    payload,
    { headers: { Authorization: getToken() } }
  )
}

export const putFileMigrationDataOnS3 = async (
  logInUserId,
  infotype,
  payload
) => {
  const fileName = `${logInUserId}/${infotype}.csv`
  axios
    .post(
      `${muniApiBaseURL}s3/put-s3-object`,
      {
        fileData: payload,
        fileName,
        contentType: "text/csv"
      },
      { headers: { Authorization: getToken() } }
    )
    .then(res => {
      console.log("ok in putMigrationDataOnS3: ", res)
    })
    .catch(err => {
      console.log("err in putMigrationDataOnS3: ", err)
    })
}

/**
 * Save migration data on S3
 */
export const putMigrationDataOnS3 = async (logInUserId, infotype, payload) => {
  const fileName = `${logInUserId}/${infotype}.csv`
  axios
    .post(
      `${muniApiBaseURL}s3/put-s3-object`,
      {
        fileData: payload,
        fileName,
        contentType: "text/csv"
      },
      { headers: { Authorization: getToken() } }
    )
    .then(res => {
      console.log("ok in putMigrationDataOnS3: ", res)
    })
    .catch(err => {
      console.log("err in putMigrationDataOnS3: ", err)
    })
}

/**
 * Save interim migration data on S3
 */
export const putMigrationDataFileOnS3 = async (
  logInUserId,
  infotype,
  payload
) => {
  const fileName = `${logInUserId}/Interim-data/${infotype}.csv`
  axios
    .post(
      `${muniApiBaseURL}s3/put-s3-object`,
      {
        fileData: payload,
        fileName,
        contentType: "text/csv"
      },
      { headers: { Authorization: getToken() } }
    )
    .then(res => {
      console.log("ok in putMigrationDataFileOnS3: ", res)
    })
    .catch(err => {
      console.log("err in putMigrationDataFileOnS3: ", err)
    })
}

export const getViewOnlyControl = async (_id, token) => {
  const res = await axios.get(`${muniApiBaseURL}controls-actions/${_id}`, {
    headers: { Authorization: token || getToken() }
  })
  console.log("res in getViewOnlyControl : ", res)
  return res && res.data
}

export const checkPlatformAdmin = async token => {
  try{
    const state = muniVisorApplicationStore.getState()
    if(state && state.platformAdmin && Object.keys(state.platformAdmin).length){
      return state.platformAdmin.platformAdmin
    }
    const res = await axios.get(
      `${muniApiBaseURL}entityUser/checkPlatFormAdmin`,
      { headers: { Authorization: token || getToken() } }
    )

    if (res) {
      muniVisorApplicationStore.dispatch({
        type: SET_PLATFORMADMIN,
        payload: { platformAdmin: res && res.data }
      })
      return res && res.data
    }
    return false
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
  }


  const res = await axios.get(
    `${muniApiBaseURL}entityUser/checkPlatFormAdmin`,
    { headers: { Authorization: token || getToken() } }
  )
  console.log("res in checkPlatFormAdmin : ", res)
  return res && res.data
}

export const getRelatedInfoForUser = async token => {
  const res = await axios.get(`${muniApiBaseURL}controls/related-info`, {
    headers: { Authorization: token || getToken() }
  })
  console.log("res in getRelatedInfoForUser : ", res)
  return res && res.data && res.data.payload
}

export const getTaskbyId = async taskId => {
  const res = await axios.get(
    `${muniApiBaseURL}tasks/get-task-details?tid=${taskId}`,
    { headers: { Authorization: token || getToken() } }
  )
  console.log("res in getTaskbyId : ", res)
  return res && res.data
}

export const saveTask = async (data, taskId, callback) => {
  let error
  const saveTaskInDB = async payload => {
    try {
      const res = await axios.post(
        `${muniApiBaseURL}tasks/add-from-ui`,
        payload,
        {
          headers: {
            Authorization: getToken()
          }
        }
      )
      console.log("task created ok : ", res.status)
    } catch (err) {
      console.log("error in creating task : ", err)
      error = err
    }
  }
  const {
    taskAssigneeType,
    taskNotes,
    taskDescription,
    taskStartDate,
    taskEndDate,
    taskPriority,
    taskStatus,
    taskType,
    documents,
    taskRefId,
    taskCreatedBy,
    relatedActivity,
    relatedEntity,
    relatedContact
  } = data
  const relatedActivityDetails = relatedActivity.map(e => ({
    activityId: e.id,
    activityProjectName: e.name
  }))[0]
  const relatedEntityDetails = relatedEntity.map(e => ({
    entityId: e.id,
    entityName: e.name
  }))[0]
  const relatedContacts = relatedContact.map(e => ({
    userId: e.id
  }))[0]
  const taskDetails = {
    taskAssigneeType,
    taskNotes,
    taskDescription,
    taskStartDate,
    taskEndDate,
    taskPriority,
    taskStatus,
    taskType
  }
  const taskRelatedDocuments = documents.map(e => ({
    docId: e._id,
    docFileName: e.name
  }))
  if (taskId) {
    taskDetails.taskAssigneeUserId = relatedContact[0].id
    taskDetails.taskAssigneeName = relatedContact[0].name
    const taskAssigneeUserDetails = { userId: relatedContact[0].id }
    const payload = {
      taskDetails,
      taskAssigneeUserDetails,
      taskRelatedDocuments,
      taskCreatedBy,
      relatedEntityDetails,
      relatedActivityDetails,
      relatedContacts,
      taskRefId
    }
    try {
      const res = await axios.post(
        `${muniApiBaseURL}tasks/update-from-ui?tid=${taskId}`,
        payload,
        {
          headers: {
            Authorization: token
          }
        }
      )
      console.log("task created ok : ", res.status)
    } catch (err) {
      console.log("error in creating task : ", err)
      error = err
    }
    // await saveTaskInDB(payload)
  } else {
    const numUsers = relatedContact.length
    for (let i = 0; i < numUsers; i++) {
      taskDetails.taskAssigneeUserId = relatedContact[i].id
      taskDetails.taskAssigneeName = relatedContact[i].name
      const taskAssigneeUserDetails = { userId: relatedContact[i].id }
      const payload = {
        taskDetails,
        taskAssigneeUserDetails,
        taskRelatedDocuments,
        taskCreatedBy,
        relatedEntityDetails,
        relatedActivityDetails,
        relatedContacts,
        taskRefId
      }
      await saveTaskInDB(payload)
    }
  }
  if (error) {
    callback(error)
  } else {
    callback()
  }
}

export const checkPasswordValid = async (email, password) => {
  try {
    const loginResponse = await axios.post(`${muniAuthBaseURL}/signin`, {
      email,
      password
    })
    const { authenticated } = loginResponse.data || {}
    if (authenticated) {
      return 1
    }
  } catch (err) {
    console.log("err in checkPasswordValid: ")
  }

  // if(loginResponse.status === 401) {
  //   return 0
  // }

  return 0
}

export const changeUserPassword = async (email, password, type, callback) => {
  try {
    const res = await axios.post(`${muniAuthBaseURL}/changePassword?type=${type || ""}`, {
      email,
      password
    })
    console.log("res in changeUserPassword : ", res)
    callback()
  } catch (err) {
    console.log("err in changeUserPassword: ", err)
    callback(err)
  }
}

export const getUsersFirmsDetails = async userIds => {
  try {
    const res = await axios.post(
      `${muniApiBaseURL}common/users-firms-details`,
      { userIds },
      { headers: { Authorization: getToken() } }
    )
    console.log("res in getUsersFirmsDetails : ", res)
    return res.data || []
  } catch (err) {
    console.log("err in getUsersFirmsDetails: ", err)
    return []
  }
}

export const updateTask = async (taskId, payload, oldValue, callback) => {
  callback = callback || (() => {})
  try {
    const res = await axios.post(
      `${muniApiBaseURL}tasks/update-from-ui?tid=${taskId}`,
      payload,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("task updated ok : ", res.status)

    if (payload["taskDetails.taskStatus"] === "Closed") {
      let getControl = []
      const getTask = await axios.get(
        `${muniApiBaseURL}tasks/get-task-details?tid=${taskId}`,
        { headers: { Authorization: token || getToken() } }
      )
      const id =
        (getTask.data &&
          getTask.data.taskDetails &&
          getTask.data.taskDetails.taskNotes && getTask.data.taskDetails.complianceTask) ||
        ""
      if (id && !getTask.taskRefId) {
        getControl = await axios.get(
          `${muniApiBaseURL}controls-actions/${id}`,
          { headers: { Authorization: token || getToken() } }
        )
      }
      if (getControl && getControl.data) {
        const updateControlAction = {
          ...getControl.data,
          status: "complete"
        }
        await axios.put(
          `${muniApiBaseURL}controls-actions/${getControl.data._id}`,
          updateControlAction
        )
      }
    }

    callback()
  } catch (err) {
    console.log("error in creating task : ", err)
    callback(err, oldValue)
  }
}

export const getControlsActions = async cacId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}controls-actions/getControlsActions/?cacId=${cacId}`,
      { headers: { Authorization: token || getToken() } }
    )
    return response
  } catch (error) {
    console.log(error)
  }
}

export const getChecklistTasks = async (
  activityId,
  activityContextTaskId,
  callback
) => {
  callback = callback || (() => {})
  const payload = {
    "relatedActivityDetails.activityId": activityId,
    "relatedActivityDetails.activityContextTaskId": activityContextTaskId
  }
  try {
    const res = await axios.post(
      `${muniApiBaseURL}tasks/get-checklist-tasks`,
      payload,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("tasks found ok : ", res.data)
    callback(null, res.data)
  } catch (err) {
    console.log("error in creating task : ", err)
    callback(err)
  }
}

export const getEntityLookUpData = async payload => {
  let result = []
  try {
    const res = await axios.post(
      `${muniApiBaseURL}entity/entity-look-up`,
      payload,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("entities found ok : ", res.data)
    result = res.data ? res.data.globIssuer || [] : []
  } catch (err) {
    console.log("error in getting entities : ", err)
  }
  return result
}

export const saveEntityDetails = async (
  entityId,
  entityDetails,
  relationshipType,
  callback
) => {
  callback = callback || (() => {})

  const apiURL = entityId
    ? `${muniApiBaseURL}entity/entities/${entityId}`
    : `${muniApiBaseURL}entity/entities`

  try {
    const res = await axios.post(
      apiURL,
      {
        entityDetails,
        relationshipType
      },
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("entity created/updated ok : ", res.data)
    const { _id } = (res && res.data && res && res.data[0]) || {}
    if (!entityId && relationshipType && _id) {
      console.log("creating config")
      axios.post(
        `${muniApiBaseURL}configs/create-ctp-config`,
        {
          entityId: _id,
          type: relationshipType
        },
        {
          headers: {
            Authorization: getToken()
          }
        }
      )
    }
    callback(null, res && res.data && res.data[0])

    if (relationshipType === "Self") {
      muniVisorApplicationStore.dispatch({
        type: AUDIT_UPDATE,
        payload: entityDetails && entityDetails.settings
      })

      const docId =
        (entityDetails &&
          entityDetails.settings &&
          entityDetails &&
          entityDetails.settings.logo &&
          entityDetails &&
          entityDetails.settings.logo.awsDocId) ||
        ""
      if (docId) {
        const baseURL = await getLogoBase64(docId)
        const toDataURL = url =>
          fetch(url)
            .then(response => response.blob())
            .then(
              blob =>
                new Promise((resolve, reject) => {
                  const reader = new FileReader()
                  reader.onloadend = () => resolve(reader.result)
                  reader.onerror = reject
                  reader.readAsDataURL(blob)
                })
            )

        toDataURL(baseURL).then(dataUrl => {
          muniVisorApplicationStore.dispatch({
            type: PDF_LOGO,
            payload: { logo: dataUrl }
          })
        })
      }
    }
  } catch (err) {
    console.log("error in creating/updating entity : ", err)
    callback(err)
  }
}

export const getUniqValueFromArray = async arr => {
  const uniqVal = []
  arr.forEach(val => {
    if (uniqVal.indexOf(val) === -1) {
      uniqVal.push(val)
    }
  })
  return uniqVal
}

export const getUsersByFlags = async flags => {
  try {
    const res = await axios.post(
      `${muniApiBaseURL}entityUser/get-users-by-flags`,
      {
        flags
      },
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("users found ok : ", res.data)
    return (res && res.data) || []
  } catch (err) {
    console.log("error in getting users : ", err)
  }
}

export const getEntityDetails = async (entityId, callback) => {
  console.log("entityId : ", entityId)
  callback = callback || (() => {})
  try {
    const res = await axios.get(
      `${muniApiBaseURL}entity/entities/${entityId}`,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    callback(null, res.data)
  } catch (err) {
    console.log("error in getEntityDetails : ", err)
    callback(err)
  }
}

export const getCalendarDateAsString = isoString => {
  const date = ""
  if (!isoString) {
    return date
  }
  return isoString.substring(0, 10)
}

export const saveEntityUserDetails = async (userId, userDetails, callback) => {
  callback = callback || (() => {})

  const apiURL = userId
    ? `${muniApiBaseURL}entityUser/users/${userId}`
    : `${muniApiBaseURL}entityUser/users`

  try {
    const res = await axios.post(
      apiURL,
      {
        userDetails
      },
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("user created/updated ok : ", res.data)
    callback(null, res.data && res.data.success)
  } catch (err) {
    console.log("error in creating/updating user : ", err)
    callback(err)
  }
}

export const getEntityUserDetails = async (userId, callback) => {
  console.log("userId : ", userId)
  callback = callback || (() => {})
  try {
    const res = await axios.get(
      `${muniApiBaseURL}entityUser/users/${userId}`,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    callback(null, res.data)
  } catch (err) {
    console.log("error in getEntityUserDetails : ", err)
    callback(err)
  }
}

export const sendUserOnboardingEmail = async (userId, callback) => {
  callback = callback || (() => {})
  try {
    const response = await axios.post(
      `${muniApiBaseURL}emails/sendonboardingemail/${userId}`,
      {},
      { headers: { Authorization: getToken() } }
    )
    callback(null, response)
  } catch (err) {
    console.log("error in sending user onboarding email : ", err)
    callback(err)
  }
}

export const getAllRelatedEntites = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/allEnt`, {
      headers: { Authorization: getToken() }
    })
    return (response && response.data) || []
  } catch (error) {
    console.log("err in getting related entities : ", error)
    return []
  }
}

export const getEntityUsers = async (entityId, required) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entityUser/get-entity-users`,
      {
        entityId,
        required
      },
      {
        headers: { Authorization: getToken() }
      }
    )
    return (response && response.data) || []
  } catch (error) {
    console.log("err in getting related entities : ", error)
    return []
  }
}

export const getFile = (query, fileName) => {
  try {
    /* const {objectName, fileName} = this.state
    query = `?objectName=${objectName}` */

    axios
      .get(`${muniApiBaseURL}s3/get-s3-object-as-stream${query}`, {
        responseType: "arraybuffer",
        headers: { Authorization: token || getToken() }
      })
      .then(res => {
        console.log("ok", res)
        if (res.status === 200) {
          return res.data
        }
        return null
      })
      .then(data => {
        const blob = new Blob([data])
        const url = window.URL.createObjectURL(blob)
        // attach blob url to anchor element with download attribute
        const anchor = document.createElement("a")
        anchor.setAttribute("href", url)
        anchor.setAttribute("download", fileName)
        anchor.click()
        window.URL.revokeObjectURL(url)
      })
      .catch(err => {
        console.log("err : ", err)
        return null
      })
  } catch (err) {
    console.log("Error in streaming object to download : ", err)
  }
}

export const getBorrowerLookUpData = async payload => {
  let result = []
  try {
    const res = await axios.post(
      `${muniApiBaseURL}entity/borrower-look-up`,
      payload,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("entities found ok : ", res.data)
    result = res.data || []
  } catch (err) {
    console.log("error in getting entities : ", err)
  }
  return result
}

export const insertNotification = async body => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}configs/notification`,
      { body },
      { headers: { Authorization: getToken() } }
    )
    console.log("notification response : ", response)
    return response
  } catch (err) {
    console.log("notification err : ", err)
    return err
  }
}

export const getNotification = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}configs/notification`, {
      headers: { Authorization: getToken() }
    })
    return response.data
  } catch (error) {
    return error
  }
}

export const getThirdPartyLookUpData = async payload => {
  let result = []
  try {
    const res = await axios.post(
      `${muniApiBaseURL}entity/third-party-look-up`,
      payload,
      {
        headers: {
          Authorization: getToken()
        }
      }
    )
    console.log("entities found ok : ", res.data)
    result = res.data || []
  } catch (err) {
    console.log("error in getting entities : ", err)
  }
  return result
}

export const checkDueInCurrentPeriod = (dueDate, unit) => {
  console.log("dueDate, unit : ", dueDate, unit)
  if (!dueDate || !unit) {
    return false
  }

  dueDate = new Date(dueDate)
  let endDate = new Date()
  switch (unit) {
    case "week":
      endDate = moment()
        .endOf("isoWeek")
        .toDate()
      console.log("endDate : ", endDate)
      return dueDate <= endDate
    default:
      endDate = moment()
        .endOf(unit)
        .toDate()
      console.log("endDate : ", endDate)
      return dueDate <= endDate
  }
}

export const getControlActionsDetails = async (
  filter,
  projection,
  callback
) => {
  callback = callback || (() => {})
  try {
    const res = await axios.post(
      `${muniApiBaseURL}controls-actions/get-actions-details`,
      { filter, projection },
      { headers: { Authorization: token || getToken() } }
    )
    const result = (res && res.data) || []
    callback(null, result)
  } catch (err) {
    console.log("err in getting control details : ", err)
    callback(err)
  }
}

export const sendOnBoardingEmailForMigratedUsers = async userId => {
  const res = await axios.post(
    `${muniApiBaseURL}emails/sendonboardingemail/${userId}`,
    {},
    { headers: { Authorization: token || getToken() } }
  )
  return (res && res.data) || {}
}

export const getEntityAddressChangeLog = (oldArr = [], newArr = []) => {
  console.log("in getEntityAddressChangeLog")
  const changeLog = []

  const fields = {
    addressName: "Name",
    website: "Website",
    addressLine1: "Address Line 1",
    addressLine2: "Address Line 2",
    country: "Country",
    state: "State",
    city: "City"
  }

  const oldLen = oldArr.length
  const newLen = newArr.length

  const lenDiff = newLen - oldLen

  if (lenDiff > 0) {
    const newItems = newArr.slice(oldLen)
    newItems.forEach(e => {
      let values = ""
      Object.keys(fields).forEach(f => {
        if (e[f]) {
          values += ` ${fields[f]}: ${e[f]}`
        }
      })
      changeLog.push({
        log: `Added new address with values - ${values}`
      })
    })
  } else if (lenDiff < 0) {
    changeLog.push({
      log: "Removed an address"
    })
  }

  newArr.slice(0, oldLen).forEach((e, i) => {
    Object.keys(fields).forEach(f => {
      if (newArr[i][f] !== oldArr[i][f]) {
        changeLog.push({
          log: `Changed value of ${fields[f]} from ${oldArr[i][f] ||
            "blank"} to ${newArr[i][f] || "No value"} for the address ${
            oldArr.addressLine1
          }`
        })
      }
    })
  })

  return changeLog
}

export const transactionUrl = (
  type,
  subtype,
  activityDescription,
  activityId,
  search,
  tab
) => {
  let url = ""
  type = type.toUpperCase()
  subtype = subtype.toUpperCase()

  switch (subtype) {
    case "BOND ISSUE":
      url = `/deals/${activityId || ""}/${tab || "summary"}`
      break
    case "BANK LOAN":
      url = `/loan/${activityId || ""}/${tab || "summary"}`
      break
    case "LETTER OF CREDIT":
      url = `/loan/${activityId || ""}/${tab || "summary"}`
      break
    case "LINES OF CREDIT":
      url = `/loan/${activityId || ""}/${tab || "summary"}`
      break
    case "LINES AND LETTER":
      url = `/loan/${activityId || ""}/${tab || "summary"}`
      break
    case "SWAP":
      url = `/derivative/${activityId || ""}/${tab || "summary"}`
      break
    case "CAP":
      url = `/derivative/${activityId || ""}/${tab || "summary"}`
      break
    case "RFP":
      url = `/marfp/${activityId || ""}/${tab || "summary"}`
      break
    default:
      url = ""
      break
  }

  if (!url) {
    switch (type) {
      case "RFP":
        url = `/rfp/${activityId || ""}/${tab || "summary"}`
        break
      case "OTHERS":
        url = `/others/${activityId || ""}/${tab || "summary"}`
        break
      case "BUSINESSDEVELOPMENT":
        url = `/bus-development/${activityId || ""}`
        break
      default:
        url = ""
        break
    }
  }
  if(!activityDescription){
    return <small/>
  }
  return (
    <Link to={url}>
      <small
        dangerouslySetInnerHTML={{
          __html: highlightSearch(activityDescription || "-", search)
        }}
      />
    </Link>
  )
}

export const checkInvalidFiles = (uploadedFiles, singleMulti) => {
  const invalidFileType = ["gif", "vbs", "exe", "zip", "mp3", "mp4", ""]
  const invalidFile = []
  let fileSize = ""
  if (singleMulti === "multi") {
    if (uploadedFiles) {
      uploadedFiles.forEach(u => {
        const fileType = (u && u.name && u.name.split(".").pop()) || ""
        if (invalidFileType.indexOf(fileType.toLowerCase()) > -1) {
          invalidFile.push(u.name)
        } else if(u.size > 52428800){
          fileSize = u.size
        }
      })
      if (invalidFile && invalidFile.length) {
        return "File Type is Invalid!"
      } else if(fileSize){
        return "File is large maximum file size is 50 MB!"
      }
    }
  } else if (singleMulti === "single") {
    const fileType =
      (uploadedFiles &&
        uploadedFiles.name &&
        uploadedFiles.name.split(".").pop()) ||
      ""
    if (invalidFileType.indexOf(fileType.toLowerCase()) > -1) {
      return `${uploadedFiles.name} File Type is Invalid!`
    } else if(uploadedFiles.size > 52428800){
      return "File is large maximum file size is 50 MB!"
    }
  }
}

export const removeWhiteSpaces = value => {
  if (typeof value === "string") {
    return value.trim().replace(/\s+/g, " ") || ""
  }
  return value
}

/**
 *
 * @param {*} service transactions | entities | users
 * @param {*} id
 * @param {*} types entity type, user type, transaction type
 * @param {*} subtypes
 */
export const urlHelper = (service, id, types, subtypes, tab) => {
  let url = "/dashboard"
  const urlFor = service.toLowerCase()
  const type = types.toUpperCase()
  const subType = (subtypes || "").toUpperCase()

  if (urlFor === "transactions") {
    switch (subType) {
    case "BOND ISSUE":
      url = `/deals/${id || ""}/${tab || "summary"}`
      break
    case "BANK LOAN":
      url = `/loan/${id || ""}/${tab || "summary"}`
      break
    case "LETTER OF CREDIT":
      url = `/loan/${id || ""}/${tab || "summary"}`
      break
    case "LINES OF CREDIT":
      url = `/loan/${id || ""}/${tab || "summary"}`
      break
    case "LINES AND LETTER":
      url = `/loan/${id || ""}/${tab || "summary"}`
      break
    case "SWAP":
      url = `/derivative/${id || ""}/${tab || "summary"}`
      break
    case "CAP":
      url = `/derivative/${id || ""}/${tab || "summary"}`
      break
    case "RFP":
      url = `/marfp/${id || ""}/${tab || "summary"}`
      break
    default:
      url = ""
      break
    }

    if (!url) {
      switch (type) {
      case "MA RFPS":
        url = `/marfp/${id || ""}/${tab || "summary"}`
        break
      case "RFP":
        url = `/rfp/${id || ""}/${tab || "summary"}`
        break
      case "OTHERS":
        url = `/others/${id || ""}/${tab || "summary"}`
        break
      case "BUSINESSDEVELOPMENT":
        url = `/bus-development/${id || ""}`
        break
      default:
        url = "/dashboard"
        break
      }
    }
  }
  if (urlFor === "entities") {
    switch (type) {
    case "FIRM":
    case "SELF":
      url = `/admin-firms/${id || ""}/firms`
      break
    case "CLIENT":
    case "PROSPECT":
      url = `/clients-propects/${id || ""}/entity`
      break
    case "THIRD PARTY":
      url = `/thirdparties/${id || ""}/entity`
      break
    case "UNDEFINED":
      url = `/migratedentities/${id || ""}/entity`
      break
    default:
      url = `/thirdparties/${id || ""}/entity`
      break
    }
  }

  if (urlFor==="users") {
    switch (type) {
    case "FIRM":
    case "SELF":
      url = `/admin-users/${id || ""}/users`
      break
    case "CLIENT":
      url = `/contacts/${id || ""}/cltprops`
      break
    case "PROSPECT":
      url = `/contacts/${id || ""}/cltprops`
      break
    case "THIRD PARTY":
      url = `/contacts/${id || ""}/thirdparties`
      break
    case "UNDEFINED":
      url = `/contacts/${id || ""}/migratedentities`
      break
    default:
      url = `/contacts/${id || ""}/thirdparties`
      break
    }
  }
  return url
}

export const checkListsList = async (token) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}configs/`, {
      headers:{"Authorization":token},
      params: { require: "checklists" }
    })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data && res.data.length) {
      const checklists = res.data
      const payload = checklists.map(c => {
        const { id, name, type, attributedTo, bestPractice, notes, lastUpdated, data } = c
        const item = { id, name, type, attributedTo, bestPractice, notes, lastUpdated }
        const [itemData, otherData] = mapAccordionsArrayToObject(data, true)
        const itemHeaders = {}
        Object.keys(otherData).forEach(k => {
          itemHeaders[k] = otherData[k].headers
        })
        item.data = itemData
        item.itemHeaders = itemHeaders
        return item
      })
      console.log("===============>", payload)
      return payload
    } else {
      console.log("no data in getConfigChecklist: ")

    }
  } catch (err) {
    console.log("err in getConfigChecklist: ", err)
  }
}

export const generateInvoicePDF = async (invoiceData, fileName) => {
  try{
    const res = await axios.post(
      `${muniApiBaseURL}pdfs/generateinvoicePdf`,
      {invoiceData, fileName},
      {  responseType: "arraybuffer", headers: { Authorization: token || getToken() } }
    )
    const blob = new Blob([res.data])
    FileSaver.saveAs(blob, `${fileName ? `${fileName}.pdf` : "invoice.pdf"}`)
  }catch (e) {
    console.log("err in generate pdf : ", e)
  }
}

export const generateJSONToPDF = async (payload) => {
  try{
    const res = await axios.post(
      `${muniApiBaseURL}pdfs/jsontopdf`,
      {...payload},
      {  responseType: "arraybuffer", headers: { Authorization: token || getToken() } }
    )
    const blob = new Blob([res.data])
    FileSaver.saveAs(blob, `${payload && payload.fileName ? `${payload.fileName}` : "Data.pdf"}`)
  }catch (e) {
    console.log("err in generate pdf : ", e)
  }
}

export const generateG10PDF = async (payload, type) => {
  try{

    const res = await axios.post(`${muniApiBaseURL}pdfs/generateg10pdf`,{...payload},{  responseType: "arraybuffer", headers: { Authorization: token || getToken() } })
    const file = new Blob([res.data], {type: "application/pdf"})
    const fileURL = URL.createObjectURL(file)
    if(type === "preview"){
      window.open(fileURL)
    }
    return file
  }catch (e) {
    console.log("err in generate pdf : ", e)
  }
}

export const generateDistributionPDF = async (payload) => {
  try{
    const res = await axios.post(`${muniApiBaseURL}pdfs/distributionpdf`, {...payload},
      {  responseType: "arraybuffer", headers: { Authorization: token || getToken() } }
    )
    const blob = new Blob([res.data])
    const filename = `${payload && payload.fileName ? `${payload.fileName}` : "Data.pdf"}`
    FileSaver.saveAs(blob, filename)
  }catch (e) {
    console.log("err in generate pdf : ", e)
  }
}

export const submitForgotPassword = async (email) => {
  const response = await axios.post(`${muniAuthBaseURL}/forgotpassword`, { email }, { responseType: "json"})
  return response.data
}

export const getBillingInvoicesList = async (entityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}plateformadmincommon/billing/${entityId}`, {
      headers: { Authorization: getToken() }
    })
    return (response && response.data) || []
  } catch (error) {
    console.log("err in getting Billing Invoices : ", error)
    return []
  }
}

export const makePayment = async (payload, callback) => {

  try {
    const response = await axios.post(`${muniApiBaseURL}plateformadmincommon/payment`, payload, {
      headers: { Authorization: getToken() }
    })
    callback(response || [])
  } catch (error) {
    callback({ error })
  }
}

export const getPlateFormDocDetails = async docId => {
  let error
  let result
  if (!docId) {
    error = "No docId provided"
  }
  try {
    const res = await axios.get(`${muniApiBaseURL}plateformadmincommon/plateformdocs/${docId}`, {
      headers: { Authorization: getToken() }
    })
    console.log("res : ", res)
    if (res && res.data) {
      result = res.data
    } else {
      error = "No document found with the docId"
    }
  } catch (err) {
    console.log("error in getting doc : ", err)
    error = "Error in getting document"
  }
  return [error, result]
}

export const dateFormatted = (val) => {
  let date = null
  if(val){
    const mm = val.getMonth() + 1
    const dd = val.getDate()
    date =  [
      (mm>9 ? "" : "0") + mm,
      (dd>9 ? "" : "0") + dd,
      val.getFullYear()
    ].join("-")
    date = `${date}`
  }
  return date
}
