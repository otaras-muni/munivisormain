import React from "react"
import { AppMainContainer } from "./AppMainContainer"
require('./images/favicon.ico');

const AppGlobal = () => {
  console.log("THE PROCESS ENV VARIABLE FOR APIS IS",process.env.API_URL)
  return (<div>
    <AppMainContainer />
  </div>)
}

export default AppGlobal
