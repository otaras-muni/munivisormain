/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import React, { Component } from "react"
import PropTypes from "prop-types"
import { ReactiveComponent } from "@appbaseio/reactivesearch"

class ReactiveFacet extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: props.data.isOpen || false
    }

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { data } = this.props
    const { isOpen } = this.state
    const iconClass = isOpen ? "up" : "down"
    return (
      <div className="facet-item-wrapper">
        <div
          className="facet-item-title-wrapper"
          onClick={this.handleToggle}
        >
          <span className="search-filter-title"> {data.title} </span>
          <span className={`fas fa-chevron-${iconClass}`} />
        </div>
        <ReactiveComponent
          {...data.props}
        >
          {data.component}
        </ReactiveComponent>
      </div>
    )
  }
}

ReactiveFacet.propTypes = {
  data: PropTypes.object.isRequired
}

export default ReactiveFacet
