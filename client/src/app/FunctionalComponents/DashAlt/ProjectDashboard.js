import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { getAllTransactions } from "AppState/actions/AdminManagement/admTrnActions";
import { connect } from "react-redux";
import Loader from "../../GlobalComponents/Loader";
import Nav from "./Nav.js";
import { toast } from "react-toastify";
import { Multiselect, DropdownList } from "react-widgets";
import { DebounceInput } from "react-debounce-input";
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import dateFormat from "../../../globalutilities/dateFormat"
import {
  saveMasterListSearchPrefs,
  deleteMasterListSearchPrefs,
  fetchMasterListSearchPrefs,
  getProjectsList
} from "AppState/actions/AdminManagement/admTrnActions";
import CONST from "../../../globalutilities/consts";
import EntityPageGrid from "../EntityManagement/CommonComponents/EntityPageGrid";

export const Modal = ({ message, closeModal, modalState, onConfirmed }) => {
  if (!modalState) {
    return null;
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => {}}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" onClick={onConfirmed}>
              Yes
            </button>
            <button className="button is-light" onClick={closeModal}>
              No
            </button>
          </div>
        </footer>
      </div>
    </div>
  );
};

class ProjectDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = this.initialState();
  }

  initialState() {
    return {
      getDataTime: undefined,
      searchProject: "",
      search: "",
      periodData: CONST.periodData,
      tranStatuses: [
        "Select All",
        ...CONST.tranStatus,
        ...CONST.opportunityStatus
      ],
      expanded: {
        filter: false
      },
      filteredValue: {
        tranSearch: "",
        createdPeriod: "YTD",
        pricingPeriod: "",
        tranStatus: [
          "Executed but not closed",
          "Pre-execution",
          "Pre-Pricing",
          "In Progress"
        ],
        serverPerformPagination: true
      },
      groups: {
        transactions: {
          currentPage: 0,
          size: 25,
          sortFields: {
            leadManager: 1
          }
        },
        projects: {
          currentPage: 0,
          size: 25,
          sortFields: {
            tranClientName: -1
          }
        },
        marfps: {
          currentPage: 0,
          size: 25,
          sortFields: {
            tranClientName: 1
          }
        }
      },
      loading: false,
      transactionList: {
        allData: {}
      },
      isSaving: false,
      savedSearches: "",
      savedSearchesList: [],
      savedSearchesData: [],
      pageSizeOptions: ["5", "10", "20", "25", "50"],
      showResult: [true, false, false],
      pageSize: 25,
      jsonSheets: [],
      startXlDownload: false
    };
  }

  async componentWillMount() {
    this.setState({
      loading: true
    });
    let filteredDefaultValue = {};
    let { transactionList } = this.state;
    let filteredValue = {};
    let prefResult = {};
    prefResult = fetchMasterListSearchPrefs("searchPrefProjectDash");

    let result = {}; //getAllTransactions("")
    prefResult = await prefResult;
    const receivedDefault = await this.setSearchPref(
      prefResult,
      filteredDefaultValue,
      filteredValue
    );
    this.setState(
      {
        savedSearches: receivedDefault.filteredDefaultValue
          ? receivedDefault.filteredDefaultValue.searchName
          : "",
        searchName: receivedDefault.filteredDefaultValue
          ? receivedDefault.filteredDefaultValue.searchName
          : "",
        filteredValue: receivedDefault.filteredValue,
        pageSize: receivedDefault.filteredValue.pageSizePref || 25,
        loading: false,
        savedSearchesData: prefResult.data.length > 0 ? prefResult.data : [],
        savedSearchesList: [
          ...(prefResult.data.length > 0
            ? prefResult.data.map(item => ({
                label: item.searchName,
                value: item._id
              }))
            : [])
        ]
      },
      async () => await this.getFilteredData(0)
    );
  }

  searchProject = async e => {
    const { name, value } = e.target;
    let searchValue = "";
    if (value.length === 0) {
      searchValue = "";
    } else {
      searchValue = value;
    }
    this.setState({
      searchProject: value,
      search: value
    });
    const result = await getAllTransactions(searchValue);
    if (result.status === 200 && result.data) {
      this.setState({
        transactionList: result.data
      });
    }
  };

  setSearchPref = async (prefResult, filteredDefaultValue, filteredValue) => {
    const defaultSearchValue = this.setSelectedDefaultValue(prefResult);
    filteredDefaultValue = defaultSearchValue.filteredDefaultValue;
    filteredValue = defaultSearchValue.filteredValue;
    prefResult.data.unshift(this.searchAllItem());
    return { prefResult, filteredDefaultValue, filteredValue };
  };

  searchAllItem() {
    return {
      _id: 0,
      searchName: "Search All",
      searchPreference: JSON.stringify({
        tranSearch: "",
        createdPeriod: "",
        pricingPeriod: "",
        tranStatus: [],
        serverPerformPagination: true
      })
    };
  }

  setSelectedDefaultValue(prefResult) {
    let filteredValue = {};
    const filteredDefaultValue = prefResult && prefResult.data && prefResult.data.find(
      pref => JSON.parse(pref.searchPreference).defaultSearch === true
    );
    if (!filteredDefaultValue) {
      filteredValue = this.initialState().filteredValue;
    } else {
      filteredValue = JSON.parse(filteredDefaultValue.searchPreference);
    }
    return { filteredDefaultValue, filteredValue };
  }

  toggleModal = () => {
    this.setState(prev => {
      const newState = !prev.modalState

      return { modalState: newState }
    })
  };

  handleGetPref = async savedSearches => {
    const { savedSearchesData, entityId } = this.state;
    let { filteredValue } = this.state;
    const { userEntities } = this.props.auth;
    if (savedSearches !== "" && savedSearches.value !== "") {
      const filteredData = savedSearchesData.find(
        item => item._id === savedSearches.value
      );
      const { searchPreference } = filteredData;
      filteredValue = JSON.parse(searchPreference);
    } else {
      filteredValue = {
        period: "",
        tranStatus: [],
        tranSearch: "",
        searchName: ""
      };
    }
    let result = {};
    this.setState(
      {
        filteredValue,
        entityList: [],
        total: 0,
        pages: 0,
        searchName:
          savedSearches.value !== "" && savedSearches.value !== 0
            ? savedSearches.label
            : "",
        loading: true,
        savedSearches,
        pageSize: filteredValue.pageSizePref || 25
      },
      async () => {
        result = await this.getFilteredData(
          0,
          null,
          filteredValue.pageSizePref
        );
        this.setState({
          searchName:
            savedSearches.value !== "" && savedSearches.value !== 0
              ? savedSearches.label
              : ""
        });
      }
    );
  };

  handleSavePref = async confirmed => {
    let {
      filteredValue,
      searchName,
      savedSearchesList,
      savedSearches,
      savedSearchesData
    } = this.state;

    if (searchName !== "") {
      const defaultSearch = savedSearchesData.find(
        data => JSON.parse(data.searchPreference).defaultSearch === true
      );
      if (
        filteredValue.defaultSearch === true &&
        defaultSearch &&
        defaultSearch.searchName !== searchName
      ) {
        if (defaultSearch && !confirmed) {
          this.setState({
            modalState: true,
            modalMessage: "Do you want to change default search to this?"
          });
          return;
        }
        if (defaultSearch && confirmed) {
          this.toggleModal();
          let defaultSavedSearchPreference = JSON.parse(
            defaultSearch.searchPreference
          );
          defaultSavedSearchPreference.defaultSearch = false;
          savedSearchesData = await this.saveSearchPrefAndRestoreValues(
            defaultSearch.searchName,
            defaultSavedSearchPreference,
            savedSearchesList,
            savedSearchesData,
            false
          );
        }
      }
      this.setState({
        isSaving: true
      });
      savedSearchesData = await this.saveSearchPrefAndRestoreValues(
        searchName,
        filteredValue,
        savedSearchesList,
        savedSearchesData,
        true
      );
    }
  };

  handleDeletePref = async () => {
    let { savedSearches, entityId } = this.state;
    let { savedSearchesList, savedSearchesData } = this.state;
    const { userEntities } = this.props.auth;
    if (
      savedSearches !== "" &&
      savedSearches.value !== "" &&
      savedSearches.value !== 0
    ) {
      if (!savedSearches.value && typeof(savedSearches) === "string") {
        savedSearches = savedSearchesList.find(
          item => item.label === savedSearches
        )
      }
      const result = await deleteMasterListSearchPrefs(savedSearches.value);
      if (result.status === 200) {
        savedSearchesList = savedSearchesList.filter(
          item => item.value !== savedSearches.value
        );
        savedSearchesData = savedSearchesData.filter(
          item => item._id !== savedSearches.value
        );
        this.setState(
          {
            savedSearchesList,
            savedSearches: "",
            savedSearchesData,
            filteredValue: {
              tranSearch: "",
              createdPeriod: "YTD",
              pricingPeriod: "",
              tranStatus: [
                "Executed but not closed",
                "Pre-execution",
                "Pre-Pricing"
              ],
              serverPerformPagination: true
            },
            searchName: ""
          },
          async () => {
            const result = await this.getFilteredData(0);
          }
        );
        toast("Successfully Deleted", {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        });
      } else
        toast("Something went wrong!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        });
    }
  };

  saveSearchPref = async (searchName, filteredValue) =>
    await saveMasterListSearchPrefs(
      "searchPrefProjectDash",
      searchName,
      filteredValue
    );

  saveSearchPrefAndRestoreValues = async (
    searchName,
    filteredValue,
    savedSearchesList,
    savedSearchesData,
    needAlert
  ) => {
    const savePrefResult = await this.saveSearchPref(searchName, filteredValue);
    if (savePrefResult.status === 200) {
      if (!savePrefResult.data.data.nModified) {
        savedSearchesList.push({
          label: searchName,
          value: savePrefResult.data.data.upserted[0]._id
        });
        savedSearchesData.push({
          _id: savePrefResult.data.data.upserted[0]._id,
          searchName,
          searchPreference: JSON.stringify(filteredValue)
        });
        needAlert &&
          toast("Preference saved successfully!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          });
      } else
        needAlert &&
          toast("Preference updated successfully!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          });
      savedSearchesData = savedSearchesData.map(item => {
        if (item.searchName === searchName) {
          item.searchPreference = JSON.stringify(filteredValue);
        }
        this.setState({
          savedSearchesList,
          savedSearches: {
            label: searchName,
            value: item._id
          },
          savedSearchesData,
          searchName
        });
        return item;
      });
    } else
      toast("Something went wrong!", {
        autoClose: 2000,
        type: toast.TYPE.ERROR
      })``;
    this.setState({
      isSaving: false
    });
    return savedSearchesData;
  };

  getFilteredData = async (page, sorted, pageSize, groupTitle) => {
    if (
      this.state.getDataTime === undefined ||
      Date.now() - this.state.getDataTime > 500
    ) {
      this.setState({ loading: true });
      let result = {};
      const listType = this.props.listType;
      const payload = this.constructPayload(
        listType,
        page,
        sorted,
        pageSize,
        groupTitle
      );
      result = await getProjectsList(payload);
      const data = await result.data;
      this.setState({
        transactionList: data,
        loading: false,
        getDataTime: Date.now()
      });
    }
  };

  constructPayload = (listType, page, sorted, pageSize, groupTitle) => {
    let { filteredValue, groups } = this.state;

    const sortedFields = {};
    if (sorted && sorted[0]) {
      sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1;
    } else {
      sortedFields.tranClientName = 1;
    }

    let filters = {
      freeTextSearchTerm: filteredValue.tranSearch,
      tranCreatePeriod: filteredValue.createdPeriod,
      tranPricingPeriod: filteredValue.pricingPeriod,
      tranStatus:
        filteredValue.tranStatus.indexOf("Select All") > -1
          ? []
          : filteredValue.tranStatus,
      serverPerformPagination: true,
      groups: {
        ...groups
      }
    };

    if (groupTitle) {
      groupTitle = groupTitle.toLowerCase().replace("-data", "");
      // groupTitle = groupTitle.indexOf("ma request") > -1 ? "marfps" : groupTitle
      filters.groups[groupTitle] = {
        currentPage: page || 0,
        size: Number(pageSize),
        sortFields: sortedFields
      };

      groups[groupTitle] = {
        currentPage: page || 0,
        size: Number(pageSize),
        sortFields: sortedFields
      };

      this.setState({ groups });
    }

    return filters;
  };

  onChangeFilter = async event => {
    let { filteredValue, groups } = this.state;
    const { name, value } = event.target;

    if (typeof value === "string") {
      filteredValue[name] = value;
    } else if (Array.isArray(value)) {
      filteredValue[name] =
        value.indexOf("Select All") === 0
          ? value.slice(1)
          : value.indexOf("Select All") > 0
          ? []
          : value;
    }
    filteredValue.defaultSearch = !value;

    if (name === "pageSizePref") {
      groups.transactions.size = Number(value);
      groups.marfps.size = Number(value);
      groups.projects.size = Number(value);
    } else {
      groups.transactions.currentPage = 0;
      groups.marfps.currentPage = 0;
      groups.projects.currentPage = 0;
    }

    this.setState(
      prevState => ({
        ...prevState,
        loading: true,
        filteredValue,
        searchName: "",
        savedSearches: "",
        pageSize: filteredValue.pageSizePref || 25,
        search: filteredValue.tranSearch,
        groups
      }),
      async () => {
        const valueReturned = await this.getFilteredData(
          0,
          null,
          filteredValue.pageSizePref
        );

        this.setState({
          filteredValue,
          search: name === "tranSearch" ? value : "",
          loading: false
        });
      }
    );
  };

  onSearchPrefChange = async event => {
    const { name, value } = event.target;
    const { filteredValue, savedSearchesData } = this.state;

    const findValue = value.label || value;
    const isExists = savedSearchesData.find(
      search => search.searchName === findValue
    );

    if (isExists && Object.keys(isExists).length) {
      const { searchPreference } = isExists;
      filteredValue.defaultSearch = JSON.parse(searchPreference).defaultSearch;
    } else {
      filteredValue.defaultSearch = false;
    }

    if (name === "searchName" || name === "savedSearches") {
      this.setState({
        [name]: value
      });
    } else if (name === "defaultSearch") {
      filteredValue[name] = event.target.checked;
      this.setState({
        filteredValue
      });
    } else {
      filteredValue[name] = value;
      this.setState({
        filteredValue
      });
    }
    if (name === "savedSearches") {
      this.handleGetPref(value);
    }
  };

  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault();
    let { expanded } = this.state;
    Object.keys(expanded).map(item => {
      if (item === val) {
        expanded[item] = !expanded[item];
      }
    });
    this.setState({
      expanded
    });
  };

  toggleResultSet = val => {
    let { showResult } = this.state;
    showResult[val] = !showResult[val];
    this.setState(showResult);
  };

  renderSearchInput = () => (
    <span>
      <p className="multiExpLbl is-capitalized">Search Projects</p>
      <p className="control has-icons-left">
        <DebounceInput
          className="input is-fullwidth is-link is-small"
          placeholder="Search"
          name="tranSearch"
          debounceTimeout={1000}
          value={this.state.filteredValue.tranSearch}
          onChange={this.onChangeFilter}
        />
        <span className="icon is-small is-left has-background-black has-text-white">
          <i className="fas fa-search" />
        </span>
      </p>
    </span>
  );

  renderSelectStatus = () => (
    <span>
      <p className="multiExpLbl is-capitalized">Select Statuses</p>
      <div className="select is-link is-fullwidth is-small tp-select-state" style={{height : "100%"}}>
        <Multiselect
          filter
          placeholder="Select Status"
          value={this.state.filteredValue.tranStatus}
          name="tranStatus"
          data={this.state.tranStatuses}
          message="Select Transaction Status"
          textField="state"
          onChange={val => {
            const event = {
              target: {
                name: "tranStatus",
                value: val
              }
            };
            this.onChangeFilter(event);
          }}
        />
      </div>
    </span>
  );

  renderCreatedPeriod = () => (
    <span>
      <p className="multiExpLbl is-capitalized">Select Created Period</p>
      <div className="select is-link is-fullwidth is-small tp-select-state">
        <DropdownList
          placeholder="Select Created Period"
          value={
            this.state.periodData.filter(
              pd => pd.value === this.state.filteredValue.createdPeriod
            )[0].key
          }
          name="createdPeriod"
          data={this.state.periodData.length > 0 && this.state.periodData}
          message="Select time period"
          textField="key"
          onChange={val => {
            const event = {
              target: {
                name: "createdPeriod",
                value: val.value
              }
            };
            this.onChangeFilter(event);
          }}
        />
      </div>
    </span>
  );

  renderPricingPeriod = () => (
    <span>
      <p className="multiExpLbl is-capitalized">Select Pricing Period</p>
      <div className="select is-link is-fullwidth is-small tp-select-state">
        <DropdownList
          placeholder="Select Pricing Period"
          value={
            this.state.periodData.filter(
              pd => pd.value === this.state.filteredValue.pricingPeriod
            )[0].key
          }
          name="pricingPeriod"
          data={this.state.periodData.length > 0 && this.state.periodData}
          message="Select time period"
          textField="key"
          onChange={val => {
            const event = {
              target: {
                name: "pricingPeriod",
                value: val.value
              }
            };
            this.onChangeFilter(event);
          }}
        />
      </div>
    </span>
  );

  renderSaveSearchElements = () => (
    <div className="columns">
      <Modal
        closeModal={this.toggleModal}
        modalState={this.state.modalState}
        message={this.state.modalMessage}
        onConfirmed={() => this.handleSavePref(true)}
      />
      <div className="column">
        <p className="multiExpLbl is-capitalized">Name and Save Search</p>
        <div className="field has-addons  is-fullwidth">
          <div className="control w-100">
            <input
              className="input  is-link is-fullwidth is-small"
              type="text"
              name="searchName"
              value={this.state.searchName}
              placeholder="Name your search"
              onChange={event => this.onSearchPrefChange(event)}
              title="Save this search criteria"
            />
          </div>
        </div>
      </div>
      <div className="column is-narrow">
        <div className="control">
          <p className="multiExpLbl is-capitalized">Default</p>
          <input
            className="checkbox is-link"
            type="checkbox"
            name="defaultSearch"
            disabled={!this.state.searchName}
            checked={this.state.filteredValue.defaultSearch}
            onChange={event => this.onSearchPrefChange(event)}
            title="Make this a default search"
          />
        </div>
      </div>
      <div className="column is-narrow">
        <div className="control">
          <p className="multiExpLbl is-capitalized">&nbsp;&nbsp;</p>
          <button
            className="button is-link is-small"
            disabled={this.state.isSaving}
            onClick={e => this.handleSavePref(false)}
          >
            {this.state.isSaving ? "Saving..." : "Save"}
          </button>
        </div>
      </div>
    </div>
  );

  renderSelectSavedSearches() {
    return (
      <span>
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl is-capitalized">Saved Searches</p>
            <div className="field has-addons">
              <div className="control w-100">
                <div
                  className="select is-link is-fullwidth is-small third-party-select-hack"
                  style={{ height: "1.8rem" }}
                >
                  <DropdownList
                    filter
                    value={this.state.savedSearches}
                    data={
                      this.state.savedSearchesList
                        ? this.state.savedSearchesList
                        : []
                    }
                    valueField="value"
                    textField="label"
                    defaultValue={0}
                    busy={this.state.loadingPickLists}
                    busySpinner={
                      <span className="fas fa-sync fa-spin is-link" />
                    }
                    onChange={val => {
                      const event = {
                        target: {
                          name: "savedSearches",
                          value: val
                        }
                      };
                      this.onSearchPrefChange(event);
                    }}
                  />
                </div>
              </div>
              <div className="control">
                <button
                  className="button is-dark is-fullwidth is-small"
                  onClick={e => this.handleDeletePref()}
                  title="Delete selected search"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
          <div className="column is-`3`">
            <p className="multiExpLbl is-capitalized">Page size</p>
            <DropdownList
              filter={false}
              data={this.state.pageSizeOptions}
              value={
                this.state.filteredValue.pageSizePref === ""
                  ? "Select Page Size"
                  : this.state.filteredValue.pageSizePref
              }
              defaultValue={25}
              onChange={val => {
                const event = {
                  target: {
                    name: "pageSizePref",
                    value: val
                  }
                };
                this.onChangeFilter(event);
              }}
            />
          </div>
        </div>
      </span>
    );
  }

  renderAccordion = () => (
    <div>
      {this.renderAccordionHeader()}{" "}
      <div>{this.state.expanded.filter && this.renderAccordionBody()}</div>{" "}
    </div>
  );

  renderAccordionBody = () => (
    <div className="accordion-body" style={{ padding: 20 }}>
      <div className="accordion-content">
        <div className="columns">
          <div className="column">{this.renderCreatedPeriod()}</div>
          <div className="column">{this.renderPricingPeriod()}</div>
          <div className="column" style={{height : "100%"}}>{this.renderSelectStatus()}</div>
        </div>
        <div className="columns">
          <div className="column">{this.renderSearchInput()}</div>
          <div className="column">{this.renderSelectSavedSearches()}</div>
          <div className="column">{this.renderSaveSearchElements()}</div>
        </div>
      </div>
    </div>
  );

  renderAccordionHeader = () => (
    <div className="accordion-header toggle" onClick={event => {
      this.toggleButton(event, "filter");
    }}>
      <p>
        Filter {this.props.title ? this.props.title : "Page"} as
      </p>
      <span>
        <i
          className={
            this.state.expanded.filter
              ? "fas fa-chevron-up"
              : "fas fa-chevron-down"
          }
        />
      </span>
    </div>
  );

  renderFilter = () => (
    <section className="accordions">
      <article
        className={
          this.state.expanded.filter ? "accordion is-active" : "accordion"
        }
      >
        {this.renderAccordion()}
      </article>
    </section>
  );

  xlDownload = () => {
    const { transactionList } = this.state
    const xlTransaction = []
    const xlProjects = []
    const xlMARFP = []

    transactionList && transactionList.allData && transactionList.allData["transactions-data"] &&
      transactionList.allData["transactions-data"].map((item) => {
        const data = {
          "Issuer Name": item.tranClientName || "--",
          "Issue Description": item.activityDescription || "--",
          "Type": `${item.tranType} / ${item.tranSubType}` || "--",
          "Principal Amount ($)": item.tranParAmount || "--",
          "Lead Manager": item.tranAssigneeDetails && item.tranAssigneeDetails.userFullName || "--",
          "Coupon Type": item.middleName || "--",
          "Purpose / Sector": item.tranPrimarySector || "--",
          "Expected Pricing Date": dateFormat(item.tranPricingDate || "") || "--",
          "Expected Closing Date": dateFormat(item.tranActualAwardDate || "") || "--",
          "Status": item.tranStatus || "--",
        }
        xlTransaction.push(data)
      })

    transactionList && transactionList.allData && transactionList.allData["projects-data"] &&
      transactionList.allData["projects-data"].map((item) => {
        const data = {
          "Issuer Name": item.tranClientName || "--",
          "Issue Description": item.activityDescription || "--",
          "Type": `${item.tranType} / ${item.tranSubType}` || "--",
          "Principal Amount ($)": item.tranParAmount || "--",
          "Lead Manager": item.tranAssigneeDetails && item.tranAssigneeDetails.userFullName || "--",
          "Coupon Type": item.middleName || "--",
          "Purpose / Sector": item.tranPrimarySector || "--",
          "Expected Pricing Date": dateFormat(item.tranPricingDate || "") || "--",
          "Expected Closing Date": dateFormat(item.tranActualAwardDate || "") || "--",
          "Status": item.tranStatus || "--",
        }
        xlProjects.push(data)
      })

    transactionList && transactionList.allData && transactionList.allData["marfps-data"] &&
      transactionList.allData["marfps-data"].map((item) => {
        const data = {
          "Issuer Name": item.tranClientName || "--",
          "Issue Description": item.activityDescription || "--",
          "Type": `${item.tranType} / ${item.tranSubType}` || "--",
          "Principal Amount ($)": item.tranParAmount || "--",
          "Lead Manager": item.tranAssigneeDetails && item.tranAssigneeDetails.userFullName || "--",
          "Coupon Type": item.middleName || "--",
          "Purpose / Sector": item.tranPrimarySector || "--",
          "Expected Pricing Date": dateFormat(item.tranPricingDate || "") || "--",
          "Expected Closing Date": dateFormat(item.tranActualAwardDate || "") || "--",
          "Status": item.tranStatus || "--",
        }
        xlMARFP.push(data)
      })

    const jsonSheets = [{
      name: "Transactions",
      headers: ["Issuer Name", "Issue Description", "Type", "Principal Amount ($)", "Lead Manager", "Coupon Type", "Purpose / Sector", "Expected Pricing Date", "Expected Closing Date", "Status"],
      data: xlTransaction,
    },
    {
      name: "Projects",
      headers: ["Issuer Name", "Issue Description", "Type", "Principal Amount ($)", "Lead Manager", "Coupon Type", "Purpose / Sector", "Expected Pricing Date", "Expected Closing Date", "Status"],
      data: xlProjects,
    },
    {
      name: "MA-RFP",
      headers: ["Issuer Name", "Issue Description", "Type", "Principal Amount ($)", "Lead Manager", "Coupon Type", "Purpose / Sector", "Expected Pricing Date", "Expected Closing Date", "Status"],
      data: xlMARFP,
    }
    ]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  render() {
    const { transactionList, showResult, groups } = this.state;
    const { allData, urls } = transactionList;
    let isDisabledDownload = (transactionList && transactionList.allData && transactionList.allData["transactions-data"] && transactionList.allData["transactions-data"].length) ||
                             (transactionList && transactionList.allData && transactionList.allData["projects-data"] && transactionList.allData["projects-data"].length) ||
                             (transactionList && transactionList.allData && transactionList.allData["marfps-data"] && transactionList.allData["marfps-data"].length)
    return (
      <div>
        <Nav />
        <div id="main">
          {this.state.loading ? <Loader /> : ""}
          <span>
            {
              <div className="columns">
                <div className="column">
                  <div className="field is-grouped">
                    <div className={`${!isDisabledDownload ? "isDisabled control" : "control"}`} onClick={this.xlDownload}>
                      <span className="has-link"><i className="far fa-2x fa-file-excel has-text-link"/></span>
                    </div>
                    <div style={{ display: "none" }}>
                      <ExcelSaverMulti label="Projects" startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
                    </div>
                  </div>
                </div>
              </div>
            }
            {this.renderFilter()}
            {Object.keys(allData).length > 0 &&
              Object.keys(allData).map((item, idx) =>
                item.indexOf("data") > -1 ? (
                  <div className="accordions" key={"groups" + idx}>
                    <div
                      className={`${
                        showResult[idx] ? "accordion is-active" : "accordion"
                      }`}
                    >
                      {item !== "" && item !== null ? (
                        <div
                          className="accordion-header"
                          onClick={e => this.toggleResultSet(idx)}
                        >
                          <span>
                            {item === "marfps-data"
                              ? "MA Request For Proposals"
                              : item.split("-")[0]}
                          </span>
                          <div>
                            (
                            {allData[item.replace("data", "meta")] &&
                            allData[item.replace("data", "meta")][0]
                              ? allData[item.replace("data", "meta")][0].total
                              : 0}{" "}
                            found) &nbsp;
                            <i
                              className={`${
                                showResult[idx]
                                  ? "fas fa-chevron-up"
                                  : "fas fa-chevron-down"
                              }`}
                            />
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                      {showResult[idx] && (
                        <EntityPageGrid
                          key={idx}
                          search={this.state.filteredValue.tranSearch}
                          entityList={allData[item]}
                          loading={this.state.loading}
                          listType={"project"}
                          auth={this.props.auth}
                          getFilteredData={this.getFilteredData}
                          routeType={this.props.routeType}
                          total={
                            allData[item.replace("data", "meta")] &&
                            allData[item.replace("data", "meta")][0] &&
                            allData[item.replace("data", "meta")][0].total
                          }
                          pages={
                            allData[item.replace("data", "meta")] &&
                            allData[item.replace("data", "meta")][0] &&
                            allData[item.replace("data", "meta")][0].pages
                          }
                          page={groups[item.split("-")[0]].currentPage}
                          pageSize={groups[item.split("-")[0]].size}
                          groupTitle={item}
                          urls={urls}
                          showResultSet={idx === 0}
                        />
                      )}
                    </div>
                  </div>
                ) : null
              )}
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { auth } = state;
  return { auth };
};

const mapDispatchToProps = {};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProjectDashboard)
);
