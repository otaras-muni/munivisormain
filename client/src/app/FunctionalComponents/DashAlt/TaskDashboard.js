import React, { Component } from "react"
import CommonTasksDashboard from "./CommonTasksDashboard"
import Nav from "./Nav"

class TaskDashboard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tabActiveIndex: this.props.activeIndex || 0,
      TABS: [
        { label: "Transaction Tasks", row: 1, Component: CommonTasksDashboard, type: "tasks" },
        { label: "Compliance/Others Tasks", row: 1, Component: CommonTasksDashboard, type: "compliance" },
      ],
      type: "tasks"
    }
  }

  componentWillMount() {
    const { TABS } = this.state
    const { activeIndex } = this.props
    const { Component } = TABS[activeIndex || 0]
    this.setState({
      TABS,
      tabActiveIndex: activeIndex || 0,
      SelectedComponent: Component
    })
  }

  identifyComponentToRender = (tab, ind) => {
    this.setState({ SelectedComponent: tab.Component, tabActiveIndex: ind, type: tab.type })
  }

  renderTabContents() {
    const { TABS, tabActiveIndex } = this.state
    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        TABS.map((t, i) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(t, i)}>
            <a className="tabSecLevel">
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedTabComponent() {
    const { SelectedComponent, type } = this.state
    return <SelectedComponent {...this.props} type={type} />
  }

  render() {

    return (
      <React.Fragment>
        <Nav />
        <div className="plr-20">
          <div className="tabs">
            {this.renderTabContents()}
          </div>
          {this.renderSelectedTabComponent()}
        </div>
      </React.Fragment>
    )
  }
}

export default TaskDashboard

