import React, { Component } from "react"
import { AgGridReact } from "ag-grid-react"
import "ag-grid-community/dist/styles/ag-grid.css"
import "ag-grid-community/dist/styles/ag-theme-balham.css"
import "ag-grid-enterprise"
import { tranColumns } from "../../GlobalComponents/GridColumns"

class ProjectAgeGrid extends Component {
  constructor(props) {
    super(props)
    const columnDefs = tranColumns(props.search || "", props.changeSelectedAction, props.user)
    this.state = {
      gridOptions: {
        columnDefs,
        rowHeight: 40,
        headerHeight: 40,
        width: `${100/columnDefs.length}%`,
        defaultColDef: {
          sortable: true,
          resizable: true,
          filter: true
        },
        getRowHeight: (params) => 28 * (Math.floor(params.data.latinText.length / 60) + 1),
      },
      pageInput: props.page+1,
      changedPage: 0,
    }
  }

  componentDidUpdate(prevProps){
    const { search, changeSelectedAction, user} = this.props
    if (search !== prevProps.search) {
      const columnDefs = tranColumns(search || "", changeSelectedAction, user)
      this.setState(prevState => ({
        gridOptions: {
          ...prevState.gridOptions,
          columnDefs
        },
      }))
    }
  }

  onGridReady = params => {
    this.gridApi = params.api
    this.gridColumnApi = params.columnApi
    const sorted = params.api.getSortModel()
    sorted.forEach(colSort => {
      colSort.id = colSort.colId
      colSort.desc = colSort.sort === "desc"
    })
    const activeEvent = {
      sorted,
      changedPage: this.state.changedPage
    }
    this.setState({
      ...activeEvent
    },() => this.props.onAgeGridOperations(activeEvent))
  }

  onSortChanged = () => {
    if(this.gridApi){
      const sorted = this.gridApi.getSortModel()
      sorted.forEach(colSort => {
        colSort.id = colSort.colId
        colSort.desc = colSort.sort === "desc"
      })
      const activeEvent = {
        sorted,
        changedPage: this.state.changedPage
      }
      this.setState({
        ...activeEvent
      },() => this.props.onAgeGridPageChanged(activeEvent))
    }
  }

  onPaginationChanged = (params, customPageChange) => {
    const { page, pages } = this.props
    const { pageInput, changedPage } = this.state

    if(customPageChange && (changedPage === page+1 || changedPage < 1 || changedPage > pages)) return

    if (this.gridApi){
      const sorted = this.gridApi.getSortModel()
      sorted.forEach(colSort => {
        colSort.id = colSort.colId
        colSort.desc = colSort.sort === "desc"
      })
      const activeEvent = {
        sorted,
        changedPage: customPageChange ? pageInput-1 : this.state.changedPage,
        pageInput: customPageChange ? pageInput : changedPage + 1
      }
      this.setState({
        ...activeEvent
      },() => this.props.onAgeGridPageChanged(activeEvent))
    }

  }

  onChange = (event) => {
    const { pages } = this.props
    const directPage = {}
    const changedPage = parseInt(event.target.value,10)

    directPage.pageInput = changedPage
    if(changedPage < 1 || changedPage > pages) return

    this.setState({
      ...directPage
    })
  }

  onBtNext = async () => {
    this.setState({
      changedPage: this.props.page + 1,
    },() => this.gridApi.paginationGoToNextPage())
  }

  onBtPrevious = () => {
    this.setState({
      changedPage: this.props.page - 1
    },() => this.gridApi.paginationGoToNextPage())
  }

  render() {
    const { pages, page, pageSize, list } = this.props
    const { gridOptions, pageInput} = this.state

    return (
      <div>
        <div
          className="ag-theme-balham"
          style={{
            height: `${(list.length ? 40*list.length : 40)+60}px`,
            width: "100%"
          }}
        >
          <AgGridReact
            {...gridOptions}
            pagination
            suppressPaginationPanel
            paginationPageSize={pageSize}
            onPaginationChanged={this.onPaginationChanged}
            onSortChanged={this.onSortChanged}
            rowData={list}
            onGridReady={this.onGridReady}
            getRowHeight={this.state.getRowHeight}
          />
        </div>
        <div className="columns margin-topTen">
          <div className="column" />
          <div className="column" style={{textAlign: "center"}}>
            <button
              type="button"
              title="Previous"
              className="button is-light"
              onClick={this.onBtPrevious}
              disabled={!page}
            >
              Previous
            </button>
            <p style={{display: "inline-block", margin: 5}}>
              Page
              <input className="input is-small is-link d-inline form-control"
                style={{ maxWidth: 50, marginLeft: 5, marginRight: 5 }}
                name="pageInput"
                type="number"
                min="0"
                max="pages"
                value={pageInput}
                onChange={this.onChange}
                onBlur={() => this.onPaginationChanged({}, true)}
              /> of {pages}
            </p>
            <button
              type="button"
              title="Next"
              className="button is-light"
              onClick={this.onBtNext}
              disabled={page+1 === pages}
            >
              Next
            </button>
          </div>
          <div className="column" />
        </div>
      </div>
    )
  }
}

export default ProjectAgeGrid
