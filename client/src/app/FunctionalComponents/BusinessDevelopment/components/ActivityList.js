import React from "react"
import moment from "moment"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Accordion from "../../../GlobalComponents/Accordion"

const ActivityList = ({ nav2, item, header, onEditSection, addNewTask, addSection, activityDetails }) => (
  <Accordion multiple
    activeItem={[0]}
    boxHidden
    render={({ activeAccordions, onAccordion }) =>
      <RatingSection onAccordion={() => onAccordion(0)} title={header || ""}>
        {activeAccordions.includes(0) &&
          <div className="accordion-body">
            <div className="accordion-content" style={{ padding: 0 }}>
              {item && Array.isArray(item) && item.map((sections, index) => (
                <div className="columns" key={index.toString()}>
                  <div className="column">
                    <div className="without-border-withul">
                      <ul>
                        <li>
                          <b>{sections.taskCreatedBy.userFirstName || ""} {sections.taskCreatedBy.userLastName || ""}&nbsp;</b>
                      Created: {sections.taskDetails.taskDescription || ""}, Due: {(sections.taskDetails.taskEndDate && moment(sections.taskDetails.taskEndDate).format("MM-DD-YYYY")) || ""}&nbsp;&nbsp;
                          <span className="has-text-link">
                            <a className="fas fa-pencil-alt" onClick={() => onEditSection(sections)} />
                          </span>
                    
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              ))}
              <div className="columns float-padding-rb">
                <div className="control">
                  <a className="button is-link is-small" onClick={() => addNewTask((activityDetails && activityDetails[header]) || addSection || {})}>TASK DETAILS</a>
                </div>
              </div>
            </div>
          </div>
        }
      </RatingSection>
    }
  />

)

export default ActivityList
