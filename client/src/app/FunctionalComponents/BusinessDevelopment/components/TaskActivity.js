import React from "react"
import moment from "moment"
import {
  MultiSelect,
  SelectLabelInput,
  TextLabelInput
} from "../../../GlobalComponents/TextViewBox"
import {ContextType} from "../../../../globalutilities/consts"
import DocumentPage from "../../../GlobalComponents/DocumentPage"
import { checkEmptyElObject } from "GlobalUtils/helpers"
import DropDownListClear from "../../../GlobalComponents/DropDownListClear"

const TaskActivity = ({dropDown, disabled, onChangeItem, errors, category, item, onSave, fetchContactsAndAssignee, onCancel, taskHistory, isSaveDisabled,
  onDeleteDoc, onStatusChange, onDocSave, tags, onUploadDocuments, documentsUpload, activity}) => {
  const onChange = (e) => {
    onChangeItem({
      ...item,
      taskDetails: {
        ...item.taskDetails,
        [e.target.name]: e.target.value
      }
    }, category)
  }

  const onIssuerChange = (key, selectItem) => {
    let stateObject = {}
    if(key === "taskAssigneeUserDetails") {
      stateObject = {
        ...item,
        taskAssigneeUserDetails: {
          ...item.taskAssigneeUserDetails,
          userId: Object.keys(selectItem || {}).length ? selectItem.id : "",
          userFirstName: Object.keys(selectItem || {}).length ? selectItem.userFirstName : "",
          userLastName: Object.keys(selectItem || {}).length ? selectItem.userLastName : "",
          userEntityId: Object.keys(selectItem || {}).length ? selectItem.entityId : "",
          userEntityName: Object.keys(selectItem || {}).length ? selectItem.userFirmName : "",
        },
        taskDetails: {
          ...item.taskDetails,
          taskAssigneeUserId: Object.keys(selectItem || {}).length ? selectItem.id : "",
          taskAssigneeName: Object.keys(selectItem || {}).length ? selectItem.name : "",
        }
      }
    }else if(key === "entityName") {
      stateObject = {
        ...item,
        relatedEntityDetails: {
          ...item.relatedEntityDetails,
          entityId: selectItem.id,
          entityName: selectItem.name,
        },
        relatedContacts: []
      }
      fetchContactsAndAssignee(selectItem.id, "contacts")
    } else {
      stateObject = {
        ...item,
        [key]: selectItem.map(select =>({
          id: select.id,
          name: select.name,
          userId: select.userId || select.id,
          userName: select.userName || select.name,
          entityId: select.entityId,
          entityName: select.entityName || select.userFirmName,
        })),
      }
    }
    onChangeItem(stateObject, category)
  }

  const staticField = {
    docCategory: "Business Development",
    docSubCategory: "Task",
  }

  return(
    <div className="column box">
      <div style={{textAlign:'center'}}>
        <b className="is-size-4">Task Detail / Activity</b>
        <p className="is-size-12">{activity.actIssuerClientEntityName || activity.activityIssuerClientName ? `${activity.actProjectName || activity.activityProjectName || ''} (${activity.actIssuerClientEntityName || activity.activityIssuerClientName || '' })` : ""} </p>
      </div>
      <hr/>
      <div className="columns">
        <div className="column">
          <p className="multiExpLblBlk">Task Type
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <SelectLabelInput list={dropDown.taskType} name="taskType" onChange={onChange} disabled={disabled} value={(item.taskDetails && item.taskDetails.taskType) || ""} error={errors.taskType || ""}/>
        </div>
        <div className="column">
          <p className="multiExpLblBlk">Task Status
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <SelectLabelInput list={dropDown.taskStatus} name="taskStatus" onChange={onChange} disabled={disabled} value={(item.taskDetails && item.taskDetails.taskStatus) || ""} error={errors.taskStatus || ""}/>
        </div>
        <div className="column margin-topTwenty">
          <a className="button is-small is-link" href="/addnew-prospect" target="_new">Add New Prospect</a>
        </div>
        <div className="column margin-topTwenty">
          <a className="button is-small is-link" href="/addnew-contact" target="new">Add New Contact</a>
        </div>
      </div>

      <div className="columns">
        {/* <div className="column">
          <p className="multiExpLblBlk">Related Entity</p>
          <DropDownInput filter groupBy={({relType}) => relType} value={(item.relatedEntityDetails && item.relatedEntityDetails.entityName) || []} data={dropDown.issuerList || []}
            onChange={(issuer) => onIssuerChange("entityName", issuer)} style={{maxWidth: "unset"}} error={errors.entityName || ""}/>
        </div> */}
        <TextLabelInput name="entityName" label="Related Entity" value={(item.relatedEntityDetails && item.relatedEntityDetails.entityName) || ""} disabled />
        <div className="column">
          <p className="multiExpLblBlk">Related Users</p>
          <MultiSelect filter data={dropDown.contacts || []} value={(item && item.relatedContacts) || []} name="relatedContacts" placeholder="Contacts"
            onChange={(item) => onIssuerChange("relatedContacts", item)} style={{maxWidth: "unset",pointerEvents: disabled ? "none" : "unset"}} error={errors.relatedContacts || ""} />
        </div>
      </div>

      <div className="columns">
        <div className="column">
          <p className="multiExpLblBlk">Task name
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <TextLabelInput placeholder="Task Name" error={errors.taskNotes || ""} disabled={disabled} style={{width: "100%"}} value={(item.taskDetails && item.taskDetails.taskNotes) || ""} name="taskNotes" onChange={onChange} />
        </div>
        <div className="column">
          <p className="multiExpLblBlk">Assign To
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          {/* <DropDownInput filter value={(item.taskAssigneeUserDetails && `${item.taskAssigneeUserDetails.userFirstName} ${item.taskAssigneeUserDetails.userLastName}`) || []} data={dropDown.assignee || []}
            onChange={(user) => onIssuerChange("taskAssigneeUserDetails", user)} disabled={disabled} style={{maxWidth: "unset"}} error={errors.userEntityName || ""}/> */}
          {
            !disabled ?
              <div>
                <DropDownListClear
                  filter
                  value={(item.taskAssigneeUserDetails && `${item.taskAssigneeUserDetails.userFirstName} ${item.taskAssigneeUserDetails.userLastName}`) || []}
                  data={dropDown.assignee || []}
                  textField="name"
                  onChange={(user) => onIssuerChange("taskAssigneeUserDetails", user)}
                  onClear={() => onIssuerChange("taskAssigneeUserDetails", {})}
                  isHideButton={!checkEmptyElObject(item.taskAssigneeUserDetails)}
                  style={{maxWidth: "unset"}}
                  // disabled={disabled}
                  // error={errors.userEntityName || ""}
                />
                {errors.userEntityName && (
                  <p className="text-error">{errors && errors.userEntityName || ""}</p>
                )}
              </div>
              : <small>{(item.taskAssigneeUserDetails && `${item.taskAssigneeUserDetails.userFirstName} ${item.taskAssigneeUserDetails.userLastName}`) || ""}</small>
          }
        </div>
      </div>

      <div className="columns">
        <div className="column">
          <p className="multiExpLblBlk">Start Date
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <TextLabelInput
            rror={errors.taskStartDate || ""}
            type="date"
            name="taskStartDate"
            value={!(item.taskDetails && item.taskDetails.taskStartDate) ? null : new Date(item.taskDetails.taskStartDate)}
            disabled={disabled}
            onChange={onChange}
          />
        </div>
        <div className="column">
          <p className="multiExpLblBlk">End Date
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <TextLabelInput
            error={errors.taskEndDate ? "Required (should be greater than or equal to start date)" : ""}
            type="date"
            name="taskEndDate"
            value={!(item.taskDetails && item.taskDetails.taskEndDate) ? null : new Date(item.taskDetails.taskEndDate)}
            disabled={disabled}
            onChange={onChange}
          />
        </div>
      </div>

      {item._id && taskHistory && Array.isArray(taskHistory) ?
        <div className="border-withul">
          {
            taskHistory.map((history, i) => (
              <div className="columns" key={i}>
                <div className="column is-full">
                  <ul>
                    <li>{history.userFirstName} {history.userLastName} {taskHistory.length-1 === i ? "Created" : "Updated" } task&nbsp;
                      {(history.createdAt && moment(history.createdAt).format("MM-DD-YYYY h:mm A")) || ""}
                      <p className="multiExpLblBlk" style={{paddingLeft:"20px"}}>{history.taskDescription}</p>
                    </li>
                  </ul>
                </div>
              </div>
            ))}
        </div> : null }

      <div className="columns">
        <div className="column is-full">
          <br/>
          <p>Write a comment/update
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <textarea className="textarea is-link" placeholder="" name="taskDescription" disabled={disabled} value={item.taskDetails && item.taskDetails.taskDescription || ""} onChange={onChange}/>
          {errors.taskDescription && <p className="is-small has-text-danger" style={{fontSize: 12}}>{errors.taskDescription}</p>}
        </div>
      </div>

      {item._id  ?
        <div className="columns">
          <div className="column">
            <button className="button is-link is-small" onClick={onUploadDocuments}>{documentsUpload ? "Hide" : "Show"} Documents </button>
          </div>
        </div> : null }

      {documentsUpload  ?
        <div className="columns">
          <div className="column is-full">
            <DocumentPage onSave={onDocSave} nav1="bus-development" isDisabled={disabled} tranId={item._id} title="Task Documents" pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickAction="LKUPDOCACTION"
              category="taskRelatedDocuments" staticField={staticField} tags={tags} documents={item.taskRelatedDocuments || []} contextType={ContextType.busDev}  onStatusChange={onStatusChange} onDeleteDoc={onDeleteDoc}/>
          </div>
        </div> :  null}

      {
        !disabled ?
          <div className="columns">
            <div className="column">
              <button className="button is-link" onClick={onSave} disabled={isSaveDisabled || false}>Save</button>&nbsp;&nbsp;
              <button className="button is-light" onClick={onCancel}>Cancel</button>
            </div>
          </div> : null
      }
    </div>
  )
}

export default TaskActivity
