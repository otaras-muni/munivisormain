import Joi from "joi-browser"
import dateFormat from "dateformat"

const bankLoanSummarySchema = minDate =>
  Joi.object().keys({
    actTranStatus: Joi.string()
      .allow("")
      .optional(),
    actTranType: Joi.string()
      .required()
      .optional(),
    actTenorMaturities: Joi.string()
      .allow("")
      .optional(),
    actTranBorrowerName: Joi.string()
      .allow("")
      .optional(),
    actTranObligorName: Joi.string()
      .allow("")
      .optional(),
    actTranSecurityType: Joi.string()
      .allow("")
      .optional(),
    actTranState: Joi.string()
      .required()
      .optional(),
    actTranCounty: Joi.string()
      .allow("")
      .optional(),
    actTranClosingDate: Joi.date()
      .example(new Date("2016-01-01"))
      .min(dateFormat(minDate || new Date(), "yyyy-mm-dd"))
      .required(),
    actTranUseOfProceeds: Joi.string()
      .allow("")
      .optional(),
    actTranEstimatedRev: Joi.number()
      .allow(null)
      .min(0)
      .required(),
    _id: Joi.string()
      .required()
      .optional()
  })

const bankLoanTermsSchema = (minDate, closingDate) =>
  Joi.object().keys({
    parAmount: Joi.number().min(0).required(),
    fedTax: Joi.string()
      .allow("")
      .optional(),
    stateTax: Joi.string()
      .allow("")
      .optional(),
    bankQualified: Joi.string()
      .allow("")
      .optional(),
    paymentType: Joi.string()
      .allow("")
      .optional(),
    prinPaymentFreq: Joi.string()
      .allow("")
      .optional(),
    prinPaymentStartDate: Joi.date()
      .example(new Date("2016-01-01"))
      .max(dateFormat(closingDate, "yyyy-mm-dd"))
      .allow(null)
      .optional(),
    prinPaymentDay: Joi.number()
      .allow(null)
      .optional(),
    intPaymentFreq: Joi.string()
      .allow("")
      .optional(),
    intPaymentStartDate: Joi.date()
      .example(new Date("2016-01-01"))
      .max(dateFormat(closingDate, "yyyy-mm-dd"))
      .allow(null)
      .optional(),
    intPaymentDay: Joi.number()
      .allow(null)
      .optional(),
    fixedRate: Joi.number()
      .allow(null)
      .min(0)
      .max(100)
      .optional(),
    floatingRateRef: Joi.string()
      .allow("")
      .optional(),
    floatingRateRatio: Joi.number()
      .max(100)
      .allow(null)
      .optional(),
    floatingRateSpread: Joi.number()
      .max(100)
      .allow(null)
      .optional(),
    dayCount: Joi.string()
      .allow("")
      .optional(),
    busConvention: Joi.string()
      .allow("")
      .optional(),
    bankHolidays: Joi.string()
      .allow("")
      .optional(),
    prePaymentTerms: Joi.string()
      .allow("")
      .optional(),
    _id: Joi.string()
      .required()
      .optional()
  })

const loanDetailsSchema = (minDate, closingDate) =>
  Joi.object().keys({
    actTranClientId: Joi.string()
      .optional()
      .required(),
    actTranClientName: Joi.string()
      .optional()
      .required(),
    actTranClientMsrbType: Joi.string()
      .allow("")
      .optional(),
    actTranSecondarySector: Joi.string()
      .allow("")
      .optional(),
    actTranPrimarySector: Joi.string()
      .allow("")
      .optional(),
    actTranIssueName: Joi.string()
      .allow("")
      .optional(),
    actTranProjectDescription: Joi.string()
      .allow(["", null])
      .optional(),
    bankLoanSummary: bankLoanSummarySchema(minDate),
    bankLoanTerms: bankLoanTermsSchema(minDate, closingDate)
  })

export const LoanDetailsValidate = (inputTransDistribute, minDate, closingDate) =>
  Joi.validate(inputTransDistribute, loanDetailsSchema(minDate, closingDate), {
    abortEarly: false,
    stripUnknown: false
  })

const loanCusIpSchema = Joi.object().keys({
  cusip: Joi.string()
    .alphanum()
    .min(6)
    .max(9)
    .required()
    .optional(),
  description: Joi.string()
    .allow("")
    .required()
    .optional(),
  tag: Joi.string()
    .allow("")
    .required()
    .optional(),
  _id: Joi.string()
    .required()
    .optional()
})

export const LoanCUSIPValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, loanCusIpSchema, {
    abortEarly: false,
    stripUnknown: false
  })
