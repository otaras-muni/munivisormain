import Joi from "joi-browser"
import dateFormat from "dateformat"

const amortSchema = (minDate) => Joi.object().keys({
  reductionDate: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required().optional(),
  prinAmountReduction: Joi.number().required().optional(),
  reviedPrinAmount: Joi.number().required().optional(),
  isNew: Joi.boolean().optional(),
  _id: Joi.string().required().optional(),
})

export const AmortValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, amortSchema(minDate), { abortEarly: false, stripUnknown:false })
