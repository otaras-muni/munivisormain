import React from "react"
import { NavLink } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "../../../GlobalComponents/Loader"
import LoanDetails from "./LoanDetails"
import LoanParticipants from "./Participants"
import Summary from "./Summary"
import {
  makeEligibleTabView
} from "GlobalUtils/helpers"
import Documents from "./Documents"
import Amortization from "./Amortization"
import { fetchLoanTransaction } from "../../../StateManagement/actions"
import Audit from "../../../GlobalComponents/Audit"
import CheckNTrack from "./CheckAndTrack"
import CONST, { activeStyle } from "../../../../globalutilities/consts"
import { fetchParticipantsAndOtherUsers } from "../../../StateManagement/actions/Transaction"

const TABS = [
  { path: "summary", label: "Summary" },
  { path: "details", label: "Details" },
  { path: "participants", label: "Participants" },
  { path: "amortization", label: "Amortization" },
  { path: "documents", label: "Documents" },
  { path: "check-track", label: "Check-n-Track" },
  { path: "audit-trail", label: "Activity Log" }
]

class LoanView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs: [],
      isSummary: false,
      loading: true,
      transaction: {},
      tranAction: {}
    }
  }

  componentWillMount() {
    const { user, nav, nav1, nav2, nav3, loginEntity } = this.props
    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])
    if (nav2) {
      fetchLoanTransaction(nav3, nav2, async transaction => {
        if (transaction && transaction.actTranClientId) {
          const isEligible = await makeEligibleTabView(
            user,
            nav2,
            TABS,
            allEligibleNav,
            transaction.actTranClientId,
            transaction.actTranFirmId,
          )

          isEligible.tranAction.canEditTran = CONST.oppDecline.indexOf(transaction.actTranStatus) !== -1 ? false : isEligible.tranAction.canEditTran

          isEligible.tranAction.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.actTranStatus) === -1 && isEligible.tranAction.canEditDocument
          // if(transaction.bankLoanParticipants && loginEntity && loginEntity.relationshipToTenant !== "Self"){
          //   transaction.bankLoanParticipants = transaction.bankLoanParticipants.filter(part => part.partFirmId === loginEntity.entityId)
          // }

          console.log("=================tranAction================", isEligible)
          if (isEligible && isEligible.tranAction && (!isEligible.tranAction.canEditTran && !isEligible.tranAction.canViewTran)) {
            this.props.history.push("/dashboard")
          } else if (isEligible.tranAction.view.indexOf(nav3) === -1) {
            const view = isEligible.tranAction.view[0] || ""
            this.props.history.push(`/${nav1}/${nav2}/${view}`)
          } else {
            const participants = await fetchParticipantsAndOtherUsers(nav2)
            this.setState({
              transaction,
              loading: false,
              participants,
              tranAction: isEligible.tranAction,
              tabs: isEligible.tabs
            })
          }
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  onStatusChange = status => {
    const { tranAction } = this.state
    tranAction.canEditTran =
      CONST.oppDecline.indexOf(status) !== -1 ? false : tranAction.canEditTran
    this.setState({
      tranAction
    })
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderTabs = (tabs, loanId, option) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/loan/${loanId}/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderViewSelection = (loanId, option) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, loanId, option)}</ul>
    </nav>
  )

  renderSelectedView = (loanId, option) => {
    const { transaction, tranAction, participants } = this.state
    switch (option) {
    case "summary":
      return (
        <Summary
          {...this.props}
          tranAction={tranAction}
          transaction={this.state.transaction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
          onStatusChange={this.onStatusChange}
        />
      )
    case "details":
      return (
        <LoanDetails
          {...this.props}
          tranAction={tranAction}
          transaction={this.state.transaction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "participants":
      return (
        <LoanParticipants
          {...this.props}
          tranAction={tranAction}
          transaction={this.state.transaction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
          tranAction={tranAction}
          transaction={this.state.transaction}
          participants={participants}
        />
      )
    case "amortization":
      return (
        <Amortization
          {...this.props}
          tranAction={tranAction}
          transaction={this.state.transaction}
          participants={participants}
        />
      )
    case "check-track":
      return (
        <CheckNTrack
          {...this.props}
          tranAction={tranAction}
          transaction={this.state.transaction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "audit-trail":
      return <Audit {...this.props} />
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { option } = this.props
    const { transaction } = this.state
    const loanId = transaction && transaction._id
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(loanId, option)}
            </div>
          </div>
        </div>
        <section id="main" className="bankloan">
          {this.renderSelectedView(loanId, option)}
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(
  mapStateToProps,
  null
)(LoanView)
