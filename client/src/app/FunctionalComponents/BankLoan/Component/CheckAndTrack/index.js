import React from "react"
import {toast} from "react-toastify"
import ProcessChecklist from "../../../../GlobalComponents/ProcessChecklist"
import {putLoanTransaction} from "../../../../StateManagement/actions"
import Loader from "../../../../GlobalComponents/Loader"
import CONST from "../../../../../globalutilities/consts"
import {SendEmailModal} from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"

class CheckNTrack extends React.Component {
  constructor() {
    super()
    this.state = {
      checkLists: [],
      checklistId: "",
      participants: [],
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
  }

  componentWillMount() {
    const {transaction} = this.props
    const participants = transaction && transaction.bankLoanParticipants ? transaction.bankLoanParticipants.map(part => ({
      _id: part.partContactId, // eslint-disable-line
      name: part.partContactName,
      type: part.partType
    })) : []
    // participants.push({
    //   _id: transaction.actTranFirmLeadAdvisorId, // eslint-disable-line
    //   name: transaction.actTranFirmLeadAdvisorName,
    //   type: "Assginees"
    // })
    this.setState(prevState => ({
      participants,
      checkLists: (transaction && transaction.bankLoanChecklists) || [],
      loading: false,
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actTranIssueName || transaction.actTranProjectDescription} - Notification`
      }
    }))
  }

  saveChecklist = (bankLoanChecklists, checklistId) => {
    this.setState({
      bankLoanChecklists,
      checklistId,
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const {email, bankLoanChecklists, checklistId } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    let url = window.location.pathname.replace("/","")
    if(checklistId) {
      url = `${url}?cid=${checklistId}`
    }
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice:true,
      emailParams: {
        url,
        ...email,
      }
    }
    console.log("==============email send to ==============", emailParams)
    this.setState({
      modalState: false
    }, () => {
      putLoanTransaction(this.props.nav3, this.props.nav2, {bankLoanChecklists}, (res)=> {
        if (res && res.status === 200) {
          toast("Bank loan check-n-track has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            checkLists: (res.data && res.data.bankLoanChecklists) || [],
          }, async () => {
            await sendEmailAlert(emailParams)
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  render() {
    const {modalState, email} = this.state
    const {participants, onParticipantsRefresh, transaction, tranAction} = this.props
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      <div>
        <SendEmailModal modalState={modalState} email={email} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onModalChange={this.onModalChange} onSave={this.onConfirmationSave}/>
        <ProcessChecklist
          checklists={this.state.checkLists}
          totalThresholds={[0.20, 15000]}
          participants={this.state.participants}
          onSaveChecklist={this.saveChecklist}
          tenantId={transaction.actTranFirmId}
          isDisabled={!tranAction.canTranStatusEditDoc}
        />
      </div>
    )
  }
}

export default CheckNTrack
