import React from "react"
import moment from "moment"
import {
  TextLabelInput,
  SelectLabelInput,
  NumberInput
} from "../../../../../GlobalComponents/TextViewBox"
import SearchCountry from "../../../../GoogleAddressForm/GoogleCountryComponent";

const TransSummary = ({
  transaction,
  errorMessages,
  getBorrowerByFirm,
  item,
  dropDown,
  onBlur,
  onChangeItem,
  category,
  tranAction,
  getCountryDetails,
}) => {
  const onChange = event => {
    if (
      event.target.name === "actTranPrimarySector" ||
      event.target.name === "actTranSecondarySector"
    ) {
      const tranSector = {
        ...transaction,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      }
      if (event.target.name === "actTranPrimarySector") {
        tranSector.actTranSecondarySector = ""
      }
      onChangeItem(tranSector, "tempTransaction")
    } else if (
      event.target.name === "actTranIssueName" ||
      event.target.name === "actTranProjectDescription"
    ) {
      onChangeItem(
        {
          ...transaction,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        },
        "tempTransaction"
      )
    } else {
      onChangeItem(
        {
          ...item,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        },
        category
      )
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onSelection = (key, selectItem) => {
    if (key === "actTranBorrowerName") {
      onChangeItem(
        {
          ...item,
          actTranBorrowerName: selectItem.name
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...transaction,
          actTranClientId: selectItem.id,
          actTranClientName: selectItem.firmName,
          actTranClientMsrbType: selectItem.msrbRegistrantType
        },
        "tempTransaction"
      )
      onChangeItem(
        {
          ...item,
          actTranBorrowerName: ""
        },
        category
      )
      getBorrowerByFirm()
    }
  }

  return (
    <div>
      <div className="columns">
        <SearchCountry
          idx={0}
          label="State/City"
          disabled={!tranAction.canEditTran}
          error={errorMessages.actTranCounty || errorMessages.actTranState || ""}
          value={item.actTranCounty && item.actTranState ? `${item.actTranCounty}, ${item.actTranState}` : item.actTranCounty ? `${item.actTranCounty}` : `${item.actTranState}`}
          getCountryDetails={getCountryDetails}
        />
        <div className="column">
          <p className="multiExpLbl">Primary Sector</p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="Primary Sector"
                value={transaction.actTranPrimarySector || ""}
                onChange={onChange}
                name="actTranPrimarySector"
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
                style={{ width: "100%" }}
              >
                <option value="" disabled="">
                  Pick Primary Sector
                </option>
                {dropDown && dropDown.primarySectors && dropDown.primarySectors.map(t => (
                  <option key={t && t.label} value={t && t.label} disabled={!t.included}>
                    {t && t.label}
                  </option>
                ))}
              </select>
              {errorMessages && errorMessages.actTranPrimarySector && (
                <small className="text-error">
                  {errorMessages.actTranPrimarySector || ""}
                </small>
              )}
            </div>
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl">Secondary Sector</p>
          <div className="select is-small is-link" style={{ width: "100%" }}>
            <select
              title="Secondary Sector"
              name="actTranSecondarySector"
              value={transaction.actTranSecondarySector || ""}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={!tranAction.canEditTran}
              style={{ width: "100%" }}
            >
              <option value="" disabled="">
                Pick Secondary Sector
              </option>
              {transaction.actTranPrimarySector &&
              dropDown.secondarySectors[transaction.actTranPrimarySector]
                ? dropDown.secondarySectors[transaction.actTranPrimarySector].map(
                  sector => (
                    <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                      {sector && sector.label}
                    </option>
                  )
                )
                : null}
            </select>
            {errorMessages && errorMessages.actTranSecondarySector && (
              <small className="text-error">
                {errorMessages.actTranSecondarySector || ""}
              </small>
            )}
          </div>
        </div>

        <NumberInput
          className="column is-3"
          label="Estimated Revenue"
          prefix="$"
          required
          disabled={!tranAction.canEditTran}
          error={errorMessages.actTranEstimatedRev ? "Required(must be larger than or equal to 0)" : "" || ""}
          name="actTranEstimatedRev"
          placeholder="$"
          value={item.actTranEstimatedRev}
          onChange={onChange}
          onBlur={onBlurInput}
        />
      </div>

      <div className="columns">
        <div className="column is-3">
          <div className="columns">
            <SelectLabelInput
              label="Transaction Type"
              required
              disabled={!tranAction.canEditTran}
              error={errorMessages.actTranType || ""}
              list={dropDown.tranType}
              name="actTranType"
              value={item.actTranType}
              onChange={onChange}
            />
          </div>
        </div>
        <div className="column is-3">
          <div className="columns">
            <TextLabelInput
              label="Closing Date"
              required
              disabled={!tranAction.canEditTran}
              error={errorMessages.actTranClosingDate ? "Required(must be larger than or equal to today))" : ""}
              name="actTranClosingDate"
              type="date"
              /* value={
                item.actTranClosingDate
                  ? moment(
                    new Date(item.actTranClosingDate)
                      .toISOString()
                      .substring(0, 10)
                  ).format("YYYY-MM-DD")
                  : ""
              } */
              value={(item.actTranClosingDate === "" || !item.actTranClosingDate) ? null : new Date(item.actTranClosingDate)}
              onChange={onChange}
              onBlur={onBlurInput}
            />
          </div>
        </div>
        <div className="column is-3">
          <div className="columns">
            <SelectLabelInput
              label="Use of Proceeds"
              disabled={!tranAction.canEditTran}
              error={errorMessages.actTranUseOfProceeds || ""}
              list={dropDown.useOfProceeds}
              name="actTranUseOfProceeds"
              value={item.actTranUseOfProceeds}
              onChange={onChange}
              onBlur={onBlurInput}
            />
          </div>
        </div>
        {/* <SelectLabelInput label="Transaction Status*" disabled={!tranAction.canEditTran} error= {errorMessages.actTranStatus || ""} list={dropDown.tranStatus} name="actTranStatus" value={item.actTranStatus} onChange={onChange}/> */}
      </div>
    </div>
  )
}

export default TransSummary
