import React from "react"
import {TextLabelInput} from "../../../../../GlobalComponents/TextViewBox"

const LinkCusIp = ({ index, item={}, onChangeItem, errors={}, isEditable, isCusIpSaveDisabled, onRemove, onBlur, category, onEditLinkCusIp, onLinkCusIpSave, tranAction}) => {

  const onChange = (event) => {
    if(event.target.name === "cusip") {
      event.target.value = event.target.value ? event.target.value.toUpperCase() : ""
    }
    onChangeItem({
      ...item,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    }, category, index)
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value) {
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  isEditable = (isEditable === index)

  return (
    <tbody>
      <tr style={{border: "1px solid #dbdbdb"}}>
        <td>
          <TextLabelInput title="CUSIP" error={(errors && errors.cusip) ? "Required (Alphanumeric character length 6 to 9 accepted)" : ""} name="cusip" value={item.cusip || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !tranAction.canEditTran} />
        </td>
        <td>
          <TextLabelInput title="Description" name="description" value={item.description || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !tranAction.canEditTran} />
        </td>
        <td>
          <TextLabelInput title="Tag" name="tag" value={item.tag || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !tranAction.canEditTran}/>
        </td>

        <td>
          {
            tranAction && tranAction.canEditTran ?
              <div className="field is-grouped">
                <div className="control">
                  <a
                    onClick={isEditable ? () => onLinkCusIpSave(category, item, index) : () => onEditLinkCusIp(index, category)}
                    className={`${isCusIpSaveDisabled ? "isDisabled" : ""}`}>
                    <span className="has-text-link">
                      {isEditable ? <i className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                    </span>
                  </a>
                </div>
                <div className="control">
                  <a onClick={() => onRemove(item._id, index, item.cusip)} className={`${isCusIpSaveDisabled ? "isDisabled" : ""}`}>
                    <span className="has-text-link">
                      <i className="far fa-trash-alt" title="Delete"/>
                    </span>
                  </a>
                </div>
              </div> : null
          }
        </td>
      </tr>
    </tbody>
  )}

export default LinkCusIp
