import React from "react"
import moment from "moment"
import {
  TextLabelInput,
  SelectLabelInput,
  NumberInput
} from "../../../../../GlobalComponents/TextViewBox"

const LoanTerms = ({
  errorMessages,
  item,
  dropDown,
  onBlur,
  onChangeItem,
  category,
  tranAction
}) => {
  const onChange = event => {
    /* if (event.target.name === "fixedRate") {
      event.target.value =
        (event.target.value && parseFloat(event.target.value / 100)) || 0
    } */
    if (event.target.name === "paymentType" && event.target.value !== "Fixed") {
      onChangeItem(
        {
          ...item,
          fixedRate: 0,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...item,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        },
        category
      )
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  // const fixedRate = (item.fixedRate && parseFloat(item.fixedRate * 100)) || 0

  return (
    <div>
      <div className="columns ">
        <NumberInput
          label="Initial Principal/Par Amount"
          prefix="$"
          required
          disabled={!tranAction.canEditTran}
          error={errorMessages.parAmount ? "Required(must be larger than or equal to 0)" : ""  || ""}
          name="parAmount"
          value={item.parAmount || ""}
          placeholder="$"
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <SelectLabelInput
          label="Fed Tax"
          disabled={!tranAction.canEditTran}
          error={errorMessages.fedTax || ""}
          list={dropDown.fedTax || []}
          name="fedTax"
          value={item.fedTax}
          onChange={onChange}
        />
        <SelectLabelInput
          label="State Tax"
          disabled={!tranAction.canEditTran}
          error={errorMessages.stateTax || ""}
          list={dropDown.stateTax || []}
          name="stateTax"
          value={item.stateTax}
          onChange={onChange}
        />
        <SelectLabelInput
          label="Bank Qualified"
          disabled={!tranAction.canEditTran}
          error={errorMessages.bankQualified || ""}
          list={dropDown.bankQualified || []}
          name="bankQualified"
          value={item.bankQualified}
          onChange={onChange}
        />
      </div>

      <div className="columns">
        <div className="column is-3">
          <p className="multiExpLbl ">Day Count</p>
          <SelectLabelInput
            title="Day Count"
            placeholder="Day Count"
            list={dropDown.dayCount || []}
            error={errorMessages.dayCount || ""}
            name="dayCount"
            value={item.dayCount}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          />
        </div>
        <div className="column">
          <p className="multiExpLbl">Principal</p>
          <div className="field is-grouped-left">
            <div className="control">
              <SelectLabelInput
                title="Principal Payment Frequency"
                placeholder="Payment Frequency"
                list={dropDown.prinPaymentFreq || []}
                error={errorMessages.prinPaymentFreq || ""}
                name="prinPaymentFreq"
                value={item.prinPaymentFreq}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
              />
            </div>
            <div className="control">
              <TextLabelInput
                title="Principal Start Date"
                error={
                  (errorMessages.prinPaymentStartDate && "must be smaller than closing date") || ""
                }
                name="prinPaymentStartDate"
                type="date"
                /* value={
                  item.prinPaymentStartDate
                    ? moment(
                        new Date(item.prinPaymentStartDate)
                          .toISOString()
                          .substring(0, 10)
                      ).format("YYYY-MM-DD")
                    : ""
                } */
                value={(item.prinPaymentStartDate === "" || !item.prinPaymentStartDate) ? null : new Date(item.prinPaymentStartDate)}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
              />
            </div>
            <div className="control">
              <SelectLabelInput
                title="Principal Payment Day"
                placeholder="Payment Day"
                list={dropDown.paymentDay || []}
                error={errorMessages.prinPaymentDay || ""}
                name="prinPaymentDay"
                value={item.prinPaymentDay}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
              />
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl ">Interest</p>
          <div className="field is-grouped-left">
            <div className="control">
              <SelectLabelInput
                title="Interest Payment Frequency"
                placeholder="Payment Frequency"
                list={dropDown.intPaymentFreq || []}
                error={errorMessages.intPaymentFreq || ""}
                name="intPaymentFreq"
                value={item.intPaymentFreq}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
              />
            </div>
            <div className="control">
              <TextLabelInput
                title="Interest Start Date"
                error={
                  (errorMessages.intPaymentStartDate && "must be smaller than closing date") || ""
                }
                name="intPaymentStartDate"
                type="date"
                /* value={
                  item.intPaymentStartDate
                    ? moment(
                        new Date(item.intPaymentStartDate)
                          .toISOString()
                          .substring(0, 10)
                      ).format("YYYY-MM-DD")
                    : ""
                } */
                value={(item.intPaymentStartDate === "" || !item.intPaymentStartDate) ? null : new Date(item.intPaymentStartDate)}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
              />
            </div>
            <div className="control">
              <SelectLabelInput
                title="Interest Payment Day"
                placeholder="Payment Day"
                list={dropDown.paymentDay || []}
                error={errorMessages.intPaymentDay || ""}
                name="intPaymentDay"
                value={item.intPaymentDay}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!tranAction.canEditTran}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column is-3">
          <p className="multiExpLbl ">Bank Holidays</p>
          <SelectLabelInput
            title="Bank Holidays"
            placeholder="Bank Holidays"
            list={dropDown.bankHolidays || []}
            error={errorMessages.bankHolidays || ""}
            name="bankHolidays"
            value={item.bankHolidays}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          />
        </div>
        <div className="column is-3">
          <p className="multiExpLbl ">Payment Type</p>
          <SelectLabelInput
            title="Payment Type"
            list={dropDown.paymentType || []}
            error={errorMessages.paymentType || ""}
            name="paymentType"
            value={(!item.paymentType || item.paymentType === "") ? item.paymentType = "Fixed" : item.paymentType}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          />
        </div>

        { item.paymentType === "Floating" ?
          <SelectLabelInput
            title="Floating Rate Index"
            label="Floating Rate Index"
            placeholder="Choose Index"
            className="column is-1"
            list={dropDown.floatingRateType || []}
            error={errorMessages.floatingRateRef || ""}
            name="floatingRateRef"
            value={item.floatingRateRef}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          /> : null }
        { item.paymentType === "Floating" ?
          <NumberInput
            title="Floating Rate Ratio"
            label="Rate Ratio(%)"
            suffix="%"
            error={
              errorMessages.floatingRateRatio &&
              "Required (must be less than 100)"
            }
            name="floatingRateRatio"
            className="column is-1"
            value={item.floatingRateRatio || ""}
            placeholder="Floating Rate Ratio %"
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          /> : null }
        { item.paymentType === "Floating" ?
          <NumberInput
            title="Floating Rate Spread"
            label="Rate Spread(%)"
            suffix="%"
            error={
              errorMessages.floatingRateSpread &&
              "Required (must be less than 100)"
            }
            name="floatingRateSpread"
            className="column is-1"
            value={item.floatingRateSpread || ""}
            placeholder="Floating Rate Spread %"
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          /> : null }

        {
          item.paymentType !== "Floating" ?
            <div className="column is-3">
              <p className="multiExpLbl ">Fixed Rate</p>
              <NumberInput
                title="Fixed Rate"
                suffix="%"
                placeholder="Enter %"
                disabled={!tranAction.canEditTran}
                error={
                  errorMessages.fixedRate && "(must be larger than 0% and less than or equal to 100%)"
                }
                name="fixedRate"
                value={item.fixedRate || 0}
                onChange={onChange}
                onBlur={onBlurInput}
              />
            </div> : null
        }

      </div>

      <div className="columns">
        <div className="column is-3">
          <p className="multiExpLbl ">Business Convention</p>
          <SelectLabelInput
            title="Business Convention"
            placeholder="Business Convention"
            list={dropDown.businessConvention || []}
            error={errorMessages.busConvention || ""}
            name="busConvention"
            value={item.busConvention}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          />
        </div>
      </div>
      <hr />
      <div className="field">
        <p className="multiExpLbl ">Prepayment Terms</p>
        <p className="control">
          <textarea
            title="Prepayment Terms"
            className="textarea"
            placeholder="Pre-payment terms..."
            name="prePaymentTerms"
            value={item.prePaymentTerms}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!tranAction.canEditTran}
          />
        </p>
      </div>
    </div>
  )
}

export default LoanTerms
