import React from "react"
import swal from "sweetalert"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TransSummary from "./components/TransSummary"
import LoanTerms from "./components/LoanTerms"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import CONST, { states } from "../../../../../globalutilities/consts"
import LinkCusIp from "./components/LinkCusIp"
import {
  LoanDetailsValidate,
  LoanCUSIPValidate
} from "../Validation/LoanDetailsValidate"
import {
  putLoanTransaction,
  putCusIpsDetails,
  pullLinkCucIps,
  fetchBorrowerObligorByFirm,
  postLoanNotes
} from "../../../../StateManagement/actions"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import Loader from "../../../../GlobalComponents/Loader"
import HistoricalData from "../../../../GlobalComponents/HistoricalData"
import TransactionNotes from "../../../../GlobalComponents/TransactionNotes"

const cols = [
  { name: "CUSIP<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
  { name: "Description" },
  { name: "Tag" },
  { name: "Delete" }
]

class LoanDetails extends React.Component {
  constructor() {
    super()
    this.state = {
      bankLoanSummary: {
        ...CONST.Loan.bankLoanSummary
      },
      bankLoanTerms: {
        ...CONST.Loan.bankLoanTerms
      },
      bankLoanLinkCusips: [CONST.Loan.bankLoanLinkCusips],
      tempBankLoanLinkCusips: [CONST.Loan.bankLoanLinkCusips],
      tempTransaction: {},
      userName: "",
      errorMessages: {},
      usersList: [],
      tranNotes: [],
      states: [...states],
      dropDown: {
        guarantorName: [],
        borrower: [],
        tranType: [],
        primarySector: {},
        secondarySector: [],
        offeringType: [],
        securityType: [],
        bankQualified: [],
        corpType: [],
        useOfProceeds: [],
        fedTax: [],
        stateTax: [],
        prinPaymentFreq: [],
        intPaymentFreq: [],
        dayCount: [],
        businessConvention: [],
        bankHolidays: [],
        paymentDay: [],
        paymentType: [],
        floatingRateType: []
      },
      isEditable: "",
      loading: true,
      isSaveDisabled: false,
      isCusIpSaveDisabled: false,
      activeItem: [],
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      confirmAlert: CONST.confirmAlert
    }
  }

  componentWillMount() {
    const { transaction } = this.props
    this.setState({
      tempTransaction: {
        actTranSecondarySector: transaction.actTranSecondarySector,
        actTranPrimarySector: transaction.actTranPrimarySector,
        actTranClientId: transaction.actTranClientId,
        actTranClientName: transaction.actTranClientName,
        actTranClientMsrbType: transaction.actTranClientMsrbType,
        actTranIssueName: transaction.actTranIssueName,
        actTranProjectDescription: transaction.actTranProjectDescription
      }
    }, () => {
      this.getBorrowerByFirm()
    })
  }

  async componentDidMount() {
    const { transaction, tranAction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""

    const picResult = await getPicklistByPicklistName([
      "LKUPMSRBFIRMNAME",
      // "LKUPCOUNTRY",
      "LKUPPRIMARYSECTOR",
      "LKUPDEALTYPE",
      "LKUPSWAPTRANSTATUS",
      "LKUPUSEOFPROCEED",
      "LKUPFEDTAX",
      "LKUPSTATETAX",
      "LKUPBANKQUALIFIED",
      "LKUPPAYMENTTYPE",
      "LKUPPAYMENTFREQ",
      "LKUPDAYCOUNT",
      "LKUPBUSCONV",
      "LKUPBANKHOLIDAY",
      "LKUPPAYMENTDAY",
      "LKUPFLOATRATETYPE"
    ])
    const result = (picResult.length && picResult[1]) || {}
    const primarySec = (picResult[2] && picResult[2].LKUPPRIMARYSECTOR) || {}
    const stateCountry =  /*(picResult[3] && picResult[3].LKUPCOUNTRY) ||*/ {}
    this.setState(prevState => ({
      dropDown: {
        ...this.state.dropDown,
        firmName: result.LKUPMSRBFIRMNAME || [],
        stateCountry,
        primarySectors: result.LKUPPRIMARYSECTOR || [],
        secondarySectors: primarySec,
        tranType: result.LKUPDEALTYPE || [],
        tranStatus: result.LKUPSWAPTRANSTATUS || [],
        useOfProceeds: result.LKUPUSEOFPROCEED || [],
        fedTax: result.LKUPFEDTAX || [],
        stateTax: result.LKUPSTATETAX || [],
        bankQualified: result.LKUPBANKQUALIFIED || [],
        paymentType: result.LKUPPAYMENTTYPE || [],
        prinPaymentFreq: result.LKUPPAYMENTFREQ || [],
        intPaymentFreq: result.LKUPPAYMENTFREQ || [],
        dayCount: result.LKUPDAYCOUNT || [],
        businessConvention: result.LKUPBUSCONV || [],
        bankHolidays: result.LKUPBANKHOLIDAY || [],
        paymentDay: result.LKUPPAYMENTDAY || [],
        floatingRateType: result.LKUPFLOATRATETYPE || []
      },
      bankLoanSummary:
        transaction && transaction.bankLoanSummary
          ? {
            ...transaction.bankLoanSummary,
            actTranType: transaction.actTranSubType,
            actTranState: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranState) || userState,
            actTranCounty: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranCounty) || userCounty,
            actTranUseOfProceeds: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranUseOfProceeds) || "",
          }
          : {
            ...prevState.bankLoanSummary,
            actTranType: transaction.actTranSubType,
            actTranState: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranState) || userState,
            actTranCounty: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranCounty) || userCounty,
            actTranUseOfProceeds: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranUseOfProceeds) || "",
          },
      bankLoanTerms:
        transaction && transaction.bankLoanTerms
          ? transaction.bankLoanTerms
          : prevState.bankLoanTerms,
      bankLoanLinkCusips:
        transaction &&
        transaction.bankLoanLinkCusips &&
        transaction.bankLoanLinkCusips.length
          ? transaction.bankLoanLinkCusips
          : prevState.bankLoanLinkCusips,
      tempBankLoanLinkCusips:
        transaction &&
        transaction.bankLoanLinkCusips &&
        transaction.bankLoanLinkCusips.length
          ? transaction.bankLoanLinkCusips
          : prevState.bankLoanLinkCusips,
      isEditable: transaction && transaction.bankLoanLinkCusips.length ? "" : 0,
      userName: (this.props.user && this.props.user.userFirstName) || "",
      tranNotes: transaction && transaction.tranNotes || [],
      loading: false,
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actTranIssueName ||
          transaction.actTranProjectDescription} - Notification`
      }
    }))
  }

  getBorrowerByFirm = () => {
    const { transaction } = this.props
    const { tempTransaction } = this.state
    fetchBorrowerObligorByFirm(
      tempTransaction.actTranClientId || transaction.actTranClientId,
      borrower => {
        this.setState(prevState => ({
          dropDown: {
            ...prevState.dropDown,
            borrower
          }
        }))
      }
    )
  }

  onChange = (item, category, index) => {
    const items = this.state[category]
    items.splice(index, 1, item)
    this.setState({
      [category]: items
    })
  }

  onChangeItem = (item, category) => {
    this.setState({
      [category]: item
    })
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `${change}`,
      date: new Date(),
      key: category
    })
  }

  onCancel = () => {
    const { tranAction, transaction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    this.setState(prevState => ({
      bankLoanSummary:
        transaction && transaction.bankLoanSummary
          ? {
              ...transaction.bankLoanSummary,
              actTranType: (transaction && transaction.actTranSubType),
              actTranState: (transaction && transaction.bankLoanSummary && transaction.bankLoanSummary.actTranState) || userState,
              actTranCounty: (transaction && transaction.bankLoanSummary && transaction.bankLoanSummary.actTranCounty) || userCounty,
              actTranUseOfProceeds: (transaction.bankLoanSummary && transaction.bankLoanSummary.actTranUseOfProceeds) || "",
            }
          : {
              ...CONST.Loan.bankLoanSummary,
            },
      bankLoanTerms: transaction && transaction.bankLoanTerms ? { ...CONST.Loan.bankLoanTerms, ...transaction.bankLoanTerms} : CONST.Loan.bankLoanTerms,
      errorMessages: {},
      tempTransaction: {
        actTranSecondarySector: transaction && transaction.actTranSecondarySector || "",
        actTranPrimarySector: transaction && transaction.actTranPrimarySector || "",
        actTranClientId: transaction && transaction.actTranClientId || "",
        actTranClientName: transaction && transaction.actTranClientName || "",
        actTranClientMsrbType: transaction && transaction.actTranClientMsrbType || "",
        actTranIssueName: transaction && transaction.actTranIssueName || "",
        actTranProjectDescription: transaction && transaction.actTranProjectDescription || ""
      },
      dropDown: { ...this.state.dropDown}
    }))
  }

  actionButtons = (key, isDisabled) => {
    const { tranAction } = this.props
    if (tranAction && !tranAction.canEditTran) return
    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
            disabled={isDisabled}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset(key)}
            disabled={isDisabled}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  onReset = key => {
    const { tempBankLoanLinkCusips } = this.state
    const auditLogs = this.props.auditLogs.filter(x => x.category !== key)
    this.setState({
      [key]: key === "bankLoanLinkCusips" ? [...tempBankLoanLinkCusips] : [],
      isEditable: ""
    })
    this.props.updateAuditLog(auditLogs)
  }

  onAdd = key => {
    const { userName, isEditable } = this.state
    const insertRow = cloneDeep(this.state[key])

    if (!!isEditable) {
      return swal("Warning", "First save the current participants", "warning")
    }

    insertRow.push(CONST.Loan[key])
    this.props.addAuditLog({
      userName,
      log: `Bank Loan ${key} Add new Item`,
      date: new Date(),
      key
    })
    this.setState({
      [key]: insertRow,
      isEditable: insertRow.length - 1
    })
  }

  onRemove = (cusIpId, index, name) => {
    const { confirmAlert } = this.state
    confirmAlert.text = `You want to delete this Link CUSIP?`
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        if (cusIpId) {
          this.props.addAuditLog({
            userName: this.state.userName,
            log: `Link cusIp ${name} remove item`,
            date: new Date(),
            key: "bankLoanLinkCusips"
          })
          this.setState(
            {
              isCusIpSaveDisabled: true
            },
            () => {
              pullLinkCucIps(this.props.nav2, cusIpId, res => {
                if (res && res.status === 200) {
                  toast("Link cusIp was removed!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.SUCCESS
                  })
                  this.props.submitAuditLogs(this.props.nav2)
                  this.setState(prevState => ({
                    isEditable: "",
                    errorMessages: {},
                    isCusIpSaveDisabled: false,
                    bankLoanLinkCusips:
                      (res.data &&
                        res.data.bankLoanLinkCusips &&
                        res.data.bankLoanLinkCusips.length &&
                        cloneDeep(res.data.bankLoanLinkCusips)) ||
                      prevState.bankLoanLinkCusips,
                    tempBankLoanLinkCusips: (res.data &&
                      res.data.bankLoanLinkCusips &&
                      res.data.bankLoanLinkCusips.length &&
                      cloneDeep(res.data.bankLoanLinkCusips)) || [
                      CONST.Loan.bankLoanLinkCusips
                    ]
                  }))
                } else {
                  toast("Something went wrong!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.ERROR
                  })
                }
              })
            }
          )
        } else {
          const bankLoanLinkCusips = cloneDeep(this.state.bankLoanLinkCusips)
          bankLoanLinkCusips.splice(index, 1)
          this.setState({
            bankLoanLinkCusips,
            isEditable: "",
            errorMessages: {}
          })
        }
      }
    })
  }

  onEditLinkCusIp = index => {
    if (this.state.isEditable) {
      return swal("Warning", "First save the current Link CUSIP", "warning")
    }
    this.props.addAuditLog({
      userName: this.state.userName,
      log: "Bank loan details CUSIP one row edited",
      date: new Date(),
      key: "bankLoanLinkCusips"
    })
    this.setState({
      isEditable: index
    })
  }

  onLinkCusIpSave = (key, item, index) => {
    const bankLoanLinkCusips = cloneDeep(this.state.bankLoanLinkCusips)
    // const cusips = bankLoanLinkCusips[index]
    const errors = LoanCUSIPValidate(item)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        const message = "Required"
        if (errorMessages.hasOwnProperty(key)) {
          //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) {
            //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    const loanCusIp = bankLoanLinkCusips.map(item => item.cusip)

    const isDuplicate = loanCusIp.some(
      (item, idx) => loanCusIp.indexOf(item) !== idx
    )

    if (isDuplicate) {
      this.setState({
        errorMessages: {
          bankLoanCusipsInvalid: "Link CUSIP must be unique."
        }
      })
      return
    }

    this.setState(
      {
        isCusIpSaveDisabled: true
      },
      () => {
        putCusIpsDetails(this.props.nav2, item, res => {
          if (res && res.status === 200) {
            this.props.submitAuditLogs(this.props.nav2)
            toast("CUSIPS has been updated!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.setState(
              {
                errorMessages: {},
                isEditable: "",
                bankLoanLinkCusips: (res.data &&
                  res.data.bankLoanLinkCusips &&
                  res.data.bankLoanLinkCusips.length &&
                  cloneDeep(res.data.bankLoanLinkCusips)) || [
                  CONST.Loan.bankLoanLinkCusips
                ],
                tempBankLoanLinkCusips: (res.data &&
                  res.data.bankLoanLinkCusips &&
                  res.data.bankLoanLinkCusips.length &&
                  cloneDeep(res.data.bankLoanLinkCusips)) || [
                  CONST.Loan.bankLoanLinkCusips
                ],
                isCusIpSaveDisabled: false
              },
            )
          } else {
            toast("Something went wrong!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isCusIpSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onSave = () => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    const { bankLoanSummary, bankLoanTerms, tempTransaction } = this.state
    const { transaction } = this.props
    const payload = { bankLoanSummary, bankLoanTerms, ...tempTransaction }

    !(bankLoanTerms && bankLoanTerms.intPaymentStartDate) && delete bankLoanTerms.intPaymentStartDate
    !(bankLoanTerms && bankLoanTerms.prinPaymentStartDate) && delete bankLoanTerms.prinPaymentStartDate

    const errors = LoanDetailsValidate(payload, transaction.createdAt, bankLoanSummary.actTranClosingDate)
    console.log("==============================>", payload)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, details: errorMessages },
        activeItem: [0, 1]
      }))
      return
    }

    /* if (
      bankLoanSummary.actTranClosingDate &&
      (bankLoanTerms && bankLoanTerms.prinPaymentStartDate || bankLoanTerms && bankLoanTerms.intPaymentStartDate)
    ) {
      const errorMessages = {}
      const closingDate = moment(bankLoanSummary.actTranClosingDate).unix()

      if (
        closingDate > moment(bankLoanTerms.prinPaymentStartDate, "YYYY-MM-DD").unix() ||
        closingDate > moment(bankLoanTerms.intPaymentStartDate, "YYYY-MM-DD").unix()
      ) {
        if (
          closingDate > moment(bankLoanTerms.prinPaymentStartDate, "YYYY-MM-DD").unix()
        ) {
          errorMessages.prinPaymentStartDate = "Required"
        }
        if (
          closingDate > moment(bankLoanTerms.intPaymentStartDate, "YYYY-MM-DD").unix()
        ) {
          errorMessages.intPaymentStartDate = "Required"
        }
        return this.setState(prevState => ({
          errorMessages: { ...prevState.errorMessages, details: errorMessages },
          activeItem: [0, 1]
        }))
      }
    } */
    this.setState(
      {
        isSaveDisabled: true
      },
      () => {
        putLoanTransaction("details", this.props.nav2, payload, res => {
          if (res && res.status === 200) {
            toast("Add loan details transaction successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            const transaction = (res && res.data) || {}
            this.props.submitAuditLogs(this.props.nav2)
            this.setState(
              prevState => ({
                bankLoanSummary:
                  transaction && transaction.bankLoanSummary
                    ? {
                      ...transaction.bankLoanSummary,
                      actTranType: transaction.actTranSubType
                    }
                    : {
                      ...CONST.Loan.bankLoanSummary,
                      actTranType: transaction.actTranSubType
                    },
                bankLoanTerms:
                  (transaction && transaction.bankLoanTerms) ||
                  CONST.Loan.bankLoanTerms,
                bankLoanLinkCusips: (transaction &&
                  transaction.bankLoanLinkCusips &&
                  transaction.bankLoanLinkCusips.length &&
                  cloneDeep(transaction.bankLoanLinkCusips)) || [
                  CONST.Loan.bankLoanLinkCusips
                ],
                tempBankLoanLinkCusips: (transaction &&
                  transaction.bankLoanLinkCusips &&
                  transaction.bankLoanLinkCusips.length &&
                  cloneDeep(transaction.bankLoanLinkCusips)) || [
                  CONST.Loan.bankLoanLinkCusips
                ],
                errorMessages: {
                  ...prevState.errorMessages,
                  details: {}
                },
                isSaveDisabled: false,
                activeItem: []
              }),
              async () => {
                await sendEmailAlert(emailPayload)
              }
            )
          } else {
            toast("something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false,
              activeItem: []
            })
          }
        })
      }
    )
  }

  handelToggle = () =>{
    this.setState({ modalState: true })
  }

  onModalSave = () => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    this.setState({
      modalState: false
    },async ()=>{
      await sendEmailAlert(emailPayload)
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  getCountryDetails = (cityStateCountry = "") => {
    const { bankLoanSummary } = this.state
    if (cityStateCountry && cityStateCountry.state) {
      bankLoanSummary.actTranState = cityStateCountry.state.trim()
    }else {
      bankLoanSummary.actTranState = ""
    }
    if (cityStateCountry && cityStateCountry.city) {
      bankLoanSummary.actTranCounty = cityStateCountry.city.trim()
    }else {
      bankLoanSummary.actTranCounty = ""
    }
    this.setState({
      bankLoanSummary
    })
  }

  onNotesSave = (payload, callback) => {
    postLoanNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add Bank Loan Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  render() {
    const {
      bankLoanSummary,
      bankLoanTerms,
      errorMessages,
      modalState,
      email,
      activeItem,
      isCusIpSaveDisabled,
      dropDown,
      bankLoanLinkCusips,
      isSaveDisabled,
      isEditable,
      tranNotes,
      tempTransaction
    } = this.state
    const { tranAction, participants, onParticipantsRefresh, transaction } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        {tranAction && tranAction.canEditTran ? (
          <button className="button is-link is-small" onClick={this.handelToggle}>
            Send Email Alert
          </button>
        ) : null}
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onModalSave}
        />
        <Accordion
          multiple
          isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Transaction Summary"
              >
                {activeAccordions.includes(0) && (
                  <TransSummary
                    transaction={tempTransaction}
                    errorMessages={
                      (errorMessages && errorMessages.details) || {}
                    }
                    item={bankLoanSummary}
                    dropDown={dropDown}
                    getBorrowerByFirm={this.getBorrowerByFirm}
                    onChangeItem={this.onChangeItem}
                    category="bankLoanSummary"
                    onBlur={this.onBlur}
                    tranAction={tranAction}
                    getCountryDetails={this.getCountryDetails}
                  />
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Loan Terms"
              >
                {activeAccordions.includes(1) && (
                  <LoanTerms
                    errorMessages={
                      (errorMessages && errorMessages.details) || {}
                    }
                    item={bankLoanTerms}
                    dropDown={dropDown}
                    onChangeItem={this.onChangeItem}
                    category="bankLoanTerms"
                    onBlur={this.onBlur}
                    onSave={this.onSave}
                    onCancel={this.onCancel}
                    tranAction={tranAction}
                  />
                )}
              </RatingSection>
              <br />
              {tranAction && tranAction.canEditTran ? (
                <div className="field is-grouped-center">
                  <div className="control">
                    <button
                      className="button is-link"
                      onClick={this.onSave}
                      disabled={isSaveDisabled || false}
                    >
                      Save
                    </button>
                  </div>
                  <div className="control">
                    <button className="button is-light" onClick={this.onCancel}>
                      Cancel
                    </button>
                  </div>
                </div>
              ) : null}
              <RatingSection
                onAccordion={() => onAccordion(2)}
                title="Link CUSIP (if applicable)"
                style={{ overflowX: "hidden" }}
                actionButtons={this.actionButtons(
                  "bankLoanLinkCusips",
                  isCusIpSaveDisabled
                )}
              >
                {activeAccordions.includes(2) && (
                  <div className="overflow-auto">
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={cols} />
                      {bankLoanLinkCusips.length
                        ? bankLoanLinkCusips.map((cusIp, index) => {
                          const errors = (errorMessages && errorMessages.bankLoanLinkCusips && errorMessages.bankLoanLinkCusips[index.toString()]) || {}
                          return (
                            <LinkCusIp
                              key={index.toString()}
                              index={index}
                              isEditable={isEditable}
                              errors={errors}
                              items={bankLoanLinkCusips || []}
                              item={cusIp}
                              category="bankLoanLinkCusips"
                              onChangeItem={this.onChange}
                              onBlur={this.onBlur}
                              onRemove={this.onRemove}
                              onEditLinkCusIp={this.onEditLinkCusIp}
                              onLinkCusIpSave={this.onLinkCusIpSave}
                              isCusIpSaveDisabled={isCusIpSaveDisabled}
                              tranAction={tranAction}
                            />
                          )
                        })
                        : null}
                    </table>
                    <p
                      className="text-error"
                      style={{ marginLeft: 10, fontSize: 15 }}
                    >
                      {errorMessages.bankLoanCusipsInvalid || ""}
                    </p>
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
        { transaction.OnBoardingDataMigrationHistorical ? <HistoricalData data={(transaction && transaction.historicalData) || {}}/> : null}
        <TransactionNotes
          notesList={tranNotes || []}
          canEditTran={tranAction && tranAction.canEditTran || false}
          onSave={this.onNotesSave}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(LoanDetails)

export default connect(mapStateToProps, null)(WrappedComponent)
