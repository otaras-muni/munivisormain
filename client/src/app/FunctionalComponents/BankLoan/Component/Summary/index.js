import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TranSummaryStatus from "../../../../GlobalComponents/TranSummaryStatus"
import ActivitySummary from "./components/ActivitySummary"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import { getPicklistByPicklistName, getDocDetails } from "GlobalUtils/helpers"
import {
  putLoanTransaction,
  putLoanStatusAndSec,
  fetchBorrowerObligorByFirm
} from "../../../../StateManagement/actions"
import CONST, { oppMsg } from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import { pullRelatedTransactions } from "../../../../StateManagement/actions/CreateTransaction"
import ReletedTranGlob from "../../../../GlobalComponents/ReletedTranGlob"

class Summary extends React.Component {
  constructor() {
    super()
    this.state = {
      debtIssuance: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      },
      tranTextChange: {
        actTranIssueName: "",
        actTranProjectDescription: ""
      },
      errorMessages: {},
      transaction: {},
      securityType: [],
      relatedTypes: [],
      loading: true,
      isSaveDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      dropDown: {
        borrower: [],
        Guarantor: []
      },
      relatedTransactions: []
    }
  }

  async componentDidMount() {
    const { transaction } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPSWAPTRANSTATUS",
      "LKUPSECTYPEGENERAL",
      "LKUPRELATEDTYPE"
    ])
    const result = (picResult.length && picResult[1]) || {}
    this.setState(
      prevState => ({
        tranStatus: result.LKUPSWAPTRANSTATUS || [],
        securityType: result.LKUPSECTYPEGENERAL || [],
        relatedTypes: result.LKUPRELATEDTYPE || ["Bond Hedge", "Loan Hedge"],
        tranTextChange: {
          actTranIssueName: transaction.actTranIssueName || "",
          actTranProjectDescription:
            transaction.actTranProjectDescription || ""
        },
        loading: false,
        transaction,
        relatedTransactions: transaction.actTranRelatedTo || [],
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.actTranIssueName ||
            transaction.actTranProjectDescription} - Notification`
        },
        tempTransaction: {
          actTranSecondarySector: transaction.actTranSecondarySector,
          actTranPrimarySector: transaction.actTranPrimarySector,
          actTranClientId: transaction.actTranClientId,
          actTranClientName: transaction.actTranClientName,
          actTranClientMsrbType: transaction.actTranClientMsrbType,
          actTranIssueName: transaction.actTranIssueName,
          actTranProjectDescription: transaction.actTranProjectDescription
        }
      }),
      () => {
        this.getBorrowerByFirm()
      }
    )
  }

  getBorrowerByFirm = () => {
    const { transaction } = this.props
    const { tempTransaction } = this.state
    fetchBorrowerObligorByFirm(
      tempTransaction.actTranClientId || transaction.actTranClientId,
      borrower => {
        this.setState(prevState => ({
          dropDown: {
            ...prevState.dropDown,
            borrower
          }
        }))
      }
    )
  }

  onRelatedTranSelect = item => {
    const { debtIssuance } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary related type ${
        debtIssuance.actTranRelatedType
      } and  derivative ${item.name || ""}`,
      date: new Date(),
      key: "summary"
    })
    this.setState(prevState => ({
      debtIssuance: {
        ...prevState.debtIssuance,
        actTranRelatedTo: item
      }
    }))
  }

  onChange = e => {
    const { debtIssuance } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary select related type ${e.target.value}`,
      date: new Date(),
      key: "summary"
    })
    debtIssuance[e.target.name] = e.target.value
    this.setState({
      debtIssuance: {
        ...debtIssuance,
        actTranRelatedTo: []
      }
    })
  }

  onModalSave = () => {
    this.setState({
      modalState: true,
      relatedSave: true
    })
  }

  onRelatedtranSave = () => {
    const { email, debtIssuance } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    const actTranRelatedTo = {
      ...debtIssuance.actTranRelatedTo,
      relType: debtIssuance.actTranRelatedType
    }

    if (!debtIssuance.actTranRelatedType || actTranRelatedTo.length === 0)
      return false

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putLoanTransaction(
          "summary",
          this.props.nav2,
          actTranRelatedTo,
          res => {
            if (res && res.status === 200) {
              toast("Add related transaction successfully", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  transaction: res.data,
                  relatedTransactions:
                    (res.data && res.data.actTranRelatedTo) || [],
                  debtIssuance: {
                    actTranRelatedType: debtIssuance.actTranRelatedType,
                    actTranRelatedTo: ""
                  },
                  isSaveDisabled: false
                },
                async () => {
                  await sendEmailAlert(emailPayload)
                }
              )
            } else {
              toast("something went wrong", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
            }
          }
        )
      }
    )
  }

  onChangeTextInput = e => {
    let tranTextChange = this.state.tranTextChange
    this.setState({
      tranTextChange: {
        ...tranTextChange,
        [e.target.name]: e.target.value
      }
    })
  }

  onBlur = e => {
    const { transaction, tranTextChange } = this.state
    if (transaction[e.target.name] !== tranTextChange[e.target.name]) {
      this.onInputChange(e)
    }
  }

  onInputChange = e => {
    const transaction = cloneDeep(this.state.transaction)
    const { user } = this.props
    let state = {}
    const { name, value, title } = e.target
    const statusChange = name === "actTranStatus"
    transaction[name] = value
    if (statusChange) {
      const payload = {
        [name]: value || ""
      }
      if (transaction.opportunity && transaction.opportunity.status && value !== "In Progress") {
        payload[name] = value === "Won" ? "Pre-Pricing" : value
        payload.opportunity = {
          ...transaction.opportunity,
          status: false,
          isWon: value === "Won"
        }
      }
      if(value === "Cancelled" || value === "Closed"){
        const msg = "You will not be able to EDIT any part of the transaction post this action. Are you sure?"
        swal({
          title: "",
          text: msg,
          icon: "warning",
          buttons: ["Cancel", "Yes"]
        }).then(willDo => {
          if (willDo) {
            state = {
              payload,
              statusChange,
              modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress")
            }
            this.setState({
              ...state,
              relatedSave: false
            })
          }
        })
      } else {
        state = {
          payload,
          statusChange,
          modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress")
        }
      }
    } else {
      state = {
        payload: {
          [name]: value || ""
        }
      }
    }
    this.setState(
      {
        ...state,
        relatedSave: false
      },
      () => {
        if (statusChange) {
          if (transaction.opportunity && transaction.opportunity.status) {
            if (value === "Won") {
              swal({
                title: "",
                text: oppMsg.won,
                icon: "warning",
                buttons: ["Cancel", "Yes, won it!"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            } else {
              const msg =
                value === "Lost"
                  ? oppMsg.lost
                  : oppMsg.cancel
              swal({
                title: "",
                text: msg,
                icon: "warning",
                buttons: ["Cancel", "Yes"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            }
          }else {
            this.props.addAuditLog({
              userName: user.userFirstName,
              log: `In summary transaction ${title ||
              "empty"} change to ${value}`,
              date: new Date(),
              key: "summary"
            })
          }
        } else {
          this.addAuditAndSave(title, value, transaction)
        }
      }
    )
  }

  addAuditAndSave = (title, value, transaction) => {
    const {user} = this.props
    if (title && value) {
      this.props.addAuditLog({
        userName: user.userFirstName,
        log: `In summary transaction ${title ||
        "empty"} change to ${value}`,
        date: new Date(),
        key: "summary"
      })
    }
    this.onConfirmationSave(transaction)
  }

  onConfirmationSave = () => {
    const { email, transaction, payload, statusChange } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putLoanStatusAndSec(transaction._id, payload, res => {
          //eslint-disable-line
          if (res && res.status === 201) {
            toast(statusChange ?"Status updated successfully" : "Transaction updated successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(transaction._id)
            this.setState(
              prevState => ({
                transaction: {
                  ...prevState.transaction,
                  ...payload
                },
                tempTransactions: {
                  ...prevState.transaction,
                  ...payload
                },
                isSaveDisabled: false
              }),
              async () => {
                if (statusChange) {
                  this.setState({
                    statusChange: false
                  })
                  this.props.onStatusChange(payload.actTranStatus)
                  await sendEmailAlert(emailParams)
                }
              }
            )
          } else {
            toast("Something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
          }
        })
      }
    )
  }

  onCancel = () => {
    this.setState({
      debtIssuance: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      }
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  pdfDownload = () => {
    const {transaction} = this.state
    const {user, logo} = this.props
    const position = user && user.settings
    const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
    const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"
    let participants = ""
    if (transaction.bankLoanParticipants) {
      transaction.bankLoanParticipants.forEach(item => {
        participants += `${item.partType} : ${item.partFirmName} ${item.partContactName ? `(${item.partContactName})` : ""},\n`
      })
    }
    const tableData = [
      {
        titles: ["Summary"],
        description: "",
        headers: [
          "Issuer name",
          "Obligor",
          "Issue name",
          "Project Description",
          "Guarantor Name",
          "Borrower or Obligor Name",
          "Borrowing Form",
          "Participants",
          "Schedule highlights",
          "Lead Advisors",
          "Principal Amount/Par Value",
          "Security",
          "Coupon / Rate Type",
          "Tenor / Maturities",
          "Notes"
        ],
        rows: [
          [
            `${transaction.actTranClientName}`,
            `${transaction.bankLoanSummary.actTranObligorName ? transaction.bankLoanSummary.actTranObligorName : "--" }`,
            `${transaction.actTranIssueName ? transaction.actTranIssueName : "--"}`,
            `${transaction.actTranProjectDescription}`,
            `${transaction.actTranObligorName ? transaction.actTranObligorName : "--"}`,
            `${transaction.actTranBorrowerName ? transaction.actTranBorrowerName : "--"}`,
            `${transaction.bankLoanSummary.actTranBorrowerName ? transaction.bankLoanSummary.actTranBorrowerName : "--"}`,
            `${transaction.bankLoanParticipants ? participants : "--"}`,
            "--",
            `${transaction.actTranFirmLeadAdvisorName ? transaction.actTranFirmLeadAdvisorName : "--"}`,
            `${transaction.bankLoanTerms.parAmount ? transaction.bankLoanTerms.parAmount : 0}`,
            `${transaction.actTranSecurityType ? transaction.actTranSecurityType : "--"}`,
            "-- Rate",
            "--",
            `${actNotes || "--"}`
          ]
        ]
      }
    ]
    pdfTableDownload("", tableData, `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} summary.pdf`, logo, position)
  }

  deleteRelatedTran = async (relatedId, relTranId, relType, relatedType) => {
    const { transaction } = this.props
    const query = `BankLoans?tranId=${transaction._id}&relatedId=${relatedId}&relTranId=${relTranId}&relType=${relType}&relatedType=${relatedType}`
    const related = await pullRelatedTransactions(query)
    if(related && related.done){
      toast("related transaction removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState({
        relatedTransactions: related && related.relatedTransaction || []
      })
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  render() {
    const {
      modalState,
      email,
      debtIssuance,
      relatedTransactions,
      tranStatus,
      dropDown,
      isSaveDisabled,
      relatedTypes,
      securityType,
      transaction,
      errorMessages,
      relatedSave,
      tranTextChange
    } = this.state
    const { tranAction, participants, onParticipantsRefresh, nav2 } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={
            relatedSave === true
              ? this.onRelatedtranSave
              : this.onConfirmationSave
          }
        />
        <TranSummaryStatus
          disabled={!tranAction.canEditTran}
          tranStatus={tranStatus}
          pdfDownload={this.pdfDownload}
          selectedStatus={transaction.actTranStatus || ""}
          tranName={transaction.actTranUniqIdentifier || ""}
          transaction={
            {
              ...transaction.bankLoanSummary,
              opportunity: transaction.opportunity || false
            } || {}
          }
          onChange={this.onInputChange}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Activity Summary"
              >
                {activeAccordions.includes(0) && (
                  <ActivitySummary
                    canEditTran={tranAction.canEditTran}
                    securityType={securityType}
                    transaction={transaction}
                    onChange={this.onInputChange}
                    onTextInputChange={this.onChangeTextInput}
                    onBlur={this.onBlur}
                    dropDown={dropDown}
                    tranTextChange={tranTextChange}
                  />
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Related transactions"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(1) && (
                  <ReletedTranGlob
                    relatedTypes={relatedTypes}
                    transaction={transaction}
                    errorMessages={errorMessages}
                    isSaveDisabled={isSaveDisabled}
                    item={debtIssuance}
                    tranId={nav2}
                    onRelatedTranSelect={this.onRelatedTranSelect}
                    onRelatedtranCancel={this.onCancel}
                    onChange={this.onChange}
                    onRelatedtranSave={this.onModalSave}
                    canEditTran={tranAction.canEditTran}
                    relatedTransactions={relatedTransactions}
                    deleteRelatedTran={this.deleteRelatedTran}
                  />
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})
const WrappedComponent = withAuditLogs(Summary)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
