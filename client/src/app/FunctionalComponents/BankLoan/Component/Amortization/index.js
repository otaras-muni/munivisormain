import React from "react"
import {toast} from "react-toastify"
import {connect} from "react-redux"
import cloneDeep from "lodash.clonedeep"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import Accordion from "../../../../GlobalComponents/Accordion"
import CONST from "../../../../../globalutilities/consts"
import PaymentSchedule from "./components/PaymentSchedule"
import {putAmortizationDetails, pullAmortizationDetails} from "../../../../StateManagement/actions"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {AmortValidate} from "../Validation/AmortValidate"
import swal from "sweetalert";

const cols = [
  {name: "Reduction Date"},
  {name: "Principal Amount Reduction"},
  {name: "Revised Principal Amount"},
  {name: "Action"}]

const mergeArray = (currentList, newItems) => {
  const newObj = newItems.find(part => part.isNew)
  if(newObj){
    currentList.push(newObj)
  }
  return currentList
}

class Amortization extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: "",
      errorMessages: {},
      bankLoanAmort: [CONST.Loan.bankLoanAmort],
      tempBankLoanAmort: [CONST.Loan.bankLoanAmort],
      activeTab: "",
      loading: false,
      isEditable: "",
    }
  }

  componentWillMount() {
    const {transaction} = this.props
    let ascOrderAmort = []
    if(transaction && transaction.bankLoanAmort && transaction.bankLoanAmort.length) {
      ascOrderAmort = this.sortAmort(transaction.bankLoanAmort)
    }
    this.setState({
      bankLoanAmort: ascOrderAmort.length ? cloneDeep(ascOrderAmort) : [CONST.Loan.bankLoanAmort],
      tempBankLoanAmort: (ascOrderAmort && ascOrderAmort.length) ? cloneDeep(ascOrderAmort) : [CONST.Loan.bankLoanAmort],
      userName: (this.props.user && this.props.user.userFirstName) || "",
      isEditable: (transaction && transaction.bankLoanAmort.length)  ? "" : 0,
      loading: false,
    })
  }

  sortAmort = (bankLoanAmort) => {
    bankLoanAmort = bankLoanAmort || []
    return bankLoanAmort.sort((a, b) => {
      const dateA = a.reductionDate.toUpperCase() // ignore upper and lowercase
      const dateB = b.reductionDate.toUpperCase() // ignore upper and lowercase
      if (dateA < dateB) {
        return -1
      }
      if (dateA > dateB) {
        return 1
      }
      return 0
    })
  }

  onChangeItem = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    })
  }

  onReset = (key) => {
    const auditLogs = this.props.auditLogs.filter(x=> x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      [key]: [...this.state.tempBankLoanAmort],
      isEditable: "",
    })
  }

  onAdd = (key) => {
    const { userName, isEditable } = this.state
    const insertRow = this.state[key]

    if(isEditable) { return swal("Warning", `First save the current payment schedule`, "warning") }

    insertRow.push(CONST.Loan[key])
    this.props.addAuditLog({userName, log: `Payment Schedule ${key} Add new Item`, date: new Date(), key})
    this.setState({
      [key]: insertRow,
      isEditable: insertRow.length-1
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({userName, log: `Payment Schedule ${change}`, date: new Date(), key: category})
  }

  actionButtons = (key) => {
    const list = this.state.bankLoanAmort
    const isAddNew = list ? list.find(item => item && item.isNew) : false
    return ( //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={!!isAddNew}>Add</button>
        </div>
        <div className="control">
          <button className="button is-light is-small" onClick={() => this.onReset(key)} >Reset</button>
        </div>
      </div>
    )
  }
  onEdit = (key, index) => {
    const {isEditable} = this.state
    this.props.addAuditLog({userName: this.state.userName, log: `Loan participants In ${key} one row edited`, date: new Date(), key})
    if(isEditable || isEditable === 0) { return swal("Warning", `First save the current bank loan participants`, "warning") }
    this.setState({
      isEditable: index,
    })
  }

  onCancel = () => {
    this.setState({
      isEditable: "",
    })
  }

  onRemove = (removeId, type, index) => {
    const loanId = this.props.nav2
    const {userName} = this.state

    if(type && loanId && removeId) {
      this.props.addAuditLog({userName, log: `Remove participants ${removeId}`, date: new Date(), key: "bankLoanParticipants" })
      const result = confirm(`You want to delete this ${type}?`) //eslint-disable-line
      if (result) {
        this.setState({
          isSaveDisabled: true,
        }, () => {
          pullAmortizationDetails(this.props.nav2, removeId, (res) => {
            if (res && res.status === 200) {
              toast("Removed payment schedule successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
              this.props.submitAuditLogs(this.props.nav2)

              let newItem = this.state[type].find(item => item.isNew)
              let ascOrderAmort = (res && res.data && res.data[type] && res.data[type].length) ? cloneDeep(res.data[type]) : []
              newItem = newItem ? cloneDeep(newItem) : !ascOrderAmort.length ? cloneDeep(CONST.Loan.bankLoanAmort) : ""
              if(newItem){
                ascOrderAmort.push(newItem)
              }

              if (ascOrderAmort.length) {
                ascOrderAmort = this.sortAmort(ascOrderAmort)
              }
              this.setState({
                [type]: ascOrderAmort,
                tempBankLoanAmort: (res && res.data && res.data[type] && res.data[type].length) ? cloneDeep(res.data[type]) : [CONST.Loan.bankLoanAmort],
                isEditable: "",
                errorMessages: {},
                isSaveDisabled: false,
              })
            } else {
              toast("something went wrong", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
            }
          })
        })
      }
    }else {
      const amortObj = cloneDeep(this.state[type])
      const result = confirm(`You want to delete this ${type}?`) //eslint-disable-line
      if(result) {
        amortObj.splice(index, 1)
        this.setState({
          [type]: amortObj,
          isEditable: "",
          errorMessages: {}
        })
      }
    }
  }

  onSave = (key, payload, index) => {
    const {transaction} = this.props
    const errors = AmortValidate(payload, transaction.createdAt)
    if(errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        const message = "Required"
        if(errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if(errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({errorMessages})
      return
    }

    this.setState({
      isSaveDisabled: true,
    }, () => {
      delete payload.isNew
      putAmortizationDetails(this.props.nav2, payload, (res) => {
        if(res && res.status === 200) {
          toast("Added Payment Schedule successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.submitAuditLogs(this.props.nav2)
          let ascOrderAmort = (res && res.data && res.data[key] && res.data[key].length) ? cloneDeep(res.data[key]) : []
          ascOrderAmort = mergeArray(ascOrderAmort, ascOrderAmort.length ? cloneDeep(this.state[key]) : [CONST.Loan.bankLoanAmort])

          if(ascOrderAmort.length) {
            ascOrderAmort = this.sortAmort(ascOrderAmort)
          }
          this.setState({
            [key]: ascOrderAmort,
            tempBankLoanAmort: (ascOrderAmort && ascOrderAmort.length) ? cloneDeep(ascOrderAmort) : [CONST.Loan.bankLoanAmort],
            isEditable: "",
            errorMessages: {},
            isSaveDisabled: false,
          })
        }else {
          toast("something went wrong",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            isSaveDisabled: false,
          })
        }
      })
    })
  }

  render() {
    const { bankLoanAmort, errorMessages, isSaveDisabled, isEditable } = this.state
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return(
      <div className="pricing">
        <Accordion multiple
          activeItem={[0]}
          boxHidden={true} //eslint-disable-line
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(0)} title="Payment Schedule" actionButtons={this.actionButtons("bankLoanAmort")} style={{overflowY: "unset"}}>
                {activeAccordions.includes(0) &&
               <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                 <TableHeader cols={cols}/>
                 {
                   bankLoanAmort.map((amort, index) => {
                     const errors = (errorMessages.bankLoanAmort && errorMessages.bankLoanAmort[index.toString()])
                     return (
                       <PaymentSchedule
                         key={index} index={index} amort={amort} errors={errors} isEditable={isEditable} onBlur={this.onBlur} category="bankLoanAmort" //eslint-disable-line
                         onChangeItem={this.onChangeItem} onRemove={this.onRemove} onEdit={this.onEdit} onSave={this.onSave} onCancel={this.onCancel} isSaveDisabled={isSaveDisabled}
                       />
                     )
                   })
                 }
               </table>
                }
              </RatingSection>
            </div>
          }/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Amortization)
export default connect(mapStateToProps, null)(WrappedComponent)
