import React from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import {putLoansTransaction} from "../../../../StateManagement/actions"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import  TranDocuments from "../../../../GlobalComponents/TranDocuments"
import {pullTransactionDoc, putTransactionDocStatus} from "../../../../StateManagement/actions/Transaction"
import {getDocumentList} from "../../../../../globalutilities/helpers"


class Documents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      documentsList: [],
      loading: true
    }
  }

  async componentWillMount() {
    const {transaction, user} = this.props
    const bankDocuments = (transaction && transaction.bankLoanDocuments && transaction.bankLoanDocuments.length) ? cloneDeep(transaction.bankLoanDocuments) : []
    const filterDocuments = getDocumentList(cloneDeep(bankDocuments), user)

    this.setState({
      documentsList: transaction.actTranFirmId === user.entityId ? bankDocuments : filterDocuments || [],
      loading: false,
    })
  }

  onDocSave = (docs, callback) => {
    const {transaction, user} = this.props
    console.log(docs)
    putLoansTransaction(this.props.nav3, docs, (res)=> {
      if (res && res.status === 200) {
        toast("Bank Loan Documents has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.props.submitAuditLogs(this.props.nav2)
        const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.bankLoanDocuments), user)
        callback({
          status: true,
          documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.bankLoanDocuments : filterDocuments || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    const {transaction, user} = this.props
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        [name]: value
      }
      if(value !== "Public"){
        document.settings = {firms: [], users: [], selectLevelType: ""}
      }
      type = "&type=status"
    }else {
      document = {
        ...doc
      }
    }
    const res = await putTransactionDocStatus(this.props.nav2, `BankLoans${type}`, {_id: doc._id, ...document})
    if (res && res.status === 200) {
      toast("Document has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.document), user)
      if(name){
        callback({
          status: true,
        })
      }else {
        callback({
          status: true,
          documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.document : filterDocuments || [],
        })
      }
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onDeleteDoc = async (documentId, callback) => {
    const {transaction, user} = this.props
    const res = await pullTransactionDoc(this.props.nav2, `?tranType=BankLoans&docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.bankLoanDocuments), user)
      callback({
        status: true,
        documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.bankLoanDocuments : filterDocuments || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  render() {
    const { documentsList } = this.state
    const { transaction, tranAction } = this.props
    const tags = {
      clientId: transaction.actTranClientId,
      clientName: `${transaction.actTranClientName}`,
      tenantId: transaction.actTranFirmId,
      tenantName: transaction.actTranFirmName,
      contextName:transaction.actTranIssueName ? transaction.actTranIssueName : transaction.actTranProjectDescription
    }
    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }
    return(
      <div className="bank-loan-documents">
        <TranDocuments {...this.props} onSave={this.onDocSave} tranId={this.props.nav2} title="Upload Bank Loan Documents" pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickAction="LKUPDOCACTION"
          category="bankLoanDocuments" isDisabled={!tranAction.canTranStatusEditDoc} documents={documentsList} contextType={ContextType.loan} tags={tags} onStatusChange={this.onStatusChange} onDeleteDoc={this.onDeleteDoc}/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Documents)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
