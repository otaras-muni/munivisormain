import React, { Component } from "react"
import Loader from "../../GlobalComponents/Loader"
import LoanView from "./Component"

class BankLoan extends Component {
  render() {
    const { nav2, nav3 } = this.props
    if(nav2) {
      return (
        <div>
          <LoanView {...this.props} option={nav3} />
        </div>
      )
    }
    return <Loader />
  }
}

export default BankLoan
