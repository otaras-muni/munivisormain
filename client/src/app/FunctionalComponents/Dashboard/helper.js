export const getAllTransactions = (payload) => {
  console.log(payload)
  let transactions = []
  Object.keys(payload).forEach(k => {
    transactions = [...transactions, ...payload[k]]
  })

  return transactions
}

export const getTransactionIds = trans => {
  const transactionIds = []
  trans.forEach(transaction => {
    transactionIds.push(transaction.tranId)
  })

  return transactionIds
}
