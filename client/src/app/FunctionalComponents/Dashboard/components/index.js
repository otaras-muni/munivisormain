/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */
import React from "react"
import { ReactiveBase, ReactiveComponent, ReactiveList, DataSearch } from "@appbaseio/reactivesearch"
import { connect } from "react-redux"
import styled from "styled-components"
import { toast } from "react-toastify"
import axios from "axios"

import UserFilter from "./filters/UserFilter"
import StatusFilter from "./filters/StatusFilter"
import DateFilter from "./filters/DateFilter"
import GroupByFilter from "./filters/GroupByFilter"
import TaskResultSet from "./TaskResultSet"
import Nav from "./Nav"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import ReactiveLoader from "../../../GlobalComponents/ReactiveLoader"

import {
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_INDEX,
  ELASTIC_SEARCH_TYPES,
  ELASTIC_SEARCH_CREDENTIALS
} from "../../../../constants"
import { saveDashboardSearchPrefs, updateDashboardSearchPref, fetchDashboardSearchPrefs } from "../../../StateManagement/actions"
import searchConfig from "../../GlobalSearch/searchConfig"
import "../scss/SearchResultItem.scss"
import Loader from "../../../GlobalComponents/Loader"
import PaginationDropDown from "./PaginationDropDown"

const Icon = () => (
  <span style={{ top: "0px", left: "0px", height: "27px", display: "flex", justifyContent: "center", alignItems: "center", borderTopLeftRadius: "3px", borderBottomLeftRadius: "3px", background: "#000", color: "#fff", position: "absolute", width: "1.6rem" }}>
    <i className="fas fa-search" />
  </span>
)

const Result = styled(ReactiveList)`
`

const Base = styled(ReactiveBase)`
  padding: 20px;
  font-family: 'GoogleSans' !important;
`

const mapStateToProps = (state) => {
  const { searchPref: { isFetching, isSaving }, dashboardSearchPref, auth } = state
  return ({
    isFetching,
    isSaving,
    pref: dashboardSearchPref,
    auth,
  })
}

const mapDispatchToProps = (dispatch) => ({
  savePref: () => dispatch(saveDashboardSearchPrefs()),
  updatePref: data => dispatch(updateDashboardSearchPref(data)),
  fetchPrefs: () => dispatch(fetchDashboardSearchPrefs())
})

const getAggregatedResultSet = (aggregations, aggregationType, items) => {
  const result = []
  console.log(aggregations)
  aggregations.forEach((aggregation, i) => {
    const aggTypes = aggregationType.split(".")
    result[i] = {
      key: aggregation.key,
      data: items.filter(item => (item[aggTypes[0]] || {})[aggTypes[1]] === aggregation.key)
    }
  })

  if (aggregationType !== "taskDetails.taskType") {
    result.push({
      key: "General / Compliance",
      data: items.filter(item => (item.taskDetails.taskType === "Compliance" || item.taskDetails.taskType === "general"))
    })
  }
  return result
}

class Dashboard extends React.Component {
  constructor() {
    super()
    this.state = {
      isFilterOpen: true,
      aggregations: null,
      aggregationType: null,
      pageSize: 25,
      isLoading: true,
      entitlements: {}
    }

    this.handleFilterView = this.handleFilterView.bind(this)
    this.updateAggregations = this.updateAggregations.bind(this)
    this.handleFilterChange = this.handleFilterChange.bind(this)
    this.handleSavePref = this.handleSavePref.bind(this)
  }

  componentDidMount() {
    this.props.fetchPrefs()
    this.fetchEntitlements()
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  toggleLoading = () => {
    this.setState(({ isLoading }) => ({
      isLoading: !isLoading,
    }))
  }

  fetchEntitlements = async () => {
    const apiRes = await axios({
      method: "GET",
      url: `${muniApiBaseURL}entitlements/all`,
      headers: { Authorization: this.props.auth.token },
    })

    const { data } = await apiRes.data

    if (!this.isCancelled) {
      this.setState({
        entitlements: data,
      }, () => {
        this.toggleLoading()
      })

    }
  }

  handlePageChange = pageSize => {
    this.setState({
      pageSize,
    })
  }

  handleFilterView() {
    this.setState({
      isFilterOpen: !this.state.isFilterOpen
    })
  }

  updateAggregations(aggregations, aggregationType) {
    this.setState({ aggregations, aggregationType })
  }

  handleFilterChange(componentId, filter) {
    const { updatePref } = this.props
    updatePref({
      [componentId]: filter
    })
  }

  handleSavePref() {
    this.props.savePref()
      .then(() => {
        toast("Preference saved successfully!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
      }).catch(() => {
        toast("Something went wrong!", { autoClose: 2000, type: toast.TYPE.ERROR, })
      })
  }

  render() {
    const { aggregations, aggregationType, pageSize, isLoading, entitlements } = this.state
    const { isSaving, isFetching, pref } = this.props
    const { searchFields, fieldWeights } = searchConfig.tasks.fieldsAndWeigts

    return (
      <div>
        <Nav />
        {
          isFetching || isLoading
            ?
            <Loader />
            :
            <Base
              app={ELASTIC_SEARCH_INDEX}
              url={ELASTIC_SEARCH_URL}
              type={ELASTIC_SEARCH_TYPES.TASKS}
              credentials={ELASTIC_SEARCH_CREDENTIALS}
            >
              <div className="accordions">
                <div className="accordion is-active">
                  <div className="accordion-header" onClick={this.handleFilterView}>
                    <div>View Settings</div>
                    {
                      this.state.isFilterOpen ? <i className="fas fa-chevron-down" style={{ cursor: "pointer" }} />
                        : <i className="fas fa-chevron-up" style={{ cursor: "pointer" }} />
                    }
                  </div>
                  {
                    this.state.isFilterOpen && (
                      <div className="accordion-body">
                        <div className="accordion-content">
                          <div className="columns">
                            <div className="column is-two-fifths">
                              <ReactiveComponent
                                componentId="groupByFilter"
                                react={{
                                  "and": ["userFilter", "dateFilter", "statusFilter"]
                                }}
                              >
                                <GroupByFilter
                                  handleFilterChange={this.handleFilterChange}
                                  updateAggregations={this.updateAggregations}
                                />
                              </ReactiveComponent>
                            </div>
                            <div className="column">
                              <ReactiveComponent
                                componentId="userFilter"
                              >
                                <UserFilter
                                  handleFilterChange={this.handleFilterChange}
                                  entitlements={entitlements}
                                />
                              </ReactiveComponent>
                            </div>
                            <div className="column">
                              <ReactiveComponent
                                componentId="dateFilter"
                              >
                                <DateFilter
                                  handleFilterChange={this.handleFilterChange}
                                />
                              </ReactiveComponent>
                            </div>
                            <div className="column">
                              <ReactiveComponent
                                componentId="statusFilter"
                              >
                                <StatusFilter
                                  handleFilterChange={this.handleFilterChange}
                                />
                              </ReactiveComponent>
                            </div>
                            <div className="column">
                              <button
                                className="button  is-link is-fullwidth is-small is-small"
                                disabled={isSaving}
                                onClick={this.handleSavePref}
                              >
                                {
                                  isSaving
                                    ? "Saving..."
                                    : "Save as my default view"
                                }
                              </button>
                            </div>
                          </div>
                          <div style={{ position: "relative" }}>
                            <DataSearch
                              componentId="taskSearch"
                              dataField={searchFields}
                              autosuggest={false}
                              iconPosition="left"
                              queryFormat="and"
                              placeholder="Search..."
                              fieldWeights={fieldWeights}
                              className="searchbox"
                              highlight
                              innerClass={{
                                input: "input customInput is-link"
                              }}
                              showIcon={false}
                            />
                            <Icon />
                          </div>
                        </div>
                      </div>
                    )
                  }
                </div>
              </div>
              <div className="top-bottom-margin box" style={{ position: "relative", marginTop: "50px" }}>
                <div
                  className="dashPaginationRight"
                >
                  <PaginationDropDown
                    defaultValue={pageSize}
                    handlePageChange={this.handlePageChange}
                  />
                </div>
                <Result
                  componentId="SearchResult"
                  dataField="updatedAt"
                  sortBy="desc"
                  pagination
                  size={pageSize}
                  react={{
                    "and": ["groupByFilter", "userFilter", "dateFilter", "statusFilter", "taskSearch"]
                  }}
                  paginationAt="both"
                  showResultStats
                  onResultStats={
                    (total, time) => (
                      <h3 className="dashPagination">Found {total} records in {time} ms</h3>
                    )
                  }
                  loader={<ReactiveLoader />}
                  renderAllData={(items) =>
                    // if (this.state.aggregations) {
                    //   const result = getAggregatedResultSet(aggregations, aggregationType, items)
                    //   return (
                    //     Object.keys(result).map((aResult, i) => (
                    //       <div key={aResult}>
                    //         {
                    //           Boolean(result[aResult].data.length) && (
                    //             <GroupByResultSet
                    //               title={result[aResult].key}
                    //               items={result[aResult].data}
                    //               showResultSet={i === 0}
                    //             />
                    //           )
                    //         }
                    //       </div>
                    //     ))
                    //   )
                    // }
                    Boolean(items.length) && (
                      <TaskResultSet items={items} />
                    )
                  }
                />
              </div>
            </Base>
        }
      </div>
    )
  }
}

const DashboardContainer = connect(mapStateToProps, mapDispatchToProps)(Dashboard)
export default DashboardContainer
