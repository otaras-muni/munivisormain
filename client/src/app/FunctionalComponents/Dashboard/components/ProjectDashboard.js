/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */

import React from "react"
import { connect } from "react-redux"
import axios from "axios"
import _ from "lodash"
import Nav from "./Nav"
import SearchComponent from "./ProjectDashboardSearch"
import ProjectResultSet from "./ProjectsResultSet"
import { ELASTIC_SEARCH_TYPES, getFieldsAndWeights } from "../../../../constants"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { getAllTransactions, getTransactionIds } from "../helper"
import Loader from "../../../GlobalComponents/Loader"

const mapStateToProps = (state) => ({
  auth: state.auth
})

class ProjectDashboard extends React.Component {
  constructor() {
    super()
    const { searchFields, fieldWeights } = getFieldsAndWeights()
    this.state = {
      isFetching: true,
      entitledIds: [],
      searchFields,
      fieldWeights,
      tranType: `${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DERIVITIVE}`
    }
  }
  componentDidMount() {
    this.fetchEntitledList()
  }

  fetchEntitledList = () => {
    const { auth } = this.props
    axios({
      method: "GET",
      url: `${muniApiBaseURL}entitlements/all`,
      headers: { Authorization: auth.token },
    }).then(res => {
      const { data } = res.data
      const transactionIds = getTransactionIds(getAllTransactions(_.pick(data, ["deals", "marfps", "derivatives", "rfps", "bankloans", "others", "busdev"])))

      // chunks.forEach(c => {
      //   setTimeout(() => {
      //     this.setState((prevState) => ({
      //       entitledIds: [...prevState.entitledIds, ...c]
      //     }))
      //   }, 3000)
      // })
      this.setState({
        isFetching: false,
        entitledIds: transactionIds,
      })
    }).catch(() => {
      this.setState({
        isFetching: false,
        entitledIds: []
      })
    })
  }

  handleTranTypeChange = (tranType) => {
    this.setState({
      tranType
    })
  }

  render() {
    const { isFetching, entitledIds, searchFields, fieldWeights, tranType } = this.state
    let activeGroup = null
    if (tranType === `${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DERIVITIVE}`) {
      activeGroup = "deals"
    }

    if (tranType === `${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.OTHERS},${ELASTIC_SEARCH_TYPES.BUSINESSDEVELOPMENT}`) {
      activeGroup = "rfps"
    }

    if (tranType === `${ELASTIC_SEARCH_TYPES.MARFPS}`) {
      activeGroup = "marfps"
    }
    return (
      <div>
        <Nav />
        <div>
          {
            isFetching
              ?
              <Loader />
              :
              <div className="box overflow-auto" style={{ marginLeft: 20, marginRight: 20 }}>
                <SearchComponent
                  esTypes={tranType}
                  textSearchFields={searchFields}
                  fieldWeights={fieldWeights}
                  SearchResultItems={ProjectResultSet}
                  defaultSearchQuery={
                    () => {
                      if (entitledIds.length) {
                        return ({
                          ids: { values: entitledIds }
                        })
                      }

                      return {
                        match_none: {}
                      }
                    }
                  }
                  activeGroup={activeGroup}
                  updateTranType={this.handleTranTypeChange}
                />
              </div>
          }
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(ProjectDashboard)
