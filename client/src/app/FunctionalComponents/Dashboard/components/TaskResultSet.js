/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */
import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { connect } from "react-redux"

import "react-table/react-table.css"
import "../scss/SearchResultItem.scss"
import dateFormat from "../../../../globalutilities/dateFormat"

const TaskResultSet = ({ items, allUrls,defaultUrls }) => (
  <div style={{ marginTop: "20px"}}>
    <ReactTable
      data={items}
      pageSize={(items || []).length}
      columns={[
        {
          id: "client",
          Header: "Client / Prospect",
          headerClassName: "text-camelcase",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            const displayItem = {
              link: ",/",
              name: ""
            }

            if (item.taskDetails.taskType === "Compliance") {
              displayItem.link = `/cac/view-control?cid=${item._id}`
              displayItem.name = "Compliance Action Center"
            } else if(item.taskDetails.taskType === "general") {
              displayItem.link = `/tasks/${item._id}`
              displayItem.name = "General Task"
            } else {
              displayItem.link = (allUrls[(item.relatedActivityDetails || {}).activityIssuerClientId] || {}).entity || "/"
              displayItem.name = (item.relatedActivityDetails || {}).activityIssuerClientName || "-"
            }
            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={displayItem.link} dangerouslySetInnerHTML={{ __html: displayItem.name }} />

              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => {
            const ar = {}
            const br = {}

            if (a.taskDetails.taskType === "Compliance") {
              ar.name = "Compliance Action Center"
            } else if(a.taskDetails.taskType === "general") {
              ar.name = "General Task"
            } else {
              ar.name = (a.relatedActivityDetails || {}).activityIssuerClientName || ""
            }

            if (b.taskDetails.taskType === "Compliance") {
              br.name = "Compliance Action Center"
            } else if(a.taskDetails.taskType === "general") {
              br.name = "General Task"
            } else {
              br.name = (b.relatedActivityDetails || {}).activityIssuerClientName || ""
            }

            return ar.name.localeCompare(br.name)

          }
        },
        {
          id: "issue",
          Header: "Deal/Issue/Activity name",
          accessor: item => item,
          Cell: row => {
            const item = row.value

            const displayItem = {}

            if (item.taskDetails.taskType === "Compliance") {
              displayItem.link = "/cac/view-control"
              displayItem.name = "Compliance Action Center"
            } else if(item.taskDetails.taskType === "general") {
              displayItem.link = null
              displayItem.name = "General Task"
            } else {
              displayItem.link = ((allUrls[(item.relatedActivityDetails || {}).activityId] || {}).summary || (allUrls[(item.relatedActivityDetails || {}).activityId] || {}).default) || "/"
              displayItem.name = (item.relatedActivityDetails || {}).activityProjectName || "-"
            }

            return (
              <div className="hpTablesTd wrap-cell-text">
                {
                  displayItem.link ? (
                    <Link to={displayItem.link} dangerouslySetInnerHTML={{ __html: displayItem.name}} />)
                    :
                    (<div dangerouslySetInnerHTML={{ __html: displayItem.name}} />)
                }
              </div>
            )
          },
          maxWidth: 320,
          sortMethod: (a, b) => {
            const ar = {}
            const br = {}

            if (a.taskDetails.taskType === "Compliance") {
              ar.name = "Compliance Action Center"
            } else if(a.taskDetails.taskType === "general") {
              ar.name = "General Task"
            } else {
              ar.name = (a.relatedActivityDetails || {}).activityProjectName || ""
            }

            if (b.taskDetails.taskType === "Compliance") {
              br.name = "Compliance Action Center"
            } else if(a.taskDetails.taskType === "general") {
              br.name = "General Task"
            } else {
              br.name = (b.relatedActivityDetails || {}).activityProjectName || ""
            }
            return ar.name.localeCompare(br.name)

          }
        },
        {
          id: "dueDate",
          Header: "Due date",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd -striped wrap-cell-text">{dateFormat(item.taskDetails && item.taskDetails.taskEndDate)}</div>
            )
          },
          maxWidth: 150,
          sortMethod: (a, b) => {
            a = new Date(a.taskDetails.taskEndDate).getTime()
            b = new Date(b.taskDetails.taskEndDate).getTime()
            return b > a ? 1 : -1
          }
        },
        {
          id: "activity",
          Header: "Activity",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: item.taskDetails.taskType || "-" }} />
            )
          },
          sortMethod: (a, b) =>  (a.taskDetails.taskType).localeCompare(b.taskDetails.taskType),
          maxWidth: 150
        },
        {
          id: "todo",
          Header: "To do",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            const displayItem = {}

            if (item.taskDetails.taskType === "Compliance") {
              displayItem.link = `/cac/view-control?cid=${item._id}`
              displayItem.name = "Compliance Action Center"
            } else if(item.taskDetails.taskType === "general") {
              displayItem.link = `/tasks/${item._id}`
              displayItem.name = "General Task"
            } else {
              displayItem.link = `/tasks/${item._id}`
              displayItem.name = item.taskDetails.taskDescription || ""
            }

            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={displayItem.link} dangerouslySetInnerHTML={{ __html: displayItem.name}} />
              </div>
            )
          },
          sortMethod: (a, b) => {
            const ar = {}
            const br = {}
            if (a.taskDetails.taskType === "Compliance") {
              ar.name = "Compliance Action Center"
            } else if(a.taskDetails.taskType === "general") {
              ar.name = "General Task"
            } else {
              ar.name = a.taskDetails.taskDescription || ""
            }

            if (b.taskDetails.taskType === "Compliance") {
              br.name = "Compliance Action Center"
            } else if(b.taskDetails.taskType === "general") {
              br.name = "General Task"
            } else {
              br.name = b.taskDetails.taskDescription || ""
            }

            return ar.name.localeCompare(br.name)

          },
          maxWidth: 250
        },
        {
          id: "assignedTo",
          Header: "Assigned to",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <Link to={defaultUrls[(item.taskDetails || {}).taskAssigneeUserId] || ""} className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.taskDetails && item.taskDetails.taskAssigneeName) }} />
            )
          },
          sortMethod: (a, b) => a.taskDetails && a.taskDetails.taskAssigneeName.localeCompare((b.taskDetails && b.taskDetails.taskAssigneeName) || ""),
          maxWidth: 200
        },
        {
          id: "status",
          Header: "Activity status",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: ((item.taskDetails || {}).taskStatus || "") }} />
            )
          },
          sortMethod: (a, b) => (a.taskDetails && a.taskDetails.taskStatus || "").localeCompare((b.taskDetails && b.taskDetails.taskStatus) || ""),
          maxWidth: 400
        }
      ]}
      minRows={2}
      // PaginationComponent={Pagination}
      className="-striped -highlight is-bordered"
      showPagination={false}
    />
  </div>
)

const mapStateToProps = (state) => ({
  allUrls: state.urls.allurls,
  defaultUrls: state.urls.defaultUrls
})

export default connect(mapStateToProps)(TaskResultSet)
