import React from "react"
import { ELASTIC_SEARCH_TYPES } from "../../../../../constants"


class TypeFilter extends React.Component {

  handleQueryChange = e => {
    const { target } = e
    this.props.updateTranType(target.value)
  }
  render() {
    return (
      <div className="select is-link is-small">
        <select
          onChange={this.handleQueryChange}
          defaultValue={this.props.defaultValue}
          id="tran-types"
        >
          <option value={`${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DERIVITIVE}`}>Deals / Bank Loans / Derivatives</option>
          <option value={`${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.OTHERS},${ELASTIC_SEARCH_TYPES.BUSINESSDEVELOPMENT}`}>RFPs / Others / Business Development</option>
          <option value={`${ELASTIC_SEARCH_TYPES.MARFPS}`}>MARFPs</option>
        </select>
      </div>
    )
  }
}

export default TypeFilter
