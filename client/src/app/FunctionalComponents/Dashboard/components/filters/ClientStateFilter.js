import React from "react"

const ClientStateFiler = ({ searchPref, onChange, states }) => (
    <div className="select is-fullwidth is-link is-small">
        <select value={(searchPref.clientState || "")} onChange={(e) => onChange("clientState", e.target.value)}>
            <option value="">State</option>
            {
                states.map(item => (
                    <option value={item} key={item}>{item}</option>
                ))
            }
        </select>
    </div>
)

export default ClientStateFiler
