/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import { connect } from "react-redux"

const mapStateToProps = state => ({
  auth: state.auth,
  pref: state.dashboardSearchPref
})

class UserFilterComponent extends React.Component {
  componentDidMount() {
    const {
      pref,
      setQuery,
      entitlements
    } = this.props

    if (pref.userFilter && pref.userFilter.value && pref.userFilter.query) {
      setQuery(pref.userFilter)
    } else {
      // const loggedInUserId = userEntities.userId
      const query = {
        query: {
          ids: { values: entitlements.tasks.edit }
        }
      }

      this.props.setQuery({
        query,
        value: "my"
      })
    }
  }

  handleOnChange = e => {
    const {
      target: { value }
    } = e
    const { setQuery, entitlements, auth } = this.props

    switch (value) {
    case "my":
      const query = {
        query: {
          term: { "taskDetails.taskAssigneeUserId": auth.userEntities.userId }
        }
      }
      setQuery({
        query,
        value
      })

      this.props.handleFilterChange("userFilter", {
        query,
        value
      })
      break
    case "myFirm":
      const query2 = {
        bool: {
          must: [
            {
              terms: {
                "taskDetails.taskAssigneeUserId.raw":
                  [...entitlements.consolidated.tranMyFirmUsers,...entitlements.entities.edit]
              }
            },
            {
              terms: {
                "relatedActivityDetails.activityId.raw":
                  entitlements.consolidated.tranViewIds
              }
            }
          ]
        }
      }
      setQuery({
        query: query2,
        value
      })
      this.props.handleFilterChange("userFilter", {
        query: query2,
        value
      })
      break
    case "myDeal":
      const query3 = {
        bool: {
          must: [
            {
              terms: {
                "taskDetails.taskAssigneeUserId.raw":
                  [...entitlements.consolidated.tranMyDealTeamUsers, ...entitlements.consolidated.tranAllFirms]
              }
            },
            {
              terms: {
                "relatedActivityDetails.activityId.raw":
                  entitlements.consolidated.tranViewIds
              }
            }
          ]
        }
      }
      setQuery({
        query: query3,
        value
      })
      this.props.handleFilterChange("userFilter", {
        query: query3,
        value
      })
      break
    case "all":
      const query4 = {
        terms: {
          "taskDetails.taskAssigneeUserId.raw":
        [...entitlements.users.edit, ...entitlements.entities.edit]
        }
      }
      setQuery({
        query: query4,
        value
      })

      this.props.handleFilterChange("userFilter", {
        query: query4,
        value
      })
      break
    }
  }
  render() {
    const { selectedValue } = this.props

    return (
      <div className="select is-fullwidth is-link is-small">
        <select value={selectedValue || ""} onChange={this.handleOnChange}>
          <option value="all">All Activites</option>
          <option value="my">All My Activities</option>
          <option value="myDeal">All My Deal Activities</option>
          <option value="myFirm">All My Firm Activities</option>
        </select>
      </div>
    )
  }
}
export default connect(mapStateToProps)(UserFilterComponent)
