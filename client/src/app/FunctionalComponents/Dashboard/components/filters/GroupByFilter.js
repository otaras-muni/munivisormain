import React, { Fragment } from "react"
import { connect } from "react-redux"
import axios from "axios"
import { Multiselect } from "react-widgets"

import {
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_INDEX,
  ELASTIC_SEARCH_TYPES
} from "../../../../../constants"
import { getHeaders } from "../../../../../globalutilities"

const mapStateToProps = state => {
  const { dashboardSearchPref } = state
  return {
    pref: dashboardSearchPref
  }
}

class GroupByFilter extends React.Component {
  componentDidMount() {
    this.initApp()
  }

  getAggregrations = async (query, value, field) => {
    const apiRes = await axios.post(
      `${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/${
      ELASTIC_SEARCH_TYPES.TASKS
      }/_search`,
      {
        size: 0,
        ...query
      },
      { headers: getHeaders() }
    )

    const { aggregations } = await apiRes.data

    const data = {
      subFilters: aggregations
        ? aggregations.group.buckets.map(i => i.key)
        : [],
      filterName: value,
      filterValue: field
    }

    this.props.handleFilterChange("groupByFilter", { query, ...data })
    return true
  }

  initApp = async () => {
    const { pref, setQuery } = this.props
    if (pref.groupByFilter && pref.groupByFilter.filterName) {
      await this.getAggregrations(
        pref.groupByFilter.query,
        pref.groupByFilter.filterName,
        pref.groupByFilter.filterValue
      )

      if (pref.groupBySubFilter && pref.groupBySubFilter.value) {
        setQuery({
          query: pref.groupBySubFilter.query,
          value: pref.groupBySubFilter.value
        })
      }
    }
  }

  handleSubFilterChange = data => {
    const { filterName, filterValue } = this.props.pref.groupByFilter

    let query = {}

    if (data.length) {
      if (filterName !== "activityType") {
        if (data.indexOf("general") > -1 || data.indexOf("Compliance") > -1) {
          const typesArr = []
          if (data.indexOf("general") > -1) {
            typesArr.push("general")
          }
          if (data.indexOf("Compliance") > -1) {
            typesArr.push("Compliance")
          }
          query = {
            bool: {
              should: [
                { terms: { "taskDetails.taskType.raw": typesArr } },
                { terms: { [filterValue]: data } }
              ]
            }
          }
        } else {
          query = {
            terms: { [filterValue]: data }
          }
        }
      } else {
        query = {
          terms: { [filterValue]: data }
        }
      }
    }

    this.props.setQuery({
      query,
      data
    })

    this.props.handleFilterChange("groupBySubFilter", { query, data })
  }

  handleSelectChange = e => {
    const {
      target: { value }
    } = e
    let query = {}
    let field = ""
    switch (value) {
      case "client":
        field = "relatedActivityDetails.activityIssuerClientName.raw"
        query = {
          aggs: {
            group: {
              terms: {
                field
              }
            }
          }
        }
        break
      case "activityType":
        field = "taskDetails.taskType.raw"
        query = {
          aggs: {
            group: {
              terms: {
                field: "taskDetails.taskType.raw"
              }
            }
          }
        }
        break
      case "activityProjectName":
        field = "relatedActivityDetails.activityProjectName.raw"
        query = {
          aggs: {
            group: {
              terms: {
                field: "relatedActivityDetails.activityProjectName.raw"
              }
            }
          }
        }
        break
      default:
        query = { aggs: {} }
        break
    }

    if (value) {
      this.getAggregrations(query, value, field)
    } else {
      const data = {
        subFilters: [],
        filterName: "",
        filterValue: ""
      }
      this.props.setQuery({
        query: {},
        value: ""
      })
      this.props.handleFilterChange("groupByFilter", { query, ...data })
      this.props.handleFilterChange("groupBySubFilter", {
        query: {},
        value: ""
      })
    }
  }

  handleReset = () => {
    const data = {
      subFilters: [],
      filterName: "",
      filterValue: ""
    }
    this.props.setQuery({
      query: {},
      value: ""
    })
    this.props.handleFilterChange("groupByFilter", { query: {}, ...data })
    this.props.handleFilterChange("groupBySubFilter", {
      query: {},
      value: ""
    })
  }
  render() {
    const { pref } = this.props
    return (
      <div className="columns">
        <div className="column">
          <div className="select is-link is-small is-fullwidth">
            <select
              onChange={this.handleSelectChange}
              defaultValue={(pref.groupByFilter || {}).filterName || ""}
            >
              <option value="">Group by</option>
              <option value="activityProjectName">Activity Name</option>
              <option value="activityType">Activity Type</option>
              <option value="client">Client</option>
            </select>
          </div>
        </div>
        {pref.groupByFilter && pref.groupByFilter.filterName && (
          <div className="column" style={{ marginLeft: -15 }}>
            <div className="field is-grouped">
              <Multiselect
                name="subfilter"
                data={pref.groupByFilter.filterName !== "activityType" ? ["Compliance", "general", ...pref.groupByFilter.subFilters] : [...pref.groupByFilter.subFilters]}
                defaultValue={Array.isArray((pref.groupBySubFilter || {}).value) ? pref.groupBySubFilter || {}.value : []}
                onChange={this.handleSubFilterChange}
                placeholder={`Select ${pref.groupByFilter.filterName}`}
                className="is-link is-small"
                style={{ marginRight: 1, fontSize: 12 }}
              />
              <button onClick={this.handleReset} className="button  is-light is-small">Reset</button>
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default connect(mapStateToProps)(GroupByFilter)
