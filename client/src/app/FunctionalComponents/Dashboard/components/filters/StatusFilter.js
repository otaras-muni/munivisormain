import React from "react"
import { connect } from "react-redux"

const mapStateToProps = state => {
    const { dashboardSearchPref } = state
    return {
        pref: dashboardSearchPref,
    }
}
class StatusFilter extends React.Component {
    constructor() {
        super()
        this.handleSelectChange = this.handleSelectChange.bind(this)
    }

    componentDidMount() {
        const { pref, setQuery } = this.props

        if (pref.statusFilter && pref.statusFilter.value && pref.statusFilter.query) {
            setQuery(pref.statusFilter)
        }
    }

    handleSelectChange(e) {
        const { target: { value } } = e
        let query = {}
        switch (value) {
            case "open":
                query = {
                    "bool": {
                        "should": [
                            { "term": { "taskDetails.taskStatus.raw": "Open" } },
                            { "term": { "taskDetails.taskStatus.raw": "Active" } }
                        ]
                    }
                }
                break
            case "close":
                query = {
                    "bool": {
                        "should": [
                            { "term": { "taskDetails.taskStatus.raw": "Closed" } },
                            { "term": { "taskDetails.taskStatus.raw": "Cancelled" } }
                        ]
                    }
                }
                break
            default:
                query = {}
                break
        }
        this.props.setQuery({
            query,
            value
        })
        this.props.handleFilterChange("statusFilter", {
            query,
            value
        })
    }

    render() {
        return (
            <div className="select is-fullwidth is-link is-small">
                <select
                    onChange={this.handleSelectChange}
                    defaultValue={(this.props.pref.statusFilter || {}).value || "all"}
                >
                    <option value="all">All activities</option>
                    <option value="open">Open or active</option>
                    <option value="close">Closed or cancelled</option>
                </select>
            </div>
        )
    }
}

export default connect(mapStateToProps)(StatusFilter)
