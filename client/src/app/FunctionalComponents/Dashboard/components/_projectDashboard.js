/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */

import React from "react"
import moment from "moment"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { numberWithCommas } from "../../../../globalutilities/helpers"
import dateFormat from "../../../../globalutilities/dateFormat"
import getParAmount from "../../../../globalutilities/amountDecorator"

const filterData = (data) => {
  const result = {
    deals: [],
    rfps: [],
    marfps: [],
  }

  data.forEach(item => {
    if (item.dealIssueTranIssueName || item.dealIssueTranProjectDescription) result.deals.push(item)
    if (item.rfpTranName) result.rfps.push(item)
    if (item.maRfpSummary) result.marfps.push(item)
    if (item.bankLoanParticipants) result.deals.push(item)
    if (item.derivativeSummary) result.deals.push(item)
  })

  return result
}

const leadManager = (participants, nameKey, idKey, typeKey) => {
  const filteredList = (participants || []).find(f => f[typeKey] === "Tenant")
  return filteredList ? {
    name: filteredList[nameKey],
    id: filteredList[idKey]
  } : null
}

const getLink = (item) => {
  if (item.dealIssueTranIssueName || item.dealIssueTranProjectDescription) return `/deals/${item._id}/summary`
  if (item.bankLoanParticipants) return `/loan/${item._id}/summary`
  if (item.derivativeSummary) return `/derivative/${item._id}/summary`
  return ""
}

const dealColumns = [
  {
    id: "issuer",
    Header: "Issuer Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/clients-propects/${item.dealIssueTranIssuerId || item.actTranClientId}/entity`} dangerouslySetInnerHTML={{ __html: (item.dealIssueTranIssuerFirmName || item.actTranClientName || "-") }} />

        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.dealIssueTranIssuerFirmName || a.actTranClientName || "").localeCompare((b.dealIssueTranIssuerFirmName || b.actTranClientName || ""))
  },
  {
    id: "description",
    Header: "Issue Description/Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={getLink(item)} dangerouslySetInnerHTML={{ __html: ((item.dealIssueTranIssueName || item.dealIssueTranProjectDescription) || (item.actTranIssueName || item.actTranProjectDescription)) }} />
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => ((a.dealIssueTranIssueName || a.dealIssueTranProjectDescription) || (a.actTranIssueName || a.actTranProjectDescription || "")).localeCompare(b.dealIssueTranIssueName || (b.actTranIssueName || b.actTranProjectDescription || ""))
  },
  {
    id: "type",
    Header: "Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.dealIssueTranType || (`${item.actTranType} / ${item.actTranSubType}`)) }}>
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.dealIssueTranType || (`${a.actTranType} / ${a.actTranSubType}`)).localeCompare((b.dealIssueTranType || (`${b.actTranType} / ${b.actTranSubType}`)))
  },
  {
    id: "principalAmount",
    Header: ()=> (<span title="Principal Amount($)">Principal Amount($)</span>),
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const amt = getParAmount(item)
      return (
        <div className="hpTablesTd wrap-cell-text">
          {numberWithCommas(amt > 0 ? amt : "-")}
        </div>
      )
    },
    maxWidth: 150,
    sortMethod: (a, b) => getParAmount(a) - getParAmount(b)
  },
  {
    id: "leadManager",
    Header: "Lead Manager",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const val = item.dealIssueParticipants
        ? leadManager(item.dealIssueParticipants, "dealPartContactName", "dealPartContactId", "dealPartType")
        : leadManager((item.derivativeParticipants || item.bankLoanParticipants), "partContactName", "partContactId", "partType")
      return (
        <Link to={`/admin-users/${val ? val.id : ""}/users`} className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{
          __html: (val ? val.name : "-")
        }} />
      )
    },
    sortMethod: (a, b) => {
      const aval = a.dealIssueParticipants
        ? leadManager(a.dealIssueParticipants, "dealPartContactName", "dealPartContactId", "dealPartType")
        : leadManager((a.derivativeParticipants || a.bankLoanParticipants), "partContactName", "partContactId", "partType")
      const bval = b.dealIssueParticipants
        ? leadManager(b.dealIssueParticipants, "dealPartContactName", "dealPartContactId", "dealPartType")
        : leadManager((b.derivativeParticipants || b.bankLoanParticipants), "partContactName", "partContactId", "partType")

      return (aval || {}).name.localeCompare((bval || {}).name)
    },
    maxWidth: 150
  },
  {
    id: "couponType",
    Header: "Coupon Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <p dangerouslySetInnerHTML={{
            __html: (((item.dealIssueSeriesDetails || {}).seriesPricingDetails || {}).dealSeriesRateType ||
              (item.bankLoanTerms || {}).paymentType ||
              (item.derivTradeClientPayLeg ? `client: ${(item.derivTradeClientPayLeg || {}).paymentType || "-"} / dealer: ${(item.derivTradeDealerPayLeg || {}).paymentType || "-"}` : "-"))
          }}></p>



        </div>
      )
    },
    sortMethod: (a, b) => (((a.dealIssueSeriesDetails || {}).seriesPricingDetails || {}).dealSeriesRateType ||
      (a.bankLoanTerms || {}).paymentType ||
      a.derivTradeClientPayLeg ? `client: ${(a.derivTradeClientPayLeg || {}).paymentType || ""} / dealer: ${(a.derivTradeDealerPayLeg || {}).paymentType || ""}` : "").localeCompare((((b.dealIssueSeriesDetails || {}).seriesPricingDetails || {}).dealSeriesRateType ||
        (b.bankLoanTerms || {}).paymentType ||
        b.derivTradeClientPayLeg ? `client: ${(b.derivTradeClientPayLeg || {}).paymentType || ""} / dealer: ${(b.derivTradeDealerPayLeg || {}).paymentType || ""}` : "")),
    maxWidth: 150
  },
  {
    id: "purposeSector",
    Header: "Purpose/Sector",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <p dangerouslySetInnerHTML={{ __html: (item.dealIssueTranPrimarySector || item.actTranPrimarySector || "-") }} ></p>

        </div>
      )
    },
    sortMethod: (a, b) => (a.dealIssueTranPrimarySector || a.actTranPrimarySector || "").localeCompare(b.dealIssueTranPrimarySector || b.actTranPrimarySector || ""),
    maxWidth: 150
  },
  {
    id: "pricingDate",
    Header: "Expected Pricing Date",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {dateFormat(
            item.dealIssuePricingDate ||
            (item.bankLoanSummary || {}).actTranClosingDate ||
            (item.derivativeSummary || {}).tranTradeDate
          )}
        </div>
      )
    },
    sortMethod: (a, b) => {
      a = moment(
        a.dealIssuePricingDate ||
        (a.bankLoanSummary || {}).actTranClosingDate ||
        (a.derivativeSummary || {}).tranTradeDate
      )
      b = moment(
        b.dealIssuePricingDate ||
        (b.bankLoanSummary || {}).actTranClosingDate ||
        (b.derivativeSummary || {}).tranTradeDate
      )
      return a.diff(b)
    },
    maxWidth: 200,
    minWidth: 100
  },
  {
    id: "closingDate",
    Header: "Expected Closing Date",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {dateFormat(
            item.dealIssueTranExpectedEndDate ||
            (item.bankLoanSummary || {}).actTranClosingDate ||
            (item.derivativeSummary || {}).tranEndDate
          )}
        </div>
      )
    },
    sortMethod: (a, b) => {
      a = moment(
        a.dealIssueTranExpectedEndDate ||
        (a.bankLoanSummary || {}).actTranClosingDate ||
        (a.derivativeSummary || {}).tranEndDate
      )
      b = moment(
        b.dealIssueTranExpectedEndDate ||
        (b.bankLoanSummary || {}).actTranClosingDate ||
        (b.derivativeSummary || {}).tranEndDate
      )
      return a.diff(b)
    },
    maxWidth: 200,
    minWidth: 100
  },
  {
    id: "status",
    Header: "Status",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{
          __html: (item.dealIssueTranStatus ||
            (item.bankLoanSummary || {}).actTranStatus ||
            (item.derivativeSummary || {}).tranStatus || "-")
        }} />
      )
    },
    sortMethod: (a, b) => (a.dealIssueTranStatus ||
      (a.bankLoanSummary || {}).actTranStatus ||
      (a.derivativeSummary || {}).tranStatus || "").localeCompare((b.dealIssueTranStatus ||
        (b.bankLoanSummary || {}).actTranStatus ||
        (b.derivativeSummary || {}).tranStatus || "")),
    maxWidth: 150
  },
]

const rfpColumns = [
  {
    id: "issuer",
    Header: "Issuer Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/clients-propects/${item.rfpTranIssuerId}/entity`} dangerouslySetInnerHTML={{ __html: (item.rfpTranIssuerFirmName || "-") }} />

        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.rfpTranIssuerFirmName || "").localeCompare((b.rfpTranIssuerFirmName || ""))
  },
  {
    id: "issueDescription",
    Header: "Issue Description/Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/rfp/${item._id}/summary`} dangerouslySetInnerHTML={{ __html: (item.rfpTranIssueName || item.rfpTranProjectDescription || "-") }} />

        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.rfpTranIssueName || a.rfpTranProjectDescription || "").localeCompare((b.rfpTranIssueName || b.rfpTranProjectDescription || ""))
  },
  {
    id: "type",
    Header: "Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.rfpTranType || "-") }} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.rfpTranType || "").localeCompare((b.rfpTranType || ""))
  },
  {
    id: "principalAmount",
    Header: ()=> (<span title="Principal Amount($)">Principal Amount($)</span>),
    accessor: item => item,
    Cell: row => { // eslint-disable-line
      return (
        <div className="hpTablesTd wrap-cell-text">
          -
        </div>
      )
    },
    maxWidth: 150,
  },
  {
    id: "leadManager",
    Header: "Lead Manager",
    accessor: item => item,
    Cell: row => { // eslint-disable-line
      const item = row.value
      const manager = item.rfpEvaluationTeam.find(f => f.rfpSelEvalContactId === item.rfpTranAssignedTo[0])
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/admin-users/${manager.rfpSelEvalContactId}/users`}>{manager.rfpSelEvalContactName || "-"}</Link>

        </div>
      )
    },
    maxWidth: 150,
  },
  {
    id: "couponType",
    Header: "Coupon Type",
    accessor: item => item,
    Cell: row => { // eslint-disable-line
      return (
        <div className="hpTablesTd wrap-cell-text">
          -
        </div>
      )
    },
    maxWidth: 150,
  },
  {
    id: "purposeSector",
    Header: "Purpose/Sector",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <p dangerouslySetInnerHTML={{ __html: (item.rfpTranPrimarySector || "-") }}></p>

        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.rfpTranPrimarySector || "").localeCompare((b.rfpTranPrimarySector || ""))
  },
  {
    id: "startDate",
    Header: "Start Date",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {dateFormat(
            item.rfpTranStartDate
          )}
        </div>
      )
    },
    sortMethod: (a, b) => {
      a = moment(
        a.rfpTranStartDate
      )
      b = moment(
        b.rfpTranStartDate
      )
      return a.diff(b)
    },
    maxWidth: 200,
    minWidth: 100
  },
  {
    id: "endDate",
    Header: "End Date",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {dateFormat(
            item.rfpTranExpectedEndDate
          )}
        </div>
      )
    },
    sortMethod: (a, b) => {
      a = moment(
        a.rfpTranExpectedEndDate
      )
      b = moment(
        b.rfpTranExpectedEndDate
      )
      return a.diff(b)
    },
    maxWidth: 200,
    minWidth: 100
  },
  {
    id: "status",
    Header: "Status",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.rfpTranStatus || "-") }} />
      )
    },
    sortMethod: (a, b) => (a.rfpTranStatus || "").localeCompare((b.rfpTranStatus || "")),
    maxWidth: 150
  },
]


const marfpColumns = [
  {
    id: "issuer",
    Header: "Issuer Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/clients-propects/${item.actIssuerClient}/entity`} dangerouslySetInnerHTML={{ __html: (item.actIssuerClientEntityName || "-") }} /> <span>{(item.actIssuerClientEntityName || "-")}</span>
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.actIssuerClientEntityName || "").localeCompare((b.actIssuerClientEntityName || ""))
  },
  {
    id: "issueDescription",
    Header: "Activity Description/Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/marfp/${item._id}/summary`} dangerouslySetInnerHTML={{ __html: (item.actProjectName || "-") }} />

        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.actProjectName || "").localeCompare((b.actProjectName || ""))
  },
  {
    id: "type",
    Header: "Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text" >
          {item.actType || "-"}

        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.actType || "").localeCompare((b.actType || ""))
  },
  {
    id: "leadManager",
    Header: "Lead Manager",
    accessor: item => item,
    Cell: row => { // eslint-disable-line
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link to={`/admin-users/${item.actLeadFinAdvClientEntityId}/users`}>{item.actLeadFinAdvClientEntityName || "-"}</Link>

        </div>
      )
    },
    maxWidth: 150,
    minWidth: 100,
    sortMethod: (a, b) => (a.actLeadFinAdvClientEntityName || "").localeCompare((b.actLeadFinAdvClientEntityName || ""))
  },
  {
    id: "purposeSector",
    Header: "Purpose/Sector",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <p dangerouslySetInnerHTML={{ __html: (item.actPrimarySector || "-") }} ></p>

        </div>
      )
    },
    sortMethod: (a, b) => (a.actPrimarySector || "").localeCompare(b.actPrimarySector || ""),
    maxWidth: 150,
    minWidth: 100
  },
  {
    id: "dueDate",
    Header: "Due Date",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {dateFormat(
            item.actExpAwaredDate
          )}
        </div>
      )
    },
    sortMethod: (a, b) => {
      a = moment(
        a.actExpAwaredDate
      )
      b = moment(
        b.actExpAwaredDate
      )
      return a.diff(b)
    },
    maxWidth: 200,
    minWidth: 100
  },
  {
    id: "status",
    Header: "Status",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text " dangerouslySetInnerHTML={{ __html: (item.actStatus || "-") }} />
      )
    },
    sortMethod: (a, b) => (a.actStatus || "").localeCompare((b.actStatus || "")),
    minWidth: 100,
    maxWidth: 250
  },
]

class SearchResultItems extends React.Component {
  constructor(props) {
    super(props)
    const res = filterData(props.data)
    this.state = {
      isDealOpen: true,
      isRFPOpen: Boolean(res.rfps.length),
      isMarfpOpen: Boolean(res.marfps.length),
      res,
    }
  }

  componentWillReceiveProps(nextProps) {
    const res = filterData(nextProps.data)
    this.updateState(res)
  }

  updateState = (res) => {
    this.setState({
      isDealOpen: true,
      isRFPOpen: Boolean(res.rfps.length),
      isMarfpOpen: Boolean(res.marfps.length),
      res,
    })
  }

  handleToggle = (group) => {
    this.setState({
      [group]: !this.state[group]
    })
  }

  render() {
    const { res, data } = this.props

    return (
      <div className="accordions">
        <div className={`accordion ${this.state.isDealOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isDealOpen") }}>
            <span>Transactions</span>
            <div>
              {
                this.state.isDealOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div></div>
          {
            this.state.isDealOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div >
                    <ReactTable
                      data={this.state.res.deals}
                      columns={dealColumns}
                      pageSize={this.state.res.deals.length}
                      minRows={2}
                      style={{
                        maxHeight: "400px"
                      }}
                      // PaginationComponent={Pagination}
                      showPagination={false}
                      className="-striped -highlight is-bordered"
                      // showPageSizeOptions={false}
                      showPageJump
                    />
                  </div>
                </div>
              </div>
            )
          }
        </div>
        <div className={`accordion ${this.state.isRFPOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isRFPOpen") }}>
            <span>Activity (Non-Transactions)</span>
            <div>
              {
                this.state.isRFPOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div></div>
          {
            this.state.isRFPOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div >
                    <ReactTable
                      data={this.state.res.rfps}
                      columns={rfpColumns}
                      showPagination={false}
                      pageSize={this.state.res.rfps.length}
                      minRows={2}
                      className="-striped -highlight is-bordered"
                      style={{
                        maxHeight: "400px"
                      }}
                    // PaginationComponent={Pagination}
                    />
                  </div>
                </div>
              </div>
            )
          }
        </div>
        <div className={`accordion ${this.state.isMarfpOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isMarfpOpen") }}>
            <span>MA-RFP</span>
            <div>
              {
                this.state.isRFPOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div></div>
          {
            this.state.isMarfpOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div>
                    <ReactTable
                      data={this.state.res.marfps}
                      pageSize={this.state.res.marfps.length}
                      columns={marfpColumns}
                      showPagination={false}
                      minRows={2}
                      className="-striped -highlight is-bordered"
                      style={{
                        maxHeight: "400px"
                      }}
                    // PaginationComponent={Pagination}
                    />
                  </div>
                </div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}

export default SearchResultItems
