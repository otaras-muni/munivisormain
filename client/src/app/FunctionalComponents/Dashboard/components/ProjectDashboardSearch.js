/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import React, { Component } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

import {
  ReactiveBase,
  DataSearch,
  ReactiveList
} from "@appbaseio/reactivesearch"
import {
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_INDEX,
  ELASTIC_SEARCH_CREDENTIALS
} from "../../../../constants"
import PaginationDropDown from "./PaginationDropDown"
import TranTypeFilter from "./filters/TranTypeFilter"
import ReactiveLoader from "../../../GlobalComponents/ReactiveLoader"

const Icon = () => (
  <span className="icon is-left searchIcon">
    <i className="fas fa-search" />
  </span>
)

const Base = styled(ReactiveBase)`
  padding: 0px;
  font-family: 'GoogleSans' !important;
`


class Search extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isFilterOpen: this.props.isFilterOpen || false,
      pageSize: 10,
    }

    this.handleFilterToggle = this.handleFilterToggle.bind(this)
  }

  handleFilterToggle() {
    this.setState({
      isFilterOpen: !this.state.isFilterOpen
    })
  }

  handlePageChange = pageSize => {
    this.setState({
      pageSize,
    })
  }

  render() {
    const {
      esTypes,
      facets,
      textSearchFields,
      searchBarPlaceHolder,
      SearchResultItems,
      fieldWeights,
      defaultSearchQuery,
      reactiveFacets,
      activeGroup
    } = this.props
    const { pageSize } = this.state

    return (
      <Base
        app={ELASTIC_SEARCH_INDEX}
        url={ELASTIC_SEARCH_URL}
        credentials={ELASTIC_SEARCH_CREDENTIALS}
        type={esTypes}
      >
        <div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              padding: "20px"
            }}
          >
            <TranTypeFilter defaultValue={esTypes} updateTranType={this.props.updateTranType} />
            <div className="searchContainer">
              <DataSearch
                componentId="mainSearch"
                dataField={textSearchFields}
                autosuggest={false}
                iconPosition="left"
                queryFormat="and"
                placeholder={searchBarPlaceHolder}
                fieldWeights={fieldWeights}
                className="searchbox"
                highlight
                innerClass={{
                  input: "input customInput is-link"
                }}
                showIcon={false}
              />
              <Icon />
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column" style={{ position: "relative" }}>
            <div className="search-result-container" style={{ position: "relative" }}>
              <div
                className="dashPaginationRightProj"
              >
                <PaginationDropDown
                  defaultValue={pageSize}
                  handlePageChange={this.handlePageChange}
                />
              </div>
              <ReactiveList
                key={activeGroup}
                componentId="ResultCard"
                dataField="created_at"
                size={pageSize}
                pagination
                loader={<ReactiveLoader />}
                urlParams={false}
                defaultQuery={defaultSearchQuery}
                react={{
                  and: [
                    "mainSearch"
                  ]
                }}
                paginationAt="both"
                renderAllData={(res) => (
                  Boolean(res.length) && (
                    <div>
                      <SearchResultItems
                        data={res}
                        activeGroup={activeGroup}
                      />
                    </div>
                  )
                )}
                showResultStats
                onResultStats={
                  (total, time) => (
                    <h3 className="dashPaginationProj">Found {total} records in {time} ms</h3>
                  )
                }
              />
            </div>
          </div>
        </div>
      </Base>
    )
  }
}

Search.propTypes = {
  esTypes: PropTypes.string.isRequired,
  facets: PropTypes.array,
  textSearchFields: PropTypes.array.isRequired,
  searchBarPlaceHolder: PropTypes.string,
  SearchResultItems: PropTypes.func.isRequired,
  fieldWeights: PropTypes.array,
  defaultSearchQuery: PropTypes.func,
  reactiveFacets: PropTypes.array
}

Search.defaultProps = {
  facets: [],
  reactiveFacets: [],
  searchBarPlaceHolder: "Search..",
  fieldWeights: [],
  defaultSearchQuery: () => ({})
}

export default Search
