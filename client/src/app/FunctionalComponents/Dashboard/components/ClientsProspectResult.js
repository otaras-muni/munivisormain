import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { connect } from "react-redux"
import "react-table/react-table.css"
import "../scss/SearchResultItem.scss"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const Result = ({
  items,
  page,
  title,
  pageSize,
  onPageSizeChange,
  onPageChange,
  search,
  allUrls
}) => {
  const columns = [
    {
      id: "type",
      Header: "Type",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.firmType || "-", search)
            }}
          />
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.firmType.localeCompare(b.firmType)
    },
    {
      id: "name",
      Header: "Firm Name",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <Link
              to={`/clients-propects/${item.firmId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.firmName || "-", search)
              }}
            />
          </div>
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.firmName.localeCompare(b.firmName)
    },
    {
      id: "city",
      Header: "City",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.city || "-", search)
            }}
          />
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.city.localeCompare(b.city)
    },
    {
      id: "state",
      Header: "State",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.state || "-", search)
            }}
          />
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.state.localeCompare(b.state)
    },
    {
      id: "country",
      Header: "Country",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.country || "-", search)
            }}
          />
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.country.localeCompare(b.country)
    },
    {
      id: "primaryContactName",
      Header: "Primary Contact name",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <Link
              to={
                item.primaryContactUserId
                  ? allUrls[item.primaryContactUserId].cltprops || ""
                  : ""
              }
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.primaryContactName || "-", search)
              }}
            />
          </div>
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.addressName.localeCompare(b.addressName)
    },
    {
      id: "primaryContact",
      Header: "Primary Contact",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.primaryContact || "-", search)
            }}
          />
        )
      },
      maxWidth: 250,
      sortMethod: (a, b) => a.primaryContact.localeCompare(b.primaryContact)
    }
  ]
  return (
    <ReactTable
      data={items}
      columns={columns}
      defaultPageSize={20}
      minRows={2}
      // PaginationComponent={Pagination}
      showPaginationBottom
      className="-striped -highlight is-bordered"
      // showPageSizeOptions={false}
      showPageJump
      pageSize={pageSize}
      page={page}
      onPageSizeChange={pageSize => onPageSizeChange(title, pageSize)}
      onPageChange={page => onPageChange(title, page)}
    />
  )
}

const mapStateToProps = state => ({
  allUrls: state.urls.allurls
})

export default connect(mapStateToProps)(Result)
