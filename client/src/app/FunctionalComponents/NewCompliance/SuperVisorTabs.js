import React from "react"
import Loader from "../../GlobalComponents/Loader"
import Disclaimer from "../../GlobalComponents/Disclaimer"

class SuperVisorTabs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      componentToRender:null,
      selectedLabel:null,
      ROWS:[1,2],
    }
  }

  componentWillMount() {
    const {ROWS} = this.state
    const {TILES} = this.props
    this.setState({
      loading: false,
      TILES,
      ROWS
    })
  }

  identifyComponent(selection){
    const {TABS, svControls} = this.props
    let TO = ""
    if(selection === "cmp-sup-complaints" && !(svControls && svControls.supervisor)){
      const index = TABS[selection].findIndex(i => i.path === "complaints")
      TO = `/compliance/${selection}/${TABS[selection][index].path}`
    }
    const to = selection === "cac" ? "cac" : !(svControls && svControls.supervisor) && selection === "cmp-sup-complaints" ? TO : ""
    this.props.history.push(to || `/compliance/${selection}/${TABS[selection][0].path}`)
  }

  renderSupervisorTiles = (rows, tiles) => {
    return (
      <section className="container">
        {rows.map( r=>
          <div key={r} className="tile is-ancestor">
            {
              tiles.filter( ({row})=> row===r).map(t =>
                <div key={t.path} style={{cursor: "pointer"}} className="tile is-parent" onClick={t.label ? () => this.identifyComponent(t.path) : () => {}}>
                  <article className={`tile is-child has-text-centered ${t.label ? "box" : ""} ${t.path === "cac" ? "" : "is-gray"}`}>
                    <p className="subtitle">{t.label}</p>
                    <hr/>
                    <p className="multiExpTblVal">{t.subTitle1 || ""}</p>
                    <p className="multiExpTblVal">{t.subTitle2 || ""}</p>
                  </article>
                </div>)
            }
          </div>
        )}
      </section>
    )
  }


  renderSelectedViewFromState =(sel) => {
    const { TILES } = this.state
    const ret = TILES.filter( t => t.path === sel )
    const {component:Component} = ret[0]
    const newProps = {...this.props,componentDisplayClass:this.state.componentDisplayClass}
    return <Component {...newProps} />
  }

  render() {
    const {user} = this.props
    const {componentToRender} = this.state
    const loading = () => <Loader/>

    if (this.state.loading) {
      return loading()
    }

    const renderView = componentToRender ?
      <div className="columns">
        {this.renderSelectedViewFromState(componentToRender)}
      </div> :
      <div>
        <section className="container has-text-centered">
          <p className="title">{(user && user.firmName) || ""}</p>
          <p className="multiExpLbl ">Implement &amp; enforce compliance policies &amp; supervisory procedures.</p>
          <p className="multiExpTblVal">
            <small>Obligations under MSRB Rule G-44</small>
            <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank">
              <i className="fas fa-info-circle" />
            </a>
          </p>
        </section>
        <hr/>
        {this.renderSupervisorTiles(this.state.ROWS, this.state.TILES)}
        <hr/>
        <Disclaimer/>
      </div>

    return (
      <div id="main">
        {renderView}
      </div>
    )
  }
}

export default SuperVisorTabs
