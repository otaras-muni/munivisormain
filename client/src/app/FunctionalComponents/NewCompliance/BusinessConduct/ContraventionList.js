import React from "react"
import ReactTable from "react-table"
import "react-table/react-table.css"
import moment from "moment"


const ContraventionList = ({
  list,
  onRemove,
  onEdit,
  canSupervisorEdit,
  pageSizeOptions = [5, 10, 20, 50, 100],
}) => {
  const action = []

  if(canSupervisorEdit){
    action.push({
      id: "edit",
      Header: "Edit",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div className="field is-grouped">
              <div className="control">
                <a onClick={() => onEdit(item)}>
                  <span className="has-text-link">
                    <i className="fas fa-pencil-alt" />
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={() => onRemove(item)}>
                  <span className="has-text-link">
                    <i className="far fa-trash-alt" />
                  </span>
                </a>
              </div>
            </div>
          </div>
        )
      },
    })
  }
  const columns = [
    {
      id: "assPersonName",
      Header: "Associated Person Name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.assPersonName || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.assPersonName || "").localeCompare((b.assPersonName || ""))
    },
    {
      id: "violationType",
      Header: "Violation",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.violationType || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.violationType || "").localeCompare((b.violationType || ""))
    },
    {
      id: "recordDate",
      Header: "Record Date",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        const data = (item.recordDate) ? moment(item.recordDate).format("MM-DD-YYYY") : "-" || "-"
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (data) }} />
        )
      },
      sortMethod: (a, b) => (a.recordDate || "").localeCompare((b.recordDate || ""))
    },
    ...action,
  ]

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={list}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default ContraventionList
