import React, { Component } from "react"
import Loader from "Global/Loader"

import Conduct from "./Conduct"
import Audit from "../../../GlobalComponents/Audit"
import {getAuditLogByType} from "../../../StateManagement/actions/audit_log_actions";

class BusinessConduct extends Component {

  constructor(props) {
    super(props)
    this.state = {
      auditLogs: [],
      loading:true,
    }
  }

  async componentWillMount() {
    await getAuditLogByType("businessConduct", "businessConduct", res => {
      this.setState({
        auditLogs: res && res.changeLog || [],
        loading: false,
      })
    })
  }

  renderSelectedView = (nav2, nav3) => {
    switch (nav2) {
    case "cmp-sup-busconduct":
      switch (nav3) {
      case "business-conduct":
        return <Conduct {...this.props} />
      case "audit":
        return <Audit auditLogs={this.state.auditLogs} />
      default:
        return <Conduct {...this.props} />
      }
    default :
      return nav2
    }
  }

  render() {
    const {nav2, nav3} = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="column">
        <section id="main">
          {this.renderSelectedView(nav2, nav3)}
        </section>
      </div>
    )
  }
}

export default BusinessConduct
