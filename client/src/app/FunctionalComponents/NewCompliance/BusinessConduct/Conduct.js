import React from "react"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify"
import { connect} from "react-redux"
import { getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import Disclaimer from "../../../GlobalComponents/Disclaimer"
import CONST, {ContextType} from "../../../../globalutilities/consts"
import Loader from "../../../GlobalComponents/Loader"
import {
  fetchBcaDetails,
  putBusConduct,
  pullBusConduct,
  pullBusinessDocument
} from "../../../StateManagement/actions/Supervisor"
import {BusinessConductValidate} from "../Validation/BusinessConductValidate"
import ContraventionDetail from "./ContraventionDetail";
import ContraventionList from "./ContraventionList";
import RatingSection from "../../../GlobalComponents/RatingSection";
import Accordion from "../../../GlobalComponents/Accordion";
import swal from "sweetalert";


class Conduct extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      dropDown:{
        violationType: []
      },
      contDetail: cloneDeep(CONST.SuperVisor.bcaContraDetails) || {},
      bucketName: CONST.bucketName,
      notes: CONST.CAC.busConduct,
      confirmAlert: CONST.confirmAlert,
      auditLogs: [],
      contraventionList: [],
      docFile: false,
      isSaveDisabled: false,
      loading: true,
      activeItem: [],
      filename: ""
    }
  }

  async componentWillMount() {
    let result = await getPicklistByPicklistName(["LKUPCONDUCTVIOLATION"])
    result = (result.length && result[1]) || {}
    fetchBcaDetails(res => {
      this.setState({
        contraventionList: res && Array.isArray(res.bcaContraDetails) && res.bcaContraDetails || [],
        dropDown: {
          ...this.state.dropDown,
          violationType: result.LKUPCONDUCTVIOLATION || ["Suspension", "Revocation of registration", "Revocation of bar", "Unfair practice",
            "Deceptive, dishonesty or unfair practice", "Other"],
        },
        activeItem: [0, 1],
        loading: false,
      })
    })
  }

  onFileSetInState = (e) => {
    const { contDetail } = this.state
    contDetail.docFileName = e.name || ""
    this.setState({
      contDetail,
      filename: e.name || ""
    })
  }

  onSave = () => {
    const {contDetail} = this.state
    const errors = BusinessConductValidate(contDetail)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, details: errorMessages },
        // activeItem: [0, 0]
      }))
      return
    }
    if(contDetail.docFileName){
      this.setState({
        isSaveDisabled: true,
        docFile: true
      })
    }
  }

  onUploadSuccess = (filename, docId) => {
    const {contDetail, docFile} = this.state
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    if (docFile && docId) {
      contDetail.docAWSFileLocation = docId
    }
    const errors = BusinessConductValidate(contDetail)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, details: errorMessages },
        // activeItem: [0, 0]
      }))
      return
    }

    this.setState(
      {
        isSaveDisabled: true,
        docFile: false
      },
      () => {
        putBusConduct(contDetail, (res)=> {
          if(this.state.filename){
            this.props.addAuditLog({userName, log: `In Contravention Detail Add New File ${filename}`, date: new Date(), key: "contDetail"})
          }
          if (res && res.status === 200) {
            toast("Added Business Conduct successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            this.setState({
              contraventionList: res.data && res.data.bcaContraDetails || [],
              contDetail: {},
              errorMessages: {},
              // docFile: false,
              isSaveDisabled: false,
              filename: ""
            }, () => this.onAuditSave("contDetail"))
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onRemove = (data) => {
    const {confirmAlert, contDetail} = this.state
    const delId = data && data._id
    confirmAlert.text = `You want to delete this ${data.assPersonName || ""} Contravention Details?`
    swal(confirmAlert).then(willDelete => {
      if(willDelete) {
        const same = data._id === contDetail._id
        this.props.addAuditLog({userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`,
          log: `${data.assPersonName || ""} removed in Contravention Details`, date: new Date(), key: "remove"})
        pullBusConduct(delId, (res) => {
          if(res && res.status === 200) {
            toast("Removed Business Conduct successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            this.onAuditSave("remove")
            this.setState({
              contraventionList: res.data && res.data.bcaContraDetails || [],
              contDetail: same ? {} : contDetail
            })
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          }
        })
      }
    })

  }

  onAuditSave = async (key) => {
    const {user} = this.props
    const audit = this.props.auditLogs.filter(log => log.key === key)
    if(audit.length) {
      audit.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Business Conduct"
        log.superVisorSubSection = "Contravention of restrictions imposed on associated persons"
      })
      await updateAuditLog("businessConduct", audit)
      this.props.updateAuditLog([])
    }
  }

  onChangeItem = (item, category) => {
    this.setState(prevState => ({
      [category]: {
        ...prevState[category],
        ...item
      }
    }))
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `${change} from details`, date: new Date(), key: category})
  }

  onEdit = (data) => {
    const {user} = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: "In Contravention Detail one row edited", date: new Date(), key: "contDetail" })
    this.setState({
      contDetail: data,
      activeItem: [0, 1],
    })
  }

  onDeleteDoc = async document => {
    const { user } = this.props
    const res = await pullBusinessDocument(`?docId=${document._id || ""}`)
    this.props.addAuditLog({userName: `${user.userFirstName} ${user.userLastName}`,
      log: `Document removed in ${document.assPersonName || ""} Contravention Details`, date: new Date(), key: "document"})
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })

      const data = res.data && res.data.bcaContraDetails.filter(item => item._id === document._id)
      this.onAuditSave("document")
      this.setState({
        contDetail: data[0] || [],
      })
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  onAdd = key => {
    this.props.addAuditLog({userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `${key} Add new Item`, date: new Date(), key})
    this.setState({
      contDetail: cloneDeep(CONST.SuperVisor.bcaContraDetails) || {},
      activeItem: [0, 1],
    })
  }

  actionButtons = (key, canEdit) => {
    return (
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={!canEdit}>
            Add
          </button>
        </div>
      </div>
    )
  }

  render() {
    const { dropDown, activeItem, contDetail, bucketName, contraventionList, errorMessages, isSaveDisabled, docFile, filename} = this.state
    const {svControls, user} = this.props
    const loading = () => <Loader/>
    const canEdit = svControls.canSupervisorEdit || false
    if(this.state.loading) {
      return loading()
    }
    return (
      <div id="main" style={{marginTop: 0}}>
        <p>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
          <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
        </p>

        <Accordion multiple isRequired={!!activeItem.length} activeItem={activeItem.length ? activeItem : [0]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Contraventions List"
                actionButtons={this.actionButtons("contDetail", canEdit)}
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(0) && (
                  <ContraventionList
                    list={contraventionList || []}
                    canSupervisorEdit={svControls.canSupervisorEdit || false}
                    user={user}
                    onEdit={this.onEdit}
                    onRemove={this.onRemove}
                  />
                )}
              </RatingSection>

              <RatingSection onAccordion={() => onAccordion(1)} title="Contravention Detail">
                {activeAccordions.includes(1) &&
                <ContraventionDetail item={contDetail} errorMessages={errorMessages} dropDown={dropDown} onChangeItem={this.onChangeItem} category="contDetail" onBlur={this.onBlur}
                  user={user} bucketName={bucketName} contextId={user.userId} contextType={ContextType.supervisor.businessConduct}
                  tenantId={user.entityId} onDeleteDoc={this.onDeleteDoc} canSupervisorEdit={svControls.canSupervisorEdit || false} isSaveDisabled={isSaveDisabled}
                  onUploadSuccess={this.onUploadSuccess} onFileSetInState={this.onFileSetInState} docFile={docFile} filename={filename}
                />
                }
              </RatingSection>
              {
                canEdit ?
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={ filename ? this.onSave : this.onUploadSuccess} disabled={isSaveDisabled}>Save</button>
                      </div>
                    </div>
                  </div> : null
              }
            </div>
          }
        />
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Conduct)
export default connect(mapStateToProps, null)(WrappedComponent)
