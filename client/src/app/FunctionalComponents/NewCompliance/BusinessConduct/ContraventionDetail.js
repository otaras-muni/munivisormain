import React from "react"
import moment from "moment"
import {TextLabelInput, SelectLabelInput} from "../../../GlobalComponents/TextViewBox"
import DocLink from "../../docs/DocLink";
import DocModal from "../../docs/DocModal";
import SingleFileDocUpload from "../../../GlobalComponents/SingleFileDocUpload"

const ContraventionDetail = ({
  dropDown,
  item,
  errorMessages = {},
  onChangeItem,
  canSupervisorEdit,
  category,
  onBlur,
  contextType,
  tenantId,
  onDeleteDoc,
  user,
  bucketName,
  contextId,
  docFile,
  onUploadSuccess,
  onFileSetInState,
  filename
}) => {
  const onChange = event => {
    onChangeItem(
      {
        ...item,
        [event.target.name]: event.target.value
      }, category)
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  return (
    <div>
      <div className="accordion-body">
        <div className="columns">
          <SelectLabelInput
            label="Violation"
            required
            error={errorMessages.violationType || ""}
            list={dropDown.violationType}
            name="violationType"
            value={item.violationType || ""}
            disabled={!canSupervisorEdit}
            onChange={onChange}
            onBlur={onBlurInput}
          />
          <TextLabelInput
            label="Associated Person Name"
            required
            error={errorMessages.assPersonName || ""}
            name="assPersonName"
            value={item.assPersonName || ""}
            disabled={!canSupervisorEdit}
            onChange={onChange}
            onBlur={onBlurInput}
          />
        </div>

        <div className="columns">
          <TextLabelInput
            label="Record Date"
            error={(errorMessages.recordDate && "Required (must be larger than or equal to today or created date)") || ""}
            disabled={!canSupervisorEdit}
            name="recordDate"
            type="date"
            // value={item.recordDate ? moment(new Date(item.recordDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
            value={(item.recordDate === "" || !item.recordDate) ? null : new Date(item.recordDate)}
            onChange={onChange}
            onBlur={onBlurInput}
          />
          <TextLabelInput
            label="Close Date"
            error={(errorMessages.closeDate && "Required (must be larger than or equal to today or created date)") || ""}
            disabled={!canSupervisorEdit}
            name="closeDate"
            type="date"
            // value={item.closeDate ? moment(new Date(item.closeDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
            value={(item.closeDate === "" || !item.closeDate) ? null : new Date(item.closeDate)}
            onChange={onChange}
            onBlur={onBlurInput}
          />
        </div>

        <div className="columns">
          <div className="column">
            <p className="multiExpLbl ">Related Documentation</p>
            {
              item._id ?
                <SingleFileDocUpload
                  bucketName={bucketName}
                  docId={item.docAWSFileLocation}
                  versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
                  showFeedback
                  docFile={docFile}
                  onFileSetInState={onFileSetInState}
                  onUploadSuccess={onUploadSuccess}
                  contextId={contextId}
                  contextType={contextType}
                  tenantId={tenantId}
                  user={user}
                  disabled={!canSupervisorEdit}
                  isNew={!item._id}
                /> :
                <SingleFileDocUpload
                  bucketName={bucketName}
                  docFile={docFile}
                  onFileSetInState={onFileSetInState}
                  onUploadSuccess={onUploadSuccess}
                  versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
                  contextType={"businessConduct"}
                  tenantId={user.entityId}
                  contextId={contextId}
                  user={user}
                  disabled={!canSupervisorEdit}
                  isNew={!item._id}
                />
            }

            {errorMessages.docAWSFileLocation && (
              <p className="text-error">{errorMessages.docAWSFileLocation || ""}</p>
            )}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Filename</p>
            <div className="field is-grouped" style={{ justifyContent: "end" }}>
              { item && item.docAWSFileLocation ?
                <div className="field is-grouped">
                  {
                    filename ?
                      <p className="multiExpLbl">{item && item.docFileName || ""}</p> :
                      <DocLink docId={item.docAWSFileLocation || ""} />
                  }
                  <DocModal
                    onDeleteAll={() => onDeleteDoc(item)}
                    selectedDocId={item.docAWSFileLocation || ""}
                  />
                </div>
                :
                <p className="multiExpLbl">{item && item.docFileName || ""}</p>
              }
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-full">
            <p className="multiExpLbl">Notes / Instructions</p>
            <div className="control">
              <textarea className="textarea" onChange={onChange} value={item && item.violationNotes} name="violationNotes" disabled={!canSupervisorEdit} />
              {errorMessages && errorMessages.violationNotes && <small className="text-error">{errorMessages.violationNotes}</small>}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContraventionDetail
