import React from "react"
import ReactTable from "react-table"
import "react-table/react-table.css"
import moment from "moment"


const ComplainList = ({
  list,
  onRemove,
  onEdit,
  canSupervisorEdit,
  pageSizeOptions = [5, 10, 20, 50, 100],
}) => {
  const action = []

  if(canSupervisorEdit){
    action.push({
      id: "edit",
      Header: "Edit",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div className="field is-grouped">
              <div className="control">
                <a onClick={() => onEdit(item)}>
                  <span className="has-text-link">
                    <i className="fas fa-pencil-alt" />
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={() => onRemove(item)} >
                  <span className="has-text-link">
                    <i className="far fa-trash-alt" />
                  </span>
                </a>
              </div>
            </div>
          </div>
        )
      }
    })
  }
  const columns = [
    {
      id: "complaintsName",
      Header: "Complainant Name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.keyDetails.complaintsName || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.keyDetails.complaintsName || "").localeCompare((b.keyDetails.complaintsName || ""))
    },
    {
      id: "assPersonName",
      Header: "Associated Person Name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.keyDetails.assPersonName || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.keyDetails.assPersonName || "").localeCompare((b.keyDetails.assPersonName || ""))
    },
    {
      id: "dateReceived",
      Header: "Record Date",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const data = (item.keyDetails && item.keyDetails.dateReceived) ? moment(item.keyDetails.dateReceived).format("MM-DD-YYYY") : "-" || "-"
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (data) }} />
        )
      },
      sortMethod: (a, b) => (a.keyDetails.dateReceived || "").localeCompare((b.keyDetails.dateReceived || ""))
    },
    {
      id: "productCode",
      Header: "Product Code",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.keyDetails.productCode) }} />
        )
      },
      sortMethod: (a, b) => (a.keyDetails.productCode || "").localeCompare((b.keyDetails.productCode || ""))
    },
    {
      id: "problemCode",
      Header: "Problem Code",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.keyDetails.problemCode) }} />
        )
      },
      sortMethod: (a, b) => (a.keyDetails.problemCode || "").localeCompare((b.keyDetails.problemCode || ""))
    },
    {
      id: "complaintStatus",
      Header: "Status",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.keyDetails.complaintStatus) }} />
        )
      },
      sortMethod: (a, b) => (a.keyDetails.complaintStatus || "").localeCompare((b.keyDetails.complaintStatus || ""))
    },
    ...action,
  ]

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={list}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default ComplainList
