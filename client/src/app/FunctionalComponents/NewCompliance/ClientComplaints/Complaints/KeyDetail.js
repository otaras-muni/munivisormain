import React from "react"
import moment from "moment"
import {
  TextLabelInput,
  SelectLabelInput,
  MultiSelect
} from "../../../../GlobalComponents/TextViewBox"
import DocLink from "../../../docs/DocLink"
import DocModal from "../../../docs/DocModal"
import SingleFileDocUpload from "../../../../GlobalComponents/SingleFileDocUpload"

const KeyDetail = ({
  dropDown,
  item,
  errors = {},
  onChangeItem,
  canSupervisorEdit,
  category,
  onBlur,
  onSave,
  isSaveDisabled,
  contextType,
  tenantId,
  bucketName,
  onFileSetInState,
  user,
  contextId,
  docFile,
  onUploadSuccess,
  onDeleteDoc,
  filename
}) => {
  const onChange = event => {
    onChangeItem(
      {
        ...item,
        keyDetails: {
          ...item.keyDetails,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        }
      },
      category
    )
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onRelatedActivitySelect = items => {
    onChangeItem(
      {
        ...item,
        keyDetails: {
          ...item.keyDetails,
          activityDetails: items.map(activity => ({
            id: activity.id,
            name: activity.name,
            relTranId: activity.id,
            relTranIssueName:
              activity.rfpTranIssueName ||
              activity.dealIssueTranIssueName ||
              activity.actTranIssueName ||
              activity.actIssueName,
            relTranProjectName:
              activity.actTranProjectDescription ||
              activity.rfpTranProjectDescription ||
              activity.dealIssueTranProjectDescription ||
              activity.actProjectName,
            relTranClientId:
              activity.actTranClientId ||
              activity.rfpTranIssuerId ||
              activity.dealIssueTranClientId ||
              activity.actIssuerClient,
            relTranClientName:
              activity.actTranClientName ||
              activity.rfpTranClientFirmName ||
              activity.dealIssueTranClientFirmName ||
              activity.actIssuerClientEntityName,
            relTranActivityType: activity.group,
            relTranActivitySubType:
              activity.rfpTranSubType ||
              activity.dealIssueTranSubType ||
              activity.actTranSubType ||
              activity.actTranFirmName
          }))
        }
      },
      category
    )
  }

  dropDown.transactions = dropDown.transactions && dropDown.transactions.map(tran => ({
    ...tran,
    group: tran.actTranType ? tran.actTranType : tran.actType
      ? tran.actType : tran.rfpTranType === "RFP"
        ? tran.rfpTranType : tran.dealIssueTranSubType === "Bond Issue"
          ? "Deal" : ""
  }))

  item.keyDetails.activityDetails = item.keyDetails.activityDetails.map(
    activity => ({
      ...activity,
      id: activity.relTranId,
      name: activity.relTranIssueName
    })
  )

  return (
    <div>
      <div className="accordion-body">
        <div>
          <div className="columns">
            <TextLabelInput
              required
              label="Complainant’s Organization Name"
              error={errors.organizationName || ""}
              name="organizationName"
              value={item.keyDetails.organizationName || ""}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={!canSupervisorEdit}
            />
            <TextLabelInput
              required
              label="Complainant's Name"
              error={errors.complaintsName || ""}
              name="complaintsName"
              value={item.keyDetails.complaintsName || ""}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={!canSupervisorEdit}
            />

            <TextLabelInput
              required
              label="Associated Person's Name"
              error={errors.assPersonName || ""}
              name="assPersonName"
              value={item.keyDetails.assPersonName || ""}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={!canSupervisorEdit}
            />
          </div>
          <div className="columns">
            <div className="column">
              <p className="multiExpLbl ">
                Product Code&nbsp;
                <a
                  href="https://s3.amazonaws.com/munivisor-platform-static-documents/G8-Complaints-Prod-Problem-Codes.pdf"
                  download
                  target="_blank"
                >
                  <i className="fas fa-info-circle" />
                </a>
                <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
              </p>
              <SelectLabelInput
                title="Product Code"
                error={errors.productCode || ""}
                list={dropDown.compProdCodes || []}
                name="productCode"
                value={item.keyDetails.productCode || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl ">
                Problem Code&nbsp;
                <a
                  href="https://s3.amazonaws.com/munivisor-platform-static-documents/G8-Complaints-Prod-Problem-Codes.pdf"
                  download
                  target="_blank"
                >
                  <i className="fas fa-info-circle" />
                </a>
                <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
              </p>
              <SelectLabelInput
                title="Problem Code"
                error={errors.problemCode || ""}
                list={dropDown.compProblemCodes}
                name="problemCode"
                value={item.keyDetails.problemCode || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl ">
                Complaint Status <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
              </p>
              <SelectLabelInput
                title="Complaint Status"
                error={errors.complaintStatus || ""}
                list={dropDown && dropDown.complaintStatus || []}
                name="complaintStatus"
                value={item.keyDetails.complaintStatus || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              />
            </div>
          </div>

          <div className="columns">
            <TextLabelInput
              label="Date the complaint was received?"
              error={
                (errors.dateReceived &&
                  "Required") ||
                ""
              }
              name="dateReceived"
              type="date"
              /* value={
                item.keyDetails.dateReceived
                  ? moment(
                    new Date(item.keyDetails.dateReceived)
                      .toISOString()
                      .substring(0, 10)
                  ).format("YYYY-MM-DD")
                  : ""
              } */
              value={(item.keyDetails.dateReceived === "" || !item.keyDetails.dateReceived) ? null : new Date(item.keyDetails.dateReceived)}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={!canSupervisorEdit}
            />

            <TextLabelInput
              label="Date of the activity that gave rise to the complaint?"
              error={
                (errors.dateOfActivity &&
                  "Required (must be larger than or equal to today)") ||
                ""
              }
              name="dateOfActivity"
              type="date"
              /* value={
                item.keyDetails.dateOfActivity
                  ? moment(
                    new Date(item.keyDetails.dateOfActivity)
                      .toISOString()
                      .substring(0, 10)
                  ).format("YYYY-MM-DD")
                  : ""
              } */
              value={(item.keyDetails.dateOfActivity === "" || !item.keyDetails.dateOfActivity) ? null : new Date(item.keyDetails.dateOfActivity)}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={!canSupervisorEdit}
            />

            {canSupervisorEdit ? (
              <MultiSelect
                label="Related Activity"
                filter
                groupBy="group"
                data={dropDown.transactions}
                value={
                  item.keyDetails.activityDetails || []
                } /* disabled={!canSupervisorEdit} */
                error={errors.activityDetails || ""}
                onChange={onRelatedActivitySelect}
                style={{ width: "unset" }}
              />
            ) : (
              <div className="column is-one-third">
                <p className="multiExpLbl">Related Activity</p>
                <small>
                  {(item.keyDetails.activityDetails &&
                    item.keyDetails.activityDetails.length &&
                    item.keyDetails.activityDetails[0].relTranIssueName) ||
                    ""}
                </small>
              </div>
            )}
          </div>

          <div className="columns">
            <div className="column">
              <p className="multiExpLbl ">Related Documentation</p>
              {
                item.keyDetails ?
                  <SingleFileDocUpload
                    bucketName={bucketName}
                    docId={item.keyDetails.docAWSFileLocation}
                    versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
                    showFeedback
                    contextId={contextId}
                    docFile={docFile}
                    onFileSetInState={onFileSetInState}
                    onUploadSuccess={onUploadSuccess}
                    contextType={contextType}
                    tenantId={tenantId}
                    user={user}
                    disabled={!canSupervisorEdit}
                    isNew={!item._id}
                  /> :
                  <SingleFileDocUpload
                    bucketName={bucketName}
                    onFileSetInState={onFileSetInState}
                    versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
                    contextType="clientComplaints"
                    tenantId={user.entityId}
                    contextId={contextId}
                    docFile={docFile}
                    onUploadSuccess={onUploadSuccess}
                    user={user}
                    disabled={!canSupervisorEdit}
                    isNew={!item._id}
                  />
              }

              {errors.docAWSFileLocation && (
                <p className="text-error">{errors.docAWSFileLocation || ""}</p>
              )}
            </div>
            <div className="column">
              <p className="multiExpLbl ">Filename</p>
              <div className="field is-grouped" style={{ justifyContent: "end" }}>
                { item  && item.keyDetails.docAWSFileLocation ?
                  <div className="field is-grouped">
                    {
                      filename ?
                        <p className="multiExpLbl">{item && item.keyDetails.docFileName || ""}</p> :
                        <DocLink docId={item.keyDetails.docAWSFileLocation || ""} />
                    }
                    <DocModal
                      onDeleteAll={() => onDeleteDoc()}
                      selectedDocId={item.keyDetails.docAWSFileLocation || ""}
                    />
                  </div>
                  :
                  <p className="multiExpLbl">{item && item.keyDetails.docFileName || ""}</p>
                }
              </div>
            </div>
          </div>
          <div className="columns">

            <div className="column is-full">
              <p className="multiExpLbl">
                Description of the nature of the complaint.
              </p>
              <div className="control">
                <textarea
                  title="Description of the nature of the complaint"
                  className="textarea"
                  onChange={onChange}
                  value={item.keyDetails.complaintDescription || ""}
                  name="complaintDescription"
                  placeholder=""
                  onBlur={onBlurInput}
                  disabled={!canSupervisorEdit}
                />
                {errors.complaintDescription && (
                  <p className="text-error">{errors.complaintDescription}</p>
                )}
              </div>
            </div>
          </div>

          <div className="columns">
            <div className="column is-full">
              <p className="multiExpLbl">
                What action, if any, has been taken in connection with such
                complaint.
              </p>
              <div className="control">
                <textarea
                  title="What action, if any, has been taken in connection with such complaint"
                  className="textarea"
                  onChange={onChange}
                  value={item.keyDetails.actionAgainstComplaint || ""}
                  name="actionAgainstComplaint"
                  placeholder=""
                  onBlur={onBlurInput}
                  disabled={!canSupervisorEdit}
                />
                {errors.actionAgainstComplaint && (
                  <p className="text-error">{errors.actionAgainstComplaint}</p>
                )}
              </div>
            </div>
          </div>
          {canSupervisorEdit ? (
            <div className="column is-full">
              <div className="field is-grouped-center">
                <div className="control">
                  <button
                    className="button is-link"
                    onClick={filename ? onSave : onUploadSuccess}
                    disabled={isSaveDisabled || false}
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  )
}

export default KeyDetail
