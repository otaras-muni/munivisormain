import React from "react"
import moment from "moment"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import Loader from "Global/Loader"
import {
  fetchComplaintDetails,
  fetchComplaintDetailsById,
  pullComplaintDocument,
  pullComplaintsDetails,
  putComplaintDetails
} from "../../../../StateManagement/actions/Supervisor/index"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import swal from "sweetalert"
import {toast} from "react-toastify"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import connect from "react-redux/es/connect/connect"
import KeyDetail from "./KeyDetail"
import ComplainList from "./ComplainList"
import { getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import {fetchUserTransactions} from "../../../../StateManagement/actions/CreateTransaction"
import {KeyDetailsValidate} from "../../Validation/ClientComplaintsValidate"

const cols = [
  {name: "Client Name"},
  {name: "Description"},
  {name: "Received Date "},
  {name: "Last Updated"},
  {name: "Product Code <a href=\"../../../../../../../public/docs/G8-Complaints-Prod-Problem-Codes.pdf\" download target=\"_blank\"><i class=\"fas fa-info-circle\"/></a>"},
  {name: "Problem Code <a href=\"../../../../../../../public/docs/G8-Complaints-Prod-Problem-Codes.pdf\" download target=\"_blank\"><i class=\"fas fa-info-circle\"/></a>"},
  {name: "Details"},
  {name: "Status"}]

class Complaints extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading:true,
      complainList: [],
      allCmplaints: [],
      searchText: "",
      notes: CONST.CAC.svComplaintDetails,
      complaint: CONST.complaint,
      confirmAlert: CONST.confirmAlert,
      bucketName: CONST.bucketName,
      dropDown: {
        compProdCodes: [],
        compProblemCodes: [],
        compDocTypes: [],
        compDocActions: [],
        transactions: []
      },
      errorMessages: {},
      auditLogs: [],
      isSaveDisabled: false,
      jsonSheets: [],
      startXlDownload: false,
      activeItem:[],
      docFile: false,
      filename: ""
    }
  }

  async componentWillMount(){
    const picResult = await getPicklistByPicklistName([
      "LKUPCMPLPRODCODES",
      "LKUPCMPLPROBLEMCODES",
      "LKUPCOMPLAINTDOCTYPE",
      "LKUPCOMPLAINTDOCACTION",
      "LKUPCOMPLAINTSTATUS"
    ])
    const result = (picResult.length && picResult[1]) || {}
    this.getTransactions()
    fetchComplaintDetails((complainList) => {
      complainList  = complainList.map(complain => {
        const data = {
          keyDetails: complain.keyDetails,
          _id: complain._id
        }
        return data
      })
      this.setState({
        complainList,
        allCmplaints: complainList || [],
        dropDown: {
          compProdCodes: (result && result.LKUPCMPLPRODCODES) || [],
          compProblemCodes: (result && result.LKUPCMPLPROBLEMCODES) || [],
          compDocTypes: (result && result.LKUPCOMPLAINTDOCTYPE) || [],
          compDocActions: (result && result.LKUPCOMPLAINTDOCACTION) || [],
          complaintStatus: (result && result.LKUPCOMPLAINTSTATUS) || [],
        },
        activeItem: [0, 1],
        isLoading:false,
      })
    })
  }

  getTransactions = async () => {
    const res = await fetchUserTransactions(this.props.user.entityId)
    let transactions = []
    if (res.transactions && Array.isArray(res.transactions)) {
      transactions = res.transactions.map(tran => ({
        ...tran,
        name:
          tran.rfpTranProjectDescription ||
          tran.dealIssueTranProjectDescription ||
          tran.actTranProjectDescription ||
          tran.actProjectName ||
          "",
        id: tran._id, // eslint-disable-line
        group: tran.actTranType ? tran.actTranType : tran.actType
          ? tran.actType : tran.rfpTranType === "RFP"
            ? tran.rfpTranType : tran.dealIssueTranSubType === "Bond Issue"
              ? "Deal" : ""
      }))
    }
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        transactions
      }
    })
  }

  onSearchText = (e) => {
    const {allCmplaints} = this.state
    this.setState({
      [e.target.name]: e.target.value
    },() => {
      if(allCmplaints.length){
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const {searchText, allCmplaints} = this.state
    if(searchText) {
      const complainList = allCmplaints.filter(obj => ["finAdvisorEntityName", "complaintDescription", "lastUpdateDate", "dateReceived", "productCode", "problemCode", "complaintStatus"].some(key => {
        if(key === "dateReceived") {
          return moment(obj[key]).format("MM.DD.YYYY hh:mm A").toLowerCase().includes(searchText.toLowerCase())
        }
        return obj[key] && obj[key].toLowerCase().includes(searchText.toLowerCase())
      }))
      this.setState({
        complainList
      })
    }
  }

  pdfDownload = () => {
    const {complainList} = this.state
    const pdfComplainList = []
    complainList && complainList.forEach((item) => {
      pdfComplainList.push([
        item.finAdvisorEntityName || "--",
        item.keyDetails.complaintDescription || "--",
        item.keyDetails.dateReceived ? moment(item.keyDetails.dateReceived).format("MM.DD.YYYY") : "" || "--",
        item.lastUpdateDate ? moment(item.lastUpdateDate).format("MM.DD.YYYY") : "" || "--",
        item.keyDetails.productCode || "--",
        item.keyDetails.problemCode || "--",
        item.keyDetails.complaintStatus || "--"
      ])
    })
    const tableData = []
    if(pdfComplainList.length > 0){
      tableData.push({
        titles:["Complaints List"],
        description: "",
        headers: ["Client Name","Description","Received Date","Last Updated","Product Code","Problem Code","Status"],
        rows: pdfComplainList
      })
    }
    pdfTableDownload("", tableData, "complainList.pdf")
  }

  xlDownload = () => {
    const {complainList} = this.state

    const exlComplainList = []

    complainList && complainList.map((item) =>  {
      const data = {
        "Client Name": item.finAdvisorEntityName || "--",
        "Description": item.keyDetails.complaintDescription || "--",
        "Received Date": item.keyDetails.dateReceived ? moment(item.keyDetails.dateReceived).format("MM.DD.YYYY") : "" || "--",
        "Last Updated": item.lastUpdateDate ? moment(item.lastUpdateDate).format("MM.DD.YYYY") : "" || "--",
        "Product Code": item.keyDetails.productCode || "--",
        "Problem Code": item.keyDetails.problemCode || "--",
        "Status": item.keyDetails.complaintStatus || "--"
      }
      exlComplainList.push(data)
    })
    const jsonSheets = [ {
      name: "Client Complaints",
      headers: ["Client Name","Description","Received Date","Last Updated","Product Code","Problem Code","Status"],
      data: exlComplainList,
    } ]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  onAdd = key => {
    this.props.addAuditLog({userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `${key} Add new Item`, date: new Date(), key})
    this.setState({
      complaint: CONST.complaint,
      errorMessages: {},
      activeItem: [0, 1],
    })
  }

  actionButtons = (key, canEdit) => {
    return (
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={!canEdit}>
            Add
          </button>
        </div>
      </div>
    )
  }

  onChangeItem = (item, category) => {
    this.setState({
      [category]: item
    })
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `${change}`,
      date: new Date(),
      key: category
    })
  }

  onRemove = (data) => {
    const {confirmAlert, complaint, comId} = this.state
    const {user} = this.props
    const delId = data && data._id
    confirmAlert.text = `You want to delete this ${data.assPersonName || ""} Complaint Details?`
    swal(confirmAlert).then(willDelete => {
      if(willDelete) {
        const same = data._id === comId
        this.props.addAuditLog({userName: `${user.userFirstName} ${user.userLastName}`,
          log: `${data.keyDetails.assPersonName || ""} removed in Complaint Details`, date: new Date(), key: "remove"})
        pullComplaintsDetails(delId, (res) => {
          if(res && res.status === 200) {
            toast(`Removed ${data.keyDetails.complaintsName || ""} Complaint Details successfully`,{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            this.onAuditSave("remove", delId)

            const complainList  = res.data && res.data.map(complain => {
              const data = {
                keyDetails: complain.keyDetails,
                _id: complain._id
              }
              return data
            })
            this.setState({
              complainList: complainList || [],
              complaint: {
                ...complaint,
                keyDetails: same ? CONST.complaint.keyDetails : complaint.keyDetails
              },
            })
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          }
        })
      }
    })
  }

  onEdit = (item) => {
    const comId = item && item._id
    const {user} = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: "In Complaint Detail one row edited", date: new Date(), key: "complaint" })
    fetchComplaintDetailsById(comId, res => {
      if (res) {
        this.setState(prevState => ({
          complaint: {
            ...prevState.complaint,
            keyDetails: res && res.keyDetails
          },
          auditLogs: res && res.auditLogs && res.auditLogs.length
            ? res.auditLogs : [],
          comId,
          errorMessages: {},
        }))
      }
    })
  }

  onFileSetInState = (e) => {
    const { complaint } = this.state
    complaint.keyDetails.docFileName = e.name || ""
    this.setState({
      complaint,
      filename: e.name || ""
    })
  }

  onUploadSuccess = (filename, docId) => {
    const {complaint, docFile, comId, complainList} = this.state
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    const key = "keyDetails"
    if (docFile) {
      complaint.keyDetails.docAWSFileLocation = docId
    }
    let errors = {}
    if (key === "keyDetails") {
      errors = KeyDetailsValidate(
        complaint[key],
        complaint.keyDetails.createdDate
      )
    }
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, [key]: errorMessages }
      }))
      return
    }
    this.setState(
      {
        isSaveDisabled: true,
        docFile: false
      },
      () => {
        putComplaintDetails(key, comId, complaint[key], res => {
          if(this.state.filename){
            this.props.addAuditLog({userName, log: `In Complaint Detail Add New File ${filename}`, date: new Date(), key: "complaint"})
          }
          if (res && res.status === 200) {
            toast(
              "Added Complaint Detail successfully",
              { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
            )
            if (res.data) {
              if (comId) {
                const index = complainList.findIndex(part => part._id === comId)
                complainList[index] = {
                  keyDetails: res.data.keyDetails,
                  _id: res.data._id
                }
              } else {
                const item = {
                  keyDetails: res.data && res.data.keyDetails,
                  _id: res.data && res.data._id
                }
                complainList.push(item)
              }
              this.onAuditSave("complaint", comId || res.data._id)
              this.setState({
                complaint: CONST.complaint,
                complainList,
                isSaveDisabled: false,
                comId: "",
                filename: "",
                errorMessages: {},
              })
            }
          } else {
            toast("Something went wrong!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onSave = key => {
    const { complaint } = this.state
    console.log(complaint[key])
    let errors = {}
    if (key === "keyDetails") {
      errors = KeyDetailsValidate(
        complaint[key],
        complaint.keyDetails.createdDate
      )
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, [key]: errorMessages }
      }))
      return
    }
    if(complaint.keyDetails.docFileName){
      this.setState({
        isSaveDisabled: true,
        docFile: true
      })
    }
  }

  onComplaintDeleteDoc = async () => {
    const { complaint, comId } = this.state
    const {user} = this.props
    const res = await pullComplaintDocument(`?docId=${comId || ""}`)
    this.props.addAuditLog({userName: `${user.userFirstName} ${user.userLastName}`,
      log: "Document removed in Complaints Details", date: new Date(), key: "document"})
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.onAuditSave("document")
      this.setState({
        complaint: {
          ...complaint,
          keyDetails: res.data && res.data.keyDetails
        },
      })
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  onAuditSave = async (key, tranId) => {
    const { user } = this.props
    const {comId} = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if (auditLogs.length && (comId || tranId)) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Client Complaints"
        log.superVisorSubSection = "keyDetails"
      })
      await updateAuditLog("clientComplaints", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  render() {
    const {isLoading, complainList, activeItem, bucketName, dropDown, complaint, errorMessages, isSaveDisabled, docFile, filename} = this.state
    const {svControls, user} = this.props
    const loading = () => <Loader/>
    const canEdit = svControls.canSupervisorEdit || false
    if (isLoading) {
      return loading()
    }

    return (
      <div>
        <p>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
          <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
        </p><br/>
        <div className="columns">
          <div className="column">
            <p className="control has-icons-left">
              <input className="input is-small is-link" type="text" placeholder="search" name="searchText" onChange={this.onSearchText}/>
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="field is-grouped">
              <div className={`${complainList.length ? "control" : "control isDisabled"}`} title="Excel" onClick={this.xlDownload}>
                <span className="has-text-link">
                  <i className="far fa-2x fa-file-excel has-text-link" />
                </span>
              </div>
              <div style={{ display: "none" }}>
                <ExcelSaverMulti label="XML" startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
              </div>
              <div className={`${complainList.length ? "control" : "control isDisabled"}`} title="PDF" onClick={this.pdfDownload}>
                <span className="has-text-link">
                  <i className="far fa-2x fa-file-pdf has-text-link" />
                </span>
              </div>
            </div>
          </div>
        </div>

        <Accordion multiple isRequired={!!activeItem.length} activeItem={activeItem.length ? activeItem : [0]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Complaints List"
                actionButtons={this.actionButtons("keyDetails", canEdit)}
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(0) && (
                  <ComplainList
                    list={complainList || []}
                    canSupervisorEdit={svControls.canSupervisorEdit || false}
                    user={user}
                    onEdit={this.onEdit}
                    onRemove={this.onRemove}
                  />
                )}
              </RatingSection>
              {
                <RatingSection
                  onAccordion={() => onAccordion(1)}
                  title="Complaint detail"
                >
                  {activeAccordions.includes(1) && (
                    <KeyDetail
                      dropDown={dropDown}
                      item={complaint || {}}
                      canSupervisorEdit={svControls.canSupervisorEdit}
                      errors={errorMessages.keyDetails || {}}
                      onAssignedSelect={this.onAssignedSelect}
                      onChangeItem={this.onChangeItem}
                      category="complaint"
                      onBlur={this.onBlur}
                      onUploadSuccess={this.onUploadSuccess}
                      onFileSetInState={this.onFileSetInState}
                      onSave={() => this.onSave("keyDetails")}
                      docFile={docFile}
                      isSaveDisabled={isSaveDisabled}
                      user={user}
                      bucketName={bucketName}
                      contextId={user.userId}
                      contextType={ContextType.supervisor.svComplaints}
                      tenantId={user.entityId}
                      onDeleteDoc={this.onComplaintDeleteDoc}
                      filename={filename}
                    />
                  )}
                </RatingSection>
              }

            </div>
          }
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Complaints)
export default connect(mapStateToProps, null)(WrappedComponent)
