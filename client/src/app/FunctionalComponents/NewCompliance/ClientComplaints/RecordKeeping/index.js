import React, { Component } from "react"
import ReactTable from "react-table"
import { connect} from "react-redux"
import { getUserList } from "AppState/actions/AdminManagement/admTrnActions"
import moment from "moment/moment"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {fetchG10RecordKeeping} from "../../../../StateManagement/actions/Supervisor"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import DocLink from "../../../docs/DocLink"
import DocModal from "../../../docs/DocModal"
import CONST from "../../../../../globalutilities/consts"
import DocModalDetails from "../../../docs/DocModalDetails"
import {SendEmailG10Documents} from "./SendEmailG10Documents"
import {fetchDocDetails} from "../../../../StateManagement/actions"
import { sendComplianceEmail } from "../../../../StateManagement/actions/Transaction"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery && searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

class RecordKeeping extends Component {
  constructor(props) {
    super(props)

    this.state = {
      bucketName: CONST.bucketName,
      modalState: false,
      showModal: false,
      recordKeepingList: [],
      action: "",
      email: {
        category: "",
        message: "",
        subject: "",
        sendDocEmailLinks: true
      },
      isRefresh: false,
      loading: false
    }
  }

  async componentWillMount() {
    fetchG10RecordKeeping("", (res) => {
      this.setState({
        recordKeepingList: res && res.recordKeeping || [],
      })
    })
  }

  onSearchEntity = (e) => {
    const { recordKeepingList } = this.state
    const {name, value} =  e.target
    this.setState({
      [name]: value
    }, () => {
      if (recordKeepingList && recordKeepingList.length) {
        this.onSearch(value)
      }
    })
  }

  onSearch = (value) => {
    const { recordKeepingList } = this.state
    const searchList = recordKeepingList.filter(obj => ["entityName", "fileName", "recordKeepingType", "recipientEmail", "deliveryAttemptDate", "deliveryStatus"].some(key => obj[key] && obj[key].toLowerCase().includes((value ).toLowerCase())))
    this.setState({
      searchList
    })
  }

  onFileAction = (e, doc) => {
    const { value, name } = e.target
    this.setState({
      [name]: value,
      partyId: doc && doc.entityId || ""
    }, async () => {
      const { action } = this.state
      if (value === "Share document") {
        this.onParticipantsRefresh(doc && doc.entityId || "")
        this.setState({
          modalState: true,
          email: {
            ...this.state.email,
            message: "",
            category: "custom",
            sendDocEmailLinks: true,
            docIds: [doc.fileAWSId || ""]
          },
        })
      } else if (action === "version") {
        fetchDocDetails(doc.fileAWSId).then(res => {
          this.handleDocDetails(res)
        })
      }
    })
  }

  onParticipantsRefresh = () => {
    this.setState({
      isRefresh: true
    }, async () => {
      const payload = {
        filters:{
          userContactTypes: [],
          entityMarketRoleFlags: [],
          entityIssuerFlags: [],
          userPrimaryAddressStates: [],
          freeTextSearchTerm: "",
          entityId: this.state.partyId || "",
          activeFlag: "",
          entityTypes: [ "Client", "Prospect" ]
        },
        pagination:{
          serverPerformPagination: true,
          currentPage: 0,
          size: 100000000,
          sortFields: { entityName:1}
        }
      }

      const result = await getUserList(payload)
      const data = await result.data
      const list = data && data.pipeLineQueryResults && data.pipeLineQueryResults[0] && data.pipeLineQueryResults[0].data
      const usersList = list.map(l => ({name: `${l.userFirstName || ""} ${l.userLastName || ""}`, id: l.userId, sendEmailTo: l.userPrimaryEmail || ""} ))
      this.setState({
        participants: usersList || [],
        isRefresh: false
      })
    })

  }

  onModalChange = (state) => {
    this.setState({
      email: {
        sendEmailTo: []
      },
      action: "",
      ...state
    })
  }

  onCKEditorChanges = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onModalSave = () => {
    const { transaction, nav1, nav2 } = this.props
    const emailPayload = {
      tranId: (transaction && transaction._id) || "",
      type: nav1 === "compliance" ? nav2 : nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        subject: "Document Action",
        ...this.state.email
      }
    }
    this.setState(
      pre => ({
        modalState: !pre.modalState
      }),
      async () => {
        if (nav1 === "compliance") {
          await sendComplianceEmail(emailPayload)
        }
      }
    )
  }

  handleDocDetails = res => {
    this.setState({
      showModal: !this.state.showModal,
      action: "",
      doc: res || {}
    })
  }


  render() {
    const { loading, searchText, searchList, action, doc, modalState, email, participants, isRefresh } = this.state
    const { svControls } = this.props
    const recordKeepingList = searchText ? searchList : this.state.recordKeepingList
    if (loading) {
      return <Loader />
    }

    return (
      <Accordion multiple
        activeItem={[0, 1, 2]}
        boxHidden
        render={({activeAccordions, onAccordion}) =>
          <div>
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title="Documents"
            >
              {activeAccordions.includes(0) && (
                <div>
                  <div className="columns">
                    <div className="column">
                      <p className="control has-icons-left">
                        <input className="input is-small is-link" type="text" name="searchText"  placeholder="search"  onChange={(e) => this.onSearchEntity(e)}/>
                        <span className="icon is-left has-background-dark">
                          <i className="fas fa-search" />
                        </span>
                      </p>
                    </div>
                  </div>
                  <ReactTable
                    columns={[
                      {
                        Header: "Entity Name",
                        id: "entityName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch( item && item.entityName || "" , searchText)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.entityName - b.entityName
                      },
                      {
                        Header: "Filename",
                        id: "fileName",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="field is-grouped-left">
                              <DocLink docId={item.fileAWSId}/>
                              <DocModal
                                docMetaToShow={["category", "subCategory"]}
                                versionMetaToShow={["uploadedBy"]}
                                selectedDocId={item.fileAWSId}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.fileName - b.fileName
                      },
                      {
                        Header: "Type",
                        id: "type",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch( item && item.recordKeepingType || "" , searchText)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.recordKeepingType - b.recordKeepingType
                      },
                      {
                        Header: "Recipient Email",
                        id: "email",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch( item && item.recipientEmail || "" , searchText)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.recipientEmail - b.recipientEmail
                      },
                      {
                        Header: "Delivery Attempt Date",
                        id: "deliveryAttemptDate",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch( item && item.deliveryAttemptDate ? moment(item.deliveryAttemptDate).format("MM-DD-YYYY h:mm A") : "" , searchText)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.deliveryAttemptDate - b.deliveryAttemptDate
                      },
                      {
                        Header: "Delivery Status",
                        id: "deliveryStatus",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: highlightSearch( item && item.deliveryStatus || "" , searchText)
                                }}
                              />
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.deliveryStatus - b.deliveryStatus
                      },
                      {
                        Header: "Action",
                        id: "deliveryStatus",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="select is-small is-fullwidth is-link">
                              <select value={action || ""} name="action" onChange={e => this.onFileAction(e, item)} disabled={!svControls.supervisor || false}>
                                <option value="" disabled>Action</option>
                                <option value="Share document">Share</option>
                                <option value="version">See version history</option>
                              </select>
                            </div>
                          )
                        },
                        sortMethod: (a, b) => a.deliveryStatus - b.deliveryStatus
                      },
                    ]}
                    data={recordKeepingList}
                    showPaginationBottom
                    defaultPageSize={5}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    showPageJump
                    minRows={2}
                  />
                </div>
              )}
            </RatingSection>
            {this.state.showModal ? (
              <DocModalDetails
                showModal={this.state.showModal}
                closeDocDetails={this.handleDocDetails}
                documentId={doc.documentId}
                docMetaToShow={[]}
                versionMetaToShow={["uploadedBy"]}
                docId={doc._id}
              />
            ) : null}
            <SendEmailG10Documents
              modalState={modalState}
              email={email}
              documentFlag
              isRefresh={isRefresh}
              onModalChange={this.onModalChange}
              participants={participants}
              onCKEditorChanges={this.onCKEditorChanges}
              onSave={this.onModalSave}
              onParticipantsRefresh={this.onParticipantsRefresh}
            />
          </div>
        }
      />
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(RecordKeeping)
export default connect(mapStateToProps, null)(WrappedComponent)
