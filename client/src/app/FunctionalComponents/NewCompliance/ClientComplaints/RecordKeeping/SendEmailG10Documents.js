import React from "react"
import CKEditor from "react-ckeditor-component"
import { Modal } from "../../../TaskManagement/components/Modal"
import {MultiSelect, TextLabelInput} from "../../../../GlobalComponents/TextViewBox"

export const SendEmailG10Documents  = (props) => {
  const toggleModal = () => {
    props.onModalChange({ modalState: !props.modalState })
  }

  const onChange = (e) => {
    let email = {}
    if(e.target.name === "category"){
      email = {
        ...props.email,
        sendEmailTo: [],
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }else {
      email = {
        ...props.email,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }
    props.onModalChange({
      email
    })
  }

  const onEditorChange = (e, name) => {
    props.onCKEditorChanges( props.onCKEditorChanges({
      [name]: e.editor.getData()
    }, name))
  }

  const onSelect = (items) => {
    props.onModalChange({
      email: {
        ...props.email,
        sendEmailTo: items
      }
    })
  }


  return(
    <Modal
      closeModal={toggleModal}
      modalState={props.modalState}
      title="Compliance Checks and Email Notification"
      saveModal={props.onSave}
    >

      <div>
        {
          props.documentFlag ?
            <label className="checkbox-button"> Send link(s) to email recipients to download documents
              <input type="checkbox" name="sendDocEmailLinks" value="none" checked={props.email.sendDocEmailLinks || false} onClick={onChange} onChange={onChange} />
              <span className="checkmark" />
            </label> : null
        }
        <br/>
        <div className="columns multi-select">
          <MultiSelect filter label="Send email to" data={props.participants} value={props.email.sendEmailTo || []} onChange={(items) => onSelect(items)}  style={{width: "100%"}}/>
        </div>
        <div className="columns">
          <a className="has-text-link" style={{fontSize: 12,padding: "0 0 8px 11px",marginTop: -10}} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
          <a className={props.isRefresh ? "fa fa-refresh fa-spin isDisabled" : "fa fa-refresh"} style={{fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px"}} onClick={props.onParticipantsRefresh}/>
        </div>
      </div>

      <div className="columns">

        <div className="column">
          <p className="multiExpLbl">Message</p>
          <div className="control">
            <CKEditor
              activeClass="p10"
              content={props.email.message}
              events={{
                /* "blur": this.onBlur,
                "afterPaste": this.afterPaste, */
                "change": (e) => onEditorChange(e, "message")
              }}
            />
          </div>
        </div>

      </div>
    </Modal>
  )
}
export default SendEmailG10Documents
