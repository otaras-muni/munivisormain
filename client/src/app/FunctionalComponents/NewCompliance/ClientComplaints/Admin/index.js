import React, { Component } from "react"
import swal from "sweetalert"
import { connect } from "react-redux"
import {toast} from "react-toastify"
import cloneDeep from "clone-deep"
import CKEditor from "react-ckeditor-component"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import { getEntityList } from "AppState/actions/AdminManagement/admTrnActions"
import { updateAuditLog } from "GlobalUtils/helpers"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import EditableReactTable from "../../GeneralAdmin/Admin/EditableReactTable"
import Accordion from "../../../../GlobalComponents/Accordion"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import {G10AdminValidate} from "../../Validation/ClientComplaintsValidate"
import {fetchG10AdminDetails, pullG10AdminDetails, putG10AdminDetails} from "../../../../StateManagement/actions/Supervisor"
import EntityListReactTable  from "./entityListReactTable"
import { g10PDFPreview } from "GlobalUtils/pdfutils"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"

class RecordNames extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      isEditable: {},
      administrationList: [CONST.NewCompliance.clientComplaint.administration],
      staticField: [{field: "Firm name as registered with the MSRB", information: "", isField: true},
        {field: "MSRB registration number", information: "", isField: true},
        {field: "Firm name as registered with the SEC", information: "", isField: true},
        {field: "SEC registration/CIK number", information: "", isField: true}],
      tables: [
        {
          name:"Required information for G-10",
          key: "administrationList",
          header: [
            {name: "Field<span class='has-text-danger'>*</span>"},
            {name: "Information"},
            {name: "Action"}
          ],
          row:  cloneDeep(CONST.NewCompliance.clientComplaint.administration) || {},
          list: [],
          tbody: [
            { name: "field", type: "text" },
            { name: "information", type: "text" },
            {name: "action"},
          ],
          handleError: (payload) => G10AdminValidate(payload),
        },
      ],
      errorMessages: {},
      selectedEntity: [],
      recordKeepingList: [],
      disclosureMessage: "",
      disclosureMessageStatic: `<div>
        <p>As you are aware, our firm is a Municipal Advisor. Our registration information with the Municipal Securities Rulemaking Board (the “MSRB), and/or with the U.S. Securities and Exchange Commission (the “SEC”) is provided above. On October 13, 2017, MSRB Rule G-10 went into effect for municipal advisors. Rule G-10 requires that beginning in 2017, and annually thereafter, municipal advisors provide clients with certain information regarding registration status and the availability of information that explains the protections in place for clients and how clients can file a complaint against a municipal advisor, should they desire to do so. As such, we are required to adhere to all MSRB rules applicable to municipal advisors.</p>
        <p>The website for the MSRB is <a href="http://www.msrb.org/">www.msrb.org</a>, and the website for the SEC is <a href="https://www.sec.gov/">www.sec.gov</a>. The MSRB publishes a brochure for clients of municipal advisors that describes the protections that may be provided by the MSRB rules and describes how you may file a complaint. That brochure may be found at <a href="http://www.msrb.org/~/media/Files/Resources/MSRB-MA-Clients-Brochure.ashx?la=en">http://www.msrb.org/~/media/Files/Resources/MSRB-MA-Clients-Brochure.ashx?la=en</a>. Specifically, any complaints regarding our firm would be filed with the MSRB via email at <a href="complaints@msrb.org">complaints@msrb.org</a>, and with the SEC at <a href="https://www.sec.gov/reportspubs/investor-publications/complaintshtml.html">https://www.sec.gov/reportspubs/investor-publications/complaintshtml.html</a>. More complete is contained in the brochure identified above. Once again, you will be receiving a notice similar to this annually as required by MSRB rule G-10.</p>
      </div>`,
      confirmAlert: CONST.confirmAlert,
      discloseStyle: "none"
    }

  }

  async componentWillMount() {
    await this.getEntityList()
    this.getRecordDetails()
  }

  onDiscloseCancel = () => {
    this.setState({
      disclosureMessage: this.state.tempDisclosureMessage,
      discloseStyle: "none",
    })
  }

  onDiscloseSave = () => {
    const { user } = this.props
    this.setState({
      loading: true
    }, () => {
      this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: "Changes in Disclosure letter content", date: new Date(), key: "disclosure" })
      putG10AdminDetails( "?type=disclosureMessage", this.state.disclosureMessage, (res)=> {
        if (res && res.status === 200) {
          this.onAuditSave("disclosure")
          toast("Disclosure letter content updated successfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS,
          })
          this.setState({
            tempDisclosureMessage: res && res.data && res.data.disclosureMessage || "",
            discloseStyle: "none",
            loading: false
          })
        }
      })
    })
  }

  onEditMode = () => {
    const { disclosureMessage } = this.state
    this.setState({
      tempDisclosureMessage: disclosureMessage,
      discloseStyle: ""
    })
  }

  onAdd = (key) => {
    const { isEditable, tables } = this.state
    const { user } = this.props
    const insertRow = cloneDeep(this.state[key])
    const tableObject = tables.find(tbl => tbl.key === key)

    if ((isEditable && isEditable[key]) || (isEditable && isEditable[key] === 0) ) { swal("Warning", "First save the current item", "warning"); return }

    if (tableObject.hasOwnProperty("row")) {
      insertRow.unshift({
        ...cloneDeep(tableObject.row),
        isNew: true
      })
    }
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: "Required information for G-10 in Add new Item", date: new Date(), key })

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: insertRow.length ? 0 : 0
      }
    })
  }

  onItemChange = (item, category, index) => {
    if(category === "disclosureMessage"){
      this.setState({
        [category]: item.editor.getData()
      })
    } else {
      const items = this.state[category]
      items[index] = item
      this.setState({
        [category]: items,
      })
    }
  }

  onBlur = (change, category) => {
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: `In Required information for G-10 ${change}`, date: new Date(), key: category})
  }

  onCancel = (key) => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: "",
      },
    })
  }

  onEdit = (key, index, tableTitle) => {
    const { user } = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `In ${tableTitle} one row edited`, date: new Date(), key })
    if (this.state.isEditable[key]) { swal("Warning", "First save the current item", "warning"); return }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index,
      },
    })
  }

  onRemoveDoc = (removeId, type, index, ) => {
    const { isEditable, confirmAlert } = this.state
    confirmAlert.text = "You want to delete General Firm Information?"
    swal(confirmAlert).then((willDelete) => {
      if (willDelete) {
        if (type) {
          this.setState({
            isSaveDisabled: {
              ...this.state.isSaveDisabled,
              [type]: true
            }
          }, () => {
            if(removeId){
              this.onRemove(type, removeId)
            } else {
              const list = this.state[type]
              if (list.length) {
                list.splice(index, 1)
                this.setState({
                  [type]: list,
                  isEditable: {
                    ...isEditable,
                    [type]: ""
                  },
                  isSaveDisabled: {
                    ...this.state.isSaveDisabled,
                    [type]: false
                  },
                  errorMessages: {}
                })
              }
            }
          })
        }
      }
    })
  }

  onSaveDoc = (key, item, index) => {
    const { tables } = this.state
    const table = tables.find(tbl => tbl.key === key)
    const itemData = {
      ...item
    }
    const minDate = this.props.minDate || itemData.createdDate || ""
    delete itemData.isNew
    const errors = (table && table.handleError) ? table.handleError(itemData, minDate) : null

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        const message = "Required"
        if (errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.context.key] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.context.key]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.context.key]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    this.setState({
      isSaveDisabled: {
        ...this.state.isSaveDisabled,
        [key]: true
      }
    }, () => {
      delete item.isNew
      this.onSave(key, item)
    })
  }

  onSave = (type) => {
    putG10AdminDetails( `?type=${type}`, this.state[type], (res)=> {
      if (res && res.status === 200) {
        toast("Added Required information for G-10 successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("administrationList")
        this.setState({
          [type]: res && res.data && res.data[type] || [],
          errorMessages: {},
          isEditable: {
            ...this.state.isEditable,
            [type]: "",
          },
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          errorMessages: {},
          isEditable: {
            ...this.state.isEditable,
            [type]: "",
          },
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      }
    })
  }

  onRemove = (type, removeId) => {
    console.log(type, removeId)
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: "Remove item from Required information for G-10", date: new Date(), key: type})
    pullG10AdminDetails(`?type=${type}&_id=${removeId}`, (res) => {
      if(res && res.status === 200) {
        toast("Removed Required information for G-10 successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          [type]: res && res.data && res.data[type] || [],
          errorMessages: {},
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          errorMessages: {},
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      }
    })
  }

  onSearchEntity = (e) => {
    const { entityList } = this.state
    const {name, value} =  e.target
    this.setState({
      [name]: value
    }, () => {
      if (entityList && entityList.length) {
        this.onSearch(value)
      }
    })
  }

  onSearch = (value) => {
    const { entityList } = this.state
    const searchEntityList = entityList.filter(obj => ["entityRelationshipType", "entityName", "userFullName", "userPrimaryEmail", "entityPrimaryAddressGoogle"].some(key => obj[key] && obj[key].toLowerCase().includes((value ).toLowerCase())))
    this.setState({
      searchEntityList
    })
  }

  onPreviewPDF = () => {
    const { selectedEntity, entityList, administrationList, disclosureMessage } = this.state
    const { logo, user } = this.props
    const position = user && user.settings
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    const duplicateAudit = []
    selectedEntity.map(id => {
      entityList.forEach(entity => {
        if(id === entity._id){
          duplicateAudit.push({
            userName, log: `${entity && entity.entityName} Send For Preview`, date: new Date(), key: "preview"
          })
          g10PDFPreview(entity, administrationList, logo, position, disclosureMessage, "preview")
        }
      })
    })
    entityList.forEach(e => {
      if(e && e.checked){
        e.checked = false
      }
    })
    if(duplicateAudit && duplicateAudit.length){
      this.props.addMultiAuditLog(duplicateAudit)
    }
    this.setState({
      entityList
    }, () => {
      this.setState({
        selectedEntity: []
      }, () => {
        if(this.props.auditLogs && this.props.auditLogs.length){
          this.onAuditSave("preview")
        }
      })
    })
  }

  onAutoGenerate = () => {
    const { selectedEntity, entityList, administrationList, disclosureMessage, recordKeepingList } = this.state
    const { logo, user } = this.props
    const userDetails = {}
    userDetails.tenantId = user && user.entityId
    userDetails.contextType = ContextType.supervisor.svComplaints
    userDetails.contextId = "cmp-sup-complaints"
    userDetails.userName = `${user && user.userFirstName} ${user && user.userLastName}`
    const position = user && user.settings
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    const duplicateAudit = []
    selectedEntity.map(id => {
      entityList.forEach(entity => {
        if(id === entity._id){
          const doc = recordKeepingList && recordKeepingList.find(r => r.entityId === entity._id) || {}
          const docId = doc && doc.fileAWSId || ""
          duplicateAudit.push({
            userName, log: `${entity && entity.entityName} Auto-generate and send`, date: new Date(), key: "generate"
          })
          this.setState({
            loading: true
          }, () => g10PDFPreview(entity, administrationList, logo, position, disclosureMessage, "generate", {userDetails}, {getPDFResponse: () => this.getPDFResponse()}, docId))
        }
      })
    })
    if(duplicateAudit && duplicateAudit.length){
      this.props.addMultiAuditLog(duplicateAudit)
    }
    this.setState({
      duplicateAudit: []
    }, () => {
      if(this.props.auditLogs && this.props.auditLogs.length){
        this.onAuditSave("generate")
      }
    })
  }

  onAuditSave = async (key) => {
    const { user } = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)

    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Client Complaints"
        log.superVisorSubSection = "G-10 Administration"
      })
      await updateAuditLog("clientComplaints", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  onSelectEntity = (e, id) => {
    const { selectedEntity, entityList } = this.state
    const { checked } = e.target
    if( checked ){
      const index = entityList.findIndex(e => e._id === id)
      entityList[index].checked = checked
      selectedEntity.push(id)
    } else {
      const index = selectedEntity.findIndex(s => s === id)
      const entityIndex = entityList.findIndex(s => s._id === id)
      selectedEntity.splice(index, 1)
      entityList[entityIndex].checked = checked
    }
    this.setState({
      selectedEntity,
      entityList
    })
  }

  getRecordDetails = () => {
    const { staticField, disclosureMessageStatic } = this.state
    fetchG10AdminDetails((res) => {
      this.setState({
        administrationList: res && res.administrationList && res.administrationList.length ? res.administrationList : staticField,
        recordKeepingList: res && res.recordKeeping || [],
        disclosureMessage: res && res.disclosureMessage || disclosureMessageStatic || "",
        loading: false
      })
    })
  }

  getEntityList = async () => {
    const payload = {
      filters:{
        entityTypes:["Client", "Prospect"],
        userContactTypes:[],
        entityMarketRoleFlags:[],
        entityIssuerFlags:[],
        entityPrimaryAddressStates:[],
        freeTextSearchTerm:""
      },
      pagination:{
        serverPerformPagination:true,
        currentPage:0,
        size:10000000,
        sortFields:{
          entityName:1
        }
      }
    }
    const result = await getEntityList(payload)
    const data = await result.data
    const entityList = data && data.pipeLineQueryResults && data.pipeLineQueryResults.length && data.pipeLineQueryResults[0] && data.pipeLineQueryResults[0].data || []
    this.setState({
      entityList
    })
  }

  getPDFResponse = () => {
    const { entityList } = this.state
    entityList.forEach(e => {
      if(e && e.checked){
        e.checked = false
      }
    })
    this.setState({
      entityList,
      selectedEntity: [],
      loading: false
    }, () => this.getRecordDetails())
  }

  disclosureActionButton = (key) => {
    const { discloseStyle } = this.state
    const { svControls } = this.props
    if (svControls && !svControls.supervisor) return
    return( //eslint-disable-line
      <div className="field is-grouped">
        { discloseStyle ?
          <div className="control">
            <button className="button is-link is-small" onClick={() => this.onEditMode(key)}>Edit</button>
          </div> :
          <div className="control">
            <button className="button is-link is-small" onClick={() => this.onDiscloseSave(key)}>Save</button>&nbsp;&nbsp;
            <button className="button is-text is-small" onClick={() => this.onDiscloseCancel(key)}>Cancel</button>
          </div>
        }
      </div>
    )
  }

  actionButtons = (key, isDisabled) => {
    const list = this.state[key]
    const isAddNew = (list && list.find(part => part.isNew)) || false
    const { svControls } = this.props
    if (svControls && !svControls.supervisor) return
    return( //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={isDisabled || !!isAddNew}>Add
          </button>
        </div>
      </div>
    )
  }

  render() {
    const { loading, isEditable, administrationList, errorMessages, disclosureMessage, searchText, searchEntityList, selectedEntity, discloseStyle } = this.state
    const { svControls } = this.props
    const entityList = searchText ? searchEntityList : this.state.entityList || []
    if (loading) {
      return <Loader />
    }
    return (
      <div>
        <Accordion multiple
          activeItem={[0, 1, 2]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                actionButtons={this.actionButtons("administrationList", "")}
                title="Required information for G-10"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <EditableReactTable
                      gaGeneralFirmInfo={administrationList}
                      onItemChange={this.onItemChange}
                      onBlur={this.onBlur}
                      onSaveDoc={this.onSaveDoc}
                      onCancel={this.onCancel}
                      onEdit={this.onEdit}
                      onRemoveDoc={this.onRemoveDoc}
                      category="administrationList"
                      svControls={svControls.canSupervisorEdit && svControls.supervisor}
                      isEditable={isEditable}
                      errors={errorMessages}
                      label="Required information for G-10"
                    />
                  </div>
                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(1)}
                actionButtons={this.disclosureActionButton("disclosure")}
                title="Disclosure letter content"
              >
                {activeAccordions.includes(1) && (
                  <div style={{pointerEvents : discloseStyle}}>
                    <div className="control">
                      <CKEditor
                        activeClass="p10"
                        content={disclosureMessage || ""}
                        events={{
                          "change": (e) => this.onItemChange(e, "disclosureMessage")
                        }}
                      />
                    </div>
                  </div>
                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(2)}
                title="Select G-10 disclosure recipients"
              >
                {activeAccordions.includes(2) && (
                  <div>
                    <EntityListReactTable
                      entityList={entityList}
                      selectedEntity={selectedEntity}
                      searchText={searchText || ""}
                      onSearchEntity={this.onSearchEntity}
                      onSelectEntity={this.onSelectEntity}
                    />
                  </div>
                )}
              </RatingSection>

              <hr/>

              <div className="columns">
                { svControls && svControls.supervisor ?
                  <div className="column">
                    <div className="field is-grouped" style={{justifyContent: "end"}}>
                      <div className="control">
                        <button className="button is-link is-small" onClick={() => this.onPreviewPDF()} disabled={!selectedEntity.length}>Preview
                        </button>
                      </div>
                      <div className="control">
                        <button className="button is-link is-small" disabled={!selectedEntity.length} onClick={() => this.onAutoGenerate()}>Auto-generate and send
                        </button>
                      </div>
                    </div>
                  </div> : null
                }
              </div>

              <Disclaimer/>

            </div>
          }
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(RecordNames)
export default connect(mapStateToProps, null)(WrappedComponent)
