import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import Loader from "Global/Loader"
import { updateAuditLog } from "GlobalUtils/helpers"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import DocumentPage from "../../../../GlobalComponents/DocumentPage"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import { fetchPqDetails, putPqDocuments, pullPqDocuments } from "../../../../StateManagement/actions/Supervisor"
import moment from "moment";

class RecordKeeping extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      documentsList: []
    }
  }

  async componentWillMount() {
    fetchPqDetails("documents", (res) => {
      this.setState({
        documentsList: (res && res.pqDocuments && res.pqDocuments.length) ? cloneDeep(res.pqDocuments) : [],
        loading: false,
      })
    })
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    putPqDocuments("", docs, (res) => {
      if (res && res.status === 200) {
        toast("Professional Qualifications Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("documents")
        callback({
          status: true,
          documentsList: (res && res.data && res.data.pqDocuments && res.data.pqDocuments.length) ? cloneDeep(res.data.pqDocuments) : [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    } else {
      document = {
        ...doc,
        LastUpdatedDate: moment(new Date()).format("MM/DD/YYYY hh:mm A")
      }
      type = "updateMeta"
    }
    putPqDocuments(`?details=${type}`, document, (res) => {
      if (res && res.status === 200) {
        toast("Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if (name) {
          callback({
            status: true,
          })
        } else {
          callback({
            status: true,
            documentsList: (res.data && res.data.pqDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = async (documentId, callback) => {
    const res = await pullPqDocuments(`?docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.onAuditSave("document")
      callback({
        status: true,
        documentsList: (res.data && res.data.pqDocuments) || [],
      })
    } else {
      toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onAuditSave = async (key) => {
    const { user } = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Professional Qualifications"
        log.superVisorSubSection = "Professional Qualifications Documents"
      })
      // putPqAuditLogs(auditLogs, (res) => {
      //   if (res && res.status === 200) {
      //     this.setState({
      //       loading: false,
      //     })
      //   }
      // })
      await updateAuditLog("profQualification", auditLogs)
      // const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      // this.props.updateAuditLog(remainLogs)
    }
  }

  render() {
    const { documentsList, loading } = this.state
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Professional Qualifications",
    }
    if(loading){
      return <Loader/>
    }
    return (
      <div>
        <div>
          <DocumentPage {...this.props} isNotTransaction onSave={this.onDocSave} tranId={this.props.nav2}
                        title="Upload Professional Qualifications Documents" onStatusChange={this.onStatusChange}
                        pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickType="LKUPDOCTYPE"
                        pickAction="LKUPCOMPLIANCEDOCACTION" staticField={staticField} category="pqDocuments" /* category is optional */
                        documents={documentsList} contextType={ContextType.supervisor.svProQualification}
                        tableStyle={{ fontSize: "smaller" }} onDeleteDoc={this.onDeleteDoc} />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(RecordKeeping)
export default connect(mapStateToProps, null)(WrappedComponent)
