import React, { Component } from "react"
import { connect} from "react-redux"
import {
  getUserList
} from "AppState/actions/AdminManagement/admTrnActions"
import DashboardTable from "./RecordNameTable"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"

class RecordNames extends Component {
  constructor(props) {
    super(props)

    this.state = {
      entityList: [],
      isLoading: false
    }
  }

  async componentWillMount() {
    const payload = {
      filters:{
        userContactTypes:[],
        entityMarketRoleFlags:[],
        entityIssuerFlags:[],
        userPrimaryAddressStates:[],
        freeTextSearchTerm:"",
        entityId: this.props.user.entityId
      },
      pagination:{
        serverPerformPagination:true,
        currentPage:0,
        size:100000,
        sortFields:{
          entityName:1
        }
      }
    }

    const result = await getUserList(payload)
    const data = await result.data
    if (data) {
      const decoupledValue = this.decoupleResult(data.pipeLineQueryResults[0])
      this.setState({
        loading: false,
        entityList: decoupledValue.data,
        total: decoupledValue.total,
        pages: decoupledValue.pages,
      })
      return decoupledValue
    }
    return {
      total: 0,
      pages: 0,
      data: []
    };
  }

  decoupleResult = result => ({
    total: result && result.metadata.length > 0 ? result.metadata[0].total : 0,
    pages: result && result.metadata.length > 0 ? result.metadata[0].pages : 0,
    data: result ? result.data : []
  });


  render() {
    const { loading, entityList } = this.state

    return (
      <div>
        {loading ? <Loader/> : null}
        <DashboardTable
          entityList={entityList}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(RecordNames)
export default connect(mapStateToProps, null)(WrappedComponent)
