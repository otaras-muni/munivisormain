import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import { getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import Loader from "Global/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import EditableTable from "../../../../GlobalComponents/EditableTable"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import {
  fetchDesignateDetails,
  pullGenAdmin,
  putGenAdmin
} from "../../../../StateManagement/actions/Supervisor"
import { fetchAssigned } from "../../../../StateManagement/actions/CreateTransaction"
import CONST from "../../../../../globalutilities/consts"
import {PeopleQualifiedValidate} from "../../Validation/GeneralAdminValidation"

class Qualifications extends Component {
  constructor(props) {
    super(props)

    this.state = {
      accordions: [1, 2, 3, 4, 5].map((r) => ({ accIndex: r, accOpen: false })),
      isEditable: {},
      tables: [
        {
          name:"Qualified Associated Persons",
          key: "gaQualifiedAssPersons",
          header: [
            {name: "Qualification<span class='has-text-danger'>*</span>"},
            {name: "Name<span class='has-text-danger'>*</span>"},
            {name: "Email<span class='has-text-danger'>*</span>"},
            {name: "Phone<span class='has-text-danger'>*</span>"},
            {name: "Fee Paid On"},
            {name: "Qualified On"},
            {name: "Qualified Till"},
            {name: "Action"}
          ],
          row:  cloneDeep(CONST.SuperVisor.gaQualifiedAssPersons) || {},
          list: [],
          tbody: [
            {name: "qualification", type: "dropdown", dropdownKey: "qualification", disabledDropdownKey: "disabledQualification", labelName: "qualification"},
            {name: "name", type: "dropdown", labelName: "userFirstName", dropdownKey: "users", style: {minWidth: "150px"}},
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "profFeePaidOn", placeholder: "Fee Paid On", type: "date"},
            {name: "series50PassDate", placeholder: "Qualified On", type: "date"},
            {name: "series50ValidityEndDate", placeholder: "Qualified Till", type: "date"},
            {name: "action"},
          ],
          handleError: (payload, minDate) => PeopleQualifiedValidate(payload, minDate)
        },
      ],
      notes: CONST.CAC.profQulifications,
      dropDown: {},
      auditLogs: [],
      createdAt: "",
      confirmAlert: CONST.confirmAlert,
      loading: true,
    }
  }

  async componentWillMount() {
    const { tables } = this.state
    const { svControls } = this.props
    let result = await getPicklistByPicklistName(["LKUPROLEWITHENTITY", "LKUPCMPLQUALIFICATION"])
    result = (result.length && result[1]) || {}
    fetchDesignateDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          userContactType: ["Designated contact A-12", "Designated contact EMMA", "Designated contact SEC", "Designated contact FINRA",
            "Other (please specify)"],
        },
        loading: false,
        tables,
        auditLogs: (res && res.auditLogs) || [],
        transaction: res,
      },() => {
        this.getUsers()
      })
    })
  }

  async componentDidMount(){
    const accordionInitiate = [1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false}))
    console.log("ACCINITIAL STATE", accordionInitiate)
    const result = await getPicklistByPicklistName(["LKUPCMPLREGISTERINGBODY", "LKUPCMPLQUALIFICATION"])
    const disabledQualification = result && result[1] && result[1].LKUPCMPLQUALIFICATION && result[1].LKUPCMPLQUALIFICATION.filter(e => !e.included).map(agency => agency.label) || []
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        registeringBody: result && result[1] && result[1].LKUPCMPLREGISTERINGBODY || [],
        qualification: result && result[1] && result[1].LKUPCMPLQUALIFICATION && result[1].LKUPCMPLQUALIFICATION.map(e => e.label) || [] || [],
        disabledQualification
      },
      accordions:accordionInitiate,
      isLoading:false
    })
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    })
  }

  onSave = (type, item, callback) => {
    putGenAdmin(type, item, (res)=> {
      if (res && res.status === 200) {
        toast("Added Qualified Associated Persons successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    pullGenAdmin(type, removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed Qualified Associated Persons successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users) => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        loading: false,
      })
    })
  }

  onDropDownChange = async (inputName, key, select, index, callback) => {
    if (key === "gaQualifiedAssPersons") {
      const user = select
      let email = ""
      let phone = ""
      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }
      if(inputName === "qualification"){
        callback({
          status: true,
          object: {
            qualification: select,
          }
        })
      } else {
        this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
        callback({
          status: true,
          object: {
            userId: user.id,
            userFirstName: `${user.userFirstName} ${user.userLastName}`,
            userLastName: user.userLastName,
            userPrimaryEmailId: email,
            userPrimaryPhone: phone,
            name: user.name || ""
          },
        })
      }
    }
  }

  onAuditSave = async (key) => {
    const { user } = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    const table = this.state.tables.find(tbl => tbl.key === key)
    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Professional Qualifications"
        log.superVisorSubSection = table.name
      })
      await updateAuditLog("profQualification", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  renderKeyDetails() {
    const { dropDown, tables } = this.state
    const { svControls } = this.props
    return (
      <div>
        <div>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
          <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle" /></a>
        </div>
        <div>
          <EditableTable {...this.props} tables={tables} canSupervisorEdit={svControls.canSupervisorEdit} dropDown={dropDown} onSave={this.onSave} onRemove={this.onRemove} onParentChange={this.onDropDownChange} />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }

  render() {
    const { accordions } = this.state

    if (this.state.loading) {
      return <Loader />
    }
    return (
      accordions && <div>
        {this.renderKeyDetails(1)}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Qualifications)
export default connect(mapStateToProps, null)(WrappedComponent)
