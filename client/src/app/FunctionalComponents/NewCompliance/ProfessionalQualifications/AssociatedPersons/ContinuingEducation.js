import React, { Component } from "react"
import swal from "sweetalert"
import axios from "axios"
import { getDocDetails, getToken, updateAuditLog } from "GlobalUtils/helpers"
import Loader from "Global/Loader"
import { muniApiBaseURL } from "GlobalUtils/consts"
import {toast} from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import { connect } from "react-redux"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import {fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import TrainingProgramDetail from "./TrainingProgramDetail"
import CONST from "../../../../../globalutilities/consts"
import {
  fetchPqDetails,
  pullPqPersonsOrTraining,
  pullPqDocuments,
  putPqPersonsOrTraining
} from "../../../../StateManagement/actions/Supervisor"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import { TrainingDetailValidate } from "../../Validation/ProfQulificationsValidate"
import DashboardTable from "./RecordNameTable"


class ContinuingEducation extends Component {
  constructor(props) {
    super(props)

    this.state = {
      bucketName: process.env.S3BUCKET,
      trainingDetailList: [],
      filename: "",
      pqTrainingDetail: {
        ...cloneDeep(CONST.SuperVisor.trainingDetail),
      },
      dropDown: {
        topicList: []
      },
      errorMessages: {},
      docFile: false,
      onShowDetail: false,
      loading: true,
      confirmAlert: CONST.confirmAlert
    }
  }

  async componentWillMount() {
    const result = await getPicklistByPicklistName(["LKUPPROGRAMLIST"])
    fetchPqDetails("pqTrainingDetail", (res) => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          topicList: result && result[1] && result[1].LKUPPROGRAMLIST || []
        },
        trainingDetailList: res && res.pqTrainingDetail || []
      })
    })
    this.getUsers()
  }

  onFileSetInState = (e) => {
    const { pqTrainingDetail } = this.state
    pqTrainingDetail.fileName = e.name || ""
    this.setState({
      pqTrainingDetail,
      filename: e.name || ""
    })
  }

  onChangeItems = (item, category) => {
    this.setState({
      [category]: item
    })
  }

  onSave = () => {
    const { pqTrainingDetail, filename } = this.state
    const errors = TrainingDetailValidate(pqTrainingDetail)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }
    if(filename){
      this.setState({
        docFile: true,
      })
    }
  }

  onUploadSuccess = (filename, docId) => {
    const { pqTrainingDetail, docFile } = this.state
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    pqTrainingDetail.conductedOn = pqTrainingDetail.conductedOn ? pqTrainingDetail.conductedOn : new Date()
    if(docFile && docId && filename){
      pqTrainingDetail.AWSFileLocation = docId
      this.props.addAuditLog({userName, log: `In Training Program Detail Add New File ${filename}`, date: new Date(), key: "pqTrainingDetail"})
    }
    const errors = TrainingDetailValidate(pqTrainingDetail)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }
    this.props.addAuditLog({userName, log: pqTrainingDetail && pqTrainingDetail._id ? "Training Program Detail In One Program Detail edited" :
      "Training Program Detail In Add New Program Detail", date: new Date(), key: "pqTrainingDetail"})
    this.setState({
      loading: true
    }, () => {
      putPqPersonsOrTraining("pqTrainingDetail", pqTrainingDetail, (res) => {
        if (res && res.status === 200) {
          toast("Add Continuing Education successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("pqTrainingDetail")
          this.setState({
            trainingDetailList: res && res.data && res.data.pqTrainingDetail || [],
            pqTrainingDetail: {
              ...cloneDeep(CONST.SuperVisor.trainingDetail),
            },
            errorMessages: {},
            filename: "",
            docFile: false,
            loading: false
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onDeleteDoc = async (documentId, callback) => {
    if(documentId && documentId.AWSFileLocation){
      documentId.AWSFileLocation = ""
      documentId.fileName = ""
      this.setState({
        loading: true
      }, () => {
        putPqPersonsOrTraining("pqTrainingDetail", documentId, (res) => {
          if (res && res.status === 200) {
            toast("Document removed successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            this.onAuditSave("pqTrainingDetail")
            this.setState({
              trainingDetailList: res && res.data && res.data.pqTrainingDetail || [],
              pqTrainingDetail: {
                ...cloneDeep(CONST.SuperVisor.trainingDetail),
              },
              errorMessages: {},
              filename: "",
              docFile: false,
              loading: false
            })
          } else {
            toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          }
        })
      })
    }
  }

  onDeleteTrainingDetail = async (docs) => {
    const { bucketName, confirmAlert } = this.state
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    let files = []
    let doc = []
    if(docs.AWSFileLocation) {
      doc = await getDocDetails(docs.AWSFileLocation)
      const {contextId, contextType, tenantId} = doc && doc[1]
      files = doc[1].meta.versions.map(e => ({
        Key: `${tenantId}/${contextType}/${contextType}_${contextId}/${e.name}`,
        VersionId: e.versionId
      }))
    }

    confirmAlert.text = "You want to delete this Training Program List?"
    swal(confirmAlert)
      .then((willDelete) => {
        if (bucketName && files) {
          if (willDelete) {
            this.setState({
              loading: true
            }, () => {
              this.props.addAuditLog({userName, log: `Training Program Detail in Program Name ${docs.programName} Deleted`, date: new Date(), key: "pqTrainingDetail"})
              if(docs.AWSFileLocation){
                axios.post(`${muniApiBaseURL}/s3/delete-s3-object`, {bucketName, files}, {headers: {"Authorization": getToken()}})
                  .then(res => {
                    console.log("res : ", res)
                    if (res.error) {
                      console.log("Error in deleting ")
                    } else {
                      axios.delete(`${muniApiBaseURL}/docs/${doc[1]._id}`,{headers:{"Authorization":getToken()}})
                        .then(res => {
                          console.log("document removed ok : ", res)
                          this.onRemove(docs._id)
                        })
                        .catch(err => {
                          console.log("err in deleting document : ", err)
                        })
                    }
                  })
              } else {
                this.onRemove(docs._id)
              }
            })
          }
        }
      })
  }

  onRemove = (docId) => {
    pullPqPersonsOrTraining("pqTrainingDetail", docId, (res) => {
      if (res && res.status === 200) {
        toast("Removed Continuing Education successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("pqTrainingDetail")
        this.setState({
          trainingDetailList: res && res.data && res.data.pqTrainingDetail || [],
          onShowDetail: false,
          filename: "",
          loading: false
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      }
    })
  }

  onEdit = (doc) => {
    this.setState({
      onShowDetail: true,
      filename: "",
      errorMessages: {},
      pqTrainingDetail: doc || {}
    })
  }

  onAdd = () => {
    this.setState({
      pqTrainingDetail: {
        ...cloneDeep(CONST.SuperVisor.trainingDetail),
      },
      filename: "",
      errorMessages: {},
      onShowDetail: true
    })
  }

  onReset = () => {
    this.setState({
      pqTrainingDetail: {
        ...cloneDeep(CONST.SuperVisor.trainingDetail),
      },
      filename: "",
      onShowDetail: false
    })
  }

  onCancel = () => {
    this.setState({
      pqTrainingDetail: {
        ...cloneDeep(CONST.SuperVisor.trainingDetail),
      },
      filename: "",
      errorMessages: {}
    })
  }

  onAuditSave = async (key) => {
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "Professional Qualifications"
        log.superVisorSubSection = `${key === "gaDesignateFirmContacts" ? "Designate Firm Contacts" : "Qualified Associated Persons"}`
      })
      await updateAuditLog("profQualification", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  onBlur = (change, category) => {
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: `In Training Program Detail ${change}`, date: new Date(), key: category})
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, users => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          attendeesList: users.usersList || []
        },
        loading: false
      })
    })
  }

  actionButtons = (isDisabled) =>{
    if (isDisabled) return
    return(
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd()}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset()}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }


  render() {
    const { dropDown, pqTrainingDetail, trainingDetailList, docFile, bucketName, onShowDetail, errorMessages, filename } = this.state
    const { user, svControls } = this.props
    if (this.state.loading) {
      return <Loader />
    }
    return (
      <div>
        <div>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
          <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle" /></a>
        </div>
        <div>
          <Accordion
            multiple
            activeItem={[0, 1]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  style={{ overflowX: "hidden" }}
                  title="Training Program List"
                  actionButtons={this.actionButtons(!svControls.canSupervisorEdit)}
                >
                  {activeAccordions.includes(0) && (
                    <div className="overflow-auto">
                      <DashboardTable
                        entityList={trainingDetailList}
                        svControls={svControls}
                        onEdit={this.onEdit}
                        onDeleteTrainingDetail={this.onDeleteTrainingDetail}
                        conEducation
                      />
                    </div>
                  )}
                </RatingSection>
                {onShowDetail ?
                  <RatingSection
                    onAccordion={() => onAccordion(1)}
                    title="Training Program Detail"
                  >
                    {activeAccordions.includes(1) && (
                      <div>
                        <TrainingProgramDetail
                          dropDown={dropDown}
                          item={pqTrainingDetail}
                          category="pqTrainingDetail"
                          docFile={docFile}
                          onFileSetInState={this.onFileSetInState}
                          onChangeItems={this.onChangeItems}
                          onSave={this.onSave}
                          errors={errorMessages}
                          onUploadSuccess={this.onUploadSuccess}
                          onDeleteDoc={this.onDeleteDoc}
                          onCancel={this.onCancel}
                          onBlur={this.onBlur}
                          bucketName={bucketName}
                          filename={filename}
                          canEdit={svControls.canSupervisorEdit}
                          contextId="cmp-sup-prof"
                          contextType="SVPROFQUALIFICATIONS"
                          tenantId={user.entityId}
                          user={user}
                        />
                      </div>
                    )}
                  </RatingSection> : null
                }
              </div>
            )}
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(ContinuingEducation)
export default connect(mapStateToProps, null)(WrappedComponent)
