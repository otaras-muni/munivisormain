import React from "react"
import moment from "moment"
import SingleFileDocUpload from "../../../../GlobalComponents/SingleFileDocUpload"
import {TextLabelInput, SelectLabelInput, MultiSelect} from "../../../../GlobalComponents/TextViewBox"
import DocLink from "../../../docs/DocLink"
import { DocModal } from "../../../docs/DocModal"


const TrainingProgramDetail = ({
  dropDown,
  item,
  category,
  onChangeItems,
  onFileSetInState,
  docFile,
  onSave,
  bucketName,
  contextId,
  contextType,
  tenantId,
  user,
  errors = {},
  onUploadSuccess,
  onDeleteDoc,
  canEdit,
  filename,
  onCancel,
  onBlur,
}) => {
  const onChange = e => {
    const newItem = {
      ...item,
      [e.target.name]: e.target.value
    }
    onChangeItems(newItem, category)
  }

  const onAssignedSelect = (items, name) => {
    onChangeItems({
      ...item,
      [name]: items.map(item => {
        const user = item
        let email = ""
        if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
          email = user.userEmails.filter(e => e.emailPrimary)
          email = email.length ? email[0].emailId : user.userEmails[0].emailId
        }
        return {
          id: item.id || item._id,
          name: item.name || `${item.userFirstName} ${item.userLastName}`,
          userEntityId: item.entityId || "",
          userFirmName: item.userFirmName || "",
          userEmailId: email || "",
        }
      })
    },category)
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(`${event.target.title || "empty"} change to ${event.target.value || "empty"}`,category)
    }
  }

  const require = <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>

  return (
    <div >
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl ">Topic{require}</p>
          <SelectLabelInput
            title="Topic"
            name="topic"
            list={dropDown.topicList || []}
            value={(item && item.topic) || ""}
            onChange={onChange}
            onBlur={onBlurInput}
            error={errors.topic}
            disabled={!canEdit}
          />
        </div>
        <div className="column">
          <p className="multiExpLbl ">Program Name{require}</p>
          <TextLabelInput
            title="Program Name"
            name="programName"
            value={(item && item.programName) || ""}
            onChange={onChange}
            onBlur={onBlurInput}
            error={errors.programName}
            disabled={!canEdit}
          />
        </div>
        <div className="column">
          <p className="multiExpLbl ">Training Date{require}</p>
          <TextLabelInput
            type="date"
            title="Training Date"
            name="trainingDate"
            // value={(item && item.trainingDate && moment(item && item.trainingDate).format("YYYY-MM-DD")) || ""}
            value={(item.trainingDate === "" || !item.trainingDate) ? null : new Date(item.trainingDate)}
            onChange={onChange}
            onBlur={onBlurInput}
            error={errors.trainingDate}
            disabled={!canEdit}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl ">Conducted by</p>
          <TextLabelInput
            title="Conducted by"
            name="conductedBy"
            value={(item && item.conductedBy) || ""}
            onChange={onChange}
            onBlur={onBlurInput}
            error={errors.conductedBy}
            disabled={!canEdit}
          />
        </div>
        <div className="column">
          <p className="multiExpLbl ">Training Content</p>
          {!canEdit ? null :
            <div>
              {item._id ?
                <SingleFileDocUpload
                  onFileSetInState={onFileSetInState}
                  docFile={docFile}
                  bucketName={bucketName}
                  contextId={contextId}
                  docId={item.AWSFileLocation}
                  contextType={contextType}
                  tenantId={tenantId}
                  user={user}
                  onUploadSuccess={onUploadSuccess}
                  isNew={!item._id}
                /> :
                <SingleFileDocUpload
                  onFileSetInState={onFileSetInState}
                  docFile={docFile}
                  bucketName={bucketName}
                  contextId={contextId}
                  contextType={contextType}
                  tenantId={tenantId}
                  user={user}
                  onUploadSuccess={onUploadSuccess}
                  isNew={!item._id}
                />
              }
            </div>
          }
          {errors.fileName && (
            <p className="text-error">{errors.fileName}</p>
          )}
        </div>
        <div className="column">
          <p className="multiExpLbl ">Filename</p>
          <div className="field is-grouped" style={{ justifyContent: "end" }}>
            { item && item._id && item.AWSFileLocation ?
              <div className="field is-grouped">
                {filename ?
                  <p className="multiExpLbl">{item && item.fileName}</p> :
                  <DocLink docId={item.AWSFileLocation}/>
                }
                { !canEdit ? null :
                  <DocModal
                    selectedDocId={item.AWSFileLocation}
                    versionMetaToShow={["uploadedBy"]}
                    onDeleteAll={() => onDeleteDoc(item)}
                  />
                }
              </div>
              :
              <p className="multiExpLbl">{item && item.fileName}</p>
            }
          </div>
        </div>
      </div>
      <div className="columns">
        <MultiSelect
          filter
          label="Attendees"
          data={dropDown.attendeesList || []}
          value={(item && item.attendees) || []}
          onChange={item => onAssignedSelect(item, "attendees")}
          style={{ maxWidth: "unset" }}
          isFull
          error={errors.attendees}
          required
        />
      </div>
      <div className="columns">
        <div className="column is-full">
          <p className="multiExpLbl">Notes/Instructions</p>
          <textarea
            title="Notes/Instructions"
            className="textarea is-link"
            placeholder=""
            name="notes"
            value={item && item.notes || ""}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!canEdit}
          />
          {errors.notes && (
            <p className="text-error">{errors.notes}</p>
          )}
        </div>
      </div>
      <div className="columns is-centered">
        {!canEdit ? null :
          <div className="field is-grouped">
            <div className="column control">
              <button className="button is-link is-small" onClick={item._id ? filename === "" ? onUploadSuccess : onSave : filename === "" ? onUploadSuccess : onSave} disabled={docFile}>
                Save
              </button>
            </div>
            <div className="column control">
              <button className="button is-light is-small" onClick={onCancel} disabled={docFile}>
                Cancel
              </button>
            </div>
          </div>
        }
      </div>
    </div>
  )
}

export default TrainingProgramDetail
