import React from "react"
import { Link } from "react-router-dom"
import moment from "moment"
import ReactTable from "react-table"

const DashboardTable = ({
  entityList,
  conEducation,
  svControls,
  onEdit,
  onDeleteTrainingDetail,
  pageSizeOptions = [5, 10, 20, 50, 100],
}) => {

  let columns = []
  if(conEducation){
    columns = [

      {
        Header: "Program Name",
        id: "programName",
        className: "multiExpTblVal",
        accessor: item => item,
        // minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd -striped">
              {item && item.programName || "--"}
            </div>
          )
        },
        sortMethod: (a, b) => `${a.programName}` - `${b.programName}`
      },
      {
        Header: "Conducted on",
        id: "conductedOn",
        className: "multiExpTblVal",
        accessor: item => item,
        // minWidth: 150,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item && item.conductedOn ? moment(item.conductedOn).format("MM-DD-YYYY hh:mm A"): "" || "--"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => a.conductedOn - b.conductedOn
      },
      {
        Header: "Conducted by",
        id: "conductedBy",
        className: "multiExpTblVal",
        accessor: item => item,
        // minWidth: 150,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item && item.conductedBy || "--"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => `${a.conductedBy}` - `${b.conductedBy}`
      },
      {
        Header: "Attendees",
        id: "attendees",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 330,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {item.attendees.map((a, i) =>
                <span>
                  {a.name}{item.attendees.length - 1 === i ? "" : "," }&nbsp;
                </span>
              )}
            </div>
          )
        },

        sortMethod: (a, b) => `${a.attendees}` - `${b.attendees}`
      },
      {
        Header: "Notes",
        id: "notes",
        className: "multiExpTblVal",
        accessor: item => item,
        // minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item && item.notes || "--"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => a.notes - b.notes
      },
      {
        Header: "Action",
        id: "exitDate",
        className: "multiExpTblVal",
        accessor: item => item,
        // minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text level-right">
              <span className="has-text-link">
                <a className="fas fa-pencil-alt" onClick={() => onEdit(item)}/>
              </span>&nbsp;&nbsp;
              { (svControls && svControls.canSupervisorEdit) ?
                <span className="has-text-link">
                  <a className="far fa-trash-alt" onClick={() => onDeleteTrainingDetail(item)}/>
                </span> : null
              }
            </div>
          )
        },
      },
    ]
  } else {
    columns = [

      {
        Header: "Full Name",
        id: "userFullName",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd -striped">
              <Link to={`/admin-users/${item.userId}/users`}>
                <small>
                  {item.userFullName || "-"}
                </small>
              </Link>
            </div>
          )
        },
        sortMethod: (a, b) => `${a.userFullName}` - `${b.userFullName}`

        // sortMethod: (a, b) => a.firstName.localeCompare(b.userFullName)
      },
      {
        Header: "Job Title",
        id: "userJobTitle",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 150,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item.userJobTitle && item.userJobTitle.toString() || "--"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => a.userJobTitle !== undefined && b.userJobTitle !== undefined ? a.userJobTitle.localeCompare(b.userJobTitle) : ""
      },
      {
        Header: "Primary Email",
        id: "userPrimaryEmail",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 150,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item.userPrimaryEmail && item.userPrimaryEmail.toString() || "-"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => `${a.userPrimaryEmail}` - `${b.userPrimaryEmail}`
      },
      {
        Header: "Primary Phone",
        id: "userPrimaryPhoneNumber",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item.userPrimaryPhoneNumber && item.userPrimaryPhoneNumber.toString() || "-"}
              </div>
            </div>
          )
        },

        sortMethod: (a, b) => `${a.userPrimaryPhoneNumber}` - `${b.userPrimaryPhoneNumber}`
      },
      {
        Header: "Join Date",
        id: "joinDate",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item && item.userJoiningDate ? moment(item && item.userJoiningDate).format("MM-DD-YYYY h:mm A") : "--"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => a.userJoiningDate !== undefined && b.userJoiningDate !== undefined ? a.userJoiningDate.localeCompare(b.userJoiningDate) : ""
      },
      {
        Header: "Exit Date",
        id: "exitDate",
        className: "multiExpTblVal",
        accessor: item => item,
        minWidth: 100,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {item && item.userExitDate ? moment(item && item.userExitDate).format("MM-DD-YYYY h:mm A") : "--"}
              </div>
            </div>
          )
        },
        sortMethod: (a, b) => a.userExitDate !== undefined && b.userExitDate !== undefined ? a.userExitDate.localeCompare(b.userExitDate) : ""
      },
    ]
  }

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={entityList || []}
        showPaginationBottom
        defaultPageSize={conEducation ? 5 : 100}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default DashboardTable

