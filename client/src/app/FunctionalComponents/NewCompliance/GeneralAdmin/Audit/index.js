import React, { Component } from "react"
import { connect} from "react-redux"

import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import Audit from "../../../../GlobalComponents/Audit"
import Loader from "../../../../GlobalComponents/Loader"
import {
  fetchDesignateDetails
} from "../../../../StateManagement/actions/Supervisor"

class People extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
    }
  }

  async componentWillMount() {
    fetchDesignateDetails((res) => {
      this.setState({
        auditLogs: (res && res.auditLogs) || [],
        loading: false
      })
    })
  }

  render() {
    const { loading, auditLogs } = this.state

    return (
      <div>
        <div>
          {loading ? <Loader/> : null}
          <Audit auditLogs={auditLogs || []}/>
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

export default connect(mapStateToProps, null)(People)
