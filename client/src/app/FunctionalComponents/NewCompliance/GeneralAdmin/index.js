import React, { Component } from "react"
import Loader from "Global/Loader"

import RecordKeeping from "./RecordKeeping"
import Admin from "./Admin"
import Audit from "../../../GlobalComponents/Audit"
import {getAuditLogByType} from "../../../StateManagement/actions/audit_log_actions"

class GeneralAdmin extends Component {

  constructor(props) {
    super(props)
    this.state = {
      auditLogs: [],
      loading:true,
    }
  }

  componentWillMount() {
    getAuditLogByType("generalAdmin", "generalAdmin", res => {
      // audit = res && res.changeLog || []
      this.setState({
        auditLogs: res && res.changeLog || [],
        loading: false,
      })
    })
  }

  renderSelectedView = (nav2, nav3) => {
    switch (nav2) {
    case "cmp-sup-general":
      switch (nav3) {
      case "admin":
        return <Admin {...this.props} />
      case "record-keeping":
        return <RecordKeeping {...this.props} />
      case "audit":
        return <Audit {...this.props} auditLogs={this.state.auditLogs}/>
      default:
        return <Admin {...this.props} />
      }
    default :
      return nav2
    }
  }

  render() {
    const {nav2, nav3} = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="column">
        <section id="main">
          {this.renderSelectedView(nav2, nav3)}
        </section>
      </div>
    )
  }
}

export default GeneralAdmin
