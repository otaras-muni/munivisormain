import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import { updateAuditLog } from "GlobalUtils/helpers"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import Documents from "../../../../GlobalComponents/DocumentPage"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import {
  fetchGenAdminDocuments,
  pullGenAdminDocument,
  putGenAdminDocument,
} from "../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import moment from "moment";

class RecordKeeping extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      documentsList: [],
    }
  }

  componentWillMount() {
    fetchGenAdminDocuments((res) => {
      this.setState({
        documentsList: (res && res.gaDocuments && res.gaDocuments.length) ? cloneDeep(res.gaDocuments) : [],
        loading: false,
      })
    })
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    putGenAdminDocument("", docs, (res) => {
      if (res && res.status === 200) {
        toast("Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.gaDocuments) || [],
        })
        this.onAuditSave("documents")
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    } else {
      document = {
        ...doc,
        lastUpdatedDate: moment(new Date()).format("MM/DD/YYYY hh:mm A")
      }
      type = "updateMeta"
    }
    putGenAdminDocument(`?details=${type}`, document, (res) => {
      if (res && res.status === 200) {
        toast("Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if (name) {
          callback({
            status: true,
          })
        } else {
          callback({
            status: true,
            documentsList: (res.data && res.data.gaDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = async (documentId, callback) => {
    const res = await pullGenAdminDocument(`?docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.onAuditSave("document")
      callback({
        status: true,
        documentsList: (res.data && res.data.gaDocuments) || [],
      })
    } else {
      toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onAuditSave = async (key) => {
    const { user } = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Professional Qualifications"
        log.superVisorSubSection = "Professional Qualifications Documents"
      })
      // putGenAuditLogs(auditLogs, (res) => {
      //   if (res && res.status === 200) {
      //     this.setState({
      //       loading: false,
      //     })
      //   }
      // })
      await updateAuditLog("generalAdmin", auditLogs)
      // const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      // this.props.updateAuditLog(remainLogs)
    }
  }

  render() {
    const { loading, documentsList } = this.state
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "General Administration",
    }
    if(loading){
      return <Loader/>
    }
    return (
      <div>
        <div>
          <Documents {...this.props} isNotTransaction onSave={this.onDocSave} tranId={this.props.nav2} title="Documents"
                     pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickType="LKUPDOCTYPE"
                     pickAction="LKUPCOMPLIANCEDOCACTION" onStatusChange={this.onStatusChange} staticField={staticField}
                     documents={documentsList} category="gaDocuments" contextType={ContextType.supervisor.svAdmin}
                     tableStyle={{ fontSize: "smaller" }} onDeleteDoc={this.onDeleteDoc} />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(RecordKeeping)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
