import React, { Component } from "react"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify"
import { connect} from "react-redux"
import { getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import EditableTable from "../../../../GlobalComponents/EditableTable"
import {PeopleQualifiedValidate, PeopleDesignateValidate, FirmRegisterValidate} from "../../Validation/GeneralAdminValidation"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import Loader from "../../../../GlobalComponents/Loader"
import CONST from "../../../../../globalutilities/consts"
import {fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import {
  fetchDesignateDetails, putGenAdmin, pullGenAdmin
} from "../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"


class People extends Component {
  constructor(props) {
    super(props)

    this.state = { accordions:[1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false})),
      designateFirmContacts: [CONST.SuperVisor.gaFirmRegInformation],
      qualifiedAssPersons: [CONST.SuperVisor.gaQualifiedAssPersons],
      isEditable: {},
      tables: [
        {
          name:"Firm Registration Information",
          key: "gaFirmRegInformation",
          row: cloneDeep(CONST.SuperVisor.gaFirmRegInformation) || {},
          header: [
            {name: "Registering Body<span class='has-text-danger'>*</span>"},
            {name: "Registration Fee Paid on"},
            {name: "Fee Amount<span class='has-text-danger'>*</span>"},
            {name: "Registration Valid Till"},
            {name: "Action"}
          ],
          list: [],
          tbody: [
            { name: "registeringBody", type: "select", dropdownKey: "registeringBody", labelName: "registeringBody"},
            {name: "regFeePaidOn", placeholder: "Registration Fee Paid on", type: "date"},
            {name: "feeAmount", placeholder: "$", type: "number", prefix: "$", error: "Fee Amount must be larger than or equal to 0"},
            {name: "regValidTill", placeholder: "Registration Valid Till", type: "date", error: "must be larger than or equal to Registration Fee Paid on"},
            {name: "action"},
          ],
          handleError: (payload) => PeopleDesignateValidate(payload),
        },
        {
          name:"Qualified Associated Persons",
          key: "gaQualifiedAssPersons",
          header: [
            {name: "Qualification<span class='has-text-danger'>*</span>"},
            {name: "Name<span class='has-text-danger'>*</span>"},
            {name: "Email<span class='has-text-danger'>*</span>"},
            {name: "Phone<span class='has-text-danger'>*</span>"},
            {name: "Fee Paid On"},
            {name: "Qualified On"},
            {name: "Qualified Till"},
            {name: "Action"}
          ],
          row:  cloneDeep(CONST.SuperVisor.gaQualifiedAssPersons) || {},
          list: [],
          tbody: [
            {name: "qualification", type: "dropdown", dropdownKey: "qualification", disabledDropdownKey: "disabledQualification", labelName: "qualification"},
            {name: "name", type: "dropdown", labelName: "userFirstName", dropdownKey: "users", style: {minWidth: "150px"}},
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "profFeePaidOn", placeholder: "Fee Paid On", type: "date"},
            {name: "series50PassDate", placeholder: "Qualified On", type: "date"},
            {name: "series50ValidityEndDate", placeholder: "Qualified Till", type: "date"},
            {name: "action"},
          ],
          handleError: (payload, minDate) => PeopleQualifiedValidate(payload, minDate)
        },
      ],
      notes: CONST.CAC.genAdmin,
      dropDown:{},
      usersList: [],
      loading: true,
    }
  }

  async componentWillMount() {
    const {tables} = this.state
    const {svControls} = this.props
    console.log("===================>", svControls.canSupervisorEdit)
    let result = [] // await getPicklistByPicklistName(["LKUPCONDUCTVIOLATION"])
    result = (result.length && result[1]) || {}
    fetchDesignateDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          userContactType: ["Designated contact A-12", "Designated contact EMMA", "Designated contact SEC", "Designated contact FINRA",
            "Other (please specify)"],
        },
        loading: false,
        tables,
        auditLogs: (res && res.auditLogs) || [],
        transaction: res,
      },() => {
        this.getUsers()
      })
    })
  }

  async componentDidMount(){
    const accordionInitiate = [1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false}))
    console.log("ACCINITIAL STATE", accordionInitiate)
    const result = await getPicklistByPicklistName(["LKUPCMPLREGISTERINGBODY", "LKUPCMPLQUALIFICATION"])
    const disabledQualification = result && result[1] && result[1].LKUPCMPLQUALIFICATION && result[1].LKUPCMPLQUALIFICATION.filter(e => !e.included).map(agency => agency.label) || []
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        registeringBody: result && result[1] && result[1].LKUPCMPLREGISTERINGBODY || [],
        qualification: result && result[1] && result[1].LKUPCMPLQUALIFICATION && result[1].LKUPCMPLQUALIFICATION.map(e => e.label) || [],
        disabledQualification
      },
      accordions:accordionInitiate,
      isLoading:false
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        loading: false,
      })
    })
  }

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    pullGenAdmin(type, removeId, (res) => {
      if(res && res.status === 200) {
        toast(`Removed ${type === "gaFirmRegInformation" ? "Firm Register Information" : "Qualified Associated Persons"} successfully`,{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    console.log(type, item)
    putGenAdmin(type, item, (res)=> {
      if (res && res.status === 200) {
        toast(`Added ${type === "gaFirmRegInformation" ? "Firm Register Information" : "Qualified Associated Persons"} successfully`,{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    let email = ""
    let phone = ""
    let userAdd = ""
    console.log("=================================>", user)
    if (key === "gaFirmRegInformation") {

      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }

      if(user && Array.isArray(user.userAddresses) && user.userAddresses.length) {
        userAdd = user.userAddresses.find(e => e.primaryAddress)
        userAdd = userAdd ? userAdd.userAddresses.addressLine1 :user.userAddresses[0].addressLine1
      }

      // this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
      callback({
        status: true,
        object: {
          [inputName]: select
        },
      })
    } else {
      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }
      if(inputName === "qualification"){
        callback({
          status: true,
          object: {
            qualification: select,
          }
        })
      } else {
        this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
        callback({
          status: true,
          object: {
            userId: user.id,
            userFirstName: `${user.userFirstName} ${user.userLastName}`,
            userLastName: user.userLastName,
            userPrimaryEmailId: email,
            userPrimaryPhone: phone,
            name: user.name || ""
          },
        })
      }

    }


  }

  onAuditSave = async (key) => {
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "General Admin"
        log.superVisorSubSection = `${key === "gaFirmRegInformation" ? "Firm Register Information" : "Qualified Associated Persons"}`
      })
      await updateAuditLog("generalAdmin", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  renderKeyDetails() {
    const { dropDown, transaction } = this.state
    const {svControls} = this.props
    console.log("===========canEdit==========>", svControls.canSupervisorEdit)
    const tables = this.state.tables.map(item => ({
      ...item,
      handleError:  item.key === "gaFirmRegInformation" ? (payload) => FirmRegisterValidate(payload) : (payload, minDate) => PeopleQualifiedValidate(payload, minDate)
    }))

    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      <div>
        <p><small>Rule G-44 based recommended sections in compliance policy and supervisory procedure document</small>
          <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
        </p>
        <EditableTable {...this.props} canSupervisorEdit={svControls.canSupervisorEdit} transaction={transaction} tables={tables} dropDown={dropDown} onSave={this.onSave} onRemove={this.onRemove} onParentChange={this.onDropDownChange}/>
        <hr />
        <Disclaimer />
      </div>
    )
  }

  render() {
    const { accordions, isLoading } = this.state
    const loading = () => <Loader/>

    if (isLoading) {
      return loading()
    }
    return (
      accordions && <div>
        {this.renderKeyDetails(1)}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(People)
export default connect(mapStateToProps, null)(WrappedComponent)
