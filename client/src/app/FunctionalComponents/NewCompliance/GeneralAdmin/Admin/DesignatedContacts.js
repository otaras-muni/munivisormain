import React, { Component } from "react"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify"
import { connect} from "react-redux"
import { updateAuditLog, getPicklistByPicklistName } from "GlobalUtils/helpers"
import EditableTable from "../../../../GlobalComponents/EditableTable"
import {PeopleDesignateValidate} from "../../Validation/GeneralAdminValidation"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import Loader from "../../../../GlobalComponents/Loader"
import CONST from "../../../../../globalutilities/consts"
import {fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import {
  fetchDesignateDetails, putGenAdmin, pullGenAdmin
} from "../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"

class DesignatedContacts extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tables: [
        {
          name:"Designate Firm Contacts",
          key: "gaDesignateFirmContacts",
          row: cloneDeep(CONST.SuperVisor.gaDesignateFirmContacts) || {},
          header: [
            {name: "Contact Type<span class='has-text-danger'>*</span>"},
            {name: "Name<span class='has-text-danger'>*</span>"},
            {name: "Email<span class='has-text-danger'>*</span>"},
            {name: "Phone<span class='has-text-danger'>*</span>"},
            {name: "Address<span class='has-text-danger'>*</span>"},
            {name: "Action"}
          ],
          list: [],
          tbody: [
            {name: "userContactType", placeholder: "Contact Type", type: "select"},
            {name: "name", type: "dropdown", labelName: "userFirstName", dropdownKey: "usersList", style: {width: "150px"}},
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "userAddress", placeholder: "Address", type: "text"},
            {name: "action"},
          ],
          handleError: (payload) => PeopleDesignateValidate(payload),
          children: this.renderChildren()
        },
      ],
      dropDown:{},
      staticValue: "",
      usersList: [],
      loading: true,
    }
  }

  async componentWillMount() {
    const {tables} = this.state
    const {svControls} = this.props
    const result = await getPicklistByPicklistName(["LKUPCMPLCONTACTTYPE"])
    fetchDesignateDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          userContactType: result && result[1] && result[1].LKUPCMPLCONTACTTYPE || [],
        },
        loading: false,
        tables,
        transaction: res,
      },() => {
        this.getUsers()
      })
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        usersList: cloneDeep(users.usersList) || [],
        loading: false,
      })
    })
  }

  renderChildren = () => (
    <div className="columns">
      <div className="column">
        <p className="multiExpTblVal">
          Municipal advisor must designate firm contacts via Form A-12 and is required to amend Form A-12 to update
          the designated
          contacts as changes occur. Use the
          <a href="/cac" target="new">Compliance Action Center</a> to
          set up reminders/alerts/notifications.
        </p>
      </div>
    </div>
  )

  onRemove = async (type, removeId, callback) => {
    console.log(type, removeId)
    const result = await getPicklistByPicklistName(["LKUPCMPLCONTACTTYPE"])
    pullGenAdmin(type, removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed Designate Firm Contacts successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          transaction: {
            gaDesignateFirmContacts: res && res.data && res.data.gaDesignateFirmContacts || [],
          },
          dropDown: {
            ...this.state.dropDown,
            userContactType: result && result[1] && result[1].LKUPCMPLCONTACTTYPE || [],
          },
        }, () => this.getUsers())
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    console.log(type, item)
    putGenAdmin(type, item, (res)=> {
      if (res && res.status === 200) {
        toast("Added Designate Firm Contacts successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          transaction: {
            gaDesignateFirmContacts: res && res.data && res.data.gaDesignateFirmContacts || [],
          }
        }, () => this.getUsers())
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    let email = ""
    let phone = ""
    let userAdd = ""
    if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }

    if(user && Array.isArray(user.userAddresses) && user.userAddresses.length) {
      userAdd = user.userAddresses.find(e => e.primaryAddress)
      let addresses = user.userAddresses[0]
      userAdd = userAdd ? `${userAdd.userAddresses.addressLine1}` :
        `${addresses.addressLine1}${addresses.city && `, ${addresses.city}`}${addresses.state && `, ${addresses.state}`}${addresses.country && `, ${addresses.country}.`}`
    }

    callback({
      status: true,
      object: {
        userId: user.id,
        userFirstName: `${user.userFirstName} ${user.userLastName}`,
        userLastName: user.userLastName,
        userPrimaryEmailId: email,
        userPrimaryPhone: phone,
        userAddress: userAdd,
        name: user.name || ""
      },
    })
  }

  onAuditSave = async (key) => {
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "General Admin"
        log.superVisorSubSection = "Designate Firm Contacts"
      })
      await updateAuditLog("generalAdmin", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  onContactChange = (value) => {
    this.setState({staticValue: value})
  }

  render() {
    const { dropDown, transaction, tables, staticValue, usersList } = this.state
    const {svControls} = this.props
    if(staticValue && (transaction && transaction.gaDesignateFirmContacts && transaction.gaDesignateFirmContacts.length)){
      dropDown.usersList = cloneDeep(usersList)
      const firmList = transaction.gaDesignateFirmContacts.filter(f => f.userContactType === staticValue) || []
      if(firmList && firmList.length && staticValue){
        const firmIds = firmList.map(m => m.userId)
        if(firmIds && firmIds.length){
          firmIds.map(f => {
            const index = dropDown.usersList.findIndex(u => u.id === f)
            if(index > -1){
              dropDown.usersList.splice(index, 1)
            }
          })
        }
      }
    }
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      <div>
        <p><small>Rule G-44 based recommended sections in compliance policy and supervisory procedure document</small>
          <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
        </p>
        <EditableTable
          {...this.props}
          canSupervisorEdit={svControls.canSupervisorEdit}
          transaction={transaction}
          tables={tables}
          dropDown={dropDown}
          onSave={this.onSave}
          onRemove={this.onRemove}
          onParentChange={this.onDropDownChange}
          onContactChange={this.onContactChange}/>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(DesignatedContacts)
export default connect(mapStateToProps, null)(WrappedComponent)
