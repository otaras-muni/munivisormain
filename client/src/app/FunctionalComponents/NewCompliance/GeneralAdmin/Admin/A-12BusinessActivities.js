import React, { Component } from "react"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify"
import { connect} from "react-redux"
import { getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import EditableTable from "../../../../GlobalComponents/EditableTable"
import {BusinessActivitiesValidate} from "../../Validation/GeneralAdminValidation"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import Loader from "../../../../GlobalComponents/Loader"
import CONST from "../../../../../globalutilities/consts"
import {
  fetchDesignateDetails, putGenAdmin, pullGenAdmin, getGenAdminById
} from "../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"

class BusinessActivities extends Component {
  constructor(props) {
    super(props)

    this.state = { accordions:[1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false})),
      businessActivities: [CONST.SuperVisor.businessActivities],
      isEditable: {},
      tables: [
        {
          name:"Business Activities (add all that apply)",
          key: "businessActivities",
          header: [
            {name: "Activity<span class='has-text-danger'>*</span>", style: {width: "25%"}},
            {name: "Sub-Activity<span class='has-text-danger'>*</span>", style: {width: "45%"}},
            {name: "Notes"},
            {name: "Action"}
          ],
          row:  cloneDeep(CONST.SuperVisor.businessActivities) || {},
          list: [],
          tbody: [
            { name: "activity", type: "dropdown", dropdownKey: "activity", disabledDropdownKey: "disabledActivity", labelName: "activity"},
            {name: "subActivity", type: "dependedSelect", childKey: "activity", selectionKey: "subActivity" },
            { name: "notes", placeholder: "Notes", type: "text" },
            {name: "action"},
          ],
        },
      ],
      dropDown:{},
      loading: true,
    }
  }

  async componentWillMount() {
    const {tables} = this.state
    const {svControls, user} = this.props
    fetchDesignateDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        loading: false,
        tables,
        auditLogs: (res && res.auditLogs) || [],
        transaction: res,
      })
    })
  }

  async componentDidMount(){
    const accordionInitiate = [1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false}))
    console.log("ACCINITIAL STATE", accordionInitiate)
    const result = await getPicklistByPicklistName(["LKUPCMPLACTIVITY"])
    console.log("===============>", this.state.transaction)
    const disabledActivity = result && result[1] && result[1].LKUPCMPLACTIVITY.filter(e => !e.included).map(a => a.label)
    this.setState({
      accordions:accordionInitiate,
      dropDown: {
        ...this.state.dropDown,
        activity: result && result[1] && result[1].LKUPCMPLACTIVITY && result[1].LKUPCMPLACTIVITY.map(e => e.label)  || [],
        subActivity: result && result[2] && result[2].LKUPCMPLACTIVITY || [],
        disabledActivity
      },
      isLoading:false
    })
  }

  renderChildren = () => (
    <div className="columns">
      <div className="column">
        <p className="multiExpTblVal">
          Municipal advisor must designate firm contacts via Form A-12 and is required to amend Form A-12 to update
          the designated
          contacts as changes occur. Use the
          <a href="cacMonitor.html" target="new">Compliance Action Center</a> to
          set up reminders/alerts/notifications.
        </p>
      </div>
    </div>
  )

  onRemove = async (type, removeId, callback) => {
    console.log(type, removeId)
    const docs = await getGenAdminById(type, removeId)
    const result = await getPicklistByPicklistName(["LKUPCMPLACTIVITY"])
    pullGenAdmin(type, removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed Business Activities successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          transaction: {
            businessActivities: res && res.data && res.data.businessActivities || [],
            finAdvisorEntityId: res && res.data && res.data.finAdvisorEntityId || [],
            _id: res && res.data && res.data._id || [],
          },
          dropDown: {
            ...this.state.dropDown,
            activity: result && result[1] && result[1].LKUPCMPLACTIVITY  || [],
            subActivity: result && result[2] && result[2].LKUPCMPLACTIVITY || [],
          },
          selectActivity: docs && docs.data && docs.data.generalAdmin && docs.data.generalAdmin.length && docs.data.generalAdmin[0].businessActivities[0].activity || ""
        })
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    console.log(type, item)
    putGenAdmin(type, item, (res)=> {
      if (res && res.status === 200) {
        toast("Added Business Activities successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          transaction: {
            businessActivities: res && res.data && res.data.businessActivities || [],
            finAdvisorEntityId: res && res.data && res.data.finAdvisorEntityId || [],
            _id: res && res.data && res.data._id || [],
          },
          selectActivity: ""
        })
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDropDownChange = async (inputName, key, select, index, callback) => {
    const user = select
    let email = ""
    let phone = ""
    let userAdd = ""
    console.log("=================================>", user)
    if (key === "gaDesignateFirmContacts") {

      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }

      if(user && Array.isArray(user.userAddresses) && user.userAddresses.length) {
        userAdd = user.userAddresses.find(e => e.primaryAddress)
        userAdd = userAdd ? userAdd.userAddresses.addressLine1 :user.userAddresses[0].addressLine1
      }

      // this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
      callback({
        status: true,
        object: {
          userId: user.id,
          userFirstName: `${user.userFirstName} ${user.userLastName}`,
          userLastName: user.userLastName,
          userPrimaryEmailId: email,
          userPrimaryPhone: phone,
          userAddress: userAdd
        },
      })
    } else if(key === "businessActivities") {
      const result = await getPicklistByPicklistName(["LKUPACTIVITY"])
      console.log("==========>", result)
      this.setState({
        selectActivity: user || ""
      })
      this.props.addAuditLog({userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `In Business Activities (add all that apply) Activity change to ${user || ""} from Business Activities (add all that apply)`, date: new Date(), key })
      callback({
        status: true,
        object: {
          activity: user,
          subActivity: ""
        },
      })
    } else {
      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }
      this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
      callback({
        status: true,
        object: {
          userId: user.id,
          userFirstName: `${user.userFirstName} ${user.userLastName}`,
          userLastName: user.userLastName,
          userPrimaryEmailId: email,
          userPrimaryPhone: phone,
        },
      })
    }
  }

  onAuditSave = async (key) => {
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "General Admin"
        log.superVisorSubSection = `${key === "gaDesignateFirmContacts" ? "Designate Firm Contacts" : "Qualified Associated Persons"}`
      })
      await updateAuditLog("generalAdmin", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  render() {
    const { accordions, dropDown, transaction, selectActivity } = this.state
    const {svControls} = this.props
    const tables = this.state.tables.map(item => ({
      ...item,
      handleError:  item.key === "businessActivities" ? (payload) => BusinessActivitiesValidate(payload) : null
    }))
    let select = []
    if(selectActivity){
      select = transaction && transaction.businessActivities && transaction.businessActivities.length && transaction.businessActivities.filter(b => b.activity === selectActivity) || []
    }
    const subActList = select && select.map(t => t.subActivity) || []
    if(selectActivity && transaction && transaction.businessActivities && transaction.businessActivities.length && select.length){
      const filterValue = dropDown && dropDown.subActivity && dropDown.subActivity[selectActivity].filter(s => !subActList.includes(s)) || []
      if(filterValue){
        dropDown.subActivity[selectActivity] = filterValue
      }
    }
    const loading = () => <Loader/>
    if(this.state.loading) {
      return loading()
    }
    return (
      accordions && <div>
        <div>
          <div>
            <p><small>Rule G-44 based recommended sections in compliance policy and supervisory procedure document</small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
            </p>
            <EditableTable {...this.props} canSupervisorEdit={svControls.canSupervisorEdit} transaction={transaction} tables={tables} dropDown={dropDown} onSave={this.onSave} onRemove={this.onRemove} onParentChange={this.onDropDownChange}/>
          </div>
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(BusinessActivities)
export default connect(mapStateToProps, null)(WrappedComponent)
