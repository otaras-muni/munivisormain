import React, { Component } from "react"
import cloneDeep from "clone-deep"
import swal from "sweetalert"
import { toast } from "react-toastify"
import { connect} from "react-redux"
import { saveEntityDetails, getEntityDetails, getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import Accordion from "../../../../GlobalComponents/Accordion"
import { GeneralFirmInfoValidate } from "../../Validation/GeneralAdminValidation"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import FirmAddressListForm from "../../../EntityManagement/CommonComponents/FirmAddressListForm"
import Loader from "../../../../GlobalComponents/Loader"
import CONST from "../../../../../globalutilities/consts"
import {fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import { fetchDesignateDetails, putGenAdmin, pullGenAdmin } from "../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {DropDownSelect} from "../../../../GlobalComponents/TextViewBox"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import EditableReactTable from "./EditableReactTable"


class People extends Component {
  constructor(props) {
    super(props)

    this.state = { accordions:[1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false})),
      gaGeneralFirmInfo: [CONST.SuperVisor.gaGeneralFirmInfo],
      isEditable: {},
      countryResult: [],
      selectAddress: [],
      addressList: [],
      viewOnly: true,
      SECRegistered: false,
      businessAddress: this.initialAddresses()[0],
      addresses: this.getInitialAddress(),
      waiting: true,
      error: {
        addresses: this.getInitialAddress()
      },
      tables: [
        {
          name:"General Firm Information",
          key: "gaGeneralFirmInfo",
          header: [
            {name: "Field<span class='has-text-danger'>*</span>"},
            {name: "Notes<span class='has-text-danger'>*</span>"},
            {name: "Information"},
            {name: "Action"}
          ],
          row:  cloneDeep(CONST.SuperVisor.gaGeneralFirmInfo) || {},
          list: [],
          tbody: [
            { name: "field", type: "text" },
            { name: "notes", type: "text" },
            { name: "information", type: "text" },
            {name: "action"},
          ],
          handleError: (payload) => GeneralFirmInfoValidate(payload),
        },
      ],
      staticField: [{field: "Dealer SEC ID", notes: "Number assigned by SEC", information: "", isField: true},
        {field: "Municipal Advisor SEC ID", notes: "Number assigned by SEC", information: "", isField: true},
        {field: "Central Registration Repository (CRD)Number", notes: "Number assigned by FINRA to the organization, not the CRD number assigned to any registered individual", information: "", isField: true},
        {field: "Legal Entity Identifier", notes: "20-digit, alpha-numeric code assigned by a Local Operating Unit (LOU), or pre-LOU, of the Global Legal Identifier System", information: "", isField: true}],
      notes: CONST.CAC.genAdmin,
      dropDown:{},
      errorMessages:{},
      usersList: [],
      isSaveDisabled: {},
      confirmAlert: CONST.confirmAlert,
      loading: true,
    }
  }

  async componentWillMount() {
    const {tables} = this.state
    const {svControls, user} = this.props
    getEntityDetails(user.entityId, this.getEntityDetailsCallback)
    fetchDesignateDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          userContactType: ["Designated contact A-12", "Designated contact EMMA", "Designated contact SEC", "Designated contact FINRA",
            "Other (please specify)"],
        },
        loading: false,
        tables,
        transaction: res,
        gaGeneralFirmInfo: res && res.gaGeneralFirmInfo && res.gaGeneralFirmInfo.length ? res.gaGeneralFirmInfo : this.state.staticField
      },() => {
        this.getUsers()
      })
    })
  }

  getEntityDetailsCallback = (err, res) => {
    let { selectAddress } = this.state
    const addressName = res && res[0] && res[0].addresses && res[0].addresses.length && res[0].addresses.map(a => a.addressName)
    selectAddress = res && res[0] && res[0].addresses && res[0].addresses.length && res[0].addresses
    const selectedAddress = res && res[0] && res[0].addresses && res[0].addresses.length && res[0].addresses.filter(a => a.addressName === "SEC Registered")
    const addressList = res && res[0] && res[0].addresses && res[0].addresses.length && res[0].addresses.filter(a => a.addressName !== "SEC Registered")
    selectedAddress && selectedAddress.length ? selectedAddress[0].organizationType = res && res[0] && res[0].organizationType || "" : null

    this.setState({
      addresses: selectedAddress && selectedAddress.length ? selectedAddress : [], // res && res[0] && res[0].SECRegistered || [],
      chooseAddress: selectedAddress && selectedAddress.length ? "SEC Address" : "",
      addressList,
      selectAddress,
      SECRegistered: !!(selectedAddress && selectedAddress.length),
      dropDown: {
        ...this.state.dropDown,
        addressType: addressName || []
      }
    })
  }

  async componentDidMount(){
    const accordionInitiate = [1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false}))
    console.log("ACCINITIAL STATE", accordionInitiate)
    const result = await getPicklistByPicklistName(["LKUPCOUNTRY", "LKUPCMPLQUALIFICATIONS"])
    const countryResult = [result[0], result[1].LKUPCOUNTRY, result[2].LKUPCOUNTRY, result[3].LKUPCOUNTRY] || []
    const organizationTypeList = result && result[1] && result[1].LKUPCMPLQUALIFICATIONS || []
    this.setState({
      accordions:accordionInitiate,
      countryResult,
      organizationTypeList,
      isLoading:false
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        loading: false,
      })
    })
  }

  onRemove = (type, removeId) => {
    console.log(type, removeId)
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: "Remove item from General Firm Information", date: new Date(), key: type})
    pullGenAdmin(type, removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed General Firm Information successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          gaGeneralFirmInfo: res && res.data && res.data.gaGeneralFirmInfo || [],
          errorMessages: {},
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          errorMessages: {},
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      }
    })
  }

  onSave = (type, item) => {
    console.log(type, item)
    putGenAdmin(type, this.state.gaGeneralFirmInfo, (res)=> {
      if (res && res.status === 200) {
        toast("Added General Firm Information successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        this.setState({
          [type]: res && res.data && res.data.gaGeneralFirmInfo || [],
          errorMessages: {},
          isEditable: {
            ...this.state.isEditable,
            [type]: "",
          },
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          errorMessages: {},
          isEditable: {
            ...this.state.isEditable,
            [type]: "",
          },
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [type]: false
          }
        })
      }
    })
  }

  onAuditSave = async (key) => {
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "General Admin"
        log.superVisorSubSection = `${key === "gaDesignateFirmContacts" ? "Designate Firm Contacts" : key === "secAddress" ? "Address filed with Securities and Exchange Commission (SEC)" : "Qualified Associated Persons"}`
      })
      await updateAuditLog("generalAdmin", auditLogs)
      this.props.updateAuditLog([])
    }
  }

  initialAddresses() {
    return [
      {
        addressName: "",
        isPrimary: false,
        isHeadQuarter: false,
        website: "",
        officePhone: [
          {
            countryCode: "",
            phoneNumber: "",
            extension: ""
          }
        ],
        officeFax: [
          {
            faxNumber: ""
          }
        ],
        officeEmails: [{ emailId: "" }],
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: { zip1: "", zip2: "" }
      }
    ]
  }

  getAddressDetails = (address = "") => {
    if (address !== "") {
      const { businessAddress, errorFirmDetail } = this.state
      let id = ""
      if (businessAddress._id !== "") {
        id = businessAddress._id
      }
      businessAddress._id = id
      if (address.addressLine1 !== "") {
        businessAddress.addressName =
          businessAddress.addressName === ""
            ? `${address.addressLine1}, ${address.city}`
            : businessAddress.addressName
        businessAddress.addressLine1 = address.addressLine1.trim()
      }
      if (address.country !== "") {
        businessAddress.country = address.country.trim()
      }
      if (address.state !== "") {
        businessAddress.state = address.state.trim()
      }
      if (address.city !== "") {
        businessAddress.city = address.city.trim()
      }
      if (address.zipcode !== "") {
        businessAddress.zipCode.zip1 = address.zipcode.trim().trim()
      }
      businessAddress.formatted_address = address.formatted_address
      businessAddress.url = address.url.trim()
      businessAddress.location = { ...address.location }

      this.setState(prevState => ({
        ...prevState,
        businessAddress,
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        }
      }))
    }
  }

  getInitialAddress = () => ([
    {
      addressName: "",
      isPrimary: false,
      isHeadQuarter: false,
      website: "",
      officePhone: [
        {
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }
      ],
      officeFax: [
        {
          faxNumber: ""
        }
      ],
      officeEmails: [
        {
          emailId: ""
        }
      ],
      addressLine1: "",
      addressLine2: "",
      country: "",
      state: "",
      city: "",
      zipCode: {
        zip1: "",
        zip2: ""
      },
      formatted_address: "",
      url: "",
      location: {
        longitude: "",
        latitude: ""
      }
    }
  ])

  onSaveSECAddress = (address) => {
    const { addressList, addresses } = this.state
    const {entityId, userFirstName, userLastName} = this.props.user
    this.props.addAuditLog({
      userName: `${userFirstName} ${userLastName}`,
      log: addresses && addresses.length ? "In General Admin SEC Address edited" : "In General Admin Add New SEC Address",
      date: new Date(),
      key: "secAddress",
    })
    const updateData = {
      organizationType: address && address[0] && address[0].organizationType || "",
    }
    address[0].addressName = "SEC Registered"
    delete address[0].organizationType
    const add = addressList && addressList.length ? address.concat(addressList) : address
    updateData.addresses = add || []
    this.setState({loading: true})
    saveEntityDetails(entityId, updateData, "", this.saveCallback )
  }

  saveCallback = (err, res) => {
    const { user } = this.props
    if (!err) {
      getEntityDetails(user.entityId, this.getEntityDetailsCallback)
      this.onAuditSave("secAddress")
      toast("Changes Saved", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState({loading: false})
    } else {
      this.setState({ waiting: false })
      toast("Error in saving changes", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  onSelectAddress = (e, i) => {
    if(e === "addressType"){
      this.setState({
        [e]: i && i.value
      }, () => {
        if(this.state.addressType && this.state.chooseAddress === "existing address"){
          let { selectAddress, addresses } = this.state
          addresses = selectAddress.filter(s => s.addressName === this.state.addressType)
          this.setState({
            addresses: addresses || [],
          })
        }
      })
    } else {
      this.setState({
        [e.target.name]: e.target.value
      }, () => {
        if(this.state.chooseAddress === "SEC Address"){
          this.setState({
            addresses: [],
          })
        }
      })
    }
  }

  actionButtons = (key, isDisabled) => {
    const list = this.state[key]
    const isAddNew = (list && list.find(part => part.isNew)) || false
    const { svControls } = this.props
    if (svControls && !svControls.canSupervisorEdit) return
     return( //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={isDisabled || !!isAddNew}>Add
          </button>
        </div>
      </div>
    )
  }

  onAdd = (key) => {
    const { isEditable, tables } = this.state
    const insertRow = cloneDeep(this.state[key])
    const tableObject = tables.find(tbl => tbl.key === key)

    if (isEditable[key] || isEditable[key] === 0) { swal("Warning", "First save the current item", "warning"); return }

    if (tableObject.hasOwnProperty("row")) {
      insertRow.unshift({
        ...cloneDeep(tableObject.row),
        isNew: true
      })
    }

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: insertRow.length ? 0 : 0
      }
    })

    console.log(`add ${key} =====>`, insertRow)
  }

  onRemoveDoc = (removeId, type, index, ) => {
    const { isEditable, confirmAlert } = this.state
    confirmAlert.text = "You want to delete General Firm Information?"
    swal(confirmAlert).then((willDelete) => {
      if (willDelete) {
        if (type) {
          this.setState({
            isSaveDisabled: {
              ...this.state.isSaveDisabled,
              [type]: true
            }
          }, () => {
            if(removeId){
              this.onRemove(type, removeId)
            } else {
              const list = this.state[type]
              if (list.length) {
                list.splice(index, 1)
                this.setState({
                  [type]: list,
                  isEditable: {
                    ...isEditable,
                    [type]: ""
                  },
                  isSaveDisabled: {
                    ...this.state.isSaveDisabled,
                    [type]: false
                  },
                  errorMessages: {}
                })
              }
            }
          })
        }
      }
    })
  }

  onCancel = (key) => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: "",
      },
    })
  }

  onEdit = (key, index, tableTitle) => {
    const { user } = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `In ${tableTitle} one row edited`, date: new Date(), key })
    if (this.state.isEditable[key]) { swal("Warning", "First save the current item", "warning"); return }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index,
      },
    })
  }

  onSaveDoc = (key, item, index) => {
    const { tables } = this.state
    const table = tables.find(tbl => tbl.key === key)
    const itemData = {
      ...item
    }
    const minDate = this.props.minDate || itemData.createdDate || ""
    delete itemData.isNew
    const errors = (table && table.handleError) ? table.handleError(itemData, minDate) : null

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        const message = "Required"
        if (errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.context.key] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.context.key]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.context.key]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    this.setState({
      isSaveDisabled: {
        ...this.state.isSaveDisabled,
        [key]: true
      }
    }, () => {
      delete item.isNew
      this.onSave(key, item)
    })
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    })
  }

  onBlur = (change, category) => {
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: `In General Firm Information ${change}`, date: new Date(), key: category})
  }

  renderKeyDetails() {
    const { waiting, error, countryResult, addresses, viewOnly, businessAddress, organizationTypeList, isSaveDisabled, errorMessages, isEditable, gaGeneralFirmInfo} = this.state
    const {svControls} = this.props

    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div>
          <Accordion multiple
            activeItem={[0, 1]}
            boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                <p><small>Rule G-44 based recommended sections in compliance policy and supervisory procedure document</small>
                  <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
                </p>

                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  actionButtons={this.actionButtons("gaGeneralFirmInfo", isSaveDisabled.gaGeneralFirmInfo)}
                  title="General Firm Information"
                >
                  {activeAccordions.includes(0) && (
                    <div>
                      <EditableReactTable
                        gaGeneralFirmInfo={gaGeneralFirmInfo}
                        isEditable={isEditable}
                        category="gaGeneralFirmInfo"
                        onSaveDoc={this.onSaveDoc}
                        onEdit={this.onEdit}
                        onCancel={this.onCancel}
                        onBlur={this.onBlur}
                        onRemoveDoc={this.onRemoveDoc}
                        onItemChange={this.onItemChange}
                        svControls={svControls.canSupervisorEdit}
                        isSaveDisabled={isSaveDisabled.gaGeneralFirmInfo || false}
                        errors={errorMessages}
                        label="General Firm Information"
                      />
                    </div>
                  )}
                </RatingSection>

                { !this.state.SECRegistered ?
                  <RatingSection
                    onAccordion={() => onAccordion(1)}
                    title="Select SEC Address"
                  >
                    {activeAccordions.includes(1) && (
                      <div>
                        <div className="columns">
                          <div className="column">
                            <label className="radio-button">
                              Do you want to choose from existing address(es)
                              <input
                                type="radio"
                                name="chooseAddress"
                                value="existing address"
                                checked={this.state.chooseAddress === "existing address"}
                                onChange={(e) => this.onSelectAddress(e)}
                                disabled={!svControls.canSupervisorEdit}
                              />
                              <span className="checkmark"/>
                            </label>
                          </div>
                          <div className="column">
                            <label className="radio-button">
                              Do you want to add a new SEC Registered address.
                              <input
                                type="radio"
                                name="chooseAddress"
                                value="SEC Address"
                                checked={this.state.chooseAddress === "SEC Address"}
                                onChange={(e) => this.onSelectAddress(e)}
                                disabled={!svControls.canSupervisorEdit}
                              />
                              <span className="checkmark"/>
                            </label>
                          </div>
                        </div>
                        {this.state.chooseAddress === "existing address" ?
                          <div className="columns">
                            <div className="column is-3">
                              <DropDownSelect
                                label="Select Addresses"
                                name="addressType"
                                data={this.state.dropDown.addressType || []}
                                value={this.state.addressType || ""}
                                onChange={event => this.onSelectAddress("addressType", event)}
                                disabled={!svControls.canSupervisorEdit}
                              />
                            </div>
                          </div> : null
                        }
                      </div>
                    )}
                  </RatingSection>: null
                }

                <div style={ this.state.chooseAddress === "SEC Address" ? null : addresses && addresses.length ? null : { display: "none" }}>
                  <FirmAddressListForm
                    countryResult={countryResult}
                    errorFirmDetail={error.addresses[0]}
                    onChangeAddressType={this.onChangeAddressType}
                    canEdit={svControls && !svControls.canSupervisorEdit ? svControls.canSupervisorEdit : viewOnly}
                    canView
                    busy={!waiting}
                    canSupervisorEdit={svControls && svControls.canSupervisorEdit}
                    showAddress
                    isCompliance
                    title="Address filed with Securities and Exchange Commission (SEC)"
                    addressList={addresses}
                    businessAddress={businessAddress}
                    organizationTypeList={organizationTypeList}
                    getAddressDetails={this.getAddressDetails}
                    onSaveAddress={this.onSaveSECAddress}
                  />
                </div>
              </div>
            }
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }

  render() {
    const { accordions, isLoading } = this.state
    const loading = () => <Loader/>

    if (isLoading) {
      return loading()
    }
    return (
      accordions && <div>
        {this.renderKeyDetails(1)}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(People)
export default connect(mapStateToProps, null)(WrappedComponent)
