import React, { Component } from "react"
import {Link} from "react-router-dom"
import Loader from "Global/Loader"
import Summary from "../components/Summary"
import Detail from "../components/Detail"
import FastTrackDetail from "../components/FastTrackDetail"

const tabs = [{
  title: <span>Summary</span>,
  value: 1,
  path: "summary",
}]

class UserView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
    }
  }

  componentWillMount() {
    console.log(this.props)
    const { nav4 } = this.props
    if(nav4){
      if(nav4 === "detail"){
        tabs.splice(1,1)
        tabs.push({
          title: <span>Detail</span>,
          value: 2,
          path: "detail",
        })
      } else if(nav4 === "fastTrackDetail"){
        tabs.splice(1,1)
        tabs.push({
          title: <span>Detail</span>,
          value: 2,
          path: "fastTrackDetail",
        })
      }
    } else {
      tabs.splice(1,1)
      this.props.history.push("/compliance/cmp-sup-political/user-view/summary")
    }
    this.setState({
      loading: false,
    })
  }

  renderTabs = (activeTab) => {
    return (
      <div className="tabs">
        <ul>
          {
            tabs.map(tab=> {
              const path = tab.path === "detail" || ""
              return (
                <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`}>
                  {
                    this.props.nav2 ? <Link to={`/compliance/cmp-sup-political/user-view/${tab.path}${path ? "?status=Record" : ""}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                      <a role="button" className="tabSecLevel">
                        {tab.title}
                      </a>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }

  renderSelectedView = () => {
    switch(this.props.nav4) {
    case "summary" :
      return <Summary {...this.props}/>
    case "detail" :
      return <Detail {...this.props}/>
    case "fastTrackDetail" :
      return <FastTrackDetail {...this.props}/>
    default:
      return "not found"
    }
  }

  render() {
    const activeTab = tabs.find(x => x && (x.path === this.props.nav4))
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        {this.props.nav2 && this.props.nav3 !== "managequemedia" && this.renderTabs(activeTab && activeTab.value) }
        <div >
          {this.renderSelectedView()}
        </div>
      </div>
    )
  }
}

export default UserView
