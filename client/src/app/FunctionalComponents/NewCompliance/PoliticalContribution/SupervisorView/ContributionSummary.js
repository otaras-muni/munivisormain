import React, { Component } from "react"
import {Link} from "react-router-dom"
import swal from "sweetalert"
import Loader from "Global/Loader"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import { getEntityList } from "AppState/actions/AdminManagement/admTrnActions"
import { updateAuditLog } from "GlobalUtils/helpers"
import connect from "react-redux/es/connect/connect"
import {toast} from "react-toastify"
import moment from "moment"
import cloneDeep from "lodash.clonedeep"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import { SelectLabelInput } from "../components/PoliticalTextViewBox"
import {
  getPoliContributionDetailsById,
  putPolContribSummary,
  getPolContribSummary,
  putContributeDetailsSumaryDocuments,
  putPolContribSummaryStatus,
  pullContributeDetailsSumaryDocuments
} from "../../../../StateManagement/actions/Supervisor"
import DocumentPage from "../../../../GlobalComponents/DocumentPage"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {getAuditLogByType} from "../../../../StateManagement/actions/audit_log_actions"
import Audit from "../../../../GlobalComponents/Audit"
import PoliticalSummaryReactTable from "../components/PoliticalSummaryReactTable"


class ContributionSummary extends Component {

  constructor(props) {
    super(props)
    this.state = {
      contribMuniEntityByState: [],
      paymentsToPartiesByState: [],
      ballotContribByState: [],
      municipalSecuritiesBusiness: [],
      municipalAdvisoryBusiness: [],
      ballotApprovedOfferings: [],
      entityList: [],
      documentsList: [],
      auditLogs: [],
      record: "",
      summaryId: "",
      label: "",
      emmaSubmissionStatus: "",
      length: 0,
      dropDown: {
        polContribQuarterOfDisclosureFirm: [{name: "First", value: 1}, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        securitiesBusiness: [{name: "Negotiated underwriting", id: "negotiated"}, {name: "Financial advisor", id: "financial"}, {name: "Remarketing agent", id: "remarketing"}, {name: "Private placement", id: "private"}],
        advisoryBusiness: [{name: "Advice", id: "advice"}, {name: "Solicitation", id: "solicitation"}],
        solicitedBusiness: [{name: "Municipal securities business", id: "muniSecuritiesBus"}, {name: "Municipal advisory business", id: "muniAdvisoryBus"}, {name: "Investment advisory business", id: "investmentAdvisoryBusiness"}],
        polContribYearOfDisclosureFirm: []
      },
      disclosureInfo: {},
      confirmAlert: CONST.confirmAlert,
      loading:true,
    }
  }

  async componentWillMount() {
    const polContribYearOfDisclosureFirm = []
    const min = 2018, max = min + 10
    for (let i = min; i <= max; i++) { polContribYearOfDisclosureFirm.push(i) }
    await this.getEntityList()
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        polContribYearOfDisclosureFirm
      },
      disclosureInfo: {
        year: parseInt(moment(new Date()).utc().format("YYYY"), 10),
        quarter: moment(new Date()).utc().quarter(),
      },
      loading: false,
    }, () => this.getSectionList())
  }

  onChange = (e, key) => {
    if(key === "status"){
      const { name, value } = e.target
      const { disclosureInfo, confirmAlert } = this.state
      const { user } = this.props
      confirmAlert.text = `You Want To EMMA Dataport Submission Status Change To ${value}?`
      swal(confirmAlert)
        .then((willDelete) => {
          if (key === "status") {
            if (willDelete) {
              this.setState({
                [name]: value
              })
              const userName = `${user.userFirstName} ${user.userLastName}` || ""
              this.props.addAuditLog({userName, log: `EMMA Dataport Submission Status Change To ${value || ""}`, date: new Date(), key: "status"})
              const query = `?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}&entityId=${user.entityId}`
              const payload = { [name]: value}
              this.setState({
                loading: true
              }, () => {
                putPolContribSummaryStatus(query, payload, res => {
                  if(res && res.status === 200){
                    toast("Status Changed Successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
                    this.setState({
                      emmaSubmissionStatus: res && res.data && res.data.emmaSubmissionStatus || "",
                      disclosureInfo: {
                        year: res && res.data && res.data.year || "",
                        quarter: res && res.data && res.data.quarter || ""
                      },
                      summaryId: res && res.data && res.data._id || "",
                      loading: false
                    }, () => this.onAuditSave("status"))
                  } else {
                    toast("Something went wrong!", {autoClose: CONST.ToastTimeout,type: toast.TYPE.ERROR})
                    this.setState({
                      loading: false
                    })
                  }
                })
              })
            }
          }
        })
    } else {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [e.target.name]: e.target.value
        },
        emmaSubmissionStatus: ""
      }, () => this.getSectionList())
    }
  }

  onAssignedSelect = (key, items, name, index) => {
    const value = this.state[key]
    value[index][name] = items
    this.setState({
      [key]: value || []
    })
  }

  onCheckSelect = (e, key, index, check) => {
    const value = this.state[key]
    if(e.target.type === "checkbox"){
      value[index][e.target.name] = e.target.checked ? check : ""
      this.setState({
        [key]: value || ""
      })
    } else{
      value[index][e.target.name] = e.target.value
      this.setState({
        [key]: value
      })
    }
  }

  onSave = (key) => {
    const { disclosureInfo } = this.state
    const { user } = this.props
    const log = this.state[key]
    let payload = []
    if(key === "municipalSecuritiesBusiness"){
      payload = log.map(l => ({selectedId: l.selectedId, securitiesBusiness: l.securitiesBusiness, objectId: l._id}))
    } else if(key === "municipalAdvisoryBusiness"){
      payload = log.map(l => ({selectedId: l.selectedId, thirdPartyName: l.thirdPartyName, advisoryBusiness: l.advisoryBusiness, solicitedBusiness: l.solicitedBusiness, objectId: l._id}))
    } else if(key === "ballotApprovedOfferings"){
      payload = log.map(l => ({selectedId: l.selectedId, reportableDate: l.reportableDate, objectId: l._id}))
    }
    const query = `?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}&entityId=${user.entityId}&type=${key}`
    this.setState({
      loading: true
    }, () => {
      putPolContribSummary(query, payload, res => {
        if(res && res.status === 200){
          this.onAuditSave(key)
          toast("Changes Saved", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
          res && res.data && res.data[key].forEach(f => {
            this.state[key].forEach(e => {
              if(f.objectId === e._id){
                e.selectedId = f.selectedId
                if(key === "municipalSecuritiesBusiness"){
                  e.securitiesBusiness = f.securitiesBusiness
                } else if(key === "municipalAdvisoryBusiness"){
                  e.advisoryBusiness = f.advisoryBusiness
                  e.solicitedBusiness = f.solicitedBusiness
                  e.thirdPartyName = f.thirdPartyName
                } else if(key === "ballotApprovedOfferings"){
                  e.reportableDate = f.reportableDate
                }
              }
            })
          })
          this.setState({
            [key]: this.state[key] || [],
            loading: false
          })
        } else {
          toast("Something went wrong!", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
        }
      })
    })
  }

  onDocSave = (docs, callback) => {
    const { summaryId, disclosureInfo } = this.state
    const { user } = this.props
    const query = summaryId ? `?summaryId=${summaryId}` : `?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}&entityId=${user.entityId}`
    putContributeDetailsSumaryDocuments(query, docs, (res) => {
      if (res && res.status === 200) {
        toast("Disclosure Related Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.setState({
          summaryId: res && res.data && res.data._id || summaryId || ""
        }, () => this.onAuditSave("documents"))
        callback({
          status: true,
          documentsList: (res.data && res.data.poliContributionSummaryDocuments) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const { summaryId } = this.state
    if(summaryId && documentId){
      pullContributeDetailsSumaryDocuments(summaryId, documentId, (res) => {
        if (res && res.status === 200) {
          toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("document")
          callback({
            status: true,
            documentsList: (res.data && res.data.poliContributionSummaryDocuments) || [],
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    }
  }

  onBlur = (change, category, key, e) => {
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    let log = ""
    if(e.target.title && e.target.value){
      log = `${change} in Municipal Entity Name ${category} in ${e.target.title} change to ${e.target.value}`
    } else {
      log = `${change} in Municipal Entity Name ${category} row edited`
    }
    this.props.addAuditLog({userName, log, date: new Date(), key})
  }

  onStatusChange = async (e, doc, callback) => {
    const { summaryId } = this.state
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc
      }
      type = "updateMeta"
    }
    this.setState({
      loading: true
    }, () => {
      putContributeDetailsSumaryDocuments(`?details=${type}&summaryId=${summaryId}`, document, (res) => {
        if (res && res.status === 200) {
          toast("Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("document")
          if(name){
            callback({
              status: true,
            })
          }else {
            callback({
              status: true,
              documentsList: (res.data && res.data.poliContributionSummaryDocuments) || [],
            })
          }
          this.setState({
            loading: false
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    })
  }

  onAuditSave = async (key) => {
    const { user } = this.props
    const { summaryId } = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)

    if (summaryId) {
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Political Contributions and Prohibitions"
          log.superVisorSubSection = "Disclosure"
        })
        await updateAuditLog(summaryId, auditLogs)
        this.props.updateAuditLog([])
        this.getAuditLogById(summaryId)
      }
    }
  }

  onSearchText = (e, key) => {
    const search = this.state[key]
    this.setState({
      [e.target.name]: e.target.value
    }, () => {
      if (search.length) {
        this.onSearch(key)
      }
    })
  }

  onSearch = (key) => {
    const { securitiesSearch, advisorySearch, ballotSearch } = this.state
    let { securitiesSearchList, advisorySearchList, ballotSearchList } = this.state

    const search = this.state[key]
    if (securitiesSearch || advisorySearch || ballotSearch) {
      if(securitiesSearch && key === "municipalSecuritiesBusiness"){
        securitiesSearchList = search.filter(obj => ["state", "city", "entityName", "reportableDate", "thirdPartyName"].some(key => {
          return obj[key] && obj[key].toLowerCase().includes((securitiesSearch).toLowerCase())
        }))
      } else if(advisorySearch && key === "municipalAdvisoryBusiness"){
        advisorySearchList = search.filter(obj => ["state", "city", "entityName", "reportableDate", "thirdPartyName"].some(key => {
          return obj[key] && obj[key].toLowerCase().includes((advisorySearch ).toLowerCase())
        }))
      } else if(ballotSearch && key === "ballotApprovedOfferings"){
        ballotSearchList = search.filter(obj => ["state", "city", "entityName", "reportableDate", "thirdPartyName"].some(key => {
          return obj[key] && obj[key].toLowerCase().includes((ballotSearch).toLowerCase())
        }))
      }
      this.setState({
        securitiesSearchList,
        advisorySearchList,
        ballotSearchList
      })
    }
  }

  getSectionList = () => {
    const { disclosureInfo, entityList, municipalSecuritiesBusiness, municipalAdvisoryBusiness, ballotApprovedOfferings } = this.state
    const { user } = this.props
    if(disclosureInfo && (disclosureInfo.year && disclosureInfo.quarter)){
      this.setState({
        loading: true
      }, async () => {
        const status = "&status=Complete"
        const query = `?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}${status}`
        const queries = `?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}&entityId=${user.entityId}`
        const res = await getPoliContributionDetailsById(query)
        const response = await getPolContribSummary(queries)
        let object = {}
        if(response && response.length){
          this.getAuditLogById(response && response[0] && response[0]._id)
          const section = ["municipalSecuritiesBusiness", "municipalAdvisoryBusiness", "ballotApprovedOfferings"]
          section.map(s => {
            const secObject = this.state[s]
            if(response && response[0] && response[0][s].length){
              response[0][s].forEach(f => {
                secObject.forEach(e => {
                  if(f.objectId === e._id){
                    e.selectedId = f.selectedId
                    if(s === "municipalSecuritiesBusiness"){
                      e.securitiesBusiness = f.securitiesBusiness
                    } else if(s === "municipalAdvisoryBusiness"){
                      e.advisoryBusiness = f.advisoryBusiness
                      e.solicitedBusiness = f.solicitedBusiness
                      e.thirdPartyName = f.thirdPartyName
                    } else if(s === "ballotApprovedOfferings"){
                      e.reportableDate = f.reportableDate
                    }
                  }
                })
              })
              object = {
                ...object,
                [s]: secObject
              }
            } else {
              const selected = entityList && entityList.length && entityList.map(e =>({state: e.entityPrimaryAddressState, entityName: e.entityName, city: e.entityPrimaryAddressCity, _id: e.userId}))
              object = {
                ...object,
                [s]:  cloneDeep(selected) || []
              }
            }
          })
        } else {
          const selected = entityList && entityList.length && entityList.map(e =>({state: e.entityPrimaryAddressState, entityName: e.entityName, city: e.entityPrimaryAddressCity, _id: e.userId}))
          object = {
            municipalSecuritiesBusiness: cloneDeep(selected) || municipalSecuritiesBusiness,
            municipalAdvisoryBusiness: cloneDeep(selected) || municipalAdvisoryBusiness,
            ballotApprovedOfferings: cloneDeep(selected) || ballotApprovedOfferings
          }
          this.setState({
            auditLogs: []
          })
        }
        const contribMuniEntityByState = []
        const paymentsToPartiesByState = []
        const ballotContribByState = []
        const section = ["contrib", "payments", "ballot"]
        if(res && res.length){
          res && res.map(r => {
            section.map(sec => {
              const depend = sec === "contrib" ? "contribMuniEntityByState" : sec === "payments" ? "paymentsToPartiesByState" : sec === "ballot" ? "ballotContribByState" : ""
              if(r && r[depend]){
                r[depend].forEach(c => {
                  if(!c.checkNone){
                    const state = sec === "contrib" ? contribMuniEntityByState : sec === "payments" ? paymentsToPartiesByState : sec === "ballot" ? ballotContribByState : []
                    state.push(c)
                  }
                })
              }
            })
          })
        }
        this.setState({
          contribMuniEntityByState,
          paymentsToPartiesByState,
          ballotContribByState,
          ...object,
          documentsList: response && response.length ? response[0] && response[0].poliContributionSummaryDocuments : [],
          summaryId: response && response.length ? response[0] && response[0]._id : "",
          record: res && res.length ? "" : "No Record Found!",
          length: res && res.length || 0,
          emmaSubmissionStatus: response && response.length ? response && response[0] && response[0].emmaSubmissionStatus : "",
          securitiesSearchList: [],
          advisorySearchList: [],
          ballotSearchList: [],
          securitiesSearch: "",
          advisorySearch: "",
          ballotSearch: "",
          loading: false
        })
      })
    }
  }

  getAuditLogById = (id) => {
    getAuditLogByType("politicalContribution", id, response => {
      this.setState({
        auditLogs: response && response.changeLog || []
      })
    })
  }

  getEntityList = async () => {
    const payload = {
      filters:{
        entityTypes:["Client", "Prospect"],
        userContactTypes:[],
        entityMarketRoleFlags:[],
        entityIssuerFlags:[],
        entityPrimaryAddressStates:[],
        freeTextSearchTerm:""
      },
      pagination:{
        serverPerformPagination:true,
        currentPage:0,
        size:100000,
        sortFields:{
          entityName:1
        }
      }
    }
    const result = await getEntityList(payload)
    const data = await result.data
    const entityList = data && data.pipeLineQueryResults && data.pipeLineQueryResults.length && data.pipeLineQueryResults[0] && data.pipeLineQueryResults[0].data || []
    const selected = entityList && entityList.length && entityList.map(e =>({state: e.entityPrimaryAddressState, entityName: e.entityName, city: e.entityPrimaryAddressCity, _id: e.userId}))
    this.setState({
      entityList,
      municipalSecuritiesBusiness: cloneDeep(selected) || [],
      municipalAdvisoryBusiness: cloneDeep(selected) || [],
      ballotApprovedOfferings: cloneDeep(selected) || [],
    })
  }

  xlDownload = (key) => {
    const { disclosureInfo } = this.state
    const section = this.state[key]
    const label = key === "municipalSecuritiesBusiness" ? "Municipal Securities Business" : key === "municipalAdvisoryBusiness" ? "Municipal Advisory Business" :
      key === "ballotApprovedOfferings" ? "Ballot-Approved Offerings" : key
    const contributionSummaryList = []
    let headers = []
    section && section.map((item) =>  {
      if(item && item.selectedId){
        let data = {}
        if(key === "municipalSecuritiesBusiness"){
          data = {
            "State": item.state || "",
            "Municipal Entity Name": item.entityName || "",
            "City/County": item.city ? `${item.city}/United States` :  "",
            "Municipal Securities Business Type": item.securitiesBusiness && item.securitiesBusiness.map(m => m.name).toString() || "",
          }
          headers = ["State","Municipal Entity Name","City/County","Municipal Securities Business Type"]
        } else if(key === "municipalAdvisoryBusiness"){
          data = {
            "State": item.state || "--",
            "Municipal Entity Name": item.entityName || "",
            "City/County": item.city ? `${item.city}/United States` :  "",
            "Municipal Advisory Business Type": item.advisoryBusiness && item.advisoryBusiness.map(m => m.name).toString() || "",
            "Name of Third Party on Behalf of which Business was Solicited": item.thirdPartyName || "",
            "Nature of the Solicited Business": item.solicitedBusiness && item.solicitedBusiness.map(m => m.name).toString() || "",
          }
          headers = ["State","Municipal Entity Name","City/County","Municipal Advisory Business Type", "Name of Third Party on Behalf of which Business was Solicited", "Nature of the Solicited Business"]
        } else if(key === "ballotApprovedOfferings"){
          data = {
            "Municipal Entity Name": item.entityName || "",
            "Full Issue Description": "Zone Development Authority Tax Revenue Bonds, Series 2018" || "",
            "Reportable Date of Selection": item.reportableDate ? moment(item.reportableDate).format("MM-DD-YYYY") : ""
          }
          headers = ["Municipal Entity Name","Full Issue Description","Reportable Date of Selection"]
        }
        contributionSummaryList.push(data)
      }
    })
    const jsonSheets = [ {
      name: `${label} List`,
      headers,
      data: contributionSummaryList,
    } ]
    this.setState({
      jsonSheets,
      startXlDownload: true,
      label: `${label} Q${disclosureInfo.quarter || ""}-${disclosureInfo.year || ""}`
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  render() {
    const {
      dropDown,
      disclosureInfo,
      contribMuniEntityByState,
      paymentsToPartiesByState,
      ballotContribByState,
      documentsList,
      auditLogs,
      record,
      securitiesSearchList,
      advisorySearchList,
      ballotSearchList,
      securitiesSearch,
      advisorySearch,
      ballotSearch,
      label,
      emmaSubmissionStatus,
      length
    } = this.state
    const {audit, svControls} = this.props
    const municipalSecuritiesBusiness = securitiesSearch ? securitiesSearchList : this.state.municipalSecuritiesBusiness
    const municipalAdvisoryBusiness = advisorySearch ? advisorySearchList : this.state.municipalAdvisoryBusiness
    const ballotApprovedOfferings = ballotSearch ? ballotSearchList : this.state.ballotApprovedOfferings
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Political Contributions Prohibitions",
    }
    const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false

    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="supervisor-summary-view">
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-37 Political Contributions and Prohibitions on Municipal Securities Business and Municipal Advisory Business
              </small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-37+Political+Contributions+and+Prohibitions+on+Municipal+Securities+Business+and+Municipal+Advisory+Business.pdf"
                target="new">
                <i className="fas fa-info-circle" />
              </a>
            </p>
          </div>
        </div>

        <Accordion multiple
          activeItem={[0, 1, 2, 3]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Please read these instructions before you proceed."
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <div className="columns">
                      <div className="column">
                        <div className="content">
                          <ol type="1">
                            <li className="multiExpTblVal">
                              Effective August 17, 2016, Forms G-37 and G-37x must be
                              submitted to the MSRB electronically. Submissions by fax or
                              paper submissions will not be accepted.
                            </li>
                            <li className="multiExpTblVal">Forms G-37 and
                              G-37x must be
                              signed and submitted by an officer of the regulated entity.
                            </li>
                            <li className="multiExpTblVal">MuniVisor helps in gathering
                              individual affirmations from your employees.
                            </li>
                            <li className="multiExpTblVal">Individual
                              submissions can be viewed in the <strong>Individual
                                Submissions</strong> section and related detail can be
                              viewed in the <strong>
                                Submission Detail</strong> section
                            </li>
                            <li className="multiExpTblVal">All approved submisisons and/or
                              disclosures are collated in the <strong>G-37 Contribution
                                Summary</strong> section that you are presently on.
                            </li>
                            <li className="multiExpTblVal">Copy paste this
                              information in the relevant EMMA Dataport G-37 form
                              section.
                            </li>
                            <li className="multiExpTblVal">Download the list of
                              municipal entities with which your firm has engaged in
                              municipal securities business or municipal advisory
                              business from the CRM as an Excel Data File.
                            </li>
                            <li className="multiExpTblVal">Update the Excel Data File as
                              required and import the same in the relevant EMMA Dataport
                              G-37 form section.
                            </li>
                          </ol>
                        </div>
                      </div>

                    </div>
                  </div>
                )}
              </RatingSection>
              <div style={{ display: "none" }}>
                <ExcelSaverMulti label={label || ""} startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
              </div>
              <hr/>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Report Quarter</p>
                  <div className="select is-small is-fullwidth is-link">
                    <select name="quarter" value={disclosureInfo.quarter || ""} onChange={this.onChange}>
                      <option value="">Pick</option>
                      {dropDown.polContribQuarterOfDisclosureFirm.map((q) => <option key={q.value} value={q.value}>{q.name}</option>)}
                    </select>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Report Year<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
                  <div className="select is-small is-fullwidth is-link">
                    <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange} className="is-link">
                      <option value="">Pick</option>
                      {dropDown.polContribYearOfDisclosureFirm.map((value) => <option key={value} value={value}>{value}</option>)}
                    </select>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">EMMA Dataport Submission Status</p>
                  <SelectLabelInput
                    title="EMMA Dataport Submission Status"
                    list={["Pending", "Submitted"] || []}
                    name="emmaSubmissionStatus"
                    disabled={emmaSubmissionStatus === "Submitted"}
                    value={emmaSubmissionStatus || ""}
                    onChange={(e) => this.onChange(e, "status")}
                  />
                </div>
              </div>
              <p className="has-text-danger" style={{fontSize: "medium", textAlign: "center"}}>{record}</p>

              { !record && length ?
                <div>
                  <RatingSection
                    onAccordion={() => onAccordion(1)}
                    title={`Report Year: ${disclosureInfo.year || ""} | Report Quarter: ${disclosureInfo.quarter || ""}`}
                  >
                    {activeAccordions.includes(1) && (
                      <div>
                        <PoliticalSummaryReactTable
                          disclosuresList={contribMuniEntityByState}
                          contribMuniEntityByState
                          header="I. Contributions made to officials of a municipal entity"
                        />
                        <PoliticalSummaryReactTable
                          disclosuresList={paymentsToPartiesByState}
                          paymentsToPartiesByState
                          header="II. Payments made to political parties of states or subdivisions"
                        />
                        <RatingSection
                          onAccordion={() => onAccordion(2)}
                          title="III. Contributions made to bond ballot campaigns"
                          headerStyle={{color: "#f29718"}}
                        >
                          {activeAccordions.includes(2) && (
                            <div>
                              <PoliticalSummaryReactTable
                                disclosuresList={ballotContribByState}
                                ballotContribByState
                                header="A. Contributions"
                              />
                              <PoliticalSummaryReactTable
                                disclosuresList={ballotContribByState}
                                ballotContribByStateReimbursement
                                header="B. Reimbursement for Contributions"
                              />
                            </div>
                          )}
                        </RatingSection>

                        <RatingSection
                          onAccordion={() => onAccordion(3)}
                          title="IV. Municipal Entities with which the entity has engaged in municipal securities or municipal advisory business."
                          headerStyle={{color: "#f29718"}}
                        >
                          {activeAccordions.includes(3) && (
                            <div>
                              <PoliticalSummaryReactTable
                                disclosuresList={municipalSecuritiesBusiness}
                                onCheckSelect={this.onCheckSelect}
                                onBlur={this.onBlur}
                                onAssignedSelect={this.onAssignedSelect}
                                onSave={this.onSave}
                                xlDownload={this.xlDownload}
                                onSearchText={this.onSearchText}
                                municipalSecuritiesBusiness
                                dropDown={dropDown || {}}
                                securitiesSearch={securitiesSearch || ""}
                                header="A. Municipal Securities Business"
                                subHeader="If you do not see the municipal entity in the list below, please add to CRM first. Select entity to be included in the list
                            for current G-37 reporting period."
                                keys="municipalSecuritiesBusiness"
                                name="securitiesSearch"
                              />
                              <PoliticalSummaryReactTable
                                disclosuresList={municipalAdvisoryBusiness}
                                onCheckSelect={this.onCheckSelect}
                                onBlur={this.onBlur}
                                onAssignedSelect={this.onAssignedSelect}
                                onSave={this.onSave}
                                xlDownload={this.xlDownload}
                                onSearchText={this.onSearchText}
                                municipalAdvisoryBusiness
                                dropDown={dropDown || {}}
                                advisorySearch={advisorySearch || ""}
                                header="B. Municipal Advisory Business"
                                subHeader="If you do not see the municipal entity in the list below, please add to CRM first. Select entity to be included in the list
                            for current G-37 reporting period."
                                keys="municipalAdvisoryBusiness"
                                name="advisorySearch"
                              />
                              <PoliticalSummaryReactTable
                                disclosuresList={ballotApprovedOfferings}
                                onCheckSelect={this.onCheckSelect}
                                onBlur={this.onBlur}
                                onAssignedSelect={this.onAssignedSelect}
                                onSave={this.onSave}
                                xlDownload={this.xlDownload}
                                onSearchText={this.onSearchText}
                                ballotApprovedOfferings
                                dropDown={dropDown || {}}
                                ballotSearch={ballotSearch || ""}
                                header="C. Ballot-Approved Offerings"
                                subHeader="If you do not see the municipal entity in the list below, please add to CRM first. If you do not see the Issue in the list below, please
                            add a transaction first. Select entity to be included in the list for current G-37 reporting period."
                                keys="ballotApprovedOfferings"
                                name="ballotSearch"
                              />
                            </div>
                          )}
                        </RatingSection>

                      </div>
                    )}
                  </RatingSection>

                  <DocumentPage
                    {...this.props}
                    tableStyle={{fontSize: "smaller"}}
                    isNotTransaction
                    tranId="cmp-sup-politicalSummary"
                    title={`Documents [Report Year: ${disclosureInfo.year || ""} | Report Quarter: ${disclosureInfo.quarter || ""}]`}
                    pickCategory="LKUPDOCCATEGORIES"
                    pickSubCategory="LKUPCORRESPONDENCEDOCS"
                    pickType="LKUPDOCTYPE"
                    pickAction="LKUPDOCACTION"
                    category="poliContributionSummaryDocuments"
                    staticField={staticField}
                    documents={documentsList || []}
                    contextType={ContextType.supervisor.poliContributeRelated}
                    onSave={this.onDocSave}
                    onDeleteDoc={this.onDeleteDoc}
                    onStatusChange={this.onStatusChange}
                  />

                  {
                    submitAudit &&
                    <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{overflowY: "scroll", fontSize: "smaller"}}>
                      {activeAccordions.includes(2) &&
                      <Audit auditLogs={auditLogs || []}/>
                      }
                    </RatingSection>
                  }
                </div> : null
              }

            </div>
          }
        />

      </div>
    )
  }
}


const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

const WrappedComponent = withAuditLogs(ContributionSummary)
export default connect(mapStateToProps, null)(WrappedComponent)
