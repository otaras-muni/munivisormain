import React, { Component } from "react"
import Loader from "Global/Loader"
import {Link} from "react-router-dom"
import Summary from "../components/Summary"
import Detail from "../components/Detail"
import ContributionSummary from "./ContributionSummary"
import FastTrackDetail from "../components/FastTrackDetail"

const tabs = [
  {
    title: <span>Individual Submissions</span>,
    value: 1,
    path: "ind-submission",
  },
  {
    title: <span>Submission Detail</span>,
    value: 2,
    path: "sub-detail",
  },
  {
    title: <span>G-37 Contribution Summary (firm)</span>,
    value: 3,
    path: "con-summary",
  }
]

class SupervisorView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
    }
  }

  componentWillMount() {
    const { nav4 } = this.props
    if(nav4){
      if(nav4 === "fastTrackDetail"){
        tabs[1].path = "fastTrackDetail"
      } else {
        tabs[1].path = "sub-detail"
      }
    } else {
      this.props.history.push("/compliance/cmp-sup-political/supervisor-view/ind-submission")
    }
    this.setState({
      loading: false,
    })
  }

  renderTabs = (activeTab) => {
    return (
      <div className="tabs">
        <ul>
          {
            tabs.map(tab=> {
              const path = tab.path === "sub-detail" || ""
              return (
                <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`}>
                  {
                    this.props.nav2 ? <Link to={`/compliance/cmp-sup-political/supervisor-view/${tab.path}${path ? "?status=Record" : ""}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                      <a role="button" className="tabSecLevel">
                        {tab.title}
                      </a>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }

  renderSelectedView = () => {
    switch(this.props.nav4) {
    case "ind-submission" :
      return <Summary {...this.props}/>
    case "sub-detail" :
      return <Detail {...this.props}/>
    case "fastTrackDetail" :
      return <FastTrackDetail {...this.props}/>
    case "con-summary" :
      return <ContributionSummary {...this.props}/>
    default:
      return "not found"
    }
  }

  render() {
    const activeTab = tabs.find(x => x && (x.path === this.props.nav4))
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        {this.props.nav2 && this.props.nav3 !== "managequemedia" && this.renderTabs(activeTab && activeTab.value) }
        <div >
          {this.renderSelectedView()}
        </div>
      </div>
    )
  }
}

export default SupervisorView
