import React, { Component } from "react"
import {Link} from "react-router-dom"
import Loader from "Global/Loader"
import PoliticalDashReactTable from "./PoliticalDashReactTable"

class Summary extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
    }
  }

  async componentWillMount() {
    this.setState({
      loading:false,
    })
  }

  render() {
    const { svControls } = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <p className="column">
            <small>Rule G-37 Political Contributions and Prohibitions on Municipal Securities Business and Municipal Advisory Business</small>
            <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-37+Political+Contributions+and+Prohibitions+on+Municipal+Securities+Business+and+Municipal+Advisory+Business.pdf"
              target="new">
              <i className="fas fa-info-circle" />
            </a>
          </p>
        </div>
        <div className="column">
          <PoliticalDashReactTable
            svControls={svControls}
          />
        </div>
      </div>
    )
  }
}

export default Summary
