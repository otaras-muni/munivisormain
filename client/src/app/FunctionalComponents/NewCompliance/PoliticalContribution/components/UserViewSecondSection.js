import React from "react"
import moment from "moment"
import {NumberInput, SelectLabelInput, TextLabelInput} from "./PoliticalTextViewBox"
import SearchCountry from "../../../GoogleAddressForm/GoogleCountryComponent";

const UserViewSecondSection = ({getCountryDetails, item, index, onChangeItems, category, dropDown, length, minDate, maxDate, onEdit, onSave, isEditable, tableTitle, onCancel, onRemove, isSaveDisabled, errors, allDisable, onBlur, quarter, year}) => {
  const onChange = (event) => {
    onChangeItems({
      ...item,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value
    }, category, index)
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(`${tableTitle} In ${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`,category)
    }
  }

  const required = <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>

  isEditable = (isEditable[category] === index)

  return(
    <div>
      <div className="columns">
        <div className="column">
          <label className="checkbox-button multiExpLbl">
            Check here if none
            <input
              type="checkbox"
              name="checkNone"
              title="Check here if none"
              checked={(item && item.checkNone) || false}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={allDisable || !isEditable || false}
            />
            <span className="checkmark" />
          </label>
        </div>
        { allDisable ? null :
          <div className="column">
            <div className="field is-grouped" style={{marginBottom: 0}}>
              <div className="control">
                <a onClick={isEditable ? () => onSave(category, item, index, tableTitle) : () => onEdit(category, index, tableTitle)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="far fa-save"/> : <i className="fas fa-pencil-alt" />}
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={isEditable ? () => onCancel(category) :() => onRemove(item._id, category, index, tableTitle)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="fa fa-times"/> : <i className="far fa-trash-alt"/>}
                  </span>
                </a>
              </div>
            </div>
          </div>
        }
      </div>

      {item && !item.checkNone ?
        <div>
          <div className="columns">
            <SearchCountry
              idx={`second-${index}`}
              required
              disabled={allDisable || !isEditable || false}
              value={(item.city && item.state && item.county) ? `${item.city}, ${item.state}, ${item.county}` :
                item.city ? `${item.city}` : item.state ? `${item.state}` :  item.county ? `${item.city}` : ""}
              label="State/City/County"
              error={(errors && errors.state) || ""}
              getCountryDetails={(e)=> getCountryDetails(e, category, index)}
            />
            <div className="column">
              <p className="multiExpLbl">Political Party{required}</p>
              <TextLabelInput
                name="politicalParty"
                title="Political Party"
                value={(item && item.politicalParty) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.politicalParty) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Contributor Category</p>
              <SelectLabelInput
                list={dropDown && dropDown.contributionCategoryList || []}
                name="contributorCategory"
                title="Contributor Category"
                value={(item && item.contributorCategory) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.contributorCategory) || ""}
              />
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <p className="multiExpLbl">Contribution Amount{required}</p>
              <NumberInput
                prefix="$"
                name="contributionAmount"
                title="Contribution Amount"
                value={(item && item.contributionAmount) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.contributionAmount) ? "Required(must be larger than or equal to 0)" : ""  || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Contribution Date{required}</p>
              <TextLabelInput
                type="date"
                name="contributionDate"
                title="Contribution Date"
                // value={item.contributionDate ? moment(item.contributionDate).format("YYYY-MM-DD") : ""}
                value={(item.contributionDate === "" || !item.contributionDate) ? null : new Date(item.contributionDate)}
                onChange={onChange}
                onBlur={onBlurInput}
                // min={minDate}
                // max={maxDate}
                disabled={allDisable || !isEditable || false}
                error={errors.contributionDate = item.contributionDate !== "" ?
                  (((moment(item.contributionDate || "").format("YYYY-MM-DD") >= moment(minDate).format("YYYY-MM-DD") && moment(item.contributionDate).format("YYYY-MM-DD") <= moment(maxDate).format("YYYY-MM-DD"))
                    ? (errors && errors.contributionDate) : `Selected Date is not in Quarter ${quarter || ""} Year ${year || ""}`)) : (errors && errors.contributionDate) || "" }
              />
            </div>
            <div className="column"/>
          </div>
        </div> : null
      }
      {length > 1 ? length - 1 === index ? null : <hr style={{border: "1px dotted"}}/> : null}
    </div>
  )
}
export default UserViewSecondSection
