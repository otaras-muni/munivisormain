import React, { Component } from "react"
import Loader from "Global/Loader"
import { getPicklistByPicklistName, updateAuditLog, updateControlTaskStatus, getToken } from "GlobalUtils/helpers"
import connect from "react-redux/es/connect/connect"
import { toast } from "react-toastify"
import swal from "sweetalert"
import * as qs from "query-string"
import { DropdownList } from "react-widgets"
import moment from "moment"
import cloneDeep from "lodash.clonedeep"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import { SelectLabelInput } from "./PoliticalTextViewBox"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import DocumentPage from "../../../../GlobalComponents/DocumentPage"
import Audit from "../../../../GlobalComponents/Audit"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import { fetchAssigned, fetchIssuers } from "../../../../StateManagement/actions/CreateTransaction/index";
import UserViewFirstSection from "./UserViewFirstSection"
import UserViewSecondSection from "./UserViewSecondSection"
import UserViewThirdSection from "./UserViewThirdSection"
import { ContributeOfMunicipalEntityValidate, PaymentPolPartiesValidate, BallotContribValidate } from "../../Validation/PolContributionValidate"
import { putPolContribMuniEntity, getPoliContributionDetailsById, pullDisclosure, putContributeDetailsDocuments, postPoliContributions } from "../../../../StateManagement/actions/Supervisor/index"
import { getAuditLogByType } from "../../../../StateManagement/actions/audit_log_actions"
import { getSupervisor } from "../../../../StateManagement/actions/Supervisor/Supervisor"

const cols = [
  { name: "User" },
  { name: "Notes/Instruction" },
  { name: "Time" }]

class Detail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      contribMuniEntityByState: [cloneDeep(CONST.NewCompliance.politicalContributions.contribMuniEntityByState)],
      paymentsToPartiesByState: [cloneDeep(CONST.NewCompliance.politicalContributions.paymentsToPartiesByState)],
      ballotContribByState: [cloneDeep(CONST.NewCompliance.politicalContributions.ballotContribByState)],
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      dropDown: {
        polContribQuarterOfDisclosureFirm: [{ name: "First", value: 1 }, { name: "Second", value: 2 }, { name: "Third", value: 3 }, { name: "Fourth", value: 4 }],
        polContribYearOfDisclosureFirm: [],
        polContribOfDisclosureFirm: ["Self", "Someone else", "Firm"],
        supervisorAction: [],
        usersList: [],
        entityList: [],
      },
      disclosureDetails: {},
      auditLogs: [],
      isEditable: {},
      isSaveDisabled: {},
      errorMessages: {},
      documentsList: [],
      supervisorList: [],
      disclosureId: "",
      navStatus: "",
      noteError: "",
      actionBy: "",
      disclosureInfo: CONST.SuperVisor.disclosureInfo || {},
      confirmAlert: CONST.confirmAlert,
      politicalConfirmAlert: CONST.politicalConfirmAlert,
      loading:true,
      issuerList: []
    }
  }

  async componentWillMount() {
    const result = await getPicklistByPicklistName(["LKUPCONTRIBUTORCAT", "LKUPSUPERVISORACTION"])
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        // stateList: result && result[3] && result[3].LKUPCOUNTRY || {},
        contributionCategoryList: result && result[1] && result[1].LKUPCONTRIBUTORCAT || [],
        supervisorAction: result && result[1] && result[1].LKUPSUPERVISORACTION || [],
      },
      loading: false
    })
    this.getIssuingMunicipalEntity()
  }

  async componentDidMount() {
    const { contribMuniEntityByState, paymentsToPartiesByState, ballotContribByState, currentYear, currentQuarter } = this.state
    const queryString = qs.parse(location.search)
    contribMuniEntityByState[0].isNew = true
    paymentsToPartiesByState[0].isNew = true
    ballotContribByState[0].isNew = true
    contribMuniEntityByState[0].checkNone = false
    paymentsToPartiesByState[0].checkNone = false
    ballotContribByState[0].checkNone = false
    let preApprovalQuarter = queryString.quarter || ""
    let preApprovalYear = queryString.year || ""
    const polContribYearOfDisclosureFirm = []
    let min = 2015, max = min + 10
    for (let i = min; i <= max; i++) { polContribYearOfDisclosureFirm.push(i) }
    const statusFlag = (queryString.year > currentYear) || ((queryString.quarter > currentQuarter) && (queryString.year === currentYear)) || queryString.status === "Pre-approval"

    if (statusFlag && !queryString.year && !queryString.quarter) {
      if (currentQuarter === 4) {
        preApprovalQuarter = 1
        preApprovalYear = currentYear + 1
      } else {
        preApprovalQuarter = currentQuarter + 1
        preApprovalYear = currentYear
      }
    }
    let res = []
    if(queryString && queryString.disclosure){
      const query = `?_id=${queryString.disclosure}`
      res = await getPoliContributionDetailsById(query)
      this.getAuditLogById(queryString.disclosure)
    }
    this.setState({
      contribMuniEntityByState: res && res.length && res[0].contribMuniEntityByState && res[0].contribMuniEntityByState.length ? res[0].contribMuniEntityByState : contribMuniEntityByState,
      paymentsToPartiesByState: res && res.length && res[0].paymentsToPartiesByState && res[0].paymentsToPartiesByState.length ? res[0].paymentsToPartiesByState : paymentsToPartiesByState,
      ballotContribByState: res && res.length && res[0].ballotContribByState && res[0].ballotContribByState.length ? res[0].ballotContribByState : ballotContribByState,
      disclosureInfo: {
        ...this.state.disclosureInfo,
        year: res && res.length && res[0].year ? res[0].year : currentQuarter === 4 && statusFlag ? preApprovalYear : queryString.year || currentYear,
        quarter: res && res.length && res[0].quarter ? res[0].quarter : statusFlag ? preApprovalQuarter : queryString.quarter || currentQuarter,
        disclosureFor: res && res.length && res[0].userOrEntityId ? res[0].userOrEntityId : queryString.disclosureFor || this.props.user.userId || "",
        status: res && res.length && res[0].status ? res[0].status : statusFlag ? "Pre-approval" : "",
      },
      isEditable: {
        ...this.state.isEditable,
        contribMuniEntityByState: res && res.length && res[0].contribMuniEntityByState && res[0].contribMuniEntityByState.length ? "" : 0,
        paymentsToPartiesByState: res && res.length && res[0].paymentsToPartiesByState && res[0].paymentsToPartiesByState.length ? "" : 0,
        ballotContribByState: res && res.length && res[0].ballotContribByState && res[0].ballotContribByState.length ? "" : 0,
      },
      dropDown: {
        ...this.state.dropDown,
        polContribYearOfDisclosureFirm,
      },
      documentsList: res && res.length && res[0].contributionsRelatedDocuments || [],
      disclosureId: res && res.length && res[0]._id || "",
      disclosureDetails: res && res.length && res[0] || {},
      preApprovalQuarter,
      preApprovalYear,
      status: statusFlag ? "Pre-approval" : "",
      detailStatus: res && res.length && res[0] && res[0].status || "",
      actionBy: res && res.length && res[0] && res[0].actionBy || "",
      navStatus: queryString && queryString.status ? queryString.status : ""
    }, () => {
      const { status, disclosureId } = this.state
      if(status === "Pre-approval" && !disclosureId){
        this.getSuperviosrList()
      }
      this.getEntitiesAndUsers()
    })
  }

  getIssuingMunicipalEntity = () => {
    fetchIssuers(this.props.user.entityId, res => {
      this.setState({
        issuerList: res.issuerList,
        loading: false,
      })
    })
  }

  getSuperviosrList = async () => {
    const supervisor = await getSupervisor()
    if (supervisor && supervisor.data) {
      this.setState({
        supervisorList: (supervisor && supervisor.data) || []
      })
    }
  }

  onChange = (e, inputName) => {
    if (inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
        }
      })
    } else if(e.target.name === "status") {
      const { name, value } = e.target
      const { confirmAlert } = this.state
      confirmAlert.text = `You Want To ${value} The Disclosure?`
      swal(confirmAlert)
        .then((willDelete) => {
          if (value) {
            if(willDelete){
              if(name === "status"){
                const { user } = this.props
                const userName = `${user.userFirstName} ${user.userLastName}` || ""
                this.props.addAuditLog({userName, log: `Supervisor ${userName} Disclosure status Change To ${value || ""}`, date: new Date(), key: "notes"})
              }
              this.setState({
                disclosureInfo: {
                  ...this.state.disclosureInfo,
                  [name]: value
                }
              }, () => {
                if (name === "status") {
                  this.onDisclosureSave(true)
                }
              })
            }
          }
        })
    } else {
      const { name, value } = e.target
      const {contribMuniEntityByState, paymentsToPartiesByState, ballotContribByState, isEditable} = this.state
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [name]: e.target.type === "checkbox" ? e.target.checked : value
        }
      }, () => {
        const quarterDate = this.getQuarterDate(this.state.disclosureInfo.quarter, this.state.disclosureInfo.year)
        contribMuniEntityByState.forEach(data => {
          if (isEditable && isEditable.contribMuniEntityByState !== -1) {
            data.contributionDate = (quarterDate && quarterDate.start.toString()) || ""
            data.dateOfExemption = (quarterDate && quarterDate.start.toString()) || ""
          }
        })
        paymentsToPartiesByState.forEach(data => {
          if (isEditable && isEditable.paymentsToPartiesByState !== -1) {
            data.contributionDate = (quarterDate && quarterDate.start.toString()) || ""
          }
        })
        ballotContribByState.forEach(data => {
          if (isEditable && isEditable.paymentsToPartiesByState !== -1) {
            data.contributionDate = (quarterDate && quarterDate.start.toString()) || ""
          }
        })
        this.setState({contribMuniEntityByState, paymentsToPartiesByState, ballotContribByState})
      })
    }
  }

  onChangeItems = (item, key, index) => {
    const changeItems = cloneDeep(this.state[key])
    changeItems[index] = item
    this.setState({ [key]: changeItems })
  }

  onAdd = (key, data, tableName) => {
    const { user } = this.props
    const quarterDate = this.getQuarterDate(this.state.disclosureInfo.quarter, this.state.disclosureInfo.year)
    const newItems = this.state[key]
    CONST.NewCompliance.politicalContributions[key].isNew = true
    CONST.NewCompliance.politicalContributions[key].checkNone = false
    CONST.NewCompliance.politicalContributions[key].contributionDate = (quarterDate && quarterDate.start.toString()) || ""

    if (key === "contribMuniEntityByState") {
      CONST.NewCompliance.politicalContributions[key].dateOfExemption = (quarterDate && quarterDate.start.toString()) || ""
    }
    newItems.push(cloneDeep(CONST.NewCompliance.politicalContributions[key]))
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${tableName} Add new Item`, date: new Date(), key })
    this.setState({
      [key]: newItems,
      activeItem: [data],
      isEditable: {
        ...this.state.isEditable,
        [key]: newItems.length - 1 || 0,
      },
    })
  }

  onEdit = (key, index, tableTitle) => {
    const { user } = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `In ${tableTitle} one row edited`, date: new Date(), key })
    if (this.state.isEditable[key]) { swal("Warning", "First save the current item", "warning"); return }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index,
      },
    })
  }

  onSave = (key, item, index) => {
    let itemData = {}
    itemData = {
      ...item
    }
    if(itemData && itemData.isNew){
      delete itemData.isNew
    }
    if (key === "contribMuniEntityByState" && !itemData.exempted) {
      delete itemData.dateOfExemption
    }
    if (itemData && itemData.checkNone){
      if(itemData && itemData._id){
        CONST.NewCompliance.politicalContributions[key]._id = item._id
        CONST.NewCompliance.politicalContributions[key].createdDate = item.createdDate
      }
      CONST.NewCompliance.politicalContributions[key].checkNone = true
      itemData = CONST.NewCompliance.politicalContributions[key]
    }
    let errors = {}
    if(key === "contribMuniEntityByState"){
      errors = ContributeOfMunicipalEntityValidate(itemData, itemData.checkNone)
    } else if(key === "paymentsToPartiesByState"){
      errors = PaymentPolPartiesValidate(itemData, itemData.checkNone)
    } else if(key === "ballotContribByState"){
      errors = BallotContribValidate(itemData, itemData.checkNone)
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        const message = "Required"
        if (errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.context.key] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.context.key]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.context.key]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    const quarterDate = this.getQuarterDate(this.state.disclosureInfo.quarter, this.state.disclosureInfo.year)

    if (!(itemData && itemData.checkNone)) {
      if (key === "contribMuniEntityByState" || key === "paymentsToPartiesByState" || key === "ballotContribByState") {
        if (!(moment(itemData.contributionDate || "").format("YYYY-MM-DD") >= moment(quarterDate && quarterDate.start.toString()).format("YYYY-MM-DD") && moment(itemData.contributionDate || "").format("YYYY-MM-DD") <= moment(quarterDate && quarterDate.end.toString()).format("YYYY-MM-DD"))) {
          return
          /* this.setState({
            errorMessages:{
              ...errorMessages,
              [key] :{
                ...errorMessages[key],
                [index]:{
                  ...errorMessages[key][index],
                  contributionDate: `Selected Date is not in Quarter ${this.state.disclosureInfo.quarter || ""} Year ${this.state.disclosureInfo.year || ""}`
                }
              }
            }
          }) */
        }
        if (key === "contribMuniEntityByState" && itemData.exempted) {
          if (!(moment(itemData.dateOfExemption || "").format("YYYY-MM-DD") >= moment(quarterDate && quarterDate.start.toString()).format("YYYY-MM-DD") && moment(itemData.dateOfExemption || "").format("YYYY-MM-DD") <= moment(quarterDate && quarterDate.end.toString()).format("YYYY-MM-DD"))) {
            return
          }
        }
      }
    }

    const { status, disclosureId, supervisorList, politicalConfirmAlert } = this.state
    if(status === "Pre-approval" && !disclosureId && !supervisorList.length){
      politicalConfirmAlert.text = "There are no supervisors in the firm, please contact your admin.  The pre-approval will be saved and can be submitted again by the user after supervisor(s) have been added."
      swal(politicalConfirmAlert)
        .then((willDelete) => {
          if (!disclosureId) {
            if(willDelete){
              this.setState({
                isSaveDisabled: {
                  ...this.state.isSaveDisabled,
                  [key]: true
                }
              }, () => {
                delete itemData.isNew
                this.onSaves(key, itemData)
              })
            }
          }
        })
    } else {
      this.setState({
        isSaveDisabled: {
          ...this.state.isSaveDisabled,
          [key]: true
        }
      }, () => {
        delete itemData.isNew
        this.onSaves(key, itemData)
      })
    }
  }

  onSaves = (key, item) => {
    const { disclosureInfo, disclosureId, navStatus } = this.state
    const type = navStatus === "Pre-approval" ? "Pre" : navStatus === "Record" ? navStatus : ""
    const userId = disclosureInfo && disclosureInfo.disclosureFor ? disclosureInfo.disclosureFor : this.props.user.userId || ""
    const queryString = `${disclosureInfo.year}?quarter=${disclosureInfo.quarter}&_id=${disclosureId}&userOrEntityId=${userId}&type=${key}&discloseType=${type}`
    putPolContribMuniEntity(queryString, item, (res) => {

      if (res && res.status === 200) {
        toast("Political contribution detail updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.setState({
          ...this.state[key],
          [key]: res && res.data && res.data[key] || [],
          disclosureId: res && res.data && res.data._id || "",
          detailStatus: res && res.data && res.data.status || "",
          disclosureDetails: res && res.data || {},
          errorMessages: {},
          isEditable: {
            ...this.state.isEditable,
            [key]: "",
          },
          disclosureInfo: {
            disclosureFor: res && res.data.userOrEntityId || "",
            quarter: res && res.data.quarter || "",
            status: res && res.data.status || "",
            year: res && res.data.year || ""
          },
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [key]: false
          }
        }, () => {
          if(this.state.disclosureId){
            this.onAuditSave(key)
          }
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [key]: false
          },
          errorMessages: {},
        })
      }
    })
  }

  onCancel = (key) => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: "",
      },
    })
  }

  onRemoveDoc = (removeId, key, index, tableTitle) => {
    const { isEditable, confirmAlert } = this.state
    confirmAlert.text = `You want to delete ${tableTitle}?`
    swal(confirmAlert).then((willDelete) => {
      if (willDelete) {
        if (key) {
          this.setState({
            isSaveDisabled: {
              ...this.state.isSaveDisabled,
              [key]: true
            }
          }, () => {
            if(removeId){
              this.props.addAuditLog({ userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `Remove item from ${tableTitle}`, date: new Date(), key: key })
              this.onRemove(key, removeId)
            } else {
              const list = this.state[key]
              if (list.length) {
                list.splice(index, 1)
                this.setState({
                  [key]: list,
                  isEditable: {
                    ...isEditable,
                    [key]: ""
                  },
                  isSaveDisabled: {
                    ...this.state.isSaveDisabled,
                    [key]: false
                  },
                  errorMessages: {}
                })
              }
            }
          })
        }
      }
    })
  }

  onRemove = (key, removeId) => {
    const { disclosureId } = this.state
    CONST.NewCompliance.politicalContributions[key].isNew = true
    pullDisclosure(key, disclosureId, removeId, (res) => {
      if (res && res.status === 200) {
        toast("Removed Political Contribution Detail successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(key)
        this.setState({
          ...this.state[key],
          [key]: res && res.data && res.data[key] && res.data[key].length ? res.data[key] : [CONST.NewCompliance.politicalContributions[key]],
          disclosureDetails: res && res.data || {},
          errorMessages: {},
          isEditable: {
            ...this.state.isEditable,
            [key]: res && res.data && res.data[key] && res.data[key].length ? "" : 0,
          },
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [key]: false
          }
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          isSaveDisabled: {
            ...this.state.isSaveDisabled,
            [key]: false
          },
          errorMessages: {},
        })
      }
    })
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    const { disclosureId } = this.state
    if (disclosureId) {
      putContributeDetailsDocuments("", disclosureId, docs, (res) => {
        if (res && res.status === 200) {
          toast("Disclosure Related Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("documents")
          callback({
            status: true,
            documentsList: (res.data && res.data.contributionsRelatedDocuments) || [],
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    }
  }

  onStatusChange = async (e, doc, callback) => {
    const { disclosureId } = this.state
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc,
        lastUpdatedDate: moment(new Date()).format("MM/DD/YYYY hh:mm A")
      }
      type = "updateMeta"
    }
    putContributeDetailsDocuments(`?details=${type}`, disclosureId, document, (res) => {
      if (res && res.status === 200) {
        toast("Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if(name){
          callback({
            status: true,
          })
        }else {
          callback({
            status: true,
            documentsList: (res.data && res.data.contributionsRelatedDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const {disclosureId} = this.state
    pullDisclosure("contributionsRelatedDocuments", disclosureId, documentId, (res) => {
      if (res && res.status === 200) {
        toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        callback({
          status: true,
          documentsList: (res.data && res.data.contributionsRelatedDocuments) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onMakeDisclosureSave = (status) => {
    const { disclosureInfo, disclosureId, supervisorNote } = this.state
    const {user, svControls} = this.props
    const nav3 =  svControls.supervisor ? "supervisor-view" : "user-view"
    const nav4 =  svControls.supervisor ? "ind-submission" : "summary"
    delete disclosureInfo.G37Obligation
    delete disclosureInfo.affirmationNotes
    disclosureInfo.status = status
    if (supervisorNote) {
      disclosureInfo.notes = {
        userName: `${user.userFirstName} ${user.userLastName}`,
        note: supervisorNote,
        supervisor: svControls.supervisor,
      }
      this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `add new notes ${supervisorNote} by ${user.userFirstName} ${user.userLastName}`, date: new Date(), key: "MakeDisclosure" })
    }
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${status === "Complete" ? "Make Disclosure" : "Send for Pre-approval"}`, date: new Date(), key: "MakeDisclosure" })
    disclosureInfo._id = disclosureId
    this.setState(prevState => ({
      isSaveDisabled: {
        ...prevState.isSaveDisabled,
        details: true
      }
    }), () => {
      postPoliContributions(disclosureInfo, async (res) => {
        if ((res && res.data && res.data.cac) && status === "Complete") {
          const taskId = res.data && res.data.taskRefId || ""
          try{
            await updateControlTaskStatus(getToken(), taskId)
          } catch (err) {
            console.log(err)
          }
        }
        this.onAuditSave("MakeDisclosure")
        if (res && res.status === 200) {
          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.history.push(`/compliance/cmp-sup-political/${nav3}/${nav4}`)
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onChangeItem = (value, name) => {
    this.setState({
      [name]: value,
    })
  }

  onDisclosureSave = (flag) => {
    const { disclosureInfo, supervisorNote, disclosureId, navStatus, detailStatus, actionBy } = this.state
    const { svControls, user } = this.props
    const type = navStatus === "Pre-approval" ? "Pre" : navStatus === "Record" ? navStatus : ""

    if (supervisorNote) {
      disclosureInfo.notes = {
        userName: `${user.userFirstName} ${user.userLastName}`,
        note: supervisorNote,
        supervisor: svControls.supervisor,
      }
      this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `add new notes ${supervisorNote} by ${user.userFirstName} ${user.userLastName}`, date: new Date(), key: "notes" })
      delete disclosureInfo.status
    }
    this.setState(prevState => ({
      isSaveDisabled: {
        ...prevState.isSaveDisabled,
        details: true
      }
    }), () => {
      delete disclosureInfo.G37Obligation
      delete disclosureInfo.affirmationNotes
      disclosureInfo._id = disclosureId
      disclosureInfo.discloseType = type
      if(!detailStatus){
        disclosureInfo.status = flag === false ? "Pending" : disclosureInfo.status
      }
      if(disclosureInfo && (disclosureInfo.status === "Approved" || disclosureInfo.status === "Rejected" || disclosureInfo.status === "Canceled") && !actionBy && (svControls && svControls.supervisor)){
        disclosureInfo.actionBy = `${user.userFirstName} ${user.userLastName}`
      }
      postPoliContributions(!flag ? { ...disclosureInfo, type: "notes" } : disclosureInfo, (res) => {
        if (res && res.status === 200) {
          this.onAuditSave("notes")
          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState(prevState => ({
            disclosureDetails: res.data || {},
            isSaveDisabled: {
              ...prevState.isSaveDisabled,
              details: false
            },
            disclosureInfo: {
              ...disclosureInfo,
              notes: "",
              status: (res.data && res.data.status) || ""
            },
            contributeId: (res.data && res.data.contributeId) || "",
            supervisorNote: "",
            actionBy: (res.data && res.data.actionBy) || "",
            noteError: ""
          }),() => {
            // this.onAuditSave(flag ? "details" : "notes")
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onAuditSave = async (key) => {
    const { user } = this.props
    const { disclosureDetails } = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)

    if (disclosureDetails && disclosureDetails._id) {
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Political Contributions and Prohibitions"
          log.superVisorSubSection = "Disclosure"
        })
        await updateAuditLog(disclosureDetails._id, auditLogs)
        this.props.updateAuditLog([])
        this.getAuditLogById(disclosureDetails._id)
      }
    }
  }

  onBlur = (change, category) => {
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: `${change}`, date: new Date(), key: category})
  }

  getEntitiesAndUsers = () => {
    const {contribMuniEntityByState, paymentsToPartiesByState, ballotContribByState} = this.state
    fetchAssigned(this.props.user.entityId, (users) => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || []
        },
        loading: false,
      })
    })
    const quarterDate = this.getQuarterDate(this.state.disclosureInfo.quarter, this.state.disclosureInfo.year)
    this.state.contribMuniEntityByState.forEach(data => {
      if (data.isNew) {
        data.contributionDate = (quarterDate && quarterDate.start.toString()) || ""
        data.dateOfExemption = (quarterDate && quarterDate.start.toString()) || ""
      }
    })
    this.state.paymentsToPartiesByState.forEach(data => {
      if (this.state.isEditable && this.state.isEditable.paymentsToPartiesByState !== -1) {
        data.contributionDate = (quarterDate && quarterDate.start.toString()) || ""
      }
    })
    this.state.ballotContribByState.forEach(data => {
      if (this.state.isEditable && this.state.isEditable.paymentsToPartiesByState !== -1) {
        data.contributionDate = (quarterDate && quarterDate.start.toString()) || ""
      }
    })
    this.setState({contribMuniEntityByState, paymentsToPartiesByState, ballotContribByState})
  }

  getAuditLogById = (id) => {
    getAuditLogByType("politicalContribution", id, response => {
      this.setState({
        auditLogs: response && response.changeLog || []
      })
    })
  }

  getQuarterDate = (quarter, year) => {
    return {
      start: new Date(moment().month(parseInt(quarter, 10)*3 - 3).year(parseInt(year, 10)).startOf("month")),
      end: new Date(moment().month(parseInt(quarter, 10)*3 - 1).year(parseInt(year, 10)).endOf("month"))
    }
  }

  actionButtons = (key, isDisabled, data, allDisable, tableName) => {
    const { disclosureInfo } = this.state
    const list = this.state[key]
    const isAddNew = (list && list.find(part => part.isNew)) || false
    if (allDisable || disclosureInfo && disclosureInfo.status === "Dropped") return
    return (
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key, data, tableName)}
            disabled={isDisabled || !!isAddNew || false}
          >
            Add
          </button>
        </div>
      </div>
    )
  }

  getCountryDetails = (cityStateCountry = "", key, index) => {
    if(cityStateCountry.state){
      const changeItems = cloneDeep(this.state[key])
      changeItems[index].city = cityStateCountry.city
      changeItems[index].state = cityStateCountry.state
      changeItems[index].county = cityStateCountry.county
      this.setState({[key]: changeItems })
    }
  }

  render() {
    const {
      contribMuniEntityByState,
      paymentsToPartiesByState,
      ballotContribByState,
      dropDown,
      disclosureInfo,
      currentYear,
      currentQuarter,
      status,
      disclosureDetails,
      isEditable,
      errorMessages,
      documentsList,
      disclosureId,
      supervisorNote,
      supervisorNotesShow,
      navStatus,
      auditLogs,
      noteError,
      isSaveDisabled,
      issuerList
    } = this.state
    const { svControls, user, nav2, audit } = this.props
    let quarterDate = ""
    let isMakeDisclose = 0
    if((disclosureDetails && disclosureDetails.contribMuniEntityByState && disclosureDetails.contribMuniEntityByState.length) || (disclosureDetails && disclosureDetails.paymentsToPartiesByState && disclosureDetails.paymentsToPartiesByState.length) ||
      (disclosureDetails && disclosureDetails.ballotContribByState && disclosureDetails.ballotContribByState.length)){
      isMakeDisclose = 1
    }
    if(disclosureInfo && disclosureInfo.quarter && disclosureInfo.year){
      quarterDate = this.getQuarterDate(disclosureInfo.quarter, disclosureInfo.year)
    }
    const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
    const flag = (parseInt(disclosureInfo.year,10) > currentYear) || ((parseInt(disclosureInfo.quarter,10) > currentQuarter) && (parseInt(disclosureInfo.year,10) === currentYear))
    const canEdit = disclosureDetails && disclosureDetails.userOrEntityId  && user ? disclosureDetails.status === "Pending" : true
    const isDisclose = disclosureDetails && disclosureDetails.userOrEntityId  && user && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved")
    const allDisable = disclosureDetails && (disclosureDetails.status === "Pre-approval" || disclosureDetails.status === "Complete" || disclosureDetails.status === "Approved" || disclosureDetails.status === "Rejected" || disclosureDetails.status === "Canceled") || false
    const discloseStatus = disclosureDetails && disclosureDetails.status === "Complete" || ""
    const dropped = disclosureInfo && disclosureInfo.status === "Dropped"

    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Political Contributions Prohibitions",
    }
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-37 Snapshot by NAMA</small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/NAMAG37CHART2.pdf" target="new">
                <i className="fas fa-info-circle"/>
              </a>
            </p>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <p className="multiExpLbl ">Disclosure for<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
            <div className="control">
              <div className="select is-link is-small is-fullwidth select-arrow-hack">
                {allDisable ? <p>{`${disclosureDetails.userFirstName} ${disclosureDetails.userLastName}`}</p> :
                  <div>
                    {svControls.supervisor && !disclosureId ?
                      <DropdownList filter value={disclosureInfo.disclosureFor || []} data={dropDown.usersList || []} textField="name" valueField="id" key="name" onChange={(user) => this.onChange(user, "disclosureFor")} disabled={!svControls.supervisor} className="is-link" /> :
                      disclosureId && discloseStatus ? <p>{`${disclosureDetails.userFirstName} ${disclosureDetails.userLastName}`}</p> : <p>{`${user.userFirstName} ${user.userLastName}`}</p> }
                  </div>
                }
                {svControls.supervisor && !disclosureId &&
                <div>
                  <a className="has-text-link" style={{ fontSize: 12, marginTop: -10 }} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                  <i className="fa fa-refresh" style={{ fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px" }} onClick={this.onUsersListRefresh} />
                </div>}
              </div>
            </div>
          </div>
          <div className="column">
            <p className="multiExpLbl">Report Quarter<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
            <div className="select is-small is-fullwidth is-link">
              <select name="quarter" value={disclosureInfo.quarter || ""} onChange={this.onChange} disabled={disclosureId}>
                <option value="">Pick</option>
                {dropDown.polContribQuarterOfDisclosureFirm.map((q) => <option key={q.value} value={q.value} >{q.name}</option>)}
              </select>
            </div>
          </div>
          <div className="column">
            <p className="multiExpLbl">Report Year<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
            <div className="select is-small is-fullwidth is-link">
              <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange} className="is-link" disabled={disclosureId}>
                <option value="">Pick</option>
                {dropDown.polContribYearOfDisclosureFirm.map((value) => <option key={value} value={value} >{value}</option>)}
              </select>
            </div>
          </div>
          {
            svControls.supervisor && ( disclosureDetails.status === "Pre-approval" || disclosureInfo.status === "Approved" || disclosureInfo.status === "Rejected" || disclosureInfo.status === "Canceled" ) &&
            <SelectLabelInput
              title="Disclosure status"
              label="Supervisor's Action"
              error=""
              list={dropDown.supervisorAction}
              name="status"
              value={disclosureInfo.status}
              onChange={this.onChange}
              disabled={ disclosureInfo.status === "Complete" || disclosureInfo.status === "Approved" || disclosureInfo.status === "Rejected" || disclosureInfo.status === "Canceled" || dropped}
            />
          }
        </div>

        <Accordion multiple
          activeItem={[0, 1, 2, 3]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="I. Contributions made to officials of a municipal entity"
                actionButtons={this.actionButtons("contribMuniEntityByState", false, 0, allDisable, "Contributions made to officials of a municipal entity")}
              >
                {activeAccordions.includes(0) && (
                  <div>
                    {contribMuniEntityByState && contribMuniEntityByState.map((first, index) => {
                      const error = (errorMessages && errorMessages.contribMuniEntityByState && errorMessages.contribMuniEntityByState[index.toString()]) || {}
                      return (
                        <UserViewFirstSection
                          item={first}
                          key={index.toString()}
                          index={index}
                          onChangeItems={this.onChangeItems}
                          getCountryDetails={this.getCountryDetails}
                          length={contribMuniEntityByState && contribMuniEntityByState.length}
                          dropDown={dropDown || {}}
                          category="contribMuniEntityByState"
                          minDate={(quarterDate && quarterDate.start) || ""}
                          maxDate={(quarterDate && quarterDate.end) || ""}
                          quarter={disclosureInfo.quarter || ""}
                          year={disclosureInfo.year || ""}
                          onEdit={this.onEdit}
                          onSave={this.onSave}
                          onCancel={this.onCancel}
                          onRemove={this.onRemoveDoc}
                          onBlur={this.onBlur}
                          isEditable={isEditable}
                          allDisable={allDisable || dropped || false}
                          errors={error || {}}
                          tableTitle="Contributions Made To Officials Of A Municipal Entity"
                          issuerList={issuerList}
                        />
                      )
                    })}
                  </div>
                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="II. Payments made to political parties of states or subdivisions"
                actionButtons={this.actionButtons("paymentsToPartiesByState", false, 1, allDisable, "Payments made to political parties of states or subdivisions")}
              >
                {activeAccordions.includes(1) && (
                  <div>
                    {paymentsToPartiesByState && paymentsToPartiesByState.map((second, index) => {
                      const error = (errorMessages && errorMessages.paymentsToPartiesByState && errorMessages.paymentsToPartiesByState[index.toString()]) || {}
                      return(
                        <UserViewSecondSection
                          item={second}
                          key={index.toString()}
                          index={index}
                          onChangeItems={this.onChangeItems}
                          getCountryDetails={this.getCountryDetails}
                          length={paymentsToPartiesByState && paymentsToPartiesByState.length}
                          dropDown={dropDown || {}}
                          category="paymentsToPartiesByState"
                          minDate={(quarterDate && quarterDate.start) || ""}
                          maxDate={(quarterDate && quarterDate.end) || ""}
                          quarter={disclosureInfo.quarter || ""}
                          year={disclosureInfo.year || ""}
                          onEdit={this.onEdit}
                          onSave={this.onSave}
                          onCancel={this.onCancel}
                          onRemove={this.onRemoveDoc}
                          onBlur={this.onBlur}
                          isEditable={isEditable}
                          allDisable={allDisable || dropped || false}
                          errors={error || {}}
                          tableTitle="Payments Made To Political Parties Of States Or Subdivisions"
                        />
                      )
                    })}
                  </div>

                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(2)}
                title="III. Contributions made to bond ballot campaigns"
                actionButtons={this.actionButtons("ballotContribByState", false, 2, allDisable, "Contributions made to bond ballot campaigns")}
              >
                {activeAccordions.includes(2) && (
                  <div>
                    {ballotContribByState && ballotContribByState.map((third, index) => {
                      const error = (errorMessages && errorMessages.ballotContribByState && errorMessages.ballotContribByState[index.toString()]) || {}
                      return(
                        <UserViewThirdSection
                          item={third}
                          key={index.toString()}
                          index={index}
                          onChangeItems={this.onChangeItems}
                          getCountryDetails={this.getCountryDetails}
                          onDelete={this.onDelete}
                          length={ballotContribByState && ballotContribByState.length}
                          dropDown={dropDown || {}}
                          category="ballotContribByState"
                          minDate={(quarterDate && quarterDate.start) || ""}
                          maxDate={(quarterDate && quarterDate.end) || ""}
                          quarter={disclosureInfo.quarter || ""}
                          year={disclosureInfo.year || ""}
                          onEdit={this.onEdit}
                          onSave={this.onSave}
                          onCancel={this.onCancel}
                          onRemove={this.onRemoveDoc}
                          onBlur={this.onBlur}
                          svControls={svControls}
                          isEditable={isEditable}
                          allDisable={allDisable || dropped || false}
                          errors={error || {}}
                          tableTitle="Contributions Made To Bond Ballot Campaigns"
                          issuerList={issuerList}
                        />
                      )
                    })}
                  </div>

                )}
              </RatingSection>

              {disclosureId ?
                <DocumentPage
                  {...this.props}
                  tableStyle={{fontSize: "smaller"}}
                  isNotTransaction
                  tranId={nav2}
                  title="Related Documents"
                  pickCategory="LKUPDOCCATEGORIES"
                  pickSubCategory="LKUPCORRESPONDENCEDOCS"
                  pickAction="LKUPDOCACTION"
                  pickType="LKUPDOCTYPE"
                  category="contributionsRelatedDocuments"
                  staticField={staticField}
                  documents={documentsList || []}
                  isDisabled={!canEdit}
                  contextType={ContextType.supervisor.poliContributeRelated}
                  onSave={this.onDocSave}
                  onDeleteDoc={this.onDeleteDoc}
                  onStatusChange={this.onStatusChange}
                /> : null
              }
              <br/>

              <a className="button is-link is-small" onClick={() => this.setState({ supervisorNotesShow: !supervisorNotesShow })} >{`${supervisorNotesShow ? "Hide" : "Show"}`} All Notes</a>

              {supervisorNotesShow &&
              <table className="table is-striped is-fullwidth">
                <TableHeader cols={cols} />
                <tbody>
                  {
                    disclosureDetails && disclosureDetails.notes && disclosureDetails.notes.length ? disclosureDetails.notes.map((note, i) => (
                      <tr key={i}>
                        <td className="emmaTablesTd">
                          <small>{`${note && note.userName || ""} ${note && note.supervisor ? "(Supervisor)" : ""}`}</small>
                        </td>
                        <td className="emmaTablesTd" style={{ width: "70%" }}>
                          <small>{note && note.note}</small>
                        </td>
                        <td className="emmaTablesTd">
                          <small>{note && note.createdDate ? moment(note.createdDate).format("MM-DD-YYYY h:mm A") : ""}</small>
                        </td>
                      </tr>
                    )) :
                      <tr>
                        <td colSpan={3} style={{textAlign: "center"}}>No Data Found!</td>
                      </tr>
                  }
                </tbody>
              </table>
              }

              {
                canEdit || (/* svControls.supervisor && */ (disclosureId || disclosureInfo.disclosureFor === user.userId)) ?
                  <div className="columns">
                    <div className="column is-full">
                      <p className="multiExpLbl">Supervisor Notes/Instructions</p>
                      <div className="control">
                        <textarea className="textarea" value={supervisorNote || ""} name="supervisorNote"  onChange={(e) => this.onChangeItem(e.target.value, e.target.name)}/>
                      </div>
                      <small className="has-text-danger">{noteError || ""}</small>
                    </div>
                  </div> : null
              }

              <div className="column is-full">
                <div className="field is-grouped">
                  <div className="control">
                    {
                      <button className="button is-link is-small" onClick={() => this.onDisclosureSave(false)} disabled={!((canEdit && disclosureId) || (disclosureId && (disclosureId || disclosureInfo.disclosureFor === user.userId)) || dropped)}>Save</button>
                    }
                  </div>
                  {
                    isDisclose ?
                      <div className="control">
                        { disclosureDetails.status === "Pending" && navStatus === "Pre-approval" ?
                          <button className="button is-link is-small" disabled={isSaveDisabled.details || !isMakeDisclose || dropped || false}
                            onClick={() => this.onMakeDisclosureSave("Pre-approval")}>Send for Pre-approval</button>
                          : !flag && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved") &&
                          <button className="button is-link is-small" disabled={!isMakeDisclose || dropped || false}
                            onClick={() => this.onMakeDisclosureSave("Complete")} >Make disclosure</button> }
                      </div> : null
                  }
                </div>
              </div>

              {
                submitAudit &&
                <RatingSection onAccordion={() => onAccordion(3)} title="Activity Log" style={{overflowY: "scroll", fontSize: "smaller"}}>
                  {activeAccordions.includes(3) &&
                  <Audit auditLogs={auditLogs || []}/>
                  }
                </RatingSection>
              }


              <hr/>
              <Disclaimer/>

            </div>
          }
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

const WrappedComponent = withAuditLogs(Detail)
export default connect(mapStateToProps, null)(WrappedComponent)
