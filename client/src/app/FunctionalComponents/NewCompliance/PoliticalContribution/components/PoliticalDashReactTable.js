import React, {Component} from "react"
import connect from "react-redux/es/connect/connect"
import ReactTable from "react-table"
import Loader from "Global/Loader"
import moment from "moment"
import "react-table/react-table.css"
import {Link} from "react-router-dom"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import {fetchAllPoliticalDisclosure} from "../../../../StateManagement/actions/Supervisor"
import {checkSupervisorControls} from "../../../../StateManagement/actions/CreateTransaction"

class PoliticalDashReactTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      disclosures: []
    }
  }

  async componentWillMount() {
    const {svControls} = this.props
    const res = await fetchAllPoliticalDisclosure(svControls && svControls.supervisor ? "?type=supervisor" : "")
    const tabs = await checkSupervisorControls()
    this.setState({
      tabs,
      disclosures: res || [],
      loading: false,
    })
  }

  render(){
    const { disclosures, tabs } = this.state
    const nav3 = tabs && tabs.supervisor ? "supervisor-view" : "user-view"
    const nav4 = tabs && tabs.supervisor ? "sub-detail" : "detail"
    const pageSizeOptions = [5, 10, 20, 50, 100]
    const columns = [
      {
        id: "controlName",
        Header: "Control Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {
                  item && item.controlName || ""
                }
              </div>
              {item && item.controlEffectiveDate && <small>action date: {moment(item.controlEffectiveDate).format("MM-DD-YYYY") } </small>}
            </div>
          )
        },
        sortMethod: (a, b) => a.year - b.year
      },
      /* {
        Header: "Reference Notes",
        id: "notes",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                item &&  item.G37Obligation ? item && item.affirmationNotes || "--" :
                  item && item.notes && item.notes.length ? item.notes && item.notes[item.notes.length - 1] && item.notes[item.notes.length - 1].note : "--" || "--"
              }
            </div>
          )
        },
        sortMethod: (a, b) => {
          if(a.G37Obligation){
            a.affirmationNotes.localeCompare(b.affirmationNotes)
          } else {
            const t1 = a.notes[a.notes.length-1] && a.notes[a.notes.length-1].note.toLowerCase() || ""
            const t2 = b.notes[b.notes.length-1] && b.notes[b.notes.length-1].note.toLowerCase() || ""
            if (t1 < t2) { return -1 }
            if (t1 > t2) { return 1 }
            return 0
          }
        }
      }, */
      {
        id: "year",
        Header: "Report Year",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                item && item.year || "--"
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.year - b.year
      },
      {
        id: "quarter",
        Header: "Report Quarter",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                item && item.quarter || "--"
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.quarter - b.quarter
      },
      /* {
        id: "submitter",
        Header: "Submitter",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                item && item.submitter || "--"
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.submitter.localeCompare(b.submitter)
      }, */
      {
        id: "disclosureFor",
        Header: "Disclosure For",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                `${item.userFirstName} ${item.userLastName}` || "--"
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.userFirstName.localeCompare(b.userFirstName)
      },
      {
        id: "preApprovalSought",
        Header: "Pre-approval Sought?",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                (item && item.discloseType === "Pre") ? "Yes" : "No" || ""
              }
            </div>
          )
        },
        sortMethod: (a, b) => (a.discloseType === "Pre" ? "Yes" : "No").localeCompare(b.discloseType === "Pre" ? "Yes" : "No")
      },
      {
        id: "actionBy",
        Header: "Action By",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                (item && item.actionBy) || "NA"
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.actionBy || "NA".localeCompare(b.actionBy || "NA")
      },
      {
        id: "preApprovalSoughtStatus",
        Header: "Pre-approval Status?",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                (item && item.discloseType === "Pre") ? (item && item.status === "Complete") ? "Approved" : item.status === "Dropped" ? "NA" : item.status : "NA" ||  ""
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.status.localeCompare(b.status)
      },
      {
        id: "disObligation",
        Header: "Disclosure Obligation",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                (item && item.discloseType === "Pre") ? (item && (item.status === "Approved" || item.status === "Pre-approval" || item.status === "Pending")) ? "Pending" :
                  (item && item.status === "Complete") ? item.status : item.status === "Dropped" ? item.status : "NA" : item.status
              }
            </div>
          )
        },
        sortMethod: (a, b) => a.status.localeCompare(b.status)
      },
      {
        id: "detail",
        Header: "Detail",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const toFastTrack = `/compliance/cmp-sup-political/${nav3}/fastTrackDetail?disclosure=${item._id}`
          const toCAC = `/compliance/cmp-sup-political/instructions?disclosure=${item._id}`
          const disObligation = item && item.preApprovalSought === "Yes" ? (item.status === "Rejected" || item.status === "Canceled" ) ? "NA" : item.status === "Approved" ?
            "Complete" : (item.status === "Pending" || item.status === "Pre-approval") ? "" : item && item.status : item && item.status || "NA"
          const link = (item && item.discloseType === "Fast") ? "fastTrackDetail?" : (item && item.discloseType === "Pre") ? `${nav4}?status=Pre-approval&` :
            (item && item.discloseType === "Record") ? `${nav4}?status=Record&` : ""
          const typeLink = `/compliance/cmp-sup-political/${nav3}/${link}disclosure=${item._id}`
          return (
            <div className="hpTablesTd wrap-cell-text">
              {(item.status === "Dropped" && item && item.discloseType === undefined) ? <span>NA</span> :
                <Link
                  to={item && item.cac ? item && item.discloseType ? `${typeLink}` : toCAC : item && item.G37Obligation ? toFastTrack : typeLink}>
                  {
                    (disObligation === "Pending") ? "Click here" : (!disObligation && item.status === "Pre-approval") ? "Read only" : !disObligation ? "Click here" : "Read only"
                  }
                </Link>
              }
            </div>
          )
        },
      }
    ]

    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }
    return(
      <div>
        <Accordion multiple activeItem={[0]} boxHidden render={({activeAccordions, onAccordion}) =>
          <RatingSection onAccordion={() => onAccordion(0)} title="G-37 Political Contributions & Prohibitions">
            {activeAccordions.includes(0) &&
            <ReactTable
              columns={columns}
              data={disclosures}
              showPaginationBottom
              defaultPageSize={10}
              pageSizeOptions={pageSizeOptions}
              className="-striped -highlight is-bordered"
              style={{ overflowX: "auto" }}
              showPageJump
              minRows={2}
            />
            }
          </RatingSection>
        }/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(mapStateToProps, null)(PoliticalDashReactTable)
