import React from "react"
import moment from "moment"
import Select from "react-select"
import ReactTable from "react-table"
import "react-table/react-table.css"
import { TextLabelInput } from "../../../../GlobalComponents/TextViewBox"
import { MultiSelect } from "./PoliticalTextViewBox"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const PoliticalSummaryReactTable = ({
  disclosuresList,
  onCheckSelect,
  onBlur,
  onAssignedSelect,
  dropDown,
  municipalSecuritiesBusiness,
  municipalAdvisoryBusiness,
  ballotApprovedOfferings,
  contribMuniEntityByState,
  paymentsToPartiesByState,
  ballotContribByState,
  ballotContribByStateReimbursement,
  onSearchText,
  onSave,
  keys,
  name,
  subHeader,
  pageSizeOptions = [5, 10, 20, 50, 100],
  securitiesSearch,
  advisorySearch,
  ballotSearch,
  xlDownload,
  header,
}) => {
  let columns = []
  if(contribMuniEntityByState){
    columns = [
      {
        id: "state",
        Header: "State",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.state || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.state.localeCompare(b.state)
      },
      {
        id: "entityOfficial",
        Header: "Municipal Entity Official",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.nameOfMuniEntity || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.nameOfMuniEntity.localeCompare(b.nameOfMuniEntity)
      },
      {
        id: "amount",
        Header: "Contribution Amount ($)",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { `$${item && Number(item.contributionAmount).toLocaleString() }` || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributionAmount - b.contributionAmount
      },
      {
        id: "state",
        Header: "City/County/State or Other Political Subdivision",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { (item && `${item && item.city || "-"}/${item && item.county || "-"}/${item && item.state || "-"}`) || item && item.otherPoliticalSubdiv || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.city.localeCompare(b.city)
      },
      {
        id: "category",
        Header: "Contributor Category",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.contributorCategory || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributorCategory.localeCompare(b.contributorCategory)
      }
    ]
  } else if(paymentsToPartiesByState){
    columns = [
      {
        id: "state",
        Header: "State",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.state || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.state.localeCompare(b.state)
      },
      {
        id: "party",
        Header: "Political Party",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.politicalParty || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.politicalParty.localeCompare(b.politicalParty)
      },
      {
        id: "amount",
        Header: "Payment Amount ($)",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { `$${item && Number(item.contributionAmount).toLocaleString() }` || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributionAmount - b.contributionAmount
      },
      {
        id: "category",
        Header: "Contributor Category",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.contributorCategory || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributorCategory.localeCompare(b.contributorCategory)
      }
    ]
  } else if(ballotContribByState){
    columns = [
      {
        id: "state",
        Header: "State",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.state || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.state.localeCompare(b.state)
      },
      {
        id: "ballotName",
        Header: "Name of the Bond Ballot Campaign",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.nameOfBondBallot || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.nameOfBondBallot.localeCompare(b.nameOfBondBallot)
      },
      {
        id: "city",
        Header: "City/County/State or Other Political Subdivision",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { (item && `${item && item.city || "-"}/${item && item.county || "-"}/${item && item.state || "-"}`) || item && item.otherPoliticalSubdiv || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.city.localeCompare(b.city)
      },
      {
        id: "muniAuthority",
        Header: "Name of Issuing Municipal Authority",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.nameOfIssueMuniEntity || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.nameOfIssueMuniEntity.localeCompare(b.nameOfIssueMuniEntity)
      },
      {
        id: "amount",
        Header: "Contribution Amount ($)",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { `$${item && Number(item.contributionAmount).toLocaleString() }` || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributionAmount - b.contributionAmount
      },
      {
        id: "category",
        Header: "Contributor Category",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.contributorCategory || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributorCategory.localeCompare(b.contributorCategory)
      },
      {
        id: "date",
        Header: "Contribution Date",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.contributionDate ? moment(item.contributionDate).format("MM-DD-YYYY") : "" || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.contributionDate.localeCompare(b.contributionDate)
      }
    ]
  } else if(ballotContribByStateReimbursement){
    columns = [
      {
        id: "thirdPartyName",
        Header: "Third Party Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item && item.reimbursements || "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.reimbursements.localeCompare(b.reimbursements)
      },
      {
        id: "amount",
        Header: "Contribution Amount ($)",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              { item.reimbursementsAmount ? `$${item && Number(item.reimbursementsAmount).toLocaleString() }` : "--" }
            </div>
          )
        },
        sortMethod: (a, b) => a.reimbursementsAmount - b.reimbursementsAmount
      },
    ]
  } else if(municipalSecuritiesBusiness){
    columns = [
      {
        Header: "Select",
        id: "select",
        className: "multiExpTblVal",
        width: 100,
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <label className="checkbox-button">
                <input
                  type="checkbox"
                  name="selectedId"
                  checked={item.selectedId === item._id}
                  onChange={(e) => onCheckSelect(e, "municipalSecuritiesBusiness", row.index, item._id)}
                  onBlur={(e) => onBlur("Municipal Securities Business", item.entityName, "municipalAdvisoryBusiness", e)}
                /><span className="checkmark"/>
              </label>
            </div>
          )
        },
      },
      {
        id: "state",
        Header: "State",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(item && item.state || "--", securitiesSearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.state.localeCompare(b.state)
      },
      {
        id: "entityName",
        Header: "Municipal Entity Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch( item && item.entityName || "--" , securitiesSearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
      },
      {
        id: "city",
        Header: "City/County",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(`${item && item.city}/United States` || "-", securitiesSearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.city.localeCompare(b.city)
      },
      {
        id: "businessType",
        Header: "Municipal Securities Business Type",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const security = dropDown.securitiesBusiness.map(sec => ({id: sec.id, name: sec.name, label: sec.name})) || []
          return (
            <div className="hpTablesTd wrap-cell-text">
              {/* <MultiSelect
                filter
                selectProps={{autoFocus: false}}
                data={dropDown.securitiesBusiness}
                value={item && item.securitiesBusiness || []}
                onChange={item => onAssignedSelect("municipalSecuritiesBusiness", item, "securitiesBusiness", row.index)}
              /> */}
              <Select
                isMulti
                multi={true}
                searchable={true}
                closeOnSelect={false}
                value={item && item.securitiesBusiness || []}
                onChange={item => onAssignedSelect("municipalSecuritiesBusiness", item, "securitiesBusiness", row.index)}
                options={security}
                valueKey="id"
                className="basic-multi-select"
                classNamePrefix="select"
              />
            </div>
          )
        },
      }
    ]
  } else if(municipalAdvisoryBusiness){
    columns = [
      {
        Header: "Select",
        id: "select",
        className: "multiExpTblVal",
        width: 100,
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <label className="checkbox-button">
                <input
                  type="checkbox"
                  name="selectedId"
                  checked={item.selectedId === item._id}
                  onChange={(e) => onCheckSelect(e, "municipalAdvisoryBusiness", row.index, item._id)}
                  onBlur={(e) => onBlur("Municipal Advisory Business", item.entityName, "municipalAdvisoryBusiness", e)}
                /><span className="checkmark"/>
              </label>
            </div>
          )
        },
      },
      {
        id: "state",
        Header: "State",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(item && item.state || "--", advisorySearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.state.localeCompare(b.state)
      },
      {
        id: "entityName",
        Header: "Municipal Entity Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch( item && item.entityName || "--" , advisorySearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
      },
      {
        id: "city",
        Header: "City/County",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(`${item && item.city}/United States` || "-", advisorySearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.city.localeCompare(b.city)
      },
      {
        id: "advisoryBusinessType",
        Header: "Municipal Advisory Business Type",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const advisory = dropDown.advisoryBusiness.map(sec => ({id: sec.id, name: sec.name, label: sec.name})) || []
          return (
            <div className="hpTablesTd wrap-cell-text">
              {/* <MultiSelect
                filter
                selectProps={{autoFocus: false}}
                data={dropDown.advisoryBusiness}
                value={item && item.advisoryBusiness || []}
                onChange={item => onAssignedSelect("municipalAdvisoryBusiness", item, "advisoryBusiness", row.index)}
              /> */}
              <Select
                isMulti
                multi={true}
                searchable={true}
                closeOnSelect={false}
                name="advisoryBusiness"
                value={item && item.advisoryBusiness || []}
                onChange={item => onAssignedSelect("municipalAdvisoryBusiness", item, "advisoryBusiness", row.index)}
                options={advisory}
                valueKey="id"
                className="basic-multi-select"
                classNamePrefix="select"
              />
            </div>
          )
        },
      },
      {
        id: "thirdPartyName",
        Header: "Name of Third Party on Behalf of which Business was Solicited",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <TextLabelInput
                name="thirdPartyName"
                title="Name of Third Party on Behalf of which Business was Solicited"
                value={(item && item.thirdPartyName) || ""}
                onChange={(e) => onCheckSelect(e, "municipalAdvisoryBusiness", row.index)}
                onBlur={(e) => onBlur("Municipal Advisory Business", item.entityName, "municipalAdvisoryBusiness", e)}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.thirdPartyName - b.thirdPartyName
      },
      {
        id: "SolicitedBusiness",
        Header: "Nature of the Solicited Business",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const business = dropDown.solicitedBusiness.map(sec => ({id: sec.id, name: sec.name, label: sec.name})) || []
          return (
            <div className="hpTablesTd wrap-cell-text">
              {/* <MultiSelect
                filter
                selectProps={{autoFocus: false}}
                data={dropDown.solicitedBusiness}
                value={item && item.solicitedBusiness || []}
                onChange={item => onAssignedSelect("municipalAdvisoryBusiness", item, "solicitedBusiness", row.index)}
              /> */}
              <Select
                isMulti
                multi={true}
                searchable={true}
                closeOnSelect={false}
                name="solicitedBusiness"
                value={item && item.solicitedBusiness || []}
                onChange={item => onAssignedSelect("municipalAdvisoryBusiness", item, "solicitedBusiness", row.index)}
                options={business}
                valueKey="id"
                className="basic-multi-select"
                classNamePrefix="select"
              />
            </div>
          )
        },
      }

    ]
  } else if(ballotApprovedOfferings){
    columns = [
      {
        Header: "Select",
        id: "select",
        className: "multiExpTblVal",
        width: 100,
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <label className="checkbox-button">
                <input
                  type="checkbox"
                  name="selectedId"
                  checked={item.selectedId === item._id}
                  onChange={(e) => onCheckSelect(e, "ballotApprovedOfferings", row.index, item._id)}
                  onBlur={(e) => onBlur("Ballot-Approved Offerings", item.entityName, "ballotApprovedOfferings", e)}
                /><span className="checkmark"/>
              </label>
            </div>
          )
        },
        sortMethod: (a, b) => `${a.userFirstName} ${a.userLastName}` - `${b.userFirstName} ${b.userLastName}`
      },
      {
        id: "entityName",
        Header: "Municipal Entity Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch( item && item.entityName || "--" , ballotSearch)
                }}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
      },
      {
        id: "description",
        Header: "Full Issue Description",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: highlightSearch("Zone Development Authority Tax Revenue Bonds, Series 2018" || "-", ballotSearch)
                }}
              />
            </div>
          )
        },
      },
      {
        id: "reportDate",
        Header: "Reportable Date of Selection",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <TextLabelInput
                type="date"
                name="reportableDate"
                title="Reportable Date of Selection"
                // value={item.reportableDate ? moment(item.reportableDate).format("YYYY-MM-DD") : ""}
                value={(item.reportableDate === "" || !item.reportableDate) ? null : new Date(item.reportableDate)}
                onChange={(e) => onCheckSelect(e, "ballotApprovedOfferings", row.index)}
                onBlur={(e) => onBlur("Ballot-Approved Offerings", item.entityName, "ballotApprovedOfferings", e)}
              />
            </div>
          )
        },
        sortMethod: (a, b) => a.reportableDate - b.reportableDate
      },
    ]
  }

  return (
    <Accordion multiple
      activeItem={[0, 1, 2, 3]}
      boxHidden
      render={({activeAccordions, onAccordion}) =>
        <div>
          <RatingSection
            onAccordion={() => onAccordion(1)}
            title={header || ""}
            headerStyle={{color: "#f29718"}}
          >
            {activeAccordions.includes(1) && (
              <div className="column">
                <p className="columns">
                  <small>{subHeader || ""}</small>
                </p>
                { subHeader ?
                  <div className="columns">
                    <div className="column control" onClick={() => xlDownload(keys)}>
                      <span className="has-link"><i className="far fa-2x fa-file-excel has-text-link"/></span>
                    </div>
                    <div className="column is-half">
                      <p className="control has-icons-left">
                        <input className="input is-small is-link" type="text" name={name || ""} placeholder="search" onChange={(e) => onSearchText(e, keys)} />
                        <span className="icon is-left has-background-dark">
                          <i className="fas fa-search" />
                        </span>
                      </p>
                    </div>
                  </div> : null
                }

                <ReactTable
                  columns={columns}
                  data={disclosuresList}
                  showPaginationBottom
                  defaultPageSize={5}
                  pageSizeOptions={pageSizeOptions}
                  className="-striped -highlight is-bordered"
                  style={{ overflowX: "auto" }}
                  showPageJump
                  minRows={2}
                />
                <div>
                  { subHeader ?
                    <div className="column">
                      <div className="columns is-centered">
                        <button className="button is-link is-small" onClick={() => onSave(keys)}>Save</button>
                      </div>
                    </div> : null
                  }
                </div>
              </div>
            )}
          </RatingSection>
        </div>
      }
    />
  )
}

export default PoliticalSummaryReactTable
