import React from "react"
import moment from "moment"
import {NumberInput, SelectLabelInput, TextLabelInput} from "./PoliticalTextViewBox"
import SearchCountry from "../../../GoogleAddressForm/GoogleCountryComponent";
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear"

const UserViewFirstSection = ({getCountryDetails, svControls, item, index, onChangeItems, category, dropDown, length, minDate, maxDate, onEdit, onSave, isEditable, tableTitle, onCancel, onRemove, isSaveDisabled, errors, allDisable, onBlur, issuerList, quarter, year}) => {
  const onChange = (event) => {
    if (event._id) {
      onChangeItems({
        ...item,
        "nameOfMuniEntity": event.name,
        "nameOfMuniEntityID": event.id
      }, category, index)
    } else {
      onChangeItems({
        ...item,
        [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value
      }, category, index)
    }
  }

  const onClear = (event) => {
    onChangeItems({
      ...item,
      "nameOfMuniEntity": event.name,
      "nameOfMuniEntityID": event.id
    }, category, index)
  }

  const onBlurInput = event => {
    if (event === item.nameOfMuniEntity) {
      onBlur(`${tableTitle} In Name of Municipal Entity Official change to ${item.nameOfMuniEntity}`, category)
    } else if (event.target.title && event.target.value) {
      onBlur(`${tableTitle} In ${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`,category)
    }
  }

  const required = <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>

  isEditable = (isEditable[category] === index)

  return(
    <div>
      <div className="columns">
        <div className="column">
          <label className="checkbox-button multiExpLbl">
            Check here if none
            <input
              type="checkbox"
              title="Check here if none"
              name="checkNone"
              checked={(item && item.checkNone) || false}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={allDisable || !isEditable || false}
            />
            <span className="checkmark"/>
          </label>
        </div>
        { allDisable ? null :
          <div className="column">
            <div className="field is-grouped" style={{marginBottom: 0}}>
              <div className="control">
                <a
                  onClick={isEditable ? () => onSave(category, item, index, tableTitle) : () => onEdit(category, index, tableTitle)}
                  className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="far fa-save"/> : <i className="fas fa-pencil-alt"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a
                  onClick={isEditable ? () => onCancel(category) : () => onRemove(item._id, category, index, tableTitle)}
                  className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="fa fa-times"/> : <i className="far fa-trash-alt"/>}
                  </span>
                </a>
              </div>
            </div>
          </div>
        }
      </div>

      {item && !item.checkNone ?
        <div>
          <div className="columns">
            <div className="column">
              <p className="multiExpLbl">Name of Municipal Entity Official{required}</p>
              {/* <TextLabelInput
                name="nameOfMuniEntity"
                title="Name of Municipal Entity Official"
                value={(item && item.nameOfMuniEntity) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.nameOfMuniEntity) || ""}
              /> */}
              <DropDownListClear
                filter="contains"
                name="nameOfMuniEntity"
                value={(item && item.nameOfMuniEntity) || ""}
                data={issuerList || []}
                message="select issuer"
                textField="name"
                valueField="id"
                key="name"
                groupBy={({ relType }) => relType}
                onChange={(e) => onChange(e)}
                onBlur={() => onBlurInput(item.nameOfMuniEntity)}
                disabled={allDisable || !isEditable || false}
                isHideButton={item.nameOfMuniEntity && isEditable}
                onClear={() => {onClear({id: "", name: ""})}}
              />
              {errors && errors.nameOfMuniEntity && (
                <small className="text-error">{errors && errors.nameOfMuniEntity}</small>
              )}
            </div>
            <div className="column">
              <p className="multiExpLbl">Title of Municipal Entity Official</p>
              <TextLabelInput
                name="titleOfMuniEntity"
                title="Title of Municipal Entity Official"
                value={(item && item.titleOfMuniEntity) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.titleOfMuniEntity) || ""}
              />
            </div>
            <SearchCountry
              idx={`first-${index}`}
              required
              disabled={allDisable || !isEditable || false}
              value={(item.city && item.state && item.county) ? `${item.state}, ${item.city}, ${item.county}` :
                item.city ? `${item.city}` : item.state ? `${item.state}` :  item.county ? `${item.city}` : ""}
              label="State/City/County"
              error={(errors && errors.state) || ""}
              getCountryDetails={(e)=> getCountryDetails(e, category, index)}
            />
          </div>

          <div className="columns">
            {/* <div className="column">
              <p className="multiExpLbl">County</p>
              <TextLabelInput
                name="county"
                title="County"
                value={(item && item.county) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.county) || ""}
              />
            </div> */}
            <div className="column">
              <p className="multiExpLbl">Other Political Subdivision</p>
              <TextLabelInput
                name="otherPoliticalSubdiv"
                title="Other Political Subdivision"
                value={(item && item.otherPoliticalSubdiv) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.otherPoliticalSubdiv) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Contributor Category</p>
              <SelectLabelInput
                title="Contributor Category"
                list={dropDown && dropDown.contributionCategoryList || []}
                name="contributorCategory"
                value={(item && item.contributorCategory) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.contributorCategory) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Contribution Amount{required}</p>
              <NumberInput
                name="contributionAmount"
                title="Contribution Amount"
                value={(item && item.contributionAmount) || ""}
                prefix="$"
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.contributionAmount) ? "Required(must be larger than or equal to 0)" : ""  || ""}
              />
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <p className="multiExpLbl">Contribution Date{required}</p>
              <TextLabelInput
                type="date"
                name="contributionDate"
                title="Contribution Date"
                // value={item.contributionDate ? moment(item.contributionDate).format("YYYY-MM-DD") : ""}
                value={(item.contributionDate === "" || !item.contributionDate) ? null : new Date(item.contributionDate)}
                onChange={onChange}
                onBlur={onBlurInput}
                // min={minDate}
                // max={maxDate}
                disabled={allDisable || !isEditable || false}
                error={errors.contributionDate = item.contributionDate !== "" ?
                  (((moment(item.contributionDate || "").format("YYYY-MM-DD") >= moment(minDate).format("YYYY-MM-DD") && moment(item.contributionDate).format("YYYY-MM-DD") <= moment(maxDate).format("YYYY-MM-DD"))
                    ? (errors && errors.contributionDate) : `Selected Date is not in Quarter ${quarter || ""} Year ${year || ""}`)) : (errors && errors.contributionDate) || "" }
              />
            </div>
            <div className="column"/>
            <div className="column"/>
          </div>

          <hr/>

          <div className="columns">
            <div className="column">
              <small className="multiExpTblVal">For use in connection with automatic exemption from ban on municipal
                securities business or municipal advisory business only. If this contributions is the subject to an
                automatic exemption pursuant to Rule G-37(j), check the following Exempted box and provide the date of
                such automatic exemption.
              </small>
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <label className="checkbox-button multiExpLbl">
                Exempted
                <input
                  type="checkbox"
                  name="exempted"
                  title="Exempted"
                  checked={(item && item.exempted) || false}
                  onChange={onChange}
                  onBlur={onBlurInput}
                  disabled={allDisable || !isEditable || false}
                />
                <span className="checkmark"/>
              </label>
            </div>
            {item && item.exempted ?
              <div className="column">
                <p className="multiExpLbl">Date of exemption</p>
                <span>
                  <TextLabelInput
                    type="date"
                    name="dateOfExemption"
                    title="Date of exemption"
                    // value={item.dateOfExemption ? moment(item.dateOfExemption).format("YYYY-MM-DD") : ""}
                    value={(item.dateOfExemption === "" || !item.dateOfExemption) ? null : new Date(item.dateOfExemption)}
                    onChange={onChange}
                    onBlur={onBlurInput}
                    // min={minDate}
                    // max={maxDate}
                    disabled={allDisable || !isEditable || false}
                    error={errors.dateOfExemption = item.dateOfExemption !== "" ?
                      (((moment(item.dateOfExemption || "").format("YYYY-MM-DD") >= moment(minDate).format("YYYY-MM-DD") && moment(item.dateOfExemption).format("YYYY-MM-DD") <= moment(maxDate).format("YYYY-MM-DD"))
                        ? (errors && errors.dateOfExemption) : `Selected Date is not in Quarter ${quarter || ""} Year ${year || ""}`)) : (errors && errors.dateOfExemption) || "" }
                  />
                </span>
              </div> : null
            }
          </div>
        </div> : null
      }
      {length > 1 ? length - 1 === index ? null : <hr style={{border: "1px dotted"}}/> : null}
    </div>
  )
}
export default UserViewFirstSection
