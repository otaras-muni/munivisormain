import React from "react"
import moment from "moment"
import { Link } from "react-router-dom"
import {NumberInput, SelectLabelInput, TextLabelInput} from "./PoliticalTextViewBox"
import { DropdownList } from "react-widgets"
import SearchCountry from "../../../GoogleAddressForm/GoogleCountryComponent"
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear"

const UserViewThirdSection = ({ getCountryDetails, svControls, item, index, onChangeItems, category, dropDown, length, minDate, maxDate, onEdit, onSave, isEditable, tableTitle, onCancel, onRemove, isSaveDisabled, errors, allDisable, onBlur, issuerList, quarter, year}) => {
  const onChange = (event) => {
    if(event._id){
      onChangeItems({
        ...item,
        "nameOfIssueMuniEntity": event.name,
        "nameOfIssueMuniEntityID": event.id
      }, category, index)
    }else {
      onChangeItems({
        ...item,
        [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
      }, category, index)
    }
  }

  const onClear = (event) => {
    onChangeItems({
      ...item,
      "nameOfIssueMuniEntity": event.name,
      "nameOfIssueMuniEntityID": event.id
    }, category, index)
  }

  const onBlurInput = event => {
    if(event === item.nameOfIssueMuniEntity){
      onBlur(`${tableTitle} In Name of Issuing Municipal Entity change to ${item.nameOfIssueMuniEntity}`,category)
    }else if (event.target.title && event.target.value) {
      onBlur(`${tableTitle} In ${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`,category)
    }
  }

  const required = <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>

  isEditable = (isEditable[category] === index)

  return(
    <div>

      <div className="columns">
        <div className="column">
          <label className="checkbox-button multiExpLbl">
            Check here if none
            <input
              type="checkbox"
              title="Check here if none"
              name="checkNone"
              checked={(item && item.checkNone) || false}
              onChange={onChange}
              onBlur={onBlurInput}
              disabled={allDisable || !isEditable || false}
            />
            <span className="checkmark" />
          </label>
        </div>
        { svControls && svControls.supervisor ?
          <div className="column">
            <span>
              <p className="title" style={{fontSize: "medium"}}>Cannot find Municipal Entity or Official?</p>
            </span>
            <div className="columns">
              <div className="column">
                <button className="button is-text is-small">
                  <Link to="/addnew-entity" target="new">Add Municipal Entity to CRM
                    first</Link>
                </button>
              </div>
              <div className="column">
                <button className="button is-text is-small">
                  <Link to="/addnew-contact" target="new">Add Municipal Entity Official to CRM
                    next</Link>
                </button>
              </div>
            </div>
          </div>
          : null}
        {allDisable ? null :
          <div className="column">
            <div className="field is-grouped" style={{marginBottom: 0}}>
              <div className="control">
                <a
                  onClick={isEditable ? () => onSave(category, item, index, tableTitle) : () => onEdit(category, index, tableTitle)}
                  className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="far fa-save"/> : <i className="fas fa-pencil-alt"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a
                  onClick={isEditable ? () => onCancel(category) : () => onRemove(item._id, category, index, tableTitle)}
                  className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="fa fa-times"/> : <i className="far fa-trash-alt"/>}
                  </span>
                </a>
              </div>
            </div>
          </div>
        }
      </div>

      {item && !item.checkNone ?
        <div>
          <div className="columns">
            <div className="column">
              <p className="title" style={{fontSize: "medium"}}>A. Contributions</p>
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <p className="multiExpLbl">Name of Bond Ballot Campaign{required}</p>
              <TextLabelInput
                name="nameOfBondBallot"
                title="Name of Bond Ballot Campaign"
                value={(item && item.nameOfBondBallot) || ""}
                onChange={(e) => onChange(e)}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.nameOfBondBallot) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Name of Issuing Municipal Entity{required}</p>
              <DropDownListClear
                filter="contains"
                name="nameOfIssueMuniEntity"
                value={(item && item.nameOfIssueMuniEntity) || ""}
                data={issuerList || []}
                message="select issuer"
                textField="name"
                valueField="id"
                key="name"
                groupBy={({ relType }) => relType}
                onChange={(e) => onChange(e)}
                onBlur={() => onBlurInput(item.nameOfIssueMuniEntity)}
                disabled={allDisable || !isEditable || false}
                isHideButton={item.nameOfIssueMuniEntity && isEditable}
                onClear={() => {onClear({id: "", name: ""})}}
              />
              {errors && errors.nameOfIssueMuniEntity && (
                <small className="text-error">
                  {errors && errors.nameOfIssueMuniEntity}
                </small>
              )}
              {/* <TextLabelInput
                name="nameOfIssueMuniEntity"
                title="Name of Issuing Municipal Entity"
                value={(item && item.nameOfIssueMuniEntity) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.nameOfIssueMuniEntity) || ""}
              /> */}
            </div>
            <SearchCountry
              idx={`third-${index}`}
              required
              disabled={allDisable || !isEditable || false}
              value={(item.city && item.state && item.county) ? `${item.city}, ${item.state}, ${item.county}` :
                item.city ? `${item.city}` : item.state ? `${item.state}` :  item.county ? `${item.city}` : ""}
              label="State/City/County"
              error={(errors && errors.state) || ""}
              getCountryDetails={(e)=> getCountryDetails(e, category, index)}
            />
          </div>

          <div className="columns">
            {/* <div className="column">
              <p className="multiExpLbl">County</p>
              <TextLabelInput
                name="county"
                title="County"
                value={(item && item.county) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.county) || ""}
              />
            </div> */}
            <div className="column">
              <p className="multiExpLbl">Other Political Subdivision</p>
              <TextLabelInput
                name="otherPoliticalSubdiv"
                title="Other Political Subdivision"
                value={(item && item.otherPoliticalSubdiv) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.otherPoliticalSubdiv) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Contributor Category</p>
              <SelectLabelInput
                list={dropDown && dropDown.contributionCategoryList || []}
                name="contributorCategory"
                title="Contributor Category"
                value={(item && item.contributorCategory) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.contributorCategory) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Contribution Amount{required}</p>
              <NumberInput
                prefix="$"
                name="contributionAmount"
                title="Contribution Amount"
                value={(item && item.contributionAmount) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.contributionAmount) ? "Required(must be larger than or equal to 0)" : ""  || ""}
              />
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <p className="multiExpLbl">Contribution Date{required}</p>
              <TextLabelInput
                type="date"
                name="contributionDate"
                title="Contribution Date"
                // value={item.contributionDate ? moment(item.contributionDate).format("YYYY-MM-DD") : ""}
                value={(item.contributionDate === "" || !item.contributionDate) ? null : new Date(item.contributionDate)}
                onChange={onChange}
                onBlur={onBlurInput}
                // min={minDate}
                // max={maxDate}
                disabled={allDisable || !isEditable || false}
                error={errors.contributionDate = item.contributionDate !== "" ?
                  (((moment(item.contributionDate || "").format("YYYY-MM-DD") >= moment(minDate).format("YYYY-MM-DD") && moment(item.contributionDate).format("YYYY-MM-DD") <= moment(maxDate).format("YYYY-MM-DD"))
                    ? (errors && errors.contributionDate) : `Selected Date is not in Quarter ${quarter || ""} Year ${year || ""}`)) : (errors && errors.contributionDate) || "" }
              />
            </div>
            <div className="column"/>
            <div className="column"/>
          </div>

          <hr/>

          <div className="columns">
            <div className="column">
              <p className="title" style={{fontSize: "medium"}}>B. Reimbursement for Contributions</p>
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <small className="multiExpTblVal">List below any payments or reimbursements to any disclosed bond ballot
                ontributions, received by each dealer, municipal finance professional, non-MFP executive officer,
                municipal advisor, municipal advisor professional, or non-MAP executive officer from any third party,
                including the amount and the name of the party making such payments or reimbursements.
              </small>
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <p className="multiExpLbl">Name of third-party making payments or reimbursements</p>
              <TextLabelInput
                name="reimbursements"
                title="Name of third-party making payments or reimbursements"
                value={(item && item.reimbursements) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.reimbursements) || ""}
              />
            </div>
            <div className="column">
              <p className="multiExpLbl">Reimbursement Amount</p>
              <NumberInput
                prefix="$"
                name="reimbursementsAmount"
                title="Reimbursement Amount"
                value={(item && item.reimbursementsAmount) || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={allDisable || !isEditable || false}
                error={(errors && errors.reimbursementsAmount) ? "Required(must be larger than or equal to 0)" : ""  || ""}
              />
            </div>
          </div>
        </div> : null
      }
      {length > 1 ? length - 1 === index ? null : <hr style={{border: "1px dotted"}}/> : null}
    </div>
  )
}
export default UserViewThirdSection
