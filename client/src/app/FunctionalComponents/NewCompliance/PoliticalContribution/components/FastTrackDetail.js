import React, { Component } from "react"
import Loader from "Global/Loader"
import {DropdownList} from "react-widgets"
import { updateControlTaskStatus, getToken } from "GlobalUtils/helpers"
import moment from "moment"
import { toast } from "react-toastify"
import * as qs from "query-string"
import { fetchAssigned } from "../../../../StateManagement/actions/CreateTransaction/index"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import CONST from "../../../../../globalutilities/consts"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import {getPoliContributionDetailsById, postPoliContributions} from "../../../../StateManagement/actions/Supervisor/index"


class FastTrackDetail extends Component {

  constructor(props) {
    super(props)
    const yearOfDisclosureFirm = []
    const min = 2015
    const max = min + 10
    for (let i = min; i<=max; i++) { yearOfDisclosureFirm.push(i) }
    this.state = {
      disclosureInfo : {
        ...CONST.SuperVisor.disclosureInfo,
      } || {},
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      dropDown: {
        quarterOfDisclosureFirm: [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        yearOfDisclosureFirm,
        users: [],
      },
      disclosureDetails: {},
      disclosureId: "",
      contDetails: {},
      tempData: {},
      isSaveDisabled: false,
      loading:true,
    }
  }

  componentWillMount() {
    this.setState({
      loading: false,
    })
  }

  async componentDidMount() {
    const {currentYear, currentQuarter} = this.state
    const {user} = this.props
    const { svControls } = this.props
    const nav3 =  svControls.supervisor ? "supervisor-view" : "user-view"
    const nav4 =  svControls.supervisor ? "ind-submission" : "summary"
    const queryString = qs.parse(location.search)
    let res = []
    if(queryString && queryString.disclosure){
      const query = `?_id=${queryString.disclosure}`
      res = await getPoliContributionDetailsById(query)
    }
    const flag = (parseInt(queryString.year,10) > currentYear) || ((parseInt(queryString.quarter,10) > currentQuarter) && (parseInt(queryString.year,10) === currentYear))
    const year = queryString.year || currentYear
    const quarter = queryString.quarter || currentQuarter
    const disclosureFor = queryString.disclosureFor || user.userId || ""
    if(!flag){
      fetchAssigned(user.entityId, (users)=> {
        this.setState(prevState => ({
          disclosureInfo: {
            ...prevState.disclosureInfo,
            year: res && res.length && res[0] && res[0].year ? res[0].year : year,
            quarter: res && res.length && res[0] && res[0].quarter ? res[0].quarter : quarter,
            disclosureFor: res && res.length && res[0] && res[0].disclosureFor ? res[0].disclosureFor : disclosureFor,
            affirmationNotes: res && res.length && res[0] && res[0].affirmationNotes ? res[0].affirmationNotes : "",
            G37Obligation: res && res.length && res[0] && res[0].G37Obligation ? res[0].G37Obligation : false,
          },
          disclosureDetails: res && res.length ? res[0] : {},
          disclosureId: res && res.length && res[0] && res[0]._id ? res[0]._id : "",
          tempData: {
            affirmationNotes: res && res.length && res[0] && res[0].affirmationNotes ? res[0].affirmationNotes : "",
            G37Obligation: res && res.length && res[0] && res[0].G37Obligation ? res[0].G37Obligation : false
          },
          dropDown: {
            ...prevState.dropDown,
            users: users.usersList || [],
          }
        }))
      })
    }else {
      this.props.history.push(`/compliance/cmp-sup-political/${nav3}/${nav4}`)
    }
  }

  onChange = (e, inputName) => {
    if(inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
          // disclosureName: `${e.userFirstName} ${e.userLastName}`
        }
      })
    }else {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
        }
      })
    }
  }

  onSave = () => {
    const { disclosureInfo,disclosureId } = this.state
    const { svControls } = this.props
    const nav3 =  svControls.supervisor ? "supervisor-view" : "user-view"
    const nav4 =  svControls.supervisor ? "ind-submission" : "summary"
    // if(!disclosureInfo.affirmationNotes) return
    disclosureInfo.status = "Pending"
    disclosureInfo.discloseType = "Fast"
    disclosureInfo.noDisclosure = true
    if(disclosureId){
      disclosureInfo._id = disclosureId
    }
    this.setState({
      isSaveDisabled: true
    }, () => {
      postPoliContributions(disclosureInfo, async (res) => {
        if(res && res.status === 200) {
          toast("Affirmation and Submission Comments updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            disclosureId: res && res.data && res.data._id || "",
            isSaveDisabled: false,
            tempData: {
              affirmationNotes: res && res.data && res.data.affirmationNotes || "",
              G37Obligation: res && res.data && res.data.G37Obligation || false
            }
          },() => {
            // this.props.history.push(`/compliance/cmp-sup-political/${nav3}/${nav4}`)
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onMakeDisclosureSave = () => {
    const { disclosureInfo,disclosureId } = this.state
    disclosureInfo.status = "Complete"
    disclosureInfo.discloseType = "Fast"
    if(disclosureId){
      disclosureInfo._id = disclosureId
    }
    this.setState({
      isSaveDisabled: true
    }, () => {
      postPoliContributions(disclosureInfo, async (res) => {
        if(res && res.status === 200) {
          toast("Affirmation and Submission Comments updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          if (res && res.data && res.data.cac) {
            const taskId = res.data && res.data.taskRefId || ""
            try{
              await updateControlTaskStatus(getToken(), taskId)
            } catch (err) {
              console.log(err)
            }
          }
          this.setState({
            disclosureInfo: {
              ...this.state.disclosureInfo,
              year: res && res.data && res.data.year || "",
              quarter: res && res.data && res.data.quarter || "",
              disclosureFor: res && res.data && res.data.disclosureFor || "",
              affirmationNotes: res && res.data && res.data.affirmationNotes || "",
              G37Obligation: res && res.data && res.data.G37Obligation || false,
            },
            disclosureDetails: res && res.data && res.data || {},
            disclosureId: res && res.data && res.data._id || "",
            isSaveDisabled: false,
          },() => {
            // this.props.history.push(`/compliance/cmp-sup-political/${nav3}/${nav4}`)
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onCancel = () => {
    const { tempData } = this.state
    this.setState({
      disclosureInfo:{
        ...this.state.disclosureInfo,
        affirmationNotes: tempData && tempData.affirmationNotes || "",
        G37Obligation: tempData && tempData.G37Obligation || ""
      }
    })
  }

  render() {
    const { dropDown, disclosureInfo, currentQuarter, currentYear, contDetails, isSaveDisabled, disclosureId, disclosureDetails } = this.state
    const status = disclosureDetails && disclosureDetails.status === "Complete" || ""
    const { svControls, user } = this.props
    const flag = (parseInt(disclosureInfo.year,10) > currentYear) || ((parseInt(disclosureInfo.quarter,10) > currentQuarter) && (parseInt(disclosureInfo.year,10) === currentYear))
    let canEdit = true
    if (contDetails.status === "Complete") {
      canEdit = false
    }
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-37 Political Contributions and Prohibitions on Municipal Securities Business and Municipal Advisory Business</small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-37+Political+Contributions+and+Prohibitions+on+Municipal+Securities+Business+and+Municipal+Advisory+Business.pdf" target="new">
                <i className="fas fa-info-circle"/>
              </a>
            </p>
          </div>
        </div>

        <Accordion multiple
          activeItem={[0, 1]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Affirmation & Submission Comments"
              >
                {activeAccordions.includes(0) && (
                  <div>

                    <div className="columns">
                      <div className="column">
                        <p className="multiExpLbl ">Disclosure for</p>
                        <div className="control">
                          <div style={{width: "150px", fontSize: 12}}>
                            {svControls.supervisor && !disclosureId ?
                              <DropdownList filter value={disclosureInfo.disclosureFor || []} data={dropDown.users || []} textField="name" valueField="id" key="name" onChange={(user) => this.onChange(user, "disclosureFor")}  disabled={ svControls && !svControls.supervisor}/> :
                              disclosureId && status ? <p>{`${disclosureDetails.userFirstName} ${disclosureDetails.userLastName}`}</p> : <p>{`${user.userFirstName} ${user.userLastName}`}</p> }
                          </div>
                        </div>
                      </div>
                      <div className="column">
                        <p className="multiExpLbl">Report Quarter</p>
                        <div className="select is-small is-link">
                          <select name="quarter" value={disclosureInfo.quarter || ""} onChange={this.onChange} disabled={disclosureId}>
                            <option value="">Pick</option>
                            {dropDown.quarterOfDisclosureFirm.map((q) => <option key={q.value} value={q.value}>{q.name}</option>)}
                          </select>
                        </div>
                      </div>
                      <div className="column">
                        <p className="multiExpLbl">Report Year</p>
                        <div className="select is-small is-link">
                          <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange} disabled={disclosureId}>
                            <option value="">Pick</option>
                            {dropDown.yearOfDisclosureFirm.map((value) => <option key={value} value={value}>{value}</option>)}
                          </select>
                        </div>
                      </div>
                    </div>

                    <div>
                      <div className="columns">
                        <div className="column is-full">
                          <div className="control">
                            <textarea className="textarea" onChange={this.onChange} value={disclosureInfo.affirmationNotes} name="affirmationNotes" placeholder="" disabled={status || !canEdit || false}/>
                          </div>
                        </div>
                      </div>
                      <div className="columns">
                        <div className="column">
                          <label className="checkbox">
                            <p className="multiExpLbl">
                              <input type="checkbox" name="G37Obligation" checked={disclosureInfo.G37Obligation || false} onClick={this.onChange} onChange={this.onChange} disabled={status || !canEdit || false}/>I confirm that I have no disclosures to be made. I
                              confirm completion of my G-37 obligation for the period.
                            </p>
                          </label>
                        </div>
                      </div>
                    </div>

                    <br/>

                    {
                      canEdit && !status ?
                        <div className="columns is-centered">
                          <div className="field is-grouped">
                            <div className="column">
                              <button className="button is-link is-small" onClick={this.onSave} disabled={!disclosureInfo.G37Obligation || isSaveDisabled || false}>
                                Save
                              </button>
                            </div>
                            { !flag && disclosureId && disclosureInfo.G37Obligation ?
                              <div className="column">
                                <button className="button is-link is-small"
                                  onClick={() => this.onMakeDisclosureSave("Complete")}>Make disclosure
                                </button>
                              </div> : null
                            }
                            <div className="column">
                              <button className="button is-light is-small" onClick={this.onCancel}>
                                Cancel
                              </button>
                            </div>
                          </div>
                        </div> : null
                    }

                  </div>
                )}
              </RatingSection>
            </div>
          }
        />
        <hr/>
        <Disclaimer/>
      </div>
    )
  }
}

export default FastTrackDetail
