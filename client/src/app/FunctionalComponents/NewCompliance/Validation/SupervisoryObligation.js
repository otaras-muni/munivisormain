import Joi from "joi-browser"
import dateFormat from "dateformat"

const responsibleSchema = Joi.object().keys({
  userId: Joi.string().required(),
  name: Joi.string().required(),
  userEntityId: Joi.string().allow("").optional(),
  userFirstName: Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userPrimaryEmailId: Joi.string().allow("").optional(),
  userPrimaryPhone: Joi.string().allow("").optional(),
  userComplianceDesignation: Joi.string().required(),
  userRoleFirm: Joi.string().allow("").optional(),
  /* userActive: Joi.string().allow("").optional(), */
  startDate: Joi.date().example(new Date("2005-01-01")).allow("", null).optional(),
  endDate: Joi.date().example(new Date("2005-01-01")).min(Joi.ref("startDate")).allow("", null).optional(),
  _id: Joi.string().required().optional(),

})

export const ResponsibleSupervisoryValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, responsibleSchema, { abortEarly: false, stripUnknown:false })

const annualSchema = (minDate) => Joi.object().keys({
  certifiedBy: Joi.object({
    id: Joi.string().required().optional(),
    name: Joi.string().required().optional(),
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userEntityId: Joi.string().required(),
    userPrimaryEmailId: Joi.string().required(),
  }).required(),
  certifiedOn: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  certifierDesignation: Joi.string().required(),
  docFileName: Joi.string().required(),
  docNotes: Joi.string().allow("").optional(),
  docAWSFileLocation: Joi.string().required(),
  docCategory : Joi.string().required(),
  docSubCategory: Joi.string().required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const AnnualCertificationValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, annualSchema(minDate), { abortEarly: false, stripUnknown:false })
