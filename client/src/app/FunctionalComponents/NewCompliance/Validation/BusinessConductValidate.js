import Joi from "joi-browser"
import dateFormat from "dateformat"

const businessConductSchema = Joi.object().keys({
  /* userId: Joi.string().allow("").required(),
  userFirstName:Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userPrimaryEmailId: Joi.string().email().allow("").optional(),
  userPrimaryPhone: Joi.string().allow("").optional(), */
  violationType: Joi.string().required().label("Violation Type"),
  violataionDate: Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).allow(null).optional(),
  violationNotes: Joi.string().allow("").optional(),
  recordDate: Joi.date().allow([null,""]).optional(),
  closeDate: Joi.date().allow([null,""]).optional(),
  assPersonName: Joi.string().required(),
  docAWSFileLocation: Joi.string().allow("").optional(),
  docFileName: Joi.string().allow("").optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const BusinessConductValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, businessConductSchema, { abortEarly: false, stripUnknown:false })

