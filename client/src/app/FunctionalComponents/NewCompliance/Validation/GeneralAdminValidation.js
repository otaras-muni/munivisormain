import Joi from "joi-browser"
import dateFormat from "dateformat"

const designateFirmSchema = Joi.object().keys({
  userId: Joi.string().allow("").required(),
  name: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().regex(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/).required(),
  userPrimaryPhone: Joi.string().required(),
  userAddress: Joi.string().required(),
  userContactType: Joi.string().required(),
  _id: Joi.string().required().optional(),
})

export const PeopleDesignateValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, designateFirmSchema, { abortEarly: false, stripUnknown:false })

const qualifiedSchema = (minDate) => Joi.object().keys({
  qualification: Joi.string().required(),
  userId: Joi.string().allow("").required(),
  name: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().regex(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/).required(),
  userPrimaryPhone: Joi.string().required(),
  profFeePaidOn: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  series50PassDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  series50ValidityEndDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PeopleQualifiedValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, qualifiedSchema(minDate), { abortEarly: false, stripUnknown:false })

const businessActivitiesSchema = Joi.object().keys({
  activity: Joi.string().required(),
  subActivity: Joi.string().required(),
  notes: Joi.string().allow("").optional(),
  _id: Joi.string().required().optional(),
})

export const BusinessActivitiesValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, businessActivitiesSchema, { abortEarly: false, stripUnknown:false })

const firmRegisterSchema = Joi.object().keys({
  registeringBody: Joi.string().required(),
  regFeePaidOn: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  feeAmount: Joi.number().label("Fee Amount").min(0).required(),
  regValidTill: Joi.date().allow([null,""]).example(new Date("2005-01-01")).min(Joi.ref("regFeePaidOn")).optional(),
  // startDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  // endDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).min(Joi.ref("startDate")).allow("", null).optional(),
  _id: Joi.string().required().optional(),
})

export const FirmRegisterValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, firmRegisterSchema, { abortEarly: false, stripUnknown:false })

const firmInfoSchema = Joi.object().keys({
  field: Joi.string().required(),
  notes: Joi.string().required(),
  information: Joi.string().allow("").optional(),
  isField: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

export const GeneralFirmInfoValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, firmInfoSchema, { abortEarly: false, stripUnknown:false })
