import Joi from "joi-browser"
import dateFormat from "dateformat"

const giftExpensesSchema = () => Joi.object().keys({
  recipientEntityId: Joi.string().required(),
  recipientEntityName: Joi.string().required(),
  recipientUserId: Joi.string().required(),
  name: Joi.string().required(),
  recipientUserFirstName: Joi.string().required(),
  recipientUserLastName: Joi.string().required(),
  giftValue: Joi.number().min(0).required(),
  giftDate: Joi.date().allow([null,""]).example(new Date("2011-01-01")).allow("", null).optional(),
  createdUserId: Joi.string().allow("").optional(),
  createdUserName: Joi.string().allow("").optional(),
  createdDate: Joi.date().allow("").optional(),
  _id: Joi.string().optional(),
})

export const RecordGiftAndExpenses = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, giftExpensesSchema(minDate), { abortEarly: false, stripUnknown:false })
