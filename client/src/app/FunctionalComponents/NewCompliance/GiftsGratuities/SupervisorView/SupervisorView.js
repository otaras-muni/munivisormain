import React, { Component } from "react"
import Loader from "Global/Loader"
import {Link} from "react-router-dom";
import Summary from "../UserView/Summary"
import Detail from "../UserView/Detail"
import ReportFirm from "../SupervisorView/Report(firm)"
import FastTrackDetail from "../UserView/FastTrackDetail"

const tabs = [
  {
    title: <span>Individual Submissions</span>,
    value: 1,
    path: "summary",
  },
  {
    title: <span>Submissions Detail</span>,
    value: 2,
    path: "detail",
  },
  {
    title: <span>Report/Summary (firm)</span>,
    value: 3,
    path: "report",
  },
]

class SupervisorView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true
    }
  }

  componentWillMount() {
    const {nav4} = this.props
    if(nav4){
      if(nav4 === "fastTrackDetail"){
        tabs[1].path = "fastTrackDetail"
      } else {
        tabs[1].path = "detail"
      }
    } else {
      this.props.history.push("/compliance/cmp-sup-gifts/supervisor-view/summary")
    }
    this.setState({
      loading: false,
    })
  }

  renderTabs = (activeTab) => {
    return (
      <div className="tabs">
        <ul>
          {
            tabs.map(tab=> {
              const path = tab.path === "detail" || ""
              return (
                <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`}>
                  {
                    this.props.nav2 ? <Link to={`/compliance/cmp-sup-gifts/supervisor-view/${tab.path}${path ? "?status=Record" : ""}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                      <a role="button" className="tabSecLevel">
                        {tab.title}
                      </a>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }

  renderSelectedView = () => {
    const {svControls} = this.props
    switch(this.props.nav4) {
    case "summary" :
      return <Summary {...this.props} svControls={svControls}/>
    case "detail" :
      return <Detail {...this.props} svControls={svControls}/>
    case "fastTrackDetail" :
      return <FastTrackDetail {...this.props} svControls={svControls}/>
    case "report" :
      return <ReportFirm {...this.props} svControls={svControls}/>
    default:
      return "not found"
    }
  }

  render() {
    const activeTab = tabs.find(x => x && (x.path === this.props.nav4))
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        {this.props.nav2 && this.props.nav3 !== "managequemedia" && this.renderTabs(activeTab && activeTab.value) }
        <div >
          {this.renderSelectedView()}
        </div>
      </div>
    )
  }
}

export default SupervisorView
