import React, { Component } from "react"
import ReactTable from "react-table"
import {toast} from "react-toastify"
import moment from "moment"
import _ from "lodash"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import Loader from "Global/Loader"
import CONST from "../../../../../globalutilities/consts"
import { getGiftDisclosure } from "../../../../StateManagement/actions/Supervisor"
import {SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"


class ReportFirm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dropDown:{
        groupBy: ["Group by recipient", "Group by recipient's employer", "Group by giver"],
        sortBy: [{label: "Last month", included: true}, {label: "Last 3 months", included: true}, {label: "Last 6 months", included: true},
          {label: "Last 1 year", included: true}, {label: "All", included: true}],
      },
      sortByDisclosure: "",
      groupByDisclosure: "",
      jsonSheets: [],
      startXlDownload: false,
      loading:true,
    }
  }

  componentWillMount() {
    this.setState({
      loading: false,
      sortByDisclosure: "Last 6 months" || ""
    }, () => this.onSearch())
  }

  onChange = (e) => {
    const { name, value} = e.target
    this.setState({
      [name]: e.target.type === "checkbox" ? e.target.checked : value
    }, () => {
      if(name === "sortByDisclosure") {
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const {sortByDisclosure} = this.state
    let endDate = ""
    if (sortByDisclosure) {
      switch(sortByDisclosure) {
      case "Last month":
        endDate = moment().subtract(1, "months").date(1).format("MM-DD-YYYY")
        break
      case "Last 3 months":
        endDate = moment().subtract(3, "months").format("MM-DD-YYYY")
        break
      case "Last 6 months":
        endDate = moment().subtract(6, "months").format("MM-DD-YYYY")
        break
      case "Last 1 year":
        endDate = moment().subtract(12, "months").format("MM-DD-YYYY")
        break
      case "All":
        endDate = moment().subtract(24, "months").format("MM-DD-YYYY")
        break
      default:
        endDate = ""
        break
      }

      this.setState({
        loading: true
      }, () => {
        getGiftDisclosure(endDate, (res) => {
          if (res) {
            const data= []
            res && res.map(item => {
              if (item.giftsToRecipients) {
                item.giftsToRecipients.forEach(w => {
                  w.preApprovalDate = item.preApprovalDate
                  data.push(w)
                })
              }
            })

            this.setState({
              data: data || [],
              loading: false
            })
          } else {
            toast("something went wrong", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR})
            this.setState({
              loading: false
            })
          }
        })
      })
    }
  }

  pdfDownload = () => {
    const {data} = this.state
    const pdfGiftList = []
    data && data.forEach((item) => {
      pdfGiftList.push([
        item.createdUserName || "-",
        `${item.recipientUserFirstName} ${item.recipientUserLastName}` || "-",
        item.recipientEntityName || "-",
        item.giftValue ? `$${Number(item.giftValue).toLocaleString()}`: "-",
        item.giftDate !== null ? moment(item.giftDate).format("MM-DD-YYYY") : "-",
        moment(item.preApprovalDate).format("MM-DD-YYYY") || "-"
      ])
    })
    const tableData = []
    if(pdfGiftList.length > 0){
      tableData.push({
        titles:["Gift Report"],
        description: "",
        headers: ["Gift/expense made by","Recipient","Recipient's Employer","Value","Date gift/expense made","Disclosure/Affirmation Date"],
        rows: pdfGiftList
      })
    }
    pdfTableDownload("", tableData, "giftReport.pdf")
  }

  xlDownload = () => {
    const {data} = this.state
    const exlGiftReport = []

    data && data.forEach((item) => {
      const data = {
        "Gift/expense made by": item.createdUserName || "-",
        "Recipient": `${item.recipientUserFirstName} ${item.recipientUserLastName}` || "-",
        "Recipient's Employer": item.recipientEntityName || "-",
        "Value": item.giftValue ? `$${Number(item.giftValue).toLocaleString()}`: "-",
        "Date gift/expense made": item.giftDate !== null ? moment(item.giftDate).format("MM-DD-YYYY") : "-",
        "Disclosure/Affirmation Date": moment(item.preApprovalDate).format("MM-DD-YYYY") || "-"
      }
      exlGiftReport.push(data)
    })
    const jsonSheets = [{
      name: "Gift Report",
      headers: ["Gift/expense made by","Recipient","Recipient's Employer","Value","Date gift/expense made","Disclosure/Affirmation Date"],
      data: exlGiftReport,
    }]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  render() {
    const {dropDown, sortByDisclosure, groupByDisclosure, data} = this.state
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }
    const columns = [
      {
        id: "createdUserName",
        Header: "Gift/expense made by",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.createdUserName || "-") }} />
          )
        },
        sortMethod: (a, b) => (a.createdUserName || "").localeCompare(b.createdUserName)
      },
      {
        id: "recipient",
        Header: "Recipient",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (`${item.recipientUserFirstName} ${item.recipientUserLastName}` || "-") }} />
          )
        },
        sortMethod: (a, b) => (a.recipientUserFirstName || "").localeCompare(b.recipientUserFirstName)
      },
      {
        id: "recipientEntityName",
        Header: "Recipient's Employer",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.recipientEntityName || "-") }} />
          )
        },
        sortMethod: (a, b) => (a.recipientEntityName || "").localeCompare(b.recipientEntityName)
      },
      {
        id: "giftValue",
        Header: "Value",
        accessor: "giftValue",
        className: "multiExpTblVal",
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">{item ? `$${Number(item).toLocaleString()}`: "-"}</div>
          )
        },
        Footer: (
          <span>
            <strong>Total:</strong>{" $"}
            {Number(_.sum(_.map(data, d => d.giftValue))).toLocaleString()}
          </span>
        )
      },
      {
        id: "giftDate",
        Header: "Date gift/expense made",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          const data = item.giftDate !== null ? moment(item.giftDate).format("MM-DD-YYYY") : "-"
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (data) }} />
          )
        },
        sortMethod: (a, b) => (a.giftDate || "").localeCompare((b.giftDate || ""))
      },
      {
        id: "preApprovalDate",
        Header: "Disclosure/Affirmation Date",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (moment(item.preApprovalDate).format("MM-DD-YYYY") || "-") }} />
          )
        },
        sortMethod: (a, b) => (a.preApprovalDate || "").localeCompare((b.preApprovalDate || ""))
      }
    ]
    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance</small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-20+Gifts%2C+Gratuities%2C+Non-Cash+Compensation+and+Expenses+of+Issuance.pdf"
                target="new">
                <i className="fas fa-info-circle" />
              </a>
            </p>
          </div>
        </div>
        <div className="columns">
          <div className="column is-2">
            <SelectLabelInput title="Pick date range" error= "" list={dropDown.sortBy} name="sortByDisclosure" value={sortByDisclosure} onChange={this.onChange} />
          </div>
          {/* <div className="column is-2">
            <SelectLabelInput title="Group by" error= "" list={dropDown.groupBy} name="groupByDisclosure" value={groupByDisclosure} onChange={this.onChange} />
          </div> */}
        </div>
        {
          data && data.length ?
            <div className="columns">
              <div className="column">
                <div className="field is-grouped" style={{justifyContent: "flex-start"}}>
                  <div className="control" onClick={this.xlDownload}>
                    <span className="has-link"><i className="far fa-2x fa-file-excel has-text-link"/></span>
                  </div>
                  <div style={{ display: "none" }}>
                    <ExcelSaverMulti label="XML" startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
                  </div>
                  <div className="control" title="PDF" onClick={this.pdfDownload}>
                    <span className="has-link"><i className="far fa-2x fa-file-pdf has-text-link"/></span>
                  </div>
                </div>
              </div>
            </div> : null
        }

        <div>
          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  title="Report / Summary"
                >
                  {activeAccordions.includes(0) && (
                    <ReactTable
                      columns={columns}
                      data={data || []}
                      showPaginationBottom
                      defaultPageSize={10}
                      pageSizeOptions={[5, 10, 20, 50, 100]}
                      className="-striped -highlight is-bordered"
                      style={{ overflowX: "auto" }}
                      showPageJump
                      minRows={2}
                    />
                  )}
                </RatingSection>
              </div>
            )}
          />
        </div>
      </div>
    )
  }
}

export default ReportFirm
