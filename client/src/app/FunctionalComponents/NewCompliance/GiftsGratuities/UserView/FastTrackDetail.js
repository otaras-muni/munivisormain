import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import {DropdownList} from "react-widgets"
import moment from "moment"
import * as qs from "query-string"
import { connect } from "react-redux"
import Loader from "Global/Loader"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import {fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import { updateCACPoliticalActionStatus, getToken, updateControlTaskStatus } from "GlobalUtils/helpers"
import {getGiftDetail, postGiftsGratuitiesInfo} from "../../../../StateManagement/actions/Supervisor"
import CONST from "../../../../../globalutilities/consts"
import {SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"

class FastTrackDetail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      disclosureInfo: {
        ...CONST.SuperVisor.disclosureInfo,
        giftsAffirmCompletionOfG20ForPeriod: false,
      } || {},
      tempDisclosureInfo: {
        ...CONST.SuperVisor.disclosureInfo,
        giftsAffirmCompletionOfG20ForPeriod: false,
      } || {},
      disclosureDetails: {},
      loading: true,
      contDetails: {},
      isSaveDisabled: false,
    }
  }

  componentWillMount() {
    const queryString = qs.parse(location.search)
    const { currentYear, currentQuarter } = this.state
    const { user } = this.props
    const flag = (parseInt(queryString.year, 10) > currentYear) || ((parseInt(queryString.quarter, 10) > currentQuarter) && (parseInt(queryString.year, 10) === currentYear))
    const year = queryString.year || currentYear
    const quarter = queryString.quarter || currentQuarter
    const disclosureFor = queryString.disclosureFor || user.userId || ""

    const yearOfDisclosureFirm = []
    const min = 2015
    const max = min + 10
    for (let i = min; i <= max; i++) { yearOfDisclosureFirm.push(i) }

    if (!flag) {
      fetchAssigned(user.entityId, (users) => {
        this.setState(prevState => ({
          disclosureInfo: {
            year, quarter, disclosureFor,
          },
          tempDisclosureInfo: {
            year, quarter, disclosureFor,
            affirmationNotes: ""
          },
          dropDown: {
            ...prevState.dropDown,
            users: users.usersList || [],
            // quarterOfDisclosureFirm: [{ name: "First", value: 1 }, { name: "Second", value: 2 }, { name: "Third", value: 3 }, { name: "Fourth", value: 4 }],
            quarterOfDisclosureFirm: ["All"],
            yearOfDisclosureFirm,
          },
          loading: false,
        }), () => {
          this.onUserSearch()
        })
      })
    } else {
      this.props.history.push("/compliance/cmp-sup-gifts/user-view/summary")
    }
  }

  onUserSearch = () => {
    const queryString = qs.parse(location.search)
    const fetchId = queryString.id || queryString.disclosure || ""
    if (fetchId) {
      const query = `?id=${fetchId}`
      this.setState({
        loading: true
      }, () => {
        getGiftDetail(query, (res) => {
          if (res) {
            this.setState(prevState => ({
              disclosureInfo: {
                disclosureFor: res && res.userOrEntityId || "",
                quarter: res && res.quarter,
                year: res && res.year,
                affirmationNotes: res && res.affirmationNotes,
                giftsAffirmCompletionOfG20ForPeriod: res && res.giftsAffirmCompletionOfG20ForPeriod,
                status: typeof res.status !== "boolean" ? res.status || "" : prevState.status,
              },
              disclosureDetails: {
                userFirstName: res && res.userFirstName || "",
                userLastName: res && res.userLastName || "",
              },
              tempDisclosureInfo: {
                ...prevState.disclosureInfo,
                disclosureFor: res && `${res.userFirstName} ${res.userLastName}`,
                quarter: res && res.quarter,
                year: res && res.year,
                affirmationNotes: res && res.affirmationNotes,
                giftsAffirmCompletionOfG20ForPeriod: res && res.giftsAffirmCompletionOfG20ForPeriod,
                status: typeof res.status !== "boolean" ? res.status || "" : prevState.status,
              },
              contDetails: res || {},
              fetchId,
              loading: false,
            }))
          } else {
            toast("something went wrong", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
            this.setState({
              loading: false
            })
          }
        })
      })
    }
  }

  onChange = (e, inputName) => {
    if (inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
        }
      })
    } else {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
        }
      })
    }
  }

  onCancel = () => {
    this.setState({
      disclosureInfo: cloneDeep(this.state.tempDisclosureInfo)
    })
  }

  onSave = () => {
    const { disclosureInfo, fetchId } = this.state
    disclosureInfo.status = "Pending"
    disclosureInfo.quarter = "All"
    disclosureInfo.noDisclosure = true
    disclosureInfo.discloseType = "Fast"
    if(fetchId){
      disclosureInfo.fetchId = fetchId
    }
    // disclosureInfo.isPreApproval = "No"
    this.setState({
      isSaveDisabled: true
    }, () => {
      postGiftsGratuitiesInfo(disclosureInfo, async (res) => {
        toast("Affirmation and Submission Comments updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        if (res && res.status === 200) {
          this.setState({
            isSaveDisabled: false,
            fetchId: res && res.data && res.data._id || "",
            disclosureDetails: {
              userFirstName: res && res.data && res.data.userFirstName || "",
              userLastName: res && res.data && res.data.userLastName || "",
            },
            tempDisclosureInfo: disclosureInfo
          }, () => {
            // this.props.history.push(`/compliance/cmp-sup-gifts/${superVisor}/summary`)
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onMakeDisclosureSave = () => {
    const { disclosureInfo, fetchId } = this.state
    delete disclosureInfo.G37Obligation
    // delete disclosureInfo.affirmationNotes
    disclosureInfo.status = "Complete"
    disclosureInfo.discloseType = "Fast"
    disclosureInfo.preApprovalDate =  Date()

    this.setState({
      loading: true
    }, () => {
      postGiftsGratuitiesInfo({ ...disclosureInfo, fetchId },async (res) => {
        if (res && res.status === 200) {
          if (res && res.data && res.data.cac) {
            const taskId = res.data && res.data.taskRefId
            try{
              await updateControlTaskStatus(getToken(), taskId)
            } catch (err) {
              console.log(err)
            }
          }
          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            contDetails: res && res.data || {},
            loading: false,
          })
          // this.props.history.push(`/compliance/cmp-sup-gifts/${superVisor}/summary`)
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            loading: false,
          })
        }
      })
    })
  }

  render() {
    const {dropDown, disclosureInfo, currentYear, contDetails, isSaveDisabled, fetchId, disclosureDetails} = this.state
    const {user, svControls} = this.props
    const disabled = disclosureInfo && disclosureInfo.status === "Dropped"
    let canEdit = true
    const noDisclosure = contDetails && contDetails.hasOwnProperty("noDisclosure") ? contDetails.noDisclosure : true
    if (contDetails.status === "Complete") {
      canEdit = false
    }
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance</small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-20+Gifts%2C+Gratuities%2C+Non-Cash+Compensation+and+Expenses+of+Issuance.pdf" target="new">
                <i className="fas fa-info-circle"/>
              </a>
            </p>
          </div>
        </div>

        <Accordion multiple
          activeItem={[0, 1]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Affirmation & Submission Comments"
              >
                {activeAccordions.includes(0) && (
                  <div>

                    <div className="columns">
                      <div className="column">
                        <p className="multiExpLbl ">Disclosure for</p>
                        <div className="control">
                          <div style={{width: "150px", fontSize: 12}}>
                            {(svControls && svControls.supervisor && !fetchId) ?
                              <DropdownList filter value={disclosureInfo.disclosureFor || []} data={dropDown.users || []} textField="name" valueField="id" key="name" onChange={(user) => this.onChange(user, "disclosureFor")} />
                              : fetchId ? <span>{`${disclosureDetails.userFirstName} ${disclosureDetails.userLastName}` || []}</span> : <span>{`${user.userFirstName} ${user.userLastName}` || []}</span>}
                          </div>
                        </div>
                      </div>

                      <SelectLabelInput
                        label="Report Quarter"
                        list={dropDown.quarterOfDisclosureFirm}
                        name="quarter"
                        value="All"
                        disabled
                        onChange={this.onChange}
                      />

                      <div className="column">
                        <p className="multiExpLbl">Report Year</p>
                        <div className="select is-small is-link">
                          <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange} disabled={fetchId}>
                            <option value="">Pick</option>
                            {dropDown.yearOfDisclosureFirm.map((value) => <option key={value} value={value}>{value}</option>)}
                          </select>
                        </div>
                      </div>
                    </div>

                    {
                      noDisclosure ?
                        <div>
                          <div className="columns">
                            <div className="column is-full">
                              <div className="control">
                                <textarea className="textarea" onChange={this.onChange} value={disclosureInfo.affirmationNotes} name="affirmationNotes" placeholder="" disabled={!canEdit || disabled}/>
                              </div>
                            </div>
                          </div>
                          <div className="columns">
                            <div className="column">
                              <label className="checkbox">
                                <p className="multiExpLbl">
                                  <input type="checkbox" name="giftsAffirmCompletionOfG20ForPeriod" checked={disclosureInfo.giftsAffirmCompletionOfG20ForPeriod || false} onClick={this.onChange} onChange={this.onChange} disabled={!canEdit || disabled}/>
                                  I confirm that I have not made gifts or gratuities in relation to the municipal securities or municipal advisory activities. I have no disclosures to be made. I confirm completion of my G-20 obligation for the period.
                                </p>
                              </label>
                            </div>
                          </div>
                        </div> : <div className="title innerPgTitle"><strong> Disclosure already exists for this quarter  </strong> </div>
                    }

                    <br/>

                    {
                      canEdit && noDisclosure && !disabled &&
                      <div className="columns is-centered">
                        <div className="field is-grouped">
                          <div className="column control">
                            <button className="button is-link is-small" onClick={this.onSave}
                              disabled={!disclosureInfo.giftsAffirmCompletionOfG20ForPeriod || isSaveDisabled || false}>Save
                            </button>
                          </div>
                          { (parseInt(currentYear,10) >= disclosureInfo.year) && fetchId ?
                            <div className="column control">
                              <button className="button is-link is-small" disabled={!disclosureInfo.giftsAffirmCompletionOfG20ForPeriod || isSaveDisabled || false}
                                onClick={() => this.onMakeDisclosureSave()}>Make disclosure
                              </button>
                            </div> : null
                          }
                          <div className="column control">
                            <button className="button is-light is-small" onClick={this.onCancel}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    }
                  </div>
                )}
              </RatingSection>
            </div>
          }
        />
        <hr/>
        <Disclaimer/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(FastTrackDetail)
export default connect(mapStateToProps, null)(WrappedComponent)
