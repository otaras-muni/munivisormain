import React, { Component } from "react"
import GiftList from "./GiftList"

const Summary = ({ svControls }) => (
  <div>
    <div className="columns">
      <p className="column">
        <small>Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance</small>
        <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-20+Gifts%2C+Gratuities%2C+Non-Cash+Compensation+and+Expenses+of+Issuance.pdf" target="new">
          <i className="fas fa-info-circle" />
        </a>
      </p>
    </div>
    <div className="columns">
      <div className="column">
        <GiftList svControls={svControls}/>
      </div>
    </div>
  </div>
)

export default Summary
