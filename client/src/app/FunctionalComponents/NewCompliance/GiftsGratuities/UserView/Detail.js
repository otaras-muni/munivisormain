import React, { Component } from "react"
import { connect } from "react-redux"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify"
import moment from "moment"
import {DropdownList} from "react-widgets"
import * as qs from "query-string"
import swal from "sweetalert"
import Loader from "Global/Loader"
import { getPicklistByPicklistName, updateControlTaskStatus, getToken } from "GlobalUtils/helpers"
import {SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import EditableTable from "../../../../GlobalComponents/EditableTable"
import DocumentPage from "../../../../GlobalComponents/DocumentPage"
import Audit from "../../../../GlobalComponents/Audit"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {
  postCheckGiftsGratuities, postGiftsGratuitiesInfo, getGiftDetail,
  pullGiftsGratuitiesDetails, putGiftsGrautitiesAuditLogs, putGiftsGrautitiesDocuments
} from "../../../../StateManagement/actions/Supervisor"
import { RecordGiftAndExpenses } from "../../Validation/GiftsGrautitiesValidate"
import {fetchSupplierContacts} from "../../../../StateManagement/actions/TransactionDistribute"
import {fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import TableHeader from "../../../../GlobalComponents/TableHeader"

const cols = [
  {name: "User"},
  {name: "Notes/Instruction"},
  {name: "Time"}]

class Detail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tables: [
        {
          name:"Record gifts and related expenses",
          key: "giftsToRecipients",
          row: cloneDeep(CONST.SuperVisor.giftsToRecipients) || {},
          header: [
            {name: "Recipient's employer<span class='has-text-danger'>*</span>"},
            {name: "Recipient<span class='has-text-danger'>*</span>"},
            {name: "Gift/Expense Value<span class='has-text-danger'>*</span>"},
            {name: "Date gift/expense made"},
            {name: "Action"}
          ],
          list: [],
          tbody: [
            {name: "recipientEntityName", type: "dropdown", labelName: "recipientEntityName", dropdownKey: "allFirms" },
            {name: "name", type: "dropdown", labelName1: "recipientUserFirstName", labelName2: "recipientUserLastName", dropdownKey: "users" },
            // {name: "recipientUserFirstName", type: "dropdown", labelName: "recipientUserFirstName", dropdownKey: "users" },
            {name: "giftValue", placeholder: "$", type: "number", prefix: "$", error: "Gift Value must be larger than or equal to 0"},
            {name: "giftDate", placeholder: "Date", type: "date"},
            {name: "action"},
          ],
          handleError: (payload, minDate) => RecordGiftAndExpenses(payload, minDate),
          children: this.renderChildren()
        }
      ],
      bucketName: process.env.S3BUCKET,
      dropDown:{
        // polContribQuarterOfDisclosureFirm: [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        polContribQuarterOfDisclosureFirm: ["All"],
        polContribYearOfDisclosureFirm: [2017, 2018, 2019],
        polContribOfDisclosureFirm: ["Firm", "Self", "Someone else"],
        supervisorAction: [],
        usersList: [],
        allUsers: [],
        entityList: [],
        contributorCategory: [],
        allSuggestions: [],
        someUsers: [],
        allFirms: [],
      },
      disclosureInfo : CONST.SuperVisor.disclosureInfo || {},
      giftsCheckBoxes: {
        giftsNotSubjectToGenLimitation: false,
        giftsOfferingProceedsExclusion: false,
        giftsPermittedCompensationArrangements: false,
        giftsMuncipalSecurities: false,
        ruleG20Section: false,
        giftsPayments: false,
        giftsRuleG20: false,
      },
      documentsList: [],
      isSaveDisabled: {},
      supervisorNote: "",
      navStatus: "",
      actionBy: "",
      supervisorNotesShow: false,
      confirmAlert: CONST.confirmAlert,
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      status: "",
      loading:true,
    }
  }

  async componentDidMount(){
    const queryString = qs.parse(location.search)
    let preApprovalQuarter = queryString.quarter || ""
    let preApprovalYear = queryString.year || ""
    const polContribYearOfDisclosureFirm = []
    let min = 2015,max = min + 10
    for (let i = min; i<=max; i++) { polContribYearOfDisclosureFirm.push(i) }

    const picResult = await getPicklistByPicklistName(["LKUPCONTRIBUTORCAT", "LKUPSUPERVISORACTION"])
    const result = (picResult.length && picResult[1]) || {}
    const {disclosureInfo, currentYear, currentQuarter} = this.state
    const statusFlag = (queryString.year > currentYear) || ((queryString.quarter > currentQuarter) && (queryString.year === currentYear)) || queryString.status === "Pre-approval"

    if(statusFlag && !queryString.year && !queryString.quarter){
      if(currentQuarter === 4){
        preApprovalQuarter = 1
        preApprovalYear = currentYear + 1
      }else {
        preApprovalQuarter = currentQuarter + 1
        preApprovalYear = currentYear
      }
    }

    this.setState({
      dropDown: {
        ...this.state.dropDown,
        contributorCategory: result.LKUPCONTRIBUTORCAT || [],
        thirdPartyCategory: result.LKUPCONTRIBUTORCAT || [],
        supervisorAction: result.LKUPSUPERVISORACTION || [],
        polContribYearOfDisclosureFirm,
      },
      disclosureDetails: {},
      disclosureInfo: {
        ...disclosureInfo,
        year: currentQuarter === 4 && statusFlag ? preApprovalYear : queryString.year || currentYear,
        quarter: statusFlag ? preApprovalQuarter : queryString.quarter || currentQuarter,
        disclosureFor: queryString.disclosureFor || this.props.user.userId || "",
        status: statusFlag ? "Pre-approval" : ""
      },
      status: statusFlag ? "Pre-approval" : "",
      navStatus: queryString && queryString.status || "",
      quarterCheck: queryString.status ? currentQuarter + 1 : currentQuarter,
      ...queryString
    },() => {
      this.getEntitiesAndUsers()
      this.getCurrentEntityUsers()
    })
  }

  getEntitiesAndUsers = () => {
    fetchSupplierContacts(this.props.user.entityId, "All", (allFirms)=> {
      const allUsers = (allFirms && allFirms.userList) ? allFirms.userList.map(e => ({ ...e, name: `${e.userFirstName} ${e.userLastName}`, id: e._id })) : []
      const partFirms = (allFirms && allFirms.suppliersList) ? allFirms.suppliersList.map(e => ({ ...e, name: e.firmName, id: e._id })) : []

      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          allFirms: partFirms,
          allUsers,
        },
      }))
    })
  }

  getCurrentEntityUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          someUsers: users.usersList || [],
        },
        loading: false,
      }, ()=>{
        this.onUserSearch()
      })
    })
  }

  onUserSearch = () => {
    const {tables, currentQuarter} = this.state
    const queryString = qs.parse(location.search)
    const fetchId = queryString.id || queryString.disclosure || ""
    if (fetchId) {
      const query = `?id=${fetchId}`
      this.setState({
        isSearchDisabled: true
      }, () => {
        getGiftDetail(query, (res) => {
          if (res) {
            tables.forEach(tbl => {
              tbl.list = (res && res[tbl.key]) || []
            })

            this.setState(prevState => ({
              disclosureDetails: res || {},
              disclosureInfo: {
                ...prevState.disclosureInfo,
                quarter: res && res.quarter,
                year: res && res.year,
                status: typeof res && res.status !== "boolean" ? res && res.status || "" : prevState.status,
              },
              documentsList: res && res.giftDisclosureDocuments || [],
              quarterCheck: res.isPreApproval === "Yes" ? currentQuarter + 1 : currentQuarter,
              tables,
              giftsCheckBoxes: {
                giftsNotSubjectToGenLimitation: res.giftsNotSubjectToGenLimitation || false,
                giftsOfferingProceedsExclusion: res.giftsOfferingProceedsExclusion || false,
                giftsPermittedCompensationArrangements: res.giftsPermittedCompensationArrangements || false,
                giftsMuncipalSecurities: res.giftsMuncipalSecurities || false,
                ruleG20Section: res.ruleG20Section || false,
                giftsPayments: res.giftsPayments || false,
                giftsRuleG20: res.giftsRuleG20 || false,
              },
              isSearchDisabled: false,
              actionBy: res && res.actionBy || "",
              auditLogs: (res && res.auditLogs) || [],
              fetchId,
              loading: false,
            }))
          } else {
            toast("something went wrong", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
            this.setState({
              isSearchDisabled: false
            })
          }
        })
      })
    }
  }

  renderChildren = () => (
    <div>
      <div className="columns is-vcentered">
        <div className="column">
          <p className="emmaTablesTd"><strong>Please note: </strong>If gifts are given
            to multiple recipients, regulated entities should record the names of
            each recipient and calculate and record the value of the gift on a pro
            rata per recipient basis, for purposes of ensuring compliance with the
            general limitation of Rule G-20 section (c).
          </p>
        </div>
      </div>
      { this.props.svControls && this.props.svControls.supervisor ?
        <div className="columns is-vcentered">
          <div className="column">
            <p className="emmaTablesTd"><strong>Cannot find recipient or related firm?</strong></p>
          </div>
          <div className="column">
            <div className="control">
              <button className="button is-text is-small">
                <a href="/addnew-entity" target="new">Add Entity/Firm to CRM first</a>
              </button>
            </div>
          </div>
          <div className="column">
            <div className="control">
              <button className="button is-text is-small">
                <a href="/addnew-contact" target="new">Add Recipient to CRM first</a>
              </button>
            </div>
          </div>
        </div>
        : null }

    </div>
  )

  onChange = (e, inputName) => {
    if (inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
        }
      })
    } else if(e.target.name === "status") {
      const { name, value, title } = e.target
      const { confirmAlert } = this.state
      confirmAlert.text = `You Want To ${value} The Disclosure?`
      swal(confirmAlert)
        .then((willDelete) => {
          if (value) {
            if(willDelete){
              if(name === "status"){
                const { user } = this.props
                const userName = `${user.userFirstName} ${user.userLastName}` || ""
                this.props.addAuditLog({userName, log: `Supervisor ${userName} Disclosure status Change To ${value || ""}`, date: new Date(), key: "notes"})
              }
              this.setState({
                disclosureInfo: {
                  ...this.state.disclosureInfo,
                  [name]: value
                }
              }, () => {
                if (name === "status") {
                  this.props.addAuditLog({userName: this.props.user.userFirstName, log: `change ${title} ${value}`, date: new Date(), key: "details" })
                  this.onActionSave()
                }
              })
            }
          }
        })
    } else {
      const { name, value } = e.target
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [name]: e.target.type === "checkbox" ? e.target.checked : value
        }
      })
    }
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    const {dropDown} = this.state
    if(inputName === "recipientEntityName") {
      this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In Disclosure ${user.name}`, date: new Date(), key })
      this.setState({
        dropDown: {
          ...dropDown,
          usersList: dropDown.allUsers.filter(e => select.id === (e.entityId && e.entityId._id)) || []
        }
      },() => {
        callback({
          status: true,
          object: {
            recipientEntityId: select.id,
            recipientEntityName: select.name,
            name: ""
          },
        })
      })

    }
    if(inputName === "name") {
      callback({
        status: true,
        object: {
          recipientUserId: select.id,
          recipientUserFirstName: select.userFirstName,
          recipientUserLastName: select.userLastName,
          name: select.name || ""
        },
      })
    }
  }

  onRemove = (type, removeId, callback) => {
    const {disclosureDetails} = this.state
    const giftsId = disclosureDetails._id
    pullGiftsGratuitiesDetails(type, giftsId, removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed Gifts And Gratuities Disclosure successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    const {disclosureDetails, disclosureInfo, navStatus} = this.state
    const {user} = this.props
    const {disclosureFor, year, quarter} = disclosureInfo
    disclosureInfo.quarter = "All"
    const types = navStatus === "Pre-approval" ? "Pre" : navStatus === "Record" ? navStatus : ""
    const giftsId = disclosureDetails._id // eslint-disable-line
    const query = `${disclosureFor}?year=${year}&quarter=${"All"}&type=${type}&discloseType=${types}`
    const queryString = `${giftsId || "none"}?type=${type}&discloseType=${types}`

    if(item && !item._id){
      item.createdUserId = user.userId
      item.createdUserName = `${user.userFirstName} ${user.userLastName}`
    }
    postCheckGiftsGratuities(giftsId ? queryString : query, item, (res)=> {
      if (res && res.status === 200) {
        toast("Record gifts and related expenses updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.setState({
          auditLogs: (res && res.data && res.data.auditLogs) || [],
          fetchId: res && res.data._id,
          disclosureDetails: {
            ...disclosureDetails,
            ...res.data
          },
          disclosureInfo: {
            ...disclosureInfo,
            status: res.data.status || disclosureInfo.status || ""
          },
        },() => {
          this.onAuditSave(type)
          callback({
            status: true,
            list: (res && res.data && res.data[type]) || [],
          })
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    const {disclosureDetails} = this.state
    if(disclosureDetails && disclosureDetails._id){
      putGiftsGrautitiesDocuments("",`?giftsId=${disclosureDetails._id}`, docs, (res)=> {
        if (res && res.status === 200) {
          toast("Disclosure Related Documents has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("documents")
          callback({
            status: true,
            documentsList: (res.data && res.data.giftDisclosureDocuments) || [],
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    }
  }

  onStatusChange = async (e, doc, callback) => {
    const {disclosureDetails} = this.state
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc,
        lastUpdatedDate: moment(new Date()).format("MM/DD/YYYY hh:mm A")
      }
      type = "updateMeta"
    }
    putGiftsGrautitiesDocuments(`&giftsId=${disclosureDetails._id}`, `?details=${type}`, document, (res)=> {
      if (res && res.status === 200) {
        toast("Document status has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if(name){
          callback({
            status: true,
          })
        }else {
          callback({
            status: true,
            documentsList: (res.data && res.data.giftDisclosureDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const {disclosureDetails} = this.state
    pullGiftsGratuitiesDetails("giftDisclosureDocuments", disclosureDetails._id, documentId, (res) => {      if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.onAuditSave("document")
      callback({
        status: true,
        documentsList: (res.data && res.data.giftDisclosureDocuments) || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
    })
  }

  onAuditSave = (key) => {
    const {user} = this.props
    const {disclosureDetails} = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)

    if(disclosureDetails && disclosureDetails._id) {
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Gifts And Gratuities"
          log.superVisorSubSection = "Disclosure"
        })
        putGiftsGrautitiesAuditLogs(`?giftsId=${disclosureDetails._id}`, auditLogs, (res) => {
          if (res && res.status === 200) {
            const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
            this.props.updateAuditLog(remainLogs)
            this.setState({
              auditLogs: (res.data && res.data.auditLogs) || []
            })
          }
        })
      }
    }
  }

  onCheck = (e) => {
    const {giftsCheckBoxes} = this.state
    const {user} = this.props
    if(e && e.target && e.target.name && e.target.title){
      this.props.addAuditLog({userName: user.userFirstName, log: `Change ${e.target.title} ${giftsCheckBoxes[e.target.name] ? "Yes" : "No"} to ${e.target.checked ? "Yes" : "No" }`, date: new Date(), key: "details"})
    }
    this.setState({
      giftsCheckBoxes: {
        ...giftsCheckBoxes,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    })
  }

  onChangeItem = (value, name) => {
    this.setState({
      [name]: value,
    })
  }

  onSaveCheckDetails = (flag) => {
    const {giftsCheckBoxes, disclosureDetails, supervisorNote, navStatus} = this.state
    const {svControls, user} = this.props
    const type = navStatus === "Pre-approval" ? "Pre" : navStatus === "Record" ? navStatus : ""
    if (supervisorNote) {
      giftsCheckBoxes.notes = {
        userName: `${user.userFirstName} ${user.userLastName}`,
        note: supervisorNote,
        supervisor: svControls && svControls.supervisor,
      }
      this.props.addAuditLog({userName: user.userFirstName, log: `add notes ${supervisorNote} by ${user.userFirstName} ${user.userLastName}`, date: new Date(), key: "details"})
    }

    if(disclosureDetails && disclosureDetails._id) {
      this.setState({
        isSaveDisabled: {
          giftsGratuitiesCheck: true
        }
      }, () => {
        postCheckGiftsGratuities(`${disclosureDetails._id}?type=details&discloseType=${type}`, giftsCheckBoxes, (res) => {
          if(res && res.status === 200) {
            if((flag && flag !== "status")){
              toast("Gifts And Gratuities updated disclosure successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            }
            this.onAuditSave("check")
            this.setState(prevState => ({
              isSaveDisabled: {
                ...prevState.isSaveDisabled,
                giftsGratuitiesCheck: false
              },
              giftsCheckBoxes: {
                giftsNotSubjectToGenLimitation: (res && res.data && res.data.giftsNotSubjectToGenLimitation) || false,
                giftsOfferingProceedsExclusion: (res && res.data && res.data.giftsOfferingProceedsExclusion) || false,
                giftsPermittedCompensationArrangements: (res && res.data && res.data.giftsPermittedCompensationArrangements) || false,
                giftsMuncipalSecurities: (res && res.data && res.data.giftsMuncipalSecurities) || false,
                ruleG20Section: (res && res.data && res.data.ruleG20Section) || false,
                giftsPayments: (res && res.data && res.data.giftsPayments) || false,
                giftsRuleG20: (res && res.data && res.data.giftsRuleG20) || false,
              },
              disclosureDetails: {
                ...prevState.disclosureDetails,
                notes: res.data.notes || [],
                ...res.data
              },
              supervisorNote: "",
              disclosureInfo: {
                ...prevState.disclosureInfo,
                status: res.data.status || prevState.disclosureInfo.status || ""
              },
              auditLogs: (res && res.data && res.data.auditLogs) || [],
            }),() => {
              this.onAuditSave("details")
            })
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            this.setState(prevState => ({
              isSaveDisabled: {
                ...prevState.isSaveDisabled,
                giftsGratuitiesCheck: false
              }
            }))
          }
        })
      })
    }else {
      this.onActionSave()
    }
  }

  onActionSave = () => {
    const {disclosureInfo, giftsCheckBoxes, fetchId, actionBy} = this.state
    const {user, svControls} = this.props
    if(disclosureInfo && (disclosureInfo.status === "Approved" || disclosureInfo.status === "Rejected" || disclosureInfo.status === "Canceled") && !actionBy && (svControls && svControls.supervisor)){
      disclosureInfo.actionBy = `${user.userFirstName} ${user.userLastName}`
    }
    this.setState({
      isSaveDisabled: true
    }, () => {
      postGiftsGratuitiesInfo({ ...disclosureInfo, ...giftsCheckBoxes, fetchId }, (res) => {
        toast("Disclosure has been updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        if(res && res.status === 200) {
          this.setState(prevState => ({
            isSaveDisabled: false,
            tempDisclosureInfo: disclosureInfo,
            disclosureDetails: {
              ...res.data
            },
            disclosureInfo: {
              ...disclosureInfo,
              status: res.data && res.data.status || prevState.disclosureInfo.status || ""
            },
            actionBy: res.data && res.data.actionBy || ""
          }), () => {
            this.onAuditSave("details")
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            isSaveDisabled: false,
          })
        }
      })
    })
  }

  onUsersListRefresh = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        }
      })
    })
  }

  onMakeDisclosureSave = (status) => {
    const { disclosureInfo, fetchId, navStatus } = this.state
    const { svControls } = this.props
    const superVisor = svControls && svControls.supervisor ? "supervisor-view" : "user-view"
    const type = navStatus === "Pre-approval" ? "Pre" : navStatus === "Record" ? navStatus : ""
    const {user} = this.props
    delete disclosureInfo.G37Obligation
    delete disclosureInfo.affirmationNotes
    disclosureInfo.status = status
    disclosureInfo.preApprovalDate = Date()
    if(type){
      disclosureInfo.discloseType = type
    }

    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${status === "Complete" ? "Make Disclosure" : "Send for Pre-approval"}`, date: new Date(), key: "MakeDisclosure" })
    this.setState({
      loading: true
    }, () => {
      this.onSaveCheckDetails("status")
      postGiftsGratuitiesInfo({ ...disclosureInfo, fetchId },async (res) => {
        if (res && res.status === 200) {
          this.onAuditSave("MakeDisclosure")
          if ((res && res.data && res.data.cac) && status === "Complete") {
            const taskId = res.data && res.data.taskRefId || ""
            try{
              await updateControlTaskStatus(getToken(), taskId)
            } catch (err) {
              console.log(err)
            }
          }

          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.history.push(`/compliance/cmp-sup-gifts/${superVisor}/summary`)
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            loading: false,
          })
        }
      })
    })
  }

  onBlurInput = (e) => {
    const {user} = this.props
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    if (name && value) {
      this.props.addAuditLog({
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `In Gifts ${name || "empty"} change to ${value || "empty"} in Required Affirmation`,
        date: new Date(),
        key: "check"
      })
    }
  }

  render() {
    const { tables, disclosureInfo, disclosureDetails, dropDown, currentYear, auditLogs, documentsList,
      giftsCheckBoxes, supervisorNotesShow, supervisorNote, isSearchDisabled, isSaveDisabled, fetchId,  navStatus} = this.state
    const {svControls, user, nav2, audit} = this.props
    const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
    const canEdit = disclosureDetails && disclosureDetails.userOrEntityId && user ? disclosureDetails.status === "Pending" : true
    const isDisclose = disclosureDetails && disclosureDetails.userOrEntityId && user && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved")

    const isMakeDisclose = (disclosureDetails && disclosureDetails.giftsToRecipients && !!disclosureDetails.giftsToRecipients.length) || false
    const actionDisabled = ["Pre-approval", "Approved", "Canceled", "Rejected", "Complete"]
    const dropped = disclosureInfo && disclosureInfo.status === "Dropped"

    const canTableEdit = svControls && svControls.canSupervisorEdit && actionDisabled.indexOf(disclosureDetails && disclosureDetails.status) === -1 && !dropped
    if(dropped){
      // svControls.canSupervisorEdit = true
    }
    const staticField = {
      docCategory: "Gifts And Gratuities",
      docSubCategory: "Gifts, Gratuities Non-Cash Compensation and Expenses of Issuance",
    }
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance</small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-20+Gifts%2C+Gratuities%2C+Non-Cash+Compensation+and+Expenses+of+Issuance.pdf" target="new">
                <i className="fas fa-info-circle"/>
              </a>
            </p>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <p className="multiExpLbl ">Disclosure for</p>
            <div className="control">
              <div style={{fontSize: 12}}>
                {(svControls && svControls.supervisor && !fetchId) ?
                  <DropdownList filter value={disclosureInfo.disclosureFor || []} data={dropDown.someUsers || []} textField="name" valueField="id" key="name" onChange={(user) => this.onChange(user, "disclosureFor")} />
                  : fetchId ? <span>{`${disclosureDetails.userFirstName} ${disclosureDetails.userLastName}` || []}</span> : <span>{`${user.userFirstName} ${user.userLastName}` || []}</span>}
                {svControls && svControls.supervisor && !fetchId ?
                  <div style={{fontSize: 12,marginTop: 5}}>
                    <a className="has-text-link" style={{fontSize: 12,marginTop: -10}} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                    <i className="fa fa-refresh" style={{fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px"}} onClick={this.onUsersListRefresh}/>
                  </div> : null}
              </div>
            </div>
          </div>

          <SelectLabelInput
            label="Report Quarter"
            list={dropDown.polContribQuarterOfDisclosureFirm}
            name="quarter"
            value="All"
            disabled
            onChange={this.onChange}
          />
          <div className="column">
            <p className="multiExpLbl">Report Year</p>
            <div className="select is-small is-fullwidth is-link">
              <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange} disabled={fetchId}>
                <option value="">Pick</option>
                {dropDown.polContribYearOfDisclosureFirm.map((value) => <option key={value} value={value} /* disabled={status === "Pre-approval" ? currentQuarter === 4 ?  value <= currentYear : value < currentYear : value > currentYear} */ >{value}</option>)}
              </select>
            </div>
          </div>
          { disclosureDetails && disclosureDetails._id && svControls.supervisor && ( disclosureDetails.status === "Pre-approval" || disclosureInfo.status === "Approved" || disclosureInfo.status === "Rejected" || disclosureInfo.status === "Canceled" ) ?
            <SelectLabelInput
              title="Disclosure status"
              label="Supervisor's Action"
              error= ""
              list={dropDown.supervisorAction}
              name="status"
              value={disclosureInfo.status}
              disabled={ disclosureInfo.status === "Complete" || disclosureInfo.status === "Approved" || disclosureInfo.status === "Rejected" || disclosureInfo.status === "Canceled" || dropped }
              onChange={this.onChange} />
            :null }
        </div>

        {
          isSearchDisabled ? <Loader/> :
            <div>
              <EditableTable {...this.props} tables={tables} canSupervisorEdit={canTableEdit} onParentChange={this.onDropDownChange} dropDown={dropDown}
                onSave={this.onSave} onRemove={this.onRemove}/>


              <Accordion multiple
                activeItem={[0, 1]}
                boxHidden
                render={({activeAccordions, onAccordion}) =>
                  <div>
                    <RatingSection
                      onAccordion={() => onAccordion(0)}
                      title="Required affirmations"
                    >
                      {activeAccordions.includes(0) && (
                        <div>
                          <div className="columns">
                            <div className="column">
                              <p className="title" style={{fontSize: "medium"}}>Gifts and Gratuities Not Subject to General Limitation</p>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <label className="checkbox-button multiExpTblVal">
                                 I confirm that I have read and understood Gifts and Gratuities Not Subject to General Limitation as defined in Rule G-20 section (d). The gifts as related to this disclosure do fall under the category of Gifts and Gratuities Not Subject to General Limitation as defined in Rule G-20 section (d).
                                <input
                                  type="checkbox"
                                  checked={giftsCheckBoxes.giftsNotSubjectToGenLimitation || false}
                                  name="giftsNotSubjectToGenLimitation"
                                  onChange={this.onCheck}
                                  onClick={this.onCheck}
                                  disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                                  onBlur = {(e) => this.onBlurInput(e)}
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <p className="title" style={{fontSize: "medium"}}>Prohibition of Use of Offering Proceeds Exclusion</p>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <label className="checkbox-button multiExpTblVal">
                                 I confirm that I have read and understood Rule G-20 section (e). The entertainment expenses as related to this disclosure include ordinary and reasonable expenses for meals hosted by the regulated entity and directly related to the offering for which the regulated entity was retained.
                                <input
                                  type="checkbox"
                                  checked={giftsCheckBoxes.giftsOfferingProceedsExclusion || false}
                                  name="giftsOfferingProceedsExclusion"
                                  onChange={this.onCheck}
                                  onClick={this.onCheck}
                                  disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                                  onBlur = {(e) => this.onBlurInput(e)}
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <p className="title" style={{fontSize: "medium"}}>Permitted non-cash compensation arrangements</p>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <label className="checkbox-button multiExpTblVal">
                                 I confirm that I have read and understood permitted non-cash compensation arrangements as defined in Rule G-20 section (g). The payments directly or indirectly accepted or made as related to this disclosure do fall under the category of permitted non-cash compensation arrangements as defined in Rule G-20 section (g).
                                <input
                                  type="checkbox"
                                  checked={giftsCheckBoxes.giftsPermittedCompensationArrangements || false}
                                  name="giftsPermittedCompensationArrangements"
                                  onChange={this.onCheck}
                                  onClick={this.onCheck}
                                  disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                                  onBlur = {(e) => this.onBlurInput(e)}
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <p className="title" style={{fontSize: "medium"}}>Other</p>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <label className="checkbox-button multiExpTblVal">
                                 I confirm that I did not directly or indirectly accept or make payments or offers of payments of any non-cash compensation in connection with the sale and distribution of a primary offering of municipal securities.
                                <input
                                  type="checkbox"
                                  checked={giftsCheckBoxes.giftsMuncipalSecurities || false}
                                  name="giftsMuncipalSecurities"
                                  onChange={this.onCheck}
                                  onClick={this.onCheck}
                                  disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                                  onBlur = {(e) => this.onBlurInput(e)}
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                          </div>

                          <div className="columns">
                            <div className="column">
                              <label className="checkbox-button multiExpTblVal">
                                 I confirm that I have read and understood Prohibition of Use of Offering Proceeds as defined in Rule G-20 section (e). At the time of making this disclosure, I are not aware of instances where I are requesting or obtaining reimbursement of costs and expenses related to the entertainment of any person, including, but not limited to, any official or other personnel of the municipal entity or personnel of the obligated person, from the proceeds of such offering of municipal securities as defined in Rule G-20 section (e).
                                <input
                                  type="checkbox"
                                  checked={giftsCheckBoxes.ruleG20Section || false}
                                  name="ruleG20Section"
                                  onChange={this.onCheck}
                                  onClick={this.onCheck}
                                  disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                                  onBlur = {(e) => this.onBlurInput(e)}
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                          </div>

                        </div>
                      )}
                    </RatingSection>
                    {
                      isMakeDisclose ?
                        <DocumentPage {...this.props} title="Related Documents" documents={documentsList} isNotTransaction
                          pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickType="LKUPDOCTYPE"
                          pickAction="LKUPDOCACTION" staticField={staticField} category="giftDisclosureDocuments"
                          contextType={ContextType.supervisor.giftsAndGrautities} tranId={nav2} isDisabled={!canEdit}
                          onDeleteDoc={this.onDeleteDoc} onSave={this.onDocSave} onStatusChange={this.onStatusChange}
                        /> : null
                    }

                    <br/>

                    <div className="columns">
                      <div className="column">
                        <label className="checkbox-button multiExpTblVal">
                           I confirm that I did not directly or indirectly accept or make payments or offers of payments of any non-cash compensation in connection with the sale and distribution of a primary offering of municipal securities.
                          <input
                            type="checkbox"
                            checked={giftsCheckBoxes.giftsPayments || false}
                            name="giftsPayments"
                            onChange={this.onCheck}
                            onClick={this.onCheck}
                            disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                            onBlur = {(e) => this.onBlurInput(e)}
                          />
                          <span className="checkmark" />
                        </label>
                      </div>
                    </div>

                    <div className="columns">
                      <div className="column">
                        <label className="checkbox-button multiExpTblVal">
                           I confirm that I have read and understood Prohibition of Use of Offering Proceeds as defined in Rule G-20 section (e). At the time of making this disclosure, I are not aware of instances where I are requesting or obtaining reimbursement of costs and expenses related to the entertainment of any person, including, but not limited to, any official or other personnel of the municipal entity or personnel of the obligated person, from the proceeds of such offering of municipal securities as defined in Rule G-20 section (e).
                          <input
                            type="checkbox"
                            checked={giftsCheckBoxes.giftsRuleG20 || false}
                            name="giftsRuleG20"
                            onChange={this.onCheck}
                            onClick={this.onCheck}
                            disabled={actionDisabled.indexOf(disclosureDetails.status) !== -1 || dropped}
                            onBlur = {(e) => this.onBlurInput(e)}
                          />
                          <span className="checkmark" />
                        </label>
                      </div>
                    </div>

                    {
                      disclosureDetails && disclosureDetails._id &&
                      <div>
                        <a className="button is-link is-small"
                          onClick={() => this.setState({supervisorNotesShow: !supervisorNotesShow})}>{`${supervisorNotesShow ? "Hide" : "Show"}`} All
                                   Notes</a>
                        {
                          supervisorNotesShow && Array.isArray(disclosureDetails.notes) &&
                            <table className="table is-striped is-fullwidth">
                              <TableHeader cols={cols}/>
                              <tbody>
                                {
                                  disclosureDetails.notes.length ? disclosureDetails.notes.map((note, i) => (
                                    <tr key={i}>
                                      <td className="emmaTablesTd">
                                        <small>{`${note.userName} ${note.supervisor ? "(Supervisor)" : ""}`}</small>
                                      </td>
                                      <td className="emmaTablesTd" style={{width: "70%"}}>
                                        <small>{note.note}</small>
                                      </td>
                                      <td className="emmaTablesTd">
                                        <small>{note.createdDate ? moment(note.createdDate).format("MM-DD-YYYY h:mm A") : ""}</small>
                                      </td>
                                    </tr>
                                  )) : null
                                }
                              </tbody>
                            </table>
                        }
                        {
                          canEdit || (/* svControls.supervisor && */(disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) ?
                            <div className="columns">
                              <div className="column is-full">
                                <p className="multiExpLbl">Supervisor Notes/Instructions</p>
                                <div className="control">
                                  <textarea className="textarea" value={supervisorNote || ""} name="supervisorNote" disabled={(!canEdit && !(/* svControls.supervisor && */(disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) || false)}
                                    onChange={(e) => this.onChangeItem(e.target.value, e.target.name)}/>
                                </div>
                              </div>
                            </div> : null
                        }
                      </div>
                    }

                    <div className="column is-full">
                      <div className="field is-grouped">
                        <div className="control">
                          <button className="button is-link is-small" onClick={this.onSaveCheckDetails}
                            disabled={isSaveDisabled.giftsGratuitiesCheck || !((canEdit && fetchId) || (fetchId && disclosureDetails && disclosureDetails._id || fetchId && disclosureInfo.disclosureFor === user.userId)) || false}>Save
                          </button>
                        </div>
                        {  isDisclose && !dropped ?
                          <div className="control">
                            {disclosureDetails.status === "Pending" && navStatus === "Pre-approval" ?
                              <button className="button is-link is-small"
                                disabled={isSaveDisabled.giftsGratuitiesCheck || !isMakeDisclose || false}
                                onClick={() => this.onMakeDisclosureSave("Pre-approval")}>Send for Pre-approval
                              </button>
                              : (parseInt(currentYear,10) >= disclosureInfo.year) && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved" ) &&
                              <button className="button is-link is-small"
                                disabled={isSaveDisabled.giftsGratuitiesCheck || !isMakeDisclose || false}
                                onClick={() => this.onMakeDisclosureSave("Complete")}>Make
                              disclosure</button>
                            }
                          </div> : null
                        }
                      </div>
                    </div>

                    {
                      submitAudit &&
                      <RatingSection onAccordion={() => onAccordion(4)} title="Activity Log" style={{overflowY: "scroll", fontSize: "smaller"}}>
                        {activeAccordions.includes(4) &&
                        <Audit auditLogs={auditLogs || []}/>
                        }
                      </RatingSection>
                    }
                    <hr/>
                    <Disclaimer/>

                  </div>
                }
              />
            </div>
        }

      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

const WrappedComponent = withAuditLogs(Detail)
export default connect(mapStateToProps, null)(WrappedComponent)
