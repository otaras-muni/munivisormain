import React, { Component } from "react"
import { connect } from "react-redux"
import Loader from "Global/Loader"
import Instructions from "./Instructions"
import UserView from "./UserView/UserView"
import SupervisorView from "./SupervisorView/SupervisorView"

class GiftsGratuities extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
    }
  }

  componentWillMount() {
    this.setState({
      loading: false,
    })
  }

  renderSelectedView = (nav2, nav3) => {
    const {svControls} = this.props
    switch (nav2) {
    case "cmp-sup-gifts":
      switch (nav3) {
      case "instructions":
        return <Instructions {...this.props} />
      case "user-view":
        return <UserView {...this.props} svControls={svControls}/>
      case "supervisor-view":
        return <SupervisorView {...this.props}/>
      default:
        return nav3
      }
    default :
      return nav3
    }
  }

  render() {
    const {nav2, nav3} = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="column">
        <section id="main">
          {this.renderSelectedView(nav2, nav3)}
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
})

export default connect(mapStateToProps, null)(GiftsGratuities)
