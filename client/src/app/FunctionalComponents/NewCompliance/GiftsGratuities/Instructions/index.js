import React, { Component } from "react"
import * as qs from "query-string"
import Loader from "Global/Loader"
import { Link } from "react-router-dom"

class Instructions extends Component {

  constructor(props) {
    super(props)
    this.state = {
      navId: "",
      loading:true
    }
  }

  componentWillMount() {
    const queryString = qs.parse(this.props.history.location.search)
    const {disclosure} = queryString
    this.setState({
      navId: disclosure || "",
      loading: false
    })
  }

  render() {
    const {navId} = this.state
    const {svControls} = this.props
    const nav3 = svControls.supervisor ? "supervisor-view" : "user-view"
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-44 based recommended sections in compliance policy and supervisory procedure
                document
              </small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf"
                target="new">
                <i className="fas fa-info-circle" />
              </a>
            </p>
          </div>
        </div>

        <div className="columns">

          <div className="column">
            <section className="container">
              <div className="tile is-ancestor">
                <div className="tile is-parent">
                  <article className="tile is-child box has-text-centered notification is-warning" style={{backgroundColor: "#F29718"}}>
                    <p>You have <a
                      href="http://www.msrb.org/Rules-and-Interpretations/MSRB-Rules/General/Rule-G-20.aspx"
                      target="new">read MSRB Rule G-20 </a>. You are
                      seeking supervisor pre-approval.
                    </p>
                    <hr/>
                    <Link to={`/compliance/cmp-sup-gifts/${nav3}/detail?${navId ? `status=Pre-approval&disclosure=${navId}` : "status=Pre-approval"}`}>
                      <p className="title mgmtConTitle">Pre-approval</p>
                    </Link>
                  </article>
                </div>
                <div className="tile is-parent">
                  <article className="tile is-child box has-text-centered notification is-link" style={{backgroundColor: "#38A341"}}>
                    <p>You have <a
                      href="http://www.msrb.org/Rules-and-Interpretations/MSRB-Rules/General/Rule-G-20.aspx"
                      target="new">read MSRB Rule G-20</a>. You have nothing
                      to
                      record and affirming as such.
                    </p>
                    <hr/>
                    <Link to={`/compliance/cmp-sup-gifts/${nav3}/fastTrackDetail${navId ? `?disclosure=${navId}` : ""}`} >
                      <p className="title mgmtConTitle">Fast Track</p>
                    </Link>
                  </article>
                </div>
                <div className="tile is-parent">
                  <article className="tile is-child box has-text-centered notification is-link" style={{backgroundColor: "#3982CC"}}>
                    <p>You have <a
                      href="http://www.msrb.org/Rules-and-Interpretations/MSRB-Rules/General/Rule-G-20.aspx"
                      target="new">read MSRB Rule G-20 </a>. You want to
                      record/disclose a Gift or Gratuity.
                    </p>
                    <hr/>
                    <Link to={`/compliance/cmp-sup-gifts/${nav3}/detail?${navId ? `status=Record&disclosure=${navId}` : "status=Record"}`}>
                      <p className="title mgmtConTitle">Make a record</p>
                    </Link>
                  </article>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    )
  }
}

export default Instructions
