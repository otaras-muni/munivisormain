import React, { Component } from "react"
import cloneDeep from "clone-deep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import EditableTable from "../../../GlobalComponents/EditableTable"
import Accordion from "../../../GlobalComponents/Accordion"
import Disclaimer from "../../../GlobalComponents/Disclaimer"
import { getPicklistByPicklistName, updateAuditLog } from "GlobalUtils/helpers"
import Loader from "../../../GlobalComponents/Loader"
import CONST from "../../../../globalutilities/consts"
import { fetchAssigned } from "../../../StateManagement/actions/CreateTransaction"
import {
  fetchResponsibleDetails,
  putSupervisory,
  pullSupervisory
} from "../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import {
  ResponsibleSupervisoryValidate,
} from "../Validation/SupervisoryObligation"

class People extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isEditable: {},
      tables: [
        {
          name: "Persons Responsible for Supervision",
          key: "responsibleSupervision",
          row: cloneDeep(CONST.SuperVisor.responsibleSupervision) || {},
          header: [
            { name: "Person<span class='has-text-danger'>*</span>" },
            {name: "Email"},
            {name: "Phone"},
            { name: "Designation<span class='has-text-danger'>*</span>" },
            {name: "Start Date"},
            {name: "End Date"},
            { name: "Action" }
          ],
          list: [],
          tbody: [
            {
              name: "name",
              type: "dropdown",
              labelName1: "userFirstName",
              labelName2: "userLastName",
              dropdownKey: "users",
              style: {minWidth: "150px"}
            },
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "userComplianceDesignation", placeholder: "Pick", type: "select"},
            {name: "startDate", type: "date"},
            {name: "endDate", type: "date", error: "must be larger than or equal to Start Date"},
            {name: "action"}
          ],
          handleError: payload => ResponsibleSupervisoryValidate(payload),
          children: this.renderChildren()
        },
      ],
      notes: CONST.CAC.svcoPeople,
      dropDown: {
        userActive: [],
        userComplianceDesignation: [],
        certifierDesignation: [],
        usersList: []
      },

      loading: true
    }
  }

  async componentWillMount() {
    const { tables } = this.state
    const { svControls } = this.props
    let result = await getPicklistByPicklistName([
      "LKUPOBLIDESIGNATION",
      "LKUPYESNO"
    ])
    result = (result.length && result[1]) || {}
    fetchResponsibleDetails(res => {
      !svControls.canSupervisorEdit
        ? tables.forEach(t => {
          t.header.splice(-1, 1)
        })
        : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState(
        {
          dropDown: {
            ...this.state.dropDown,
            userActive: result.LKUPYESNO || [],
            userComplianceDesignation: result.LKUPOBLIDESIGNATION || [],
            certifierDesignation: result.LKUPOBLIDESIGNATION || []
          },
          loading: false,
          contextId: (res && res._id) || "",
          auditLogs: (res && res.auditLogs) || []
        },
        () => {
          this.getUsers()
        }
      )
    })
  }

  renderChildren = () => (
    <div className="columns">
      <div className="column">
        <p className="multiExpTblVal">
          The CCO may be a principal of the firm or a non-employee of the firm.
        </p>
      </div>
    </div>
  )

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, users => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || []
        },
        loading: false
      })
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    if (inputName === "certifiedBy") {
      const user = select
      let email = ""
      if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      this.props.addAuditLog({
        userName: this.props.user.userFirstName,
        log: `In Annual Certification select certified by ${user.name}`,
        date: new Date(),
        key
      })
      callback({
        status: true,
        object: {
          certifiedBy: {
            id: user.id,
            name: user.name,
            userEntityId: user.entityId,
            userId: user.id,
            userFirstName: user.userFirstName,
            userLastName: user.userLastName,
            userPrimaryEmailId: email
          }
        }
      })
    } else if (inputName === "doc") {
      callback({
        status: true,
        object: {
          docCategory: "Supervisory Obligations",
          docSubCategory: "Annual Certification",
          docAWSFileLocation: (select && select.docAWSFileLocation) || "",
          docFileName: (select && select.docFileName) || ""
        }
      })
    } else {
      const user = select
      let email = ""
      let phone = ""
      if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }

      this.props.addAuditLog({
        userName: this.props.user.userFirstName,
        log: `In Persons Responsible for Supervision select ${
          user.name
        } person`,
        date: new Date(),
        key
      })
      callback({
        status: true,
        object: {
          userId: user.id || "",
          userFirstName: user.userFirstName || "",
          userLastName: user.userLastName || "",
          userPrimaryEmailId: email || "",
          userPrimaryPhone: phone || "",
          userRoleFirm: user.userFlags && user.userFlags.toString() || "",
          userEntityId: user.entityId || "",
          name: user.name || ""
        }
      })
    }
  }

  onRemove = (type, removeId, callback) => {
    pullSupervisory(type, removeId, res => {
      if (res && res.status === 200) {
        toast("Removed Supervisory Obligation Admin successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    putSupervisory(type, item, res => {
      if (res && res.status === 200) {
        toast("Added Supervisory Obligation Admin successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onAuditSave = async key => {
    const { user } = this.props
    if (key === "responsibleSupervision") {
      const auditLogs = this.props.auditLogs.filter(log => log.key === key)
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Supervisory Obligation"
          log.superVisorSubSection = "Persons Responsible for Supervision"
        })
        await updateAuditLog("supervisoryObligation", auditLogs)
        this.props.updateAuditLog([])
      }
    }
  }

  onDeleteDoc = async (type, versionId, documentId, callback) => {
    this.props.addAuditLog({
      userName: `${this.props.user.userFirstName} ${
        this.props.user.userLastName
      }`,
      log: "document removed from Supervisory Obligation",
      date: new Date(),
      key: "annualCertification"
    })
    pullSupervisory(type, documentId, res => {
      if (res && res.status === 200) {
        toast("Removed Supervisory Obligation Admin successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  render() {
    const { dropDown, tables } = this.state
    const { svControls, nav2 } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div>
          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <EditableTable
                  {...this.props}
                  tables={tables}
                  canSupervisorEdit={svControls.canSupervisorEdit}
                  contextType="SVOBLIGATIONS"
                  contextId={nav2}
                  dropDown={dropDown}
                  onSave={this.onSave}
                  onRemove={this.onRemove}
                  onParentChange={this.onDropDownChange}
                  deleteDoc={this.onDeleteDoc}
                  getUsers={this.getUsers}
                />
              </div>
            )}
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(People)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
