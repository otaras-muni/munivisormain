import React, { Component } from "react"
import Loader from "Global/Loader"

import People from "./People"
import Documents from "./Documents"
import Audit from "../../../GlobalComponents/Audit"
import {getAuditLogByType} from "../../../StateManagement/actions/audit_log_actions";

class SupervisoryObligation extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
    }
  }

  async componentWillMount() {
    await getAuditLogByType("supervisoryObligation", "supervisoryObligation", res => {
      this.setState({
        auditLogs: res && res.changeLog || [],
        loading: false,
      })
    })
  }

  renderSelectedView = (nav2, nav3) => {
    switch (nav2) {
    case "cmp-sup-compobligations":
      switch (nav3) {
      case "designated-people":
        return <People {...this.props} />
      case "recordkeeping":
        return <Documents {...this.props} />
      case "audit":
        return <Audit auditLogs={this.state.auditLogs} />
      default:
        return <People {...this.props} />
      }
    default :
      return nav2
    }
  }

  render() {
    const {nav2, nav3} = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="column">
        <section id="main">
          {this.renderSelectedView(nav2, nav3)}
        </section>
      </div>
    )
  }
}

export default SupervisoryObligation
