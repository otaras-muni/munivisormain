import React from "react"
import { connect } from "react-redux"
import {NavLink, Link} from "react-router-dom"
import Loader from "../../GlobalComponents/Loader"
import SuperVisorTabs from "./SuperVisorTabs"
import SidebarView from "../Compliance1/Component/SuperVisor/SidebarView"
import ProfessionQualifications from "./ProfessionalQualifications"
import PoliticalContribution from "./PoliticalContribution"
import ClientComplaints from "./ClientComplaints"
import SupervisoryObligations from "./SupervisoryObligation"
import BusinessConduct from "./BusinessConduct"
import GeneralAdmin from "./GeneralAdmin"
import GiftsGratuities from "./GiftsGratuities"
import CACMain from "../../FunctionalComponents/CAC/CACMain"
import {checkSupervisorControls} from "../../StateManagement/actions/CreateTransaction"
import {getAllTenantUserDetails} from "../../../globalutilities/helpers"
import {activeStyle} from "../../../globalutilities/consts"


class NewCompliance extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      TABS: {
        "cmp-sup-general" : [
          { path: "admin", label: "Administration" },
          { path: "record-keeping", label: "Recordkeeping" },
          { path: "audit", label: "Activity Log" }
        ],
        "cmp-sup-prof" : [
          { path: "associate-persons", label: "Associated Persons" },
          { path: "record-keeping", label: "Recordkeeping" },
          { path: "audit", label: "Activity Log" }
        ],
        "cmp-sup-busconduct" : [
          { path: "business-conduct", label: "Business Conduct" },
          { path: "audit", label: "Activity Log" }
        ],
        "cmp-sup-gifts" : [
          { path: "instructions", label: "Instructions" },
          { path: "user-view", label: "Disclosure (User View)" },
          { path: "supervisor-view", label: "Disclosure (Supervisor View)" },
        ],
        "cmp-sup-political" : [
          { path: "instructions", label: "Instructions" },
          { path: "user-view", label: "Disclosure (User View)" },
          { path: "supervisor-view", label: "Disclosure (Supervisor View)" },
        ],
        "cmp-sup-complaints" : [
          { path: "admin", label: "G-10 Administration" },
          { path: "recordkeeping", label: "G-10 Recordkeeping" },
          { path: "complaints", label: "Client Complaints" },
          { path: "audit", label: "Activity Log" }
        ],
        "cmp-sup-compobligations" : [
          { path: "designated-people", label: "Designated People" },
          { path: "recordkeeping", label: "Recordkeeping" },
          { path: "audit", label: "Activity Log" },
        ],
        "cac" : [
          { path: "monitor", label: "Control Center Monitor" },
          { path: "admin", label: "Control Administration" },
          { path: "activity", label: "CAC - User Activity" }
        ]
      },
      svControls: {},
      participants: [],
      loading: true,
      tiles: [
        { path: "cmp-sup-general", label: "General Administration Activities", subTitle1: "MSRB Rule A-11, A-12, G-8", subTitle2: "SEC Rule 15B(a)1-8", row: 1, sidebarPath: "admin" },
        { path: "cmp-sup-prof", label: "Professional Qualifications", subTitle1: "MSRB Rule G-2, G-3, G-8, G-9", subTitle2: "SEC Rule 15B(a)1-8", row: 1, sidebarPath: "associate-persons" },
        { path: "cmp-sup-busconduct", label: "Business Conduct Activities", subTitle1: "MSRB Rule G-5, G-17", subTitle2: "SEC Rule 15B(c)2-4", row: 1, sidebarPath: "business-conduct" },
        { path: "cmp-sup-gifts", label: "Gifts and Gratuities", subTitle1: "MSRB Rule G-20, G-8, G-9", subTitle2: "", row: 1, sidebarPath: "instructions" },
        { path: "cmp-sup-political", label: "Political Contributions and Prohibitions", subTitle1: "MSRB Rule G-37, G-8, G-9", subTitle2: "", row: 2, sidebarPath: "instructions" },
        { path: "cmp-sup-complaints", label: "Client Education & Protection", subTitle1: "MSRB Rule G-8, G-9, G-10", subTitle2: "", row: 2, sidebarPath: "admin" },
        { path: "cmp-sup-compobligations", label: "Supervisory and Compliance Obligations", subTitle1: "MSRB Rule G-8, G-9, G-44(c)(d)", subTitle2: "SEC Rule 15B(a)1-8", row: 2, sidebarPath: "designated-people" },
        { path: "cac", label: "Control Center / Supervisory System", subTitle1: "MSRB Rule G-44(a)(b)", row: 2, sidebarPath: "monitor" },
      ],
      isSideBarOpen: false,
    }
  }

  componentWillMount = async () => {
    const { nav2, audit } = this.props
    const { TABS } = this.state
    const svControls = await checkSupervisorControls()
    svControls.canSupervisorEdit = true

    const auditTab = ["cmp-sup-general", "cmp-sup-prof", "cmp-sup-busconduct", "cmp-sup-complaints", "cmp-sup-compobligations"]
    if (auditTab.includes(nav2)){
      const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
      if (!submitAudit) {
        const index = TABS[nav2].findIndex(tab => tab.path === "audit")
        if(index !== -1){
          TABS[nav2].splice(index, 1)
        }
      }
    }

    const tabPath = (nav2 === "cmp-sup-gifts" || "cmp-sup-political" || "cmp-sup-complaints") ? nav2 : ""
    if(tabPath){
      if (svControls.supervisor) {
        const index = TABS[tabPath].findIndex(tab => tab.path === "user-view")
        if(index !== -1){
          TABS[tabPath].splice(index, 1)
        }
      } else {
        const index = TABS[tabPath].findIndex(tab => tab.path === "supervisor-view")
        if(index !== -1){
          TABS[tabPath].splice(index, 1)
        }
        if(nav2 === "cmp-sup-complaints"){
          TABS[tabPath] = []
          const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
          if(submitAudit){
            TABS[tabPath].push(
              { path: "complaints", label: "Client Complaints" },
              { path: "audit", label: "Activity Log" }
            )
          } else {
            TABS[tabPath].push(
              { path: "complaints", label: "Client Complaints" }
            )
          }
        }
      }
    }
    this.setState({
      svControls,
      loading: false
    }, () => this.onParticipantsRefresh())
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await getAllTenantUserDetails()
    })
  }

  handleSideBarToggle = () => {
    this.setState({
      isSideBarOpen: !this.state.isSideBarOpen,
    })
  }

  renderTabs = (tabs, option) =>
    tabs.map(t => {
      console.log("The selected option is", option)
      return (
        <li
          key={t.path}
          className={t.path === option ? "is-active" : "inactive-tab"}
        >
          <NavLink to={`/compliance/${option}/${t.path}`} activeStyle={activeStyle}>
            {t.label}
          </NavLink>
        </li>
      )
    })

  renderViewSelection = (option) =>{
    const { TABS } = this.state
    return (
      <div className="tabs is-boxed">
        <ul>{this.renderTabs(TABS[option], option)}</ul>
      </div>
    )
  }

  renderSelectedView = (option) => {
    const { svControls, participants, TABS } = this.state

    switch (option) {
    case "cmp-sup-prof":
      return <ProfessionQualifications {...this.props} svControls={svControls} participants={participants}/>
    case "cmp-sup-gifts":
      return <GiftsGratuities {...this.props} svControls={svControls} participants={participants}/>
    case "cmp-sup-political":
      return <PoliticalContribution {...this.props} svControls={svControls} participants={participants}/>
    case "cmp-sup-complaints":
      return <ClientComplaints {...this.props} svControls={svControls} participants={participants}/>
    case "cmp-sup-compobligations":
      return <SupervisoryObligations {...this.props} svControls={svControls} participants={participants}/>
    case "cmp-sup-busconduct":
      return <BusinessConduct {...this.props} svControls={svControls} participants={participants}/>
    case "cmp-sup-general":
      return <GeneralAdmin {...this.props} TABS={TABS} svControls={svControls} participants={participants}/>
    case "cac":
      return <CACMain {...this.props}/>
    default:
      return option
    }
  }

  render() {
    const { nav2 } = this.props
    const { svControls, isSideBarOpen, tiles, participants, TABS } = this.state
    if (this.state.loading) {
      return <Loader />
    }
    return (
      <div>
        {
          !nav2 ?
            <div className="overflow-auto" style={{ padding: 12 }}>
              <SuperVisorTabs {...this.props} TILES={tiles} TABS={TABS} svControls={svControls}/>
            </div>
            :
            <div>
              <div className="hero is-link">
                <div className="hero-foot hero-footer-padding">
                  <div className="container">
                    {this.renderViewSelection(nav2)}
                  </div>
                </div>
              </div>
              <div style={{ minWidth: 150 }}>
                <div className="switchContainer">
                  <span className="filterText"> Side Menu </span>
                  <label className="customswitch" style={{ "marginLeft": "10px" }}>
                    <input type="checkbox" onChange={this.handleSideBarToggle} checked={isSideBarOpen} />
                    <span className="customslider round" />
                  </label>
                  <Link to="/compliance"><i className="fa fa-home" title="Compliance Home" style={{fontSize: 25, paddingLeft: 20}} /></Link>
                </div>
                <div className="columns">
                  <SidebarView {...this.props} svControls={svControls} participants={participants} showSidebar={isSideBarOpen} superVisorCategories={tiles} selectedSuperVisorCategory={nav2} />
                    {this.renderSelectedView(nav2)}
                </div>
              </div>
            </div>
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

export default connect(
  mapStateToProps,
  null
)(NewCompliance)
