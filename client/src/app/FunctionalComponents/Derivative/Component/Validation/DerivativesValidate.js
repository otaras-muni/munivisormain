import Joi from "joi-browser"
import dateFormat from "dateformat"

const derivativeSummary = minDate =>
  Joi.object().keys({
    tranIssuer: Joi.string()
      .allow("",null)
      .optional(),
    tranProjectDescription: Joi.string()
      .allow("",null)
      .optional(),
    tranType: Joi.string().required(),
    tranStatus: Joi.string()
      .allow("",null)
      .optional(),
    tranBorrowerName: Joi.string()
      .allow("",null)
      .optional(),
    tranGuarantorName: Joi.string()
      .allow("",null)
      .optional(),
    tranNotionalAmt: Joi.number().min(0).required(),
    tranNotionalAmtFlag: Joi.boolean()
      .required()
      .optional(),
    tranTradeDate: Joi.date()
      .example(new Date("2016-01-01"))
      /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
      .max(Joi.ref("tranEndDate"))
      .required(),
    tranEffDate: Joi.date()
      .example(new Date("2016-01-01"))
      /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
      .max(Joi.ref("tranEndDate"))
      .required(),
    tranEndDate: Joi.date()
      .example(new Date("2016-01-01"))
      /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
      .required(),
    tranCounterpartyClient: Joi.string().required(),
    tranCounterpartyDealer: Joi.string().required(),
    tranCounterpartyType: Joi.string()
      .allow("",null)
      .optional(),
    tranLastMTMValuation: Joi.number()
      .allow("",null)
      .min(0)
      .optional(),
    tranEstimatedRev: Joi.number().min(0)
      .required(),
    tranLastMTMDate: Joi.date()
      .example(new Date("2016-01-01"))
      /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
      .allow(null)
      .optional(),
    tranMTMProvider: Joi.string()
      .allow("",null)
      .optional(),
    createdAt: Joi.date()
      .required()
      .optional(),
    _id: Joi.string()
      .required()
      .optional()
  })

const derivativesSchema = minDate =>
  Joi.object().keys({
    actTranSecondarySector: Joi.string().allow(""),
    actTranPrimarySector: Joi.string().allow(""),
    actTranClientId: Joi.string()
      .required()
      .optional(),
    actTranClientName: Joi.string()
      .required()
      .optional(),
    actTranIssueName: Joi.string().allow(""),
    derivativeSummary: derivativeSummary(minDate)
  })

export const DerivativesValidate = (inputTransDistribute, minDate) =>
  Joi.validate(inputTransDistribute, derivativesSchema(minDate), {
    abortEarly: false,
    stripUnknown: false
  })
