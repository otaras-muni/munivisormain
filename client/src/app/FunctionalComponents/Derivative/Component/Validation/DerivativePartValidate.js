import Joi from "joi-browser";

const derivativePartSchema = Joi.object().keys({
  cntrParty: Joi.string().required(),
  cntrPartyFirmName: Joi.string().required(),
  cntrPartyFirmtype: Joi.string().required(),
  cntrPartyMoodysRating: Joi.string().allow("").optional(),
  cntrPartyFitchRating: Joi.string().allow("").optional(),
  cntrPartyKrollRating: Joi.string().allow("").optional(),
  cntrPartySPRating: Joi.string().allow("").optional(),
  _id: Joi.string().required().optional(),
})

export const DerivativePartValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, derivativePartSchema, { abortEarly: false, stripUnknown:false })
}

const workingGroupSchema = Joi.object().keys({
  partType: Joi.string().required().optional(),
  partFirmId: Joi.string().required().optional(),
  partFirmName: Joi.string().required().optional(), // May not need this
  partContactTitle: Joi.string().allow("").optional(),
  partContactId: Joi.string().required().optional(),
  partContactName: Joi.string().required().optional(),
  partContactEmail: Joi.string().email().required().optional(), // Same reasoning as above
  partContactAddrLine1: Joi.string().allow("").optional(), // Same reasoning as above
  partContactAddrLine2: Joi.string().allow("").optional(), // Same reasoning as above
  participantState: Joi.string().allow("").optional(), // Same reasoning as above
  partContactPhone: Joi.string().required().optional(),
  partContactAddToDL: Joi.boolean().required().optional(),
  createdDate: Joi.date().example(new Date("2016-01-01")).optional(),
  _id: Joi.string().required().optional(),
})

export const WorkingGroupValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, workingGroupSchema, { abortEarly: false, stripUnknown:false })
}
