import Joi from "joi-browser"
import dateFormat from "dateformat"

const derivTradeSwapAndCepSchema = (minDate) => Joi.object().keys({
  payor: Joi.string().label("Payor").required(),
  currency: Joi.string().label("Currency").required(),
  paymentType: Joi.string().label("Payment Type").required(),
  upfrontPayment: Joi.number().label("Upfront Payment").min(0).required(),
  tradeDate: Joi.date().example(new Date("2016-01-01")).max(Joi.ref("periodEndDate")).label("Trade Date").required(),
  effDate: Joi.date().example(new Date("2016-01-01")).max(Joi.ref("periodEndDate")).label("Effective Date").required(),
  fixedRate: Joi.number().label("Fixed Rate").min(0).allow("",null).label("Fixed Rate").optional(),
  floatingRateType: Joi.string().allow("").label("Floating Rate Index").optional(),
  floatingRateRatio: Joi.number().allow("").allow(null).label("Floating Rate Ratio").optional(),
  floatingRateSpread: Joi.number().allow("").allow(null).label("Floating Rate Spread").optional(),
  firstPaymentDate: Joi.date().example(new Date("2016-01-01")) /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */.label("First Payment Date").required().optional(),
  stub: Joi.string().allow("").optional().label("Stub"),
  paymentSchedule: Joi.string().required().optional().label("Payment Schedule"),
  paymentBusConvention: Joi.string().allow("").optional().label("Payment Business Convention"),
  periodEndDate: Joi.date().example(new Date("2016-01-01")).label("Period End Date").required(),
  periodEndDateBusConvention: Joi.string().allow("").optional().label("Period End Date Business Convention"),
  initialIndexSetting: Joi.string().allow("").label("Initial Index Setting").optional(),
  dayCount: Joi.string().allow("").label("Day Count").optional(),
  additionalPayments: Joi.string().label("Additional Payments").allow("").optional(),
  createdAt: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

const derivativeTrade = (minDate) => Joi.object().keys({
  derivTradeDealerPayLeg: derivTradeSwapAndCepSchema(minDate),
  derivTradeClientPayLeg: derivTradeSwapAndCepSchema(minDate)
})

export const TradeValidate = (inputTransDistribute, minDate) => {
  return Joi.validate(inputTransDistribute, derivativeTrade(minDate), { abortEarly: false, stripUnknown:false })
}
