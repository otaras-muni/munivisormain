import React from "react"
import moment from "moment"
import {TextLabelInput, NumberInput, SelectLabelInput, DropDownInput} from "../../../../../GlobalComponents/TextViewBox"

const ClientPayLeg = ({ category, item, errors={}, payor, canEditTran, flag, onChangeItem,  onBlur, dropDown}) => {

  const onChange = (event) => {
    if(event.target && event.target.name === "paymentType") {
      if( event.target.value !== "Fixed") {
        item.fixedRate = ""
      } else if (event.target.value !== "Floating") {
        item.floatingRateType = ""
        item.floatingRateRatio = ""
        item.floatingRateSpread = ""
      }
    }
    onChangeItem({
      ...item,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value || null,
    }, category)
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value){
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  const onSelection = (selectItem) => {
    onChangeItem({
      ...item,
      payor: selectItem.name,
    }, category)
  }

  return (
    <div>

      <div className="columns">
        <DropDownInput label="Payor" required error={errors.payor || ""} filter data={payor || []} value={item.payor || ""} style={{width: 150, fontSize: 12}} message="select payor" textField="name" valueField="id" key="name" onChange={onSelection} disabled={flag}/>
        {/* <TextLabelInput label="Payor" error={(errors.payor && "Required/Valid") || ""} placeholder="auto complete feature box for Issuer/Obligator" name="payor" value={item.payor || ""} onChange={onChange} onBlur={onBlurInput}/> */}
        <SelectLabelInput label="Currency" required error={errors.currency || ""} list={dropDown.currency || []} name="currency" value={item.currency} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <SelectLabelInput label="Payment Type" required error={errors.paymentType || ""} list={dropDown.paymentType || []} name="paymentType" value={(!item.paymentType || item.paymentType === "") ? item.paymentType = "Fixed" : item.paymentType} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
        <NumberInput prefix="$" label="Upfront Payment" required error={errors.upfrontPayment || ""} placeholder="$" name="upfrontPayment" value={item.upfrontPayment || ""} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      {
        item.paymentType === "Floating" ?
          <div className="columns">
            <SelectLabelInput title="Floating Rate Type" label="Floating Rate Index" error={errors.floatingRateType || ""} list={dropDown.floatingRateType || []} name="floatingRateType" value={item.floatingRateType} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>&nbsp;
            <NumberInput title="Floating Rate Ratio" label="Floating Rate Ratio(%)" suffix="%" error={errors.floatingRateRatio || ""} placeholder="Floating Rate Ratio %" name="floatingRateRatio" value={item.floatingRateRatio || ""} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>&nbsp;
            <NumberInput title="Floating Rate Spread" label="Floating Rate Spread(%)" suffix="%" error={errors.floatingRateSpread || ""} placeholder="Floating Rate Spread %" name="floatingRateSpread" value={item.floatingRateSpread || ""} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
          </div>
          : <div className="columns">
            <div className="column is-6">
              <div className="columns">
                <NumberInput prefix="$" label="Fixed Rate" error={errors.fixedRate || ""} disabled={!canEditTran} placeholder="Enter %" name="fixedRate" value={item.fixedRate || ""} onChange={onChange} onBlur={onBlurInput}/>
              </div>
            </div>
          </div>
      }

      <div className="columns">
        <TextLabelInput label="Trade Date" required type="date" error= {errors.tradeDate || ""} name="tradeDate" /* value={item.tradeDate ? moment(new Date(item.tradeDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""} */ value={(item.tradeDate === "" || !item.tradeDate) ? null : new Date(item.tradeDate)}
          disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="Effective Date" required type="date" error= {errors.effDate || ""} name="effDate" /* value={item.effDate ? moment(new Date(item.effDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""} */ value={(item.effDate === "" || !item.effDate) ? null : new Date(item.effDate)}
          disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <SelectLabelInput label="Additional Payments" error={errors.additionalPayments || ""} list={dropDown.additionalPayments || ["Yes", "No"]} name="additionalPayments" value={item.additionalPayments} disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="Period End Date" required type="date" error= {errors.periodEndDate || ""} name="periodEndDate" /* value={item.periodEndDate ? moment(new Date(item.periodEndDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""} */ value={(item.periodEndDate === "" || !item.periodEndDate) ? null : new Date(item.periodEndDate)}
          disabled={!canEditTran} onChange={onChange} onBlur={onBlurInput}/>
      </div>
    </div>
  )
}
export default ClientPayLeg
