import React from "react"
import swal from "sweetalert"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import cloneDeep from "clone-deep"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import CounterParties from "./components/CounterParties"
import ParticipantsView from "../../../../GlobalComponents/ParticipantsView"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {
  DerivativePartValidate,
  WorkingGroupValidate
} from "../Validation/DerivativePartValidate"
import {
  putParticipantsDetails,
  pullParticipantsDetails
} from "../../../../StateManagement/actions"
import { getPicklistByPicklistName, removeWhiteSpaces, generateDistributionPDF } from "GlobalUtils/helpers"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import { postCheckedAddToDL } from "../../../../StateManagement/actions/Common"

const cols = [
  { name: "Firm Type<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
  { name: "Firm Name<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
  { name: "Moody's " },
  { name: "Fitch" },
  { name: "Kroll" },
  { name: "S & P" },
  { name: "Action" }
]

/* const Firm = {
  Dealer: ["Dealer1", "Dealer2", "Dealer3"],
  Other: ["Other1", "Other2", "Other3"],
  "Special Purpose Vehicle": ["SPV1", "SPV2", "SPV3"]
} */

const rate = {
  Fitch: ["AAA", "AA", "A+"],
  Moodys: ["AAA", "AA", "A+"],
  SP: ["AAA", "AA", "A+"],
  Kroll: ["AAA", "AA", "A+"]
}

class DerivativeParticipants extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: "",
      derivativeCounterparties: [CONST.Derivative.derivativeCounterparties],
      derivativeParticipants: [],
      tempDrivativeCounterparties: [CONST.Derivative.derivativeCounterparties],
      isEditable: {},
      underWritingFirms: [],
      filterUser: [],
      participantsType: [],
      errorMessages: {},
      dropDown: {
        rate
      },
      loading: true,
      isSaveDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      confirmAlert: CONST.confirmAlert,
      jsonSheets: [],
      startXlDownload: false
    }
  }

  async componentWillMount() {
    const { transaction, nav2 } = this.props
    const firmId = (transaction && transaction.actTranFirmId) || ""
    let result = await getPicklistByPicklistName(["LKUPRATING", "LKUPCOUNTERPARTYFIRMTYPE"])
    const partRating = (result && result[2].LKUPRATING) || {};

    if (nav2) {
      this.setState(prevState => ({
        dropDown: {
          ...this.state.dropDown,
          rate,
          partRating,
          firm: (result && result[1] && result[1].LKUPCOUNTERPARTYFIRMTYPE) || [],
        },
        derivativeCounterparties:
          transaction && transaction.derivativeCounterparties.length
            ? cloneDeep(transaction.derivativeCounterparties)
            : [CONST.Derivative.derivativeCounterparties],
        derivativeParticipants:
          transaction && transaction.derivativeParticipants.length
            ? cloneDeep(transaction.derivativeParticipants)
            : [],
        tempDrivativeCounterparties:
          transaction && transaction.derivativeCounterparties.length
            ? cloneDeep(transaction.derivativeCounterparties)
            : [CONST.Derivative.derivativeCounterparties],
        isEditable: {
          derivativeCounterparties:
            transaction && transaction.derivativeCounterparties.length ? "" : 0
        },
        loading: false,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.actTranIssueName ||
            transaction.actTranProjectDescription} - Notification`
        }
      }))
    } else {
      this.setState({ loading: false })
    }
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items
    })
  }

  onReset = key => {
    const auditLogs = this.props.auditLogs.filter(x => x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState(prevState => ({
      loading: false,
      [key]:
        key === "derivativeCounterparties"
          ? cloneDeep(prevState.tempDrivativeCounterparties)
          : key === "derivativeParticipants"
          ? cloneDeep(prevState.tempDerivativeParticipants)
          : [],
      isEditable: {
        ...prevState.isEditable,
        [key]: ""
      }
    }))
  }

  onAdd = key => {
    const { userName, isEditable } = this.state
    const insertRow = cloneDeep(this.state[key])
    insertRow.push(CONST.Derivative[key])
    this.props.addAuditLog({
      userName,
      log: `${
        key === "derivativeCounterparties" ? "CounterParties" : key
      } Add new Item`,
      date: new Date(),
      key
    })

    if (isEditable[key] || isEditable[key] === 0) {
      return swal("Warning", `First save the current ${key}`, "warning")
    }
    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: insertRow.length ? insertRow.length - 1 : 0
      }
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({
      userName,
      log: `${change} from participants`,
      date: new Date(),
      key: category
    })
  }

  actionButtons = (key, canEditTran, isSaveDisabled) => {
    //eslint-disable-line
    const isDisabled = key === "derivativeCounterparties"
    if (!canEditTran) return
    return (
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
            disabled={isSaveDisabled}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset(key)}
            disabled={isSaveDisabled}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  onEdit = (key, index) => {
    this.props.addAuditLog({
      userName: this.state.userName,
      log: `working group In ${key} one row edited`,
      date: new Date(),
      key
    })
    if (this.state.isEditable[key]) {
      swal(
        "Warning",
        `First save the current ${
          key === "derivativeCounterparties" ? "Counterparties" : key
        }`,
        "warning"
      )
      return
    }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index
      }
    })
  }

  onCancel = key => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: ""
      }
    })
  }

  onRemoveParticipants = (removeId, type, index, firmName) => {
    const derivativeId = this.props.nav2
    const { userName, isEditable, confirmAlert } = this.state
    const delObject = cloneDeep(this.state[type])
    if (delObject.length === 1) {
      swal("Warning", `Minimum one row is required`, "warning")
      return
    }

    confirmAlert.text = `You want to delete this ${
      type === "derivativeCounterparties" ? "counterParties" : type
    }?`
    swal(confirmAlert).then(willDelete => {
      if (type && derivativeId && removeId) {
        this.props.addAuditLog({
          userName,
          log: `Remove participants ${firmName} from ${
            type === "derivativeCounterparties" ? "CounterParties" : type
          }`,
          date: new Date(),
          key: type
        })
        if (willDelete) {
          pullParticipantsDetails(
            type === "derivativeCounterparties"
              ? "counterParties"
              : "workingGroup",
            this.props.nav2,
            removeId,
            res => {
              if (res && res.status === 200) {
                toast("Removed CounterParties successfully", {
                  autoClose: CONST.ToastTimeout,
                  type: toast.TYPE.SUCCESS
                })
                this.props.submitAuditLogs(this.props.nav2)
                this.setState({
                  [type]:
                    res.data && res.data[type].length
                      ? cloneDeep(res.data[type])
                      : [],
                  tempDrivativeCounterparties:
                    res.data && res.data.derivativeCounterparties.length
                      ? cloneDeep(res.data.derivativeCounterparties)
                      : [CONST.Derivative.derivativeCounterparties],
                  tempDerivativeParticipants:
                    res.data && res.data.derivativeParticipants.length
                      ? cloneDeep(res.data.derivativeParticipants)
                      : [CONST.Derivative.derivativeParticipants],
                  isEditable: {
                    ...isEditable,
                    [type]: res.data && res.data[type].length ? "" : 0
                  },
                  errorMessages: {}
                })
              } else {
                toast("something went wrong", {
                  autoClose: CONST.ToastTimeout,
                  type: toast.TYPE.ERROR
                })
              }
            }
          )
        }
      } else {
        const participants = this.state[type]
        if (willDelete && participants.length) {
          participants.splice(index, 1)
          this.setState({
            [type]: participants,
            isEditable: {
              ...isEditable,
              [type]: ""
            },
            errorMessages: {}
          })
        }
      }
    })
  }

  onSave = (key, participant, index) => {
    const { isEditable } = this.state
    let errors = {}
    if (key === "derivativeCounterparties") {
      errors = DerivativePartValidate(participant)
    } else {
      errors = WorkingGroupValidate(participant)
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        const message = "Required"
        if (errorMessages.hasOwnProperty(key)) {
          //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) {
            //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    const counterPartyList = this.state.derivativeCounterparties.map(
      item => item.cntrPartyFirmName
    )
    const isDuplicate = counterPartyList.some(
      (item, idx) => counterPartyList.indexOf(item) !== idx
    )

    if (isDuplicate) {
      toast("You Enter Duplicate Firm Name", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      return
    }

    this.setState(
      {
        isSaveDisabled: true
      },
      () => {
        putParticipantsDetails(
          key === "derivativeCounterparties"
            ? "counterParties"
            : "workingGroup",
          this.props.nav2,
          participant,
          res => {
            if (res && res.status === 200) {
              toast("Added Counter Parties successfully", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  [key]:
                    res.data && res.data[key].length
                      ? cloneDeep(res.data[key])
                      : [],
                  isEditable: {
                    ...isEditable,
                    [key]: res.data && res.data[key].length ? "" : 0
                  },
                  errorMessages: {},
                  isSaveDisabled: false
                })
            } else {
              toast("something went wrong", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false
              })
            }
          }
        )
      }
    )
  }

  onRemove = (removeId, callback) => {
    pullParticipantsDetails(this.props.nav3, this.props.nav2, removeId, res => {
      if (res && res.status === 200) {
        toast("Removed participant successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.props.submitAuditLogs(this.props.nav2)
        this.setState(
          {
            derivativeParticipants:
              (res.data && res.data.derivativeParticipants) || []
          },
          () => {
            callback({
              status: true,
              participants: (res.data && res.data.derivativeParticipants) || []
            })
          }
        )
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onSavePart = (participant, callback) => {
    putParticipantsDetails(
      this.props.nav3,
      this.props.nav2,
      participant,
      res => {
        if (res && res.status === 200) {
          toast("Added Working Group successfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS
          })
          this.props.submitAuditLogs(this.props.nav2)
          this.setState(
            {
              derivativeParticipants:
                (res.data && res.data.derivativeParticipants) || []
            },
            () => {
              callback({
                status: true,
                participants:
                  (res.data && res.data.derivativeParticipants) || []
              })
            }
          )
        } else {
          toast("Something went wrong!", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
          callback({
            status: false
          })
        }
      }
    )
  }

  handleToggle = () => {
    this.setState(pre => ({
      modalState: !pre.modalState
    }))
  }

  onModalSave = () => {
    const { email } = this.state
    const { nav1, nav2 } = this.props
    const emailPayload = {
      tranId: nav2,
      type: nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState({
      modalState: false
    }, async () => {
      await sendEmailAlert(emailPayload)
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  distributePDFDownload = async () => {
    let { derivativeParticipants } = this.state
    const { derivativeCounterparties } = this.state
    const { logo, transaction, user } = this.props
    const partTypeQueue = ["Issuer", "Bond Counsel", "Municipal Advisor"]

    const data = []
    const participantsData = []

    if(derivativeParticipants && derivativeParticipants.length || derivativeCounterparties && derivativeCounterparties.length){
      derivativeParticipants.sort((a, b) => {
        const nameA = a && a.partType && a.partType.toLowerCase()
        const nameB = b && b.partType && b.partType.toLowerCase()
        if (nameA < nameB)
          return -1
        if (nameA > nameB)
          return 1
        return 0
      })

      partTypeQueue.forEach(partType => {
        derivativeParticipants.forEach(d => {
          if(d && d.partType === partType && d.addToDL){
            data.push({
              stack: [
                { text: d && d.partType && d.partType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
                { text: `Name: ${d.partContactName || ""}`, bold: true, fontSize: 10},
                { text: `Address: ${d.partContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
                { text: `Phone: ${d.partContactPhone || ""}`, fontSize: 10},
                { text: `Fax: ${d.partContactFax || ""}`, fontSize: 10},
                { text: `E-Mail: ${d.partContactEmail || ""}`, fontSize: 10}
              ]
            })
          }
        })
      })

      derivativeParticipants = derivativeParticipants.filter(d => (d && partTypeQueue.indexOf(d.partType) === -1  && d.addToDL))

      derivativeCounterparties.forEach(d => {
        if(d && (d.cntrPartyFirmName && d.cntrPartyFirmtype)){
          data.push({
            stack: [
              { text: d && d.cntrPartyFirmName && d.cntrPartyFirmName.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
              { text: `Firm Type: ${d.cntrPartyFirmtype || ""}`, bold: true, fontSize: 10},
              { text: `Moody's: ${d.cntrPartyMoodysRating || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
              { text: `Fitch: ${d.cntrPartyFitchRating || ""}`, fontSize: 10},
              { text: `Kroll: ${d.cntrPartyKrollRating || ""}`, fontSize: 10},
              { text: `S & P: ${d.cntrPartySPRating || ""}`, fontSize: 10}
            ]
          })
        }
      })

      derivativeParticipants.forEach(d => {
        if(d && d.addToDL){
          data.push({
            stack: [
              { text: d && d.partType && d.partType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
              { text: `Name: ${d.partContactName || ""}`, bold: true, fontSize: 10},
              { text: `Address: ${d.partContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
              { text: `Phone: ${d.partContactPhone || ""}`, fontSize: 10},
              { text: `Fax: ${d.partContactFax || ""}`, fontSize: 10},
              { text: `E-Mail: ${d.partContactEmail || ""}`, fontSize: 10}
            ]
          })
        }
      })
    }

    if(data && data.length){
      const stackData = []
      let duplicateData = []
      let dataIndex = 0
      data.forEach(d => {
        if(duplicateData.length < 2){
          dataIndex = dataIndex + 1
          const index = [dataIndex - 1]
          duplicateData.push(data[index])
          if(duplicateData.length === 1){
            stackData.push(duplicateData)
          }
        }
        if (duplicateData.length === 2){
          duplicateData = []
        }
      })
      if(stackData && stackData.length){
        stackData.forEach(stack => {
          if(stack && stack.length === 1){
            stack.push({stack: []})
          }
          participantsData.push({columns: stack}, {marginTop:10, text: [ "" ]})
        })
      }
      await generateDistributionPDF({
        logo,
        firmName: user && user.firmName && user.firmName.toUpperCase() || "",
        clientName: transaction && transaction.actTranClientName && transaction.actTranClientName.toUpperCase() || "",
        transactionType: transaction && transaction.actTranSubType || "",
        data: participantsData,
        fileName: `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} Participants.pdf`
      })
    } else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  pdfDownload = () => {
    const {transaction, user, logo} = this.props
    const position = user && user.settings
    const { derivativeCounterparties, derivativeParticipants } = this.state
    const pdfCounterParties = []
    const pdfParticipants = []
    if(derivativeCounterparties[0] && derivativeCounterparties[0]._id){
      derivativeCounterparties &&
      derivativeCounterparties.forEach(item => {
        pdfCounterParties.push([
          item.cntrPartyFirmName || "--",
          item.cntrPartyFirmtype || "--",
          item.cntrPartyMoodysRating || "--",
          item.cntrPartyFitchRating || "--",
          item.cntrPartyKrollRating || "--",
          item.cntrPartySPRating || "--"
        ])
      })
    }

    derivativeParticipants &&
      derivativeParticipants.forEach(item => {
        const address1 = removeWhiteSpaces(item.partContactAddrLine1 || "")
        if (item.addToDL) {
          pdfParticipants.push([
            item.partType || "--",
            item.partFirmName || "--",
            item.partContactName || "--",
            item.partContactPhone || "--",
            item.partContactEmail || "--",
            item.addToDL ? "Yes" : "No" || "--",
            address1 || "--"
          ])
        }
      })
    const tableData = []
    if (pdfCounterParties.length > 0) {
      tableData.push({
        titles: ["Derivative CounterParties"],
        description: "",
        headers: [
          "Firm name",
          "Firm Type",
          "Moody's Rating",
          "Fitch Rating",
          "Kroll Rating",
          "SP Rating"
        ],
        rows: pdfCounterParties
      })
    }
    if (pdfParticipants.length > 0) {
      tableData.push({
        titles: ["Derivative WorkingGroup"],
        description: "",
        headers: [
          "Participant Type",
          "Firm name",
          "Contact Name",
          "Phone",
          "Email",
          "Add to DL",
          "Address"
        ],
        rows: pdfParticipants
      })
    }
    pdfTableDownload("", tableData, `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} participants.pdf`, logo, position)
  }

  xlDownload = () => {
    const { transaction } = this.props
    const { derivativeCounterparties, derivativeParticipants } = this.state
    const pdfUnderWriter = []
    const pdfParticipants = []
    derivativeCounterparties &&
      derivativeCounterparties.map(item => {
        const data = {
          "Firm Name": item.cntrPartyFirmName || "--",
          "Part Type": item.cntrPartyFirmtype || "--",
          "Moody's Rating": item.cntrPartyMoodysRating || "--",
          "Fitch Rating": item.cntrPartyFitchRating || "--",
          "Kroll Rating": item.cntrPartyKrollRating || "--",
          "SP Rating": item.cntrPartySPRating || "--"
        }
        pdfUnderWriter.push(data)
      })

    derivativeParticipants &&
    derivativeParticipants.map(item => {
      const data = {
        "Add to DL": item.addToDL || false,
        "Firm Name": item.partFirmName || "--",
        "Part Type": item.partType || "--",
        "Name": item.partContactName || "--",
        "Email": item.partContactEmail || "--",
        "Phone": item.partContactPhone || "--",
        "Address": item.partContactAddrLine1 || "--"
      }
      pdfParticipants.push(data)
    })
    const jsonSheets = [
      {
        name: "UnderWriter",
        headers: [
          "Firm Name",
          "Part Type",
          "Moody's Rating",
          "Fitch Rating",
          "Kroll Rating",
          "SP Rating"
        ],
        data: pdfUnderWriter
      },
      {
        name: "Participants",
        headers: [
          "Add to DL",
          "Firm Name",
          "Part Type",
          "Name",
          "Email",
          "Phone",
          "Address"
        ],
        data: pdfParticipants
      }
    ]
    this.setState(
      {
        jsonSheets,
        startXlDownload: true
      },
      () => this.resetXLDownloadFlag
    )
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  onCheckAddTODL = async (
    e,
    item,
    participantType,
    type,
    tranId,
    payload,
    callback
  ) => {
    const { derivativeParticipants } = this.state
    const res = await postCheckedAddToDL(type, tranId, payload)
    if (res && res.status === 200 && res.data && res.data.updateParticipant) {
      const partIndex = derivativeParticipants.findIndex(
        part => part._id === item._id
      )
      derivativeParticipants[partIndex].addToDL =
        res.data.updateParticipant && res.data.updateParticipant.addToDL
      this.setState(
        {
          derivativeParticipants
        },
        () => {
          callback({
            status: true,
            participants: derivativeParticipants || []
          })
        }
      )
    }
  }

  render() {
    const {
      derivativeParticipants,
      modalState,
      email,
      derivativeCounterparties,
      dropDown,
      errorMessages,
      participantsType,
      isSaveDisabled,
      isEditable
    } = this.state
    const {
      transaction,
      nav2,
      tranAction,
      participants,
      onParticipantsRefresh
    } = this.props
    const loading = () => <Loader />
    let isDisabledDownload = (derivativeCounterparties[0] && derivativeCounterparties[0]._id) ||
                             (derivativeParticipants && derivativeParticipants.length)
    if (this.state.loading) {
      return loading()
    }
    return (
      <div className="participants derivative-counterparties">
        {tranAction && tranAction.canEditTran ? (
          <div className="columns">
            <div className="column">
              <button
                className="button is-link is-small"
                onClick={this.handleToggle}
              >
                Send Email Alert
              </button>
            </div>
            <div className="column">
              <div className="field is-grouped">
                <div className={`${!isDisabledDownload ? "control isDisabled" : "control"}`} onClick={this.distributePDFDownload}>
                  <span className="has-text-link">
                    <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                  </span>
                </div>
                {/* <div className={`${!isDisabledDownload ? "control isDisabled" : "control"}`} onClick={this.pdfDownload}>
                  <span className="has-link">
                    <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                  </span>
                </div> */}
                <div className={`${!isDisabledDownload ? "control isDisabled" : "control"}`} onClick={this.xlDownload}>
                  <span className="has-link">
                    <i className="far fa-2x fa-file-excel has-text-link" title="Excel Download"/>
                  </span>
                </div>
                <div style={{ display: "none" }}>
                  <ExcelSaverMulti
                    label={`${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} participants`}
                    startDownload={this.state.startXlDownload}
                    afterDownload={this.resetXLDownloadFlag}
                    jsonSheets={this.state.jsonSheets}
                  />
                </div>
              </div>
            </div>
          </div>
        ) : null}
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={this.onModalSave}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="CounterParties"
                actionButtons={this.actionButtons(
                  "derivativeCounterparties",
                  tranAction.canEditTran || false,
                  isSaveDisabled
                )}
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(0) && (
                  <div className="tbl-scroll">
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={tranAction.canEditTran ? cols : cols}/>
                      {Array.isArray(derivativeCounterparties) &&
                      derivativeCounterparties.length
                        ? derivativeCounterparties.map((participant, i) => {
                          const errors = (errorMessages && errorMessages.derivativeCounterparties && errorMessages.derivativeCounterparties[i.toString()]) || {}
                          return (
                            <CounterParties
                              key={i}
                              category="derivativeCounterparties"
                              index={i}
                              partType={participantsType}
                              errors={errors}
                              isEditable={isEditable}
                              participant={participant}
                              onItemChange={this.onItemChange}
                              isSaveDisabled={isSaveDisabled}
                              canEditTran={tranAction.canEditTran || false}
                              onBlur={this.onBlur}
                              dropDown={dropDown}
                              onRemove={this.onRemoveParticipants}
                              onEdit={this.onEdit}
                              onSave={this.onSave}
                              onCancel={this.onCancel}
                            />
                          )
                        })
                        : null}
                    </table>
                  </div>
                )}
              </RatingSection>
              <br />
              <ParticipantsView
                id={nav2}
                transaction={transaction}
                firmId={(transaction && transaction.actTranFirmId) || ""}
                title="Working Group"
                onRemove={this.onRemove}
                type="Derivatives"
                onSave={this.onSavePart}
                participants={derivativeParticipants}
                canEditTran={tranAction.canEditTran || false}
                emailInfo={email}
                onCheckAddTODL={this.onCheckAddTODL}
              />
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(DerivativeParticipants)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
