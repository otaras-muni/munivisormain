import React from "react"
import { NavLink } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "../../../GlobalComponents/Loader"
import DerivativeDetails from "./DerivativeDetails"
import { fetchTransaction } from "../../../StateManagement/actions"
import { makeEligibleTabView } from "../../../../globalutilities/helpers"
import Summary from "./Summary"
import Participants from "./Participants"
import TradeSwap from "./TradeSwap"
import TradeCap from "./TradeCap"
import Documents from "./Documents"
import Audit from "../../../GlobalComponents/Audit"
import CONST, { activeStyle } from "../../../../globalutilities/consts"
import { fetchParticipantsAndOtherUsers } from "../../../StateManagement/actions/Transaction"

const TABS1 = [
  { path: "summary", label: "Summary" },
  { path: "details", label: "Details" },
  { path: "participants", label: "Participants" },
  { path: "tradeSwap", label: "Trade (Swap)" },
  { path: "tradeCap", label: "Trade (Cap)" },
  { path: "documents", label: "Documents" },
  { path: "audit-trail", label: "Activity Log" }
]

class DerivativeView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs: [],
      isSummary: false,
      loading: true,
      transaction: {},
      tranAction: {},
      TABS: [
        { path: "summary", label: "Summary" },
        { path: "details", label: "Details" },
        { path: "participants", label: "Participants" },
        { path: "tradeSwap", label: "Trade (Swap)" },
        { path: "tradeCap", label: "Trade (Cap)" },
        { path: "documents", label: "Documents" },
        { path: "audit-trail", label: "Activity Log" }
      ]
    }
  }

  componentWillMount() {
    const { user, nav, nav1, nav2, loginEntity } = this.props
    const tranId = this.props.nav2 || ""
    const type = this.props.nav3 || ""
    const { TABS } = this.state
    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])
    if (tranId) {
      fetchTransaction(type, tranId, async transaction => {
        if (transaction && transaction.actTranClientId) {
          const isEligible = await makeEligibleTabView(
            user,
            tranId,
            TABS,
            allEligibleNav,
            transaction.actTranClientId,
            transaction.actTranFirmId,
          )

          isEligible.tranAction.canEditTran = CONST.oppDecline.indexOf(transaction.actTranStatus) !== -1 ? false : isEligible.tranAction.canEditTran

          isEligible.tranAction.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.actTranStatus) === -1 && isEligible.tranAction.canEditDocument

          // if(transaction.derivativeParticipants && loginEntity && loginEntity.relationshipToTenant !== "Self"){
          //   transaction.derivativeParticipants = transaction.derivativeParticipants.filter(part => part.partFirmId === loginEntity.entityId)
          // }

          console.log("=================tranAction================", isEligible)
          if (isEligible && isEligible.tranAction && (!isEligible.tranAction.canEditTran && !isEligible.tranAction.canViewTran)) {
            this.props.history.push("/dashboard")
          } else {
            if (isEligible.tranAction.view.indexOf(type) === -1) {
              const view = isEligible.tranAction.view[0] || ""
              this.props.history.push(`/${nav1}/${tranId}/${view}`)
            } else {
              const participants = await fetchParticipantsAndOtherUsers(nav2)
              if (transaction.actTranSubType === "Swap") {
                const index = isEligible.tabs.findIndex(tab => tab.path === "tradeCap")
                if(index !== -1){
                  isEligible.tabs.splice(index, 1)
                }
              }
              if (transaction.actTranSubType === "Cap") {
                const index = isEligible.tabs.findIndex(tab => tab.path === "tradeSwap")
                if(index !== -1){
                  isEligible.tabs.splice(index, 1)
                }
              }
              this.setState({
                transaction,
                participants,
                loading: false,
                tranAction: isEligible.tranAction,
                tabs: isEligible.tabs
              })
            }
          }
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  onStatusChange = status => {
    const { tranAction } = this.state
    tranAction.canEditTran =
      CONST.oppDecline.indexOf(status) !== -1 ? false : tranAction.canEditTran
    this.setState({
      tranAction
    })
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderTabs = (tabs, derivativeId, option) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink
          to={`/derivative/${derivativeId}/${t.path}`}
          activeStyle={activeStyle}
        >
          {t.label}
        </NavLink>
      </li>
    ))

  renderViewSelection = (derivativeId, option) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, derivativeId, option)}</ul>
    </nav>
  )

  renderSelectedView = (derivativeId, option) => {
    const { transaction, tranAction, participants } = this.state

    switch (option) {
    case "details":
      return (
        <DerivativeDetails
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "summary":
      return (
        <Summary
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
          onStatusChange={this.onStatusChange}
        />
      )
    case "participants":
      return (
        <Participants
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "tradeSwap":
      return (
        <TradeSwap
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "tradeCap":
      return (
        <TradeCap
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
        />
      )
    case "audit-trail":
      return <Audit {...this.props} />
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { option } = this.props
    const { transaction } = this.state
    const derivativeId = transaction && transaction._id
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(derivativeId, option)}
            </div>
          </div>
        </div>
        {/* <section className="hero is-small is-link">
          <div className="hero-body">
            <div className="columns is-vcentered">
              <div className="column">
                <div className="subtitle">
                  <nav className="breadcrumb has-arrow-separator is-small is-right" aria-label="breadcrumbs">
                    <ul>
                      <li>
                        Derivative
                      </li>
                      <li className="is-active">
                        {option}
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>

          <div className="hero-foot">
            <div className="container">
              {this.renderViewSelection(derivativeId, option)}
            </div>
          </div>
        </section> */}
        <section id="main">
          {this.renderSelectedView(derivativeId, option)}
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(
  mapStateToProps,
  null
)(DerivativeView)
