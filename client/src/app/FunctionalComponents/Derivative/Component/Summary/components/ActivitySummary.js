import React from "react"
import moment from "moment/moment"
import NumberFormat from "react-number-format"
import BorrowerLookup from "Global/BorrowerLookup"
import ThirdPartyLookup from "Global/ThirdPartyLookup"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"
import { TextLabelInput } from "../../../../../GlobalComponents/TextViewBox"

const ActivitySummary = ({ transaction = {}, onChange, onTextInputChange, onBlur, dropDown, tranTextChange, canEditTran }) => {

  const borrowerName = {
    _id: "",
    firmName: transaction.tranBorrowerName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const guarantorName = {
    _id: "",
    firmName: transaction.tranGuarantorName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"

  return (
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          { transaction.actTranClientName ?
            <small style={{fontSize: 15}}>{getTranEntityUrl("Issuer", transaction.actTranClientName, transaction.actTranClientId, "")}</small> :
            <small>--</small>
          }
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Transaction Name</p>
        </div>
        <div className="column">
          <TextLabelInput title="Transaction Name" name="actTranIssueName" value={tranTextChange.actTranIssueName} onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Project Description (internal)</p>
        </div>
        <div className="column">
          <TextLabelInput title="Project Description" name="actTranProjectDescription" value={tranTextChange.actTranProjectDescription} onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrower or Obligor Name</p>
        </div>
        <div className="column">
          {/* <DropDownInput filter groupBy={(row) => row.type} data={dropDown.borrower || []} value={transaction.tranBorrowerName || ""} style={{ width: 300, fontSize: 12 }} message="select borrower" textField="name"
                         valueField="id" key="name" onChange={(e) => onChange({ target: { name: "tranBorrowerName", value: e.borOblFirmName } })} disabled={!canEditTran || false} /> */}
          <BorrowerLookup
            entityName={borrowerName}
            onChange={(e) => onChange({ target: { name: "tranBorrowerName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.tranBorrowerName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Guarantor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput list={dropDown.Guarantor} name="tranGuarantorName" value={transaction.tranGuarantorName || ""} inputStyle={{ width: 300 }} onChange={onChange} disabled={!canEditTran || false} /> */}
          <ThirdPartyLookup
            entityName={guarantorName}
            onChange={(e) => onChange({ target: { name: "tranGuarantorName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.tranGuarantorName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule highlights</p>
        </div>
        <div className="column">

          <small className="innerSummaryTitle">Client Trade Date (est.):
            <small> {(transaction.derivTradeClientPayLeg && transaction.derivTradeClientPayLeg.tradeDate ? moment(transaction.derivTradeClientPayLeg.tradeDate).format("MM-DD-YYYY") || "" : "")}
            </small>
          </small><br/>
          <small className="innerSummaryTitle">Client Effective Date:
            <small> {(transaction.derivTradeClientPayLeg && transaction.derivTradeClientPayLeg.effDate ? moment(transaction.derivTradeClientPayLeg.effDate).format("MM-DD-YYYY") || "" : "")}
            </small>
          </small><br/>

          <small className="innerSummaryTitle">Dealer Trade Date (est.):
            <small>
              {(transaction.derivTradeDealerPayLeg && transaction.derivTradeDealerPayLeg.tradeDate ? moment(transaction.derivTradeDealerPayLeg.tradeDate).format("MM-DD-YYYY") || "" : "")}
            </small>
          </small><br/>
          <small className="innerSummaryTitle">Dealer Effective Date:
            <small>
              {(transaction.derivTradeDealerPayLeg && transaction.derivTradeDealerPayLeg.effDate ? moment(transaction.derivTradeDealerPayLeg.effDate).format("MM-DD-YYYY") || "" : "")}
            </small>
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Derivative Type</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{(transaction.actTranSubType && transaction.actTranSubType || "")}</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <small className="multiExpLbl">Counterparties</small>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">Dealer:
            {
              transaction.derivativeCounterparties ? transaction.derivativeCounterparties.map((item, i) => (
                <small key={i.toString()}><a> {item.cntrPartyFirmName}{transaction.derivativeCounterparties.length - 1 === i ? "" : ","} </a></small>
              )) : null
            }
          </small><br/>
          <small className="innerSummaryTitle">Client:
            {
              transaction.derivativeCounterparties ? transaction.derivativeCounterparties.map((item, i) => (
                <small key={i.toString()}><a> {item.cntrPartyFirmtype}{transaction.derivativeCounterparties.length - 1 === i ? "" : ","} </a></small>
              )) : null
            }
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notional Amount</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{transaction.derivativeSummary && transaction.derivativeSummary.tranNotionalAmt ?
            <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={transaction.derivativeSummary.tranNotionalAmt} />
            :"---"}
            Amortizing: {transaction.derivativeSummary &&
            transaction.derivativeSummary.tranNotionalAmtFlag ? "Yes" : " No"}
          </small>
        </div>
        {/* <div className="column">
        <p>
          <small>${(transaction.derivativeSummary && transaction.derivativeSummary.tranNotionalAmt) || 0}; Amortizing: {transaction.derivativeSummary &&
            transaction.derivativeSummary.tranNotionalAmtFlag ? "Yes" : " No"}</small>
        </p>
      </div> */}
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Lead Advisors</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{transaction.actTranFirmLeadAdvisorName || "--"}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon / Rate Type:</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{/* {(transaction.bankLoanTerms && transaction.bankLoanTerms.paymentType) || "--"} */} -- Rate
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small>--</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">End Date</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle"> {(transaction.derivativeSummary && transaction.derivativeSummary.tranEndDate ? moment(transaction.derivativeSummary.tranEndDate).format("MM-DD-YYYY") || "" : "")}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Working Group Participants</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">
            {
              transaction.derivativeParticipants ? transaction.derivativeParticipants.map((item, i) => (
                <p key={i.toString()}>
                  { item.partContactId ? (
                    <div style={{fontSize: 15}}>{getTranUserUrl(item.partType, `${item.partType} : ${item.partFirmName} (${item.partContactName})`, item.partContactId, "")}</div>
                  ) :
                    <div style={{fontSize: 15}}>{getTranEntityUrl(item.partType, `${item.partType} : ${item.partFirmName}`, item.partFirmId, "")}</div>
                  }
                </p>
              )) : null
            }
          </small>
        </div>
      </div>

      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notes / Instructions</p>
        </div>
        <div className="column">
          <small>{actNotes || "--"}</small>
        </div>
      </div>
    </div>
  )
}


export default  ActivitySummary
