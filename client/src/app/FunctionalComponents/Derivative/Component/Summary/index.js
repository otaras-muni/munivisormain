import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import moment from "moment"
import swal from "sweetalert"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TranSummaryStatus from "../../../../GlobalComponents/TranSummaryStatus"
import ActivitySummary from "./components/ActivitySummary"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import {
  fetchBorrowerObligorByFirm,
  putDerivativeSummary,
  putTransaction
} from "../../../../StateManagement/actions"
import CONST, { oppMsg } from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import { fetchSupplierContacts } from "../../../../StateManagement/actions/TransactionDistribute"
import { pullRelatedTransactions } from "../../../../StateManagement/actions/CreateTransaction"
import ReletedTranGlob from "../../../../GlobalComponents/ReletedTranGlob"

class Summary extends React.Component {
  constructor() {
    super()
    this.state = {
      derivative: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      },
      tranTextChange: {
        actTranIssueName: "",
        actTranProjectDescription: ""
      },
      errorMessages: {},
      transaction: {},
      tranStatus: [],
      relatedTypes: [],
      loading: true,
      isSaveDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      dropDown: {
        borrower: [],
        Guarantor: []
      },
      relatedTransactions: []
    }
  }

  async componentDidMount() {
    const { transaction } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPSWAPTRANSTATUS",
      "LKUPRELATEDTYPE"
    ])
    const result = (picResult.length && picResult[1]) || {}

    this.setState(
      prevState => ({
        tranStatus: result.LKUPSWAPTRANSTATUS || [],
        relatedTypes: result.LKUPRELATEDTYPE || ["Bond Hedge", "Loan Hedge"],
        tranTextChange: {
          actTranIssueName: transaction.actTranIssueName || "",
          actTranProjectDescription: transaction.actTranProjectDescription || ""
        },
        transaction,
        relatedTransactions: transaction.actTranRelatedTo || [],
        loading: false,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.actTranIssueName ||
            transaction.actTranProjectDescription} - Notification`
        },
        tempTransaction: {
          actTranSecondarySector: transaction.actTranSecondarySector,
          actTranPrimarySector: transaction.actTranPrimarySector,
          actTranClientId: transaction.actTranClientId,
          actTranClientName: transaction.actTranClientName,
          actTranIssueName: transaction.actTranIssueName
        }
      }),
      () => {
        this.getBorrowerByFirm()
      }
    )
  }

  getBorrowerByFirm = () => {
    const { transaction } = this.props
    const firmId = transaction.actTranFirmId
    const { tempTransaction } = this.state
    fetchBorrowerObligorByFirm(
      tempTransaction.actTranClientId || transaction.actTranClientId,
      borrower => {
        this.setState(
          prevState => ({
            dropDown: {
              ...prevState.dropDown,
              borrower
            }
          }),
          () => {
            this.getFirmDetails(firmId)
          }
        )
      }
    )
  }

  getFirmDetails = firmId => {
    fetchSupplierContacts(firmId, "Bond Counsel", allFirms => {
      const name = []
      allFirms && allFirms.suppliersList.map(ent => name.push(ent.name))
      console.log(name)

      this.setState({
        loading: false,
        dropDown: {
          ...this.state.dropDown,
          Guarantor: name
        }
      })
    })
  }

  onRelatedTranSelect = item => {
    const { derivative } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary related type ${
        derivative.actTranRelatedType
      } and  derivative ${item.name || ""}`,
      date: new Date(),
      key: "summary"
    })
    this.setState(prevState => ({
      derivative: {
        ...prevState.derivative,
        actTranRelatedTo: item
      }
    }))
  }

  onChange = e => {
    const { derivative } = this.state
    const { user } = this.props
    derivative[e.target.name] = e.target.value
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary select related type ${e.target.value}`,
      date: new Date(),
      key: "summary"
    })
    this.setState({
      derivative: {
        ...derivative,
        actTranRelatedTo: []
      }
    })
  }

  onModalSave = () => {
    this.setState({
      modalState: true,
      relatedSave: true
    })
  }

  onRelatedtranSave = () => {
    const { email, derivative } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    const actTranRelatedTo = {
      ...derivative.actTranRelatedTo,
      relType: derivative.actTranRelatedType
    }

    if (!derivative.actTranRelatedType || actTranRelatedTo.length === 0)
      return false

    this.setState(
      {
        isSaveDisabled: true
      },
      () => {
        putTransaction(
          this.props.nav3,
          this.props.nav2,
          actTranRelatedTo,
          res => {
            if (res && res.status === 200) {
              toast("Add related transaction successfully", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  transaction: res.data,
                  relatedTransactions:
                    (res.data && res.data.actTranRelatedTo) || [],
                  derivative: {
                    actTranRelatedType: derivative.actTranRelatedType,
                    actTranRelatedTo: ""
                  },
                  isSaveDisabled: false,
                  modalState: false
                },
                async () => {
                  await sendEmailAlert(emailPayload)
                }
              )
            } else {
              toast("something went wrong", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false
              })
            }
          }
        )
      }
    )
  }

  onChangeTextInput = e => {
    const tranTextChange = this.state.tranTextChange
    this.setState({
      tranTextChange: {
        ...tranTextChange,
        [e.target.name]: e.target.value
      }
    })
  }

  onBlur = e => {
    const { transaction, tranTextChange } = this.state
    if (transaction[e.target.name] !== tranTextChange[e.target.name]) {
      this.onInputChange(e)
    }
  }

  onInputChange = e => {
    const transaction = cloneDeep(this.state.transaction)
    const { user } = this.props
    let { name, value, title } = e.target
    let state = {}
    const statusChange = name === "actTranStatus"
    if (statusChange) {
      name = "actTranStatus"
    }
    transaction[name] = value || ""

    if (statusChange) {
      const payload = {
        [name]: value
      }
      if (transaction.opportunity && transaction.opportunity.status && value !== "In Progress") {
        payload[name] =
          value === "Won" ? "Pre-Pricing" : value
        payload.opportunity = {
          ...transaction.opportunity,
          status: false,
          isWon: value === "Won"
        }
      }
      if(value === "Cancelled" || value === "Closed"){
        const msg = "You will not be able to EDIT any part of the transaction post this action. Are you sure?"
        swal({
          title: "",
          text: msg,
          icon: "warning",
          buttons: ["Cancel", "Yes"]
        }).then(willDo => {
          if (willDo) {
            state = {
              payload,
              statusChange,
              modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress"),
              relatedSave: false
            }
            this.setState({
              ...state,
              relatedSave: false
            })
          }
        })
      } else {
        state = {
          payload,
          statusChange,
          modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress"),
          relatedSave: false
        }
      }
    } else {
      state = {
        payload: {
          [name]: value || ""
        }
      }
    }
    this.setState(
      {
        ...state,
        relatedSave: false
      },
      () => {
        if (statusChange) {
          if (transaction.opportunity && transaction.opportunity.status) {
            if (value === "Won") {
              swal({
                title: "",
                text: oppMsg.won,
                icon: "warning",
                buttons: ["Cancel", "Yes, won it!"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            } else {
              const msg =
                value === "Lost"
                  ? oppMsg.lost
                  : oppMsg.cancel
              swal({
                title: "",
                text: msg,
                icon: "warning",
                buttons: ["Cancel", "Yes"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            }
          }else {
            this.props.addAuditLog({
              userName: user.userFirstName,
              log: `In summary transaction ${title ||
              "empty"} change to ${value}`,
              date: new Date(),
              key: "summary"
            })
          }
        } else {
          this.addAuditAndSave(title, value, transaction)
        }
      }
    )
  }

  addAuditAndSave = (title, value, transaction) => {
    const {user} = this.props
    if (title && value) {
      this.props.addAuditLog({
        userName: user.userFirstName,
        log: `In summary transaction ${title ||
        "empty"} change to ${value}`,
        date: new Date(),
        key: "summary"
      })
    }
    this.onConfirmationSave(transaction)
  }

  onConfirmationSave = transaction => {
    const { email, payload, statusChange } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putDerivativeSummary("status", tranId, payload, res => {
          //eslint-disable-line
          if (res && res.status === 200) {
            toast("Successfully Status updated transaction", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(tranId)
            if (statusChange) {
              this.setState(
                {
                  statusChange: false,
                  transaction: {
                    ...this.state.transaction,
                    ...payload
                  },
                  isSaveDisabled: false
                },
                async () => {
                  this.props.onStatusChange(payload.actTranStatus)
                  await sendEmailAlert(emailParams)
                }
              )
            } else {
              this.setState({
                statusChange: false,
                transaction: {
                  ...transaction,
                  ...payload
                },
                isSaveDisabled: false
              })
            }
          } else {
            toast("Something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
          }
        })
      }
    )
  }

  onRelatedtranCancel = () => {
    this.setState({
      derivative: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      }
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  pdfDownload = () => {
    const { transaction } = this.state
    const {user, logo} = this.props
    const position = user && user.settings
    const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
    const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"
    let counterFirmName = ""
    let counterFirmType = ""
    let participants = ""
    if (
      transaction.derivativeParticipants ||
      transaction.derivativeCounterparties
    ) {
      transaction.derivativeParticipants.forEach(item => {
        participants += `${item.partType} : ${item.partFirmName} ${item.partContactName ? `(${item.partContactName})` : ""},\n`
      })
      transaction.derivativeCounterparties.map(item => {
        counterFirmName += `${item.cntrPartyFirmName},`
      })
      transaction.derivativeCounterparties.map(item => {
        counterFirmType += `${item.cntrPartyFirmtype},`
      })
    }
    const tableData = [
      {
        titles: ["Summary"],
        description: "",
        headers: [
          "Issuer name",
          "Transaction Name",
          "Project Description",
          "Borrower or Obligor Name",
          "Guarantor Name",
          "Schedule highlights",
          "Derivative Type",
          "Counterparties",
          "Notional Amount",
          "Lead Advisors",
          "Coupon/Rate Type",
          "Tenor / Maturities",
          "End Date",
          "Working Group Participants",
          "Notes / Instructions"
        ],
        rows: [
          [
            `${transaction.actTranClientName}`,
            `${transaction.actTranIssueName}`,
            `${transaction.actTranProjectDescription}`,
            `${
              transaction.tranBorrowerName ? transaction.tranBorrowerName : "--"
            }`,
            `${
              transaction.tranGuarantorName
                ? transaction.tranGuarantorName
                : "--"
            }`,
            `Client: (Trade Date: ${
              transaction.derivTradeClientPayLeg.tradeDate
                ? moment(transaction.derivTradeClientPayLeg.tradeDate).format(
                  "MM-DD-YYYY"
                )
                : "--"
            }), (Effective Date: ${
              transaction.derivTradeClientPayLeg.effDate
                ? moment(transaction.derivTradeClientPayLeg.effDate).format(
                  "MM-DD-YYYY"
                )
                : "--"
            }) 
           Dealer: (Trade Date: ${
             transaction.derivTradeDealerPayLeg.tradeDate
               ? moment(transaction.derivTradeDealerPayLeg.tradeDate).format(
                 "MM-DD-YYYY"
               )
               : "--"
           }), (Effective Date: ${
             transaction.derivTradeDealerPayLeg.effDate
               ? moment(transaction.derivTradeDealerPayLeg.effDate).format(
                 "MM-DD-YYYY"
               )
               : "--"
           })`,
            `${transaction.actTranSubType}`,
            `Dealer: ${
              transaction.derivativeParticipants ? counterFirmName : "--"
            }\nClient: ${
              transaction.derivativeParticipants ? counterFirmType : "--"
            }`,
            `${
              transaction.derivativeSummary.tranNotionalAmt
                ? transaction.derivativeSummary.tranNotionalAmt
                : 0
            } Amortizing ${
              transaction.derivativeSummary.tranNotionalAmtFlag
                ? transaction.derivativeSummary.tranNotionalAmtFlag
                : false
            }`,
            `${
              transaction.actTranFirmLeadAdvisorName
                ? transaction.actTranFirmLeadAdvisorName
                : "--"
            }`,
            `${transaction.rateType ? transaction.rateType : "-- Rate"}`,
            "--",
            `${
              transaction.derivativeSummary.tranEndDate
                ? moment(transaction.derivativeSummary.tranEndDate).format(
                  "MM-DD-YYYY"
                )
                : "--"
            }`,
            `${transaction.derivativeParticipants ? participants : "--"}`,
            `${actNotes || "--"}`
          ]
        ]
      }
    ]
    pdfTableDownload("", tableData, `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} summary.pdf`, logo, position)
  }

  deleteRelatedTran = async (relatedId, relTranId, relType, relatedType) => {
    const { transaction } = this.props
    const query = `Derivatives?tranId=${transaction._id}&relatedId=${relatedId}&relTranId=${relTranId}&relType=${relType}&relatedType=${relatedType}`
    const related = await pullRelatedTransactions(query)
    if(related && related.done){
      toast("related transaction removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState({
        relatedTransactions: related && related.relatedTransaction || []
      })
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  render() {
    const {
      modalState,
      email,
      derivative,
      relatedTransactions,
      relatedTypes,
      transaction,
      errorMessages,
      tranStatus,
      isSaveDisabled,
      relatedSave,
      dropDown,
      tranTextChange
    } = this.state
    const { tranAction, participants, onParticipantsRefresh, nav2 } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={
            relatedSave === true
              ? this.onRelatedtranSave
              : this.onConfirmationSave
          }
        />
        <TranSummaryStatus
          tranStatus={tranStatus || []}
          disabled={!tranAction.canEditTran}
          selectedStatus={transaction.actTranStatus || ""}
          tranName={transaction.actTranUniqIdentifier || ""}
          pdfDownload={this.pdfDownload}
          transaction={
            {
              ...transaction.derivativeSummary,
              opportunity: transaction.opportunity || false
            } || {}
          }
          onChange={this.onInputChange}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Activity Summary"
              >
                {activeAccordions.includes(0) && (
                  <ActivitySummary
                    transaction={transaction}
                    onChange={this.onInputChange}
                    onTextInputChange={this.onChangeTextInput}
                    onBlur={this.onBlur}
                    dropDown={dropDown}
                    tranTextChange={tranTextChange}
                    canEditTran={tranAction.canEditTran}
                  />
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Related transactions"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(1) && (
                  <ReletedTranGlob
                    relatedTypes={relatedTypes}
                    transaction={transaction}
                    errorMessages={errorMessages}
                    isSaveDisabled={isSaveDisabled}
                    item={derivative}
                    tranId={nav2}
                    canEditTran={tranAction.canEditTran || false}
                    onRelatedTranSelect={this.onRelatedTranSelect}
                    onChange={this.onChange}
                    onRelatedtranSave={this.onModalSave}
                    onRelatedtranCancel={this.onRelatedtranCancel}
                    relatedTransactions={relatedTransactions}
                    deleteRelatedTran={this.deleteRelatedTran}
                  />
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(Summary)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
