import React from "react"
import {toast} from "react-toastify"
import {connect} from "react-redux"
import cloneDeep from "lodash.clonedeep"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import PayLeg from "./components/PayLeg"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import {TradeValidate } from "../Validation/TradeValidate"
import {putTransaction} from "../../../../StateManagement/actions"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"

class TradeSwap extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: "Annon",
      changeLog: [],
      errorMessages: {},
      loading: true,
      derivTradeClientPayLeg: cloneDeep(CONST.Derivative.TradeSwap.payLeg),
      derivTradeDealerPayLeg: cloneDeep(CONST.Derivative.TradeSwap.payLeg),
      tempDerivTradeClientPayLeg: cloneDeep(CONST.Derivative.TradeSwap.payLeg),
      tempDerivTradeDealerPayLeg: cloneDeep(CONST.Derivative.TradeSwap.payLeg),
      dropDown: {
        currency: [],
        paymentType: [],
        floatingRateType: [],
        stub: [],
        paymentSchedule: [],
        paymentBusConvention: [],
        periodEndDateBusConvention: [],
        initialIndexSetting: [],
        dayCount: []
      },
      derivativeCounterparties: [],
      isSaveDisabled: false,
      activeItem: [],
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
  }

  async componentWillMount() {
    const {transaction} = this.props
    const picResult = await getPicklistByPicklistName(["LKUPCURRENCY", "LKUPPAYMENTTYPE", "LKUPFLOATRATETYPE", "LKUPAMT", "LKUPPAYMENTFREQ", "LKUPBUSCONV", "LKUPDAYCOUNT"])
    const result = (picResult.length && picResult[1]) || {}
    const derivativeCounterparties = transaction.derivativeCounterparties.map(counterParty => ({
      ...counterParty,
      name: counterParty.cntrPartyFirmName,
      id: counterParty._id
    }))
    if(transaction.actTranSubType === "Swap") {
      this.setState(prevState => ({
        derivTradeClientPayLeg: (transaction && transaction.derivTradeClientPayLeg && Object.keys(transaction.derivTradeClientPayLeg)) ? cloneDeep(transaction.derivTradeClientPayLeg) : prevState.derivTradeClientPayLeg,
        derivTradeDealerPayLeg: (transaction && transaction.derivTradeDealerPayLeg && Object.keys(transaction.derivTradeDealerPayLeg)) ? cloneDeep(transaction.derivTradeDealerPayLeg) : prevState.derivTradeDealerPayLeg,
        tempDerivTradeClientPayLeg: (transaction && transaction.derivTradeClientPayLeg && Object.keys(transaction.derivTradeClientPayLeg)) ? cloneDeep(transaction.derivTradeClientPayLeg) : prevState.derivTradeClientPayLeg,
        tempDerivTradeDealerPayLeg: (transaction && transaction.derivTradeDealerPayLeg && Object.keys(transaction.derivTradeDealerPayLeg)) ? cloneDeep(transaction.derivTradeDealerPayLeg) : prevState.derivTradeDealerPayLeg,
        dropDown: {
          ...this.state.dropDown,
          currency: result.LKUPCURRENCY || [],
          paymentType: result.LKUPPAYMENTTYPE || [],
          floatingRateType: result.LKUPFLOATRATETYPE || [],
          stub: result.LKUPAMT || [],
          paymentSchedule: result.LKUPPAYMENTFREQ || [],
          paymentBusConvention: result.LKUPBUSCONV || [],
          periodEndDateBusConvention: result.LKUPBUSCONV || [],
          initialIndexSetting: result.LKUPFLOATRATETYPE || [],
          dayCount: result.LKUPDAYCOUNT || [],
        },
        derivativeCounterparties,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.actTranIssueName || transaction.actTranProjectDescription} - Notification`
        },
        loading: false,
      }))
    }else {
      this.props.history.push("/dashboard")
    }
  }

  onChange = (items, key) => {
    this.setState({
      [key]: items
    })
  }

  onBlurInput = (key, change, category) => {
    const { userName } = this.state
    this.props.addAuditLog({userName, log: `${change} from  tradeSwap in ${key}`, date: new Date(), key: category})
  }

  onSubmit = () => {
    const {derivTradeClientPayLeg, derivTradeDealerPayLeg} = this.state
    const payload = {
      derivTradeClientPayLeg,
      derivTradeDealerPayLeg
    }
    const errors = TradeValidate(payload, derivTradeClientPayLeg.createdAt)

    if(errors && errors.error) {
      const errorMessages = {}
      console.log("===================error",errors.error.details)

      errors.error.details.map(err => { //eslint-disable-line
        if(errorMessages.hasOwnProperty([err.path[0]])) { //eslint-disable-line
          if(errorMessages[err.path[0]].hasOwnProperty(err.path[1])) { //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: err.message
            }
          }else {
            errorMessages[err.path[0]][err.path[1]] = err.message
          }
        } else if(err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] =
              {
                [err.path[1]]: {
                  [err.path[2]]: err.message
                }
              }
        }else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: err.message
          }
        }
      })
      console.log("===================error",errorMessages)
      this.setState({errorMessages})
      this.setState(prevState => ({errorMessages: {...prevState.errorMessages, details: errorMessages}, activeItem: [2,3]}))
      return
    }
    this.setState({
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const {email, derivTradeClientPayLeg, derivTradeDealerPayLeg} = this.state
    const payload = {
      derivTradeClientPayLeg,
      derivTradeDealerPayLeg
    }
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace('/',''),
        ...email,
      }
    }
    console.log("==============email send to ==============", emailParams)
    this.setState({
      isSaveDisabled: true,
      modalState: false
    }, () => {
      putTransaction(this.props.nav3, this.props.nav2, payload, (res) => {
        if(res && res.status === 200) {
          toast("Client & Dealer Pay Leg updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.submitAuditLogs(this.props.nav2)
          this.setState({
            derivTradeClientPayLeg:  (res.data && res.data.derivTradeClientPayLeg) || cloneDeep(CONST.Derivative.TradeSwap.payLeg),
            derivTradeDealerPayLeg: (res.data && res.data.derivTradeDealerPayLeg) || cloneDeep(CONST.Derivative.TradeSwap.payLeg),
            tempDerivTradeClientPayLeg:  (res.data && res.data.derivTradeClientPayLeg) || cloneDeep(CONST.Derivative.TradeSwap.payLeg),
            tempDerivTradeDealerPayLeg: (res.data && res.data.derivTradeDealerPayLeg) || cloneDeep(CONST.Derivative.TradeSwap.payLeg),
            errorMessages: {},
            isSaveDisabled: false,
            activeItem: []
          }, async () => {
            await sendEmailAlert(emailParams)
          })
        }else {
          toast("something went wrong",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            isSaveDisabled: false,
            activeItem: []
          })
        }
      })
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onCancel = () => {
    const {transaction} = this.props
    const {tempDerivTradeClientPayLeg, tempDerivTradeDealerPayLeg} = this.state
    this.setState(prevState => ({
      derivTradeClientPayLeg: tempDerivTradeClientPayLeg,
      derivTradeDealerPayLeg: tempDerivTradeDealerPayLeg,
      errorMessages: {},
    }))
  }

  render() {
    const {modalState, email, derivativeCounterparties, errorMessages, activeItem, derivTradeClientPayLeg, derivTradeDealerPayLeg, dropDown, isSaveDisabled} = this.state
    const {transaction, tranAction, participants, onParticipantsRefresh} = this.props
    const canEditTran = (this.props.tranAction && this.props.tranAction.canEditTran) || false
    derivTradeClientPayLeg.payor = transaction.actTranClientName
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }
    return(
      <section className='transactionRFP'>
        <SendEmailModal modalState={modalState} email={email} onModalChange={this.onModalChange} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onSave={this.onConfirmationSave}/>
        <Accordion multiple isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [2,3]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>

              <div className="columns">
                <div className="column is-half">
                  <RatingSection onAccordion={() => onAccordion(2)} title="Client Pay Leg">
                    {activeAccordions.includes(2) &&
                      <PayLeg category="derivTradeClientPayLeg" errors={errorMessages.derivTradeClientPayLeg} item={derivTradeClientPayLeg} canEditTran={canEditTran} onChangeItem={this.onChange} onBlur={this.onBlurInput}
                        dropDown={dropDown} flag/>
                    }
                  </RatingSection>
                </div>
                <div className="column is-half">
                  <RatingSection onAccordion={() => onAccordion(3)} title="Counterparty Pay Leg">
                    {activeAccordions.includes(3) &&
                      <PayLeg category="derivTradeDealerPayLeg" payor={derivativeCounterparties} errors={errorMessages.derivTradeDealerPayLeg} canEditTran={canEditTran} dropDown={dropDown} item={derivTradeDealerPayLeg} onChangeItem={this.onChange} onBlur={this.onBlurInput}/>
                    }
                  </RatingSection>
                </div>
              </div>
              {
                tranAction && tranAction.canEditTran ?
              <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button className="button is-link" onClick={this.onSubmit} disabled={isSaveDisabled || false}>Save</button>
                    </div>
                    <div className="control">
                      <button className="button is-light" onClick={this.onCancel}>Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
                  : null
              }
            </div>
          }/>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(TradeSwap)
export default connect(mapStateToProps, null)(WrappedComponent)
