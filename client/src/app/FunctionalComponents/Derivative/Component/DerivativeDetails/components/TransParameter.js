import React from "react"
import moment from "moment"
import { DropdownList } from "react-widgets"
import ThirdPartyLookup from "Global/ThirdPartyLookup"
import {
  TextLabelInput,
  SelectLabelInput,
  NumberInput
} from "../../../../../GlobalComponents/TextViewBox"

const TransParameter = ({
  errorMessages,
  item = {},
  transaction,
  description,
  canEditTran,
  dropDown,
  onBlur,
  onChangeItem,
  category,
  getBorrowerByFirm
}) => {
  const onChange = event => {
    if (
      event.target.name === "actTranPrimarySector" ||
      event.target.name === "actTranSecondarySector"
    ) {
      const tranSector = {
        ...transaction,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || null
      }
      if (event.target.name === "actTranPrimarySector") {
        tranSector.actTranSecondarySector = ""
      }
      onChangeItem(tranSector, "tempTransaction")
    } else if (event.target.name === "actTranIssueName") {
      onChangeItem(
        {
          ...transaction,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value || null
        },
        "tempTransaction"
      )
    } else {
      onChangeItem(
        {
          ...item,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value || null
        },
        category
      )
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onSelection = (key, selectItem) => {
    if (key === "tranBorrowerName") {
      onChangeItem(
        {
          ...item,
          tranBorrowerName: selectItem.name
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...transaction,
          actTranClientId: selectItem.id,
          actTranClientName: selectItem.firmName,
          actTranClientMsrbType: selectItem.msrbRegistrantType
        },
        "tempTransaction"
      )
      onChangeItem(
        {
          ...item,
          tranBorrowerName: ""
        },
        category
      )
    }
  }

  const counterName = {
    _id: "",
    firmName: item.tranCounterpartyDealer || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const counterClient = {
    _id: "",
    firmName: item.tranCounterpartyClient || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  return (
    <div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Primary Sector</p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="Primary Sector"
                value={transaction.actTranPrimarySector || ""}
                onChange={onChange}
                disabled={!canEditTran}
                name="actTranPrimarySector"
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="" disabled="">
                  Pick Primary Sector
                </option>
                {dropDown && dropDown.primarySectors && dropDown.primarySectors.map(t => (
                  <option key={t && t.label} value={t && t.label} disabled={!t.included}>
                    {t && t.label}
                  </option>
                ))}
              </select>
              {errorMessages && errorMessages.actTranPrimarySector && (
                <small className="text-error">
                  {errorMessages.actTranPrimarySector || ""}
                </small>
              )}
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">Secondary Sector</p>
          <div className="select is-small is-link" style={{ width: "100%" }}>
            <select
              title="Secondary Sector"
              name="actTranSecondarySector"
              value={transaction.actTranSecondarySector || ""}
              disabled={!canEditTran}
              onChange={onChange}
              onBlur={onBlurInput}
              style={{ width: "100%" }}
            >
              <option value="" disabled="">
                Pick Secondary Sector
              </option>
              {transaction.actTranPrimarySector &&
              dropDown.secondarySectors[transaction.actTranPrimarySector]
                ? dropDown.secondarySectors[transaction.actTranPrimarySector].map(
                  sector => (
                    <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                      {sector && sector.label}
                    </option>
                  )
                )
                : null}
            </select>
            {errorMessages && errorMessages.actTranSecondarySector && (
              <small className="text-error">
                {errorMessages.actTranSecondarySector || ""}
              </small>
            )}
          </div>
        </div>
        <SelectLabelInput
          label="Transaction Type"
          required
          error={errorMessages.tranType || ""}
          list={dropDown.tranType}
          name="tranType"
          value={item.tranType || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <div className="column">
          <p className="multiExpLbl ">
            Notional Amount<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
          </p>
          <p className="emmaTablesTd">
            <input
              title="Notional Amount Amortizing"
              type="checkbox"
              name="tranNotionalAmtFlag"
              checked={item.tranNotionalAmtFlag || false}
              disabled={!canEditTran}
              onClick={onChange}
              onChange={onChange}
              onBlur={onBlurInput}
            />
            Amortizing?
          </p>
          <NumberInput
            title="Notional Amount"
            prefix="$"
            placeholder="$10,000,000"
            error={errorMessages.tranNotionalAmt ? "Required(must be larger than or equal to 0)" : "" || ""}
            name="tranNotionalAmt"
            value={item.tranNotionalAmt || ""}
            disabled={!canEditTran}
            onChange={onChange}
            onBlur={onBlurInput}
          />
        </div>
      </div>

      <div className="columns">
        <TextLabelInput
          label="Trade Date"
          required
          error={errorMessages.tranTradeDate ? "Required(must be smaller than End Date)" : "" || ""}
          disabled={!canEditTran}
          name="tranTradeDate"
          type="date"
          value={(item.tranTradeDate === "" || !item.tranTradeDate) ? null : new Date(item.tranTradeDate)}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <TextLabelInput
          label="Effective Date"
          required
          error={errorMessages.tranEffDate ? "Required(must be smaller than End Date)" : "" || ""}
          disabled={!canEditTran}
          name="tranEffDate"
          type="date"
          // value={item.tranEffDate ? moment(new Date(item.tranEffDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
          value={(item.tranEffDate === "" || !item.tranEffDate) ? null : new Date(item.tranEffDate)}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <TextLabelInput
          label="End Date"
          required
          error={errorMessages.tranEndDate || ""}
          disabled={!canEditTran}
          name="tranEndDate"
          type="date"
          // value={item.tranEndDate ? moment(new Date(item.tranEndDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
          value={(item.tranEndDate === "" || !item.tranEndDate) ? null : new Date(item.tranEndDate)}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        {/* <TextLabelInput
          label="Counterparty-Client"
          required
          error={errorMessages.tranCounterpartyClient || ""}
          name="tranCounterpartyClient"
          disabled={!canEditTran}
          placeholder="The School Board of Monroe Country, Florida"
          value={item.tranCounterpartyClient || ""}
          onChange={onChange}
          onBlur={onBlurInput}
        /> */}
        <div className="column is-one-quarter">
          <p className="multiExpLbl">Counterparty-Client<span className='icon has-text-danger'><i className='fas fa-asterisk extra-small-icon'/></span></p>
          <ThirdPartyLookup
            entityName={counterClient}
            onChange={(e) => onChange({ target: { name: "tranCounterpartyClient", value: e.firmName } })}
            type="other"
            error={errorMessages.tranCounterpartyClient || ""}
            style={{ fontSize: 12 }}
            notEditable={!canEditTran || false}
            isWidth="is-12"
            isHide={item.tranCounterpartyClient && canEditTran}
          />
        </div>

      </div>

      <div className="columns ">
        <SelectLabelInput
          label="Counterparty Type"
          required
          error={errorMessages.tranCounterpartyType || ""}
          list={dropDown && dropDown.firm || []}
          name="tranCounterpartyType"
          value={item.tranCounterpartyType || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        {/* <div className="column">
          <p className="multiExpLbl">Counterparty Type</p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="Counterparty Type"
                name="tranCounterpartyType"
                value={item.tranCounterpartyType || ""}
                disabled={!canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="">Pick Type</option>
                {dropDown.firm &&
                  Object.keys(dropDown.firm).map((key, i) => (
                    <option key={i} value={key}>
                      {key}
                    </option>
                  ))}
              </select>
              {errorMessages.tranCounterpartyType && (
                <small className="text-error">
                  {errorMessages.tranCounterpartyType || ""}
                </small>
              )}
            </div>
          </div>
        </div> */}
        <div className="column is-one-quarter">
          <p className="multiExpLbl">Counterparty<span className='icon has-text-danger'><i className='fas fa-asterisk extra-small-icon'/></span></p>
          {/* <div className="control">
            <div className="is-small is-link" >
              <select
                title="Counterparty-Dealer"
                name="tranCounterpartyDealer"
                value={item.tranCounterpartyDealer || ""}
                disabled={!canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="">Pick Dealer</option>
                {dropDown.firm && dropDown.firm[item.tranCounterpartyType]
                  ? dropDown.firm[item.tranCounterpartyType].map((name, i) => (
                      <option key={i} value={name}>
                        {name}
                      </option>
                    ))
                  : null}
              </select> */}
          <ThirdPartyLookup
            entityName={counterName}
            onChange={(e) => onChange({ target: { name: "tranCounterpartyDealer", value: e.firmName } })}
            type="other"
            style={{ fontSize: 12 }}
            error={errorMessages.tranCounterpartyDealer || ""}
            notEditable={!canEditTran || false}
            isWidth="is-12"
            isHide={item.tranCounterpartyDealer && canEditTran}
          />
        </div>
        {/* </div>
        </div> */}
        <NumberInput
          prefix="$"
          label="Last mark-to-market valuation"
          error={errorMessages.tranLastMTMValuation ? "Required(must be larger than or equal to 0)" : "" || ""}
          name="tranLastMTMValuation"
          placeholder="$10,000,000"
          value={item.tranLastMTMValuation || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <TextLabelInput
          label="Last mark-to-market date"
          error={errorMessages.tranLastMTMDate || ""}
          name="tranLastMTMDate"
          type="date"
          // value={item.tranLastMTMDate ? moment(new Date(item.tranLastMTMDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
          value={(item.tranLastMTMDate === "" || !item.tranLastMTMDate) ? null : new Date(item.tranLastMTMDate)}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
      </div>

      <div className="columns">
        <div className="column is-3">
          <div className="columns">
            <TextLabelInput
              label="Mark-to-market provider"
              error={errorMessages.tranMTMProvider || ""}
              name="tranMTMProvider"
              value={item.tranMTMProvider || ""}
              disabled={!canEditTran}
              onChange={onChange}
              onBlur={onBlurInput}
            />
          </div>
        </div>
        <NumberInput
          className="column is-3"
          label="Estimated Revenue"
          prefix="$"
          required
          disabled={!canEditTran}
          error={errorMessages.tranEstimatedRev ? "Required(must be larger than or equal to 0)" : "" || ""}
          name="tranEstimatedRev"
          placeholder="$"
          value={item.tranEstimatedRev}
          onChange={onChange}
          onBlur={onBlurInput}
        />
      </div>
    </div>
  )
}

export default TransParameter
