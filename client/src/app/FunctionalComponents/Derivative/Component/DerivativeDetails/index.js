import React from "react"
import {connect} from "react-redux"
import {toast} from "react-toastify"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TransParameter from "./components/TransParameter"
import {DerivativesValidate} from "../Validation/DerivativesValidate"
import {putTransaction, fetchBorrowerObligorByFirm, postDerivativeNotes} from "../../../../StateManagement/actions"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "../../../../GlobalComponents/Loader"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import CONST, {
  states,
} from "../../../../../globalutilities/consts"
import {fetchSupplierContacts} from "../../../../StateManagement/actions/TransactionDistribute"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import HistoricalData from "../../../../GlobalComponents/HistoricalData"
import TransactionNotes from "../../../../GlobalComponents/TransactionNotes"

class DerivativeDetails extends React.Component {
  constructor() {
    super()
    this.state = {
      derivativeSummary : CONST.Derivative.derivativeSummary,
      userName: "",
      errorMessages: {},
      usersList: [],
      tranNotes: [],
      tempTransaction: {},
      states: [...states],
      dropDown: {
        borrowerOrObligorName: [],
        guarantorName: [],
        tranType: [],
        primarySector: {},
        secondarySector: [],
        offeringType: [],
        securityType: [],
        bankQualified: [],
        corpType: [],
        useOfProceeds: [],
        Guarantor: [],
      },
      loading: true,
      isSaveDisabled: false,
      activeItem: [],
      userList: [],
      participantsType: [],
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
  }

  async componentDidMount() {
    const {transaction} = this.props
    const picResult = await getPicklistByPicklistName(["LKUPPRIMARYSECTOR", "LKUPMSRBFIRMNAME", "LKUPDEALTYPE", "LKUPSWAPTRANSTATUS", "LKUPCOUNTERPARTYTYPE", "LKUPCOUNTERPARTYFIRMTYPE"])
    const result = (picResult.length && picResult[1]) || {}
    const primarySec =  picResult[2].LKUPPRIMARYSECTOR || {}
    this.setState(prevState => ({
      dropDown: {
        ...this.state.dropDown,
        primarySectors: result.LKUPPRIMARYSECTOR || [],
        secondarySectors: primarySec,
        tranIssuer: result.LKUPMSRBFIRMNAME || [],
        tranType: result.LKUPDEALTYPE || [],
        tranCounterpartyType: result.LKUPCOUNTERPARTYTYPE || [],
        firm: (result && result.LKUPCOUNTERPARTYFIRMTYPE) || [],
        /* firm : {
          "Dealer": ["Dealer1", "Dealer2", "Dealer3"],
          "Other": ["Other1", "Other2", "Other3"],
          "Special Purpose Vehicle": ["SPV1", "SPV2", "SPV3"]
        } */
      },
      derivativeSummary : (transaction && transaction.derivativeSummary) ?
        {...(transaction && transaction.derivativeSummary), tranType: transaction.actTranSubType || ""}
        : { ...CONST.Derivative.derivativeSummary, tranType: transaction.actTranSubType || ""},
      userName: (this.props.user && this.props.user.userFirstName) || "",
      loading: false,
      tempTransaction: {
        actTranSecondarySector: transaction.actTranSecondarySector,
        actTranPrimarySector: transaction.actTranPrimarySector,
        actTranClientId: transaction.actTranClientId,
        actTranClientName: transaction.actTranClientName,
        actTranIssueName: transaction.actTranIssueName,
      },
      tranNotes: transaction && transaction.tranNotes || [],
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actTranIssueName || transaction.actTranProjectDescription} - Notification`
      }
    }), () => {
      this.getBorrowerByFirm()
    })

  }

  getBorrowerByFirm = () => {
    const {transaction} = this.props
    const firmId = transaction.actTranFirmId
    const {tempTransaction} = this.state
    fetchBorrowerObligorByFirm(tempTransaction.actTranClientId || transaction.actTranClientId, (borrower)=> {
      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          borrower
        }
      }), () => {
        this.getFirmDetails(firmId)
      })
    })
  }

  getFirmDetails = (firmId) => {
    fetchSupplierContacts(firmId, "Bond Counsel", (allFirms)=> {
      let name = []
      allFirms && allFirms.suppliersList.map((ent) => {
        return name.push(ent.name)
      })
      console.log(name)

      this.setState({
        loading: false,
        dropDown: {
          ...this.state.dropDown,
          Guarantor : name
        }
      })
    })
  }

  onChange = (item, category, index) => {
    const items = this.state[category]
    items.splice(index, 1, item)
    this.setState({
      [category]: items,
    })
  }

  onChangeItem = (item, category) => {
    this.setState(prevState => ({
      [category]: {
        ...prevState[category],
        ...item
      }
    }), () => {
      if(category === "tempTransaction") {
        this.getBorrowerByFirm()
      }
    })
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `${change} from details`, date: new Date(), key: category})
  }

  onCancel = () => {
    const {transaction} = this.props
    this.setState(prevState => ({
      derivativeSummary : (transaction && transaction.derivativeSummary) ?
        {...(transaction && transaction.derivativeSummary), tranType: transaction.actTranSubType || ""}
        : { ...CONST.Derivative.derivativeSummary, tranType: transaction.actTranSubType || ""},
      errorMessages: {},
      tempTransaction: {
        actTranSecondarySector: transaction && transaction.actTranSecondarySector || "",
        actTranPrimarySector: transaction && transaction.actTranPrimarySector || "",
      },
    }))
  }

  onSave = () => {
    const {derivativeSummary, tempTransaction} = this.state
    const payload = {derivativeSummary, ...tempTransaction}
    const errors = DerivativesValidate(payload, derivativeSummary.createdAt)

    if(errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({errorMessages})
      this.setState(prevState => ({errorMessages: {...prevState.errorMessages, details: errorMessages}, activeItem: [0,0]}))
      return
    }
    console.log("===============>",derivativeSummary)
    this.setState({
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const {email, derivativeSummary, tempTransaction} = this.state
    const {transaction} = this.props
    const isData = transaction && transaction.derivTradeDealerPayLeg && transaction.derivTradeDealerPayLeg.paymentType
    const PayLeg = {
        tradeDate: (derivativeSummary && derivativeSummary.tranTradeDate) || "",
        effDate: (derivativeSummary && derivativeSummary.tranEffDate) || "",
        periodEndDate: (derivativeSummary && derivativeSummary.tranEndDate) || "",
    };

    const derivTradeClientPayLeg = PayLeg || {}
    const derivTradeDealerPayLeg = PayLeg || {}

    const payload = {derivativeSummary, ...tempTransaction, derivTradeClientPayLeg, derivTradeDealerPayLeg }
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace('/',''),
        ...email,
      }
    }
    if(isData){
      delete payload.derivTradeClientPayLeg
      delete payload.derivTradeDealerPayLeg
    }
    console.log("==============email send to ==============", emailParams)
    this.setState({
      isSaveDisabled: true,
      modalState: false
    }, () => {
      putTransaction("details", this.props.nav2, payload, (res) => {
        if(res && res.status === 200) {
          toast("Add derivative Details successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.submitAuditLogs(this.props.nav2)
          this.setState({
            derivativeSummary: (res.data && res.data.derivativeSummary) || CONST.Derivative.derivativeSummary,
            isSaveDisabled: false,
            activeItem: [],
            errorMessages: {},
          }, async () => {
            await sendEmailAlert(emailParams)
          })
        }else {
          toast("something went wrong",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            isSaveDisabled: false,
            activeItem: []
          })
        }
      })
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onNotesSave = (payload, callback) => {
    postDerivativeNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add Derivative Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  render() {
    const {modalState, email, derivativeSummary, errorMessages, dropDown, activeItem, tempTransaction, isSaveDisabled, tranNotes} = this.state
    const {transaction, tranAction, participants, onParticipantsRefresh} = this.props
    const canEditTran = (this.props.tranAction && this.props.tranAction.canEditTran) || false
    const desc = transaction.actTranProjectDescription
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal modalState={modalState} email={email} onModalChange={this.onModalChange} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onSave={this.onConfirmationSave}/>
        <Accordion multiple isRequired={!!activeItem.length} activeItem={activeItem.length ? activeItem : [0]}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(0)} title="Key Transaction Parameter">
                {activeAccordions.includes(0) &&
                          <TransParameter transaction={tempTransaction || {}} description={desc} errorMessages={errorMessages} item={derivativeSummary} dropDown={dropDown}
                            canEditTran={canEditTran} onChangeItem={this.onChangeItem} getBorrowerByFirm={this.getBorrowerByFirm}  category="derivativeSummary" onBlur={this.onBlur}/>
                }
              </RatingSection>
              {
                tranAction && tranAction.canEditTran ?
                  <div className="column is-full" style={{marginTop:"25px"}}>
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={this.onSave} disabled={isSaveDisabled || false}>Save</button>
                      </div>
                      <div className="control">
                        <button className="button is-light" onClick={this.onCancel}>Cancel</button>
                      </div>
                    </div>
                  </div>
                  : null
              }
            </div>
          }
        />
        { transaction.OnBoardingDataMigrationHistorical ? <HistoricalData data={(transaction && transaction.historicalData) || {}}/> : null}
        <TransactionNotes
          notesList={tranNotes || []}
          canEditTran={canEditTran || false}
          onSave={this.onNotesSave}
        />
      </div>

    )
  }
}


const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})
const WrappedComponent = withAuditLogs(DerivativeDetails)

export default connect(mapStateToProps,null)(WrappedComponent)
