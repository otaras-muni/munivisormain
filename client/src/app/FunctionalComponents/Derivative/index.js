import React, { Component } from "react"
import Loader from "../../GlobalComponents/Loader"
import DerivativeView from "./Component"

class Derivative extends Component {
  render() {
    const { nav2, nav3 } = this.props
    if(nav2) {
      return (
        <div>
          <DerivativeView {...this.props} option={nav3} />
        </div>
      )
    }
    return <Loader />
  }
}

export default Derivative
