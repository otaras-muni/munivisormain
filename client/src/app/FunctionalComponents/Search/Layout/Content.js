import styled from "styled-components"

import media from "../utils/media"

const Content = styled.div`
  width: 100%;
  left: 0;
  position: relative;
  padding: 30px;
  background-color: #f5f6f5;

  ${media.desktop`
  width: calc(100% - 320px);
  left: 320px;
  padding: 30px 100px;
  `}
`

export default Content
