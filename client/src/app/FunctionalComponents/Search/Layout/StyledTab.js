import styled from "styled-components"
import Flex from "./Flex"
import media from "../utils/media"

const Tab = styled(Flex)`
  width: ${props => (props.isSidebar ? "calc(100% - 320px)" : "100%")};
  left: ${props => (props.isSidebar ? "320px" : "0")};
  position: fixed;
  top: 52px;
  z-index: 3;
  height: 56px;
  background: #fff;
  padding: 20px;

  li {
    list-style: none;
    margin-right: 25px;
    cursor: pointer;
  }

  li.active {
    color: #4288dd;
  }

  li:hover {
    color: #4288dd;
  }
  ${media.desktop`
  width: calc(100% - 320px);
  left: 320px;
  padding: 10px 100px;
  `}
`

export default Tab
