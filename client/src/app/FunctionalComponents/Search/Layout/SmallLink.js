import styled from "styled-components"

const SmallLink = styled.span`
  color: #424242;
  cursor: pointer;
  &:hover {
    color: rgba(66, 136, 221, 0.8);
  }
`
export default SmallLink
