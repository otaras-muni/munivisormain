import styled from "styled-components"

const Layout = styled.div`
  display: flex;
  margin-top: 60px;
  min-height: calc(100vh - 100px);
`

export default Layout
