import React from "react"
import styled from "styled-components"
import media from "../utils/media"

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 4;
  background-color: rgba(0, 0, 0, 0.8);
  box-sizing: border-box;
  ${media.desktop`
    display: none;
    z-index: 0;
    position: relative;
  `};
`

const StyledSideBar = styled.div`
  width: 320px;
  height: calc(100%);
  overflow-y: auto;
  padding: 15px 20px;
  position: fixed;
  left: 0;
  top: 52px;
  padding-bottom: 100px;
  z-index: 5;
  display: ${props => (props.isSideBarOpen ? "block" : "none")};
  background-color: white;

  ${media.desktop`
   width: 320px;
   border-right: 1px solid #e1e4e8;
   display: block;
  `};
`

const Toggle = styled.div`
  position: fixed;
  z-index: 10;
  bottom: 10px;
  left: 10px;
  height: 50px;
  width: 50px;
  display: flex;
  justify-content: center;
  color: white;
  background: dodgerblue;
  border-radius: 50%;
  align-items: center;
  cursor: pointer;

  ${media.desktop`
    display: none;
  `}
`

class SideBar extends React.Component {
  state = {
    isSideBarOpen: false
  }

  toggleState = () => {
    this.setState(prevState => ({
      isSideBarOpen: !prevState.isSideBarOpen
    }))
  }

  render() {
    const { children } = this.props
    const { isSideBarOpen } = this.state
    return (
      <>
        {isSideBarOpen && <Wrapper />}
        <StyledSideBar isSideBarOpen={isSideBarOpen}>{children}</StyledSideBar>
        <Toggle onClick={this.toggleState}>
          <i className={`fa ${isSideBarOpen ? "fa-close" : "fa-filter"}`} />
        </Toggle>
      </>
    )
  }
}

export default SideBar
