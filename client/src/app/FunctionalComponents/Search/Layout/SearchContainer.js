import styled from "styled-components"
import media from "../utils/media"

const SearchContainer = styled.div`
  width: 100%;
  left: 0;
  position: fixed;
  top: ${props => (props.hasTopBar ? "106px" : "52px")};
  z-index: 3;
  display: flex;
  align-items: center;
  background-color: #4288dd;
  height: 60px;
  font-size: 15px;
  letter-spacing: 0.05rem;
  justify-content: center;

  ${media.desktop`
  width: calc(100% - 320px);
  left: 320px;
  `}
`

export default SearchContainer
