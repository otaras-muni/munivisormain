import styled from "styled-components"
import { DataSearch } from "@appbaseio/reactivesearch"

const StyledSearch = styled(DataSearch)`
  width: 100%;
  padding: 0 100px;

  input {
    border-radius: 3px;
  }
`

export default StyledSearch
