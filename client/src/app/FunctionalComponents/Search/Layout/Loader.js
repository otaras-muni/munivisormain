import React from "react"
import LoaderImage from "../../../../images/loader.gif"

const Loader = () => (
  <div
    style={{
      position: "absolute",
      textAlign: "center",
      top: 50,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 2,
      height: "100vh",
      background: "rgba(255,255,255,0.5)",
      borderRadius: 3
    }}
  >
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        width: "100%",
        height: "100%",
        alignItems: "center",
        marginTop: "-50px"
      }}
    >
      <img src={LoaderImage} alt="loader" height={100} width={100} />
    </div>
  </div>
)

export default Loader
