import React from "react"

import Flex from "./Flex"

export default () => (
  <Flex
    justifyContent="center"
    alignItems="center"
    style={{ fontSize: 24, fontWeight: "bold", minHeight: "60vh" }}
    flexDirection="column"
  >
    <i className="fa fa-frown-o" />
    <p>No results found</p>
  </Flex>
)
