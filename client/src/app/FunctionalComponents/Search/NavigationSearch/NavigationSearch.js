/* eslint-disable no-underscore-dangle, indent */
import React from "react"
import {
  ReactiveBase,
  DataSearch,
  ReactiveComponent
} from "@appbaseio/reactivesearch"
import styled from "styled-components"
import _ from "lodash"
import axios from "axios"

import Flex from "../Layout/Flex"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import { globalSearchEntitlement } from "../utils/entitlement-query"
import { config, getSearchableFields, getWeights } from "./searchConfig"
import { urlHelper } from "../../../../globalutilities/helpers"
import { muniApiBaseURL } from "../../../../globalutilities/consts"

const isTran = value =>
  value === "Deals" ||
  value === "Manage Rfps" ||
  value === "Bank Loans" ||
  value === "Derivatives" ||
  value === "Business Development Activities" ||
  value === "Other Transactions" ||
  value === "MA RFPs"

const isEntity = value => value === "Entity Details"

const isUser = value => value === "User Details"

const RenderTransaction = ({ suggestion }) => (
  <Flex wrap="no-wrap" justifyContent="space-between" alignItems="center">
    <div style={{ maxWidth: "90%" }}>
      <div
        dangerouslySetInnerHTML={{
          __html:
            suggestion.highlight &&
            suggestion.highlight["data.activityDescription"]
              ? _.get(suggestion.highlight, "data.activityDescription")
              : _.get(suggestion._source, "data.activityDescription")
        }}
      />
      <small>
        <i>Client:</i>
        <span
          dangerouslySetInnerHTML={{
            __html:
              suggestion.highlight &&
              suggestion.highlight["data.tranClientName"]
                ? _.get(suggestion.highlight, "data.tranClientName")
                : _.get(suggestion._source, "data.tranClientName")
          }}
        />
      </small>
    </div>

    <small>
      <b
        dangerouslySetInnerHTML={{
          __html:
            suggestion.highlight && suggestion.highlight.category
              ? _.get(suggestion.highlight, "category")
              : _.get(suggestion._source, "category")
        }}
      />
    </small>
  </Flex>
)

const RenderEntity = ({ suggestion }) => (
  <Flex wrap="no-wrap" justifyContent="space-between" alignItems="center">
    <span
      dangerouslySetInnerHTML={{
        __html:
          suggestion.highlight && suggestion.highlight["data.entityName"]
            ? _.get(suggestion.highlight, "data.entityName")
            : _.get(suggestion._source, "data.entityName")
      }}
    />
    <b>Entities</b>
  </Flex>
)

const RenderUser = ({ suggestion }) => (
  <Flex wrap="no-wrap" justifyContent="space-between" alignItems="center">
    <Flex>
      <span
        dangerouslySetInnerHTML={{
          __html:
            suggestion.highlight && suggestion.highlight["data.userFirstName"]
              ? _.get(suggestion.highlight, "data.userFirstName")
              : _.get(suggestion._source, "data.userFirstName")
        }}
      />
      &nbsp;
      <span
        dangerouslySetInnerHTML={{
          __html:
            suggestion.highlight && suggestion.highlight["data.userLastName"]
              ? _.get(suggestion.highlight, "data.userLastName")
              : _.get(suggestion._source, "data.userLastName")
        }}
      />
    </Flex>
    <b>Users</b>
  </Flex>
)

const Suggestions = styled.div`
  position: absolute;
  color: #424242;
  font-size: 12px !important;
  border: 1px solid #ddd;
  background: white;
  border-radius: 2;
  margin-top: 0;
  width: 100%;
  z-index: 10;
  max-height: 300px;
  overflow: auto;
`

const RenderSuggestion = ({ suggestion, getItemProps, isActive }) => {
  const source = suggestion._source
  const isTranItem = isTran(source.category)
  const isEntityItem = isEntity(source.category)
  const isUserItem = isUser(source.category)
  const value = isTranItem
    ? source.data.activityDescription
    : isEntityItem
    ? source.data.entityName
    : `${_.get(source, "data.userFirstName")} ${_.get(
        source,
        "data.userLastName"
      )}`
  return (
    <div
      {...getItemProps({
        item: {
          value,
          source
        }
      })}
      style={{
        background: isActive ? "#eee" : "transparent",
        borderBottom: "1px solid #dfdfdf",
        cursor: "pointer"
      }}
    >
      <div style={{ padding: 5 }}>
        {isTranItem && <RenderTransaction suggestion={suggestion} />}
        {isEntityItem && <RenderEntity suggestion={suggestion} />}
        {isUserItem && <RenderUser suggestion={suggestion} />}
      </div>
    </div>
  )
}

class NavSearch extends React.Component {
  state = {
    isBlured: true,
    isFetching: true,
    defaultQuery: null
  }

  componentDidMount() {
    this.fetchEntitlements()
  }

  handleBlurChange = isBlured => {
    this.setState({
      isBlured
    })
  }

  fetchEntitlements = async () => {
    try {
      const {
        auth: {
          userEntities: { relationshipToTenant }
        }
      } = this.props

      if (relationshipToTenant === "Self") {
        const defaultQuery = globalSearchEntitlement(relationshipToTenant, [])
        this.setState({
          isFetching: false,
          defaultQuery
        })
      } else {
        const res = await axios({
          method: "GET",
          url: `${muniApiBaseURL}entitlements/nav`,
          headers: { Authorization: localStorage.getItem("token") }
        })

        const { data } = res.data
        const defaultQuery = globalSearchEntitlement(
          relationshipToTenant,
          data.all || []
        )

        this.setState({
          isFetching: false,
          defaultQuery
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetching: false
        })
      }
      console.error(err)
    }
  }

  render() {
    const { auth, history } = this.props
    const { isBlured, defaultQuery, isFetching } = this.state

    return (
      !isFetching && (
        <ReactiveBase
          url={ELASTIC_SEARCH_URL}
          app={`tenant_data_${auth.loginDetails.relatedFaEntities[0].entityId}`}
          type="doc"
          headers={{
            Authorization: localStorage.getItem("token")
          }}
        >
          <ReactiveComponent
            customQuery={() => config.query}
            componentId="NavSearchCategory"
            key={`viewQuery_${config.name}`}
          />
          <ReactiveComponent
            customQuery={() => defaultQuery}
            componentId="NavSearchEntitlement"
            key="globalSearchEntitlement"
          />
          <DataSearch
            componentId="NavSearch"
            react={{
              and: ["NavSearchCategory", "NavSearchEntitlement"]
            }}
            debounce={300}
            dataField={[...getSearchableFields()]}
            fieldWeights={[...getWeights()]}
            fuzziness={0}
            showIcon={false}
            placeholder="Search transactions, entities, users"
            style={{
              marginTop: 13,
              minWidth: 300
            }}
            onValueSelected={(value, cause, source) => {
              const isTranItem = isTran(source.category)
              const isEntityItem = isEntity(source.category)
              const isUserItem = isUser(source.category)
              let url = "/dashboard"
              if (isTranItem) {
                url = urlHelper(
                  "transactions",
                  source.data._id,
                  source.data.tranType,
                  source.data.tranSubType
                )
              }

              if (isEntityItem) {
                url = urlHelper(
                  "entities",
                  source.data._id,
                  source.data.relationshipToTenant
                )
              }

              if (isUserItem) {
                url = urlHelper(
                  "users",
                  source.data._id,
                  source.data.entityRelationshipToTenant
                )
              }

              history.push(url)
            }}
            onFocus={() => {
              this.handleBlurChange(false)
            }}
            onBlur={() => {
              this.handleBlurChange(true)
            }}
            highlight
            renderAllSuggestions={({
              currentValue,
              suggestions,
              getItemProps,
              highlightedIndex
            }) =>
              suggestions &&
              Boolean(suggestions.length) &&
              Boolean(currentValue.length) &&
              !isBlured && (
                <Suggestions>
                  {suggestions.map((item, index) => (
                    <RenderSuggestion
                      suggestion={item}
                      getItemProps={getItemProps}
                      currentValue={currentValue}
                      key={item._id}
                      isActive={highlightedIndex === index}
                    />
                  ))}
                </Suggestions>
              )
            }
          />
        </ReactiveBase>
      )
    )
  }
}

export default NavSearch
