export const config = {
  query: {
    query: {
      terms: {
        "category.keyword": [
          "Deals",
          "Manage Rfps",
          "MA RFPs",
          "Bank Loans",
          "Derivatives",
          "Business Development Activities",
          "Other Transactions",
          "Entity Details",
          "User Details"
        ]
      }
    }
  },
  searchFields: [
    "data.tranClientName",
    "data.activityDescription",
    "data.entityName",
    "data.userFirstName",
    "data.userLastName",
    "category"
  ],
  weights: [10, 9, 8, 7, 6, 8]
}

export const getSearchableFields = () => {
  const fields = []
  config.searchFields.forEach(item => {
    fields.push(item)
    fields.push(`${item}.keyword`)
    fields.push(`${item}.search`)
  })

  return fields
}

export const getWeights = () => {
  const weights = []
  config.searchFields.forEach((item, index) => {
    weights.push(index)
    weights.push(index)
    weights.push(1)
  })

  return weights
}
