import React from "react"

import TransactionFilters from "./TransactionFilters"
import EntitiesFilter from "./EntitiesFilters"
import TasksFilter from "./TaskFilters"
import UserFilter from "./UserFilters"

const Filters = ({ view }) => (
  <>
    {view === "Transactions" && <TransactionFilters />}
    {view === "Entities" && <EntitiesFilter />}
    {view === "Users" && <UserFilter />}
    {view === "Tasks" && <TasksFilter />}
  </>
)

export default Filters
