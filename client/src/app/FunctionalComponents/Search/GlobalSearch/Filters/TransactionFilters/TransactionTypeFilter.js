import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="TransactionType"
    dataField="category.keyword"
    title="Transaction Type"
    filter="Transaction Type"
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Transactions"]
    }}
    showCount={false}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
