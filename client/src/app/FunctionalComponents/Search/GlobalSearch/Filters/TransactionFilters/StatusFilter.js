import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="Status"
    dataField="data.tranStatus.keyword"
    title="Transaction Status"
    filter="Transaction Status"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Transactions"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
