import React from "react"
import styled from "styled-components"
import { SingleRange } from "@appbaseio/reactivesearch"

const StyledRange = styled(SingleRange)`
  div {
    border-radius: 3px;
  }
`

const PrincipalAmount = () => (
  <StyledRange
    componentId="PrincipalAmount"
    dataField="data.tranParAmount"
    title="Principal Amount"
    data={[
      { end: 10000000, start: 0, label: "Less than USD 10M" },
      { end: 20000000, start: 11000000, label: "USD 11M - 20M" },
      { end: 50000000, start: 21000000, label: "USD 21M - 50M" },
      { end: 100000000, start: 51000000, label: "USD 51M - 100M" },
      { end: 100000000000000000, start: 100000000, label: "Greater than 100M" }
    ]}
    showFilter
    filterLabel="Principal Amount"
    URLParams={false}
  />
)

export default PrincipalAmount
