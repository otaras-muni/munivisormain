import React from "react"
import styled from "styled-components"
import { DateRange } from "@appbaseio/reactivesearch"
import moment from "moment"

const StyledDate = styled(DateRange)`
  div {
    border-radius: 3px;
  }

  input {
    height: 27px !important;
    font-size: 0.8rem !important;
  }
`

const DateFilter = () => (
  <StyledDate
    componentId="CreatedAtRange"
    dataField="data.createdAt"
    title=""
    showFilter
    filterLabel="Created At Range"
    numberOfMonths={1}
    URLParams={false}
    clickUnselectsDay
    customQuery={(value, props) => {
      let query = null
      if (value) {
        query = [
          {
            range: {
              [props.dataField]: {
                gte: moment(value.start).startOf("day"),
                lte: moment(value.end).endOf("day")
              }
            }
          }
        ]
      }
      return query ? { query: { bool: { must: query } } } : null
    }}
    queryFormat="date_time_no_millis"
  />
)

export default DateFilter
