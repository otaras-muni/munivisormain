import React from "react"

import CreateDateFilter from "./CreateDateFilter"
import CreateDateRangeFilter from "./CreateDateRange"
import ParAmountFilter from "./ParAmountFilter"
import StatusFilter from "./StatusFilter"
import TransactionTypeFilter from "./TransactionTypeFilter"

const Filters = () => (
  <>
    <TransactionTypeFilter />
    <hr />
    <ParAmountFilter />
    <hr />
    <StatusFilter />
    <hr />
    <CreateDateFilter />
    <CreateDateRangeFilter />
  </>
)

export default Filters
