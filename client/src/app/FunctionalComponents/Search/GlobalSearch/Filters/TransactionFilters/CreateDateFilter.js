import React from "react"
import styled from "styled-components"
import { SingleRange } from "@appbaseio/reactivesearch"

const StyledRange = styled(SingleRange)`
  div {
    border-radius: 3px;
  }
`

const DateFilter = () => (
  <StyledRange
    componentId="CreatedDate"
    dataField="data.createdAt"
    title="Created Date"
    data={[
      { end: "now", start: "now-1d/m", label: "Last 24 Hours" },
      { end: "now", start: "now-1M/m", label: "Last Month" },
      { end: "now", start: "now-1y/m", label: "Last Year" },
      { end: "now", start: "now-5y/m", label: "Last 5 Years" },
      { end: "now", start: "now-20y/m", label: "Last 20 Years" }
    ]}
    showFilter
    filterLabel="Date"
    URLParams={false}
  />
)

export default DateFilter
