import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="TaskType"
    dataField="data.taskDetails.taskType.keyword"
    title="Task Type"
    filter="Task Type"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Tasks"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
