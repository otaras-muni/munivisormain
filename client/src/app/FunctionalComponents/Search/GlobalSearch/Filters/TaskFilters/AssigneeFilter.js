import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="Assignee"
    dataField="data.taskDetails.taskAssigneeName.keyword"
    title="Assignee"
    filter="Assignee"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Tasks"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
