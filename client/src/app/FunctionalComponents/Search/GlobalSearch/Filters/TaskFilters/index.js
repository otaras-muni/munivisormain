import React from "react"

import TaskStatus from "./TaskStatusFilter"
import TaskType from "./TaskTypeFilter"
import TaskDueDate from "./DueDateFilter"
import TaskStartDate from "./StartDateFilter"
import TaskPriority from "./TaskPriority"
import TaskRead from "./TaskRead"
import AssigneeFilter from "./AssigneeFilter"
import TaskCategory from "./TaskCategory"

const Filters = () => (
  <>
    <TaskStartDate />
    <hr />
    <TaskDueDate />
    <hr />
    <TaskStatus />
    <hr />
    <TaskRead />
    <hr />
    <TaskType />
    <hr />
    <TaskCategory />
    <hr />
    <TaskPriority />
    <hr />
    <AssigneeFilter />
  </>
)

export default Filters
