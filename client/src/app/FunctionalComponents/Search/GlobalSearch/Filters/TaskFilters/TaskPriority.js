import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="TaskPriority"
    dataField="data.taskDetails.taskPriority.keyword"
    title="Task Priority"
    filter="Task Priority"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Tasks"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
