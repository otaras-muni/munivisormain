import React from "react"
import { MultiDataList } from "@appbaseio/reactivesearch"

export default () => (
  <MultiDataList
    componentId="TaskRead"
    dataField="data.taskDetails.taskUnread"
    data={[
      {
        label: "Read",
        value: true
      },
      {
        label: "Unread",
        value: false
      }
    ]}
    title="Task Read"
    filter="Task Read"
    showSearch={false}
  />
)
