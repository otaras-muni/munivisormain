import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="TaskStatus"
    dataField="data.taskDetails.taskStatus.keyword"
    title="Task Status"
    filter="Task Status"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Tasks"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
