import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="TaskCategory"
    dataField="data.taskCategory.keyword"
    title="Task Category"
    filter="Task Category"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Tasks"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
