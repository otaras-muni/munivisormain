import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="MarketRole"
    dataField="data.entityFlags.marketRole.keyword"
    title="Market Role"
    filter="Market Role"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Entities"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
