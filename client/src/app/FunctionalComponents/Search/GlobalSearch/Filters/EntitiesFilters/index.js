import React from "react"

import MarketRole from "./MarketRole"
import TenantRelation from "./TenantRelationFilter"

const Filters = () => (
  <>
    <MarketRole />
    <hr />
    <TenantRelation />
  </>
)

export default Filters
