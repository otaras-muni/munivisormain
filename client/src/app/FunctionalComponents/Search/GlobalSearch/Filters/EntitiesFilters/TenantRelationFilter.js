import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="TenantRelationShip"
    dataField="data.relationshipToTenant.keyword"
    title="Tenant Relation"
    filter="Tenant Relation"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Entities"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
