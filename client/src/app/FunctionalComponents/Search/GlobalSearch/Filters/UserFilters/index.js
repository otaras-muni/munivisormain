import React from "react"

import FirmFilter from "./FirmFilter"
import TenantRelation from "./TenantRelationFilter"

const Filters = () => (
  <>
    <FirmFilter />
    <hr />
    <TenantRelation />
  </>
)

export default Filters
