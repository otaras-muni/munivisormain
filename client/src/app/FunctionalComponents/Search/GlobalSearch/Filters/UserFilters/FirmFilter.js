import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="FirmFilter"
    dataField="data.userFirmName.keyword"
    title="Firm"
    filter="Firm"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Users"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
