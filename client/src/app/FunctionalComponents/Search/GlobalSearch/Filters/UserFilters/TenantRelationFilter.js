import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default () => (
  <StyledMultiList
    componentId="UserTenantRelationShip"
    dataField="data.entityRelationshipToTenant.keyword"
    title="Tenant Relation"
    filter="Tenant Relation"
    showCount={false}
    react={{
      and: ["globalSearchEntitlement", "viewQuery_Users"]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
