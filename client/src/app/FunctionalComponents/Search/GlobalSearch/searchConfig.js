export const tabsConfig = [
  {
    name: "Transactions",
    query: {
      query: {
        terms: {
          "category.keyword": [
            "Deals",
            "Manage Rfps",
            "MA RFPs",
            "Bank Loans",
            "Derivatives",
            "Business Development Activities",
            "Other Transactions"
          ]
        }
      }
    },
    searchFields: [
      "data.tranClientName",
      "data.tranFirmName",
      "data.tranType",
      "data.tranSubType",
      "data.activityDescription",
      "data.tranPrimarySector",
      "data.tranSecondarySector",
      "data.tranStatus",
      "data.historicalData"
    ]
  },
  {
    name: "Entities",
    query: {
      query: {
        term: { "category.keyword": "Entity Details" }
      }
    },
    searchFields: [
      "data.entityName",
      "data.entityType",
      "data.entityFlags.marketRole",
      "data.entityAliases",
      "data.entityPrimaryAddress.formatted_address",
      "data.historicalData"
    ]
  },
  {
    name: "Users",
    query: {
      query: {
        term: { "category.keyword": "User Details" }
      }
    },
    searchFields: [
      "data.userFirstName",
      "data.userFirmName",
      "data.userLastName",
      "data.userPrimaryPhone.phoneNumber",
      "data.userPrimaryAddress.addressLine1",
      "data.userPrimaryAddress.addressLine2",
      "data.userPrimaryAddress.country",
      "data.userPrimaryAddress.state",
      "data.userPrimaryAddress.city",
      "data.userPrimaryAddress.zipCode.zip1",
      "data.userPrimaryAddress.zipCode.zip2",
      "data.userFlags",
      "data.entityRelationshipToTenant",
      "data.historicalData"
    ]
  },
  {
    name: "Tasks",
    query: {
      query: {
        term: { "category.keyword": "Task Info" }
      }
    },
    searchFields: [
      "data.taskDetails.taskStatus",
      "data.taskDetails.taskType",
      "data.taskDetails.taskDescription",
      "data.taskDetails.taskAssigneeName",
      "data.taskDetails.taskPriority",
      "data.activityDescription"
    ]
  }
]

export const SEARCH_FIELDS = [
  "category",
  "data.tranClientName",
  "data.tranFirmName",
  "data.tranType",
  "data.tranSubType",
  "data.activityDescription",
  "data.userDetails.userName",
  "data.userDetails.userLastName",
  "data.tranPrimarySector",
  "data.tranSecondarySector",
  "data.tranStatus",
  "data.entityName",
  "data.entityType",
  "data.entityFlags.marketRole",
  "data.entityFlags.issuerFlags",
  "data.entityAliases",
  "data.entityLinkedCusips.debtType",
  "data.entityLinkedCusips.associatedCusip6",
  "data.userDetails.userAddresses.country",
  "data.userDetails.userAddresses.state",
  "data.taskDetails.taskStatus",
  "data.taskDetails.taskType",
  "data.taskDetails.taskDescription",
  "data.taskDetails.taskAssigneeName",
  "data.taskDetails.taskPriority"
]

export const getSearchableFields = view => {
  const fields = []
  tabsConfig[view].searchFields.forEach(item => {
    fields.push(item)
    fields.push(`${item}.keyword`)
    fields.push(`${item}.search`)
  })

  return fields
}

export const getWeights = view => {
  const weights = []
  tabsConfig[view].searchFields.forEach(() => {
    weights.push(3)
    weights.push(3)
    weights.push(1)
  })

  return weights
}
