import React from "react"

import StyledSearchBar from "../../Layout/StyledSearchBar"
import { getWeights, getSearchableFields } from "../searchConfig"

const Search = ({ view }) => (
  <StyledSearchBar
    componentId="Search"
    dataField={[...getSearchableFields(view)]}
    fieldWeights={[...getWeights(view)]}
    autosuggest={false}
    debounce={300}
    highlight
  />
)

export default Search
