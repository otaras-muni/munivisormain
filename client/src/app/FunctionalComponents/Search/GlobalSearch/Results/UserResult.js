/* eslint-disable react/no-danger, no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import { Link } from "react-router-dom"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import HistoricalData from "./HistoricalData"
import Stats from "../../Layout/Stats"

const defaultUrl = "/dashboard"

const Result = ({ urls, pageSize }) => (
  <>
    <SelectedFilters />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="UserSearchResult"
        dataField="_doc"
        react={{
          and: [
            "Search",
            "viewQuery_Users",
            "FirmFilter",
            "UserTenantRelationShip",
            "globalSearchEntitlement"
          ]
        }}
        pagination
        paginationAt="both"
        excludeFields={["content"]}
        size={pageSize}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * pageSize + 1} -{" "}
              {stats.currentPage * pageSize + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        renderNoResults={NoResult}
        renderError={() => <Error />}
        renderAllData={({ results }) => (
          <div style={{ marginTop: 10 }}>
            {Boolean(results.length) && (
              <ReactTable
                showPagination={false}
                pageSize={results.length}
                minRows={1}
                className="-striped -highlight is-bordered"
                data={results}
                columns={[
                  {
                    id: "User Name",
                    Header: "User Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={urls.defaultUrls[value.data._id] || defaultUrl}
                        >
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(value, "data.userFirstName")
                            }}
                          />
                          &nbsp;
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(value, "data.userLastName")
                            }}
                          />
                        </Link>
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userFirstName) || "").localeCompare(
                        (b && b.data && b.data.userFirstName) || ""
                      )
                  },
                  {
                    id: "Entity Name",
                    Header: "Entity Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={
                            urls.defaultUrls[value.data.userEntityId] ||
                            defaultUrl
                          }
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.userFirmName")
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userFirmName) || "").localeCompare(
                        (b && b.data && b.data.userFirmName) || ""
                      )
                  },
                  {
                    id: "Type",
                    Header: "Type",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.entityRelationshipToTenant"
                            )
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.entityRelationshipToTenant) || "").localeCompare(
                        (b && b.data && b.data.entityRelationshipToTenant) || ""
                      )
                  },
                  {
                    id: "User Address",
                    Header: "User Address",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.userPrimaryAddress.addressLine1"
                            )
                          }}
                        />
                        &nbsp;
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.userPrimaryAddress.addressLine2"
                            )
                          }}
                        />
                        &nbsp;
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.userPrimaryAddress.city")
                          }}
                        />
                        &nbsp;
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.userPrimaryAddress.state"
                            )
                          }}
                        />
                        &nbsp;
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.userPrimaryAddress.zipCode.zip1"
                            )
                          }}
                        />
                        -
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.userPrimaryAddress.zipCode.zip2"
                            )
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userPrimaryAddress && a.data.userPrimaryAddress.city) || "").localeCompare(
                        (b && b.data && b.data.userPrimaryAddress && b.data.userPrimaryAddress.city) || ""
                      )
                  },
                  {
                    id: "User Flags",
                    Header: "User Flags",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {value.data.userFlags.length ? (
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(value, "data.userFlags")
                            }}
                          />
                        ) : (
                          "-"
                        )}
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userFlags)  || [])
                        .join(",")
                        .localeCompare(((a && a.data && b.data.userFlags) || []).join(","))
                  },
                  {
                    id: "User Phone",
                    Header: "User Phone",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.userPrimaryPhone.phoneNumber",
                              "-"
                            )
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userPrimaryPhone && a.data.userPrimaryPhone.phoneNumber) || "").localeCompare(
                        (b && b.data && b.data.userPrimaryPhone && b.data.userPrimaryPhone.phoneNumber) || ""
                      )
                  },
                  {
                    id: "Historical Data",
                    Header: "Historical Data",
                    accessor: item => item,
                    Cell: ({ value }) => {
                      const val = _.get(value, "data.historicalData")
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {val && val !== "{}" ? (
                            <HistoricalData
                              data={(value.data || {}).historicalData || "{}"}
                            />
                          ) : (
                            <>-</>
                          )}
                        </div>
                      )
                    }
                  }
                ]}
              />
            )}
          </div>
        )}
      />
    </div>
  </>
)

export default Result
