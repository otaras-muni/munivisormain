/* eslint-disable react/no-danger, no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import { Link } from "react-router-dom"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import HistoricalData from "./HistoricalData"
import Stats from "../../Layout/Stats"

import { unitConvertor } from "../../../../../globalutilities/amountDecorator"

const defaultUrl = "/dashboard"

const Result = ({ urls, pageSize }) => (
  <>
    <SelectedFilters />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="TransactionSearchResult"
        dataField="_score"
        react={{
          and: [
            "Search",
            "viewQuery_Transactions",
            "Status",
            "TransactionType",
            "PrincipalAmount",
            "CreatedDate",
            "CreatedAtRange",
            "globalSearchEntitlement"
          ]
        }}
        pagination
        paginationAt="both"
        excludeFields={["content"]}
        size={pageSize}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * pageSize + 1} -{" "}
              {stats.currentPage * pageSize + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        renderNoResults={NoResult}
        renderError={() => <Error />}
        renderAllData={({ results }) => (
          <div style={{ marginTop: 10 }}>
            {Boolean(results.length) && (
              <ReactTable
                showPagination={false}
                pageSize={results.length}
                minRows={1}
                className="-striped -highlight is-bordered"
                data={results}
                columns={[
                  {
                    id: "Issuer",
                    Header: "Issuer",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={
                            urls.defaultUrls[value.data.clientEntityId] ||
                            defaultUrl
                          }
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.tranClientName")
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.tranClientName || "").localeCompare(
                        b.data.tranClientName || ""
                      )
                  },
                  {
                    id: "Description",
                    Header: "Issue Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={urls.defaultUrls[value.data._id] || defaultUrl}
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.activityDescription")
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.activityDescription || "").localeCompare(
                        b.data.activityDescription || ""
                      )
                  },
                  {
                    id: "Type",
                    Header: "Type / Sub Type",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.tranType", "-")
                          }}
                        />
                        {value.data.tranSubType && (
                          <>
                            /
                            <span
                              dangerouslySetInnerHTML={{
                                __html: _.get(value, "data.tranSubType", "-")
                              }}
                            />
                          </>
                        )}
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.tranType || "").localeCompare(
                        b.data.tranType || ""
                      )
                  },
                  {
                    id: "ParAmount",
                    Header: "Principal Amount($)",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {value.data.tranParAmount &&
                        value.data.tranParAmount !== "-"
                          ? `$ ${unitConvertor(value.data.tranParAmount)}`
                          : "-"}
                      </div>
                    ),
                    maxWidth: 250
                  },
                  {
                    id: "Primary Sector",
                    Header: "Primary Sector",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.tranPrimarySector", "-")
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.tranPrimarySector || "").localeCompare(
                        b.data.tranPrimarySector || ""
                      )
                  },
                  {
                    id: "Secondary Sector",
                    Header: "Secondary Sector",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.tranSecondarySector",
                              "-"
                            )
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.tranSecondarySector || "").localeCompare(
                        b.data.tranSecondarySector || ""
                      )
                  },
                  {
                    id: "Status",
                    Header: "Status",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.tranStatus", "-")
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.tranStatus || "").localeCompare(
                        b.data.tranStatus || ""
                      )
                  },
                  {
                    id: "Historical Data",
                    Header: "Historical Data",
                    accessor: item => item,
                    Cell: ({ value }) => {
                      const val = _.get(value, "data.historicalData")
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {val && val !== "{}" ? (
                            <HistoricalData
                              data={(value.data || {}).historicalData || "{}"}
                            />
                          ) : (
                            <>-</>
                          )}
                        </div>
                      )
                    }
                  }
                ]}
              />
            )}
          </div>
        )}
      />
    </div>
  </>
)

export default Result
