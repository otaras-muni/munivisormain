/* eslint-disable jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
import React from "react"
import { Modal } from "Global/BulmaModal" // eslint-disable-line
import ReactTable from "react-table"

class HistoricalData extends React.Component {
  state = {
    isModalOpen: false
  }

  toggleModal = () => {
    this.setState(currentState => ({
      isModalOpen: !currentState.isModalOpen
    }))
  }

  render() {
    const { isModalOpen } = this.state
    const { data } = this.props
    const parsedData = JSON.parse(data)
    const tableData = Object.keys(parsedData).reduce(
      (array, key) => [
        ...array,
        {
          key,
          value: parsedData[key]
        }
      ],
      []
    )
    return (
      <>
        <span
          onClick={this.toggleModal}
          style={{
            color: "dodgerblue",
            cursor: "pointer"
          }}
        >
          View more
        </span>
        <Modal
          closeModal={this.toggleModal}
          modalState={isModalOpen}
          title="Historical Information"
        >
          <ReactTable
            showPagination={false}
            pageSize={tableData.length}
            minRows={1}
            className="-striped -highlight is-bordered"
            data={tableData}
            columns={[
              {
                id: "key",
                Header: "Information",
                accessor: item => item,
                Cell: ({ value }) => (
                  <div className="hpTablesTd wrap-cell-text">{value.key}</div>
                ),
                sortMethod: (a, b) => (a.key || "").localeCompare(b.key || "")
              },
              {
                id: "value",
                Header: "Details",
                accessor: item => item,
                Cell: ({ value }) => (
                  <div className="hpTablesTd wrap-cell-text">{value.value}</div>
                ),
                sortMethod: (a, b) =>
                  (a.value || "").localeCompare(b.value || "")
              }
            ]}
          />
        </Modal>
      </>
    )
  }
}

export default HistoricalData
