/* eslint-disable indent, react/no-danger, no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import { NavLink } from "react-router-dom"
import ReactTooltip from "react-tooltip"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import Stats from "../../Layout/Stats"
import dateFormat from "../../../../../globalutilities/dateFormat"

const defaultUrl = "/dashboard"

const Result = ({ urls, pageSize }) => (
  <>
    <SelectedFilters />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="TaskSearchResult"
        dataField="_doc"
        react={{
          and: [
            "Search",
            "viewQuery_Tasks",
            "TaskStartDate",
            "TaskDueDate",
            "TaskPriority",
            "TaskRead",
            "TaskStatus",
            "TaskType",
            "Assignee",
            "TaskCategory",
            "globalSearchEntitlement"
          ]
        }}
        pagination
        paginationAt="both"
        excludeFields={["content"]}
        size={pageSize}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * pageSize + 1} -{" "}
              {stats.currentPage * pageSize + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        renderNoResults={NoResult}
        renderError={() => <Error />}
        renderAllData={({ results }) => (
          <div style={{ marginTop: 10 }}>
            {Boolean(results.length) && (
              <ReactTable
                showPagination={false}
                pageSize={results.length}
                minRows={1}
                className="-striped -highlight is-bordered"
                data={results}
                columns={[
                  {
                    id: "Task",
                    Header: "Task",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <>
                        <span
                          style={{
                            width: "140px",
                            height: "18px"
                          }}
                          className="overflow"
                          data-tip
                          data-for={`task${value._id}`}
                        >
                          {value && !value.data.relatedActivityDetails ? (
                            value.data.taskDetails.taskType === "Compliance" ? (
                              <div className="hpTablesTd wrap-cell-text overflow">
                                <NavLink
                                  to={
                                    value.data.taskDetails.taskNotes
                                      ? `/cac/view-control/view-detail?id=${
                                          value.data.taskDetails.taskNotes
                                        }`
                                      : "/cac/view-control"
                                  }
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(
                                      value,
                                      "data.taskDetails.taskDescription",
                                      "-"
                                    )
                                  }}
                                />
                              </div>
                            ) : (
                              <span className="hpTablesTd wrap-cell-text overflow">
                                <NavLink
                                  to={`/tasks/${value._id}`}
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(
                                      value,
                                      "data.taskDetails.taskDescription",
                                      "-"
                                    )
                                  }}
                                />
                              </span>
                            )
                          ) : value.data.relatedActivityDetails ? (
                            <div className="hpTablesTd wrap-cell-text overflow">
                              <NavLink
                                to={`${(urls.allurls[value.data.tranId] || {})[
                                  "check-track"
                                ] || defaultUrl}?cid=${
                                  value.data.relatedActivityDetails
                                    .activityContext
                                }`}
                                dangerouslySetInnerHTML={{
                                  __html: _.get(
                                    value,
                                    "data.taskDetails.taskDescription",
                                    "-"
                                  )
                                }}
                              />
                            </div>
                          ) : (
                            <span className="hpTablesTd wrap-cell-text overflow">
                              <NavLink
                                to={`/tasks/${value._id}`}
                                dangerouslySetInnerHTML={{
                                  __html: _.get(
                                    value,
                                    "data.taskDetails.taskDescription",
                                    "-"
                                  )
                                }}
                              />
                            </span>
                          )}
                        </span>
                        <ReactTooltip
                          id={`task${value._id}`}
                          className="custom-tooltip"
                          place="right"
                        >
                          <div
                            dangerouslySetInnerHTML={{
                              __html: _.get(
                                value,
                                "data.taskDetails.taskDescription",
                                "-"
                              )
                            }}
                          />
                        </ReactTooltip>
                      </>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.taskDetails.taskDescription || "").localeCompare(
                        b.data.taskDetails.taskDescription || ""
                      )
                  },
                  {
                    id: "Assignee",
                    Header: "Assignee",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <NavLink
                          to={
                            urls.defaultUrls[
                              value.data.taskDetails.taskAssigneeUserId
                            ] || defaultUrl
                          }
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.taskDetails.taskAssigneeName"
                            )
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.taskDetails.taskAssigneeName || "").localeCompare(
                        b.data.taskDetails.taskAssigneeName || ""
                      )
                  },
                  {
                    id: "Task Type",
                    Header: "Task Type",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.taskDetails.taskType",
                              "-"
                            )
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.taskDetails.taskType || "").localeCompare(
                        b.data.taskDetails.taskType || ""
                      )
                  },
                  {
                    id: "Task Status",
                    Header: "Task Status",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.taskDetails.taskStatus",
                              "-"
                            )
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.taskDetails.taskStatus || "").localeCompare(
                        b.data.taskDetails.taskStatus || ""
                      )
                  },
                  {
                    id: "Task Priority",
                    Header: "Task Priority",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(
                              value,
                              "data.taskDetails.taskPriority",
                              "-"
                            )
                          }}
                        />
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.taskDetails.taskPriority || "").localeCompare(
                        b.data.taskDetails.taskPriority || ""
                      )
                  },
                  {
                    id: "startDate",
                    Header: "Start date",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value.data
                      return (
                        <div className="hpTablesTd -striped wrap-cell-text">
                          {dateFormat(
                            item.taskDetails && item.taskDetails.taskStartDate
                          )}
                        </div>
                      )
                    },
                    maxWidth: 150,
                    sortMethod: (a, b) => {
                      a = new Date(a.data.taskDetails.taskStartDate).getTime()
                      b = new Date(b.data.taskDetails.taskStartDate).getTime()
                      return b > a ? 1 : -1
                    }
                  },
                  {
                    id: "dueDate",
                    Header: "Due date",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value.data
                      return (
                        <div className="hpTablesTd -striped wrap-cell-text">
                          {dateFormat(
                            item.taskDetails && item.taskDetails.taskEndDate
                          )}
                        </div>
                      )
                    },
                    maxWidth: 150,
                    sortMethod: (a, b) => {
                      a = new Date(a.data.taskDetails.taskEndDate).getTime()
                      b = new Date(b.data.taskDetails.taskEndDate).getTime()
                      return b > a ? 1 : -1
                    }
                  },
                  {
                    id: "Related Transaction",
                    Header: "Related Transaction",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {value.data.activityDescription ? (
                          <NavLink
                            to={
                              urls.defaultUrls[value.data.tranId] || defaultUrl
                            }
                          >
                            <span
                              dangerouslySetInnerHTML={{
                                __html: _.get(
                                  value,
                                  "data.activityDescription",
                                  "-"
                                )
                              }}
                            />
                          </NavLink>
                        ) : (
                          "-"
                        )}
                      </div>
                    ),
                    maxWidth: 250,
                    sortMethod: (a, b) =>
                      (a.data.taskDetails.taskPriority || "").localeCompare(
                        b.data.taskDetails.taskPriority || ""
                      )
                  }
                ]}
              />
            )}
          </div>
        )}
      />
    </div>
  </>
)

export default Result
