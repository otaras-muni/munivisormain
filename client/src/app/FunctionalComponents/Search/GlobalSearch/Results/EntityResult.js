/* eslint-disable react/no-danger, no-underscore-dangle, indent */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import { Link } from "react-router-dom"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import HistoricalData from "./HistoricalData"
import Stats from "../../Layout/Stats"

const defaultUrl = "/dashboard"

const Result = ({ urls, pageSize }) => (
  <>
    <SelectedFilters />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="EntitySearchResult"
        dataField="_doc"
        react={{
          and: [
            "Search",
            "viewQuery_Entities",
            "MarketRole",
            "TenantRelationShip",
            "globalSearchEntitlement"
          ]
        }}
        pagination
        paginationAt="both"
        excludeFields={["content"]}
        size={pageSize}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * pageSize + 1} -{" "}
              {stats.currentPage * pageSize + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        renderNoResults={NoResult}
        renderError={() => <Error />}
        renderAllData={({ results }) => (
          <div style={{ marginTop: 10 }}>
            {Boolean(results.length) && (
              <ReactTable
                showPagination={false}
                pageSize={results.length}
                minRows={1}
                className="-striped -highlight is-bordered"
                data={results}
                columns={[
                  {
                    id: "Entity Name",
                    Header: "Entity Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={urls.defaultUrls[value.data._id] || defaultUrl}
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.entityName")
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      (a.data.entityName || "").localeCompare(
                        b.data.entityName || ""
                      )
                  },
                  {
                    id: "Entity Type",
                    Header: "Entity Type",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.entityType")
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      (a.data.entityType || "").localeCompare(
                        b.data.entityType || ""
                      )
                  },
                  {
                    id: "Entity Address",
                    Header: "Entity Address",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {value &&
                        value.data &&
                        value.data.entityPrimaryAddress &&
                        value.data.entityPrimaryAddress &&
                        value.data.entityPrimaryAddress.url ? (
                          <a
                            href={value.data.entityPrimaryAddress.url}
                            target="_blank"
                            rel="noopener noreferrer"
                            dangerouslySetInnerHTML={{
                              __html: _.get(
                                value,
                                "data.entityPrimaryAddress.formatted_address"
                              )
                            }}
                          />
                        ) : (
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(
                                value,
                                "data.entityPrimaryAddress.formatted_address"
                              )
                            }}
                          />
                        )}
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      (
                        (a && a.data && a.data.entityPrimaryAddress && a.data.entityPrimaryAddress.formatted_address) || ""
                      ).localeCompare(
                        (b && b.data && b.data.entityPrimaryAddress && b.data.entityPrimaryAddress.formatted_address) || ""
                      )
                  },
                  {
                    id: "Entity Aliases",
                    Header: "Entity Aliases",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {value.data.entityAliases.length ? (
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(value, "data.entityAliases")
                            }}
                          />
                        ) : (
                          "-"
                        )}
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.entityAliases) || [])
                        .join(",")
                        .localeCompare(((b && b.data && b.data.entityAliases) || []).join(","))
                  },
                  {
                    id: "Market Role",
                    Header: "Market Role",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {((value.data.entityFlags || {}).marketRole || [])
                          .length ? (
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(
                                value,
                                "data.entityFlags.marketRole",
                                "-"
                              )
                            }}
                          />
                        ) : (
                          "-"
                        )}
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.entityFlags && a.data.entityFlags.marketRole) || [])
                        .join(",")
                        .localeCompare(
                          ((b && b.data && b.data.entityFlags && b.data.entityFlags.marketRole) || []).join(",")
                        )
                  },
                  {
                    id: "Historical Data",
                    Header: "Historical Data",
                    accessor: item => item,
                    Cell: ({ value }) => {
                      const val = _.get(value, "data.historicalData")
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {val && val !== "{}" ? (
                            <HistoricalData
                              data={(value.data || {}).historicalData || "{}"}
                            />
                          ) : (
                            <>-</>
                          )}
                        </div>
                      )
                    }
                  }
                ]}
              />
            )}
          </div>
        )}
      />
    </div>
  </>
)

export default Result
