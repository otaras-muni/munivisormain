import React from "react"

import TransactionResult from "./TransactionResult"
import EntityResult from "./EntityResult"
import TaskResult from "./TaskResult"
import UserResult from "./UserResult"

const PAGE_SIZE = 10

const Result = ({ view, urls }) => (
  <>
    {view === "Transactions" && (
      <TransactionResult pageSize={PAGE_SIZE} urls={urls} />
    )}
    {view === "Entities" && <EntityResult pageSize={PAGE_SIZE} urls={urls} />}
    {view === "Users" && <UserResult pageSize={PAGE_SIZE} urls={urls} />}
    {view === "Tasks" && <TaskResult pageSize={PAGE_SIZE} urls={urls} />}
  </>
)

export default Result
