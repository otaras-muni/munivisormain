import React from "react"
import { ReactiveBase, ReactiveComponent } from "@appbaseio/reactivesearch"
import axios from "axios"

import Layout from "../Layout/Layout"
import Container from "../Layout/Container"
import SearchContainer from "../Layout/SearchContainer"
import SideBar from "../Layout/Sidebar"
import Content from "../Layout/Content"
import FilterLabel from "../Layout/FilterLabel"
import SearchBar from "./SearchBar"
import Tab from "./Tab"
import Filters from "./Filters"
import Result from "./Results"
import Loader from "../../../GlobalComponents/Loader"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import { tabsConfig } from "./searchConfig"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { globalSearchEntitlement } from "../utils/entitlement-query"

class GlobalSearch extends React.Component {
  state = {
    currentTab: 0,
    isFetching: true,
    defaultQuery: null,
    isFetchingUrls: true,
    urls: null
  }

  componentDidMount() {
    this.fetchEntitlements()
    this.fetchUrls()
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  fetchUrls = async () => {
    try {
      const {
        auth: {
          loginDetails: { relatedFaEntities }
        }
      } = this.props

      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}common/geturlsfortenant/${
          relatedFaEntities[0].entityId
        }`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { urlObject } = res.data

      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false,
          urls: urlObject
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false
        })
      }
      console.error(err)
    }
  }

  handleTabChange = newTabIndex => {
    this.setState({
      currentTab: newTabIndex
    })
  }

  fetchEntitlements = async () => {
    try {
      const {
        auth: {
          userEntities: { relationshipToTenant }
        }
      } = this.props
      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}entitlements/nav`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { data } = res.data
      const defaultQuery = globalSearchEntitlement(
        relationshipToTenant,
        data.all || []
      )

      if (!this.isCancelled) {
        this.setState({
          isFetching: false,
          defaultQuery
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetching: false
        })
      }
      console.error(err)
    }
  }

  handleTabChange = newTabIndex => {
    this.setState({
      currentTab: newTabIndex
    })
  }

  render() {
    const { auth } = this.props
    const {
      currentTab,
      isFetching,
      defaultQuery,
      isFetchingUrls,
      urls
    } = this.state

    return (
      <>
        {isFetching || isFetchingUrls ? (
          <Loader />
        ) : (
          <ReactiveBase
            url={ELASTIC_SEARCH_URL}
            app={`tenant_data_${
              auth.loginDetails.relatedFaEntities[0].entityId
            }`}
            type="doc"
            headers={{
              Authorization: localStorage.getItem("token")
            }}
          >
            <ReactiveComponent
              customQuery={() => tabsConfig[currentTab].query}
              componentId={`viewQuery_${tabsConfig[currentTab].name}`}
              key={`viewQuery_${tabsConfig[currentTab].name}`}
            />
            <ReactiveComponent
              customQuery={() => defaultQuery}
              componentId="globalSearchEntitlement"
              key="globalSearchEntitlement"
            />
            <Container>
              <Tab currentTab={currentTab} onTabChange={this.handleTabChange} />
              <SearchContainer hasTopBar>
                <SearchBar view={currentTab} />
              </SearchContainer>
              <Layout>
                <SideBar>
                  <FilterLabel>Filters</FilterLabel>
                  <Filters view={tabsConfig[currentTab].name} />
                </SideBar>
                <Content style={{ paddingTop: 80 }}>
                  <Result view={tabsConfig[currentTab].name} urls={urls} />
                </Content>
              </Layout>
            </Container>
          </ReactiveBase>
        )}
      </>
    )
  }
}

export default GlobalSearch
