export const RELATIONSHIPS = {
  SELF: "Self",
  CLIENT: "Client",
  PROSPECT: "Prospect",
  THIRD_PARTY: "Third Party"
}

const relationValues = Object.values(RELATIONSHIPS)
const selfOnly = relationValues[0]

const contextMapper = {
  ENTITYDOCS: {
    url: "/clients-propects/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Clients / Prospects - Documents"
  },
  RFPS: {
    url: "/rfp/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Manage RFPs for Clients"
  },
  DEALS: {
    url: "/deals/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Debt Issuance"
  },
  BANKLOANS: {
    url: "/loan/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Debt Issue - Bank Loans"
  },
  MARFPS: {
    url: "/marfps/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "MA RFPs"
  },
  DERIVATIVES: {
    url: "/derivatives/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Derivatives"
  },
  OTHERS: {
    url: "/others/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Other Transactions"
  },
  BUSDEV: {
    url: "/bus-development/___CONTEXT_ID___",
    entitlementContext: relationValues,
    title: "MA Business Development Activities"
  },
  ENTITYCONTRACT: {
    url: "/clients-propects/___CONTEXT_ID___/contracts",
    entitlementContext: relationValues,
    title: "Entity Contract"
  },
  THIRDPARTYDOCS: {
    url: "/thirdparties/___CONTEXT_ID___/documents",
    entitlementContext: relationValues,
    title: "Third Party - Documents"
  },
  TASKS: {
    url: "/tasks/___CONTEXT_ID___",
    entitlementContext: relationValues,
    title: "Tasks - Documents"
  },
  POLICIESPROCEDURES: {
    url: "/comp-polandproc",
    entitlementContext: selfOnly,
    title: "Compliance - Policies & Procedures"
  },
  SVGENADMIN: {
    url: "/compliance/cmp-sup-general/record-keeping",
    entitlementContext: selfOnly,
    title: "Compliance - General Administration"
  },
  CONTROLS: {
    url: "/cac/controls-list/controls-lists",
    entitlementContext: selfOnly,
    title: "Controls"
  },
  USERSDOCS: {
    url: "/tools-docs/manage-doc",
    entitlementContext: selfOnly,
    title: "User documents"
  },
  SHAREDOCS: {
    url: "/tools-docs/manage-doc",
    entitlementContext: selfOnly,
    title: "Shared documents"
  },
  MIGRATIONTOOLS: {
    url: "/admin-migtools",
    entitlementContext: selfOnly,
    title: "Firm - Migration and Onboarding Files"
  },
  ENTITYLOGO: {
    url: "/admin-firms/___CONTEXT_ID___/firms",
    entitlementContext: selfOnly,
    title: "Firm - Entity Logo"
  },
  SVOBLIGATIONS: {
    url: "/compliance/cmp-sup-compobligations/recordkeeping",
    entitlementContext: selfOnly,
    title: "Supervisory Compliance Obligations"
  },
  BILLINGEXPENSERECEIPTS: {
    url: "/tools-billing",
    entitlementContext: selfOnly,
    title: "Billing & Exepenses Receipts"
  },
  SVCLIENTCOMPLAINTS: {
    url: "/compliance/cmp-sup-complaints/complaints",
    entitlementContext: selfOnly,
    title: "Compliance - Client Education & Research - Compliants"
  },
  SVBUSINESSCONDUCT: {
    url: "/compliance/cmp-sup-busconduct/business-conduct",
    entitlementContext: selfOnly,
    title: "Compliance Business Conduct"
  },
  SVPROFQUALIFICATIONS: {
    url: "/compliance/cmp-sup-prof/associate-persons",
    entitlementContext: selfOnly,
    title: "Compliance - Professional Qualifications"
  },
  SVPOLICONTRELATEDDOCS: {
    url: "/compliance/cmp-sup-political/instructions",
    entitlementContext: selfOnly,
    title: "Compliance - Political Contributions"
  },
  SVGIFTSGRATUITIES: {
    url: "/compliance/cmp-sup-gifts/instructions",
    entitlementContext: selfOnly,
    title: "Compliance - Gifts and Gratuities"
  },
  USERDOCS: {
    url: "/tools-docs/manage-doc",
    entitlementContext: selfOnly,
    title: "User documents"
  }
}
export default contextMapper
