import contextMapper, { RELATIONSHIPS } from "./context-mapper"

export const docParserEntitlement = (relationshipToTenant, contextIds) => {
  if (relationshipToTenant === RELATIONSHIPS.SELF) {
    if (contextIds.length) {
      return {
        query: {
          bool: {
            must: [
              {
                terms: {
                  "meta.contextId.keyword": contextIds
                }
              }
            ]
          }
        }
      }
    }
    return {}
  }

  const allowedContext = Object.keys(contextMapper).filter(item =>
    Array.isArray(contextMapper[item].entitlementContext)
  )

  const query = {
    query: {
      bool: {
        must: [
          {
            terms: { "meta.contextType.keyword": allowedContext }
          },
          { terms: { "meta.contextId.keyword": contextIds } }
        ]
      }
    }
  }

  return query
}

export const globalSearchEntitlement = (relationshipToTenant, contextIds) => {
  if (relationshipToTenant === RELATIONSHIPS.SELF) {
    if (contextIds.length) {
      return {
        query: {
          bool: {
            must: [
              {
                terms: {
                  "data._id.keyword": contextIds
                }
              }
            ]
          }
        }
      }
    }
    return {}
  }

  const query = {
    query: {
      bool: {
        must: [{ terms: { "data._id.keyword": contextIds } }]
      }
    }
  }

  return query
}
