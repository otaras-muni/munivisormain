const hightlightPhrase = str => {
  if (
    /<(?=.*? .*?\/ ?>|br|hr|input|!--|wbr)[a-z]+.*?>|<([a-z]+).*?<\/\1>/i.test(
      str
    )
  ) {
    const tmp = str.replace(/<\/?[^>]+(>|$)/g, "")
    return `<mark>${tmp}</mark>`
  }

  return str
}

export default hightlightPhrase
