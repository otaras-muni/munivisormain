import React from "react"

import { getSearchableFields, getWeights, searchFields } from "../searchConfig"

import StyledSearch from "../../Layout/StyledSearchBar"

const getfields = () => {
  const fields = getSearchableFields()
  const weights = getWeights()
  return fields.map((f, i) => `${f}^${weights[i]}`)
}

const getHighlightFields = (fields = searchFields) =>
  fields.reduce(
    (obj, f) => ({
      ...obj,
      [f]: {},
      [`${f}.search`]: {}
    }),
    {}
  )

const SearchBar = () => (
  <StyledSearch
    componentId="Search"
    dataField={["meta.createdBy"]}
    autosuggest={false}
    highlight
    debounce={300}
    customQuery={value => {
      if (value) {
        return {
          query: {
            bool: {
              should: [
                {
                  match_phrase: {
                    doc_content: value
                  }
                },
                {
                  match_phrase: {
                    "doc_content.search": value
                  }
                },
                {
                  multi_match: {
                    query: value,
                    fields: [...getfields()]
                  }
                }
              ]
            }
          },
          _source: {
            excludes: ["doc_content"]
          },
          highlight: {
            pre_tags: ["<mark>"],
            post_tags: ["</mark>"],
            order: "score",
            fields: {
              doc_content: {
                fragment_size: 300,
                matched_fields: ["doc_content"],
                type: "fvh"
              },
              "doc_content.search": {
                fragment_size: 300,
                matched_fields: ["doc_content.search"],
                type: "fvh"
              },
              ...getHighlightFields()
            }
          }
        }
      }
      return {}
    }}
  />
)

export default SearchBar
