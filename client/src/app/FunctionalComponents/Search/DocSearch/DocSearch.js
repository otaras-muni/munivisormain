import React from "react"
import { ReactiveBase, ReactiveComponent } from "@appbaseio/reactivesearch"
import axios from "axios"

import Layout from "../Layout/Layout"
import Container from "../Layout/Container"
import SearchContainer from "../Layout/SearchContainer"
import SearchBar from "./SearchBar"
import SideBar from "../Layout/Sidebar"
import Content from "../Layout/Content"
import FilterLabel from "../Layout/FilterLabel"
import Result from "./Result"
import Filters from "./Filters"
import Loader from "../../../GlobalComponents/Loader"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { docParserEntitlement } from "../utils/entitlement-query"

class DocSearch extends React.Component {
  state = {
    isFetching: true,
    defaultQuery: null,
    isFetchingUrls: true,
    urls: null
  }

  componentDidMount() {
    this.fetchEntitlements()
    this.fetchUrls()
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  fetchUrls = async () => {
    try {
      const {
        auth: {
          loginDetails: { relatedFaEntities }
        }
      } = this.props

      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}common/geturlsfortenant/${
          relatedFaEntities[0].entityId
        }`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { urlObject } = res.data

      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false,
          urls: urlObject
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false
        })
      }
      console.error(err)
    }
  }

  fetchEntitlements = async () => {
    try {
      const {
        auth: {
          userEntities: { relationshipToTenant }
        }
      } = this.props
      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}entitlements/nav`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { data } = res.data
      const defaultQuery = docParserEntitlement(
        relationshipToTenant,
        data.all || []
      )

      if (!this.isCancelled) {
        this.setState({
          isFetching: false,
          defaultQuery
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetching: false
        })
      }
      console.error(err)
    }
  }

  render() {
    const { auth } = this.props
    const { isFetching, defaultQuery, isFetchingUrls, urls } = this.state

    return (
      <>
        {isFetching || isFetchingUrls ? (
          <Loader />
        ) : (
          <ReactiveBase
            url={ELASTIC_SEARCH_URL}
            app={`tenant_docs_${
              auth.loginDetails.relatedFaEntities[0].entityId
            }`}
            headers={{
              Authorization: localStorage.getItem("token")
            }}
          >
            <ReactiveComponent
              componentId="docEntitlement"
              key="docEntitlement"
              customQuery={() => defaultQuery}
            />
            <Container>
              <SearchContainer>
                <SearchBar />
              </SearchContainer>
              <Layout>
                <SideBar>
                  <FilterLabel>
                    <b>Filters</b>
                  </FilterLabel>
                  <Filters />
                </SideBar>
                <Content>
                  <Result urls={urls} />
                </Content>
              </Layout>
            </Container>
          </ReactiveBase>
        )}
      </>
    )
  }
}

export default DocSearch
