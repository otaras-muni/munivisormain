import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const Category = () => (
  <StyledList
    componentId="User"
    dataField="meta.createdBy.keyword"
    filterLabel="Uploaded By"
    title="Uploaded By"
    showCount={false}
    renderNoResults={() => <div>Please upload docs to get user filter</div>}
    loader="Fetching users ..."
    react={{
      and: ["docEntitlement"]
    }}
  />
)

export default Category
