import React from "react"

import contextMapper from "../../utils/context-mapper"
import StyledList from "../../Layout/StyledMultiList"

const ContextType = () => (
  <StyledList
    componentId="ContextType"
    dataField="meta.contextType.keyword"
    filterLabel="Context"
    title="Context"
    showCount={false}
    renderNoResults={() => <div>Please upload docs to get context filter</div>}
    loader="Fetching document contexts ..."
    renderItem={label => (
      <span>{(contextMapper[label] || {}).title || label}</span>
    )}
  />
)

export default ContextType
