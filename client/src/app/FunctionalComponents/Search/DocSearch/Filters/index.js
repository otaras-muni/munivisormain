import React from "react"

import CreateDateRangeFilter from "./CreateDateFilter"
import UpdateDateFilter from "./UpdateDateFilter"
import ContextTypeFilter from "./ContextFilter"
import CategoryFilter from "./CategoryFilter"
import UserFilter from "./UserFilter"
import DocumentTypeFilter from "./DocumentTypeFilter"
import CreateDateRange from "./CreateDateRange"
import UpdateDateRange from "./UpdateDateRange"
import ClientFilter from "./ClientFilter"
import ActivityFilter from "./ActivityFilter"

export default () => (
  <div>
    <ActivityFilter />
    <br />
    <ClientFilter />
    <br />
    <CategoryFilter />
    <br />
    <DocumentTypeFilter />
    <br />
    <CreateDateRangeFilter />
    <CreateDateRange />
    <br />
    <UpdateDateFilter />
    <UpdateDateRange />
    <br />
    <UserFilter />
    <br />
    <ContextTypeFilter />
  </div>
)
