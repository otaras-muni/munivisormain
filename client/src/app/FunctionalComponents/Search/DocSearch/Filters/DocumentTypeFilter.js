import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const Type = () => (
  <StyledList
    componentId="Type"
    dataField="meta.type.keyword"
    filterLabel="Type"
    title="Type"
    showCount={false}
    react={{
      and: ["docEntitlement"]
    }}
    renderNoResults={() => <div>Please upload docs to get type filter</div>}
    loader="Fetching types ..."
  />
)

export default Type
