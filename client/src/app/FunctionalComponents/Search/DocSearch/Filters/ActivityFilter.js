import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const Activity = () => (
  <StyledList
    componentId="ActivityFilter"
    dataField="details.activityDescription.keyword"
    filterLabel="Activity"
    title="Activity"
    showCount={false}
    react={{
      and: ["docEntitlement"]
    }}
    renderNoResults={() => <div>Please upload docs to get Activity filter</div>}
    loader="Fetching document categories ..."
  />
)

export default Activity
