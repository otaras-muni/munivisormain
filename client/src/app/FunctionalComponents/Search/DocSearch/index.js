import { connect } from "react-redux"
import DocSearch from "./DocSearch"

import { getURLsForTenant } from "../../../StateManagement/actions/CreateTransaction"

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  getURLsForTenant: tenantID => getURLsForTenant(dispatch, tenantID)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocSearch)
