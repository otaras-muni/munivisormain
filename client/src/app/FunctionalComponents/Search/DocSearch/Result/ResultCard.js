/* eslint-disable react/no-danger, no-underscore-dangle */
import React from "react"
import styled from "styled-components"
import dateFormat from "dateformat"
import _ from "lodash"
import { Link } from "react-router-dom"
import ReactTooltip from "react-tooltip"

import Flex from "../../Layout/Flex"
import CustomLink from "../../Layout/CustomLink"
import SmallLink from "../../Layout/SmallLink"
import DownloadButton from "./DownloadButton"

import { getFileIcon, getDocUrl } from "../searchConfig"
import hightlightPhrase from "../../utils/phrase-highlighter"
import media from "../../utils/media"

const Card = styled.div`
  background: #fff;
  margin: 20px 0;
  border-radius: 3px;
  align-items: center;
  border: 1px solid #e1e4e8;
`

const InfoContainer = styled(Flex)`
  font-size: small;
  margin-top: 5px;
`

const InfoItem = styled(Flex)`
  padding-left: 10px;
  border-left: ${props => (props.noborder ? 0 : "1px solid #e1e4e8")};
  font-size: small;
  width: 32%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  > div {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`

const Content = styled.div`
  padding: 10px;
  border-top: 1px solid #e1e4e8;
  font-size: small;
  padding-bottom: 30px;
`

const Icon = styled.i`
  display: none;
  ${media.desktop`display: block;`};
`

const defaultUrl = "/dashboard"

const ResultCard = ({ data, urls }) => (
  <Card>
    <Flex
      style={{
        padding: "10px 10px 15px",
        width: "100%"
      }}
      wrap="nowrap"
    >
      <Icon
        className={`far ${getFileIcon(data.meta.originalName)}`}
        style={{
          fontSize: 24,
          marginTop: 7,
          width: "3%"
        }}
      />
      <Flex
        flexDirection="column"
        style={{
          width: "85%"
        }}
      >
        <Flex alignItems="center">
          <Link
            to={
              getDocUrl(data.meta.contextType, data.meta.contextId) ||
              defaultUrl
            }
          >
            <CustomLink
              style={{
                fontWeight: "bold",
                marginLeft: 10
              }}
              dangerouslySetInnerHTML={{
                __html:
                  data["meta.originalName.search"] ||
                  _.get(data, "meta.originalName")
              }}
            />
          </Link>
          {data.meta.uploadedBy && (
            <>
              {" "}
              <span
                style={{
                  marginLeft: 10
                }}
              >
                -{"  "}
              </span>{" "}
              <Link
                to={urls.defaultUrls[data.meta.uploadedByUserId] || defaultUrl}
              >
                {" "}
                <SmallLink
                  style={{
                    fontSize: "small"
                  }}
                  dangerouslySetInnerHTML={{
                    __html:
                      data["meta.uploadedBy.search"] ||
                      _.get(data, "meta.uploadedBy")
                  }}
                />{" "}
              </Link>
            </>
          )}
        </Flex>
        <InfoContainer alignItems="center">
          {data.details.entityName && (
            <InfoItem noborder>
              <b>Entity: &nbsp;</b>
              <Link to={urls.defaultUrls[data.details._id] || defaultUrl}>
                <SmallLink>
                  <div
                    data-tip
                    data-for={`entityName${data._id}`}
                    dangerouslySetInnerHTML={{
                      __html: hightlightPhrase(
                        data["details.entityName.search"] ||
                          _.get(data, "details.entityName", "-")
                      )
                    }}
                  />
                  <ReactTooltip id={`entityName${data._id}`}>
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          data["details.entityName.search"] ||
                          _.get(data, "details.entityName", "-")
                      }}
                    />
                  </ReactTooltip>
                </SmallLink>
              </Link>
            </InfoItem>
          )}
          {data.details.tranFirmName && (
            <InfoItem noborder>
              <b>Firm: &nbsp;</b>
              <Link
                to={urls.defaultUrls[data.details.firmEntityId] || defaultUrl}
              >
                <SmallLink>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: hightlightPhrase(
                        data["details.tranFirmName.search"] ||
                          _.get(data, "details.tranFirmName", "-")
                      )
                    }}
                    data-tip
                    data-for={`tranFirmName${data._id}`}
                  />
                  <ReactTooltip id={`tranFirmName${data._id}`}>
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          data["details.tranFirmName.search"] ||
                          _.get(data, "details.tranFirmName", "-")
                      }}
                    />
                  </ReactTooltip>
                </SmallLink>
              </Link>
            </InfoItem>
          )}
          {data.details.tranClientName && (
            <InfoItem>
              <b>Client: &nbsp;</b>
              <Link
                to={urls.defaultUrls[data.details.clientEntityId] || defaultUrl}
              >
                <SmallLink>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: hightlightPhrase(
                        data["details.tranClientName.search"] ||
                          _.get(data, "details.tranClientName")
                      )
                    }}
                    data-tip
                    data-for={`tranClientName${data._id}`}
                  />
                  <ReactTooltip id={`tranClientName${data._id}`}>
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          data["details.tranClientName.search"] ||
                          _.get(data, "details.tranClientName")
                      }}
                    />
                  </ReactTooltip>
                </SmallLink>
              </Link>
            </InfoItem>
          )}
          {data.details.activityDescription && (
            <InfoItem>
              <b>Activity: &nbsp;</b>
              <Link to={urls.defaultUrls[data.details._id] || defaultUrl}>
                <SmallLink>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: hightlightPhrase(
                        data["details.activityDescription.search"] ||
                          _.get(data, "details.activityDescription", "-")
                      )
                    }}
                    data-tip
                    data-for={`activityDescription${data._id}`}
                  />
                  <ReactTooltip id={`activityDescription${data._id}`}>
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          data["details.activityDescription.search"] ||
                          _.get(data, "details.activityDescription", "-")
                      }}
                    />
                  </ReactTooltip>
                </SmallLink>
              </Link>
            </InfoItem>
          )}
        </InfoContainer>
        <InfoContainer alignItems="center">
          {data.meta.type && (
            <InfoItem noborder>
              <b>Type: &nbsp;</b>
              <div
                dangerouslySetInnerHTML={{
                  __html: hightlightPhrase(
                    data["meta.type.search"] || _.get(data, "meta.type", "-")
                  )
                }}
                data-tip
                data-for={`type${data._id}`}
              />
              <ReactTooltip id={`type${data._id}`}>
                <span
                  dangerouslySetInnerHTML={{
                    __html:
                      data["meta.type.search"] || _.get(data, "meta.type", "-")
                  }}
                />
              </ReactTooltip>
            </InfoItem>
          )}
          {data.meta.category && (
            <InfoItem>
              <b>Category: &nbsp;</b>
              <div
                dangerouslySetInnerHTML={{
                  __html: hightlightPhrase(
                    data["meta.category.search"] ||
                      _.get(data, "meta.category", "-")
                  )
                }}
                data-tip
                data-for={`category${data._id}`}
              />
              <ReactTooltip id={`category${data._id}`}>
                <span
                  dangerouslySetInnerHTML={{
                    __html:
                      data["meta.category.search"] ||
                      _.get(data, "meta.category", "-")
                  }}
                />
              </ReactTooltip>
            </InfoItem>
          )}
          {data.meta.subCategory && (
            <InfoItem>
              <b>Sub Category: &nbsp;</b>
              <div
                dangerouslySetInnerHTML={{
                  __html: hightlightPhrase(
                    data["meta.subCategory.search"] ||
                      _.get(data, "meta.subCategory", "-")
                  )
                }}
                data-tip
                data-for={`subCategory${data._id}`}
              />
              <ReactTooltip id={`subCategory${data._id}`}>
                <span
                  dangerouslySetInnerHTML={{
                    __html:
                      data["meta.subCategory.search"] ||
                      _.get(data, "meta.subCategory", "-")
                  }}
                />
              </ReactTooltip>
            </InfoItem>
          )}
        </InfoContainer>
        <InfoContainer alignItems="center">
          {data.details.tranPrimarySector && (
            <InfoItem noborder>
              <b>Primary Sector: &nbsp;</b>
              <div
                dangerouslySetInnerHTML={{
                  __html: hightlightPhrase(
                    data["details.tranPrimarySector.search"] ||
                      _.get(data, "details.tranPrimarySector")
                  )
                }}
                data-tip
                data-for={`tranPrimarySector${data._id}`}
              />
              <ReactTooltip id={`tranPrimarySector${data._id}`}>
                <span
                  dangerouslySetInnerHTML={{
                    __html:
                      data["details.tranPrimarySector.search"] ||
                      _.get(data, "details.tranPrimarySector")
                  }}
                />
              </ReactTooltip>
            </InfoItem>
          )}
          {data.details.tranSecondarySector && (
            <InfoItem>
              <b>Secondary Sector: &nbsp;</b>
              <div
                dangerouslySetInnerHTML={{
                  __html: hightlightPhrase(
                    data["details.tranSecondarySector.search"] ||
                      _.get(data, "details.tranSecondarySector")
                  )
                }}
                data-tip
                data-for={`tranSecondarySector${data._id}`}
              />
              <ReactTooltip id={`tranSecondarySector${data._id}`}>
                <span
                  dangerouslySetInnerHTML={{
                    __html:
                      data["details.tranSecondarySector.search"] ||
                      _.get(data, "details.tranSecondarySector")
                  }}
                />
              </ReactTooltip>
            </InfoItem>
          )}
        </InfoContainer>
      </Flex>
      <div
        style={{
          fontSize: "small",
          textAlign: "right",
          width: "10%"
        }}
      >
        {dateFormat(data.updatedAt, "yyyy-mm-dd")}
      </div>
    </Flex>
    <Content>
      <p
        dangerouslySetInnerHTML={{
          __html:
            data["doc_content.search"] ||
            data.doc_content ||
            `${data.doc_preview}...`
        }}
        style={{
          overflowX: "hidden",
          textOverflow: "ellipsis"
        }}
      />
    </Content>
    <Flex
      justifyContent="flex-end"
      style={{
        padding: "10px",
        borderTop: "1px solid #e1e4e8"
      }}
    >
      <DownloadButton docId={data.mid} />
    </Flex>
  </Card>
)

export default ResultCard
