import React from "react"
import { connect } from "react-redux"
import axios from "axios"
import FileSaver from "file-saver"

import { muniApiBaseURL } from "../../../../../globalutilities/consts"

const mapStateToProps = state => ({
  auth: state.auth,
  allurls: state.urls.allurls
})
class FileDownloadButton extends React.Component {
  state = {
    isDownloading: false
  }

  handleDowload = async () => {
    const { docId } = this.props
    this.setState({
      isDownloading: true
    })
    try {
      const res = await axios({
        method: "POST",
        url: `${muniApiBaseURL}s3/get-s3-download-urls`,
        headers: { Authorization: localStorage.getItem("token") },
        data: {
          docIds: [docId]
        }
      })

      const data = await res.data
      this.setState({
        isDownloading: false
      })
      if (data[0]) {
        FileSaver.saveAs(data[0])
      }
    } catch (err) {
      this.setState({
        isDownloading: false
      })
    }
  }

  render() {
    const { isDownloading } = this.state
    return (
      <button
        type="button"
        onClick={this.handleDowload}
        disabled={isDownloading}
        className="button is-link is-small "
      >
        {isDownloading ? "Downloading..." : "Download"}
      </button>
    )
  }
}

const DownloadButton = connect(mapStateToProps)(FileDownloadButton)
export default DownloadButton
