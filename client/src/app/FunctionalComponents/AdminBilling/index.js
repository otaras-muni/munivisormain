import React, { Component } from "react"
import moment from "moment"
import ReactTable from "react-table"
import { getBillingInvoicesList } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/Loader"
import RatingSection from "../../GlobalComponents/RatingSection"
import Accordion from "../../GlobalComponents/Accordion"
// eslint-disable-next-line import/no-named-as-default
import InvoicePreviewModal from "./components/InvoicePreviewModal"
// eslint-disable-next-line import/no-named-as-default
import MakePaymentModal from "./components/MakePaymentModal"
import DocLink from "../../FunctionalComponents/docs/DocLink"


class AdminBilling extends Component {

  constructor(props) {
    super(props)
    this.state = {
      invoiceList: [],
      invoiceDetails: {},
      previewModal: false,
      paymentModal: false,
      loading: true
    }
  }

  async componentDidMount() {
    const { loginDetails } = this.props
    const entityId = (loginDetails && loginDetails.userEntities && loginDetails.userEntities[0] && loginDetails.userEntities[0].entityId) || ""
    const response = await getBillingInvoicesList(entityId)
    this.setState({
      invoiceList: response && response.invoiceList || [],
      loading: false
    })
  }

  onModalChange = (state) => {
    this.setState({
      ...state,
      invoiceDetails: {}
    })
  }

  onPreview = (type, item) => {
    this.setState({ [type]: true, invoiceDetails: item })
  }

  onMakePaymentSuccess = async () => {
    const { loginDetails } = this.props
    const entityId = (loginDetails && loginDetails.userEntities && loginDetails.userEntities[0] && loginDetails.userEntities[0].entityId) || ""
    const response = await getBillingInvoicesList(entityId)
    this.setState({
      invoiceList: response && response.invoiceList || [],
      paymentModal: false,
      previewModal: true
    }, () => {
      setTimeout(() => {
        this.setState({
          previewModal: false
        })
      }, 5000)
    })
  }

  render() {
    const { invoiceList, previewModal, invoiceDetails, paymentModal } = this.state

    const loading = <Loader/>
    if(this.state.loading){
      return loading
    }
    return (
      <div id="main">
        <InvoicePreviewModal previewModal={previewModal} onModalChange={this.onModalChange} invoice={invoiceDetails || {}}/>
        <MakePaymentModal paymentModal={paymentModal} onModalChange={this.onModalChange} invoice={invoiceDetails || {}} onMakePaymentSuccess={this.onMakePaymentSuccess}/>
        <Accordion
          multiple activeItem={[0]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Invoice"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
                  <ReactTable
                    minRows={2}
                    defaultPageSize={5}
                    className="-striped -highlight is-bordered"
                    data={invoiceList}
                    columns={[
                      {
                        id: "invoiceId",
                        Header: "Invoice ID",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.invoiceId )|| ""}
                            </div>
                          )
                        }
                      },
                      {
                        id: "approvedAt",
                        Header: "Created At",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.meta && item.meta.approvedAt ? moment(item.meta.approvedAt).format("MM.DD.YYYY") : "" )}
                            </div>
                          )
                        }
                      },
                      {
                        id: "series50User",
                        Header: "Number of series 50 users",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.series50 && `${Number(item.series50).toLocaleString()}`) || "0"}
                            </div>
                          )
                        }
                      },
                      {
                        id: "nonSeries50User",
                        Header: "Number of non series 50 users",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.nonSeries50 && `${Number(item.nonSeries50).toLocaleString()}`) || "0"}
                            </div>
                          )
                        }

                      },
                      {
                        id: "totalCost",
                        Header: "Total Cost",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.totalCost && `$${Number(item.totalCost).toLocaleString()}`) || "0"}
                            </div>
                          )
                        }
                      },
                      {
                        id: "payableAmount",
                        Header: "Payable Amount",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.payableAmount && `$${Number(item.payableAmount).toLocaleString()}`) || "0"}
                            </div>
                          )
                        }

                      },
                      {
                        id: "typeOfBilling",
                        Header: "Type Of Billing",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.typeOfBilling && item.typeOfBilling.toString()) || "-"}
                            </div>
                          )
                        }
                      },
                      {
                        id: "billFrequency",
                        Header: "Bill Frequency",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.billFrequency && item.billFrequency.toString()) || "-"}
                            </div>
                          )
                        }

                      },
                      {
                        id: "status",
                        Header: "Status",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text">
                              {(item && item.status && item.status.toString()) || "-"}
                            </div>
                          )
                        }
                      },
                      {
                        id: "action",
                        Header: "Action",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text" >
                              { item && item.invoiceDocId ? <DocLink className="text-center d-inline-block" plateForm downloadIcon docId={item.invoiceDocId}/> : null}
                              {/* <a onClick={() => this.onPreview("previewModal", item)} className="fas fa-eye" title="Preview"/> */}
                            </div>
                          )
                        }
                      },
                      {
                        id: "makePayment",
                        Header: "Make Payment",
                        className: "multiExpTblVal",
                        accessor: item => item,
                        Cell: row => {
                          const item = row.value
                          return (
                            <div className="hpTablesTd wrap-cell-text" >
                              {item && item.status === "Approved" ?
                                <a
                                  className="button is-fullwidth multiExpTblVal button is-success"
                                  onClick={() => this.onPreview("paymentModal", item)}
                                  title="Make Payment"
                                >Pay {(item && item.payableAmount && `$${Number(item.payableAmount).toLocaleString()}`) || "0"}</a> :
                                <a className="button is-fullwidth multiExpTblVal" href={item.receipt_url} target="_blank">Get Receipt</a>
                              }
                            </div>
                          )
                        }
                      }
                    ]}
                  />
                }
              </RatingSection>
            </div>
          }
        />
      </div>
    )
  }
}

export default AdminBilling
