import Joi from "joi-browser"

const transDetailSchema = isBondIssue =>
  Joi.object().keys({
    actTranType: Joi.string()
      .required()
      .optional(),
    actTranSubType: Joi.string()
      .required()
      .optional(),
    actTranOtherSubtype: Joi.string()
      .allow("")
      .optional(),
    actTranUniqIdentifier: Joi.string()
      .required()
      .optional(), // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
    actTranFirmId: Joi.string()
      .required()
      .optional(), // Entity ID
    actTranFirmName: Joi.string()
      .required()
      .optional(),
    actTranClientId: Joi.string()
      .required()
      .optional(), // Entity ID
    actTranClientName: Joi.string()
      .required()
      .optional(), // Name of the Issuer Client
    actTranIsConduitBorrowerFlag: Joi.string().optional(),
    actTranPrimarySector: Joi.string()
      .allow("")
      .optional(),
    actTranSecondarySector: Joi.string()
      .allow("")
      .optional(),
    actTranFirmLeadAdvisorId: Joi.string().required(), // User Id
    actTranFirmLeadAdvisorName:
      isBondIssue === false
        ? Joi.string()
            .required()
            .optional()
        : Joi.string()
            .allow("")
            .optional(), // First Name and Last Name
    actTranIssueName: Joi.string()
      .allow("")
      .optional(),
    actTranProjectDescription: Joi.string()
      .required()
      .optional(),
    actTranRelatedTo: Joi.array().optional(),
    rfpTranPurposeOfRequest: Joi.string()
      .allow("")
      .optional(),
    actTranNotes: Joi.string()
      .allow("")
      .optional(),
    actTranRelatedType: Joi.string()
      .allow("")
      .optional(),
    opportunity: Joi.boolean().optional()
  })

export const CreateTransValidate = (inputTransDetail, isBondIssue) =>
  Joi.validate(inputTransDetail, transDetailSchema(isBondIssue), {
    abortEarly: false,
    stripUnknown: false
  })

const maRfpSchema = actOtherSubtypeRequired =>
  Joi.object().keys({
    actType: Joi.string()
      .required()
      .optional(),
    actSubType: Joi.string()
      .required()
      .optional(),
    actOtherSubtype:
      actOtherSubtypeRequired === "Other (please specify)"
        ? Joi.string()
            .required()
            .optional()
        : Joi.string()
            .allow("")
            .optional(),
    actTranFirmId: Joi.string().required(),
    actTranFirmName: Joi.string().required(),
    actStatus: Joi.string()
      .allow("")
      .required()
      .optional(),
    actUniqIdentifier: Joi.string().required(),
    actLeadFinAdvClientEntityId: Joi.string()
      .required()
      .optional(),
    actLeadFinAdvClientEntityName: Joi.string()
      .required()
      .optional(),
    actIssuerClient: Joi.string()
      .required()
      .optional(),
    actIssuerClientEntityName: Joi.string()
      .required()
      .optional(),
    actIssueName: Joi.string()
      .allow("")
      .required()
      .optional(),
    actProjectName: Joi.string()
      .required()
      .optional(),
    actPrimarySector: Joi.string()
      .allow("")
      .optional(),
    actSecondarySector: Joi.string()
      .allow("")
      .optional(),
    actNotes: Joi.string()
      .allow("")
      .required()
      .optional(),
    actTranNotes: Joi.string()
      .allow("")
      .optional(),
    actRelTrans: Joi.array().optional(),
    opportunity: Joi.boolean().optional()
  })

export const CreateMARFPValidate = (
  inputTransDetail,
  actOtherSubtypeRequired
) =>
  Joi.validate(inputTransDetail, maRfpSchema(actOtherSubtypeRequired), {
    abortEarly: false,
    stripUnknown: false
  })

const rfpDeslsDetailSchema = Joi.object().keys({
  actTranType: Joi.string()
    .required()
    .optional(),
  actTranSubType: Joi.string()
    .required()
    .optional(),
  actTranOtherSubtype: Joi.string()
    .allow("")
    .optional(),
  actTranUniqIdentifier: Joi.string()
    .required()
    .optional(), // Generate Using UUID with appropriate key - RFP, DEAL, BANKLOAN etc
  actTranFirmId: Joi.string()
    .required()
    .optional(), // Entity ID
  actTranFirmName: Joi.string()
    .required()
    .optional(),
  actTranClientId: Joi.string()
    .required()
    .optional(), // Entity ID
  actTranClientName: Joi.string()
    .required()
    .optional(), // Name of the Issuer Client
  actTranIsConduitBorrowerFlag: Joi.string().optional(),
  actTranPrimarySector: Joi.string()
    .allow("")
    .optional(),
  actTranSecondarySector: Joi.string()
    .allow("")
    .optional(),
  actTranFirmLeadAdvisorId: Joi.string().required(), // User Id
  actTranFirmLeadAdvisorName: Joi.string()
    .allow("")
    .optional(), // First Name and Last Name
  actTranIssueName: Joi.string()
    .allow("")
    .optional(),
  actTranProjectDescription: Joi.string()
    .required()
    .optional(),
  actTranRelatedTo: Joi.array().optional(),
  rfpTranPurposeOfRequest: Joi.string().optional(),
  actTranNotes: Joi.string()
    .allow("")
    .optional(),
  actTranRelatedType: Joi.string()
    .allow("")
    .optional(),
  opportunity: Joi.boolean().optional()
})

export const CreateTransRFPDealsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, rfpDeslsDetailSchema, {
    abortEarly: false,
    stripUnknown: false
  })
