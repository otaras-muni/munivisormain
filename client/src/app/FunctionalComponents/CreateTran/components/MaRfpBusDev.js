import React from "react"
import {
  TextLabelInput,
  SelectLabelInput,
  MultiSelect
} from "../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../GlobalComponents/DropDownListClear"
import ReletedTranGlob from "../../../GlobalComponents/ReletedTranGlob";

const MaRfpBusDev = ({
  item,
  errorMessages,
  advisor,
  issuerList,
  dropDown,
  onTranSave,
  allTrans,
  transactions,
  onChangeItem,
  category,
  onRelatedTranSelect,
  related,
  deleteRelatedTran,
  onRelatedtranCancel,
  onTranChange,
  onSearch,
  relatedTransactions
}) => {
  const onChange = event => {
    if (event.target.type === "radio") {
      const isClientBorrower =
        event.target.attributes.id.value === "isClientBorrower"
      const isClientConduit =
        event.target.attributes.id.value === "isClientConduit"
      onChangeItem(
        {
          ...item,
          isClientBorrower,
          isClientConduit
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...item,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        },
        category
      )
    }
  }

  const onIssuerChange = (key, selectItem) => {
    if (key === "actTranFirmLeadAdvisorId") {
      onChangeItem(
        {
          ...item,
          actLeadFinAdvClientEntityId: selectItem.id,
          actLeadFinAdvClientEntityName: selectItem.name
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...item,
          actIssuerClient: selectItem.id,
          actIssuerClientEntityName: selectItem.firmName,
          actPrimarySector: selectItem.primarySector,
          actSecondarySector: selectItem.secondarySector
        },
        category
      )
    }
  }

  transactions = transactions.map(tran => ({
    ...tran,
    group: tran.actTranType
      ? tran.actTranType
      : tran.actType
        ? tran.actType
        : tran.rfpTranType === "RFP"
          ? tran.rfpTranType
          : tran.dealIssueTranSubType === "Bond Issue"
            ? "Deal"
            : ""
  }))

  if (
    allTrans &&
    Object.keys(allTrans).length &&
    item.actRelTrans &&
    item.actRelTrans.length
  ) {
    item.actRelTrans.forEach(relatedTran => {
      let finRelatedTran = {}
      Object.keys(allTrans).forEach(key => {
        if (!finRelatedTran || !Object.keys(finRelatedTran).length) {
          finRelatedTran = allTrans[key].find(
            tran => tran.activityId === relatedTran
          )
        }
      })
      if (finRelatedTran && Object.keys(finRelatedTran).length) {
        transactions.push(finRelatedTran)
      }
    })
  }

  return (
    <div>
      {category === "businessDevelopment" ? (
        <div className="columns">
          <SelectLabelInput
            required
            label="Activity Subtype"
            error={errorMessages.actSubType || ""}
            list={dropDown.actTranSubType || []}
            name="actSubType"
            value={item.actSubType}
            onChange={onChange}
          />
          {item.actSubType === "Other (please specify)" && (
            <TextLabelInput
              label="Other Activity Subtype"
              error={errorMessages.actOtherSubtype || ""}
              name="actOtherSubtype"
              value={item.actOtherSubtype}
              onChange={onChange}
            />
          )}
        </div>
      ) : null}
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl ">Client Name
            <span className='icon has-text-danger'>
              <i className='fas fa-asterisk extra-small-icon'/>
            </span>
          </p>

          <div className="control">
            <div style={{ fontSize: 12 }}>
              <DropDownListClear
                filter="contains"
                value={item.actIssuerClientEntityName || []}
                data={issuerList || []}
                message="select issuer"
                textField="name"
                groupBy={({ relType }) => relType}
                valueField="id"
                key="name"
                isHideButton={item.actIssuerClientEntityName}
                onSearch={onSearch}
                onChange={issuer => onIssuerChange("actIssuerClient", issuer)}
                onClear={() => onIssuerChange("actIssuerClient", {})}
              />
              {errorMessages && errorMessages.actIssuerClient && (
                <small className="text-error">
                  {errorMessages.actIssuerClient}
                </small>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Primary Sector</p>
          <div className="control">
            <div className="select is-small is-link is-fullwidth">
              <select
                value={item.actPrimarySector}
                onChange={onChange}
                name="actPrimarySector"
              >
                <option value="" disabled="">
                  Pick Primary Sector
                </option>
                {dropDown.primarySector &&
                  Object.keys(dropDown.primarySector).map(key => (
                    <option key={key} value={key}>
                      {key}
                    </option>
                  ))}
              </select>
              {/* {errorMessages && errorMessages.actPrimarySector &&
              <small className="text-error">{errorMessages.actPrimarySector}</small>} */}
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">Secondary Sector</p>
          <div className="select is-small is-link is-fullwidth">
            <select
              name="actSecondarySector"
              value={item.actSecondarySector}
              onChange={onChange}
            >
              <option value="" disabled="">
                Pick Secondary Sector
              </option>
              {item.actPrimarySector &&
              dropDown.primarySector[item.actPrimarySector]
                ? dropDown.primarySector[item.actPrimarySector].map(sector => (
                  <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                    {sector && sector.label}
                  </option>
                ))
                : null}
            </select>
            {/* {errorMessages && errorMessages.actSecondarySector &&
            <small className="text-error">{errorMessages.actSecondarySector}</small>} */}
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl ">Lead Advisor
            <span className='icon has-text-danger'>
              <i className='fas fa-asterisk extra-small-icon'/>
            </span>
          </p>
          <div className="control">
            <div style={{ fontSize: 12 }}>
              <DropDownListClear
                filter
                value={item.actLeadFinAdvClientEntityName || []}
                data={advisor || []}
                message="Search"
                textField="name"
                valueField="id"
                key="name"
                isHideButton={item.actLeadFinAdvClientEntityName}
                onChange={user => onIssuerChange("actTranFirmLeadAdvisorId", user)}
                onClear={() => onIssuerChange("actTranFirmLeadAdvisorId", {})}
              />
              {errorMessages && errorMessages.actLeadFinAdvClientEntityId && (
                <small className="text-error">
                  {errorMessages.actLeadFinAdvClientEntityId}
                </small>
              )}
            </div>
          </div>
        </div>
      </div>
      {category === "marfp" ? (
        <div>
          <div className="columns">
            <TextLabelInput
              label="Activity (or RFP) Name"
              placeholder="Enter Activity name"
              error={errorMessages.actIssueName || ""}
              name="actIssueName"
              value={item.actIssueName}
              onChange={onChange}
            />
          </div>
          <div className="columns">
            <TextLabelInput
              required
              label="Project Description"
              placeholder="Assignor notes to assignee"
              error={errorMessages.actProjectName || ""}
              name="actProjectName"
              value={item.actProjectName}
              onChange={onChange}
            />
          </div>
        </div>
      ) : null}
      {category === "businessDevelopment" ? (
        <div>
          <div className="columns">
            <TextLabelInput
              label="Issue Name"
              placeholder="Enter Issue Name"
              error={errorMessages.actIssueName || ""}
              name="actIssueName"
              value={(item && item.actIssueName) || ""}
              onChange={onChange}
            />
          </div>
          <div className="columns">
            <TextLabelInput
              required
              label="Project Description"
              placeholder="Enter Project Description"
              error={errorMessages.actProjectName || ""}
              name="actProjectName"
              value={item.actProjectName || ""}
              onChange={onChange}
            />
          </div>
        </div>
      ) : null}
      <hr />

      <div>
        <p className="multiExpLbl">Related Transactions</p>
        <ReletedTranGlob
          relatedTypes={dropDown.relatedTypes}
          transaction={{ actTranRelatedTo: relatedTransactions }}
          errorMessages={errorMessages}
          item={related}
          canEditTran
          onRelatedTranSelect={onRelatedTranSelect}
          onChange={onTranChange}
          onRelatedtranSave={onTranSave}
          onRelatedtranCancel={onRelatedtranCancel}
          relatedTransactions={relatedTransactions}
          deleteRelatedTran={deleteRelatedTran}
        />
      </div>

      <div className="columns">
        <div className="column is-full">
          <p className="multiExpLbl">Notes / Instructions</p>
          <div className="control">
            <textarea
              className="textarea is-link"
              onChange={onChange}
              value={item && item.actNotes}
              name="actNotes"
              placeholder="Assignor notes to assignee"
            />
            {errorMessages && errorMessages.actNotes && (
              <small className="text-error">{errorMessages.actNotes}</small>
            )}
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column is-full">
          <p className="multiExpLbl">Opportunity</p>
          <div className="control">
            <label className="switch" style={{ marginTop: 5 }}>
              <input
                type="checkbox"
                name="opportunity"
                checked={(item && item.opportunity) || false}
                onChange={onChange}
              />
              <span className="slider round" />
            </label>
          </div>
        </div>
      </div>
    </div>
  )
}
export default MaRfpBusDev
