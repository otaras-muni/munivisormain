import React from "react"
import uuidv1 from "uuid/v1"
import {
  TextLabelInput,
  SelectLabelInput,
} from "../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../GlobalComponents/DropDownListClear"
import ReletedTranGlob from "../../../GlobalComponents/ReletedTranGlob"

const generateName = uuidv1()
  .replace(/-/g, "")
  .toUpperCase()

const BoxView = ({
  item,
  errorMessages,
  dropDown,
  advisor,
  onTranSave,
  allTrans,
  related,
  transactions,
  onChangeItem,
  category,
  onRelatedTranSelect,
  issuerList,
  deleteRelatedTran,
  onRelatedtranCancel,
  onTranChange,
  relatedTransactions,
  onSearch,
  onLeadAdvisorRefresh
}) => {
  const onChange = event => {
    if (event.target.name === "actTranSubType") {
      if (event.target.value === "Bond Issue") {
        item.rfpTranPurposeOfRequest = ""
        item.actTranUniqIdentifier = `TXDEAL${generateName.substring(0, 17)}`
      } else {
        if (event.target.value === "Bank Loan") {
          item.actTranUniqIdentifier = `TXLOAN${generateName.substring(0, 17)}`
        }
        delete item.rfpTranPurposeOfRequest
      }
    }
    onChangeItem(
      {
        ...item,
        [event.target.name]:
          event.target.type === "checkbox" || event.target.type === "radio"
            ? (event.target.attributes.id &&
                event.target.attributes.id.value) ||
              event.target.checked
            : event.target.value
      },
      category
    )
  }

  const onIssuerChange = (key, selectItem) => {
    if (key === "actTranFirmLeadAdvisorId") {
      onChangeItem(
        {
          ...item,
          actTranFirmLeadAdvisorId: selectItem.id,
          actTranFirmLeadAdvisorName: selectItem.name,
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...item,
          actTranClientId: selectItem.id,
          actTranClientName: selectItem.firmName,
          actTranPrimarySector: selectItem.primarySector,
          actTranSecondarySector: selectItem.secondarySector
        },
        category
      )
    }
  }

  transactions = transactions.map(tran => ({
    ...tran,
    group: tran.actTranType
      ? tran.actTranType
      : tran.actType
      ? tran.actType
      : tran.rfpTranType === "RFP"
      ? tran.rfpTranType
      : tran.dealIssueTranSubType === "Bond Issue"
      ? "Deal"
      : ""
  }))

  if (
    allTrans &&
    Object.keys(allTrans).length &&
    item.actTranRelatedTo &&
    item.actTranRelatedTo.length
  ) {
    item.actTranRelatedTo.forEach(relatedTran => {
      let finRelatedTran = {}
      Object.keys(allTrans).forEach(key => {
        if (!finRelatedTran || !Object.keys(finRelatedTran).length) {
          finRelatedTran = allTrans[key].find(
            tran => tran.activityId === relatedTran
          )
        }
      })
      if (finRelatedTran && Object.keys(finRelatedTran).length) {
        transactions.push(finRelatedTran)
      }
    })
  }

  return (
    <div>
      <div className="columns">
        <div>
          <SelectLabelInput
            label="Activity Subtype"
            required
            error={errorMessages.actTranSubType || ""}
            list={dropDown.actTranSubType || []}
            name="actTranSubType"
            value={item.actTranSubType}
            onChange={onChange}
          />
        </div>

        <div className="column">
          <div>
            <p className="multiExpLbl">Client Name
              <span className='icon has-text-danger'>
                    <i className='fas fa-asterisk extra-small-icon'/>
                  </span>
            </p>
            <div className="control">
              <div style={{ width: 300, fontSize: 12 }}>
                <DropDownListClear
                  filter="contains"
                  value={item.actTranClientName || []}
                  data={issuerList || []}
                  message="select issuer"
                  textField="name"
                  valueField="id"
                  key="name"
                  isHideButton={item.actTranClientName}
                  groupBy={({ relType }) => relType}
                  onSearch={onSearch}
                  onChange={issuer => onIssuerChange("actTranClientId", issuer)}
                  onClear={() => {onIssuerChange("actTranClientId", {})}}
                />
                {errorMessages && errorMessages.actTranClientId && (
                  <small className="text-error">
                    {errorMessages.actTranClientId}
                  </small>
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl">Primary Sector</p>
          <div className="control">
            <div className="select is-small is-fullwidth is-link">
              <select
                value={item.actTranPrimarySector || ""}
                onChange={onChange}
                name="actTranPrimarySector"
              >
                <option value="" disabled="">
                  Pick Primary Sector
                </option>
                {dropDown.primarySector &&
                  Object.keys(dropDown.primarySector).map(key => (
                    <option key={key} value={key}>
                      {key}
                    </option>
                  ))}
              </select>
              {/* {errorMessages && errorMessages.actTranPrimarySector &&
                  <small className="text-error">{errorMessages.actTranPrimarySector}</small>} */}
            </div>
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl">Secondary Sector</p>
          <div className="select is-small is-fullwidth is-link">
            <select
              name="actTranSecondarySector"
              value={item.actTranSecondarySector || ""}
              onChange={onChange}
            >
              <option value="" disabled="">
                Pick Secondary Sector
              </option>
              {item.actTranPrimarySector &&
              dropDown.primarySector[item.actTranPrimarySector]
                ? dropDown.primarySector[item.actTranPrimarySector].map(
                  sector => (
                    <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                      {sector && sector.label}
                    </option>
                  )
                )
                : null}
            </select>
            {/* {errorMessages && errorMessages.actTranSecondarySector &&
                <small className="text-error">{errorMessages.actTranSecondarySector}</small>} */}
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl ">
            {`${
              category === "debt" || category === "rfp" || category === "others"
                ? "Lead Advisor"
                : category === "derivative"
                ? "Swap (or Lead) Advisor"
                : ""
            }`}
            <span className='icon has-text-danger'>
              <i className='fas fa-asterisk extra-small-icon'/>
            </span>
          </p>
          <div className="control">
            <div style={{ width: "100%", fontSize: 12 }}>
              <DropDownListClear
                filter
                value={item.actTranFirmLeadAdvisorId || []}
                data={advisor || []}
                message="Search"
                textField="name"
                valueField="id"
                key="name"
                isHideButton={item.actTranFirmLeadAdvisorId}
                onChange={user => onIssuerChange("actTranFirmLeadAdvisorId", user)}
                onClear={() => {onIssuerChange("actTranFirmLeadAdvisorId", {})}}
              />
              {errorMessages && errorMessages.actTranFirmLeadAdvisorId && (
                <small className="text-error">
                  {errorMessages.actTranFirmLeadAdvisorId}
                </small>
              )}
            </div>
          </div>
          {
            advisor && advisor.length < 1 ?
              <div>
                <span className="is-link" style={{ fontSize: 12 }}>
                  <a target="_blank" href="/addnew-contact">
                    Add Advisor?
                  </a>&nbsp;&nbsp;&nbsp;&nbsp;
                  <a target="_blank" href="/mast-allcontacts">
                    Edit Advisor?
                  </a>
                </span>
                <i className="fa fa-refresh"
                  style={{
                    fontSize: 12,
                    cursor: "pointer",
                    padding: "0 0 0 10px"
                  }}
                  onClick={onLeadAdvisorRefresh}
                />
              </div> : null
          }

        </div>
      </div>

      {(category === "derivative" ||
        category === "rfp" ||
        category === "others") &&
      item.actTranSubType === "Other (please specify)" ? (
        <div className="columns">
          <TextLabelInput
            placeholder="Enter Other Activity Subtype"
            label="Other Activity Subtype"
            error={errorMessages.actTranOtherSubtype || ""}
            name="actTranOtherSubtype"
            value={item.actTranOtherSubtype || ""}
            onChange={onChange}
          />
        </div>
      ) : null}

      <div className="columns" />
      {category === "debt" ||
      category === "rfp" ||
      category === "derivative" ||
      category === "others" ? (
        <div className="columns">
          <TextLabelInput
            label="Issue Name"
            placeholder="Enter Issue Name"
            error={errorMessages.actTranIssueName || ""}
            name="actTranIssueName"
            value={item.actTranIssueName || ""}
            onChange={onChange}
          />
        </div>
      ) : null}

      <div className="columns">
        <TextLabelInput
          required
          label={`${
            category === "debt" || category === "rfp" || category === "others"
              ? "Project Description"
              : category === "derivative"
              ? "Project Description / Transaction Name"
              : ""
          }`}
          placeholder="Enter Project Description"
          error={errorMessages.actTranProjectDescription || ""}
          name="actTranProjectDescription"
          value={item.actTranProjectDescription || ""}
          onChange={onChange}
        />
      </div>
      <hr />

      <div>
        <p className="multiExpLbl">Related Transactions</p>
        <ReletedTranGlob
          relatedTypes={dropDown.relatedTypes}
          transaction={{ actTranRelatedTo: relatedTransactions }}
          errorMessages={errorMessages}
          item={related}
          canEditTran
          onRelatedTranSelect={onRelatedTranSelect}
          onChange={onTranChange}
          onRelatedtranSave={onTranSave}
          onRelatedtranCancel={onRelatedtranCancel}
          relatedTransactions={relatedTransactions}
          deleteRelatedTran={deleteRelatedTran}
        />
      </div>

      <div className="columns">
        <div className="column is-full">
          <p className="multiExpLbl">Notes / Instructions</p>
          <div className="control">
            <textarea
              className="textarea"
              onChange={onChange}
              value={item.actTranNotes || ""}
              name="actTranNotes"
              placeholder="Assignor notes to assignee"
            />
            {errorMessages && errorMessages.actTranNotes && (
              <small className="text-error">{errorMessages.actTranNotes}</small>
            )}
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column is-full">
          <p className="multiExpLbl">Opportunity</p>
          <div className="control">
            <label className="switch" style={{ marginTop: 5 }}>
              <input
                type="checkbox"
                name="opportunity"
                checked={(item && item.opportunity) || false}
                onChange={onChange}
              />
              <span className="slider round" />
            </label>
          </div>
        </div>
      </div>
    </div>
  )
}
export default BoxView
