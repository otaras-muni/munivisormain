import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import uuidv1 from "uuid/v1"
import * as qs from "query-string"
import swal from "sweetalert"
import moment from "moment"
import { getDocDetails, getPicklistByPicklistName, createCACTask } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/Loader"
import BoxView from "./components/BoxView"
import MaRfpBusDev from "./components/MaRfpBusDev"
import CONST from "../../../globalutilities/consts"
import { createTrans } from "../../StateManagement/actions/BankLoans"
import {
  fetchIssuers,
  fetchLeadAdvisor,
  createTranEmailAlert,
  checkSupervisorControls,
  getURLsForTenant,
  getUserTransactions,
} from "../../StateManagement/actions/CreateTransaction"
import {
  CreateTransValidate,
  CreateMARFPValidate,
  CreateTransRFPDealsValidate
} from "./Validation/CreateTransValidate"
import withAuditLogs from "../../GlobalComponents/withAuditLogs"
import TranCreateModal from "../../GlobalComponents/TranCreateModal"
import { getactivecontracts } from "../../StateManagement/actions/ClientProspectDashboard/actionCreators"
import { FRONTENDURL } from "GlobalUtils/consts"
import {getSupervisor} from "../../StateManagement/actions/Supervisor"

const generateName = uuidv1()
  .replace(/-/g, "")
  .toUpperCase()


class CreateTran extends React.Component {
  constructor() {
    super()
    this.state = {
      debtIssuance: {
        ...CONST.CREATE.loan
      },
      related: {
        actTranRelatedType: "",
        actTranRelatedTo: [],
      },
      relatedTransactions: [],
      advisor: [],
      transactions: [],
      errorMessages: {},
      loading: true,
      category: "",
      dropDown: {},
      title: "",
      issuerList: [],
      modalState: false,
      contractShow: false,
      pdfShow: false,
      contractEnable: "",
      activityType: "",
      checked: {},
      contract: [],
      number: 1,
      tranConfig: {
        svComplianceFlag: true,
        clientActive: false,
        defaultStatus: "",
        sendEmail: false,
        message: ""
      },
      allTrans: {},
      svControls: {},
      relatedTran: [],
      searchList: [],
      expireContract: [],
      tranType: "",
      confirmAlert: CONST.confirmAlert,
      search: ""
    }
  }

  componentWillMount() {
    const { nav2 } = this.props
    const queryString = qs.parse(location.search)
    const actTranUniqIdentifier =
      nav2 === "debt"
        ? `TXLOAN${generateName.substring(0, 17)}`
        : nav2 === "derivative"
          ? `TXDERIVATIVE${generateName.substring(0, 11)}`
          : nav2 === "rfp"
            ? `TXRFP${generateName.substring(0, 18)}`
            : nav2 === "marfp"
              ? `TXMARFP${generateName.substring(0, 16)}`
              : nav2 === "others"
                ? `TXOTHERS${generateName.substring(0, 16)}`
                : nav2 === "businessDevelopment"
                  ? `TXBUSDEV${generateName.substring(0, 16)}`
                  : ""
    fetchIssuers(
      nav2 === "businessDevelopment"
        ? `${this.props.user.entityId}?type=Prospect`
        : this.props.user.entityId,
      res => {
        let debtIssuance = {}
        let primary = {}
        if(queryString && queryString.id && res && res.issuerList && res.issuerList.length){
          primary = res && res.issuerList.filter(i => i.id === queryString.id)[0] || {}
        }
        if (nav2 === "marfp" || nav2 === "businessDevelopment") {
          debtIssuance = {
            ...CONST.CREATE.MARFP,
            actUniqIdentifier: actTranUniqIdentifier,
            actIssuerClient: (queryString && queryString.id) || "",
            actIssuerClientEntityName: (queryString && queryString.name) || "",
            actPrimarySector: (primary && primary.primarySector) || "",
            actSecondarySector: (primary && primary.secondarySector) || "",
          }
        } else {
          debtIssuance = {
            ...this.state.debtIssuance,
            actTranUniqIdentifier,
            actTranClientId: (queryString && queryString.id) || "",
            actTranClientName: (queryString && queryString.name) || "",
            actTranPrimarySector: (primary && primary.primarySector) || "",
            actTranSecondarySector: (primary && primary.secondarySector) || ""
          }
          if (nav2 === "rfp") {
            debtIssuance.rfpTranPurposeOfRequest = ""
          }
        }
        this.setState({
          issuerList: res.issuerList,
          rfpTranClientId: this.props.user.entityId,
          debtIssuance,
          title:
            nav2 === "debt"
              ? "Add - Debt Issuance"
              : nav2 === "derivative"
                ? "Add - Debt Derivative"
                : nav2 === "rfp"
                  ? "Add -Manage RFP for Client"
                  : nav2 === "marfp"
                    ? "Add - New MA RFP"
                    : nav2 === "others"
                      ? "Add - Other Activity"
                      : nav2 === "businessDevelopment"
                        ? "Add - Business Development"
                        : `Add - ${nav2}`,
          category: nav2
        })
      }
    )
  }

  async componentDidMount() {
    const { nav2 } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPPRIMARYSECTOR",
      "LKUPRELATEDTYPE",
      "LKUPPROPOSALFORTYPE",
      "LKUPSWAPTRANSTATUS",
      "LKUPPARTICIPANTTYPE",
      "LKUPTRANSACTIONSTATUS",
      "LKUPDEBTTRAN",
      "LKUPDERIVATIVETRAN",
      "LKUPOTHERSTRAN",
      "LKUPBUSDEVTRAN"
    ])
    const svControls = await checkSupervisorControls()
    const result = (picResult.length && picResult[1]) || {}
    const primarySec = (picResult[2] && picResult[2].LKUPPRIMARYSECTOR) || {}
    const advisorData = await fetchLeadAdvisor(this.props.user.entityId)
    if((result && result.LKUPSWAPTRANSTATUS && result.LKUPSWAPTRANSTATUS.length) && (nav2 && nav2 === "rfp")){
      const removedStatus = ["Executed but not closed", "Pre-execution", "Pre-Munivisor"]
      result.LKUPSWAPTRANSTATUS.forEach(r => {
        if(removedStatus.indexOf(r) !== -1){
          const index = result.LKUPSWAPTRANSTATUS.indexOf(r)
          result.LKUPSWAPTRANSTATUS.splice(index, 1)
        }
      })
    }
    this.setState(prevState => ({
      advisor: advisorData || [],
      dropDown: {
        ...this.state.dropDown,
        purposeOfRFP: result.LKUPPROPOSALFORTYPE || [],
        primarySector: primarySec,
        relatedTypes: result.LKUPRELATEDTYPE || [],
        tranStatus: result.LKUPSWAPTRANSTATUS,
        actTranSubType:
          nav2 === "debt"
            ? result.LKUPDEBTTRAN || []
            : nav2 === "derivative"
              ? result.LKUPDERIVATIVETRAN || []
              : nav2 === "businessDevelopment"
                ? result.LKUPBUSDEVTRAN || []
                : nav2 === "rfp"
                  ? result.LKUPPARTICIPANTTYPE
                  : nav2 === "others"
                    ? result.LKUPOTHERSTRAN || []
                    : []
      },
      loading: false,
      svControls,
      tranConfig: {
        ...prevState.tranConfig,
        message: !svControls.firmHasSupervisor
          ? "There is neither a Supervisory Principal or Compliance Officer designated for your firm. Please contact admin to set up one."
          : ""
      }
    }))
  }

  onRelatedTranSelect = items => {
    const { related } = this.state
    this.setState({
      related: {
        ...related,
        actTranRelatedTo: items
      }
    })
  }

  onChangeGetTransactions = event => {
    const { name, value } = event.target
    let {debtIssuance} = this.state
    debtIssuance = {
      ...debtIssuance,
      // actTranRelatedTo: []
    }
    this.setState(
      {
        [name]: value,
        transactions: [],
        tranType: value || "",
        loading: true
      },
      async () => {
        if (value) {
          const transactions = await getUserTransactions(value)

          this.setState(prevState => ({
            transactions,
            allTrans: {
              ...prevState.allTrans,
              [value]: transactions
            },
            debtIssuance,
            loading: false
          }))
        } else {
          this.setState({
            loading: false
          })
        }
      }
    )
  }

  onSave = () => {
    const { category, transactions, allTrans, tranConfig, relatedTransactions } = this.state
    const { debtIssuance } = this.state
    const { user } = this.props

    let payload = {}
    let addedTrans = []
    addedTrans = relatedTransactions.map((item) => {
      if (category === "rfp" || category === "derivative" || category === "others" || category === "debt") {
        return {
          relTranId: item.id,
          relTranIssueName: item.name,
          relTranProjectName: item.relTranProjectName,
          relTranClientId: item.relTranClientId,
          relTranClientName: item.relTranClientName,
          relType: item.relType,
          tranType: item.tranType
        }
      } if(category === "marfp" || category === "businessDevelopment") {
        return {
          relTranId: item.id,
          relTranIssueName: item.name,
          relTranProjectName: item.relTranProjectName,
          relTranClientId: item.relTranClientId,
          relTranClientName: item.relTranClientName,
          relType: item.relType,
          tranType: item.tranType
        }
      }
    })

    if (category === "marfp" || category === "businessDevelopment") {
      payload = {
        ...debtIssuance,
        actType: category,
        actTranFirmId: user.entityId,
        actTranFirmName: user.firmName,
        actSubType: category === "marfp" ? "marfp" : debtIssuance.actSubType,
        actRelTrans: addedTrans
      }
    } else {
      payload = {
        ...debtIssuance,
        actTranType: category === "rfp" ? "RFP" : category,
        actTranFirmId: user.entityId,
        actTranFirmName: user.firmName,
        actTranRelatedTo: addedTrans
      }
    }

    delete payload.actIssuerClientMsrbName
    if (
      category !== "derivative" &&
      category !== "rfp" &&
      category !== "others" &&
      category !== "marfp" &&
      category !== "businessDevelopment"
    ) {
      delete payload.actTranOtherSubtype
    } else if (category === "marfp" || category === "businessDevelopment") {
      delete payload.actTranOtherSubtype
      delete payload.actRelatedType
    }

    let errors = {}
    const projectDescription =
      payload.actProjectName ||
      payload.actTranProjectDescription ||
      ""
    const issueName = payload.actTranIssueName || payload.actIssueName || ""
    if (category === "marfp" || category === "businessDevelopment") {
      errors = CreateMARFPValidate(payload, payload.actSubType)
    } else if (category === "rfp") {
      errors = CreateTransRFPDealsValidate(payload)
    } else {
      errors = CreateTransValidate(
        payload,
        payload.actTranSubType === "Bond Issue"
      )
    }
    console.log(payload)
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      this.setState({ errorMessages })
      return
    }

    const clientId =
      category === "marfp" || category === "businessDevelopment"
        ? payload.actIssuerClient
        : payload.actTranClientId

    tranConfig.defaultStatus = payload.opportunity
      ? "In Progress"
      : "Pre-Pricing"
    this.setState(
      prevState => {
        const emailTitle = issueName || projectDescription
        return {
          tranConfig,
          errorMessages: {},
          modalState: true,
          payload,
          email: {
            ...prevState.email,
            subject: `Create Transaction - ${emailTitle}`
          },
          checked: {},
          contractEnable: "",
          payloadCategory: category
        }
      },
      () => this.onContractRefresh(clientId)
    )
  }

  onConfirmationSave = () => {
    const {
      payloadCategory,
      payload,
      tranConfig,
      advisor,
      svControls,
      checked,
      contractEnable,
      contract,
      issuerList
    } = this.state

    const { userFirstName, userLastName, userId } = this.props.user

    const otherSubType = (payload && payload.actTranOtherSubtype) || ""
    const tranSubType = (payload && payload.actTranSubType) || ""

    if (tranSubType === "Other (please specify)" && otherSubType.length > 0 ) {
      payload.actTranSubType = otherSubType
    }

    console.log("tranConfig=============", tranConfig)
    const leadId = payload.actTranFirmLeadAdvisorId || payload.actLeadFinAdvClientEntityId
    const clientId = payload.actTranClientId || payload.actIssuerClient || ""
    const leadAdv = advisor.find(adv => adv.id === leadId)
    const client = issuerList.find(c => c.id === clientId)
    let email = ""
    let phone = ""
    let fax = ""
    if (leadAdv && Array.isArray(leadAdv.userEmails) && leadAdv.userEmails.length) {
      email = leadAdv.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : leadAdv.userEmails[0].emailId
    }
    if (leadAdv && Array.isArray(leadAdv.userPhone) && leadAdv.userPhone.length) {
      phone = leadAdv.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : leadAdv.userPhone[0].phoneNumber
    }
    if (leadAdv && Array.isArray(leadAdv.userFax) && leadAdv.userFax.length) {
      fax = leadAdv.userFax.find(e => e.faxPrimary)
      fax = fax ? fax.faxNumber : leadAdv.userFax[0].faxNumber
    }
    const userAddress = leadAdv.userAddresses || {}
    const clientAddress = (client && client.addresses) || {}
    /* if (leadAdv && Array.isArray(leadAdv.userAddresses) && leadAdv.userAddresses.length) {
      userAddress = leadAdv.userAddresses.find(e => e.isPrimary)
      userAddress = !userAddress ? leadAdv.userAddresses[0] : userAddress
    } */
    const participant = {
      partType: "Municipal Advisor",
      partFirmId: leadAdv.entityId || "",
      partFirmName: leadAdv.userFirmName || "",
      partContactId: leadAdv.id || "",
      partContactName: leadAdv.name || "",
      partContactEmail: email || "",
      partContactPhone: phone || "",
      partContactFax: fax || "",
      partContactAddToDL: false,
      createdDate: new Date()
    }

    const addressType = (payloadCategory === "debt" && payload.actTranSubType === "Bond Issue") ? "dealPartAddressGoogle" : "participantGoogleAddress"

    if(userAddress && userAddress._id){
      participant.partUserAddress = (userAddress && userAddress._id) || ""

      const googleAddress = `${userAddress.addressLine1 || ""} ${userAddress.city || ""} ${userAddress.state || ""} ${userAddress.zipCode ? `${userAddress.zipCode.zip1}` : ""}` || ""
      let addr = (userAddress && userAddress.addressLine1) || ""
      /* if(userAddress.addressLine2){
        addr = addr ? `${addr}, ${userAddress.addressLine2}` : userAddress.addressLine2
      } */
      if(userAddress.state){
        addr = addr ? `${addr}, ${userAddress.state}` : userAddress.state
      }
      if(userAddress.city){
        addr = addr ? `${addr}, ${userAddress.city}` : userAddress.city
      }
      if(userAddress.zipCode && userAddress.zipCode.zip1){
        addr = addr ? `${addr}, ${userAddress.zipCode.zip1}` : userAddress.zipCode.zip1
      }
      if(userAddress.country){
        addr = addr ? `${addr}, ${userAddress.country}` : userAddress.country
      }
      participant.partContactAddrLine1 = addr || ""
      participant[addressType] = googleAddress || ""
      // participant.partContactAddrLine2= (userAddress && userAddress.addressLine2) || ""
      // participant.participantState= (userAddress && userAddress.state) || ""
    }

    if (!tranConfig.defaultStatus) return

    if (payloadCategory === "marfp" || payloadCategory === "businessDevelopment") {
      payload.actStatus = tranConfig.defaultStatus
      payload.maRfpSummary = {
        actState: (clientAddress && clientAddress.state) || "",
        actCounty: (clientAddress && clientAddress.city) || "",
      }
    } else {
      payload.actTranStatus = tranConfig.defaultStatus
      payload.actTranState = (clientAddress && clientAddress.state) || ""
      payload.actTranCounty = (clientAddress && clientAddress.city) || ""
    }
    payload.contracts = checked

    if (checked.contractRef && checked.contractRef.contractName) {
      this.props.addAuditLog({
        userName: this.props.user.userFirstName,
        log: `${
          checked.contractRef.contractName
        } Contract was selected while the transaction was created`,
        date: new Date(),
        key: "newTransaction"
      })
    }
    if(payload && (payload.actTranNotes || payload.actNotes) && payloadCategory !== "businessDevelopment"){
      const actTranNotes = payload.actTranNotes || payload.actNotes || ""
      delete payload.actTranNotes
      delete payload.actNotes
      payload.tranNotes = {
        note: actTranNotes || "",
        createdAt: new Date(),
        updatedAt: new Date(),
        updatedByName: `${userFirstName || ""} ${userLastName || ""}` || "",
        updatedById: userId || "",
      }
    } else if(payload && (!payload.actTranNotes || !payload.actNotes) && payloadCategory !== "businessDevelopment"){
      delete payload.actTranNotes
      delete payload.actNotes
    }

    console.log("========payload============>", payload)
    this.setState(
      {
        loading: true,
        modalState: false
      },
      () => {
        payload.createdByUser = this.props.user.userId
        payload.clientActive =
          (contractEnable && contractEnable.agree === "yes") || false
        payload.participant = participant
        if (this.props.nav2 === "rfp") {
          payload.participant.partContactLastName = leadAdv.userLastName
        }

        this.props.addAuditLog({
          userName: this.props.user.userFirstName,
          log: payload.opportunity
            ? `Created an opportunity for ${payload.dealIssueTranType ||
                payload.actType ||
                payload.actTranType ||
                ""}/${payload.dealIssueTranSubType || payload.actTranSubType || payload.actSubType || ""}`
            : "Create transaction",
          date: new Date(),
          key: "newTransaction"
        })

        createTrans(payloadCategory, payload, async res => {
          if (res && (res.status === 200 || res.status === 201)) {
            toast("Transaction has been created!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            if (tranConfig.sendEmail) {
              const type =
                res.data && res.data.dealIssueTranSubType === "Bond Issue"
                  ? "deals"
                  : res.data.actTranSubType === "Bank Loan" || res.data.actTranSubType === "Lines Of Credit" || res.data.actTranSubType === "Letter Of Credit"
                    ? "loan"
                    : payloadCategory
              const tranId = (res.data && res.data._id) || ""
              const emailPayload = {
                ...svControls,
                tranId: (res.data && res.data._id) || "",
                type,
                sendEmailUserChoice: true,
                emailParams: {
                  ...this.state.email,
                  url:
                    type === "businessDevelopment"
                      ? `bus-development/${tranId}`
                      : `${type}/${tranId}/summary`,
                  message: tranConfig.message || ""
                }
              }
              await createTranEmailAlert(emailPayload)
            }
            console.log(
              "tranConfig.svComplianceFlag : ",
              tranConfig.svComplianceFlag, contractEnable.reviewed, contractEnable.agree
            )

            if ((tranConfig.svComplianceFlag && contractEnable.reviewed && contractEnable.agree === "no") || !contract.length) {
              const { svControls } = this.state
              const userDetails = this.props.user
              const userEmail = this.props.userEmail
              const { supervisorUserId, supervisorUserName } = svControls || {}
              if (supervisorUserId) {
                try {
                  const {
                    entityId,
                    userFirstName,
                    userLastName
                  } = this.props.user
                  const supervisor = await getSupervisor()

                  const name = `${userFirstName} ${userLastName}`
                  const recipient = supervisor && supervisor.data.map(item => ({
                    id: item._id,
                    name: `${item.userFirstName} ${item.userLastName}`,
                    firmName: item.userFirmName,
                    emails: item.userEmails
                  }))
                  /* const recipient = {
                    id: supervisorUserId,
                    name: supervisorUserName,
                    emails:["munivisor.dev@otaras.com"]
                  } */
                  const transactionType = payloadCategory === "debt" && payload.actTranSubType === "Bond Issue" ? "deals" :
                    payloadCategory === "debt" && payload.actTranSubType === "Bank Loan" ? "loan" : payloadCategory
                  const controlText = {
                    clientName  : res && res.data && res.data.actTranClientName,
                    firmName : payload.actTranClientName || payload.actIssuerClientEntityName || "",
                    transactionUrl : `${FRONTENDURL}/${transactionType}/${res && res.data && res.data._id}/summary`,
                    contractUrl: `${FRONTENDURL}/clients-propects/${payload.actTranClientId}/contracts`
                  }
                  // export const createCACTask = (token, entityId, recipient, meta, createdBy) => {

                  console.log({ entityId, recipient, controlText, userEmail })
                  const projectDescription =
                    payload.actTranProjectDescription ||
                    payload.actProjectName ||
                    ""
                  const issueName = payload.actTranIssueName || payload.actIssueName || ""
                  const clientName = payload.actTranClientName || payload.actIssuerClientEntityName || ""
                  const tranUrlCac = `${transactionType}/${res && res.data && res.data._id}/summary` || ""
                  // const taskName = !contract.length ? `${clientName} | Transaction - ${issueName || projectDescription}-(No active contracts)` : ""
                  // const taskName = !contract.length ? "No active contracts" : ""
                  // const topic = !contract.length ? `${clientName} | Transaction - ${issueName || projectDescription}` : "Supervisory & Compliance Obligations"
                  const actionDescription = `${clientName}|Transaction - ${issueName || projectDescription} | No active or eligible contracts`
                  createCACTask("", entityId, recipient, controlText, userEmail,actionDescription, tranUrlCac)
                } catch (err) {
                  console.log("error in handling svComplianceFlag : ", err)
                }
              } else {
                console.log("no supervisorUserId in handling svComplianceFlag")
              }
            }
            await this.props.submitAuditLogs(res && res.data && res.data._id)
            // await this.props.getURLsForTenant(this.props.user.entityId)
            setTimeout(() => {
              this.props.history.push("/dash-projects")
            }, 1000)

          } else {
            this.setState(
              {
                loading: false
              },
              () => {
                const errMsg =
                  (res &&
                    res.error &&
                    res.error.response &&
                    res.error.response.data &&
                    res.error.response.data.error) ||
                  "Something went wrong!"
                toast(errMsg, {
                  autoClose: CONST.ToastTimeout,
                  type: toast.TYPE.ERROR
                })
              }
            )
          }
        })
      }
    )
  }

  onContractConfirm = () => {
    const { confirmAlert } = this.state
    confirmAlert.text = "You have chosen an expired contract?"
    swal(confirmAlert)
      .then((willDelete) => {
        if(willDelete){
          this.setState({
            checked: {},
            contractEnable: "",
            contract: []
          }, () => this.onConfirmationSave())
        }
      })
  }

  onCancel = () => {
    this.setState({
      debtIssuance: cloneDeep(CONST.CREATE.loan),
      errorMessages: {},
      contractEnable: ""
    })
  }

  onChangeItem = (item, category) => {
    this.setState({
      debtIssuance: item
    })
  }

  onModalChange = state => {
    this.setState({
      ...state
    })
  }

  contractShow = () => {
    const { contractShow } = this.state
    this.setState({
      contractShow: !contractShow
    })
  }

  onPdfShow = pdf => {
    this.setState(
      {
        contractEnable: ""
      },
      () =>
        this.setState({
          contractEnable: pdf.pdf
        })
    )
  }

  onContractRefresh = async () => {
    const { debtIssuance } = this.state
    let expireContract = []
    const res = await getactivecontracts(
      debtIssuance.actTranClientId || debtIssuance.actIssuerClient
    )
    res && res.data && res.data.length && res.data.forEach(d => {
      if(moment(d.contractRef.endDate).format("YYYY-MM-DD") < moment(new Date()).format("YYYY-MM-DD")){
        d.expired = true
      } else {
        expireContract.push(d)
      }
    })
    this.setState({
      contract: (res && res.data) || [],
      expireContract
    })
  }

  handleCheckClick = async (e, contract) => {
    const { checked } = this.state
    if(!(contract && contract.documents && contract.documents.length)) return
    const res = await getDocDetails(
      contract.documents[0].docAWSFileLocation || ""
    )
    console.log("res : ", res)
    const [error, doc] = res
    if (error) {
      this.setState({ error, waiting: false })
    } else {
      let err = ""
      let objectName = ""
      const { contextId, contextType, tenantId, name } = doc
      if (tenantId && contextId && contextType && name) {
        objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      } else {
        err = "invalid doc details"
      }
      contract.pdfLink = objectName
      contract.fileType = doc.originalName.split(".").pop()
      contract.fileName = doc.originalName
      contract.agree = "no"
      contract.reviewed = false
      checked.contractRef = contract.contractRef || {}
      checked.nonTranFees = contract.nonTranFees || {}
      checked.retAndEsc = contract.retAndEsc || {}
      checked.sof = contract.sof || {}
      checked.digitize = contract.digitize || false
      checked.agree = "no"
      checked.reviewed = false
      checked._id = contract._id || ""
      this.setState(
        {
          error: err,
          contractEnable: "",
          checked,
          page: 1,
          pages: contract.fileType !== "pdf" ? 1 : 0,
          number: 1,
          waiting: false
        },
        () => {
          contract.pdfLink = objectName
          this.setState({
            contractEnable: contract
          })
        }
      )
    }
  }

  onDocumentComplete = pages => {
    this.setState({
      page: 1,
      pages
    })
  }

  handleNext = () => {
    this.setState({
      page: this.state.page + 1
    })
  }

  onNumberChange = (e, name) => {
    this.setState(
      {
        number: name === "button" ? parseInt(e) : parseInt(e.target.value)
      },
      () => this.handlePrevious()
    )
  }

  onReviewed = (value, review) => {
    const { page, pages, checked, contractEnable } = this.state
    if (page === pages) {
      if (review) {
        contractEnable.reviewed = value.target.checked || false
        checked.reviewed = value.target.checked || false
      } else {
        contractEnable.agree = value || "no"
        checked.agree = value || "no"
      }
      this.setState({ checked, contractEnable })
    }
  }

  handlePrevious = () => {
    this.setState({ page: this.state.number || 1 })
  }

  onTranSave = () => {
    const {related, relatedTransactions} = this.state
    const actTranRelatedTo = {
      ...related.actTranRelatedTo,
      relType: related.actTranRelatedType
    }
    relatedTransactions.push(actTranRelatedTo)

    this.setState({
      relatedTransactions,
      related: {
        ...related,
        actTranRelatedTo: ""
      }
    })
  }

  onRemove = index => {
    const {relatedTran} = this.state
    if (relatedTran) {
      relatedTran.splice(index, 1)
      this.setState({relatedTran})
    }
  }

  onTranChange = e => {
    const { related } = this.state
    related[e.target.name] = e.target.value
    this.setState({
      related: {
        ...related,
        actTranRelatedTo: []
      }
    })
  }

  deleteRelatedTran = relatedId => {
    const { relatedTransactions } = this.state
    relatedTransactions.splice(relatedId, 1)
    this.setState({
      relatedTransactions
    })
  }

  onRelatedtranCancel = () => {
    this.setState({
      related: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      }
    })
  }

  onLeadAdvisorRefresh = async () => {
    const advisorData = await fetchLeadAdvisor(this.props.user.entityId)
    this.setState({
      advisor: advisorData || [],
    })
  }

  onSearch = (search) => {
    let searchList = []
    this.setState({
      search
    }, () => {
      if(this.state.search){
        searchList = this.state.issuerList.filter(obj =>
          ["firmName"].some(key => {
            return (
              obj[key] &&
              obj[key].toLowerCase().includes(search.toLowerCase())
            )
          })
        )
      }
      this.setState({
        searchList
      })
    })
  }

  render() {
    const {
      modalState,
      page,
      pages,
      tranConfig,
      searchList,
      search,
      allTrans,
      activityType,
      contractEnable,
      pdfShow,
      checked,
      debtIssuance,
      contract,
      contractShow,
      svControls,
      title,
      transactions,
      dropDown,
      errorMessages,
      category,
      // issuerList,
      advisor,
      number,
      relatedTran,
      related,
      expireContract,
      relatedTransactions
    } = this.state
    const issuerList = search ? searchList : this.state.issuerList || []
    const loading = <Loader />

    if (this.state.loading) {
      return loading
    }

    return (
      <div id="main">
        <TranCreateModal
          modalState={modalState}
          page={page}
          pages={pages}
          handleNext={this.handleNext}
          handlePrevious={this.handlePrevious}
          onPdfShow={this.onPdfShow}
          number={number}
          opportunity={debtIssuance.opportunity || false}
          onDocumentComplete={this.onDocumentComplete}
          checked={checked}
          contractShow={contractShow}
          handleCheckClick={this.handleCheckClick}
          pdfShow={pdfShow}
          onReviewed={this.onReviewed}
          onContractRefresh={this.onContractRefresh}
          contract={contract}
          svControls={svControls}
          issuerId={
            debtIssuance.actTranClientId || debtIssuance.actIssuerClient || ""
          }
          tranConfig={tranConfig}
          tranStatus={dropDown.tranStatus || []}
          onModalChange={this.onModalChange}
          onSave={this.onConfirmationSave}
          contractEnable={contractEnable}
          onContractConfirm={this.onContractConfirm}
          expireContract={expireContract}
          onNumberChange={this.onNumberChange}
        />
        <div className="accordions">
          <div className="accordion is-active">
            <div className="accordion-header">
              <p>{title}</p>
            </div>

            <div className="accordion-body">
              <div className="accordion-content">
                <div className="tile is-ancestor">
                  <div className="tile is-12 is-vertical is-parent">
                    {category === "debt" ||
                    category === "derivative" ||
                    category === "rfp" ||
                    category === "others" ? (
                        <BoxView
                          redirect="debt"
                          title="Debt"
                          activityType={activityType}
                          allTrans={allTrans}
                          onChangeGetTransactions={this.onChangeGetTransactions}
                          item={debtIssuance}
                          advisor={advisor}
                          errorMessages={errorMessages}
                          dropDown={dropDown}
                          transactions={transactions}
                          onChangeItem={this.onChangeItem}
                          category={category}
                          onRelatedTranSelect={this.onRelatedTranSelect}
                          issuerList={issuerList}
                          onTranSave={this.onTranSave}
                          relatedTran={relatedTran}
                          onRemove={this.onRemove}
                          tranType={this.state.tranType}
                          related={related}
                          onTranChange={this.onTranChange}
                          deleteRelatedTran={this.deleteRelatedTran}
                          onRelatedtranCancel={this.onRelatedtranCancel}
                          relatedTransactions={relatedTransactions}
                          onLeadAdvisorRefresh={this.onLeadAdvisorRefresh}
                          onSearch={this.onSearch}
                        />
                      ) : null}
                    {category === "marfp" ||
                    category === "businessDevelopment" ? (
                        <MaRfpBusDev
                          activityType={activityType}
                          onChangeGetTransactions={this.onChangeGetTransactions}
                          allTrans={allTrans}
                          item={debtIssuance}
                          errorMessages={errorMessages}
                          advisor={advisor}
                          dropDown={dropDown}
                          transactions={transactions}
                          onChangeItem={this.onChangeItem}
                          category={category}
                          onRelatedTranSelect={this.onRelatedTranSelect}
                          issuerList={issuerList}
                          onTranSave={this.onTranSave}
                          relatedTran={relatedTran}
                          onRemove={this.onRemove}
                          tranType={this.state.tranType}
                          related={related}
                          onTranChange={this.onTranChange}
                          deleteRelatedTran={this.deleteRelatedTran}
                          onRelatedtranCancel={this.onRelatedtranCancel}
                          onSearch={this.onSearch}
                          relatedTransactions={relatedTransactions}
                        />
                      ) : null}
                    <br />
                    <div className="columns">
                      <div className="column is-full">
                        <div className="field is-grouped-center">
                          <div className="control">
                            <button
                              className="button is-link"
                              onClick={this.onSave}
                            >
                              Save
                            </button>
                          </div>
                          <div className="control">
                            <button
                              className="button is-light"
                              onClick={this.onCancel}
                            >
                              Cancel
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const mapDispatchToProps = dispatch => ({
  getURLsForTenant: tenantID => getURLsForTenant(dispatch, tenantID)
})
const WrappedComponent = withAuditLogs(CreateTran)
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrappedComponent)
