import Joi from "joi-browser"

const rfpTeamAssignee = Joi.object().keys({
  _id: Joi.string().required().optional(),
  rfpTeamAssigneeId: Joi.string().required(),
  rfpTeamAssigneeFirstName: Joi.string().required().optional(),
  rfpTeamAssigneeLastName: Joi.string().required().optional(),
  rfpTeamAssigneeEmailId: Joi.string().required().optional(),
})

const checkListItems = Joi.object().keys({
  _id: Joi.string().required().optional(),
  rfpListItemDetails: Joi.string().required().optional(),
  rfpListItemConsider: Joi.boolean().required().optional(),
  rfpListItemPriority: Joi.string().required().optional(),
  rfpListItemEndDate: Joi.string().required().optional(),
  rfpListItemResolved: Joi.boolean().required().optional(),
  rfpListItemAssignees: Joi.array().min(1).items(rfpTeamAssignee).required()
})

const rfpBidPacketChecklist = Joi.object().keys({
  checkListCategoryId: Joi.string().required().optional(),
  checkListItems: Joi.array().items(checkListItems).required().optional(),
  _id: Joi.string().required().optional(),
})

const checkInTrackSchema = Joi.object().keys({
  rfpBidCheckList:  Joi.array().items(rfpBidPacketChecklist).required().optional(),
})

export const CheckInTrackValid = (input) => {
  return Joi.validate(input, checkInTrackSchema, { abortEarly: false, stripUnknown:false })
}

