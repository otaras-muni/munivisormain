import React from "react"
import moment from "moment"
import { DropdownList, Multiselect } from "react-widgets"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import TableHeader from "../../../../GlobalComponents/TableHeader"

const CheckIn = ({ isActive, onAccordion, category, actionButtons, dropDown, cols, items, onCategoryInputChange, categoryInputValue, assignList, onAssignedSelect, onTextChange, errors}) => (
  <RatingSection key={category.key} title={category.name} actionButtons={actionButtons} onAccordion={onAccordion}>
    {(isActive || !onAccordion) &&
    <div className="accordion-content">
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <TableHeader cols={cols}/>
        <tbody>
          {
            items && items.map((item, i) => (
              <tr key={i}>
                <td>
                  <label className="checkbox">
                    <input type="checkbox" checked={item.rfpListItemConsider} name="rfpListItemConsider" onChange={(e) => onCategoryInputChange(e, category.key, i)}/>
                  </label>
                </td>
                <td>{item.rfpListItemDetails}</td>
                <td>
                  <div className="select is-small is-link">
                    <select value={item.rfpListItemPriority} name="rfpListItemPriority" onChange={(e) => onCategoryInputChange(e, category.key, i)}>
                      <option value="">Pick priority</option>
                      {
                        dropDown.priority ? dropDown.priority.map((p, i) => (
                          <option key={i}>{p}</option>
                        )): null
                      }
                    </select>
                    {errors[i] && <p className="text-error">{errors[i].rfpListItemPriority}</p> }
                  </div>
                </td>
                <td>
                  <div className="control">
                    <DropdownList filter data={assignList} defaultValue={0} value={item.rfpListItemAssignees.length ? item.rfpListItemAssignees[0].rfpTeamAssigneeId : ""} onChange={(e) => onAssignedSelect(e, category.key, i)}  textField="name" valueField="id" />
                    {errors[i] && <p className="text-error">{errors[i].rfpListItemAssignees}</p> }
                  </div>
                  {/* <div className="control">
                    <Multiselect filter data={assignList} style={{maxWidth: 160,overflowY: "visible"}} textField="name" value={item.rfpListItemAssignees ? item.rfpListItemAssignees.map(item=>item.rfpTeamAssigneeId) : []} valueField="id" onChange={(e) => onAssignedSelect(e, category.key, i)} />
                  </div> */}
                </td>
                <td>
                  <div className="field">
                    <div className="control">
                      <input id="datepickerDemo" className="input is-small is-link" name="rfpListItemEndDate" onChange={(e) => onCategoryInputChange(e, category.key, i)} value={item.rfpListItemEndDate && moment(item.rfpListItemEndDate).format("YYYY-MM-DD")} type="date" />
                      {errors[i] && <p className="text-error">{errors[i].rfpListItemEndDate}</p> }
                    </div>
                  </div>
                </td>
                <td>
                  <label className="checkbox">
                    <input type="checkbox" checked={item.rfpListItemResolved} name="rfpListItemResolved" onChange={(e) => onCategoryInputChange(e, category.key, i)}/>
                  </label>
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
      <div className="addItem">
        <input type="text" value={categoryInputValue || ""} onChange={onTextChange} name={`${category.key}`} placeholder="New List Item"/>
        {errors[category.key] && <p className="text-error">{errors[category.key]}</p>}
      </div>
    </div>
    }
  </RatingSection>
)

export default CheckIn
