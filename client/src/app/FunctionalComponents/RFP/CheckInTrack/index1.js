import React from "react"
import {connect} from "react-redux";
import { withRouter } from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import Accordion from "../../../GlobalComponents/Accordion"
import CONST from "../../../../globalutilities/consts"
import CheckIn from "./components/CheckIn"
import {fetchAssigned} from "../../../StateManagement/actions/CreateTransaction"
import {
  fetchTransaction,
  updateTransaction
} from "../../../StateManagement/actions/TransactionDistribute"
import {
  CheckInTrackValid
} from "./Validation/CheckInTrackValid"
import Loader from "../../../GlobalComponents/Loader"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"

const cols = [
  {name: "Consider?"},
  {name: "List Item"},
  {name: "Priority"},
  {name: "Assigned to"},
  {name: "End Date"},
  {name: "Resolved"}
]

const newCheckIn =  {
  rfpListItemConsider: true,
  rfpListItemDetails: "",
  rfpListItemPriority: "",
  rfpListItemAssignees: [],
  rfpListItemEndDate: "",
  rfpListItemResolved: false,
}
class CheckInTrack1 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      transId: "",
      errorMessages: {},
      transaction: {},
      rfpBidCheckList: CONST.CheckInTrack,
      tempRfpBidCheckList: CONST.CheckInTrack,
      assignList: [],
      suppliersList: [],
      notes: "",
      isSummary: false,
      loading: true,
      dropDown: {
        priority: []
      },
    }
  }
  componentWillMount() {
    const transId = this.props.nav2
    let {rfpBidCheckList} = this.state
    if (transId) {
      fetchTransaction(this.props.nav3, transId, (transaction)=> {
        if(transaction && transaction.rfpTranClientId) {
          const ids = `${transaction.rfpTranClientId},${transaction.rfpTranIssuerId}`
          fetchAssigned(ids, (res)=> {
            if(transaction.rfpBidCheckList.length) {
              rfpBidCheckList = cloneDeep(transaction.rfpBidCheckList)
              rfpBidCheckList = this.setRfpBidCheckList(rfpBidCheckList)
            }
            this.setState({
              transId,
              transaction,
              assignList: res.usersList,
              rfpBidCheckList,
              loading: false,
              tempRfpBidCheckList: cloneDeep(rfpBidCheckList)
            })
          })
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  async componentDidMount() {
    let result = await getPicklistByPicklistName(["LKUPHIGHMEDLOW"])
    result = (result.length && result[1]) || {}
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        priority: result["LKUPHIGHMEDLOW"] || [],
      }
    })
  }

  onTextChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    })
  }

  onChange = (e, category, index) => {
    const {rfpBidCheckList} = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `${e.target.name || "empty"} user change to ${e.target.type === "checkbox" ? e.target.checked : e.target.value}`, date: new Date(), category})

    const newCheckInList = rfpBidCheckList.map(item => {
      if(item.key === category) {
        if(e.target.type === "checkbox") {
          item.checkListItems[index] = {
            ...item.checkListItems[index],
            [e.target.name]: e.target.checked
          }
        } else {
          item.checkListItems[index] = {
            ...item.checkListItems[index],
            [e.target.name]: e.target.value
          }
        }
      }
      return item
    })
    this.setState({
      rfpBidCheckList: newCheckInList,
    })
  }

  onAssignedSelect = (items, category, index) => {
    const {rfpBidCheckList} = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `${category} Assign user change to ${items.name || "empty"}`, date: new Date(), category})
    let email = ""
    if(items && Array.isArray(items.userEmails) && items.userEmails.length) {
      email = items.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : items.userEmails[0].emailId
    }
    const newCheckInList = rfpBidCheckList.map(item => {
      if(item.key === category) {
        item.checkListItems[index] = {
          ...item.checkListItems[index],
          rfpListItemAssignees: [{
            rfpTeamAssigneeId: items.id,
            rfpTeamAssigneeFirstName: items.userFirstName,
            rfpTeamAssigneeLastName: items.userLastName,
            rfpTeamAssigneeEmailId: email,
          }], // items.map(item=>({ rfpTeamAssigneeId: item.id} )),
        }
      }
      return item
    })
    this.setState({
      rfpBidCheckList: newCheckInList,
    })
  }

  onSave = () => {
    const {transId, rfpBidCheckList} = this.state
    let payload = cloneDeep(rfpBidCheckList)
    payload = payload.map(x => {
      delete x.name
      delete x.key
      return x
    })
    payload = {rfpBidCheckList: payload}

    const errors = CheckInTrackValid(payload)
    if(errors && errors.error) {
      const errorMessages = {}
      errors.error.details.map(err => { //eslint-disable-line
        if(errorMessages.hasOwnProperty([err.path[1]])) { //eslint-disable-line
          if(errorMessages[err.path[1]].hasOwnProperty(err.path[3])) { //eslint-disable-line
            errorMessages[err.path[1]][err.path[3]][err.path[4]] = "Required" // err.message
          } else {
            errorMessages[err.path[1]][err.path[3]] = {
              [err.path[4]]: "Required" // err.message
            }
          }
        } else {
          errorMessages[err.path[1]] =
            {
              [err.path[3]]: {
                [err.path[4]]: "Required" // err.message
              }
            }
        }
      })
      this.setState({errorMessages})
      return
    }
    updateTransaction(transId, this.props.nav3, payload, [], (res)=> { //eslint-disable-line
      if(res) {
        this.props.submitAuditLogs(transId)
        this.setState({
          errorMessages: {}
        })
        toast("Checkin in track Update successfully!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      }
    })
  }

  onAdd = (key) => {
    const {rfpBidCheckList} = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `${key} Add new Item`, date: new Date(), key})
    let isValid = true
    rfpBidCheckList.forEach(item => {
      if(item.key === key) {
        const isDuplicate = item.checkListItems.filter(c => c.rfpListItemDetails === this.state[key])
        isValid = !isDuplicate.length
      }
    })
    if(!isValid) return this.setState({
      errorMessages: {
        [key]: `${this.state[key]} is already exists.`
      }
    })
    const newObj  = {
      ...newCheckIn,
      rfpListItemDetails: this.state[key],
    }
    rfpBidCheckList.map(item => {
      if(item.key === key) {
        item.checkListItems.push(newObj)
      }
      return item
    })
    this.setState({
      rfpBidCheckList,
      [key]: "",
      errorMessages: {}
    })
  }

  onReset = (key) => {
    let {rfpBidCheckList} = this.state
    const auditLogs = this.props.auditLogs.filter(x=>x.key !== key)
    rfpBidCheckList = rfpBidCheckList.map(x => {
      if(x.key === key) {
        const tempCheck = this.state.tempRfpBidCheckList.find(t => t.key === key)
        x.checkListItems = cloneDeep(tempCheck.checkListItems)
      }
      return x
    })
    this.props.updateAuditLog(auditLogs)
    this.setState({
      rfpBidCheckList,
    })
  }

  setRfpBidCheckList = (rfpBidCheckList) => {
    rfpBidCheckList.map(x => {
      x.name = CONST.CheckInTrack.find(c => c.key === x.checkListCategoryId) && CONST.CheckInTrack.find(c => c.key === x.checkListCategoryId).name
      x.key = x.checkListCategoryId
      return x
    })
    return rfpBidCheckList
  }

  actionButtons = (key) => (
    <div className="field is-grouped">
      <div className="control">
        <button className="button is-link is-small" disabled={!(this.state[`${key}`] && this.state[`${key}`].trim())} onClick={()=>this.onAdd(key)}>Add</button>
      </div>
      <div className="control">
        <button className="button is-light is-small" onClick={()=>this.onReset(key)}>Reset</button>
      </div>
    </div>
  )

  handleSummary = () => {
    this.setState(prevState => ({isSummary: !prevState.isSummary}))
  }

  render() {
    const {rfpBidCheckList, dropDown, assignList, errorMessages, transaction, isSummary} = this.state

    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }

    return(
      <div className='accordions distributeRFP'>
        <br/>
        <Accordion multiple
          activeItem={[0]}
          render={({activeAccordions, onAccordion}) =>
            rfpBidCheckList.map((category, index) => {
              const isActive = activeAccordions.includes(index)
              return (
                <CheckIn key={category.key+ this.state.activeTab} isActive={isActive} items={category.checkListItems}
                  actionButtons={this.actionButtons(category.key)} cols={cols} category={category} errors={errorMessages[index] || {}}
                  onRating={this.onRating} onCategoryInputChange={this.onChange} onTextChange={this.onTextChange}
                  onAccordion={() => onAccordion(index)} assignList={assignList} onAssignedSelect={this.onAssignedSelect}
                  categoryInputValue={this.state[`${category.key}`]} dropDown={dropDown}
                />
              )
            })}
        />

        <article className="accordion is-active">
          <div className="accordion-header">
            <p>Notes/Instructions</p>
          </div>
          <div className="accordion-body">
            <div className="accordion-content">
              <div className="columns">
                <div className="column is-full">
                  <div className="field">
                    <label className="label">
                      <abbr title="Notes and instructions by transaction participants">Notes/Instructions</abbr>
                    </label>
                    <div className="control">
                      <textarea className="textarea" name="notes" onChange={this.onTextChange} placeholder="" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button className="button is-link" onClick={this.onSave}>Save</button>
                    </div>
                    <div className="control">
                      <button className="button is-light">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
        <br/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(CheckInTrack1)

export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
