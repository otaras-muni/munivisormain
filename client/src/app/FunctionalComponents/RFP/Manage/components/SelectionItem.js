import React from "react"

const InputRadio = ({name, value, onRating, category, listName, selected}) => (
  <td>
    <label className="radio">
      <input type="radio" radioGroup={name} onChange={() => onRating(value, category, listName, null)}
        checked={selected === value} name={name} value={value}/>
    </label>
  </td>
)

const InputRadioGroup = ({item, selection, onRating, category}) => [-1, 1, 2, 3, 4, 5].map(i => (
  <InputRadio key={i} name={item.name} selected={selection.evalRating} value={i} onRating={onRating} category={category}
    listName={item.name}/>
))

const SelectionItem = ({item, onRating, category, selection, isAuth, onRemove}) => (
  <tr>
    <td>{item.name}</td>
    <td>
      <div className="select is-small is-link">
        <select value={selection.evalPriority || "Low"}
          onChange={(event) => onRating(null, category, item.name, event.target.value)} disabled={!isAuth}>
          <option value="Low">Low</option>
          <option value="Medium">Medium</option>
          <option value="High">High</option>
        </select>
      </div>
    </td>
    <InputRadioGroup item={item} selection={selection} onRating={onRating} category={category} />
    {/*<td>
      <div>
        <span onClick={onRemove}>X</span>
      </div>
    </td>*/}
  </tr>
)

export default SelectionItem
