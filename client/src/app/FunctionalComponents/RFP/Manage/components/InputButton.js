import React from "react"
import Select from "react-select"

const InputButton = ({ onChange, value, onPick, suppliers = [] }) => {
  const distributors = suppliers.map(mem => ({ value: mem.rfpParticipantFirmId, label: mem.rfpParticipantFirmName, firmName: mem.rfpParticipantRealFirmName, msrbType: mem.rfpParticipantRealMSRBType }))

  const onSelect = (obj) => {
    onChange({ target: { name: "rfpParticipantFirmId", value: obj.value, firm: obj } })
  }

  return (
    <div className="field has-addons">
      <div className="control">
        {/* <input className="input" type="text" name="rfpParticipantFirmId" value={value} onChange={onChange} placeholder="Pick Respondent / Supplier" /> */}
        <Select
          name="rfpParticipantFirmId"
          value={value}
          onChange={onSelect}
          options={[
            { value: "", label: "Pick Respondent / Supplier" },
            ...distributors,
          ]}
        />
      </div>
    </div>
  )
}

export default InputButton
