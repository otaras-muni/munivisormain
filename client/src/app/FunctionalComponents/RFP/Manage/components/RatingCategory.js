import React from "react"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import SelectionItem from "./SelectionItem"
import TableHeader from "../../../../GlobalComponents/TableHeader"

const RatingCategory = ({ isAuth, isActive, onAccordion, category, actionButtons, cols, items, onRating, onCategoryInputChange, categoryInputValue, onRemove, error}) => (
  <RatingSection key={category.key} title={category.name} actionButtons={actionButtons} onAccordion={onAccordion}>
    {(isActive || !onAccordion) &&
    <div className="accordion-content">
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <TableHeader cols={cols}/>
        <tbody>
          {
            category.listItems.map((item, i) => {
              const selection = items.find(x => x.evalItem === item.name) || {}
              return <SelectionItem key={item.name} item={item} onRating={onRating} selection={selection}
                category={category.key} isAuth={isAuth} onRemove={() => onRemove(category.key, item.name)}/>
            })
          }
        </tbody>
      </table>
      {isAuth &&
      <div className="addItem">
        <input type="text" value={categoryInputValue || ""} onChange={onCategoryInputChange} name={`${category.key}Input`} placeholder="New List Item"/>
        {error && <p className="text-error">{error}</p> }
      </div>
      }
    </div>
    }
  </RatingSection>
)

export default RatingCategory
