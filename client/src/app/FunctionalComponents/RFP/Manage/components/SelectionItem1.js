import React from "react"

const InputRadio = ({name, value, onRating, category, listName, selected, canEditTran}) => (
  <td>
    <label className="radio">
      <input type="radio" radioGroup={name} onChange={() => onRating(value, category, listName, null)}
             checked={selected === value} name={name} value={value} disabled={!canEditTran}/>
    </label>
  </td>
)

const InputRadioGroup = ({item, onRating, category, canEditTran}) => [-1, 1, 2, 3, 4, 5].map(i => (
  <InputRadio key={i} name={item.evalItem} selected={item.evalRating} value={i} onRating={onRating} category={category}
              listName={item.evalItem} canEditTran={canEditTran}/>
))

const SelectionItem = ({item, onRating, category, priority, isAuth, canEditTran}) => (
  <tr>
    <td>{item.evalItem}</td>
    {
      canEditTran ?
        <td>
          <div className="select is-small is-link">
            <select value={item.evalPriority || "Low"} onChange={(event) => onRating(null, category, item.evalItem, event.target.value)} disabled={!isAuth || !canEditTran}>
              <option value="">Pick</option>
              {
                priority.map((p, i) => (
                  <option key={i} disabled={!p.included}>{p && p.label}</option>
                ))
              }
            </select>
          </div>
        </td> :
        <td>{item.evalPriority || "N/A"}</td>
    }

    <InputRadioGroup item={item} onRating={onRating} category={category} canEditTran={canEditTran}/>
  </tr>
)

export default SelectionItem
