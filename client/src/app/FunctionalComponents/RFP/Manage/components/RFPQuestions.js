import React from "react"
import {connect} from "react-redux"
import {toast} from "react-toastify"
import {fetchTransaction} from "../../../../StateManagement/actions/TransactionDistribute"
import {addRfpQuestion} from "../../../../StateManagement/actions/CreateTransaction"
import QuestionBox from "./QuestionBox"
import Loader from "../../../../GlobalComponents/Loader"
import DocUpload from "../../../docs/DocUpload"
import CONST from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {fetchDocDetails, getS3FileGetURL, sendEmailAlert} from "../../../../StateManagement/actions/Transaction"

const question = {
  rfpPartContactId: "",
  rfpPartQuestionId: "",
  rfpPartQuestDocId: "",
  rfpPartQuestDetails: "",
  rfpPartQuestPosts: []
}

class RFPQuestions extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      bucketName: CONST.bucketName,
      docId: "",
      filename: "",
      errors: {},
      transId: "",
      rfpParticipantQuestions: [],
      keyInfoUser: [],
      rfpQuestion: {
        ...question
      },
      isSaveDisabled: false,
      loading: true
    }
  }
  componentWillMount() {
    const {rfpParticipants, transactionId} = this.props
    const keyInfoUser = rfpParticipants.map(id => id.rfpParticipantContactId)
    if (transactionId) {
      this.getTransactionQuestions(transactionId)
    }
    this.setState({
      keyInfoUser
    })
  }

  getTransactionQuestions = (transId) => {  //eslint-disable-line
    const { user, userEmail } = this.props
    fetchTransaction("question", transId, (transaction)=> {
      if(transaction && transaction.rfpTranClientId) {
        let rfpParticipantQuestions = []
        if(user && user.relationshipToTenant && user.relationshipToTenant !== "Third Party"){
          rfpParticipantQuestions = transaction.rfpParticipantQuestions || []
        } else {
          rfpParticipantQuestions = transaction && transaction.rfpParticipantQuestions.filter(r => user.userId === r.rfpPartContactId) || []
        }
        this.setState((prevState) => ({
          transId,
          rfpParticipantQuestions,
          rfpQuestion: {
            ...prevState.rfpQuestion,
            rfpPartContactId: user.userId || "",
            rfpPartUserFirstName: user.userFirstName || "",
            rfpPartUserLastName: user.userLastName || "",
            rfpPartUserEmailId: userEmail || "",
          },
          loading: false,
        }))
      }else {
        this.setState({
          loading: false,
        })
      }
    })
  }

  onChange = (e) => {
    const { rfpQuestion } = this.state
    rfpQuestion[e.target.name] = e.target.value
    this.setState({
      rfpQuestion
    })
  }

  onSubmit = () => {
    const { docId, transId, rfpQuestion } = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    if(rfpQuestion && !rfpQuestion.rfpPartQuestDetails)   {
      return this.setState({
        errors: {
          rfpPartQuestDetails: "Required"
        }
      })
    }
    this.props.addAuditLog({userName, log: `Supplier Question ${rfpQuestion.rfpPartQuestDetails} added`, date: new Date(), category: "Supplier Question"})
    docId ? rfpQuestion.rfpPartQuestDocId = docId : delete rfpQuestion.rfpPartQuestDocId

    this.setState({
      isSaveDisabled: true
    },() => {
      rfpQuestion.createdAt =  new Date()
      addRfpQuestion(transId, {rfpParticipantQuestions: rfpQuestion}, (res) => { //eslint-disable-line
        if (res) {
          this.props.submitAuditLogs(transId)
          this.setState({
            errors: {},
            rfpQuestion: {
              ...question,
            },
            docId: "",
            filename: "",
            isSaveDisabled: false
          }, async () => {
            this.getTransactionQuestions(transId)
            await sendEmailAlert(this.props.emailPayload)
          })
          toast("Question Added successfully!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
        }
      })
    })
  }

  getBidBucket = (filename, docId) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `Supplier Question ${filename} Uploaded`, date: new Date(), category: "Supplier Question"})
    this.setState({
      docId,
      filename
    })
  }

  onFileAction = (e, docId) => {
    fetchDocDetails(docId).then(res => {
      this.getFileURL(this.state.bucketName, res.name, this.downloadFile)
    })
  }

  downloadFile = () => {
    this.downloadAnchor.click()
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, (res) => {
      this.setState({
        ...res
      }, callback.bind(this, bucketName, fileName))
    })
  }

  render() {
    const {rfpQuestion, keyInfoUser, rfpParticipantQuestions, errors, isSaveDisabled, bucketName, filename, transId, signedS3Url} = this.state
    const { user, tranAction } = this.props
    const canEditTran = (tranAction && tranAction.canManageEdit && tranAction.canTranStatusEditDoc && keyInfoUser &&
      keyInfoUser.indexOf(user.userId) !== -1) || (tranAction && tranAction.canManageEdit && tranAction.canTranStatusEditDoc) || false
    const tags = {
      userName: user.userFirstName,
      userId: user.userId,
      entityId: user.entityId,
    }
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return(
      <div className="accordions">
        {canEditTran ?
          <div className="columns">
            <div className="column">
              <article className="media">
                <div className="media-content">
                  <div className="field">
                    <div className="control">
                      <p className="multiExpLblBlk">Question
                        <span className="icon has-text-danger">
                            <i className="fas fa-asterisk extra-small-icon"/>
                          </span>
                      </p>
                      <textarea className="textarea" name='rfpPartQuestDetails' value={rfpQuestion.rfpPartQuestDetails} onChange={this.onChange}
                        placeholder="Post question to evaluation / selection committee..."/>
                      {errors.rfpPartQuestDetails ? <p className="text-error">{errors.rfpPartQuestDetails}</p> : null}
                    </div>
                  </div>
                  <nav className="level">
                    <div className="level-left">
                      <div className="level-item">
                        <DocUpload bucketName={bucketName} onUploadSuccess={this.getBidBucket} responseOrQuestion showFeedback tags={tags} contextId={this.props.transactionId} contextType={"RFPS"} tenantId={this.props.user.entityId}/>
                        <p>{filename}</p>
                      </div>
                      <div className="level-right">
                        <div className="level-item">
                          <button className="button is-info" role="button" onClick={this.onSubmit} disabled={isSaveDisabled}>Post</button>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
              </article>
            </div>
          </div>
          : null }
        <a className="hidden-download-anchor"
          style={{ display: "none" }}
          ref={ el => { this.downloadAnchor = el } }
          href={signedS3Url} target="_blank" />
        <QuestionBox transId={transId} rfpParticipantQuestions={rfpParticipantQuestions} onFileAction={this.onFileAction}/>
      </div>
    )
  }

}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(RFPQuestions)

export default connect(mapStateToProps, null)(WrappedComponent)
