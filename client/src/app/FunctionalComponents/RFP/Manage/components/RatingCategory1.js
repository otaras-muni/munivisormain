import React from "react"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import SelectionItem1 from "./SelectionItem1"
import TableHeader from "../../../../GlobalComponents/TableHeader"

const RatingCategory1 = ({
  isAuth,
  isActive,
  onAccordion,
  category,
  dropDown,
  title,
  canEditTran,
  actionButtons,
  cols,
  items,
  onRating,
  onCategoryInputChange,
  categoryInputValue,
  onRemove,
  error
}) => (
  <RatingSection
    key={category}
    title={title}
    actionButtons={actionButtons}
    onAccordion={onAccordion}
  >
    {(isActive || !onAccordion) && (
      <div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <TableHeader cols={cols} />
          <tbody>
            {items.map((item, i) => {
              const selection = items.find(x => x.evalItem === item.name) || {}
              return (
                <SelectionItem1
                  key={item.evalItem}
                  item={item}
                  priority={dropDown.priority || []}
                  onRating={onRating}
                  selection={selection}
                  category={category}
                  isAuth={isAuth}
                  onRemove={() => onRemove(category, item.evalItem)}
                  canEditTran={canEditTran}
                />
              )
            })}
          </tbody>
        </table>
        {isAuth && canEditTran && (
          <div className="addItem">
            <input
              className="input is-link"
              type="text"
              value={categoryInputValue || ""}
              onChange={onCategoryInputChange}
              name={`${category}Input`}
              placeholder="New List Item"
            />
            {error && <p className="text-error">{error}</p>}
          </div>
        )}
      </div>
    )}
  </RatingSection>
)

export default RatingCategory1
