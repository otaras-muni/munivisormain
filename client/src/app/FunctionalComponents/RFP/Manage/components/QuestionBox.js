import React from "react"
import {Link} from "react-router-dom"
import DocLink from "../../../docs/DocLink";
import moment from "moment";
import "../../../../../public/images/user-avatar.jpg"
const QuestionBox = ({rfpParticipantQuestions, transId, onFileAction}) => (
  <div className="columns">
    <div className="column">
      <div className="box">
        {
          rfpParticipantQuestions && rfpParticipantQuestions.map((que,i) => (
            <article className="media" key={i}>
              <figure className="media-left">
                <p className="image is-64x64">
                  <img src="/images/user-avatar.jpg"/>
                </p>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong>
                      {`${que.rfpPartUserFirstName} ${que.rfpPartUserLastName}`}
                    </strong>&nbsp;
                    <small className="has-text-grey is-size-7">
                      {que.createdAt ? moment(que.createdAt).format("DD-MMM-YYYY hh:mm A") : ""}
                    </small>
                    <Link to={`/rfp/${transId}/managequemedia/${que._id}`}
                          className="button is-small is-info pull-right">Comments ({que.rfpPartQuestPosts.length})
                      &nbsp;<span className="icon is-small">
                    <i className="fa fa-comments"/>
                  </span>
                    </Link>
                  </p>
                </div>
                <small>
                  <span style={{ display: "inline-block" }}
                        className="has-text-justified">{que.rfpPartQuestDetails}</span>
                  {que.rfpPartQuestDocId &&
                  <a style={{ display: "inline-block", marginLeft: 10 }}
                     onClick={(e) => onFileAction(e, que.rfpPartQuestDocId)}>
                    {que.rfpPartQuestDocId ? <div className="field is-grouped">
                      <DocLink docId={que.rfpPartQuestDocId}/>
                    </div> : null}
                  </a>
                  }
                </small>
              </div>
            </article>
          ))
        }
      </div>
    </div>
  </div>
)

export default QuestionBox
