import React from "react"
import {connect} from "react-redux"
import {toast} from "react-toastify"
import {
  fetchDocDetails, fetchTransactionQueMedia, getS3FileGetURL,
  postRfpMedia, sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import DocUpload from "../../../docs/DocUpload"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import PostBox from "./PostBox"
import CONST from "../../../../../globalutilities/consts"

const post = {
  postDetails: "",
  postDate: new Date(),
  postVisibilityFilter: "",
  postContactId: "",
  postDocument: "",
  postParentId: "",
  postVisibilityFlag: false,
  rfpPartQuestPosts: []
}

class QueMedia extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      bucketName: CONST.bucketName,
      docId: "",
      filename: "",
      errors: {},
      transId: "",
      rfpParticipantQuestions: [],
      keyInfoUser: [],
      rfpPost: {
        ...post
      }
    }
  }
  componentWillMount() {
    const {transactionId, nav4, rfpParticipants} = this.props
    const keyInfoUser = rfpParticipants.map(id => id.rfpParticipantContactId)

    if (transactionId) {
      this.getTransactionQueMedia(transactionId, nav4)
    }
    this.setState({
      keyInfoUser
    })
  }

  getTransactionQueMedia = (transId, queId) => {  //eslint-disable-line
    fetchTransactionQueMedia({transId, queId}, (transaction)=> {
      if(transaction && transaction.rfpTranClientId) {
        this.setState((prevState) => ({
          transId,
          queId,
          rfpParticipantQuestions: transaction.rfpParticipantQuestions,
          rfpPost: {
            ...prevState.rfpPost,
            postContactId: this.props.user.userId || "",
            postParentId: queId,
            postUserFirstName: this.props.user.userFirstName || "",
            postUserLastName: this.props.user.userLastName || "",
            postUserEmailId: this.props.userEmail || "",
          }
        }))
      }
    })
  }

  onChange = (e) => {
    const { rfpPost } = this.state
    if(e.target.type === "checkbox") {
      rfpPost[e.target.name] = e.target.checked
    }else {
      rfpPost[e.target.name] = e.target.value
    }
    this.setState({
      rfpPost
    })
  }

  onSubmit = () => {
    const { docId, transId, rfpPost, queId } = this.state
    rfpPost.postDate = new Date()
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    if(rfpPost && !rfpPost.postDetails)   {
      return this.setState({
        errors: {
          postDetails: "Required"
        }
      })
    }
    this.props.addAuditLog({userName, log: `Supplier Post Media ${rfpPost.postDetails} added`, date: new Date(), category: "Supplier Post Media"})
    docId ? rfpPost.postDocument = docId : delete rfpPost.postDocument

    this.setState({
      isSaveDisabled: true
    }, () => {
      postRfpMedia({transId, queId}, {rfpPost: rfpPost}, [], (res)=> { //eslint-disable-line
        this.props.submitAuditLogs(transId)
        if(res && Array.isArray(res.rfpParticipantQuestions) && res.rfpParticipantQuestions.length) {
          this.setState({
            errors: {},
            rfpParticipantQuestions: res.rfpParticipantQuestions[0],
            rfpPost: {
              ...post,
              postContactId: this.props.user.userId || "",
              postUserFirstName: this.props.user.userFirstName || "",
              postUserLastName: this.props.user.userLastName || "",
              postUserEmailId: this.props.userEmail || "",
              postParentId: queId,
            },
            docId: "",
            filename: "",
            isSaveDisabled: false
          }, async () => {
            toast("comment added successfully!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            await sendEmailAlert(this.props.emailPayload)
          })
        }
      })
    })
  }

  getBidBucket = (filename, docId) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `Supplier Post Media ${filename} Uploaded`, date: new Date(), category: "Supplier Post Media"})
    this.setState({
      docId,
      filename
    })
  }

  onFileAction = (e, docId) => {
    fetchDocDetails(docId).then(res => {
      this.getFileURL(this.state.bucketName, res.name, this.downloadFile)
    })
  }

  downloadFile = () => {
    this.downloadAnchor.click()
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, (res) => {
      this.setState({
        ...res
      }, callback.bind(this, bucketName, fileName))
    })
  }

  render() {
    const {isSaveDisabled, rfpParticipantQuestions, rfpPost, errors, bucketName, filename, signedS3Url, keyInfoUser} = this.state
    const {user, tranAction, transactionId} = this.props
    const canEditTran = (tranAction && tranAction.canManageEdit && tranAction.canTranStatusEditDoc && keyInfoUser &&
      keyInfoUser.indexOf(user.userId) !== -1) || (tranAction && tranAction.canManageEdit && tranAction.canTranStatusEditDoc) || false

    return(
      <section className="box">
        <div className="columns">
          <div className="column">
            {
             (rfpParticipantQuestions &&
              rfpParticipantQuestions.rfpPartQuestPosts &&
              rfpParticipantQuestions.rfpPartQuestPosts.length) ?
              rfpParticipantQuestions.rfpPartQuestPosts.map((post,i) => (
                <PostBox key={i} post={post} user={user} onFileAction={this.onFileAction}/>
              )) : "Response not found..."
            }
            <a  className="hidden-download-anchor"
              style={{ display: "none" }}
              ref={ el => { this.downloadAnchor = el } }
              href={signedS3Url} target="_blank" />
            <br/>
            {canEditTran ?
              <article className="media">
                <div className="media-content">
                  <div className="field">
                    <div className="control">
                  <textarea className="textarea" name='postDetails' value={rfpPost.postDetails} onChange={this.onChange}
                            placeholder="Post response..."/>
                      {errors.postDetails ? <p className="text-error">{errors.postDetails}</p> : null}
                    </div>
                  </div>
                  <nav className="level">
                    <div className="level-left">
                      <div className="level-item">
                        <DocUpload bucketName={bucketName} onUploadSuccess={this.getBidBucket} contextId={transactionId} contextType="RFPMANAGESUPPLIERQUESTIONS" tenantId={user.entityId} responseOrQuestion showFeedback/>
                        <p>{filename}</p>
                      </div>
                      <div className="level-right">
                        <div className="level-item">
                          <button className="button is-info" role="button" onClick={this.onSubmit} disabled={isSaveDisabled || false}>Post</button>
                        </div>
                        {/* <div className="level-item">
                          <label className="checkbox">
                            <input type="checkbox" name="postVisibilityFlag" checked={rfpPost.postVisibilityFlag} onChange={this.onChange}/> Reponse visible to all suppliers
                          </label>
                        </div> */}
                      </div>
                    </div>
                  </nav>
                </div>
              </article> :null}
          </div>
        </div>
      </section>
    )
  }

}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(QueMedia)

export default connect(mapStateToProps, null)(WrappedComponent)
