import React from "react"
import { connect} from "react-redux"
import cloneDeep from "lodash.clonedeep"
import _  from "lodash"
import {toast} from "react-toastify"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import {
  postRfpTransaction,
  fetchRfpContacts,
  fetchRfpEvaluations1,
  pullRfpEvaluationDocument,
  putRfpEvaluationDocuments,
  sendEmailAlert} from "../../../../StateManagement/actions/Transaction"
import Accordion from "../../../../GlobalComponents/Accordion"
import Tabs from "../../../../GlobalComponents/Tabs"
import Loader from "../../../../GlobalComponents/Loader"
import RatingCategory1 from "./RatingCategory1"
import InputButton from "./InputButton"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import TranDocuments from "../../../../GlobalComponents/TranDocuments"

const cols = [
  {name: "List Item"},
  {name: "Priority"},
  {name: "NA"},
  {name: 1},
  {name: 2},
  {name: 3},
  {name: 4},
  {name: 5},
]

const boxs = [
  {name: "Cost of Service", key: "costOfService"},
  {name: "Qualifications of firm and key personnel", key: "qualifications"},
  {name: "Financing approach", key: "financingApproach"},
  {name: "Legal issues / conflicts of interest", key: "legalIssues"},
]
const mergeEvaluation = (evaluations) => {
  const customEval = CONST.rfpMemberEvaluations.rfpEvaluationCategories
  if(evaluations && Array.isArray(evaluations) && evaluations.length) {
    customEval.forEach(evalCat => {
      const evalIndex = evaluations.findIndex(cat => cat.categoryName === evalCat.categoryName)
      if (evalIndex === -1) {
        evaluations.push({
          categoryName: evalCat.categoryName,
          categoryItems: evalCat.categoryItems
        })
      } else {
        evalCat.categoryItems.forEach(ev => {
          const isExists = evaluations[evalIndex].categoryItems.findIndex(cat => cat.evalItem === ev.evalItem)
          if(isExists===-1) {
            evaluations[evalIndex].categoryItems.push(ev)
          }
        })
      }
    })
    return evaluations
  }
  return CONST.rfpMemberEvaluations
}

class Evaluate1 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rfpParticipantFirmId: "",
      rfpEvaluationCommitteeMemberId: props.user.userId,
      activeTab: props.user.userId,
      rfpSelEvalCommittee: [],
      rfpParticipantDist: [],
      documentsList: [],
      tempFirmId: null,
      rfpEvaluations: CONST.rfpMemberEvaluations,
      unAuthEvaluations: CONST.rfpMemberEvaluations,
      tempRfpEvaluations: cloneDeep(CONST.rfpMemberEvaluations),
      evaluateId: "",
      authEvaluateId: "",
      dropDown: {
        priority: []
      },
      docId: "",
      loading: true,
      isSaveDisabled: false
    }
  }

  async componentDidMount() {
    const {transactionId} = this.props
    let result = await getPicklistByPicklistName(["LKUPHIGHMEDLOW"])
    result = (result.length && result[1]) || {}
    if (transactionId) {
      const url =`?transId=${transactionId}`
      fetchRfpContacts(url, (res)=> {
        this.setState({
          ...res,
          dropDown: {
            ...this.state.dropDown,
            priority: result.LKUPHIGHMEDLOW,
          },
          loading: false,
        })
      })
    }
  }

  onPick = (e, isFromTab) => {
    const {transactionId, user} = this.props
    const {rfpEvaluationCommitteeMemberId, rfpParticipantFirmId, rfpEvaluations, activeTab} = this.state
    const isAuth = user.userId === activeTab
    if (transactionId && rfpEvaluationCommitteeMemberId && rfpParticipantFirmId) {
      const url =`?transId=${transactionId}&memberId=${activeTab}&firmId=${rfpParticipantFirmId}`

      const manageState = {}
      if (isFromTab) {
        rfpEvaluations.rfpParticipantFirmId =rfpParticipantFirmId
        manageState.tempFirmId = rfpParticipantFirmId
      } else {
        manageState.tempFirmId = null
        manageState.tempRfpEvaluationCategories = null
      }
      manageState.isCallEvaluate = true
      this.setState(manageState,() => {
        fetchRfpEvaluations1(url, (res)=> {
          const documentsList = (res && res.rfpEvaluationDocuments) || []
          if(res && res.rfpEvaluationCategories && Array.isArray(res.rfpEvaluationCategories) && res.rfpEvaluationCategories.length) {
            res.rfpEvaluationCategories = mergeEvaluation(res.rfpEvaluationCategories)
            if(isAuth) {
              this.setState({
                evaluateId: res._id,
                authEvaluateId: res._id,
                rfpEvaluations: cloneDeep(res),
                tempRfpEvaluations: cloneDeep(res),
                documentsList,
                tempDocumentsList: documentsList,
                isCallEvaluate: false
              })
            }else {
              this.setState({
                evaluateId: res._id,
                documentsList,
                unAuthEvaluations: cloneDeep(res),
                isCallEvaluate: false
              })
            }
          } else {
            if(isAuth) {
              this.setState({
                authEvaluateId: res._id,
                rfpEvaluations: CONST.rfpMemberEvaluations,
                tempRfpEvaluations: CONST.rfpMemberEvaluations,
                documentsList,
                tempDocumentsList: documentsList,
                isCallEvaluate: false
              })
            }else {
              this.setState({
                unAuthEvaluations: CONST.rfpMemberEvaluations,
                documentsList,
                isCallEvaluate: false
              })
            }
          }
        })
      })

    }
  }

  onChange = (e) => {
    if (e.target.name === "evalNotes") {
      const rfpEvaluations = cloneDeep(this.state.rfpEvaluations)
      rfpEvaluations[e.target.name] = e.target.value
      this.setState({rfpEvaluations})
      return
    }
    const statConfig = {
      [e.target.name]: e.target.value,
    }
    if (e.target.name === "rfpParticipantFirmId") {
      const {rfpEvaluations,rfpSelEvalCommittee} = this.state
      const {user} = this.props
      rfpEvaluations.rfpParticipantFirmName = (e.target.firm && e.target.firm.firmName) || ""
      rfpEvaluations.rfpParticipantMSRBType = (e.target.firm && e.target.firm.msrbType) || ""
      statConfig.tempRfpEvaluationCategories = null
      statConfig.tempFirmId = null
      const checkActiveTab = rfpSelEvalCommittee.find(r => r.rfpSelEvalContactId === user.userId)
      if(!checkActiveTab){
        statConfig.activeTab = (rfpSelEvalCommittee && rfpSelEvalCommittee.length && rfpSelEvalCommittee[0].rfpSelEvalContactId) || ""
      }
      return this.setState(statConfig, () => this.onPick())
    }
    this.setState(statConfig)
  }

  onAdd = (category) => {
    const { rfpEvaluations } = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    const isDuplicate = rfpEvaluations.rfpEvaluationCategories.find(e => e.categoryName === category).categoryItems.find(e => e.evalItem === this.state[`${category}Input`])
    if(isDuplicate) {
      return this.setState({
        [category]: `${this.state[`${category}Input`]} is Already Exists`
      })
    }

    rfpEvaluations.rfpEvaluationCategories.forEach(evals => {
      if(evals.categoryName === category) {
        evals.categoryItems.push({
          evalItem: this.state[`${category}Input`],
          isEditable: true,
        })
      }
    })

    this.props.addAuditLog({userName, log: `Add new list item with name ${this.state[`${category}Input`]} under ${category}`, date: new Date()})
    this.setState({
      [`${category}Input`]: "",
      rfpEvaluations,
      [category]: ""
    })
  }

  onRating = (rate, category, listName, priority) => {
    const {rfpEvaluationCommitteeMemberId } = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    const rfpEvaluations = cloneDeep(this.state.rfpEvaluations)
    const {user} = this.props

    if( user.userId !== rfpEvaluationCommitteeMemberId) {
      return
    }

    rfpEvaluations.rfpEvaluationCategories.forEach(evalexist => {
      if(evalexist.categoryName === category) {
        evalexist.categoryItems.map((pos) => {
          if(pos.evalItem === listName) {
            if (rate !== null) {
              pos.evalRating = rate
            } else {
              pos.evalPriority = priority
            }
          }
          return pos
        })
      }
    })

    if (rate !== null) {
      this.props.addAuditLog({userName, log: `Transaction:${this.props.transactionId}, gave rating ${rate} for ${listName} `, date: new Date(), category})
    } else {
      this.props.addAuditLog({userName, log: `Transaction ${this.props.transactionId}, gave priority ${priority} for ${listName}`, date: new Date(), category})
    }

    this.setState({
      rfpEvaluations,
      tempFirmId: this.state.rfpParticipantFirmId,
    })
  }

  onReset = (category) => {
    const {rfpEvaluations, tempRfpEvaluations} = this.state
    const auditLogs = this.props.auditLogs.filter(x=>x.category !== category)
    const customEval = Object.assign({}, {...tempRfpEvaluations})

    rfpEvaluations.rfpEvaluationCategories.forEach(evals => {
      if(evals.categoryName === category) {
        evals.categoryItems = customEval.rfpEvaluationCategories.find(e => e.categoryName === category).categoryItems
      }
    })

    // rfpEvaluations.rfpEvaluationCategories[category] = customEval.rfpEvaluationCategories[category]
    this.props.updateAuditLog(auditLogs)
    this.setState({
      rfpEvaluations,
    })
  }

  onSubmit = () => {
    const {transactionId, user, docId } = this.props
    const {rfpParticipantFirmId, tempRfpEvaluations, activeTab} = this.state
    const isAuth = this.props.user.userId === activeTab
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    let inValid = false
    const rfpEvaluations = cloneDeep(this.state.rfpEvaluations)
    if (!transactionId) {
      toast("There is no transaction to be saved",{ autoClose: CONST.ToastTimeout, success: toast.TYPE.ERROR, })
      return
    }
    if (!user.userId) {
      toast("There is no commity to be saved",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      return
    }
    if (!rfpParticipantFirmId) {
      toast("There is no firm to be saved",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      return
    }

    rfpEvaluations.rfpEvaluationCategories.forEach(evalCat => {
      inValid = evalCat.categoryItems.find(ev => ev.isEditable && ev.evalRating && ev.evalPriority)
    })

    if(inValid) {
      toast("Please rate new items added",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      return
    }

    rfpEvaluations.rfpEvaluationCategories.forEach(evalCat => {
      evalCat.categoryItems = evalCat.categoryItems.filter(ev => ev.hasOwnProperty("evalRating") || ev.hasOwnProperty("evalPriority"))
    })

    rfpEvaluations.rfpParticipantFirmId = rfpParticipantFirmId
    rfpEvaluations.rfpEvaluationCommitteeMemberId = this.props.user.userId
    rfpEvaluations.rfpCommitteeMemberFirstName =  this.props.user.userFirstName || ""
    rfpEvaluations.rfpCommitteeMemberLastName = this.props.user.userLastName || ""
    rfpEvaluations.rfpCommitteeMemberEmailId = this.props.userEmail
    if (tempRfpEvaluations.evalNotes !== rfpEvaluations.evalNotes) {
      this.props.addAuditLog({userName, log: `Eval notes changed from' ${tempRfpEvaluations.evalNotes} to ${rfpEvaluations.evalNotes}`, date: new Date()})
    }

    this.setState({
      isSaveDisabled: true
    },async () => {
      const res = await postRfpTransaction({transactionId, rfpMemberEvaluations: rfpEvaluations})
      if(res && res.status === 200){
        this.setState({
          tempFirmId: null,
          evaluateId: (res.data && res.data._id) || "",
          authEvaluateId: isAuth ? (res.data && res.data._id) || "" : this.state.authEvaluateId,
          tempRfpEvaluationCategories: {},
          tempRfpEvaluations: res && res.data || {},
          isSaveDisabled: false
        }, async () => {
          const { tempRfpEvaluations, unAuthEvaluations } = this.state
          tempRfpEvaluations.rfpEvaluationCategories.forEach(t => {
            unAuthEvaluations.rfpEvaluationCategories.forEach(u => {
              if(u.categoryName === t.categoryName){
                const evalLists = t.categoryItems.map(c => c.evalItem) || []
                u.categoryItems.forEach(tempCate => {
                  if(evalLists.indexOf(tempCate.evalItem) === -1){
                    t.categoryItems.push({evalItem: tempCate.evalItem})
                  }
                })
              }
            })
          })
          this.setState({tempRfpEvaluations})
          await sendEmailAlert(this.props.emailPayload)
        })
        this.props.submitAuditLogs(transactionId)
        toast("Evaluation saved!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
      }else {
        toast("Something went wrong!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
      }
    })

  }

  onTab = (activeTab) => {
    const isAuth = this.props.user.userId === activeTab
    this.setState({
      activeTab,
      rfpEvaluationCommitteeMemberId: activeTab,
    }, ()=> {
      if(!isAuth) {
        this.onPick(null, true)
      }else {
        this.setState({
          documentsList: this.state.tempDocumentsList
        })
      }
    })
  }

  actionButtons = (key, isDisabled, canEditTran) => {
    if(!canEditTran) return
    return (
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small"
            disabled={!(this.state[`${key}Input`] && this.state[`${key}Input`].trim() && this.state.rfpParticipantFirmId)}
            onClick={() => this.onAdd(key)}>Add
          </button>
        </div>
        <div className="control">
          <button className="button is-light is-small" onClick={() => this.onReset(key)}
            disabled={isDisabled || false}>Reset
          </button>
        </div>
      </div>
    )
  }

  onDeleteDoc = async (documentId, callback) => {
    const {authEvaluateId} = this.state
    const res = await pullRfpEvaluationDocument(`?tranId=${this.props.nav2}&evaluateId=${authEvaluateId}&docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(this.props.nav2)
      this.setState({
        documentsList: (res.data && res.data.rfpEvaluationDocuments) || [],
        tempDocumentsList: (res.data && res.data.rfpEvaluationDocuments) || [],
      },() => {
        callback({
          status: true,
          documentsList: (res.data && res.data.rfpEvaluationDocuments) || [],
        })
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onDocSave = async (docs, callback) => {
    const {authEvaluateId} = this.state
    const res = await putRfpEvaluationDocuments(`?type=rfpEvaluationDocuments&evaluateId=${authEvaluateId}`, docs)
    if (res && res.status === 200) {
      toast("Document has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(this.props.nav2)
      this.setState({
        documentsList: (res.data && res.data.rfpEvaluationDocuments) || [],
        tempDocumentsList: (res.data && res.data.rfpEvaluationDocuments) || [],
      },() => {
        callback({
          status: true,
          documentsList: (res.data && res.data.rfpEvaluationDocuments) || [],
        })
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  /*  onStatusChange = async (e, doc, callback) => {
    const res = await putRfpEvaluationDocuments(this.props.nav2, "RFP", {_id: doc._id, [e.target.name]: e.target.value})

    if (res && res.status === 200) {
      toast("Document status has been updated!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
      this.props.submitAuditLogs(this.props.nav2)
      callback({
        status: true,
      })
    } else {
      toast("Something went wrong!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
      callback({
        status: false
      })
    }
  } */

  render() {
    const {user, transaction, tranAction} = this.props
    const {loading, isCallEvaluate, evaluateId, authEvaluateId, activeTab, rfpEvaluations, documentsList, dropDown, unAuthEvaluations, rfpParticipantDist, rfpSelEvalCommittee, rfpParticipantFirmId, isSaveDisabled} = this.state
    const canEditTran = (tranAction && (tranAction.canManageEdit && tranAction.canTranStatusEditDoc)) || false
    const isAuth = ((user.userId || "") === activeTab)
    const tags = {
      clientId: transaction.rfpTranIssuerId,
      clientName: transaction.rfpTranIssuerFirmName,
      tenantId: transaction.rfpTranClientId,
      tenantName: transaction.rfpTranClientFirmName,
      contextName:transaction.rfpTranIssueName ? transaction.rfpTranIssueName : transaction.rfpTranProjectDescription
      // rfpTranIssueName: String,
      // rfpTranProjectDescription: String,

    }
    const rfpSelEvalCommitteeTabs = rfpSelEvalCommittee.map(comMember => ({
      title: <span>{comMember.rfpSelEvalContactName}</span>,
      value: comMember.rfpSelEvalContactId,
    }))
    const suppliers = _.uniqBy(rfpParticipantDist || [], "rfpParticipantFirmId")

    const evaluations = isAuth ? rfpEvaluations : unAuthEvaluations

    if(loading) {
      return <Loader/>
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <span className="label">Respondent / Supplier being evaluated</span>
            <InputButton onChange={this.onChange} suppliers={suppliers} value={rfpParticipantFirmId} onPick={this.onPick}/>
          </div>
        </div>
        {activeTab && rfpParticipantFirmId && <Tabs activeTab={activeTab} tabs={rfpSelEvalCommitteeTabs} type="rfp" onTab={this.onTab}/>}
        {activeTab && rfpParticipantFirmId && isCallEvaluate ? <Loader/> : null}
        {activeTab && rfpParticipantFirmId && !isCallEvaluate &&
        <Accordion
          key={activeTab}
          multiple boxHidden
          activeItem={[0]}
          render={({activeAccordions, onAccordion}) =>
            evaluations && Array.isArray(evaluations.rfpEvaluationCategories) && evaluations.rfpEvaluationCategories.map((evals, index) => {
              const isActive = activeAccordions.includes(index)
              const items = evals.categoryItems || []
              const title = boxs.find(l => l.key === evals.categoryName)
              return (
                <RatingCategory1 key={evals.categoryName + this.state.activeTab} error={this.state[evals.categoryName]} canEditTran={canEditTran} isActive={isActive} items={items} isAuth={isAuth}
                  actionButtons={isAuth ? this.actionButtons(evals.categoryName, isSaveDisabled, canEditTran) : ""} cols={cols} dropDown={dropDown}
                  category={evals.categoryName} title={title.name} onRating={this.onRating} onCategoryInputChange={this.onChange}
                  onAccordion={() => onAccordion(index)}
                  categoryInputValue={this.state[`${evals.categoryName}Input`]}/>
              )
            })}
        />
        }
        <hr/>
        {
          evaluateId && authEvaluateId && rfpParticipantFirmId ?
            <TranDocuments {...this.props} onSave={this.onDocSave} tranId={this.props.nav2} title="Documents"
                           pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickAction="LKUPDOCACTION"
                           category="rfpEvaluationDocuments" documents={documentsList} contextType={ContextType.rfp}
                           tags={tags} onStatusChange={this.onStatusChange} onDeleteDoc={this.onDeleteDoc}
                           tableStyle={{fontSize: "smaller"}} isDisabled={!isAuth || !canEditTran || false}/> :
            !isCallEvaluate  && isAuth && rfpParticipantFirmId &&
            <p className="multiExpLbl">Document can be upload after saving evaluation</p>
        }
        {activeTab && rfpParticipantFirmId && !isCallEvaluate &&
          <div>
            <div className="columns">
              <div className="column">
                <article className="media">
                  <div className="media-content">
                    <div className="field">
                      <p className="control">
                        <textarea className="textarea" name='evalNotes' disabled={!isAuth || !canEditTran} value={evaluations.evalNotes} onChange={this.onChange}
                                  placeholder="Evaluation notes or conclusion..."/>
                      </p>
                    </div>
                    {
                      isAuth &&
                      <nav className="level">
                        { canEditTran ?
                          <div className="level-left">
                            <div className="level-right">
                              <div className="level-item">
                                <button onClick={this.onSubmit} className="button is-info" role="button" disabled={isSaveDisabled || !rfpParticipantFirmId}>Submit</button>
                              </div>
                            </div>
                          </div>
                          : null }
                      </nav>
                    }
                  </div>
                </article>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Evaluate1)

export default connect(mapStateToProps, {postRfpTransaction})(WrappedComponent)
