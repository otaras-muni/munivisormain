import React from "react"
import moment from "moment/moment"
import DocLink from "../../../docs/DocLink"
import "../../../../../public/images/user-avatar.jpg"

const PostBox = ({post, onFileAction, user}) => (
  <article className="media">
    <figure className="media-left">
      <p className="image is-64x64">
        <img src="/images/user-avatar.jpg"/>
      </p>
    </figure>
    <div className="media-content">
      <div className="content">
        <p>
          <strong className={user.userId === post.postContactId ? "has-text-success" : ""}>
            {`${post.postUserFirstName} ${post.postUserLastName}`}
            {/*<div className="tags has-addons" style={{display: "inline-block",marginLeft: 10}}>*/}
                {/*<span className={`tag ${post.postVisibilityFlag ? "is-link" : "is-success"}`}>*/}
                {/*{post.postVisibilityFlag ? "Private" : "Public"}*/}
              {/*</span> */}
            {/*</div>*/}
          </strong>
          <small className="has-text-grey is-size-7">
            {/* <a>Reply</a> · */} {post.postDate ? moment(post.postDate).format("DD-MMM-YYYY hh:mm A") : ""}
          </small>
        </p>
      </div>
      <small>
        <span style={{ display: "inline-block"}} className="has-text-justified">{post.postDetails}</span>
        {post.postDocument &&
        <a style={{ display: "inline-block"}}>
          { post.postDocument ? <div className="field is-grouped">
            <DocLink docId={post.postDocument} />
          </div> : null }
        </a>
        }
      </small>
      {/* <article className="media">
        <figure className="media-left">
          <p className="image is-48x48">
            <img src="https://bulma.io/images/placeholders/96x96.png"/>
          </p>
        </figure>
        <div className="media-content">
          <div className="content">
            <p>
              <strong>Kayli Eunice </strong>
              <br/> Test Reply
              <br/>
              <small>
                <a>Reply</a> · 2 hrs
              </small>&nbsp;&nbsp;
              <i className="fas fa-paperclip is-small"/>
            </p>
          </div>
        </div>
      </article> */}
    </div>
  </article>
)

export default PostBox
