import React from "react"

class Discuss extends React.Component {
  constructor(props){
    super(props)
  }

  render() {
    return(
      <div className="columns">
        <div className="column">
          <div className="chatWrapper">
            <iframe src="https://rocket.chat/" allowFullScreen="" />
          </div>
        </div>
      </div>
    )
  }

}

export default Discuss