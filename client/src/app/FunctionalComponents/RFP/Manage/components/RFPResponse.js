import React from "react"
import moment from "moment"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import { DropdownList } from "react-widgets"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import { addRfpResponse } from "../../../../StateManagement/actions/CreateTransaction"
import Loader from "../../../../GlobalComponents/Loader"
import {
  fetchTrnsactionResponse,
  fetchDocDetails,
  getS3FileGetURL,
  sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import { getPicklistByPicklistName, checkInvalidFiles } from "GlobalUtils/helpers"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import DocLink from "../../../docs/DocLink"
import KeyInformation from "../components/KeyInformation"
import InterestRate from "../components/InterestRate"
import { keyInformationValidate } from "./Validation/ResponseValidate"
import ResponseInfoModal from "./ResponseInfoModal"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import DocModalDetails from "../../../docs/DocModalDetails";
import SingleFileDocUpload from "../../../../GlobalComponents/SingleFileDocUpload";
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear"

const cols = [
  { name: "Action" },
  { name: "Firm Name" },
  { name: "User" },
  { name: "LUD" },
  { name: "Comments" },
  { name: "Files" },
  { name: "Additional Info" },
  { name: "Edit" }
]

const viewCols = [
  { name: "Action" },
  { name: "Firm Name" },
  { name: "User" },
  { name: "LUD" },
  { name: "Comments" },
  { name: "Files" },
  { name: "Additional Info" }
]

const colsInterest = [
  { name: "Rate Type<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
  { name: "Loan Amount (from)" },
  { name: "Loan Amount (to)" },
  { name: "Maturities" },
  { name: "Rate" },
  { name: "Action" }
]

const comments = [
  {
    firmName: "Stifel, Co.",
    user: "Christopher Hopkins",
    lud: "3.30.2018 00:13:56 UTC",
    comment: "Thanks for the opportunity."
  }
]

class RFPResponse extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cols,
      viewCols,
      bucketName: CONST.bucketName,
      keyInformation: CONST.keyInformation,
      keyInterest: [CONST.keyInterest],
      docId: "",
      filename: "",
      contacts: [],
      firms: [],
      submissionComment: "",
      rfpSupplierResponses: [],
      errors: {},
      doc: {},
      errorMessages: {},
      activeItem: [],
      showModal: false,
      isSaveDisabled: false,
      docFile: false,
      loading: true,
      keyInfoUser: [],
      supplierRes: [],
      loadDocuments: [],
      filterBy: "",
      submitFor: "",
      submitName: "",
      responseId: "",
      responseInfo: {},
      modalVisible: false,
      visibleKeyInfo: ["Bank Loans / Lease"],
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
        sendDocEmailLinks: true
      }
    }
  }

  async componentWillMount() {
    const { transactionId, rfpParticipants, rfpTranSubType } = this.props
    const { visibleKeyInfo } = this.state
    const keyInfoUser = rfpParticipants.map(id => id.rfpParticipantContactId)

    const out = Array.from(Array(30), (_, x) => x + 1)
    const picResult = await getPicklistByPicklistName([
      "LKUPFLOATRATETYPE",
      "LKUPPAYMENTTYPE",
      "LKUPFLOATRATETYPE",
      "LKUPBBGSECTORCODE",
      "LKUPPAYMENTFREQ",
      "LKUPSECTYPEGENERAL",
      "LKUPTOBECONFIRMED",
      "LKUPSTATE"
    ])
    const result = (picResult.length && picResult[1]) || {}
    this.setState({
      keyInformation: CONST.keyInformation,
      keyInfoUser,
      dropDown: {
        ...this.state.dropDown,
        tenor: out,
        averageLife: out,
        paymentType: result.LKUPPAYMENTTYPE || [],
        floatingRateType: result.LKUPFLOATRATETYPE || [],
        sectorCode: result.LKUPBBGSECTORCODE || [],
        creditType: result.LKUPSECTYPEGENERAL || [],
        provisionType: result.LKUPTOBECONFIRMED || [
          "Pre-Payment",
          "Make-whole"
        ],
        state: result.LKUPSTATE || [],
        rateType: [
          "Non-Bank Qualified - Secured by Assessments Only",
          "Non Bank Qualified - Secured by Assessments and Covenants for Non Ad valorem revenues",
          "Bank Qualified - Secured by Assessments Only"
        ]
      }
    })

    if (transactionId) {
      this.setState({
        cols:
          visibleKeyInfo.indexOf(rfpTranSubType) === -1
            ? this.state.cols.splice(6, 2) : cols,
        viewCols:
          visibleKeyInfo.indexOf(rfpTranSubType) === -1
            ? this.state.viewCols.splice(6, 1) : viewCols
      })
      this.getSupplierResponses(transactionId)
    }
  }

  getSupplierResponses = transId => {
    const { user, rfpParticipants } = this.props
    fetchTrnsactionResponse(transId, transaction => {
      if (transaction && transaction._id) {
        const supplierRes =
          this.state.keyInfoUser &&
          this.state.keyInfoUser.indexOf(user.userId) !== -1
            ? transaction.rfpSupplierResponses.filter(
                supplier => supplier.PartContactId === user.userId
              )
            : transaction.rfpSupplierResponses

        this.setState(
          {
            transId,
            rfpSupplierResponses: transaction.rfpSupplierResponses,
            contacts: transaction.contacts,
            firms: transaction.firms,
            loading: false,
            supplierRes
          },
          () => {
            if (rfpParticipants && rfpParticipants.length === 1) {
              this.onFilter({
                ...rfpParticipants[0],
                id: rfpParticipants[0].rfpParticipantFirmId
              })
            }
          }
        )
      } else {
        this.setState({
          loading: false
        })
      }
    })
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onSubmitForChange = e => {
    const {rfpParticipantContactId} = e
    this.setState({
      submitFor: rfpParticipantContactId || "",
      submitName: `${e.firstName || ""} ${e.lastName || ""}` || "",
    })
  }

  onSubmit = () => {
    const {
      visibleKeyInfo,
      responseId,
      keyInfoUser,
      supplierRes,
      transId,
      filename,
      submissionComment,
      docId,
      keyInformation,
      keyInterest,
      submitFor
    } = this.state
    const { user, rfpTranSubType, userEmail, tranAction } = this.props
    const userName = (user && user.userFirstName) || ""
    let payload = {}
    let rfpSupplierResponses = {}

    if (submitFor) {
      const submitForDetails = this.props.rfpParticipants.find(participant => participant.rfpParticipantContactId === submitFor) || {}
      rfpSupplierResponses = {
        PartContactId: submitForDetails && submitForDetails.rfpParticipantContactId || "",
        PartUserFirstName: submitForDetails && submitForDetails.rfpParticipantRealFirstName || "",
        PartUserLastName: submitForDetails && submitForDetails.rfpParticipantRealLastName || "",
        PartFirmId: submitForDetails && submitForDetails.rfpParticipantFirmId || "",
        PartFirmName: submitForDetails && submitForDetails.rfpParticipantFirmName || "",
        PartUserEmailId: submitForDetails && submitForDetails.rfpParticipantContactEmail || "",
        DatePosted: new Date(),
        LastUpdatedDate: new Date(),
        // DocId: docId
      }
    } else {
      rfpSupplierResponses = {
        PartContactId: user && user.userId,
        PartUserFirstName: user && user.userFirstName,
        PartUserLastName: user && user.userLastName,
        PartFirmId: user && user.entityId,
        PartFirmName: user && user.firmName,
        PartUserEmailId: userEmail,
        DatePosted: new Date(),
        LastUpdatedDate: new Date(),
        // DocId: docId
      }
    }

    if (!(visibleKeyInfo && keyInfoUser && visibleKeyInfo.indexOf(rfpTranSubType) !== -1 && (keyInfoUser.indexOf(user.userId) !== -1 || (user && user.relationshipToTenant === "Self")) ||
      (visibleKeyInfo && visibleKeyInfo.indexOf(rfpTranSubType) !== -1 && tranAction && tranAction.canEditTran))) {
      if (!filename && !responseId) {
        let commentError = ""
        if(rfpTranSubType !== "Bank Loans / Lease"){
          commentError = "Required" || ""
        }
        return this.setState({
          errors: {
            docId: "Required",
            submissionComment: commentError || ""
          }
        })
      }
      this.props.addAuditLog({
        userName,
        log: `Supplier Response ${submissionComment} added`,
        date: new Date(),
        category: "Supplier Response"
      })
      rfpSupplierResponses.Comment = submissionComment
    } else {
      payload = { keyInformation, keyInterest }
      if (responseId) {
        payload._id = responseId
      }
      console.log("=========payload=========", payload)
      const errors = keyInformationValidate(payload)

      if (errors && errors.error) {
        const errorMessages = {}
        console.log("=======>", errors.error.details)
        errors.error.details.map(err => {
          //eslint-disable-line
          if (errorMessages.hasOwnProperty([err.path[0]])) {
            //eslint-disable-line
            if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) {
              //eslint-disable-line
              errorMessages[err.path[0]][err.path[1]][err.path[2]] = "Required" // err.message
            } else if (err.path[2]) {
              errorMessages[err.path[0]][err.path[1]] = {
                [err.path[2]]: "Required" // err.message
              }
            } else {
              errorMessages[err.path[0]][err.path[1]] = "Required" // err.message
            }
          } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
            errorMessages[err.path[0]] = {
              [err.path[1]]: {
                [err.path[2]]: "Required" // err.message
              }
            }
          } else {
            errorMessages[err.path[0]] = {
              [err.path[1]]: "Required" // err.message
            }
          }
        })
        console.log(errorMessages)
        return this.setState({ errorMessages, activeItem: [0, 1] })
      }

      if (!filename && !responseId) {
        let commentError = ""
        if(rfpTranSubType !== "Bank Loans / Lease"){
          commentError = "Required" || ""
        }
        return this.setState({
          errors: {
            docId: "Required",
            submissionComment: commentError || ""
          }
        })
      }
      this.props.addAuditLog({
        userName,
        log: `Supplier Response ${submissionComment} added`,
        date: new Date(),
        category: "Supplier Response"
      })

      if (responseId) {
        const supRes = supplierRes.find(res => res._id === responseId)
        rfpSupplierResponses = {
          ...supRes,
          keyInformation,
          keyInterest
        }
      } else {
        rfpSupplierResponses.keyInformation = keyInformation
        rfpSupplierResponses.keyInterest = keyInterest
        submissionComment
          ? (rfpSupplierResponses.Comment = submissionComment)
          : ""
      }
    }

    this.setState({
      isSaveDisabled: true,
      rfpSupplierResponses,
      docFile: true,
    })
  }

  onUploadSuccess = (filename, docId) => {
    const { transId, responseId, keyInterest, keyInformation, supplierRes } = this.state
    let rfpSupplierResponses = this.state.rfpSupplierResponses
    if (responseId) {
      const supRes = supplierRes.find(res => res._id === responseId)
      rfpSupplierResponses = {
        ...supRes,
        keyInformation,
        keyInterest
      }
    }
    if(!responseId){
      rfpSupplierResponses.DocId = docId
    }
    this.setState({
      isSaveDisabled: true
    }, () => {
      addRfpResponse(transId, { rfpSupplierResponses }, res => {
        //eslint-disable-line
        if (res) {
          toast("Response Added successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
          this.props.submitAuditLogs(transId)
          this.setState(
            {
              errors: {},
              docId: "",
              filename: "",
              submissionComment: "",
              isSaveDisabled: false,
              docFile: false,
              responseId: "",
              keyInformation: CONST.keyInformation,
              keyInterest: [CONST.keyInterest],
              errorMessages: {}
            },
            async () => {
              this.getSupplierResponses(transId)
            }
          )
        }
      })
    }
    )
  }

  getBidBucket = (filename, docId) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `Supplier Response ${filename} Uploaded`,
      date: new Date(),
      category: "Supplier Response"
    })
    this.setState({
      docId,
      filename
    })
  }

  onFileAction = (e, docId, index) => {
    const { name, value } = e.target
    const {rfpSupplierResponses} = this.state
    if (value === "email") {
      const docIds = [docId]
      const comment = rfpSupplierResponses[index] && rfpSupplierResponses[index].Comment
      this.setState({
        docIds,
        comment,
        modalState: true
      })
    } else if (value === "version") {
      fetchDocDetails(docId).then(res => {
        this.handleDocDetails(res)
      })
    }
    this.setState({
      [name]: value
    })
  }

  onConfirmationSave = () => {
    const {email, docIds, comment} = this.state
    const {transactionId, type} = this.props
    const emailPayload = {
      tranId: transactionId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        docIds: email.sendDocEmailLinks ? docIds : [],
        comment,
        ...email
      }
    }
    this.setState({
      modalState: false
    }, async () => {
      await sendEmailAlert(emailPayload)
    })
  }

  downloadFile = () => {
    this.downloadAnchor.click()
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, res => {
      this.setState(
        {
          ...res
        },
        callback.bind(this, bucketName, fileName)
      )
    })
  }

  handleDocDetails = (res) => {
    this.setState({
      showModal: !this.state.showModal,
      action: "",
      doc: res || {}
    })
  }

  onChangeItem = (item, category) => {
    this.setState({
      [category]: { ...item }
    })
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `${change} from details`,
      date: new Date(),
      key: category
    })
  }

  onReset = key => {
    this.setState({
      [key]: key === "keyInterest" ? [CONST.keyInterest] : []
    })
  }

  onAdd = key => {
    this.setState({
      [key]: [
        ...this.state[key],
        key === "keyInterest" ? CONST.keyInterest : {}
      ]
    })
  }

  actionButtons = (key, isDisabled, canEditTran) => {
    //eslint-disable-line
    if (!canEditTran) return
    return (
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
            disabled={isDisabled}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onReset(key)}
            disabled={isDisabled}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  onChangeInterest = (item, index, category) => {
    const items = this.state[category]
    items.splice(index, 1, item)
    this.setState({
      [category]: items
    })
  }

  onRemove = (key, i) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    const items = this.state[key]
    if (Array.isArray(items)) {
      items.splice(i, 1)
      this.props.addAuditLog({
        userName,
        log: `Item removed in ${
          key === "keyInterest" ? "RFP Key Interest" : key
        }`,
        date: new Date(),
        key
      })
      this.setState({
        [key]: items
      })
    }
  }

  onFilter = participant => {
    const { keyInfoUser, rfpSupplierResponses } = this.state
    const { user } = this.props

    const supplierRes =
      participant === "reset"
        ? rfpSupplierResponses
        : rfpSupplierResponses.filter(
          supplier => supplier.PartFirmId === participant.id
        ) || []
    this.setState({
      filterBy:
        participant === "reset"
          ? ""
          : (participant && participant.rfpParticipantFirmId) || "",
      supplierRes
    })
  }

  closeInfoModal = () => {
    this.setState({ modalVisible: false })
  }

  handleInfoModal = responseInfo => {
    this.setState({ responseInfo, modalVisible: true })
  }

  onCancel = () => {
    this.setState({
      responseId: "",
      filename: "",
      submissionComment: "",
      keyInformation: CONST.keyInformation,
      keyInterest: [CONST.keyInterest],
      errorMessages: {},
      errors: {}
    })
  }

  onEdit = responseInfo => {
    this.setState({
      responseId: responseInfo._id || "",
      keyInformation: responseInfo.keyInformation || {},
      keyInterest: responseInfo.keyInterest || []
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state,
      action: ""
    })
  }

  onFileSetInState = (e) => {
    const invalidFile = checkInvalidFiles(e, "single")
    if(invalidFile){
      this.setState({
        errors: {
          ...this.state.errors,
          docId: invalidFile || ""
        }

      })
      return
    }
    this.setState({
      filename: e.name || ""
    })
  }

  render() {
    const {
      submissionComment,
      loading,
      responseId,
      responseInfo,
      modalVisible,
      supplierRes,
      filterBy,
      visibleKeyInfo,
      keyInfoUser,
      submitFor,
      submitName,
      bucketName,
      contacts,
      firms,
      docId,
      isSaveDisabled,
      errorMessages,
      dropDown,
      activeItem,
      keyInformation,
      keyInterest,
      rfpSupplierResponses,
      docFile,
      errors = {},
      filename,
      doc,
      email,
      modalState
    } = this.state
    const { rfpTranSubType, user, tranAction, transaction, onParticipantsRefresh } = this.props
    const canEditTran =
      (tranAction && tranAction.canManageEdit && tranAction.canTranStatusEditDoc) || false
    const tags = {
      userName: this.props.user.userFirstName,
      userId: this.props.user.userId,
      entityId: this.props.user.entityId
    }
    const errorsKeyInfo = (errorMessages && errorMessages.keyInformation) || {}
    const keyInfoUserDetails =
      this.props.rfpParticipants.find(
        participant => participant.rfpParticipantContactId === user.userId
      ) || {}
    const rfpParticipants = []
    const data = []
    this.props.rfpParticipants.forEach(participant => {
      data.push({
        ...participant,
        name: participant.rfpParticipantFirmName,
        id: participant.rfpParticipantFirmId,
        userId: participant.rfpParticipantContactId,
        fullName: participant.rfpParticipantContactName,
        firstName: participant.rfpParticipantRealFirstName,
        lastName: participant.rfpParticipantRealLastName
      })

      const inParticipant = rfpParticipants.find(part => part.id === participant.rfpParticipantFirmId)
      if (!inParticipant) {
        rfpParticipants.push({
          ...participant,
          name: participant.rfpParticipantFirmName,
          id: participant.rfpParticipantFirmId
        })
      }
    })
    // const supplierRes = (keyInfoUser && keyInfoUser.indexOf(user.userId) !== -1) ? rfpSupplierResponses.filter(supplier => supplier.PartContactId === user.userId) : rfpSupplierResponses

    if (loading) {
      return <Loader />
    }

    return (
      <div className="accordions">
        <SendEmailModal modalState={modalState} email={email} documentFlag onModalChange={this.onModalChange} participants={this.props.participants} onParticipantsRefresh={onParticipantsRefresh} onSave={this.onConfirmationSave} />
        <ResponseInfoModal
          response={responseInfo}
          modalVisible={modalVisible}
          toggleModal={this.closeInfoModal}
        />
        {(canEditTran && visibleKeyInfo && keyInfoUser && (keyInfoUser.indexOf(user.userId)!== -1 || (user && user.relationshipToTenant === "Self"))) ? (
          <div className="columns">
            <div className="column is-one-quarter">
              <p className="multiExpLbl ">Respondent / Supplier</p>
              {
                keyInfoUserDetails.rfpParticipantFirmName ?
                  <p className="multiExpTblVal">
                    {keyInfoUserDetails.rfpParticipantFirmName || ""}
                  </p> :
                  <DropDownListClear
                    filter
                    data={data || []}
                    value={submitName || ""}
                    textField="fullName"
                    groupBy={(row) =>row.name}
                    onChange={this.onSubmitForChange}
                    isHideButton={submitFor && canEditTran}
                    onClear={() => {this.onSubmitForChange({})}}
                  />
              }
            </div>
          </div>
        ) : null}

        {(canEditTran && visibleKeyInfo && keyInfoUser && visibleKeyInfo.indexOf(rfpTranSubType) !== -1 && (keyInfoUser.indexOf(user.userId)!== -1 || (user && user.relationshipToTenant === "Self")) && (submitFor || keyInfoUserDetails.rfpParticipantFirmName)) ? (
          <div>
            <div className="columns">
              <div className="column is-12">
                <Accordion
                  multiple
                  isRequired={!!activeItem.length}
                  activeItem={activeItem.length ? activeItem : [0]}
                  boxHidden
                  render={({ activeAccordions, onAccordion }) => (
                    <div>
                      <RatingSection
                        onAccordion={() => onAccordion(0)}
                        title="RFP Response - Key Information"
                      >
                        {activeAccordions.includes(0) && (
                          <KeyInformation
                            item={keyInformation}
                            dropDown={dropDown}
                            canEditTran={canEditTran}
                            errors={errorsKeyInfo}
                            onChangeItem={this.onChangeItem}
                            category="keyInformation"
                            onBlur={this.onBlur}
                          />
                        )}
                      </RatingSection>
                      <RatingSection
                        onAccordion={() => onAccordion(1)}
                        title="Interest Rate - Key Terms"
                        actionButtons={this.actionButtons(
                          "keyInterest",
                          isSaveDisabled,
                          canEditTran
                        )}
                      >
                        {activeAccordions.includes(1) && (
                          <div>
                            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                              <TableHeader cols={colsInterest} />
                              {keyInterest.length
                                ? keyInterest.map((item, index) => {
                                  const errors =
                                    (errorMessages &&
                                      errorMessages.keyInterest &&
                                      errorMessages.keyInterest[
                                        index.toString()
                                      ]) ||
                                    {}
                                  return (
                                    <InterestRate
                                      key={index}
                                      canEditTran={canEditTran}
                                      item={item}
                                      dropDown={dropDown}
                                      onRemove={() =>
                                        this.onRemove("keyInterest", index)
                                      }
                                      errors={errors}
                                      onChangeInterest={(
                                        changedItem,
                                        change
                                      ) =>
                                        this.onChangeInterest(
                                          changedItem,
                                          index,
                                          "keyInterest",
                                          change
                                        )
                                      }
                                      onBlur={change =>
                                        this.onBlur("keyInterest", change)
                                      }
                                    />
                                  )
                                })
                                : null}
                            </table>
                          </div>
                        )}
                      </RatingSection>
                    </div>
                  )}
                />
              </div>
            </div>
          </div>
        ) : null}

        {(canEditTran && keyInfoUser && (keyInfoUser.indexOf(user.userId) !== -1 || (user && user.relationshipToTenant === "Self")) && (submitFor || keyInfoUserDetails.rfpParticipantFirmName)) ? (
          <div className="columns">
            <div className="column">
              <article className="media">
                <div className="media-content">
                  {!responseId ? (
                    <div className="field">
                      <p className="control multiExpLblBlk">Comment
                        <span className="icon has-text-danger">
                          <i className="fas fa-asterisk extra-small-icon"/>
                        </span>
                        <textarea
                          className="textarea"
                          name="submissionComment"
                          value={submissionComment}
                          onChange={this.onChange}
                          placeholder="Submission comments from supplier to evaluation / selection committee..."
                        />
                        {errors.submissionComment ? (
                          <p className="is-small text-error has-text-danger" style={{ fontSize: 12 }}>{errors.submissionComment}</p>
                        ) : null}
                      </p>
                    </div>
                  ) : null}
                  <nav className="level">
                    <div className="level-left">
                      {!responseId ? (
                        <div className="level-item">
                          <div>
                              <SingleFileDocUpload
                                onFileSetInState={this.onFileSetInState}
                                docFile={docFile}
                                required
                                bucketName={bucketName}
                                contextId={this.props.transactionId}
                                contextType={ContextType.rfp || ""}
                                tenantId={this.props.user.entityId || ""}
                                user={user}
                                onUploadSuccess={this.onUploadSuccess}
                                isNew={!responseId}
                              />
                            {!docId ? (<small className="is-size-7 is-small text-error has-text-danger">{errors.docId}</small>) : ""}
                          </div>

                          {/* <DocUpload
                            bucketName={bucketName}
                            onUploadSuccess={this.getBidBucket}
                            responseOrQuestion
                            showFeedback
                            tags={tags}
                            contextId={this.props.transactionId}
                            contextType={ContextType.rfp}
                            tenantId={this.props.user.entityId}
                          /> */}

                          {/*{!docId ? (*/}
                            {/*<p*/}
                              {/*className="is-small text-error has-text-danger"*/}
                              {/*style={{ fontSize: 12 }}*/}
                            {/*>*/}
                              {/*{errors.docId}*/}
                            {/*</p>*/}
                          {/*) : (*/}
                            {/*""*/}
                          {/*)}*/}
                          {filename && <p>{filename}</p>}
                        </div>
                      ) : null}
                      <div className="level-right">
                        <div className="level-item">
                          <button
                            onClick={responseId ? this.onUploadSuccess : this.onSubmit}
                            className="button is-info"
                            role="button"
                            disabled={isSaveDisabled}
                          >
                            Submit
                          </button>
                        </div>
                        <div className="level-item">
                          <a
                            className="button is-light"
                            onClick={this.onCancel}
                          >
                            Cancel
                          </a>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
              </article>
            </div>
          </div>
        ) : null}

        <RatingSection title="Supplier RFP Response Document Upload">
          <div>
            {keyInfoUser && keyInfoUser.indexOf(user.userId) === -1 ? (
              <div className="columns">
                <div className="column is-one-quarter">
                  <p className="multiExpLblBlk">Filter</p>
                  <DropdownList
                    filter
                    data={rfpParticipants || []}
                    value={filterBy || ""}
                    textField="name"
                    valueField="id"
                    onChange={this.onFilter}
                  />
                </div>
                <div className="column is-one-fifth">
                  <button
                    className="button is-text is-small"
                    onClick={() => this.onFilter("reset")}
                  >
                    Filter reset
                  </button>
                </div>
              </div>
            ) : null}
            <div className="tbl-scroll">
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <TableHeader
                cols={keyInfoUser.indexOf(user.userId) !== -1 ? cols : viewCols}
              />
              <tbody>
                {supplierRes && supplierRes.length
                  ? supplierRes.map((comment, i) => {
                    const user = contacts.find(contact => contact._id === comment.PartContactId) || ""
                    const firm = firms.find(firm => firm._id === user.entityId) || ""
                    return (
                      <tr key={i}>
                        <td>
                          <div className="select">
                            <select
                              name="action"
                              onChange={e => this.onFileAction(e, comment.DocId, i)}
                              disabled={!comment.DocId || !canEditTran}
                              value={this.state.action || ""}
                            >
                              <option value="">Pick action</option>
                              <option value="email">Have Email a Copy</option>
                              <option value="version">Versions</option>
                            </select>
                            <a
                              className="hidden-download-anchor"
                              style={{ display: "none" }}
                              ref={el => {
                                this.downloadAnchor = el
                              }}
                              href={this.state.signedS3Url}
                              target="_blank"
                            >
                              click here to download S3 file securely
                            </a>
                          </div>
                        </td>
                        <td>{firm.firmName}</td>
                        <td>
                          {`${user.userFirstName || ""} ${user.userLastName || ""}`}
                        </td>
                        <td>
                          {comment.LastUpdatedDate ? moment(comment.LastUpdatedDate).format("DD-MM-YYYY hh:mm A") : ""}
                        </td>
                        <td>{comment.Comment}</td>
                        <td>
                          {comment.DocId ? (
                            <a
                              style={{
                                display: "inline-block",
                                marginLeft: 10
                              }}
                            >
                              {comment.DocId ? (
                                <div className="field is-grouped">
                                  <DocLink docId={comment.DocId} />
                                </div>
                              ) : null}
                            </a>
                          ) : null}
                        </td>
                        {visibleKeyInfo.indexOf(rfpTranSubType) !== -1 ? (
                          <td>
                            <a>
                              <span
                                className="is-link"
                                onClick={() => this.handleInfoModal(comment)}
                              >
                                Details
                              </span>
                            </a>
                          </td>
                        ) : null}

                        {visibleKeyInfo.indexOf(rfpTranSubType) !== -1 &&
                        keyInfoUser.indexOf(this.props.user.userId) !== -1 ? (
                            <td className="emmaTablesTd">
                              <div className="field is-grouped">
                                <div className="control">
                                  <a onClick={() => this.onEdit(comment)}>
                                    <span className="has-text-link">
                                      <i className="fas fa-pencil-alt" />
                                    </span>
                                  </a>
                                </div>
                              </div>
                            </td>
                          ) : null}
                      </tr>
                    )
                  })
                  : null}
              </tbody>
            </table>
            </div>
          </div>
        </RatingSection>
        {this.state.showModal ? (
          <div>
            <DocModalDetails
              showModal={this.state.showModal}
              closeDocDetails={this.handleDocDetails}
              documentId={doc.documentId}
              // onDeleteAll={this.deleteDoc}
              docMetaToShow={["category", "subCategory"]}
              versionMetaToShow={["uploadedBy"]}
              docId={doc._id}
            />
          </div>
        ) : (
          null
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(RFPResponse)

export default connect(mapStateToProps,null)(WrappedComponent)
