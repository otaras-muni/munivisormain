import React from "react"
import {TextLabelInput, SelectLabelInput, NumberInput} from "../../../../GlobalComponents/TextViewBox"

const KeyInformation = ({errors, category, onChangeItem, onBlur, item, dropDown}) => {

  const onChange = (event) => {
    onChangeItem({
      ...item,
      [event.target.name]: event.target.value,
    }, category)
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value) {
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  return (
    <div>

      <div className="columns">
        <TextLabelInput label="Proposal Date" className="column is-one-quarter" name="proposalDate" type="date" /* value={item.proposalDate || ""} */ value={(item.proposalDate === "" || !item.proposalDate) ? null : new Date(item.proposalDate)} error= {errors.proposalDate || ""} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Term/Tenor of Loan" className="column is-one-quarter" list={dropDown.tenor} name="tenorLoan" value={item.tenorLoan || ""} error= {errors.tenorLoan || ""} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Average Life" className="column is-one-quarter" list={dropDown.averageLife} name="averageLife" value={item.averageLife || ""} error= {errors.averageLife || ""} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Provision Type" className="column is-one-quarter" required list={dropDown.provisionType} name="provisionType" value={item.provisionType || ""} error= {errors.provisionType || ""} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <SelectLabelInput label="Payment Type" className="column is-one-quarter" list={dropDown.paymentType} name="paymentType" value={item.paymentType || ""} error= {errors.paymentType || ""} onChange={onChange} onBlur={onBlurInput}/>

        { item.paymentType === "Fixed" ?
          <NumberInput label="Fixed Rate" name="fixedRate" className="column is-one-quarter" decimalScale={4} suffix="%" value={item.fixedRate || ""} error={errors.fixedRate || ""} onChange={onChange} onBlur={onBlurInput}/>
          : null
        }

        { item.paymentType === "Floating" ?
          <div className="column" style={{ paddingBottom: 0}}>
            <p className="multiExpLbl ">
              Floating Rate Index
            </p>
            <div className="field is-grouped" style={{justifyContent: "unset" }}>
              <div className="control">
                <SelectLabelInput title="Floating Rate Type" list={dropDown.floatingRateType || []} name="floatingRateType" value={item.floatingRateType || ""} error= {errors.floatingRateType || ""} onChange={onChange} onBlur={onBlurInput}/>&nbsp;
              </div>
              <div className="control">
                <NumberInput title="Floating Rate Ratio" suffix="%" placeholder="Floating Rate Ratio %" decimalScale={4} name="floatingRateRatio" value={item.floatingRateRatio || ""} error= {errors.floatingRateRatio || ""} onChange={onChange} onBlur={onBlurInput}/>&nbsp;
              </div>
              <div className="control">
                <NumberInput title="Floating Rate Spread" suffix="%" placeholder="Floating Rate Spread %" decimalScale={4} name="floatingRateSpread" value={item.floatingRateSpread || ""} error= {errors.floatingRateSpread || ""} onChange={onChange} onBlur={onBlurInput}/>
              </div>
            </div>
          </div>
          :null
        }
      </div>

      <div className="columns">
        <SelectLabelInput label="Underlying Credit Type" className="column is-one-quarter" list={dropDown.creditType} name="underlyingCredit" value={item.underlyingCredit || ""} error= {errors.underlyingCredit || ""} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Sector Code" required className="column is-one-quarter" list={dropDown.sectorCode} name="sectorCode" value={item.sectorCode || ""} error= {errors.sectorCode || ""} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="State" required className="column is-one-quarter" list={dropDown.state} name="state" value={item.state || ""} error= {errors.state || ""} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="Bank Leader" name="bankLender" className="column is-one-quarter" value={item.bankLender || ""} error= {errors.bankLender || ""} onChange={onChange} onBlur={onBlurInput}/>
      </div>

    </div>
  )
}

export default KeyInformation
