import React from "react"
import { connect} from "react-redux"
import _ from "lodash"
import swal from "sweetalert"
import {toast} from "react-toastify"
import Select from "react-select"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import {
  fetchRfpContacts,
  putRfpTransaction,
  sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import DocUpload from "../../../docs/DocUpload"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import DocLink from "../../../docs/DocLink"
import SelectWinnerEmailSendModal from "../../../../GlobalComponents/SelectWinnerEmailSendModal"
import {updateTransaction} from "../../../../StateManagement/actions/TransactionDistribute"

class Decide extends React.Component {
  constructor(props) {
    super(props)
    const categories = CONST.EvalCategories.map(x => ({name: x.name, key: x.key}))
    this.state = {
      bucketName: CONST.bucketName,
      docId: "",
      filename: "",
      evalCategories: [
        {name: "Evaluation related information", key: "information"},
        ...categories,
        {name: "Overall Score / Rating", key: "overallScore"},
      ],
      cols: [
        {name: "List Item"},
      ],
      loading: true,
      disabled: false,
      modalState: false,
      decideList: [],
      rfpFinalSelections:{
        evaluationComments: "",
        notesWinningBidder: "",
        rfpParticipantFirmId: "",
        rfpParticipantFirmName: "",
        rfpParticipantMSRBType: ""
      },
      supplier: [],
      supplierParticipants: [],
      errors: {},
      confirmAlert: CONST.confirmAlert,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      isSaveDisabled: false,
      isEnableEdit: false,
    }
  }

  componentWillMount() {
    const {transactionId, transaction, user, tranAction} = this.props
    let isEnableEdit = false
    if(transaction && transaction.rfpEvaluationTeam){
      isEnableEdit = transaction.rfpEvaluationTeam.find(evl => evl.rfpSelEvalContactId === user.userId)
    }
    if (transactionId) {
      const url =`?transId=${transactionId}`
      fetchRfpContacts(url, (supplier)=> {
        this.setState({
          isEnableEdit: (!!isEnableEdit && tranAction && tranAction.canManageEdit && tranAction.canTranStatusEditDoc) || false,
          transactionId,
          rfpFinalSelections: (transaction && transaction.rfpFinalSelections) || "",
          supplier:  supplier.rfpParticipantDist,
          disabled: !!(transaction && transaction.rfpFinalSelections && transaction.rfpFinalSelections._id),
          loading: false,
        })
      })
    }
  }

  onChange = (e) => {
    const {name, value} = e.target
    this.setState(prevState => ({
      rfpFinalSelections: {
        ...prevState.rfpFinalSelections,
        [name]: value
      }
    }))
  }

  onBlur = (event) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    if(event.target.title && event.target.value){
      this.props.addAuditLog({userName, log: `In Decide ${event.target.title || "empty"} change to ${event.target.value || 'empty'}`, date: new Date()})
    }
  }

  onPickWinner = (e) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `In Decide choose winner ${e.label}`, date: new Date()})

    this.setState(prevState => ({
      rfpFinalSelections: {
        ...prevState.rfpFinalSelections,
        rfpParticipantFirmId: e.mem && e.mem.rfpParticipantFirmId || "",
        rfpParticipantFirmName : e.mem && e.mem.rfpParticipantRealFirmName || "",
        rfpParticipantMSRBType: e.mem && e.mem.rfpParticipantRealMSRBType || "",
      }
    }))
  }

  setDecideListDataCategoryWise = () => {
    const { supplier, decideList, cols, evalCategories } = this.state;
    decideList.forEach(decide => {
      const tabName = supplier.find(x => x.rfpParticipantFirmId === decide._id)
      if(tabName) {
        decide.username = tabName.rfpParticipantContactName
        decide.firmName = tabName.rfpParticipantRealFirmName
        decide.msrbType = tabName.rfpParticipantRealMSRBType
        cols.push({name: tabName.rfpParticipantRealFirmName, key: decide._id})
        evalCategories[evalCategories.length-1][decide._id] = decide.totalRfpScore.toFixed(2);
        decide.categoryScores.forEach(decCat => {
          const index = evalCategories.findIndex(i => i.key ===  decCat.category)
          if(index !== -1) {
            evalCategories[index][decide._id] = decCat.averageScore.toFixed(2)
          }
        })
      }
    })
    // const winnerBidder = decideList.reduce(function(max, x) { return (x.totalRfpScore > max) ? x : max; }, 0)
    this.setState({
      cols,
      loading: false,
      decideList,
      evalCategories,
    })
  }

  getBidBucket = (filename, docId) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({userName, log: `In Decide file ${filename} Uploaded`, date: new Date(), category: "Supplier Post Media"})
    this.setState({
      docId,
      filename
    })
  }

  onSubmit = () => {
    const {rfpFinalSelections, evaluationComments, filename, docId, notesWinningBidder, errors, supplier} = this.state
    let { supplierParticipants } = this.state
    const { transaction } = this.props
    /* const rfpFinalSelections = {
      evaluationComments,
      notesWinningBidder,
      rfpParticipantFirmId: winnerBidder.rfpParticipantFirmId,
      rfpParticipantFirmName : winnerBidder.rfpParticipantRealFirmName,
      rfpParticipantMSRBType: winnerBidder.rfpParticipantRealMSRBType,
    } */
    if(docId && !rfpFinalSelections.documentId) {
      rfpFinalSelections.documentId = docId
      rfpFinalSelections.documentName = filename
    }
    if(!rfpFinalSelections.evaluationComments || !rfpFinalSelections.rfpParticipantFirmId) {
      if(!rfpFinalSelections.evaluationComments) errors.evaluationComments = "Required"
      this.setState({errors})
      return
    }

    if((supplier && supplier.length && rfpFinalSelections) && (rfpFinalSelections.rfpParticipantFirmId)){
      supplierParticipants = []
      supplier.forEach(s => {
        if(s.rfpParticipantFirmId === rfpFinalSelections.rfpParticipantFirmId){
          supplierParticipants.push({
            id: s && s._id || "",
            name: s && s.rfpParticipantContactName || "",
            sendEmailTo: s && s.rfpParticipantContactEmail || ""
          })
        }
      })
    }

    const { confirmAlert } = this.state
    confirmAlert.text = "The members of the evaluation committee will not be able to edit the RFP after choosing the winner"
    swal(confirmAlert)
      .then((willDelete) => {
        if (this.props.transactionId) {
          if (willDelete) {
            this.setState({
              modalState: true,
              supplierParticipants,
              email: {
                ...this.state.email,
                category: "custom",
                subject: `${rfpFinalSelections.rfpParticipantFirmName} Won The Transaction-${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription}`,
                rfpFinalSelections
              },
            })
          }
        }
      })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onModalSave = () => {
    const { email, supplierParticipants, rfpFinalSelections } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace("/",""),
        ...email,
        sendEmailTo: supplierParticipants || []
      }
    }
    const payload = {rfpTranStatus: "Closed"}
    this.setState({
      modalState: false,
      loading: true
    }, async () => {
      putRfpTransaction("winner", this.props.transactionId, {rfpFinalSelections: rfpFinalSelections}, (res) => { //eslint-disable-line
        updateTransaction(this.props.nav2, "summary", payload, async response => {
          console.log(response)
        })
        if (res && res.status === 200) {
          toast("Transaction updated successfully!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
          this.props.submitAuditLogs(this.props.transactionId)
          this.setState({
            errors: {},
            disabled: !!(res && res.data && res.data.rfpFinalSelections && res.data.rfpFinalSelections._id),
            isSaveDisabled: false,
            loading: false
          })
        } else {
          toast("something went wrong!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
          this.setState({
            isSaveDisabled: false,
            loading: false
          })
        }
      })
      await sendEmailAlert(emailPayload)
    })
  }

  render() {
    const {modalState, isEnableEdit, bucketName, email, supplier, rfpFinalSelections, errors, isSaveDisabled, disabled, supplierParticipants} = this.state
    let firmsList = supplier && supplier.length && supplier.map(mem=>({value: mem.rfpParticipantFirmId, label: mem.rfpParticipantFirmName, mem }))
    firmsList = _.uniqBy(firmsList || [], "value")

    const {documentId, rfpParticipantFirmId, evaluationComments, notesWinningBidder} = rfpFinalSelections
    const tags = {
      userName: this.props.user.userFirstName,
      userId: this.props.user.userId,
      entityId: this.props.user.entityId,
    }
    const loading = () => {
      return <Loader/>
    }

    if(this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SelectWinnerEmailSendModal modalState={modalState} email={email} onModalChange={this.onModalChange} supplierParticipants={supplierParticipants} onSave={this.onModalSave} /* onParticipantsRefresh={onParticipantsRefresh}  */ />
        <div className="columns">
          <div className="column">
            <label className="label">Pick Bid Winner</label>
            <div className="field has-addons">
              <div className="control">
                <Select
                  value={rfpParticipantFirmId || ""}
                  onChange={this.onPickWinner}
                  disabled={!isEnableEdit || disabled}
                  options={[
                    {value:"", label:"Pick Winner"},
                    ...firmsList,
                  ]}
                />
              </div>
            </div>
            <div className="field">
              <p className="control">
                <textarea title="Comments" className="textarea" onBlur={this.onBlur} name="evaluationComments" value={evaluationComments} disabled={!isEnableEdit || disabled} placeholder="Internal Use Only: Final closing comments of evaluation / selection committee..." onChange={this.onChange}/>
                {errors && errors.evaluationComments && <p className="text-error">{errors.evaluationComments}</p>}
              </p>
            </div>
          </div>
        </div>
        <hr/>
        <div className="columns">
          <div className="column">
            <article className="media">
              <div className="media-content">
                <div className="field">
                  <p className="control">
                    <textarea title="Notes" className="textarea" onBlur={this.onBlur} name="notesWinningBidder" value={notesWinningBidder} disabled={!isEnableEdit || disabled} placeholder="Notes to the winning bidder" onChange={this.onChange} />
                    {errors && errors.notesWinningBidder && <p className="text-error">{errors.notesWinningBidder}</p>}
                  </p>
                </div>
                <nav className="level">
                  {
                    (isEnableEdit || disabled) ?
                      <div className="level-left">
                        <div className="level-item">
                          {(isEnableEdit && !disabled) && !documentId ? <DocUpload bucketName={bucketName} onUploadSuccess={this.getBidBucket} responseOrQuestion tags={tags} showFeedback contextId={this.props.transactionId} contextType={"RFPS"} tenantId={this.props.user.entityId}/> : null }
                          {
                            documentId ?
                              <div className="field is-grouped">
                                { (isEnableEdit && !disabled) ? <DocUpload bucketName={bucketName} docId={documentId} onUploadSuccess={this.getBidBucket} responseOrQuestion tags={tags} showFeedback contextId={this.props.transactionId} contextType={"RFPS"} tenantId={this.props.user.entityId}/> : null }
                                <DocLink docId={documentId} />
                              </div> : null
                          }
                        </div>
                        <div className="level-right">
                          <div className="level-item">
                            <button className="button is-info" role="button" onClick={this.onSubmit} disabled={isSaveDisabled || !evaluationComments || !rfpParticipantFirmId || disabled}>Save</button>
                          </div>
                        </div>
                      </div>
                      : null
                  }
                </nav>
              </div>
            </article>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Decide)

export default connect(mapStateToProps, null)(WrappedComponent)
