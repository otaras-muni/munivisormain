import Joi from "joi-browser"

const keyInformationSchema = Joi.object().keys({
  proposalDate: Joi.date().example(new Date("2016-01-01")).allow("").optional(),
  tenorLoan: Joi.string().allow("").optional(),
  averageLife: Joi.string().allow("").optional(),
  paymentType: Joi.string().allow("").optional(),
  fixedRate: Joi.number().allow(null).optional(),
  floatingRateType: Joi.string().allow("").optional(),
  floatingRateRatio: Joi.number().allow(null).optional(),
  floatingRateSpread: Joi.number().allow(null).optional(),
  underlyingCredit: Joi.string().allow("").optional(),
  sectorCode: Joi.string().required().optional(),
  state: Joi.string().required().optional(),
  bankLender: Joi.string().allow("").optional(),
  provisionType: Joi.string().required().optional(),
  _id: Joi.string().optional(),
})

const keyInterestSchema = Joi.object().keys({
  rateType: Joi.string().required().optional(),
  loanAmountFrom: Joi.number().allow(null, "").min(0).optional(),
  loanAmountTo: Joi.number().allow(null, "").min(0).optional(),
  maturities: Joi.string().allow("").optional(),
  rate: Joi.number().allow(null, "").min(0).optional(),
  _id: Joi.string().optional(),
})

const responseSchema = Joi.object().keys({
  _id: Joi.string().allow("").required().optional(),
  keyInformation: keyInformationSchema,
  keyInterest: Joi.array().items(keyInterestSchema).optional(),
})

export const keyInformationValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, responseSchema, { abortEarly: false, stripUnknown:false })

