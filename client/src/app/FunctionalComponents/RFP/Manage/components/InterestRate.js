import React from "react"
import {SelectLabelInput, TextLabelInput, NumberInput} from "../../../../GlobalComponents/TextViewBox"

const InterestRate = ({ item={}, canEditTran, onChangeInterest, onRemove, errors={}, onBlur, dropDown}) => {

  const onChange = (event) => {
    onChangeInterest({
      ...item,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    })
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value){
      onBlur(`${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  return (
    <tbody>
      <tr>
        <td>
          <SelectLabelInput title="Rate Type" list={dropDown.rateType} value={item.rateType || ""} error={errors.rateType || ""} name="rateType" onChange={onChange} onBlur={onBlurInput}/>
        </td>
        <td>
          <NumberInput required name="loanAmountFrom" prefix="$" value={item.loanAmountFrom || ""} error={errors.loanAmountFrom ? "(must be larger than or equal to 0)" : "" || ""} onChange={onChange} onBlur={onBlurInput}/>
        </td>
        <td>
          <NumberInput required name="loanAmountTo" prefix="$" value={item.loanAmountTo || ""} error={errors.loanAmountTo ? "(must be larger than or equal to 0)" : "" || ""} onChange={onChange} onBlur={onBlurInput}/>
        </td>
        <td>
          <TextLabelInput required name="maturities" value={item.maturities || ""} error={errors.maturities || ""} onChange={onChange} onBlur={onBlurInput}/>
        </td>
        <td>
          <NumberInput required name="rate" decimalScale={4} suffix="%" value={item.rate || ""} error= {errors.rate ? "(must be larger than or equal to 0)" : "" || ""} onChange={onChange} onBlur={onBlurInput}/>
        </td>
        {
          canEditTran ?
            <td>
              <div className="field is-grouped">
                <div className="control">
                  <a onClick={onRemove}>
                    <span className="has-text-link">
                      <i className="far fa-trash-alt"/>
                    </span>
                  </a>
                </div>
              </div>
            </td>
            : null
        }
      </tr>
    </tbody>
  )}

export default InterestRate
