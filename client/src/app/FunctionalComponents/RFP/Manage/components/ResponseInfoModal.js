import React from "react"
import NumberFormat from "react-number-format"
import moment from "moment"
import { Modal } from "../../../../FunctionalComponents/TaskManagement/components/Modal"
import { TextLabelBox } from "../../../../GlobalComponents/TextViewBox"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"

const colsInterest = [
  { name: "Rate Type" },
  { name: "Loan Amount (from)" },
  { name: "Loan Amount (to)" },
  { name: "Maturities" },
  { name: "Rate" }
]

export const ResponseInfoModal = ({ response, modalVisible, toggleModal }) => {
  const keyInfo = (response && response.keyInformation) || {}
  const keyInterest = (response && response.keyInterest) || []
  return (
    <Modal
      closeModal={toggleModal}
      modalState={modalVisible}
      title="Additional Submission Details"
      modalWidth={{ width: "70%" }}
    >
      <div>
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="RFP Response - Key Information"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <div className="columns">
                      <TextLabelBox
                        label="Proposal Date"
                        className="is-one-quarter"
                        value={
                          keyInfo.proposalDate
                            ? moment(keyInfo.proposalDate).format("YYYY-MM-DD")
                            : "--" || "--"
                        }
                      />
                      <TextLabelBox
                        label="Term/Tenor of Loan"
                        className="is-one-quarter"
                        value={keyInfo.tenorLoan || ""}
                      />
                      <TextLabelBox
                        label="Average Life"
                        className="is-one-quarter"
                        value={keyInfo.averageLife || ""}
                      />
                      <TextLabelBox
                        label="Provision Type"
                        className="is-one-quarter"
                        value={keyInfo.paymentType || ""}
                      />
                    </div>
                    {keyInfo.paymentType === "Fixed" ? (
                      <div className="columns">
                        <TextLabelBox
                          label="Payment Type"
                          className="is-one-quarter"
                          value={keyInfo.paymentType || "--"}
                        />
                        <div className="column is-one-quarter">
                          <p className="multiExpLbl">Fixed Rate</p>
                          <div className="dashExpLblVal">
                            $
                            {parseInt(
                              keyInfo.fixedRate || 0,
                              10
                            ).toLocaleString()}
                          </div>
                        </div>
                      </div>
                    ) : null}
                    {keyInfo.paymentType === "Floating" ? (
                      <div className="columns">
                        <TextLabelBox
                          label="Payment Type"
                          className="is-one-quarter"
                          value={keyInfo.paymentType || "--"}
                        />
                        <TextLabelBox
                          label="Floating Rate Type"
                          className="is-one-quarter"
                          value={keyInfo.floatingRateType || ""}
                        />
                        <div className="column is-one-quarter">
                          <p className="multiExpLbl">Floating Rate Ratio</p>
                          <div className="control">
                            <NumberFormat
                              className="input is-fullwidth is-size-7"
                              thousandSeparator
                              style={{
                                width: 300,
                                background: "transparent",
                                border: "none"
                              }}
                              decimalScale={2}
                              suffix="%"
                              disabled
                              value={parseInt(keyInfo.floatingRateRatio, 10)}
                            />
                          </div>
                        </div>
                        <div className="column is-one-quarter">
                          <p className="multiExpLbl">Floating Rate Spread</p>
                          <div className="control">
                            <NumberFormat
                              label="Floating Rate Spread"
                              className="input is-fullwidth is-size-7"
                              thousandSeparator
                              style={{
                                width: 300,
                                background: "transparent",
                                border: "none"
                              }}
                              decimalScale={2}
                              suffix="%"
                              disabled
                              value={parseInt(keyInfo.floatingRateSpread, 10)}
                            />
                          </div>
                        </div>
                      </div>
                    ) : null}
                    <div className="columns">
                      <TextLabelBox
                        label="Underlying Credit Type"
                        className="is-one-quarter"
                        value={keyInfo.underlyingCredit || "--"}
                      />
                      <TextLabelBox
                        label="Sector Code"
                        className="is-one-quarter"
                        value={keyInfo.sectorCode || "--"}
                      />
                      <TextLabelBox
                        label="State"
                        className="is-one-quarter"
                        value={keyInfo.state || "--"}
                      />
                      <TextLabelBox
                        label="Bank Lender"
                        className="is-one-quarter"
                        value={keyInfo.bankLender || "--"}
                      />
                    </div>
                  </div>
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Interest Rate - Key Terms"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={colsInterest} />
                      <tbody>
                        {keyInterest && keyInterest.length
                          ? keyInterest.map((interest, i) => (
                              <tr key={i.toString()}>
                                <td>{interest.rateType}</td>
                                <td>
                                  $
                                  {parseInt(
                                    interest.loanAmountFrom || 0,
                                    10
                                  ).toLocaleString()}
                                </td>
                                <td>
                                  $
                                  {parseInt(
                                    interest.loanAmountTo || 0,
                                    10
                                  ).toLocaleString()}
                                </td>
                                <td>{interest.maturities || "--"}</td>
                                <td>
                                  {parseInt(
                                    interest.rate || 0,
                                    10
                                  ).toLocaleString()}
                                  %
                                </td>
                              </tr>
                            ))
                          : null}
                      </tbody>
                    </table>
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    </Modal>
  )
}

export default ResponseInfoModal
