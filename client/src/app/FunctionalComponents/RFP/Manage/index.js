import React from "react"
import {Link, withRouter} from "react-router-dom"
import "./scss/ratings.scss"
import Evaluate1 from "./components/Evaluate1"
import Decide from "./components/Decide"
import RFPResponse from "./components/RFPResponse"
import RFPQuestions from "./components/RFPQuestions"
import QueMedia from "./components/QueMedia"
import Loader from "../../../GlobalComponents/Loader"
import {SendEmailModal} from "../../../GlobalComponents/SendEmailModal"
import {sendEmailAlert} from "../../../StateManagement/actions/Transaction";


const tabs = [{
  title: <span>Supplier RFP Response</span>,
  value: 1,
  path: "manageresponse",
},{
  title: <span>Supplier Questions</span>,
  value: 2,
  path: "managequestions",
},/*{
  title: <span>Discuss</span>,
  value: 3,
  path: "managediscuss",
}*/,{
  title: <span>Evaluate</span>,
  value: 3,
  path: "manageevaluate",
},{
  title: <span>Decide</span>,
  value: 4,
  path: "managedecide",
},]

class Manage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  componentWillMount() {
    const {transaction} = this.props
    if(transaction && transaction.rfpTranClientId) {
      this.setState(prevState => ({
        transaction,
        loading: false,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription} - Notification`
        }
      }))
    } else {
      this.props.history.push("/dashboard")
    }
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onModalSave = () => {
    const { transaction } = this.props
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace("/",""),
        subject: `Transaction - ${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription} - Notification`,
        ...email,
      }
    }
    this.setState({
      modalState: false
    }, async () => {
      await sendEmailAlert(emailPayload)
    })
  }

  handleToggle = () => {
    this.setState(pre => ({
      modalState: !pre.modalState
    }))
  }

  renderTabs = (activeTab) => {
    const {nav3, tranAction} = this.props
    return (
      <div className="tabs">
        <ul>
          {
            tabs.map(tab=> {
              if((tranAction && tranAction.view && tranAction.view.indexOf(tab.path) === -1) && (tranAction && tranAction.edit && tranAction.edit.indexOf(tab.path) === -1)) return
              return (
                <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`}>
                  {
                    this.props.nav2 ? <Link to={`/rfp/${this.props.nav2}/${tab.path}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                      <a role="button" className="tabSecLevel">
                        {tab.title}
                      </a>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }

  renderSelectedView = () => {
    const { tranAction, transaction, onParticipantsRefresh, participants } = this.props
    const  props = this.props
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace("/",""),
        subject: `Transaction - ${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription} - Notification`,
        ...email,
      }
    }
    switch(this.props.nav3) {
    case "manageresponse" :
      return <RFPResponse emailPayload={emailPayload} type={this.props.nav1} transactionId={this.props.nav2} tranAction={tranAction} rfpTranSubType={transaction.rfpTranSubType}
        rfpParticipants={transaction.rfpParticipants} participants={participants} onParticipantsRefresh={onParticipantsRefresh}/>
    case "managequestions" :
      return <RFPQuestions emailPayload={emailPayload} transactionId={this.props.nav2} tranAction={tranAction} rfpParticipants={transaction.rfpParticipants}/>
    case "managequemedia" :
      return <QueMedia emailPayload={emailPayload} transactionId={this.props.nav2} {...this.props} tranAction={tranAction} rfpParticipants={transaction.rfpParticipants}/>
    case "manageevaluate" :
      return <Evaluate1 {...props} emailPayload={emailPayload} transactionId={this.props.nav2}/>
    case "managedecide" :
      return <Decide {...props} emailPayload={emailPayload} transactionId={this.props.nav2}/>
    default:
      return <RFPResponse emailPayload={emailPayload} transactionId={this.props.nav2} tranAction={tranAction}/>
    }
  }

  onBack = () =>{
    this.props.history.push(`/rfp/${this.props.nav2}/managequestions`)
  }

  render() {
    const { modalState, email } = this.state
    const { tranAction, participants, onParticipantsRefresh } = this.props
    const activeTab = tabs.find(x => x && (x.path === this.props.nav3))

    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }

    return(
      <section className='transactionRFP'>
        {this.props.nav2 && this.props.nav3 !== "managequemedia" && this.renderTabs(activeTab && activeTab.value) }
        <div className="columns">
          {
            tranAction && tranAction.canEditTran ?
              <div className="column">
                <button className="button is-link is-small" onClick={this.handleToggle} >Send Email Alert</button>
              </div>
              : null
          }
          {
            this.props.nav3 === "managequemedia" &&
            <div className="column">
              <button className="button btn-default is-small pull-right" onClick={this.onBack}>Back</button>
            </div>
          }
        </div>

        <SendEmailModal modalState={modalState} email={email} onModalChange={this.onModalChange} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onSave={this.onModalSave}/>
        {this.renderSelectedView()}
      </section>
    )
  }
}

export default withRouter(Manage)
