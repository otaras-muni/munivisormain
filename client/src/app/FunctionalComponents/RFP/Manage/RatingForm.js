import React from "react"
import "./scss/ratings.scss"

export const ratingSectionsForTenant = [
  {
    sectionName: "Section 1",
    sectionDescrption: "This is Section 1",
    sectionHeaders: ["Sect 1 Head1", "Sect 2 Head 2"],
    sectionItems: [
      {
        sectionItemId: 1,
        sectionItemPriority: 1,
        sectionItemName: "Section Item 1.1"
      },
      {
        sectionItemId: 2,
        sectionItemPriority: 2,
        sectionItemName: "Section Item 1.2"
      }
    ],
    radioItems: [
      { "Muni finance": 1 },
      { "Muni Loan": 2 },
      { "Bank Loan": 3 },
      { Swaps: 4 },
      { "Pooled Loans": 5 }
    ]
  },
  {
    sectionName: "Section 2",
    sectionDescrption: "This is Section 2 - Hot Module Replacement",
    sectionHeaders: ["Sect 2 Head1", "Sect 2 Head 2"],
    sectionItems: [
      {
        sectionItemId: 1,
        sectionItemPriority: 3,
        sectionItemName: "Section Item 2.1 -naveen balawat ALLJLSAKFJ"
      },
      {
        sectionItemId: 2,
        sectionItemPriority: 1,
        sectionItemName: "Section Item 2.2"
      }
    ],
    radioItems: [
      { "Muni finance": 1 },
      { "Muni Loan": 2 },
      { "Bank Loan": 3 },
      { Swaps: 4 },
      { "Pooled Loans": 5 }
    ]
  }
]

export const radioGroupInTableConfig = {
  radioItemHeader: "This Renders a Radio Button Component",
  radioControlName: "TestControl",
  radioItems: [
    { "Muni WHATTR": 1 },
    { "Muni Loan": 2 },
    { "Bank Loan": 3 },
    { Swaps: 4 },
    { "Pooled Loans": 5 }
  ]
}

export const RenderRatingSection = props => {
  const { ratingConfigObject } = props
  return ratingConfigObject.map((section) => {
    const radioItemHeaders = section.radioItems.map((v) => Object.keys(v)[0])
    const radioItemValues = section.radioItems.map((v) => Object.values(v)[0])
    return (
      <div key={section.sectionName} className="container">
        <h1 className="title is-4">{`Rating sections New :${section.sectionName}`}</h1>
        <section className="section">
          <div className="container">
            <table>
              <tbody>
                <tr>
                  {section.sectionHeaders.map(
                    (sectionHeader) => (
                      <th
                        key={`${sectionHeader} - ${section.sectionName} - itemheader`}
                        style={{ textAlign: "center" }}
                      >
                        {sectionHeader}
                      </th>
                    )
                  )}

                  {radioItemHeaders.map((radioheader) => (
                    <th
                      key={`${radioheader}-${section.sectionName} - radioheader`}
                      style={{ textAlign: "center" }}
                    >
                      {radioheader}
                    </th>
                  ))}
                </tr>

                {section.sectionItems.map((sectionData) => (
                  <tr key={`${section.sectionName}-${sectionData.sectionItemName}`}>
                    <td
                    >
                      {sectionData.sectionItemName}
                    </td>
                    <td
                    >
                      {sectionData.sectionItemPriority}
                    </td>
                    {radioItemValues.map((radioItem) => (
                      <td
                        key={`${section.sectionName} - ${radioItem}`}
                        style={{ textAlign: "center" }}
                      >
                        <input
                          type="radio"
                          name={sectionData.sectionItemName}
                          value={radioItem}
                        />
                      </td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </section>
      </div>
    )
  })
}

export const RenderRadioGroupInTable = props => {
  const { radioGroupInTableConfig } = props
  const radioItemHeaders = radioGroupInTableConfig.radioItems.map((v) => Object.keys(v)[0])
  const radioItemValues = radioGroupInTableConfig.radioItems.map((v) => Object.values(v)[0])

  return (
    <section className="section">
      <div className="container">
        <table>
          <tbody>
            <tr>
              {radioItemHeaders.map((header) => (
                <th
                  key={`${header} - radioheader`}
                  style={{
                    backgroundColor: "white",
                    borderCollapse: "collapse"
                  }}
                >
                  {header}
                </th>
              ))}
            </tr>
            <tr>
              {radioItemValues.map((radioitem) => (
                <td key={`${radioitem} - radioitem`} style={{ textAlign: "center" }}>
                  <input
                    type="radio"
                    name={radioGroupInTableConfig.radioControlName}
                    value={radioitem}
                  />
                </td>
              ))}
            </tr>
          </tbody>
        </table>
      </div>
    </section>
  )
}

