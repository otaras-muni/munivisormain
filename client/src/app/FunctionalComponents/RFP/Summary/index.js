import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import swal from "sweetalert"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import TranSummaryStatus from "../../../GlobalComponents/TranSummaryStatus"
import ActivitySummary from "./components/ActivitySummary"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import { pullRelatedTransactions } from "../../../StateManagement/actions/CreateTransaction"
import {
  putRelatedTran,
  updateTransaction
} from "../../../StateManagement/actions/TransactionDistribute"
import CONST, { oppMsg } from "../../../../globalutilities/consts"
import Loader from "../../../GlobalComponents/Loader"
import SendEmailModal from "../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../StateManagement/actions/Transaction"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ReletedTranGlob from "../../../GlobalComponents/ReletedTranGlob"
import HistoricalData from "../../../GlobalComponents/HistoricalData"

class Summary extends React.Component {
  constructor() {
    super()
    this.state = {
      transactions: [],
      tempTransactions: [],
      relatedTransactions: [],
      debtIssuance: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      },
      tranTextChange: {
        rfpTranIssueName: "",
        rfpTranProjectDescription: "",
        rfpParAmount: "",
        rfpTranSecurityType: ""
      },
      errorMessages: {},
      transaction: {},
      securityType: [],
      relatedTypes: [],
      loading: true,
      isSaveDisabled: false,
      integerError: {},
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  componentWillMount() {
    const { transaction, nav2 } = this.props
    if (transaction && transaction.rfpTranClientId) {
      this.setState(prevState => ({
        transId: nav2,
        transaction,
        relatedTransactions: transaction.rfpTranRelatedTo || [],
        tranTextChange: {
          rfpTranIssueName: transaction.rfpTranIssueName || "",
          rfpTranProjectDescription:
            transaction.rfpTranProjectDescription || "",
          rfpParAmount: transaction.rfpParAmount || null,
          rfpEstimatedRev: transaction.rfpEstimatedRev || null,
          rfpTranSecurityType: transaction.rfpTranSecurityType || "",
          rfpTranIsConduitBorrowerFlag: transaction.rfpTranIsConduitBorrowerFlag || ""
        },
        duplicateStatus: transaction && transaction.rfpTranStatus || "",
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.rfpTranIssueName ||
            transaction.rfpTranProjectDescription} - Notification`
        }
      }))
    } else {
      this.props.history.push("/dashboard")
    }
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName([
      "LKUPSWAPTRANSTATUS",
      "LKUPSECTYPEGENERAL",
      "LKUPRELATEDTYPE"
    ])
    const result = (picResult.length && picResult[1]) || {}
    if(result && result.LKUPSWAPTRANSTATUS && result.LKUPSWAPTRANSTATUS.length){
      const removedStatus = ["Executed but not closed", "Pre-execution", "Pre-Munivisor"]
      result.LKUPSWAPTRANSTATUS.forEach(r => {
        if(removedStatus.indexOf(r) !== -1){
          const index = result.LKUPSWAPTRANSTATUS.indexOf(r)
          result.LKUPSWAPTRANSTATUS.splice(index, 1)
        }
      })
    }
    this.setState({
      tranStatus: result.LKUPSWAPTRANSTATUS || [],
      securityType: result.LKUPSECTYPEGENERAL || [],
      relatedTypes: result.LKUPRELATEDTYPE || ["Bond Hedge", "Loan Hedge"],
      loading: false
    })
  }

  onRelatedTranSelect = item => {
    const { debtIssuance } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary related type ${
        debtIssuance.actTranRelatedType
      } and ${item.name || ""}`,
      date: new Date(),
      key: "summary"
    })
    this.setState(prevState => ({
      debtIssuance: {
        ...prevState.debtIssuance,
        actTranRelatedTo: item
      }
    }))
  }

  onChange = e => {
    const { debtIssuance } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary select related type ${e.target.value}`,
      date: new Date(),
      key: "summary"
    })
    debtIssuance[e.target.name] = e.target.value
    this.setState({
      debtIssuance: {
        ...debtIssuance,
        actTranRelatedTo: []
      }
    })
  }

  onModalSave = () => {
    this.setState({
      modalState: true,
      relatedSave: true
    })
  }

  onRelatedtranSave = () => {
    const { email, debtIssuance } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    const actTranRelatedTo = {
      ...debtIssuance.actTranRelatedTo,
      relType: debtIssuance.actTranRelatedType
    }

    if (!debtIssuance.actTranRelatedType || actTranRelatedTo.length === 0)
      return false
    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putRelatedTran(this.props.nav2, actTranRelatedTo, res => {
          if (res && res.status === 200) {
            toast("Successfully Add related transaction", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(this.props.nav2)
            this.setState(
              {
                transaction: res.data,
                relatedTransactions:
                  (res.data && res.data.rfpTranRelatedTo) || [],
                debtIssuance: {
                  actTranRelatedType: debtIssuance.actTranRelatedType,
                  actTranRelatedTo: ""
                },
                isSaveDisabled: false
              },
              async () => {
                await sendEmailAlert(emailParams)
              }
            )
          } else {
            toast("something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onChangeTextInput = e => {
    const { name, value } = e.target

    this.setState(prevState => ({
      tranTextChange: {
        ...prevState.tranTextChange,
        [name]: value
      }
    }))
  }

  onBlur = e => {
    const { transaction, tranTextChange } = this.state
    const integer = ["rfpParAmount", "rfpEstimatedRev"]
    if (transaction[e.target.name] !== tranTextChange[e.target.name]) {
      if(integer.indexOf(e.target.name) > -1 && tranTextChange[e.target.name] < 1){
        if(tranTextChange[e.target.name] < 0){
          this.setState({
            integerError: {
              [e.target.name]: "enter positive value"
            }
          })
        }
        return
      } else {
        this.setState({
          integerError: {
            [e.target.name]: ""
          }
        })
        this.onInputChange(e)
      }
    }
  }

  onInputChange = e => {
    const { tranTextChange, transaction, duplicateStatus } = this.state
    const { user } = this.props
    let { name, value, title } = e.target
    const statusChange = name === "actTranStatus" || name === "rfpTranStatus"
    if (statusChange) {
      name = "rfpTranStatus"
    }
    if (name === "rfpTranSecurityType") {
      tranTextChange[name] = value
    } else {
      transaction[name] = value
    }
    if (name === "rfpTranIsConduitBorrowerFlag") {
      tranTextChange[name] = value || ""
    }

    if (statusChange) {
      const payload = {
        [name]: value
      }
      if (transaction.opportunity && transaction.opportunity.status && value !== "In Progress") {
        payload[name] = value === "Won" ? "Pre-Pricing" : value
        payload.opportunity = {
          ...transaction.opportunity,
          status: false,
          isWon: value === "Won"
        }
      }
      if(value === "Cancelled" || value === "Closed"){
        const msg = "You will not be able to EDIT any part of the transaction post this action. Are you sure?"
        swal({
          title: "",
          text: msg,
          icon: "warning",
          buttons: ["Cancel", "Yes"]
        }).then(willDo => {
          if (willDo) {
            this.setState({
              payload,
              statusChange,
              modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress"),
              relatedSave: false
            })
          } else {
            return this.setState({
              transaction: {
                ...this.state.transaction,
                rfpTranStatus: duplicateStatus || ""
              }
            })
          }
        })
      } else {
        this.setState(
          {
            payload,
            statusChange,
            modalState: !(
              transaction.opportunity &&
              transaction.opportunity.status &&
              value !== "In Progress"
            ),
            relatedSave: false
          },
          () => {
            if (transaction.opportunity && transaction.opportunity.status) {
              if (value === "Won") {
                swal({
                  title: "",
                  text: oppMsg.won,
                  icon: "warning",
                  buttons: ["Cancel", "Yes, won it!"]
                }).then(willDo => {
                  if (willDo) {
                    this.addAuditAndSave(title, value, transaction)
                  }
                })
              } else {
                const msg =
                  value === "Lost"
                    ? oppMsg.lost
                    : oppMsg.cancel
                swal({
                  title: "",
                  text: msg,
                  icon: "warning",
                  buttons: ["Cancel", "Yes"]
                }).then(willDo => {
                  if (willDo) {
                    this.addAuditAndSave(title, value, transaction)
                  }
                })
              }
            } else {
              this.props.addAuditLog({
                userName: user.userFirstName,
                log: `In summary transaction ${title ||
                "empty"} change to ${value}`,
                date: new Date(),
                key: "summary"
              })
            }
          }
        )
      }
    } else {
      this.setState(
        {
          transaction,
          payload: {
            [name]: tranTextChange[name]
          },
          relatedSave: false
        },
        () => {
          !statusChange ? this.addAuditAndSave(title, value, transaction) : null
        }
      )
    }
  }

  addAuditAndSave = (title, value, transaction) => {
    const { user } = this.props
    if (title && value) {
      this.props.addAuditLog({
        userName: user.userFirstName,
        log: `In summary transaction ${title || "empty"} change to ${value}`,
        date: new Date(),
        key: "summary"
      })
    }
    this.onConfirmationSave(transaction)
  }

  onConfirmationSave = transaction => {
    const { email, payload, statusChange } = this.state
    // if (payload.rfpParAmount) {
    //   payload.rfpParAmount = parseInt(
    //     payload.rfpParAmount.replace(/,/g, "") || 0,
    //     10
    //   )
    // }
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState(
      {
        modalState: false
      },
      () => {
        updateTransaction(tranId, "summary", payload, async res => {
          //eslint-disable-line
          if (res && res.status === 200) {
            toast("Transaction updated successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(tranId)
            if (statusChange) {
              this.setState(
                {
                  statusChange: false,
                  transaction: {
                    ...this.state.transaction,
                    ...payload
                  }
                },
                async () => {
                  this.props.onStatusChange(payload.rfpTranStatus)
                  await sendEmailAlert(emailParams)
                }
              )
            } else {
              this.setState({
                statusChange: false,
                transaction: {
                  ...transaction,
                  ...payload
                }
              })
            }
          } else {
            toast("Something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
          }
        })
      }
    )
  }

  onCancel = () => {
    this.setState({
      debtIssuance: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      }
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  pdfDownload = () => {
    const { transaction } = this.state
    const {logo, user} = this.props
    const position = user && user.settings
    let text = ""
    if (transaction.rfpParticipants) {
      transaction.rfpParticipants.forEach(item => {
        text += `${item.rfpParticipantFirmName} ${item.rfpParticipantContactName ? `(${item.rfpParticipantContactName})` : ""},\n`
      })
    }
    const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
    const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.rfpTransNotes || "--"
    const tableData = [
      {
        titles: ["Summary"],
        description: "",
        headers: [
          "Issuer name",
          "Obligor",
          "Issue name",
          "Project Description",
          "Borrowing Form",
          "Schedule highlights",
          "Principal Amount / Par Value",
          "Security",
          "Coupon / Rate Type",
          "Tenor / Maturities",
          "Participants Firm",
          "Notes"
        ],
        rows: [
          [
            `${transaction.rfpTranIssuerFirmName}`,
            `${
              transaction.rfpTranIsConduitBorrowerFlag
                ? transaction.rfpTranIsConduitBorrowerFlag
                : "--"
            }`,
            `${
              transaction.rfpTranIssueName ? transaction.rfpTranIssueName : "--"
            }`,
            `${transaction.rfpTranProjectDescription}`,
            `${transaction.rfpTranType ? transaction.rfpTranType : "--"}`,
            `${transaction.rfpTranType ? transaction.rfpTranType : "--"}`,
            `${transaction.rfpParAmount ? transaction.rfpParAmount : "--"}`,
            `${
              transaction.rfpTranSecurityType
                ? transaction.rfpTranSecurityType
                : "--"
            }`,
            "-- Rate",
            "--",
            `${transaction.rfpParticipants ? text : "--"}`,
            `${actNotes || "--"}`
          ]
        ]
      }
    ]
    pdfTableDownload("", tableData, `${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription || ""} summary.pdf`, logo, position)
  }

  deleteRelatedTran = async (relatedId, relTranId, relType, relatedType) => {
    const { transaction } = this.props
    const query = `RFP?tranId=${transaction._id}&relatedId=${relatedId}&relTranId=${relTranId}&relType=${relType}&relatedType=${relatedType}`
    const related = await pullRelatedTransactions(query)
    if(related && related.done){
      toast("related transaction removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState({
        relatedTransactions: related && related.relatedTransaction || []
      })
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  render() {
    const {
      modalState,
      email,
      debtIssuance,
      relatedTransactions,
      tranStatus,
      isSaveDisabled,
      relatedTypes,
      securityType,
      errorMessages,
      relatedSave,
      integerError,
      tranTextChange
    } = this.state
    const { participants, onParticipantsRefresh, nav2 } = this.props
    const transaction = this.state.transaction ? this.state.transaction : {}
    const canEditTran =
      (this.props.tranAction && this.props.tranAction.canEditTran) || false
    const loading = () => {
      return <Loader />
    }

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={
            relatedSave === true
              ? this.onRelatedtranSave
              : this.onConfirmationSave
          }
        />
        <TranSummaryStatus
          disabled={!canEditTran}
          tranStatus={tranStatus}
          selectedStatus={transaction.rfpTranStatus || ""}
          pdfDownload={this.pdfDownload}
          tranName={transaction.rfpTranName || ""}
          transaction={transaction || {}}
          onChange={this.onInputChange}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Activity Summary"
              >
                {activeAccordions.includes(0) && (
                  <ActivitySummary
                    canEditTran={canEditTran}
                    securityType={securityType}
                    onTextInputChange={this.onChangeTextInput}
                    onBlur={this.onBlur}
                    integerError={integerError || {}}
                    transaction={transaction}
                    onChange={this.onInputChange}
                    tranTextChange={tranTextChange}
                  />
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Related transactions"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(1) && (
                  <ReletedTranGlob
                    relatedTypes={relatedTypes}
                    transaction={transaction}
                    errorMessages={errorMessages}
                    isSaveDisabled={isSaveDisabled}
                    item={debtIssuance}
                    tranId={nav2}
                    canEditTran={canEditTran || false}
                    onRelatedTranSelect={this.onRelatedTranSelect}
                    onRelatedtranCancel={this.onCancel}
                    onChange={this.onChange}
                    onRelatedtranSave={this.onModalSave}
                    relatedTransactions={relatedTransactions}
                    deleteRelatedTran={this.deleteRelatedTran}
                  />
                )}
              </RatingSection>
            </div>
          )}
        />
        { transaction.OnBoardingDataMigrationHistorical ? <HistoricalData data={(transaction && transaction.historicalData) || {}}/> : null}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(Summary)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
