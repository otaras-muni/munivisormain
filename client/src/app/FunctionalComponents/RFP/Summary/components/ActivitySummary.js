import React from "react"
import {
  NumberInput,
  SelectLabelInput,
  TextLabelInput
} from "../../../../GlobalComponents/TextViewBox"
import BorrowerLookup from "Global/BorrowerLookup"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"


const ActivitySummary = ({
  transaction = {},
  canEditTran,
  securityType,
  onChange,
  onBlur,
  onTextInputChange,
  tranTextChange,
  integerError,
}) => {
  const borrowerName = {
    _id: "",
    firmName: tranTextChange.rfpTranIsConduitBorrowerFlag || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.rfpTransNotes || "--"
  return (
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          {transaction.rfpTranIssuerFirmName ?
            <small style={{fontSize: 15}}>{getTranEntityUrl("Issuer", transaction.rfpTranIssuerFirmName, transaction.rfpTranIssuerId, "")}</small> : <small>--</small>
          }
        </div>
      </div>
      {/* <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Obligor</p>
        </div>
        <div className="column">
          <a>
            <small className="innerSummaryTitle">{transaction.rfpTranIsConduitBorrowerFlag || "--"}</small>
          </a>
        </div>
      </div> */}
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Issue name (as in OS)</p>
        </div>
        <div className="column">
          <TextLabelInput
            title="Issue name"
            name="rfpTranIssueName"
            value={tranTextChange.rfpTranIssueName}
            placeholder=""
            onBlur={onBlur}
            onChange={onTextInputChange}
            disabled={
              !canEditTran ||
              transaction.rfpTranStatus === "Cancelled" ||
              transaction.rfpTranStatus === "Closed" ||
              false
            }
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">RFP Sub Type</p>
        </div>
        <div className="column">
          <small>{transaction.rfpTranSubType}</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">
            Project Description (internal)
          </p>
        </div>
        <div className="column">
          <TextLabelInput
            title="Project Description"
            name="rfpTranProjectDescription"
            value={tranTextChange.rfpTranProjectDescription}
            onBlur={onBlur}
            onChange={onTextInputChange}
            disabled={
              !canEditTran ||
              transaction.rfpTranStatus === "Cancelled" ||
              transaction.rfpTranStatus === "Closed" ||
              false
            }
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrower or Obligor Name</p>
        </div>
        <div className="column">
          <BorrowerLookup
            entityName={borrowerName}
            onChange={(e) => onChange({ target: { name: "rfpTranIsConduitBorrowerFlag", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.rfpTranIsConduitBorrowerFlag && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule highlights</p>
        </div>
        <div className="column">
          <small>
            <p>---</p>
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Principal Amount / Par Value</p>
        </div>
        <div className="column">
          <NumberInput
            name="rfpParAmount"
            title="Par Amount"
            prefix="$"
            error={integerError.rfpParAmount || ""}
            value={tranTextChange.rfpParAmount || 0}
            onChange={onTextInputChange}
            onBlur={onBlur}
            disabled={!canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Estimated Revenue</p>
        </div>
        <div className="column">
          <NumberInput
            name="rfpEstimatedRev"
            title="Estimated Revenue"
            prefix="$"
            error={integerError.rfpEstimatedRev || ""}
            value={tranTextChange.rfpEstimatedRev || 0}
            disabled={!canEditTran || false}
            onChange={onTextInputChange}
            onBlur={onBlur}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Security</p>
        </div>
        <div className="column">
          <SelectLabelInput
            list={securityType || []}
            name="rfpTranSecurityType"
            value={tranTextChange.rfpTranSecurityType || ""}
            onChange={onChange}
            disabled={!canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon / Rate Type:</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{"--"} Rate</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small>---</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Participants Firm</p>
        </div>
        <div className="column">
          <p>
            {transaction.rfpParticipants ?
              transaction.rfpParticipants.map((item, i) => (
                <p key={i}>
                  {item.rfpParticipantContactId ? (
                    <div style={{fontSize: 15}}>{getTranUserUrl("", `${item.rfpParticipantFirmName} (${item.rfpParticipantContactName})${transaction.rfpParticipants.length - 1 === i ? "" : ","}`, item.rfpParticipantContactId, "")}</div>
                  ) :
                    <div style={{fontSize: 15}}>{getTranEntityUrl("", `${item.rfpParticipantFirmName}${transaction.rfpParticipants.length - 1 === i ? "" : ","}`, item.rfpParticipantFirmId, "")}</div>
                  }
                </p>
              ))
              : null}
          </p>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notes / Instructions</p>
        </div>
        <div className="column">
          <small>{actNotes || "--"}</small>
        </div>
      </div>
    </div>
  )
}


export default ActivitySummary
