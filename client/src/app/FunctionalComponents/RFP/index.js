import React from "react"
import {Link, NavLink } from "react-router-dom"
import { connect } from "react-redux"
import Manage from "./Manage"
import Distribute from "./Distribute"
import CheckInTrack from "./CheckInTrack"
import Audit from "../../GlobalComponents/Audit"
import {
  makeEligibleTabView,
  makeEligibleTabViewForRFP
} from "GlobalUtils/helpers"
import Summary from "./Summary"
import {
  fetchTransaction,
  isValidForRfpSettings
} from "../../StateManagement/actions/TransactionDistribute"
import CONST, { activeStyle } from "../../../globalutilities/consts"
import Loader from "../../GlobalComponents/Loader"
import { fetchParticipantsAndOtherUsers } from "../../StateManagement/actions/Transaction"

const TABS = [
  { path: "summary", label: "Summary" },
  { path: "distribute", label: "Distribute" },
  { path: "manage", label: "Manage" },
  { path: "check-track", label: "Check-n-Track" },
  { path: "audit-trail", label: "Activity Log" }
]

class RFP extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rfpId: this.props.nav2,
      option: this.props.nav3 || "summary",
      tabs: [],
      isValid: false,
      loading: true
    }
  }

  componentWillMount() {
    const { user, nav, nav1, nav2, loginEntity } = this.props
    const manage = [
      "manageresponse",
      "managequestions",
      "managequemedia",
      /* "managediscuss", */ "manageevaluate",
      "managedecide"
    ]
    const nav3 =
      manage.indexOf(this.props.nav3) !== -1 ? "manage" : this.props.nav3
    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])

    if (this.props.nav3 === "manage") {
      this.props.history.push(`/rfp/${this.props.nav2}/manageresponse`)
    } else if (nav2) {
      fetchTransaction(nav3, nav2, async transaction => {
        if (transaction && transaction.rfpTranClientId) {
          const isEligible1 = await makeEligibleTabViewForRFP(
            user,
            transaction,
            nav3,
            allEligibleNav,
            transaction.rfpTranClientId
          )

          isEligible1.canEditTran = CONST.oppDecline.indexOf(transaction.rfpTranStatus) !== -1 ? false : isEligible1.canEditTran
          isEligible1.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.rfpTranStatus) === -1 && isEligible1.canEditDocument

          if(transaction.rfpParticipants && loginEntity && ["Third Party"].indexOf(loginEntity.relationshipToTenant) !== -1){
            transaction.rfpParticipants = transaction.rfpParticipants.filter(part => part.rfpParticipantFirmId === loginEntity.entityId)
          }

          console.log(isEligible1)
          if (!isEligible1.canEditTran && !isEligible1.canViewTran) {
            this.props.history.push("/dashboard")
          } else {
            if (isEligible1.view && isEligible1.view.indexOf(nav3) === -1 && (isEligible1.edit && isEligible1.edit.indexOf(nav3) === -1)) {
              if (isEligible1.view) {
                const view = isEligible1.view[0] || ""
                this.props.history.push(`/${nav1}/${nav2}/${view}`)
              } else {
                const view = isEligible1.edit[0] || ""
                this.props.history.push(`/${nav1}/${nav2}/${view}`)
              }
            } else {
              const participants = await fetchParticipantsAndOtherUsers(nav2)
              this.setState(
                {
                  transaction,
                  participants,
                  loading: false,
                  tranAction: isEligible1
                },
                () => {
                  this.checkIsValid()
                }
              )
            }
          }
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  onStatusChange = status => {
    const { tranAction } = this.state
    tranAction.canEditTran =
      CONST.oppDecline.indexOf(status) !== -1 ? false : tranAction.canEditTran
    this.setState({
      tranAction
    })
  }

  checkIsValid = async () => {
    const { nav3 } = this.props
    const { tranAction } = this.state
    const res = await isValidForRfpSettings(this.props.nav2)
    if (res.status === 200 && typeof res.data === "boolean" && res.data) {
      const tabs = [
        { path: "summary", label: "Summary" },
        { path: "distribute", label: "Distribute" },
        { path: "manage", label: "Manage" },
        { path: "check-track", label: "Check-n-Track" },
        { path: "audit-trail", label: "Activity Log" }
      ]
      const allTab = tranAction.edit.concat(tranAction.view)

      const notIn = tabs.filter(ftab => allTab.indexOf(ftab.path) === -1)
      notIn.forEach(nI => {
        const notInIndex = tabs.findIndex(ftab => ftab.path === nI.path)
        tabs.splice(notInIndex, 1)
      })

      this.setState({
        isValid: res.data,
        tabs
      })
    } else {
      const tabs = [
        { path: "summary", label: "Summary" },
        { path: "distribute", label: "Distribute" },
        { path: "check-track", label: "Check-n-Track" },
        { path: "audit-trail", label: "Activity Log" }
      ]

      const allTab = tranAction.edit.concat(tranAction.view)

      const notIn = tabs.filter(ftab => allTab.indexOf(ftab.path) === -1)
      notIn.forEach(nI => {
        const notInIndex = tabs.findIndex(ftab => ftab.path === nI.path)
        tabs.splice(notInIndex, 1)
      })

      this.setState({
        isValid: res.data,
        tabs
      },() => {
        if (
          nav3 !== "distribute" &&
          nav3 !== "summary" &&
          nav3 !== "check-track" &&
          nav3 !== "audit-trail"
        ) {
          this.props.history.push(`/rfp/${this.props.nav2}/distribute`)
        }
      })
      /* this.setState(
        {
          isValid: res.data,
          tabs: [
            { path: "summary", label: "Summary" },
            { path: "distribute", label: "Distribute" },
            { path: "check-track", label: "Check-n-Track" },
            { path: "audit-trail", label: "Activity Log" }
          ]
        },
        () => {
          if (
            nav3 !== "distribute" &&
            nav3 !== "summary" &&
            nav3 !== "check-track" &&
            nav3 !== "audit-trail"
          ) {
            this.props.history.push(`/rfp/${this.props.nav2}/distribute`)
          }
        }
      ) */
    }
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderTabs = (tabs, rfpId, option) => {
    const manage = [
      "manageresponse",
      "managequestions",
      "managequemedia",
      "manageevaluate",
      "managedecide"
    ]
    const nav3 = manage.indexOf(option) !== -1 ? "manage" : option

    const isActive = (to, set) => (match, location) => {
      // console.log(`********to********${to}`, `*******set********${set}`)
      return to === set
    }

    return tabs.map(t => (
      <li key={t.path}>
        <NavLink
          to={`/rfp/${rfpId}/${t.path}`}
          activeStyle={activeStyle}
          isActive={isActive(t.path, nav3)}
        >
          {t.label}
        </NavLink>
      </li>
    ))
  }

  renderSelectedView = (dealId, option) => {
    const { tranAction, transaction, participants } = this.state
    const props = this.props
    switch (option) {
    case "manage":
    case "manageresponse":
    case "managequestions":
    case "managequemedia":
    // case "managediscuss" :
    case "manageevaluate":
    case "managedecide":
      return (
        <Manage
          {...props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "distribute":
      return (
        <Distribute
          {...props}
          transaction={transaction}
          isValid={this.state.isValid}
          checkIsValid={this.checkIsValid}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "check-track":
      return (
        <CheckInTrack
          {...props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "audit-trail":
      return (
        <Audit {...props} transaction={transaction} tranAction={tranAction} />
      )
    case "summary":
      return (
        <Summary
          {...props}
          transaction={transaction}
          nav2={this.props.nav2}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
          onStatusChange={this.onStatusChange}
        />
      )
    default:
      return <p>{option}</p>
    }
  }

  renderViewSelection = (dealId, option) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, dealId, option)}</ul>
    </nav>
  )

  render() {
    const { loading, rfpId, option } = this.state
    if (!rfpId) {
      return <div>No Transaction found!</div>
    }

    if (loading) {
      return <Loader />
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(rfpId, option)}
            </div>
          </div>
        </div>
        {/* <section className="hero is-small is-link">
          <div className="hero-body">
            <div className="columns is-vcentered">
              <div className="column">
                <div className="subtitle">
                  <nav className="breadcrumb has-arrow-separator is-small is-right" aria-label="breadcrumbs">
                    <ul>
                      <li>
                        <a>Transactions</a>
                      </li>
                      <li>
                        <a>RFP Process for Client</a>
                      </li>
                      <li className="is-active">
                        <a>{option}</a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>

          <div className="hero-foot">
            <div className="container">
              {this.renderViewSelection(rfpId, option)}
            </div>
          </div>
        </section> */}
        <section id="main">{this.renderSelectedView(rfpId, option)}</section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(
  mapStateToProps,
  null
)(RFP)
