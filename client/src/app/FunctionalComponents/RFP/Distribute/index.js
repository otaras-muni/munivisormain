import React from "react"
import { withRouter, Link } from "react-router-dom"
import swal from "sweetalert"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import {formatPhoneNumber} from "react-phone-number-input"
import RatingSection from "../../../GlobalComponents/RatingSection"
import TableHeader from "../../../GlobalComponents/TableHeader"
import Accordion from "../../../GlobalComponents/Accordion"
import RfpEvaluationSection from "./components/RfpEvaluationSection"
import RfpSupplierSection from "./components/RfpSupplierSection"
import RfpContactsSection from "./components/RfpContactsSection"
import { getPicklistByPicklistName, generateDistributionPDF } from "GlobalUtils/helpers"
import { fetchAssigned } from "../../../StateManagement/actions/CreateTransaction/index"
import {
  postProcesstasks,
  pullTransactionDoc,
  putRfpTransaction,
  putTransactionDocStatus,
  sendEmailAlert,
  postRFPNotes
} from "../../../StateManagement/actions/Transaction"
import {
  fetchRFPSupplierContacts,
  updateTransaction,
  pullDistribute
} from "../../../StateManagement/actions/TransactionDistribute"
import { RfpEvaluationValidate, RfpParticipantsValidate, RfpProcessValidate } from "./Validation/TransDistribute"
import Loader from "../../../GlobalComponents/Loader"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import CONST, { ContextType } from "../../../../globalutilities/consts"
import "./scss/distribute.scss"
import SendEmailModal from "../../../GlobalComponents/SendEmailModal"
import DocModalDetails from "../../docs/DocModalDetails"
import TranDocuments from "../../../GlobalComponents/TranDocuments"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import TransactionNotes from "../../../GlobalComponents/TransactionNotes"

const cols = [
  [
    { name: "Contacts Database<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Evaluator Role<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Name<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Email<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Phone" },
    { name: "Add to DL" },
    { name: "Action" }
  ],
  [
    { name: "Contact Database<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Contact For<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Name<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Email<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Phone" },
    { name: "Add to DL" },
    { name: "Action" }
  ],
  [
    { name: "Firm Name<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Contact Database<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Name<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Email<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Phone" },
    { name: "Add to DL" },
    { name: "Action" }
  ]
]

const usersList = [
  { name: "Issuer 1", id: 1 },
  { name: "Issuer 2", id: 2 },
  { name: "Issuer 3", id: 3 },
  { name: "Issuer 4", id: 4 }
]

const rfpEvaluationTeam = {
  rfpSelEvalContactId: "",
  rfpSelEvalFirmId: "",
  rfpSelEvalRealFirstName: "",
  rfpSelEvalRealLastName: "",
  rfpSelEvalRealEmailId: "",
  rfpSelEvalRole: "",
  rfpSelEvalContactName: "",
  rfpSelEvalEmail: "",
  rfpSelEvalPhone: "",
  rfpSelEvalAddToDL: false
}

const rfpProcessContacts = {
  rfpProcessContactId: "",
  rfpProcessFirmId: "",
  rfpProcessContactRealFirstName: "",
  rfpProcessContactRealLastName: "",
  rfpProcessContactRealEmailId: "",
  rfpContactFor: "",
  rfpContactName: "",
  rfpContactEmail: "",
  rfpContactPhone: "",
  rfpContactAddToDL: false
}

const rfpParticipants = {
  rfpParticipantFirmId: "",
  rfpParticipantRealFirmName: "",
  rfpParticipantRealMSRBType: "",
  rfpParticipantRealFirstName: "",
  rfpParticipantRealLastName: "",
  rfpParticipantRealEmailId: "",
  rfpParticipantFirmName: "",
  rfpParticipantContactName: "",
  rfpParticipantContactEmail: "",
  rfpParticipantContactPhone: "",
  rfpParticipantContactAddToDL: false
}

class TransactionDistribute extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      errorMessages: {},
      transId: "",
      commityMembers: usersList,
      rfpEvaluationTeam: [rfpEvaluationTeam],
      rfpProcessContacts: [rfpProcessContacts],
      rfpParticipants: [rfpParticipants],
      tempRfpParticipants: [rfpParticipants],
      evaluationCommity: [],
      processContacts: [],
      suppliersList: [],
      documentsList: [],
      tranNotes: [],
      isSummary: false,
      bucketName: CONST.bucketName,
      fileName: "",
      signedS3Url: "",
      selectedDocId: "",
      showSigneeInput: "",
      waiting: false,
      loading: true,
      doc: {},
      transaction: {},
      showModal: false,
      dropDown: {
        rfpEvalTeam: [],
        rfpContactFor: [],
        rfpFirmName: [],
        rfpDocTypes: [],
        rfpDocStatus: [],
        rfpDocAction: [],
        relatedTypes: []
      },
      userList: [],
      filterTrans: [],
      rfpTranRelatedTo: [],
      rfpTranRelatedType: "Precedes",
      isRelatedSaveDisabled: false,
      isSaveDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
        sendDocEmailLinks: true
      },
      manageBidPacketShow: false,
      confirmAlert: CONST.confirmAlert,
      isEditable: {},
      docActonModal: false
    }
  }

  async componentDidMount() {
    const { transaction, nav2 } = this.props
    let result = await getPicklistByPicklistName([
      "LKUPRELATEDTYPE",
      "LKUPRFPEVALTEAM",
      "LKUPRFPCONTACTFOR",
      "LKUPFIRMNAME",
      "LKUPRFPDOCTYPES",
      "LKUPDOCSTATUS",
      "LKUPDOCACTION"
    ])
    result = (result.length && result[1]) || {}

    if (transaction && transaction.rfpTranClientId) {
      const ids = `${transaction.rfpTranClientId},${
        transaction.rfpTranIssuerId
      }`
      fetchAssigned(ids, res => {
        res.usersList && res.usersList.forEach(eUser => {
          // eUser.group = eUser.entityId === user.entityId ? "Tenant" : eUser.entityId === transaction.rfpTranIssuerId ? "Issuer" : ""
          eUser.group = eUser.userFirmName || ""
        })

        this.setState(
          prevState => ({
            transId: nav2,
            transaction,
            loading: false,
            rfpEvaluationTeam: (transaction &&
              transaction.rfpEvaluationTeam &&
              transaction.rfpEvaluationTeam.length && [
              ...transaction.rfpEvaluationTeam
            ]) || [rfpEvaluationTeam],
            rfpProcessContacts: (transaction &&
              transaction.rfpProcessContacts &&
              transaction.rfpProcessContacts.length && [
              ...transaction.rfpProcessContacts
            ]) || [rfpProcessContacts],
            rfpParticipants: (transaction &&
              transaction.rfpParticipants &&
              transaction.rfpParticipants.length && [
              ...transaction.rfpParticipants
            ]) || [rfpParticipants],
            documentsList:
              (transaction &&
                transaction.rfpBidDocuments &&
                transaction.rfpBidDocuments.length && [
                ...transaction.rfpBidDocuments
              ]) ||
              [],
            isEditable: {
              rfpEvaluationTeam: (transaction && transaction.rfpEvaluationTeam && transaction.rfpEvaluationTeam.length) ? "" : 0,
              rfpProcessContacts: (transaction && transaction.rfpProcessContacts && transaction.rfpProcessContacts.length) ? "" : 0,
              rfpParticipants: (transaction && transaction.rfpParticipants && transaction.rfpParticipants.length) ? "" : 0,
            },
            dropDown: {
              ...this.state.dropDown,
              relatedTypes: result.LKUPRELATEDTYPE || [
                "Bond Hedge",
                "Loan Hedge"
              ],
              rfpEvalTeam: result.LKUPRFPEVALTEAM,
              rfpContactFor: result.LKUPRFPCONTACTFOR,
              rfpFirmName: result.LKUPFIRMNAME,
              rfpDocTypes: result.LKUPRFPDOCTYPES,
              rfpDocStatus: result.LKUPDOCSTATUS,
              rfpDocAction: result.LKUPDOCACTION
            },
            evaluationCommity: res.usersList,
            processContacts: res.usersList,
            tranNotes: transaction && transaction.tranNotes || [],
            email: {
              ...prevState.email,
              subject: `Transaction - ${transaction.rfpTranIssueName ||
                transaction.rfpTranProjectDescription} - Notification`
            }
          }),
          () => {
            this.fetchSupplier(transaction.rfpTranClientId, transaction.rfpTranSubType)
          }
        )
      })
    } else {
      this.props.history.push("/dashboard")
    }
  }

  fetchSupplier = (id, type) => {
    // const { isEditable } = this.state
    const {rfpParticipants, tempRfpParticipants} = this.state
    fetchRFPSupplierContacts(
      id, type, "RFP",
      res => {
        const data = res && res.suppliersList && res.suppliersList.length
          ? (rfpParticipants && rfpParticipants.length ? cloneDeep(rfpParticipants) : cloneDeep(tempRfpParticipants)) : []
        this.setState({
          suppliersList: res && res.suppliersList,
          userList: res && res.userList,
          rfpParticipants: data,
          // isEditable: edit ? {} : isEditable
        })
      }
    )
  }

  onChange = e => {
    if (e.target.name === "actTranRelatedType") {
      this.setState(
        {
          rfpTranRelatedTo: [],
          rfpTranRelatedType: e.target.value
        },
        () => {
          this.onFilterRelatedTrans()
        }
      )
    } else {
      this.setState = {
        [e.target.name]: e.target.value
      }
    }
  }

  onReset = key => {
    const { transaction, suppliersList, isEditable } = this.state
    const auditLogs = this.props.auditLogs.filter(x => x.category !== key)
    if (transaction && transaction.rfpTranClientId) {
      this.setState({
        [key]:
          key === "rfpEvaluationTeam"
            ? [...transaction.rfpEvaluationTeam]
            : key === "rfpProcessContacts"
              ? [...transaction.rfpProcessContacts]
              : key === "rfpParticipants"
                ? [...transaction.rfpParticipants]
                : [],
        isEditable: {
          ...isEditable,
          [key]: ""
        }
      })
    } else {
      this.setState({
        [key]:
          key === "rfpEvaluationTeam"
            ? [rfpEvaluationTeam]
            : key === "rfpProcessContacts"
              ? [rfpProcessContacts]
              : key === "rfpParticipants"
                ? suppliersList.length
                  ? [rfpParticipants]
                  : []
                : [],
        isEditable: {
          ...isEditable,
          [key]: ""
        }
      })
    }
    this.props.updateAuditLog(auditLogs)
  }

  onAdd = (key) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    const {isEditable} = this.state
    const insertRow = cloneDeep(this.state[key])
    insertRow.push(key === "rfpEvaluationTeam" ? rfpEvaluationTeam : key === "rfpProcessContacts" ? rfpProcessContacts :
      key === "rfpParticipants" ? rfpParticipants : {})
    this.props.addAuditLog({
      userName,
      log: `${
        key === "rfpEvaluationTeam" ? "Selection and Evaluation Committee" 
          : key === "rfpProcessContacts" ? "RFP Process Contacts" 
            : key === "rfpParticipants" ? "RFP Recipient Distribution List" : key
      } Add new Item`,
      date: new Date(),
      key
    })

    if (isEditable[key] || isEditable[key] === 0 || isEditable[key] === "") {
      if(key === "rfpParticipants" && (this.state.suppliersList === undefined || !this.state.suppliersList.length)) {
        return swal("Warning", "No Supplier List or RFP Recipient Distribution List in it", "warning")
      } else if (isEditable[key] || isEditable[key] === 0) {
        return swal("Warning", `First save the current ${
          key === "rfpEvaluationTeam" ? "Selection and Evaluation Committee"
            : key === "rfpProcessContacts" ? "RFP Process Contacts"
              : key === "rfpParticipants" ? "RFP Recipient Distribution List" : key
        }`, "warning")
      }
    }

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: insertRow.length ? insertRow.length - 1 : 0
      }
    })
  }

  onEdit = (key, index) => {
    this.props.addAuditLog({
      userName: this.state.userName,
      log: `one row edited in ${
        key === "rfpEvaluationTeam" ? "Selection and Evaluation Committee"
          : key === "rfpProcessContacts" ? "RFP Process Contacts"
            : key === "rfpParticipants" ? "RFP Recipient Distribution List" : key
      }`,
      date: new Date(),
      key
    })
    if (this.state.isEditable[key]) {
      swal("Warning", `First save the current ${
        key === "rfpEvaluationTeam" ? "Selection and Evaluation Committee"
          : key === "rfpProcessContacts" ? "RFP Process Contacts"
            : key === "rfpParticipants" ? "RFP Recipient Distribution List" : key
      }`, "warning")
      return
    }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index
      }
    })
  }

  onCancel = key => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: ""
      }
    })
  }

  onRemove = (key, i) => {
    const { confirmAlert, isEditable, transaction } = this.state
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    const message = key === "rfpEvaluationTeam" ? "Selection and Evaluation Committee"
      : key === "rfpProcessContacts" ? "RFP Process Contacts"
        : key === "rfpParticipants" ? "RFP Recipient Distribution List" : key

    if(isEditable[key] || isEditable[key] === 0) { swal("Warning", `First save the current ${message}`, "warning"); return }

    confirmAlert.text = `You want to delete this ${message}?`
    const items = this.state[key][i]
    const delId = items && items._id
    swal(confirmAlert).then(willDelete => {
      if (key && delId) {
        if (willDelete) {
          if (!Object.keys(items).length && !items._id) {
            swal("Warning", "Minimum one row is required", "warning")
            return
          }

          this.props.addAuditLog({userName, log: `Item removed in ${message}`, date: new Date(), key})
          pullDistribute(transaction._id, key, items._id, (res) => {
            if (res && res.status === 200) {
              toast(`${message} has been deleted!`, {
                autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,
              })
              this.props.submitAuditLogs(transaction._id)
              const distributeObj = key === "rfpEvaluationTeam" ? [cloneDeep(rfpEvaluationTeam)] :
                key === "rfpProcessContacts" ? [cloneDeep(rfpProcessContacts)] :
                  key === "rfpParticipants" ? [cloneDeep(rfpParticipants)] :  []
              const list = (res && res.data && res.data[key] && res.data[key].length) ? cloneDeep(res.data[key]) : distributeObj
              this.setState({
                [key]: list,
                isEditable: {
                  ...isEditable,
                  [key]: (res && res.data && res.data[key] && !res.data[key].length) ? 0 : ""
                },
                errorMessages: {}
              })
            } else {
              toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            }
          })
        }
      } else {
        const distribute = this.state[key]
        if (willDelete && distribute.length) {
          distribute.splice(i, 1)
          this.setState({
            [key]: distribute,
            isEditable: {},
            errorMessages: {}
          })
        }
      }
    })
  }

  onChangeItem = (item, index, category, change, type) => {
    if(type && type === "checkbox"){
      updateTransaction(this.props.nav2, category, item, async res => {
        const items = this.state[category]
        items.splice(index, 1, item)
        this.setState({
          [category]: items
        })
      })
    } else {
      const items = this.state[category]
      items.splice(index, 1, item)
      this.setState({
        [category]: items
      })
    }
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `${change}`,
      date: new Date(),
      category
    })
  }

  actionButtons = (key, isDisabled, canEditTran, RatingSection) => {
    //eslint-disable-line
    const {transaction} = this.state
    if (!canEditTran) return
    return (
      <div className="field is-grouped">
        {
          RatingSection === 2 ?
            <div className="control">
              <a className="fa fa-refresh"
                style={{
                  fontSize: 25,
                  cursor: "pointer",
                  padding: "0 0 0 10px"
                }}
                onClick={() => this.fetchSupplier(transaction.rfpTranClientId, transaction.rfpTranSubType)}
              />
            </div>
            : null
        }

        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key, RatingSection)}
            disabled={isDisabled}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset(key)}
            disabled={isDisabled}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  onSubmit = async (errorFlag, name, index) => {
    const {
      rfpEvaluationTeam,
      rfpProcessContacts,
      rfpParticipants
    } = this.state
    let payload = {}
    let key = ""
    if (name === "rfpEvaluation") {
      payload = rfpEvaluationTeam[index]
      key = "rfpEvaluationTeam"
    } else if (name === "rfpProcess") {
      payload = rfpProcessContacts[index]
      key = "rfpProcessContacts"
    } else if (name === "rfpParticipants") {
      payload = rfpParticipants[index]
      key = "rfpParticipants"
    } else {
      const evaluationData = []
      const reqEvaluation = ["rfpSelEvalContactId", "rfpSelEvalFirmId", "rfpSelEvalRole", "rfpSelEvalPhone", "rfpSelEvalEmail", "rfpSelEvalContactName"]
      rfpEvaluationTeam.forEach((data, i) => {
        if(i === 0 ) {
          return evaluationData.push(data)
        }
        const isValid = reqEvaluation.every(s => data[s])
        if(isValid) {
          evaluationData.push(data)
        }
      })

      const contactData = []
      const reqContact = ["rfpProcessContactId", "rfpProcessFirmId", "rfpContactFor", "rfpContactName", "rfpContactEmail", "rfpContactPhone"]
      rfpProcessContacts.forEach((data) => {
        const isValid = reqContact.every(s => data[s])
        if(isValid) {
          contactData.push(data)
        }
      })

      const participantData = []
      const reqParticipants = ["rfpParticipantFirmId", "rfpParticipantFirmName", "rfpParticipantContactName", "rfpParticipantContactEmail", "rfpParticipantContactPhone", "rfpParticipantContactId"]
      rfpParticipants.forEach((data) => {
        const isValid = reqParticipants.every(s => data[s])
        if(isValid) {
          participantData.push(data)
        }
      })

      payload = {
        rfpEvaluationTeam: evaluationData,
        rfpProcessContacts: contactData,
        rfpParticipants: participantData
      }
    }

    const type = "distributeSave"
    let errors = {}
    if (key === "rfpEvaluationTeam") {
      errors = RfpEvaluationValidate(payload)
    } else if (key === "rfpProcessContacts") {
      errors = RfpProcessValidate(payload)
    } else if (key === "rfpParticipants"){
      errors = RfpParticipantsValidate(payload)
    }
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        const message = "Required"
        if (errorMessages.hasOwnProperty(key)) {
          //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) {
            //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    if (errorFlag) {
      this.setState(
        {
          saveType: key
        },
        () => {
          this.onSave(errorFlag, payload)
        }
      )
    } else {
      this.setState({
        modalState: true,
        saveType: type,
        payload
      })
    }
  }

  onSave = (emailFlage, payloads) => {
    const { transaction, saveType, email, documentsList, isEditable } = this.state
    const payload = saveType === "distributeSave" ? this.state.payload : payloads
    const docIds = documentsList.map(doc => doc.docAWSFileLocation)
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        docIds,
        ...email
      }
    }

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false,
        errorMessages: {},
        loading: true,
      },
      async () => {
        updateTransaction(transaction._id, saveType, payload, async res => {
          //eslint-disable-line
          if (res && res.status === 200) {
            const list = (res && res.data && res.data[saveType] && res.data[saveType].length) ? cloneDeep(res.data[saveType]) : []
            let objState = {}
            if(saveType === "distributeSave") {
              objState = {
                rfpEvaluationTeam: res.data && res.data.rfpEvaluationTeam,
                rfpProcessContacts: res.data && res.data.rfpProcessContacts,
                rfpParticipants: res.data && res.data.rfpParticipants,
                isEditable: {}
              }
            } else {
              objState = {
                [saveType]: list,
                isEditable: {
                  ...isEditable,
                  [saveType]: null
                }
              }
            }
            this.setState({
              ...objState,
              errorMessages: {},
              isSaveDisabled: false,
              loading: false,
              saveType: "",
              payload: {},
            }, async () => {
              if (emailFlage) {
                await sendEmailAlert(emailPayload)
              }
            })
            this.props.checkIsValid()
            this.props.submitAuditLogs(transaction._id)
            postProcesstasks(transaction._id)
            toast("Transaction Updated successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
          } else {
            toast("Something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false,
              loading: false
            })
          }
        })
      }
    )
  }

  onModalSave = () => {
    const {documentsList} = this.state
    const docIds = documentsList.map(doc => doc.docAWSFileLocation)
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        docIds,
        ...this.state.email
      }
    }
    this.setState(
      pre => ({
        modalState: !pre.modalState,
        docActonModal: false,
        email: {
          ...pre.email,
          message: ""
        }
      }),
      async () => {
        await sendEmailAlert(emailPayload)
      }
    )
  }

  handleDocDetails = res => {
    this.setState({
      showModal: !this.state.showModal,
      doc: res || {}
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onDeleteDoc = async (documentId, callback) => {
    const res = await pullTransactionDoc(
      this.props.nav2,
      `?tranType=RFP&docId=${documentId}`
    )
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.props.submitAuditLogs(this.props.nav2)
      this.setState(
        {
          documentsList: (res.data && res.data.rfpBidDocuments) || []
        },
        () => {
          this.props.checkIsValid()
          callback({
            status: true,
            documentsList: (res.data && res.data.rfpBidDocuments) || []
          })
        }
      )
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    putRfpTransaction("documents", this.props.nav2, docs, res => {
      if (res && res.status === 200) {
        toast("Bid Packet has been updated!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.props.submitAuditLogs(this.props.nav2)
        this.setState(
          {
            documentsList: (res.data && res.data.rfpBidDocuments) || []
          },
          () => {
            this.props.checkIsValid()
            callback({
              status: true,
              documentsList: (res.data && res.data.rfpBidDocuments) || []
            })
          }
        )
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        [name]: value
      }
      type = "&type=status"
    } else {
      document = {
        ...doc
      }
    }
    const res = await putTransactionDocStatus(this.props.nav2, `RFP${type}`, {
      _id: doc._id,
      ...document
    })
    if (res && res.status === 200) {
      toast("Document has been updated!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.props.submitAuditLogs(this.props.nav2)
      if (name) {
        callback({
          status: true
        })
      } else {
        callback({
          status: true,
          documentsList: (res.data && res.data.document) || []
        })
      }
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  }

  onChangeError = (name, key, index, state) => {
    if (name) {
      this.setState(prevState => ({
        errorMessages: {
          ...prevState.errorMessages,
          [key]: {
            ...(prevState.errorMessages && prevState.errorMessages[key]),
            [index]: {
              ...(prevState.errorMessages &&
                prevState.errorMessages[key] &&
                prevState.errorMessages[key][index.toString()]),
              [name]: ""
            }
          }
        }
      }))
    } else {
      this.setState(prevState => ({
        errorMessages: {
          ...prevState.errorMessages,
          [key]: {
            ...(prevState.errorMessages && prevState.errorMessages[key]),
            [index]: {
              ...(prevState.errorMessages &&
                prevState.errorMessages[key] &&
                prevState.errorMessages[key][index.toString()]),
              ...state
            }
          }
        }
      }))
    }
  }

  distributePDFDownload = async () => {
    const { rfpEvaluationTeam, rfpProcessContacts, rfpParticipants } = this.state
    const { logo, transaction, user } = this.props

    const evaluationData = []
    const processData = []
    const participantsStackData = []

    const participantsData = []

    const evaluationTeamData = []
    const processContactsData = []
    const rfpParticipantsData = []

    rfpEvaluationTeam.forEach(d => {
      if(d && d.rfpSelEvalAddToDL){
        evaluationData.push({
          stack: [
            { text: `Name: ${d.rfpSelEvalRealFirstName || ""} ${d.rfpSelEvalRealLastName || ""}`, bold: true, fontSize: 10, margin: [ 0, 10, 0, 0 ]},
            { text: `Evaluator Role: ${d.rfpSelEvalRole || ""}`, fontSize: 10},
            { text: `E-Mail: ${d.rfpSelEvalEmail || ""}`, fontSize: 10},
            { text: `Phone: ${d.rfpSelEvalPhone || ""}`, fontSize: 10}
          ]
        })
      }
    })

    rfpProcessContacts.forEach(d => {
      if(d && d.rfpContactAddToDL){
        processData.push({
          stack: [
            { text: `Name: ${d.rfpProcessContactRealFirstName || ""} ${d.rfpProcessContactRealLastName || ""}`, bold: true, fontSize: 10, margin: [ 0, 10, 0, 0 ]},
            { text: `Contact For: ${d.rfpContactFor || ""}`, fontSize: 10},
            { text: `E-Mail: ${d.rfpContactEmail || ""}`, fontSize: 10},
            { text: `Phone: ${d.rfpContactPhone || ""}`, fontSize: 10}
          ]
        })
      }
    })

    rfpParticipants.forEach(d => {
      if(d && d.rfpParticipantContactAddToDL){
        participantsStackData.push({
          stack: [
            { text: d && d.rfpParticipantFirmName && d.rfpParticipantFirmName.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
            { text: `Name: ${d.rfpParticipantRealFirstName || ""} ${d.rfpParticipantRealLastName || ""}`, bold: true, fontSize: 10},
            { text: `Phone: ${d.rfpParticipantContactEmail || ""}`, fontSize: 10},
            { text: `E-Mail: ${d.rfpParticipantContactPhone || ""}`, fontSize: 10}
          ]
        })
      }
    })

    if(evaluationData && evaluationData.length || processData && processData.length || participantsStackData && participantsStackData.length){
      if(evaluationData && evaluationData.length){
        const stackData = []
        let duplicateData = []
        let dataIndex = 0
        evaluationData.forEach(d => {
          if(duplicateData.length < 2){
            dataIndex = dataIndex + 1
            const index = [dataIndex - 1]
            duplicateData.push(evaluationData[index])
            if(duplicateData.length === 1){
              stackData.push(duplicateData)
            }
          }
          if (duplicateData.length === 2){
            duplicateData = []
          }
        })
        if(stackData && stackData.length){
          stackData.forEach(stack => {
            if(stack && stack.length === 1){
              stack.push({stack: []})
            }
            if(evaluationTeamData && !evaluationTeamData.length){
              evaluationTeamData.push(
                { columns: [ {stack: [{ text: "Selection and Evaluation Committee".toUpperCase(), alignment: "center", fontSize: 14, bold: true, margin: [ 0, 10, 0, 0 ] }]} ]}
              )
            }
            evaluationTeamData.push({columns: stack}, {marginTop:10, text: [ "" ]})
          })
        }
      }
      if(processData && processData.length){
        const stackData = []
        let duplicateData = []
        let dataIndex = 0
        processData.forEach(d => {
          if(duplicateData.length < 2){
            dataIndex = dataIndex + 1
            const index = [dataIndex - 1]
            duplicateData.push(processData[index])
            if(duplicateData.length === 1){
              stackData.push(duplicateData)
            }
          }
          if (duplicateData.length === 2){
            duplicateData = []
          }
        })
        if(stackData && stackData.length){
          stackData.forEach(stack => {
            if(stack && stack.length === 1){
              stack.push({stack: []})
            }
            if(processContactsData && !processContactsData.length){
              processContactsData.push(
                { columns: [ {stack: [{ text: "RFP Process Contacts".toUpperCase(), alignment: "center", fontSize: 14, bold: true, margin: [ 0, 10, 0, 0 ] }]} ]}
              )
            }
            processContactsData.push({columns: stack}, {marginTop:10, text: [ "" ]})
          })
        }
      }
      if(participantsStackData && participantsStackData.length){
        const stackData = []
        let duplicateData = []
        let dataIndex = 0
        participantsStackData.forEach(d => {
          if(duplicateData.length < 2){
            dataIndex = dataIndex + 1
            const index = [dataIndex - 1]
            duplicateData.push(participantsStackData[index])
            if(duplicateData.length === 1){
              stackData.push(duplicateData)
            }
          }
          if (duplicateData.length === 2){
            duplicateData = []
          }
        })
        if(stackData && stackData.length){
          stackData.forEach(stack => {
            if(stack && stack.length === 1){
              stack.push({stack: []})
            }
            if(rfpParticipantsData && !rfpParticipantsData.length){
              rfpParticipantsData.push(
                { columns: [ {stack: [{ text: "Supplier or RFP Recipient Distribution List".toUpperCase(), alignment: "center", fontSize: 14, bold: true, margin: [ 0, 10, 0, 0 ] }]} ]}
              )
            }
            rfpParticipantsData.push({columns: stack}, {marginTop:10, text: [ "" ]})
          })
        }
      }

      if(evaluationTeamData && evaluationTeamData.length){
        participantsData.push(evaluationTeamData)
      }
      if(processContactsData && processContactsData.length){
        participantsData.push(processContactsData)
      }
      if(rfpParticipantsData && rfpParticipantsData.length){
        participantsData.push(rfpParticipantsData)
      }

      await generateDistributionPDF({
        logo,
        firmName: user && user.firmName && user.firmName.toUpperCase() || "",
        clientName: transaction && transaction.rfpTranIssuerFirmName && transaction.rfpTranIssuerFirmName.toUpperCase() || "",
        transactionType: "RFP",
        data: participantsData,
        fileName: `${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription || ""} participants.pdf`
      })
    } else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  pdfDownload = () => {
    const {user, logo, transaction} = this.props
    const position = user && user.settings
    const {
      rfpEvaluationTeam,
      rfpProcessContacts,
      rfpParticipants
    } = this.state
    const pdfParticipants = []
    const pdfEvaluation = []
    const pdfSelection = []
    rfpParticipants &&
      rfpParticipants.forEach(item => {
        if (item.rfpParticipantContactAddToDL) {
          pdfParticipants.push([
            item.rfpParticipantRealFirmName || "--",
            item.rfpParticipantRealMSRBType || "--",
            item.rfpParticipantRealFirstName || "--",
            item.rfpParticipantRealLastName || "--",
            item.rfpParticipantRealEmailId || "--",
            item.rfpParticipantFirmName || "--",
            item.rfpParticipantContactName || "--",
            item.rfpParticipantContactEmail || "--",
            item.rfpParticipantContactPhone || "--",
            item.rfpParticipantContactAddToDL ? "Yes" : "No"
          ])
        }
      })

    rfpEvaluationTeam &&
      rfpEvaluationTeam.forEach(item => {
        if (item.rfpSelEvalAddToDL) {
          pdfEvaluation.push([
            item.rfpSelEvalRealFirstName || "--",
            item.rfpSelEvalRealLastName || "--",
            item.rfpSelEvalRealEmailId || "--",
            item.rfpSelEvalContactName || "--",
            item.rfpSelEvalEmail || "--",
            item.rfpSelEvalPhone || "--",
            item.rfpSelEvalAddToDL ? "Yes" : "No"
          ])
        }
      })

    rfpProcessContacts &&
      rfpProcessContacts.forEach(item => {
        if (item.rfpContactAddToDL) {
          pdfSelection.push([
            item.rfpProcessContactRealFirstName || "--",
            item.rfpProcessContactRealLastName || "--",
            item.rfpProcessContactRealEmailId || "--",
            item.rfpContactFor || "--",
            item.rfpContactName || "--",
            item.rfpContactEmail || "--",
            item.rfpContactPhone || "--",
            item.rfpContactAddToDL ? "Yes" : "No"
          ])
        }
      })

    const tableData = []

    if (pdfEvaluation.length > 0) {
      tableData.push({
        titles: ["Selection & Evaluation"],
        description: "",
        headers: [
          "First Name",
          "Last Name",
          "Email",
          "Contact Name",
          "Contact Email",
          "Contact Phone",
          "Add to DL"
        ],
        rows: pdfEvaluation
      })
    }
    if (pdfSelection.length > 0) {
      tableData.push({
        titles: ["Process Contacts"],
        description: "",
        headers: [
          "First Name",
          "Last Name",
          "Email",
          "Contact For",
          "Contact Name",
          "Contact Email",
          "Contact Phone",
          "Add to DL"
        ],
        rows: pdfSelection
      })
    }
    if (pdfParticipants.length > 0) {
      tableData.push({
        titles: ["RFP Supplier"],
        description: "",
        headers: [
          "Firm Name",
          "MSRB Type",
          "First Name",
          "Last Name",
          "Email",
          "Firm Name",
          "Contact Name",
          "Contact Email",
          "Contact Phone",
          "Add to DL"
        ],
        rows: pdfParticipants
      })
    }
    if((pdfParticipants && pdfParticipants.length) || (pdfEvaluation && pdfEvaluation.length) || (pdfSelection && pdfSelection.length)){
      pdfTableDownload("", tableData, `${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription || ""} participants.pdf`, logo, position)
    }else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  xlDownload = () => {
    const {
      rfpEvaluationTeam,
      rfpProcessContacts,
      rfpParticipants
    } = this.state
    const pdfParticipants = []
    const pdfEvaluation = []
    const pdfSelection = []

    rfpEvaluationTeam &&
      rfpEvaluationTeam.map(item => {
        const data = {
          "Contacts Database": `${item.rfpSelEvalRealFirstName || ""} ${item.rfpSelEvalRealLastName || ""}` || "",
          "Evaluator Role": item.rfpSelEvalRole || "",
          "Name": item.rfpSelEvalContactName || "",
          "Email": item.rfpSelEvalRealEmailId || "",
          "Phone": item.rfpSelEvalPhone || "",
          "Add to DL": item.rfpSelEvalAddToDL || false,
        }
        pdfEvaluation.push(data)
      })

    rfpProcessContacts &&
      rfpProcessContacts.map(item => {
        const data = {
          "Contacts Database": `${item.rfpProcessContactRealFirstName || ""} ${item.rfpProcessContactRealLastName || ""}` || "",
          "Contact For": item.rfpContactFor || "",
          "Name": item.rfpContactName || "",
          "Email": item.rfpProcessContactRealEmailId || "",
          "Phone": item.rfpContactPhone || "",
          "Add to DL": item.rfpContactAddToDL || false
        }
        pdfSelection.push(data)
      })

    rfpParticipants &&
    rfpParticipants.map(item => {
      const data = {
        "Firm Name": item.rfpParticipantRealFirmName || "",
        "Contact Database": `${item.rfpParticipantRealFirstName || ""} ${item.rfpParticipantRealLastName || ""}` || "",
        "Name": item.rfpParticipantContactName || "",
        "Email": item.rfpParticipantContactEmail || "",
        "Phone": item.rfpParticipantContactPhone || "",
        "Add to DL": item.rfpParticipantContactAddToDL || false
      }
      pdfParticipants.push(data)
    })

    const jsonSheets = [
      {
        name: "Selection And Evaluation",
        headers: [
          "Contacts Database",
          "Evaluator Role",
          "Name",
          "Email",
          "Phone",
          "Add to DL",
        ],
        data: pdfEvaluation
      },
      {
        name: "Process Contacts",
        headers: [
          "Contacts Database",
          "Contact For",
          "Name",
          "Email",
          "Phone",
          "Add to DL",
        ],
        data: pdfSelection
      },
      {
        name: "RFP Supplier",
        headers: [
          "Firm Name",
          "Contact Database",
          "Name",
          "Email",
          "Phone",
          "Add to DL"
        ],
        data: pdfParticipants
      }
    ]
    this.setState(
      {
        jsonSheets,
        startXlDownload: true
      },
      () => this.resetXLDownloadFlag
    )
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  handleToggle = () => {
    this.setState({ modalState: true, docActonModal: true })
  }

  onPhoneChange = (e, flag, index, key) => {
    const { rfpEvaluationTeam, rfpProcessContacts, rfpParticipants } = this.state
    const Internationals = e && formatPhoneNumber(e, "International") || ""
    if(flag === "rfpEvaluationTeam"){
      rfpEvaluationTeam[index][key] = Internationals
    } else if(flag === "rfpProcessContacts"){
      rfpProcessContacts[index][key] = Internationals
    } else if(flag === "rfpParticipants"){
      rfpParticipants[index][key] = Internationals
    }
    this.setState({
      rfpEvaluationTeam,
      rfpProcessContacts,
      rfpParticipants
    })
  }

  onNotesSave = (payload, callback) => {
    postRFPNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add Bank Loan Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  render() {
    const {
      doc,
      manageBidPacketShow,
      documentsList,
      modalState,
      email,
      isEditable,
      transaction,
      docActonModal,
      isSaveDisabled,
      dropDown,
      rfpParticipants,
      userList,
      rfpProcessContacts,
      rfpEvaluationTeam,
      evaluationCommity,
      processContacts,
      tranNotes,
      suppliersList
    } = this.state
    const { participants, onParticipantsRefresh } = this.props
    const canEditTran = (this.props.tranAction && this.props.tranAction.canEditTran) || false
    const isSubmit = (rfpEvaluationTeam.length && rfpProcessContacts.length && rfpParticipants.length && documentsList && documentsList.length) || false
    const tags = {
      clientId: transaction.rfpTranIssuerId,
      clientName: `${transaction.rfpTranIssuerFirmName}`,
      tenantId: transaction.rfpTranClientId,
      tenantName: transaction.rfpTranClientFirmName
    }
    let errorMessages = this.state.errorMessages ? this.state.errorMessages : {}
    errorMessages = {
      ...errorMessages,
      rfpEvaluationTeam: errorMessages.rfpEvaluationTeam ? errorMessages.rfpEvaluationTeam : {},
      rfpProcessContacts: errorMessages.rfpProcessContacts ? errorMessages.rfpProcessContacts : {},
      rfpParticipants: errorMessages.rfpParticipants ? errorMessages.rfpParticipants : {}
    }
    const columns1 = canEditTran ? cloneDeep(cols[0]) : cloneDeep(cols[0].filter(col => col.name !== "Action"))
    const columns2 = canEditTran ? cloneDeep(cols[1]) : cloneDeep(cols[1].filter(col => col.name !== "Action"))
    const columns3 = canEditTran ? cloneDeep(cols[2]) : cloneDeep(cols[2].filter(col => col.name !== "Action"))
    const loading = () => <Loader />
    const isDisabledDownload = rfpEvaluationTeam && rfpEvaluationTeam.length && rfpEvaluationTeam && rfpEvaluationTeam[0] && rfpEvaluationTeam[0]._id ||
      rfpProcessContacts && rfpProcessContacts.length && rfpProcessContacts && rfpProcessContacts[0] && rfpProcessContacts[0]._id ||
      rfpParticipants && rfpParticipants.length && rfpParticipants && rfpParticipants[0] && rfpParticipants[0]._id

    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="distributeRFP">
        <div className="columns">
          <div className="column">
            <button
              className="button is-link is-small"
              onClick={this.handleToggle}
            >
              Send Email Alert
            </button>
          </div>
          <div className="column">
            <div className="field is-grouped">
              <div className={`${isDisabledDownload ? "control" : "control isDisabled"}`} onClick={this.distributePDFDownload}>
                <span className="has-text-link">
                  <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                </span>
              </div>
              {/* <div className={`${isDisabledDownload ? "control" : "control isDisabled"}`} onClick={this.pdfDownload}>
                <span className="has-link">
                  <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                </span>
              </div> */}
              <div className={`${isDisabledDownload ? "control" : "control isDisabled"}`} onClick={this.xlDownload}>
                <span className="has-link">
                  <i className="far fa-2x fa-file-excel has-text-link" title="Excel Download"/>
                </span>
              </div>
              <div style={{ display: "none" }}>
                <ExcelSaverMulti
                  label={`${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription || ""} participants`}
                  startDownload={this.state.startXlDownload}
                  afterDownload={this.resetXLDownloadFlag}
                  jsonSheets={this.state.jsonSheets}
                />
              </div>
            </div>
          </div>
        </div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          documentFlag
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={docActonModal ? this.onModalSave : this.onSave}
        />
        <Accordion
          multiple
          boxHidden
          activeItem={[0,1,2]}
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Selection and Evaluation Committee"
                actionButtons={this.actionButtons(
                  "rfpEvaluationTeam",
                  isSaveDisabled,
                  canEditTran, 0
                )}
              >
                {activeAccordions.includes(0) && (
                  <div className="tbl-scroll">
                    {/* commented so that dropdownlist works className="overflow-auto"> */}
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={columns1} />
                      {rfpEvaluationTeam.length
                        ? rfpEvaluationTeam.map((item, index) => {
                          const errors = errorMessages.rfpEvaluationTeam && errorMessages.rfpEvaluationTeam[index.toString()]
                          const isExists = cloneDeep(rfpEvaluationTeam).map(e => e.rfpSelEvalContactId !== item.rfpSelEvalContactId && e.rfpSelEvalContactId)
                          const evalCommity = cloneDeep(evaluationCommity).filter(e => isExists.indexOf(e._id) === -1)
                          const user = cloneDeep(evalCommity).find(e => e.id === item.rfpSelEvalContactId && e.userEmails)
                          const userEmails = (user && user.userEmails.map(u => u.emailId)) || []
                          return (
                            <RfpEvaluationSection
                              key={index}
                              usersList={evalCommity}
                              errors={errors}
                              canEditTran={canEditTran}
                              dropDown={dropDown.rfpEvalTeam}
                              index={index}
                              item={item}
                              onRemove={() =>
                                this.onRemove("rfpEvaluationTeam", index)
                              }
                              userEmails={userEmails}
                              onChangeItem={(changedItem, change, type) =>
                                this.onChangeItem(
                                  changedItem,
                                  index,
                                  "rfpEvaluationTeam",
                                  change,
                                  type
                                )
                              }
                              onBlur={change =>
                                this.onBlur("rfpEvaluationTeam", change)
                              }
                              onPhoneChange={this.onPhoneChange}
                              onChangeError={this.onChangeError}
                              onSave={this.onSubmit}
                              isEditable={isEditable}
                              onEdit={this.onEdit}
                              category="rfpEvaluationTeam"
                              isSaveDisabled={isSaveDisabled}
                              onCancel={this.onCancel}
                            />
                          )
                        })
                        : /* (errorMessages && errorMessages.rfpEvaluationTeam) ? <tr> <td colSpan={7} className="text-error">{errorMessages && errorMessages.rfpEvaluationTeam}</td></tr> : */ null}
                    </table>
                  </div>
                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="RFP Process Contacts"
                actionButtons={this.actionButtons(
                  "rfpProcessContacts",
                  isSaveDisabled,
                  canEditTran, 1
                )}
              >
                {activeAccordions.includes(1) && (
                  <div className="tbl-scroll">
                    {/* commented so that dropdownlist works className="overflow-auto"> */}
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={columns2} />
                      {rfpProcessContacts.length
                        ? rfpProcessContacts.map((item, index) => {
                          const errors = errorMessages.rfpProcessContacts && errorMessages.rfpProcessContacts[index.toString()]
                          const isExists = cloneDeep(rfpProcessContacts).map(e => e.rfpProcessContactId !== item.rfpProcessContactId && e.rfpProcessContactId)
                          const proContacts = processContacts.filter(e => isExists.indexOf(e._id) === -1)
                          const user = cloneDeep(proContacts).find(e => e.id === item.rfpProcessContactId && e.userEmails)
                          const userEmails = (user && user.userEmails.map(u => u.emailId)) || []
                          return (
                            <RfpContactsSection
                              key={index}
                              usersList={proContacts}
                              errors={errors}
                              canEditTran={canEditTran}
                              dropDown={dropDown.rfpContactFor}
                              index={index}
                              item={item}
                              onRemove={() =>
                                this.onRemove("rfpProcessContacts", index)
                              }
                              userEmails={userEmails}
                              onChangeItem={(changedItem, change, type) =>
                                this.onChangeItem(
                                  changedItem,
                                  index,
                                  "rfpProcessContacts",
                                  change,
                                  type
                                )
                              }
                              onBlur={change =>
                                this.onBlur("rfpProcessContacts", change)
                              }
                              onPhoneChange={this.onPhoneChange}
                              onChangeError={this.onChangeError}
                              onSave={this.onSubmit}
                              isEditable={isEditable}
                              onEdit={this.onEdit}
                              category="rfpProcessContacts"
                              isSaveDisabled={isSaveDisabled}
                              onCancel={this.onCancel}
                            />
                          )
                        })
                        : null}
                    </table>
                  </div>
                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(2)}
                title="Supplier or RFP Recipient Distribution List"
                actionButtons={this.actionButtons(
                  "rfpParticipants",
                  isSaveDisabled,
                  canEditTran, 2
                )}
              >
                {activeAccordions.includes(2) && (
                  <div className="tbl-scroll">
                    {/* commented so that dropdownlist works className="overflow-auto"> */}
                    <Link className="level-right" to="/addnew-contact">Add new contact</Link>
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={columns3} />
                      {rfpParticipants.length
                        ? rfpParticipants.map((item, index) => {
                          const errors = errorMessages.rfpParticipants && errorMessages.rfpParticipants[index.toString()]
                          // const suppliers = suppliersList.filter(e => isExists.indexOf(e.id) === -1)
                          const isExistsUser = rfpParticipants.map(e => e.rfpParticipantContactId !== item.rfpParticipantContactId && e.rfpParticipantContactId)
                          const users = (userList && userList.filter(e => e.entityId._id === item.rfpParticipantFirmId && isExistsUser.indexOf(e._id) === -1)) || []
                          const user = (userList && userList.find(e => e._id === item.rfpParticipantContactId && e.userEmails)) || []
                          const userEmails = (user && user.userEmails && user.userEmails.map(u => u.emailId)) || []
                          return (
                            <RfpSupplierSection
                              key={index}
                              suppliersList={suppliersList}
                              users={_.orderBy(users, ['userFirstName'])}
                              errors={errors}
                              canEditTran={canEditTran}
                              dropDown={dropDown.rfpFirmName}
                              index={index}
                              item={item}
                              onRemove={() =>
                                this.onRemove("rfpParticipants", index)
                              }
                              userEmails={userEmails}
                              onChangeItem={(changedItem, change, type) =>
                                this.onChangeItem(
                                  changedItem,
                                  index,
                                  "rfpParticipants",
                                  change,
                                  type
                                )
                              }
                              onBlur={change =>
                                this.onBlur("rfpParticipants", change)
                              }
                              onPhoneChange={this.onPhoneChange}
                              onChangeError={this.onChangeError}
                              onSave={this.onSubmit}
                              isEditable={isEditable}
                              onEdit={this.onEdit}
                              category="rfpParticipants"
                              isSaveDisabled={isSaveDisabled}
                              onCancel={this.onCancel}
                            />
                          )
                        })
                        : null}
                    </table>
                  </div>
                )}
              </RatingSection>
              <br />
              <div>
                {/* commented so that dropdownlist works className="overflow-auto"> */}
                <a
                  className="button is-link"
                  onClick={() =>
                    this.setState({ manageBidPacketShow: !manageBidPacketShow })
                  }
                >
                  {`${manageBidPacketShow ? "Hide" : "Show"}`} Bid Packet
                </a>
              </div>
              {manageBidPacketShow && (
                <div className="box">
                  <TranDocuments
                    {...this.props}
                    onSave={this.onDocSave}
                    tranId={this.props.nav2}
                    title="Bid Packet"
                    pickCategory="LKUPDOCCATEGORIES"
                    pickSubCategory="LKUPCORRESPONDENCEDOCS"
                    pickAction="LKUPDOCACTION"
                    category="rfpBidDocuments"
                    documents={documentsList}
                    contextType={ContextType.rfp}
                    tags={tags}
                    onStatusChange={this.onStatusChange}
                    onDeleteDoc={this.onDeleteDoc}
                    tableStyle={{ fontSize: "smaller" }}
                    isDisabled={!canEditTran || false}
                  />
                </div>
              )}
              {canEditTran ? (
                <div>
                  <br />

                  <div className="control is-grouped-center">
                    <br />
                    {/* <button
                      className="button is-link"
                      onClick={() => this.onSubmit(true)}
                      disabled={isSaveDisabled || false}
                    >
                      Save
                    </button>
                    &nbsp;&nbsp; */}
                    <button
                      className="button is-link"
                      onClick={() => this.onSubmit(false)}
                      disabled={!isSubmit || isSaveDisabled || false}
                    >
                      Distribute Bid Packet
                    </button>
                  </div>
                </div>
              ) : null}

              <TransactionNotes
                notesList={tranNotes || []}
                canEditTran={canEditTran || false}
                onSave={this.onNotesSave}
              />

            </div>
          )}
        />
        <br />
        {this.state.showModal ? (
          <DocModalDetails
            showModal={this.state.showModal}
            closeDocDetails={this.handleDocDetails}
            documentId={doc.documentId}
            onDeleteAll={this.deleteDoc}
            docMetaToShow={[]}
            versionMetaToShow={[]}
            docId={doc._id}
          />
        ) : null}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(TransactionDistribute)

export default withRouter(connect(mapStateToProps,null)(WrappedComponent))
