import Joi from "joi-browser"

const rfpEvaluationTeam = Joi.object().keys({
  rfpSelEvalContactId: Joi.string().required(),
  rfpSelEvalFirmId: Joi.string().required(),
  rfpSelEvalRealFirstName: Joi.string().allow("").optional(),
  rfpSelEvalRealLastName: Joi.string().allow("").optional(),
  rfpSelEvalRealEmailId: Joi.string().allow("").optional(),
  rfpSelEvalRole: Joi.string().required(),
  rfpSelEvalContactName: Joi.string().required(),
  rfpSelEvalEmail: Joi.string().email().required(),
  rfpSelEvalPhone: Joi.string().allow("", null).optional(),
  rfpSelEvalAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

export const RfpEvaluationValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, rfpEvaluationTeam, { abortEarly: false, stripUnknown:false })
}

const rfpProcessContacts = Joi.object().keys({
  rfpProcessContactId: Joi.string().required(),
  rfpProcessFirmId: Joi.string().required(),
  rfpProcessContactRealFirstName: Joi.string().allow("").optional(),
  rfpProcessContactRealLastName: Joi.string().allow("").optional(),
  rfpProcessContactRealEmailId: Joi.string().allow("").optional(),
  rfpContactFor: Joi.string().required(),
  rfpContactName: Joi.string().required(),
  rfpContactEmail: Joi.string().email().required(),
  rfpContactPhone: Joi.string().allow("", null).optional(),
  rfpContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

export const RfpProcessValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, rfpProcessContacts, { abortEarly: false, stripUnknown:false })
}

const rfpParticipants = Joi.object().keys({
  rfpParticipantFirmId:  Joi.string().required(),
  rfpParticipantRealFirmName: Joi.string().allow("").optional(),
  rfpParticipantRealMSRBType: Joi.string().allow("").optional(),
  rfpParticipantRealFirstName: Joi.string().allow("").optional(),
  rfpParticipantRealLastName: Joi.string().allow("").optional(),
  rfpParticipantRealEmailId: Joi.string().allow("").optional(),
  rfpParticipantFirmName: Joi.string().required(),
  rfpParticipantContactName: Joi.string().required(),
  rfpParticipantContactEmail: Joi.string().email().required(),
  rfpParticipantContactPhone: Joi.string().allow("", null).optional(),
  rfpParticipantContactId: Joi.string().required(),
  rfpParticipantContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

export const RfpParticipantsValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, rfpParticipants, { abortEarly: false, stripUnknown:false })
}

const docAction = Joi.object().keys({
  actionType: Joi.string().required(),
  user:  Joi.string().required(),
  date: Joi.string().required().optional(),
  userFirstName: Joi.string().required().optional(),
  userLastName: Joi.string().required().optional(),
  userEmailId: Joi.string().required().optional(),
  _id: Joi.string().required().optional(),
})

const rfpBidDocuments = Joi.object().keys({
  rfpBidDocName: Joi.string().required(),
  rfpBidDocId: Joi.string().required().optional(),
  rfpBidDocType: Joi.string().required(),
  rfpBidDocStatus: Joi.string().required(),
  rfpBidDocUploadUser: Joi.string().required(),
  rfpBidDocAction: Joi.array().optional(),
  rfpBidDocUploadFirstName: Joi.string().required(),
  rfpBidDocUploadLastName: Joi.string().required(),
  rfpBidDocUploadEmailId: Joi.string().required(),
  _id: Joi.string().required().optional(),
})

/* const transDistributeSchema = (minLength) => Joi.object().keys({
  rfpEvaluationTeam:  Joi.array().min(minLength).items(rfpEvaluationTeam).required().optional(),
  rfpProcessContacts:  Joi.array().min(minLength).items(rfpProcessContacts).required().optional(),
  rfpParticipants:  Joi.array().min(minLength).items(rfpParticipants).required().optional(),
})

export const TransDistribute = (inputTransDistribute, minLength) => Joi.validate(inputTransDistribute, transDistributeSchema(minLength), { abortEarly: false, stripUnknown:false }) */

