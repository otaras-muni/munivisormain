import React from "react"
import { DropdownList } from "react-widgets"
import PhoneInput, {formatPhoneNumber} from "react-phone-number-input"
import { SelectLabelInput } from "../../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear";

const RfpEvaluationSection = ({
  usersList,
  item = {},
  dropDown,
  index,
  userEmails,
  canEditTran,
  onChangeItem,
  onRemove,
  errors = {},
  onBlur,
  onChangeError,
  onSave,
  onEdit,
  isEditable,
  category,
  isSaveDisabled,
  onPhoneChange,
  onCancel
}) => {
  isEditable = (isEditable[category] === index)

  const onChange = event => {
    onChangeItem({
      ...item,
      [event.target.name]:
        event.target.type === "checkbox"
          ? event.target.checked
          : event.target.value
    }, "", event.target.type)
    if (event.target.name && event.target.value) {
      onChangeError(event.target.name, "rfpEvaluationTeam", index)
    }
  }

  const onUserChange = user => {
    let email = ""
    let phone = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    onChangeItem(
      {
        ...item,
        rfpSelEvalContactId: Object.keys(user).length === 0 ? "" : user.id,
        rfpSelEvalFirmId: Object.keys(user).length === 0 ? "" : user.entityId,
        rfpSelEvalContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
        rfpSelEvalEmail: email || "",
        rfpSelEvalPhone: phone || "",
        rfpSelEvalRealFirstName: user.userFirstName,
        rfpSelEvalRealLastName: user.userLastName,
        rfpSelEvalRealEmailId: email
      },
      `user change to ${user.userFirstName}`
    )
    if (user.id) {
      onChangeError("", "rfpEvaluationTeam", index, {
        rfpSelEvalContactId: "",
        rfpSelEvalFirmId: "",
        rfpSelEvalContactName: "",
        rfpSelEvalEmail: "",
        rfpSelEvalPhone: "",
        rfpSelEvalRealFirstName: "",
        rfpSelEvalRealLastName: "",
        rfpSelEvalRealEmailId: ""
      })
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const value = item && item.rfpSelEvalPhone && item.rfpSelEvalPhone.includes("+") || false
  const Internationals = item.rfpSelEvalPhone && formatPhoneNumber(value ? item.rfpSelEvalPhone : `+1${item.rfpSelEvalPhone}`, "International") || "International"

  return (
    <tbody>
      <tr>
        <td>
          <div className="field has-addons">
            <div className="control">
              <DropDownListClear
                filter
                groupBy="group"
                data={usersList}
                value={item.rfpSelEvalContactName || item.rfpSelEvalContactId}
                textField="name"
                valueField="id"
                onChange={onUserChange}
                disabled={!canEditTran || !isEditable}
                isHideButton={item.rfpSelEvalContactId && isEditable && canEditTran}
                onClear={() => {onUserChange({})}}
              />
              {errors.rfpSelEvalContactId && (
                <p className="text-error">Required</p>
              )}
            </div>
          </div>
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                title="Evaluator Role"
                error={errors.rfpSelEvalRole || ""}
                list={dropDown}
                value={item.rfpSelEvalRole}
                name="rfpSelEvalRole"
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canEditTran}
              />
              : <small>{item.rfpSelEvalRole}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <input
                  title="Name"
                  value={item.rfpSelEvalContactName}
                  name="rfpSelEvalContactName"
                  onChange={onChange}
                  onBlur={onBlurInput}
                  className="input is-small is-link"
                  type="text"
                  placeholder="Picklist from CRM"
                  disabled={!canEditTran}
                />
                {errors.rfpSelEvalContactName && (
                  <p className="text-error">Required</p>
                )}
              </div>
              : <small>{item.rfpSelEvalContactName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="select is-small is-link">
                <select
                  title="Email"
                  value={item.rfpSelEvalEmail}
                  name="rfpSelEvalEmail"
                  onChange={onChange}
                  onBlur={onBlurInput}
                  style={{ width: 180 }}
                  disabled={!canEditTran}
                >
                  {userEmails.map(e => (
                    <option key={e}>{e}</option>
                  ))}
                </select>
                {errors.rfpSelEvalEmail && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <small>{item.rfpSelEvalEmail}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <PhoneInput
                  value={ Internationals || "" }
                  disabled={!canEditTran}
                  onChange={event => onPhoneChange(event, "rfpEvaluationTeam", index, "rfpSelEvalPhone")}
                />
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  mask="_"
                  className="input is-small is-link"
                  name="rfpSelEvalPhone"
                  placeholder="Auto-populated as/if from CRM"
                  value={item.rfpSelEvalPhone}
                  onBlur={event => onBlurInput(event)}
                  onChange={event => onChange(event)}
                  disabled={!canEditTran}
                /> */}
                {errors.rfpSelEvalPhone && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <PhoneInput
                value={ Internationals || "" }
                disabled
              />
          }
        </td>
        <td>
          <label className="checkbox">
            <input
              title="Add to DL"
              type="checkbox"
              checked={item.rfpSelEvalAddToDL || false}
              name="rfpSelEvalAddToDL"
              onBlur={onBlurInput}
              onChange={() => {}}
              onClick={onChange}
              disabled={!canEditTran || !(item && item._id)}
            />
          </label>
        </td>
        {canEditTran ? (
          <td>
            <div className="field is-grouped">
              <div className="control">
                <button onClick={isEditable ? () => onSave(true, "rfpEvaluation", index) : () => onEdit(category, index)} className={`${isSaveDisabled ? "borderless btn-blue isDisabled" : "borderless btn-blue"}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                  </span>
                </button>
              </div>
              <div className="control">
                <button onClick={isEditable ? () => onCancel("rfpEvaluationTeam") : onRemove} className={`${isSaveDisabled ? "borderless btn-blue isDisabled" : "borderless btn-blue"}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="fa fa-times" title="Cancel"/> : <i className="far fa-trash-alt" title="Delete"/>}
                  </span>
                </button>
              </div>
            </div>
          </td>
        ) : null}
      </tr>
    </tbody>
  )
}

export default RfpEvaluationSection
