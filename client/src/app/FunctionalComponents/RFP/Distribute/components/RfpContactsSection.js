import React from "react"
import { DropdownList } from "react-widgets"
import PhoneInput, {formatPhoneNumber} from "react-phone-number-input"
import { SelectLabelInput } from "../../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear";

const RfpContactsSection = ({
  usersList,
  userEmails,
  dropDown,
  index,
  item = {},
  canEditTran,
  onChangeItem,
  onRemove,
  errors = {},
  onBlur,
  onChangeError,
  onSave,
  onEdit,
  isEditable,
  category,
  isSaveDisabled,
  onPhoneChange,
  onCancel
}) => {
  isEditable = (isEditable[category] === index)

  const onChange = event => {
    onChangeItem(
      {
        ...item,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      },
      `${event.target.name || "empty"} change to ${
        event.target.type === "checkbox"
          ? event.target.checked
          : event.target.value
      }`, event.target.type
    )
    if (event.target.name && event.target.value) {
      onChangeError(event.target.name, "rfpProcessContacts", index)
    }
  }

  const onUserChange = user => {
    let email = ""
    let phone = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    onChangeItem(
      {
        ...item,
        rfpProcessContactId: Object.keys(user).length === 0 ? "" : user.id,
        rfpProcessFirmId: Object.keys(user).length === 0 ? "" : user.entityId,
        rfpContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
        rfpContactEmail: email || "",
        rfpContactPhone: phone || "",
        rfpProcessContactRealFirstName: user.userFirstName || "",
        rfpProcessContactRealLastName: user.userLastName || "",
        rfpProcessContactRealEmailId: email || ""
      },
      `user change to ${user.userFirstName}`
    )

    if (user.id) {
      onChangeError("", "rfpProcessContacts", index, {
        rfpProcessContactId: "",
        rfpProcessFirmId: "",
        rfpContactName: "",
        rfpContactEmail: "",
        rfpContactPhone: "",
        rfpProcessContactRealFirstName: "",
        rfpProcessContactRealLastName: "",
        rfpProcessContactRealEmailId: ""
      })
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const value = item && item.rfpContactPhone && item.rfpContactPhone.includes("+") || false
  const Internationals = item.rfpContactPhone && formatPhoneNumber(value ? item.rfpContactPhone : `+1${item.rfpContactPhone}`, "International") || "International"

  return (
    <tbody>
      <tr>
        <td>
          <div className="field has-addons">
            <div className="control">
              <DropDownListClear
                filter
                groupBy="group"
                data={usersList}
                value={item.rfpContactName || item.rfpProcessContactId}
                textField="name"
                valueField="id"
                onChange={onUserChange}
                disabled={!canEditTran || !isEditable}
                isHideButton={item.rfpProcessContactId && isEditable && canEditTran}
                onClear={() => {onUserChange({})}}
              />
              {errors.rfpProcessContactId && (
                <p className="text-error">Required</p>
              )}
            </div>
          </div>
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                title="Contact For"
                error={errors.rfpContactFor || ""}
                list={dropDown}
                value={item.rfpContactFor}
                name="rfpContactFor"
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canEditTran}
              />
              : <small>{item.rfpContactFor}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <input
                  title="Name"
                  value={item.rfpContactName}
                  name="rfpContactName"
                  onChange={onChange}
                  onBlur={onBlurInput}
                  className="input is-small is-link"
                  type="text"
                  placeholder="Picklist from CRM"
                  disabled={!canEditTran}
                />
                {errors.rfpContactName && <p className="text-error">Required</p>}
              </div>
              : <small>{item.rfpContactName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="select is-small is-link">
                <select
                  title="Email"
                  value={item.rfpContactEmail}
                  name="rfpContactEmail"
                  style={{ width: 180 }}
                  onChange={onChange}
                  onBlur={onBlurInput}
                  disabled={!canEditTran}
                >
                  {userEmails.map(e => (
                    <option key={e}>{e}</option>
                  ))}
                </select>
                {errors.rfpContactEmail && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <small>{item.rfpContactEmail}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <PhoneInput
                  value={ Internationals || "" }
                  disabled={!canEditTran}
                  onChange={event => onPhoneChange(event, "rfpProcessContacts", index, "rfpContactPhone")}
                />
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  mask="_"
                  className="input is-small is-link"
                  name="rfpContactPhone"
                  value={item.rfpContactPhone}
                  placeholder="Auto-populated as/if from CRM"
                  onBlur={event => onBlurInput(event)}
                  onChange={event => onChange(event)}
                  disabled={!canEditTran}
                /> */}
                {errors.rfpContactPhone && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <PhoneInput
                value={ Internationals || "" }
                disabled
              />
          }
        </td>
        <td>
          <label className="checkbox">
            <input
              title="Add to DL"
              type="checkbox"
              checked={item.rfpContactAddToDL || false}
              name="rfpContactAddToDL"
              onBlur={onBlurInput}
              onChange={() => {}}
              onClick={onChange}
              disabled={!canEditTran || !(item && item._id)}
            />
          </label>
        </td>
        {canEditTran ? (
          <td>
            <div className="field is-grouped">
              <div className="control">
                <a onClick={isEditable ? () => onSave(true, "rfpProcess", index) : () => onEdit(category, index)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                  <span className="has-text-link">
                    {isEditable ? <i className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={isEditable ? () => onCancel("rfpProcessContacts") : onRemove}>
                  <span className="has-text-link">
                    {isEditable ? <i className="fa fa-times" title="Cancel"/> : <i className="far fa-trash-alt" title="Delete"/>}
                  </span>
                </a>
              </div>
            </div>
          </td>
        ) : null}
      </tr>
    </tbody>
  )
}

export default RfpContactsSection
