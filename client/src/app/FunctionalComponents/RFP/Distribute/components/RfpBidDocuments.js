import React from "react"
import DocUpload from "../../../docs/DocUpload"
import {SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"
import {ContextType} from "../../../../../globalutilities/consts"
import { updateS3DocTags } from "GlobalUtils/helpers"
import DocLink from "../../../docs/DocLink"
import DocModal from "../../../docs/DocModal"

const RfpBidDocuments = ({userEmail, user, doc={}, index, tranId, dropDown, bucketName, getBidBucket, onChangeItem, onRemove, onFileAction, errors = {}, canEditTran, onDeleteDoc}) => {

  const onUserChange = async (e) => {
    let newItem = {}
    if(e.target.name === "rfpBidDocAction") {
      const actionType = [{
        actionType: e.target.value,
        user:  user.userId,
        date: new Date().toDateString(),
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        userEmailId: userEmail,
      }]
      newItem = {
        ...doc,
        [e.target.name]: actionType,
      }
    } else {
      newItem = {
        ...doc,
        [e.target.name]: e.target.value,
      }
    }
    onChangeItem(newItem, `user change to ${e.target.name}`)
  }

  const handleFile = (type) => {
    onFileAction(type, doc.rfpBidDocId)
  }
  const docUploadProps = {
    bucketName,
    onUploadSuccess: getBidBucket,
    showFeedback: true,
    bidIndex: index,
    contextId: tranId,
    contextType: ContextType.rfp,
    tenantId: user.entityId
  }

  if(doc.rfpBidDocId) {
    docUploadProps.docId = doc.rfpBidDocId
  }

  return (
    <tbody>
      <tr>
        {
          canEditTran ?
            <td>
              <DocUpload {...docUploadProps} />
              {errors.rfpBidDocId && <p className="text-error">{errors.rfpBidDocId}</p>}
            </td> : null
        }
        <td>
          {
            doc.rfpBidDocId ?
              <div className="field is-grouped">
                <DocLink docId={doc.rfpBidDocId} />
                <DocModal onDeleteAll={onDeleteDoc} selectedDocId={doc.rfpBidDocId}  documentId={doc._id} />
              </div> : null
          }
        </td>
        <td>
          <SelectLabelInput error= {errors.rfpBidDocType || ""} list={dropDown.rfpDocTypes} value={doc.rfpBidDocType} name="rfpBidDocType" onChange={(e) => onUserChange(e, doc, index, "rfpBidDocuments")} disabled={!canEditTran}/>
        </td>
        <td>
          <SelectLabelInput error= {errors.rfpBidDocStatus || ""} list={dropDown.rfpDocStatus} value={doc.rfpBidDocStatus} name="rfpBidDocStatus" onChange={(e) => onUserChange(e, doc, index, "rfpBidDocuments")} disabled={!canEditTran}/>
        </td>
        <td>
          <div className="select is-small is-link">
            <select  name="docAction" value={doc.docAction} onChange={(e) => onFileAction(e.target.value, doc)}>
              <option value="">Pick</option>
              <option>See version history</option>
              <option disabled>Send to EMMA</option>
              <option>Share document</option>
            </select>
          </div>
          {/* <SelectLabelInput list={dropDown.actions} name="docAction" value={doc.docAction} onChange={(e) => this.onFileAction(e.target.value, doc)}/> */}
        </td>
        {
          canEditTran ?
            <td>
              <div className="field is-grouped">
                <div className="control">
                  <a onClick={onRemove}>
                    <span className="has-text-link">
                      <i className="far fa-trash-alt"/>
                    </span>
                  </a>
                </div>
              </div>
            </td> : null
        }
      </tr>
    </tbody>
  )}

export default RfpBidDocuments
