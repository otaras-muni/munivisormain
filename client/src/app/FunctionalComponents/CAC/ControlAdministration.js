import React, { Component } from "react"
import {Link} from "react-router-dom"
import Loader from "Global/Loader"
import ControlsList from "./ControlsList"
import ControlDetails from "./ControlDetails"
import AuditLog from "Global/AuditLog"

const tabs = [{
  title: <span>List of Controls</span>,
  value: 1,
  path: "controls-lists",
},
/* {
  title: <span>Control Detail</span>,
  value: 2,
  path: "edit-control",
},
{
  title: <span>Activity Log</span>,
  value: 3,
  path: "audit",
} */
]

class ControlAdministration extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
    }
  }

  componentWillMount() {
    const {nav3, nav4} = this.props
    if (nav3 === "edit-control" && (nav4 === "" || nav4 === undefined)) {
      this.props.history.push("/cac/controls-list/controls-lists")
    }
    if (nav3){
      if (nav3 === "controls-lists"){
        tabs.splice(1,1)
      }
      if (nav3 === "edit-control"){
        tabs.splice(1,1)
        tabs.push({
          title: <span>Control Detail</span>,
          value: 2,
          path: "edit-control",
        })
      }
    } else {
      tabs.splice(1,1)
      this.props.history.push("/cac/controls-list/controls-lists")
    }
    this.setState({
      loading: false,
    })
  }

  renderTabs = (activeTab) => {
    return (
      <div className="tabs">
        <ul>
          {
            tabs.map(tab=> {
              const style = tab.path === "edit-control" ? {pointerEvents : "none"} : {}
              return (
                <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`} style={style}>
                  {
                    this.props.nav2 ? <Link to={`/cac/controls-list/${tab.path}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                      <a role="button" className="tabSecLevel">
                        {tab.title}
                      </a>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }

  renderSelectedView = () => {
    const { token, entityId, userId, userEmail, controls, nav4, nav3, existingQuarterlyAffirmations, saveAll, getNewControl } = this.props
    switch(nav3) {
    case "controls-lists" :
      return <ControlsList {...this.props}/>
    // case "edit-control" :
    //   return <ControlDetails {...this.props}/>
    case "edit-control": {
      /* if (!nav4) {
        const controls = [...this.props.controls]
        const ids = controls.map(c => +c.id.split("CTRL")[1])
        ids.sort((a, b) => b - a)
        const id = `CTRL${(ids[0] || 0) + 1}`
        controls.push(getNewControl(id, "new", userEmail))
        return (
          <ControlDetails
            id={id}
            token={token}
            entityId={entityId}
            existingQuarterlyAffirmations={existingQuarterlyAffirmations}
            userId={userId}
            userEmail={userEmail}
            controls={controls}
            saveAll={saveAll}
          />
        )
      } */
      return (
        <ControlDetails
          id={nav4}
          token={token}
          entityId={entityId}
          existingQuarterlyAffirmations={existingQuarterlyAffirmations}
          userId={userId}
          userEmail={userEmail}
          controls={controls}
          saveAll={saveAll}
        />
      )
    }
    // case "audit" :
    //   return <AuditLog type="cac"/>
    default:
      return "not Found"
    }
  }

  render() {
    const activeTab = tabs.find(x => x && (x.path === this.props.nav3))
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="column">
          {this.props.nav2 && this.renderTabs(activeTab && activeTab.value) }
        </div>
        <div>
          {this.renderSelectedView()}
        </div>
      </div>
    )
  }
}

export default ControlAdministration
