import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import ReactTable from "react-table"
import qs from "qs"
import { getCurrentQuarter } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/Loader"
import { getControlsActions } from "../../StateManagement/actions/cac"
import moment from "moment"
import GiftList from "../NewCompliance/GiftsGratuities/UserView/GiftList";
import PoliticalDashReactTable from "../NewCompliance/PoliticalContribution/components/PoliticalDashReactTable";
import RatingSection from "../../GlobalComponents/RatingSection";
import Accordion from "../../GlobalComponents/Accordion";
import Disclaimer from "../../GlobalComponents/Disclaimer";

class CACActionList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedId: "", pageSizeOptions: [5, 10, 20, 25, 50], pageSize: 10,
      searchString: "", stringToMatch: "", waiting: true, controlActions: []
    }

    this.toggleAccordion = this.toggleAccordion.bind(this)
    this.onPageSizeChange = this.onPageSizeChange.bind(this)
    this.changeSearchString = this.changeSearchString.bind(this)
    this.handleKeyPressinSearch = this.handleKeyPressinSearch.bind(this)
  }

  async componentDidMount() {
    const query = this.props.location && this.props.location.search
    let params
    let cid
    if (query) {
      params = qs.parse(query, { ignoreQueryPrefix: true }) || {}
      cid = (params && params.cid) || ""
    }
    if (cid) {
      this.selectControl(cid)
    }
    const res = await getControlsActions(this.props.token)
    this.setState({
      controlActions: res || [],
      waiting: false
    })
  }

  /* componentWillUnmount() {
    const { token, supervisor } = this.props
    if (supervisor) {
      this.props.getControls(token, supervisor)
    }
  } */

  selectControl = (selectedId) => {
    console.log("in selectControl")
    console.log("selectedId : ", selectedId)
    this.props.history.push(`/cac/view-control/view-detail?id=${selectedId}`)
  }

  changeSearchString(e) {
    const searchString = e.target.value
    if (!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString })
    }
  }

  handleKeyPressinSearch(e) {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  onPageSizeChange = (item) => {
    this.setState({
      pageSize: item
    })
  }

  toggleAccordion() {
    this.setState(prevState => ({ expanded: !prevState.expanded }))
  }

  renderTable() {
    const { pageSize, pageSizeOptions, stringToMatch, controlActions } = this.state
    let controls = [...controlActions]
    const [currentQuarter, currentYear] = getCurrentQuarter()
    controls = controls.filter(e => {
      if (stringToMatch) {
        const match = stringToMatch.toLocaleLowerCase()
        const searchFields = ["id", "name", "topic", "status"]
        let found = false
        searchFields.some(d => {
          if (e[d].toLocaleLowerCase().includes(match)) {
            found = true
            return true
          }
        })
        if (!found) {
          return false
        }
      }

      if (e.meta && e.meta.quarter && e.meta.year) {
        if (currentQuarter >= e.meta.quarter && currentYear >= e.meta.year) {
          return true
        }
        return false
      }
      return true
    })
    return (
      <ReactTable
        columns={[
          {
            Header: "ID",
            id: "_id",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <a onClick={() => {
                this.selectControl(e.value._id)
              }}>{e.value.id}</a></div>
          },
          {
            Header: "Control Name",
            id: "name",
            className: "multiExpTblVal",
            accessor: e => e.name,
            Cell: e => <small className="hpTablesTd wrap-cell-text">{e.value}</small>
          },
          {
            Header: "Compliance Topic",
            id: "topic",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <small className="hpTablesTd wrap-cell-text">{e.value.topic}</small>
          },
          {
            Header: "Status",
            id: "status",
            className: "multiExpTblVal",
            accessor: e => e.status,
            Cell: e => <small className="hpTablesTd wrap-cell-text">{e.value}</small>
          },
          {
            Header: "Due Date",
            id: "dueDate",
            className: "multiExpTblVal",
            accessor: e => e.dueDate,
            Cell: e => <small className="hpTablesTd wrap-cell-text">{moment(e && e.value).format("MM-DD-YYYY")}</small>
          }
        ]}
        data={controls}
        defaultPageSize={pageSize}
        onPageSizeChange={this.onPageSizeChange}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        pageSize={pageSize}
        loading={this.state.waiting}
        minRows={1}
      />
    )
  }

  render() {
    const {waiting } = this.state
    let { svControls } = this.props
    svControls = {
      ...svControls,
      supervisor: false
    }
    if (!svControls && waiting) {
      return <Loader />
    }

    return (
      <div id="main">
        <div>
          <div className="column">
            <p className="control has-icons-left">
              <input className="input is-fullwidth is-link is-small"
                type="text"
                placeholder="search"
                value={this.state.searchString}
                onChange={this.changeSearchString}
                onKeyPress={this.handleKeyPressinSearch}
              />
              <span className="icon is-small is-left has-background-dark has-text-white">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  title="Action Center Topics"
                >
                  {activeAccordions.includes(0) && (
                    <div>
                      {this.renderTable()}
                    </div>
                  )}
                </RatingSection>
              </div>
            )}
          />
          {svControls ? <GiftList svControls={svControls} /> : null}
          {svControls ? <PoliticalDashReactTable svControls={svControls} /> : null}
          <hr />
          <Disclaimer />
        </div>
      </div>
    )
  }
}

export default withRouter(CACActionList)
