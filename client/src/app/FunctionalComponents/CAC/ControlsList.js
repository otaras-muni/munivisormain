import React, { Component } from "react"
import { Link } from "react-router-dom"
import {fetchAllControls} from "../../StateManagement/actions/cac"

import ReactTable from "react-table"
const ObjectID = require("bson-objectid")

import Loader from "../../GlobalComponents/Loader"
import Disclaimer from "../../GlobalComponents/Disclaimer";

class ControlsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedListId: "", selectedAction: "", searchString: "", waiting: true,
      stringToMatch: "", pageSizeOptions: [5, 10, 20, 25, 50], pageSize: 5,
    }
    this.addNewControl = this.addNewControl.bind(this)
    this.copyControl = this.copyControl.bind(this)
    this.changeSearchString = this.changeSearchString.bind(this)
    this.handleKeyPressinSearch = this.handleKeyPressinSearch.bind(this)
    this.onPageSizeChange = this.onPageSizeChange.bind(this)
  }

  async componentWillMount() {
    const res = await fetchAllControls()
    this.setState({
      controls : res && res[0] && res[0].controls || [],
      waiting: false
    })
  }

  onPageSizeChange = (item) => {
    this.setState({
      pageSize: item
    })
  }

  changeSearchString(e) {
    const searchString = e.target.value
    if (!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString })
    }
  }

  handleKeyPressinSearch(e) {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  performAction() {
    const { selectedListId, selectedAction } = this.state
    switch (selectedAction) {
    case "edit":
      this.props.history.push(`/cac/controls-list/edit-control/${selectedListId}`)
      break
    case "copy":
      console.log("copying : ", selectedListId)
      this.copyControl(selectedListId)
      break
    default:
      console.log("unknown action")
      break
    }
  }

  addNewControl() {
    // const controls = [...this.state.controls]
    // const ids = controls.filter(c => /^CTRL\d/.test(c.id)).map(c => +c.id.split("CTRL")[1])
    // ids.sort((a, b) => b - a)
    // const newListId = `CTRL${(ids[0] || 0) + 1}`
    const newListId = `CTRL-${Math.random()
      .toString(36)
      .toUpperCase()
      .substring(2, 17)}`
    this.props.history.push(`/cac/controls-list/edit-control/${newListId}?src=new`)
  }

  copyControl(id) {
    const controls = [...this.state.controls]
    const ids = controls.filter(c => /^CTRL\d/.test(c.id)).map(c => +c.id.split("CTRL")[1])
    ids.sort((a, b) => b - a)
    const newListId = `CTRL-${Math.random()
      .toString(36)
      .toUpperCase()
      .substring(2, 17)}`
    this.props.history.push(`/cac/controls-list/edit-control/${newListId}?src=copy&id=${id}`)
    // this.props.addNewControl(id, "copy")
  }

  changeSelectedAction(selectedListId, e) {
    console.log("selectedListId : ", selectedListId)
    console.log("e : ", e)
    const selectedAction = e.target.value
    this.setState({ selectedListId, selectedAction }, this.performAction)
  }

  matchControl(controls, stringToMatch) {
    let matchingLists = controls
    if (stringToMatch) {
      matchingLists = controls && controls.length && controls.filter(c =>
        c && c.id && c.id.toLowerCase().includes(stringToMatch.toLowerCase()) ||
        c && c.name && c.name.toLowerCase().includes(stringToMatch.toLowerCase()) ||
        c && c.topic && c.topic.toLowerCase().includes(stringToMatch.toLowerCase()) ||
        c && c.notes && c.notes.toLowerCase().includes(stringToMatch.toLowerCase())
      )
    }
    return matchingLists
  }

  renderTable(controls) {
    const { pageSize, pageSizeOptions, selectedAction } = this.state
    return (
      <ReactTable
        columns={[
          {
            Header: "ID",
            id: "_id",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <Link to={`/cac/controls-list/edit-control/${e.value.id}`}>{e.value.id}</Link></div>
          },
          {
            Header: "Control Name",
            id: "name",
            className: "multiExpTblVal",
            accessor: e => e.name,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Set up by",
            id: "createdBy",
            className: "multiExpTblVal",
            accessor: e => e.createdBy,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Compliance Topic",
            id: "topic",
            className: "multiExpTblVal",
            accessor: e => e.topic,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Notes",
            id: "notes",
            className: "multiExpTblVal",
            accessor: e => e.notes,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Last Updated",
            id: "lastUpdated",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value.lastUpdated && e.value.lastUpdated.date &&
                new Date(e.value.lastUpdated.date).toLocaleString()} {e.value.lastUpdated && e.value.lastUpdated.by}
              </small></div>
          },
          {
            Header: "Action",
            id: "action",
            className: "multiExpTblVal",
            accessor: e => e.id,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <div className="select is-small is-link">
                <select value={selectedAction} onChange={this.changeSelectedAction.bind(this, e.value)}>
                  <option value="" disabled>Action</option>
                  <option value="edit">Edit</option>
                  <option value="copy">Make a copy</option>
                </select>
              </div>
            </div>
          }
        ]}
        data={controls}
        defaultPageSize={pageSize}
        onPageSizeChange={this.onPageSizeChange}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        pageSize={pageSize}
        loading={this.state.waiting}
        minRows={1}
      />
    )
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Control Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Set up by</p>
        </th>
        <th>
          <p className="emmaTablesTh">Compliance Topic</p>
        </th>
        <th>
          <p className="emmaTablesTh">Notes</p>
        </th>
        <th>
          <p className="emmaTablesTh">Last Updated</p>
        </th>
        <th>
          <p className="emmaTablesTh">Action</p>
        </th>
      </tr>
    )
  }

  renderControlsRows(controls) {
    const rows = controls.map(d =>
      <tr key={d._id || d.id}>
        <td className="emmaTablesTd">
          <Link to={`/cac/controls-list/edit-control/${d.id}`}>{d.id}</Link>
        </td>
        <td className="emmaTablesTd">
          <small>{d.name}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.createdBy}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.topic}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.notes}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.lastUpdated && d.lastUpdated.date &&
            new Date(d.lastUpdated.date).toLocaleString()} {d.lastUpdated && d.lastUpdated.by}</small>
        </td>
        <td>
          <div className="select is-small is-link">
            <select value={this.state.selectedAction} onChange={this.changeSelectedAction.bind(this, d.id)}>
              <option value="" disabled>Action</option>
              <option value="edit">Edit</option>
              <option value="copy">Make a copy</option>
            </select>
          </div>
        </td>
      </tr>
    )
    return rows
  }

  renderControls(controls) {
    return (
      <section className="box">
        {/* <section className="accordions box top-bottom-margin"> */}
        {/* <p className="title innerPgTitle has-text-centered">Search by control id, name, topic or notes</p> */}
        <div className="columns">

          <div className="column">
            <p className="control has-icons-left">
              <input className="input is-small is-link"
                type="text"
                placeholder="search by control id, name, topic or notes"
                value={this.state.searchString}
                onChange={this.changeSearchString}
                onKeyPress={this.handleKeyPressinSearch}
              />
              <span className="icon is-small is-left has-background-dark has-text-white">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <div className="column">
            <button className="button is-link is-small" onClick={this.addNewControl}>Add new control</button>
          </div>
        </div>

        {this.renderTable(controls)}

        {/* <div className="box" style={{overflowX:"auto"}}>
            <table className="table is-striped is-fullwidth is-bordered">
              <thead >
                {this.renderTableHeader()}
              </thead>
              <tbody>
                {this.renderControlsRows(controls)}
              </tbody>
            </table>
          </div> */}
      </section>

    )
  }

  render() {
    const { stringToMatch } = this.state
    const { controls } = this.state
    const matchingLists = this.matchControl(controls, stringToMatch)
    if (matchingLists) {
      return (
        <div id="main">
          {this.renderControls(matchingLists)}
          <hr />
          <Disclaimer />
        </div>
      )
    }

    return <Loader />
  }
}

export default ControlsList
