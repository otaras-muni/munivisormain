import React, { Component } from "react"
import { NavLink, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import moment from "moment"
import Audit from "../../GlobalComponents/Audit"
import {checkSupervisorControls} from "../../StateManagement/actions/CreateTransaction"

import Loader from "../../GlobalComponents/Loader"

import {
  createNotification,
  createControlActionsTask,
  getQuartersBetweenDates,
  checkExistingQuarterlyAffirmationsAction
} from "GlobalUtils/helpers"

import {
  addNewControl,
  saveControls,
  saveDisableControls,
  checkCACSupervisor
} from "../../StateManagement/actions"
import ControlAdministration from "./ControlAdministration"
import UserActivity from "./UserActivity"
import CACDashboard from "./CACDashboard"

const ObjectID = require("bson-objectid")

const quarterlyAffirmations = [
  "Political Contributions and Prohibitions on Municipal Advisory Business",
  "Gifts and Gratuities"
]

const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

const TABS = [
  { path: "monitor", label: "Control Center Monitor", supervisor: true },
  { path: "controls-list", label: "Control Administration", supervisor: true },
  // { path: "controls-list", label: "Controls", supervisor: true },
  // { path: "edit-control", label: "Add/Edit Control", supervisor: true },
  { path: "view-control", label: "CAC - User Activity", supervisor: false },
  { path: "audit", label: "Activity Log", supervisor: true }
]

class CACMain extends Component {
  constructor(props) {
    super(props)
    this.state = {
      controlId: "",
      newControl: {},
      waiting: true,
      existingQuarterlyAffirmations: [],
      newOption: ""
    }
    this.addNewControl = this.addNewControl.bind(this)
    this.renderControlDetails = this.renderControlDetails.bind(this)
    this.saveControls = this.saveControls.bind(this)
  }

  async componentDidMount() {
    const { token, supervisor, controls, nav2, audit } = this.props
    console.log("mount supervisor : ", supervisor)
    const svControls = await checkSupervisorControls()
    await this.props.checkCACSupervisor(token, svControls)
    const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
    if (!submitAudit) {
      const index = TABS.findIndex(tab => tab.path === "audit")
      if(index !== -1){
        TABS.splice(index, 1)
      }
    }
    if (nav2 === "view-control") {
      return this.setState({ waiting: false, svControls })
    }
    if (supervisor && controls && !controls.length) {
      console.log("1 : ")
      // await this.props.getControls(token, supervisor)
      this.setState({ waiting: false, svControls })
    } else {
      if (controls && controls.length) {
        await this.checkQuarterlyAffirmations(token, controls)
      }
      this.setState({ waiting: false, svControls })
    }
  }

  async componentWillReceiveProps(nextProps) {
    // console.log("this.props : ", this.props)
    // console.log("nextProps : ", nextProps)
    // console.log("this supervisor : ", this.props.supervisor)
    // console.log("next supervisor : ", nextProps.supervisor)
    const { token, supervisor, controls, nav2 } = nextProps
    if (supervisor !== this.props.supervisor) {
      console.log("2 : ")
      // const supervisor = await this.checkSupervisor()
      // await nextProps.checkCACSupervisor(token)
      // console.log("supervisor : ", supervisor)
      // if(nextProps.supervisor !== supervisor) {
      //   await nextProps.checkCACSupervisor(nextProps.token)
      // } else {
      if (nav2 !== "view-control") {
        // await nextProps.getControls(token, supervisor)
      }

      this.setState({ waiting: false })
      // }
    } else {
      console.log("3 : ")
      // if(nextProps.supervisor) {
      //   await nextProps.getControls(nextProps.token, nextProps.supervisor)
      // }
      if (controls && controls.length) {
        await this.checkQuarterlyAffirmations(token, controls)
      }
      this.setState({ waiting: false })
    }
  }

  getTabLabel(path) {
    const item = TABS.filter(e => e.path === path)[0]
    if (item) {
      return item.label
    }
    return path
  }

  getNewControl(controlId, option, userEmail) {
    // const { userEmail } = this.props
    if (option === "new") {
      return {
        refId: ObjectID(),
        id: controlId,
        name: "",
        type: "alert",
        target: "others",
        topic: "",
        subTopic: "",
        otherTopic: "",
        refRule: "",
        notificationSentDate: null,
        actionCompletedBy: [],
        numActions: 0,
        dueDate: null,
        notification: {
          recurringType: "",
          recurringPattern1: 1,
          recurringPattern2: 1,
          recurring: false,
          frequency: 0,
          unit: "",
          startDate: null,
          endDate: null
        },
        recipients: [],
        toList: [],
        ccList: [],
        relatedActivity: [],
        relatedEntity: [],
        relatedContact: [],
        docIds: [],
        checklist: [],
        checklistApplicable: true,
        notes: "",
        state: "enabled",
        status: "open",
        saveType: "draft",
        createdBy: userEmail,
        lastUpdated: {
          date: new Date(),
          by: userEmail
        }
      }
    } if (option === "copy") {
      const controls = [...this.props.controls]
      const ids = controls
        .filter(c => /^CTRL\d/.test(c.id))
        .map(c => +c.id.split("CTRL")[1])
      ids.sort((a, b) => b - a)
      const newListId = `CTRL${(ids[0] || 0) + 1}`
      const control = controls.filter(e => e.id === controlId)[0]
      return {
        ...control,
        refId: ObjectID(),
        id: newListId,
        notificationSentDate: null,
        actionCompletedBy: [],
        numActions: 0,
        dueDate: null,
        notification: {
          ...control.notification,
          startDate: control.notification.startDate
            ? new Date(control.notification.startDate).toISOString()
            : null,
          endDate: control.notification.endDate
            ? new Date(control.notification.endDate).toISOString()
            : null
        },
        state: "enabled",
        status: "open",
        saveType: "draft",
        recipients: [...control.recipients],
        toList: [...control.toList],
        ccList: [...control.ccList],
        relatedActivity: [...control.relatedActivity],
        relatedEntity: [...control.relatedEntity],
        relatedContact: [...control.relatedContact],
        docIds: [],
        checklist: control.checklist
          ? control.checklist.map(e => ({ ...e }))
          : [],
        createdBy: userEmail,
        lastUpdated: {
          date: new Date(),
          by: userEmail
        }
      }
    }
  }

  async checkQuarterlyAffirmations(token, controls) {
    const existingQuarterlyAffirmations = []
    const idxp = controls.findIndex(
      c =>
        c.type === "affirm" &&
        c.topic ===
        "Political Contributions and Prohibitions on Municipal Advisory Business"
    )
    const idxg = controls.findIndex(
      c => c.type === "affirm" && c.topic === "Gifts and Gratuities"
    )
    if (idxp > -1) {
      const { refId } = controls[idxp]
      const exists = await checkExistingQuarterlyAffirmationsAction(refId)
      const { startDate, endDate } = controls[idxp].notification || {}
      const quarters = getQuartersBetweenDates(startDate, endDate)
      existingQuarterlyAffirmations.push({
        topic:
          "Political Contributions and Prohibitions on Municipal Advisory Business",
        quarters,
        refId,
        exists
      })
    }
    if (idxg > -1) {
      const { refId } = controls[idxg]
      const exists = await checkExistingQuarterlyAffirmationsAction(refId)
      const { startDate, endDate } = controls[idxg].notification || {}
      const quarters = getQuartersBetweenDates(startDate, endDate)
      existingQuarterlyAffirmations.push({
        topic: "Gifts and Gratuities",
        quarters,
        refId,
        exists
      })
    }
    this.setState({ existingQuarterlyAffirmations })
  }

  // async checkSupervisor() {
  //   const user = await getUserDetails(this.props.userId)
  //   console.log("user : ", user)
  //   if(user && user.userFlags && (
  //     user.userFlags.includes("Supervisory Principal") ||
  //     user.userFlags.includes("Compliance Officer"))) {
  //     return true
  //   }
  //   return false
  // }

  addNewControl(id, option) {
    if (["new", "copy"].includes(option)) {
      const newControl = this.getNewControl(id, option)
      console.log("newControl : ", newControl)
      this.setState(
        { controlId: newControl.id, newControl, newOption: option },
        this.renderControlDetails
      )
    }
  }

  createNewNotification(control) {
    const { token } = this.props
    let {
      id,
      notification: {
        recurring,
        startDate,
        endDate,
        frequency,
        unit,
        recurringType,
        recurringPattern1,
        recurringPattern2
      },
      docIds,
      topic,
      recipients
    } = control
    console.log(
      "recurr : ",
      recurring,
      startDate,
      endDate,
      frequency,
      unit,
      recurringType,
      recurringPattern1,
      recurringPattern2
    )
    if (recurringType === "weekly") {
      // console.log("startDate1 : ", startDate)
      startDate = moment(new Date(startDate))
        .subtract(recurringPattern1, `${unit}s`)
        .day(`${recurringPattern2}`)
        .subtract(1, "days")
        .toDate()
      // console.log("startDate2 : ", startDate)
    } else if (recurringType === "monthly" || recurringType === "quarterly") {
      console.log("startDate1 : ", startDate)
      startDate = moment(new Date(startDate))
        .subtract(recurringPattern1, `${unit}s`)
        .date(`${recurringPattern2}`)
        .toDate()
      console.log("startDate : ", startDate)
    }
    const modes = ["app"]
    const meta = {
      contextType: "compliance",
      contextId: id,
      topic
    }
    const options = {
      modes,
      recurring,
      startDate,
      endDate,
      frequency,
      unit,
      docIds: docIds.map(e => e._id).filter(e => e)
    }
    const userIds = recipients.map(e => e.id)
    const name = "NOTIFY_COMPLIANCE_CONTROL_ASSIGNED"
    createNotification(token, name, userIds, options, meta)
  }

  createNewControlActions(control) {
    console.log("control : ", control)
    const { token, userName } = this.props
    const {
      refId,
      id,
      name,
      topic,
      recipients,
      notification,
      dueDate,
      docIds,
      checklist,
      relatedEntity,
      relatedActivity,
      relatedContact,
      subTopic,
      refRule,
      otherTopic,
      notes,
      checklistApplicable
    } = control
    const userIds = recipients.map(e => e.id)
    const userNames = recipients.map(e => e.name)
    let docsActions = []
    if (docIds && docIds.length) {
      docsActions = docIds.map(e => ({
        docId: e._id,
        action: "",
        name: e.name,
        date: new Date(),
        by: "system"
      }))
    }
    const data = {
      userIds,
      id,
      parentId: refId,
      name,
      topic,
      dueDate,
      docIds: docsActions,
      recipients,
      notification,
      checklist: checklistApplicable ? checklist : [],
      relatedEntity,
      relatedActivity,
      relatedContact,
      subTopic,
      otherTopic,
      refRule,
      notes
    }

    /* const { unit, recurringType, recurringPattern1, recurringPattern2, startDate,
      frequency, endDate  } = data && data.notification
    const createControls = []
    let dueEndDate
    if (recurringType === "daily") {
      let nextDate = new Date(startDate)
      const lastDate = new Date(endDate)
      console.log("nextDate, endDate : ", nextDate, lastDate)
      while(nextDate && lastDate && (nextDate <= lastDate)) {
        console.log("nextDate : ", nextDate)
        // const recurData = { ...data }
        dueEndDate = moment(nextDate)
        createControls.push(dueEndDate)
        // createControlActions(token, recurData, userNames)

        nextDate = moment(new Date(nextDate)).add(1, "days").toDate()
      }
    }
    else if (recurringType === "once") {
      // const recurData = { ...data }
      dueEndDate = new Date()
      createControls.push(dueEndDate)
      // createControlActions(token, recurData, userNames)
    }
    else if (recurringType === "weekly") {
      // console.log("startDate1 : ", startDate)
      const preStartDate = moment(new Date(startDate))
        .subtract(recurringPattern1, `${unit}s`)
        .day(`${recurringPattern2}`)
        .subtract(1, "days")
        .toDate()
      let nextDate = moment(new Date(preStartDate)).add(frequency, `${unit}s`).toDate()
      const lastDate = new Date(endDate)
      console.log("nextDate, endDate : ", nextDate, lastDate)
      while(nextDate && lastDate && (nextDate <= lastDate)) {
        console.log("nextDate : ", nextDate)
        if(nextDate >= new Date(startDate)) {
          // const recurData = { ...data }
          dueEndDate = moment(nextDate)
          createControls.push(dueEndDate)
          // createControlActions(token, recurData, userNames)
        }
        nextDate = moment(new Date(nextDate)).add(frequency, `${unit}s`).toDate()
      }
      // console.log("startDate2 : ", startDate)
    } else if (recurringType === "monthly" || recurringType === "quarterly") {
      // console.log("startDate1 : ", startDate)
      const preStartDate = moment(new Date(startDate))
        .subtract(recurringPattern1, `${unit}s`)
        .date(`${recurringPattern2}`)
        .toDate()
      let nextDate = moment(new Date(preStartDate)).add(frequency, `${unit}s`).toDate()
      const lastDate = new Date(endDate)
      console.log("nextDate, endDate : ", nextDate, lastDate)
      while(nextDate && lastDate && (nextDate <= lastDate)) {
        console.log("nextDate : ", nextDate)
        if(nextDate >= new Date(startDate)) {
          // const recurData = { ...data }
          dueEndDate = moment(nextDate)
          createControls.push(dueEndDate)
          // createControlActions(token, recurData, userNames)
        }
        nextDate = moment(new Date(nextDate)).add(frequency, `${unit}s`).toDate()
      }
    } else if (recurringType === "yearly") {
      // console.log("startDate1 : ", startDate)
      let nextDate = new Date(startDate)
      const lastDate = new Date(endDate)
      while(nextDate && lastDate && (nextDate <= lastDate)) {
        // const recurData = { ...data }
        dueEndDate = moment(nextDate)
        createControls.push(dueEndDate)
        nextDate = moment(new Date(nextDate)).add(1, "y").toDate()
        // createControlActions(token, recurData, userNames)
      }
    } else {
      // createControlActions(token, data, userNames)
    } */
    createControlActionsTask(token, data)
  }

  /* getChangeLog(updatedControl, control, date, userId, userName) {
    console.log("updatedControl : ", updatedControl)
    console.log("control : ", control)
    const changeLog = []
    const fields = [
      "status",
      "name",
      "type",
      "target",
      "topic",
      "subTopic",
      "otherTopic",
      "refRule",
      "checklistApplicable",
      "notes",
      "state"
    ]
    const checklistFields = ["subject", "action", "answer"]
    const notificationFields = [
      "recurring",
      "recurringType",
      "recurringPattern1",
      "recurringPattern2",
      "startDate",
      "endDate"
    ]
    fields.forEach(f => {
      if (control[f] !== updatedControl[f]) {
        changeLog.push({
          userId,
          userName,
          log: `Control ${control.id} ${f} changed to ${
            updatedControl[f]
          } from ${control[f] || "No Value"}`,
          date
        })
      }
    })
    notificationFields.forEach(f => {
      if (control.notification[f] !== updatedControl.notification[f]) {
        changeLog.push({
          userId,
          userName,
          log: `Control ${control.id} notification attribute ${f} changed to ${
            updatedControl.notification[f]
          } from ${control.notification[f]}`,
          date
        })
      }
    })
    control.checklist.forEach((d, i) => {
      checklistFields.forEach(f => {
        if (d[f] !== updatedControl.checklist[i][f]) {
          changeLog.push({
            userId,
            userName,
            log: `Control ${control.id} checklist item "${
              d.subject
            }" ${f} changed to ${updatedControl.checklist[i][f]} from ${d[f]}`,
            date
          })
        }
      })
    })
    console.log("changeLog : ", changeLog)
    return changeLog
  } */

  async saveControls(control, src, auditLog, callback) {
    console.log("control : ", control)
    const changeLog = []
    const date = new Date()
    const { id, notificationSentDate, saveType, state } = control
    control = {
      ...control,
      notificationSentDate: notificationSentDate || new Date()
    }
    const controls = [...this.props.controls]
    const { token, entityId, userId, userName } = this.props
    const idx = controls.findIndex(e => e.id === id)
    console.log("src : ", src)
    /* if (idx > -1) {
      console.log("idx : ", idx)
      if (src === "new" || src === "copy") {
        changeLog.push({
          userId,
          userName,
          log: `Control ${control.id} - ${control.name} was created`,
          date
        })
      } else {
        changeLog = this.getChangeLog(
          control,
          controls[idx],
          date,
          userId,
          userName
        )
      }
      controls[idx] = control
    } else {
      controls.push(control)
      changeLog.push({
        userId,
        userName,
        log: `Control ${control.id} - ${control.name} was created`,
        date
      })
    } */
    /* if (
      quarterlyAffirmations.includes(control.topic) &&
      control.type === "affirm"
    ) {
      if (saveType === "final" && state !== "disabled") {
        const affirmActions = await createQuarterlyAffirmationActions(
          token,
          control
        )
        /!* if(affirmActions) {

        } *!/
        /!* this.createNewControlActions(control)
        this.createNewNotification(control) *!/
      }
    } else if (saveType === "final" && state !== "disabled") {
      this.createNewControlActions(control)
      this.createNewNotification(control)
    } */
    auditLog && auditLog.push({
      userId,
      userName,
      log: `${control.id} - ${control.name} was created`,
      date: new Date()
    })
    if(control && control.state === "enabled"){
      this.props.saveControls(saveType, token, entityId, control, callback, auditLog)
    } else {
      this.props.saveDisableControls(saveType, token, entityId, control, callback, auditLog)
    }

    // this.props.getControls(token)
  }

  renderControlDetails() {
    const { controlId, newControl, newOption } = this.state
    this.props.addNewControl([...this.props.controls, newControl])
    this.props.history.push(`/cac/controls-list/edit-control/${controlId}?src=${newOption}`)
  }

  renderTabs(tabs, nav2) {
    const { supervisor } = this.props
    const currentTab = nav2 || "monitor"
    const applicableTabs = supervisor
      ? tabs
      : tabs.filter(e => e.supervisor === false)
    return applicableTabs.map(t => (
      <li
        key={t.path}
        className={currentTab === t.path ? "is-active" : "inactive-tab"}
      >
        <NavLink to={`/cac/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))
  }

  renderViewSelection(nav2) {
    return (
      <div className="container">
        <div className="tabs is-boxed">
          <ul>{this.renderTabs(TABS, nav2)}</ul>
        </div>
      </div>
    )
  }

  renderSelectedView(
    nav2,
    nav3,
    token,
    entityId,
    firmName,
    userId,
    userEmail,
    controls
  ) {
    const {svControls} = this.state
    const { supervisor, userName } = this.props
    // console.log("nav2 : ", nav2)
    switch (nav2) {
    case "monitor":
      return (
      /* <MonitorDashboard
            token={token}
            entityId={entityId}
            supervisor={supervisor}
            userId={userId}
            userName={userName}
            userEmail={userEmail}
            svControls={svControls}
            firmName={firmName}
            controls={controls}
          /> */
        <CACDashboard
          svControls={svControls}
          token={token}
          entityId={entityId}
          supervisor={supervisor}
          userId={userId}
          firmName={firmName}
          userName={userName}
          userEmail={userEmail}
        />
      )
    case "controls-list": {
      const { existingQuarterlyAffirmations } = this.state
      const Component = withRouter(ControlAdministration)
      return (
        <Component controls={controls} saveAll={this.saveControls} getNewControl={this.getNewControl} {...this.props} addNewControl={this.addNewControl} existingQuarterlyAffirmations={existingQuarterlyAffirmations}/>
      )
    }
    /* case "edit-control": {
        const { existingQuarterlyAffirmations } = this.state
        if (!nav3) {
          const controls = [...this.props.controls]
          const ids = controls.map(c => +c.id.split("CTRL")[1])
          ids.sort((a, b) => b - a)
          const id = `CTRL${(ids[0] || 0) + 1}`
          controls.push(this.getNewControl(id, "new"))
          return (
            <ControlDetails
              id={id}
              token={token}
              entityId={entityId}
              existingQuarterlyAffirmations={existingQuarterlyAffirmations}
              userId={userId}
              userEmail={userEmail}
              controls={controls}
              saveControls={this.saveControls}
            />
          )
        }
        return (
          <ControlDetails
            id={nav3}
            token={token}
            entityId={entityId}
            existingQuarterlyAffirmations={existingQuarterlyAffirmations}
            userId={userId}
            userEmail={userEmail}
            controls={controls}
            saveControls={this.saveControls}
          />
        )
      } */
    case "view-control":
      return (
        <UserActivity
          userName={userName}
          userId={userId}
          id={nav3}
          {...this.props}
          token={token}
          supervisor={supervisor}
          svControls={svControls}
        />
      )
    case "audit":
      return <Audit nav1="cac" nav2="cac"/>
    default:
      return <p>{nav2}</p>
    }
  }

  render() {
    let {
      nav2,
      nav3,
      token,
      controls,
      entityId,
      userId,
      userEmail,
      firmName,
      supervisor
    } = this.props
    const { waiting } = this.state

    if ( /* (supervisor !== true && supervisor !== false) || */ waiting) {
      return <Loader />
    }

    if (supervisor) {
      nav2 = nav2 || "monitor"
    } else if (
      !TABS.filter(e => !e.supervisor)
        .map(e => e.path)
        .includes(nav2)
    ) {
      nav2 = "view-control"
    }

    return (
      <div>
        <section className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            {this.renderViewSelection(nav2)}
          </div>
        </section>
        {this.renderSelectedView(
          nav2,
          nav3,
          token,
          entityId,
          firmName,
          userId,
          userEmail,
          controls
        )}
      </div>
    )
  }
}

const mapStateToProps = ({
  controls,
  auth: {
    token,
    userEntities: {
      entityId,
      userId,
      firmName,
      msrbFirmName,
      userFirstName,
      userLastName,
      settings
    },
    userEmail,
    cacSupervisor
  }
}) => ({
  controls,
  token,
  entityId,
  firmName: firmName || msrbFirmName,
  userName: `${userFirstName} ${userLastName}`,
  userId,
  userEmail,
  supervisor: cacSupervisor,
  audit: settings && settings.auditFlag
})

export default connect(
  mapStateToProps,
  {
    addNewControl,
    saveControls,
    saveDisableControls,
    checkCACSupervisor
  }
)(withRouter(CACMain))
