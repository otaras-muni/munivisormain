import React, { Component } from "react"
import {Link} from "react-router-dom"
import Loader from "Global/Loader"
import ControlView from "./ControlView"
import CACActionList from "./CACActionList"
import AuditLog from "Global/AuditLog"

const tabs = [{
  title: <span>Actions</span>,
  value: 1,
  path: "view-actions",
}]

class UserActivity extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
    }
  }

  componentWillMount() {
    const {nav3} = this.props
    if(nav3){
      if (nav3 === "view-actions"){
        tabs.splice(1,1)

      }
      if (nav3 === "view-detail"){
        tabs.splice(1,1)
        tabs.push({
          title: <span>Action Detail</span>,
          value: 2,
          path: "view-detail",
        })
      }
    } else {
      tabs.splice(1,1)
      this.props.history.push("/cac/view-control/view-actions")
    }
    this.setState({
      loading: false,
    })
  }

  renderTabs = (activeTab) => {
    return (
      <div className="tabs">
        <ul>
          {
            tabs.map(tab=> {
              const style = tab.path === "view-detail" ? {pointerEvents : "none"} : {}
              return (
                <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`} style={style}>
                  {
                    this.props.nav2 ? <Link to={`/cac/view-control/${tab.path}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                      <a role="button" className="tabSecLevel">
                        {tab.title}
                      </a>
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }

  renderSelectedView = () => {
    const { token, userName, userId, supervisor, nav3, svControls } = this.props
    switch(nav3) {
    case "view-actions" :
      return <CACActionList
        userName={userName}
        userId={userId}
        id={nav3}
        token={token}
        svControls={svControls}
        supervisor={supervisor}/>
    case "view-detail": {
      return (
        <ControlView
          userName={userName}
          userId={userId}
          id={nav3}
          token={token}
          supervisor={supervisor}
        />
      )
    }
    // case "audit" :
    // return <AuditLog type="cac"/>
    default:
      return "not Found"
    }
  }

  render() {
    const activeTab = tabs.find(x => x && (x.path === this.props.nav3))
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="column">
          {this.props.nav2 && this.renderTabs(activeTab && activeTab.value) }
        </div>
        <div>
          {this.renderSelectedView()}
        </div>
      </div>
    )
  }
}

export default UserActivity
