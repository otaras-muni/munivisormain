import React, { Component } from "react"
import { connect } from "react-redux"
import {Link, withRouter} from "react-router-dom"
import { toast } from "react-toastify"
import * as qs from "query-string"
import CONST, { FRONTENDURL }  from "GlobalUtils/consts"
import { getCurrentQuarter, getViewOnlyControl } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/Loader"
import DocLink from "../docs/DocLink"
import {fetchSpecificControlsActions, saveControlsAction} from "../../StateManagement/actions/cac"
import Disclaimer from "../../GlobalComponents/Disclaimer"
import {fetchDocDetails} from "../../StateManagement/actions/Transaction"
import DocModalDetails from "../docs/DocModalDetails"

class ControlView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedId: "", docIds: [], checklist: [], viewOnly: false, searchString: "", showModal: false,
      control: {}, comments: "", expanded: false, status: "", waiting: true, auditLog: []
    }
    this.goBack = this.goBack.bind(this)
    this.changeComments = this.changeComments.bind(this)
    this.toggleAccordion = this.toggleAccordion.bind(this)
    this.save = this.save.bind(this)
    this.saveAndComplete = this.saveAndComplete.bind(this)
    this.cancel = this.cancel.bind(this)
  }

  async componentDidMount() {
    const queryString = qs.parse(location.search)
    const selectedId = queryString && queryString.id || ""
    const query = this.props.location && this.props.location.search
    let params
    let cid
    if (query) {
      params = qs.parse(query, { ignoreQueryPrefix: true }) || {}
      cid = (params && params.cid) || ""
    }
    if (cid) {
      this.selectControl(cid)
    }
    // await this.props.getControlsActions(this.props.token)
    const action = await fetchSpecificControlsActions(selectedId)

    this.setState({
      control: action || {},
      waiting: false
    }, () => this.setDataInState(selectedId))
  }

  /* componentWillUnmount() {
    const { token, supervisor } = this.props
    if (supervisor) {
      this.props.getControls(token, supervisor)
    }
  } */

  onSaveCallback = (err, action) => {
    this.setState({ waiting: false })
    if (err) {
      toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    } else {
      toast("Changes Saved", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
      this.setState({
        control: action || {}
      }, () => {
        const save = action && action.status || ""
        this.setDataInState(action && action._id, save)
      }
      )
    }
  }

  async setDataInState(selectedId, save) {
    const {control} = this.state
    const viewOnly = control && control.status === "complete"
    /* if(!control) {
      control = await getViewOnlyControl(selectedId, this.props.token) || {}
      viewOnly = true
    } */
    const docIds = control && control.docIds || []
    const checklist = control && control.checklist || []
    const comments = control && control.comments || ""
    const status = control && control.status || ""
    this.setState({
      selectedId, docIds, checklist, comments, status, viewOnly, control
    },() => {
      if(save) {
        this.renderSelectedControl()
      }
    })
  }

  getControlNotes(control) {
    // console.log("getControlNotes ", control)
    if (/^CTRLTTACC/.test(control.id)) {
      console.log("CTRLTTACC")
      if (control.meta && Object.keys(control.meta).length) {
        const { clientName, transactionUrl, contractUrl } = control.meta
        return (
          <span>
            Client Name: {clientName}. Please note: A <a href={transactionUrl}>transaction</a> has been created.
            Please ensure the appropriate disclosure, disclaimer and/or engagement letter as appropriate is uploaded / available
            accordingly to <a href={contractUrl}>contracts</a>
          </span>
        )
      }
    } else {
      return (
        control.tranUrl ? <a href={`${FRONTENDURL}/${control.tranUrl}`}>{control.notes}</a> : control.notes
      )
    }
  }

  onBlurInput = (e) => {
    const {user, userId} = this.props
    const {auditLog} = this.state
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    if (name && value) {
      const audit = {
        userId,
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `${name || "empty"} change to ${value || "empty"} in User Activities`,
        date: new Date()
      }
      auditLog.push(audit)
    }
  }

  getChangeLog(control, updatedControl) {
    const { userName, userId } = this.props
    const fields = ["status", "comments"]
    const checklistFields = ["answer"]
    const date = new Date()
    const changeLog = []
    if(control && Array.isArray(control)){
      control.docIds.forEach((d, i) => {
        if (d && updatedControl.docIds[i] && d.action !== updatedControl.docIds[i].action) {
          changeLog.push({
            userId,
            userName,
            log: `Control Action ${control.id} document status changed to ${updatedControl.docIds[i].action} from ${d.action} for ${d.name || "No Value"}`,
            date
          })
        }
      })
    }

    fields.forEach(f => {
      if (control && updatedControl && control[f] !== updatedControl[f]) {
        changeLog.push({
          userId,
          userName,
          log: `Control Action ${control.id} ${f} changed to ${updatedControl[f]} from ${control[f] || "No Value"}`,
          date
        })
      }
    })
    if(control && Array.isArray(control.checklist)){
      control.checklist.forEach((d, i) => {
        checklistFields.forEach(f => {
          if (d && updatedControl.checklist[i] && d[f] !== updatedControl.checklist[i][f]) {
            changeLog.push({
              userId,
              userName,
              log: `Control Action ${control.id} checklist item "${d.subject}" ${f} changed to ${updatedControl.checklist[i][f]} from ${d[f]}`,
              date
            })
          }
        })
      })
    }
    return changeLog
  }

  selectControl(selectedId) {
    console.log("in selectControl")
    console.log("selectedId : ", selectedId)
    this.setDataInState(selectedId)
  }

  goBack() {
    this.setState({
      selectedId: "", docIds: [], checklist: [],
      comments: "", expanded: false, status: ""
    }, () => this.props.history.push("/cac/view-control/view-actions"))
  }

  save() {
    const {control, auditLog} = this.state
    this.setState({ waiting: true })
    const { selectedId, docIds, checklist, comments, status } = this.state
    // const control = this.props.controls.filter(e => e._id === selectedId)[0]
    const updatedControl = { ...control, docIds, checklist, comments, status }
    /* const changeLog = this.getChangeLog(control, updatedControl) */
    this.props.saveControlsAction(this.props.token, selectedId, updatedControl, auditLog, (err, action) => this.onSaveCallback(err, action))
  }

  saveAndComplete() {
    this.setState({ status: "complete" }, this.save)
  }

  cancel() {
    this.setDataInState(this.state.selectedId)
  }

  toggleAccordion() {
    this.setState(prevState => ({ expanded: !prevState.expanded }))
  }

  changeComments(e) {
    this.setState({ comments: e.target.value })
  }

  changeDocAction(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const docIds = [...prevState.docIds]
      docIds[i] = { ...docIds[i] }
      docIds[i].action = value
      return { docIds }
    })
  }

  changeChecklistAnswer(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const checklist = [...prevState.checklist]
      checklist[i] = { ...checklist[i] }
      checklist[i].answer = value
      return { checklist }
    })
  }

  onFileAction = (e, docId) => {
    const { name, value } = e.target
    if (value === "history") {
      fetchDocDetails(docId).then(res => {
        this.handleDocDetails(res)
      })
    }
    this.setState({
      [name]: value
    })
  }

  handleDocDetails = (res) => {
    this.setState({
      showModal: !this.state.showModal,
      doc: res || {}
    })
  }

  renderDocActionsTable() {
    const { docIds } = this.state
    return (
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>
              <p className="multiExpLbl " title="What do you want to do with the document?">Action</p>
            </th>
            <th>
              <p className="multiExpLbl ">Filename</p>
            </th>
            <th>
              <p className="multiExpLbl " title="">Last Updated On</p>
            </th>
            <th>
              <p className="multiExpLbl " title="">Last Updated By</p>
            </th>
          </tr>
        </thead>
        <tbody>
          {docIds && docIds.map((e, i) => (
            <tr key={e.docId}>
              <td className="multiExpTblVal">
                <div className="select is-small is-link">
                  <select value={docIds[i].action || ""} name="state"
                    // onChange={this.changeDocAction.bind(this, i)}
                    onChange={e => this.onFileAction(e, docIds[i].docId)}
                    onBlur={(event) => this.onBlurInput(event)}>
                    <option value="" disabled>Action</option>
                    <option value="history">See version history</option>
                    {/* <option value="read">Acknowledge read</option>
                    <option value="under review">Mark as under review</option>
                    <option value="upload revised">Upload revised version</option>
                    <option value="reviewed">Mark as reviewed</option> */}
                  </select>
                </div>
              </td>
              <td className="multiExpTblVal">
                {e.docId ? <DocLink docId={e.docId} /> : ""}
              </td>
              <td className="multiExpTblVal">
                <small>{e.date && (new Date(e.date)).toLocaleString()}</small>
              </td>
              <td className="multiExpTblVal">
                <small>{e.by}</small>
              </td>
            </tr>
          ))
          }
        </tbody>
      </table>
    )
  }

  renderChecklistItems() {
    const { checklist } = this.state
    return checklist.map((e, i) => (
      <tr key={i}>
        <td className="soeExpLblVal">
          {e.subject}
        </td>
        <td className="multiExpTblVal">
          <div className="select is-small is-link">
            <select value={e.answer} onChange={this.changeChecklistAnswer.bind(this, i)} onBlur={(event) => this.onBlurInput(event)}>
              <option value="" disabled>NA</option>
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </select>
          </div>
        </td>
      </tr>
    ))
  }

  renderChecklist() {
    return (
      <div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl " title="">Subject</p>
              </th>
              <th>
                <p className="multiExpLbl " title="">Affirm</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.renderChecklistItems()}
          </tbody>
        </table>
      </div>
    )
  }

  renderControlInfo(expanded, control) {
    const { notification } = control
    let recurringType = "NA"
    if(notification) {
      recurringType = `recurring ${notification.recurringType || ""}`
    }
    return (
      <section className="accordions">
        <article className={expanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.toggleAccordion}>
            <p>Click here to see detail about the control</p>
            <i className={expanded ? "fas fa-chevron-down" : "fas fa-chevron-up"} style={{ cursor: "pointer" }} />
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Control ID</p>
                  <p className="multiExpTblVal">{control && control.id}</p>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Status</p>
                  <p className="multiExpTblVal">{control && control.status}</p>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Notifications</p>
                  <p className="multiExpTblVal">{recurringType}</p>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Name</p>
                  <p className="multiExpTblVal">{control && control.name}</p>
                </div>
              </div>

              <div className="columns">
                <div className="column">
                  <p className="multiExpGrpLbl">What is this concerning?</p>
                  <hr />
                  {/* <div className="columns">
                    <div className="column">
                      <p className="multiExpLbl ">Related Activity</p>
                      <p className="multiExpTblVal">
                        {control && control.relatedActivity && control.relatedActivity.map(e => e.name).toString()}</p>
                    </div>
                    <div className="column">
                      <p className="multiExpLbl ">Related Entity</p>
                      <p className="multiExpTblVal">
                        {control && control.relatedEntity && control.relatedEntity.map(e => e.name).toString()}</p>
                    </div>
                    <div className="column">
                      <p className="multiExpLbl ">Related Contact</p>
                      <p className="multiExpTblVal">
                        {control && control.relatedContact && control.relatedContact.map(e => e.name).toString()}</p>
                    </div>
                  </div> */}
                  <div className="columns">
                    <div className="column">
                      <p className="multiExpLbl">Topic</p>
                      <p className="multiExpTblVal">{control && control.topic}</p>
                    </div>
                    <div className="column">
                      <p className="multiExpLbl">Sub-Topic</p>
                      <p className="multiExpTblVal">{control && control.subTopic}</p>
                    </div>
                    <div className="column">
                      <p className="multiExpLbl">Other (please specify)</p>
                      <p className="multiExpTblVal">{control && control.otherTopic}</p>
                    </div>
                  </div>
                  <div className="columns">
                    <div className="column">
                      <p className="multiExpLbl">Reference rule</p>
                      <p className="multiExpTblVal">{control && control.refRule}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </section>
    )
  }

  renderSelectedControl() {
    // const control = this.props.controls.filter(e => e._id === this.state.selectedId)[0]
    const { comments, expanded, viewOnly, control } = this.state
    const disabled = (control && control.status === "complete") || (control && control.status === "Complete") || (control && control.status === "Dropped") || viewOnly
    if (control) {
      return <div className="box">
        <a onClick={this.goBack}>View the list of assigned controls</a>
        <hr />
        <div className="columns">
          <div className="column">
            <p className="multiExpGrpLbl">What does the sender need from you?</p>
            <br />
            <div className="columns">
              <div className="column is-full">
                <div className="field">
                  <p className="multiExpLbl " title="Notes and instructions by transaction participants">Notes/Instructions</p>
                  <p className="multiExpTblVal">{this.getControlNotes(control)}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {control.docIds && control.docIds.length ?
          this.renderDocActionsTable()
          : undefined
        }
        {control.checklist && control.checklist.length ?
          this.renderChecklist()
          : undefined
        }
        <div className="columns">
          <div className="column is-full">
            <div className="field">
              <p className="multiExpLbl " title="Notes and instructions by transaction participants">Responder Comments</p>
              <div className="control">
                <textarea
                  className="textarea is-link"
                  name="comments"
                  value={comments}
                  onChange={this.changeComments}
                  onBlur={(event) => this.onBlurInput(event)}
                  placeholder="" />
              </div>
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link"
                  disabled={disabled}
                  onClick={this.save}>Save</button>
              </div>
              <div className="control">
                <button className="button is-link"
                  disabled={disabled}
                  onClick={this.saveAndComplete}>Save & Mark Action Complete</button>
              </div>
              <div className="control">
                <button className="button is-light"
                  disabled={disabled}
                  onClick={this.cancel}>Cancel</button>
              </div>
            </div>
          </div>
        </div>
        <hr />
        {this.renderControlInfo(expanded, control)}
      </div>
    }
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Control Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Compliance Topic</p>
        </th>
        <th>
          <p className="emmaTablesTh">Status</p>
        </th>
      </tr>
    )
  }

  renderControlsRows(controls) {
    const rows = controls.map((d, i) =>
      <tr key={d._id ? d._id + i : i}>
        <td className="emmaTablesTd">
          <a onClick={this.selectControl.bind(this, d._id)}>{d.id}</a>
        </td>
        <td className="emmaTablesTd">
          <small>{d.name}</small>
        </td>
        <td className="emmaTablesTd">
          <small>
            {d.topic}
          </small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.status}</small>
        </td>
      </tr>
    )
    return rows
  }

  renderControlsList() {
    let controls = [...this.props.controls]
    const [currentQuarter, currentYear] = getCurrentQuarter()
    controls = controls.filter(e => {
      if (e.meta && e.meta.quarter && e.meta.year) {
        if (currentQuarter >= e.meta.quarter && currentYear >= e.meta.year) {
          return true
        }
        return false
      }
      return true
    })
    return (
      <div className="box" style={{overflowX:"auto"}}>
        <table className="table is-striped is-fullwidth is-bordered">
          <thead >
            {this.renderTableHeader()}
          </thead>
          <tbody>
            {this.renderControlsRows(controls)}
          </tbody>
        </table>
      </div>
    )
  }

  render() {
    const { selectedId, waiting, doc } = this.state

    if (waiting) {
      return <Loader />
    }

    return (
      <div id="main">
        { selectedId ? this.renderSelectedControl() : null }
        <Disclaimer />
        {this.state.showModal ? (
          <div>
            <DocModalDetails
              showModal={this.state.showModal}
              closeDocDetails={this.handleDocDetails}
              documentId={doc._id}
              // onDeleteAll={this.deleteDoc}
              docMetaToShow={["category", "subCategory"]}
              versionMetaToShow={["uploadedBy"]}
              docId={doc._id}
            />
          </div>
        ) : (
          null
        )}
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth && state.auth.userEmail,
  user: (state.auth && state.auth.userEntities) || {},
})
// const mapStateToProps = ({ controls }) => ({ controls })
export default connect(mapStateToProps, { saveControlsAction })(withRouter(ControlView))
