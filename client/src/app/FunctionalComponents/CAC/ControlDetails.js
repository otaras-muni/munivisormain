import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"
import { Multiselect } from "react-widgets"
import moment from "moment"
import { toast } from "react-toastify"
import * as qs from "query-string"
const ObjectID = require("bson-objectid")

import CONST, { muniApiBaseURL, TIME_UNITS, DAY_WEEK_MONTHS, ContextType } from "GlobalUtils/consts.js"
import { getPicklistValues, getQuartersBetweenDates, getCalendarDateAsString } from "GlobalUtils/helpers"
import { Modal } from "Global/BulmaModal"
import Loader from "../../GlobalComponents/Loader"

import DocUpload from "../docs/DocUpload"
import DocLink from "../docs/DocLink"
import DocModal from "../docs/DocModal"
import Disclaimer from "../../GlobalComponents/Disclaimer";
import {fetchIssuers, fetchUserTransactions} from "../../StateManagement/actions/CreateTransaction";
import {fetchSpecificControls} from "../../StateManagement/actions/cac";
import withAuditLogs from "../../GlobalComponents/withAuditLogs";
import {SelectLabelInput, TextLabelInput} from "../../GlobalComponents/TextViewBox"
import {sendComplianceEmail} from "../../StateManagement/actions/Transaction"

const requiredFields = ["target", "name", "recipients", "topic"]
const quarterlyAffirmations = [
  "Political Contributions and Prohibitions on Municipal Advisory Business",
  "Gifts and Gratuities"
]

class ControlDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalState: false,
      modalType: "",
      control: {},
      validationError: {},
      generalError: "",
      refId: "",
      name: "",
      type: "",
      topic: "",
      target: "",
      subTopic: "",
      refRule: "",
      otherTopic: "",
      notificationSentDate: null,
      actionCompletedBy: [],
      numActions: 0,
      dueDate: null,
      recurring: true,
      recurringType: "",
      recurringPattern1: 1,
      recurringPattern2: 1,
      frequency: 0,
      unit: "",
      startDate: null,
      endDate: null,
      recipients: [],
      toList: [],
      ccList: [],
      relatedActivity: [],
      relatedEntity: [],
      relatedContact: [],
      docIds: [],
      checklist: [],
      checklistApplicable: true,
      notes: "",
      state: "",
      status: "",
      createdBy: "",
      lastUpdatedAt: null,
      lastUpdatedBy: "",
      expanded: false,
      selectedChecklistItem: -1,
      topics: [],
      subTopics: {},
      users: [],
      issuerList: [],
      transactions: [],
      waiting: true,
      saveType: "draft",
      actionExpanded: true,
      patternExpanded: false,
      concerningExpanded: false,
      recipentsExpanded: false,
      uploadExpanded: false,
      clickExpanded: false,
      auditLog: [],
      tempControl: {},
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
    this.toggleSaveDraftModal = this.toggleSaveDraftModal.bind(this)
    this.toggleSaveFinalModal = this.toggleSaveFinalModal.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.validateSubmition = this.validateSubmition.bind(this)
    this.uploadDoc = this.uploadDoc.bind(this)
    this.deleteDoc = this.deleteDoc.bind(this)
    this.changeField = this.changeField.bind(this)
    this.changeFrequencyField = this.changeFrequencyField.bind(this)
    this.changeTarget = this.changeTarget.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.undoChanges = this.undoChanges.bind(this)
    this.changeRelatedActivity = this.changeRelatedActivity.bind(this)
    this.changeRecipients = this.changeRecipients.bind(this)
    this.changeToList = this.changeToList.bind(this)
    this.changeCCList = this.changeCCList.bind(this)

    this.toggleActionAccordion = this.toggleActionAccordion.bind(this)
    this.togglePatternAccordion = this.togglePatternAccordion.bind(this)
    this.toggleConcerningAccordion = this.toggleConcerningAccordion.bind(this)
    this.toggleRecipentsAccordion = this.toggleRecipentsAccordion.bind(this)
    this.toggleUploadAccordion = this.toggleUploadAccordion.bind(this)
    this.toggleClickAccordion = this.toggleClickAccordion.bind(this)

    this.addChecklistItem = this.addChecklistItem.bind(this)
    this.onSaveCallback = this.onSaveCallback.bind(this)
    this.SaveAndNotify = this.SaveAndNotify.bind(this)
    this.toggleDisableModal = this.toggleDisableModal.bind(this)
    this.changeControlState = this.changeControlState.bind(this)
    this.toggleDisableModalOnCancel = this.toggleDisableModalOnCancel.bind(this)
  }

  async componentDidMount() {
    const {id, token, userEmail} = this.props
    const queryString = qs.parse(location.search)
    const type = queryString.src || ""
    const copyId = queryString.id || ""
    let newControl = {}

    if (type === "new") {
      newControl = this.getNewControl(id, type, userEmail)
    } else if (type === "copy" && copyId) {
      const copyControl = await fetchSpecificControls(copyId)
      newControl = this.getCopyControl(id, type, userEmail, copyControl)
    } else if (id) {
      newControl = await fetchSpecificControls(id)
    }
    this.setState({
      control : newControl || {},
      waiting: false
    }, () => this.setControlInState(id))
    this.getTransactions()
    fetchIssuers(`${this.props.entityId}?type=Third Party`, res => {
      this.setState({
        issuerList: res.issuerList || []
      })
    })
    const res1 = await axios.get(`${muniApiBaseURL}controls/users`, {headers: {"Authorization": token}})
    if (res1 && res1.data) {
      this.setState({users: res1.data})
    }
    const [res2] = await getPicklistValues(["LKUPCMPLTOPICSUBTOPIC"])
    this.setState({ topics: res2[1], subTopics: res2[2] })
  }

  /* componentWillReceiveProps(nextProps) {
    const { controls, id } = nextProps
    this.setControlInState(controls, id)
  } */

  getNewControl(controlId, option, userEmail) {
    if (option === "new") {
      return {
        refId: ObjectID(),
        id: controlId,
        name: "",
        type: "alert",
        target: "others",
        topic: "",
        subTopic: "",
        otherTopic: "",
        refRule: "",
        notificationSentDate: null,
        actionCompletedBy: [],
        numActions: 0,
        dueDate: null,
        notification: {
          recurringType: "",
          recurringPattern1: 1,
          recurringPattern2: 1,
          recurring: false,
          frequency: 0,
          unit: "",
          startDate: null,
          endDate: null
        },
        recipients: [],
        toList: [],
        ccList: [],
        relatedActivity: [],
        relatedEntity: [],
        relatedContact: [],
        docIds: [],
        checklist: [],
        checklistApplicable: true,
        notes: "",
        state: "enabled",
        status: "open",
        saveType: "draft",
        createdBy: userEmail,
        lastUpdated: {
          date: new Date(),
          by: userEmail
        }
      }
    }
  }

  getCopyControl(controlId, option, userEmail, control) {
    if (option === "copy") {
      return {
        ...control,
        refId: ObjectID(),
        id: controlId,
        notificationSentDate: null,
        actionCompletedBy: [],
        numActions: 0,
        dueDate: null,
        notification: {
          ...control.notification,
          startDate: control.notification.startDate
            ? new Date(control.notification.startDate).toISOString()
            : null,
          endDate: control.notification.endDate
            ? new Date(control.notification.endDate).toISOString()
            : null
        },
        state: "enabled",
        status: "open",
        saveType: "draft",
        recipients: [...control.recipients],
        toList: [...control.toList],
        ccList: [...control.ccList],
        relatedActivity: [...control.relatedActivity],
        relatedEntity: [...control.relatedEntity],
        relatedContact: [...control.relatedContact],
        docIds: [],
        checklist: control.checklist
          ? control.checklist.map(e => ({ ...e }))
          : [],
        createdBy: userEmail,
        lastUpdated: {
          date: new Date(),
          by: userEmail
        }
      }
    }
  }

  setControlInState(id) {
    const {control} = this.state
    // const control = controls.filter(e => e.id === id)[0]
    // console.log("controls : ", controls)
    console.log("id : ", id)
    console.log("control : ", control)
    if (control) {
      const { refId, name, type, target, topic, subTopic, otherTopic, refRule,
        notificationSentDate, numActions, dueDate,
        notification: { recurring, frequency, unit, startDate, endDate,
          recurringType, recurringPattern1, recurringPattern2 },
        recipients, toList, ccList, relatedActivity, createdBy, saveType,
        relatedEntity, relatedContact, docIds, checklist, notes, status, state,
        lastUpdated: { date, by }, checklistApplicable } = control
      let actionCompletedBy = []
      if(Array.isArray(control.actionCompletedBy)) {
        actionCompletedBy = [ ...control.actionCompletedBy ]
      }
      const validationError = {}
      const tempControl = control || {}
      /* if (type === "affirm" && quarterlyAffirmations.includes(topic)) {
        if (this.findExistingQuarterlyAffirmations(this.props.existingQuarterlyAffirmations, startDate, endDate, topic)) {
          validationError.topic = `Affirmation -${topic}- already exists for a quarter in the selected dates`
        }
      } */
      this.setState({
        control, refId, name, type, target, topic, subTopic,
        otherTopic, refRule, notificationSentDate, actionCompletedBy,
        numActions, dueDate, recurring, recurringType, recurringPattern1,
        recurringPattern2, frequency, unit, recipients, saveType,
        toList, ccList, relatedActivity, createdBy, state, startDate, endDate,
        relatedEntity, relatedContact, docIds, checklist, notes, status,
        lastUpdatedAt: date, lastUpdatedBy: by, checklistApplicable,
        validationError, generalError: "", tempControl
      })
    } else {
      console.log("No control found. Rendering default value")
    }
  }

  getTransactions = async () => {
    const res = await fetchUserTransactions(this.props.entityId)
    let transactions = []
    if (res.transactions && Array.isArray(res.transactions)) {
      transactions = res.transactions.map(tran => ({
        ...tran,
        name:
          tran.rfpTranProjectDescription ||
          tran.dealIssueTranProjectDescription ||
          tran.actTranProjectDescription ||
          tran.actProjectName ||
          "",
        id: tran._id, // eslint-disable-line
        group: tran.actTranType ? tran.actTranType : tran.actType
          ? tran.actType : tran.rfpTranType === "RFP"
            ? tran.rfpTranType : tran.dealIssueTranSubType === "Bond Issue" ? "Deal" : ""
      }))
    }
    this.setState({
      transactions
    })
  }

  toggleSaveDraftModal() {
    this.toggleModal("draft")
  }

  toggleSaveFinalModal() {
    this.toggleModal("final")
  }

  toggleDisableModal() {
    this.toggleModal("disable")
  }

  toggleDisableModalOnCancel() {
    this.toggleModal("disable", "cancel")
  }

  changeControlState(e) {
    const { value } = e.target
    this.setState({ state: value }, this.toggleDisableModal)
  }

  addChecklistItem(e) {
    let {clickExpanded} = this.state
    e.stopPropagation()
    this.setState(prevState => {
      const checklist = [...prevState.checklist]
      const selectedChecklistItem = checklist.length + 1
      checklist.push({
        subject: "",
        action: "show",
        answer: ""
      })
      clickExpanded = true
      return { checklist, selectedChecklistItem, clickExpanded }
    })
  }

  handleKeyPressInChecklistItem(i, e) {
    if (e.key === "Enter") {
      this.setState({ selectedChecklistItem: -1 })
    }
  }

  removeChecklistItem(i) {
    this.setState(prevState => {
      const checklist = [...prevState.checklist]
      checklist.splice(i, 1)
      return { selectedChecklistItem: -1, checklist }
    })
  }

  changeField(e) {
    const { name, type } = e.target
    let value = type === "checkbox" ? e.target.checked : e.target.value

    this.setState((prevState, props) => {
      console.log("name : ", name)
      console.log("value : ", value)
      const validationError = { ...prevState.validationError }
      if (requiredFields.includes(name)) {
        if (!value) {
          validationError[name] = "Please provide input"
        } else {
          validationError[name] = ""
        }
      }
      if (name === "endDate") {
        const { topic, type, startDate } = prevState
        value = value === "" ? null : moment(value).format("YYYY-MM-DD")
        if (startDate && value && (new Date(startDate) > new Date(value))) {
          validationError.endDate = "End date cannot before the start date"
        } else {
          validationError.endDate = ""
        } /* else if (type === "affirm" && quarterlyAffirmations.includes(topic) && value) {
          if (this.findExistingQuarterlyAffirmations(props.existingQuarterlyAffirmations, startDate, value, topic)) {
            validationError.endDate = `Affirmation -${value}- already exists for a quarter in the selected dates`
          } else {
            validationError.startDate = ""
            validationError.endDate = ""
            validationError.type = ""
            validationError.topic = ""
          }
        } */
      }
      if (name === "startDate") {
        const { topic, type, endDate } = prevState
        value = value === "" ? null : moment(value).format("YYYY-MM-DD")
        if (endDate && value && (new Date(endDate) < new Date(value))) {
          validationError.startDate = "Start date cannot after the end date"
        } else {
          validationError.startDate = ""
        }
        /* else if (value && moment(value).format("YYYY-MM-DD") < moment(new Date()).format("YYYY-MM-DD")) {
          validationError.startDate = "Start date cannot before today's date"
        } else if (type === "affirm" && quarterlyAffirmations.includes(topic) && value) {
          if (this.findExistingQuarterlyAffirmations(props.existingQuarterlyAffirmations, value, endDate, topic)) {
            validationError.startDate = `Affirmation -${value}- already exists for a quarter in the selected dates`
          } else {
            validationError.startDate = ""
            validationError.endDate = ""
            validationError.type = ""
            validationError.topic = ""
          }
        } */
      }
      if (name === "topic") {
        if (quarterlyAffirmations.includes(value)) {
          // let { type, startDate, endDate } = prevState
          const type = "affirm"
          if (type === "affirm") {
            /* if (this.findExistingQuarterlyAffirmations(props.existingQuarterlyAffirmations, startDate, endDate, value)) {
              validationError.topic = `Affirmation -${value}- already exists for a quarter in the selected dates`
              return { [name]: value, validationError, generalError: "" }
            } */
            validationError.topic = ""
            const recurringType = value === "Gifts and Gratuities" ? "yearly" : "quarterly"
            const recurring = true
            const recurringPattern1 = value === "Gifts and Gratuities" ? 12 : 3
            const frequency = 3
            const unit = value === "Gifts and Gratuities" ? "year" : "month"
            return ({
              [name]: value, validationError, generalError: "", type,
              recurringType, recurring, recurringPattern1, frequency, unit
            })
          }
        }
        validationError.startDate = ""
        validationError.endDate = ""
        validationError.topic = ""
        validationError.type = ""
      }
      if (name === "type") {
        this.setState({topic: ""})
        if (value === "affirm") {
          const { topic, startDate, endDate } = prevState
          if (quarterlyAffirmations.includes(topic)) {
            /* if (this.findExistingQuarterlyAffirmations(props.existingQuarterlyAffirmations, startDate, endDate, topic)) {
              validationError.type = `Affirmation -${value}- already exists for a quarter in the selected dates`
              return { [name]: value, validationError, generalError: "" }
            } */
            validationError.type = ""
            const recurringType = topic === "Gifts and Gratuities" ? "yearly" : "quarterly"
            const recurring = true
            const recurringPattern1 = topic === "Gifts and Gratuities" ? 12 : 3
            const frequency = 3
            const unit = topic === "Gifts and Gratuities" ? "year" : "month"
            return ({
              [name]: value, validationError, generalError: "",
              recurringType, recurring, recurringPattern1, frequency, unit
            })
          }
        }
        validationError.startDate = ""
        validationError.endDate = ""
        validationError.type = ""
        validationError.topic = ""
      }
      return ({ [name]: value, validationError, generalError: "" })
    })
  }

  onSaveCallback(control) {
    this.setState({ waiting: false })
    if (control) {
      this.setState({ control })
      toast("Changes Saved", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    }
  }

  getRecurringUnit(recurringType) {
    let unit = ""
    if (recurringType === "weekly") {
      unit = "week"
    } else if (recurringType === "monthly" || recurringType === "quarterly") {
      unit = "month"
    } else if (recurringType === "yearly") {
      unit = "year"
    } else if (recurringType === "daily") {
      unit = "day"
    }
    return unit
  }

  findExistingQuarterlyAffirmations(existingQuarterlyAffirmations, startDate, endDate, topic) {
    const quarters = getQuartersBetweenDates(startDate, endDate).map(e => `${e[0]}-${e[1]}`)
    console.log("quarters : ", quarters)
    const { refId, saveType } = this.state
    if (!refId) {
      return false
    }
    console.log("refId, saveType : ", refId, saveType)
    let result = false
    existingQuarterlyAffirmations.some(e => {
      console.log("e : ", e)
      let found = false
      if (saveType === "final" && e.refId === refId && e.exists) {
        found = true
        return true
      }
      const lookIn = e.quarters.map(e => `${e[0]}-${e[1]}`)
      if (e.topic === topic && e.refId !== refId) {
        quarters.some(q => {
          if (lookIn.includes(q)) {
            found = true
            return true
          }
        })
      }
      if (found) {
        result = true
        return true
      }
    })
    return result
  }

  toggleModal(modalType, option) {
    modalType = modalType || ""
    this.setState((prev) => {
      const newState = !prev.modalState
      let { state } = prev
      if (modalType === "disable" && option === "cancel") {
        state = "enabled"
      }
      return { modalState: newState, modalType, state }
    })
  }

  toggleActionAccordion() {
    this.setState(prevState => ({ actionExpanded: !prevState.actionExpanded }))
  }

  togglePatternAccordion() {
    this.setState(prevState => ({ patternExpanded: !prevState.patternExpanded }))
  }


  toggleConcerningAccordion() {
    this.setState(prevState => ({ concerningExpanded: !prevState.concerningExpanded }))
  }


  toggleRecipentsAccordion() {
    this.setState(prevState => ({ recipentsExpanded: !prevState.recipentsExpanded }))
  }


  toggleUploadAccordion() {
    this.setState(prevState => ({ uploadExpanded: !prevState.uploadExpanded }))
  }

  toggleClickAccordion() {
    this.setState(prevState => ({ clickExpanded: !prevState.clickExpanded }))
  }

  uploadDoc(name, _id) {
    const {userId, user} = this.props
    const {auditLog} = this.state
    console.log("name, _id : ", name, _id)
    this.setState(prevState => {
      const docIds = [...prevState.docIds]
      const idx = docIds.findIndex(e => e._id === _id)
      if (idx === -1) {
        docIds.push({
          _id, name,
          docId: _id,
          date: new Date(),
          by: `${user.userFirstName} ${user.userLastName}`
        })
        const audit = {
          userId,
          userName: `${user.userFirstName} ${user.userLastName}`,
          log: `${name || "empty"} document uploaded in Control Details`,
          date: new Date()
        }
        auditLog.push(audit)
      } else {
        docIds[idx].name = name
      }
      return { docIds }
    })
  }

  deleteDoc(_id) {
    console.log("_id : ", _id)
    this.setState(prevState => {
      const docIds = [...prevState.docIds].filter(e => e._id !== _id)
      return { docIds }
    })
  }

  changeFrequencyField(e) {
    const { name, value } = e.target
    console.log("changeFrequencyField : ", name, value)
    this.setState(prevState => {
      let { recurringType, recurring, recurringPattern1, frequency, unit,
        startDate, endDate, validationError, patternExpanded } = prevState
      if (name === "recurringType") {
        if (!value || value === "once") {
          recurring = false
          frequency = 0
          unit = ""
        } else {
          recurring = true
          frequency = recurringPattern1
          unit = this.getRecurringUnit(value)
          console.log("changeFrequencyField unit : ", unit)
          if (unit === "day" || unit === "year") {
            frequency = 1
          }
          if (value === "quarterly") {
            recurringPattern1 = 3
            frequency = 3
          }
        }
      }
      if (name === "recurringPattern1") {
        if (recurring) {
          frequency = value
          // unit = this.getRecurringUnit(recurringType)
        }
      }
      // if(name === "recurringPattern2") {
      //   if(recurring) {
      //     unit = this.getRecurringUnit(recurringType)
      //   }
      // }
      if (!recurring) {
        startDate = null
        endDate = null
        validationError = { ...validationError }
        validationError.endDate = ""
        validationError.startDate = ""
      } else if(!patternExpanded){
        this.togglePatternAccordion()
      }

      return {
        recurring, frequency, unit, validationError, startDate, endDate,
        recurringPattern1, [name]: value
      }
    })
  }

  changeTarget(e) {
    const { value } = e.target
    this.setState((prevState, props) => {
      const recipients = []
      // const validationError
      if (value === "self") {
        const myUser = prevState.users.filter(e => e.id === props.userId)[0]
        if (myUser) {
          recipients.push(myUser)
        } else if (props.userId && props.userEmail) {
          recipients.push({ id: props.userId, name: props.userName || "", emails: [props.userEmail] })
        }
      } else if (value === "all") {
        recipients.push(...prevState.users)
      }
      return { target: value, recipients }
    })
  }

  changeRelatedActivity(relatedActivity, key) {
    const activity = relatedActivity.map((item) => ({id: item.id, name: item.name}))
    this.setState({
      [key]: activity
    })
  }

  changeRecipients(recipients) {
    // console.log("recipients : ", recipients)
    this.setState(prevState => {
      const validationError = { ...prevState.validationError }
      if (!recipients || !recipients.length) {
        validationError.recipients = "Please select recipients"
      } else {
        validationError.recipients = ""
      }
      const recipientsId = recipients.map(r => r.id)
      const actionCompletedBy = [ ...prevState.actionCompletedBy.filter(d =>
        recipientsId.includes(d.userId)) ]

      recipients.forEach(r => {
        const idx = actionCompletedBy.findIndex(d => d.userId === r.id)
        if(idx === -1) {
          actionCompletedBy.push({
            userId: r.id,
            userName: r.name,
            status: "due"
          })
        }
      })
      return { recipients, validationError, actionCompletedBy }
    })
  }

  changeToList(toList) {
    this.setState(prevState => {
      const validationError = { ...prevState.validationError }
      // if (!toList || !toList.length) {
      //   validationError.toList = "Please select toList"
      // } else {
      //   validationError.toList = ""
      // }

      return { toList, validationError }
    })
    // this.setState({ toList })
  }

  changeCCList(ccList) {
    this.setState({ ccList })
  }

  selectChecklistItem(selectedChecklistItem) {
    this.setState({ selectedChecklistItem })
  }

  changeChecklistItemSubject(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const checklist = [...prevState.checklist]
      checklist[i].subject = value
      return { checklist, selectedChecklistItem: i }
    })
  }

  changeChecklistAction(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const checklist = [...prevState.checklist]
      checklist[i].action = value
      return { checklist }
    })
  }

  changeChecklistAnswer(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const checklist = [...prevState.checklist]
      checklist[i].answer = value
      return { checklist }
    })
  }

  SaveAndNotify() {
    this.setState({ saveType: "final", modalState: false, modalType: "" },
      this.validateSubmition)
  }

  validateSubmition() {
    let {actionExpanded, concerningExpanded, recipentsExpanded, patternExpanded} = this.state
    console.log("in validateSubmition")
    this.setState((prevState, props) => {
      const validationError = { ...prevState.validationError }
      const { recurring, type, topic, startDate, endDate, target, recipients } = prevState
      const checkFields = recurring ?
        [...requiredFields, "startDate", "endDate"] : [...requiredFields]
      checkFields.forEach(f => {
        if (!prevState[f] || (Array.isArray(prevState[f]) && !prevState[f].length)) {
          validationError[f] = "Please provide input"
        }
      })
      if ((target === "self" || target === "all") && recipients && recipients.length) {
        validationError.recipients = ""
      }
      if (recurring && startDate && endDate &&
        (new Date(startDate) > new Date(endDate))) {
        validationError.startDate = "Start Date cannot be after the end date"
      }
      if (validationError && (validationError.name || validationError.recipients)) {
        actionExpanded = validationError && validationError.name || validationError && validationError.recipients
      }
      if (validationError && validationError.topic) {
        concerningExpanded = validationError && validationError.topic
      }
      if (validationError && validationError.recipients) {
        recipentsExpanded = validationError && validationError.recipients
      }
      if (validationError && (validationError.startDate || validationError.endDate)) {
        patternExpanded = validationError && (validationError.startDate || validationError.endDate)
      }

      /* if (startDate && endDate && type === "affirm" && quarterlyAffirmations.includes(topic)) {
        if (this.findExistingQuarterlyAffirmations(this.props.existingQuarterlyAffirmations, startDate, endDate, topic)) {
          validationError.topic = `Affirmation -${topic}- already exists for a quarter in the selected dates`
          return { validationError }
        }
      } */
      console.log("validationError : ", validationError)
      return { validationError, waiting: true, modalState: false, modalType: "", actionExpanded, concerningExpanded, recipentsExpanded, patternExpanded }
    }, this.saveChanges)
  }

  async saveChanges() {
    console.log("in saveChanges")
    let error = false
    const { control, type, refId, name, target, topic, subTopic,
      otherTopic, refRule, notificationSentDate, actionCompletedBy,
      dueDate, recurring, frequency, unit, recipients, recurringType,
      recurringPattern1, recurringPattern2,
      toList, ccList, relatedActivity, createdBy, startDate, endDate,
      relatedEntity, relatedContact, docIds, checklist, notes, status, state,
      checklistApplicable, validationError, saveType, auditLog, email } = this.state
    if(saveType === "final" || saveType === "draft") {
      const errKeys = Object.keys(validationError)
      errKeys.some(k => {
        if (validationError[k]) {
          error = true
          return true
        }
      })
      console.log("validationError : ", validationError, error)
      if (error) {
        toast("Please fix highlighted errors", { autoClose: 5000, type: toast.TYPE.WARNING });
        this.setState({
          waiting: false, modalState: false, modalType: "", saveType: "draft"
        })
        return
      }
    }
    const updatedControl = {
      id: control.id, refId, name, type, target, topic, subTopic,
      notificationSentDate, actionCompletedBy, numActions: recipients.length, dueDate,
      notification: {
        recurring, frequency, unit, startDate, endDate,
        recurringType, recurringPattern1, recurringPattern2
      }, saveType,
      recipients, toList, ccList, relatedActivity, otherTopic, refRule,
      createdBy: createdBy || this.props.userEmail, checklistApplicable,
      relatedEntity, relatedContact, docIds, checklist, notes, status, state,
      lastUpdated: { date: new Date(), by: this.props.userEmail }
    }
    if (startDate && endDate && type === "affirm" && quarterlyAffirmations.includes(topic)) {
      const quarters = getQuartersBetweenDates(startDate, endDate)
      updatedControl.numActions = recipients.length * quarters.length
    }
    this.setState({ control: updatedControl, generalError: "" })

    const queryString = qs.parse(location.search)
    const saveSrc = queryString.src
    this.props.saveAll(updatedControl, saveSrc, auditLog, this.onSaveCallback)

    if(!(control && control._id)) {
      const sendEmail = recipients && recipients.map(item => {
        const isEmailPrimary = item && item.emails && item.emails.find(ent => ent.emailPrimary) || {}
        const email = Object.keys(isEmailPrimary || {}).length ? isEmailPrimary.emailId : item.emails[0] && item.emails[0].emailId
        const newObject = {
          name: item.name,
          sendEmailTo: email || ""
        }
        return newObject
      })

      const description = `Compliance Control Action Center - topic: ${topic} is created`
      const emailParams = {
        type: "cac",
        sendEmailUserChoice:true,
        emailParams: {
          url: window.location.pathname.replace("/",""),
          ...email,
          category: "custom",
          subject: description || "",
          message: "",
          sendEmailTo: sendEmail || []
        }
      }
      await sendComplianceEmail(emailParams)
    }
  }

  undoChanges() {
    const { controls, id } = this.props
    this.setControlInState(controls, id)
  }

  undoCheckListChanges = () => {
    const {tempControl} = this.state
    if (tempControl) {
      const checklist = [...tempControl.checklist]
      this.setState({ checklist })
    }
  }

  onBlurInput = (e) => {
    const {user, userId} = this.props
    const {auditLog} = this.state
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    if (name && value) {
      const audit = {
        userId,
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `${name || "empty"} change to ${value || "empty"} in Control Details`,
        date: new Date()
      }
      auditLog.push(audit)
    }
  }

  renderBasicDetails() {
    const { control, state, saveType, name } = this.state
    return (
      <div className="columns">
        <div className="column">
          <p><strong>Control ID:</strong>{control.id}</p>
          {/* <p className="multiExpLbl">{control.id}</p> */}
        </div>
        { name !==  "No active or eligible contracts" ?
          <div className="column">
            <p className="multiExpLbl">Status</p>
            <div className="select is-fullwidth is-small is-link">
              <select value={state} disabled={state === "disabled"}
                onChange={this.changeControlState} onBlur={(event) => this.onBlurInput(event)}>
                <option value="" disabled >Pick...</option>
                <option value="disabled">Disabled</option>
                <option value="enabled">Enabled</option>
              </select>
            </div>
          </div> : <div className="column"/>
        }
        <div className="column is-vcentered">
          <div className="columns is-vcentered">
            <div className="column is-fullwidth">&nbsp;</div>
            <div className="column">
              <div className="control">
                <button className="button is-link"
                  disabled={saveType === "final" || state === "disabled"}
                  onClick={this.toggleSaveDraftModal}>Save</button>
              </div>
            </div>
            <div className="column">
              <div className="control">
                <button className="button is-link"
                  disabled={saveType === "final" || state === "disabled"}
                  onClick={this.toggleSaveFinalModal}>Save & Trigger Notifications</button>
              </div>
            </div>
            <div className="column">
              <div className="control">
                <button className="button is-light"
                  onClick={this.undoChanges}>Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderActionDetails() {
    const { type, topic, target, recurringType, name,
      validationError, actionExpanded, checklistApplicable } = this.state

    return (
      <section className="accordions">
        <article className={actionExpanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.toggleActionAccordion}>
            <p>What ACTION do you want to take (in other words, what CONTROL do you want to set up)?</p>
            <i className={actionExpanded ? "fas fa-chevron-up" : "fas fa-chevron-down"} style={{ cursor: "pointer" }} />
            <br />
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">

              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Type?
                    <span className="icon has-text-danger">
                      <i className="fas fa-asterisk extra-small-icon"/>
                    </span>
                  </p>
                  <div className="select is-small is-fullwidth is-link">
                    <select value={type} name="type" onChange={this.changeField} onBlur={(event) => this.onBlurInput(event)}>
                      <option value="" disabled >Pick...</option>
                      <option value="affirm">Affirmation request</option>
                      <option value="task">Task</option>
                      <option value="alert">Tickler/Alert</option>
                    </select>
                  </div>
                  {(validationError && validationError.type) &&
                  <small className="has-text-danger is-size-7">{validationError.type}</small>}
                </div>
                <div className="column">
                  <p className="multiExpLbl">Target or Recipients?<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span></p>
                  <div className="select is-small is-fullwidth is-link">
                    <select value={target} onChange={this.changeTarget} onBlur={(event) => this.onBlurInput(event)}>
                      <option value="" disabled>Pick...</option>
                      <option value="all">All users of my firm</option>
                      <option value="others">Choose users</option>
                      <option value="self">Self</option>
                    </select>
                  </div>
                  {(validationError && validationError.target) &&
                  <small className="has-text-danger is-size-7">{validationError.target}</small>}
                </div>
                <div className="column">
                  <p className="multiExpLbl">How about notifications?</p>
                  {/* <div>
                  <input className="checkbox"
                    name="recurring"
                    type="checkbox"
                    checked={!!recurring}
                    onChange={this.changeField} />
                </div> */}
                  <div className="select is-small is-fullwidth is-link">
                    <select value={recurringType} name="recurringType"
                      disabled={type === "affirm" && quarterlyAffirmations.includes(topic)}
                      onChange={this.changeFrequencyField} onBlur={(event) => this.onBlurInput(event)}>
                      <option value="" disabled>Pick...</option>
                      <option value="once">One time only</option>
                      <option value="daily">Recurring - Daily</option>
                      <option value="weekly">Recurring - Weekly</option>
                      <option value="monthly">Recurring - Monthly</option>
                      <option value="quarterly">Recurring - Quarterly</option>
                      <option value="yearly">Recurring - Yearly</option>
                    </select>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Name (Unique & used to track/search)
                    <span className="icon has-text-danger">
                      <i className="fas fa-asterisk extra-small-icon"/>
                    </span>
                  </p>
                  <input className="input is-small is-link"
                    type="text"
                    placeholder="Less than 140 characters"
                    value={name}
                    name="name"
                    onChange={this.changeField}
                    onBlur={(event) => this.onBlurInput(event)}
                  />
                  {(validationError && validationError.name) &&
                  <small className="has-text-danger is-size-7">{validationError.name}</small>}
                </div>
              </div>
            </div>
          </div>
        </article>
      </section>

    )
  }

  renderNotificationsDetails() {
    const { recurringPattern1, recurringPattern2, startDate, type, topic,
      recurringType, endDate, validationError, recurring, patternExpanded } = this.state
    return (
      <section className="accordions">
        <article className={patternExpanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.togglePatternAccordion}>
            <p>Let us set up the pattern for repeat/recurring notifications</p>
            <i className={patternExpanded ? "fas fa-chevron-up" : "fas fa-chevron-down"} style={{ cursor: "pointer" }} />
            <br />
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl ">Pattern Input 1</p>
                  {(["weekly", "monthly", "quarterly"].includes(recurringType)) ?
                    <p className="multiExpTblVal">{"Every "}<input className="input is-small is-link"
                      disabled={recurringType === "quarterly"}
                      style={{ "maxWidth": "50px", marginLeft: "5px", marginRight: "5px" }}
                      name="recurringPattern1"
                      type="number"
                      min="1"
                      max="12"
                      value={recurringPattern1}
                      onChange={this.changeFrequencyField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />{recurringType === "weekly" ? " Week" : " Month"}
                    </p>
                    : "-"
                  }
                </div>
                <div className="column ">
                  <p className="multiExpLbl ">On Which Day ?</p>
                  {(["weekly", "monthly", "quarterly"].includes(recurringType)) ?
                    <div className="select is-small is-fullwidth is-link">
                      <select value={recurringPattern2} name="recurringPattern2" onChange={this.changeFrequencyField} onBlur={(event) => this.onBlurInput(event)}>
                        <option value="" disabled>Pick...</option>
                        {DAY_WEEK_MONTHS[recurringType === "weekly" ? "weekly" : "monthly"].map(e => (
                          <option key={e.value} value={e.value}>{e.label}</option>
                        ))}
                      </select>
                    </div>
                    : "-"
                  }
                  {/* <select value={recurring ? unit : ""}
                  name="unit"
                  disabled={!recurring}
                  onChange={this.changeField}>
                  {TIME_UNITS.map(e => (
                    <option key={e} value={e}>{e}</option>
                  ))}
                  <option key="NA" value="" disabled />
                </select> */}
                </div>
                <div className="column ">
                  <p className="multiExpLbl ">Notification Start Date
                    {recurring ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}</p>
                  {recurring ?
                    /* <input className="input is-small is-fullwidth is-link"
                      type="date"
                      name="startDate"
                      value={startDate ? moment(new Date(getCalendarDateAsString(startDate))).format("YYYY-MM-DD") : ""}
                      onChange={this.changeField} onBlur={(event) => this.onBlurInput(event)}
                    /> */
                    <TextLabelInput
                      name="startDate"
                      type="date"
                      value={(startDate === "" || !startDate) ? null : new Date(startDate)}
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />
                    : <span>-</span>
                  }
                  {(validationError && validationError.startDate) &&
                    <small className="has-text-danger is-size-7">{validationError.startDate}</small>
                  }
                </div>
                <div className="column ">
                  <p className="multiExpLbl ">Notification End Date
                    {recurring ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}</p>
                  {recurring ?
                    /* <input className="input is-small is-fullwidth is-link"
                      type="date"
                      name="endDate"
                      disabled={!recurring}
                      value={endDate ? moment(new Date(getCalendarDateAsString(endDate))).format("YYYY-MM-DD") : ""}
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    /> */
                    <TextLabelInput
                      name="endDate"
                      type="date"
                      value={(endDate === "" || !endDate) ? null : new Date(endDate)}
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />
                    : <span>-</span>
                  }
                  {(validationError && validationError.endDate) &&
                    <small className="has-text-danger is-size-7">{validationError.endDate}</small>
                  }
                </div>
              </div>
            </div>
          </div>
        </article>
      </section>
    )
  }

  renderRelatedDetails() {
    const { relatedActivity, relatedEntity, relatedContact, topic, subTopic,
      refRule, otherTopic, topics, subTopics, validationError, concerningExpanded } = this.state
    const topicOptions = topics.length ? topics : []
    const subTopicOptions = subTopics[topic] || []
    return (
      <section className="accordions">
        <article className={concerningExpanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.toggleConcerningAccordion}>
            <p>What is this concerning (update as appropriate)?</p>
            <i className={concerningExpanded ? "fas fa-chevron-up" : "fas fa-chevron-down"} style={{ cursor: "pointer" }}/>
            <br />
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">
              {/* <div className="column">
                <p className="multiExpLbl">Related Activity</p>
                <div className="select is-small is-fullwidth is-link">
                  <Multiselect
                    filter="contains"
                    groupBy="group"
                    data={this.state.transactions || []}
                    placeholder="search & select activity by name (multi select)"
                    textField="name"
                    valueField="id"
                    caseSensitive={false}
                    minLength={1}
                    value={relatedActivity}
                    onChange={(value) => this.changeRelatedActivity(value, "relatedActivity")}
                    disabled={!!relatedEntity.length || !!relatedContact.length}
                  />
                </div>
              </div>
              <div className="column">
                <p className="multiExpLbl ">Related Entity</p>
                <div className="select is-small is-fullwidth is-link">
                  <Multiselect
                    filter="contains"
                    groupBy="relType"
                    data={this.state.issuerList || []}
                    placeholder="search & select entities (multi select)"
                    textField="name"
                    valueField="id"
                    caseSensitive={false}
                    minLength={1}
                    value={relatedEntity}
                    onChange={(value) => this.changeRelatedActivity(value, "relatedEntity")}
                    disabled={!!relatedActivity.length || !!relatedContact.length}
                  />
                </div>
              </div>
              <div className="column">
                <p className="multiExpLbl ">Related Contact</p>
                <div className="select is-small is-fullwidth is-link">
                  <Multiselect
                    filter="contains"
                    data={this.state.users || []}
                    placeholder="search & select contacts (multi select)"
                    textField="name"
                    valueField="id"
                    caseSensitive={false}
                    minLength={1}
                    value={relatedContact}
                    onChange={(value) => this.changeRelatedActivity(value, "relatedContact")}
                    disabled={!!relatedEntity.length || !!relatedActivity.length}
                  />
                </div>
              </div> */}
              <div className="column">
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLbl">Topic<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span></p>
                    {/* <div className="select is-small is-fullwidth is-link">
                      <select value={topic} name="topic" onChange={this.changeField} onBlur={(event) => this.onBlurInput(event)}>
                        <option value="" disabled>Pick...</option>
                        {topicOptions.map((e, i) => (
                          <option key={e + i} value={e}>{e}</option>
                        ))}
                         <option disabled>COMPLIANCE</option>
                      <option disabled>-----------------</option>
                      <option value="Business Conduct Activities">Business Conduct Activities</option>
                      <option value="Client Complaints">Client Complaints</option>
                      <option value="Duties of Non-Solicitor Municipal Advisors">Duties of Non-Solicitor Municipal Advisors</option>
                      <option value="General Administration Activities">General Administration Activities</option>
                      <option value="Gifts and Gratuities">Gifts and Gratuities</option>
                      <option value="Political Contributions and Prohibitions on Municipal Advisory Business">Political Contributions and Prohibitions on Municipal Advisory Business</option>
                      <option value="Professional Qualifications">Professional Qualifications</option>
                      <option value="Supervisory and Compliance Obligations">Supervisory and Compliance Obligations</option>
                      <option value="Other">Other</option>
                      <option disabled>-----------------</option>
                      <option value="MISCELLANEOUS">MISCELLANEOUS</option>
                      </select>
                    </div> */}
                    <SelectLabelInput
                      placeholder="Pick..."
                      list={topicOptions || []}
                      name="topic"
                      value={topic || ""}
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />
                    {(validationError && validationError.topic) &&
                      <small className="has-text-danger is-size-7">{validationError.topic}</small>}
                  </div>
                  <div className="column">
                    <p className="multiExpLbl">Sub-Topic</p>
                    {/* <div className="select is-small is-fullwidth is-link">
                      <select value={subTopic} name="subTopic" onChange={this.changeField} onBlur={(event) => this.onBlurInput(event)}>
                        <option value="" disabled>Pick...</option>
                        {subTopicOptions.map((e, i) => (
                          <option key={e + i} value={e}>{e}</option>
                        ))}
                         <option value="" disabled>Pick...</option>
                      <option value="Dropdown values based on topic selected">Dropdown values based on topic selected</option>
                      </select>
                    </div> */}
                    <SelectLabelInput
                      placeholder="Pick..."
                      list={subTopicOptions || []}
                      name="subTopic"
                      value={subTopic || ""}
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />
                  </div>
                  <div className="column">
                    <p className="multiExpLbl">Other (please specify)</p>
                    {/* <input className="input is-small is-fullwidth is-link"
                      type="text"
                      placeholder="Mandatory of Topic/Sub-Topic not chosen"
                      value={otherTopic}
                      name="otherTopic"
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    /> */}
                    <TextLabelInput
                      name="otherTopic"
                      placeholder="Mandatory of Topic/Sub-Topic not chosen"
                      value={otherTopic || ""}
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLbl">Reference rule</p>
                    <input className="input is-small is-fullwidth is-link"
                      type="text"
                      placeholder="MSRB Rule A-12(j) and (k)"
                      value={refRule}
                      name="refRule"
                      onChange={this.changeField}
                      onBlur={(event) => this.onBlurInput(event)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </section>
    )
  }

  renderRecipientsDetails() {
    const { recipients, toList, ccList, target, users, validationError, recipentsExpanded, recurring } = this.state
    return (
      <section className="accordions">
        <article className={recipentsExpanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.toggleRecipentsAccordion}>
            <p>Recipients & what do you want the recipient to do?</p>
            <i className={recipentsExpanded ? "fas fa-chevron-up" : "fas fa-chevron-down"} style={{ cursor: "pointer" }} />
            <br />
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">
              <p className="multiExpLbl" style={{ marginBottom: "5px" }}>Who are the notifications recipients?
                {target === "others" ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}</p>
              {target === "others" ?
                <Multiselect
                  data={users}
                  textField="name"
                  placeholder="ACCOUNTABLE OWNER(S) TO COMPLETE THE ACTION"
                  caseSensitive={false}
                  minLength={1}
                  value={recipients}
                  filter="contains"
                  onChange={this.changeRecipients}
                />
                : undefined
              }
              {target !== "others" ? "" : validationError && validationError.recipients ?
                <small className="has-text-danger is-size-7">{validationError.recipients}</small> : ""
              }
              {/* <Multiselect
                data={users}
                textField="name"
                placeholder="TO LIST: multi-select"
                caseSensitive={false}
                minLength={1}
                value={toList}
                filter="contains"
                onChange={this.changeToList}
              />
              <br />
              <Multiselect
                data={users}
                textField="name"
                placeholder="CC LIST: multi-select"
                caseSensitive={false}
                minLength={1}
                value={ccList}
                filter="contains"
                onChange={this.changeCCList}
              />
              <br /> */}
            </div>
          </div>
        </article>
      </section>
    )
  }

  renderDocActionsTable() {
    const { docIds, control, uploadExpanded } = this.state
    const { entityId } = this.props
    const documents = docIds && docIds.length ?
      [...docIds, { _id: 0, name: "" }] : [{ _id: 0, name: "" }]
    return (
      <section className="accordions">
        <article className={uploadExpanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.toggleUploadAccordion}>
            <p>Upload File Associated With Control</p>
            <i className={uploadExpanded ? "fas fa-chevron-up" : "fas fa-chevron-down"} style={{ cursor: "pointer" }} />
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                    <thead>
                      <tr>
                        <th>
                          <p className="multiExpLbl ">Upload File</p>
                        </th>
                        <th>
                          <p className="multiExpLbl ">Filename</p>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {documents.map((e, i) => (
                        <tr key={e._id ? e._id + i : i}>
                          <td className="multiExpTblVal">
                            <DocUpload
                              docId={e._id}
                              tenantId={entityId}
                              contextType={ContextType.cac}
                              contextId={control.id}
                              // docMeta={{"t1": "v1", "t2": "v2"}}
                              // versionMeta={{"t3": new Date().getTime()}}
                              onUploadSuccess={this.uploadDoc} />
                          </td>
                          <td className="multiExpTblVal">
                            {e._id ?
                              <div className="field is-grouped">
                                <DocLink docId={e._id} name={e.name} />
                                <DocModal
                                  // docMetaToShow={["t1", "t2"]}
                                  // versionMetaToShow={["t3"]}
                                  onDeleteAll={this.deleteDoc}
                                  selectedDocId={e._id} />
                              </div>
                              : ""
                            }
                          </td>
                        </tr>
                      ))
                      }
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </article>
      </section>
    )
  }

  renderChecklistItems() {
    const { checklist, selectedChecklistItem } = this.state
    return checklist.map((e, i) => (
      <tr key={i}>
        <td className="multiExpTblVal">
          <div className="select is-small is-fullwidth is-link">
            <select value={e.action} onChange={this.changeChecklistAction.bind(this, i)} onBlur={(event) => this.onBlurInput(event)}>
              <option value="" disabled>NA</option>
              <option value="hide">Hide in list</option>
              <option value="show">Show in list</option>
            </select>
          </div>
        </td>
        <td className="soeExpLblVal">
          {selectedChecklistItem === i || !e.subject ?
            <input className="input is-small is-fullwidth is-link"
              type="text"
              placeholder="Less than 140 characters"
              value={e.subject}
              onKeyPress={this.handleKeyPressInChecklistItem.bind(this, i)}
              onChange={this.changeChecklistItemSubject.bind(this, i)}
              onBlur={(event) => this.onBlurInput(event)}
            /> :
            <span onClick={this.selectChecklistItem.bind(this, i)}>
              {e.subject}
            </span>
          }
        </td>
        <td className="multiExpTblVal">
          <div className="field is-grouped">
            <div className="control select is-small is-link">
              <select value={e.answer} onChange={this.changeChecklistAnswer.bind(this, i)} onBlur={(event) => this.onBlurInput(event)}>
                <option value="" disabled>NA</option>
                <option value="no">No</option>
                <option value="yes">Yes</option>
              </select>
            </div>
            <span className="control has-text-link"
              onClick={this.selectChecklistItem.bind(this, i)}>
              <i className="fas fa-pencil-alt is-small" />
            </span>
            <span className="control has-text-danger">
              <i className="fa fa-trash-o"
                onClick={this.removeChecklistItem.bind(this, i)} />
            </span>
          </div>
        </td>
      </tr>
    ))
  }

  renderChecklist() {
    const { clickExpanded, state, saveType, checklistApplicable } = this.state
    return (
      <section className="accordions">
        <article className={clickExpanded ? "accordion is-active" : "accordion"}>
          <div className="accordion-header toggle" onClick={this.toggleClickAccordion}>
            <p>
              Click/expand to send a checklist for recipient affirmation?
            </p>
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link is-small" style={{"right":"10px"}} disabled={saveType === "final" || state === "disabled"}
                  onClick={this.addChecklistItem}>Add a list item</button>
                <button className="button is-link is-small" disabled={saveType === "final" || state === "disabled"}
                  onClick={this.undoCheckListChanges}>Reset</button>
              </div>
            </div>
          </div>
          <div className="accordion-body acco-hack">
            <div className="accordion-content">
              <p className="multiExpLbl ">
                <label className="checkbox">
                  <input
                    type="checkbox"
                    checked={!!checklistApplicable}
                    name="checklistApplicable"
                    onChange={this.changeField}
                    onBlur={(event) => this.onBlurInput(event)}
                  /> Applicable
                </label>
              </p>
              <div>
                <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                  <thead>
                    <tr>
                      <th>
                        <p className="multiExpLbl" title="">Action</p>
                      </th>
                      <th>
                        <p className="multiExpLbl " title="">Subject</p>
                      </th>
                      <th>
                        <p className="multiExpLbl " title="">Yes/No</p>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.renderChecklistItems()}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </article>
      </section>
    )
  }

  renderNotes() {
    const { notes } = this.state
    return (
      <div className="columns">
        <div className="column is-full">
          <div className="field">
            <div className="multiExpLbl ">
              <p className="multiExpLbl " title="Notes and instructions by transaction participants">Notes/Instructions</p>
            </div>
            <div className="control">
              <textarea className="textarea is-link"
                name="notes"
                value={notes}
                onChange={this.changeField}
                placeholder=""
                onBlur={(event) => this.onBlurInput(event)}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { control, waiting, state, saveType, modalType, modalState, target } = this.state

    if (waiting || !control.id) {
      return <Loader />
    }

    if (modalType && modalState) {
      const modalText = modalType === "disable" ?
        "Disabling this control will terminate all tasks and control actions associated with this control.\n" +
        "Are you sure you want to disable this control? " :
        modalType === "draft" ?
          "This will save the control for further update/edit. Notifications to the audience and related workflow will not be triggered"
          : "You are finalizing the control and sending notifications to the audience. You will not be able to edit the control once notifications are sent out. You can always Copy the control and create new ones"
      return (
        <Modal closeModal={modalType === "disable" ?
          this.toggleDisableModalOnCancel : this.toggleModal}
        modalState={this.state.modalState}>
          <p>{modalText}</p>
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small"
                onClick={modalType === "final" ? this.SaveAndNotify :
                  this.validateSubmition}>Proceed</button>
            </div>
            <div className="control">
              <button className="button is-light is-small"
                onClick={modalType === "disable" ?
                  this.toggleDisableModalOnCancel : this.toggleModal}>Cancel</button>
            </div>
          </div>
        </Modal>
      )
    }

    return (
      <div id="main">
        {this.renderBasicDetails()}
        {this.renderActionDetails()}
        {this.renderNotificationsDetails()}
        {this.renderRelatedDetails()}
        {target === "others" ? this.renderRecipientsDetails() : null}
        {this.renderDocActionsTable()}
        {this.renderChecklist()}
        {this.renderNotes()}
        <div className="columns">
          <div className="column is-fullwidth">&nbsp;</div>
          <div className="column is-narrow">
            <div className="control">
              <button className="button is-link"
                disabled={saveType === "final" || state === "disabled"}
                onClick={this.toggleSaveDraftModal}>Save</button>
            </div>
          </div>
          <div className="column is-narrow">
            <div className="control">
              <button className="button is-link"
                disabled={saveType === "final" || state === "disabled"}
                onClick={this.toggleSaveFinalModal}>Save & Trigger Notifications</button>
            </div>
          </div>
          <div className="column is-narrow">
            <div className="control">
              <button className="button is-light"
                onClick={this.undoChanges}>Cancel</button>
            </div>
          </div>
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

// export default withRouter(ControlDetails)
const mapStateToProps = (state) => ({
  auth: state.auth && state.auth.userEmail,
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(ControlDetails)
export default connect(mapStateToProps, null)(WrappedComponent)
