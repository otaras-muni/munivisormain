import React, {Component} from "react"
import { connect } from "react-redux"
import ReactTable from "react-table"
import moment from "moment"
import {toast} from "react-toastify"
import {Link} from "react-router-dom"
import { getPicklistValues, createMessage, getCalendarDateAsString } from "GlobalUtils/helpers"
import Loader from "Global/Loader"
import { Modal } from "Global/BulmaModal"
import CONST from "../../../globalutilities/consts"
import {putCACDashboard, fetchControlsMonitor} from "../../StateManagement/actions/cac"
import { sendComplianceEmail } from "../../StateManagement/actions/Transaction"
import RatingSection from "../../GlobalComponents/RatingSection"
import Accordion from "../../GlobalComponents/Accordion"
import GiftList from "../NewCompliance/GiftsGratuities/UserView/GiftList"
import PoliticalDashReactTable from "../NewCompliance/PoliticalContribution/components/PoliticalDashReactTable"
import "react-table/react-table.css"
import {SelectLabelInput} from "../../GlobalComponents/TextViewBox"

class CACDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      controls: [],
      dueDate: [],
      status: [],
      modalRows: [],
      modalState: false,
      modalTitle: "",
      email: {
        category: "",
        message: "",
        subject: "",
      },
      searchString: "",
      searchList: [],
      topics: [],
      topic: "",
      dateRange: "",
    }
  }

  async componentWillMount() {
    const res = await fetchControlsMonitor()
    const [res2] = await getPicklistValues(["LKUPCMPLTOPICSUBTOPIC"])
    const dateRangeList = [{label: "Year to Date", included: true}, {label: "Quarter to Date", included: true}, {label: "Month to Date", included: true}]
    this.setState({
      controls: res || [],
      topics: res2 && res2[1],
      dateRangeList,
      loading: false,
    })
  }

  onChange = async (e, i, id) => {
    const { name, value } = e.target
    const {entityId, userId, userName} = this.props
    const changeLog = {
      userId,
      userName,
      log: "",
      date: new Date()
    }
    this.setState(prevState => {
      if (name === "status") {
        changeLog.log = `Control ${prevState.controls[i].id} Status changed to ${prevState.controls[i].status} from ${value || "No Value"}`
        const status = [...prevState.status]
        status[i] = value
        return { status }
      } else if (name === "dueDate") {
        changeLog.log = `Control ${prevState.controls[i].id} Due date changed to ${new Date(prevState.controls[i].dueDate).toLocaleDateString()} from ${value ? new Date(value).toLocaleDateString() : "No Value"}`
        const dueDate = [...prevState.dueDate]
        dueDate[i] = value
        return { dueDate }
      }
      return {loading: true}
    })
    if (id) {
      await putCACDashboard({entityId, id, name, value}, [changeLog])
      this.setState({loading: false})
    }
  }

  sendNotification = async (i) => {
    const { userEmail, user } = this.props
    const { controls, email } = this.state
    const control = controls[i]
    const { controlActionsPending } = control
    const topic = control._id && control._id.controlTopic
    const dueDate = control._id && control._id.dueDate && moment(control._id.dueDate).format("MM-DD-YYYY")
    const description = `Compliance Control Action Center - topic: ${topic}${dueDate ? ` & due date: ${dueDate}` : ""}`
    const count = controlActionsPending && controlActionsPending.filter(alr => alr.controlActionUserId === user.userId) || []
    const emailMessage = `You have ${count && count.length} control-actions remaining for complete`

    const sendEmail = controlActionsPending && controlActionsPending.map(item => {
      const newObject = {
        name: item.controlActionUserName,
        sendEmailTo: (item && item.controlActionUserEmails) || ""
      }
      return newObject
    })

    const emailParams = {
      type: "cac",
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace("/",""),
        ...email,
        category: "custom",
        subject: description,
        message: emailMessage,
        sendEmailTo: sendEmail || []
      }
    }
    if (controlActionsPending && controlActionsPending.length) {
      await sendComplianceEmail(emailParams)
    } else {
      toast(`No Action remaining to complete for user ${user.userFirstName} ${user.userLastName}`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
    }
  }

  showStatusModal = (completed=[], type) => {
    const modalTitle = type === "complete" ? "Actions Completed" : "Actions Due"
    this.setState({ modalState: true, modalRows: completed, modalTitle })
  }

  getActionCompletedBy(data=[], count, type) {
    if(data && Array.isArray(data) && data.length) {
      const rows = []
      if(type === "complete" || type === "pending") {
        data.forEach(e => {
          const date = e && e.controlActionDueDate ? moment(e.controlActionDueDate).format("MM-DD-YYYY") : ""
          rows.push(`${e && e.controlActionUserName} - ${e && e.controlTopic} - ${date}`)
        })
      }
      if(!rows.length) {
        rows.push("None")
      }
      return (
        <a onClick={() => this.showStatusModal(rows, type)}>{count}</a>
      )
    }
    return 0
  }

  toggleModal = () => {
    const {modalRows, modalState} =this.state
    this.setState({
      modalState: !modalState,
      modalRows
    })
  }

  renderStatusModal(modalRows) {
    return modalRows.map((e, i) => (
      <p key={i.toString()}>
        {e}
      </p>
    ))
  }

  changeSearchString = (e) => {
    const { controls } = this.state
    this.setState({
      [e.target.name]: e.target.value
    },() => {
      if (controls.length) {
        this.matchControl()
      }
    })
  }

  matchControl = async () => {
    const {controls, topic, searchString, dateRange} = this.state
    let searchList = []
    const data = controls || []
    if (searchString) {
      searchList = data && data.filter(obj =>
        ["controlName", "controlType", "controlTopic", "controlId", "dueDate"].some(key => {
          return (
            obj._id && obj._id[key] &&
            obj._id[key].toLowerCase().includes(searchString.toLowerCase())
          )
        })
      )
      if (searchString && topic) {
        searchList = await this.onTopicSort(searchList)

      }
      if (searchString && topic && dateRange) {
        searchList = await this.onDateSearch(searchList)
      }
    } else if (topic) {
      searchList = await this.onTopicSort(data)
      if (topic && dateRange) {
        searchList = await this.onDateSearch(searchList)
      }
    } else {
      searchList = await this.onDateSearch(data)
    }
    this.setState({
      searchList: searchList || []
    })
  }

  onTopicSort = (searchList) => {
    const {topic} = this.state
    searchList = searchList && searchList.length && searchList.filter(item =>
      item && item._id && item._id.controlTopic.toLowerCase().includes(topic.toLowerCase())
    ) || []
    return searchList
  }

  onDateSearch = (searchList) => {
    const {dateRange} = this.state
    let endDate = ""
    if (dateRange) {
      switch (dateRange) {
      case "Year to Date":
        endDate = moment(new Date()).startOf("year").format("MM-DD-YYYY")
        break
      case "Quarter to Date":
        endDate = moment(new Date()).startOf("quarter").format("MM-DD-YYYY")
        break
      case "Month to Date":
        endDate = moment(new Date()).startOf("month").format("MM-DD-YYYY")
        break
      default:
        endDate = ""
        break
      }
      searchList = searchList && searchList.length && searchList.filter(alr =>
        moment(endDate).format("YYYY-MM-DD") <= moment(alr && alr._id && alr._id.dueDate).format("YYYY-MM-DD") &&
        moment(new Date()).format("YYYY-MM-DD") >= moment(alr && alr._id && alr._id.dueDate).format("YYYY-MM-DD")
      )
      return searchList
    }
  }

  render() {
    const {controls, dueDate, status, modalRows, modalState, modalTitle, searchList, searchString, topics, topic, dateRange, dateRangeList} = this.state
    const { svControls } = this.props
    const data = (searchString || topic || dateRange) ? searchList : controls

    const columns = [
      {
        Header: <span title='Control Name'>Control Name</span>,
        id: "name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value._id
          return (
            <Link to={`/cac/controls-list/edit-control/${item.controlId}`}>
              {item.controlName}
            </Link>
          )
        }
      },
      {
        Header: <span title='Control Type'>Control Type</span>,
        id: "type",
        className: "multiExpTblVal",
        accessor: e => e._id.controlType,
        Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
          <small>{e && e.value || "--"}</small></div>
      },
      {
        Header: <span title='Compliance Topic'>Compliance Topic</span>,
        id: "topic",
        className: "multiExpTblVal",
        accessor: e => e._id.controlTopic,
        Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
          <small>{e && e.value || "--"}</small></div>
      },
      {
        Header: <span title='Notification Sent Date'>Notification Sent Date</span>,
        id: "notificationSentDate",
        className: "multiExpTblVal",
        accessor: e => e._id,
        Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
          <small>{e.value && e.value.controlNotificationSentDate && new Date(e.value.controlNotificationSentDate).toLocaleString()}</small></div>
      },
      {
        Header: <span title='No Action'>No Action</span>,
        id: "numActions",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const count = item.controlActionsPendingCount
          return (
            <div className="hpTablesTd wrap-cell-text tooltips">
              {this.getActionCompletedBy(item.controlActionsPending, count, "pending")}</div>
          )
        }
      },
      {
        Header: <span title='Action completed by'>Action completed by</span>,
        id: "actionCompletedBy",
        className: "multiExpTblVal",
        accessor: e => e,
        Cell: row => {
          const item = row.value
          const count = item.controlActionsCompletedCount
          return (
            <div className="hpTablesTd wrap-cell-text tooltips">
              {this.getActionCompletedBy(item.controlActionsCompleted, count, "complete")}</div>
          )
        }
      },
      // {
      //   Header: <span title='Due Date for Tracking'>Due Date for Tracking</span>,
      //   id: "dueDate",
      //   className: "multiExpTblVal",
      //   accessor: e => e,
      //   Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
      //     <input className="input is-small is-link"
      //       type="date"
      //       name="dueDate"
      //       value={dueDate[e.index] ? moment(new Date(dueDate[e.index])).format("YYYY-MM-DD") : "" || moment(e.value && e.value._id.dueDate).format("YYYY-MM-DD")}
      //       onChange={(event) => this.onChange(event, e.index, e.value && e.value._id.controlUniqueId)}
      //     />
      //   </div>
      // },
      // {
      //   Header: <span title='Control Status'>Control Status</span>,
      //   id: "status",
      //   className: "multiExpTblVal",
      //   accessor: e => e,
      //   Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
      //     <div className="control">
      //       <div className="select is-small is-fullwidth is-link">
      //         <select value={status[e.index] || e.value && e.value._id.status} name="status" onChange={(event) => this.onChange(event, e.index, e.value && e.value._id.controlUniqueId)} >
      //           <option disabled value="">Mark status...</option>
      //           <option value="open">Open</option>
      //           <option value="closed">Closed</option>
      //           <option value="completed">Completed</option>
      //           <option value="due">Past Due</option>
      //           <option value="cancelled">Cancelled</option>
      //         </select>
      //       </div>
      //     </div>
      //   </div>
      // },
      {
        Header: <span title='Due Date for Tracking'>Control Action Date</span>,
        id: "dueDate",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value._id
          return (
            <div>
              {item && item.dueDate && moment(item.dueDate).format("MM-DD-YYYY")}
            </div>
          )
        }
      },
      {
        Header: <span title='Action'>Action</span>,
        id: "action",
        className: "multiExpTblVal",
        minWidth:130,
        accessor: e => e,
        Cell: e => <div className="hpTablesTd">
          <button className="button is-link is-small is-fullwidth" onClick={() => this.sendNotification(e.index)}>
            Push Notification</button></div>
      }
    ]

    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div id="main">
        <section >
          <p className="title innerPgTitle has-text-centered">{this.props.firmName}</p>
          <p className="multiExpLbl has-text-centered">Compliance Supervisor's Monitor / Dashboard</p>
        </section>
        <hr />
        <Modal
          closeModal={this.toggleModal}
          modalState={modalState}
          showBackground
          title={modalTitle}
        >
          <div>
            {this.renderStatusModal(modalRows)}
          </div>
        </Modal>
        <div className="columns">
          <div className="column is-half">
            <p className="control has-icons-left">
              <input className="input is-small is-link"
                type="text"
                placeholder="search by control id, name, topic or notes"
                name="searchString"
                value={this.state.searchString}
                onChange={(e) => this.changeSearchString(e)}
              />
              <span className="icon is-small is-left has-background-dark has-text-white">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <div className="column">
            <SelectLabelInput
              placeholder="Topics"
              list={topics}
              name="topic"
              value={topic || ""}
              onChange={(e) => this.changeSearchString(e)}
            />
          </div>

          <div className="column">
            <SelectLabelInput
              placeholder="Date Range"
              list={dateRangeList}
              name="dateRange"
              value={dateRange || ""}
              onChange={(e) => this.changeSearchString(e)}
            />
          </div>
        </div>
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Action Center Topics"
              >
                {activeAccordions.includes(0) && (
                  <ReactTable
                    columns={columns || []}
                    data={data}
                    showPaginationBottom
                    defaultPageSize={10}
                    pageSizeOptions={[5, 10, 20, 50, 100]}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    showPageJump
                    minRows={2}
                  />
                )}
              </RatingSection>
            </div>
          )}
        />
        {svControls ? <GiftList svControls={svControls} /> : null }
        {svControls ? <PoliticalDashReactTable svControls={svControls} /> : null }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(mapStateToProps, null)(CACDashboard)
