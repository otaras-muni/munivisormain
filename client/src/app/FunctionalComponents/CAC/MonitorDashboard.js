import React, { Component } from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { toast } from "react-toastify"

import moment from "moment"
import { connect } from "react-redux"
import qs from "qs"

import { Modal } from "Global/BulmaModal"
import { createMessage, getPreApprovalListForCAC, getQuartersBetweenDates,
  checkDueInCurrentPeriod, getControlActionsDetails, getCalendarDateAsString } from "GlobalUtils/helpers"
import { saveControlsStatus } from "../../StateManagement/actions"
import PoliticalDashReactTable from "../NewCompliance/PoliticalContribution/components/PoliticalDashReactTable"
import GiftList from "../NewCompliance/GiftsGratuities/UserView/GiftList"
import RatingSection from "../../GlobalComponents/RatingSection"
import Accordion from "../../GlobalComponents/Accordion"

const quarterlyAffirmations = [
  "Political Contributions and Prohibitions on Municipal Advisory Business",
  "Gifts and Gratuities"
]

const TIME_UNITS = {
  "daily": "day",
  "weekly": "week",
  "monthly": "month",
  "quarterly": "quarter",
  "yearly": "year",
  "once":"day"
}

class MonitorDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      status: [], dueDate: [], preApporvals: [], statusFound: false, statusErr: "",
      actionDetails: [], modalRows: [], modalState: false, modalTitle: "",
      pageSizeOptions: [5, 10, 20, 25, 50], pageSize: 5, pageSize1: 5, users: {}
    }
    this.onPageSizeChange = this.onPageSizeChange.bind(this)
    this.onPageSizeChange1 = this.onPageSizeChange1.bind(this)
    this.actionsDetailsCallback = this.actionsDetailsCallback.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
  }

  async componentDidMount() {
    await this.setDataInState(this.props)
  }

  async componentWillReceiveProps(nextProps) {
    await this.setDataInState(nextProps)
  }

  componentWillUnmount() {
    const { token, entityId, controls, userId, userName } = this.props
    const { status, dueDate } = this.state
    const date = new Date()
    const data = []
    const changeLog = []
    controls.forEach((e, i) => {
      data[i] = {}
      data[i].refId = controls[i].refId
      data[i].status = status[i]
      data[i].dueDate = dueDate[i]
      if (e.status !== status[i]) {
        changeLog.push({
          userId,
          userName,
          log: `Control ${e.id} status changed to ${status[i]} from ${e.status || "No Value"}`,
          date
        })
      }
      if (e.dueDate !== dueDate[i]) {
        changeLog.push({
          userId,
          userName,
          log: `Control ${e.id} due date changed to ${new Date(dueDate[i]).toLocaleDateString()} from ${e.dueDate ? new Date(e.dueDate).toLocaleDateString() : "No Value"}`,
          date
        })
      }
    })
    if (controls.length && changeLog.length) {
      this.props.saveControlsStatus(token, entityId, data, changeLog)
    }
  }

  onPageSizeChange = (item) => {
    this.setState({
      pageSize: item
    })
  }

  onPageSizeChange1 = (item) => {
    this.setState({
      pageSize1: item
    })
  }

  async setDataInState(data) {
    const { controls, token, supervisor } = data
    console.log("controls : ", controls)
    const parentIds = controls.map(e => e.refId)
    console.log("parentIds : ", parentIds)
    if(parentIds.length) {
      getControlActionsDetails(
        { parentId: { $in: parentIds } },
        { parentId: 1, userId: 1, topic: 1, dueDate: 1, status: 1, notification: 1 },
        this.actionsDetailsCallback
      )
    }

    let preApporvals = []
    if (token && supervisor) {
      preApporvals = await getPreApprovalListForCAC(token)
    }
    const users = {}
    controls.forEach(e => {
      e.recipients.forEach(r => {users[r.id] = r.name})
    })
    const status = controls.map(e => e.status)
    const dueDate = controls.map(e => e.dueDate)
    this.setState({ status, dueDate, preApporvals, users })
  }

  getActionCompletedBy(e) {
    const { actionDetails } = this.state
    console.log("actionDetails : ", actionDetails)
    const { refId } = e
    const actions = actionDetails.filter(d => d.parentId === refId)
    const completed = actions.filter(d => (d.status === "complete" || d.status === "Complete"))
    console.log("completed : ", completed)
    if(completed.length) {
      return (
        <a onClick={this.showStatusModal.bind(this, completed, "complete")}>{completed.length}</a>
      )
    }
    // const  { actionCompletedBy } = e
    // if(Array.isArray(actionCompletedBy)) {
    //   const completed = actionCompletedBy.filter(d => (d.status === "complete" || d.status === "Complete"))
    //   console.log("completed : ", completed)
    //   return (
    //     <a onClick={this.showStatusModal.bind(this, completed, "complete")}>{completed.length}</a>
    //   )
    // }
    return 0
  }

  getNoActions(e) {
    const { actionDetails } = this.state
    console.log("actionDetails : ", actionDetails)
    const { refId } = e
    const actions = actionDetails.filter(d => d.parentId === refId)
    const pending = actions.filter(d => (
      (d.status !== "complete" && d.status !== "Complete") &&
      (checkDueInCurrentPeriod(d.dueDate, (d.notification &&
        TIME_UNITS[d.notification.recurringType])))
    ))
    console.log("pending : ", pending)
    if(pending.length) {
      return (
        <a onClick={this.showStatusModal.bind(this, pending, "pending")}>{pending.length}</a>
      )
    }
    return 0
  }

  getPreApprovalLink(userOrEntityId, quarter, year, type) {
    const query = qs.stringify({
      disclosureFor: userOrEntityId,
      year,
      quarter,
      status: "Pre-approval"
    })
    return `/compliance/cmp-sup-${type}/disclosure?${query}`
  }

  actionsDetailsCallback(err, res) {
    console.log("err, res : ", err, res)
    if(err) {
      this.setState({ actionDetails: [], statusFound: true,
        statusErr: "Error in getting details." })
    } else if(res && res.length) {
      this.setState({ actionDetails: res.filter(e => e.parentId),
        statusFound: true, statusErr: ""})
    }
  }

  showStatusModal(completed=[], type) {
    const modalTitle = type === "complete" ? "Actions Completed" : "Actions Due"
    let modalRows = []
    const { users } = this.state
    if(type === "complete" || type === "pending") {
      modalRows = completed.map(e => {
        const date = getCalendarDateAsString(e.dueDate)
        return `${users[e.userId]} - ${e.topic} - ${date ? new Date(date).toDateString() : ""}`
      })
    }
    if(!modalRows.length) {
      modalRows.push("None")
    }
    this.setState({ modalState: true, modalRows, modalTitle })
  }

  changeDueDate(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const dueDate = [...prevState.dueDate]
      dueDate[i] = value
      return { dueDate }
    })
  }

  changeStatus(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const status = [...prevState.status]
      status[i] = value
      return { status }
    })
  }

  sendNotification(i) {
    const { controls, userEmail } = this.props
    const { dueDate } = this.state
    const control = controls[i]
    const { topic, recipients } = control
    const description = `compliance notification - topic: ${topic}${dueDate[i] ? ` & due date: ${dueDate[i]}` : ""}`
    recipients.forEach(r => {
      if (r.actionStatus !== "complete") {
        createMessage({
          userId: r.id, from: userEmail, sentDate: new Date(), description,
          sourceType: "CAC", sourceId: control.id, docIds: control.docIds
        })
      }
    })
    toast("Notification sent", {
      autoClose: 1000,
      type: toast.TYPE.SUCCESS
    })
  }

  toggleModal() {
    this.setState(prev => {
      const modalState = !prev.modalState
      const modalRows = modalState ? prev.modalRows : []
      return { modalState, modalRows }
    })
  }

  renderTable() {
    const { controls } = this.props
    const { pageSize, pageSizeOptions, dueDate, status } = this.state
    return (
      <ReactTable
        columns={[
          {
            Header: props => <span title='Control Name'>Control Name</span>, // Custom header components!
            // Header: <span>Control Name</span>,
            id: "name",
            className: "multiExpTblVal",
            accessor: e => e.name,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: props => <span title='Control Type'>Control Type</span>, // Custom header components!
            // Header: "Control Type",
            id: "type",
            className: "multiExpTblVal",
            accessor: e => e.type,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: props => <span title='Compliance Topic'>Compliance Topic</span>, // Custom header components!
            // Header: "Compliance Topic",
            id: "topic",
            className: "multiExpTblVal",
            accessor: e => e.topic,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: props => <span title='Notification Sent Date'>Notification Sent Date</span>, // Custom header components!
            // Header: "Notification Sent Date",
            id: "notificationSentDate",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value.notificationSentDate &&
                new Date(e.value.notificationSentDate).toLocaleString()}
              </small></div>
          },
          {
            Header: props => <span title='No Action'>No Action</span>, // Custom header components!
            // Header: "No Action",
            id: "noAction",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              {this.getNoActions(e.value)}</div>
          },
          {
            Header: props => <span title='Action completed by'>Action completed by</span>, // Custom header components!
            // Header: "Action completed by",
            id: "actionCompletedBy",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              {this.getActionCompletedBy(e.value)}</div>
          },
          {
            Header: props => <span title='Due Date for Tracking'>Due Date for Tracking</span>, // Custom header components!
            // Header: "Due Date for Tracking",
            id: "dueDate",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <input className="input is-small is-link"
                type="date"
                name="dueDate"
                value={dueDate[e.index] ? moment(new Date(dueDate[e.index])).format("YYYY-MM-DD") : ""}
                onChange={this.changeDueDate.bind(this, e.index)} />
            </div>
          },
          {
            Header: props => <span title='Control Status'>Control Status</span>, // Custom header components!
            // Header: "Control Status",
            id: "status",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <div className="control">
                <div className="select is-small is-fullwidth is-link">
                  <select value={status[e.index] || "due"} name="status"
                    onChange={this.changeStatus.bind(this, e.index)}>
                    <option disabled value="">Mark status...</option>
                    <option value="open">Open</option>
                    <option value="closed">Closed</option>
                    <option value="completed">Completed</option>
                    <option value="due">Past Due</option>
                    <option value="cancelled">Cancelled</option>
                  </select>
                </div>
              </div>
            </div>
          },
          {
            Header: props => <span title='Action'>Action</span>, // Custom header components!
            // Header: "Action",
            id: "action",
            className: "multiExpTblVal",
            minWidth:130,
            accessor: e => e,
            Cell: e => <div className="hpTablesTd">
              <button className="button is-link is-small is-fullwidth"
                onClick={this.sendNotification.bind(this, e.index)}>
                Push Notification</button></div>
          }
        ]}
        data={controls.filter(c => c.saveType === "final")}
        defaultPageSize={pageSize}
        onPageSizeChange={this.onPageSizeChange}
        pageSizeOptions={pageSizeOptions}
        className=" overflow-auto -striped -highlight is-bordered "
        pageSize={pageSize}
        loading={this.state.waiting}
        minRows={1}
      />
    )
  }

  renderControlsRows() {
    const { controls } = this.props
    const { dueDate, status } = this.state
    return controls.map((e, i) => (
      <tr key={i}>
        <td className="hpTablesTd">
          <small>{e.name}</small>
        </td>
        <td className="hpTablesTd">
          <small>{e.type}</small>
        </td>
        <td className="hpTablesTd">
          <small>{e.topic}</small>
        </td>
        <td className="hpTablesTd">
          <small>{e.notificationSentDate ?
            new Date(e.notificationSentDate).toLocaleString() : ""}</small>
        </td>
        <td className="hpTablesTd">
          <small>{this.getNoActions(e) - e.actionCompletedBy}</small>
        </td>
        <td className="hpTablesTd">
          <small>{e.actionCompletedBy}</small>
        </td>
        <td className="hpTablesTd">
          <input className="input is-small is-fullwidth"
            type="date"
            name="dueDate"
            value={dueDate[i] ? moment(new Date(dueDate[i])).format("YYYY-MM-DD") : ""}
            onChange={this.changeDueDate.bind(this, i)} />
        </td>
        <td className="hpTablesTd">
          <div className="control">
            <div className="select is-small is-fullwidth">
              <select value={status[i] || "due"} name="status"
                onChange={this.changeStatus.bind(this, i)}>
                <option disabled value="">Mark status...</option>
                <option value="open">Open</option>
                <option value="closed">Closed</option>
                <option value="completed">Completed</option>
                <option value="due">Past Due</option>
                <option value="cancelled">Cancelled</option>
              </select>
            </div>
          </div>
        </td>
        <td className="hpTablesTd">
          <button className="button is-link is-small is-fullwidth"
            onClick={this.sendNotification.bind(this, i)}>Push Notification</button>
        </td>
      </tr>
    ))
  }

  renderPreApporvalsRows(preApporvals) {
    return preApporvals.map((e, i) => {
      const { _id, userOrEntityId, userFirstName, userLastName, contributeId: { quarter, year } } = e
      return (
        <tr key={_id + i}>
          <td className="hpTablesTd">
            <small>Political Contributions and Prohibitions</small>
          </td>
          <td className="hpTablesTd">
            <small>{`${userFirstName} ${userLastName}`}</small>
          </td>
          <td className="hpTablesTd">
            <a href={this.getPreApprovalLink(userOrEntityId, quarter, year)} target="new">
              <small>Details</small>
            </a>
          </td>
        </tr>
      )
    })
  }

  renderPreApporvals() {
    const { pageSize1, pageSizeOptions, preApporvals } = this.state
    return (
      <ReactTable
        columns={[
          {
            Header: "Compliance Topic",
            id: "topic",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>Political Contributions and Prohibitions</small></div>
          },
          {
            Header: "Submitted by",
            id: "actionCompletedBy",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{`${e.value.userFirstName} ${e.value.userLastName}`}</small></div>
          },
          {
            Header: "Action",
            id: "action",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              { e.value.contributeId ?
                <a href={this.getPreApprovalLink(e.value.userOrEntityId,
                  e.value.contributeId.quarter, e.value.contributeId.year, "political")} target="new">
                  <small>Details</small></a> :
                <a href={this.getPreApprovalLink(e.value.userOrEntityId,
                  e.value.quarter, e.value.year, "gifts")} target="new">
                  <small>Details</small></a>
              }
            </div>
          }
        ]}
        data={preApporvals}
        defaultPageSize={pageSize1}
        onPageSizeChange1={this.onPageSizeChange1}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        pageSize={pageSize1}
        loading={this.state.waiting}
        minRows={1}
      />
    )
  }

  renderPreApporvals1() {
    const { preApporvals } = this.state
    return (
      <div>
        <section className="container has-text-centered">
          <p className="multiExpLbl ">Pending Pre-approvals</p>
        </section>

        <hr />

        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <small>Compliance Topic</small>
              </th>
              <th>
                <small>Submitted by</small>
              </th>
              <th>
                <small>Action</small>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.renderPreApporvalsRows(preApporvals)}
          </tbody>
        </table>
      </div>
    )
  }

  renderTable1() {
    return (
      <div className="box overflow-auto">
        <table className="table is-striped is-fullwidth is-bordered">
          <thead className="is-link is-uppercase">
            <tr>
              <th>
                <small>Control Name</small>
              </th>
              <th>
                <small>Control Type</small>
              </th>
              <th>
                <small>Compliance Topic</small>
              </th>
              <th>
                <small>Notification Sent Date</small>
              </th>
              <th>
                <small>No Action</small>
              </th>
              <th>
                <small>Action completed by</small>
              </th>
              <th>
                <small>Due Date for Tracking</small>
              </th>
              <th>
                <small>Control Status</small>
              </th>
              <th>
                <small>Action</small>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.renderControlsRows()}
          </tbody>
        </table>
        <hr />
      </div>
    )
  }

  renderStatusModal(modalRows) {
    return modalRows.map((e, i) => (
      <p key={i}>
        {e}
      </p>
    ))
  }

  render() {
    const { svControls } = this.props
    const { modalState, modalRows, modalTitle } = this.state
    return (
      <div id="main">
        <div>
          <section >
            <p className="title innerPgTitle has-text-centered">{this.props.firmName}</p>
            <p className="multiExpLbl has-text-centered">Compliance Supervisor's Monitor / Dashboard</p>
          </section>
          <hr />
          <Modal
            closeModal={this.toggleModal}
            modalState={modalState}
            showBackground
            title={modalTitle}
          >
            <div>
              {this.renderStatusModal(modalRows)}
            </div>
          </Modal>
          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  title="Action Center Topics"
                >
                  {activeAccordions.includes(0) && (
                    <div>
                      {this.renderTable()}
                    </div>
                  )}
                </RatingSection>
              </div>
            )}
          />

          {/* {this.renderTable1()} */}
        </div>
        {/* <div className="box cac-table"> */}
        {/* {this.renderPreApporvals()} */}
        {/* {this.renderPreApporvals1()} */}
        {/* </div> */}
        {svControls ? <GiftList svControls={svControls} /> : null }
        {svControls ? <PoliticalDashReactTable svControls={svControls} /> : null }
      </div>
    )
  }
}

export default connect(null, { saveControlsStatus })(MonitorDashboard)
