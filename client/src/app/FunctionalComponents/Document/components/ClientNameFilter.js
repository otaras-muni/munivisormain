import React from "react"
import styled from "styled-components"
import axios from "axios"
import { connect } from "react-redux"
import _ from "lodash"
import {DropdownList } from "react-widgets"
import { muniApiBaseURL } from "../../../../globalutilities/consts"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, key = "issuerId.raw") => {
  let query = {}

  if (value) {
    query = {
      "bool" : {
        "should": [
          { "term": {[key]: value }}
        ]
      }
    }
  }

  return query
}

class DocumentTypeFilter extends React.PureComponent {

  constructor(props) {
    super(props)

    this.state = {
      options: [],
      search: "",
      searchList: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.defaultValue !== this.props.defaultValue) {
      const query = prepareQuery(nextProps.defaultValue, nextProps.queryKey)

      this.props.setQuery({
        query,
        value: nextProps.defaultValue,
      })
    }
  }

  getOptions = async () => {
    const { auth } = this.props
    const res = await axios({
      method: "GET",
      url: `${muniApiBaseURL}common/getalltenantentitydetails`,
      headers: { Authorization: auth.token },
    })

    const data = await res.data.entities
    const entities = data.map(item => ({ id: item.entityId, name: item.name, type: item.relType}))

    this.setState({
      options: _.uniqBy(entities, "name"),
    })
  }

  handleQueryChange = (e) => {
    // const { target: { value }} = e
    const value = e && e.id

    this.props.handleFilterChange("clientFilter", value)
  }

  onSearch = (search) => {
    let searchList = []
    this.setState({
      search
    }, () => {
      if(this.state.search){
        searchList = this.state.options && this.state.options.filter(obj =>
          ["name"].some(key => {
            return (
              obj[key] &&
              obj[key].toLowerCase().includes(search.toLowerCase())
            )
          })
        )
      }
      this.setState({
        searchList
      })
    })
  }

  render() {
    const { defaultValue } = this.props
    const {search, options, searchList} = this.state
    const list = search ? searchList : options || []
    return (
      <FilterContainer>
        {/* <div className="select is-small is-link is-fullwidth">
          <select onChange={this.handleQueryChange}>
            <option value="">Client</option>
            {
              this.state.options.map(state => (
                <option key={state.id} value={state.id}>{state.name}</option>
              ))
            }
          </select>
        </div> */}
        <DropdownList
          filter="contains"
          groupBy={(row) =>row.type}
          data={list || []}
          value={defaultValue}
          onChange={(value) => this.handleQueryChange(value)}
          style={{ width: "100%" }}
          textField="name"
          valueField="id"
          placeholder="search entities"
          onSearch={(value) => this.onSearch(value)}
        />
      </FilterContainer>
    )
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth
})

export default connect(mapStateToProps)(DocumentTypeFilter)
