import React from "react"
import moment from "moment"
import {DropdownList} from "react-widgets";
import styled from "styled-components";

const KEY = "docDetails.uploadDate"

const FilterContainer = styled.div`
  display: flex;
`

class DateFilter extends React.Component {
  constructor() {
    super()
    this.state = {
      options: ["Today", "Week", "Month", "Quarter", "Year"]
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.defaultValue !== this.props.defaultValue) {
      this.handleSelectChange(nextProps.defaultValue)
    }
  }

  handleSelectChange = (value) => {
    let query = {}
    switch (value) {
    case "today":
      query = {
        "range": {
          [KEY]: {
            "lte": moment().format("YYYY-MM-DD"),
            "gte": moment().format("YYYY-MM-DD")
          }
        }
      }
      break
    case "week":
      query = {
        "range": {
          [KEY]: {
            lte: moment().subtract(1, "week").endOf("week").format("YYYY-MM-DD"),
            gte: moment().subtract(1, "week").startOf("week").format("YYYY-MM-DD")
          }
        }
      }
      break
    case "month":
      query = {
        "range": {
          [KEY]: {
            "lte": moment().format("YYYY-MM-") + moment().daysInMonth(),
            "gte": moment().format("YYYY-MM-01")
          }
        }
      }
      break
    case "quarter":
      query = {
        "range": {
          [KEY]: {
            lte: moment().format("YYYY-MM-DD"),
            gte: moment().subtract(3, "months").date(1).format("YYYY-MM-DD")
          }
        }
      }
      break
    case "year":
      query = {
        "range": {
          [KEY]: {
            lte: moment().format("YYYY-MM-DD"),
            gte: moment().subtract(12, "months").date(1).format("YYYY-MM-DD")
          }
        }
      }
      break
    default:
      query = {}
      break
    }
    this.props.setQuery({
      query,
      value
    })
  }

  handleChange = (value) => {
    // const { target: { value }} = e

    this.props.handleFilterChange("dateFilter", value)
  }

  render() {
    const {defaultValue} = this.props
    const {options} = this.state
    return (
      <FilterContainer>
        <DropdownList
          filter
          placeholder="Uploaded At"
          style={{ width: "100%" }}
          value={defaultValue}
          data={options || []}
          onChange={this.handleChange}
        />
      </FilterContainer>
    )
  }
}

export default DateFilter
