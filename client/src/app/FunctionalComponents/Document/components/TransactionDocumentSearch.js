/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */
import React from "react"
import styled from "styled-components"
import {
  ReactiveBase,
  DataSearch,
  ReactiveComponent,
  ReactiveList
} from "@appbaseio/reactivesearch"
import {
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_INDEX,
  DOCUMENT_TYPE,
  ELASTIC_SEARCH_CREDENTIALS
} from "../../../../constants"
import { getFieldsAndWeights } from "../searchConfig"
import DocumentTypeFilter from "./DocumentTypeFilter"
import DocumentCategoryFilter from "./DocumentCategoryFilter"
import DocumentSubCategoryFilter from "./DocuemntSubCategoryFilter"
import ClientFilter from "./ClientNameFilter"
import TranTypeFilter from "./TranTypeFilter"
import DateFilter from "./DateFilter"
import TransactionResultSet from "./TransactionResultSet"
import PaginationDropDown from "../../Dashboard/components/PaginationDropDown"
import ReactiveLoader from "../../../GlobalComponents/ReactiveLoader"
import "./result.css"
import { getPicklistByPicklistName } from "../../../../globalutilities/helpers";

const Result = styled(ReactiveList)`
  border: 1px solid #dfdfdf;
  border-radius: 5px;
  padding: 1.25rem;
  background: #fff;
  margin-top: 10px;
  overflow-x: auto;
`

export const TRANSACTION_CONTEXT = "Transactions"

class DocumentSearch extends React.Component {
  constructor() {
    super()
    const { searchFields, fieldWeights } = getFieldsAndWeights()
    this.state = {
      searchFields: [...searchFields],
      fieldWeights: [...fieldWeights],
      pageSize: 10,
      actionExpanded: true,
      filters: {},
      subCategoryItem: [],
      subCategoryList: []
    }
    this.toggleActionAccordion = this.toggleActionAccordion.bind(this)
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName(["LKUPDOCCATEGORIES"])
    const subCategory = (picResult.length && picResult[2].LKUPDOCCATEGORIES) || []
    if(_.isObject(subCategory)){
      this.setState({
        subCategoryItem: subCategory,
      })
    }
  }


  handlePageChange = pageSize => {
    this.setState({
      pageSize
    })
  }

  handleFilterChange = (key, value) => {
    const {subCategoryItem} = this.state
    let subCategoryList = []
    if(key === "docCategoryFilter"){
      subCategoryList = subCategoryItem[value]
    }
    this.setState((oldState) => ({
      filters: {
        ...oldState.filters,
        [key]: value
      },
      subCategoryList
    }))
  }

  toggleActionAccordion() {
    this.setState(prevState => ({ actionExpanded: !prevState.actionExpanded }))
  }

  resetFilters = () => {
    this.setState({
      filters: {}
    })
  }

  render() {
    const { searchFields, fieldWeights, pageSize, actionExpanded, filters, subCategoryList } = this.state
    const { defaultQueryKey, defaultQueryValues, context } = this.props
    return (
      <div className="top-bottom-margin">
        <ReactiveBase
          app={ELASTIC_SEARCH_INDEX}
          url={ELASTIC_SEARCH_URL}
          credentials={ELASTIC_SEARCH_CREDENTIALS}
          type={DOCUMENT_TYPE}
        >
          <section className="accordions">
            <article
              className={actionExpanded ? " accordion is-active" : "accordion"}
            >
              <div>
                <div
                  className="accordion-header toggle"
                  onClick={this.toggleActionAccordion}
                >
                  <p>Search Documents</p>
                  {actionExpanded ? (
                    <i
                      className="fas fa-chevron-up"
                      style={{ cursor: "pointer" }}
                    />
                  ) : (
                    <i
                      className="fas fa-chevron-down"
                      style={{ cursor: "pointer" }}
                    />
                  )}
                  <br />
                </div>

                <div className="accordion-body acco-hack">
                  <div className="accordion-content">
                    <div className="columns" style={{ flexGrow: "initial" }}>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "10px" }}
                      >
                        <ReactiveComponent componentId="clientName">
                          <ClientFilter
                            queryKey="issuerId.raw"
                            handleFilterChange={this.handleFilterChange}
                            defaultValue={filters.clientFilter || ""}
                          />
                        </ReactiveComponent>
                      </div>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "5px" }}
                      >
                        <ReactiveComponent componentId="tranType">
                          <TranTypeFilter
                            handleFilterChange={this.handleFilterChange}
                            defaultValue={filters.tranTypeFilter || ""}
                          />
                        </ReactiveComponent>
                      </div>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "5px" }}
                      >
                        <ReactiveComponent componentId="docType">
                          <DocumentTypeFilter
                            handleFilterChange={this.handleFilterChange}
                            defaultValue={filters.docTypeFilter || ""}
                          />
                        </ReactiveComponent>
                      </div>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "5px" }}
                      >
                        <ReactiveComponent componentId="docCategory">
                          <DocumentCategoryFilter
                            handleFilterChange={this.handleFilterChange}
                            defaultValue={filters.docCategoryFilter || ""}
                          />
                        </ReactiveComponent>
                      </div>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "5px" }}
                      >
                        <ReactiveComponent componentId="docSubCategory">
                          <DocumentSubCategoryFilter
                            handleFilterChange={this.handleFilterChange}
                            defaultValue={filters.docSubCategoryFilter || ""}
                            subCategoryList={subCategoryList || []}
                          />
                        </ReactiveComponent>
                      </div>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "5px" }}
                      >
                        <ReactiveComponent componentId="dateFilter">
                          <DateFilter
                            handleFilterChange={this.handleFilterChange}
                            defaultValue={filters.dateFilter || ""}
                          />
                        </ReactiveComponent>
                      </div>
                      <div
                        className="column is-small"
                        style={{ marginLeft: "3" }}
                      >
                        <button
                          className="button is-dark is-small"
                          type="button"
                          onClick={this.resetFilters}
                        >
                          Reset
                        </button>
                      </div>
                    </div>
                    <div className="columns">
                      <div className="column">
                        <div className="searchContainer">
                          <DataSearch
                            componentId="docSearch"
                            dataField={searchFields}
                            fieldWeights={fieldWeights}
                            autosuggest={false}
                            iconPosition="left"
                            queryFormat="and"
                            placeholder="Enter search phrases"
                            showIcon={false}
                            highlight
                            innerClass={{
                              input: "input is-small searchInput is-link"
                            }}
                            style={{ width: "100%" }}
                          />
                          <span className="icon is-left searchIcon">
                            <i className="fas fa-search" />
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </section>
          <div
            className="box search-result-container top-bottom-margin"
            style={{ position: "relative" }}
          >
            <div className="dashPaginationRight">
              <PaginationDropDown
                defaultValue={pageSize}
                handlePageChange={this.handlePageChange}
              />
            </div>
            <Result
              key={context}
              componentId="DocSearchResult"
              dataField="taskName"
              pagination
              paginationAt="both"
              size={pageSize}
              loader={<ReactiveLoader />}
              excludeFields={["attachment.content", "attachment_data"]}
              defaultQuery={() => {
                if (defaultQueryValues.length) {
                  return {
                    terms: { [defaultQueryKey]: defaultQueryValues }
                  }
                }

                return {
                  match_none: {}
                }
              }}
              react={{
                and: [
                  "dateFilter",
                  "clientName",
                  "tranType",
                  "docSearch",
                  "docType",
                  "docCategory",
                  "docSubCategory"
                ]
              }}
              showResultStats
              onResultStats={(total, time) => (
                <h3 className="dashPagination">
                  Found {total} records in {time} ms
                </h3>
              )}
              renderAllData={items =>
                Boolean(items.length) && <TransactionResultSet items={items} />
              }
            />
          </div>
        </ReactiveBase>
      </div>
    )
  }
}

export default DocumentSearch
