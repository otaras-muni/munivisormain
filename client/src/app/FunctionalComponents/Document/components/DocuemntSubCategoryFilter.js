import React from "react"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX, DOCUMENT_TYPE } from "../../../../constants"
import { getHeaders } from "../../../../globalutilities"
import { getPicklistByPicklistName } from "../../../../globalutilities/helpers";
import { DropdownList } from "react-widgets";

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value) => {
  let query = {}

  if (value) {
    query = {
      "bool" : {
        "should": [
          { "term": {"docDetails.docSubCategory.raw": value }}
        ]
      }
    }
  }

  return query
}

class DocumentTypeFilter extends React.PureComponent {

  constructor() {
    super()

    this.state = {
      options: [],
      docCategory: ""
    }
  }

  // componentDidMount() {
  //   this.getOptions()
  // }

  componentWillReceiveProps(nextProps) {
    const { options } = this.state
    if (nextProps.defaultValue !== this.props.defaultValue) {
      const query = prepareQuery(nextProps.defaultValue, nextProps.queryKey)
      this.props.setQuery({
        query,
        value: nextProps.defaultValue,
      })
    }
    if(nextProps.subCategoryList.length !== options.length){
      this.setState({options: nextProps.subCategoryList})
    }
  }

  // getOptions = async () => {
  //   const headers = getHeaders()
  //   axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/${DOCUMENT_TYPE}/_search`, {
  //     "size": 0,
  //     "aggs": {
  //       "doc_types": {
  //         "terms": {
  //           "field": "docDetails.docSubCategory.raw"
  //         }
  //       },
  //     }
  //   }, {headers}).then(res => {
  //     const docTypeOptions = res.data.aggregations.doc_types.buckets
  //
  //     const result = _.uniqBy([...docTypeOptions], "key")
  //     const options = result.map(state => state.key)
  //
  //     this.setState({
  //       options,
  //     })
  //   })
  // }

  handleQueryChange = (value) => {
    // const { target: { value }} = e

    this.props.handleFilterChange("docSubCategoryFilter", value)
  }

  render() {
    const { defaultValue } = this.props
    const { options } = this.state
    return (
      <FilterContainer>
        <DropdownList
          filter
          placeholder="Document Sub Category"
          style={{ width: "100%" }}
          value={defaultValue}
          data={options}
          onChange={this.handleQueryChange}
        />
        {/* <div className="select is-small is-link is-fullwidth">
          <select value={defaultValue} onChange={this.handleQueryChange}>
            <option value="">Document Sub Category</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div> */}
      </FilterContainer>
    )
  }
}

export default DocumentTypeFilter
