import React from "react"
import { connect } from "react-redux"
import axios from "axios"
import { muniApiBaseURL } from "../../../../globalutilities/consts"

const mapStateToProps = (state) => ({
  auth: state.auth,
  allurls: state.urls.allurls
})
class FileDownloadButton extends React.Component {
  state = {
    isDownloading: false,
  }

  handleDowload = async () => {
    const { auth, docId } = this.props
    this.setState({
      isDownloading: true,
    })
    try {
      const res = await axios({
        method: "POST",
        url: `${muniApiBaseURL}s3/get-s3-download-urls`,
        headers: { Authorization: auth.token },
        data: {
          docIds: [docId]
        }
      })

      const data = await res.data
      this.setState({
        isDownloading: false,
      })
      if (data[0]) {
        const a = document.createElement("a")
        a.href = data[0] // eslint-disable-line
        a.setAttribute("target", "_blank")
        a.click()
      }
    } catch (err) {
      console.error(err)
      this.setState({
        isDownloading: false,
      })
    }
  }

  render() {
    const { isDownloading } = this.state
    return (
      <button
        type="button"
        onClick={this.handleDowload}
        disabled={isDownloading}
        className="button is-link is-fullwidth is-small "
      >
        {
          isDownloading ? "Downloading..." : "Download"
        }
      </button>
    )
  }
}

const DownloadButton = connect(mapStateToProps)(FileDownloadButton)
export default DownloadButton
