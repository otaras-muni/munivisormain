import React from "react"
import {Modal} from "../../../../../FunctionalComponents/TaskManagement/components/Modal"
import {MultiSelect, SelectLabelInput} from "../../../../../GlobalComponents/TextViewBox"

export const ShareDocumentsModal = (props) => {
  const share = props.sharingType
  const shareType = props.sharingLevel === "Document" ? "Document" : props.sharingLevel === "Folder" ? "Folder" : ""
  const onUserChange = (name, items) => {
    props.onSharingDocChange({
      [name]: items.map(item => {
        if(props.sharingType === "share" && props.sharingLevel === "Document"){
          return {
            id: item.id || "",
            name: item.name || "",
            files: [props.documentId] || [],
          }
        } else {
          return {
            id: item.id || "",
            name: item.name || "",
          }
        }
      })
    }, name)
  }

  const disable = props.sharingLevel !== "Document" ? props.sharingType === "copy" || props.sharingType === "move" ?
    !props.selectFrom || !props.selectTo : props.sharingType === "share" ? !props.selectFrom || props && props.firmUser && props.firmUser.length === 0 : null
    : props.sharingLevel === "Document" ? props.sharingType === "copy" || props.sharingType === "move" ? !props.selectTo
      : props.sharingType === "share" ? props && props.firmUser && props.firmUser.length === 0 : null : null

  const folder = props.folderPropList.filter(f => f.myDocuments)
  const dupFolder = folder && folder.length && folder.map(f => f.folderName)

  return(
    <Modal
      closeModal={props.onCreateFolderModal}
      saveModal={props.onShareDocsSave}
      modalState={props.shareDocumentModal}
      title={`Share ${props.sharingLevel}`}
      buttonName={share === "copy" ? `Copy ${shareType}` : share === "move" ? `Move ${shareType}` : share === "share" ? `Share ${shareType}` : ""}
      styleBody={{overflow: "unset"}}
      modalWidth={{overflow: "unset"}}
      disabled={ !props.sharingType || disable }
    >
      <div>
        <div className="columns">
          <div className="column">
            <label className="radio-button">
              Copy {props.sharingLevel}
              <input type="radio" name="sharingType" value="copy" onChange={() => props.onSharingDocChange("copy")} checked={props.sharingType === "copy"}/>
              <span className="checkmark" />
            </label>
          </div>
          {props.isDisable ? null :
            <div className="column">
              <label className="radio-button">
                Move {props.sharingLevel}
                <input type="radio" name="sharingType" value="move" onChange={() => props.onSharingDocChange("move")} checked={props.sharingType === "move"}/>
                <span className="checkmark" />
              </label>
            </div>
          }
          {props.contextType === "SHAREDOCS" ? null :
            <div className="column">
              <label className="radio-button">
                Share {props.sharingLevel}
                <input type="radio" name="sharingType" value="share" onChange={() => props.onSharingDocChange("share")} checked={props.sharingType === "share"}/>
                <span className="checkmark" />
              </label>
            </div>
          }
        </div>

        { props.sharingType !== "" && props.sharingLevel === "Document" ?
          <div >
            <span>
              <strong>FileName: </strong>
              <span>
                {props.fileName || ""}
              </span>
            </span>
          </div> : null
        }

        { props.sharingType === "share" && props.sharingLevel === "Document" && props.sendToUser && props.sendToUser.length ?
          <div >
            <strong>This Document Share With: </strong>
            <span>
              { props.sendToUser && props.sendToUser.length && props.sendToUser.map((s, i) => (
                <span key={i}>
                  {s.name}{props.sendToUser.length - 1 === i ? "" : ","}&nbsp;
                </span>
              ))}
            </span>
          </div> : null
        }

        { props.sharingType !== "" ?
          <div className="columns">
            { props.sharingLevel === "Document" ? null :
              <div className="column">
                <SelectLabelInput
                  label="From"
                  name="selectFrom"
                  list={props.dropDown.folderList || []}
                  value={props.selectFrom || ""}
                  disableValue={props.contextType === "SHAREDOCS" ? dupFolder : null}
                  onChange={e => props.onSharingDocChange("selectFrom", e)}
                />
              </div>
            }
            { props.sharingLevel === "Folder" && props.sharingType === "share" || props.sharingLevel === "Document" && props.sharingType === "share" ?  null :
              <div className="column">
                <SelectLabelInput
                  label="To"
                  name="selectTo"
                  list={props.contextType === "SHAREDOCS" ? props.dupList : props.folderChange || []}
                  value={props.selectTo || ""}
                  onChange={e => props.onSharingDocChange("selectTo", e)}
                />
              </div>
            }
          </div> : null
        }

        {props.sharingType === "share" ?
          <MultiSelect
            filter
            label="To"
            data={props.dropDown.firmUsersList || []}
            value={( props.firmUser ) || []}
            placeholder="Select User"
            onChange={item => onUserChange("firmUser", item)}
            style={{maxWidth: "unset"}}
            isFull
          /> : null
        }

      </div>
    </Modal>
  )
}

export default ShareDocumentsModal
