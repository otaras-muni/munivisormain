import React from "react"
import {toast} from "react-toastify"
import {withRouter} from "react-router-dom"
import connect from "react-redux/es/connect/connect"
import Documents from "../component/Document"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import CONST, {ContextType} from "../../../../../../globalutilities/consts"
import Loader from "../../../../../GlobalComponents/Loader"
import {
  getDocFolders,
  getDocsByContextId,
  putDocFolders
} from "../../../../../StateManagement/actions/docs_actions/docs"
import {fetchAssigned} from "../../../../../StateManagement/actions/CreateTransaction"



class MyDocuments extends React.Component {
  constructor(){
    super()
    this.state = {
      folderList: [],
      folderPropList: [],
      userDocsList: [],
      showDocuments: [],
      firmUsersList: [],
      auditLogs: [],
      loading: true,
    }
  }

  async componentWillMount() {
    const {user} = this.props
    const docFolder = await getDocFolders(`?userId=${user.userId}`)
    const folder = docFolder.data.filter(f => f.myDocuments === true) || []
    const folderList = folder.map(a => a.folderName)
    fetchAssigned(this.props.user.entityId, res => {
      this.setState({
        firmUsersList: res && res.usersList || [],
        folderList,
        folderPropList: folder || [],
        loading: false
      })
    })
  }

  onDeleteDoc = async (doc, folderId, callback) => {
    const docs = {}
    if(doc.length){
      docs.pullDocuments = doc
      putDocFolders(folderId, docs, res => {
        console.log("==============>", res && res.data || [])
      })
    }
    const query = `?contextType=USERSDOCS&&folderId=${folderId}`
    const getDocs = await getDocsByContextId(query)
    if (getDocs && getDocs.status === 200) {
      toast("documents removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      callback({
        status: true,
        documentsList: getDocs && getDocs.data || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onSave = async (folderId, callback) => {
    const query = `?contextType=USERSDOCS&&folderId=${folderId}`
    const getDocs = await getDocsByContextId(query)
    if (getDocs && getDocs.status === 200) {
      toast("documents saved successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      callback({
        status: true,
        documentsList: getDocs && getDocs.data || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  render() {
    const { folderList, loading, folderPropList, userDocsList, showDocuments, firmUsersList } = this.state
    const { svControls } = this.props
    if(loading) {
      return <Loader />
    }
    return (
      <div>
        <Documents {...this.props} title="My Documents" folder={this.state.folder} pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickAction="LKUPDOCACTION" svControls={svControls}
          onDeleteDoc={this.onDeleteDoc} onSave={this.onSave} firmUsersList={firmUsersList} folderList={folderList} folderPropList={folderPropList} userDocsList={userDocsList} showDocuments={showDocuments} contextType={ContextType.usersDocs}/>
      </div>
    )
  }
}


const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default withRouter(connect(mapStateToProps, null)(MyDocuments))
