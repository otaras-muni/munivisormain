import React from "react"
import ReactTable from "react-table"
import moment from "moment"
import "react-table/react-table.css"
import DocLink from "../../../../docs/DocLink"
import DocUpload from "../../../../docs/DocUpload"
import { DocModal }  from "../../../../docs/DocModal"


const DocumentsListTable = ({
  documentsList,
  bucketName,
  svControls,
  deleteDoc,
  isDisable,
  onCreateFolderModal,
  pageSizeOptions = [5, 10, 20, 50, 100],
  user,
  contextId,
  contextType,
}) => {
  const createDate = []
  const createBy = []
  if(contextType === "SHAREDOCS" && !isDisable){
    createBy.push({
      id: "createdBy",
      Header: "Created By",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const doc = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {
              doc.createdBy ? doc.createdBy : "" || "-"
            }
          </div>
        )
      },
      sortMethod: (a, b) => a.createdBy.localeCompare(b.createdDate)
    })
  }
  if(contextType === "SHAREDOCS" && isDisable){
    createDate.push({
      id: "sharedBy",
      Header: "Shared By",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const doc = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {
              doc.sharedBy ? doc.sharedBy : "" || "-"
            }
          </div>
        )
      },
      sortMethod: (a, b) => a.createdDate ? a.createdDate.localeCompare(b.createdDate) : a.createdDate.localeCompare(b.createdDate)
    })
  }
  const columns = [
    {
      Header: "Filename",
      id: "docFileName",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const doc = row.value
        return (
          <div className="field is-grouped-left">
            { svControls && !svControls.edit && contextType === "SHAREDOCS" ? null :
              <DocUpload
                bucketName={bucketName}
                docMeta={{
                  category: doc.docCategory,
                  subCategory: doc.docSubCategory,
                  type: doc.docType
                }}
                docId={doc._id}
                versionMeta={{
                  uploadedBy: `${user && user.userFirstName} ${user && user.userLastName}`
                }}
                showFeedback
                contextId={user.userId || contextId}
                contextType={contextType || ""}
                tenantId={user.entityId}
              />
            }
            &nbsp;&nbsp;
            <DocLink docId={doc._id} />
            &nbsp;&nbsp;
            <DocModal
              onDeleteAll={deleteDoc}
              documentId={doc._id}
              versionMetaToShow={["uploadedBy"]}
              selectedDocId={doc._id}
              isDisable={isDisable}
              contextType={contextType || ""}
              svControls={svControls}
              myDocuments
            />
            {/* )} */}
          </div>
        )
      },
      sortMethod: (a, b) => a.originalName.localeCompare(b.originalName)
    },
    {
      id: "createdDate",
      Header: "Created Date",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const doc = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {
              doc.createdDate ? moment(doc.createdDate).format("MM.DD.YYYY hh:mm A") : "" || "-"
            }
          </div>
        )
      },
      sortMethod: (a, b) => a.createdDate ? a.createdDate.localeCompare(b.createdDate) : a.createdDate.localeCompare(b.createdDate)
    },
    ...createBy,
    ...createDate,
    {
      id: "edit",
      Header: "Action",
      accessor: item => item,
      Cell: row => {
        const doc = row.value
        return (
          <div>
            {contextType === "SHAREDOCS" ? doc.contextType === "USERSDOCS" ? null :
              <div>
                { svControls && svControls.edit ?
                  <div style={{textAlign: "center"}}>
                    <a title="Share File" className="fas fa-share-alt" name="shareDocumentModal"
                      onClick={(e) => onCreateFolderModal(e, "Document", doc)}/>
                  </div> : null
                }
              </div> :
              <div>
                { svControls && svControls.edit ?
                  <div style={{textAlign: "center"}}>
                    <a title="Share File" className="fas fa-share-alt" name="shareDocumentModal"
                      onClick={(e) => onCreateFolderModal(e, "Document", doc)}/>
                  </div> : null
                }
              </div>
            }
          </div>
        )
      }
    }
  ]

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={documentsList}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default DocumentsListTable
