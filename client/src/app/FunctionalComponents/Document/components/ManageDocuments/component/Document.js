import React from "react"
import { Prompt, withRouter } from "react-router-dom";
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import moment from "moment"
import {
  createDocFolders,
  getDocFolders, getDocsByContextId,
  putDocFolders,
  putDocs
} from "../../../../../StateManagement/actions/docs_actions/docs"
import { checkInvalidFiles } from "GlobalUtils/helpers"
import { getUserById } from "../../../../../StateManagement/actions/AdminManagement/admTrnActions"
import RatingSection from "../../../../../GlobalComponents/RatingSection"
import TableHeader from "../../../../../GlobalComponents/TableHeader"
import Loader from "../../../../../GlobalComponents/Loader"
import DocumentsView from "../../../../../GlobalComponents/DocumentsView"
import { DocumentsValidate } from "../../../../../GlobalComponents/DocumentsValidate"
import { CreateNewFolderModal } from "./CreateNewFolderModal"
import { ShareDocumentsModal } from "./ShareDocumentsModal"
import DocModalDetails from "../../../../../FunctionalComponents/docs/DocModalDetails"
import CONST from "../../../../../../globalutilities/consts"
import withAuditLogs from "../../../../../GlobalComponents/withAuditLogs"
import Accordion from "../../../../../GlobalComponents/Accordion"
import DragAndDropFileInState from "../../../../../GlobalComponents/DragAndDropFileInState"
import DocumentsListTable from "./DocumentsListTable"
import {DropDownSelect} from "../../../../../GlobalComponents/TextViewBox"

const cols = [
  [
    { name: "Select Folder" },
    { name: "Upload File <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
    { name: "Filename <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
    { name: "Action" }
  ]
]

class Documents extends React.Component {
  constructor(props) {
    super(props)
    const { staticField, user } = this.props
    this.state = {
      bucketName: process.env.S3BUCKET,
      userName: "",
      loading: true,
      isSave: false,
      fileName: "",
      signedS3Url: "",
      selectedDocId: "",
      isEditable: "",
      auditLogs: [],
      uploadedFiles: [],
      loadDocuments: [],
      documents: [
        {
          ...cloneDeep(CONST.Documents),
          ...staticField,
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        }
      ],
      resetDocuments: [
        {
          ...cloneDeep(CONST.Documents),
          ...staticField,
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        }
      ],
      errorMessages: {},
      waiting: false,
      doc: {},
      showModal: false,
      isSaveDisabled: false,
      documentsList: [],
      searchList: [],
      dropDown: {
        folderList: [],
        docType: [],
        actions: [],
      },
      activeItem: [],
      folderPropList: [],
      firmUser: [],
      folderId: "",
      showFolderName: "",
      showFolderId: "",
      fileTypeError: "",
      folderModal: false,
      chooseMultiple: false,
      shareDocumentModal: false,
      isDisable: false,
      sharingLevel: "",
      sharingType: "",
      selectFrom: "",
      selectTo: "",
      error: "",
      sortBy: "",
      searchText: "",
      errors: {}
    }
  }

  async componentDidMount() {
    const {
      documents,
      user,
      folderList,
      folderPropList,
      userDocsList,
      firmUsersList,
      showDocuments
    } = this.props

    documents && documents.forEach(doc => {
      doc.timeStamp = doc.LastUpdatedDate || doc.lastUpdatedDate ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix() : 0
    })
    this.setState({
      documentsList: documents && documents.length ? cloneDeep(documents) : [],
      dropDown: {
        ...this.state.dropDown,
        folderList,
        firmUsersList
      },
      folderPropList,
      userDocsList,
      showDocuments,
      userName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
      loading: false
    }, () => {
      if(this.props.contextType === "SHAREDOCS"){
        const nonDoc = this.state.folderPropList.filter(f => !f.myDocuments)
        const duplicateList = nonDoc.map(m => m.folderName)
        this.setState({duplicateList})
      }
    })
  }

  onChangeItem = (item, index, category, change, name) => {
    const items = this.state[category]
    items.splice(index, 1, item)

    if (change) {
      this.updateAudit(category, change, name)
    }
    this.setState({
      [category]: items
    })
  }

  updateAudit = (category, change, key) => {
    const { userName } = this.state
    const { auditLogs } = this.props
    const alreadyExists = auditLogs.findIndex(audit => audit.key === key)
    const audit = {
      userName,
      log: `In ${category} ${change}`,
      date: new Date(),
      key
    }
    if(alreadyExists !== -1){
      auditLogs[alreadyExists] = audit
    }else {
      auditLogs.push(audit)
    }
    this.props.updateAuditLog(auditLogs)
  }

  onAdd = key => {
    const { userName } = this.state
    const { staticField, user } = this.props
    this.props.addAuditLog({
      userName,
      log: `${key} Add new Item`,
      date: new Date(),
      key
    })
    this.setState({
      [key]: [
        ...this.state[key],
        key === "documents"
          ? {
            ...cloneDeep(CONST.Documents),
            ...staticField,
            createdBy: (this.props.user && this.props.user.userId) || "",
            createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
          }
          : {}
      ]
    })
  }

  onReset = category => {
    const auditLogs = this.props.auditLogs.filter(x => x.key !== category)
    this.props.updateAuditLog(auditLogs)
    this.setState(prevState => ({
      documents: cloneDeep(prevState.resetDocuments)
    }))
  }

  onSave = (documents) => {
    const {tranId, category, user, staticField, contextType} = this.props
    const folderId = this.state.documents && this.state.documents.length ? this.state.documents && this.state.documents[0] && this.state.documents[0].folderId : ""
    const { showFolderId } = this.state
    let payload = { _id: tranId, documents }
    if (category) {
      payload = { _id: tranId, [category]: payload.documents }
    }
    const docs = { updatedDate: new Date()}
    if(documents && documents.length > 1){
      const doc = []
      documents.forEach(d => {
        if(doc.indexOf(d.folderId) === -1){
          putDocFolders(d.folderId, docs, res => {
            console.log("==============>", res && res.data || [])
          })
          doc.push(d.folderId)
        }
      })
    } else {
      putDocFolders(folderId || documents && documents[0] && documents[0].folderId, docs, res => {
        console.log("==============>", res && res.data || [])
      })
    }
    if(payload._id && documents.length){
      this.setState({
        isSaveDisabled: true
      },() => {
        this.props.onSave(payload, res => {
          if (res && res.status) {
            res.documentsList.forEach(doc => {
              doc.timeStamp = doc.LastUpdatedDate || doc.lastUpdatedDate ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix() : 0
            })
            this.setState({
              errorMessages: {},
              documents: [
                {
                  ...cloneDeep(CONST.Documents),
                  ...staticField,
                  createdBy: (user && user.userId) || "",
                  createdUserName:
                    (user && `${user.userFirstName} ${user.userLastName}`) || ""
                }
              ],
              documentsList: res.documentsList,
              isSaveDisabled: false,
              activeItem: [],
              uploadedFiles: []
            })
          } else {
            this.setState({
              errorMessages: {},
              isSaveDisabled: false,
              activeItem: []
            })
          }
        })
      })
    }else {
      this.props.onSave(showFolderId, res => {
        this.props.submitAuditLogs(contextType === "USERSDOCS" ? user.userId : user.entityId)
        this.setState({
          userDocsList: res && res.documentsList || [],
          errorMessages: {},
          documents:                                                                                                 [
            {
              ...cloneDeep(CONST.Documents),
              ...staticField,
              createdBy: (user && user.userId) || "",
              createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
            }
          ],
          isSaveDisabled: false,
          activeItem: [],
          uploadedFiles: [],
          isSave: false,
          loadDocuments: null
        }, async () => {
          if(res && res.documentsList){
            const query = contextType === "USERSDOCS" ? `?userId=${user.userId}` : ""
            const folderProps = await getDocFolders(query)
            const folderList = []
            let folderProperty = []
            if(contextType === "SHAREDOCS" && folderProps && folderProps.data && folderProps.data.length){
              folderProps.data.map(doc => {
                const matchFolder = doc.shareFolder.filter(f => f.id === user.userId)
                const matchDocument = doc.shareDocuments.filter(s => s.id === user.userId)
                if (matchFolder.length > 0 || matchDocument.length > 0) {
                  folderList.push(doc)
                } else {
                  if (doc.myDocuments === false) {
                    folderList.push(doc)
                  }
                }
              })
              folderProperty = folderList.filter(u => u.entityId === user.entityId)
            } else {
              folderProperty = folderProps.data.filter(u => u.myDocuments)
            }
            this.setState({
              folderPropList: folderProperty.length ? folderProperty : folderProps && folderProps.data || []
            }, () => {
              if(!showFolderId){
                const { folderPropList } = this.state
                const folder = folderPropList.filter(f => f._id === documents[0].folderId) || []
                this.showDocuments(folder && folder[0] || folderPropList && folderPropList[0])
              } else {
                this.onShowingDocs()
              }
            })
          }
        })
      })
    }
  }

  onShowingDocs = async () => {
    const { showFolderId, folderPropList, userDocsList } = this.state
    const { contextType, user } = this.props
    let showDocuments = []
    let dangerMode = ""
    let isDisable = Boolean
    const folder = folderPropList.filter(f => f._id === showFolderId)
    if(showFolderId && contextType === "USERSDOCS" && folder && folder[0] && folder[0].myDocuments){
      showDocuments = userDocsList.filter(f => f.folderId === showFolderId)
      dangerMode = showDocuments.length === 0 ? folder && folder[0] && folder[0]._id : ""
      isDisable = false
    } else if(showFolderId && contextType === "SHAREDOCS" && folder && folder[0] && folder[0].myDocuments){
      const shareFol = folder && folder[0] && folder[0].shareFolder.filter(s => s.id === user.userId)
      isDisable = true
      if(shareFol.length > 0){
        showDocuments = userDocsList.filter(f => f.folderId === showFolderId)
        dangerMode = showDocuments.length === 0 ? showFolderId : ""
      } else if(folder && folder[0] && folder[0].shareDocuments.length > 0){
        const shareFol = folder && folder[0] && folder[0].shareDocuments.filter(s => s.id === user.userId)
        if(shareFol.length > 0){
          userDocsList.map(u => {
            shareFol.map(s => {
              const a = s.files[0] === u._id
              if(a){
                showDocuments.push(u)
              }
            })
          })
        }
        dangerMode = showDocuments.length === 0 ? showFolderId : ""
      }
      if(showDocuments.length){
        const getUser = await getUserById(folder && folder[0] && folder[0].userId)
        showDocuments.forEach(s => {
          s.sharedBy = `${getUser.data.userFirstName} ${getUser.data.userLastName}` || ""
        })
      }
    } else if(showFolderId && contextType === "SHAREDOCS" && !folder.myDocuments){
      showDocuments = userDocsList.filter(f => f.folderId === showFolderId)
      dangerMode = showDocuments.length === 0 ? showFolderId : ""
      isDisable = false
    }
    this.setState({
      showDocuments,
      dangerMode,
      showFolderId,
      showFolderName: folder && folder[0] && folder[0].folderName || "",
      isDisable,
    })
  }

  onRemove = index => {
    const { documents } = this.state
    documents.splice(index, 1)
    this.setState({
      documents
    })
  }

  onChangeError = (name, key, index) => {
    this.setState(prevState => ({
      errorMessages: {
        ...prevState.errorMessages,
        [key]: {
          ...(prevState.errorMessages && prevState.errorMessages[key]),
          [index]: {
            ...(prevState.errorMessages && prevState.errorMessages[key] && prevState.errorMessages[key][index.toString()]),
            [name]: ""
          }
        }
      }
    }))
  }

  onCreateFolderModal = (e, name, doc) => {
    const {folderModal, shareDocumentModal} = this.state
    if(folderModal || shareDocumentModal){
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          firmUsersList: this.props.firmUsersList || [],
          folderList: this.props.folderList || []
        },
        folderModal: false,
        shareDocumentModal: false,
        folderName: "",
        sharingType: "",
        selectFrom: "",
        selectTo: "",
        sharingLevel: "",
        error: "",
        fileName: "",
        firmUser: []
      })
    } else {
      this.setState({
        [e.target.name]: true,
        sharingLevel: name || "",
        documentId: name === "Document" ? doc._id : "",
        fileName: name === "Document" ? doc.originalName : "",
      })
    }
  }

  onFolderChange = (e, name, fold, i) => {
    const { documents } = this.state
    const value = fold === "multipleFolder" ? name.target.value : name && name.value
    if(e === "folderName"){
      documents[i].folderName = value
      this.setState({
        documents
      }, () => {
        const {folderPropList} = this.state
        if(value){
          const folder = folderPropList.filter(f => f.folderName === value)
          this.state.documents[i].folderId = folder[0]._id
          this.setState({documents})
        }
      })
    } else if(e === "multiFile"){
      this.setState({
        [name]: fold && fold.value || ""
      }, () => {
        const {folderPropList} = this.state
        if(this.state.folderName){
          const folder = folderPropList.filter(f => f.folderName === this.state.folderName)
          this.setState({folderId: folder[0]._id || ""})
        }
      })
    } else {
      this.setState({
        [e.target.name]: e.target.value
      })
    }
  }

  onFolderSave = () => {
    const {folderName, folderPropList, duplicateList} = this.state
    const {user, contextType} = this.props
    const folderError = folderPropList.filter(f => f.folderName === folderName)
    if(folderError.length > 0 || folderName === "My Files Without Folder" || folderName === "Share Files Without Folder"){
      this.setState({
        error: "folder name is already exit"
      })
      return
    }
    this.props.addAuditLog({
      userName: `${user.userFirstName} ${user.userLastName}`,
      log: `Created New Folder ${folderName} In ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents`,
      date: new Date(),
      key: contextType === "USERSDOCS" ? "myDocuments" : "sharedDocuments"
    })
    const doc = {
      folderName,
      entityId: user.entityId,
      userId: user.userId,
      createdDate: new Date(),
      updatedDate: new Date(),
      myDocuments: contextType === "USERSDOCS" ? true : false
    }
    const docs = {
      folderName: contextType === "USERSDOCS" ? "My Files Without Folder" : "Share Files Without Folder",
      entityId: user.entityId,
      userId: user.userId,
      createdDate: new Date(),
      updatedDate: new Date(),
      myDocuments: contextType === "USERSDOCS" ? true : false
    }
    if(folderPropList && folderPropList.length === 0 && contextType === "USERSDOCS"){
      createDocFolders(docs, res => {
        console.log(res && res.data || [])
      })
    } else if(duplicateList && duplicateList.length === 0 && contextType === "SHAREDOCS"){
      createDocFolders(docs, res => {
        console.log(res && res.data || [])
      })
    }
    createDocFolders(doc, data => {
      toast("Create New Folder successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.setState({
        folderModal: false, folderName: "", error: ""
      }, async () => {
        const docFolderList = await getDocFolders(`?userId=${user.userId}`)
        const docFolder = docFolderList.data.filter(d => contextType === "USERSDOCS" ? d.myDocuments === true : d.myDocuments === false)
        const shareDoc = contextType === "SHAREDOCS" ? this.state.folderPropList.filter(p => p.myDocuments) : []
        const folder = docFolder.map(a => a.folderName)
        this.props.submitAuditLogs(contextType === "USERSDOCS" ? user.userId : user.entityId)
        this.setState({
          dropDown:{
            ...this.state.dropDown,
            folderList: folder || []
          },
          folderPropList: shareDoc.length ? shareDoc.concat(docFolder) : docFolder || []
        }, () => {
          if(this.props.contextType === "SHAREDOCS"){
            const nonDoc = this.state.folderPropList.filter(f => !f.myDocuments)
            const duplicateList = nonDoc.map(m => m.folderName)
            this.setState({duplicateList})
          }
        })
      })
    })
  }

  onSharingDocChange = (name, e) => {
    if(e && e.target && e.target.value){
      this.setState({
        [name]: e.target.value
      }, () => {
        if(this.state.selectFrom && this.state.sharingType === "share"){
          const userFirm = this.state.folderPropList.filter(f => f.folderName === this.state.selectFrom)
          this.setState({firmUser: userFirm[0].shareFolder || []})
        }
      })
    } else if(e === "firmUser") {
      this.setState({
        [e]: name && name.firmUser
      })
    } else {
      this.setState({
        sharingType: name
      }, () => {
        if(this.props.contextType === "SHAREDOCS" && (this.state.sharingLevel === "Folder" || this.state.sharingLevel === "Document")){
          if(this.state.sharingType === "copy"){
            this.setState({
              dropDown: {
                ...this.state.dropDown,
                folderList: this.props.folderList || []
              }
            })
          } else if(this.state.sharingType === "move"){
            const  folder = this.state.folderPropList.filter(f => f.myDocuments === false)
            const filterFolder = folder.map(f => f.folderName)
            this.setState({
              dropDown: {
                ...this.state.dropDown,
                folderList: filterFolder || []
              }
            })
          }
        }
        if(this.state.sharingLevel === "Document" && this.state.sharingType === "share"){
          const shareDocList = this.state.folderPropList.filter(f => f._id === this.state.showFolderId)
          const docs = []
          if(shareDocList.length > 0){
            shareDocList.map(s => {
              s.shareDocuments.map(m => {
                const a = m.files.filter(f => f === this.state.documentId)
                if(a.length > 0){
                  docs.push(m)
                }
              })
            })
          }
          const filterFirmUserList = this.state.dropDown.firmUsersList.filter(o => !docs.find(o2 => o.id === o2.id))
          this.setState({
            dropDown: {
              ...this.state.dropDown,
              firmUsersList: filterFirmUserList || [],
            },
            sendToUser: docs
          })
        }
      })
    }
  }

  onShareDocsSave = () => {
    const { selectFrom, selectTo, folderPropList, sharingType, documentId, sharingLevel, showFolderId, firmUser, fileName } = this.state
    const { contextType, user } = this.props
    const selectedFromId = folderPropList.filter(f => f.folderName === selectFrom)
    const a = firmUser && firmUser.length && firmUser.map(f => f.name) || []
    let log = ""
    if(sharingType === "share" && (sharingLevel === "Document" || sharingLevel === "Folder")){
      log = sharingLevel === "Document" ? `${fileName} File Shared With [${a}] In ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents` :
        `${selectFrom} Shared With [${a}] In ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents`
    } else {
      const docFile = sharingLevel === "Document" ? `${fileName} File` : selectFrom
      log = `${docFile} ${sharingType === "move" ? "moved" : sharingType} To ${selectTo} In ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents`
    }
    this.props.addAuditLog({
      userName: `${user.userFirstName} ${user.userLastName}`,
      log,
      date: new Date(),
      key: contextType === "USERSDOCS" ? "myDocuments" : "sharedDocuments"
    })
    if(sharingType !== "share"){
      const selectToId = folderPropList.filter(f => f.folderName === selectTo)
      const doc = {folderId: selectToId[0]._id, sharingType, contextType: selectToId[0].myDocuments ? "USERSDOCS" : "SHAREDOCS"}
      let query = ""
      if(contextType === "SHAREDOCS"){
        if(sharingLevel === "Folder" && selectedFromId && selectedFromId.length && selectedFromId[0].myDocuments && sharingType === "copy"){
          query = `?contextType=USERSDOCS&folderId=${selectedFromId[0]._id}`
        } else if(sharingLevel === "Document" && sharingType === "copy" && this.state.isDisable){
          query = `?contextType=USERSDOCS&_id=${documentId}`
        } else {
          query  = sharingLevel === "Document" ? `?contextType=${contextType}&_id=${documentId}` : `?contextType=${contextType}&folderId=${selectedFromId[0]._id}`
        }
      } else if(contextType === "USERSDOCS") {
        query  = sharingLevel === "Document" ? `?contextType=${contextType}&_id=${documentId}` : `?contextType=${contextType}&folderId=${selectedFromId[0]._id}`
      }
      putDocs(query, doc, res => {
        const docs = { updatedDate: new Date()}
        putDocFolders(doc.folderId, docs, res => {
          console.log("==============>", res && res.data || [])
        })
        toast(`${sharingLevel} ${sharingType === "copy" ? "copied" : sharingType === "move" ? "moved" : sharingType} Successfully.`, {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
        this.props.submitAuditLogs(contextType === "USERSDOCS" ? user.userId : user.entityId)
        this.setState({
          dropDown: {
            ...this.state.dropDown,
            firmUsersList: this.props.firmUsersList || [],
          },
          userDocsList: res && res.data || [],
          shareDocumentModal: false,
          inProgress: false,
          folderModal: false,
          folderName: "",
          sharingType: "",
          selectFrom: "",
          selectTo: "",
          fileName: "",
          sharingLevel: ""
        }, async () => {
          if(res && res.data && res.data.length && showFolderId){
            const query = contextType === "USERSDOCS" ? `?userId=${user.userId}` : ""
            const folderProps = await getDocFolders(query)
            const folderList = []
            let folderProperty = []
            if(contextType === "SHAREDOCS" && folderProps && folderProps.data && folderProps.data.length){
              folderProps.data.map(doc => {
                const matchFolder = doc.shareFolder.filter(f => f.id === user.userId)
                const matchDocument = doc.shareDocuments.filter(s => s.id === user.userId)
                if (matchFolder.length > 0 || matchDocument.length > 0) {
                  folderList.push(doc)
                } else {
                  if (doc.myDocuments === false) {
                    folderList.push(doc)
                  }
                }
              })
              folderProperty = folderList.filter(u => u.entityId === user.entityId)
            } else {
              folderProperty = folderProps.data.filter(u => u.myDocuments)
            }
            this.setState({
              folderPropList: folderProperty.length ? folderProperty : folderProps && folderProps.data || []
            })
            this.onShowingDocs()
          }
        })
      })
    } else {
      const doc = {
        user: this.props.user
      }
      if(sharingLevel === "Folder"){
        doc.shareFolder = firmUser
      } else if(sharingLevel === "Document"){
        doc.shareDocuments = firmUser
      }
      const docId = sharingLevel === "Document" ? showFolderId : selectedFromId[0]._id
      putDocFolders(docId, doc, res => {
        toast(`${sharingLevel} ${sharingType === "share" ? "shared" : sharingType} Successfully.`, {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
        this.props.submitAuditLogs(contextType === "USERSDOCS" ? user.userId : user.entityId)
        const folderList = res && res.data.filter(r => contextType === "USERSDOCS" ? r.myDocuments === true : r.myDocuments === false) || []
        this.setState({
          dropDown: {
            ...this.state.dropDown,
            firmUsersList: this.props.firmUsersList || [],
          },
          folderPropList: folderList || [],
          shareDocumentModal: false,
          inProgress: false,
          folderModal: false,
          folderName: "",
          sharingType: "",
          selectFrom: "",
          selectTo: "",
          firmUser: [],
          fileName: "",
          sharingLevel: ""
        })
      })
    }
  }

  onClickUpload = (event) => {
    event.target.value = null
  }

  onChangeUpload = (event) => {
    this.saveFileInState(event.target.files[0], true, event.target.id)
  }

  onFolderSearch = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    }, () => {
      const { searchText, folderPropList } = this.state
      if (searchText) {
        const searchFolderList = folderPropList.filter(obj => ["folderName"].some(key => {
          return obj[key] && obj[key].toLowerCase().includes(searchText.toLowerCase())
        }))
        this.setState({
          searchFolderList
        })
      }
    })
  }

  onFolderSorting = (sortType, date) => {
    const { searchText, searchFolderList, folderPropList } = this.state
    const sort = searchText ? searchFolderList : folderPropList
    sort.sort(function(a, b){
      const dateA = new Date(a[date]), dateB = new Date(b[date])
      if(sortType === "dsc"){
        return dateB-dateA
      } else {
        return dateA-dateB
      }
    })
    this.setState({
      searchFolderList,
      folderPropList,
      sortBy: sortType === "asc" ? "dsc" : "asc"
    })
  }

  setStateParentLevel = (state) => {
    this.setState({
      ...state
    })
  }

  showDocuments = async (folder) => {
    const { contextType } = this.props
    let query = ""
    if(contextType === "SHAREDOCS" && folder.myDocuments){
      query = `?contextType=USERSDOCS&&folderId=${folder._id}`
    } else {
      query = `?contextType=${contextType}&&folderId=${folder._id}`
    }
    const getDocs = await getDocsByContextId(query)
    this.setState({
      showFolderId: folder._id,
      showFolderName: folder.folderName,
      userDocsList: getDocs && getDocs.data || []
    }, () => {
      this.onShowingDocs()
    })
  }

  toggleMultipleFile = () => {
    this.setState(prevState => ({
      chooseMultiple: !prevState.chooseMultiple,
      documents: [],
      folderName: "",
      folderId: ""
    }))
  }

  saveFileInState = (uploadedFiles, single, index) => {
    console.log(uploadedFiles)
    const { documents, folderId, folderName } = this.state
    const { staticField, user, contextType } = this.props
    const docs = documents|| []

    if(single){
      const invalidFile = checkInvalidFiles(uploadedFiles, "single")
      if(invalidFile){
        this.setState({
          fileTypeError: invalidFile || ""
        })
        return
      }
      documents[index].docFileName = uploadedFiles.name
      documents[index].file = uploadedFiles
      documents[index].docCategory = "Firm Documents"
      documents[index].docSubCategory = `Firm ${contextType === "USERSDOCS" ? "User" : "Shared "} Documents`
      documents[index].docType = "Firm Documents"
    }else if (uploadedFiles && Array.isArray(uploadedFiles) && uploadedFiles.length) {
      if(docs && docs[0] && docs[0].docFileName === ""){
        docs.splice(0, 1)
      }
      const invalidFile = checkInvalidFiles(uploadedFiles, "multi")
      if(invalidFile){
        this.setState({
          fileTypeError: invalidFile || ""
        })
        return
      }
      uploadedFiles.forEach(file => {
        docs.push({
          file,
          docCategory: "Firm Documents",
          docSubCategory: `Firm ${contextType === "USERSDOCS" ? "User" : "Shared "} Documents`,
          docType: "Firm Documents",
          docAWSFileLocation: "",
          docAction: "",
          docFileName: file.name,
          markedPublic: {
            publicFlag: false,
            publicDate: new Date().toString()
          },
          sentToEmma: {
            emmaSendFlag: false,
            emmaSendDate: new Date().toString()
          },
          docNote: "",
          ...staticField,
          folderId,
          folderName,
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        })
      })
    }

    this.setState({
      uploadedFiles,
      chooseMultiple: false,
      fileTypeError: "",
      documents: uploadedFiles && Array.isArray(uploadedFiles) && uploadedFiles.length ? docs : documents
    })
  }

  actionButtons = (key, isDisabled) => {
    if (isDisabled) return

    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset(key)}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  deleteDoc = (versionId, documentId, filename) => {
    const { showFolderId, showFolderName, folderPropList } = this.state
    console.log("versionId : ", this.state)
    console.log("versionId : ", versionId)
    console.log("documentId : ", documentId)
    const selectFolder = folderPropList.filter(f => f._id === showFolderId)
    const shareDocList = selectFolder[0].shareDocuments.filter(s => s.files[0] === versionId )
    const shareDocId = shareDocList.map(m => m._id)
    const { onDeleteDoc, contextType, user } = this.props
    const log = showFolderName === "" ? `${filename} document deleted In ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents` :
      `${showFolderName} In ${filename} document deleted In ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents`
    if (onDeleteDoc) {
      this.props.addAuditLog({
        userName: `${user.userFirstName} ${user.userLastName}`,
        log,
        date: new Date(),
        key: contextType === "USERSDOCS" ? "myDocuments" : "sharedDocuments"
      })
      const docs = { updatedDate: new Date()}
      putDocFolders(showFolderId, docs, res => {
        console.log("==============>", res && res.data || [])
      })
      onDeleteDoc(shareDocId, showFolderId, res => {
        if (res && res.status) {
          this.setState({
            userDocsList: res && res.documentsList || []
          }, async () => {
            this.props.submitAuditLogs(contextType === "USERSDOCS" ? user.userId : user.entityId)
            if(res && res.documentsList || this.state.userDocsList){
              const query = contextType === "USERSDOCS" ? `?userId=${user.userId}` : ""
              const folderProps = await getDocFolders(query)
              const folderList = []
              let folderProperty = []
              if(contextType === "SHAREDOCS" && folderProps && folderProps.data && folderProps.data.length){
                folderProps.data.map(doc => {
                  const matchFolder = doc.shareFolder.filter(f => f.id === user.userId)
                  const matchDocument = doc.shareDocuments.filter(s => s.id === user.userId)
                  if (matchFolder.length > 0 || matchDocument.length > 0) {
                    folderList.push(doc)
                  } else {
                    if (doc.myDocuments === false) {
                      folderList.push(doc)
                    }
                  }
                })
                folderProperty = folderList.filter(u => u.entityId === user.entityId)
              } else {
                folderProperty = folderProps.data.filter(u => u.myDocuments)
              }
              this.setState({
                folderPropList: folderProperty.length ? folderProperty : folderProps && folderProps.data || []
              })
              this.onShowingDocs()
            }
          })
        }
      })
    }
  }

  handleDocDetails = res => {
    this.setState({
      showModal: !this.state.showModal,
      doc: res || {}
    })
  }

  updateSaveFlag = async () => {
    const { documents, folderPropList, duplicateList } = this.state
    const {
      tranId,
      user,
      tags,
      contextType,
    } = this.props
    const payload = { _id: tranId, documents }
    const errors = DocumentsValidate(payload)
    const docs = {
      folderName: contextType === "USERSDOCS" ? "My Files Without Folder" : "Share Files Without Folder",
      entityId: user.entityId,
      userId: user.userId,
      createdDate: new Date(),
      updatedDate: new Date(),
      myDocuments: contextType === "USERSDOCS" ? true : false
    }
    if(folderPropList && folderPropList.length === 0 && contextType === "USERSDOCS" || duplicateList && duplicateList.length === 0 && contextType === "SHAREDOCS"){
      await createDocFolders(docs, res => {
        this.setState({
          ...this.state.folderPropList,
          folderPropList: [res && res.data] || []
        })
        console.log(res && res.data || [])
        payload.documents.forEach(f => {
          if(f.folderId === undefined && f.folderName === undefined || f.folderId === "" && f.folderName === ""){
            f.folderName = res && res.data && res.data.folderName || ""
            f.folderId = res && res.data && res.data._id || ""
          }
        })
      })
    }else {
      const folder = folderPropList.find(f => contextType === "USERSDOCS" ? f.folderName === "My Files Without Folder" : f.folderName === "Share Files Without Folder") || {}
      payload.documents.forEach(f => {
        if(f.folderId === undefined && f.folderName === undefined || f.folderId === "" && f.folderName === ""){
          f.folderName = (folder && folder.folderName) || ""
          f.folderId = (folder && folder._id) || ""
        }
      })
    }
    if (tags) {
      delete tags.tenantId
      delete tags.tenantName
      delete tags.clientId
    }
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) { //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) { //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = "Required" // err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: "Required" // err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = "Required" // err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] = {
            [err.path[1]]: {
              [err.path[2]]: "Required" // err.message
            }
          }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: "Required" // err.message
          }
        }
      })
      this.setState({ errorMessages })
      return
    }
    const audit = []
    let duplicateAudit = {}
    let log = ""
    documents.forEach(d => {
      log = d.folderName ? `${d.folderName} in ${d.docFileName} Document Upload in ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents` :
        `${d.docFileName} Document Upload in ${contextType === "USERSDOCS" ? "My" : "Shared"} Documents`
      duplicateAudit = {
        userName: `${user.userFirstName} ${user.userLastName}`,
        log,
        date: new Date(),
        key: contextType === "USERSDOCS" ? "myDocuments" : "sharedDocuments"
      }
      audit.push(duplicateAudit)
    })
    this.props.addMultiAuditLog(audit)
    this.setState({
      isSaveDisabled: true,
      isSave: true,
      loadDocuments: payload.documents
    })
  }

  clickUpload = (index) => {
    document.getElementById(index).click()
  }

  render() {
    const {
      bucketName,
      chooseMultiple,
      contextId,
      isSave,
      loadDocuments,
      documentId,
      sendToUser,
      fileName,
      folderName,
      documents,
      errorMessages,
      doc,
      dropDown,
      isSaveDisabled,
      sharingLevel,
      sharingType,
      shareDocumentModal,
      selectFrom,
      selectTo,
      dangerMode,
      isDisable,
      duplicateList,
      fileTypeError,
      firmUser,
      folderPropList,
      showDocuments,
      showFolderName,
      folderId,
      folderModal,
      error,
      searchText,
      searchFolderList,
      sortBy,
      activeItem
    } = this.state
    const {
      title,
      user,
      isDisabled,
      contextType,
      staticField,
      svControls,
      tableStyle
    } = this.props
    const isExistsRole = selectFrom || []
    const FolderName = showFolderName || []
    const filterFolder = dropDown.folderList !== undefined ? dropDown.folderList.filter(role => isExistsRole.indexOf(role) === -1) || [] : []
    const folderChange = sharingLevel === "Document" ? filterFolder.filter(role => FolderName.indexOf(role) === -1) || [] : filterFolder
    const duplicate = duplicateList && duplicateList.length ? duplicateList.filter(role => isExistsRole.indexOf(role) === -1) || [] : []
    const dupList = contextType === "SHAREDOCS" && sharingLevel === "Document" ? duplicate.filter(role => FolderName.indexOf(role) === -1) || [] : duplicate
    const viewFolderList = (searchText ? searchFolderList : folderPropList) || []
    const loading = <Loader />
    return (
      <div className="manage-documents">
        {documents.filter(o => o.docFileName).length ? <Prompt
          when={true}
          message='You have unsaved documents, are you sure you want to leave?'
        /> : null}
        {this.state.loading ? loading : null}
        <CreateNewFolderModal
          folderModal={folderModal}
          folderName={folderName}
          error={error}
          onCreateFolderModal={this.onCreateFolderModal}
          onFolderChange={this.onFolderChange}
          onFolderSave={this.onFolderSave}
        />
        <ShareDocumentsModal
          shareDocumentModal={shareDocumentModal}
          sharingLevel={sharingLevel}
          sharingType={sharingType}
          dropDown={dropDown}
          folderChange={folderChange}
          dupList={dupList}
          selectFrom={selectFrom}
          selectTo={selectTo}
          fileName={fileName}
          documentId={documentId}
          sendToUser={sendToUser}
          firmUser={firmUser}
          isDisable={isDisable}
          contextType={contextType || ""}
          folderPropList={folderPropList || []}
          onCreateFolderModal={this.onCreateFolderModal}
          onSharingDocChange={this.onSharingDocChange}
          onShareDocsSave={this.onShareDocsSave}
        />
        <Accordion
          multiple
          boxHidden
          isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [0, 1, 2]}
          render={({ activeAccordions, onAccordion }) => (
            <div style={{ paddingLeft: "0.25em", paddingRight: "0.25em" }}>
              { svControls && !svControls.edit && contextType === "SHAREDOCS" ? null :
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  title={title || "Upload Documents"}
                  actionButtons={this.actionButtons("documents", isDisabled)}
                >
                  {activeAccordions.includes(0) && (
                    <div style={{paddingLeft: "0.1em"}}>
                      <div className="columns">
                        <div className="column">
                          <div className="field has-addons">
                            <div>
                              <button className="button" name="folderModal" onClick={(e) => this.onCreateFolderModal(e)}>Create New Folder</button>
                            </div>
                            <div className="control">
                              <button
                                className="button"
                                name="folderModal" onClick={(e) => this.onCreateFolderModal(e)}
                                title="Create New Folder"
                              >
                                <a className="far fa-folder-open" name="folderModal" onClick={(e) => this.onCreateFolderModal(e)} style={{fontSize: 25}}/>
                              </button>
                            </div>
                          </div>
                        </div>
                        {chooseMultiple ?
                          <small style={{marginTop: 15}}>Select Folder</small> : null
                        }
                          {chooseMultiple ?
                            <div className="column">
                            <DropDownSelect
                              name="folderName"
                              data={contextType === "SHAREDOCS" ? duplicateList : dropDown.folderList || []}
                              value={folderName || ""}
                              placeholder="Choose Folder"
                              onChange={value => this.onFolderChange("multiFile", "folderName", value, "", )}
                            /></div>
                            : null
                          }
                          {
                            fileTypeError &&
                            <div className="column">
                              <small className="has-text-danger">{fileTypeError || ""}</small>
                            </div>
                          }
                        <div className="column is-half">
                          <div className="field is-grouped">
                            <div className="control is-pulled-right">
                              <button
                                className="button is-link"
                                onClick={this.toggleMultipleFile}
                              >
                                {!chooseMultiple ? "Choose Multiple File" : "Close Multiple File"}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                        <DragAndDropFileInState
                          bucketName={bucketName}
                          contextId={user.userId || contextId}
                          {...this.props}
                          loadDocuments={loadDocuments}
                          isSave={isSave}
                          saveFileInState={this.saveFileInState}
                          setStateParentLevel={this.setStateParentLevel}
                          contextType={contextType || ""}
                          tenantId={user.entityId}
                          folderId={folderId || ""}
                          audit="audit"
                          onSave={this.onSave}
                          docMeta={{
                            category: "Firm Documents",
                            subCategory: `Firm ${contextType === "USERSDOCS" ? "User" : "Shared "} Documents`,
                            type: "Firm Documents"
                          }}
                          chooseMultiple={chooseMultiple}
                        />
                        { !chooseMultiple ? (
                          <div className="tbl-scroll">
                            <table
                              id="my-table"
                              className="table is-bordered is-striped is-hoverable is-fullwidth"
                              style={tableStyle}
                            >
                              <TableHeader cols={cols[0]} />
                              {documents.map((doc, index) => {
                                const errors = errorMessages.documents && errorMessages.documents[index.toString()]
                                return (
                                  <DocumentsView
                                    contextType={contextType || ""}
                                    staticField={staticField}
                                    key={index.toString()}
                                    index={index}
                                    doc={doc}
                                    dropDown={dropDown}
                                    folderName={folderName}
                                    errors={errors}
                                    chooseMultiple={chooseMultiple}
                                    onChangeItem={(changedItem, change, name) => this.onChangeItem(changedItem, index, "documents", change, name)}
                                    onRemove={this.onRemove}
                                    onFolderChange={this.onFolderChange}
                                    duplicateList={duplicateList || []}
                                    folder
                                    onChangeError={this.onChangeError}
                                    clickUpload={this.clickUpload}
                                    onClickUpload={this.onClickUpload}
                                    onChangeUpload={this.onChangeUpload}
                                  />
                                )
                              })}
                            </table>
                          </div>
                        ) : null }

                        {!chooseMultiple ? (
                          <div className="field is-grouped-center">
                            <div className="control">
                              <button
                                className="button is-link"
                                onClick={this.updateSaveFlag}
                                disabled={isSaveDisabled || !documents.length|| false}
                              >
                                Save
                              </button>
                            </div>
                          </div>
                        ) : null}
                      </div>
                    </div>
                  )}
                </RatingSection>
              }
              <br />

              <div className="columns" style={{marginTop: "-35px"}}>
                {folderPropList && folderPropList.length ?
                  <div className="column is-one-quarter">
                    <RatingSection
                      onAccordion={() => onAccordion(1)}
                      title="Folder List"
                    >
                      {activeAccordions.includes(1) && (
                        <div style={{paddingLeft: "0.1em"}}>
                          <div className="columns">
                            <div className="column is-10">
                              <p className="control has-icons-left">
                                <input
                                  className="input is-small is-link"
                                  type="text"
                                  name="searchText"
                                  placeholder="Search Folder"
                                  onChange={(e) => this.onFolderSearch(e)}
                                />
                                <span className="icon is-left has-background-dark">
                                  <i className="fas fa-search" />
                                </span>
                              </p>
                            </div>
                            { svControls && !svControls.edit && contextType === "SHAREDOCS" ? null :
                              <div className="column" style={{fontSize: 24}}>
                                <a title="Share Folder" className="fas fa-share-alt" name="shareDocumentModal"
                                  onClick={(e) => this.onCreateFolderModal(e, "Folder")}/>
                              </div>
                            }
                          </div>
                          <div >
                            <div className="columns" style={{fontSize: 16}}>
                              <div className="column" style={{cursor: "pointer"}} onClick={() => this.onFolderSorting(sortBy || "dsc", "createdDate")}>
                                Created At
                                <a title="Ascending & Descending Order"  className="fas fa-sort" />
                              </div>
                              <div className="column" style={{ cursor: "pointer"}}  onClick={() => this.onFolderSorting(sortBy || "dsc", "updatedDate")} >
                                Updated At
                                <a title="Ascending & Descending Order"  className="fas fa-sort"/>
                              </div>
                            </div>
                            <div style={{maxHeight: 400, overflowX: "auto"}}>
                              {viewFolderList && viewFolderList.map((folder, i) => (
                                <div key={i.toString()}>
                                  <div>
                                    {/* <a
                                      className={`${dangerMode === folder._id ? "button is-danger is-rounded" : showDocuments && showDocuments.length && showDocuments[0].folderId === folder._id ?
                                        "button is-success is-rounded" : folder.folderName === "My Files Without Folder" || folder.folderName === "Share Files Without Folder" ? "button is-info is-rounded" : "button is-rounded"}`}
                                      style={{marginBottom: 5, width: "100%"}} title={folder.folderName || ""} onClick={() => this.showDocuments(folder)}>{folder.folderName}</a> */}
                                    <div
                                      style={{marginBottom: 5,
                                        width: "100%",
                                        whiteSpace: "nowrap",
                                        textOverflow: "ellipsis",
                                        overflow: "hidden",
                                        border: "1px solid",
                                        padding: "5px",
                                        textAlign: "center",
                                        cursor: "pointer",
                                        borderRadius: "290486px"}}
                                      title={folder.folderName || ""}
                                      className={`${dangerMode === folder._id ? "danger-folder" : showDocuments && showDocuments.length && showDocuments[0].folderId === folder._id ?
                                        "success-folder" : folder.folderName === "My Files Without Folder" || folder.folderName === "Share Files Without Folder" ? "static-folder" : "round-folder"}`}
                                      onClick={() => this.showDocuments(folder)}
                                    >
                                      {folder.folderName}
                                    </div>
                                  </div>
                                </div>
                              ))}
                            </div>
                          </div>
                        </div>
                      )}
                    </RatingSection>
                  </div> : null
                }
                <div className="column" >
                  <RatingSection
                    onAccordion={() => onAccordion(2)}
                    title={`${(showFolderName === ("My Files Without Folder" || "Share Files Without Folder")) ? `${showFolderName}` : `${showFolderName} Documents` } `}
                  >
                    {activeAccordions.includes(2) && (
                      <div style={{paddingLeft: "0.1em"}}>
                        <DocumentsListTable
                          documentsList={showDocuments}
                          svControls={svControls}
                          bucketName={this.state.bucketName}
                          isDisable={isDisable}
                          user={user}
                          contextType={contextType}
                          contextId={contextId}
                          onCreateFolderModal={this.onCreateFolderModal}
                          deleteDoc={this.deleteDoc}
                        />
                      </div>
                    )}
                  </RatingSection>
                </div>
              </div>

            </div>
          )}
        />
        <br />

        <a
          className="hidden-download-anchor"
          style={{ display: "none" }}
          ref={el => {
            this.downloadAnchor = el
          }}
          href={this.state.signedS3Url}
          target="_blank"
        />
        {this.state.showModal ? (
          <DocModalDetails
            showModal={this.state.showModal}
            closeDocDetails={this.handleDocDetails}
            documentId={doc.documentId}
            onDeleteAll={this.deleteDoc}
            docMetaToShow={["category", "subCategory"]}
            versionMetaToShow={["uploadedBy"]}
            docId={doc._id}
          />
        ) : null}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(Documents)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
