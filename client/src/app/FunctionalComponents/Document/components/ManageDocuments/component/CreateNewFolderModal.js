import React from "react"
import {Modal} from "../../../../../FunctionalComponents/TaskManagement/components/Modal"

export const CreateNewFolderModal = (props) => {

  return(
    <Modal
      closeModal={props.onCreateFolderModal}
      saveModal={props.onFolderSave}
      modalState={props.folderModal}
      styleBody={{overflow: "unset"}}
      modalWidth={{overflow: "unset", width: "25%"}}
      disabled={!props.folderName}
      title="Create New Folder"
    >
      <div>
        <input className="input is-rounded" type="text" placeholder="Enter Folder Name" name="folderName" value={props.folderName || ""} onChange={props.onFolderChange}/>
        <small className="has-text-danger">{props.error || ""}</small>
      </div>
    </Modal>
  )
}

export default CreateNewFolderModal
