import React from "react"
import Loader from "Global/Loader"
import MyDocuments from "./component/MyDocuments"
import SharedDocuments from "./component/SharedDocuments"
import Audit from "../../../../GlobalComponents/Audit"
import {withRouter} from "react-router-dom"
import connect from "react-redux/es/connect/connect"
import {getAuditLogByType} from "../../../../StateManagement/actions/audit_log_actions"
import {checkSupervisorControls} from "../../../../StateManagement/actions/CreateTransaction";


class Index extends React.Component {
  constructor() {
    super()
    this.state = {
      loading:true,
      tabActiveIndex: 0,
      auditLogs: [],
      svControls: {},
      TABS: [
        { label: "My Documents",row:1,Component: MyDocuments},
        { label: "Shared Documents",row:1,Component: SharedDocuments},
        { label: "Activity Log",row:1,Component: Audit},
      ]
    }
  }

  async componentWillMount() {
    const {TABS} = this.state
    const {user} = this.props
    const svControls = await checkSupervisorControls()
    const submitAudit = user && user.settings && user.settings.auditFlag === "no" ? false
      : user && user.settings && user.settings.auditFlag === "currentState" ? true
        : (user && user.settings && user.settings.auditFlag === "supervisor" && (svControls && svControls.supervisor)) || false
    if (!submitAudit) {
      const index = TABS.findIndex(tab => tab.label === "Activity Log")
      if(index !== -1){
        TABS.splice(index, 1)
      }
    }
    const {activeIndex} = this.props
    const {Component} = TABS[activeIndex || 0]
    this.setState({
      loading: false,
      TABS,
      tabActiveIndex:activeIndex || 0,
      SelectedComponent:Component,
      svControls
    })
  }

  identifyComponentToRender = (tab, ind) => {
    const { user } = this.props
    if(tab.label === "Activity Log"){
      let audit = []
      getAuditLogByType("documents", user.entityId, res => {
        audit = res && res.changeLog || []
      })
      getAuditLogByType("documents", user.userId, res => {
        this.setState({
          auditLogs: audit.length ? audit.concat(res && res.changeLog || []) : res && res.changeLog || [],
          SelectedComponent:tab.Component,
          tabActiveIndex:ind
        })
      })
    } else {
      this.setState({
        SelectedComponent:tab.Component,
        tabActiveIndex:ind
      })
    }
  }

  renderTabContents = () => {
    const { TABS, tabActiveIndex } = this.state;
    const tabActive = TABS[tabActiveIndex].label;
    return (
      <nav className="tabs is-boxed">
        <ul>
          {
            TABS.map((t, i) =>
              <li key={t.label} className={tabActive === t.label ? "is-active" : ""}
                  onClick={() => this.identifyComponentToRender(t, i)}>
                <a className="tabSecLevel">
                  <span>{t.label}</span>
                </a>
              </li>
            )
          }
        </ul>
      </nav>);
  }

  renderSelectedTabComponent = () => {
    const {SelectedComponent, auditLogs, svControls} = this.state
    return <SelectedComponent {...this.props} auditLogs={auditLogs} svControls={svControls}/>
  }


  render() {
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderTabContents()}
            </div>
          </div>
        </div>
        <section id="main" className="bankloan">
          {this.renderSelectedTabComponent()}
        </section>
      </div>
    )
  }
}

// export default Index

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default withRouter(connect(mapStateToProps, null)(Index))
