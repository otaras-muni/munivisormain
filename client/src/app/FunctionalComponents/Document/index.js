import React from "react"
import {NavLink} from "react-router-dom"
import ManageDocuments from "./components/ManageDocuments/index"
import {activeStyle} from "../../../globalutilities/consts"
import ContentDocs from "../ContentDocs"

class MainDocumentView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      TABS: [
        { path: "upload-doc", label: "Upload Documents" },
        { path: "manage-doc", label: "Manage Documents" }
      ]
    }
  }

  componentWillMount(){
    const { loginDetails } = this.props
    const loggedDetails = loginDetails && loginDetails.userEntities && loginDetails.userEntities.length && loginDetails.userEntities && loginDetails.userEntities[0]
    if (loggedDetails && loggedDetails.relationshipToTenant && loggedDetails.relationshipToTenant !== "Self"){
      this.setState({
        TABS: [
          { path: "upload-doc", label: "Upload Documents" }
        ]
      })
    }
    if(!this.props.nav2){
      this.props.history.push("/tools-docs/upload-doc")
    }
  }

  renderTabs = (tabs, nav2) =>
    tabs.map(t => (
      <li
        key={t.path}
        className={t.path === nav2 ? "is-active" : "inactive-tab"}
      >
        <NavLink to={`/tools-docs/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderViewSelection = nav2 => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.TABS, nav2)}</ul>
    </nav>
  )

  renderSelectedView = (nav2) => {
    switch (nav2) {
    case "manage-doc":
      return <ManageDocuments />
    case "upload-doc":
      return <ContentDocs />
    default:
      return <p>{nav2}</p>
    }
  }


  render(){
    const { nav2 } = this.props
    return (
      <div>
        <section className="hero is-small">
          <div className="hero-foot">
            <div className="container">{this.renderViewSelection(nav2)}</div>
          </div>
        </section>
        {this.renderSelectedView(nav2)}
      </div>
    )
  }

}

export default MainDocumentView
