import React, { Component } from "react"
import { connect } from "react-redux"
import { DropdownList, Multiselect } from "react-widgets"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import axios from "axios"
import { getToken } from "../../../globalutilities/helpers"

import { getPicklistByPicklistName, getDocDetails } from "GlobalUtils/helpers"
import CONST, { ContextType, muniApiBaseURL } from "GlobalUtils/consts"

import { DropDownInput } from "../../GlobalComponents/TextViewBox"
import {
  fetchAssigned,
  fetchIssuers,
  fetchUserTransactions
} from "../../StateManagement/actions/CreateTransaction"
import Loader from "../../GlobalComponents/Loader"
import GeneralDragAndDropFile from "./GeneralDragAndDropFile"
import { postGeneralDocs } from "../../StateManagement/actions/Common"
import {
  getEligibleEntitiesForLoggedInUser,
  getEligibleTransForLoggedInUser, getEligibleUsersForLoggedInUser
} from "../../StateManagement/actions/TaskManagement/taskManageActions"

class ContentDocs extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bucketName: process.env.S3BUCKET,
      document: {
        docCategory: "",
        docSubCategory: "",
        docType: "",
        contacts: [],
        projects: [],
        issuers: []
      },
      tempDocuments: {
        docCategory: "",
        docSubCategory: "",
        docType: "",
        contacts: [],
        projects: [],
        issuers: []
      },
      search: {},
      errors: {},
      dropDown: {
        category: [],
        docType: [],
        actions: [],
        docStatus: [],
        transactions: [],
        contacts: []
      },
      uploadedFiles: [],
      loading: true,
      chooseMultiple: false,
      waiting: false,
      actionExpanded: true,
      docReset: false
    }
    this.toggleActionAccordion = this.toggleActionAccordion.bind(this)
  }
  async componentDidMount() {
    const { user } = this.props
    // this.getTransactions()
    let result = await getPicklistByPicklistName([
      "LKUPDOCCATEGORIES",
      "LKUPDOCACTION",
      "LKUPDOCTYPE",
      // "LKUPDOCSTATUS"
    ])
    const multiSubCategory = (result[2] && result[2].LKUPDOCCATEGORIES) || {}
    result = (result.length && result[1]) || {}
    const relatedActivityList = await getEligibleTransForLoggedInUser();
    const relatedEntityList = await getEligibleEntitiesForLoggedInUser();
    const relatedContactList = await getEligibleUsersForLoggedInUser();
    /*fetchAssigned(this.props.user.entityId, contacts => {
      fetchIssuers(`${this.props.user.entityId}?type=Third Party`, res => {*/
        this.setState({
          dropDown: {
            ...this.state.dropDown,
            contacts: relatedContactList || [],
            issuerList: relatedEntityList || [],
            transactions: relatedActivityList || [],
            category: result.LKUPDOCCATEGORIES || [],
            multiSubCategory,
            docType: result.LKUPDOCTYPE || [],
            actions: result.LKUPDOCACTION || [],
            // docStatus: result.LKUPDOCSTATUS || []
          },
          userName:
            (user && `${user.userFirstName} ${user.userLastName}`) || "",
          loading: false
        })
     /* })
    })*/
  }

  // getTransactions = async () => {
  //   const res = await fetchUserTransactions(this.props.user.entityId)
  //   let transactions = []
  //   if (res.transactions && Array.isArray(res.transactions)) {
  //     transactions = res.transactions.map(tran => ({
  //       ...tran,
  //       name:
  //         tran.rfpTranProjectDescription ||
  //         tran.dealIssueTranProjectDescription ||
  //         tran.actTranProjectDescription ||
  //         tran.actProjectName ||
  //         "",
  //       id: tran._id, // eslint-disable-line
  //       group: tran.actTranType
  //         ? tran.actTranType
  //         : tran.actType
  //           ? tran.actType
  //           : tran.rfpTranType === "RFP"
  //             ? tran.rfpTranType
  //             : tran.dealIssueTranSubType === "Bond Issue"
  //               ? "Deal"
  //               : ""
  //     }))
  //   }
  //   this.setState({
  //     dropDown: {
  //       ...this.state.dropDown,
  //       transactions
  //     }
  //   })
  // };

  toggleActionAccordion() {
    this.setState(prevState => ({ actionExpanded: !prevState.actionExpanded }))
  }

  onChangeSelect = (name, value) => {
    const { document } = this.state
    if (name === "docCategory") {
      document.docSubCategory = ""
    }
    this.setState({
      document: {
        ...document,
        [name]: value
      }
    })
  };

  onSearchSelect = (name, value) => {
    const { search } = this.state
    if (name === "docCategory") {
      search.docSubCategory = ""
    }
    this.setState({
      search: {
        ...search,
        [name]: value
      }
    })
  };

  onMultipleFileUpload = uploadedFiles => {
    console.log(uploadedFiles)
    uploadedFiles = this.state.uploadedFiles.concat(uploadedFiles)
    this.setState(
      {
        uploadedFiles,
        chooseMultiple: false
      },
      () => this.onSave()
    )
  };

  getBidBucket = (filename, docId) => {
    const { uploadedFiles } = this.state
    uploadedFiles.push({ fileId: docId, fileName: filename })
    this.setState({
      uploadedFiles
    })
  };

  toggleMultipleFile = () => {
    this.setState(prevState => ({
      chooseMultiple: !prevState.chooseMultiple
    }))
  };

  getDocInfo = async _id => {
    const res = await getDocDetails(_id)
    console.log("res : ", res)
    const [error, doc] = res
    if (error) {
      this.setState({ waiting: false, error })
    } else {
      const { contextId, contextType, tenantId, name } = doc
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      return { doc, error: "", objectName, waiting: false, fileName: name }
    }
  };

  deleteDoc = async (file, index) => {
    const docInfo = await this.getDocInfo(file.fileId)
    const { doc, objectName, fileName } = docInfo
    const { contextId, contextType, tenantId } = doc
    if (fileName && objectName) {
      const bucketName = contextType || "DEALS"
      const versionId = "All"
      if ((!doc && !doc.name) || !versionId) {
        this.setState({
          error: "No bucket/file name/versionId provided",
          deleteVersion: ""
        })
        return
      }
      this.setState(
        {
          waiting: true
        },
        () => {
          const files = doc.meta.versions.map(e => ({
            Key: `${tenantId}/${contextType}/${contextType}_${contextId}/${
              e.name
            }`,
            VersionId: e.versionId
          }))
          axios
            .post(
              `${muniApiBaseURL}s3/delete-s3-object`,
              {
                bucketName,
                files
              },
              { headers: { Authorization: getToken() } }
            )
            .then(res => {
              console.log("res : ", res)
              if (res.error) {
                const error = `Error in deleting ${versionId}`
                this.setState({ error, deleteVersion: "", waiting: false })
              } else {
                axios
                  .delete(`${muniApiBaseURL}docs/${doc._id}`,{headers:{"Authorization":getToken()}})
                  .then(res => {
                    console.log("document removed ok : ", res)
                    const { uploadedFiles } = this.state
                    uploadedFiles.splice(index, 1)
                    this.setState({
                      uploadedFiles,
                      waiting: false
                    })
                  })
                  .catch(err => {
                    console.log("err in deleting document : ", err)
                    this.setState({
                      error: "err in deleting document",
                      waiting: false
                    })
                  })
              }
            })
            .catch(err => {
              console.log("err : ", err)
              this.setState({
                error: "Error!!",
                deleteVersion: "",
                waiting: false
              })
            })
        }
      )
    }
  };

  onSave = async () => {
    const { uploadedFiles } = this.state
    const document = cloneDeep(this.state.document)
    const { docCategory, docSubCategory, docType } = document
    const createDocuments = {}
    const type = document.contacts.length
      ? "individual"
      : document.projects.length
        ? "transaction"
        : document.issuers.length
          ? "firm"
          : ""
    const documents =
      type === "individual"
        ? document.contacts || []
        : type === "transaction"
          ? document.projects || []
          : type === "firm"
            ? document.issuers || []
            : []

    if (type) {
      uploadedFiles.forEach(file => {
        documents.forEach(dd => {
          if (dd.id === file.contextId && file.fileId) {
            if (createDocuments.hasOwnProperty(file.contextId)) {
              createDocuments[file.contextId].push({
                docCategory: docCategory || "N/A",
                docSubCategory: docSubCategory || "N/A",
                docType: docType || "N/A",
                docFileName: file.fileNeme,
                docAWSFileLocation: file.fileId,
                type: dd.type || ""
              })
            } else {
              createDocuments[file.contextId] = [
                {
                  docCategory: docCategory || "N/A",
                  docSubCategory: docSubCategory || "N/A",
                  docType: docType || "N/A",
                  docFileName: file.fileNeme,
                  docAWSFileLocation: file.fileId,
                  type: dd.type || ""
                }
              ]
            }
          }
        })
      })
      console.log({ documents: createDocuments, type })

      this.setState(
        {
          waiting: true
        },
        async () => {
          if (createDocuments && Object.keys(createDocuments).length) {
            const response = await postGeneralDocs({
              documents: createDocuments,
              type
            })
            if (response && response.status === 200) {
              toast("Documents added successfully", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.setState({
                document: {
                  docCategory: "",
                  docSubCategory: "",
                  docType: "",
                  contacts: [],
                  projects: [],
                  issuers: []
                },
                errors: {},
                uploadedFiles: [],
                waiting: false
              })
            } else {
              toast(
                (response && response.data && response.data.error) ||
                  "something went wrong",
                { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR }
              )
              this.setState({
                waiting: false
              })
            }
          } else {
            this.setState({
              document: {
                docCategory: "",
                docSubCategory: "",
                docType: "",
                contacts: [],
                projects: [],
                issuers: []
              },
              errors: {},
              uploadedFiles: [],
              waiting: false
            })
          }
        }
      )
    }
  };

  onMultipleSelect = (key, items) => {
    const { user } = this.props
    if (key === "projects") {
      // const tranType = ["Deals","RFP","ActMaRFP","Derivatives","BankLoans","Others", "ActBusDev"]
      items = items.map(item => {
        const tranType = (item && item.tranAttributes && item.tranAttributes.type) || ""
        const subType = (item && item.tranAttributes && item.tranAttributes.subType) || ""
        const type = item
          ? subType === "Bond Issue"
            ? "Deals"
            : tranType === "MA-RFP"
              ? "ActMaRFP"
              :  subType === "RFP"
                ? "RFP"
                : tranType === "Derivative"
                  ? "Derivatives"
                  : subType === "Bank Loan"
                    ? "BankLoans"
                    : tranType === "Others"
                      ? "Others"
                      : tranType === "BusinessDevelopment"
                        ? "ActBusDev"
                        : ""
          : ""
        const contextType = item
          ? subType === "Bond Issue"
            ? ContextType.deal
            : tranType === "MA-RFP"
              ? ContextType.maRfp
              : subType === "RFP"
                ? ContextType.rfp
                : tranType === "Derivative"
                  ? ContextType.derivative
                  : subType === "Bank Loan"
                    ? ContextType.loan
                    : tranType === "Others"
                      ? ContextType.others
                      : tranType === "BusinessDevelopment"
                        ? ContextType.busDev
                        : ""
          : ""

        return {
          id: item.id,
          name: item.name,
          type,
          contextId: item.id,
          contextType,
          tenantId: (user && user.entityId) || ""
        }
      })
    } else {
      items = items.map(item => ({
        id: item.id,
        name: item.name,
        contextId: item.id,
        contextType: key === "issuers" ? ContextType.entity : ContextType.user,
        tenantId: (user && user.entityId) || ""
      }))
    }
    this.setState(prevState => ({
      document: {
        ...prevState.document,
        [key]: items
      }
    }))
  }

  onReset = () => {
    this.setState(prevState => ({
      document: cloneDeep(prevState.tempDocuments),
      docReset: true
    }))
  }

  onClearDocFile = () => {
    this.setState({
      docReset: false
    })
  }

  render() {
    const {
      loading,
      waiting,
      bucketName,
      document,
      search,
      errors,
      dropDown,
      uploadedFiles,
      actionExpanded,
      docReset
    } = this.state
    const { user } = this.props
    const type = document.contacts.length
      ? "individual"
      : document.projects.length
        ? "transaction"
        : document.issuers.length
          ? "firm"
          : ""

    const chooseMultiple =
      !!document.contacts.length ||
      !!document.projects.length ||
      !!document.issuers.length ||
      false
    if (loading) {
      return <Loader />
    }
    return (
      <div id="main">
        <section>
          <div className="top-bottom-margin">
            <section className="accordions">
              <article
                className={
                  this.state.actionExpanded ? " accordion is-active" : "accordion"
                }
              >
                <div
                  className="accordion-header toggle"
                  onClick={this.toggleActionAccordion}
                >
                  <p>Add Documents</p>
                  <i className={actionExpanded ? "fas fa-chevron-up" : "fas fa-chevron-down"}/>
                  <br/>
                </div>
                <div className="accordion-body acco-hack">
                  <div className="accordion-content">
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <thead>
                        <tr>
                          <th>Category</th>
                          <th>Sub Category</th>
                          <th>Type</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <DropDownInput
                              filter
                              // label="Category"
                              data={dropDown.category || []}
                              value={document.docCategory || ""}
                              style={{ width: "100%", fontSize: 12 }}
                              onChange={value =>
                                this.onChangeSelect("docCategory", value)
                              }
                            />
                            {errors && errors.docCategory && (
                              <small className="text-error">
                                {errors.docCategory}
                              </small>
                            )}
                          </td>
                          <td>
                            <div
                              style={{
                                fontSize: 12,
                                paddingLeft: "13px",
                                paddingRight: "13px"
                              }}
                            >
                              <DropdownList
                                filter="contains"
                                data={
                                  document.docCategory
                                    ? dropDown.multiSubCategory[
                                      document.docCategory
                                    ]
                                    : []
                                }
                                value={document.docSubCategory || ""}
                                onChange={value =>
                                  this.onChangeSelect("docSubCategory", value)
                                }
                              />
                              {errors && errors.docSubCategory && (
                                <small className="text-error">
                                  {errors.docSubCategory}
                                </small>
                              )}
                            </div>
                          </td>
                          <td>
                            <DropDownInput
                              filter
                              // label="Type"
                              data={dropDown.docType || []}
                              value={document.docType || ""}
                              style={{ width: "100%", fontSize: 12 }}
                              onChange={value =>
                                this.onChangeSelect("docType", value)
                              }
                            />
                            {errors.docType ? (
                              <p className="text-error">{errors.docType}</p>
                            ) : null}
                          </td>
                          <td>
                            <div className="control">
                              <button
                                className="button is-dark is-small"
                                style={{ marginLeft: "40%" }}
                                onClick={this.onReset}
                              >
                            Reset
                              </button>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                    <div className="columns">
                      <div className="column is-one-third">
                        <p className="multiExpLbl ">
                      Link the document to a firm(s)?
                        </p>
                      </div>
                      <div className="column is-6">
                        <div className="control has-icons-left">
                          <div style={{ fontSize: 12 }}>
                            <Multiselect
                              filter
                              groupBy="group"
                              data={dropDown.issuerList || []}
                              value={document.issuers || []}
                              onChange={items =>
                                this.onMultipleSelect("issuers", items)
                              }
                              style={{ width: "100%" }}
                              disabled={
                                !!document.contacts.length ||
                            !!document.projects.length
                              }
                              textField="name"
                              valueField="id"
                              placeholder="search firm name (multi-select)"
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    {/*<div className="columns">
                      <div className="column is-one-third">
                        <p className="multiExpLbl ">
                      Link the document to an individual(s)?
                        </p>
                      </div>
                      <div className="column is-6">
                        <div
                          className="control has-icons-left"
                          style={{ fontSize: 12 }}
                        >
                          <Multiselect
                            filter
                            data={dropDown.contacts}
                            value={document.contacts || []}
                            onChange={items =>
                              this.onMultipleSelect("contacts", items)
                            }
                            style={{ width: "100%" }}
                            disabled={
                              !!document.issuers.length ||
                          !!document.projects.length
                            }
                            placeholder="search contacts (multi-select)"
                            textField="name"
                            valueField="id"
                          />
                        </div>
                      </div>
                    </div>*/}

                    <div className="columns">
                      <div className="column is-one-third">
                        <p className="multiExpLbl ">
                      Link the document to a transaction(s)?
                        </p>
                      </div>
                      <div className="column is-6">
                        <div
                          className="control has-icons-left"
                          style={{ fontSize: 12 }}
                        >
                          <Multiselect
                            filter
                            groupBy="group"
                            data={dropDown.transactions}
                            value={document.projects || []}
                            onChange={items =>
                              this.onMultipleSelect("projects", items)
                            }
                            style={{ width: "100%" }}
                            placeholder="search transaction name (multi-select)"
                            textField="name"
                            valueField="id"
                            disabled={
                              !!document.issuers.length ||
                          !!document.contacts.length ||
                          false
                            }
                          />
                        </div>
                      </div>
                    </div>
                    <GeneralDragAndDropFile
                      bucketName={bucketName}
                      contextId={user.userId || ""}
                      docReset={docReset}
                      contextType={ContextType.generalDoc || "GENERALDOCS"}
                      tenantId={(user && user.entityId) || ""}
                      onMultipleFileUpload={this.onMultipleFileUpload}
                      onReset={this.onReset}
                      onClearDocFile={this.onClearDocFile}
                      documentInfo={{ ...document, type }}
                      disabled={!chooseMultiple}
                      docMeta={{
                        category: document.docCategory,
                        subCategory: document.docSubCategory,
                        type: document.docType
                      }}
                    />
                  </div>
                </div>
              </article>
            </section>
          </div>
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(
  mapStateToProps,
  null
)(ContentDocs)
