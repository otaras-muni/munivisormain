import React, { Component } from "react"
import Dropzone from "react-dropzone"
import qs from "qs"
import fileSize from "file-size"
import moment from "moment"
import axios from "axios"
import mime from "mime"
import {connect} from "react-redux"
import { getToken, checkInvalidFiles } from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts"

import { Modal } from "../../FunctionalComponents/TaskManagement/components/Modal"
import Loader from "../../GlobalComponents/Loader"
import withAuditLogs from "../../GlobalComponents/withAuditLogs"

class GeneralDragAndDropFile extends Component {
  constructor(props) {
    super(props)

    this.state = {
      files: [],
      uploadedFiles: [],
      conflictDocId: [],
      newVersion: [],
      fileTypeError: "",
      showModal: false,
      inProgress: false
    }
    this.uploadedFiles = []
  }

  componentWillReceiveProps(nextProps){
    const {docReset} = nextProps
    if(this.props.docReset !== docReset && docReset){
      this.setState({
        files: []
      },this.props.onClearDocFile())
    }
  }

  onHandleDrop = files => {
    const invalidFile = checkInvalidFiles(files, "multi")
    if(invalidFile){
      this.setState({
        fileTypeError: invalidFile || ""
      })
      return
    }
    this.setState({
      fileTypeError: "",
      conflictDocId: [],
      files
    })
  }

  onFileUpload = async () => {
    this.setState({ inProgress: true })
    const { files, conflictDocId, newVersion } = this.state
    const { documentInfo, user } = this.props

    const { docCategory, docSubCategory, docType } = documentInfo
    const documents =
      documentInfo.type === "individual"
        ? documentInfo.contacts || []
        : documentInfo.type === "transaction"
          ? documentInfo.projects || []
          : documentInfo.type === "firm"
            ? documentInfo.issuers || []
            : []
    console.log("files : ", files)
    if (files && Array.isArray(files) && files.length) {
      for (const d in documents) {
        const { bucketName } = this.props
        const { contextId, contextType, tenantId } = documents[d]

        for (const i in files) {
          const file = files[i]
          let fileName = file.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")
          const tags = {
            docCategory:
              (docCategory &&
                docCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
              "",
            docSubCategory:
              (docSubCategory &&
                docSubCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
              "",
            docType:
              (docType && docType.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
              "",
            docContext:
              (contextType &&
                contextType.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
              "",
            docName: (fileName && fileName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            uploadUserName:
              (user && `${user.userFirstName} ${user.userLastName}`) || "",
            uploadDate: moment(new Date()).format("MM-DD-YYYY")
          }
          const extnIdx = fileName.lastIndexOf(".")
          if (extnIdx > -1) {
            fileName = `${fileName.substr(
              0,
              extnIdx
            )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
          }
          console.log("fileName2 : ", fileName)
          const contentType = file.type
          if (!fileName) {
            return this.setState({
              error: "No file name provided",
              inProgress: false
            })
          }
          let filePath
          if (tenantId && contextId && contextType) {
            filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
          }
          const opType = "upload"
          const options = { contentType, tags }

          const res = await axios.post(`${muniApiBaseURL}/s3/get-signed-url`, {
            opType,
            bucketName,
            fileName: filePath,
            options
          },{headers:{"Authorization":getToken()}})
          if (res && res.status === 200) {
            console.log("res : ", res)

            const findConflictFile = conflictDocId.find(doc =>
              doc.contextId === contextId &&
              doc.contextType === contextType &&
              doc.tenantId === tenantId &&
              doc.originalName === file.name
            )

            const document = findConflictFile && newVersion && newVersion.indexOf(findConflictFile._id) !== -1 ? findConflictFile : documents[d]

            this.uploadWithSignedUrl(
              bucketName,
              res.data.url,
              file,
              fileName,
              tags,
              document,
              upFiles => {
                // console.log("================================")
                console.log(
                  files.length * documents.length,
                  upFiles.uploadedFiles.length
                )
                if (
                  upFiles &&
                  upFiles.uploadedFiles &&
                  upFiles.uploadedFiles.length ===
                  parseInt(files.length * documents.length, 10)
                ) {
                  files &&
                  files.forEach(item => {
                    this.props.addAuditLog({
                      userName: this.state.userName,
                      log: `Documents upload ${item.name}`,
                      date: new Date(),
                      key: "documents"
                    })
                  })

                  console.log("*******conflictDocId*****", conflictDocId)
                  console.log("*******findConflictFile*****", findConflictFile)
                  const conflictDocIds = conflictDocId.map(d => d._id)
                  const docs = upFiles.uploadedFiles.filter(upFile => conflictDocIds.indexOf(upFile.docAWSFileLocation) === -1)

                  this.props.onMultipleFileUpload(docs || [])
                  this.setState({
                    files: [],
                    error: "",
                    message: "",
                    newVersion: [],
                    conflictDocId: [],
                    inProgress: false
                  })
                  this.uploadedFiles = []
                }
              }
            )
          } else {
            console.log("err : ", err)
            this.setState({
              error: "Error in getting signed url for upload",
              message: "",
              inProgress: false
            })
          }
        }
      }
    } else {
      console.log("No file")
      this.setState({ error: "No file", inProgress: false })
    }
  }

  checkOnS3File = () => {
    this.setState({ inProgress: true })
    const { files } = this.state
    let { conflictDocId } = this.state
    const { documentInfo } = this.props

    const documents =
      documentInfo.type === "individual"
        ? documentInfo.contacts || []
        : documentInfo.type === "transaction"
          ? documentInfo.projects || []
          : documentInfo.type === "firm"
            ? documentInfo.issuers || []
            : []

    if (files && Array.isArray(files) && files.length) {
      for (const d in documents) {
        const { contextId, contextType, tenantId } = documents[d]
        for (const i in files) {
          const file = files[i]
          const fileName = file.name

          if (!fileName) {
            return this.setState({
              error: "No file name provided",
              inProgress: false
            })
          }
          let filePath
          if (tenantId && contextId && contextType) {
            filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
          }
          const docFilter = {
            contextId,
            contextType,
            tenantId,
            originalName: fileName
          }
          axios
            .get(`${muniApiBaseURL}/docs`, { params: { ...docFilter },headers:{"Authorization":getToken()} })
            .then(async (res) => {
              const doc = (res && res.data[0]) || {}
              const { _id } = await doc
              if (_id) {
                conflictDocId.push(doc)
              }
              if (((parseInt(d,10)+1)*(parseInt(i,10)+1)) === parseInt(files.length * documents.length, 10)) {
                if(conflictDocId && conflictDocId.length){
                  this.setState({
                    conflictDocId,
                    inProgress: false,
                    showModal: true
                  })
                }else {
                  if(!this.state.showModal){
                    this.onFileUpload()
                  }
                }
              }
            })
        }
      }
    } else {
      console.log("No file")
      this.setState({ error: "No file", inProgress: false })
    }
  }

  uploadWithSignedUrl = (
    bucketName,
    signedS3Url,
    file,
    fileName,
    tags,
    document,
    callback
  ) => {
    console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      bucketName,
      fileName,
      file.type
    )
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("no tagging")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = async () => {
      console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        console.log("file uploaded ok")
        // this.setState({ error: "", message: `Last upload successful for "${fileName}" at ${new Date()}` })
        if(document && document.meta){
          this.updateVersionDocsDB(
            bucketName,
            file,
            fileName,
            document,
            fileCallback => {
              callback({
                error: "",
                message: `Last upload successful for "${fileName}" at ${new Date()}`,
                ...fileCallback
              })
            }
          )
        }else {
          this.updateDocsDB(
            bucketName,
            file,
            fileName,
            document,
            fileCallback => {
              callback({
                error: "",
                message: `Last upload successful for "${fileName}" at ${new Date()}`,
                ...fileCallback
              })
            }
          )
        }
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded :", err)
      this.setState({
        message: "",
        error: "Error in uploading to S3",
        inProgress: false
      })
    }
    xhr.send(file)
  }

  updateDocsDB = (bucketName, file, fileName, document, cb) => {
    const { docMeta, versionMeta, user } = this.props
    const { contextId, contextType, tenantId, name } = document
    const meta = { ...docMeta }
    let filePath
    if (tenantId && contextType && contextId) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    console.log("bucketName, fileName : ", bucketName, fileName)
    axios
      .post(`${muniApiBaseURL}/s3/get-s3-object-versions`, {
        bucketName,
        fileName: filePath
      },{headers:{"Authorization":getToken()}})
      .then(res => {
        if (res.data.error) {
          console.log("err in getting version : ", res.error)
          this.setState({
            message: "",
            error: "Error in getting S3 versions",
            inProgress: false
          })
        } else {
          let versionId
          let uploadDate
          let size
          res.data.result.Versions.some(v => {
            if (v.IsLatest) {
              versionId = v.VersionId
              uploadDate = v.LastModified
              size = fileSize(v.Size).human("jedec")
              return true
            }
          })
          if (versionId) {
            meta.versions = [
              {
                versionId,
                name: fileName,
                originalName: file.name,
                uploadDate: moment(new Date()).format("MM-DD-YYYY"),
                uploadedBy: (user && `${user.userFirstName} ${user.userLastName}`) || "",
                size: file.size,
                ...versionMeta
              }
            ]
            const doc = {
              name: fileName,
              originalName: file.name,
              meta,
              contextType,
              contextId,
              tenantId
            }
            axios
              .post(`${muniApiBaseURL}/docs`, doc,{headers:{"Authorization":getToken()}})
              .then(res => {
                console.log("inserted ok in docs db : ", res)
                this.uploadedFiles.push({
                  fileNeme: file.name,
                  fileId: res.data._id,
                  tenantId,
                  contextId,
                  contextType,
                  name,
                  ...res.meta
                })
                cb({
                  message: "",
                  error: "",
                  inProgress: false,
                  uploadedFiles: this.uploadedFiles
                })
              })
              .catch(err => {
                console.log("err in inserting in docs db : ", err)
                this.setState({
                  message: "",
                  error: "Error in inserting in docs DB",
                  inProgress: false
                })
              })
          } else {
            console.log("No version error")
            this.setState({
              message: "",
              error: "S3 version error",
              inProgress: false
            })
          }
        }
      })
      .catch(err => {
        console.log("err in getting version : ", err)
        this.setState({
          message: "",
          error: "Error in getting S3 versions",
          inProgress: false
        })
      })
  }

  updateVersionDocsDB = ( bucketName, file, fileName, document, cb) => {
    const type = mime.getExtension(file.type)
    const { _id, meta, contextId, contextType, tenantId } = document

    let filePath
    if (tenantId && contextType && contextId) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const docId = _id || ""

    axios
      .post(`${muniApiBaseURL}/s3/get-s3-object-versions`, {
        bucketName,
        fileName: filePath
      })
      .then(res => {
        if (res.data.error) {
          console.log("err in getting version : ", res.error)
          this.setState({
            message: "",
            error: "Error in getting S3 versions",
            inProgress: false
          })
        } else {
          console.log("got versions : ", res)
          // let latestVersion = res.Versions.filter(v => v.IsLatest)[0];
          let versionId
          let uploadDate
          let size
          res.data.result.Versions.some(v => {
            if (v.IsLatest) {
              versionId = v.VersionId
              uploadDate = v.LastModified
              size = fileSize(v.Size).human("jedec")
              return true
            }
          })
          if (versionId) {
            if (docId) {
              axios
                .get(`${muniApiBaseURL}/docs`, { params: { _id: docId },headers:{"Authorization":getToken()} })
                .then(res => {
                  console.log("res : ", res)
                  const doc = (res && res.data[0]) || {}
                  const { _id } = doc
                  if (_id) {
                    axios
                      .post(`${muniApiBaseURL}/docs/update-versions`, {
                        _id,
                        versions: [
                          {
                            versionId,
                            name: fileName,
                            originalName: file.name,
                            uploadDate,
                            size,
                            type,
                          }
                        ],
                        name: fileName,
                        originalName: file.name,
                        option: "add"
                      },{headers:{"Authorization":getToken()}})
                      .then(res => {
                        console.log("updated versions ok in docs db : ", res)
                        this.setState({
                          message: "",
                          error: "",
                          inProgress: false
                        })
                        this.uploadedFiles.push({
                          fileNeme: file.name,
                          fileId: res.data._id,
                          tenantId,
                          contextId,
                          contextType,
                          ...res.meta
                        })
                        cb({
                          message: "",
                          error: "",
                          inProgress: false,
                          uploadedFiles: this.uploadedFiles
                        })
                      })
                      .catch(err => {
                        console.log(
                          "err in updating versions in docs db : ",
                          err
                        )
                        this.setState({
                          message: "",
                          error: "Error in updating versions in docs DB",
                          inProgress: false
                        })
                      })
                  } else {
                    meta.versions = [
                      {
                        versionId,
                        name: fileName,
                        originalName: file.name,
                        uploadDate,
                        size,
                        type,
                      }
                    ]
                    const doc = {
                      name: fileName,
                      originalName: file.name,
                      meta,
                      contextType,
                      contextId,
                      tenantId
                    }
                    axios
                      .post(`${muniApiBaseURL}/docs`, doc,{headers:{"Authorization":getToken()}})
                      .then(res => {
                        console.log("inserted ok in docs db : ", res)
                        this.setState({
                          message: "",
                          error: "",
                          inProgress: false
                        })
                        this.uploadedFiles.push({
                          fileNeme: file.name,
                          fileId: res.data._id,
                          tenantId,
                          contextId,
                          contextType,
                          ...res.meta
                        })
                        cb({
                          message: "",
                          error: "",
                          inProgress: false,
                          uploadedFiles: this.uploadedFiles
                        })
                      })
                      .catch(err => {
                        console.log("err in inserting in docs db : ", err)
                        this.setState({
                          message: "",
                          error: "Error in inserting in docs DB",
                          inProgress: false
                        })
                      })
                  }
                })
                .catch(err => {
                  console.log("err in getting docs : ", err)
                  this.setState({
                    message: "",
                    error: "Error in getting docs",
                    inProgress: false
                  })
                })
            }
          } else {
            console.log("No version error")
            this.setState({
              message: "",
              error: "S3 version error",
              inProgress: false
            })
          }
        }
      })
      .catch(err => {
        console.log("err in getting version : ", err)
        this.setState({
          message: "",
          error: "Error in getting S3 versions",
          inProgress: false
        })
      })
  }

  handleCheckClick = (event) => {
    const {newVersion} = this.state

    if(newVersion.indexOf(event.target.value) !== -1){
      const finIndex = newVersion.indexOf(event.target.value)
      newVersion.splice(finIndex, 1)
    }else {
      newVersion.push(event.target.value)
    }

    this.setState({
      newVersion
    })
  }

  toggleModal = () => {
    this.setState({
      newVersion: [],
      conflictDocId: [],
      showModal: false,
    })
  }

  onSave = () => {
    this.setState({
      showModal: false,
    },() => this.onFileUpload())
  }

  render() {
    const { error, files, showModal, conflictDocId, inProgress, newVersion, fileTypeError } = this.state
    const { disabled, documentInfo } = this.props
    const docsInfo =
      documentInfo.type === "individual"
        ? documentInfo.contacts || []
        : documentInfo.type === "transaction"
          ? documentInfo.projects || []
          : documentInfo.type === "firm"
            ? documentInfo.issuers || []
            : []

    const styles = {
      width: "100%",
      height: 100,
      border: "2px dashed #ccc",
      padding: 35
    }

    if (inProgress) {
      return <Loader />
    }

    return (
      <section>
        <Modal
          closeModal={this.toggleModal}
          modalState={showModal}
          saveModal={this.onSave}
          title="New file or new version ?"
        >
          <p className="multiExpGrpLbl">
            We have found an existing file with the same name and upload options
            selected by you.
          </p>
          <p className="multiExpGrpLbl">
            Please confirm if you want to upload a new version to the existing
            file or upload as a separate new file?
          </p>

          {
            conflictDocId ?
              conflictDocId.map((doc, index) => {
                const  context = docsInfo.find(info => info.id === doc.contextId)
                return (
                  <div className="columns" key={index.toString()}>
                    <div className="column">
                      <p className="multiExpLblBlk">{`${doc.originalName} (${(context && context.name) || ""})`} </p>
                    </div>
                    <div className="column" >
                      <label className="radio-button">
                        New Version
                        <input
                          type="radio"
                          value={doc._id}
                          checked={newVersion.indexOf(doc._id) !== -1}
                          onChange={this.handleCheckClick}
                        />
                        <span className="checkmark" />
                      </label>
                    </div>
                    <div className="column" >
                      <label className="radio-button">
                        New File
                        <input
                          type="radio"
                          value={doc._id}
                          checked={newVersion.indexOf(doc._id) === -1}
                          onChange={this.handleCheckClick}
                        />
                        <span className="checkmark" />
                      </label>
                    </div>
                  </div>
                )
              }) : null
          }
        </Modal>
        {
          !disabled ?
            <div className="dropzone">
              <Dropzone onDrop={this.onHandleDrop} style={styles}>
                <p style={{ textAlign: "center" }}>
                  Try dropping some files here, or click to select files to upload.
                </p>
              </Dropzone>
            </div>
            : null
        }
        <p className="is-small text-error has-text-danger">{error || ""}</p>
        <br/>
        {
          files && files.length ?
            <aside>
              <h2 className="multiExpLblBlk">Uploaded files</h2>
              <ul>
                {files.map(f => <li key={f.name}>{f.name}</li>)}
              </ul>
            </aside>
            : null
        }
        { fileTypeError ? <small className="has-text-danger">{fileTypeError || ""}</small> : null }
        <br/>
        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped-center">
              <div className="control">
                <button
                  className="button is-link"
                  onClick={this.checkOnS3File}
                  disabled={disabled || !files.length || fileTypeError}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(GeneralDragAndDropFile)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
