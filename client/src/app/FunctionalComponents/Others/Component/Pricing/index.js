import React from "react"
import swal from "sweetalert"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import { connect } from "react-redux"
import { getPicklistByPicklistName, checkEmptyElObject } from "GlobalUtils/helpers"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import Accordion from "../../../../GlobalComponents/Accordion"
import CONST from "../../../../../globalutilities/consts"
import Series from "./components/Series"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import {
  putSeriesDetails,
  fetchOthersTransaction,
  putOthersTransaction,
  fetchSeriesDetailsBySeriesId,
  removeOtherSeries,
  putOthersPricingDetails
} from "../../../../StateManagement/actions/Transaction/others"
import Tabs from "../../../../GlobalComponents/Tabs"
import PriceCoordinate from "./components/PriceCoordinate"
import PricingData from "./components/PricingData"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import { PricingDetailsValidate } from "../../../Others/Component/Validation/PricingDeatailsValidation"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import DocExcelReader from "../../../../GlobalComponents/DocExcelReader"

class Pricing extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cols: [
        [
          { name: "Series Code<span class=\"icon has-text-danger\"><i class=\"fas fa-asterisk extra-small-icon\" /></span>" },
          { name: "Description" },
          { name: "Tag" },
          { name: "Edit/Delete" }
        ],
        [
          { name: "Term" },
          { name: "Maturity Date" },
          { name: "Amount" },
          { name: "Coupon" },
          { name: "Yield" },
          { name: "Price" },
          { name: "YTM" },
          { name: "CUSIP" },
          { name: "Call Date" },
          { name: "Takedown" },
          { name: "Insured" },
          { name: "Drop" },
          { name: "Action" }
        ]
      ],
      userName: "",
      actTranSeriesDetails: [CONST.actTranSeriesDetails],
      errorMessages: {},
      seriesPricingDetails: CONST.OtherSeriesPricingDetails,
      seriesPricingData: [CONST.seriesPricingData],
      otherSeriesList: [],
      dropDown: {
        securityType: [],
        callFeatures: [],
        fadTax: [],
        stateTax: [],
        amt: [],
        bankQualified: [],
        accrueFrom: [],
        form: [],
        couponFrequency: [],
        dayCount: [],
        rateType: [],
        insurance: [],
        underwriterInventory: [],
        syndicateStructure: []
      },
      activeTab: "",
      loading: true,
      isEditable: "",
      isSaveDisabled: false,
      isSavePricingDisabled: false,
      activeItem: [],
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      confirmAlert: CONST.confirmAlert,
      seriesloading: true,
      jsonSheet: [
        {
          term: 32,
          maturityDate: "2018-10-17",
          amount: 25,
          coupon: 0.02,
          yield: 0.03,
          price: 3,
          YTM: 0.03,
          cusip: "3DFSF434",
          callDate: "2018-10-11",
          takeDown: 32,
          insured: false,
          drop: false
        },
        {
          term: 15,
          maturityDate: "2018-10-18",
          amount: 23,
          coupon: 0.02,
          yield: 0.03,
          price: 3,
          YTM: 0.03,
          cusip: "3DFSF435",
          callDate: "2018-10-12",
          takeDown: 32,
          insured: false,
          drop: false
        }
      ]
    }
  }

  componentWillMount() {
    const { transaction, nav2 } = this.props
    if (transaction && transaction._id) {
      if (transaction && transaction.actTranSeriesDetails) {
        this.setState(
          {
            actTranSeriesDetails: transaction.actTranSeriesDetails,
            otherSeriesList: transaction.actTranSeriesDetails,
            activeTab:
              (transaction.actTranSeriesDetails &&
                transaction.actTranSeriesDetails.length &&
                transaction.actTranSeriesDetails[0]._id) ||
              "",
            email: {
              ...this.state.email,
              subject: `Transaction - ${transaction.actTranIssueName ||
                transaction.actTranProjectDescription} - Notification`
            }
          },
          () => {
            this.getActiveSeriesDetails()
          }
        )
      } else {
        this.setState({
          isEditable: 0,
          loading: false
        })
      }
    }
  }

  getDealDetails = dealId => {
    fetchOthersTransaction("pricing", dealId, res => {
      if (res && res.actTranSeriesDetails) {
        this.setState(
          {
            actTranSeriesDetails: res.actTranSeriesDetails,
            otherSeriesList: res.actTranSeriesDetails,
            seriesPricingDetails: res.actTranSeriesDetails,
            activeTab:
              (res.actTranSeriesDetails && res.actTranSeriesDetails[0]._id) ||
              "",
            isSaveDisabled: false
          },
          () => {
            this.getActiveSeriesDetails()
          }
        )
      } else {
        this.setState({
          isEditable: 0,
          loading: false
        })
      }
    })
  }

  async componentDidMount() {
    let result = await getPicklistByPicklistName([
      "LKUPTOBECONFIRMED",
      "LKUPCALLFEATURE",
      "LKUPFEDTAX",
      "LKUPSTATETAX",
      "LKUPAMT",
      "LKUPBANKQUALIFIED",
      "LKUPACCRUEFROM",
      "LKUPFORM",
      "LKUPCOUPONFREQUENCY",
      "LKUPDAYCOUNT",
      "LKUPRATETYPE",
      "LKUPSECTYPEGENERAL",
      "LKUPINSURANCE",
      "LKUPYESNO",
      "LKUPSYNDSTRUCT"
    ])
    result = (result.length && result[1]) || {}

    this.setState({
      dropDown: {
        ...this.state.dropDown,
        securityType: result.LKUPSECTYPEGENERAL,
        callFeatures: result.LKUPCALLFEATURE,
        fadTax: result.LKUPFEDTAX,
        stateTax: result.LKUPSTATETAX,
        amt: result.LKUPAMT,
        bankQualified: result.LKUPBANKQUALIFIED,
        accrueFrom: result.LKUPACCRUEFROM,
        form: result.LKUPFORM,
        couponFrequency: result.LKUPCOUPONFREQUENCY,
        dayCount: result.LKUPDAYCOUNT,
        rateType: result.LKUPRATETYPE,
        insurance: result.LKUPINSURANCE,
        underwriterInventory: result.LKUPYESNO,
        syndicateStructure: result.LKUPSYNDSTRUCT
      },
      userName: (this.props.user && this.props.user.userFirstName) || "",
      loading: false
    })
  }

  onChangeItem = (item, category, index) => {
    let items = this.state[category]
    if (category === "seriesPricingDetails") {
      items = item
    } else {
      items[index] = item
    }

    this.setState({
      [category]: items
    }, () => {
      if(category === "seriesPricingData"){
        this.principalTotal()
      }
    })
  }

  onReset = key => {
    const auditLogs = this.props.auditLogs.filter(x => x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      [key]: [...this.state.tempSeriesPricingData],
      isEditable: "",
      errorMessages: {}
    })
  }

  onAdd = key => {
    const { userName, isEditable } = this.state
    const insertRow = this.state[key]

    if (!!isEditable) {
      swal("Warning", "First save the current series", "warning")
      return
    }

    insertRow.unshift(CONST[key])
    this.props.addAuditLog({ userName, log: `Other series ${key === "actTranSeriesDetails" ? "Series" : key} Add new Item`,
      date: new Date(),
      key
    })
    this.setState({
      [key]: insertRow,
      isEditable: insertRow.length ? 0 : 0
    },() => {
      if(key === "seriesPricingData" || key === "actTranSeriesDetails"){
        this.setState({
          activeItem: [0,1]
        })
      }
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({
      userName,
      log: `Other series ${change}`,
      date: new Date(),
      key: category
    })
  }

  actionButtons = (key, isDisabled) => {
    const canEditTran =
      (this.props.tranAction && this.props.tranAction.canEditTran) || false
    if (!canEditTran) return
    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
            disabled={isDisabled || false}
          >
            Add
          </button>
        </div>
        {key === "actTranSeriesDetails" ? null : (
          <div className="control">
            <button
              className="button is-light is-small"
              onClick={() => this.onReset(key)}
              disabled={isDisabled || false}
            >
              Reset
            </button>
          </div>
        )}
      </div>
    )
  }
  onSeriesSave = index => {
    const series = this.state.actTranSeriesDetails[index]
    let isValid = false
    if (this.state.actTranSeriesDetails.length) {
      isValid = Object.keys(series).some(
        key => typeof series[key] === "string" && !series[key]
      )
    }

    if (!series.seriesName) {
      this.setState({
        errorMessages: {
          series: "Series Code is Required."
        }
      })
      return
    }

    const otherSeries = this.state.actTranSeriesDetails.map(function(item) {
      return item.seriesName
    })

    const isDuplicate = otherSeries.some(function(item, idx) {
      return otherSeries.indexOf(item) !== idx
    })

    if (isDuplicate) {
      this.setState({
        errorMessages: {
          series: "Series Code must be unique."
        }
      })
      return
    }

    if (series && series._id) {
      this.setState(
        {
          isSaveDisabled: true,
          loading: true,
        },
        () => {
          putSeriesDetails(this.props.nav2, series, res => {
            if (res && res.status === 200) {
              this.props.submitAuditLogs(this.props.nav2)
              toast("Other series has been updated!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.setState(
                {
                  errorMessages: {},
                  isEditable: "",
                  loading: false,
                },
                async () => {
                  this.getDealDetails(this.props.nav2)
                }
              )
            } else {
              toast("Something went wrong!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false,
                loading: false,
              })
            }
          })
        }
      )
    } else {
      const otherSeriesDetails = {
        _id: this.props.nav2,
        actTranSeriesDetails: [series]
      }
      this.setState(
        {
          isSaveDisabled: true,
          loading: true,
        },
        () => {
          putOthersTransaction("series", otherSeriesDetails, res => {
            if (res && res.status === 200) {
              toast("Other series has been added!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  errorMessages: {},
                  isEditable: "",
                  isSaveDisabled: false,
                  loading: false
                },
                () => {
                  this.getDealDetails(this.props.nav2)
                }
              )
            } else {
              toast("Something went wrong!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false,
                loading: false,
              })
            }
          })
        }
      )
    }
  }

  getActiveSeriesDetails = () => {
    const { activeTab } = this.state
    const { transaction } = this.props
    if (!activeTab) return
    fetchSeriesDetailsBySeriesId(this.props.nav2, activeTab, res => {
      if (res) {
        const seriesPricing = res.actTranSeriesDetails && res.actTranSeriesDetails.seriesPricingData
          && Array.isArray(res.actTranSeriesDetails.seriesPricingData)
          && res.actTranSeriesDetails.seriesPricingData.map(item => ({
            ...item,
            coupon: item.coupon * 100,
            yield: item.yield * 100,
            YTM: item.YTM * 100
          }))

        if(transaction && transaction.actTranSecurityType){
          CONST.OtherSeriesPricingDetails.seriesSecurityType = transaction.actTranSecurityType
        }
        this.setState({
          loading: false,
          seriesloading: false,
          seriesPricingDetails: res.actTranSeriesDetails
            ? res.actTranSeriesDetails.seriesPricingDetails
              ? res.actTranSeriesDetails.seriesPricingDetails
              : CONST.OtherSeriesPricingDetails
            : CONST.OtherSeriesPricingDetails,
          seriesPricingData: res.actTranSeriesDetails
            ? seriesPricing && seriesPricing.length
              ? seriesPricing
              : [CONST.seriesPricingData]
            : [CONST.seriesPricingData],
          tempSeriesPricingData: res.actTranSeriesDetails
            ? seriesPricing && seriesPricing.length
              ? [...seriesPricing]
              : [CONST.seriesPricingData]
            : [CONST.seriesPricingData]
        })
      }
    })
  }

  onTab = activeTab => {
    this.setState(
      {
        activeTab,
        seriesloading: true,
        loading: true,
        errorMessages: {},
      },
      () => {
        this.getActiveSeriesDetails()
      }
    )
  }

  onEditSeries = index => {
    if (!!this.state.isEditable) {
      swal("Warning", "First save the current series", "warning")
      return
    }
    this.setState({
      isEditable: index
    })
  }

  onRemove = (seriesId, index, seriesName) => {
    const { confirmAlert } = this.state
    confirmAlert.text = `You want to delete this Add Series?`
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        if (seriesId) {
          this.props.addAuditLog({
            userName: this.state.userName,
            log: `Other series ${seriesName} remove item`,
            date: new Date(),
            key: "actTranSeriesDetails"
          })
          this.setState(
            {
              isSaveDisabled: true
            },
            () => {
              removeOtherSeries(this.props.nav2, seriesId, res => {
                if (res && res.status === 200) {
                  toast("Other series was removed!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.SUCCESS
                  })
                  this.setState(
                    {
                      isEditable: "",
                      errorMessages: {},
                      isSaveDisabled: false,
                      actTranSeriesDetails: res.data,
                      otherSeriesList: res.data
                    },
                    () => {
                      this.getDealDetails(this.props.nav2)
                    }
                  )
                } else {
                  toast("Something went wrong!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.ERROR
                  })
                  this.setState({
                    isSaveDisabled: false
                  })
                }
              })
            }
          )
        } else {
          const { actTranSeriesDetails } = this.state
          actTranSeriesDetails.splice(index, 1)
          this.setState({
            actTranSeriesDetails,
            isEditable: "",
            errorMessages: {}
          })
        }
      }
    })
  }

  onSave = () => {
    const { activeTab, seriesPricingDetails, seriesPricingData } = this.state
    let isValid = false
    const actTranSeriesDetails = {
      seriesPricingDetails
    }

    const pricingData = []
    if (seriesPricingData && Object.keys(seriesPricingData).length) {
      seriesPricingData.forEach(series => {
        const data = {
          ...series,
          coupon: series.coupon === "" ? series.coupon : series.coupon / 100,
          yield: series.yield === "" ? series.yield : series.yield / 100,
          YTM: series.YTM === "" ? series.YTM : series.YTM / 100,
        }
        isValid = Object.keys(series).some(key => series[key])
        if (isValid) {
          pricingData.push(data)
        }
      })
    }
    if (pricingData.length) {
      actTranSeriesDetails.seriesPricingData = pricingData
    }
    const errors = PricingDetailsValidate(actTranSeriesDetails)
    if (errors && errors.error) {
      const errorMessages = {}
      console.log("=======>", errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) {
          //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) {
            //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message // "Required"
          } else {
            if (err.path[2]) {
              errorMessages[err.path[0]][err.path[1]] = {
                [err.path[2]]: err.message // "Required"
              }
            } else {
              errorMessages[err.path[0]][err.path[1]] = err.message // "Required"
            }
          }
        } else {
          if (err.path[1] !== undefined && err.path[2] !== undefined) {
            errorMessages[err.path[0]] = {
              [err.path[1]]: {
                [err.path[2]]: err.message // "Required"
              }
            }
          } else {
            errorMessages[err.path[0]] = {
              [err.path[1]]: err.message // "Required"
            }
          }
        }
      })
      if(errorMessages) {
        const seriesPricingDetails = errorMessages && errorMessages.seriesPricingDetails
        const toastMessage = seriesPricingDetails && seriesPricingDetails.seriesPrincipal && seriesPricingDetails && seriesPricingDetails.seriesSecurityType ?
          "Principal & Security Type cannot be blank in Pricing Information" :
          seriesPricingDetails && seriesPricingDetails.seriesPrincipal ? "Principal cannot be blank in Pricing Information" :
            seriesPricingDetails && seriesPricingDetails.seriesSecurityType ? "Security Type cannot be blank in Pricing Information" : errorMessages && errorMessages.seriesPricingData ? Object.values(errorMessages.seriesPricingData).length ? "Some Field Required" : "" : ""
        toast(toastMessage, { autoClose: 4000, type: toast.TYPE.WARNING })
      }
      this.setState({ errorMessages, activeItem: [0, 1] })
      return
    }

    this.setState(
      {
        isSavePricingDisabled: true,
        loading: true,
      },
      () => {
        putOthersPricingDetails(
          this.props.nav2,
          activeTab,
          actTranSeriesDetails,
          res => {
            if (res && res.status === 200) {
              toast("Other pricing has been updated!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  errorMessages: {},
                  isSavePricingDisabled: false,
                  activeItem: [],
                  loading: false,
                })
            } else {
              toast("Something went wrong!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSavePricingDisabled: false,
                activeItem: [],
                loading: false,
              })
            }
          }
        )
      }
    )
  }

  handleToggle = () => {
    this.setState(pre => ({
      modalState: !pre.modalState
    }))
  }

  onModalSave = () => {
    const { email } = this.state
    const { nav1, nav2 } = this.props
    const emailPayload = {
      tranId: nav2,
      type: nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState({
      modalState: false
    }, async () => {
      await sendEmailAlert(emailPayload)
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onExcelRead = docSeries => {
    let { seriesPricingData } = this.state
    const pricingData = []
    if(docSeries && Array.isArray(docSeries)){
      const booleanValues = ["insured", "drop"]
      docSeries.forEach(series => {
        booleanValues.forEach(key => {
          if(typeof series[key] !== "boolean"){
            series[key] = false
          }
        })
      })
      docSeries.map(item => ({
        ...item,
        coupon: item.coupon * 100,
        yield: item.yield * 100,
        YTM: item.YTM * 100
      }))

      seriesPricingData = seriesPricingData.concat(docSeries)
      seriesPricingData.forEach(item => {
        if (!checkEmptyElObject(item)) {
          pricingData.push(item)
        }
      })
      this.setState({
        seriesPricingData: pricingData,
        errorMsg: ""
      }, () => this.principalTotal())
    } else {
      this.setState({
        errorMsg: docSeries
      })
    }
  }

  principalTotal = () => {
    const { seriesPricingData, seriesPricingDetails } = this.state
    if(seriesPricingData && seriesPricingData.length){
      let number = 0
      seriesPricingData.forEach(s => {
        number += parseFloat(s.amount || 0)
      })
      seriesPricingDetails.seriesPrincipal = number
      this.setState({
        seriesPricingDetails
      })
    } else {
      seriesPricingDetails.seriesPrincipal = ""
      this.setState({
        seriesPricingDetails
      })
    }
  }

  onRemovePricingData = index => {
    const seriesPricingData = cloneDeep(this.state.seriesPricingData)
    seriesPricingData.splice(index, 1)
    this.setState({
      seriesPricingData,
      isEditable: "",
      errorMessages: {}
    }, () => this.principalTotal())
  }

  render() {
    const {
      seriesloading,
      jsonSheet,
      modalState,
      email,
      loading,
      cols,
      actTranSeriesDetails,
      errorMessages,
      dropDown,
      activeItem,
      otherSeriesList,
      isSaveDisabled,
      isSavePricingDisabled,
      activeTab,
      seriesPricingDetails,
      seriesPricingData,
      isEditable,
      errorMsg
    } = this.state
    const { tranAction, participants, onParticipantsRefresh } = this.props
    const canEditTran =
      (this.props.tranAction && this.props.tranAction.canEditTran) || false
    let otherSeriesTabs = otherSeriesList.filter(series => series._id)
    otherSeriesTabs = otherSeriesTabs.map(series => ({
      title: <span>{series.seriesName}</span>,
      value: series._id
    }))

    if (loading) {
      return <Loader />
    }

    return (
      <div className="pricing">
        <button className="button is-link is-small" onClick={this.handleToggle}>
          Send Email Alert
        </button>
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onModalSave}
        />
        <Accordion
          multiple
          isRequired={!!activeItem.length}
          boxHidden={true}
          activeItem={activeItem.length ? activeItem : [0]}
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Add Series"
                actionButtons={this.actionButtons(
                  "actTranSeriesDetails",
                  isSaveDisabled
                )}
              >
                {activeAccordions.includes(0) && (
                  <table
                    className="table is-bordered is-striped is-hoverable is-fullwidth"
                    style={{ marginBottom: 0 }}
                  >
                    <TableHeader cols={cols[0]} />
                    {actTranSeriesDetails.map((series, index) => {
                      const errors =
                        errorMessages.actTranSeriesDetails &&
                        errorMessages.actTranSeriesDetails[index.toString()]
                      return (
                        <Series
                          key={index.toString()}
                          index={index}
                          series={series}
                          errors={errors}
                          isEditable={isEditable}
                          onBlur={this.onBlur}
                          category="actTranSeriesDetails"
                          canEditTran={canEditTran}
                          onChangeItem={this.onChangeItem}
                          onRemove={this.onRemove}
                          onEditSeries={this.onEditSeries}
                          onSeriesSave={this.onSeriesSave}
                          isSaveDisabled={isSaveDisabled}
                        />
                      )
                    })}
                    <small className="text-error">
                      {errorMessages.series || ""}
                    </small>
                  </table>
                )}
              </RatingSection>
            </div>
          )}
        />
        {otherSeriesTabs.length ? (
          <Tabs
            activeTab={activeTab}
            tabs={otherSeriesTabs}
            type="other"
            onTab={this.onTab}
          />
        ) : null}
        {otherSeriesTabs.length && !seriesloading ? (
          <Accordion
            multiple
            boxHidden={true}
            isRequired={!!activeItem.length}
            activeItem={activeItem.length ? activeItem : [0]}
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  title="Pricing Information"
                >
                  {activeAccordions.includes(0) && (
                    <PriceCoordinate
                      dropDown={dropDown}
                      category="seriesPricingDetails"
                      errors={
                        (errorMessages && errorMessages.seriesPricingDetails) ||
                        {}
                      }
                      price={seriesPricingDetails}
                      onChangeItem={this.onChangeItem}
                      onBlur={this.onBlur}
                      canEditTran={canEditTran}
                    />
                  )}
                </RatingSection>
                <RatingSection
                  onAccordion={() => onAccordion(1)}
                  title="Pricing Data"
                  actionButtons={this.actionButtons(
                    "seriesPricingData",
                    isSavePricingDisabled
                  )}
                >
                  {activeAccordions.includes(1) && (
                    <div>
                      {canEditTran ? (
                        <DocExcelReader
                          onExcelRead={this.onExcelRead}
                          jsonSheet={jsonSheet}
                        />
                      ) : null}
                      <small className="text-error">
                        {errorMsg}
                      </small>
                      <div>
                        {/* <table
                        className="table is-bordered is-striped is-hoverable is-fullwidth"
                        style={{ marginBottom: 0 }}>
                          <TableHeader cols={cols[1]} /> */}
                        {seriesPricingData.map((priceData, index) => {
                          const errors = errorMessages.seriesPricingData && errorMessages.seriesPricingData[index.toString()]
                          return (
                            <PricingData
                              key={index.toString()}
                              category="seriesPricingData"
                              index={index}
                              errors={errors}
                              priceData={priceData}
                              length={seriesPricingData && seriesPricingData.length}
                              onChangeItem={this.onChangeItem}
                              onBlur={this.onBlur}
                              canEditTran={canEditTran}
                              onRemove={this.onRemovePricingData}
                            />
                          )
                        })}
                        {/* </table> */}
                      </div>
                    </div>
                  )}
                </RatingSection>
                {canEditTran ? (
                  <div className="columns" style={{ marginTop: 10 }}>
                    <div className="column is-full">
                      <div className="field is-grouped-center">
                        <div className="control">
                          <button
                            className="button is-link"
                            onClick={this.onSave}
                            disabled={isSavePricingDisabled}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            )}
          />
        ) : null}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(Pricing)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
