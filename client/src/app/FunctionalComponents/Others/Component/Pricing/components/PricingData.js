import React from "react"
import {
  NumberInput,
  TextLabelInput
} from "../../../../../GlobalComponents/TextViewBox"
import { setDateFormat } from "GlobalUtils/helpers"

const PricingData = ({
  priceData = {},
  index,
  onChangeItem,
  errors = {},
  onBlur,
  category,
  canEditTran,
  length,
  onRemove
}) => {

  const onUserChange = e => {
    if (e.target.name === "cusip") {
      e.target.value = e.target.value ? e.target.value.toUpperCase() : ""
    }

    const newItem = {
      ...priceData,
      [e.target.name]:
        e.target.type === "checkbox" ? e.target.checked : e.target.value
    }
    onChangeItem(newItem, category, index)
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  return (
    <div>
      <div>
        <div className="columns">
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Term</p>
                <NumberInput
                  title="term"
                  name="term"
                  disabled={!canEditTran}
                  error={
                    (errors.term && "Required (should not be less than 0 and greater than 50)") || ""
                  }
                  value={priceData.term || ""}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
          {/* <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Maturity Date</p>
                <input
                  className="input is-small is-link"
                  title="Maturity date"
                  disabled={!canEditTran}
                  type="date"
                  name="maturityDate"
                  value={setDateFormat(priceData.maturityDate)}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
                {errors.maturityDate && (
                  <small className="text-error">{errors.maturityDate}</small>
                )}
              </div>
            </div>
          </div> */}
          <TextLabelInput
            label="Maturity Date"
            disabled={!canEditTran}
            error= {errors.maturityDate || ""}
            type="date"
            name="maturityDate"
            value={(priceData.maturityDate === "" || !priceData.maturityDate) ? null : new Date(priceData.maturityDate)}
            onChange={onUserChange}
            onBlur={onBlurInput}
          />
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Amount</p>
                <NumberInput
                  prefix="$"
                  title="amount"
                  disabled={!canEditTran}
                  decimalScale={4}
                  error={errors.amount || ""}
                  name="amount"
                  value={priceData.amount || 0}
                  placeholder="$"
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Coupon</p>
                <NumberInput
                  title="Coupon"
                  name="coupon"
                  disabled={!canEditTran}
                  decimalScale={6}
                  error={errors.coupon && "Required (should not be less than 0% and greater than 15%)" || ""}
                  suffix="%"
                  value={priceData.coupon || 0}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Yield</p>
                <NumberInput
                  title="Yield"
                  disabled={!canEditTran}
                  decimalScale={6}
                  error={errors.yield && "Required (should not be less than 0% and greater than 10%)" || ""}
                  suffix="%"
                  name="yield"
                  value={priceData.yield || 0}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Price</p>
                <NumberInput
                  title="Price"
                  disabled={!canEditTran}
                  decimalScale={4}
                  error={errors.price || ""}
                  placeholder="$"
                  prefix="$"
                  name="price"
                  value={priceData.price || 0}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">YTM</p>
                <NumberInput
                  disabled={!canEditTran}
                  decimalScale={6}
                  error={errors.YTM && "Required (should not be less than 0% and greater than 15%)" || ""}
                  suffix="%"
                  name="YTM"
                  value={priceData.YTM || 0}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">CUSIP</p>
                <TextLabelInput
                  title="Cusip"
                  error={
                    errors && errors.cusip
                      ? "Required (Alphanumeric character length 6 to 9 accepted)"
                      : ""
                  }
                  name="cusip"
                  value={priceData.cusip || ""}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                  disabled={!canEditTran}
                />
              </div>
            </div>
          </div>
          {/* <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Call Date</p>
                <input
                  className="input is-small is-link"
                  title="Call Date"
                  disabled={!canEditTran}
                  type="date"
                  name="callDate"
                  value={setDateFormat(priceData.callDate)}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
                {errors.callDate && (
                  <small className="text-error">{errors.callDate}</small>
                )}
              </div>
            </div>
          </div> */}
          <TextLabelInput
            label="Call Date"
            disabled={!canEditTran}
            error= {errors.callDate || ""}
            type="date"
            name="callDate"
            value={(priceData.callDate === "" || !priceData.callDate) ? null : new Date(priceData.callDate)}
            onChange={onUserChange}
            onBlur={onBlurInput}
          />
          <div className="column">
            <div className="control">
              <div className="is-small">
                <p className="multiExpLbl">Takedown</p>
                <NumberInput
                  name="takeDown"
                  title="Take down"
                  disabled={!canEditTran}
                  error={errors.takeDown || ""}
                  value={priceData.takeDown || 0}
                  onChange={onUserChange}
                  onBlur={onBlurInput}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="field is-grouped" style={{justifyContent: "start"}}>
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Insured</p>
                  <label className="checkbox">
                    <input
                      type="checkbox"
                      title="insured"
                      name="insured"
                      disabled={!canEditTran}
                      checked={priceData.insured || false}
                      onChange={onUserChange}
                      onBlur={onBlurInput}
                    />
                  </label>
                </div>
              </div>
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Drop</p>
                  <label className="checkbox">
                    <input
                      type="checkbox"
                      title="drop"
                      name="drop"
                      disabled={!canEditTran}
                      checked={priceData.drop || false}
                      onChange={onUserChange}
                      onBlur={onBlurInput}
                    />
                  </label>
                </div>
              </div>
            </div>
          </div>
          { canEditTran ?
            <div className="column">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">&nbsp;</p>
                  <div className="control" style={{textAlign: "end"}}>
                    <a onClick={() => onRemove(index)}>
                      <span className="has-text-link">
                        <i className="far fa-trash-alt" title="Delete"/>
                      </span>
                    </a>
                  </div>
                </div>
              </div>
            </div> : <div className="column"/>
          }
        </div>
      </div>
      { length - 1 === index ? null : <hr/> }
    </div>
  )
}

export default PricingData
