import React from "react"

const Series = ({series={}, index, onChangeItem, onSeriesSave, onEditSeries, isEditable, errors = {}, onBlur, category, onRemove, isSaveDisabled, canEditTran}) => {

  const onUserChange = (e) => {
    const newItem = {
      ...series,
      [e.target.name]: e.target.value,
    }
    onChangeItem(newItem, category, index)
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value){
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }
  isEditable = (isEditable === index)
  return (
    <tbody>
      <tr style={{border: "1px solid #dbdbdb"}}>
        <td>
          <input title="Series Code" className="input is-small is-link" type="text" placeholder="Series 2018Q3" name="seriesName" value={series.seriesName || ""} onChange={onUserChange} onBlur={onBlurInput} disabled={!isEditable}/>
        </td>
        <td>
          <input title="Description" className="input is-small is-link" name="description" type="text" placeholder="Series 2018 Quarter 3" disabled={!isEditable} value={series.description || ""} onChange={onUserChange} onBlur={onBlurInput}/>
        </td>
        <td>
          <input title="Tag" className="input is-small is-link" name="tag" type="text" disabled={!isEditable} placeholder="For internal purposes" value={series.tag || ""} onChange={onUserChange} onBlur={onBlurInput}/>
        </td>
        {
          canEditTran ?
            <td>
              <div className="field is-grouped">
                <div className="control">
                  <a onClick={isEditable ? () => onSeriesSave(index) : () => onEditSeries(index)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                    <span className="has-text-link">
                      {isEditable ? <i className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                    </span>
                  </a>
                </div>
                <div className="control">
                  <a onClick={() => onRemove(series._id, index, series.seriesName)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                    <span className="has-text-link">
                      <i className="far fa-trash-alt" title="Delete"/>
                    </span>
                  </a>
                </div>
              </div>
            </td> : null
        }
      </tr>
    </tbody>
  )}


export default Series
