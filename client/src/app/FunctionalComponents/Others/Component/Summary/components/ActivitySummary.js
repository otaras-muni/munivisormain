import React from "react"
import NumberFormat from "react-number-format"
import {
  SelectLabelInput,
  TextLabelInput
} from "../../../../../GlobalComponents/TextViewBox"
import BorrowerLookup from "Global/BorrowerLookup"
import ThirdPartyLookup from "Global/ThirdPartyLookup"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"

const ActivitySummary = ({ transaction = {}, securityType, onChange, onBlur, disabled, borrowerOrObligorName, guarantorName, onTextInputChange, tranTextChange, canEditTran }) => {

  const borrowerName = {
    _id: "",
    firmName: transaction.actTranBorrowerName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const guarantor = {
    _id: "",
    firmName: transaction.actTranGuarantorName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"

  return (
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          {transaction.actTranClientName ?
            <small style={{fontSize: 15}}>{getTranEntityUrl("Issuer", transaction.actTranClientName, transaction.actTranClientId, "")}</small> : <small>--</small>
          }

        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Issue name (as in OS)</p>
        </div>
        <div className="column">
          <TextLabelInput title="Issue name" name="actTranIssueName" value={tranTextChange.actTranIssueName} placeholder="Issue Name" onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">
            Project Description (internal)
          </p>
        </div>
        <div className="column">
          <TextLabelInput title="Project Description" name="actTranProjectDescription" value={tranTextChange.actTranProjectDescription} placeholder="Issue Name"
            onBlur={onBlur} onChange={onTextInputChange} disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrower or Obligor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput disabled={disabled} list={borrowerOrObligorName || []} name="actTranBorrowerName" value={transaction.actTranBorrowerName || ""}
                            onChange={onChange} inputStyle={{ width: 300 }} /> */}
          <BorrowerLookup
            entityName={borrowerName}
            onChange={(e) => onChange({ target: { name: "actTranBorrowerName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.actTranBorrowerName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Guarantor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput disabled={disabled} list={guarantorName || []} name="actTranGuarantorName" value={transaction.actTranGuarantorName || ""}
            onChange={onChange} inputStyle={{ width: 300 }} /> */}
          <ThirdPartyLookup
            entityName={guarantor}
            onChange={(e) => onChange({ target: { name: "actTranGuarantorName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.actTranGuarantorName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule highlights</p>
        </div>
        <div className="column">
          <small>
            <p>---</p>
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Principal Amount / Par Value</p>
        </div>
        <div className="column">
          <small>
            {transaction.actTranParAmount ? (
              <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{ background: "transparent", border: "none" }}
                decimalScale={2} prefix="$" disabled value={transaction.actTranParAmount} />
            ) : ("---")
            }
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Security</p>
        </div>
        <div className="column">
          <SelectLabelInput disabled={disabled} list={securityType || []} name="actTranSecurityType" value={transaction.actTranSecurityType || ""}
            onChange={onChange}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon / Rate Type:</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{"--"} Rate </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small>---</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Participants Firm</p>
        </div>
        <div className="column">
          <small>
            {transaction.participants ?
              transaction.participants.map((item, i) => (
                <p key={i}>
                  {item.partContactId ? (
                    <div style={{fontSize: 15}}>{getTranUserUrl(item.partType, `${item.partType} : ${item.partFirmName} (${item.partContactName})${transaction.participants.length - 1 === i ? "" : ","}`, item.partContactId, "")}</div>
                  ) :
                    <div style={{fontSize: 15}}>{getTranEntityUrl(item.partType, `${item.partType} : ${item.partFirmName}${transaction.participants.length - 1 === i ? "" : ","}`, item.partFirmId, "")}</div>
                  }
                </p>
              ))
              : null}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notes / Instructions</p>
        </div>
        <div className="column">
          <small>{actNotes || "--"}</small>
        </div>
      </div>
    </div>
  )
}


export default ActivitySummary
