import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TranSummaryStatus from "../../../../GlobalComponents/TranSummaryStatus"
import ActivitySummary from "./components/ActivitySummary"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import {
  putOthersRelatedTran,
  putOthersTransaction
} from "../../../../StateManagement/actions/Transaction/others"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import CONST, { oppMsg } from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { pullRelatedTransactions } from "../../../../StateManagement/actions/CreateTransaction"
import ReletedTranGlob from "../../../../GlobalComponents/ReletedTranGlob"

class Summary extends React.Component {
  constructor() {
    super()
    this.state = {
      debtIssuance: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      },
      tranTextChange: {
        actTranIssueName: "",
        actTranProjectDescription: ""
      },
      errorMessages: {},
      transaction: {},
      securityType: [],
      relatedTypes: [],
      relatedTransactions: [],
      loading: true,
      isSaveDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  componentWillMount() {
    const { transaction } = this.props
    this.setState(prevState => ({
      transaction,
      relatedTransactions: transaction.actTranRelatedTo || [],
      tranTextChange: {
        actTranIssueName: transaction.actTranIssueName || "",
        actTranProjectDescription: transaction.actTranProjectDescription || ""
      },
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actTranIssueName ||
          transaction.actTranProjectDescription} - Notification`
      }
    }))
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName([
      "LKUPSWAPTRANSTATUS",
      "LKUPSECTYPEGENERAL",
      "LKUPRELATEDTYPE",
      "LKUPFIRMNAME"
    ])
    const result = (picResult.length && picResult[1]) || {}

    this.setState({
      tranStatus: result.LKUPSWAPTRANSTATUS || [],
      securityType: result.LKUPSECTYPEGENERAL || [],
      borrowerOrObligorName: result.LKUPFIRMNAME,
      guarantorName: result.LKUPFIRMNAME,
      relatedTypes: result.LKUPRELATEDTYPE || ["Bond Hedge", "Loan Hedge"],
      loading: false
    })
  }

  onRelatedTranSelect = item => {
    const { debtIssuance } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary related type ${
        debtIssuance.actTranRelatedType
      } and ${item.name || ""}`,
      date: new Date(),
      key: "summary"
    })
    this.setState(prevState => ({
      debtIssuance: {
        ...prevState.debtIssuance,
        actTranRelatedTo: item
      }
    }))
  }

  onChange = e => {
    const { debtIssuance } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary select related type ${e.target.value}`,
      date: new Date(),
      key: "summary"
    })
    debtIssuance[e.target.name] = e.target.value
    this.setState({
      debtIssuance: {
        ...debtIssuance,
        actTranRelatedTo: []
      }
    })
  }

  onModalSave = () => {
    this.setState({
      modalState: true,
      relatedSave: true
    })
  }

  onRelatedtranSave = () => {
    const { email, debtIssuance } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    const actTranRelatedTo = {
      ...debtIssuance.actTranRelatedTo,
      relType: debtIssuance.actTranRelatedType
    }

    if (!debtIssuance.actTranRelatedType || actTranRelatedTo.length === 0)
      return false
    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putOthersRelatedTran(this.props.nav2, actTranRelatedTo, res => {
          if (res && res.status === 200) {
            toast("Add related transaction successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(this.props.nav2)
            this.setState(
              {
                transaction: res.data,
                relatedTransactions:
                  (res.data && res.data.actTranRelatedTo) || [],
                debtIssuance: {
                  actTranRelatedType: debtIssuance.actTranRelatedType,
                  actTranRelatedTo: ""
                },
                isSaveDisabled: false
              },
              async () => {
                await sendEmailAlert(emailPayload)
              }
            )
          } else {
            toast("something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onChangeTextInput = e => {
    let tranTextChange = this.state.tranTextChange
    this.setState({
      tranTextChange: {
        ...tranTextChange,
        [e.target.name]: e.target.value
      }
    })
  }

  onBlur = e => {
    const { transaction, tranTextChange } = this.state
    if (transaction[e.target.name] !== tranTextChange[e.target.name]) {
      this.onInputChange(e)
    }
  }

  onInputChange = e => {
    const transaction = cloneDeep(this.state.transaction)
    const { user } = this.props
    let state = {}
    let { name, value, title } = e.target
    const statusChange =
      name === "actTranStatus" ||
      name === "dealIssueTranStatus"
    if (statusChange) {
      name = "actTranStatus"
    }
    transaction[name] = value || ""

    if (statusChange) {
      const payload = {
        [name]: value
      }
      if (transaction.opportunity && transaction.opportunity.status && value !== "In Progress") {
        payload[name] =
          value === "Won" ? "Pre-Pricing" : value
        payload.opportunity = {
          ...transaction.opportunity,
          status: false,
          isWon: value === "Won"
        }
      }
      if(value === "Cancelled" || value === "Closed"){
        const msg = "You will not be able to EDIT any part of the transaction post this action. Are you sure?"
        swal({
          title: "",
          text: msg,
          icon: "warning",
          buttons: ["Cancel", "Yes"]
        }).then(willDo => {
          if (willDo) {
            state = {
              payload,
              statusChange,
              modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress")
            }
            this.setState({
              ...state,
              relatedSave: false
            })
          }
        })
      } else {
        state = {
          payload,
          statusChange,
          modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress")
        }
      }
    } else {
      state = {
        payload: {
          [name]: value || ""
        }
      }
    }
    this.setState(
      {
        ...state,
        relatedSave: false
      },
      () => {
        if (statusChange) {
          if (transaction.opportunity && transaction.opportunity.status) {
            if (value === "Won") {
              swal({
                title: "",
                text: oppMsg.won,
                icon: "warning",
                buttons: ["Cancel", "Yes, won it!"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            } else {
              const msg =
                value === "Lost"
                  ? oppMsg.lost
                  : oppMsg.cancel
              swal({
                title: "",
                text: msg,
                icon: "warning",
                buttons: ["Cancel", "Yes"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            }
          }else {
            this.props.addAuditLog({
              userName: user.userFirstName,
              log: `In summary transaction ${title ||
              "empty"} change to ${value}`,
              date: new Date(),
              key: "summary"
            })
          }
        } else {
          this.addAuditAndSave(title, value, transaction)
        }
      }
    )
  }

  addAuditAndSave = (title, value, transaction) => {
    const {user} = this.props
    if (title && value) {
      this.props.addAuditLog({
        userName: user.userFirstName,
        log: `In summary transaction ${title ||
        "empty"} change to ${value}`,
        date: new Date(),
        key: "summary"
      })
    }
    this.onConfirmationSave(transaction)
  }

  onConfirmationSave = transaction => {
    const { email, payload, statusChange } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState(
      {
        modalState: false
      },
      () => {
        payload._id = tranId
        //updateDealsTransaction(tranId, payload, (res) => { //eslint-disable-line
        putOthersTransaction("summary", payload, res => {
          //eslint-disable-line
          if (res && res.status === 200) {
            toast("Transaction updated successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(tranId)
            if (statusChange) {
              this.setState(
                {
                  statusChange: false,
                  transaction: {
                    ...this.state.transaction,
                    ...payload
                  }
                },
                async () => {
                  this.props.onStatusChange(payload.actTranStatus)
                  await sendEmailAlert(emailParams)
                }
              )
            } else {
              this.setState({
                statusChange: false,
                transaction: {
                  ...transaction,
                  ...payload
                }
              })
            }
          } else {
            toast("Something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
          }
        })
      }
    )
  }

  onCancel = () => {
    this.setState({
      debtIssuance: {
        actTranRelatedType: "",
        actTranRelatedTo: []
      }
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  pdfDownload = () => {
    const { transaction } = this.state
    const {user, logo} = this.props
    const position = user && user.settings
    const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
    const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"
    let participants = ""
    if (transaction.participants) {
      transaction.participants.forEach(item => {
        participants += `${item.partType} : ${item.partFirmName} ${item.partContactName ? `(${item.partContactName})` : ""},\n`
      })
    }
    const tableData = [
      {
        titles: ["Summary"],
        description: "",
        headers: [
          "Issuer name",
          "Issue name",
          "Project Description",
          "Borrower or Obligor Name",
          "Guarantor Name",
          "Schedule highlights",
          "Principal Amount/Par Value",
          "Security",
          "Coupon / Rate Type",
          "Tenor / Maturities",
          "Participants Firm",
          "Notes / Instructions"
        ],
        rows: [
          [
            `${transaction.actTranClientName}`,
            `${
              transaction.actTranIssueName ? transaction.actTranIssueName : "--"
            }`,
            `${transaction.actTranProjectDescription}`,
            `${
              transaction.actTranBorrowerName
                ? transaction.actTranBorrowerName
                : "--"
            }`,
            `${
              transaction.actTranGuarantorName
                ? transaction.actTranGuarantorName
                : "--"
            }`,
            "--",
            `${
              transaction.actTranParAmount ? transaction.actTranParAmount : 0
            }`,
            `${
              transaction.actTranSecurityType
                ? transaction.actTranSecurityType
                : "--"
            }`,
            "-- Rate",
            "--",
            `${transaction.participants ? participants : "--"}`,
            `${actNotes || "--"}`
          ]
        ]
      }
    ]
    pdfTableDownload("", tableData, `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} summary.pdf`, logo, position)
  }

  deleteRelatedTran = async (relatedId, relTranId, relType, relatedType) => {
    const { transaction } = this.props
    const query = `Others?tranId=${transaction._id}&relatedId=${relatedId}&relTranId=${relTranId}&relType=${relType}&relatedType=${relatedType}`
    const related = await pullRelatedTransactions(query)
    if(related && related.done){
      toast("related transaction removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState({
        relatedTransactions: related && related.relatedTransaction || []
      })
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  render() {
    const {
      modalState,
      email,
      debtIssuance,
      relatedTransactions,
      tranStatus,
      borrowerOrObligorName,
      guarantorName,
      isSaveDisabled,
      relatedTypes,
      securityType,
      errorMessages,
      relatedSave,
      tranTextChange
    } = this.state
    const { tranAction, participants, onParticipantsRefresh, nav2 } = this.props
    const transaction = this.state.transaction ? this.state.transaction : {}
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={
            relatedSave === true
              ? this.onRelatedtranSave
              : this.onConfirmationSave
          }
        />
        <TranSummaryStatus
          disabled={!tranAction.canEditTran}
          tranStatus={tranStatus}
          selectedStatus={transaction.actTranStatus || ""}
          pdfDownload={this.pdfDownload}
          transaction={transaction || {}}
          onChange={this.onInputChange}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Activity Summary"
              >
                {activeAccordions.includes(0) && (
                  <ActivitySummary
                    canEditTran={tranAction.canEditTran || false}
                    disabled={!tranAction.canEditTran}
                    securityType={securityType}
                    tranStatus={tranStatus}
                    borrowerOrObligorName={borrowerOrObligorName}
                    guarantorName={guarantorName}
                    transaction={transaction}
                    onChange={this.onInputChange}
                    onTextInputChange={this.onChangeTextInput}
                    onBlur={this.onBlur}
                    tranTextChange={tranTextChange}
                  />
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Related transactions"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(1) && (
                  <ReletedTranGlob
                    relatedTypes={relatedTypes}
                    transaction={transaction}
                    errorMessages={errorMessages}
                    isSaveDisabled={isSaveDisabled}
                    item={debtIssuance}
                    tranId={nav2}
                    onRelatedTranSelect={this.onRelatedTranSelect}
                    onRelatedtranCancel={this.onCancel}
                    onChange={this.onChange}
                    onRelatedtranSave={this.onModalSave}
                    canEditTran={tranAction.canEditTran || false}
                    relatedTransactions={relatedTransactions}
                    deleteRelatedTran={this.deleteRelatedTran}
                  />
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(Summary)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
