import React, { Component } from "react"
import { connect } from "react-redux"
import moment from "moment"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "../../../../GlobalComponents/Loader"
import { CreateOthersValidate } from "../Validation/CreateOthersValidate"
import { putOthersTransaction, postOthersNotes } from "../../../../StateManagement/actions/Transaction/others"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import {
  TextLabelInput,
  SelectLabelInput,
  NumberInput
} from "../../../../GlobalComponents/TextViewBox"
import CONST, { states } from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import HistoricalData from "../../../../GlobalComponents/HistoricalData"
import SearchCountry from "../../../GoogleAddressForm/GoogleCountryComponent"
import TransactionNotes from "../../../../GlobalComponents/TransactionNotes"

const getInitialState = () => ({
  actTranIssueName: "",
  actTranProjectDescription: "",
  // dealIssueTranIssuerId: "",
  actTranClientId: "",
  // dealIssueTranName: "",
  actTranBorrowerName: "",
  actTranGuarantorName: "",
  actTranSubType: "",
  actTranStatus: "",
  actTranState: "",
  actTranCounty: "",
  actTranPrimarySector: "",
  actTranSecondarySector: "",
  actTranOfferingType: "",
  actTranSecurityType: "",
  actTranBankQualified: "",
  actTranCorpType: "",
  actTranParAmount: null,
  actTranPricingDate: "",
  actTranExpAwardDate: "",
  actTranActAwardDate: "",
  actTranSdlcCreditPerc: 0,
  actTranEstimatedRev: "",
  actTranUseOfProceeds: "",
  actTranPlaceholder: "",
  updateUser: "",
  updateDate: "",
  _id: ""
})

class OthersDetails extends Component {
  constructor() {
    super()
    this.state = {
      others: {
        ...getInitialState()
      },
      tempOthers: {
        ...getInitialState()
      },
      userName: "",
      errorMessages: {},
      usersList: [],
      tranNotes: [],
      states: [...states],
      dropDown: {
        borrowerOrObligorName: [],
        guarantorName: [],
        tranType: [],
        tranStatus: [],
        primarySector: {},
        secondarySector: [],
        offeringType: [],
        securityType: [],
        bankQualified: [],
        corpType: [],
        useOfProceeds: [],
        stateCountry: {}
      },
      isSaveDisabled: false,
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  async componentWillMount() {
    const { tranAction, transaction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    this.setState(prevState => ({
      others: {
        ...this.state.others,
        ...transaction,
        actTranPricingDate:
          (transaction.actTranPricingDate &&
            moment(transaction.actTranPricingDate).format("YYYY-MM-DD")) ||
          "",
        actTranExpAwardDate:
          (transaction.actTranExpAwardDate &&
            moment(transaction.actTranExpAwardDate).format("YYYY-MM-DD")) ||
          "",
        actTranActAwardDate:
          (transaction.actTranActAwardDate &&
            moment(transaction.actTranActAwardDate).format("YYYY-MM-DD")) ||
          "",
        actTranState: transaction.actTranState || userState,
        actTranCounty: transaction.actTranCounty || userCounty
      },
      tempDeals: {
        ...this.state.tempDeals,
        ...transaction,
        actTranPricingDate:
          (transaction.actTranPricingDate &&
            moment(transaction.actTranPricingDate).format("YYYY-MM-DD")) ||
          "",
        actTranExpAwardDate:
          (transaction.actTranExpAwardDate &&
            moment(transaction.actTranExpAwardDate).format("YYYY-MM-DD")) ||
          "",
        actTranActAwardDate:
          (transaction.actTranActAwardDate &&
            moment(transaction.actTranActAwardDate).format("YYYY-MM-DD")) ||
          ""
      },
      tranNotes: transaction && transaction.tranNotes || [],
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actTranIssueName ||
          transaction.actTranProjectDescription} - Notification`
      }
    }))
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName([
      "LKUPPRIMARYSECTOR",
      "LKUPFIRMNAME",
      "LKUPOTHERSTRAN",
      "LKUPSWAPTRANSTATUS",
      "LKUPCORPTYPE",
      "LKUPDEALOFFERING",
      "LKUPPARTICIPANTTYPE",
      "LKUPBANKQUALIFIED",
      "LKUPSECTYPEGENERAL",
      "LKUPUSEOFPROCEED",
      /*"LKUPCOUNTRY"*/
    ])
    const result = (picResult.length && picResult[1]) || {}
    const primarySec = picResult[2].LKUPPRIMARYSECTOR || {}
    const stateCountry = /*(picResult[3] && picResult[3].LKUPCOUNTRY) ||*/ {}
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        primarySectors: result.LKUPPRIMARYSECTOR || [],
        secondarySectors: primarySec,
        stateCountry,
        borrowerOrObligorName: result.LKUPFIRMNAME,
        guarantorName: result.LKUPFIRMNAME,
        tranType: result.LKUPOTHERSTRAN,
        tranStatus: result.LKUPSWAPTRANSTATUS,
        offeringType: result.LKUPDEALOFFERING,
        securityType: result.LKUPSECTYPEGENERAL,
        bankQualified: result.LKUPBANKQUALIFIED,
        corpType: result.LKUPCORPTYPE,
        useOfProceeds: result.LKUPUSEOFPROCEED
      },
      userName: (this.props.user && this.props.user.userFirstName) || "",
      loading: false
    })
  }

  onChange = event => {
    this.setState({
      others: {
        ...this.state.others,
        [event.target.name]: event.target.value
      }
    })
  }

  onBlurInput = event => {
    const { userName } = this.state
    if (event.target.title && event.target.value) {
      this.props.addAuditLog({
        userName,
        log: `In Other Details ${event.target.title ||
          "empty"} change to ${event.target.value || "empty"}`,
        date: new Date(),
        key: "Other Details"
      })
    }
  }

  onCancel = () => {
    const { transaction, tranAction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    this.setState({
      others: {
        ...this.state.tempOthers,
        ...transaction,
        actTranState: transaction.actTranState || userState,
        actTranCounty: transaction.actTranCounty || userCounty
      },
      errorMessages: {}
    })
  }

  onSubmit = () => {
    const others = cloneDeep(this.state.others)
    const initialOthers = {
      ...getInitialState(),
      actTranClientId: this.props.user.entityId || ""
    }
    Object.keys(others).forEach(key => {
      if (!initialOthers.hasOwnProperty(key)) {
        delete others[key]
      }
    })
    others.updateDate = new Date()
    others.updateUser = (this.props.user && this.props.user.userId) || ""
    const errors = CreateOthersValidate(others, this.state.others.created_at)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      this.setState({ errorMessages })
      return
    }

    this.setState({
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    // console.log("==============email send to ==============", emailParams)

    const others = cloneDeep(this.state.others)
    const initialOthers = {
      ...getInitialState(),
      actTranClientId: this.props.user.entityId || ""
    }
    Object.keys(others).forEach(key => {
      if (!initialOthers.hasOwnProperty(key)) {
        delete others[key]
      }
    })
    others.updateDate = new Date()
    others.updateUser = (this.props.user && this.props.user.userId) || ""

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putOthersTransaction("details", others, res => {
          if (res && res.status === 200) {
            toast("Add Other Details Successfully!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(this.props.nav2)
            this.setState(
              {
                errorMessages: {},
                others: cloneDeep(this.state.others),
                tempOthers: others,
                isSaveDisabled: false
              },
              async () => {
                await sendEmailAlert(emailPayload)
              }
            )
          } else {
            toast("Something went wrong!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  getCountryDetails = (cityStateCountry = "") => {
    const { others } = this.state
    if (cityStateCountry && cityStateCountry.state) {
      others.actTranState = cityStateCountry.state.trim()
    }else {
      others.actTranState = ""
    }
    if (cityStateCountry && cityStateCountry.city) {
      others.actTranCounty = cityStateCountry.city.trim()
    }else {
      others.actTranCounty = ""
    }
    this.setState({
      others
    })
  }

  onNotesSave = (payload, callback) => {
    postOthersNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add Derivative Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  render() {
    const {
      modalState,
      email,
      others,
      errorMessages,
      loading,
      isSaveDisabled,
      tranNotes,
      dropDown
    } = this.state
    const { tranAction, participants, onParticipantsRefresh } = this.props
    if (loading) {
      return <Loader />
    }
    return (
      <section className="accordions">
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onConfirmationSave}
        />
        <article className="accordion is-active">
          <div className="accordion-header toggle">
            <p>Key Transaction Parameters</p>
          </div>

          <div className="accordion-body">
            <div className="accordion-content">
              <div className="columns">
                {/* <div className="column is-one-fourth">
                  <p className="multiExpLbl">
                    State<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <div className="control">
                    <div
                      className="select is-small is-link"
                      style={{ width: "100%" }}
                    >
                      <select
                        title="State"
                        name="actTranState"
                        value={others.actTranState}
                        onChange={this.onChange}
                        onBlur={this.onBlurInput}
                        style={{ width: "100%" }}
                        disabled={!tranAction.canEditTran}
                      >
                        <option value="">Pick State</option>
                        {dropDown.stateCountry &&
                          Object.keys(
                            dropDown.stateCountry["United States"]
                          ).map((key, i) => (
                            <option key={i} value={key}>
                              {key}
                            </option>
                          ))}
                      </select>
                      {errorMessages.actTranState && (
                        <small className="text-error">
                          {errorMessages.actTranState || ""}
                        </small>
                      )}
                    </div>
                  </div>
                </div>

                <div className="column">
                  <p className="multiExpLbl">County</p>
                  <div
                    className="select is-small is-link"
                    style={{ width: "100%" }}
                  >
                    <select
                      title="County"
                      name="actTranCounty"
                      value={others.actTranCounty}
                      onChange={this.onChange}
                      onBlur={this.onBlurInput}
                      disabled={!tranAction.canEditTran}
                      style={{ width: "100%" }}
                    >
                      <option value="">Pick County</option>
                      {dropDown.stateCountry &&
                      dropDown.stateCountry["United States"] &&
                      dropDown.stateCountry["United States"][
                        others.actTranState
                      ]
                        ? dropDown.stateCountry["United States"][
                          others.actTranState
                        ].map((country, i) => (
                          <option key={country} value={country}>
                            {country}
                          </option>
                        ))
                      : null}
                    </select>
                    {errorMessages.actTranCounty && (
                      <small className="text-error">
                        {errorMessages.actTranCounty || ""}
                      </small>
                    )}
                  </div>
                </div> */}

                <SearchCountry
                  idx={0}
                  label="State/City"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.actTranState || errorMessages.actTranCounty || ""}
                  value={others.actTranState && others.actTranCounty ? `${others.actTranState}, ${others.actTranCounty}` : others.actTranCounty ? `${others.actTranCounty}` : `${others.actTranState}`}
                  getCountryDetails={this.getCountryDetails}
                />

                <div className="column">
                  <p className="multiExpLbl">
                    Primary Sector<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <div className="control">
                    <div
                      className="select is-small is-link"
                      style={{ width: "100%" }}
                    >
                      <select
                        title="Primary Sector"
                        value={others.actTranPrimarySector}
                        onChange={this.onChange}
                        name="actTranPrimarySector"
                        onBlur={this.onBlurInput}
                        style={{ width: "100%" }}
                        disabled={!tranAction.canEditTran}
                      >
                        <option value="" disabled="">
                          Pick Primary Sector
                        </option>
                        {dropDown && dropDown.primarySectors && dropDown.primarySectors.map(t => (
                          <option key={t && t.label} value={t && t.label} disabled={!t.included}>
                            {t && t.label}
                          </option>
                        ))}
                      </select>
                      {errorMessages && errorMessages.actTranPrimarySector && (
                        <small className="text-error">
                          {errorMessages.actTranPrimarySector}
                        </small>
                      )}
                    </div>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Secondary Sector</p>
                  <div
                    className="select is-small is-link"
                    style={{ width: "100%" }}
                  >
                    <select
                      title="Secondary Sector"
                      name="actTranSecondarySector"
                      value={others.actTranSecondarySector}
                      onChange={this.onChange}
                      onBlur={this.onBlurInput}
                      style={{ width: "100%" }}
                      disabled={!tranAction.canEditTran}
                    >
                      <option value="" disabled="">
                        Pick Secondary Sector
                      </option>
                      {others.actTranSecondarySector &&
                      dropDown.secondarySectors[others.actTranPrimarySector]
                        ? dropDown.secondarySectors[
                          others.actTranPrimarySector
                        ].map(sector => (
                          <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                            {sector && sector.label}
                          </option>
                        ))
                        : null}
                    </select>
                    {errorMessages && errorMessages.actTranSecondarySector && (
                      <small className="text-error">
                        {errorMessages.actTranSecondarySector}
                      </small>
                    )}
                  </div>
                </div>
                <SelectLabelInput
                  label="Transaction Type"
                  error={errorMessages.actTranSubType || ""}
                  list={dropDown.tranType}
                  name="actTranSubType"
                  value={others.actTranSubType}
                  onChange={this.onChange}
                  disabled={!tranAction.canEditTran}
                />
              </div>

              <div className="columns">
                <SelectLabelInput
                  label="Use of Proceeds"
                  error={errorMessages.actTranUseOfProceeds || ""}
                  list={dropDown.useOfProceeds}
                  name="actTranUseOfProceeds"
                  value={others.actTranUseOfProceeds}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
                <SelectLabelInput
                  label="Offering Type"
                  error={errorMessages.actTranOfferingType || ""}
                  list={dropDown.offeringType}
                  name="actTranOfferingType"
                  value={others.actTranOfferingType}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                  className="column is-3"
                />
                {/*<SelectLabelInput*/}
                  {/*label="Security Type"*/}
                  {/*error={errorMessages.actTranSecurityType || ""}*/}
                  {/*list={dropDown.securityType}*/}
                  {/*name="actTranSecurityType"*/}
                  {/*value={others.actTranSecurityType}*/}
                  {/*onChange={this.onChange}*/}
                  {/*onBlur={this.onBlurInput}*/}
                  {/*disabled={!tranAction.canEditTran}*/}
                {/*/>*/}
                <SelectLabelInput
                  label="Bank Qualified"
                  error={errorMessages.actTranBankQualified || ""}
                  name="actTranBankQualified"
                  list={dropDown.bankQualified}
                  value={others.actTranBankQualified}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
                <SelectLabelInput
                  label="Corp Type"
                  error={errorMessages.actTranCorpType || ""}
                  list={dropDown.corpType}
                  name="actTranCorpType"
                  value={others.actTranCorpType}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
              </div>

              <div className="columns">
                <NumberInput
                  label="Par Amount"
                  prefix="$"
                  error={errorMessages.actTranParAmount ? "Required(must be larger than or equal to 0)" : "" || ""}
                  name="actTranParAmount"
                  placeholder="$"
                  value={others.actTranParAmount}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
                <TextLabelInput
                  label="Pricing Date"
                  error={errorMessages.actTranPricingDate || ""}
                  name="actTranPricingDate"
                  type="date"
                  // value={others.actTranPricingDate ? moment(others.actTranPricingDate).format("YYYY-MM-DD") : ""}
                  value={(others.actTranPricingDate === "" || !others.actTranPricingDate) ? null : new Date(others.actTranPricingDate)}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
                <TextLabelInput
                  label="Expected Award Date"
                  error={(errorMessages.actTranExpAwardDate && "Required (must be larger than or equal to  Pricing Date)") || ""}
                  name="actTranExpAwardDate"
                  type="date"
                  // value={others.actTranExpAwardDate ? moment(others.actTranExpAwardDate).format("YYYY-MM-DD") : ""}
                  value={(others.actTranExpAwardDate === "" || !others.actTranExpAwardDate) ? null : new Date(others.actTranExpAwardDate)}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
                <TextLabelInput
                  label="Actual Award Date"
                  error={(errorMessages.actTranActAwardDate && "Required (must be larger than or equal to Exp. Award Date)") || ""}
                  name="actTranActAwardDate"
                  type="date"
                  // value={others.actTranActAwardDate ? moment(others.actTranActAwardDate).format("YYYY-MM-DD") : ""}
                  value={(others.actTranActAwardDate === "" || !others.actTranActAwardDate) ? null : new Date(others.actTranActAwardDate)}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                />
              </div>

              <div className="columns">
                <NumberInput
                  label=" SDLC Credit %"
                  suffix="%"
                  error={(errorMessages.actTranSdlcCreditPerc && "(must be larger than 0% and less than or equal to 100%)") || ""}
                  name="actTranSdlcCreditPerc"
                  placeholder="%"
                  value={others.actTranSdlcCreditPerc}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                  className="column is-3"
                />
                <NumberInput
                  label="Estimated Revenue"
                  prefix="$"
                  error={errorMessages.actTranEstimatedRev ? "Required(must be larger than or equal to 0)" : "" || ""}
                  name="actTranEstimatedRev"
                  placeholder="$"
                  value={others.actTranEstimatedRev}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                  disabled={!tranAction.canEditTran}
                  className="column is-3"
                />
              </div>
            </div>
          </div>
        </article>
        {tranAction.canEditTran ? (
          <div className="columns">
            <div className="column is-full" style={{ marginTop: "25px" }}>
              <div className="field is-grouped-center">
                <div className="control">
                  <button
                    className="button is-link"
                    onClick={this.onSubmit}
                    disabled={isSaveDisabled}
                  >
                    Save
                  </button>
                </div>
                <div className="control">
                  <button className="button is-light" onClick={this.onCancel}>
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        { others.OnBoardingDataMigrationHistorical ? <HistoricalData data={(others && others.historicalData) || {}}/> : null}
        <TransactionNotes
          notesList={tranNotes || []}
          canEditTran={tranAction && tranAction.canEditTran || false}
          onSave={this.onNotesSave}
        />
        <br />
      </section>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(OthersDetails)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
