import React from "react"
import { DropdownList } from "react-widgets"
import {
  NumberInput,
  SelectLabelInput
} from "../../../../../GlobalComponents/TextViewBox"

const UnderWriters = ({
  underWriter,
  index,
  isEditable,
  onSave,
  firms,
  dropDown,
  onEdit,
  onCancel,
  onItemChange,
  onRemove,
  errors = {},
  onBlur,
  category,
  isSaveUnderWriter,
  canEditTran,
  tableTitle,
  onCheckAddTODL,
  liabilityTotalPerc,
  managementFeeTotalPerc
}) => {
  const onChange = event => {
    if (
      event.target.name === "roleInSyndicate" &&
      event.target.value === "Sole Manager"
    ) {
      underWriter.liabilityPerc = 100
      underWriter.managementFeePerc = 100
    }
    onItemChange(
      {
        ...underWriter,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      },
      category,
      index
    )
  }

  const onUserChange = firm => {
    onItemChange(
      {
        ...underWriter,
        partFirmId: firm.id,
        partFirmName: firm.name
      },
      category,
      index
    )
  }

  const onBlurInput = (event, name) => {
    if (event && event.target && event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || name || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : name
            ? underWriter.partFirmName
            : event.target.value || "empty"
        }`
      )
    }
    if (event && event.name) {
      onBlur(category, `Underwriters participant firm change to ${event.name}`)
    }
  }

  isEditable = canEditTran ? isEditable && isEditable[category] === index : ""

  return (
    <tr>
      <td>
        <label className="checkbox">
          <input
            title="Add To DL"
            type="checkbox"
            name="partContactAddToDL"
            checked={underWriter.partContactAddToDL}
            onChange={() => {}}
            onClick={e => onCheckAddTODL(e, underWriter, "underWriter")}
            onBlur={onBlurInput}
            disabled={!underWriter._id || !canEditTran}
          />
          {errors.partContactAddToDL && (
            <p className="text-error">{errors.partContactAddToDL}</p>
          )}
        </label>
      </td>
      <td>
        <div className="control">
          <DropdownList
            filter
            data={firms}
            value={underWriter.partFirmName}
            textField="name"
            valueField="id"
            onChange={onUserChange}
            onBlur={e => onBlurInput(e, "partFirmName")}
            disabled={!isEditable}
          />
          {errors.partFirmName && <p className="text-error">Required</p>}
        </div>
        {/* <SelectLabelInput error= {errors.dealPartFirmName || ""} name="dealPartFirmName" value={underWriter.dealPartFirmName} list={firms} onChange={onChange} onBlur={onBlurInput}  disabled={!isEditable}/> */}
      </td>
      <td>
        <SelectLabelInput
          title="Role In Syndicate"
          error={errors.roleInSyndicate || ""}
          name="roleInSyndicate"
          value={underWriter.roleInSyndicate}
          list={dropDown.syndicates}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!isEditable}
        />
      </td>
      <td>
        <NumberInput
          title="Liability Percentage"
          suffix="%"
          error={errors.liabilityPerc/* || isEditable && liabilityTotalPerc > 100 ?  "Total Liability is should be less than or equal to 100%" : "" */}
          name="liabilityPerc"
          placeholder="%"
          value={underWriter.liabilityPerc}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!isEditable}
        />
      </td>
      <td>
        <NumberInput
          title="Management Fee Percentage"
          suffix="%"
          error={errors.managementFeePerc/* || isEditable && managementFeeTotalPerc > 100 ? "Total Management Fee is should be less than or equal to 100%" : "" */}
          name="managementFeePerc"
          placeholder="%"
          value={underWriter.managementFeePerc}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!isEditable}
        />
      </td>
      {canEditTran ? (
        <td>
          <div className="field is-grouped">
            <div className="control">
              <button
                onClick={isEditable ? () => onSave(category, underWriter, index) : () => onEdit(category, index)}
                className={`${isSaveUnderWriter ? "borderless isDisabled" : "borderless"}`}
              >
                <span className="has-text-link">
                  {isEditable ? (
                    <i className="far fa-save" title="Save"/>
                  ) : (
                    <i className="fas fa-pencil-alt" title="Edit"/>
                  )}
                </span>
              </button>
            </div>
            <div className="control">
              <button
                onClick={isEditable ? () => onCancel(category) : () => onRemove(underWriter._id, category, index, underWriter.partFirmName, tableTitle)}
                className={`${isSaveUnderWriter ? "borderless isDisabled" : "borderless"}`}
              >
                <span className="has-text-link">
                  {isEditable ? (
                    <i className="fa fa-times" title="Cancel"/>
                  ) : (
                    <i className="far fa-trash-alt" title="Delete"/>
                  )}
                </span>
              </button>
            </div>
            {underWriter.isNew && !isEditable ? (
              <small className="text-error">New(Not Save)</small>
            ) : null}
          </div>
        </td>
      ) : null}
    </tr>
  )
}

export default UnderWriters
