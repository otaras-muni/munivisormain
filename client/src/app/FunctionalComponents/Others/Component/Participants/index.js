import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import { getPicklistByPicklistName, removeWhiteSpaces, generateDistributionPDF } from "GlobalUtils/helpers"
import CONST from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import UnderWriters from "./components/UnderWriters"
import Loader from "../../../../GlobalComponents/Loader"
import ParticipantsView from "../../../../GlobalComponents/ParticipantsView"
import {
  putOtherParticipantsDetails,
  pullOtherParticipantsDetails,
  postUnderwriterCheckAddToDL
} from "../../../../StateManagement/actions/Transaction/others"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import { SendEmailModal } from "../../../../GlobalComponents/SendEmailModal"
import { fetchSupplierContacts } from "../../../../StateManagement/actions/TransactionDistribute"
import { OtherPartUnderWritersValidate } from "../../../Others/Component/Validation/OthersParticipantsValidate"
import { postCheckedAddToDL } from "../../../../StateManagement/actions/Common"

const mergeArray = (currentList, newItems) => {
  const newObj = newItems.find(part => part.isNew)
  if (newObj) {
    currentList.push(newObj)
  }
  return currentList
}

class Participants extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cols: [
        { name: "Add to DL" },
        { name: "Firm Name<span class=\"icon has-text-danger\"><i class=\"fas fa-asterisk extra-small-icon\" /></span>" },
        { name: "Role in Syndicate<span class=\"icon has-text-danger\"><i class=\"fas fa-asterisk extra-small-icon\" /></span>" },
        { name: "Liability %" },
        { name: "Management Fee %" },
        { name: "Action" }
      ],
      participants: [],
      actTranUnderwriters: [CONST.actTranUnderwriters],
      isSaveUnderWriter: false,
      loading: false,
      modalState: false,
      dropDown: {
        firmName: [],
        syndicates: []
      },
      email: {
        category: "",
        message: "",
        subject: ""
      },
      confirmAlert: CONST.confirmAlert,
      jsonSheets: [],
      startXlDownload: false
    }
  }

  componentWillMount() {
    const { nav2, transaction } = this.props
    this.setState({
      participants:
        (transaction &&
          transaction.participants &&
          transaction.participants.length &&
          transaction.participants) ||
        []
    })
    if (nav2) {
      this.getOtherDetails(nav2)
    }
  }

  getOtherDetails = (nav2, key) => {
    const { transaction } = this.props
    if (transaction && transaction.actTranUnderwriters) {
      fetchSupplierContacts(
        transaction.actTranFirmId,
        "Underwriting Services",
        firms => {
          const underWritingFirms = (firms && firms.suppliersList) || []
          if (key) {
            const isDisabled =
              key === "actTranUnderwriters"
                ? "isSaveUnderWriter"
                : "isSaveParticipants"
            let newPart = this.state[key].find(part => part.isNew)
            const list =
              transaction && transaction[key] && transaction[key].length
                ? cloneDeep(transaction[key])
                : []
            newPart = newPart
              ? cloneDeep(newPart)
              : !list.length
                ? cloneDeep(CONST[key])
                : ""
            if (newPart) {
              list.push(newPart)
            }

            this.setState(prevState => ({
              [key]: list,
              isEditable: {
                ...this.state.isEditable,
                [key]: transaction[key] && !transaction[key].length ? 0 : ""
              },
              underWritingFirms,
              [isDisabled]: false
              /* email: {
              ...prevState.email,
              subject: `Transaction - ${transaction.actTranIssueName || transaction.actTranProjectDescription} - Notification`
            } */
            }))
          } else {
            this.setState(prevState => ({
              underWritingFirms,
              actTranUnderwriters:
                transaction.actTranUnderwriters &&
                transaction.actTranUnderwriters.length
                  ? transaction.actTranUnderwriters
                  : [CONST.actTranUnderwriters],
              isEditable: {
                actTranUnderwriters:
                  transaction.actTranUnderwriters &&
                  !transaction.actTranUnderwriters.length
                    ? 0
                    : ""
              }
              /* email: {
              ...prevState.email,
              subject: `Transaction - ${transaction.actTranIssueName || transaction.actTranProjectDescription} - Notification`
            } */
            }))
          }
        }
      )
    } else {
      this.setState({ loading: false })
    }
  }

  async componentDidMount() {
    let result = await getPicklistByPicklistName([
      "LKUPUNDERWRITER",
      "LKUPUNDERWRITERROLE",
      "LKUPPARTICIPANTTYPE"
    ])
    result = (result.length && result[1]) || {}
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        firmName: result.LKUPUNDERWRITER,
        syndicates: result.LKUPUNDERWRITERROLE
      },
      userName: (this.props.user && this.props.user.userFirstName) || ""
    })
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items
    })
  }

  actionButtons = (key, canEditTran) => {
    const isDisabled =
      key === "actTranUnderwriters"
        ? this.state.isSaveUnderWriter
        : this.state.isSaveParticipants
    const list = this.state[key]
    const isAddNew = (list && list.find(item => item.isNew)) || false

    if (!canEditTran) return

    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
            disabled={isDisabled || !!isAddNew}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset(key)}
            disabled={isDisabled}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  onAdd = key => {
    const { userName, isEditable } = this.state
    const insertRow = this.state[key]
    insertRow.unshift(CONST[key])
    this.props.addAuditLog({
      userName,
      log: `${
        key === "actTranUnderwriters" ? "UnderWriting Team" : "Participants"
      } Add new Item`,
      date: new Date(),
      key
    })

    if (isEditable[key]) {
      swal(
        "Warning",
        `First save the current ${
          key === "actTranUnderwriters" ? "UnderWriting Team" : "Participants"
        }`,
        "warning"
      )
      return
    }

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: insertRow.length ? 0 : 0
      }
    })
  }

  onReset = key => {
    const { isEditable } = this.state
    const list = this.state[key].filter(item => !item.isNew)
    const auditLogs = this.props.auditLogs.filter(x => x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      loading: false,
      [key]: list || [cloneDeep(CONST[type])],
      isEditable: {
        ...isEditable,
        [key]: list && !list.length ? 0 : ""
      }
    })
  }

  distributePDFDownload = async () => {
    let { participants } = this.state
    const { actTranUnderwriters } = this.state
    const { logo, transaction, user } = this.props
    const partTypeQueue = ["Issuer", "Bond Counsel", "Municipal Advisor"]

    const data = []
    const participantsData = []

    if(participants && participants.length || actTranUnderwriters && actTranUnderwriters.length){
      participants.sort((a, b) => {
        const nameA = a && a.partType && a.partType.toLowerCase()
        const nameB = b && b.partType && b.partType.toLowerCase()
        if (nameA < nameB)
          return -1
        if (nameA > nameB)
          return 1
        return 0
      })

      partTypeQueue.forEach(partType => {
        participants.forEach(d => {
          if(d && d.partType === partType && d.addToDL){
            data.push({
              stack: [
                { text: d && d.partType && d.partType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
                { text: `Name: ${d.partContactName || ""}`, bold: true, fontSize: 10},
                { text: `Address: ${d.partContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
                { text: `Phone: ${d.partContactPhone || ""}`, fontSize: 10},
                { text: `Fax: ${d.partContactFax || ""}`, fontSize: 10},
                { text: `E-Mail: ${d.partContactEmail || ""}`, fontSize: 10}
              ]
            })
          }
        })
      })

      participants = participants.filter(d => (d && partTypeQueue.indexOf(d.partType) === -1  && d.addToDL))

      actTranUnderwriters.forEach(d => {
        if(d && d.partContactAddToDL){
          data.push({
            stack: [
              { text: `${d && d.roleInSyndicate && d.roleInSyndicate.toUpperCase()}(Underwriter)`, decoration: "underline", bold: true, margin: [ 0, 10, 0, 0 ]},
              { text: `Firm Name: ${d.partFirmName || ""}`, fontSize: 10},
            ]
          })
        }
      })

      participants.forEach(d => {
        if(d && d.addToDL){
          data.push({
            stack: [
              { text: d && d.partType && d.partType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
              { text: `Name: ${d.partContactName || ""}`, bold: true, fontSize: 10},
              { text: `Address: ${d.partContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
              { text: `Phone: ${d.partContactPhone || ""}`, fontSize: 10},
              { text: `Fax: ${d.partContactFax || ""}`, fontSize: 10},
              { text: `E-Mail: ${d.partContactEmail || ""}`, fontSize: 10}
            ]
          })
        }
      })
    }

    if(data && data.length){
      const stackData = []
      let duplicateData = []
      let dataIndex = 0
      data.forEach(d => {
        if(duplicateData.length < 2){
          dataIndex = dataIndex + 1
          const index = [dataIndex - 1]
          duplicateData.push(data[index])
          if(duplicateData.length === 1){
            stackData.push(duplicateData)
          }
        }
        if (duplicateData.length === 2){
          duplicateData = []
        }
      })
      if(stackData && stackData.length){
        stackData.forEach(stack => {
          if(stack && stack.length === 1){
            stack.push({stack: []})
          }
          participantsData.push({columns: stack}, {marginTop:10, text: [ "" ]})
        })
      }
      await generateDistributionPDF({
        logo,
        firmName: user && user.firmName && user.firmName.toUpperCase() || "",
        clientName: transaction && transaction.actTranClientName && transaction.actTranClientName.toUpperCase() || "",
        transactionType: "Others",
        data: participantsData,
        fileName: `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} participants.pdf`
      })
    } else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  pdfDownload = () => {
    const { transaction, logo, user } = this.props
    const position = user && user.settings
    const { actTranUnderwriters, participants } = this.state
    const pdfUnderWriter = []
    const pdfParticipants = []
    actTranUnderwriters &&
      actTranUnderwriters.forEach(item => {
        if (item.partContactAddToDL) {
          pdfUnderWriter.push([
            item.partContactAddToDL ? "Yes" : "No" || "--",
            item.partFirmName || "--",
            item.roleInSyndicate || "--",
            item.liabilityPerc || "--",
            item.managementFeePerc || "--"
          ])
        }
      })

    participants && participants.forEach(item => {
      const address1 = removeWhiteSpaces(item.partContactAddrLine1 || "")
      if (item.addToDL) {
        pdfParticipants.push([
          item.partType || "--",
          item.partFirmName || "--",
          item.partContactName || "--",
          item.partContactPhone || "--",
          item.partContactEmail || "--",
          item.addToDL ? "Yes" : "No" || "--",
          address1 || "--"
        ])
      }
    })
    const tableData = []
    if (pdfUnderWriter.length > 0) {
      tableData.push({
        titles: ["Other UnderWriter"],
        description: "",
        headers: [
          "Add to DL",
          "Firm name",
          "Role in Syndicate",
          "Liability",
          "Management Fee"
        ],
        rows: pdfUnderWriter
      })
    }
    if (pdfParticipants.length > 0) {
      tableData.push({
        titles: ["Other Participants"],
        description: "",
        headers: [
          "Participant Type",
          "Firm name",
          "Contact Name",
          "Phone",
          "Email",
          "Add to DL",
          "Address"
        ],
        rows: pdfParticipants
      })
    }
    if((pdfUnderWriter && pdfUnderWriter.length) || (pdfParticipants && pdfParticipants.length)){
      pdfTableDownload("", tableData, `${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} participants.pdf`, logo, position)
    }else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  xlDownload = () => {
    const { transaction } = this.props
    const { actTranUnderwriters, participants } = this.state
    const pdfUnderWriter = []
    const pdfParticipants = []
    actTranUnderwriters &&
    actTranUnderwriters.map(item => {
      const data = {
        "Add to DL": item.partContactAddToDL || false,
        "Firm Name": item.partFirmName || "--",
        "Part Type": item.partType || "--",
        "Role in Syndicate": item.roleInSyndicate || "--",
        Liability: item.liabilityPerc || "--",
        "Management Fee": item.managementFeePerc || "--"
      }
      pdfUnderWriter.push(data)
    })

    participants &&
    participants.map(item => {
      const data = {
        "Add to DL": item.addToDL || false,
        "Firm Name": item.partFirmName || "--",
        "Part Type": item.partType || "--",
        "Name": item.partContactName || "--",
        "Email": item.partContactEmail || "--",
        "Phone": item.partContactPhone || "--",
        "Address": item.partContactAddrLine1 || "--"
      }
      pdfParticipants.push(data)
    })
    const jsonSheets = [
      {
        name: "UnderWriter",
        headers: [
          "Add to DL",
          "Firm Name",
          "Part Type",
          "Role in Syndicate",
          "Liability",
          "Management Fee"
        ],
        data: pdfUnderWriter
      },
      {
        name: "Participants",
        headers: [
          "Add to DL",
          "Firm Name",
          "Part Type",
          "Name",
          "Email",
          "Phone",
          "Address"
        ],
        data: pdfParticipants
      }
    ]
    this.setState(
      {
        jsonSheets,
        startXlDownload: true
      },
      () => this.resetXLDownloadFlag
    )
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  onEdit = (key, index) => {
    this.props.addAuditLog({
      userName: this.state.userName,
      log: `Others participants In ${key} one row edited`,
      date: new Date(),
      key
    })
    if (this.state.isEditable[key]) {
      swal(
        "Warning",
        `First save the current ${
          key === "actTranUnderwriters" ? "UnderWriting Team" : "Participants"
        }`,
        "warning"
      )
      return
    }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index
      }
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({
      userName,
      log: `${change}`,
      date: new Date(),
      key: category
    })
  }

  onRemove = (removeId, callback) => {
    pullOtherParticipantsDetails(
      "participants",
      this.props.nav2,
      removeId,
      res => {
        if (res && res.status === 200) {
          toast("Removed participant successfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS
          })
          this.setState(
            {
              participants: (res.data && res.data.participants) || []
            },
            () => {
              callback({
                status: true,
                participants: (res.data && res.data.participants) || []
              })
            }
          )
        } else {
          toast("Something went wrong!", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
          callback({
            status: false
          })
        }
      }
    )
  }

  onSavePart = (participant, callback) => {
    putOtherParticipantsDetails(
      "participants",
      this.props.nav2,
      participant,
      async res => {
        if (res && res.status === 200) {
          toast("Added participant successfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS
          })
          this.setState(
            {
              participants: (res.data && res.data.participants) || []
            },
            () => {
              callback({
                status: true,
                participants: (res.data && res.data.participants) || []
              })
            }
          )
        } else {
          toast("Something went wrong!", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
          callback({
            status: false
          })
        }
      }
    )
  }

  onRemoveParticipants = (removeId, type, index, name, tableTitle) => {
    const otherId = this.props.nav2
    const { userName, isEditable, confirmAlert } = this.state
    confirmAlert.text = `You want to delete this ${tableTitle}?`
    swal(confirmAlert).then(willDelete => {
      if (type && otherId && removeId) {
        const changeLog =
          type === "actTranUnderwriters"
            ? `Removed Underwriting Team ${name}`
            : `Removed Participant ${name}`
        this.props.addAuditLog({
          userName,
          log: `${changeLog}`,
          date: new Date(),
          key: "actTranUnderwriters"
        })

        if (willDelete) {
          const isDisabled =
            type === "actTranUnderwriters"
              ? "isSaveUnderWriter"
              : "isSaveParticipants"
          this.setState(
            {
              [isDisabled]: true
            },
            () => {
              pullOtherParticipantsDetails(type, otherId, removeId, res => {
                if (res && res.status === 200) {
                  toast(
                    `Other ${
                      type === "actTranUnderwriters"
                        ? "Underwriting Team"
                        : "Participants"
                    } has been deleted!`,
                    {
                      autoClose: CONST.ToastTimeout,
                      type: toast.TYPE.SUCCESS
                    }
                  )
                  this.props.submitAuditLogs(otherId)
                  let newPart = this.state[type].find(part => part.isNew)
                  const list =
                    res && res.data && res.data[type] && res.data[type].length
                      ? cloneDeep(res.data[type])
                      : []
                  newPart = newPart
                    ? cloneDeep(newPart)
                    : !list.length
                      ? cloneDeep(CONST[type])
                      : ""
                  if (newPart) {
                    list.push(newPart)
                  }

                  this.setState({
                    [type]: list,
                    isEditable: {
                      ...isEditable,
                      [type]:
                        res &&
                        res.data &&
                        res.data[type] &&
                        !res.data[type].length
                          ? 0
                          : ""
                    },
                    errorMessages: {},
                    [isDisabled]: false
                  })
                } else {
                  toast("Something went wrong!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.ERROR
                  })
                  this.setState({
                    [isDisabled]: false
                  })
                }
              })
            }
          )
        }
      } else {
        const { actTranUnderwriters } = this.state
        if (willDelete) {
          if (type === "actTranUnderwriters") {
            actTranUnderwriters.splice(index, 1)
          }
          this.setState({
            actTranUnderwriters,
            isEditable: {
              ...isEditable,
              [type]: ""
            },
            errorMessages: {}
          })
        }
      }
    })
  }

  onSave = (key, underPart, index) => {
    const { isEditable, actTranUnderwriters } = this.state
    let liabilityTotalPerc = 0
    let managementFeeTotalPerc = 0
    actTranUnderwriters.forEach(underWriter => {
      liabilityTotalPerc += underWriter.liabilityPerc
        ? parseInt(underWriter.liabilityPerc, 10)
        : 0
      managementFeeTotalPerc += underWriter.managementFeePerc
        ? parseInt(underWriter.managementFeePerc, 10)
        : 0
    })

    if (liabilityTotalPerc > 100 || managementFeeTotalPerc > 100) return

    let isValid = false
    let errors = {}
    if (key === "actTranUnderwriters") {
      /* if (underPart && Object.keys(underPart).length) {
        isValid = Object.keys(underPart).some(key => typeof underPart[key] === "string" && !underPart[key])
      } */
      errors = OtherPartUnderWritersValidate(underPart)
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        let message = "Required"
        if (
          err.path[0] === "managementFeePerc" ||
          err.path[0] === "liabilityPerc"
        ) {
          message = "Required (must be less than or equal to 100)"
        }
        if (errorMessages.hasOwnProperty(key)) {
          //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) {
            //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    if (isValid) {
      this.setState({
        errorMessages: {
          [key]: "All field is Required."
        }
      })
      return
    }

    const isDisabled =
      key === "actTranUnderwriters" ? "isSaveUnderWriter" : "isSaveParticipants"
    const Toast =
      key === "actTranUnderwriters" ? "Underwriting Team" : "Participants"
    this.setState(
      {
        [isDisabled]: true
      },
      () => {
        delete underPart.isNew
        putOtherParticipantsDetails(
          key === "actTranUnderwriters" ? "underwriters" : "participants",
          this.props.nav2,
          underPart,
          res => {
            if (res && res.status === 200) {
              toast(`Other ${Toast} has been updated!`, {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)

              let list =
                res && res.data && res.data[key] && res.data[key].length
                  ? cloneDeep(res.data[key])
                  : []
              list = mergeArray(
                list,
                list.length
                  ? cloneDeep(this.state[key])
                  : [cloneDeep(CONST[key])]
              )

              this.setState(
                {
                  [key]: list,
                  isEditable: {
                    ...isEditable,
                    [key]:
                      res.data && res.data[key] && !res.data[key].length
                        ? 0
                        : ""
                  },
                  errorMessages: {},
                  [isDisabled]: false
                })
            } else {
              const error =
                (res &&
                  res.response &&
                  res.response.data &&
                  res.response.data.error) ||
                "Something went wrong!"
              toast(error, {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                [isDisabled]: false
              })
            }
          }
        )
      }
    )
  }

  handleToggle = () => {
    this.setState(pre => ({
      modalState: !pre.modalState
    }))
  }

  onModalSave = () => {
    const { email } = this.state
    const { nav1, nav2 } = this.props
    const emailPayload = {
      tranId: nav2,
      type: nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState({
      modalState: false
    }, async () => {
      await sendEmailAlert(emailPayload)
    })
  }

  onCancel = key => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: ""
      }
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onCheckAddTODL = async (
    e,
    item,
    participantType,
    type,
    tranId,
    payload,
    callback
  ) => {
    const { participants, actTranUnderwriters } = this.state
    if (participantType === "participant") {
      const res = await postCheckedAddToDL(type, tranId, payload)
      if (res && res.status === 200 && res.data && res.data.updateParticipant) {
        const partIndex = participants.findIndex(part => part._id === item._id)
        participants[partIndex].addToDL =
          res.data.updateParticipant && res.data.updateParticipant.addToDL
        this.setState(
          {
            participants
          },
          () => {
            callback({
              status: true,
              participants: participants || []
            })
          }
        )
      }
    } else if (participantType === "underWriter") {
      const payloads = {
        _id: item._id,
        addToDL: e.target.checked
      }
      const unIndex = actTranUnderwriters.findIndex(
        under => under._id === item._id
      )
      actTranUnderwriters[unIndex].partContactAddToDL = e.target.checked
      this.setState(
        {
          actTranUnderwriters
        },
        async () => {
          const res = await postUnderwriterCheckAddToDL(
            item._id,
            this.props.nav2,
            payloads
          )
          if (
            res &&
            res.status === 200 &&
            res.data &&
            res.data.updateUnderwriter
          ) {
            console.log(res.data.updateUnderwriter)
            actTranUnderwriters[unIndex].partContactAddToDL =
              res.data.updateUnderwriter &&
              res.data.updateUnderwriter.partContactAddToDL
            this.setState({
              actTranUnderwriters
            })
          }
        }
      )
    }
  }

  render() {
    const {
      participants,
      modalState,
      cols,
      email,
      actTranUnderwriters,
      isSaveUnderWriter,
      dropDown,
      isEditable,
      underWritingFirms,
      errorMessages
    } = this.state
    const { transaction, nav2, tranAction, onParticipantsRefresh } = this.props
    const columns =
      cloneDeep(cols) || cloneDeep(cols.filter(col => col.name !== "Action"))

    let liabilityTotalPerc = 0
    let managementFeeTotalPerc = 0
    actTranUnderwriters.forEach(underWriter => {
      liabilityTotalPerc += underWriter.liabilityPerc
        ? parseInt(underWriter.liabilityPerc, 10)
        : 0
      managementFeeTotalPerc += underWriter.managementFeePerc
        ? parseInt(underWriter.managementFeePerc, 10)
        : 0
    })

    liabilityTotalPerc > 100 ? columns.splice(3, 1, {name: "<b class='has-text-danger'>Liability %</b>"})
      : columns.splice(3, 1, { name: "Liability %" })
    managementFeeTotalPerc > 100 ? columns.splice(4, 1, {name: "<b class='has-text-danger'>Management Fee %</b>"})
      : columns.splice(4, 1, { name: "Management Fee %" })
    const isDisabledDownload = (actTranUnderwriters[0] && actTranUnderwriters[0]._id) || (participants && participants.length)
    const loading = () => <Loader />
    if (this.state.loading) {
      return loading()
    }
    return (
      <div className="participants">
        <div className="columns">
          <div className="column">
            <button
              className="button is-link is-small"
              onClick={this.handleToggle}
            >
              Send Email Alert
            </button>
          </div>
          <div className="column">
            <div className="field is-grouped">
              <div className={`${!isDisabledDownload ? "control isDisabled" : "control"}`} onClick={this.distributePDFDownload}>
                <span className="has-text-link">
                  <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                </span>
              </div>
              {/* <div className={`${!isDisabledDownload ? "control isDisabled" : "control"}`} onClick={this.pdfDownload}>
                <span className="has-link">
                  <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                </span>
              </div> */}
              <div className={`${!isDisabledDownload ? "control isDisabled" : "control"}`} onClick={this.xlDownload}>
                <span className="has-link">
                  <i className="far fa-2x fa-file-excel has-text-link" title="Excel Download"/>
                </span>
              </div>
              <div style={{ display: "none" }}>
                <ExcelSaverMulti
                  label={`${transaction.actTranIssueName || transaction.actTranProjectDescription || ""} participants`}
                  startDownload={this.state.startXlDownload}
                  afterDownload={this.resetXLDownloadFlag}
                  jsonSheets={this.state.jsonSheets}
                />
              </div>
            </div>
          </div>
        </div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={this.onModalSave}
          participants={this.props.participants}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Underwriting Team"
                actionButtons={this.actionButtons(
                  "actTranUnderwriters",
                  tranAction.canEditTran || false
                )}
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(0) && (
                  <div className="tbl-scroll">
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={columns} />
                      <tbody>
                        {Array.isArray(actTranUnderwriters) &&
                        actTranUnderwriters.length
                          ? actTranUnderwriters.map((underWriter, i) => {
                            const errors =
                              (errorMessages &&
                                errorMessages.actTranUnderwriters &&
                                errorMessages.actTranUnderwriters[
                                  i.toString()
                                ]) ||
                              {}
                            const isExists = actTranUnderwriters.map(
                              e =>
                                e.partFirmId !== underWriter.partFirmId &&
                                e.partFirmId
                            )
                            const firms =
                              underWritingFirms &&
                              underWritingFirms.filter(
                                e => isExists.indexOf(e.id) === -1
                              )

                            return (
                              <UnderWriters
                                key={i.toString()}
                                category="actTranUnderwriters"
                                index={i}
                                errors={errors}
                                firms={firms}
                                dropDown={dropDown}
                                isEditable={isEditable}
                                underWriter={underWriter}
                                onItemChange={this.onItemChange}
                                onBlur={this.onBlur}
                                onRemove={this.onRemoveParticipants}
                                onEdit={this.onEdit}
                                onSave={this.onSave}
                                onCancel={this.onCancel}
                                tableTitle="Underwriting Team"
                                isSaveUnderWriter={isSaveUnderWriter}
                                onCheckAddTODL={this.onCheckAddTODL}
                                canEditTran={tranAction.canEditTran}
                                liabilityTotalPerc={liabilityTotalPerc}
                                managementFeeTotalPerc={managementFeeTotalPerc}
                              />
                            )
                          })
                          : null}
                      </tbody>
                    </table>
                  </div>
                )}
              </RatingSection>
              <br />
              <ParticipantsView
                id={nav2}
                transaction={transaction}
                firmId={(transaction && transaction.actTranFirmId) || ""}
                title="Other Participants"
                onRemove={this.onRemove}
                onSave={this.onSavePart}
                participants={participants}
                emailInfo={email}
                canEditTran={tranAction && tranAction.canEditTran}
                type="Others"
                onCheckAddTODL={this.onCheckAddTODL}
                underwriters={actTranUnderwriters && Array.isArray(actTranUnderwriters) ? actTranUnderwriters : []}
              />
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
})
const WrappedComponent = withAuditLogs(Participants)

export default connect(mapStateToProps, null)(WrappedComponent)
