import React from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import {toast} from "react-toastify"
import cloneDeep from "clone-deep"
import Loader from "../../../../GlobalComponents/Loader"
import  TranDocuments from "../../../../GlobalComponents/TranDocuments"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import {
  pullTransactionDoc,
  putTransactionDocStatus
} from "../../../../StateManagement/actions/Transaction"
import {putOthersTransaction} from "../../../../StateManagement/actions/Transaction/others"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {getDocumentList} from "../../../../../globalutilities/helpers"


class Documents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      actTranDocuments: [],
      transaction: {},
      loading: true
    }
  }

  async componentWillMount() {
    const {transaction, user} = this.props
    const filterDocuments = getDocumentList(cloneDeep(transaction.actTranDocuments), user)

    if(transaction) {
      this.setState({
        transaction,
        actTranDocuments: transaction.actTranFirmId === user.entityId ? transaction.actTranDocuments : filterDocuments || [],
        loading: false,
      })
    }else {
      this.setState({
        loading: false
      })
    }
  }


  onDocSave = (docs, callback) => {
    const {transaction, user} = this.props
    console.log(docs)
    putOthersTransaction(this.props.nav3, docs, (res)=> {
      if (res && res.status === 200) {
        toast("Other Documents has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.props.submitAuditLogs(this.props.nav2)
        const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.actTranDocuments), user)
        callback({
          status: true,
          documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.actTranDocuments : filterDocuments || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    const {transaction, user} = this.props
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        [name]: value
      }
      if(value !== "Public"){
        document.settings = {firms: [], users: [], selectLevelType: ""}
      }
      type = "&type=status"
    }else {
      document = {
        ...doc
      }
    }
    const res = await putTransactionDocStatus(this.props.nav2, `Others${type}`, {_id: doc._id, ...document})
    if (res && res.status === 200) {
      toast("Document has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.document), user)
      if(name){
        callback({
          status: true,
        })
      }else {
        callback({
          status: true,
          documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.document : filterDocuments || [],
        })
      }
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onDeleteDoc = async (documentId, callback) => {
    const {transaction, user} = this.props
    const res = await pullTransactionDoc(this.props.nav2, `?tranType=Others&docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.actTranDocuments), user)
      callback({
        status: true,
        documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.actTranDocuments : filterDocuments || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  render() {
    const { actTranDocuments, transaction } = this.state
    const { tranAction } = this.props
    const tags = {
      clientId: transaction.actTranFirmId,
      clientName: `${transaction.actTranFirmName}`,
      tenantId: transaction.actTranClientId,
      tenantName: transaction.actTranClientName,
      contextName:transaction.actTranIssueName ? transaction.actTranIssueName : transaction.actTranProjectDescription
    }
    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }
    return(
      <div>
        <div className="bank-loan-documents">
          <TranDocuments {...this.props} onSave={this.onDocSave} tranId={this.props.nav2} title="Upload Others Documents" pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickAction="LKUPDOCACTION"
            category="actTranDocuments" isDisabled={!tranAction.canTranStatusEditDoc} documents={actTranDocuments} contextType={ContextType.others} tags={tags} onStatusChange={this.onStatusChange} onDeleteDoc={this.onDeleteDoc}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Documents)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
