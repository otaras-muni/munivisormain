import Joi from "joi-browser";

const otherUnderWritersSchema = Joi.object().keys({
  partFirmId: Joi.string().required(),
  partType: Joi.string().required(),
  partFirmName: Joi.string().required(),
  roleInSyndicate: Joi.string().required(),
  liabilityPerc: Joi.number().allow(null, "").optional(),
  managementFeePerc: Joi.number().allow(null, "").optional(),
  partContactAddToDL: Joi.boolean().required().optional(),
  isNew: Joi.boolean().optional(),
  _id: Joi.string().required().optional(),
})

export const OtherPartUnderWritersValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, otherUnderWritersSchema, { abortEarly: false, stripUnknown:false })
}

