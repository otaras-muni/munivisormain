import Joi from "joi-browser"
import dateFormat from "dateformat";

const seriesPricingDataSchema = Joi.object().keys({
  term: Joi.number().min(0).max(50).required(),
  maturityDate: Joi.date()/* .min(Joi.ref("callDate")) */.required(),
  amount: Joi.number().min(0).required(),
  coupon: Joi.number().min(0).max(0.15).required(),
  yield: Joi.number().min(0).max(0.10).required(),
  price: Joi.number().min(0).required(),
  YTM: Joi.number().min(0).max(0.15).required(),
  cusip: Joi.string().uppercase().min(6).max(9).required().optional(),
  callDate: Joi.date().required(),
  takeDown: Joi.number().min(0).required(),
  insured: Joi.boolean().required().optional(),
  drop: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

const seriesPricingDetailsSchema = Joi.object().keys({
  seriesPrincipal: Joi.number().min(0).required(),
  seriesSecurityType: Joi.string().required().optional(),
  seriesSecurity: Joi.string().allow("", null).optional(),
  seriesSecurityDetails: Joi.string().allow("", null).optional(),
  seriesDatedDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat( new Date(), "yyyy-mm-dd")) */.allow(null).optional(),
  seriesSettlementDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat( new Date(), "yyyy-mm-dd")) */.allow(null).optional(),
  seriesFirstCoupon: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat( new Date(), "yyyy-mm-dd")) */.allow(null).optional(),
  seriesPutDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat( new Date(), "yyyy-mm-dd")) */.allow(null).optional(),
  seriesRecordDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat( new Date(), "yyyy-mm-dd")) */.allow(null).optional(),
  seriesCallDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat( new Date(), "yyyy-mm-dd")) */.allow(null).optional(),
  seriesFedTax: Joi.string().allow("", null).optional(),
  seriesStateTax: Joi.string().allow("", null).optional(),
  seriesPricingAMT: Joi.string().allow("", null).optional(),
  seriesBankQualified: Joi.string().allow("", null).optional(),
  seriesAccrueFrom: Joi.string().allow("", null).optional(),
  seriesPricingForm: Joi.string().allow("", null).optional(),
  seriesCouponFrequency: Joi.string().allow("", null).optional(),
  seriesDayCount: Joi.string().allow("", null).optional(),
  seriesRateType: Joi.string().allow("", null).optional(),
  seriesCallFeature: Joi.string().allow("", null).optional(),
  seriesCallPrice: Joi.number().min(0).allow(null).optional(),
  seriesInsurance: Joi.string().allow("", null).optional(),
  seriesUnderwtiersInventory: Joi.string().allow("", null).optional(),
  seriesMinDenomination: Joi.number().min(0).allow(null).optional(),
  seriesSyndicateStructure: Joi.string().allow("", null).optional(),
  seriesGrossSpread: Joi.number().min(0).allow(null).optional(),
  seriesEstimatedTakeDown: Joi.number().min(0).allow(null).optional(),
  seriesInsuranceFee: Joi.number().min(0).allow(null).optional(),
  _id: Joi.string().required().optional(),
})

const OtherPricingSchema = Joi.object().keys({
  seriesPricingDetails: seriesPricingDetailsSchema,
  seriesPricingData: Joi.array().items(seriesPricingDataSchema).optional(),
})

export const PricingDetailsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, OtherPricingSchema, { abortEarly: false, stripUnknown:false })
