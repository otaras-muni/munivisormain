import Joi from "joi-browser"
import dateFormat from "dateformat"

const transDetailSchema = minDate =>
  Joi.object().keys({
    actTranIssueName: Joi.string()
      .allow(["", null])
      .required()
      .optional(),
    actTranProjectDescription: Joi.string()
      .allow("")
      .optional(),
    // dealIssueTranIssuerId: Joi.string().required().optional(),
    actTranClientId: Joi.string()
      .required()
      .optional(),
    // dealIssueTranName: Joi.string().required().optional(),
    actTranBorrowerName: Joi.string()
      .allow("")
      .optional(),
    actTranGuarantorName: Joi.string()
      .allow("")
      .optional(),
    actTranSubType: Joi.string()
      .required()
      .optional(),
    actTranStatus: Joi.string()
      .required()
      .optional(),
    actTranState: Joi.string()
      .required()
      .optional(),
    actTranCounty: Joi.string()
      .allow("")
      .optional(),
    actTranPrimarySector: Joi.string()
      .required()
      .optional(),
    actTranSecondarySector: Joi.string()
      .allow("")
      .optional(),
    actTranOfferingType: Joi.string()
      .allow("")
      .optional(),
    actTranSecurityType: Joi.string()
      .allow("")
      .optional(),
    actTranBankQualified: Joi.string()
      .allow("")
      .optional(),
    actTranCorpType: Joi.string()
      .allow("")
      .optional(),
    actTranParAmount: Joi.number().min(0).allow(null, "").optional(),
    actTranPricingDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
    actTranExpAwardDate: Joi.date().allow([null,""]).example(new Date("2016-01-01")).min(Joi.ref("actTranPricingDate")).allow("", null).optional(),
    actTranActAwardDate: Joi.date().allow("", null).example(new Date("2016-01-01")).min(Joi.ref("actTranExpAwardDate")).allow("", null).optional(),
    actTranSdlcCreditPerc: Joi.number().max(100).min(0).allow(null, "").optional(),
    actTranEstimatedRev: Joi.number().min(0).allow(null, "").optional(),
    actTranUseOfProceeds: Joi.string()
      .allow("")
      .optional(),
    actTranPlaceholder: Joi.string()
      .allow("")
      .optional(),
    updateUser: Joi.string().optional(),
    updateDate: Joi.date().optional(),
    _id: Joi.string().required()
  })

export const CreateOthersValidate = (inputTransDetail, minDate) =>
  Joi.validate(inputTransDetail, transDetailSchema(minDate), {
    abortEarly: false,
    stripUnknown: false
  })
