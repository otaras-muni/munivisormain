import React, { Component } from "react"
import Cleave from "cleave.js/react"
import {toast} from "react-toastify"
import CONST from "GlobalUtils/consts"
import {numberWithCommas}  from "GlobalUtils/functionalutilities"
import "../scss/dealScheduleOfEvents.scss"
import {putDealsCheckNTrack, removeDealsCheckInTrack, fetchDealsTransaction} from "../../../../StateManagement/actions/Transaction"
import Loader from "../../../../GlobalComponents/Loader"
import DealScheduleOfEvents from "./DealScheduleOfEvents"

const defaultData = [{
  costOfIssCategory:"Undewriter's Discount",
  items:[
    "Average TakeDown",
    "Underwriter's Discount",
    "Ipreo",
    "CUSIP",
    "DTC",
    "Travel & Insurance"
  ]
},
{
  costOfIssCategory:"Cost of Issuance",
  items:[
    "Special Counsel",
    "Disclosure Counsel",
    "Disclosure Counsel Expenses",
    "Financial Advisor Expenses",
    "Fitch Rating Fee"
  ]
}
]

const fieldDetails = [{
  accordion:"Underwriter's Discount",
  fields:[
    {
      colName:"Cost Item",
      abbr:"Cost Item",
      itemplaceholder:true,
      fieldName:"costItem",
      initialValue:"",
    },
    {
      colName:"$/1000",
      abbr:"Cost Item",
      itemplaceholder:false,
      fieldName:"costPerParBond",
      initialValue:"",
    },
    {
      colName:"Amount",
      abbr:"Cost Item",
      itemplaceholder:false,
      fieldName:"costAmount",
      initialValue:"",
    },
    {
      colName:"Drop",
      abbr:"Delete cost item",
      itemplaceholder:false,
      fieldName:"costItemDrop",
      initialValue:true,
    }
  ]
},{
  accordion:"Cost of Issuance",
  fields:[
    {
      colName:"Cost Item",
      abbr:"Cost Item",
      itemplaceholder:true,
      fieldName:"costItem",
      initialValue:"",
    },
    {
      colName:"$/1000",
      abbr:"Cost Item",
      itemplaceholder:false,
      fieldName:"costPerParBond",
      initialValue:"",
    },
    {
      colName:"Amount",
      abbr:"Cost Item",
      itemplaceholder:false,
      fieldName:"costAmount",
      initialValue:"",
    },
    {
      colName:"Drop",
      abbr:"Delete cost item",
      itemplaceholder:false,
      fieldName:"costItemDrop",
      initialValue:true,
    }
  ]
}]

const customOptions={
  prefix:"USD $ : ",
  numeral: true,
  numeralThousandsGroupStyle: "thousand",
  rawValueTrimPrefix: true
}

const mergeCheckInTrack = (checkInTrack, type, scheduleOfEvents) => {
  let initialData = []
  if(type === "scheduleOfEvents") {
    initialData = scheduleOfEvents || []
  }else {
    initialData = defaultData.map(({items},i) => {
      const {fields} = fieldDetails[i]
      return items.reduce( (finalData, item) => {
        const initialRowData = fields.reduce ( (row, {itemplaceholder,fieldName,initialValue}) => {
          const fieldValue = itemplaceholder ? item : initialValue
          return {...row,...{[fieldName]:fieldValue}}
        },{})
        initialRowData.dirty = false
        return [...finalData,initialRowData]
      },[])
    })
  }


  if(checkInTrack && Array.isArray(checkInTrack) && checkInTrack.length) {
    initialData.forEach((initCheckTrack, catIndex) => {
      initCheckTrack.forEach(initCheck => {
        let checkIndex
        if(type === "scheduleOfEvents") {
          checkIndex = checkInTrack[catIndex].findIndex(event => event.eventItem === initCheck.eventItem)
        }else {
          checkIndex = checkInTrack[catIndex].findIndex(cost => cost.costItem === initCheck.costItem)
        }
        if (checkIndex === -1) {
          checkInTrack[catIndex].push(initCheck)
        }
      })
    })
    return checkInTrack
  }
  return initialData
}

class DealCostOfIssuance extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions:[], dealIssueScheduleOfEvents: [], dealIssueCostOfIssuance:[], totalCosts:[],dealIssueCostOfIssuanceNotes:"", loading: true, activeTrack: "Cost of Issuance",checkTrack: false}
    this.toggleAccordion = this.toggleAccordion.bind(this)
    this.renderAccordionHeaders = this.renderAccordionHeaders.bind(this)
    this.renderAccordionBody = this.renderAccordionBody.bind(this)
    this.renderAccordionRow = this.renderAccordionRow.bind(this)
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.addAccordionrow = this.addAccordionrow.bind(this)
    this.resetAccordion = this.resetAccordion.bind(this)
    this.removeAccordionRow = this.removeAccordionRow.bind(this)
    this.handleNotesChange = this.handleNotesChange.bind(this)
  }

  componentWillMount() {
    this.getCheckInTrack()
  }

  componentDidMount(){
    const accordionInitate = defaultData.map((r,i) => ({accIndex:i,accOpen:false,accHeader:r.costOfIssCategory}))
    const accordionTotalInitiate = defaultData.map( (r,i) => ({amountTotal:0,dollarPerUnitBondTotal:0,errors:[false,false]}))
    const initialData = defaultData.map(({items},i) => {
      const {fields} = fieldDetails[i]
      return items.reduce( (finalData, item) => {
        const initialRowData = fields.reduce ( (row, {itemplaceholder,fieldName,initialValue}) => {
          const fieldValue = itemplaceholder ? item : initialValue
          return {...row,...{[fieldName]:fieldValue}}
        },{})
        initialRowData.dirty = false
        return [...finalData,initialRowData]
      },[])
    })
    this.setState({accordions:accordionInitate,dealIssueCostOfIssuance:initialData,totalCosts:accordionTotalInitiate})
  }

  getCheckInTrack = () => {
    fetchDealsTransaction(this.props.nav3, this.props.nav2, (res) => {
      if (res) {
        const totalCosts = []
        let items = []
        const dealIssueCostOfIssuance = res.dealIssueCostOfIssuance || []
        dealIssueCostOfIssuance.forEach(cost => {
          items.push(cost.costItemDetails)
          totalCosts.push({
            amountTotal: cost.totalCosts.costAmount || 0,
            dollarPerUnitBondTotal: cost.totalCosts.costPerParBond || 0,
            errors:[false,false]
          })
        })
        items = mergeCheckInTrack(items)
        this.setState({
          dealIssueCostOfIssuance: items,
          dealIssueScheduleOfEvents: res.dealIssueScheduleOfEvents || [],
          totalCosts,
          loading: false,
          dealIssueCostOfIssuanceNotes: res.dealIssueCostOfIssuanceNotes || ""
        }, () => this.computeTotals())
      }else {
        this.setState({
          loading: false,
        })
      }
    })
  }

  toggleAccordion =(aIndex) => {
    const { multiple } = this.props
    const { accordions } = this.state
    if(multiple) {
      const newMultipleState = accordions.map ( ({accIndex,accOpen,accHeader}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen,accHeader} : {accIndex,accOpen,accHeader})
      this.setState(  {accordions:newMultipleState} )
    } else {
      const newSingleState = accordions.map ( ({accIndex,accOpen,accHeader}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen,accHeader} : {accIndex,accOpen:false,accHeader})
      this.setState(  {accordions:newSingleState} )
    }
  }

  handleFieldChange = (aIndex,rowIndex,fieldPos, e) => {
    const {fieldName} = fieldDetails[aIndex].fields[fieldPos]
    const val =  e.target.name === "cleaveinput" ? e.target.rawValue : e.target.value
    const {dealIssueCostOfIssuance:newAccordionData} = this.state
    newAccordionData[aIndex][rowIndex][fieldName] = val
    this.setState( {dealIssueCostOfIssuance:newAccordionData},() => this.computeTotals())
  }

  handleNotesChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
  }

  addAccordionrow = (aIndex) => {
    // Open the accordion only if it is closed
    if(!this.state.accordions[aIndex].accOpen) {
      this.toggleAccordion(aIndex)
    }
    const newAccordionData=[...this.state.dealIssueCostOfIssuance]
    // Check if already there is a blank row
    const singleAccordionData = [...newAccordionData[aIndex]]
    const emptyRowExists = singleAccordionData.reduce( ( isAnyRowEmpty, row) => {
      const {dirty,costItemDrop,...maindata} = row
      const rowEmpty = Object.values(maindata).reduce( (isEmpty, field) => isEmpty && (field === ""),true)
      return isAnyRowEmpty || rowEmpty
    }, false)

    if(!emptyRowExists){
      const {fields} = fieldDetails[aIndex]
      const rowToInsert = fields.reduce ( (row, {fieldName,initialValue}) => ({...row,...{[fieldName]:initialValue}}),{})
      rowToInsert.dirty = true
      newAccordionData[aIndex].push(rowToInsert)
      this.setState({dealIssueCostOfIssuance:newAccordionData})
    }
  }

  removeAccordionRow = (aIndex, rowId, trackId, type) => {
    const newAccordionData = [...this.state.dealIssueCostOfIssuance]
    const singleAccordionDetail = [...this.state.dealIssueCostOfIssuance[aIndex]]
    const newAccordionDetail = singleAccordionDetail.filter( (row, ind) => rowId !== ind )
    newAccordionData[aIndex] = newAccordionDetail
    const result  = confirm("Are you sure?, you want to remove this row !")
    if(!result) return
    if(trackId && type) {
      removeDealsCheckInTrack(this.props.nav2, type, trackId, "costOfIssuance", (res) => {
        if(res && res.status === 200) {
          toast("Deals checkntrack row removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
          this.setState({dealIssueCostOfIssuance:newAccordionData})
        }else {
          toast("Something went wrong",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR })
        }
      })
    }else {
      toast("Deals checkntrack row removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
      this.setState({dealIssueCostOfIssuance:newAccordionData})
    }
  }

  resetAccordion = (aIndex) => {
    const newState = Object.assign({},this.state)
    const {dealIssueCostOfIssuance:newAccordionData} = newState
    const accordionRows = newAccordionData[aIndex]

    // find only those that are eligible for deletion
    const revisedRows = accordionRows.filter ( row => row.dirty === false )
    newAccordionData[aIndex] = revisedRows
    this.setState({dealIssueCostOfIssuance:newAccordionData})
  }

  resetNotes =() => {
    this.setState({dealIssueCostOfIssuanceNotes:""})
  }

  resetForm =() => {
    const {accordions} = this.state
    accordions.forEach( acc => this.resetAccordion(acc.accIndex))
    this.resetNotes()
    toast("All data will be reset to the previous saved state",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
  }

  handleSaveInformation = () => {
    const { accordions, dealIssueCostOfIssuanceNotes, totalCosts} = this.state
    const inValid = []
    this.state.dealIssueCostOfIssuance.forEach(initCheckTrack => {
      inValid.push(initCheckTrack.map(cost => cost.dirty && (!cost.costItem || !cost.costAmount || !cost.costPerParBond)))
    })

    if(inValid && ( inValid[0].indexOf(true) !== -1 || inValid[0].indexOf(true) !== -1)) {
      toast("Please costItem, costAmount and costPerParBond new items added",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      return
    }

    const dealIssueCostOfIssuance = this.state.dealIssueCostOfIssuance.reduce( (finalObject,data,index) => {
      const revisedData = data.map( ({dirty,costItemDrop,...saveFields }) => saveFields)
      const total = {costAmount: totalCosts[index].amountTotal, costPerParBond:totalCosts[index].dollarPerUnitBondTotal }
      return [...finalObject, { costOfIssCategory: accordions[index].accHeader, costItemDetails:revisedData, totalCosts: total}]
    },[])

    const payload = {
      dealIssueCostOfIssuance,
      dealIssueCostOfIssuanceNotes
    }

    putDealsCheckNTrack(this.props.nav2, payload, (res)=> {
      if (res && res.status === 201) {
        toast("Check in track Cost of Issuance updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
        this.getCheckInTrack()
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      }
    })
  }

  computeTotals = () => {
    // The sum for every row should not be greater than 10% for $Item
    // The sum for every field should not be greater than the overall facevalue on the deal
    const  dealIssueCostOfIssuance = [...this.state.dealIssueCostOfIssuance]
    const accordionTotals = dealIssueCostOfIssuance.map (singleAccordionDetail => {
      const accordionTotals = singleAccordionDetail.reduce ( (accTotal,{costAmount,costPerParBond}) => {
        const { amountTotal, dollarPerUnitBondTotal} = accTotal
        const newAmountTotal = amountTotal + parseFloat(costAmount || 0)
        const newDollarPerUnitBondTotal = dollarPerUnitBondTotal + parseFloat(costPerParBond || 0)
        const amountGreaterThanThreshold = newAmountTotal > 150000
        const percGreatherThanThreshold = newDollarPerUnitBondTotal > 0.20
        return { amountTotal:newAmountTotal, dollarPerUnitBondTotal:newDollarPerUnitBondTotal,errors:[percGreatherThanThreshold,amountGreaterThanThreshold]}
      },{amountTotal:0,dollarPerUnitBondTotal:0,errors:[]})
      return  accordionTotals
    } )

    this.setState({totalCosts:accordionTotals})

  }


  renderAccordionHeaders = (aIndex) => {
    const fieldsDetails = fieldDetails[aIndex]
    const {fields:fieldHeaders} = fieldsDetails
    return (
      <thead>
        <tr>
          {
            fieldHeaders.map ( ({colName,abbr}) => (
              <th key={colName}>
                <abbr title={abbr}>{colName}</abbr>
              </th>
            ))
          }
        </tr>
      </thead>
    )
  }


  renderAccordionBody =(aIndex, category) => {
    const singleAccordionDetail = [...this.state.dealIssueCostOfIssuance[aIndex]]
    console.log("THESE ARE ALL THE ROWS FOR A GIVEN ACCORDION",singleAccordionDetail)
    const {errors} = this.state.totalCosts[aIndex]


    const accordionTotals = singleAccordionDetail.reduce ( (accTotal,{costAmount,costPerParBond}) => {
      const { amountTotal, dollarPerUnitBondTotal} = accTotal
      const newAmountTotal = amountTotal + parseFloat(costAmount || 0)
      const newDollarPerUnitBondTotal = dollarPerUnitBondTotal + parseFloat(costPerParBond || 0)
      return { amountTotal:newAmountTotal, dollarPerUnitBondTotal:newDollarPerUnitBondTotal}
    },{amountTotal:0,dollarPerUnitBondTotal:0})

    console.log("THE ACCORDION TOTALS ARE :", accordionTotals)

    return (<tbody>
      {singleAccordionDetail.map( (row, rowindex) => this.renderAccordionRow(aIndex,rowindex,row, category))}
      <tr key={`rowtotal-${aIndex}`}>
        <td>
          Total
        </td>
        <td>
          {`$ ${numberWithCommas(accordionTotals.dollarPerUnitBondTotal)}`}
        </td>
        <td>
          {`$ ${numberWithCommas(accordionTotals.amountTotal)}`}
        </td>
      </tr>
      {errors[0] || errors[1] ? (<tr key={`rowerror-${aIndex}`}>
        <td />
        <td style={{color:"red"}}>
          {errors[0] ? "Total $/1000 is greater than 20%" : ""}
        </td>
        <td style={{color:"red"}}>
          {errors[1] ? "The total amount is greater than USD 150,000" : ""}
        </td>
      </tr>) : null}
    </tbody>)

  }


  renderAccordionRow = (aIndex,rowIndex,rowData,category) => {
    const {errors} = this.state.totalCosts[aIndex]
    console.log("ERRORS IN THE COLUMN",errors)

    const values = Object.values(rowData)
    return (
      <tr key={`${aIndex}-${rowIndex}`}>
        <td>
          {
            rowData.dirty ?
              <input className="input is-small is-link" type="text" value = {rowData.costItem || ""} placeholder="costItem" onChange={(e) => this.handleFieldChange(aIndex,rowIndex,0,e)}/>
              :
              rowData.costItem
          }
        </td>
        <td style={errors[0] ? {borderLeft:"3px solid red",borderRight:"3px solid red"}:{}}>
          <Cleave
            placeholder="$/1000"
            name="cleaveinput"
            type="text"
            value={rowData.costPerParBond || ""}
            className="input is-small is-link"
            options={customOptions}
            onChange={(e) => this.handleFieldChange(aIndex,rowIndex,1,e)}
          />
        </td>
        <td style={errors[1] ? {borderLeft:"3px solid red",borderRight:"3px solid red"} : {}}>
          <Cleave
            placeholder="$/1000"
            name="cleaveinput"
            type="text"
            value={rowData.costAmount || ""}
            className="input is-small is-link"
            options={customOptions}
            onChange={(e) => this.handleFieldChange(aIndex,rowIndex,2,e)}
          />
        </td>
        <td>
          <span className="has-text-link"  style={{cursor: "pointer"}} onClick={() => this.removeAccordionRow(aIndex,rowIndex,rowData._id || "", category)}>
            <i className="far fa-trash-alt" />
          </span>
        </td>
      </tr>
    )
  }

  render() {
    const { accordions, dealIssueCostOfIssuanceNotes, totalCosts, checkTrack, activeTrack, dealIssueScheduleOfEvents } = this.state
    const dealIssueCostOfIssuance = this.state.dealIssueCostOfIssuance.reduce( (finalObject,data,index) => {
      const revisedData = (Array.isArray(data) && data.map( ({dirty,costItemDrop,...saveFields }) => saveFields)) || []
      return [...finalObject, { costOfIssCategory:accordions[index].accHeader,costItemDetails:revisedData}]
    },[])

    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      accordions && <div className="container">
        <div className="column">
          <div className={`dropdown is-left ${checkTrack ? "is-active" : ""}`}>
            <div className="dropdown-trigger">
              <button className="button is-small is-success" onClick={() => this.setState({checkTrack: !checkTrack})}>
                <span>Linked checklists and trackers</span>
                <span className="icon is-small">
                  <i className="fas fa-angle-down" />
                </span>
              </button>
            </div>
            <div className="dropdown-menu" id="dropdown-menu2" role="menu">
              <div className="dropdown-content">
                <a className="dropdown-item" onClick={() => this.setState({activeTrack: "Cost of Issuance"})}>
                  <strong>Cost of Issuance
                  </strong>
                  <p>COI tracker used by Ford &amp; Associates, Inc.
                  </p>
                </a>
                <hr className="dropdown-divider" />
                <a className="dropdown-item" onClick={() => this.setState({activeTrack: "Schedule of Events"})}>
                  <strong>Schedule of Events
                  </strong>
                  <p>Schedule tracker for negotiated sales used by Ford &amp; Associates, Inc.
                  </p>
                </a>
              </div>
            </div>
          </div>
        </div>
        {activeTrack === "Cost of Issuance" &&
          <section className="accordions box">
            { accordions.map ( ({accOpen,accIndex,accHeader}) => <article key={accIndex} className={ accOpen ? "accordion is-active" : "accordion"}>
              <div className="accordion-header">
                <p onClick={() => this.toggleAccordion(accIndex) } style={{width:"100%"}}>{accHeader}</p>
                <div className="field is-grouped">
                  <div className="control">
                    <button className="button is-link is-small" onClick={() => this.addAccordionrow(accIndex)}>Add</button>
                  </div>
                  <div className="control">
                    <button className="button is-light is-small" onClick={() => this.resetAccordion(accIndex)}>Reset</button>
                  </div>
                </div>
              </div>
              {accOpen && <div className="accordion-body">
                <div className="accordion-content">
                  <div style={{overflowX:"auto"}}>
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      {this.renderAccordionHeaders(accIndex)}
                      {this.renderAccordionBody(accIndex, accHeader)}
                    </table>
                  </div>
                </div>
              </div>}
            </article>)}

            <article className="accordion is-active">
              <div className="accordion-header toggle">
                <p>Notes/Instructions</p>
              </div>
              <div className="accordion-body">
                <div className="accordion-content">
                  <div className="columns">
                    <div className="column is-full">
                      <div className="field">
                        <label className="label">
                          <abbr title="Notes and instructions by transaction participants">Notes/Instructions</abbr>
                        </label>
                        <div className="control">
                          <textarea className="textarea" placeholder="" name="dealIssueCostOfIssuanceNotes" value={dealIssueCostOfIssuanceNotes} onChange={this.handleNotesChange} />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="columns">
                    <div className="column is-full">
                      <div className="field is-grouped-center">
                        <div className="control">
                          <button className="button is-link" onClick={() => this.handleSaveInformation() }
                          >Save</button>
                        </div>
                        <div className="control">
                          <button className="button is-link" onClick={() => this.resetForm()}>Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </section>
        }
        {activeTrack === "Schedule of Events" &&
          <DealScheduleOfEvents nav2={this.props.nav2} mergeCheckInTrack={mergeCheckInTrack} getCheckInTrack={this.getCheckInTrack} dealIssueScheduleOfEvents={dealIssueScheduleOfEvents} dealIssueCostOfIssuanceNotes={dealIssueCostOfIssuanceNotes}/>
        }
      </div>
    )
  }
}

export default DealCostOfIssuance
