import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import swal from "sweetalert"
import moment from "moment"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TranSummaryStatus from "../../../../GlobalComponents/TranSummaryStatus"
import ActivitySummary from "./components/ActivitySummary"
import OpportunityResult from "./components/OpportunityResult"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import {
  putMarfpTransaction,
  putMarfpStatusAndSec
} from "../../../../StateManagement/actions"
import CONST, { oppMsg } from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import { ResultValidate } from "../Validation/MarfpDetailsValidate"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import { pullRelatedTransactions } from "../../../../StateManagement/actions/CreateTransaction"
import ReletedTranGlob from "../../../../GlobalComponents/ReletedTranGlob"

class Summary extends React.Component {
  constructor() {
    super()
    this.state = {
      maRfpResult: {
        actResult: "",
        actClosingNotes: ""
      },
      relatedMaRfp: {
        actTranRelatedType: "",
        actRelTrans: []
      },
      tranTextChange: {
        actIssueName: "",
        actProjectName: ""
      },
      errorMessages: {},
      transaction: {},
      dropDown: {
        tranStatus: [],
        actSecurityType: [],
        actSaleType: [],
        relatedTypes: [],
        opportunities: []
      },
      relatedTransactions: [],
      loading: true,
      isSaveDisabled: false,
      isResultDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  async componentDidMount() {
    const { transaction } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPSWAPTRANSTATUS",
      "LKUPSECTYPEGENERAL",
      "LKUPRELATEDTYPE",
      "LKUPOPPORTUNITYRESULT",
      "LKUPDEALOFFERING"
    ])
    const result = (picResult.length && picResult[1]) || {}

    this.setState(prevState => ({
      tranTextChange: {
        actIssueName: transaction.actIssueName || "",
        actProjectName: transaction.actProjectName || ""
      },
      transaction,
      relatedTransactions: transaction.actRelTrans || [],
      dropDown: {
        ...this.state.dropDown,
        relatedTypes: result.LKUPRELATEDTYPE || ["Bond Hedge", "Loan Hedge"],
        actSecurityType: result.LKUPSECTYPEGENERAL || [],
        actSaleType: result.LKUPDEALOFFERING || [],
        tranStatus: result.LKUPSWAPTRANSTATUS || [],
        opportunities: result.LKUPOPPORTUNITYRESULT || []
      },
      maRfpResult:
        (transaction && transaction.maRfpResult) || prevState.maRfpResult,
      loading: false,
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actIssueName ||
          transaction.actProjectName} - Notification`
      }
    }))
  }

  onRelatedTranSelect = item => {
    const { relatedMaRfp } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary related type ${
        relatedMaRfp.actTranRelatedType
      } and  related transaction is ${item.name || ""}`,
      date: new Date(),
      key: "summary"
    })
    this.setState(prevState => ({
      relatedMaRfp: {
        ...prevState.relatedMaRfp,
        actRelTrans: item
      }
    }))
  }

  onChange = e => {
    const { relatedMaRfp } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: user.userFirstName,
      log: `In summary select related type ${e.target.value}`,
      date: new Date(),
      key: "summary"
    })
    relatedMaRfp[e.target.name] = e.target.value
    this.setState({
      relatedMaRfp: {
        ...relatedMaRfp,
        actRelTrans: []
      }
    })
  }

  onChangeItem = (item, category) => {
    this.setState(prevState => ({
      [category]: {
        ...prevState[category],
        ...item
      }
    }))
  }

  onResultSave = () => {
    const { maRfpResult } = this.state
    /*if(!maRfpResult.actResult || !maRfpResult.actClosingNotes) return false*/
    const errors = ResultValidate(maRfpResult)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, details: errorMessages },
        activeItem: [0, 0]
      }))
      return
    }

    this.setState(
      {
        isResultDisabled: true
      },
      () => {
        putMarfpTransaction(
          "summary",
          `${this.props.nav2}?subType=result`,
          { maRfpResult },
          res => {
            if (res && res.status === 200) {
              toast("Successfully updated transaction", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(prevState => ({
                maRfpResult:
                  (res.data && res.data.maRfpResult) || prevState.maRfpResult,
                transaction: {
                  ...prevState.transaction,
                  maRfpResult:
                    (res.data && res.data.maRfpResult) || (prevState.transaction && prevState.transaction.maRfpResult) || {}
                },
                isResultDisabled: false
              }))
            } else {
              toast("something went wrong", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isResultDisabled: false
              })
            }
          }
        )
      }
    )
  }

  onModalSave = () => {
    this.setState({
      modalState: true,
      relatedSave: true
    })
  }

  onRelatedtranSave = () => {
    const { email, relatedMaRfp } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    const actRelTrans = {
      ...relatedMaRfp.actRelTrans,
      relType: relatedMaRfp.actTranRelatedType
    }

    if (!relatedMaRfp.actTranRelatedType || actRelTrans.length === 0)
      return false

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putMarfpTransaction(
          this.props.nav3,
          `${this.props.nav2}?subType=related`,
          actRelTrans,
          res => {
            if (res && res.status === 200) {
              toast("Add related transaction successfully", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  transaction: res.data,
                  relatedTransactions: (res.data && res.data.actRelTrans) || [],
                  relatedMaRfp: {
                    actTranRelatedType: relatedMaRfp.actTranRelatedType,
                    actRelTrans: ""
                  },
                  isSaveDisabled: false
                },
                async () => {
                  await sendEmailAlert(emailPayload)
                }
              )
            } else {
              toast("something went wrong", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false
              })
            }
          }
        )
      }
    )
  }

  onChangeTextInput = e => {
    let tranTextChange = this.state.tranTextChange
    this.setState({
      tranTextChange: {
        ...tranTextChange,
        [e.target.name]: e.target.value
      }
    })
  }

  onBlur = e => {
    const { transaction, tranTextChange } = this.state
    if (transaction[e.target.name] !== tranTextChange[e.target.name]) {
      this.onInputChange(e)
    }
  }

  onInputChange = e => {
    const { transaction } = this.state
    const { user } = this.props
    let state = {}
    let maRfpSummary = {}
    let { name, value, title } = e.target
    const statusChange =
      name === "actTranStatus" || name === "actStatus"
    if (name === "actTranStatus") {
      name = "actStatus"
    }
    if (name === "actSecurityType" || name === "actOfferingType") {
      maRfpSummary = {
        maRfpSummary: {
          ...transaction.maRfpSummary,
          [name]: value
        }
      }
    } else {
      maRfpSummary = { [name]: value }
    }

    if (statusChange) {
      if (transaction.opportunity && transaction.opportunity.status && value !== "In Progress") {
        maRfpSummary[name] = value === "Won" ? "Pre-Pricing" : value
        maRfpSummary.opportunity = {
          ...transaction.opportunity,
          status: false,
          isWon: value === "Won"
        }
      }
      if(value === "Cancelled" || value === "Closed"){
        const msg = "You will not be able to EDIT any part of the transaction post this action. Are you sure?"
        swal({
          title: "",
          text: msg,
          icon: "warning",
          buttons: ["Cancel", "Yes"]
        }).then(willDo => {
          if (willDo) {
            state = {
              maRfpSummary,
              statusChange,
              modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress")
            }
            this.setState({
              ...state,
              relatedSave: false
            })
          }
        })
      } else {
        state = {
          maRfpSummary,
          statusChange,
          modalState: !(transaction.opportunity && transaction.opportunity.status && value !== "In Progress")
        }
      }
    } else {
      state = {
        maRfpSummary
      }
    }
    this.setState(
      {
        ...state,
        relatedSave: false
      },
      () => {
        if (statusChange) {
          if (transaction.opportunity && transaction.opportunity.status) {
            if (value === "Won") {
              swal({
                title: "",
                text: oppMsg.won,
                icon: "warning",
                buttons: ["Cancel", "Yes, won it!"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            } else {
              const msg =
                value === "Lost"
                  ? oppMsg.lost
                  : oppMsg.cancel
              swal({
                title: "",
                text: msg,
                icon: "warning",
                buttons: ["Cancel", "Yes"]
              }).then(willDo => {
                if (willDo) {
                  this.addAuditAndSave(title, value, transaction)
                }
              })
            }
          }else {
            this.props.addAuditLog({
              userName: user.userFirstName,
              log: `In summary transaction ${title ||
              "empty"} change to ${value}`,
              date: new Date(),
              key: "summary"
            })
          }
        } else {
          this.addAuditAndSave(title, value, transaction)
        }
      }
    )
  }

  addAuditAndSave = (title, value, transaction) => {
    const {user} = this.props
    if (title && value) {
      this.props.addAuditLog({
        userName: user.userFirstName,
        log: `In summary transaction ${title ||
        "empty"} change to ${value}`,
        date: new Date(),
        key: "summary"
      })
    }
    this.onConfirmationSave(transaction)
  }

  onConfirmationSave = () => {
    const { email, transaction, maRfpSummary, statusChange } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState(
      {
        modalState: false
      },
      () =>
        putMarfpStatusAndSec(transaction._id, maRfpSummary, res => {
          //eslint-disable-line
          if (res && res.status === 201) {
            toast("Successfully updated transaction", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(transaction._id)
            if (statusChange) {
              this.setState(
                {
                  statusChange: false,
                  transaction: {
                    ...this.state.transaction,
                    ...maRfpSummary
                  }
                },
                async () => {
                  this.props.onStatusChange(maRfpSummary.actStatus)
                  await sendEmailAlert(emailParams)
                }
              )
            } else {
              this.setState({
                statusChange: false,
                transaction: {
                  ...transaction,
                  ...maRfpSummary
                }
              })
            }
          } else {
            toast("Something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
          }
        })
    )
  }

  onRelatedtranCancel = () => {
    this.setState({
      relatedMaRfp: {
        actTranRelatedType: "",
        actRelTrans: []
      },
      maRfpResult: {
        actResult: "",
        actClosingNotes: ""
      },
      errorMessages: {}
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  pdfDownload = () => {
    const { transaction } = this.state
    const {user, logo} = this.props
    const position = user && user.settings
    const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
    const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actNotes || "--"
    let counterFirmName = ""
    if (transaction.derivativeCounterparties) {
      transaction.derivativeCounterparties.map(item => {
        counterFirmName += `${item.cntrPartyFirmName},`
      })
    }
    let participants = ""
    if (transaction.maRfpParticipants) {
      transaction.maRfpParticipants.forEach(item => {
        participants += `${item.partType} : ${item.partFirmName} ${item.partContactName ? `(${item.partContactName})` : ""},\n`
      })
    }
    const tableData = [
      {
        titles: ["Summary"],
        description: "",
        headers: [
          "Issuer name",
          "Transaction Name",
          "Purpose/Project",
          "Principal Amount / Par Value",
          "Security",
          "Coupon Type",
          "Swaps",
          "Tenor/Maturities",
          "Lead Advisors",
          "Sale Type",
          "Schedule Highlights",
          "Participants",
          "Related Transactions"
        ],
        rows: [
          [
            `${
              transaction.actIssuerClientEntityName
                ? transaction.actIssuerClientEntityName
                : "--"
            }`,
            `${transaction.actIssueName ? transaction.actIssueName : "--"}`,
            `${transaction.actProjectName ? transaction.actProjectName : "--"}`,
            `${
              transaction.maRfpSummary.actParAmount
                ? `$${transaction.maRfpSummary.actParAmount}`
                : "--"
            }`,
            `${
              transaction.maRfpSummary.actSecurityType
                ? transaction.maRfpSummary.actSecurityType
                : "--"
            }`,
            `${transaction.derivativeCounterparties ? counterFirmName : "--"}`,
            `${
              transaction.maRfpSummary.tranNotionalAmt
                ? transaction.maRfpSummary.tranNotionalAmt
                : 0
            }`,
            `${
              transaction.maRfpSummary.tranEndDate
                ? moment(transaction.maRfpSummary.tranEndDate).format(
                  "MM-DD-YYYY"
                )
                : "--"
            }`,
            `${
              transaction.actLeadFinAdvClientEntityName
                ? transaction.actLeadFinAdvClientEntityName
                : "--"
            }`,
            `${transaction.actSaleType ? transaction.actSaleType : "--"}`,
            "--",
            `${transaction.maRfpParticipants ? participants : "--"}`,
            `${actNotes}`
          ]
        ]
      }
    ]
    pdfTableDownload("", tableData, `${transaction.actIssueName || transaction.actProjectName || ""} summary.pdf`, logo, position)
  }

  deleteRelatedTran = async (relatedId, relTranId, relType, relatedType) => {
    const { transaction } = this.props
    const query = `ActMaRFP?tranId=${transaction._id}&relatedId=${relatedId}&relTranId=${relTranId}&relType=${relType}&relatedType=${relatedType}`
    const related = await pullRelatedTransactions(query)
    if(related && related.done){
      toast("related transaction removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState({
        relatedTransactions: related && related.relatedTransaction || []
      })
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  render() {
    const {
      modalState,
      email,
      relatedMaRfp,
      relatedTransactions,
      transaction,
      errorMessages,
      dropDown,
      maRfpResult,
      isSaveDisabled,
      isResultDisabled,
      relatedSave,
      tranTextChange
    } = this.state
    const { tranAction, participants, onParticipantsRefresh, nav2 } = this.props
    const canEditTran = (this.props.tranAction && this.props.tranAction.canEditTran) || false
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={
            relatedSave === true
              ? this.onRelatedtranSave
              : this.onConfirmationSave
          }
        />
        <TranSummaryStatus
          tranStatus={dropDown.tranStatus || []}
          disabled={!canEditTran}
          selectedStatus={transaction.actStatus || ""}
          pdfDownload={this.pdfDownload}
          tranName={transaction.actUniqIdentifier || ""}
          transaction={transaction}
          onChange={this.onInputChange}
        />
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Activity Summary"
              >
                {activeAccordions.includes(0) && (
                  <ActivitySummary
                    canEditTran={canEditTran}
                    transaction={transaction}
                    onChange={this.onInputChange}
                    onTextInputChange={this.onChangeTextInput}
                    onBlur={this.onBlur}
                    dropDown={dropDown}
                    tranTextChange={tranTextChange}
                  />
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Related transactions"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(1) && (
                  <ReletedTranGlob
                    relatedTypes={dropDown.relatedTypes || []}
                    transaction={transaction}
                    errorMessages={errorMessages}
                    isSaveDisabled={isSaveDisabled}
                    item={relatedMaRfp}
                    tranId={nav2}
                    canEditTran={tranAction.canEditTran || false}
                    onRelatedTranSelect={this.onRelatedTranSelect}
                    onChange={this.onChange}
                    onRelatedtranSave={this.onModalSave}
                    onRelatedtranCancel={this.onRelatedtranCancel}
                    relatedTransactions={relatedTransactions}
                    deleteRelatedTran={this.deleteRelatedTran}
                  />
                )}
              </RatingSection>
              {canEditTran || (!canEditTran && maRfpResult.actResult !== "") ? (
                <RatingSection
                  onAccordion={() => onAccordion(2)}
                  title="Opportunity Result"
                >
                  {activeAccordions.includes(2) && (
                    <OpportunityResult
                      dropDown={dropDown}
                      transaction={transaction}
                      item={maRfpResult}
                      errorMessages={errorMessages}
                      isResultDisabled={isResultDisabled}
                      canEditTran={canEditTran}
                      onRelatedTranSelect={this.onRelatedTranSelect}
                      category="maRfpResult"
                      onResultSave={this.onResultSave}
                      onChangeItem={this.onChangeItem}
                      onRelatedtranCancel={this.onRelatedtranCancel}
                    />
                  )}
                </RatingSection>
              ) : null}
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(Summary)

export default connect(mapStateToProps, null)(WrappedComponent)
