import React from "react"
import NumberFormat from "react-number-format"
import moment from "moment/moment"
import { SelectLabelInput, TextLabelInput } from "../../../../../GlobalComponents/TextViewBox"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"


const ActivitySummary = ({ transaction = {}, dropDown, canEditTran, onChange, onTextInputChange, onBlur, tranTextChange }) => {

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actNotes || "--"

  return(
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          {transaction.actIssuerClient ?
            <small style={{fontSize: 15}}>{getTranEntityUrl("Issuer", transaction.actIssuerClientEntityName, transaction.actIssuerClient, "")}</small> : <small>--</small>
          }
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Transaction Name</p>
        </div>
        <div className="column">
          <TextLabelInput title="Transaction Name" name="actIssueName" value={tranTextChange.actIssueName} onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actStatus === "Closed" || transaction.actStatus === "Cancelled" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Purpose/ Project</p>
        </div>
        <div className="column">
          <TextLabelInput title="Purpose/ Project" name="actProjectName" value={tranTextChange.actProjectName} onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actStatus === "Closed" || transaction.actStatus === "Cancelled" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Principal Amount / Par Value</p>
        </div>
        <div className="column">
          <small>{transaction.maRfpSummary && transaction.maRfpSummary.actParAmount ?
            <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={transaction.maRfpSummary.actParAmount} />
            :"---"}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Security</p>
        </div>
        <div className="column">
          <SelectLabelInput list={dropDown.actSecurityType} name="actSecurityType" disabled={!canEditTran} onChange={onChange} value={(transaction.maRfpSummary && transaction.maRfpSummary.actSecurityType || "")}  />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon Type</p>
        </div>
        <div className="column">
          <p>
            {
              transaction.derivativeCounterparties ? transaction.derivativeCounterparties.map((item, i) => (
                <small key={i}><a> {item.cntrPartyFirmName}{transaction.derivativeCounterparties.length - 1 === i ? "" : ","} </a></small>
              )) : null
            }
          </p>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Swaps</p>
        </div>
        <div className="column">
          <small>{transaction.maRfpSummary && transaction.maRfpSummary.tranNotionalAmt ?
            <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={transaction.maRfpSummary.tranNotionalAmt} />
            :"---"}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small> {(transaction.maRfpSummary && transaction.maRfpSummary.tranEndDate ? moment(transaction.maRfpSummary.tranEndDate).format("MM-DD-YYYY") || "" : "")}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Lead Advisors</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{transaction.actLeadFinAdvClientEntityName || "--"}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Sale Type</p>
        </div>
        <div className="column">
          <SelectLabelInput list={dropDown.actSaleType} title="Sale Type" name="actOfferingType" value={(transaction && transaction.maRfpSummary && transaction.maRfpSummary.actOfferingType) || "--"} disabled={!canEditTran} onChange={onChange} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule Highlights</p>
        </div>
        <div className="column">
          <p>
            <small>
              {/* {(transaction.derivTradeDealerPayLeg && transaction.derivTradeDealerPayLeg.effDate ? moment(transaction.derivTradeDealerPayLeg.effDate).format("MM-DD-YYYY") || "--" : "--")} */}
              {"--"}
            </small>
          </p>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Participants</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">
            {transaction.maRfpParticipants
              ? transaction.maRfpParticipants.map((item, i) => (
                <p key={i}>
                  {item.partContactId ? (
                    <div style={{fontSize: 15}}>{getTranUserUrl(item.partType, `${item.partType} : ${item.partFirmName} (${item.partContactName})${transaction.maRfpParticipants.length - 1 === i ? "" : ","}`, item.partContactId, "")}</div>
                  ) :
                    <div style={{fontSize: 15}}>{getTranEntityUrl(item.partType, `${item.partFirmName}${transaction.maRfpParticipants.length - 1 === i ? "" : ","}`, item.partFirmId, "")}</div>
                  }
                </p>
              ))
              : null}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Related Transactions</p>
        </div>
        <div className="column">
          <small>{(actNotes || "--")}</small>
        </div>
      </div>
    </div>
  )

}


export default ActivitySummary
