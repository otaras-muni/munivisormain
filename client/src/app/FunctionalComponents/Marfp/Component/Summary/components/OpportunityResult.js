import React from "react"
import {SelectLabelInput} from "../../../../../GlobalComponents/TextViewBox"

const OpportunityResult = ({ onChangeItem, transaction, item={}, category, dropDown, canEditTran, errorMessages, onResultSave, onRelatedtranCancel, isResultDisabled}) => {

  const onChange = (event) => {
    onChangeItem({
      ...item,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    }, category)
  }
  const result = (transaction && transaction.maRfpResult) || {}
  return (
    <div>
      <div className="columns">
        <div className="column">
          <SelectLabelInput error={errorMessages.actResult} list={dropDown.opportunities || []}  name="actResult" value={(item && item.actResult) || ""} disabled={!canEditTran || result.actResult} onChange={onChange} inputStyle={{width: 100}} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-full">
          <p className="multiExpLbl">Notes / Instructions
            <span className='icon has-text-danger'>
              <i className='fas fa-asterisk extra-small-icon'/></span>
          </p>
          <div className="control">
            <textarea className="textarea" disabled={!canEditTran || result.actResult} onChange={onChange} value={(item && item.actClosingNotes) || ""} name="actClosingNotes"
              placeholder="Enter Closing Command or notes"/>
            {errorMessages && errorMessages.actClosingNotes &&
          <small className="text-error">{errorMessages.actClosingNotes}</small>}
          </div>
        </div>
      </div>
      {
        canEditTran && !(result && result.actResult) ?
          <div className="columns">
            <div className="column">
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link is-small" onClick={onResultSave} disabled={isResultDisabled}>Save</button>
                </div>
                <div className="control">
                  <button className="button is-light is-small" onClick={onRelatedtranCancel}>Cancel</button>
                </div>
              </div>
            </div>
          </div>
          : null
      }
    </div>
  )}

export default OpportunityResult
