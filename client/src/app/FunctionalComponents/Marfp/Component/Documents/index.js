import React from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TranDocuments from "../../../../GlobalComponents/TranDocuments"
import { putMarfpDocumentTransaction } from "../../../../StateManagement/actions/"
import {
  pullTransactionDoc,
  putTransactionDocStatus
} from "../../../../StateManagement/actions/Transaction"
import {getDocumentList} from "../../../../../globalutilities/helpers"

class Documents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      documentsList: [],
      loading: true
    }
  }

  async componentWillMount() {
    const { transaction, user } = this.props
    const marfpDocument = transaction && transaction.maRfpDocuments && transaction.maRfpDocuments.length ? cloneDeep(transaction.maRfpDocuments) : []
    const filterDocuments = getDocumentList(cloneDeep(marfpDocument), user)

    this.setState({
      documentsList: transaction.actTranFirmId === user.entityId ? marfpDocument : filterDocuments || [],
      loading: false,
    })
  }

  onDocSave = (docs, callback) => {
    const { transaction, user } = this.props
    console.log(docs)
    putMarfpDocumentTransaction(this.props.nav3, docs, res => {
      if (res && res.status === 200) {
        toast("MaRfp Documents has been updated!", {autoClose: CONST.ToastTimeout,type: toast.TYPE.SUCCESS})
        this.props.submitAuditLogs(this.props.nav2)
        const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.maRfpDocuments), user)
        callback({
          status: true,
          documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.maRfpDocuments : filterDocuments || [],
        })
      } else {
        toast("Something went wrong!", {autoClose: CONST.ToastTimeout,type: toast.TYPE.ERROR})
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    const { transaction, user } = this.props
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        [name]: value
      }
      if(value !== "Public"){
        document.settings = {firms: [], users: [], selectLevelType: ""}
      }
      type = "&type=status"
    } else {
      document = {
        ...doc
      }
    }
    const res = await putTransactionDocStatus(this.props.nav2, `ActMaRFP${type}`, { _id: doc._id, ...document })
    if (res && res.status === 200) {
      toast("Document has been updated!", {autoClose: CONST.ToastTimeout,type: toast.TYPE.SUCCESS})
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.document), user)
      if (name) {
        callback({
          status: true
        })
      } else {
        callback({
          status: true,
          documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.document : filterDocuments || [],
        })
      }
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  }

  onDeleteDoc = async (documentId, callback) => {
    const { transaction, user } = this.props
    const res = await pullTransactionDoc(this.props.nav2, `?tranType=ActMaRFP&docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully", {autoClose: CONST.ToastTimeout,type: toast.TYPE.SUCCESS})
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(cloneDeep(res.data && res.data.maRfpDocuments), user)
      callback({
        status: true,
        documentsList: transaction.actTranFirmId === user.entityId ? res.data && res.data.maRfpDocuments : filterDocuments || [],
      })
    } else {
      toast("Something went wrong!", {autoClose: CONST.ToastTimeout,type: toast.TYPE.ERROR})
      callback({
        status: false
      })
    }
  }

  render() {
    const { documentsList } = this.state
    const { transaction, tranAction } = this.props
    const tags = {
      clientId: transaction.actIssuerClient,
      clientName: `${transaction.actIssuerClientEntityName}`,
      tenantId: transaction.actTranFirmId,
      tenantName: transaction.actTranFirmName,
      contextName: transaction.actIssueName ? transaction.actIssueName : transaction.actProjectName
    }
    const loading = <Loader />

    if (this.state.loading) {
      return loading
    }
    return (
      <div className="derivative-documents">
        <TranDocuments
          {...this.props}
          isDisabled={!tranAction.canTranStatusEditDoc}
          onSave={this.onDocSave}
          nav1={this.props.nav1}
          tranId={this.props.nav2}
          transaction={transaction}
          title="Upload MaRfp Documents"
          pickCategory="LKUPDOCCATEGORIES"
          pickSubCategory="LKUPCORRESPONDENCEDOCS"
          pickAction="LKUPDOCACTION"
          category="maRfpDocuments"
          documents={documentsList}
          contextType={ContextType.maRfp}
          tags={tags}
          onStatusChange={this.onStatusChange}
          onDeleteDoc={this.onDeleteDoc}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(Documents)
export default withRouter(connect(mapStateToProps,null)(WrappedComponent))
