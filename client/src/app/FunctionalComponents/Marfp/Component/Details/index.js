import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import Accordion from "../../../../GlobalComponents/Accordion"
import { DetailsValidate } from "../Validation/MarfpDetailsValidate"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import KeyOpportunity from "./components/KeyOpportunity"
import { putMarfpTransaction, postMARFPNotes } from "../../../../StateManagement/actions"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, { states } from "../../../../../globalutilities/consts"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import HistoricalData from "../../../../GlobalComponents/HistoricalData"
import TransactionNotes from "../../../../GlobalComponents/TransactionNotes"

class MarfpDetails extends React.Component {
  constructor() {
    super()
    this.state = {
      maRfpSummary: CONST.Marfp.maRfpSummary,
      tempMaRfpSummary: CONST.Marfp.maRfpSummary,
      userName: "",
      errorMessages: {},
      tempTransaction: {},
      usersList: [],
      tranNotes: [],
      states: [...states],
      dropDown: {
        primarySector: {},
        actPlaceholder1: [],
        actPlaceholder2: [],
        actOppType: [],
        actOppStatus: [],
        actOfferingType: [],
        actSecurityType: [],
        actBankQualified: [],
        actCorpType: [],
        actUseOfProceeds: []
      },
      loading: true,
      isSaveDisabled: false,
      activeItem: [],
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  async componentDidMount() {
    const { transaction, tranAction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    const picResult = await getPicklistByPicklistName([
      "LKUPPRIMARYSECTOR",
      // "LKUPCOUNTRY",
      "LKUPPLACEHOLDER",
      "LKUPMSRBFIRMNAME",
      "LKUPDEALOFFERING",
      "LKUPCORPTYPE",
      "LKUPSOEEVENTSTATUS",
      "LKUPTRANSACTIONTYPE",
      "LKUPSECTYPEGENERAL",
      "LKUPUSEOFPROCEED",
      "LKUPBANKQUALIFIED",
    ])
    const result = (picResult.length && picResult[1]) || {}
    const primarySec = picResult[2].LKUPPRIMARYSECTOR || {}
    // const stateCountry =  (picResult[2] && picResult[2].LKUPCOUNTRY) || {}
    const stateCountry = /*(picResult[3] && picResult[3].LKUPCOUNTRY) ||*/ {}
    this.setState(prevState => ({
      dropDown: {
        ...this.state.dropDown,
        primarySectors: result.LKUPPRIMARYSECTOR || [],
        secondarySectors: primarySec,
        stateCountry,
        actPlaceholder1: result.LKUPPLACEHOLDER || [],
        actPlaceholder2: result.LKUPPLACEHOLDER || [],
        actOppType: result.LKUPTRANSACTIONTYPE || [],
        actOppStatus: result.LKUPSOEEVENTSTATUS || [],
        actOfferingType: result.LKUPDEALOFFERING || [],
        actSecurityType: result.LKUPSECTYPEGENERAL || [],
        actBankQualified: result.LKUPBANKQUALIFIED || [],
        actCorpType: result.LKUPCORPTYPE || [],
        actUseOfProceeds: result.LKUPUSEOFPROCEED || [],
        actIssuer: result.LKUPMSRBFIRMNAME || [],
      },
      /* maRfpSummary : (transaction && transaction.maRfpSummary) || CONST.Marfp.maRfpSummary, */
      maRfpSummary:
          transaction && transaction.maRfpSummary
            ? {
              ...transaction.maRfpSummary,
              actState: transaction.maRfpSummary.actState || userState,
              actCounty: transaction.maRfpSummary.actCounty || userCounty,
            }
            : {
              ...prevState.maRfpSummary,
              actState: transaction.maRfpSummary.actState || userState,
              actCounty: transaction.maRfpSummary.actCounty || userCounty,
            },
      tempMaRfpSummary:
        (transaction && transaction.maRfpSummary) || CONST.Marfp.maRfpSummary,
      userName: (this.props.user && this.props.user.userFirstName) || "",
      loading: false,
      transaction,
      tranNotes: transaction && transaction.tranNotes || [],
      tempTransaction: {
        actSecondarySector: transaction.actSecondarySector,
        actPrimarySector: transaction.actPrimarySector,
        actIssuerClientEntityName: transaction.actIssuerClientEntityName,
        actIssuerClient: transaction.actIssuerClient,
        actIssuerClientMsrbType: transaction.actIssuerClientMsrbType,
        actIssueName: transaction.actIssueName,
        actProjectName: transaction.actProjectName
      },
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actIssueName ||
          transaction.actProjectName} - Notification`
      }
    }))
  }

  onChange = (item, category, index) => {
    const items = this.state[category]
    items.splice(index, 1, item)
    this.setState({
      [category]: items
    })
  }

  onChangeItem = (item, category) => {
    this.setState(prevState => ({
      [category]: {
        ...prevState[category],
        ...item
      }
    }))
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `${change} from details`,
      date: new Date(),
      key: category
    })
  }

  onCancel = () => {
    const { tempMaRfpSummary } = this.state
    const { transaction, tranAction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    this.setState(prevState => ({
      maRfpSummary: {...tempMaRfpSummary,
                     actState: transaction.maRfpSummary.actState || userState,
                     actCounty: transaction.maRfpSummary.actCounty || userCounty},
      errorMessages: {},
      tempTransaction: {
        actSecondarySector: transaction.actSecondarySector,
        actPrimarySector: transaction.actPrimarySector,
        actIssuerClientEntityName: transaction.actIssuerClientEntityName,
        actIssuerClient: transaction.actIssuerClient,
        actIssuerClientMsrbType: transaction.actIssuerClientMsrbType,
        actIssueName: transaction.actIssueName,
        actProjectName: transaction.actProjectName
      },
    }))
  }

  onSave = () => {
    const { maRfpSummary, tempTransaction } = this.state
    const payload = { maRfpSummary, ...tempTransaction }
    const errors = DetailsValidate(payload, maRfpSummary.createdAt)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, details: errorMessages },
        activeItem: [0, 0]
      }))
      return
    }
    console.log("===============>", payload)

    this.setState({
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const { email, maRfpSummary, tempTransaction } = this.state
    const payload = { maRfpSummary, ...tempTransaction }
    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    console.log("==============email send to ==============", emailParams)
    this.setState(
      {
        isSaveDisabled: true,
        modalState: false,
        errorMessages: {}
      },
      () => {
        putMarfpTransaction("details", this.props.nav2, payload, res => {
          if (res && res.status === 200) {
            toast("Add MA-RFP Details successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(this.props.nav2)
            this.setState(
              {
                maRfpSummary:
                  (res.data && res.data.maRfpSummary) ||
                  CONST.Marfp.maRfpSummary,
                tempMaRfpSummary:
                  (res.data && res.data.maRfpSummary) ||
                  CONST.Marfp.maRfpSummary,
                isSaveDisabled: false,
                activeItem: [],
              },
              async () => {
                await sendEmailAlert(emailParams)
              }
            )
          } else {
            toast("something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false,
              activeItem: []
            })
          }
        })
      }
    )
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  getCountryDetails = (cityStateCountry = "") => {
    const { maRfpSummary } = this.state
    if (cityStateCountry && cityStateCountry.state) {
      maRfpSummary.actState = cityStateCountry.state.trim()
    }else {
      maRfpSummary.actState = ""
    }
    if (cityStateCountry && cityStateCountry.city) {
      maRfpSummary.actCounty = cityStateCountry.city.trim()
    }else {
      maRfpSummary.actCounty = ""
    }
    this.setState({
      maRfpSummary
    })
  }

  onNotesSave = (payload, callback) => {
    postMARFPNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add MA-RFP Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        // this.props.submitAuditLogs(this.props.nav2)
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  render() {
    const {
      modalState,
      email,
      maRfpSummary,
      errorMessages,
      dropDown,
      activeItem,
      tempTransaction,
      tranNotes,
      isSaveDisabled
    } = this.state
    const {
      transaction,
      tranAction,
      participants,
      onParticipantsRefresh
    } = this.props
    const canEditTran =
      (this.props.tranAction && this.props.tranAction.canEditTran) || false
    const desc = transaction.actTranNotes
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <SendEmailModal
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={this.onConfirmationSave}
        />
        <Accordion
          multiple
          isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Key Opportunity Parameters"
              >
                {activeAccordions.includes(0) && (
                  <KeyOpportunity
                    transaction={tempTransaction}
                    description={desc}
                    errorMessages={errorMessages}
                    item={maRfpSummary}
                    dropDown={dropDown}
                    canEditTran={canEditTran}
                    onChangeItem={this.onChangeItem}
                    category="maRfpSummary"
                    onBlur={this.onBlur}
                    getCountryDetails={this.getCountryDetails}
                  />
                )}
              </RatingSection>
              {tranAction && tranAction.canEditTran ? (
                <div className="column is-full" style={{ marginTop: "25px" }}>
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button
                        className="button is-link"
                        onClick={this.onSave}
                        disabled={isSaveDisabled || false}
                      >
                        Save
                      </button>
                    </div>
                    <div className="control">
                      <button
                        className="button is-light"
                        onClick={this.onCancel}
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          )}
        />
        { transaction.OnBoardingDataMigrationHistorical ? <HistoricalData data={(transaction && transaction.historicalData) || {}}/> : null}
        <TransactionNotes
          notesList={tranNotes || []}
          canEditTran={canEditTran}
          onSave={this.onNotesSave}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(MarfpDetails)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
