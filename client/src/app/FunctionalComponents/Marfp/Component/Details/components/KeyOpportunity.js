import React from "react"
import moment from "moment"
import { DropdownList } from "react-widgets"
import {
  TextLabelInput,
  SelectLabelInput,
  NumberInput
} from "../../../../../GlobalComponents/TextViewBox"
import SearchCountry from "../../../../GoogleAddressForm/GoogleCountryComponent";

const KeyOpportunity = ({
  errorMessages,
  item = {},
  dropDown,
  onBlur,
  description,
  canEditTran,
  onChangeItem,
  transaction,
  category,
  getCountryDetails
}) => {
  const onChange = event => {
    if (event.target.name === "actPrimarySector" || event.target.name === "actSecondarySector") {
      const tranSector = {
        ...transaction,
        [event.target.name]: event.target.type === "checkbox" ? event.target.checked: event.target.value
      }
      if (event.target.name === "actPrimarySector") {
        tranSector.actSecondarySector = ""
      }
      onChangeItem(tranSector, "tempTransaction")
    } else if (event.target.name === "actIssueName" || event.target.name === "actProjectName") {
      onChangeItem(
        {
          ...transaction,
          [event.target.name]: event.target.value
        },
        "tempTransaction"
      )
    } else {
      onChangeItem(
        {
          ...item,
          [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value
        },
        category
      )
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onSelection = selectItem => {
    onChangeItem(
      {
        ...transaction,
        actIssuerClient: selectItem.id,
        actIssuerClientEntityName: selectItem.firmName,
        actIssuerClientMsrbType: selectItem.msrbRegistrantType
      },
      "tempTransaction"
    )
  }

  return (
    <div>
      <div className="columns">
        {/* <div className="column">
          <p className="multiExpLbl">
            State<span className="has-text-danger">*</span>
          </p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="State"
                name="actState"
                value={item.actState || ""}
                disabled={!canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="">Pick State</option>
                {dropDown.stateCountry &&
                  Object.keys(dropDown.stateCountry["United States"]).map(
                    (key, i) => (
                      <option key={i} value={key}>
                        {key}
                      </option>
                    )
                  )}
              </select>
              {errorMessages.actState && (
                <small className="text-error">
                  {errorMessages.actState || ""}
                </small>
              )}
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">County</p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="County"
                name="actCounty"
                value={item.actCounty || ""}
                disabled={!canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="">Pick County</option>
                {dropDown.stateCountry &&
                dropDown.stateCountry["United States"] &&
                dropDown.stateCountry["United States"][item.actState]
                  ? dropDown.stateCountry["United States"][item.actState].map(
                    (country, i) => (
                      <option key={i} value={country}>
                        {country}
                      </option>
                    )
                  )
                  : null}
              </select>
              {errorMessages.actCounty && (
                <small className="text-error">
                  {errorMessages.actCounty || ""}
                </small>
              )}
            </div>
          </div>
        </div> */}
        <SearchCountry
          idx={0}
          label="State/City"
          disabled={!canEditTran}
          error={errorMessages.actState || errorMessages.actCounty || ""}
          value={item.actCounty && item.actState ? `${item.actCounty}, ${item.actState}` : item.actCounty ? `${item.actCounty}` : `${item.actState}`}
          getCountryDetails={getCountryDetails}
        />
        <div className="column">
          <p className="multiExpLbl">Primary Sector</p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="Primary Sector"
                value={transaction.actPrimarySector || ""}
                disabled={!canEditTran}
                onChange={onChange}
                name="actPrimarySector"
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="" disabled="">
                  Pick Primary Sector
                </option>
                {dropDown && dropDown.primarySectors && dropDown.primarySectors.map(t => (
                  <option key={t && t.label} value={t && t.label} disabled={!t.included}>
                    {t && t.label}
                  </option>
                ))}
              </select>
              {errorMessages && errorMessages.actPrimarySector && (
                <small className="text-error">
                  {errorMessages.actPrimarySector || ""}
                </small>
              )}
            </div>
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl">Secondary Sector</p>
          <div className="select is-small is-link" style={{ width: "100%" }}>
            <select
              title="Secondary Sector"
              name="actSecondarySector"
              value={transaction.actSecondarySector || ""}
              disabled={!canEditTran}
              onChange={onChange}
              onBlur={onBlurInput}
              style={{ width: "100%" }}
            >
              <option value="" disabled="">
                Pick Secondary Sector
              </option>
              {transaction.actPrimarySector &&
              dropDown.secondarySectors[transaction.actPrimarySector]
                ? dropDown.secondarySectors[transaction.actPrimarySector].map(
                  (sector, i) => (
                    <option key={i} value={sector && sector.label} disabled={!sector.included}>
                      {sector && sector.label}
                    </option>
                  )
                )
                : null}
            </select>
            {errorMessages && errorMessages.actSecondarySector && (
              <small className="text-error">
                {errorMessages.actSecondarySector || ""}
              </small>
            )}
          </div>
        </div>

        <SelectLabelInput
          label="Offering Type"
          error={errorMessages.actOfferingType || ""}
          list={dropDown.actOfferingType}
          name="actOfferingType"
          value={item.actOfferingType || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        {/* <TextLabelInput
          label="Opportunity Name"
          required
          error={errorMessages.actOppName || ""}
          name="actOppName"
          placeholder="Opportunity Name"
          value={
            description ? (item.actOppName = description) : item.actOppName
          }
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        /> */}

      </div>

      <div className="columns">

        <SelectLabelInput
          label="Security Type"
          error={errorMessages.actSecurityType || ""}
          list={dropDown.actSecurityType}
          name="actSecurityType"
          value={item.actSecurityType || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <SelectLabelInput
          label="Bank Qualified"
          error={errorMessages.actBankQualified || ""}
          list={dropDown.actBankQualified}
          name="actBankQualified"
          value={item.actBankQualified || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <SelectLabelInput
          label="Use of Proceed"
          error={errorMessages.actUseOfProceeds || ""}
          list={dropDown.actUseOfProceeds}
          name="actUseOfProceeds"
          value={item.actUseOfProceeds || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <NumberInput
          prefix="$"
          label="Par Amount"
          error={errorMessages.actParAmount ? "Required(must be larger than or equal to 0)" : "" || ""}
          name="actParAmount"
          placeholder="Par Amount"
          value={item.actParAmount || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
      </div>

      <div className="columns">

        <TextLabelInput
          label="Pricing Date"
          error={errorMessages.actPricingDate  || ""}
          name="actPricingDate"
          type="date"
          // value={item.actPricingDate ? moment(new Date(item.actPricingDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
          value={(item.actPricingDate === "" || !item.actPricingDate) ? null : new Date(item.actPricingDate)}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />

        <TextLabelInput
          label="Expected Award Date"
          error={(errorMessages.actExpAwaredDate && "Required (must be larger than or equal to  Pricing Date)") || ""}
          name="actExpAwaredDate"
          type="date"
          // value={item.actExpAwaredDate ? moment(new Date(item.actExpAwaredDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
          value={(item.actExpAwaredDate === "" || !item.actExpAwaredDate) ? null : new Date(item.actExpAwaredDate)}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />

        <TextLabelInput
          label="Actual Award Date"
          error={(errorMessages.actActAwardDate && "Required (must be larger than or equal to Exp. Award Date)") || ""}
          name="actActAwardDate"
          type="date"
          // value={item.actActAwardDate ? moment(new Date(item.actActAwardDate).toISOString().substring(0, 10)).format("YYYY-MM-DD") : ""}
          value={(item.actActAwardDate === "" || !item.actActAwardDate) ? null : new Date(item.actActAwardDate)}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />

        <SelectLabelInput
          label="Corp Type"
          error={errorMessages.actCorpType || ""}
          list={dropDown.actCorpType}
          name="actCorpType"
          value={item.actCorpType || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
      </div>

      <div className="columns">
        {/* <SelectLabelInput
          label="Opportunity type"
          required
          error={errorMessages.actOppType || ""}
          list={dropDown.actOppType}
          name="actOppType"
          value={item.actOppType || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        /> */}
        {/* <SelectLabelInput
          label="Opportunity status"
          required
          error={errorMessages.actOppStatus || ""}
          list={dropDown.actOppStatus}
          name="actOppStatus"
          value={item.actOppStatus || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        /> */}

        <NumberInput
          prefix="$"
          label="Estimated Revenue"
          error={errorMessages.actEstRev ? "Required(must be larger than or equal to 0)" : "" || ""}
          name="actEstRev"
          placeholder="Estimated Revenue"
          value={item.actEstRev || ""}
          disabled={!canEditTran}
          onChange={onChange}
          onBlur={onBlurInput}
        />
        <div className="column"/>
        <div className="column"/>
        <div className="column"/>
      </div>
    </div>
  )
}

export default KeyOpportunity
