import React from "react"
import {toast} from "react-toastify"
import {connect} from "react-redux"
import cloneDeep from "lodash.clonedeep"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import ParticipantsView from "../../../../GlobalComponents/ParticipantsView"
import {
  putMarfpParticipantsDetails,
  pullMarfpParticipantsDetails,
  sendEmailAlert
} from "../../../../StateManagement/actions"
import { removeWhiteSpaces, generateDistributionPDF } from "GlobalUtils/helpers"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {SendEmailModal} from "../../../../GlobalComponents/SendEmailModal"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import {postCheckedAddToDL} from "../../../../StateManagement/actions/Common"

class MarfpParticipants extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      maRfpParticipants: [],
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      jsonSheets: [],
      startXlDownload: false
    }
  }

  async componentDidMount() {
    const {nav2, transaction} = this.props
    if(nav2) {
      this.setState({
        maRfpParticipants: (transaction && transaction.maRfpParticipants && transaction.maRfpParticipants.length) ? cloneDeep(transaction.maRfpParticipants) : [],
        loading: false,
      })
    }else {
      this.setState({loading: false})
    }
    this.setState(prevState => ({
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actIssueName || transaction.actProjectName} - Notification`
      },
    }))
  }

  onRemove = (removeId, callback) => {
    pullMarfpParticipantsDetails(removeId, this.props.nav2, (res) => {
      if(res && res.status === 200) {
        toast("Removed participant successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.props.submitAuditLogs(this.props.nav2)
        this.setState({
          maRfpParticipants: (res.data && res.data.maRfpParticipants) || [],
        }, () => {
          callback({
            status: true,
            participants: (res.data && res.data.maRfpParticipants) || [],
          })
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onSavePart = (participant, callback) => {
    putMarfpParticipantsDetails(this.props.nav2, participant, (res)=> {
      if (res && res.status === 200) {
        toast("Added participant successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.props.submitAuditLogs(this.props.nav2)
        this.setState({
          maRfpParticipants: (res.data && res.data.maRfpParticipants) || [],
        }, () => {
          callback({
            status: true,
            participants: (res.data && res.data.maRfpParticipants) || [],
          })
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  handelToggle = () =>{
    this.setState({ modalState: true })
  }

  onModalSave = () => {
    const {nav1, nav2} = this.props.match.params
    const {email} = this.state
    const emailPayload = {
      tranId: nav2,
      type: nav1,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace('/',''),
        ...email,
      }
    }
    this.setState({
      modalState: false
    },async ()=>{
      await sendEmailAlert(emailPayload)
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  distributePDFDownload = async () => {
    let { maRfpParticipants } = this.state
    const { logo, transaction, user } = this.props
    const partTypeQueue = ["Issuer", "Bond Counsel", "Municipal Advisor"]

    const data = []
    const participantsData = []

    if(maRfpParticipants && maRfpParticipants.length){
      maRfpParticipants.sort((a, b) => {
        const nameA = a && a.partType && a.partType.toLowerCase()
        const nameB = b && b.partType && b.partType.toLowerCase()
        if (nameA < nameB)
          return -1
        if (nameA > nameB)
          return 1
        return 0
      })

      partTypeQueue.forEach(partType => {
        maRfpParticipants.forEach(d => {
          if(d && d.partType === partType && d.addToDL){
            data.push({
              stack: [
                { text: d && d.partType && d.partType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
                { text: `Name: ${d.partContactName || ""}`, bold: true, fontSize: 10},
                { text: `Address: ${d.partContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
                { text: `Phone: ${d.partContactPhone || ""}`, fontSize: 10},
                { text: `Fax: ${d.partContactFax || ""}`, fontSize: 10},
                { text: `E-Mail: ${d.partContactEmail || ""}`, fontSize: 10}
              ]
            })
          }
        })
      })

      maRfpParticipants = maRfpParticipants.filter(d => (d && partTypeQueue.indexOf(d.partType) === -1  && d.addToDL))

      maRfpParticipants.forEach(d => {
        if(d && d.addToDL){
          data.push({
            stack: [
              { text: d && d.partType && d.partType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
              { text: `Name: ${d.partContactName || ""}`, bold: true, fontSize: 10},
              { text: `Address: ${d.partContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
              { text: `Phone: ${d.partContactPhone || ""}`, fontSize: 10},
              { text: `Fax: ${d.partContactFax || ""}`, fontSize: 10},
              { text: `E-Mail: ${d.partContactEmail || ""}`, fontSize: 10}
            ]
          })
        }
      })
    }

    if(data && data.length){
      const stackData = []
      let duplicateData = []
      let dataIndex = 0
      data.forEach(d => {
        if(duplicateData.length < 2){
          dataIndex = dataIndex + 1
          const index = [dataIndex - 1]
          duplicateData.push(data[index])
          if(duplicateData.length === 1){
            stackData.push(duplicateData)
          }
        }
        if (duplicateData.length === 2){
          duplicateData = []
        }
      })
      if(stackData && stackData.length){
        stackData.forEach(stack => {
          if(stack && stack.length === 1){
            stack.push({stack: []})
          }
          participantsData.push({columns: stack}, {marginTop:10, text: [ "" ]})
        })
      }
      await generateDistributionPDF({
        logo,
        firmName: user && user.firmName && user.firmName.toUpperCase() || "",
        clientName: transaction && transaction.actIssuerClientEntityName && transaction.actIssuerClientEntityName.toUpperCase() || "",
        transactionType: "MARFP",
        data: participantsData,
        fileName: `${transaction.actIssueName || transaction.actProjectName || ""} participants.pdf`
      })
    } else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  pdfDownload = () => {
    const {transaction, logo, user} = this.props
    const position = user && user.settings
    const {maRfpParticipants} = this.state
    const pdfParticipants = []
    maRfpParticipants && maRfpParticipants.forEach((item) => {
      const address1 = removeWhiteSpaces(item.partContactAddrLine1 || "")
      if(item.addToDL){
        pdfParticipants.push([
          item.partType || "--",
          item.partFirmName || "--",
          item.partContactName || "--",
          item.partContactPhone || "--",
          item.partContactEmail || "--",
          item.addToDL ? "Yes" : "No" || "--",
          address1 || "--"
        ])
      }
    })
    const tableData = []
    if(pdfParticipants.length > 0){
      tableData.push({
        titles:["MaRfp Participants"],
        description: "",
        headers: ["Participant Type","Firm name","Contact Name","Phone","Email","Add to DL","Address"],
        rows: pdfParticipants
      })
    }
    if(pdfParticipants && pdfParticipants.length){
      pdfTableDownload("", tableData, `${transaction.actIssueName || transaction.actProjectName || ""} participants.pdf`, logo, position)
    }else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  xlDownload = () => {
    const {maRfpParticipants} = this.state
    const pdfParticipants = []

    maRfpParticipants && maRfpParticipants.map((item) =>  {
      const data = {
        "Add to DL": item.addToDL || false,
        "Firm Name": item.partFirmName || "--",
        "Part Type": item.partType || "--",
        "Name": item.partContactName || "--",
        "Email": item.partContactEmail || "--",
        "Phone": item.partContactPhone || "--",
        "Address": item.partContactAddrLine1 || "--",
      }
      pdfParticipants.push(data)
    })
    const jsonSheets = [ {
      name: "Participants",
      headers: ["Add to DL","Firm Name","Part Type","Name","Email","Phone","Address"],
      data: pdfParticipants,
    } ]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  onCheckAddTODL = async (e, item, participantType, type, tranId, payload, callback) => {
    const { maRfpParticipants } = this.state
    const res = await postCheckedAddToDL(type, tranId, payload)
    if(res && res.status === 200 && res.data && res.data.updateParticipant) {
      const partIndex = maRfpParticipants.findIndex(part => part._id === item._id)
      maRfpParticipants[partIndex].addToDL = res.data.updateParticipant && res.data.updateParticipant.addToDL
      this.setState({
        maRfpParticipants
      },() => {
        callback({
          status: true,
          participants: maRfpParticipants || [],
        })
      })
    }
  }

  render() {
    const {maRfpParticipants, modalState, email} = this.state
    const {transaction, nav2, tranAction, participants, onParticipantsRefresh} = this.props
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }
    return (
      <div className="participants">
        {tranAction && tranAction.canEditTran ?
          <div className="columns">
            <div className="column">
              <button className="button is-link is-small" onClick={this.handelToggle} >Send Email Alert</button>
            </div>
            <div className="column">
              <div className="field is-grouped">
                <div className={`${(maRfpParticipants && maRfpParticipants.length) ? "control" : "control isDisabled"}`} onClick={this.distributePDFDownload}>
                  <span className="has-text-link">
                    <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                  </span>
                </div>
                {/* <div className={`${(maRfpParticipants && maRfpParticipants.length) ? "control" : "control isDisabled"}`} onClick={this.pdfDownload}>
                  <span className="has-link">
                    <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                  </span>
                </div> */}
                <div className={`${(maRfpParticipants && maRfpParticipants.length) ? "control" : "control isDisabled"}`} onClick={this.xlDownload}>
                  <span className="has-link">
                    <i className="far fa-2x fa-file-excel has-text-link" title="Excel Download"/>
                  </span>
                </div>
                <div style={{ display: "none" }}>
                  <ExcelSaverMulti label={`${transaction.actIssueName || transaction.actProjectName || ""} participants.pdf`} startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag}
                    jsonSheets={this.state.jsonSheets}/>
                </div>
              </div>
            </div>
          </div>
          : null }
        <SendEmailModal modalState={modalState} email={email} onModalChange={this.onModalChange} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onSave={this.onModalSave}/>
        <ParticipantsView id={nav2} transaction={transaction} firmId={(transaction && transaction.actTranFirmId) || ""} title="Add Ma-RFP Participants" onRemove={this.onRemove}
           canEditTran={tranAction.canEditTran || false} onSave={this.onSavePart} participants={maRfpParticipants} type="ActMaRFP" onCheckAddTODL={this.onCheckAddTODL}/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(MarfpParticipants)
export default connect(mapStateToProps, null)(WrappedComponent)

