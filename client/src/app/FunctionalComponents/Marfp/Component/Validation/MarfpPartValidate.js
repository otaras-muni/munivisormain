import Joi from "joi-browser";

const activityParticipantsSchema = Joi.object().keys({
  partType: Joi.string().required().optional(),
  partFirmId: Joi.string().required().optional(),
  partFirmName: Joi.string().required().optional(), // May not need this
  partContactTitle: Joi.string().allow("").optional(),
  partContactId: Joi.string().required().optional(),
  partContactName: Joi.string().required().optional(),
  partContactEmail: Joi.string().email().required().optional(), // Same reasoning as above
  partContactAddrLine1: Joi.string().allow("").optional(), // Same reasoning as above
  partContactAddrLine2: Joi.string().allow("").optional(), // Same reasoning as above
  participantState: Joi.string().allow("").optional(), // Same reasoning as above
  partContactPhone: Joi.string().required().optional(),
  partContactAddToDL: Joi.boolean().optional(),
  _id: Joi.string().required().optional(),
})

export const activityParticipantsValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, activityParticipantsSchema, { abortEarly: false, stripUnknown:false })
}

