import Joi from "joi-browser"
import dateFormat from "dateformat"

const maRfpSummary = minDate =>
  Joi.object().keys({
    actState: Joi.string().required(),
    actCounty: Joi.string()
      .allow("")
      .optional(),
    actPlaceholder1: Joi.string()
      .allow("")
      .optional(),
    actPlaceholder2: Joi.string()
      .allow("")
      .optional(),
    actPlaceholder3: Joi.string()
      .allow("")
      .optional(),
    actOfferingType: Joi.string()
      .allow("")
      .optional(),
    actSecurityType: Joi.string()
      .allow("")
      .optional(),
    actBankQualified: Joi.string()
      .allow("")
      .optional(),
    actCorpType: Joi.string()
      .allow("")
      .optional(),
    actParAmount: Joi.number().allow("", null).min(0).required().optional(),
    actPricingDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
    actExpAwaredDate: Joi.date().allow([null,""]).example(new Date("2016-01-01")).min(Joi.ref("actPricingDate")).allow("", null).optional(),
    actActAwardDate: Joi.date().allow("", null).example(new Date("2016-01-01")).min(Joi.ref("actExpAwaredDate")).allow("", null).optional(),
    actEstRev: Joi.number().integer().allow("", null).min(0).required().optional(),
    actUseOfProceeds: Joi.string()
      .allow("")
      .optional(),
    actOppName: Joi.string().allow("").optional(),
    actOppType: Joi.string().allow("").optional(),
    actOppStatus: Joi.string().allow("").optional(),
    actPrimarySector: Joi.string().allow("").optional(),
    actSecondarySector: Joi.string()
      .allow("")
      .optional(),
    createdAt: Joi.date()
      .required()
      .optional(),
    _id: Joi.string()
      .required()
      .optional()
  })

const marfpSchema = minDate =>
  Joi.object().keys({
    actIssuerClientEntityName: Joi.string()
      .required()
      .optional(),
    actIssuerClient: Joi.string()
      .required()
      .optional(),
    actIssuerClientMsrbType: Joi.string()
      .required()
      .optional(),
    actPrimarySector: Joi.string()
      .allow("")
      .optional(),
    actSecondarySector: Joi.string()
      .allow("")
      .optional(),
    actIssueName: Joi.string()
      .allow("")
      .optional(),
    actProjectName: Joi.string().required(),
    maRfpSummary: maRfpSummary(minDate)
  })

export const DetailsValidate = (inputTransDistribute, minDate) =>
  Joi.validate(inputTransDistribute, marfpSchema(minDate), {
    abortEarly: false,
    stripUnknown: false
  })

const maRfpResultSchema = Joi.object().keys({
  actResult: Joi.string().required(),
  actClosingNotes: Joi.string().required()
})

export const ResultValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, maRfpResultSchema, {
    abortEarly: false,
    stripUnknown: false
  })
