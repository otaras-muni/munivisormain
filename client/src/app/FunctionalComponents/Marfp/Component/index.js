import React from "react"
import { Link, NavLink } from "react-router-dom"
import Loader from "../../../GlobalComponents/Loader"
import Details from "./Details"
import { fetchMarfpTransaction } from "../../../StateManagement/actions"
import { makeEligibleTabView } from "../../../../globalutilities/helpers"
import Summary from "./Summary"
import Documents from "./Documents"
import Participants from "./Participants"
import Audit from "../../../GlobalComponents/Audit"
import CONST, { activeStyle } from "../../../../globalutilities/consts"
import { connect } from "react-redux"
import { fetchParticipantsAndOtherUsers } from "../../../StateManagement/actions/Transaction"

class MarfpView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs: [],
      loading: true,
      transaction: {},
      tranAction: {},
      TABS: [
        { path: "summary", label: "Summary" },
        { path: "details", label: "Details" },
        { path: "participants", label: "Participants" },
        { path: "documents", label: "Documents" },
        { path: "audit-trail", label: "Activity Log" }
      ]
    }
  }

  componentWillMount() {
    const { user, nav, nav1, nav2, loginEntity } = this.props
    const tranId = this.props.nav2 || ""
    const type = this.props.nav3 || ""
    const { TABS } = this.state
    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])
    if (tranId) {
      fetchMarfpTransaction(type, tranId, async transaction => {
        if (transaction && transaction.actIssuerClient) {
          const isEligible = await makeEligibleTabView(
            user,
            tranId,
            TABS,
            allEligibleNav,
            transaction.actIssuerClient,
            transaction.actTranFirmId,
          )

          isEligible.tranAction.canEditTran = CONST.oppDecline.indexOf(transaction.actStatus) !== -1 ? false : isEligible.tranAction.canEditTran

          isEligible.tranAction.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.actStatus) === -1 && isEligible.tranAction.canEditDocument

          // if(transaction.maRfpParticipants && loginEntity && loginEntity.relationshipToTenant !== "Self"){
          //   transaction.maRfpParticipants = transaction.maRfpParticipants.filter(part => part.partFirmId === loginEntity.entityId)
          // }

          console.log("=================tranAction================", isEligible)
          if (isEligible && isEligible.tranAction && (!isEligible.tranAction.canEditTran && !isEligible.tranAction.canViewTran)) {
            this.props.history.push("/dashboard")
          } else {
            if (isEligible.tranAction.view.indexOf(type) === -1) {
              const view = isEligible.tranAction.view[0] || ""
              this.props.history.push(`/${nav1}/${tranId}/${view}`)
            } else {
              const participants = await fetchParticipantsAndOtherUsers(nav2)
              const ids = `${transaction.actIssuerClient},${
                transaction.actTranFirmId
              }`
              this.setState({
                transaction,
                participants,
                loading: false,
                tranAction: isEligible.tranAction,
                tabs: isEligible.tabs
              })
            }
          }
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  onStatusChange = status => {
    const { tranAction } = this.state
    tranAction.canEditTran =
      CONST.oppDecline.indexOf(status) !== -1 ? false : tranAction.canEditTran
    this.setState({
      tranAction
    })
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderTabs = (tabs, marfpId, option) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/marfp/${marfpId}/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderViewSelection = (marfpId, option) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, marfpId, option)}</ul>
    </nav>
  )

  renderSelectedView = (marfpId, option) => {
    const { transaction, tranAction, participants } = this.state

    switch (option) {
    case "details":
      return (
        <Details
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "summary":
      return (
        <Summary
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
          onStatusChange={this.onStatusChange}
        />
      )
    case "participants":
      return (
        <Participants
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
        />
      )
    case "audit-trail":
      return <Audit {...this.props} />
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { option } = this.props
    const { transaction } = this.state
    const marfpId = transaction && transaction._id
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(marfpId, option)}
            </div>
          </div>
        </div>
        <section id="main">{this.renderSelectedView(marfpId, option)}</section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(
  mapStateToProps,
  null
)(MarfpView)
