import React, { Component } from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import MaskedInput from "react-text-mask"

import { Modal } from "Global/BulmaModal"
import EntityLookup from "Global/EntityLookup"
import Loader from "Global/Loader"

import { getPicklistValues, getEntityDetails, saveEntityDetails,
  updateAuditLog, checkNavAccessEntitlrment } from "GlobalUtils/helpers"

import { updateFirmLogoAndAuditSetting } from "AppState/actions/AdminManagement/admTrnActions"

import FirmAddressListForm from "../EntityManagement/CommonComponents/FirmAddressListForm"
import AuditAndLogo from "../EntityManagement/clients/components/AuditAndLogo"
import Notes from "../../GlobalComponents/Notes"
import {SelectLabelInput} from "../../GlobalComponents/TextViewBox"

const toastTimeout = 1000

const defaultValues = {
  isMuniVisorClient: true,
  entityType: "Municipal Advisor"
}

const getInitialAddress = () => (
  [
    {
      addressName: "",
      isPrimary: false,
      isHeadQuarter: false,
      /* isActive: true, */
      website: "",
      officePhone: [
        {
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }
      ],
      officeFax: [
        {
          faxNumber: ""
        }
      ],
      officeEmails: [
        {
          emailId: ""
        }
      ],
      addressLine1: "",
      addressLine2: "",
      country: "",
      state: "",
      city: "",
      zipCode: {
        zip1: "",
        zip2: ""
      },
      formatted_address: "",
      url: "",
      location: {
        longitude: "",
        latitude: ""
      }
    }
  ]
)

const getInitialState = () => ({
  expanded: {
    firmDetailSection: true,
  },
  entityId: "",
  viewOnly: true,
  entityAliases: [],
  aliasIndex: -1,
  businessStructure: "",
  numEmployees: "",
  annualRevenue: "",
  taxId: "",
  msrbRegistrantType: "",
  msrbFirmName: "",
  firmName: "",
  msrbId: "",
  settings: {
    auditFlag: "no",
    logo: {
      awsDocId: "",
      fileName: ""
    }
  },
  addresses: getInitialAddress(),
  modalState: false,
  modalIndex: -1,
  modalType: "",
  error: { addresses: getInitialAddress() },
  generalError: "",
  updateData: {}
})

class FirmNew extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...getInitialState(), waiting: true, revenueList: [], changeLog: [], logoLocation: [], logoAlignment: [],
      registrantTypesList: [], businessStructuresList: [], numEmployeesList: [], currentEntity: {}
    }
    this.onChangeMSRBFirmName = this.onChangeMSRBFirmName.bind(this)
    this.onChangeFirmName = this.onChangeFirmName.bind(this)
    this.changeField = this.changeField.bind(this)
    this.saveBasicDetails = this.saveBasicDetails.bind(this)
    this.saveCallback = this.saveCallback.bind(this)
    this.onSaveAddress = this.onSaveAddress.bind(this)
    this.saveAddressChanges = this.saveAddressChanges.bind(this)
    this.getEntityData = this.getEntityData.bind(this)
    this.getEntityDetailsCallback = this.getEntityDetailsCallback.bind(this)
    this.addAlias = this.addAlias.bind(this)
    this.cancelAlias = this.cancelAlias.bind(this)
    // this.toggleModal = this.toggleModal.bind(this)
  }

  async componentDidMount() {
    console.log("did mount")
    const result = await getPicklistValues(["LKUPREGISTRANTTYPE",
      "LKUPBUSSTRUCT", "LKUPANNUALREVENUE", "LKUPNUMOFPEOPLE", "LKUPLOGOLOCATION", "LKUPLOGOALIGNMENT"])
    console.log("result : ", result)
    const registrantTypesList = result[0] && result[0][2] ?
      (result[0][2]["Tenants / Firms"] || []) : []
    const businessStructuresList = result[1] && result[1][1] ? result[1][1] : []
    const revenueList = result[2] && result[2][1] ? result[2][1] : []
    const numEmployeesList = result[3] && result[3][1] ? result[3][1] : []
    const logoLocation = result[4] && result[4][1] ? result[4][1] : []
    const logoAlignment = result[5] && result[5][1] ? result[5][1] : []


    const { nav2 } = this.props
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit

    this.setState({
      registrantTypesList, businessStructuresList, revenueList, logoLocation, logoAlignment,
      numEmployeesList, entityId: nav2 || "", viewOnly, waiting: false
    }, this.getEntityData)
  }

  async componentWillReceiveProps(nextProps) {
    const { nav2 } = nextProps
    if (nav2 !== this.props.nav2) {
      const action = await checkNavAccessEntitlrment(nav2)
      const viewOnly = action && nav2 && !action.edit
      this.setState({ entityId: nav2 || "", viewOnly }, this.getEntityData)
    }
  }

  onAuditAndLogoChange = async (settings, type) => {
    const { entityId } = this.state
    if(entityId){
      const payload = {
        settings,
        _id: entityId
      }

      if(type === "logo") {
        const res = await updateFirmLogoAndAuditSetting(payload)
        if(res && res.status === 200){
          toast("Firm has been updated!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
          this.setState({
            settings
          })
        }else {
          toast("Something went wrong!", {
            autoClose: 2000,
            type: toast.TYPE.ERROR
          })
        }
      }else {
        this.setState({
          settings
        })
      }
    }
  }

  onSaveAddress(addressList) {
    console.log("addressList : ", addressList)
    this.setState({ addressList }, this.saveAddressChanges)
  }

  onChangeMSRBFirmName(entityName) {
    // this.setState({ entityName })
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.msrbFirmName = ""
      const firmName = (entityName && entityName.firmName) || ""
      error.firmName = ""
      const msrbRegistrantType = (entityName && entityName.participantType) || prevState.msrbRegistrantType
      const msrbId = (entityName && entityName.msrbId) || prevState.msrbId

      return { msrbFirmName: firmName, error, msrbRegistrantType,
        msrbId, firmName: (prevState.firmName || firmName) }
    })
  }

  onChangeFirmName(entityName) {
    // this.setState({ entityName })
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.firmName = ""
      error.msrbFirmName = ""
      const firmName = (entityName && entityName.firmName) || ""
      return { msrbFirmName: (prevState.msrbFirmName || firmName),
        error, firmName }
    })
  }

  getEntityDetailsCallback(err, res) {
    if (err) {
      this.setState({ waiting: false, generalError: "Error in getting entity data" })
    } else {
      console.log("res : ", res)
      const currentEntity = res[0] || {}
      if(currentEntity.relationshipType !== "Self") {
        this.setState({
          generalError: `Wrong Entity Type provided for ${currentEntity.firmName}`,
          waiting: false
        })
        return
      }
      const { entityType, firmName, taxId, addresses, businessStructure,
        numEmployees, annualRevenue, entityAliases, msrbRegistrantType,
        msrbFirmName, msrbId, settings, notes } = currentEntity


      this.setState({
        waiting: false,
        generalError: "",
        currentEntity,
        entityAliases,
        aliasIndex: -1,
        entityType,
        businessStructure,
        numEmployees,
        firmName,
        annualRevenue,
        taxId,
        msrbRegistrantType,
        msrbFirmName,
        msrbId,
        settings,
        addresses,
        notes: (notes || []),
        error: { addresses: getInitialAddress() }
      })
    }
  }

  getEntityData() {
    const { entityId } = this.state
    if (entityId) {
      this.setState({ waiting: true })
      getEntityDetails(entityId, this.getEntityDetailsCallback)
    }
  }

  toggleExpand(val) {
    console.log("toggle : ", val)
    // e.preventDefault()
    this.setState(prevState => {
      const expanded = { ...prevState.expanded }
      Object.keys(expanded).forEach(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
      return { expanded }
    })
  }

  changeField(e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    this.setState(prevState => {
      const error = { ...prevState.error }
      return { [name]: value, error }
    })
  }

  toggleModal(modalType, modalIndex) {
    this.setState(prev => {
      const modalState = !prev.modalState
      return { modalState, modalType, modalIndex }
    })
  }

  checkDupInArray(arr) {
    console.log("arr : ", arr)
    let result = [-1, -1]
    arr.some((e, i) => {
      if(e) {
        const dupIdx = arr.slice(i + 1).findIndex(d => d === e)
        if(dupIdx > -1) {
          result = [i, dupIdx+i+1]
          return true
        }
      }
    })
    console.log("dupResult : ", result)
    return result
  }

  checkEmptyArray(arr) {
    let result = -1
    arr.some((e, i) => {
      if(!e) {
        result = i
        return true
      }
    })
    return result
  }

  checkEmptyRows(rows) {
    let isEmpty
    rows.some((d, i) => {
      Object.keys(d).some(k => {
        if (!d[k]) {
          isEmpty = [i, k]
          return true
        }
      })
      if (isEmpty) {
        return true
      }
    })
    console.log("isEmpty : ", isEmpty)
    return isEmpty
  }

  editAlias(aliasIndex) {
    this.setState({ aliasIndex })
  }

  cancelEditAlias(i) {
    console.log(" cancel i : ", i)
    this.setState(prevState => {
      const { currentEntity } = prevState
      const error = { ...prevState.error }
      error.entityAliases = ""
      const entityAliases = [...prevState.entityAliases]
      const newItems = currentEntity && currentEntity.entityAliases &&
        currentEntity.entityAliases.length ?
        (entityAliases.length - currentEntity.entityAliases.length) : 0
      entityAliases[i] = currentEntity.entityAliases[(i - newItems)]
      return ({ entityAliases, aliasIndex: -1, error })
    })
  }

  addAlias() {
    console.log("in addAlias")
    this.setState(prevState => {
      const error = { ...prevState.error }
      const entityAliases = [...prevState.entityAliases]
      if (entityAliases.length) {
        const emptyAliasIdx = this.checkEmptyArray(entityAliases)
        if(emptyAliasIdx > -1) {
          error.entityAliases = { ...error.entityAliases }
          error.entityAliases[emptyAliasIdx] = "Please provide a value here or reset before adding another Alias"
          return { error }
        }
      }
      error.entityAliases = ""
      entityAliases.unshift("")
      console.log("entityAliases : ", entityAliases)
      return { entityAliases, error }
    })
  }

  cancelAlias() {
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityAliases = [...(currentEntity.entityAliases || [])]
      const error = { ...prevState.error }
      error.entityAliases = ""
      return {
        entityAliases, modalType: "", modalIndex: -1,
        modalState: false, error, aliasIndex: -1
      }
    })
  }

  changeAlias(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const entityAliases = [...prevState.entityAliases]
      const error = { ...prevState.error }
      error.entityAliases = ""
      entityAliases[i] = value
      return { entityAliases, error }
    })
  }

  removeAlias(i) {
    this.setState(prevState => {
      const entityAliases = [...prevState.entityAliases]
      entityAliases.splice(i, 1)
      const error = { ...prevState.error }
      error.entityAliases = ""
      return {
        entityAliases, modalIndex: -1, modalType: "",
        modalState: false, error
      }
    })
  }

  arrayDiffs(oldArr=[], newArr=[]) {
    const removed = []
    const added = []
    oldArr.forEach(e => {
      if(e && !newArr.includes(e) && !removed.includes(e)) {
        removed.push(e)
      }
    })
    newArr.forEach(e => {
      if(e && !oldArr.includes(e) && !added.includes(e)) {
        added.push(e)
      }
    })
    return [removed, added]
  }

  saveBasicChangeLog(oldEntity, newEntity={}) {
    const { auth } = this.props
    const { entityId, changeLog } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const primitiveFields = {
      "firmName": "Firm Name",
      "msrbRegistrantType": "MSRB Registrant Type",
      "msrbId": "MSRB Id",
      "taxId": "Tax Id",
      "businessStructure": "Business Structure",
      "numEmployees": "Number of Employees",
      "annualRevenue": "Annual Revenue"
    }
    // const changeLog = []
    Object.keys(newEntity).forEach(k => {
      if(primitiveFields[k] && (oldEntity[k] !== newEntity[k])) {
        changeLog.push({
          userId,
          userName,
          log: oldEntity[k] ?
            `Changed ${primitiveFields[k]} from ${oldEntity[k]} to ${newEntity[k]}`
            : `Added ${newEntity[k]} as ${primitiveFields[k]}`,
          date
        })
      }
    })

    const oldAliases = (oldEntity.entityAliases) || []
    const newAliases = (newEntity.entityAliases) || []
    const [removedAliases, addedAliases] = this.arrayDiffs(oldAliases, newAliases)
    if(removedAliases && removedAliases.length) {
      removedAliases.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Removed alias ${f}`,
          date
        })
      })
    }
    if(addedAliases && addedAliases.length) {
      addedAliases.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Added alias ${f}`,
          date
        })
      })
    }
    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  getAuditChangeLog = (log) => {
    const { changeLog } = this.state
    if(log) {
      changeLog.push(log)
      this.setState({changeLog})
    }
  }

  saveCallback(err, res) {
    if (!err) {
      const { _id } = res
      if (_id) {
        this.setState({ entityId: _id })
        // this.props.history.push(`/add-new-taks/${_id}`)
      }
      this.getEntityData()
      toast("Changes Saved", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      this.setState({ waiting: false })
      toast("Error in saving changes", { autoClose: toastTimeout, type: toast.TYPE.WARNING })
    }
  }

  checkNonEmptyErrorKey(error, keys) {
    if (!error) {
      return false
    }
    if (Object.getPrototypeOf(error).constructor.name === "Object") {
      keys = keys || Object.keys(error).filter(k => k !== "addresses")
      if (!keys.length) {
        return false
      }
      let nonEmptyError = false
      keys.some(k => {
        if (this.checkNonEmptyErrorKey(error[k])) {
          nonEmptyError = true
          return true
        }
      })
      return nonEmptyError
    }
    return true
  }

  checkAnyError(keys) {
    const { error } = this.state
    return this.checkNonEmptyErrorKey(error, keys)
  }

  saveBasicDetails() {
    this.setState({ waiting: true })
    const { entityId, entityType, firmName, businessStructure,
      numEmployees, annualRevenue, entityAliases, msrbRegistrantType,
      msrbFirmName, msrbId, settings, taxId, currentEntity } = this.state
    const keys = ["entityType", "firmName", "entityAliases", "taxId"]
    if (this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      return
    }
    const requiredFields = ["firmName"]
    let emptyRequiredField = false
    requiredFields.forEach(d => {
      if (!this.state[d]) {
        emptyRequiredField = true
        this.setState(prevState => {
          const error = { ...prevState.error }
          error[d] = "Please provide a value"
          return { error, waiting: false }
        })
      }
    })
    if (emptyRequiredField) {
      this.setState({ waiting: false })
      return
    }
    if(firmName !== msrbFirmName) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.firmName = "Provide the same value (case sensitive) for MSRB Firm Name and Firm Name"
        error.msrbFirmName = "Provide the same value (case sensitive) for MSRB Firm Name and Firm Name"
        return { error, waiting: false }
      })
      return
    }
    const emptyAliasIdx = this.checkEmptyArray(entityAliases)
    console.log("emptyAliasIdx : ", emptyAliasIdx)
    if(emptyAliasIdx > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityAliases = { ...error.entityAliases }
        error.entityAliases[emptyAliasIdx] = "Please provide a value"
        return { error, waiting: false }
      })
      return
    }
    const dupResult = this.checkDupInArray(entityAliases)
    if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityAliases = { ...error.entityAliases }
        error.entityAliases[dupResult[0]] = "Duplicate values are not allowed"
        error.entityAliases[dupResult[1]] = "Duplicate values are not allowed"
        return { error, waiting: false }
      })
      return
    }

    const basicDetails = {
      entityType, firmName, taxId, businessStructure,
      numEmployees, annualRevenue, entityAliases, msrbRegistrantType,
      msrbFirmName, msrbId, settings, ...defaultValues
    }
    this.saveBasicChangeLog(currentEntity, basicDetails)
    saveEntityDetails(entityId, basicDetails, "Self", this.saveCallback)
  }

  saveAddressChanges() {
    this.setState({ waiting: false })
    const { entityId, addressList } = this.state
    const updateData = { addresses: addressList || [] }
    saveEntityDetails(entityId, updateData, "", this.saveCallback)
  }

  renderBasicDetails() {
    const { expanded, firmName, taxId, businessStructure,
      numEmployees, annualRevenue, entityAliases, msrbRegistrantType,
      msrbFirmName, msrbId, currentEntity, viewOnly,
      error, modalState, modalType, modalIndex, aliasIndex,
      registrantTypesList, businessStructuresList, numEmployeesList,
      revenueList } = this.state
    return (
      <article className={expanded.firmDetailSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <p onClick={this.toggleExpand.bind(this, "firmDetailSection")}>
            Firm Details</p>
        </div>
        {expanded.firmDetailSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    MSRB Firm Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <EntityLookup
                    entityName={msrbFirmName}
                    onChange={this.onChangeMSRBFirmName}
                    entityType="Firms/Tenants"
                    participantType="Municipal Advisors"
                    isRequired={false}
                    // isHide={msrbFirmName}
                  />
                  {error && error.msrbFirmName ?
                    <p className="has-text-danger">
                      {error.msrbFirmName}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  <div className="columns">
                    <div className="column">
                      <p className="multiExpLblBlk">
                        MSRB Registrant Type
                      </p>
                      <SelectLabelInput
                        placeholder="Pick Registrant Type"
                        list={registrantTypesList || []}
                        name="msrbRegistrantType"
                        value={msrbRegistrantType || ""}
                        onChange={this.changeField}
                      />
                    </div>
                    <div className="column">
                      <p className="multiExpLblBlk">
                        MSRB ID
                      </p>
                      <input
                        className="input is-small is-link"
                        type="text"
                        name="msrbId"
                        placeholder=""
                        value={msrbId}
                        onChange={this.changeField}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Firm Name (if not MSRB registered)
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <EntityLookup
                    entityName={firmName}
                    onChange={this.onChangeFirmName}
                    entityType="Firms/Tenants"
                    participantType="Municipal Advisors"
                    isRequired={false}
                    // isHide={firmName}
                  />
                  {error && error.firmName ?
                    <p className="has-text-danger">
                      {error.firmName}
                    </p>
                    : undefined
                  }
                  {modalType === "alias" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Alias ${entityAliases[modalIndex]}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removeAlias.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                  }
                  {currentEntity && currentEntity.entityAliases &&
                    currentEntity.entityAliases.length ?
                    <h6 className="title is-6">Aliases</h6>
                    : undefined
                  }
                  {
                    entityAliases.map((a, i) => {
                      if (!(currentEntity && currentEntity.entityAliases &&
                        currentEntity.entityAliases.length) ||
                        (i < (entityAliases.length - currentEntity.entityAliases.length))) {
                        return (
                          <div key={i} className="columns">
                            <div className="column is-11">
                              <div className="field is-grouped">
                                <input
                                  className="input is-small is-link"
                                  type="text"
                                  placeholder="alias"
                                  value={a}
                                  onChange={this.changeAlias.bind(this, i)}
                                />
                                <span className="has-text-danger pl-10">
                                  <i className="far fa-trash-alt"
                                    onClick={this.toggleModal.bind(this, "alias", i)}
                                  />
                                </span>
                              </div>
                              {error && error.entityAliases && error.entityAliases[i]
                                ? <p className="text-error has-text-danger">
                                  {error.entityAliases[i]}
                                </p>
                                : undefined
                              }
                            </div>
                          </div>
                        )
                      }
                      if (aliasIndex === i) {
                        return (
                          <div key={i} className="columns">
                            <div className="column is-11">
                              <div className="field is-grouped">
                                <input
                                  className="input is-small is-link"
                                  type="text"
                                  placeholder="alias"
                                  value={a}
                                  onChange={this.changeAlias.bind(this, i)}
                                />
                                <span className="has-text-danger">
                                  <i className="fa fa-times"
                                    onClick={this.cancelEditAlias.bind(this, i)}
                                  />
                                </span>
                              </div>
                              {error && error.entityAliases && error.entityAliases[i]
                                ? <p className="text-error has-text-danger">
                                  {error.entityAliases[i]}
                                </p>
                                : undefined
                              }
                            </div>
                          </div>
                        )
                      }
                      return (
                        <div className="columns" key={i}>
                          <span className="column is-11">
                            {a}
                            <i className="fa fa-pencil has-text-danger is-pulled-right"
                              onClick={this.editAlias.bind(this, i)}
                            />
                          </span>
                        </div>
                      )
                    })
                  }
                  <div className="column">
                    <div className="level-right">
                      <div className="field is-grouped">
                        <div className="control">
                          <button className="button is-link is-small" type="button"
                            onClick={this.addAlias}>Add Alias</button>
                        </div>
                        <div className="control">
                          <button className="button is-light is-small" type="button"
                            onClick={this.cancelAlias}>Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="columns">
                    <div className="column">
                      <p className="multiExpLblBlk">Tax Id</p>
                      <div>
                        <MaskedInput
                          mask={[/\d/, /\d/, /\d/, "-", /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                          className="input is-small is-link"
                          type="text"
                          name="taxId"
                          placeholder="Enter Tax ID"
                          value={taxId}
                          onChange={this.changeField}
                        />
                        {error && error.taxId ?
                          <p className="has-text-danger">
                            {error.taxId}
                          </p>
                          : undefined
                        }
                      </div>
                    </div>
                    <div className="column">
                      <p className="multiExpLblBlk">Business Structure</p>
                      <SelectLabelInput
                        placeholder="Pick Business Structure"
                        list={businessStructuresList || []}
                        name="businessStructure"
                        value={businessStructure || ""}
                        onChange={this.changeField}
                      />
                    </div>
                  </div>
                  <div className="columns">
                    <div className="column">
                      <p className="multiExpLblBlk">Number of Employees</p>
                      <SelectLabelInput
                        placeholder="Pick Number of Employees"
                        list={numEmployeesList || []}
                        name="numEmployees"
                        value={numEmployees || ""}
                        onChange={this.changeField}
                      />
                    </div>
                    <div className="column">
                      <p className="multiExpLblBlk">Annual Revenue</p>
                      <SelectLabelInput
                        placeholder="Pick Annual Revenue"
                        list={revenueList || []}
                        name="annualRevenue"
                        value={annualRevenue || ""}
                        onChange={this.changeField}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              {/* <div className="columns">
                <div className="column">
                  <div className="title is-6 has-text-weight-semibold has-text-centered">
                    Activity Log Configuration
                  </div>
                </div>
              </div> */}
              {this.renderSettings()}
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={this.saveBasicDetails}>
                          Save Firm Details
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderSettings() {
    const { settings, logoLocation, logoAlignment } = this.state
    const { auth } = this.props

    return (
      <AuditAndLogo
        user={auth.userEntities || {}}
        logoLocation={logoLocation}
        logoAlignment={logoAlignment}
        settings={settings || {}}
        onChange={this.onAuditAndLogoChange}
        getAuditChangeLog={this.getAuditChangeLog}
      />
    )
  }

  renderAddress() {
    const { entityId, waiting, addresses, error,
      viewOnly } = this.state

    if (!entityId) {
      return null
    }

    return (
      <FirmAddressListForm
        errorFirmDetail={error.addresses[0]}
        onChangeAddressType={this.onChangeAddressType}
        canEdit={!viewOnly}
        canView
        busy={waiting}
        addressList={addresses}
        onSaveAddress={this.onSaveAddress}
      />
    )
  }

  render() {
    const { waiting, generalError, entityId, notes, viewOnly } = this.state
    const { auth } = this.props
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    if (generalError) {
      return (
        <div id="main">
          <strong className="has-text-danger">{generalError}</strong>
        </div>
      )
    }
    return (
      <div id="main">
        {waiting && <Loader />}
        <section className="accordions">
          {this.renderBasicDetails()}
          {this.renderAddress()}
          <Notes
            userId={userId}
            userFirstName={userFirstName}
            userLastName={userLastName}
            entityId={entityId}
            saveCallback={this.saveCallback}
            notes={notes}
            canEdit={viewOnly || false}
          />
        </section>
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default connect(mapStateToProps)(FirmNew)
