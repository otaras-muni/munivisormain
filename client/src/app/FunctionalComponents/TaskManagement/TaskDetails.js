import React, { Component } from "react"
import { Prompt, withRouter } from "react-router-dom"
import {Multiselect} from "react-widgets"
import { toast } from "react-toastify"
import CONST, { ContextType } from "GlobalUtils/consts.js"
import {
  getRelatedInfoForUser, getTaskbyId, saveTask, getPicklistValues,
  getAllTenantUserDetails
} from "GlobalUtils/helpers"
import {SelectLabelInput, TextLabelInput} from "../../GlobalComponents/TextViewBox"
import Loader from "../../GlobalComponents/Loader"
import DocUpload from "../docs/DocUpload"
import DocLink from "../docs/DocLink"
import DocModal from "../docs/DocModal"
import {
  getEligibleTransForLoggedInUser,
  getEligibleEntitiesForLoggedInUser,
} from "../../StateManagement/actions/TaskManagement/taskManageActions"
import {fetchAssigned} from "../../StateManagement/actions/CreateTransaction"

const ObjectID = require("bson-objectid")

const requiredFields = ["taskDescription", "taskStartDate", "taskEndDate",
  "taskType", "taskPriority"]

const initialState = {
  waiting: true,
  taskRefId: ObjectID().toHexString(),
  validationError: {},
  taskTypes: [],
  taskPriorities: [],
  taskStatuses: [],
  relatedActivity: [],
  relatedEntity: [],
  relatedContact: [],
  activities: [],
  users: [],
  documents: [],
  taskAssigneeUserId: "",
  taskAssigneeName: "",
  taskAssigneeType: "",
  taskNotes: "",
  taskDescription: "",
  taskStartDate: "",
  taskEndDate: "",
  taskPriority: "",
  taskStatus: "Active",
  taskType: "",
  taskCreatedBy: {
    userId: ""
  },
  relatedActivityList: [],
  relatedEntityList: [],
  relatedContactList: []
}

class TaskDetails extends Component {
  constructor(props) {
    super(props)
    this.state = { ...initialState }
    this.changeRelatedActivity = this.changeRelatedActivity.bind(this)
    this.changeRelatedEntity = this.changeRelatedEntity.bind(this)
    this.changeRelatedContact = this.changeRelatedContact.bind(this)
    this.uploadDoc = this.uploadDoc.bind(this)
    this.deleteDoc = this.deleteDoc.bind(this)
    this.changeField = this.changeField.bind(this)
    this.saveTaskDetails = this.saveTaskDetails.bind(this)
    this.undoChanges = this.undoChanges.bind(this)
    this.onSaveCallback = this.onSaveCallback.bind(this)
  }

  async componentDidMount() {
    const { taskId, loginDetails } = this.props
    const { userEntities } = loginDetails
    const [taskTypeResult, taskPriorityResult, taskStatusResult] = await getPicklistValues(["LKUPTASKTYPE", "LKUPHIGHMEDLOW", "LKUPSOEEVENTSTATUS"])
    const taskTypes = taskTypeResult[1]
    const taskPriorities = taskPriorityResult[1]
    const taskStatuses = taskStatusResult[1]
    const relatedData = await getRelatedInfoForUser()
    // const res = await axios.get(`${muniApiBaseURL}/controls/related-info`,
    //   { headers: { "Authorization": token } })
    // console.log("res : ", res)
    if (relatedData) {
      // console.log("====>>>",res.data.payload)
      this.setRelatedInfoInState(relatedData)
    }
    const allUsers = await getAllTenantUserDetails()
    const users = allUsers.map(e => ({ id: e.userId, name: e.name }))
    if (taskId) {
      const task = await getTaskbyId(taskId)
      const { taskAssigneeUserId, taskAssigneeName, taskAssigneeType,
        taskNotes, taskDescription, taskStartDate, taskEndDate,
        taskPriority, taskStatus, taskType } = (task && task.taskDetails) || {}
      const taskRelatedDocuments = (task && task.taskRelatedDocuments) || []
      const relatedActivityDetails = (task && task.relatedActivityDetails) || {}
      const relatedEntityDetails = (task && task.relatedEntityDetails) || {}
      const documents = taskRelatedDocuments.map(e => ({ _id: e.docId, name: e.docFileName }))
      const relatedActivity = relatedActivityDetails.activityId ? [{
        _id: relatedActivityDetails.activityId,
        name: relatedActivityDetails.activityProjectName
      }] : []
      const relatedEntity = relatedEntityDetails.entityId ? [{
        _id: relatedEntityDetails.entityId,
        name: relatedEntityDetails.entityName
      }] : []
      const relatedContact = [{ id: taskAssigneeUserId, name: taskAssigneeName }]
      const { taskRefId, taskCreatedBy } = task
      // const relatedEntity = relatedActivityDetails.map(e => e.activityId)
      this.setState({
        users, taskAssigneeUserId, taskAssigneeName, taskAssigneeType,
        taskNotes, taskDescription, taskStartDate, taskEndDate,
        taskPriority, taskStatus, taskType, documents, relatedActivity,
        relatedEntity, relatedContact, taskRefId, taskCreatedBy,
        taskTypes, taskPriorities, taskStatuses, waiting: false
      })
    } else {
      const { loginDetails: { userEntities } } = this.props
      const { userId } = userEntities[0]
      this.setState({
        users, taskTypes, taskPriorities, taskStatuses, taskCreatedBy: { userId },
        waiting: false
      })
    }
    const relatedActivityList = await getEligibleTransForLoggedInUser()
    const relatedEntityList = await getEligibleEntitiesForLoggedInUser()
    // const relatedContactList = await getEligibleUsersForLoggedInUser()
    fetchAssigned(userEntities && userEntities.length && userEntities[0] && userEntities[0].entityId, users => {
      this.setState({
        relatedActivityList: relatedActivityList || [],
        relatedEntityList: relatedEntityList || [],
        relatedContactList: users && users.usersList || [],
        validationError: {}
      })
    })
  }

  onSaveCallback(err) {
    const { taskId } = this.props
    this.setState({ waiting: false })
    if (err) {
      toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    } else {
      toast(taskId ? "Task updated successfully" : "New task added successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
      setTimeout(() => {
        this.props.history.push("/dashboard")
      }, 1000)
    }
  }

  setRelatedInfoInState(payload) {
    const activities = []
    Object.keys(payload).forEach(k => {
      if (payload[k] && payload[k].transactions) {
        activities.push(...payload[k].transactions.map(e => (
          {
            id: e.activityId,
            name: e.activityDescription,
            type: k,
            entities: e.participantEntityDetails ?
              e.participantEntityDetails.map(a => (
                {
                  id: a.participantEntityId,
                  name: a.firmName || a.msrbFirmName
                }
              )) : [],
            contacts: e.participantUsers ?
              e.participantUsers.map(u => (
                {
                  id: u.tranUserId,
                  name: `${u.tranUserFirstName || ""} ${u.tranUserLastName || ""}`
                }
              )) : []
          }
        )))
      }
    })
    console.log("activities===>>>", activities)
    this.setState({ activities })
  }

  // getRelatedActivities() {
  //   let { activities, taskType } = this.state
  //   if (taskType && taskType !== "general") {
  //     activities = activities.filter(e => e.type === taskType)
  //   }
  //   return activities.map(e => ({ id: e.id, name: e.name }))
  // }

  // getRelatedEntities() {
  //   const result = []
  //   const finalResult = []
  //   const { activities, relatedActivity } = this.state
  //   const relatedActivityIds = relatedActivity.map(a => a.id)
  //   const relatedActivities = activities.filter(a => relatedActivityIds.includes(a.id))
  //   relatedActivities.forEach(e => {
  //     result.push(...e.entities)
  //   })
  //   const uniqueIds = []
  //   result.forEach(e => {
  //     if (!uniqueIds.includes(e.id)) {
  //       uniqueIds.push(e.id)
  //       finalResult.push(e)
  //     }
  //   })
  //   return finalResult
  // }

  // getRelatedContacts() {
  //   const result = []
  //   const finalResult = []
  //   const { activities, relatedActivity, users } = this.state
  //   const relatedActivityIds = relatedActivity.map(a => a.id)
  //   const relatedActivities = activities.filter(a => relatedActivityIds.includes(a.id))
  //   if (relatedActivities.length) {
  //     relatedActivities.forEach(e => {
  //       result.push(...e.contacts)
  //     })
  //   } else {
  //     // activities.forEach(e => {
  //     //   result.push(...e.contacts)
  //     // })
  //     result.push(...users)
  //   }
  //   const uniqueIds = []
  //   result.forEach(e => {
  //     if (!uniqueIds.includes(e.id)) {
  //       uniqueIds.push(e.id)
  //       finalResult.push(e)
  //     }
  //   })
  //   return finalResult
  // }

  changeRelatedActivity(relatedActivity) {
    // console.log("relatedActivity : ", relatedActivity)
    this.setState(prevState => {
      const { activities, relatedEntity, relatedContact, validationError } = prevState
      // console.log("relatedEntity, relatedContact : ", relatedEntity, relatedContact)
      // const relatedActivityIds = relatedActivity.slice(-1).map(a => a.id)
      // const relatedActivities = activities.filter(a => relatedActivityIds.includes(a.id))
      // const entityIds = []
      // const contactIds = []
      // relatedActivities.forEach(a => {
      //   entityIds.push(...a.entities.map(e => e.id))
      //   contactIds.push(...a.contacts.map(e => e.id))
      // })
      // const relatedEntityNew = relatedEntity.filter(e => entityIds.includes(e.id))
      /*const relatedContactNew = relatedContact.filter(e => contactIds.includes(e.id))*/
      validationError.taskSelectError = ""
      // console.log("entityIds, contactIds : ", entityIds, contactIds)
      // console.log("relatedEntityNew, relatedContactNew : ", relatedEntityNew, relatedContactNew)
      return {
        relatedActivity: relatedActivity.slice(-1),
        // relatedEntity: relatedEntityNew,
       /* relatedContact: relatedContactNew,*/
        validationError
      }
    })
  }

  changeRelatedEntity(relatedEntity) {
    this.setState(prevState => {
      const { validationError } = prevState
      validationError.taskSelectError = ""
      // const entityIds = relatedEntity.slice(-1).map(e => e.id)
     /* const relatedContact = prevState.relatedContact.filter(e => entityIds.includes(e.entityId))*/
      return {
              relatedEntity: relatedEntity.slice(-1),
              /*relatedContact,*/
              validationError }
    })
  }

  changeRelatedContact(relatedContact) {
    this.setState({ relatedContact, validationError:{taskSelectError: ""}})
  }

  uploadDoc(name, _id) {
    console.log("name, _id : ", name, _id)
    this.setState(prevState => {
      const documents = [...prevState.documents]
      const idx = documents.findIndex(e => e._id === _id)
      if (idx === -1) {
        documents.push({ _id, name })
      } else {
        documents[idx].name = name
      }
      return { documents }
    })
  }

  deleteDoc(_id) {
    console.log("_id : ", _id)
    this.setState(prevState => {
      const documents = [...prevState.documents].filter(e => e._id !== _id)
      return { documents }
    })
  }

  changeField(e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    this.setState((prevState) => {
      const { validationError } = prevState
      if (requiredFields.includes(name)) {
        if (!value) {
          validationError[name] = "Please provide input"
        } else {
          validationError[name] = ""
        }
      }
      if (name === "taskEndDate") {
        const { taskStartDate } = prevState
        if (taskStartDate && value
          && (new Date(taskStartDate) > new Date(value))) {
          validationError.taskStartDate = "End date cannot before the start date"
        } else {
          validationError.taskStartDate = ""
        }
      }
      if (name === "taskStartDate") {
        const { taskEndDate } = prevState
        if (taskEndDate && value
          && (new Date(taskEndDate) < new Date(value))) {
          validationError.taskEndDate = "End date cannot before the start date"
        } else {
          validationError.taskEndDate = ""
        }
      }
      const newState = {
        [name]: value,
        validationError
      }
      if (name === "taskType") {
        newState.relatedActivity = []
        newState.relatedEntity = []
        newState.relatedContact = []
      }
      return newState
    })
  }

  handleSaveTaskDetailsValidation() {
    const { validationError, relatedActivity, relatedEntity, relatedContact } = this.state
    let formIsValid = true
    requiredFields.map(name => {
      if (!this.state[name]) {
        formIsValid = false
        validationError[name] = "Please provide input"
      } else {
        formIsValid = true
        validationError[name] = ""
      }
    }, this)
    const isAllowed = relatedActivity.length || relatedEntity.length || relatedContact.length
    if (!isAllowed) {
      validationError.taskSelectError = "Please select atlist one value."
    }else{
      validationError.taskSelectError = ""
    }

    this.setState({ validationError })
    return formIsValid
  }

  saveTaskDetails() {
    const { validationError, relatedContact } = this.state
    let error = false

    this.handleSaveTaskDetailsValidation()

    Object.keys(validationError).some(k => {
      if (validationError[k]) {
        console.log("validation error : ", k, validationError[k])
        error = true
        return true
      }
    })
    // if (!relatedContact.length) {
    //   error = true
    // }

    if (error) {
      toast("Please fix highlighted errors", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
      return
    }
    this.setState({ waiting: true })
    const { taskId } = this.props
    saveTask({ ...this.state }, taskId, this.onSaveCallback)
  }

  undoChanges() {
    this.props.history.push("/dashboard")
  }

  renderDocActionsTable() {
    let { documents, taskRefId } = this.state
    const { loginDetails: { relatedFaEntities } } = this.props
    const { entityId } = relatedFaEntities[0]
    documents = documents && documents.length ?
      [...documents, { _id: 0, name: "" }] : [{ _id: 0, name: "" }]
    return (
      <div className="column">
        {documents.filter(o => o.docFileName).length ? <Prompt
          when={true}
          message='You have unsaved documents, are you sure you want to leave?'
        /> : null}
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl ">Upload File</p>
              </th>
              <th>
                <p className="multiExpLbl ">Filename</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {documents.map((e, i) => (
              <tr key={e._id ? e._id + i : i}>
                <td className="multiExpTblVal">
                  <DocUpload
                    docId={e._id}
                    tenantId={entityId}
                    contextType={ContextType.tasks}
                    contextId={taskRefId}
                    // docMeta={{"t1": "v1", "t2": "v2"}}
                    // versionMeta={{"t3": new Date().getTime()}}
                    onUploadSuccess={this.uploadDoc}/>
                </td>
                <td className="multiExpTblVal">
                  {e._id ?
                    <div className="field is-grouped">
                      <DocLink docId={e._id} name={e.name}/>
                      <DocModal
                        // docMetaToShow={["t1", "t2"]}
                        // versionMetaToShow={["t3"]}
                        onDeleteAll={this.deleteDoc}
                        selectedDocId={e._id}/>
                    </div>
                    : ""
                  }
                </td>
              </tr>
            ))
            }
          </tbody>
        </table>
      </div>
    )
  }

  render() {
    const {
      waiting,
      relatedActivity,
      relatedEntity,
      relatedContact,
      taskAssigneeUserId,
      taskAssigneeName,
      taskAssigneeType,
      taskNotes,
      taskDescription,
      taskStartDate,
      taskEndDate,
      taskPriority,
      taskStatus,
      taskType,
      validationError,
      taskTypes,
      taskPriorities,
      taskStatuses,
      taskCreatedBy,
      relatedActivityList,
      relatedEntityList,
      relatedContactList
    } = this.state
    const { loginDetails: { userEntities } } = this.props
    const { userId } = userEntities[0]
    if (waiting) {
      return <Loader />
    }

    return (
      <div id="main">
        <section className="accordions">
          <article className="accordion is-active">
            <div className="accordion-header">
              <p>Task Details</p>
              <br/>
            </div>
            <div className="accordion-body">
              <div className="accordion-content">

                <div className="columns">
                  <TextLabelInput
                    required
                    name="taskDescription"
                    value={taskDescription}
                    placeholder="City of Lakewood - John Doe"
                    style={{ width: "100%" }}
                    label="Task name:"
                    onChange={this.changeField}
                    // disabled={!props.canEdit || false}
                    error={validationError && validationError.taskDescription ? validationError.taskDescription : ""}
                  />

                  <div className="column">
                    <p className="dashExpLblVal">Task Type
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                    </p>
                    {/* <div className="select is-link is-fullwidth is-small">
                      <select value={taskType} name="taskType"
                              onChange={this.changeField}>
                        <option value="">Select Task Type</option>
                        {taskTypes.map((t, i) => (
                          <option key={t + i} value={t}>{t && `${t[0].toUpperCase()}${t.slice(1)}`}</option>
                        ))}
                      </select>
                    </div> */}
                    <SelectLabelInput
                      placeholder="Select Task Type"
                      list={taskTypes || []}
                      name="taskType"
                      value={taskType || ""}
                      onChange={this.changeField}
                    />
                    {validationError && validationError.taskType &&
                    <small className="text-error">{validationError.taskType}</small>}
                  </div>
                </div>

                <div className="columns">
                  <div className="column">
                    <p className="dashExpLblVal">Task Priority
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                    </p>
                    {/* <div className="select is-link is-fullwidth is-small">
                      <select value={taskPriority} name="taskPriority"
                        onChange={this.changeField}>
                        <option key="nopriority" value="">Select Task Priority</option>
                         <option key="nopriority" value="" />
                        {taskPriorities.map((p, i) => (
                          <option key={p + i} value={p}>{p}</option>
                        ))}
                      </select>
                    </div> */}
                    <SelectLabelInput
                      placeholder="Select Task Priority"
                      list={taskPriorities || []}
                      name="taskPriority"
                      value={taskPriority || ""}
                      onChange={this.changeField}
                    />
                    {validationError && validationError.taskPriority &&
                    <small className="text-error">{validationError.taskPriority}</small>}
                  </div>
                  <div className="column">
                    <p className="dashExpLblVal">Task Status
                    </p>
                    {/* <div className="select is-link is-fullwidth is-small">
                      <select value={taskStatus === "Open" ? "Active" : taskStatus} name="taskStatus"
                        onChange={this.changeField}>
                        {taskStatuses.map((s, i) => (
                          <option key={s + i} value={s}>{s}</option>
                        ))}
                      </select>
                    </div> */}
                    <SelectLabelInput
                      list={taskStatuses || []}
                      name="taskStatus"
                      value={taskStatus === "Open" ? "Active" : taskStatus}
                      onChange={this.changeField}
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <p className="dashExpLblVal">Start Date
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                    </p>
                    <TextLabelInput
                      type="date"
                      name="taskStartDate"
                      value={(taskStartDate === "" || !taskStartDate) ? null : new Date(taskStartDate)}
                      onChange={this.changeField}
                    />
                    {validationError && validationError.taskStartDate &&
                    <small className="text-error">{validationError.taskStartDate}</small>}
                  </div>
                  <div className="column">
                    <p className="dashExpLblVal">Due Date
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                    </p>
                    <TextLabelInput
                      type="date"
                      name="taskEndDate"
                      value={(taskEndDate === "" || !taskEndDate) ? null : new Date(taskEndDate)}
                      onChange={this.changeField}
                    />
                    {validationError && validationError.taskEndDate &&
                    <small className="text-error">{validationError.taskEndDate}</small>}
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLbl">Assigned To</p>
                    <Multiselect
                      data={relatedContactList}
                      placeholder="Search & Select contacts"
                      textField="name"
                      caseSensitive={false}
                      minLength={1}
                      value={relatedContact}
                      filter="contains"
                      groupBy={({ firmName }) => firmName }
                      onChange={this.changeRelatedContact}
                    />
                    {validationError.taskSelectError &&
                    <small className="text-error">{validationError.taskSelectError}</small>}
                  </div>
                  <div className="column"/>
                </div>
              </div>

              <div style={{ margin: 20 }}>
                <div className="accordion-header" style={{ cursor: "default" }}>
                  <p>CHOOSE RELATED ACTIVITY / ENTITIES AND ENTER NOTES</p>
                </div>

                <div className="column">
                  <p className="multiExpLbl">Related Activity</p>
                  <Multiselect
                    data={relatedActivityList || []}
                    placeholder="Search & select activity by name"
                    textField="name"
                    caseSensitive={false}
                    minLength={1}
                    value={relatedActivity}
                    filter="contains"
                    onChange={this.changeRelatedActivity}
                    groupBy="group"
                  />
                </div>
                <div className="column">
                  <p className="multiExpLbl">Related Entity</p>
                  <Multiselect
                    data={relatedEntityList || []}
                    placeholder="Search & select entities"
                    textField="name"
                    caseSensitive={false}
                    minLength={1}
                    value={relatedEntity}
                    filter="contains"
                    groupBy="group"
                    onChange={this.changeRelatedEntity}
                  />
                </div>
                <div className="column">
                  <p className="dashExpLblVal">Notes / Instructions</p>
                  <textarea className="textarea is-link" placeholder="" name="taskNotes"
                    value={taskNotes} onChange={this.changeField}/>
                </div>
                {this.renderDocActionsTable()}
              </div>
            </div>
          </article>
        </section>
        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped-center">
              <div className="control">
                <button className="button is-link"
                  onClick={this.saveTaskDetails}>Save</button>
              </div>
              <div className="control">
                <button className="button is-light"
                  onClick={this.undoChanges}>Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default withRouter(TaskDetails)
