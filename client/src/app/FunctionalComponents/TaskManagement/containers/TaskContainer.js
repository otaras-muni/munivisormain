import React, { Component } from "react"
import {connect} from "react-redux"
import {withRouter} from "react-router"
import {toast} from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import uuidv4 from "uuid/v4"
import {TASK_RELATED_DOCUMENT} from "GlobalUtils/consts.js"
import { fetchDocDetails, getS3FileGetURL, getUsersDetailsWithRelatedActivity,
  saveTaskManager, getRelatedInfo, getTaskDetailById} from "AppState/actions/TaskManagement/taskManageActions"
import {getPicklistValues, convertError} from "GlobalUtils/helpers"
import LinkToTaskComponent from "../components/LinkToTask"
import UpdateLinkToTaskComponent from "../components/UpdateLinkToTask"
import { validateTaskMgt } from "./../../../Validations/taskMgt"
import Loader from "./../../../GlobalComponents/Loader"
import TaskComponent from "../components/MainTask"
import TitleComponent from "../components/MainTitle"
import DocDetails from "../../docs/DocDetails"
import CONST from "../../../../globalutilities/consts";

const dateCompare = (startDate, endDate)=>{
  if (startDate > endDate) return 1
  else if (startDate < endDate) return -1
  return 0
}
class TaskContainer extends Component {
  constructor(props) {
    super(props)
    this.state={
      taskMgt:{
        taskName:"",
        taskNotes:"",
        taskType:"",
        taskPriority:"",
        taskStartDate:"",
        taskDueDate:"",
        userTaskFollowing:true,
        taskCreatedBy:{
          userId:"",
          userFirstName:"",
          userLastName:"",
          userEntityName:"",
          userEntityId:""
        },
        taskAssignees:[],
        taskRelatedActivities:[],
        taskRelatedEntities:[],
        taskRelatedDocuments:[cloneDeep(TASK_RELATED_DOCUMENT)],
        activities:[]
      },
      taskMgtErrors:{
        taskName:"",
        taskNotes:"",
        taskType:"",
        taskPriority:"",
        taskStartDate:"",
        taskDueDate:"",
        taskAssignees:"",
        taskRelatedActivities:"",
        taskRelatedEntities:""
      },
      bucketName: CONST.bucketName,
      showModal: false,
      signedS3Url: "",
      tenantId:"",
      contextType:"",
      contextId:"",
      doc: {},
      resultData:[],
      relatedUsers:[],
      relatedActivicties:{},
      taskTypeData:[]
    }
    this.submitted = false
    this.intialtaskMgt = cloneDeep(this.state.taskMgt)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async componentWillMount () {
    this.setState({
      loading:true
    })
  }

  async componentDidMount () {
    const {userEntities} = this.props.auth.loginDetails
    let {relatedUsers, taskMgt} = this.state
    const {taskId} = this.props
    const {relatedActivicties} = this.state
    const resultData = await getUsersDetailsWithRelatedActivity(userEntities[0].entityId)
    if (resultData && Object.keys(resultData).length > 0) {
      relatedUsers = resultData.assigneeUsers.map(item => ({
        "entityId": item.entityId,
        "userEntityName": item.firmName,
        "userId": item.userId,
        "userFirstName": item.userFirstName,
        "userLastName": item.userLastName
      }))
    }
    const res = await getRelatedInfo()
    if (res && res.payload) {
      Object.keys(res.payload).forEach(k => {
        relatedActivicties[k] = res.payload[k].transformedTrans
      })
    }
    const [taskTypeResult, taskPriorityResult] = await getPicklistValues(["LKUPTASKTYPE", "LKUPTASKPRIORITY"])
    taskMgt.taskCreatedBy = {
      userId: userEntities[0].userId,
      userFirstName: userEntities[0].userFirstName,
      userLastName: userEntities[0].userLastName,
      userEntityName: userEntities[0].entityId,
      userEntityId: userEntities[0].firmName
    }
    this.setState(prevState => ({
      ...prevState,
      taskMgt,
      resultData,
      relatedActivicties,
      relatedUsers,
      tenantId: userEntities[0].entityId,
      contextId: uuidv4(),
      contextType: "Task",
      taskTypeData: taskTypeResult[1],
      taskPriorityData: taskPriorityResult[1],
      loading: false
    }))
    if (taskId && taskId.length === 24) {
      const taskDetails = await getTaskDetailById(taskId)
      if (!taskDetails.error) {
        taskMgt = {...taskDetails.data}
        this.setState({
          taskMgt
        })
      } else {
        toast(`${taskDetails.error}`, {autoClose: 3000, type: toast.TYPE.ERROR})
      }
    }
  }
  componentWillReceiveProps (nextProps) {
  }
  componentWillUpdate(){
  }
  componentDidUpdate(){
  }
  componentWillUnmount () {
  }
  setRelatedInfoInState(payload) {
    const activities = []
    const {taskMgt} = this.state
    Object.keys(payload).forEach(k => {
      if(payload[k] && payload[k].transactions) {
        activities.push(...payload[k].transactions.map(e => (
          {
            id: e.activityId,
            name: e.activityDescription,
            entities: e.participantEntityDetails ?
              e.participantEntityDetails.map(a => (
                {
                  id: a.participantEntityId,
                  name: a.firmName || a.msrbFirmName
                }
              )) : [],
            contacts: e.participantUsers ?
              e.participantUsers.map(u => (
                {
                  id: u.tranUserId,
                  name: `${u.tranUserFirstName || ""} ${u.tranUserLastName || ""}`
                }
              )) : []
          }
        )))
      }
    })
    taskMgt.activities = activities
    this.setState({ taskMgt })
  }
  changeObj = (key, list, currentVal)=>{
    const obj = list.filter(item => item.activityId!==currentVal.activityId)
  }

  onChangeTask = (e,dataItem)=>{
    const {taskMgt,taskMgtErrors} = this.state
    switch (e.target.name) {
    case "taskType":{
      taskMgt.taskRelatedActivities=[],
      taskMgt.taskRelatedEntities=[],
      taskMgt.taskAssignees=[],
      taskMgtErrors.taskRelatedActivities="",
      taskMgtErrors.taskRelatedEntities="",
      taskMgtErrors.taskAssignees=""
      break
    }
    case "taskRelatedActivities":{
      taskMgt.taskRelatedEntities=taskMgt.taskRelatedEntities.length>0 ? taskMgt.taskRelatedEntities.filter(item => item.activityId!==dataItem.activityId):[],
      taskMgt.taskAssignees=taskMgt.taskAssignees.length>0 ? taskMgt.taskAssignees.filter(item => item.activityId!==dataItem.activityId) : []
      break
    }
    case "taskRelatedEntities":{
      taskMgt.taskAssignees= taskMgt.taskAssignees.length > 0 ?  taskMgt.taskAssignees.filter(item => item.entityId!==dataItem.entityId): []
      break
    }
    default:
      break
    }
    e.target.name==="userTaskFollowing"? taskMgt[e.target.name] = !taskMgt[e.target.name]:taskMgt[e.target.name] = e.target.value
    const validator  ={
      [e.target.name]: e.target.value
    }
    this.setState(prevState => ({
      ...prevState,
      taskMgt
    }))
    if (this.submitted) {
      if(e.target.name==="taskStartDate" || e.target.name==="taskDueDate"){
        if(taskMgt.taskStartDate!=="" && taskMgt.taskDueDate!==""){
          const dateVal = dateCompare(taskMgt.taskStartDate, taskMgt.taskDueDate)
          switch (dateVal) {
          case 1:
            taskMgtErrors[e.target.name] = e.target.name ==="taskStartDate" ? "Start Date  should be less Exit Date.": "End Date  should be Greate Thant Start Date."
            break
          default:
            taskMgtErrors[e.target.name] =""
            break
          }
        } else if(taskMgt[e.target.name]===""){
          const dateVal = e.target.name ==="taskStartDate" ? "Start Date":"Due Date"
          taskMgtErrors[e.target.name] = `${dateVal} Required`
        }else{
          taskMgtErrors[e.target.name] = ""
        }
        this.setState({
          taskMgtErrors
        })
        return
      }
      const errorData = validateTaskMgt(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.context.key === e.target.name) {
          taskMgtErrors[e.target.name] = `${err.context.label} Required`
          this.setState({
            taskMgtErrors
          })
        } else {
          taskMgtErrors[e.target.name] = ""
          this.setState({
            taskMgtErrors
          })
        }
      } else {
        taskMgtErrors[e.target.name] = ""
        this.setState({
          taskMgtErrors
        })
      }
    }

  }

  onFileAction = (value, docId) => {
    fetchDocDetails(docId).then(res => {
      if(value === "open") {
        this.getFileURL(this.state.bucketName, res.name, this.downloadFile)
      } else if(value === "version") {
        this.handleDocDetails(res)
      }
    })
  }
  getBidBucket = (filename, docId, index) => {
    const taskRelatedDocument = _.cloneDeep(this.state.taskMgt.taskRelatedDocuments)
    taskRelatedDocument[index].docId = docId
    taskRelatedDocument[index].taskDocumentName = filename
    taskRelatedDocument[index].taskDocumentId = this.state.contextId
    taskRelatedDocument[index].taskDocumentUploadDate = new Date()
    taskRelatedDocument[index].taskDocumentUploadUser = this.state.taskMgt.taskCreatedBy.userId
    this.setState(prevState => ({
      taskMgt: {
        ...prevState.taskMgt,
        taskRelatedDocuments:taskRelatedDocument
      }
    }))
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, (res) => {
      this.setState({
        ...res
      }, callback.bind(this, bucketName, fileName))
    })
  }
  downloadFile = () => {
    this.downloadAnchor.click()
  }
  handleDocDetails(res) {
    this.setState((prevState) => ({
      showModal: !prevState.showModal,
      doc: res || {}
    }))
  }

   handleSubmit = async(e) => {
     e.preventDefault()
     this.submitted = true
     const {taskMgtErrors}  = this.state
     const {taskId} = this.props
     let {taskMgt} = this.state
     const {userEntities} = this.props.auth.loginDetails
     const validateData = {
       taskName:taskMgt.taskName,
       taskType:(taskMgt.taskType!=="" ? taskMgt.taskType : ""),
       taskPriority:taskMgt.taskPriority,
       taskNotes:taskMgt.taskNotes,
       taskStartDate:taskMgt.taskStartDate,
       taskDueDate:taskMgt.taskDueDate,
       taskAssignees:taskMgt.taskAssignees,
       taskRelatedActivities:taskMgt.taskRelatedActivities,
       taskRelatedEntities:taskMgt.taskRelatedEntities
     }
     try{
       const errors = validateTaskMgt(validateData)
       if (errors.error) {
         convertError(taskMgtErrors,errors)
         this.setState({
           taskMgtErrors
         })
       } else {
         this.setState({
           loading:true
         })
         const result = await saveTaskManager(taskMgt, taskId)
         if(result.error===""){
           taskMgt = cloneDeep(this.intialtaskMgt)
           taskMgt.taskCreatedBy = {
             userId:userEntities[0].userId,
             userFirstName:userEntities[0].userFirstName,
             userLastName:userEntities[0].userLastName
           }
           this.setState((prevState) => ({ ...prevState,
             ...{
               taskMgt,
               signedS3Url: "",
               tenantId:"",
               contextType:"",
               contextId:"",
               doc: {}
             }}))
           toast(`Task ${taskId ? "Updated" : "Created"} Successfully.`,{ autoClose: 3000, type: toast.TYPE.SUCCESS })
           this.submitted = false
         }else{
           const {response} = result
           toast(`${response.data.error}`,{ autoClose: 3000, type: toast.TYPE.ERROR })
         }
         this.setState({
           loading:false
         }, (
             () => {
               if(taskId) {
                 this.props.history.push(window.location.pathname)
               }
             }
           ))
       }
     }catch(ex){
       console.log("Exception==>>",ex)
     }
   }
   render () {
     const loading = () => <Loader/>
     if(this.state.loading) {
       return loading()
     }
     const {taskMgt, relatedActivicties, taskMgtErrors,taskTypeData,resultData, taskPriorityData } = this.state
     return (
       <div id="main">
         <section className="container">
           <div className="column box">
             <p className="title innerPgTitle">Add New Task</p>
             <TitleComponent
               userTaskFollowing={taskMgt.userTaskFollowing}
               onChangeTask = {this.onChangeTask}
             />
             <TaskComponent
               taskMgt={taskMgt}
               onChangeTask = {this.onChangeTask}
               taskMgtErrors = {taskMgtErrors}
               resultData = {resultData}
               relatedActivicties={relatedActivicties}
               taskTypeData = {taskTypeData}
               taskPriorityData = {taskPriorityData}
             />
             <LinkToTaskComponent
               taskMgt={taskMgt}
               onChangeTask = {this.onChangeTask}
               handleSubmit = {this.handleSubmit}
               resultData = {this.state.resultData}
               relatedActivicties={this.state.relatedActivicties}
               taskMgtErrors = {this.state.taskMgtErrors}
               bucketName={this.state.bucketName}
               getBidBucket={this.getBidBucket}
               onFileAction = {this.onFileAction}
               tenantId={this.state.tenantId}
               contextType={this.state.contextType}
               contextId={this.state.contextId}
               relatedUsers = {this.state.relatedUsers}
             />
           </div>
         </section>
         <a  className="hidden-download-anchor"style={{ display: "none" }} ref={ el => { this.downloadAnchor = el } }href={this.state.signedS3Url} target="_blank" />
         {this.state.showModal ?
           <DocDetails showModal={this.state.showModal}
             closeDocDetails={this.handleDocDetails}
             bucketName={this.state.bucketName}
             downloadAnchor={this.downloadAnchor}
             doc={this.state.doc}/>
           : undefined
         }
       </div>
     )
   }
}
const mapStateToProps = (state) => {
  const {auth} = state
  return {auth}
}
export default connect(mapStateToProps, null)(withRouter(TaskContainer))
