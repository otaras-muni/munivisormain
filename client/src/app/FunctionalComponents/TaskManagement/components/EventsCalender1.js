import React, { Component } from "react"
import { connect } from "react-redux"
import moment from "moment"
import BigCalendar from "react-big-calendar"
import Nav from "../../Dashboard/components/Nav"
import "react-big-calendar/lib/css/react-big-calendar.css"
import { fetchUserTransactions } from "../../../StateManagement/actions/CreateTransaction"
import Loader from "../../../GlobalComponents/Loader"
import { SelectLabelInput } from "../../../GlobalComponents/TextViewBox"
import { fetchCurrentFirmTasksList } from "../../../StateManagement/actions/TaskManagement/taskManageActions"
const allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k])
BigCalendar.momentLocalizer(moment) // or globalizeLocalizer

const navigatePath = [
  { name: "Manage RFP for Client", label: "Managed Client RFP", path: "rfp" },
  { name: "Debt / Bond Issue", path: "deals" },
  { name: "Debt / Bank Loan", path: "loan" },
  { name: "MA-RFP", path: "marfp" },
  { name: "Derivative / Swap", label: "Derivative", path: "derivative" },
  { name: "Derivative / Cap", label: "Derivative", path: "derivative" }
]
class EventsCalender extends Component {
  constructor() {
    super()
    this.state = {
      events: [],
      loading: true,
      filterBy: "All",
      transactionType: "All",
      dropDown: {
        filter: ["My Tasks", "Transaction Milestones", "My Firm Tasks", "All"],
        transactionTypes: [
          "All",
          "Debt / Bond Issue",
          "Debt / Bank Loan",
          "Manage RFP for Client",
          "MA-RFP",
          "Derivative / Swap",
          "Derivative / Cap"
        ]
      }
    }
  }

  async componentDidMount() {
    this.eventMaker()
  }

  eventMaker = async () => {
    const res = await fetchUserTransactions(
      `${this.props.user.entityId}?type=calender`
    )
    const events = []
    if (res.transactions && Array.isArray(res.transactions)) {
      res.transactions.forEach(tran => {
        if (
          /*tran.rfpTranType === "RFP" ||*/ tran.dealIssueTranSubType ===
            "Bond Issue" ||
          tran.actTranSubType === "Bank Loan" ||
          tran.actTranType === "Derivative" ||
          tran.actType === "MA-RFP"
        ) {
          const newObject = {
            ...tran,
            id:
              tran.actTranUniqIdentifier ||
              tran.dealIssueTranName ||
              tran.rfpTranName ||
              tran.actUniqIdentifier,
            allDay: true,
            title: `${tran.actTranProjectDescription ||
              tran.rfpTranProjectDescription ||
              tran.dealIssueTranProjectDescription ||
              tran.actProjectName ||
              ""}, ${tran.rfpTranIssueName ||
              tran.dealIssueTranIssueName ||
              tran.actTranIssueName ||
              tran.actIssueName ||
              ""}`
          }
          if (tran.dealIssueTranSubType === "Bond Issue") {
            newObject.type = `Debt / ${tran.dealIssueTranSubType}`
            if (tran.dealIssuePricingDate) {
              newObject.start = tran.dealIssuePricingDate
                ? new Date(tran.dealIssuePricingDate)
                : null
              newObject.end = tran.dealIssuePricingDate
                ? new Date(tran.dealIssuePricingDate)
                : null
            }
            if (tran.dealIssueExpAwardDate) {
              newObject.start = tran.dealIssueExpAwardDate
                ? new Date(tran.dealIssueExpAwardDate)
                : null
              newObject.end = tran.dealIssueExpAwardDate
                ? new Date(tran.dealIssueExpAwardDate)
                : null
            }
            events.push(newObject)
          }
          if (tran.actTranSubType === "Bank Loan") {
            newObject.type = `Debt / ${tran.actTranSubType}`
            newObject.start =
              tran.bankLoanSummary && tran.bankLoanSummary.actTranClosingDate
                ? new Date(tran.bankLoanSummary.actTranClosingDate)
                : null
            newObject.end =
              tran.bankLoanSummary && tran.bankLoanSummary.actTranClosingDate
                ? new Date(tran.bankLoanSummary.actTranClosingDate)
                : null
            events.push(newObject)
          }
          if (tran.actTranSubType === "Swap" || tran.actTranSubType === "Cap") {
            newObject.type = `Derivative / ${tran.actTranSubType}`
            newObject.start =
              tran.derivativeSummary && tran.derivativeSummary.tranEffDate
                ? new Date(tran.derivativeSummary.tranEffDate)
                : null
            newObject.end =
              tran.derivativeSummary && tran.derivativeSummary.tranEffDate
                ? new Date(tran.derivativeSummary.tranEffDate)
                : null
            events.push(newObject)
          }
          if (tran.actType === "MA-RFP") {
            newObject.type = tran.actType
            if (tran.maRfpSummary && tran.maRfpSummary.actExpAwaredDate) {
              newObject.start =
                tran.maRfpSummary && tran.maRfpSummary.actExpAwaredDate
                  ? new Date(tran.maRfpSummary.actExpAwaredDate)
                  : null
              newObject.end =
                tran.maRfpSummary && tran.maRfpSummary.actExpAwaredDate
                  ? new Date(tran.maRfpSummary.actExpAwaredDate)
                  : null
              events.push(newObject)
            }
            if (tran.maRfpSummary && tran.maRfpSummary.actPricingDate) {
              newObject.start =
                tran.maRfpSummary && tran.maRfpSummary.actPricingDate
                  ? new Date(tran.maRfpSummary.actPricingDate)
                  : null
              newObject.end =
                tran.maRfpSummary && tran.maRfpSummary.actPricingDate
                  ? new Date(tran.maRfpSummary.actPricingDate)
                  : null
              events.push(newObject)
            }
          }
        }
      })
    }
    let tasks = await fetchCurrentFirmTasksList("My Firm Tasks")
    tasks.forEach(task => {
      task.id = task.taskIdentifier
      task.allDay = true
      task.title = task.taskDescription || "" // task.activityType
      task.filter = "My Firm Tasks"
      task.type =
        task.activityType === "Debt"
          ? `Debt / ${task.activitySubType || ""}`
          : task.activityType === "Derivative"
          ? `Derivative / ${task.activitySubType || ""}`
          : task.activityType || ""
      task.start =
        task.taskEndDate && task.taskEndDate ? new Date(task.taskEndDate) : null
      task.end =
        task.taskEndDate && task.taskEndDate ? new Date(task.taskEndDate) : null
    })
    const allEvents = tasks.concat(events)

    console.log("===Calender events==>", events)
    this.setState({
      events: allEvents,
      allTransactions: events,
      loading: false
    })
  }

  event = ({ event }) => {
    return (
      <span>
        <strong>{`${event.title || event.id || ""}`}</strong>
      </span>
    )
  }

  eventAgenda = ({ event }) => {
    const title = `${event.id || ""}${event.id ? " - " : ""}${event.title ||
      ""}`
    return (
      <span>
        <em>{title}</em>
      </span>
    )
  }

  onChange = async e => {
    const { filterBy, allTransactions } = this.state
    let events = []
    let object = {}
    const { name, value } = e.target

    if (filterBy === "Transaction Milestones" && name === "transactionType") {
      events =
        !value || value === "All"
          ? allTransactions
          : allTransactions.filter(tran => tran.type === value)
    } else if (name === "filterBy" && value === "All") {
      let tasks = await fetchCurrentFirmTasksList("My Firm Tasks")
      tasks.forEach(task => {
        task.id = task.taskIdentifier
        task.allDay = true
        task.title = task.taskDescription // task.activityType
        task.filter = "My Firm Tasks"
        task.type =
          task.activityType === "Debt"
            ? `Debt / ${task.activitySubType}`
            : task.activityType === "Derivative"
            ? `Derivative / ${task.activitySubType}`
            : task.activityType
        task.start =
          task.taskEndDate && task.taskEndDate
            ? new Date(task.taskEndDate)
            : null
        task.end =
          task.taskEndDate && task.taskEndDate
            ? new Date(task.taskEndDate)
            : null
      })
      events = tasks.concat(allTransactions)
    } else if (name === "filterBy") {
      events = value === "Transaction Milestones" ? allTransactions : []
      object = {
        transactionType: "All"
      }
    }
    this.setState(
      {
        [name]: value,
        ...object,
        events
      },
      () => {
        if (
          this.state.filterBy === "My Firm Tasks" ||
          this.state.filterBy === "My Tasks"
        ) {
          this.setState(
            {
              loading: true
            },
            () => {
              this.getFirmTask(this.state.filterBy)
            }
          )
        }
      }
    )
  }

  getFirmTask = async filter => {
    const tasks = await fetchCurrentFirmTasksList(filter)
    tasks.forEach(task => {
      task.id = task.taskIdentifier
      task.allDay = true
      task.title = task.taskDescription // task.activityType
      task.filter = filter
      task.type =
        task.activityType === "Debt"
          ? `Debt / ${task.activitySubType}`
          : task.activityType === "Derivative"
          ? `Derivative / ${task.activitySubType}`
          : task.activityType
      task.start =
        task.taskEndDate && task.taskEndDate ? new Date(task.taskEndDate) : null
      task.end =
        task.taskEndDate && task.taskEndDate ? new Date(task.taskEndDate) : null
    })
    this.setState({
      events: tasks,
      loading: false
    })
  }

  onEventClick = event => {
    const filters = ["My Firm Tasks", "My Tasks"]
    const notIn = ["Derivative / Swap", "Derivative / Cap", "MA-RFP"]
    const type = navigatePath.find(
      nav => nav.name === event.type || nav.label === event.type
    )
    const tranId =
      filters.indexOf(event.filter) !== -1 ? event.activityId : event._id
    const route =
      filters.indexOf(event.filter) !== -1 && notIn.indexOf(event.type) === -1
        ? "check-track"
        : "summary"
    if (type) this.props.history.push(`${type.path}/${tranId}/${route}`)
  }

  eventStyleGetter = (event, start, end, isSelected) => {
    var bgColor =
      event.filter === "My Tasks"
        ? "#77C2FF"
        : event.filter === "My Firm Tasks"
        ? "#FFC677"
        : event.filter === "Pick"
        ? "#BBE0FF"
        : "FFDFB3"
    const style = {
      backgroundColor: bgColor,
      color: "black",
      display: "block",
      borderColor: "#000"
    }
    return {
      style: style
    }
  }

  render() {
    const { events, loading, filterBy, transactionType, dropDown } = this.state
    return (
      <div>
        <Nav />
        <div id="main">
          <section>
            <div className="columns">
              <div className="column is-6">
                <div className="columns is-gapless">
                  <div className="column has-text-left is-narrow">
                    Filter By &nbsp; &nbsp; &nbsp;
                  </div>
                  <div className="column">
                    <SelectLabelInput
                      list={dropDown.filter || []}
                      name="filterBy"
                      value={filterBy || ""}
                      onChange={this.onChange}
                      className="select is-link is-fullwidth"
                    />
                  </div>
                  {filterBy === "Transaction Milestones" ? (
                    <div className="column has-text-right is-narrow">
                      &nbsp; &nbsp; &nbsp;Type or Subtype &nbsp; &nbsp; &nbsp;
                    </div>
                  ) : null}
                  {filterBy === "Transaction Milestones" ? (
                    <div className="column">
                      <SelectLabelInput
                        list={dropDown.transactionTypes || []}
                        name="transactionType"
                        value={transactionType || ""}
                        onChange={this.onChange}
                        className="select is-link is-fullwidth"
                      />
                    </div>
                  ) : null}
                </div>
              </div>
              <div className="column">&nbsp;</div>
            </div>

            {loading ? (
              <Loader />
            ) : (
              <div className="box" style={{ height: "80vh" }}>
                <BigCalendar
                  selectable
                  events={events}
                  views={allViews}
                  step={60}
                  defaultView="month"
                  showMultiDayTimes={false}
                  defaultDate={new Date()}
                  onSelectSlot={slotInfo => console.log(slotInfo)}
                  onSelectEvent={event => this.onEventClick(event)}
                  onSelecting={range => console.log(range)}
                  eventPropGetter={this.eventStyleGetter}
                  components={{
                    event: this.event,
                    agenda: {
                      event: this.eventAgenda
                    }
                  }}
                />
              </div>
            )}
          </section>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(
  mapStateToProps,
  null
)(EventsCalender)
