import React from "react"
import "../scss/tasks.css"


// <DisplayChecklistItemForChecklist checkListItems={checklist.checkListItems} />

const DisplayTrelloBoardView = props => {
  const { boards, projectId } = props
  return (
    <div>
      <h4>{props.projectId}</h4>
      <div className="taskcontainer">
        {boards.map((boardObject) =>
          // const boardObject = Object.values(board)[0];
          (
            <div
              key={`${boardObject.boardId}-${projectId}-${boardObject.boardTitle}`}
              className="boardcontainer"
            >
              <div className="boardheader">{boardObject.boardTitle}</div>
              <div className="boarditemadd" onClick={() => alert(boardObject.boardId)} role="presentation" onKeyPress={() => { }}
              >
                +
              </div>
              {boardObject.cards.map((cardObject) =>
                // const cardObject = Object.values(card)[0];
                (
                  <div
                    key={`${boardObject.boardId}-${projectId}-${boardObject.boardTitle}-${
                      cardObject.cardId
                      }-${cardObject.cardTitle}`}
                    className="boarditem"
                    role="presentation"
                    onClick={e => console.log(e.target)}
                    onKeyPress={() => { }}
                  >
                    {cardObject.cardTitle}
                  </div>
                )
              )}
            </div>
          )
        )}
      </div>
    </div>
  )
}

const CardModal = () => (
  <div className="modal">
    <div className="modal-background" />
    <div className="modal-card">
      <header className="modal-card-head">
        <p className="modal-card-title">Modal title</p>
        <button className="delete" aria-label="close" />
      </header>
      <section className="modal-card-body" />
      <footer className="modal-card-foot">
        <button className="button is-success">Save changes</button>
        <button className="button">Cancel</button>
      </footer>
    </div>
  </div>
)

const ProjectList = props => {
  const { projects } = props
  console.log("IAM IN THE PROJECT LIST COMPONENT", projects)
  return (
    <div>
      <ul>
        {projects.map((project) => (
          <DisplayTrelloBoardView key={`project - ${project.projectId} -${project.projectName}`} {...project} />
        ))}
      </ul>
      <CardModal />
    </div>
  )
}

export default ProjectList
