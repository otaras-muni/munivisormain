import React from "react"
import {Multiselect, DropdownList} from "react-widgets"
import moment from "moment"

const MainTask = (props) => (
  <div>    
    <div className="columns">
      <div className="column">
        <p className="dashExpLblVal">Task name:</p>
        <input
          className="input is-small is-link"
          name="taskName"
          type="text"
          placeholder="City of Lakewood - John Doe"
          value={props.taskMgt.taskName}
          onChange={props.onChangeTask}/> {props.taskMgtErrors && props.taskMgtErrors.taskName && <small className="text-error">{props.taskMgtErrors.taskName}</small>}
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <p className="dashExpLblVal">Task Type</p>
        <DropdownList
          filter
          data={Object
            .keys(props.taskTypeData)
            .length > 0
            ? props.taskTypeData
            : []}
          name="taskType"
          textField={(item) => item.length>0 ? item.charAt(0).toUpperCase() + item.slice(1):item}
          value={props.taskMgt.taskType || []}
          onChange={(val) => {
            const event = {
              target: {
                name: "taskType",
                value: val
              }
            }
            props.onChangeTask(event)
          }}/> {props.taskMgtErrors && props.taskMgtErrors.taskType && <small className="text-error">{props.taskMgtErrors.taskType}</small>}
      </div>
      <div className="column">
        <p className="dashExpLblVal">Task Priority</p>
        <DropdownList
          filter
          data={Object
            .keys(props.taskPriorityData)
            .length > 0
            ? props.taskPriorityData
            : []}
          name="taskPriority"
          value={props.taskMgt.taskPriority || []}
          onChange={(val) => {
            const event = {
              target: {
                name: "taskPriority",
                value: val
              }
            }
            props.onChangeTask(event)
          }}/> {props.taskMgtErrors && props.taskMgtErrors.taskPriority && <small className="text-error">{props.taskMgtErrors.taskPriority}</small>}
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <p className="dashExpLblVal">Start Date</p>
        <input
          className="input is-small is-link"
          type="date"
          name="taskStartDate"
          min={new Date()}
          value={(props.taskMgt.taskStartDate && moment(props.taskMgt.taskStartDate).format("YYYY-MM-DD")) || ""}
          onChange={props.onChangeTask}/> {props.taskMgtErrors && props.taskMgtErrors.taskStartDate && <small className="text-error">{props.taskMgtErrors.taskStartDate}</small>}
      </div>
      <div className="column">
        <p className="dashExpLblVal">Due Date</p>
        <input
          placeholder="Pick Date"
          className="input is-small is-link"
          type="date"
          id="taskDueDate"
          name="taskDueDate"
          value={(props.taskMgt.taskDueDate && moment(props.taskMgt.taskDueDate).format("YYYY-MM-DD")) || ""}
          onChange={props.onChangeTask}/> {props.taskMgtErrors && props.taskMgtErrors.taskDueDate && <small className="text-error">{props.taskMgtErrors.taskDueDate}</small>}
      </div>
    </div>
    <br/>
    <hr/>
  </div>
)
export default MainTask
