import React, { Component } from "react"
import BigCalendar from "react-big-calendar"
import moment from "moment"
import "react-big-calendar/lib/css/react-big-calendar.css"

const allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k])
BigCalendar.momentLocalizer(moment) // or globalizeLocalizer

const projList = [
  {
    tranId: 1,
    tranDesc: "Tran ID - Issuer 1",
    userList: ["Will"],
    tranType: "RFP",
    firm: "Ford & Associates",
    checkLists: [
      {
        chkListId: "Proj1 - chkitem1",
        chkListTitle: "Finalize the RFP rating methodology",
        startDate: moment()
          .add(Math.floor(Math.random() * 4), "d")
          .toDate(),
        endDate: moment()
          .add(4 + Math.floor(Math.random() * 5), "d")
          .toDate()
      },
      {
        chkListId: "Proj1 - chkitem2",
        chkListTitle: "Provide transaction package to all participants",
        startDate: moment()
          .subtract(Math.floor(Math.random() * 5), "d")
          .toDate(),
        endDate: moment().toDate()
      }
    ]
  },
  {
    tranId: 2,
    userList: ["Will", "Jon"],
    tranType: "Deal",
    firm: "Ford & Associates",
    checkLists: [
      {
        chkListId: "Proj2 - chkitem1",
        chkListTitle: "Get Cusip from Cusip Agency ",
        startDate: moment()
          .add(Math.floor(Math.random() * 5), "d")
          .toDate(),
        endDate: moment()
          .add(5 + Math.floor(Math.random() * 2), "d")
          .toDate()
      },
      {
        chkListId: "Proj2 - chkitem2",
        chkListTitle: "Call Rating Agency to get ratings",
        startDate: moment()
          .subtract(1, "d")
          .toDate(),
        endDate: moment()
          .add(1, "d")
          .toDate()
      }
    ]
  },

  {
    tranId: 3,
    userList: ["Jon"],
    firm: "Ford & Associates",
    tranType: "Deal",
    checkLists: [
      {
        chkListId: "Proj3 - chkitem1",
        chkListTitle: "Call Treasurer at the location ",
        startDate: moment()
          .subtract(2, "d")
          .toDate(),
        endDate: moment()
          .subtract(1, "d")
          .toDate()
      },
      {
        chkListId: "Proj3 - chkitem2",
        chkListTitle: "Tran ID 3 - Provide Printed Copies to Issuer",
        startDate: moment()
          .add(10, "d")
          .toDate(),
        endDate: moment()
          .add(11, "d")
          .toDate()
      }
    ]
  },
  {
    tranId: 4,
    userList: ["Naveen"],
    tranType: "RFP",
    firm: "Another Tenant",
    checkLists: [
      {
        chkListId: "Proj4 - chkitem1",
        chkListTitle: "Submit Rating for the Deal",
        startDate: moment()
          .subtract(6, "d")
          .toDate(),
        endDate: moment()
          .subtract(4, "d")
          .toDate()
      },
      {
        chkListId: "Proj4 - chkitem2",
        chkListTitle: "Submit RFP for Deal transaction 4",
        startDate: moment()
          .subtract(2, "d")
          .toDate(),
        endDate: moment()
          .add(2, "d")
          .toDate()
      }
    ]
  }
]

class BigCalendarComponent extends Component {
  state = { projects: projList, renderContext: "User" };

  getDealsForFirm(firmName) {
    return this.state.projects.filter(proj => proj.firm === firmName)
  }

  getDealsForFirmAndUser(firmName, userName) {
    return this.state.projects.filter(
      proj => proj.firm === firmName && proj.userList.indexOf(userName) >= 0
    )
  }

  getProjectsToRender() {
    return this.state.renderContext === "User"
      ? this.getDealsForFirmAndUser("Ford & Associates", "Will")
      : this.getDealsForFirm("Ford & Associates")
  }

  getFlattenedTasks(projects) {
    return projects.reduce((finalCheckListItems, project) => {
      const tasksForProject = project.checkLists.map(checkList => ({ tranId: project.tranId, ...checkList }))
      return [...finalCheckListItems, ...tasksForProject]
    }, [])
  }

  render() {
    const projects = this.getProjectsToRender()
    const allTaskItems = this.getFlattenedTasks(projects)
    return (
      <div>
        <ul>
          {allTaskItems.map((task) => <li key={`calendar events ${task.tranId} - ${task.chkListId}`}>{task.tranId}</li>)}
        </ul>

        <BasicCalendar eventsTobeRendered={allTaskItems} />
      </div>
    )
  }
}

function Event({ event }) {
  return (
    <span>
      <strong>{`${event.tranId} -${event.chkListId}`}</strong>
      {event.chkListTitle && `:  ${event.chkListTitle}`}
    </span>
  )
}

function EventAgenda({ event }) {
  return (
    <span>
      <em>{`${event.tranId} -${event.chkListId} - ${event.chkListTitle}`}</em>
      <p>{event.chkListTitle}</p>
    </span>
  )
}

export const BasicCalendar = props => {
  const { eventsTobeRendered } = props

  return (
    <div style={{ height: "80vh" }}>
      <BigCalendar
        selectable
        events={eventsTobeRendered}
        views={allViews}
        startAccessor='startDate'
        endAccessor='endDate'
        step={60}
        defaultView="agenda"
        showMultiDayTimes={false}
        defaultDate={new Date()}
        onSelectSlot={slotInfo => console.log(slotInfo)}
        onSelectEvent={event => alert(event.chkListId)}
        onSelecting={range => console.log(range)}
        components={{
          event: Event,
          agenda: {
            event: EventAgenda
          }
        }}
      />
    </div>
  )
}

export default BigCalendarComponent