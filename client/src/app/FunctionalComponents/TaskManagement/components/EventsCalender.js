import React, { Component } from "react"
import { connect } from "react-redux"
import moment from "moment"
import BigCalendar from "react-big-calendar"
import Nav from "../../Dashboard/components/Nav"
import "react-big-calendar/lib/css/react-big-calendar.css"
import Loader from "../../../GlobalComponents/Loader"
import {
  getDashboardData,
  getPicklistValues,
  updateTask,
  getToken
} from "GlobalUtils/helpers"
import { SelectLabelInput } from "../../../GlobalComponents/TextViewBox"

const allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k])
BigCalendar.momentLocalizer(moment) // or globalizeLocalizer

const navigatePath = [
  { name: "Manage RFP for Client", label: "Managed Client RFP", path: "rfp" },
  { name: "Debt / Bond Issue", path: "deals" },
  { name: "Debt / Bank Loan", path: "loan" },
  { name: "MA-RFP", path: "marfp" },
  { name: "Derivative / Swap", label: "Derivative", path: "derivative" },
  { name: "Derivative / Cap", label: "Derivative", path: "derivative" }
]

class EventsCalender extends Component {
  constructor() {
    super()
    this.state = {
      events: [],
      loading: true,
      filterBy: "All",
      transactionType: "All",
      dropDown: {
        transactionTypes: [
          "All",
          "Debt / Bond Issue",
          "Debt / Bank Loan",
          "Manage RFP for Client",
          "MA-RFP",
          "Derivative / Swap",
          "Derivative / Cap",
          "Business Development"
        ]
      },
      activityTime: "month",
      activityCategory: "self",
      otherActivityTime: "",
      allTasks: [],
      calendarStart: "",
      calendarEnd: "",
      showOnlyTasks: "All Tasks"
    }
  }

  async componentDidMount() {
    // this.eventMaker()
    this.pollDashboardData()
  }

    pollDashboardData = () => {
    console.log("in pollDashboardData : ")
    const { token, user } = this.props
    const { activityCategory, calendarStart, calendarEnd, activityTime } = this.state
    if (token) {
      this.setState(
        {
          loading: true
        },
        async () => {
          const result = await getDashboardData(getToken(), {
            groupBy: "",
            activityCategory,
            activityTime,
            taskStatus: "",
            searchTerm: "",
            calendarStart,
            calendarEnd,
            loading: false
          })
          let transactions = ""
          if (result) {
            transactions = result.allData
          }
          console.log("**************result***********", transactions)
          this.setEventState(transactions)
        }
      )
    }
  }

  setEventState = (transactions) => {
    const tasks = []
    const events = []
    const {showOnlyTasks} = this.state
    transactions.deals.forEach(tran => {
      const newObject = {
        // ...tran,
        tranType: tran.tranType,
        tranSubType: tran.tranSubType,
        id: tran.tranId || "",
        taskId: tran.taskId,
        cid: tran.taskActivityContext,
        allDay: true,
        title: `${tran.taskDescription || ""}`
      }
      newObject.type = `Debt / ${tran.tranSubType}`
      if (tran.tranPricingDate && tran.tranPricingDate !== "-" && tran.tranPricingDate !== "--") {
        newObject.start = tran.tranPricingDate
          ?  moment(new Date(tran.tranPricingDate).toISOString().substring(0, 10)).utc()
          : null
        newObject.end = tran.tranPricingDate
          ? moment(new Date(tran.tranPricingDate).toISOString().substring(0, 10)).utc()
          : null
      }
      if (tran.taskEndDate) {
        newObject.start = tran.taskEndDate
          ? moment(new Date(tran.taskEndDate).toISOString().substring(0, 10)).utc()
          : null
        newObject.end = tran.taskEndDate
          ? moment(new Date(tran.taskEndDate).toISOString().substring(0, 10)).utc()
          : null
      }
      events.push(newObject)
    })

    Object.keys(transactions).forEach(keyTypes => {
      if (
        transactions &&
        transactions[keyTypes] &&
        transactions[keyTypes].length &&
        [
          "rfps",
          "marfps",
          "busdevs",
          "others",
          "bankloans",
          "derivatives"
        ].indexOf(keyTypes) !== -1
      ) {
        transactions[keyTypes].forEach(tran => {
          const newObject = {
            // ...tran,
            tranType: tran.tranType,
            tranSubType: tran.tranSubType,
            id: tran.tranId || "",
            taskId: tran.taskId,
            cid: tran.taskActivityContext,
            allDay: true,
            title: `${tran.taskDescription || ""}`
          }
          newObject.type =
            keyTypes === "derivatives"
              ? `Derivative / ${tran.tranSubType}`
              : keyTypes === "bankloans"
                ? `Debt / ${tran.tranSubType}`
                : keyTypes === "rfps"
                  ? "Manage RFP for Client"
                  : keyTypes === "busdevs"
                    ? "Business Development" : tran.tranSubType
          console.log(newObject.type)
          newObject.start = tran.taskEndDate
            ?  moment(new Date(tran.taskEndDate).toISOString().substring(0, 10)).utc()
            : null
          newObject.end = tran.taskEndDate
            ?  moment(new Date(tran.taskEndDate).toISOString().substring(0, 10)).utc()
            : null
          events.push(newObject)
        })
      }
    })
    console.log("**************events***********", events)

    transactions && (transactions.catchalltasks || []).forEach(tran => {
      const newObject = {
        ...tran,
        tranType: tran.tranType,
        tranSubType: tran.tranSubType,
        id: tran.tranId || "",
        taskId: tran.taskId,
        cid: tran.taskActivityContext,
        allDay: true,
        title: `${tran.taskDescription || ""}`
      }
      newObject.type = tran.tranSubType
      if (tran.taskEndDate) {
        newObject.start = tran.taskEndDate
          ? moment(new Date(tran.taskEndDate).toISOString().substring(0, 10)).utc()
          : null
        newObject.end = tran.taskEndDate
          ? moment(new Date(tran.taskEndDate).toISOString().substring(0, 10)).utc()
          : null
      }
      tasks.push(newObject)
    })

    this.setState({
      events: showOnlyTasks === "All Tasks" ? [...tasks, ...events] :
        showOnlyTasks === "Compliance Tasks" ? tasks :
          showOnlyTasks === "Transactions Tasks" ? events : [],
      allEvents: events,
      allTasks: tasks || [],
      transactions,
      loading: false
    })
  }

  onFilter = event => {
    this.setState(
      {
        transactionType: "All",
        [event.target.name]: event.target.value
      },
      () => this.pollDashboardData()
    )
  }

  onTranTypeChange = event => {
    const { allEvents } = this.state
    const { name, value } = event.target
    this.setState(
      {
        [name]: value
      },
      () => {
        const events =
          value === "All"
            ? allEvents
            : allEvents.filter(tran => tran.type === value)
        this.setState({ events })
      }
    )
  }

  event = ({ event }) => (
    <span>
      <strong>{`${event.title || event.id || ""}`}</strong>
    </span>
  )

  eventAgenda = ({ event }) => {
    const title = `${event.id || ""}${event.id ? " - " : ""}${event.title ||
      ""}`
    return (
      <span>
        <em>{title}</em>
      </span>
    )
  }

  onEventClick = event => {
    // console.log("event : ", event)
    if(event.taskId && !event.id) {
      if(event.taskType === "Compliance"){
        this.props.history.push(`/cac/view-control/view-detail?id=${event.taskNotes}`)
      }else {
        this.props.history.push(`/tasks/${event.taskId}`)
      }
    } else {
      const notIn = ["Derivative / Swap", "Derivative / Cap", "MA-RFP"]
      const type = navigatePath.find(
        nav => nav.name === event.type || nav.label === event.type
      )
      let route = notIn.indexOf(event.type) === -1 ? "check-track" : "summary"
      if(route === "check-track" && event.cid) {
        // console.log("cid : ", event.cid)
        route = `${route}?cid=${event.cid}`
      }
      if (type) this.props.history.push(`${type.path}/${event.id}/${route}`)
    }
  }

  onSelectSlot = (slotInfo) => {
    let calendarStart = ""
    let calendarEnd = ""
    if(slotInfo && Array.isArray(slotInfo)){
      if(slotInfo.length){
        calendarStart = new Date(moment(slotInfo[0]).format("YYYY-MM-DD"))
        calendarEnd = new Date(moment(slotInfo[slotInfo.length - 1]).format("YYYY-MM-DD"))
      }
    }else if(slotInfo && typeof slotInfo === "object"){
      calendarStart = slotInfo.start || ""
      calendarEnd = slotInfo.end || ""
    }
    console.log({calendarStart, calendarEnd})
    this.setState({
      calendarStart,
      calendarEnd,
      activityTime: "",
      loading: true
    },() => this.pollDashboardData())
  }

  eventStyleGetter = (event, start, end, isSelected) => {
    const bgColor =
      event.filter === "My Tasks"
        ? "#77C2FF"
        : event.filter === "My Firm Tasks"
          ? "#FFC677"
          : event.filter === "Pick"
            ? "#BBE0FF"
            : "FFDFB3"
    const style = {
      backgroundColor: bgColor,
      color: "black",
      display: "block",
      borderColor: "#000"
    }
    return {
      style
    }
  }

  showOnlyTasksCheck = event => {
    this.setState({
      [event.target.name]: event.target.value
    },() => this.setEventState(this.state.transactions))
  }

  render() {
    const {
      events,
      activityCategory,
      transactionType,
      dropDown,
      showOnlyTasks,
      loading
    } = this.state
    return (
      <div>
        <Nav />
        <div id="main">
          {loading ? <Loader /> : null}
          <section>
            <div className="columns">
              <div className="column is-6">
                <div className="columns is-gapless">
                  <div className="column has-text-left is-narrow">
                    Filter By &nbsp; &nbsp; &nbsp;
                  </div>
                  <div className="column">
                    <div className="select is-fullwidth is-link is-small">
                      <select
                        name="activityCategory"
                        value={activityCategory || ""}
                        onChange={this.onFilter}
                      >
                        <option value="">All activities</option>
                        <option value="self">All my activities</option>
                        <option value="dealteam">All my deal activities</option>
                        <option value="myfirm">All my firm activities</option>
                      </select>
                    </div>
                  </div>
                  {activityCategory === "dealteam" ? (
                    <div className="column has-text-right is-narrow">
                      &nbsp; &nbsp; &nbsp;Type or Subtype &nbsp; &nbsp; &nbsp;
                    </div>
                  ) : null}
                  {activityCategory === "dealteam" ? (
                    <div className="column">
                      <SelectLabelInput
                        list={dropDown.transactionTypes || []}
                        name="transactionType"
                        value={transactionType || ""}
                        onChange={this.onTranTypeChange}
                      />
                    </div>
                  ) : null}
                </div>
              </div>
              <div className="column">&nbsp;</div>
              <SelectLabelInput
                className="column is-1"
                list={["Compliance Tasks", "Transactions Tasks", "All Tasks"]}
                name="showOnlyTasks"
                value={showOnlyTasks || ""}
                onChange={this.showOnlyTasksCheck}
              />
            </div>

            <div className="box" style={{ height: "100vh", overflowY: "auto" }}>
              <BigCalendar
                selectable
                events={events}
                views={allViews}
                step={60}
                defaultView="month"
                showMultiDayTimes={false}
                defaultDate={new Date()}
                onRangeChange={this.onSelectSlot}
                onSelectEvent={event => this.onEventClick(event)}
                onSelecting={range => console.log(range)}
                eventPropGetter={this.eventStyleGetter}
                components={{
                  event: this.event,
                  agenda: {
                    event: this.eventAgenda
                  }
                }}
              />
            </div>
          </section>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(
  mapStateToProps,
  null
)(EventsCalender)
