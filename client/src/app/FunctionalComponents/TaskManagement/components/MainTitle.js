import React from "react"

const MainTask = (props)=>(
  <div className="columns">
    <div className="column is-full">
      <div className="field is-grouped">
        <div className="control">
          <a 
            className={props.userTaskFollowing ? "button is-link is-small":"button is-dark is-small" } 
            href="#"
            onClick = {(event)=>{
              event.preventDefault()
              event.target.name="userTaskFollowing"
              props.onChangeTask(event)
            }}
          >{props.userTaskFollowing ? "Following":"Unfollow" }</a>
        </div>        
      </div>
    </div>
  </div>
)

export default MainTask