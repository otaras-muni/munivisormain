import React from "react"
import TaskDetails from "./TaskDetails"

const TaskMain = props => {
  const {loginDetails, nav2, nav3} = props
  // if(nav2 && nav3==="update"){
  //   return(
  //     <div>
  //       <UpdateTaskContainer taskId={nav2}/>
  //     </div>
  //   )
  // }
  return(
    <div>
      <TaskDetails taskId={nav2} mode={nav3} loginDetails={loginDetails} />
    </div>
  )}
export default TaskMain
