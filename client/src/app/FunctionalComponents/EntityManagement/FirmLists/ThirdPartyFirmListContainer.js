import React, { Component } from "react"
import { toast } from "react-toastify"
import ReactTable from "react-table"
import "react-table/react-table.css"
import { withRouter, Link } from "react-router-dom"
import Fuse from "fuse.js"
import { connect } from "react-redux"
import { getPicklistValues } from "GlobalUtils/helpers"
import { thirdPartyFirmList, saveMasterListSearchPrefs, deleteMasterListSearchPrefs, fetchMasterListSearchPrefs } from "AppState/actions/AdminManagement/admTrnActions"
import Loader from "../../../GlobalComponents/Loader"
import "./../scss/entity.scss"
import PageFilter from "./../CommonComponents/PageFilter"


const searchOptions = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    {
      name: "firmType",
      weight: 0.8
    },
    {
      name: "firmName",
      weight: 0.9,
    },
    {
      name: "state",
      weight: 0.89,
    },
    {
      name: "city",
      weight: 0.98
    },
    {
      name: "addressName",
      weight: 0.86
    },
    {
      name: "primaryContact",
      weight: 0.6,
    }
  ]
}
const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"), "i")
  return string.replace(reg, str => (
    `<mark>${str}</mark>`
  ))
}

export const Modal = ({ children, closeModal, modalState, title }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div className="modal-background" onClick={closeModal} role="presentation" onKeyPress={() => { }} />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{title}</p>
          <button className="delete" onClick={closeModal} />
        </header>
        <section className="modal-card-body">
          <div className="content">
            {children}
          </div>
        </section>
      </div>
    </div>
  )
}
class ThirdPartyFirmListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: {
        pagefilter: false
      },
      thirdPartyList: [],
      modalState: false,
      marketRoles: "",
      loading: false,
      pageSizeOptions: [5, 10, 20, 25, 50],
      pageSize: 10,
      countryResult: [],
      filteredValue: {
        entityType: "",
        enityState: [],
        entitySearch: ""
      },
      search: "",
      searchname: "",
      entityId: "",
      isSaving: false,
      savedSearches: "",
      savedSearchesList: [],
      savedSearchesData: [],
      marketRoleList: []
    }
    this.toggleModal = this.toggleModal.bind(this)
  }
  async componentWillMount() {

  }
  async componentDidMount() {
    const { userEntities } = this.props.auth
    this.setState({
      loading: true
    })
    const result = await thirdPartyFirmList(userEntities.entityId)
    const [countryResult, marketRole] = await getPicklistValues(["LKUPCOUNTRY", "LKUPPARTICIPANTTYPE"])
    const prefResult = await fetchMasterListSearchPrefs("mast-thirdparty")
    this.setState({
      thirdPartyList: result,
      countryResult,
      loading: false,
      entityId: userEntities.entityId,
      marketRoleList: ["Select Market Role", ...marketRole[1]],
      savedSearchesList: [{ label: "Saved Searches", value: "" }, ...prefResult.data.length > 0 ? prefResult.data.map(item => ({ label: item.searchName, value: item._id })) : []],
      savedSearchesData: prefResult.data.length > 0 ? prefResult.data : []
    })
  }
  /** *************************************************** */
  toggleModal(marketRoles) {
    this.setState((prev) => {
      const newState = !prev.modalState

      return { modalState: newState, marketRoles }
    })
  }
  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }
  onPageSizeChange = (item) => {
    let { pageSize } = this.state
    if (item === "")
      pageSize = 10
    else
      pageSize = item
    this.setState({
      pageSize
    })
  }
  onChangeFilter = async (event) => {
    const { name, value } = event.target
    const { filteredValue, entityId } = this.state
    filteredValue[name] = value
    if (name === "entityType" && value === "Select Market Role")
      filteredValue[name] = ""
    const result = await thirdPartyFirmList(entityId, filteredValue)
    this.setState({
      filteredValue,
      thirdPartyList: result,
      search: name === "entitySearch" ? value : ""
    })
  }
  onChangeFilterPage = async (event) => {
    const { name, value } = event.target
    if (name === "searchname" || name === "savedSearches") {
      this.setState({
        [name]: value
      })
    } else {
      const { filteredValue } = this.state
      filteredValue[name] = value
      this.setState({
        filteredValue
      })
    }
    if (name === "savedSearches")
      this.handleGetPref(value)
  }

  handleSavePref = async () => {
    const { filteredValue, searchname, savedSearchesList, savedSearches } = this.state
    let { savedSearchesData } = this.state
    if (searchname !== "") {
      this.setState({
        isSaving: true
      })
      const savePrefResult = await saveMasterListSearchPrefs("mast-thirdparty", searchname, filteredValue)
      if (savePrefResult.status === 200) {
        if (!savePrefResult.data.data.nModified) {
          savedSearchesList.push({ label: searchname, value: savePrefResult.data.data.upserted[0]._id })
          savedSearchesData.push({ _id: savePrefResult.data.data.upserted[0]._id, searchName: searchname, searchPreference: JSON.stringify(filteredValue) })
          toast("Preference saved successfully!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        } else
          toast("Preference updated successfully!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        savedSearchesData = savedSearchesData.map(item => {
          if (item._id === savedSearches.value) {
            item.searchPreference = JSON.stringify(filteredValue)
          }
          return item
        })
      } else
        toast("Something went wrong!", { autoClose: 2000, type: toast.TYPE.ERROR, })``
      this.setState({
        isSaving: false
      })
    }
  }
  handleGetPref = async (savedSearches) => {
    const { savedSearchesData, entityId } = this.state
    let { filteredValue } = this.state
    if (savedSearches !== "" && savedSearches.value !== "") {
      const filteredData = savedSearchesData.find(item => item._id === savedSearches.value)
      const { searchPreference } = filteredData
      filteredValue = JSON.parse(searchPreference)
    } else {
      filteredValue = {
        entityType: "",
        enityState: [],
        entitySearch: "",
        searchname: ""
      }
    }
    const result = await thirdPartyFirmList(entityId, filteredValue)
    this.setState({
      filteredValue,
      thirdPartyList: result,
      loading: false,
      searchname: savedSearches.value !== "" ? savedSearches.label : "",
    })
  }
  handleDeletePref = async () => {
    const { savedSearches, entityId } = this.state
    let { savedSearchesList, savedSearchesData } = this.state
    if (savedSearches !== "" && savedSearches.value !== "") {
      const result = await deleteMasterListSearchPrefs(savedSearches.value)
      if (result.status === 200) {
        savedSearchesList = savedSearchesList.filter(item => item.value !== savedSearches.value)
        savedSearchesData = savedSearchesData.filter(item => item.__id !== savedSearches.value)
        const result = await thirdPartyFirmList(entityId, { entityType: "", enityState: [], entitySearch: "" })
        this.setState({
          savedSearchesList,
          savedSearches: { label: "Saved Searches", value: "" },
          savedSearchesData,
          filteredValue: {
            entityType: "",
            enityState: [],
            entitySearch: ""
          },
          searchname: "",
          thirdPartyList: result
        })
        toast("Successfully Deleted", { autoClose: 2000, type: toast.TYPE.SUCCESS })
      } else
        toast("Something went wrong!", { autoClose: 2000, type: toast.TYPE.ERROR, })
    }
  }
  fuse = (e) => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    const res = fuse.search(search)
    return res
  }
  render() {
    const loading = () => <Loader />
    if (this.state.loading) {
      return loading()
    }
    const { pageSize, thirdPartyList, search } = this.state
    const searchOn = search.length > 0
    let output = []
    if (searchOn) {
      output = this.fuse(thirdPartyList)
    } else {
      output = thirdPartyList
    }
    return (
      <div id="main" className="firms">
        <p className="innerPgTitle">
          <b>3rd Party - Master List</b>
        </p>
        <PageFilter
          {...this.state}
          toggleButton={this.toggleButton}
          onPageSizeChange={this.onPageSizeChange}
          onChangeFilter={this.onChangeFilter}
          onChangeFilterPage={this.onChangeFilterPage}
          handleSavePref={this.handleSavePref}
          handleGetPref={this.handleGetPref}
          handleDeletePref={this.handleDeletePref}
          listType="third-party"
        />
        <div className="columns">
          <div className="column is-narrow">&nbsp;</div>
          {thirdPartyList.length > 0 && <div className="column">
            <div className="top-bottom-margin box">
              <ReactTable
                columns={[
                  {
                    Header: "Type",
                    id: "firmType",
                    className: "multiExpTblVal",
                    accessor: item => item,
                    maxWidth: 250,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd -striped">
                          {
                            item.firmType.length > 2
                              ? <small>{
                                item.firmType.slice(1, 3).join()
                              }
                                <a
                                  onClick={(event) => {
                                    event.preventDefault()
                                    this.toggleModal(item.firmType.join())
                                  }}
                                >  More Roles...</a>
                              </small>
                              : <small>{item.firmType.join()}</small>
                          }</div>
                      )
                    },
                    // sortMethod: (a, b) => a.firmType.localeCompare(b.firmType)
                  },
                  {
                    id: "firmName",
                    Header: "Entity Name",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd">
                          {
                            this.props.userType === "admin"
                              ? <Link to={`/admin-thirdparty/${item.firmId}/entity`} dangerouslySetInnerHTML={{ __html: highlightSearch(item.firmName || "-", search) }} />
                              : <Link to={`/thirdparties/${item.firmId}/entity`} dangerouslySetInnerHTML={{ __html: highlightSearch(item.firmName || "-", search) }} />
                          }
                        </div>
                      )
                    },
                    maxWidth: 250,
                    sortMethod: (a, b) => a.firmName.localeCompare(b.firmName)
                  },
                  {
                    Header: "City",
                    className: "multiExpTblVal",
                    id: "city",
                    maxWidth: 150,
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.city || "-", search) }} />
                      )
                    },
                    sortMethod: (a, b) => a.city.localeCompare(b.city)
                  },
                  {
                    Header: "State",
                    className: "multiExpTblVal",
                    id: "state",
                    maxWidth: 150,
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.state || "-", search) }} />
                      )
                    },
                    sortMethod: (a, b) => a.state.localeCompare(b.state)
                  },
                  {
                    Header: "Primary Contact Name",
                    className: "multiExpTblVal",
                    id: "addressName",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.addressName || "-", search) }} />
                      )
                    },
                    maxWidth: 200,
                    sortMethod: (a, b) => a.addressName.localeCompare(b.addressName)
                  },
                  {
                    Header: "Primary Contact Phone",
                    className: "multiExpTblVal",
                    id: "primaryContact",
                    accessor: item => item,
                    Cell: row => {
                      const item = row.value
                      return (
                        <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.primaryContact || "-", search) }} />
                      )
                    },
                    sortMethod: (a, b) => a.primaryContact.localeCompare(b.primaryContact)
                  }
                ]}
                data={output}
                showPaginationBottom
                defaultPageSize={pageSize}
                pageSizeOptions={[5, 10, 20, 25, 50, 100]}
                className="-striped -highlight is-bordered"
                pageSize={pageSize}
                // showPageSizeOptions={false}
                showPageJump={false}
                // PaginationComponent={Pagination}
                minRows={2}
              />
              <Modal
                closeModal={this.toggleModal}
                modalState={this.state.modalState}
                title="Market Roles"
              >
                <p>{this.state.marketRoles}</p>
              </Modal></div>
          </div>}
          <div className="column is-narrow">&nbsp;</div>
        </div>

      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ThirdPartyFirmListContainer))
