import React, { Component } from "react"
import { toast } from "react-toastify"
import Switch, { Case, Default } from "react-switch-case"
import { entityUsersList, getFirmById } from "AppState/actions/AdminManagement/admTrnActions"
import { withRouter, Link } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "../../../GlobalComponents/Loader"
import "./../scss/entity.scss"
import UsersList from "./components/UsersList"
import UpdateContactContainer from "./../contacts/containers/UpdateContactContainer"
import AddContactContainer from "./../contacts/containers/AddContactContainer"
import { checkPlatformAdmin, checkNavAccessEntitlrment } from "GlobalUtils/helpers"

class ClientContactListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firmName: "",
      filterUsersList: [],
      searchString: "",
      userList: [],
      viewList: "",
      userId: "",
      entityId: "",
      search: "",
      pageSize: 10,
      canEdit: false,
      canView: false
    }
  }
  async componentWillMount() {
    let isPlatformAdmin = checkPlatformAdmin()
    this.setState({
      loading: true
    })
    const { entityId } = this.props.auth.userEntities
    const action = await checkNavAccessEntitlrment(this.props.match.params.nav2)
    const { edit, view } = action || {}

    let { canEdit, canView } = this.state
    isPlatformAdmin = await isPlatformAdmin
    if (isPlatformAdmin) {
      canEdit = true
    } else {
      canEdit = edit
      canView = view
    }
    this.setState({
      loading: true,
      canEdit,
      entityId,
      canView
    })
  }
  async componentDidMount() {
    const { entityId } = this.props.auth.userEntities
    let result = entityUsersList(entityId)
    let firmDetailById = getFirmById(this.props.nav2)
    if (this.props.nav2 !== "new" && this.props.nav2.length === 24) {
      this.setState({
        loading: true
      })
      const userList = []
      result = await result
      if (result && result.length > 0) {
        result.forEach(item => {
          if (item.entityId === this.props.nav2) {
            userList.push(item)
          }
        })
      }

      firmDetailById = await firmDetailById
      if (firmDetailById !== null && !firmDetailById.error) {
        this.setState({
          firmName: firmDetailById.firmName,
          userList,
          filterUsersList: userList,
          loading: false
        })
      } else {
        toast("Entity Not Found!", { autoClose: 2000, type: toast.TYPE.ERROR })
      }
    }
  }
  getUsers = (userId, entityId) => {
    this.setState({
      viewList: "viewUsers",
      userId,
      entityId
    })
  }
  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }

  changeSearchString = (e) => {
    const searchString = e.target.value
    if (searchString === "") {
      this.setState(prevState => {
        const { userList } = prevState
        return { filterUsersList: userList, searchString }
      })
    } else {
      this.setState({ searchString })
      this.performSearch()
    }
  }
  performSearch = () => {
    this.setState(prevState => {
      const searchUsers = this.searchUsers(prevState.searchString, prevState.filterUsersList, prevState.userList)
      return { filterUsersList: searchUsers }
    })
  }
  searchUsers = (searchString, filterUsersList, userList) => {
    searchString = searchString.trim().toLowerCase()
    const usersListRes = []
    userList.forEach(item => {

      if ((
        item.firstName.toLowerCase().includes(searchString)
        || item.middleName.toLowerCase().includes(searchString)
        || item.lastName.toLowerCase().includes(searchString)
        || (typeof item.primaryEmail !== "undefined" && item.primaryEmail !== "") && item.primaryEmail.toLowerCase().includes(searchString)
      )) {
        usersListRes.push(item)
      }
    })
    return usersListRes
  }

  handleKeyPressinSearch = (e) => {
    if (e.key === "Enter") {
      this.performSearch()
    }
  }

  addNewUsers = () => {
    this.setState({
      viewList: "addNewUsers"
    })
  }
  onSearchUsers = (item) => {
    this.setState({
      viewList: "viewUsers",
      userId: item.value
    })
  }
  onUpdateContact = (event, userId) => {
    this.setState({
      viewList: "viewUsers",
      userId
    })
  }
  render() {
    const loading = () => <Loader />
    const { searchString, filterUsersList, pageSize, canEdit, canView } = this.state
    return (
      <div>
        {this.state.loading ? loading() : null}
        <Switch condition={this.state.viewList}>
          <Case value="viewUsers">
            <UpdateContactContainer
              userId={this.state.userId}
              entityId={this.state.entityId}
              canEdit={canEdit}
              canView={canView}
            />
          </Case>
          <Case value="addNewUsers">
            <AddContactContainer
              entityId={this.props.nav2}
              entityName={this.state.firmName}
              onSearchUsers={this.onSearchUsers}
            />
          </Case>
          <Default>
            <div className="columns">
              <div className="column is-two-thirds">
                <p className="control has-icons-left">
                  <input className="input is-small is-link"
                    type="text"
                    placeholder="search users"
                    value={searchString}
                    onChange={this.changeSearchString}
                    onKeyPress={this.handleKeyPressinSearch}
                  />
                  <span className="icon is-left has-background-dark">
                    <i className="fas fa-search" />
                  </span>
                </p>
              </div>
              <div className="column">
                <Link to="#" onClick={(event) => {
                  event.preventDefault()
                  this.addNewUsers()
                }}>Add new contact</Link>
              </div>
            </div>
            <UsersList
              userslist={filterUsersList}
              getUsers={this.getUsers}
              pageSizeOptions={[5, 10, 20, 25, 50, 100]}
              pageSize={pageSize}
              search={searchString}
              updateContact={this.onUpdateContact}

            />
          </Default>
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientContactListContainer))
