import React, { Component } from "react"
import ReactTable from "react-table"
import { toast } from "react-toastify"
import "react-table/react-table.css"
import Fuse from "fuse.js"
import { clientProspectFirmList, saveMasterListSearchPrefs, deleteMasterListSearchPrefs, fetchMasterListSearchPrefs } from "AppState/actions/AdminManagement/admTrnActions"
import { withRouter, Link } from "react-router-dom"
import { getPicklistValues } from "GlobalUtils/helpers"
import { connect } from "react-redux"
import Loader from "./../../../GlobalComponents/Loader"
import PageFilter from "./../CommonComponents/PageFilter"
import "./../scss/entity.scss"

const searchOptions = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    {
      name: "firmType",
      weight: 0.8
    },
    {
      name: "firmName",
      weight: 0.9,
    },
    {
      name: "state",
      weight: 0.89,
    },
    {
      name: "city",
      weight: 0.98
    },
    {
      name: "addressName",
      weight: 0.86
    },
    {
      name: "primaryContact",
      weight: 0.6,
    }
  ]
}
const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"), "i")
  return string.replace(reg, str => (
    `<mark>${str}</mark>`
  ))
}

class ClientFirmListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: {
        pagefilter: false
      },
      loading: false,
      clientList: [],
      pageSizeOptions: [5, 10, 20, 25, 50],
      pageSize: 10,
      countryResult: [],
      filteredValue: {
        entityType: "",
        enityState: [],
        entitySearch: ""
      },
      search: "",
      searchname: "",
      entityId: "",
      isSaving: false,
      savedSearches: "",
      savedSearchesList: [],
      savedSearchesData: []
    }
  }
  async componentWillMount() {
    this.setState({
      loading: true
    })
  }
  async componentDidMount() {
    const { userEntities } = this.props.auth
    let result = clientProspectFirmList(userEntities.entityId)
    let picArray = getPicklistValues(["LKUPCOUNTRY"])
    const prefResult = await fetchMasterListSearchPrefs("mast-cltprosp")
    result = await result
    const [countryResult] = await picArray
    this.setState({
      clientList: result,
      savedSearchesList: [{ label: "Saved Searches", value: "" }, ...prefResult.data.length > 0 ? prefResult.data.map(item => ({ label: item.searchName, value: item._id })) : []],
      countryResult,
      loading: false,
      entityId: userEntities.entityId,
      savedSearchesData: prefResult.data.length > 0 ? prefResult.data : []
    })
  }
  /** ******************************************************************************** */

  onChangeFilterPage = async (event) => {
    const { name, value } = event.target
    const { filteredValue } = this.state
    if (name === "searchname" || name === "savedSearches") {
      this.setState({
        [name]: value
      })
    } else {
      filteredValue[name] = value
      this.setState({
        filteredValue
      })
    }
    if (name === "savedSearches") {
      this.handleGetPref(value)
    }
  }
  onPageSizeChange = (item) => {
    this.setState({
      pageSize: item
    })
  }
  onChangeFilter = async (event) => {

    const { name, value } = event.target
    const { filteredValue, entityId } = this.state
    filteredValue[name] = value
    const result = await clientProspectFirmList(entityId, filteredValue)
    this.setState({
      filteredValue,
      clientList: result,
      search: name === "entitySearch" ? value : ""
    })
  }
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }

  fuse = (e) => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    const res = fuse.search(search)
    return res
  }

  handleSavePref = async () => {
    const { filteredValue, searchname, savedSearchesList, savedSearches } = this.state
    let { savedSearchesData } = this.state
    if (searchname !== "") {
      this.setState({
        isSaving: true
      })
      const savePrefResult = await saveMasterListSearchPrefs("mast-cltprosp", searchname, filteredValue)
      if (savePrefResult.status === 200) {
        if (!savePrefResult.data.data.nModified) {
          savedSearchesList.push({ label: searchname, value: savePrefResult.data.data.upserted[0]._id })
          savedSearchesData.push({ _id: savePrefResult.data.data.upserted[0]._id, searchName: searchname, searchPreference: JSON.stringify(filteredValue) })
          toast("Preference saved successfully!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        } else
          toast("Preference updated successfully!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        savedSearchesData = savedSearchesData.map(item => {
          if (item._id === savedSearches.value) {
            item.searchPreference = JSON.stringify(filteredValue)
          }
          return item
        })
      } else
        toast("Something went wrong!", { autoClose: 2000, type: toast.TYPE.ERROR, })``
      this.setState({
        isSaving: false
      })
    }
  }
  handleGetPref = async (savedSearches) => {

    const { savedSearchesData, entityId } = this.state
    let { filteredValue } = this.state
    if (savedSearches !== "" && savedSearches.value !== "") {
      const filteredData = savedSearchesData.find(item => item._id === savedSearches.value)
      const { searchPreference } = filteredData
      filteredValue = JSON.parse(searchPreference)
    } else {
      filteredValue = {
        entityType: "",
        enityState: [],
        entitySearch: "",
        searchname: ""
      }
    }
    const result = await clientProspectFirmList(entityId, filteredValue)
    this.setState({
      filteredValue,
      clientList: result,
      searchname: savedSearches.value !== "" ? savedSearches.label : "",
      loading: false
    })
  }
  handleDeletePref = async () => {
    const { savedSearches, entityId } = this.state
    let { savedSearchesList, savedSearchesData } = this.state
    if (savedSearches !== "" && savedSearches.value !== "") {
      const result = await deleteMasterListSearchPrefs(savedSearches.value)
      if (result.status === 200) {
        savedSearchesList = savedSearchesList.filter(item => item.value !== savedSearches.value)
        savedSearchesData = savedSearchesData.filter(item => item.__id !== savedSearches.value)
        const result = await clientProspectFirmList(entityId, { entityType: "", enityState: [], entitySearch: "" })
        this.setState({
          savedSearchesList,
          savedSearches: { label: "Saved Searches", value: "" },
          savedSearchesData,
          filteredValue: {
            entityType: "",
            enityState: [],
            entitySearch: ""
          },
          searchname: "",
          clientList: result
        })
        toast("Successfully Deleted", { autoClose: 2000, type: toast.TYPE.SUCCESS })
      } else
        toast("Something went wrong!", { autoClose: 2000, type: toast.TYPE.ERROR, })
    }
  }
  render() {
    const loading = () => <Loader />
    const { pageSize, clientList, search } = this.state
    return (
      <div id="main" className="firms">
        {this.state.loading ? loading() : null}
        <p className="innerPgTitle">
          <b>Client/Prospect - Master List</b>
        </p>
        <PageFilter
          {...this.state}
          toggleButton={this.toggleButton}
          onPageSizeChange={this.onPageSizeChange}
          onChangeFilter={this.onChangeFilter}
          onChangeFilterPage={this.onChangeFilterPage}
          handleSavePref={this.handleSavePref}
          handleGetPref={this.handleGetPref}
          handleDeletePref={this.handleDeletePref}
          listType="client-prospect"
        />
        {clientList.length > 0 && <div className="column box">
          <ReactTable
            columns={[
              {
                Header: "Type",
                id: "firmType",
                className: "multiExpTblVal",
                accessor: item => item,
                maxWidth: 100,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.firmType || "-", search) }} />
                  )
                },
                sortMethod: (a, b) => a.firmType.localeCompare(b.firmType)
              },
              {
                id: "firmName",
                Header: "Entity Name",
                accessor: item => item,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd" >
                      {
                        this.props.userType === "admin"
                          ? <Link to={`/admin-cltprosp/${item.firmId}/entity`} dangerouslySetInnerHTML={{ __html: highlightSearch(item.firmName || "-", search) }} />
                          : <Link to={`/clients-propects/${item.firmId}/entity`} dangerouslySetInnerHTML={{ __html: highlightSearch(item.firmName || "-", search) }} />
                      }
                    </div>
                  )
                },
                maxWidth: 250,
                sortMethod: (a, b) => a.firmName.localeCompare(b.firmName)
              },
              {
                Header: "City",
                className: "multiExpTblVal",
                id: "city",
                maxWidth: 200,
                accessor: item => item,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.city || "-", search) }} />
                  )
                },
                sortMethod: (a, b) => a.city.localeCompare(b.city)
              },
              {
                Header: "State",
                className: "multiExpTblVal",
                id: "state",
                maxWidth: 200,
                accessor: item => item,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.state || "-", search) }} />
                  )
                },
                sortMethod: (a, b) => a.state.localeCompare(b.state)
              },
              {
                Header: "Primary Contact Name",
                className: "multiExpTblVal",
                id: "addressName",
                accessor: item => item,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.addressName || "-", search) }} />
                  )
                },
                maxWidth: 200,
                sortMethod: (a, b) => a.addressName.localeCompare(b.addressName)
              },
              {
                Header: "Primary Contact Phone",
                className: "multiExpTblVal",
                id: "primaryContact",
                accessor: item => item,
                Cell: row => {
                  const item = row.value
                  return (
                    <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.primaryContact || "-", search) }} />
                  )
                },
                maxWidth: 200,
                sortMethod: (a, b) => a.primaryContact.localeCompare(b.primaryContact)
              }
            ]}
            data={clientList}
            showPaginationBottom
            defaultPageSize={pageSize}
            pageSizeOptions={[5, 10, 20, 25, 50, 100]}
            className="-striped -highlight is-bordered"
            pageSize={pageSize}
            // showPageSizeOptions={false}
            showPageJump={false}
            loading={this.state.loading}
            // PaginationComponent={Pagination}
            minRows={2}
          />
        </div>}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { auth } = state
  return ({ auth })
}

const mapDispatchToProps = {}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientFirmListContainer))
