import { validateThirdPartyDetail } from "Validation/thirdParty"
import {
  getAllThirdPartyFirmList,
  getThirdPartyFirmDetailById,
  updateThirdPartyFirm,
  searchFirms,
  checkDuplicateEntity
} from "AppState/actions/AdminManagement/admTrnActions"
import React from "react"
import { toast } from "react-toastify"
import { withRouter } from "react-router-dom"
import cloneDeep from "clone-deep"
import { connect } from "react-redux"
import {
  getPicklistValues,
  convertError,
  checkEmptyElObject,
  checkPlatformAdmin,
  checkNavAccessEntitlrment
} from "GlobalUtils/helpers"
import FirmAddressListForm from "./../../CommonComponents/FirmAddressListForm"
import FirmProfileForm from "./../components/FirmProfileForm"
import EntityType from "./../components/EntityType"
import Loader from "../../../../GlobalComponents/Loader"
import "./../../scss/entity.scss"

let errorData = {
  error: null
}
const dateCompare = (startDate, endDate) => {
  if (startDate > endDate) return 1
  else if (startDate < endDate) return -1
  return 0
}
const initialAddressError = (errorFirmDetail, address, initialFirmDetails) => {
  const arr = ["officeEmails", "officePhone", "officeFax"]
  arr.map(item => {
    if (address[item].length > 0) {
      for (let index = 1; index < address[item].length; index++) {
        errorFirmDetail.addresses[0][item].push(
          initialFirmDetails.addresses[0][item][0]
        )
      }
    }
  })
}

class UpdateThirdPartyContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.loading = false
    this.state.errorFirmDetail = Object.assign(
      {},
      this.initialState().firmDetails
    )
    this.cleanErrorFirmDetail = Object.assign(
      {},
      this.initialState().firmDetails
    )
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
    this.routeNavigate = this.routeNavigate.bind(this)
  }

  async componentWillMount() {
    const action = await checkNavAccessEntitlrment(this.props.match.params.nav2)
    const { edit, view } = action || {}
    let { canEdit, canView } = this.state
    const isPlatformAdmin = await checkPlatformAdmin()
    if (isPlatformAdmin) {
      canEdit = true
    } else {
      canEdit = edit
      canView = view
    }
    this.setState({
      loading: true,
      canEdit,
      canView
    })
    if (!canEdit && !canView) {
      this.props.history.push("/mast-thirdparty")
    }
    if (
      this.props.match.params.nav2 !== "new" &&
      this.props.match.params.nav2.length === 24
    ) {
      await this.props.getThirdPartyFirmDetailById(this.props.match.params.nav2)
    }
  }
  async componentDidMount() {
    this.setState({
      loadingPickLists: true
    })
    console.log("Did Mount")
    const { firmDetails } = this.state
    firmDetails.addresses[0].isPrimary = true
    /* firmDetails.addresses[0].isActive = true */
    firmDetails.addresses[0].isHeadQuarter = true
    const [
      msrbFirmList,
      countryResult,
      msrbRegTypeResult,
      primarySectors,
      marketRoleList
    ] = await getPicklistValues([
      "LKUPMSRBFIRMS",
      "LKUPCOUNTRY",
      "LKUPREGISTRANTTYPE",
      "LKUPPRIMARYSECTOR",
      "LKUPPARTICIPANTTYPE"
    ])
    const msrbFirmListResult = [
      { label: "Select MSRB Firm", value: "", registrantType: "" }
    ]
    msrbRegTypeResult[2]["Third Party"].forEach(item => {
      Object.keys(msrbFirmList[3][item]).forEach(firm => {
        msrbFirmListResult.push({
          label: firm,
          value: msrbFirmList[3][item][firm][0],
          registrantType: item
        })
      })
    })
    this.setState(prevState => ({
      ...prevState,
      ...{
        countryResult,
        msrbRegTypeResult,
        primarySectorsList: primarySectors,
        marketRoleList,
        msrbFirmListResult,
        loading: false,
        entityType: "Third Party",
        loadingPickLists: false
      }
    }))
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.admThirdParty.firmDetailById &&
      Object.keys(nextProps.admThirdParty.firmDetailById).length > 0 &&
      this.props.match.params.nav2 !== "new" &&
      !nextProps.admThirdParty.updated
    ) {
      let { errorFirmDetail, oldFirmDetails } = this.state
      oldFirmDetails = cloneDeep(nextProps.admThirdParty.firmDetailById)
      const firmDetails = cloneDeep(nextProps.admThirdParty.firmDetailById)
      const addressList = cloneDeep(
        nextProps.admThirdParty.firmDetailById.addresses
      )
      firmDetails.addresses = this.initialState().firmDetails.addresses
      errorFirmDetail = cloneDeep(this.initialState().firmDetails)
      initialAddressError(
        errorFirmDetail,
        firmDetails.addresses[0],
        this.initialState().firmDetails
      )
      const oldErrorFirmDetail = cloneDeep(errorFirmDetail)
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        errorFirmDetail,
        readOnlyFeatures: {
          msrbFirmNameReadOnly: true,
          msrbIdReadOnly: true,
          firmName: true,
          registrantTypeReadOnly: true
        },
        loading: false,
        oldFirmDetails,
        oldErrorFirmDetail
      }))
    }
  }

  /** ********************************************** onChangeAliases *********************** */
  onChangeAliases = (e, key) => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails.entityAliases[key] = e.target.value
    const validator = {
      entityAliases: [e.target.value]
    }
    this.setState({
      firmDetails
    })
    if (this.submitted) {
      errorData = validateThirdPartyDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name) {
          errorFirmDetail[e.target.name][key] = `${err.context.label} Required.`
          this.setState({
            errorFirmDetail
          })
        } else {
          errorFirmDetail[e.target.name][key] = ""
          this.setState({
            errorFirmDetail
          })
        }
      } else {
        errorFirmDetail[e.target.name][key] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }

  /** ************************************************* */
  onChangeAddOns = (e, idx) => {
    const { firmAddOns } = this.state.firmDetails.firmAddOns
    firmAddOns[idx].serviceEnabled = !firmAddOns[idx].serviceEnabled
    this.setState({
      firmAddOns
    })
  }

  /** ************************ this is for onchange ********************************* */

  onChangeEntity = (e, key) => {
    let { firmDetails } = this.state
    if (key === "borrower") {
      firmDetails = {
        ...firmDetails,
        firmName: e
      }
      this.setState({
        firmDetails
      })
    }
  }

  onChangeFirmDetail = (e, key, depFields, type = "addresses") => {
    const { firmDetails, errorFirmDetail } = this.state
    let keyFirmDetail, errorKeyFirmDetail
    let validator
    if (typeof key !== "undefined") {
      const value =
        e.target.type === "checkbox"
          ? !firmDetails.addresses[key][e.target.name]
          : e.target.value
      if (e.target.name == "zip1" || e.target.name == "zip2") {
        keyFirmDetail = firmDetails.addresses[key].zipCode
        errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
        validator = {
          addresses: [
            {
              zipCode: { [e.target.name]: e.target.value }
            }
          ]
        }
      } else {
        keyFirmDetail = firmDetails[type][key]
        errorKeyFirmDetail = errorFirmDetail[type][key]
        validator = {
          [type]: [{ [e.target.name]: e.target.value }]
        }
      }
      keyFirmDetail[e.target.name] = value
      if (depFields) {
        depFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    } else {
      if (e.target.name === "primarySectors") firmDetails.secondarySectors = ""
      if (e.target.name === "marketRole") {
        this.toggleCheckbox(e.target.value)
        validator = {
          entityFlags: {
            [e.target.name]: firmDetails.entityFlags[e.target.name]
          }
        }
        errorKeyFirmDetail = errorFirmDetail.entityFlags
      } else {
        firmDetails[e.target.name] = e.target.value
        validator = { [e.target.name]: e.target.value }
        errorKeyFirmDetail = errorFirmDetail
      }

      if (depFields) {
        depFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    }
    if (this.submitted) {
      if (
        e.target.name === "borOblStartDate" ||
        e.target.name === "borOblEndDate"
      ) {
        if (
          firmDetails[type][key].borOblStartDate !== "" &&
          firmDetails[type][key].borOblEndDate !== ""
        ) {
          const dateVal = dateCompare(
            firmDetails[type][key].borOblStartDate,
            firmDetails[type][key].borOblEndDate
          )
          switch (dateVal) {
            case 1:
              errorKeyFirmDetail[e.target.name] =
                e.target.name === "borOblStartDate"
                  ? "Start Date is should be less Exit Date."
                  : "End Date is should be Greate Thant Start Date."
              break
            default:
              errorKeyFirmDetail[e.target.name] = ""
              break
          }
        } else if (firmDetails[type][key][e.target.name] === "") {
          const dateVal =
            e.target.name === "borOblStartDate" ? "Start Date" : "End Date"
          errorKeyFirmDetail[e.target.name] = `${dateVal} Required`
        } else {
          errorKeyFirmDetail[e.target.name] = ""
        }
        this.setState({
          errorKeyFirmDetail
        })
        return
      }
      errorData = validateThirdPartyDetail(validator)

      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (
          err.path[0] === e.target.name ||
          err.context.key === e.target.name
        ) {
          errorKeyFirmDetail[e.target.name] = `${err.context.label} Required.`
          this.setState({
            errorFirmDetail
          })
        } else {
          errorKeyFirmDetail[e.target.name] = ""
          this.setState({
            errorFirmDetail
          })
        }
      } else {
        errorKeyFirmDetail[e.target.name] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }

  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, key, idx) => {
    e.preventDefault()
    const addresses = this.state.firmDetails.addresses[key]
    const { errorFirmDetail } = this.state
    addresses[type][idx][e.target.name] = e.target.value
    const validator = {
      addresses: [
        {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      ]
    }
    this.setState({
      addresses
    })
    if (this.submitted) {
      errorData = validateThirdPartyDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorFirmDetail.addresses[key][type][idx][e.target.name] =
          "Required" || err.message
        this.setState({
          errorFirmDetail
        })
      } else {
        errorFirmDetail.addresses[key][type][idx][e.target.name] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }

  getFirmDetails = item => {
    const { firmDetails, readOnlyFeatures, errorFirmDetail } = this.state
    if (item.value !== "") {
      firmDetails.firmName = item.label
      firmDetails.msrbId = item.value
      firmDetails.msrbFirmName = item.label
      readOnlyFeatures.msrbFirmNameReadOnly = true
      readOnlyFeatures.msrbIdReadOnly = true
      firmDetails.msrbRegistrantType = item.registrantType
      errorFirmDetail.firmName = ""
      errorFirmDetail.msrbId = ""
    } else {
      firmDetails.firmName = ""
      firmDetails.msrbId = ""
      firmDetails.msrbFirmName = ""
      readOnlyFeatures.msrbFirmNameReadOnly = false
      readOnlyFeatures.msrbIdReadOnly = false
      firmDetails.msrbRegistrantType = ""
      if (this.submitted) {
        errorFirmDetail.firmName = "FirmName Required."
        errorFirmDetail.msrbId = "MSRB Id Required."
        errorFirmDetail.msrbRegistrantType = "MSRB Registrant Type Required"
      } else {
        errorFirmDetail.firmName = ""
        errorFirmDetail.msrbId = ""
        errorFirmDetail.msrbRegistrantType = ""
      }
    }
    this.setState({
      firmDetails,
      readOnlyFeatures
    })
  }

  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const { firmDetails } = this.state
    const { marketRole } = firmDetails.entityFlags
    if (marketRole.includes(label)) {
      marketRole.splice(marketRole.indexOf(label), 1)
    } else {
      marketRole.push(label)
    }
    this.setState(prevState => ({
      marketRole: {
        ...prevState.firmDetails.entityFlags,
        marketRole
      }
    }))
  }

  /** **************************************this is for initial default value********************* */
  initialState = () => ({
    firmDetails: {
      entityFlags: {
        marketRole: []
      },
      entityAliases: [],
      isMuniVisorClient: false,
      msrbFirmName: "",
      msrbRegistrantType: "",
      msrbId: "",
      firmName: "",
      taxId: "",
      primarySectors: "",
      secondarySectors: "",
      addresses: [
        {
          addressName: "",
          isPrimary: false,
          isHeadQuarter: false,
          /* isActive: true, */
          website: "",
          officePhone: [
            {
              countryCode: "",
              phoneNumber: "",
              extension: ""
            }
          ],
          officeFax: [
            {
              faxNumber: ""
            }
          ],
          officeEmails: [
            {
              emailId: ""
            }
          ],
          addressLine1: "",
          addressLine2: "",
          country: "",
          state: "",
          city: "",
          zipCode: {
            zip1: "",
            zip2: ""
          },
          formatted_address: "",
          url: "",
          location: {
            longitude: "",
            latitude: ""
          }
        }
      ]
    },
    addressList: [],
    LinkCusipList: [],
    LinkBorrowerList: [],
    firmList: [],
    expanded: {
      business: false,
      addresslist: false,
      entityname: true,
      entitytype: true
    },
    readOnlyFeatures: {
      msrbFirmNameReadOnly: false,
      msrbIdReadOnly: false,
      registrantTypeReadOnly: false,
      clientTypeDisabled: false,
      firmName: false
    },
    msrbRegTypeResult: [],
    countryResult: [],
    annualRevenue: [],
    primarySectorsList: [],
    marketRoleList: [],
    linkCusipDebType: [],
    isPrimaryAddress: "",
    isEntityExists: {
      msrbId: "",
      firmName: ""
    },
    canEdit: false,
    canView: false,
    entityType: "",
    suggestions: [],
    issuer: []
  })

  onSaveAddress = addressList => {
    this.setState(
      prevState => ({
        ...prevState,
        firmDetails: {
          ...prevState.firmDetails,
          addresses: addressList
        },
        addressList
      }),
      () => this.handleSubmit("address")
    )
  }

  /** ************************ this is for submit form ********************************* */
  async handleSubmit(type) {
    this.submitted = true
    const { firmDetails, errorFirmDetail } = this.state
    const entityData = {
      _id: firmDetails._id ? firmDetails._id : "",
      entityFlags: firmDetails.entityFlags,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      taxId: firmDetails.taxId
    }

    const addressData = {
      _id: firmDetails._id ? firmDetails._id : "",
      addresses: firmDetails.addresses
    }

    let firmDetailsData = {
      _id: firmDetails._id ? firmDetails._id : "",
      entityFlags: firmDetails.entityFlags,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      taxId: firmDetails.taxId,
      addresses: firmDetails.addresses
    }

    try {
      let addresses = []
      let { addressList } = this.state
      firmDetailsData.addresses.forEach(address => {
        if (!checkEmptyElObject(address)) {
          addresses.push(address)
        }
      })
      firmDetailsData.addresses = addresses
      if (addresses.length > 0) {
        addresses = addresses.sort((a, b) => a._id > b._id)
        addressList =
          addressList.length > 0
            ? addressList.filter(obj => obj._id != addresses[0]._id)
            : []
      }
      const mergeAddresses = [...addresses]
      // const filteredAddresses  = mergeAddresses.filter(item=>item.isPrimary)
      let errorData

      if (type === "entity") {
        errorData = validateThirdPartyDetail(entityData, type)
        firmDetailsData = entityData
        if (entityData.firmName !== "") {
          errorFirmDetail.firmName = ""
          this.setState({ errorFirmDetail })
        }
      } else if (type === "address") {
        errorData = validateThirdPartyDetail(addressData, type)
        firmDetailsData = addressData
      } else {
        errorData = validateThirdPartyDetail(firmDetailsData, type)
        // firmDetailsData.addresses = mergeAddresses
      }
      // const errorData = validateThirdPartyDetail(firmDetailsData)
      if (
        errorData.error /* || filteredAddresses.length!==1 */ ||
        this.state.isEntityExists.firmName !== "" ||
        this.state.isEntityExists.msrbId !== ""
      ) {
        if (errorData.error) {
          convertError(errorFirmDetail, errorData)
        }
        /* if(filteredAddresses.length ===0)
          this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
        else if(filteredAddresses.length >1)
          this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
        else
          this.state.isPrimaryAddress  = "" */
        this.setState({ errorFirmDetail })
      } else {
        this.setState({
          loading: true
        })
        const result = await updateThirdPartyFirm({
          firmDetails: firmDetailsData,
          mergeAddresses
        })
        if (!result.error) {
          this.setState({
            firmDetails,
            errorFirmDetail /* :cloneDeep(this.initialState().firmDetails) */,
            loading: false,
            readOnlyFeatures: {
              msrbFirmNameReadOnly: false,
              msrbIdReadOnly: false
            }
          })
          this.submitted = false
          toast("Third Party Firms (" + type + ") has been Update!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
          if (type === "all") {
            this.props.history.push("/mast-thirdparty")
          }
        } else {
          toast("Something Went Wrong !", {
            autoClose: 2000,
            type: toast.TYPE.DANGER
          })
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    }
  }

  /** ************************ this is for add new address ********************************* */
  addNewBussinessAddress(event) {
    event.preventDefault()
    const { firmDetails } = this.state
    const { addresses } = firmDetails
    let isEmpty = false
    addresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true
      }
    })
    if (!isEmpty) {
      this.setState(prevState => {
        const firmDetails = {
          ...prevState.firmDetails
        }
        const errorFirmDetail = {
          ...prevState.errorFirmDetail
        }
        firmDetails.addresses.push(this.initialState().firmDetails.addresses[0])
        errorFirmDetail.addresses.push(
          this.initialState().firmDetails.addresses[0]
        )
        return {
          firmDetails,
          errorFirmDetail
        }
      })
    } else {
      toast("Already an empty address!", {
        autoClose: 2000,
        type: toast.TYPE.INFO
      })
    }
  }

  /** ************************ this is for reset form ********************************* */
  resetBussinessAddress = (e, type) => {
    e.preventDefault()
    const address = cloneDeep(this.initialState().firmDetails.addresses[0])
    address.isPrimary = true
    /* address.isActive = true */
    switch (type) {
      case "address":
        this.setState(prevState => ({
          firmDetails: {
            ...prevState.firmDetails,
            addresses: [address]
          },
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            addresses: [cloneDeep(this.initialState().firmDetails.addresses[0])]
          }
        }))
        break
      default:
        console.log("Default")
    }
  }

  routeNavigate() {
    this.props.history.push("new")
  }

  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = e => {
    e.preventDefault()
    const { oldFirmDetails } = this.state
    const { entityAliases } = oldFirmDetails
    this.setState(prevState => ({
      firmDetails: {
        ...prevState.firmDetails,
        entityAliases
      },
      errorFirmDetail: {
        ...prevState.errorFirmDetail,
        entityAliases: cloneDeep(this.initialState().firmDetails.entityAliases)
      }
    }))
  }

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type, key) => {
    e.preventDefault()
    const { firmDetails } = this.initialState()
    const { addresses } = firmDetails
    this.setState(prevState => {
      const addMoreData = cloneDeep({ ...prevState.firmDetails })
      const errorFirmDetail = cloneDeep({ ...prevState.errorFirmDetail })
      if (type === "aliases") {
        addMoreData.entityAliases.push("")
        errorFirmDetail.entityAliases.push("")
      } else {
        addMoreData.addresses[key][type].push(cloneDeep(addresses[0][type][0]))
        errorFirmDetail.addresses[key][type].push(
          cloneDeep(addresses[0][type][0])
        )
      }
      return { firmDetails: cloneDeep(addMoreData), errorFirmDetail }
    })
  }

  //* **************************************function used to update business addresses *****************************
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this.state.addressList.find(item => item._id == id)
    this.setState(prevState => {
      let {
        errorFirmDetail,
        firmDetails,
        expanded,
        oldErrorFirmDetail
      } = prevState
      errorFirmDetail = this.initialState().firmDetails
      firmDetails.addresses = [address]
      const arr = ["officeEmails", "officePhone", "officeFax"]
      arr.forEach(item => {
        if (address[item].length > 1) {
          for (let i = 1; i < address[item].length; i++) {
            errorFirmDetail.addresses[0][item].push(
              this.initialState().firmDetails.addresses[0][item][0]
            )
          }
        }
      })
      expanded.business = true
      oldErrorFirmDetail.addresses = errorFirmDetail.addresses
      return { firmDetails, errorFirmDetail, expanded }
    })
  }

  /** *****************************************Toggle Button********************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map(item => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }

  /** *********************************************** update LinkCusip*********************************** */
  deleteAliases = idx => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails.entityAliases.splice(idx, 1)
    errorFirmDetail.entityAliases.splice(idx, 1)
    this.setState({
      firmDetails,
      errorFirmDetail
    })
  }
  deleteAddressAliases = (e, type, key, idx) => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails.addresses[key][type].splice(idx, 1)
    errorFirmDetail.addresses[key][type].splice(idx, 1)
    this.setState({
      firmDetails,
      errorFirmDetail
    })
  }
  resetAddressAliases = (e, type, key, id) => {
    if (id !== "") {
      this.setState(prevState => {
        const oldFirmDetails = cloneDeep({ ...prevState.oldFirmDetails })
        console.log("oldFirmDetails=====>>>", oldFirmDetails)
        const { firmDetails, errorFirmDetail } = prevState
        const oldErrorFirmDetail = cloneDeep({
          ...prevState.oldErrorFirmDetail
        })
        const { addresses } = oldFirmDetails
        const address = addresses.filter(item => item._id === id)
        firmDetails.addresses[key][type] = address[0][type]
        errorFirmDetail.addresses[key][type] = cloneDeep(
          oldErrorFirmDetail.addresses[key][type]
        )
        return { firmDetails, errorFirmDetail }
      })
    } else {
      const { firmDetails, errorFirmDetail } = this.state
      firmDetails.addresses[key][
        type
      ] = this.initialState().firmDetails.addresses[0][type]
      errorFirmDetail.addresses[key][
        type
      ] = this.initialState().firmDetails.addresses[0][type]
      this.setState({
        firmDetails,
        errorFirmDetail
      })
    }
  }
  deleteAddress = (e, type, idx) => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails[type].splice(idx, 1)
    errorFirmDetail[type].splice(idx, 1)
    this.setState({
      firmDetails,
      errorFirmDetail
    })
  }
  checkDuplicateFirm = async e => {
    e.preventDefault()
    const { value, name } = e.target
    const { entityId } = this.props.auth.userEntities
    const { isEntityExists } = this.state
    if (
      value.length > 0 &&
      value !== this.admThirdParty.firmDetailById[e.target.name]
    ) {
      const result = await checkDuplicateEntity(value, entityId)
      if (result.success.isExist) {
        isEntityExists[name] = "Firms Name is Already Exists"
      } else {
        isEntityExists[name] = ""
      }
      this.setState({
        isEntityExists
      })
    }
  }
  onChangeAddressType = (e, idx) => {
    const { firmDetails } = this.state
    let { addressList } = this.state
    let { addresses } = firmDetails
    if (e.target.name === "isPrimary") {
      firmDetails.addresses[idx].isPrimary = !firmDetails.addresses[idx]
        .isPrimary
    } else {
      firmDetails.addresses[idx].isPrimary = false
      firmDetails.addresses[idx].isHeadQuarter = true
    }
    if (this.submitted) {
      addressList =
        addressList.length > 0
          ? addressList.filter(obj => obj._id != addresses[0]._id)
          : []
      addresses = addresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [...addressList, ...addresses]
      /* const filteredAddresses  = mergeAddresses.filter(item=>item.isPrimary)
      if(filteredAddresses.length ===0)
        this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
      else if(filteredAddresses.length >1)
        this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
      else
        this.state.isPrimaryAddress  = "" */
    }
    this.setState({
      firmDetails
    })
  }
  getAddressDetails = (address = "", idx) => {
    if (address !== "") {
      const { firmDetails, errorFirmDetail } = this.state
      let id = ""
      if (firmDetails.addresses[idx]._id !== "") {
        id = firmDetails.addresses[idx]._id
      }
      firmDetails.addresses[idx] = {
        ...this.initialState().firmDetails.addresses[0]
      }
      firmDetails.addresses[idx]._id = id
      if (address.addressLine1 !== "") {
        firmDetails.addresses[idx].addressName =
          firmDetails.addresses[idx].addressName === ""
            ? `${address.addressLine1}, ${address.city}`
            : address.addressLine1
        firmDetails.addresses[idx].addressLine1 = address.addressLine1
        errorFirmDetail.addresses[idx].addressName = ""
        errorFirmDetail.addresses[idx].addressLine1 = ""
      }
      if (address.country !== "") {
        errorFirmDetail.addresses[idx].country = ""
        firmDetails.addresses[idx].country = address.country
      }
      if (address.state !== "") {
        errorFirmDetail.addresses[idx].state = ""
        firmDetails.addresses[idx].state = address.state
      }
      if (address.city !== "") {
        errorFirmDetail.addresses[idx].city = ""
        firmDetails.addresses[idx].city = address.city
      }
      if (address.zipcode !== "") {
        errorFirmDetail.addresses[idx].zipCode.zip1 = ""
        firmDetails.addresses[idx].zipCode.zip1 = address.zipcode
      }
      firmDetails.addresses[idx].formatted_address = address.formatted_address
      firmDetails.addresses[idx].url = address.url
      firmDetails.addresses[idx].location = { ...address.location }

      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...firmDetails
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        }
      }))
    }
  }
  checkAddressStatus = async (e, id, field) => {
    const { addressList } = this.state
    let addressIdx = ""
    addressList.forEach((address, idx) => {
      if (address._id == id) {
        addressIdx = idx
      }
    })
    if (field === "isPrimary") {
      addressList.forEach((address, idx) => {
        if (address._id === id) addressList[idx][field] = true
        else addressList[idx][field] = false
      })
      toast("Your Primary Address is Updated.", {
        autoClose: 2000,
        type: toast.TYPE.WARNING
      })
    }
    this.setState(prevState => ({
      ...prevState,
      addressList
    }))
  }

  getEntityIssuerList = async inputVal => {
    const input = inputVal && inputVal.value.trim()
    this.getEntityIssuer(input)
  }

  getEntityIssuer = async inputVal => {
    const { issuer } = this.state
    const firmName =
      issuer && issuer.filter(item => item.firmName.includes(inputVal))
    const firm =
      firmName && firmName.filter(item => item.firmName.toLowerCase())
    this.setState({
      suggestions: firm || []
    })
    console.log("===============firm========================>", firm)
  }

  onClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  render() {
    const loading = () => <Loader />
    const { canEdit, canView, countryResult } = this.state
    return (
      <section className="firms">
        <section className="accordions">
          {this.state.loading ? loading() : null}
          <EntityType
            expanded={this.state.expanded}
            toggleButton={this.toggleButton}
            onChangeIssuerFlag={this.onChangeFirmDetail}
            marketRoleList={this.state.marketRoleList[1]}
            entityFlags={this.state.firmDetails.entityFlags}
            errorFirmDetail={this.state.errorFirmDetail}
            canEdit={canEdit}
            canView={canView}
            onSave={this.handleSubmit}
            busy={this.state.loadingPickLists}
          />
          <article
            className={
              this.state.expanded.entityname
                ? "accordion is-active"
                : "accordion"
            }
          >
            <div className="accordion-header toggle">
              <p
                onClick={event => {
                  this.toggleButton(event, "entityname")
                }}
              >
                Entity Name
              </p>
            </div>
            {this.state.expanded.entityname && <FirmProfileForm
              firmDetails={this.state.firmDetails}
              onChangeFirmDetail={this.onChangeFirmDetail}
              errorFirmDetail={this.state.errorFirmDetail}
              firmList={this.state.firmList}
              getFirmDetails={this.getFirmDetails}
              addMore={this.addMore}
              onChangeAliases={this.onChangeAliases}
              resetAliases={this.resetAliases}
              readOnlyFeatures={this.state.readOnlyFeatures}
              msrbRegTypeResult={this.state.msrbRegTypeResult}
              secondarySectors={this.state.secondarySectors}
              numberOfEmployee={this.state.numberOfEmployee}
              primarySectorsList={this.state.primarySectorsList}
              deleteAliases={this.deleteAliases}
              msrbFirmListResult={this.state.msrbFirmListResult}
              checkDuplicateFirm={this.checkDuplicateFirm}
              isEntityExists={this.state.isEntityExists}
              getEntityFirmList={this.getEntityIssuerList}
              onClearRequested={this.onClearRequested}
              canEdit={canEdit}
              canView={canView}
              entityType={this.state.entityType}
              onChangeEntity={this.onChangeEntity}
              onSave={this.handleSubmit}
              suggestions={this.state.suggestions}
              getSuggestionValue={this.getSuggestionValue}
            />}
          </article>

          <FirmAddressListForm
            key={parseInt(1 * 30, 10)}
            errorFirmDetail={this.state.errorFirmDetail.addresses[0]}
            countryResult={countryResult}
            canEdit={canEdit}
            canView={canView}
            busy={this.state.loadingPickList}
            addressList={this.state.addressList}
            onSaveAddress={this.onSaveAddress}
          />
          {canEdit && (
            <div className="columns">
              <div className="column is-full">
                <div className="field is-grouped-center">
                  <div className="control">
                    <button
                      className="button is-link"
                      onClick={() => this.handleSubmit("all")}
                    >
                      Save All
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </section>
      </section>
    )
  }
}

const mapStateToProps = state => {
  const { admThirdParty, auth } = state
  return { admThirdParty, auth }
}

const mapDispatchToProps = {
  getAllThirdPartyFirmList,
  getThirdPartyFirmDetailById,
  searchFirms
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UpdateThirdPartyContainer)
)
