import React, { Component } from "react"
import "react-table/react-table.css"
import { withRouter, Link } from "react-router-dom"
import Fuse from "fuse.js"
import { connect } from "react-redux"
import "./../../scss/entity.scss"
import EntityPageFilter from "./../../../EntityManagement/CommonComponents/EntityPageFilter"

class ThirdPartyFirmListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: {
        pagefilter: false
      },
      thirdPartyList: [],
      modalState: false,
      marketRoles: "",
      loading: false,
      pageSizeOptions: [5, 10, 20, 25, 50],
      pageSize: 10,
      countryResult: [],
      filteredValue: {
        entityType: "",
        enityState: [],
        entitySearch: ""
      },
      search: "",
      searchname: "",
      entityId: "",
      isSaving: false,
      savedSearches: "",
      savedSearchesList: [],
      savedSearchesData: [],
      marketRoleList: [],
      viewList: true
    }
  }
  async componentWillMount() {}
  async componentDidMount() {}

  fuse = e => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    const res = fuse.search(search)
    return res
  }

  render() {
    return (
      <div className="firms">
        <EntityPageFilter
          listType="third-party"
          auth={this.props.auth}
          entityId={this.props.nav2 || ""}
          nav1={this.props.nav1 || ""}
          searchPref="mast-thirdparty"
          title="Third Parties"
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ThirdPartyFirmListContainer)
)
