import React from "react"
import { DropdownList, Multiselect } from "react-widgets"

const EntityType = props => (
  <article className={props.expanded.entitytype ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "entitytype")
        }}
      >Entity Type</p>{
        props.expanded.entitytype
          ? <i className="fas fa-chevron-up" />
          : <i className="fas fa-chevron-down" />
      }
    </div>
    {props.expanded.entitytype && <div className="accordion-body">
      <div className="accordion-content">
        <p className="title innerPgTitle">Please check all that apply</p>
        <div className="columns" style={{ display: "inline-block" }} >
          {props.busy ?
            <div style={{ padding: 30 }} className="is-link"><span className="fas fa-sync fa-spin is-link" /></div> :
            props.marketRoleList && props.marketRoleList.map((item, idx) => (
              <div className="column" key={Math.random()} style={{ display: "inline-block" }}>
                {!props.canEdit ? <small>{(props.entityFlags && props.entityFlags.marketRole || []).includes(item) && item}</small> :
                  <h1>
                    <p className="multiExpLbl " title={`Is ${item}?`}>
                      {item}
                      <input
                        type="checkbox"
                        name="marketRole"
                        value={item}
                        onChange={
                          (event) => {
                            event.target.name = "marketRole"
                            event.target.value = item
                            props.onChangeIssuerFlag(event)
                          }}
                        checked={!!(props.entityFlags && props.entityFlags.marketRole || []).includes(item)}
                      />
                    </p>
                  </h1>}
              </div>
            ))
          }
        </div>
        {props.errorFirmDetail && props.errorFirmDetail.entityFlags.marketRole && <small className="text-error">{props.errorFirmDetail.entityFlags.marketRole}</small>}
      </div>
    </div>}
  </article>
)
export default EntityType
