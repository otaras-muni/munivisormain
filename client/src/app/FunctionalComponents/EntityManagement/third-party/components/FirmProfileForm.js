import React from "react"
import { TextLabelInput, AsyncCreatableSelectInput } from "Global/TextViewBox"
import Autosuggest from "react-autosuggest"
import BorrowerList from "../../../../GlobalComponents/BorrowerList";

const FirmProfileForm = (props) => (
  <div className="accordion-body">
    <div className="accordion-content">
      <div className="columns firmProfile">
        <div className="column complain-details">
          <p className="multiExpLbl">Firm Name<span className='has-text-danger'>*</span></p>
          <BorrowerList value={props.firmDetails.firmName}
            onChange={props.onChangeEntity}
            entityType={props.entityType}
            thirdParty={true}
            onError={props.onError}/>
          <p className="has-text-danger text-error">{props.errorFirmDetail && props.errorFirmDetail.firmName || props.firmError}</p>

          <br />
          {props.firmDetails.entityAliases &&
            props.firmDetails.entityAliases.length
            ? props.firmDetails.entityAliases.map((item, idx) => (
              <div className="field is-grouped-left-without-padding">
                {!props.canEdit ? <small>{item}</small>
                  : <div className="control w-100 d-flex">
                    <input
                      className="input is-small is-link"
                      name="entityAliases"
                      type="text"
                      placeholder="Enter Aliases"
                      value={item}
                      onChange={event => {
                        console.log("Event ")
                        props.onChangeAliases(event, idx)
                      }}
                    />
                    <span className="has-text-link fa-delete deleteadressaliases"
                      onClick={() => {
                        props.deleteAliases(idx)
                      }}
                    >
                      <i className="far fa-trash-alt" />
                    </span>
                  </div>}
                {props.errorFirmDetail && props.errorFirmDetail.entityAliases[idx] && <small className="text-error">{props.errorFirmDetail.entityAliases[idx]}</small>}
              </div>
            ))
            : ""}
          {props.canEdit ? <div className="field is-grouped-left">
            <div className="control d-flex">
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "aliases")
                }}
              >
                Add Alias
              </button>
            </div>
            <div className="control d-flex">
              <button
                className="button is-light is-small"
                onClick={props.resetAliases}
              >
                Cancel
              </button>
            </div>
          </div>
            : ""}
        </div>
        <TextLabelInput
          name="taxId"
          value={props.firmDetails.taxId}
          placeholder="Enter Tax ID"
          style={{ width: "100%" }}
          label="Tax ID"
          onChange={props.onChangeFirmDetail}
          disabled={!props.canEdit || false}
          error={props.errorFirmDetail && props.errorFirmDetail.taxId ? props.errorFirmDetail.taxId : ""}
        />
      </div>
      <div className="columns">
        <div className="column is-full">
          <div className="field is-grouped-center">
            <div className="control">
              <button
                className="button is-link"
                onClick={() => props.onSave("entity")}
              >
                Save Entity
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div >
)

export default FirmProfileForm
