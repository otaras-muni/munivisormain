import React from "react"
import { Redirect } from "react-router"
import ClientsView from "./ClientsView"
import {checkSupervisorControls} from "../../../StateManagement/actions/CreateTransaction";
import connect from "react-redux/es/connect/connect";

class ClientsMain extends React.Component {
  constructor() {
    super()
    this.state = {
      svControls: {}
    }
  }
  async componentDidMount() {
    const svControls = await checkSupervisorControls()
    this.setState({ svControls })
  }
  render () {
    const { nav1, nav, nav2, nav3, audit } = this.props
    const { svControls } = this.state

    if (nav2) {
      return (
        <ClientsView id={nav2} nav1={nav1} accessibleTabs={Object.keys((nav && nav[nav1]) || {})} navComponent={nav3} audit={audit} svControls={svControls}/>
      )
    }
    return (
      <Redirect to="/mast-thirdparty" />
    )
  }
}

const mapStateToProps = state => ({
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

export default connect(mapStateToProps, null)(ClientsMain)
