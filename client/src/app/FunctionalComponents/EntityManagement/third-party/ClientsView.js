import React from "react"
import { NavLink,Redirect } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
// import UpdateThirdPartyContainer from "./containers/UpdateThirdPartyContainer"
import AuditLog from "Global/AuditLog"
import ThirdPartyNew from "../../ThirdParty/ThirdParty"
import ThirdPartyContactListContainer from "../contacts/containers/ContactListContainer"
import BusinessActivity from "../business-activity/containers/BusinessActivityListContainer"
import Documents from "../Documents"
import {ContextType} from "../../../../globalutilities/consts"
import Audit from "../../../GlobalComponents/Audit";

const TABS = [
  { path: "entity", label: "Entity" },
  { path: "business-activity", label: "Business Activity"},
  { path: "contacts", label: "People"},
  // { path: "contracts", label: "Contracts"},
  { path: "documents", label: "Documents"},
  { path: "activity-log", label: "Activity Log" }
]
const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}
const ClientsView = props => {
  let { navComponent, audit, svControls } = props
  const {id, accessibleTabs, nav1} = props
  navComponent = navComponent || "details"
  const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
  if (!submitAudit) {
    const index = accessibleTabs.findIndex(tab => tab === "activity-log")
    if(index !== -1){
      accessibleTabs.splice(index, 1)
    }
  }

  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => {
      if(accessibleTabs.indexOf(t.path) === -1){
        return false
      }
      return (
        <li key={t.path}>
          <NavLink key={t.path} to={`/thirdparties/${id}/${t.path}`} activeStyle={activeStyle}> {t.label} </NavLink>
        </li>
      )
    })
  const renderViewSelection = (id, navComponent) => (
    <nav className="tabs is-boxed">
      <ul>{renderTabs(TABS, navComponent, id)}</ul>
    </nav>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(id, navComponent)}
          </div>
        </div>
      </div>
      <div id="main">
        <Switch condition={navComponent}>
          <Case value="entity">
            <ThirdPartyNew nav2={id} />
          </Case>
          <Case value="business-activity">
            <BusinessActivity/>
          </Case>
          <Case value="metrix">
            <div>Just for test</div>
          </Case>
          <Case value="contacts">
            <ThirdPartyContactListContainer nav2={id} />
          </Case>
          <Case value="contracts">
            <div>Just for test</div>
          </Case>
          <Case value="documents">
            <Documents nav2 ={id} contextType={ContextType.thirdParty} />
          </Case>
          <Case value="activity-log">
            <Audit nav1={nav1} nav2={id}/>
          </Case>
          <Default>
            <Redirect to="/mast-thirdparty" />
          </Default>
        </Switch>
      </div>
    </div>
  )
}

export default ClientsView
