import React from "react"
import styled from "styled-components"
import UsersList from "./UsersList"
import "./../../../SearchComponent/scss/search.scss"

const TitleBar = styled.div`
  display: flex;
  font-size: 24px;
  font-weight: bold;
  cursor: pointer;
  text-transform: uppercase;
`
class GroupByResultSet extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      showResult: props.showResultSet || false
    }

    this.toggleResultSet = this.toggleResultSet.bind(this)
  }

  toggleResultSet() {
    this.setState({
      showResult: !this.state.showResult
    })
  }
  render() {
    const { title, items, search, urls } = this.props
    const { showResult } = this.state
    return (
      <div>
        <div className="columns">
          <div className="column">
            <div className="accordions">
              <div className={`${showResult ? "accordion is-active":"accordion"}`}>
                {title!==""&& title!==null ?
                  <div className="accordion-header" onClick={this.toggleResultSet}>
                    <span>{title}</span>
                    <div>({items.length} found) &nbsp;<i className={`${showResult ? "fas fa-chevron-down":"fas fa-chevron-up"}`}/>
                    </div>
                  </div>          
                  :""
                }        
                {
                  showResult && (
                    <UsersList 
                      userslist={items}
                      search = {search}
                      urls = {urls}
                    />
                  )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default GroupByResultSet
