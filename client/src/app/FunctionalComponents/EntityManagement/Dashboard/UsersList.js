import React from "react"
import { Link } from "react-router-dom"

import {numberWithCommas} from "GlobalUtils/helpers"
import ReactTable from "react-table"
import "react-table/react-table.css"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"), "i")
  return string.replace(reg, str => (
    `<mark>${str}</mark>`
  ))
}
const UsersList = ({ userslist, pageSizeOptions = [5,10, 20, 50, 100], search,urls }) => {  
  const {allurls} = urls
  const columns = [
    {
      Header: "Issuer Name",
      id: "issuer",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value       
        return (
          <Link to={allurls[item.clientEntityId].entity} dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranClientName || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.tranClientName.localeCompare(b.tranClientName)
    },
    {
      id: "description",
      Header: "Issue Description/Name",
      accessor: item => item,
      Cell: row => {
        const item = row.value       
        return (
          <Link to={allurls[item.tranId] && allurls[item.tranId].summary||""} dangerouslySetInnerHTML={{ __html: highlightSearch(item.activityDescription || "-", search) }}/>
        )
      },
      sortMethod: (a, b) => a.activityDescription.localeCompare(b.activityDescription)
    },
    {
      id: "type",
      Header: "Type",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranType || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.tranType.localeCompare(b.tranType)
    },
    {
      id: "principalAmount",
      Header: ()=> (<span title="Principal Amount($)">Principal Amount($)</span>),
      accessor: item => item,
      Cell: row => {
        const item = row.value     
        return (
          <div className="hpTablesTd">
            {numberWithCommas(item.tranParAmount)}
          </div>
        )
      },
      maxWidth: 150,
      sortMethod: (a, b) => a.tranParAmount - b.tranParAmount
    },
    {
      id: "leadManager",
      Header: "Lead Manager",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <Link to={allurls[item.tranAssigneeDetails.userId] && allurls[item.tranAssigneeDetails.userId].users||""} dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranAssigneeDetails.userFullName || "-", search) }}/>
        )
      },
      sortMethod: (a, b) => a.leadManager.localeCompare(b.leadManager)
    },
    {
      id: "couponType",
      Header: "Coupon Type",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.middleName || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.middleName.localeCompare(b.middleName)
    },
    {
      id: "purposeSector",
      Header: "Purpose/Sector",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranPrimarySector || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.tranPrimarySector.localeCompare(b.tranPrimarySector)
    },
    {
      id: "pricingDate",
      Header: "Expected Pricing Date",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranPricingDate || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.tranPricingDate.localeCompare(b.tranPricingDate)
    },
    {
      id: "closingDate",
      Header: "Expected Closing Date",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranExpectedAwardDate || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.tranExpectedAwardDate.localeCompare(b.tranExpectedAwardDate)
    },
    {
      id: "status",
      Header: "Status",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{ __html: highlightSearch(item.tranStatus || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.tranStatus.localeCompare(b.tranStatus)
    }                                       
    
  ]
  return (
    <div className="column box">
      <ReactTable
        columns={columns}
        data={userslist}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        // ={pageSize || 10}
        showPageJump
        minRows={2}
      />
    </div>

  )
}

export default UsersList