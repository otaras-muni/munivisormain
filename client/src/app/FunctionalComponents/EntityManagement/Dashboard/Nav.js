import React from "react"
import { NavLink, withRouter } from "react-router-dom"

const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

const Nav = () => (
  <div className="hero is-link">
    <div className="hero-foot hero-footer-padding">
      <div className="container">
        <div className="tabs is-boxed">
          <ul className="is-uppercase">
            <li>
              <NavLink to="/dashboard" activeStyle={activeStyle}> Dashboard </NavLink>
            </li>
            <li>
              <NavLink to="/dash-projects-alt" activeStyle={activeStyle}> Projects </NavLink>
            </li>
            <li>
              <NavLink to="/dash-cltprosp" activeStyle={activeStyle}> Client / Prospects </NavLink>
            </li>
            <li>
              <NavLink to="/dash-cal" activeStyle={activeStyle}> Calendar </NavLink>
            </li>
            <li>
              <NavLink to="/bus-development" activeStyle={activeStyle}> Business Development </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
)

export default withRouter(Nav)
