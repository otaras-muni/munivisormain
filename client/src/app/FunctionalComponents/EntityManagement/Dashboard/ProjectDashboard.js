import React, { Component } from "react"
import { toast } from "react-toastify"
import { withRouter } from "react-router-dom"
import { getAllTransactions} from "AppState/actions/AdminManagement/admTrnActions"
import { connect } from "react-redux"
import GroupByResultSet from "./GroupByResultSet"
import Loader from "../../../GlobalComponents/Loader"
import Nav from "./Nav.js"

class ProjectDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      transactionList:[],
      searchProject:"",
      search: ""
    }
  }
  async componentWillMount() {
    this.setState({
      loading: true
    })
  }
  async componentDidMount() {
    let {transactionList} = this.state
    const result = await getAllTransactions("")     
    if(result.status===200 && result.data){
      transactionList = result.data
    }
    this.setState({
      loading:false,
      transactionList
    })
  }

  searchProject = async(e)=>{
    const {name, value} = e.target
    let searchValue = ""
    if( value.length === 0) {
      searchValue = ""
    } else {
      searchValue = value
    }
    this.setState({     
      searchProject:value,
      search:value
    })    
    const result = await getAllTransactions(searchValue)
    if(result.status===200 && result.data){
      this.setState({              
        transactionList:result.data
      })
    }    
  }


  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }
  render() {
    const loading = () => <Loader />
    if (this.state.loading) {
      return loading()            
    }
    const {transactionList} = this.state
    const {allData,urls} = transactionList    
    return (
      <div>
        <Nav/>
        <div className="top-bottom-margin">
          <div className="column is-one-third is-offset-8">
            <p className="control has-icons-left">
              <input className="input customInput css-dnqvel ep3169p0" 
                placeholder="Search.." 
                value={this.state.searchProject}
                name="searchProject"
                onChange={this.searchProject}
              />
              <span className="icon is-left searchIcon">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>          
          {
            Object.keys(allData).length>0 && Object.keys(allData).map((item, idx)=>(
              <GroupByResultSet
                title={item}
                items={allData[item]}
                showResultSet={idx === 0}
                search={this.state.search}
                key={idx}
                urls={urls}
              />
            ))
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProjectDashboard))