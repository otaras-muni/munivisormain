import React from "react"

const ClientTypeFiler = ({ searchPref, onChange }) => (
  <div className="select is-fullwidth is-link is-small">
    <select value={(searchPref.clientType || "")} onChange={(e) => onChange("clientType", e.target.value)}>
      <option value="">Client and Prospects</option>
      <option value="Client">Client Only</option>
      <option value="Prospect">Prospect Only</option>
    </select>
  </div>
)

export default ClientTypeFiler
