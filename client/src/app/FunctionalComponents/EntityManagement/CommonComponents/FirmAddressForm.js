import React from "react"
import SearchAddress from "./../../GoogleAddressForm/GoogleAddressFormComponent"
import {
  TextLabelInput,
  DropDownInput,
  ZipCodeNumberInput
} from "./../../../GlobalComponents/TextViewBox"

const AdmTrnAddressForm = props => (
  <div
    className="accordion-content"
    style={{ borderBottom: "1px solid #8393a8" }}
  >
    <div className="tile is-ancestor">
      <div className="tile is-12 is-vertical is-parent">
        {props.canEdit && (
          <SearchAddress
            idx={props.idx}
            getAddressDetails={props.getAddressDetails}
          />
        )}
        <div className="columns">
          <TextLabelInput
            error={
              (props.errorFirmDetail && props.errorFirmDetail.addressName) || ""
            }
            name="addressName"
            type="text"
            label="Address Name"
            value={props.businessAddress.addressName || ""}
            onChange={event => {
              props.onChangeBusinessAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
          <div className="column is-half">
            <div className="column">
              <div
                className="column is-half"
                style={{ display: "inline-block" }}
              >
                <p className="multiExpLbl">
                  <input
                    name="isPrimary"
                    type="checkbox"
                    value={props.businessAddress.isPrimary}
                    checked={props.businessAddress.isPrimary}
                    onChange={event => {
                      props.onChangeAddressType(event, props.idx)
                    }}
                    disabled={!props.canEdit}
                  />{" "}
                  Is Primary?
                </p>
              </div>
              <div style={{ padding: "0.75rem", float: "right" }}>
                {props.idx > 0 ? (
                  <span
                    className="has-text-link fa-delete"
                    onClick={() => {
                      props.deleteAddress(event, "addresses", props.idx)
                    }}
                  >
                    <i className="far fa-trash-alt" />
                  </span>
                ) : (
                  ""
                )}
              </div>
            </div>
            {props.isPrimaryAddress && (
              <small className="text-error">{props.isPrimaryAddress}</small>
            )}
          </div>
        </div>

        <div className="columns">
          <TextLabelInput
            error={
              (props.errorFirmDetail && props.errorFirmDetail.website) || ""
            }
            name="website"
            type="text"
            label="Website(s)"
            value={props.businessAddress.website || ""}
            onChange={event => {
              props.onChangeBusinessAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
        </div>

        <div className="columns">
          <div className="column is-third">
            <p className="multiExpLbl">Office Phone</p>
            {props.businessAddress.officePhone.map((item, idx) => (
              <div
                className="field is-grouped-left"
                key={parseInt(idx * 20, 10)}
              >
                <div className="control d-flex">
                  <ZipCodeNumberInput
                    format="+1 (###) ###-####"
                    mask="_"
                    className="input is-small is-link"
                    name="phoneNumber"
                    size="15"
                    value={item.phoneNumber || ""}
                    disabled={!props.canEdit}
                    error={
                      (props.errorFirmDetail &&
                        props.errorFirmDetail.officePhone[idx].phoneNumber) ||
                      ""
                    }
                    onChange={event => {
                      props.onChangeAddMore(
                        event,
                        "officePhone",
                        props.idx,
                        idx
                      )
                    }}
                  />
                </div>
                <div>
                  <div className="control d-flex">
                    <ZipCodeNumberInput
                      format="##########"
                      className="input is-small is-link"
                      name="extension"
                      size="8"
                      value={item.extension || ""}
                      disabled={!props.canEdit}
                      onChange={event => {
                        props.onChangeAddMore(
                          event,
                          "officePhone",
                          props.idx,
                          idx
                        )
                      }}
                    />
                    {idx && props.canEdit > 0 ? (
                      <span
                        className="has-text-link fa-delete deleteadressaliases"
                        onClick={() => {
                          props.deleteAddressAliases(
                            event,
                            "officePhone",
                            props.idx,
                            idx
                          )
                        }}
                      >
                        <i className="far fa-trash-alt" />
                      </span>
                    ) : (
                      ""
                    )}
                    {props.errorFirmDetail &&
                      props.errorFirmDetail.officePhone[idx].extension && (
                        <small className="text-error">
                          {props.errorFirmDetail.officePhone[idx].extension}
                        </small>
                      )}
                  </div>
                </div>
              </div>
            ))}
            {props.canEdit && (
              <div className="control">
                <button
                  className="button is-link is-small"
                  onClick={event => {
                    props.addMore(event, "officePhone", props.idx)
                  }}
                >
                  Add More
                </button>
                <button
                  className="button is-light is-small"
                  onClick={event => {
                    props.resetAddressAliases(
                      event,
                      "officePhone",
                      props.idx,
                      props.businessAddress._id ? props.businessAddress._id : ""
                    )
                  }}
                >
                  Cancel
                </button>
              </div>
            )}
          </div>
          <div className="column">
            <p className="multiExpLbl">Office Fax</p>
            {props.businessAddress.officeFax.map((item, idx) => (
              <div className="field" key={parseInt(idx * 10, 10)}>
                <div className="control d-flex">
                  <ZipCodeNumberInput
                    format="+1 (###) ###-####"
                    mask="_"
                    className="input is-small is-link"
                    name="faxNumber"
                    minWidth="90%"
                    value={item.faxNumber ? item.faxNumber : ""}
                    onChange={event => {
                      props.onChangeAddMore(event, "officeFax", props.idx, idx)
                    }}
                    disabled={!props.canEdit}
                  />
                  {idx > 0 && props.canEdit ? (
                    <span
                      className="has-text-link fa-delete deleteadressaliases"
                      onClick={() => {
                        props.deleteAddressAliases(
                          event,
                          "officeFax",
                          props.idx,
                          idx
                        )
                      }}
                    >
                      <i className="far fa-trash-alt" />
                    </span>
                  ) : (
                    ""
                  )}
                </div>
                {props.errorFirmDetail &&
                  props.errorFirmDetail.officeFax[idx].faxNumber && (
                    <small className="text-error">
                      {props.errorFirmDetail.officeFax[idx].faxNumber}
                    </small>
                  )}
              </div>
            ))}
            {props.canEdit && (
              <div className="control">
                <button
                  className="button is-link is-small"
                  onClick={event => {
                    props.addMore(event, "officeFax", props.idx)
                  }}
                >
                  Add More
                </button>
                <button
                  className="button is-light is-small"
                  onClick={event => {
                    props.resetAddressAliases(
                      event,
                      "officeFax",
                      props.idx,
                      props.businessAddress._id ? props.businessAddress._id : ""
                    )
                  }}
                >
                  Cancel
                </button>
              </div>
            )}
          </div>

          <div className="column">
            <p className="multiExpLbl">Office Email</p>
            {props.businessAddress.officeEmails.map((item, idx) => (
              <div className="field" key={parseInt(idx * 5, 10)}>
                <div className="control d-flex">
                  <TextLabelInput
                    error={
                      (props.errorFirmDetail &&
                        props.errorFirmDetail.website) ||
                      ""
                    }
                    name="emailId"
                    type="email"
                    placeholder="e.g. alexsmith@gmail.com"
                    value={item.emailId ? item.emailId : ""}
                    onChange={event => {
                      props.onChangeAddMore(
                        event,
                        "officeEmails",
                        props.idx,
                        idx
                      )
                    }}
                    disabled={!props.canEdit}
                  />
                  {idx > 0 && props.canEdit ? (
                    <span
                      className="has-text-link fa-delete deleteadressaliases"
                      onClick={() => {
                        props.deleteAddressAliases(
                          event,
                          "officeEmails",
                          props.idx,
                          idx
                        )
                      }}
                    >
                      <i className="far fa-trash-alt" />
                    </span>
                  ) : (
                    ""
                  )}
                </div>
                {props.errorFirmDetail &&
                  props.errorFirmDetail.officeEmails[idx].emailId && (
                    <small className="text-error">
                      {props.errorFirmDetail.officeEmails[idx].emailId}
                    </small>
                  )}
              </div>
            ))}
            {props.canEdit && (
              <div className="control">
                <button
                  className="button is-link is-small"
                  onClick={event => {
                    props.addMore(event, "officeEmails", props.idx)
                  }}
                >
                  Add More
                </button>
                <button
                  className="button is-light is-small"
                  onClick={event => {
                    props.resetAddressAliases(
                      event,
                      "officeEmails",
                      props.idx,
                      props.businessAddress._id ? props.businessAddress._id : ""
                    )
                  }}
                >
                  Cancel
                </button>
              </div>
            )}
          </div>
        </div>
        <div className="columns">
          <TextLabelInput
            error={
              (props.errorFirmDetail && props.errorFirmDetail.addressLine1) ||
              ""
            }
            type="text"
            label="Address Line 1"
            name="addressLine1"
            placeholder="address line 1"
            value={props.businessAddress.addressLine1 || ""}
            onChange={event => {
              props.onChangeBusinessAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
          <TextLabelInput
            error={
              (props.errorFirmDetail && props.errorFirmDetail.addressLine2) ||
              ""
            }
            type="text"
            label="Address Line 2"
            name="addressLine2"
            placeholder="address line 2"
            value={props.businessAddress.addressLine2 || ""}
            onChange={event => {
              props.onChangeBusinessAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
        </div>
        <div className="columns">
          <DropDownInput
            filter
            value={props.businessAddress.country}
            data={props.countryResult[1] || [""]}
            message="select Country"
            textField="country"
            style={{ fontSize: 12 }}
            valueField="value"
            key="name"
            label="Country"
            onChange={val => {
              const event = {
                target: {
                  name: "country",
                  value: val
                }
              }
              props.onChangeBusinessAddress(event, props.idx, ["state", "city"])
            }}
            disabled={!props.canEdit}
            disableValue={props.businessAddress.country}
            error={props.errorFirmDetail && props.errorFirmDetail.country}
            busy={props.busy}
          />
          <DropDownInput
            filter
            value={props.businessAddress.state}
            data={
              props.businessAddress.country &&
              props.countryResult[2][props.businessAddress.country]
                ? [...props.countryResult[2][props.businessAddress.country]]
                : []
            }
            message="Select State"
            textField="state"
            style={{ fontSize: 12 }}
            valueField="value"
            label="State"
            onChange={val => {
              const event = {
                target: {
                  name: "state",
                  value: val
                }
              }
              props.onChangeBusinessAddress(event, props.idx, ["city"])
            }}
            disabled={!props.canEdit}
            disableValue={props.businessAddress.state}
            error={props.errorFirmDetail && props.errorFirmDetail.state}
            busy={props.busy}
          />
          <DropDownInput
            filter
            value={props.businessAddress.city}
            data={
              props.businessAddress.state &&
              props.businessAddress.country &&
              (props.countryResult[3][props.businessAddress.country] &&
                props.countryResult[3][props.businessAddress.country][
                  props.businessAddress.state
                ])
                ? [
                    ...props.countryResult[3][props.businessAddress.country][
                      props.businessAddress.state
                    ]
                  ]
                : []
            }
            message="Select City"
            textField="city"
            style={{ fontSize: 12 }}
            valueField="value"
            label="City"
            onChange={val => {
              const event = {
                target: {
                  name: "city",
                  value: val
                }
              }
              props.onChangeBusinessAddress(event, props.idx, [])
            }}
            disabled={!props.canEdit}
            disableValue={props.businessAddress.city}
            error={props.errorFirmDetail && props.errorFirmDetail.city}
            busy={props.busy}
          />

          <div className="column" style={{ marginTop: 0 }}>
            <p className="multiExpLbl">Zip Code</p>
            <div className="field is-grouped-left">
              <div className="control d-flex">
                <ZipCodeNumberInput
                  name="zip1"
                  size="5"
                  type="text"
                  format="#####"
                  placeholder="Zip Code 1"
                  label=""
                  disabled={!props.canEdit}
                  error={
                    (props.errorFirmDetail &&
                      props.errorFirmDetail.zipCode.zip1) ||
                    ""
                  }
                  value={props.businessAddress.zipCode.zip1 || ""}
                  onChange={event => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }}
                />
              </div>
              <div className="control d-flex">
                <ZipCodeNumberInput
                  name="zip2"
                  size="4"
                  type="text"
                  format="####"
                  placeholder="Zip Code 2"
                  label=""
                  disabled={!props.canEdit}
                  error={
                    (props.errorFirmDetail &&
                      props.errorFirmDetail.zipCode.zip2) ||
                    ""
                  }
                  value={props.businessAddress.zipCode.zip2 || ""}
                  onChange={event => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped-center">
              <div className="control">
                <button
                  className="button is-link"
                  onClick={() => props.onSave("address")}
                >
                  Save Address
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)
export default AdmTrnAddressForm
