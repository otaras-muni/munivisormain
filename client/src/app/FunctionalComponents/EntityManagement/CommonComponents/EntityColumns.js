import React from "react"
import { withRouter, Link, NavLink } from "react-router-dom"
import { numberWithCommas, transactionUrl } from "GlobalUtils/helpers"
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu"
import dateFormat from "../../../../globalutilities/dateFormat"
import moment from "moment"

const highlightSearch = (value, searchQuery) => {
  const reg = new RegExp(
    searchQuery ? searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&") : "",
    "i"
  )
  try {
    return value.replace(reg, str => `<mark>${str}</mark>`)
  } catch (ex) {
    return value
  }
}

const entityUrl = (entityType, entityName, entityId, search) => {
  let url = ""
  switch (entityType) {
  case "Firm":
  case "Self":
    url = `/admin-firms/${entityId || ""}/firms`
    break
  case "Client":
  case "Prospect":
    url = `/clients-propects/${entityId || ""}/entity`
    break
  case "Third Party":
    url = `/thirdparties/${entityId || ""}/entity`
    break
  case "Undefined":
    url = `/migratedentities/${entityId || ""}/entity`
    break
  default:
    url = `/thirdparties/${entityId || ""}/entity`
    break
  }
  return (
    <Link to={url}>
      <small
        dangerouslySetInnerHTML={{
          __html: highlightSearch(entityName || "-", search)
        }}
      />
    </Link>
  )
}

const userUrl = (userType, userName, userId, search, taskAssigneeType) => {
  let url = ""

  if( taskAssigneeType === "entity") {
    switch (userType) {
      case "Firm":
      case "Self":
        url = `/admin-firms/${userId || ""}/firms`
        break
      case "Client":
      case "Prospect":
        url = `/clients-propects/${userId || ""}/entity`
        break
      case "Third Party":
        url = `/thirdparties/${userId || ""}/entity`
        break
      case "Undefined":
        url = `/migratedentities/${userId || ""}/entity`
        break
      default:
        url = `/thirdparties/${userId || ""}/entity`
        break
      }

  }
  else {
    switch (userType) {
    case "Firm":
    case "Self":
      url = `/admin-users/${userId || ""}/users`
      break
    case "Client":
      url = `/contacts/${userId || ""}/cltprops`
      break
    case "Prospect":
      url = `/contacts/${userId || ""}/cltprops`
      break
    case "Third Party":
      url = `/contacts/${userId || ""}/thirdparties`
      break
    case "Undefined":
      url = `/contacts/${userId || ""}/migratedentities`
      break
    default:
      url = ""
      break
    }
  }
  return (
    <Link to={url}>
      <small
        dangerouslySetInnerHTML={{
          __html: highlightSearch(userName || "-", search)
        }}
      />
    </Link>
  )
}

export const thirdPartyColumns = (search, userType, toggleModal) => [
  {
    Header: "Type",
    id: "entityFlagsAggregated",
    className: "multiExpTblVal",
    accessor: item => item,
    minWidth: 150,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd -striped">
          {item.entityFlagsAggregated.split(",").length > 2 ? (
            <small>
              {item.entityFlagsAggregated
                .split(",")
                .slice(1, 3)
                .join()}
              <a
                onClick={event => {
                  event.preventDefault()
                  toggleModal(item.entityFlagsAggregated)
                }}
              >
                {" "}
                  More Roles...
              </a>
            </small>
          ) : (
            <small>{item.entityFlagsAggregated}</small>
          )}
        </div>
      )
    }
  },
  {
    id: "entityName",
    Header: "Entity Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {userType === "admin" ? (
            <Link
              to={`/admin-thirdparty/${item.firmId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.entityName || "-", search)
              }}
            />
          ) : (
            <Link
              to={`/thirdparties/${item.entityId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.entityName || "-", search)
              }}
            />
          )}
        </div>
      )
    },
    maxWidth: 200,
    sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
  },
  {
    Header: "City",
    className: "multiExpTblVal",
    id: "entityPrimaryAddressCity",
    maxWidth: 150,
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityPrimaryAddressCity || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityPrimaryAddressCity.localeCompare(b.entityPrimaryAddressCity)
  },
  {
    Header: "State",
    className: "multiExpTblVal",
    id: "entityPrimaryAddressState",
    maxWidth: 150,
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityPrimaryAddressState || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityPrimaryAddressState.localeCompare(b.entityPrimaryAddressState)
  },
  {
    Header: "User Name",
    className: "hpTablesTd",
    id: "userFullName",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return userUrl(item.entityRelationshipType, item.userFullName, item.userId, search)
    },
    maxWidth: 200,
    sortMethod: (a, b) => a.userFullName.localeCompare(b.userFullName)
  },
  {
    Header: "Primary Contact Phone",
    className: "multiExpTblVal",
    id: "userPrimaryPhoneNumber",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div>
          {
            (item && item.userPrimaryPhoneNumber !== "-") ?
              <a
                href={`tel:${item.userPrimaryPhoneNumber}`}
                className="hpTablesTd"
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(
                    item.userPrimaryPhoneNumber || "-",
                    search
                  )
                }}
              /> : "-"
          }
        </div>
      )
    },
    sortMethod: (a, b) =>
      a.userPrimaryPhoneNumber.localeCompare(b.userPrimaryPhoneNumber)
  }
]

export const clientProspectColumns = (search, userType) => [
  {
    Header: "Type",
    id: "entityRelationshipType",
    className: "multiExpTblVal",
    accessor: item => item,
    maxWidth: 150,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityRelationshipType || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityRelationshipType.localeCompare(b.entityRelationshipType)
  },
  {
    id: "entityName",
    Header: "Entity Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {userType === "admin" ? (
            <Link
              to={`/admin-cltprosp/${item.firmId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.entityName || "-", search)
              }}
            />
          ) : (
            <Link
              to={`/clients-propects/${item.entityId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.entityName || "-", search)
              }}
            />
          )}
        </div>
      )
    },
    maxWidth: 350,
    sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
  },
  {
    Header: "City",
    className: "multiExpTblVal",
    id: "entityPrimaryAddressCity",
    maxWidth: 200,
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityPrimaryAddressCity || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityPrimaryAddressCity.localeCompare(b.entityPrimaryAddressCity)
  },
  {
    Header: "State",
    className: "multiExpTblVal",
    id: "entityPrimaryAddressState",
    maxWidth: 200,
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityPrimaryAddressState || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityPrimaryAddressState.localeCompare(b.entityPrimaryAddressState)
  },
  {
    Header: "User Name",
    className: "hpTablesTd",
    id: "userFullName",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return userUrl(item.entityRelationshipType, item.userFullName, item.userId, search)
    },
    maxWidth: 300,
    sortMethod: (a, b) => a.userFullName.localeCompare(b.userFullName)
  },
  {
    Header: "Primary Contact Phone",
    className: "multiExpTblVal",
    id: "userPrimaryPhoneNumber",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div>
          {
            (item && item.userPrimaryPhoneNumber !== "-") ?
              <a href={`tel:${item.userPrimaryPhoneNumber}`}
                 className="hpTablesTd wrap-cell-text"
                 dangerouslySetInnerHTML={{
                   __html: highlightSearch(
                     item.userPrimaryPhoneNumber,
                     search
                   )
                 }}
              /> : "-"
          }
        </div>
      )
    },
    sortMethod: (a, b) =>
      a.userPrimaryPhoneNumber.localeCompare(b.userPrimaryPhoneNumber)
  }
]

export const usersColumns = (search, userType, routeType, onSelect, listType) => {
  const renderSwitch = (userType, item, search, routeType) => {
    switch (routeType) {
    case "adminUsers":
      return userUrl(routeType, item.userFullName, item.userId, search)
    case "users":
      return userUrl(routeType, item.userFullName, item.userId, search)
    default:
      return (
        <Link
          to=""
          onClick={event => {
            event.preventDefault()
            updateContact(event, item.userId)
          }}
        >
          <small
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.userFullName || "-", search)
            }}
          />
        </Link>
      )
    }
  }

  const status = []
  const additionalCols = []
  if(listType === "people"){
    status.push({
      Header: "User Status",
      id: "userStatus",
      className: "hpTablesTd",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {item && item.userRevisedStatus || ""}
          </div>
        )
      },
      sortMethod: (a, b) => a.userFirstName.localeCompare(b.userLastName)
    })
  }
  if(listType === "users" || listType === "adminUsers" || listType === "migratedusers"){
    additionalCols.push({
      Header: "Select",
      id: "selected",
      className: "multiExpTblVal",
      accessor: item => item,
      maxWidth: 150,
      Cell: row => {
        const item = row.value
        return (
          <input type="checkbox" value = {item._id} checked={item.selected || false} onChange={event => onSelect(event, row.index, true)}/>
        )
      }
    })
  }
  return [
    ...status,
    ...additionalCols,
    {
      Header: "Full Name",
      id: "userFullName",
      className: "hpTablesTd",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return userUrl(item.entityRelationshipType, `${item.userFirstName} ${item.userLastName}`, item.userId, search)
      },
      sortMethod: (a, b) => a.userFirstName.localeCompare(b.userLastName)
    },
    {
      Header: "Associated Entity",
      id: "entityName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 150,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd -striped wrap-cell-text">
            {item.entityRelationshipType === "Firm" ? (
              <small>{item.entityName}</small>
            ) : (
              entityUrl(
                item.entityRelationshipType,
                item.entityName,
                item.entityId,
                search
              )
            )}{" "}
          </div>
        )
      },
      sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
    },
    {
      Header: "Primary Email",
      id: "userPrimaryEmail",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 150,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(
                (item.userPrimaryEmail && item.userPrimaryEmail.toString()) ||
                "-",
                search
              )
            }}
          />
        )
      },
      sortMethod: (a, b) =>
        a.userPrimaryEmail !== undefined && b.userPrimaryEmail !== undefined
          ? a.phoneNumber.localeCompare(b.userPrimaryEmail)
          : ""
    },
    {
      Header: "Primary Phone",
      id: "userPrimaryPhoneNumber",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div>
            {
              item && item.userPrimaryPhoneNumber !== "-" ?
                <a
                  href={`tel:${item.userPrimaryPhoneNumber}`}
                  className="hpTablesTd wrap-cell-text"
                  dangerouslySetInnerHTML={{
                    __html: highlightSearch(
                      (item.userPrimaryPhoneNumber && item.userPrimaryPhoneNumber.toString()) || "-",
                      search
                    )
                  }}
                /> : "-"
            }
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.userPrimaryPhoneNumber !== undefined &&
          b.userPrimaryPhoneNumber !== undefined
          ? a.phoneNumber.localeCompare(b.userPrimaryPhoneNumber)
          : ""
    },
    {
      Header: "Primary Address",
      id: "userPrimaryAddressName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 250,
      Cell: row => {
        const item = row.value
        const url = item.userPrimaryAddressGoogleUrl ? item.userPrimaryAddressGoogleUrl : item.userPrimaryAddressGoogle ? `https://maps.google.com/?q=${item.userPrimaryAddressGoogle || ""}` :
          item.userPrimaryAddressLine1 ? `https://maps.google.com/?q=${item.userPrimaryAddressLine1} ${item.userPrimaryAddressCity || ""} ${item.userPrimaryAddressState || ""} ${item.userPrimaryAddressZipCode || ""}` : ""
        return (
          <div className="hpTablesTd wrap-cell-text">
            {
              url ?
                <a
                  href={url || ""} target="_blank"
                  dangerouslySetInnerHTML={{
                    __html: highlightSearch(`${item.userPrimaryAddressGoogle}` || "-", search)
                  }}
                /> :
                <span>-</span>
            }
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.userPrimaryAddressName.localeCompare(b.userPrimaryAddressName)
    }
  ]
}

export const tranColumns = (search, changeSelectedAction, user) => {
  const action = {
    id: "action",
    Header: "Action",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text tooltips">
          <div className="select is-small is-link">
            <select value="" onChange={(e) => changeSelectedAction(e.target.value, item)}>
              <option value="">Action</option>
              <option value="copy">Make a copy</option>
            </select>
          </div>
        </div>
      )
    },
    sortMethod: (a, b) => a.tranStatus.localeCompare(b.tranStatus)
  }

  const tranCols = [
    {
      Header: "Client Name",
      id: "tranClientName",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <Link
              to={`/clients-propects/${item.clientEntityId || ""}/entity` || ""}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.tranClientName || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.tranClientName.localeCompare(b.tranClientName)
    },
    {
      id: "activityDescription",
      Header: "Description",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return transactionUrl(item.tranType, item.tranSubType, item.activityDescription, item.tranId, search)
      },
      sortMethod: (a, b) =>
        a.activityDescription.localeCompare(b.activityDescription)
    },
    {
      id: "tranType",
      Header: "Type",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(
                item.tranType === "Others"
                  ? item.tranSubType
                  : `${item.tranType} / ${item.tranSubType}` || "-",
                search
              )
            }}
          />
        )
      },
      sortMethod: (a, b) => a.tranType.localeCompare(b.tranType)
    },
    {
      id: "tranParAmount",
      Header: "Principal Amount($)",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {numberWithCommas(item.tranParAmount)}
          </div>
        )
      },
      maxWidth: 150,
      sortMethod: (a, b) => a.tranParAmount - b.tranParAmount
    },
    {
      id: "tranAssigneeDetails.userFullName",
      Header: "Lead Manager",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {
              (item.tranAssigneeDetails &&
                item.tranAssigneeDetails.userFullName) ?
                <Link
                  to={`/admin-users/${(item.tranAssigneeDetails &&
                    item.tranAssigneeDetails.userId) ||
                  ""}/users` || ""}
                  dangerouslySetInnerHTML={{
                    __html: highlightSearch((item.tranAssigneeDetails &&
                      item.tranAssigneeDetails.userFullName) ||
                      "-", search)
                  }}
                /> : "-"
            }
          </div>
        )
      },
      sortMethod: (a, b) => a.leadManager.localeCompare(b.leadManager)
    },
    {
      id: "middleName",
      Header: "Coupon Type",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.middleName || "-", search)
            }}
          />
        )
      },
      sortMethod: (a, b) => a.middleName.localeCompare(b.middleName)
    },
    {
      id: "tranPrimarySector",
      Header: "Purpose/Sector",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <p
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.tranPrimarySector || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.tranPrimarySector.localeCompare(b.tranPrimarySector)
    },
    {
      id: "tranPricingDate",
      Header: "Expected Pricing Date",
      accessor: item => item,
      minWidth: 50,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {dateFormat(item.tranPricingDate || "")}
          </div>
        )
      },
      sortMethod: (a, b) => a.tranPricingDate.localeCompare(b.tranPricingDate)
    },
    {
      id: "tranExpectedAwardDate",
      Header: "Expected Closing Date",
      accessor: item => item,
      minWidth: 50,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {dateFormat(item.tranExpectedAwardDate || "")}
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.tranExpectedAwardDate.localeCompare(b.tranExpectedAwardDate)
    },
    {
      id: "tranStatus",
      Header: "Status",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.tranStatus || "-", search)
            }}
          />
        )
      },
      sortMethod: (a, b) => a.tranStatus.localeCompare(b.tranStatus)
    }
  ]

  if(user && user.userEntitlement === "global-edit"){
    tranCols.push(action)
  }
  return tranCols
}

export const marfpColumns = (search, changeSelectedAction, user) => [
  {
    Header: "Issuer Name",
    id: "tranClientName",
    className: "multiExpTblVal",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link
            to={`/clients-propects/${item.clientEntityId || ""}/entity` || ""}
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.tranClientName || "-", search)
            }}
          />
        </div>
      )
    },
    sortMethod: (a, b) => a.tranClientName.localeCompare(b.tranClientName)
  },
  {
    id: "activityDescription",
    Header: "Activity Description/Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return transactionUrl(item.tranType, item.tranSubType, item.activityDescription, item.tranId, search)
    },
    sortMethod: (a, b) =>
      a.activityDescription.localeCompare(b.activityDescription)
  },
  {
    id: "tranType",
    Header: "Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(`${item.tranType}` || "-", search)
          }}
        />
      )
    },
    sortMethod: (a, b) => a.tranType.localeCompare(b.tranType)
  },
  {
    id: "userFullName",
    Header: "Lead Advisor",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          <Link
            to={`/admin-users/${(item.tranAssigneeDetails &&
                item.tranAssigneeDetails.userId) ||
              ""}/users` || ""}
            dangerouslySetInnerHTML={{
              __html: highlightSearch((item.tranAssigneeDetails &&
                  item.tranAssigneeDetails.userFullName) ||
                  "-", search)
            }}
          />
        </div>
      )
    },
    sortMethod: (a, b) => a.leadManager.localeCompare(b.leadManager)
  },
  {
    id: "tranStatus",
    Header: "Status",
    accessor: item => item,
    minWidth: 100,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(item.tranStatus || "-", search)
          }}
        />
      )
    },
    sortMethod: (a, b) => a.tranStatus.localeCompare(b.tranStatus)
  },
  {
    id: "tranExpectedAwardDate",
    Header: "Due Date",
    accessor: item => item,
    minWidth: 50,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {dateFormat(item.tranExpectedAwardDate || "")}
        </div>
      )
    },
    sortMethod: (a, b) =>
      a.tranExpectedAwardDate.localeCompare(b.tranExpectedAwardDate)
  }
]

export const tasksColumns = (
  search,
  handleContextMenuClick,
  groupBy,
  renderTaskStatusSelect,
  loginDetails
) => [
  {
    Header: <i
      className="fa fa-envelope"
      aria-hidden="true"
    />,
    id: "taskUnreadStatus",
    minWidth: 25,
    accessor: e => e,
    Cell: e =>
      e.value.taskUnreadStatus === false ? (
        <span>
          <ContextMenuTrigger id={`${e.value.taskId  }conMen`} holdToDisplay={1000}>
            <span>
              <i
                className="multiExpTblVal-open fa fa-envelope-open"
                aria-hidden="true"
                title="Right click to mark as unread"
              />
            </span>
          </ContextMenuTrigger>
          <ContextMenu id={`${e.value.taskId  }conMen`}>
            <MenuItem onClick={
              () => handleContextMenuClick(e.value)}>
              {"Mark as UnRead"}
            </MenuItem>
          </ContextMenu>
        </span>
      ) : (
        <span>
          <ContextMenuTrigger id={`${e.value.taskId  }conMen2`} holdToDisplay={1000}>
            <i
              className="multiExpTblVal-close fa fa-envelope"
              aria-hidden="true"
              title="Right click to mark as read"
            />
          </ContextMenuTrigger>
          <ContextMenu id={`${e.value.taskId  }conMen2`}>
            <MenuItem data={{ foo: "bar" }} onClick={
              async () => handleContextMenuClick(e.value)}>
              {"Mark as Read"}
            </MenuItem>
          </ContextMenu>
        </span>
      )
  },
  {
    Header: "Client/Prospect",
    id: "tranClientName",
    className: "multiExpTblVal",
    accessor: e => e,
    sortMethod: (a, b) => a.tranClientName.localeCompare(b.tranClientName),
    Cell: e =>
      groupBy ||
          (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
          e.value.taskType === "Compliance" ? (
            <div className="hpTablesTd wrap-cell-text">
              <NavLink
                to={
                  e.value.taskNotes
                    ? `/cac/view-control?cid=${e.value.taskNotes}`
                    : "/cac/view-control"
                }
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(e.value.taskDescription || "notes", search)
                }}
              />
            </div>
          ) : (
            <span className="hpTablesTd wrap-cell-text">
              <NavLink
                to={`/clients-propects/${e.value.clientEntityId || ""}/entity`}
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(e.value.tranClientName || "all", search)
                }}
              />
            </span>
          )
        ) : (
          <div className="hpTablesTd wrap-cell-text tooltips">
            <NavLink
              to={`/clients-propects/${e.value.clientEntityId || ""}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(e.value.tranClientName || "something", search)
              }}
            />
          </div>
        )
  },
  {
    Header: "Transaction",
    id: "activityDescription",
    className: "multiExpTblVal",
    accessor: e => e,
    sortMethod: (a, b) =>
      a.sortTranValue && a.sortTranValue.localeCompare(b.sortTranValue),
    Cell: e =>
      groupBy ||
          (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
          e.value.tranSubType === "Compliance" ? (
            <div className="hpTablesTd wrap-cell-text tooltips">
              <NavLink
                to="/cac/view-control"
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(e.value.tranSubType || "-", search)
                }}
              />
            </div>
          ) :
            <div className="hpTablesTd wrap-cell-text tooltips">
              {
                transactionUrl(e.value.tranType, e.value.tranSubType, e.value.activityDescription, e.value.tranId, search)
              }
            </div>
        ) :
        <div className="hpTablesTd wrap-cell-text tooltips">
          {
            transactionUrl(e.value.tranType, e.value.tranSubType, e.value.activityDescription, e.value.tranId, search)
          }
        </div>
  },
  {
    Header: "Due Date",
    id: "taskEndDate",
    className: "multiExpTblVal",
    accessor: e => e.taskEndDate,
    Cell: e =>
      e.value ? (
        <span
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              moment(e.value).format("MM-DD-YYYY") || "-",
              search
            )
          }}
        />
      ) : (
        ""
      )
  },
  {
    Header: "Activity",
    id: "tranType",
    className: "multiExpTblVal",
    accessor: e => e.tranType,
    Cell: e => (
      <small
        className="hpTablesTd wrap-cell-text"
        dangerouslySetInnerHTML={{
          __html: highlightSearch(e.value || "-", search)
        }}
      />
    )
  },
  {
    Header: "To Do",
    id: "taskDescription",
    className: "multiExpTblVal",
    accessor: e => e,
    Cell: e =>
      groupBy ||
          (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
          e.value.tranSubType === "Compliance" ? (
            <div className="hpTablesTd wrap-cell-text">
              <NavLink
                to={
                  e.value.taskNotes
                    ? `/cac/view-control/view-detail?id=${e.value.taskNotes}`
                    : "/cac/view-control"
                }
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(
                    e.value.taskDescription || "-",
                    search
                  )
                }}
              />
            </div>
          ) : (
            <span className="hpTablesTd wrap-cell-text">
              <NavLink
                to={`/tasks/${e.value.taskId}`}
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(
                    e.value.taskDescription || "-",
                    search
                  )
                }}
              />
            </span>
          )
        ) : e.value.tranType === "BusinessDevelopment" ?(
          <span className="hpTablesTd wrap-cell-text">
            <NavLink
              to={`/bus-development?id=${e.value.taskId}`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(
                  e.value.taskDescription || "-",
                  search
                )
              }}
            />
          </span>
        ) : e.value.taskActivityContext ? (
          <div className="hpTablesTd wrap-cell-text">
            <div>
              {
                transactionUrl(e.value.tranType, e.value.tranSubType, e.value.taskDescription, e.value.tranId, search, `check-track?cid=${e.value.taskActivityContext}`)
              }
            </div>
          </div>
        ) : (
          <span className="hpTablesTd wrap-cell-text">
            <NavLink
              to={`/tasks/${e.value.taskId}`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(e.value.taskDescription || "-", search)
              }}
            />
          </span>
        )
  },
  {
    Header: "Assigned To",
    id: "taskAssigneeName",
    className: "multiExpTblVal",
    accessor: e => e,
    Cell: e => {
      const relationType = loginDetails && loginDetails.userEntities && loginDetails.userEntities.length && loginDetails.userEntities[0] && loginDetails.userEntities[0].relationshipToTenant
      return(
        <div className="hpTablesTd wrap-cell-text">
          {userUrl(relationType, e.value.taskAssigneeName, e.value.taskAssigneeId, search,e.value.taskAssigneeType)}
        </div>
      )
    }
  },
  {
    Header: "Activity Status",
    id: "taskStatus",
    className: "multiExpTblVal",
    accessor: e => e,
    Cell: e => (e.value.taskId ? renderTaskStatusSelect(e.value) : undefined)
  }
]

export const complianceColumns = (
  search,
  handleContextMenuClick,
  groupBy,
  renderTaskStatusSelect,
  loginDetails
) => [
  {
    Header: <i
      className="fa fa-envelope"
      aria-hidden="true"
    />,
    id: "taskUnreadStatus",
    minWidth: 25,
    accessor: e => e,
    Cell: e =>
      e.value.taskUnreadStatus === false ? (
        <span>
          <ContextMenuTrigger id={`${e.value.taskId  }conMen`} holdToDisplay={1000}>
            <span>
              <i
                className="multiExpTblVal-open fa fa-envelope-open"
                aria-hidden="true"
                title="Right click to mark as unread"
              />
            </span>
          </ContextMenuTrigger>
          <ContextMenu id={`${e.value.taskId  }conMen`}>
            <MenuItem onClick={
              () => handleContextMenuClick(e.value)}>
              {"Mark as UnRead"}
            </MenuItem>
          </ContextMenu>
        </span>
      ) : (
        <span>
          <ContextMenuTrigger id={`${e.value.taskId  }conMen2`} holdToDisplay={1000}>
            <i
              className="multiExpTblVal-close fa fa-envelope"
              aria-hidden="true"
              title="Right click to mark as read"
            />
          </ContextMenuTrigger>
          <ContextMenu id={`${e.value.taskId  }conMen2`}>
            <MenuItem data={{ foo: "bar" }} onClick={
              async () => handleContextMenuClick(e.value)}>
              {"Mark as Read"}
            </MenuItem>
          </ContextMenu>
        </span>
      )
  },
  {
    Header: "Task Category",
    id: "taskCategory",
    className: "multiExpTblVal",
    accessor: e => e,
    sortMethod: (a, b) => a.taskCategory.localeCompare(b.taskCategory),
    Cell: e =>
      e.value.taskType === "Compliance" ? (
        <div className="hpTablesTd wrap-cell-text">
          <NavLink
            to={
              e.value.taskNotes
                ? `/cac/view-control/view-detail?id=${e.value.taskNotes}`
                : "/cac/view-control"
            }
            dangerouslySetInnerHTML={{
              __html: highlightSearch(e.value.taskCategory || "notes", search)
            }}
          />
        </div>
      ) : (
        <span className="hpTablesTd wrap-cell-text">
          <NavLink
            to={`/tasks/${e.value.taskId}`}
            dangerouslySetInnerHTML={{
              __html: highlightSearch(e.value.taskCategory || "all", search)
            }}
          />
        </span>
      )
  },
  {
    Header: "To Do",
    id: "taskDescription",
    className: "multiExpTblVal",
    accessor: e => e,
    Cell: e =>
      groupBy ||
          (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
          e.value.taskType === "Compliance" ? (
            <div className="hpTablesTd wrap-cell-text">
              <NavLink
                to={
                  e.value.taskNotes
                    ? `/cac/view-control/view-detail?id=${e.value.taskNotes}`
                    : "/cac/view-control"
                }
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(
                    e.value.taskDescription || "-",
                    search
                  )
                }}
              />
            </div>
          ) : (
            <span className="hpTablesTd wrap-cell-text">
              <NavLink
                to={`/tasks/${e.value.taskId}`}
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(
                    e.value.taskDescription || "-",
                    search
                  )
                }}
              />
            </span>
          )
        ) : e.value.tranType === "BusinessDevelopment" ? (
          <span className="hpTablesTd wrap-cell-text">
            <NavLink
              to={`/bus-development?id=${e.value.taskId}`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(
                  e.value.taskDescription || "-",
                  search
                )
              }}
            />
          </span>
        ) : (
          <span className="hpTablesTd wrap-cell-text">
            <NavLink
              to={`/tasks/${e.value.taskId}`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(e.value.taskDescription || "-", search)
              }}
            />
          </span>
        )
  },
  {
    Header: "Due Date",
    id: "taskEndDate",
    className: "multiExpTblVal",
    accessor: e => e.taskEndDate,
    Cell: e =>
      e.value ? (
        <span
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              moment(e.value).format("MM-DD-YYYY") || "-",
              search
            )
          }}
        />
      ) : (
        ""
      )
  },
  {
    Header: "Assigned To",
    id: "taskAssigneeName",
    className: "multiExpTblVal",
    accessor: e => e,
    Cell: e => {
      const relationType = loginDetails && loginDetails.userEntities && loginDetails.userEntities.length && loginDetails.userEntities[0] && loginDetails.userEntities[0].relationshipToTenant
      return (
        <div className="hpTablesTd wrap-cell-text">
          {userUrl(relationType, e.value.taskAssigneeName, e.value.taskAssigneeId, search)}
        </div>
      )
    }
  },
  {
    Header: "Activity Status",
    id: "taskStatus",
    className: "multiExpTblVal",
    accessor: e => e,
    Cell: e => (e.value.taskId ? renderTaskStatusSelect(e.value) : undefined)
  }
]

export const migratedEntityColumns = (search, userType, onSelect) => [
  {
    Header: "Select",
    id: "selected",
    className: "multiExpTblVal",
    accessor: item => item,
    maxWidth: 150,
    Cell: row => {
      const item = row.value
      return (
        <input type="checkbox" value = {item._id} checked={item.selected || false} onChange={event => onSelect(event, row.index)}/>
      )
    }
  },
  {
    Header: "Type",
    id: "entityRelationshipType",
    className: "multiExpTblVal",
    accessor: item => item,
    maxWidth: 150,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityRelationshipType || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityRelationshipType.localeCompare(b.entityRelationshipType)
  },
  {
    id: "entityName",
    Header: "Entity Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd wrap-cell-text">
          {userType === "admin" ? (
            <Link
              to={`/admin-migratedentities/${item.firmId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.entityName || "-", search)
              }}
            />
          ) : (
            <Link
              to={`/migratedentities/${item.entityId}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.entityName || "-", search)
              }}
            />
          )}
        </div>
      )
    },
    maxWidth: 350,
    sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
  },
  {
    Header: "City",
    className: "multiExpTblVal",
    id: "entityPrimaryAddressCity",
    maxWidth: 200,
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityPrimaryAddressCity || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityPrimaryAddressCity.localeCompare(b.entityPrimaryAddressCity)
  },
  {
    Header: "State",
    className: "multiExpTblVal",
    id: "entityPrimaryAddressState",
    maxWidth: 200,
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div
          className="hpTablesTd wrap-cell-text"
          dangerouslySetInnerHTML={{
            __html: highlightSearch(
              item.entityPrimaryAddressState || "-",
              search
            )
          }}
        />
      )
    },
    sortMethod: (a, b) =>
      a.entityPrimaryAddressState.localeCompare(b.entityPrimaryAddressState)
  },
  {
    Header: "User Name",
    className: "hpTablesTd",
    id: "userFullName",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return userUrl(item.entityRelationshipType, item.userFullName, item.userId, search)
    },
    maxWidth: 300,
    sortMethod: (a, b) => a.userFullName.localeCompare(b.userFullName)
  },
  {
    Header: "Primary Contact Phone",
    className: "multiExpTblVal",
    id: "userPrimaryPhoneNumber",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div>
          {
            item && item.userPrimaryPhoneNumber !== "-" ?
              <a
                href={`tel:${item.userPrimaryPhoneNumber}`}
                className="hpTablesTd wrap-cell-text"
                dangerouslySetInnerHTML={{
                  __html: highlightSearch(
                    item.userPrimaryPhoneNumber || "-",
                    search
                  )
                }}
              /> : "-"
          }
        </div>
      )
    },
    sortMethod: (a, b) =>
      a.userPrimaryPhoneNumber.localeCompare(b.userPrimaryPhoneNumber)
  }
]
