import React from "react"
import Switch, { Case, Default } from "react-switch-case"
import NumberFormat from "react-number-format"
import moment from "moment"
import {
  TextLabelInput,
  ZipCodeNumberInput
} from "./../../../GlobalComponents/TextViewBox"

const UserContact = props => (
  <article
    className={
      props.expanded.contactdetail ? "accordion is-active" : "accordion"
    }
  >
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "contactdetail")
        }}
      >
        Contact Details
      </p>
      {props.expanded.contactdetail ? (
        <i className="fas fa-chevron-up" />
      ) : (
        <i className="fas fa-chevron-down" />
      )}
    </div>
    {props.expanded.contactdetail && (
      <div className="accordion-body">
        <div className="accordion-content">
          <div className="columns">
            <TextLabelInput
              error={
                (props.errorUserDetail && props.errorUserDetail.userFirmName) ||
                ""
              }
              name="userFirmName"
              type="text"
              label="Firm Name"
              value={props.userDetails.userFirmName || ""}
              onChange={props.onChangeUserDetail}
              disabled
            />
          </div>
          <hr />
          <div className="columns">
            <TextLabelInput
              required
              error={
                (props.errorUserDetail &&
                  props.errorUserDetail.userFirstName) ||
                ""
              }
              type="text"
              label="First Name"
              name="userFirstName"
              value={props.userDetails.userFirstName || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
            <TextLabelInput
              error={
                (props.errorUserDetail &&
                  props.errorUserDetail.userMiddleName) ||
                ""
              }
              type="text"
              label="Middle Name"
              name="userMiddleName"
              value={props.userDetails.userMiddleName || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
            <TextLabelInput
              required
              error={
                (props.errorUserDetail && props.errorUserDetail.userLastName) ||
                ""
              }
              type="text"
              label="Last Name"
              name="userLastName"
              placeholder="Last Name"
              value={props.userDetails.userLastName || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
          </div>
          <hr />
          <div className="columns">
            <div className="column is-third">
              <p className="multiExpLbl ">
                Email <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>                  
              </p>
              {props.userDetails.userEmails.map((item, idx) => (
                <div key={2 * idx}>
                  <div className="field">
                    <div className="control d-flex">
                      <TextLabelInput
                        type="email"
                        name="emailId"
                        style={{ width: "100%" }}
                        placeholder="e.g. alexsmith@gmail.com"
                        value={item.emailId || ""}
                        onChange={event => {
                          props.onChangeAddMore(event, "userEmails", idx)
                        }}
                        disabled={
                          !props.canEdit ||
                          (props.primaryEmails.isPrimary &&
                            props.primaryEmails.emailId === item.emailId &&
                            true)
                        }
                      />
                      {idx > 0 && props.canEdit ? (
                        <span
                          className="has-text-link fa-delete deleteadressaliases"
                          onClick={() => {
                            props.deleteAliases("userEmails", idx)
                          }}
                        >
                          <i className="far fa-trash-alt" />
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                    {props.primaryEmails.isPrimary &&
                      props.primaryEmails.emailId === item.emailId && (
                        <p className="emmaTablesTd">
                          {" "}
                          Is Primary
                          <input
                            type="checkbox"
                            name="emailPrimary"
                            disabled
                            value={item.emailPrimary}
                            checked={item.emailPrimary ? item.emailPrimary : ""}
                            onChange={event => {
                              props.onChangeAddMore(event, "userEmails", idx)
                            }}
                          />
                        </p>
                      )}
                  </div>
                  <span>
                    {props.errorUserDetail &&
                      props.errorUserDetail.userEmails[idx].emailId && (
                        <small className="text-error">
                          {props.errorUserDetail.userEmails[idx].emailId}
                        </small>
                      )}
                  </span>
                </div>
              ))}
              {props.canEdit && (
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={event => {
                      props.addMore(event, "userEmails")
                    }}
                  >
                    Add More
                  </button>
                  <button
                    className="button is-light is-small"
                    onClick={event => {
                      props.resetAliases(event, "userEmails")
                    }}
                  >
                    Cancel
                  </button>
                </div>
              )}
            </div>
            <div className="column">
              <p className="multiExpLbl ">Phone</p>
              {props.userDetails.userPhone.map((item, idx) => (
                <div key={3 * idx}>
                  <div className="is-grouped-left d-flex">
                    <div className="control d-flex">
                      <ZipCodeNumberInput
                        format="+1 (###) ###-####"
                        mask="_"
                        className="input is-small is-link"
                        name="phoneNumber"
                        placeholder="Numbers only"
                        value={item.phoneNumber || ""}
                        onChange={event => {
                          props.onChangeAddMore(event, "userPhone", idx)
                        }}
                        disabled={!props.canEdit}
                      />
                    </div>
                    <div className="control d-flex">
                      <ZipCodeNumberInput
                        format="####"
                        className="input is-small is-link"
                        name="extension"
                        placeholder="Ext"
                        size="5"
                        value={item.extension || ""}
                        onChange={event => {
                          props.onChangeAddMore(event, "userPhone", idx)
                        }}
                        disabled={!props.canEdit}
                      />
                      {idx > 0 && props.canEdit ? (
                        <span
                          className="has-text-link fa-delete deleteadressaliases"
                          onClick={() => {
                            props.deleteAliases("userPhone", idx)
                          }}
                        >
                          <i className="far fa-trash-alt" />
                        </span>
                      ) : (
                        ""
                      )}
                      {props.errorUserDetail &&
                        props.errorUserDetail.userPhone[idx].extension && (
                          <small className="text-error">
                            {props.errorUserDetail.userPhone[idx].extension}
                          </small>
                        )}
                    </div>
                  </div>

                  <p className="emmaTablesTd" style={{ marginBottom: "13px" }}>
                    {" "}
                    Is Primary
                    <input
                      type="checkbox"
                      name="phonePrimary"
                      value={item.phonePrimary ? item.phonePrimary : ""}
                      checked={item.phonePrimary}
                      onChange={event => {
                        props.onChangeAddMore(event, "userPhone", idx)
                      }}
                      disabled={!props.canEdit}
                    />
                  </p>
                </div>
              ))}
              {props.canEdit && (
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={event => {
                      props.addMore(event, "userPhone")
                    }}
                  >
                    Add More
                  </button>
                  <button
                    className="button is-light is-small"
                    onClick={event => {
                      props.resetAliases(event, "userPhone")
                    }}
                  >
                    Cancel
                  </button>
                </div>
              )}
            </div>
            <div className="column">
              <p className="multiExpLbl ">Fax</p>
              {props.userDetails.userFax.map((item, idx) => (
                <div key={4 * idx}>
                  <div className="field">
                    <div className="control d-flex">
                      <ZipCodeNumberInput
                        format="+1 (###) ###-####"
                        mask="_"
                        className="input is-small is-link"
                        name="faxNumber"
                        size="10"
                        value={item.faxNumber ? item.faxNumber : ""}
                        onChange={event => {
                          props.onChangeAddMore(event, "userFax", idx)
                        }}
                        disabled={!props.canEdit}
                      />
                      {idx > 0 && props.canEdit ? (
                        <span
                          className="has-text-link fa-delete deleteadressaliases"
                          onClick={() => {
                            props.deleteAliases("userFax", idx)
                          }}
                        >
                          <i className="far fa-trash-alt" />
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                    <p className="emmaTablesTd">
                      {" "}
                      Is Primary
                      <input
                        type="checkbox"
                        name="faxPrimary"
                        value={item.faxPrimary}
                        checked={item.faxPrimary ? item.faxPrimary : ""}
                        onChange={event => {
                          props.onChangeAddMore(event, "userFax", idx)
                        }}
                        disabled={!props.canEdit}
                      />
                    </p>
                  </div>
                  <span>
                    {props.errorUserDetail &&
                      props.errorUserDetail.userFax[idx].faxNumber && (
                        <small className="text-error">
                          {props.errorUserDetail.userFax[idx].faxNumber}
                        </small>
                      )}
                  </span>
                </div>
              ))}
              {props.canEdit && (
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={event => {
                      props.addMore(event, "userFax")
                    }}
                  >
                    Add More
                  </button>
                  <button
                    className="button is-light is-small"
                    onClick={event => {
                      props.resetAliases(event, "userFax")
                    }}
                  >
                    Cancel
                  </button>
                </div>
              )}
            </div>
          </div>

          <hr />

          <div className="columns">
            <TextLabelInput
              error={
                (props.errorUserDetail &&
                  props.errorUserDetail.userEmployeeID) ||
                ""
              }
              type="text"
              label="Employee ID"
              name="userEmployeeID"
              value={props.userDetails.userEmployeeID || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
            <TextLabelInput
              error={
                (props.errorUserDetail && props.errorUserDetail.userJobTitle) ||
                ""
              }
              type="text"
              label="Job Title"
              name="userJobTitle"
              value={props.userDetails.userJobTitle || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
            <TextLabelInput
              error={
                (props.errorUserDetail &&
                  props.errorUserDetail.userManagerEmail) ||
                ""
              }
              type="email"
              label="Manager's Email"
              name="userManagerEmail"
              placeholder="e.g. alexsmith@gmail.com"
              value={props.userDetails.userManagerEmail || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
          </div>

          <hr />
          <div className="columns">
            <Switch condition={props.userType}>
              <Case value="users">
                <TextLabelInput
                  error={
                    (props.errorUserDetail &&
                      props.errorUserDetail.userJoiningDate) ||
                    ""
                  }
                  type="date"
                  label="Joining Date"
                  name="userJoiningDate"
                  placeholder="e.g. alexsmith@gmail.com"
                  value={
                    props.userDetails.userJoiningDate
                      ? moment(props.userDetails.userJoiningDate).format(
                          "YYYY-MM-DD"
                        )
                      : ""
                  }
                  onChange={props.onChangeUserDetail}
                  disabled={!props.canEdit}
                />
                <TextLabelInput
                  error={
                    (props.errorUserDetail &&
                      props.errorUserDetail.userExitDate) ||
                    ""
                  }
                  type="date"
                  label="Exit Date"
                  name="userExitDate"
                  placeholder="e.g. alexsmith@gmail.com"
                  value={
                    props.userDetails.userExitDate
                      ? moment(props.userDetails.userExitDate).format(
                          "YYYY-MM-DD"
                        )
                      : ""
                  }
                  onChange={props.onChangeUserDetail}
                  disabled={!props.canEdit}
                />
              </Case>
              <Default>
                <TextLabelInput
                  error={
                    (props.errorUserDetail &&
                      props.errorUserDetail.userDepartment) ||
                    ""
                  }
                  type="text"
                  label="Department"
                  name="userDepartment"
                  value={props.userDetails.userDepartment || ""}
                  onChange={props.onChangeUserDetail}
                  disabled={!props.canEdit}
                />
                <TextLabelInput
                  error={
                    (props.errorUserDetail &&
                      props.errorUserDetail.userEmployeeType) ||
                    ""
                  }
                  type="text"
                  label="Employee Type"
                  name="userEmployeeType"
                  value={props.userDetails.userEmployeeType || ""}
                  onChange={props.onChangeUserDetail}
                  disabled={!props.canEdit}
                />
              </Default>
            </Switch>
            <TextLabelInput
              error={
                (props.errorUserDetail &&
                  props.errorUserDetail.userCostCenter) ||
                ""
              }
              type="text"
              label="Cost Center"
              name="userCostCenter"
              value={props.userDetails.userCostCenter || ""}
              onChange={props.onChangeUserDetail}
              disabled={!props.canEdit}
            />
          </div>
        </div>
      </div>
    )}
  </article>
)

export default UserContact
