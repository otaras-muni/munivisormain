import React from "react"
import Fuse from "fuse.js"
import styled from "styled-components"
import UsersList from "./UsersList"

const searchOptions = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    {
      name: "firmType",
      weight: 0.8
    },
    {
      name: "firmName",
      weight: 0.9,
    },
    {
      name: "state",
      weight: 0.89,
    },
    {
      name: "city",
      weight: 0.98
    },
    {
      name: "addressName",
      weight: 0.86
    },
    {
      name: "primaryContact",
      weight: 0.6,
    }
  ]
}

class GroupByResultSet extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showResult: props.showResultSet || false
    }

    this.toggleResultSet = this.toggleResultSet.bind(this)
  }

  toggleResultSet() {
    this.setState({
      showResult: !this.state.showResult
    })
  }

  fuse = (e, search) => {
    const fuse = new Fuse(e, searchOptions)
    const res = fuse.search(search)
    return res
  }

  render() {
    const { title, items, pageSize, pageSizeOptions, search,onPageSizeChange } = this.props
    const { showResult } = this.state
    return (
      <div>
        <div className="columns">
          <div className="column overflow-auto">
            <div className="accordions">
              <div className={`${showResult ? "accordion is-active" : "accordion"}`}>
                {title !== "" && title !== null ?
                  <div className="accordion-header" onClick={this.toggleResultSet}>
                    <span>{title}</span>
                    <div>({items.length} found) &nbsp;<i className={`${showResult ? "fas fa-chevron-down" : "fas fa-chevron-up"}`} />
                    </div>
                  </div>
                  : ""
                }
                {
                  showResult && (
                    <UsersList
                      userslist={items}
                      userType="users"
                      pageSize={pageSize}
                      pageSizeOptions={pageSizeOptions}
                      search = {search}
                      onPageSizeChange={onPageSizeChange}
                    />
                  )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default GroupByResultSet
