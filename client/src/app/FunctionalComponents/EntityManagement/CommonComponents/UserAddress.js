import React from "react"
import { FormGroup } from "react-bootstrap"

import { DropdownList } from "react-widgets"

import NumberFormat from "react-number-format"
import SearchAddress from "./../../GoogleAddressForm/GoogleAddressFormComponent"
import {
  TextLabelInput,
  DropDownInput,
  ZipCodeNumberInput
} from "./../../../GlobalComponents/TextViewBox"

const UserAddress = props => (
  <div
    className="accordion-content"
    style={{ borderBottom: "1px solid #8393a8" }}
    key={props.idx}
  >
    <div className="tile is-ancestor">
      <div className="tile is-12 is-vertical is-parent">
        {props.canEdit && (
          <SearchAddress
            idx={props.idx}
            getAddressDetails={props.getAddressDetails}
          />
        )}
        <div className="columns">
          <TextLabelInput
            error={
              (props.errorUserDetail &&
                props.errorUserDetail.userAddresses[props.idx].addressName) ||
              ""
            }
            name="addressName"
            type="text"
            label="Address Name"
            value={props.userAddress.addressName || ""}
            onChange={event => {
              props.onChangeUserAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
          <div className="column is-two-third">
            <div
              className="column is-one-fourth"
              style={{ display: "inline-block" }}
            >
              <p className="emmaTablesTd">
                <input
                  type="checkbox"
                  name="isPrimary"
                  value={props.userAddress.isPrimary}
                  checked={props.userAddress.isPrimary}
                  onChange={event => {
                    props.onChangeAddressType(event, props.idx)
                  }}
                  disabled={!props.canEdit}
                />{" "}
                Use Primary Office Address?
              </p>
              {props.isPrimaryAddress && (
                <small className="text-error">{props.isPrimaryAddress}</small>
              )}
            </div>
            <div
              className="column is-one-fourth"
              style={{ padding: "0.75rem", float: "right" }}
            >
              {props.idx > 0 ? (
                <span
                  className="has-text-link fa-delete"
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    props.deleteAddress(event, "userAddresses", props.idx)
                  }}
                >
                  <i className="far fa-trash-alt" />
                </span>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>

        <div className="columns">
          <TextLabelInput
            required
            error={
              (props.errorUserDetail &&
                props.errorUserDetail.userAddresses[props.idx].addressLine1) ||
              ""
            }
            type="text"
            label="Address Line 1"
            name="addressLine1"
            value={props.userAddress.addressLine1}
            onChange={event => {
              props.onChangeUserAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
          <TextLabelInput
            error={
              (props.errorUserDetail &&
                props.errorUserDetail.userAddresses[props.idx].addressLine2) ||
              ""
            }
            type="text"
            label="Address Line 2"
            name="addressLine2"
            value={props.userAddress.addressLine2}
            onChange={event => {
              props.onChangeUserAddress(event, props.idx)
            }}
            disabled={!props.canEdit}
          />
        </div>

        <div className="columns">
          <DropDownInput
            filter
            required
            value={props.userAddress.country}
            data={props.countryResult[1] || [""]}
            message="select Country"
            textField="country"
            style={{ fontSize: 12 }}
            valueField="value"
            key="name"
            label="Country"
            busy={props.busy}
            busySpinner={<span className="fas fa-sync fa-spin is-link" />}
            onChange={val => {
              const event = {
                target: {
                  name: "country",
                  value: val
                }
              }
              props.onChangeUserAddress(event, props.idx, ["state", "city"])
            }}
            disabled={!props.canEdit}
            disableValue={props.userAddress.country}
            error={
              props.errorUserDetail &&
              props.errorUserDetail.userAddresses[props.idx].country
            }
          />
          <DropDownInput
            filter
            required
            value={props.userAddress.state}
            data={
              props.userAddress.country &&
              props.countryResult[2][props.userAddress.country]
                ? [...props.countryResult[2][props.userAddress.country]]
                : []
            }
            message="Select State"
            textField="state"
            style={{ fontSize: 12 }}
            valueField="value"
            label="State"
            busy={props.busy}
            busySpinner={<span className="fas fa-sync fa-spin is-link" />}
            onChange={val => {
              const event = {
                target: {
                  name: "state",
                  value: val
                }
              }
              props.onChangeUserAddress(event, props.idx, ["city"])
            }}
            disabled={!props.canEdit}
            disableValue={props.userAddress.state}
            error={
              props.errorUserDetail &&
              props.errorUserDetail.userAddresses[props.idx].state
            }
          />
          <DropDownInput
            filter
            required
            value={props.userAddress.city}
            data={
              props.userAddress.state &&
              props.userAddress.country &&
              (props.countryResult[3][props.userAddress.country] &&
                props.countryResult[3][props.userAddress.country][
                  props.userAddress.state
                ])
                ? [
                    ...props.countryResult[3][props.userAddress.country][
                      props.userAddress.state
                    ]
                  ]
                : []
            }
            message="Select City"
            textField="city"
            style={{ fontSize: 12 }}
            valueField="value"
            label="City"
            busy={props.busy}
            busySpinner={<span className="fas fa-sync fa-spin is-link" />}
            onChange={val => {
              const event = {
                target: {
                  name: "city",
                  value: val
                }
              }
              props.onChangeUserAddress(event, props.idx, [])
            }}
            disabled={!props.canEdit}
            disableValue={props.userAddress.city}
            error={
              props.errorUserDetail &&
              props.errorUserDetail.userAddresses[props.idx].city
            }
          />
          <div className="column">
            <p className="multiExpLbl">
              Zip Code<span className="has-text-danger">*</span>
            </p>
            <div className="field is-grouped-left">
              <div className="control d-flex">
                <ZipCodeNumberInput
                  name="zip1"
                  size="5"
                  type="text"
                  format="#####"
                  placeholder="Zip Code 1"
                  label=""
                  disabled={!props.canEdit}
                  error={
                    (props.errorUserDetail &&
                      props.errorUserDetail.userAddresses[props.idx].zipCode
                        .zip1) ||
                    ""
                  }
                  value={props.userAddress.zipCode.zip1 || ""}
                  onChange={event => {
                    props.onChangeUserAddress(event, props.idx)
                  }}
                />
              </div>
              <div className="control d-flex">
                <ZipCodeNumberInput
                  name="zip2"
                  size="4"
                  type="text"
                  format="####"
                  placeholder="Zip Code 2"
                  label=""
                  disabled={!props.canEdit}
                  error={
                    (props.errorUserDetail &&
                      props.errorUserDetail.userAddresses[props.idx].zipCode
                        .zip2) ||
                    ""
                  }
                  value={props.userAddress.zipCode.zip2 || ""}
                  onChange={event => {
                    props.onChangeUserAddress(event, props.idx)
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default UserAddress
