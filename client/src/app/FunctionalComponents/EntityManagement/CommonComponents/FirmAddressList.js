import React from "react"
import uuidv4 from "uuid"

const FirmAddressList = props => (
  <article
    className={props.listAddressToggle ? "accordion is-active" : "accordion"}
  >
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "addresslist")
        }}
      >
        Manage Available Addresses
      </p>
      {props.listAddressToggle ? (
        <i className="fas fa-chevron-up" />
      ) : (
        <i className="fas fa-chevron-down" />
      )}
    </div>

    <div className="accordion-body">
      {props.listAddressToggle && (
        <div className="accordion-content">
          <div style={{ overflowX: "auto" }}>
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                <tr>
                  <th>
                    <p className="multiExpLbl ">Address Name</p>
                  </th>
                  <th>
                    <p className="multiExpLbl ">Is Primary?</p>
                  </th>
                  {/*   <th>
                  <p className="multiExpLbl ">Is Headquarter?</p>
                </th>
                <th>
                  <p className="multiExpLbl ">Inactive</p>
                </th> */}
                  <th>
                    <p className="multiExpLbl ">
                      {!props.canEdit ? "View" : "Update"}
                    </p>
                  </th>
                </tr>
              </thead>
              <tbody>
                {props.addressList
                  ? props.addressList.map((address, idx) => (
                      <tr key={idx}>
                        <td className="multiExpTblVal">
                          {address.addressName}
                        </td>
                        <td className="multiExpTblVal">
                          <p className="emmaTablesTd">
                            <input
                              type="checkbox"
                              value={address.isPrimary}
                              checked={address.isPrimary}
                              onChange={event => {
                                props.checkAddressStatus(
                                  event,
                                  address._id,
                                  "isPrimary"
                                )
                              }}
                              disabled={!props.canEdit}
                            />
                          </p>
                        </td>
                        {/* <td className="multiExpTblVal">
                        <p className="emmaTablesTd">
                          <input type="checkbox"
                            value = {address.isActive}
                            checked={address.isActive}
                            onChange = {
                              (event)=>{
                                props.checkAddressStatus(event, address._id,"isActive")
                              }
                            }
                            disabled={!props.canEdit}
                          />
                        </p>
                      </td> */}
                        <td className="multiExpTblVal">
                          <a href="#">
                            <span className="has-text-link">
                              <i
                                className="fas fa-pencil-alt is-small"
                                onClick={event => {
                                  event.preventDefault()
                                  props.updateAddress(event, address._id)
                                }}
                              />
                            </span>
                          </a>
                        </td>
                      </tr>
                    ))
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
      )}
    </div>
  </article>
)

export default FirmAddressList
