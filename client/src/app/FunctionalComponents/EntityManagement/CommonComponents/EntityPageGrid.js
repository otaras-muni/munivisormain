import React from "react"
import ReactTable from "react-table"
import "react-table/react-table.css"
import { toast } from "react-toastify"
import Loader from "../../../GlobalComponents/Loader"
import CONST from "../../../../globalutilities/consts"

import {
  clientProspectColumns,
  thirdPartyColumns,
  usersColumns,
  tranColumns,
  marfpColumns,
  tasksColumns,
  complianceColumns,
  migratedEntityColumns
} from "./EntityColumns"
import {
  getDashboardData,
  getPicklistValues,
  updateTask,
  setTableCache,
  getTableCache
} from "GlobalUtils/helpers"
import {CloneTransaction} from "../../../GlobalComponents/CloneTransaction"
import {
  checkSupervisorControls,
  createTranEmailAlert,
  makeCopyOfTransaction
} from "../../../StateManagement/actions/CreateTransaction"
import ProjectAgeGrid from "../../DashAlt/ProjectAgeGrid"

export const Modal = ({ children, closeModal, modalState, title }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => {}}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{title}</p>
          <button className="delete" onClick={closeModal} />
        </header>
        <section className="modal-card-body">
          <div className="content">{children}</div>
        </section>
      </div>
    </div>
  )
}

class EntityPageGrid extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
  }

  initialState() {
    return {
      entityList: [],
      pageSizeOptions: [5, 10, 20, 25, 50],
      pageSize: 25,
      viewList: true,
      copyModalState: false,
      loading: false,
      total: 0,
      pages: 0,
      changedPage: 0,
      copyFrom: {},
      errors: {},
      svControls: {},
      makeCopy: {
        issueName: "",
        projectName: "",
        tabs: [
          "summary",
          "details",
          "participants",
          // "check-track"
        ],
        emailMessage: ""
      }
    }
  }

  async componentWillMount() {
    const svControls = await checkSupervisorControls()
    this.setState({
      gridTimeStamp: Date.now(),
      svControls
    })
  }

  componentWillReceiveProps(newProps) {
    this.setState({ ...newProps, changedPage: newProps.page })
  }

  onPageSizeChange = item => {
    this.setState({
      pageSize: item
    })
  };

  onPageChanged = changedPage => {
    this.setState({ changedPage })
  };

  onGridOperations = async (state, instance) => {
    if (Date.now() - this.state.gridTimeStamp > 500) {
      this.setState(
        prevState => ({
          ...prevState,
          loading: true,
          gridTimeStamp: Date.now()
        }),
        async () => {
          const valueReturned = await this.props.getFilteredData(
            this.state.changedPage,
            state.sorted,
            this.state.pageSize,
            this.props.groupTitle
          )
          if (valueReturned) {
            this.setState(prevState => ({
              ...prevState,
              entityList: valueReturned.data,
              total: valueReturned.total,
              pages: valueReturned.pages,
              page: this.state.changedPage,
              search: name === "entitySearch" ? value : "",
              loading: false
            }))
          }
        }
      )
    }
  };

  onAgeGridPageChanged = (eventActive) => {
    if(eventActive) {
      this.setState({ changedPage: eventActive.changedPage },() => this.onAgeGridOperations(eventActive))
    }
  };

  onAgeGridOperations = async (eventActive) => {
    if (eventActive) {
      this.setState(
        prevState => ({
          ...prevState,
          loading: true,
          gridTimeStamp: Date.now()
        }),
        async () => {
          const valueReturned = await this.props.getFilteredData(
            this.state.changedPage,
            eventActive.sorted,
            this.state.pageSize,
            this.props.groupTitle
          )
          if (valueReturned) {
            this.setState(prevState => ({
              ...prevState,
              entityList: valueReturned.data,
              total: valueReturned.total,
              pages: valueReturned.pages,
              page: this.state.changedPage,
              search: "",
              loading: false
            }))
          }
        }
      )
    }
  };

  toggleModal = marketRoles => {
    this.setState(prev => {
      const newState = !prev.modalState

      return { modalState: newState, marketRoles }
    })
  };

  renderTableBox = (search, entityList, pageSize, pages, page, user) => (
    <React.Fragment>
      <div className="accordion-body">
        {
          <div className="accordion-content">
            {this.renderTable(
              search,
              entityList,
              pageSize,
              this.props.listType,
              pages,
              page,
              user
            )}
            {this.props.listType === "third-party"
              ? this.renderModal()
              : null}
          </div>
        }
      </div>
    </React.Fragment>
  );

  renderModal = () => (
    <Modal
      closeModal={this.toggleModal}
      modalState={this.state.modalState}
      title="Market Roles"
    >
      <p>{this.state.marketRoles}</p>
    </Modal>
  );

  renderTable = (search, entityList, pageSize, listType, pages, page, user) => {

    if(listType === "project"){
      return (
        <div style={{padding: 10}}>
          <ProjectAgeGrid
            marfps={this.props.groupTitle === "marfps-data"}
            user={user}
            search={search}
            changeSelectedAction={this.changeSelectedAction}
            onAgeGridPageChanged={this.onAgeGridPageChanged}
            onAgeGridOperations={this.onAgeGridOperations}
            list={entityList}
            pages={pages}
            page={page}
            pageSize={pageSize}
          />
        </div>
      )
    }

    return (
      <ReactTable
        columns={this.getColumns(listType, search, user)}
        manual
        data={entityList}
        showPaginationBottom
        defaultPageSize={25}
        pages={pages}
        page={page}
        className="-striped -highlight is-bordered"
        pageSize={pageSize}
        showPageJump
        loading={this.state.loading}
        minRows={2}
        showPageSizeOptions={false}
        onFetchData={(state, instance) => {
          this.onGridOperations(state, instance)
        }}
        onPageChange={changedPage => {
          this.onPageChanged(changedPage)
        }}
      />
    )
  };

  onSelect = (e, index, isUser) => {
    const { entityList } = this.state
    let selectedUsers = []
    entityList[index].selected = e.target.checked
    if(isUser) {
      selectedUsers = entityList.filter(item => item.selected).map(item => item.userId) || []
      if (selectedUsers && selectedUsers.length) {
        console.log(selectedUsers)
        this.props.getSelectedUsers(selectedUsers)
      }else {
        this.props.getSelectedUsers([])
      }
    }
    this.setState({
      entityList
    })
  };

  changeSelectedAction = (value, item) => {
    const {makeCopy} = this.state
    let copyFrom = {}
    if(value === "copy"){
      this.cloneToggleModal()
      copyFrom = item
      console.log(copyFrom)
      makeCopy.issueName = item.tranIssueName
      makeCopy.projectName = item.tranProjectName
    }
    this.setState({
      copyFrom,
      makeCopy
    })
  }

  getColumns(listType, search, user) {
    switch (listType) {
    case "client-prospect":
      return clientProspectColumns(
        search,
        this.props.userType,
      )
    case "migratedentities":
      return migratedEntityColumns(
        search,
        this.props.userType,
        this.onSelect
      )
    case "users":
    case "migratedusers":
    case "people":
      return usersColumns(
        search,
        this.props.userType,
        this.props.routeType,
        this.onSelect,
        listType
      )
    case "third-party":
      return thirdPartyColumns(
        search,
        this.props.userType,
        this.toggleModal,
      )
    case "project":
      if (this.props.groupTitle !== "MA Request For Proposals") {
        return tranColumns(search, this.changeSelectedAction, user)
      } else if (this.props.groupTitle === "MA Request For Proposals") {
        return marfpColumns(search, this.changeSelectedAction, user)
      }
    case "task":
      if (this.props.routeType === "tasks") {
        return tasksColumns(
          search,
          this.props.handleContextMenuClick,
          this.props.groupBy,
          this.renderTaskStatusSelect,
          this.props.loginDetails
        )
      }
      return complianceColumns(
        search,
        this.props.handleContextMenuClick,
        this.props.groupBy,
        this.renderTaskStatusSelect,
        this.props.loginDetails
      )

    }
  }

  statusChangeCallback = (err, oldValue) => {
    this.setState({ laoding: false })
    if (err) {
      toast("Error in updating task status. Please try again.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    } else {
      toast("Task Status Changed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
    }
  };

  changeTaskStatus = (type, idx, e) => {
    const { value } = e.target
    const { entityList } = this.state
    this.setState(prevState => {
      const transactions = entityList
      const oldValue = transactions.filter(tran => tran.taskId === idx)[0]
        .taskStatus
      transactions.filter(tran => tran.taskId === idx)[0].taskStatus = value
      const payload = { "taskDetails.taskStatus": value }
      updateTask(idx, payload, oldValue, this.statusChangeCallback)
      return { transactions, laoding: true }
    })
  };

  renderTaskStatusSelect = task => {
    const { entityList } = this.state
    let type
    let idx
    let status
    const transactionsStatus = entityList
    status = transactionsStatus.filter(tran => tran.taskId === task.taskId)[0]
      .taskStatus
    const taskDisabled = task.complianceTask
    if (taskDisabled) {
      return (
        <p>{status === "complete" ? "Closed" : status}</p>
      )
    } else {
      return (
        <div className="select is-small is-link">
          <select
            value={status}
            onChange={this.changeTaskStatus.bind(this, "", task.taskId)}
            disabled={status === "Closed" || task.complianceTask}
          >
            {this.state.eventStatus.map((e, i) => (
              <option key={e + i} value={e}>
                {e}
              </option>
            ))}
          </select>
        </div>
      )
    }
  };

  cloneToggleModal = () => {
    this.setState(prev => {
      const newState = !prev.copyModalState
      return { copyModalState: newState }
    })
  }

  onSave = () => {
    const {copyFrom, makeCopy} = this.state
    const {tranSubType, tranType} = copyFrom
    const payload = {
      tranId: (copyFrom && copyFrom.tranId) || "",
      tranSubType: (copyFrom && copyFrom.tranSubType) || "",
      tranType: (copyFrom && copyFrom.tranType) || "",
      ...makeCopy,
    }
    console.log("Copy Transaction ====>", copyFrom)
    if(!makeCopy.projectName){
      return toast("Project name is required.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
    const type =
      tranSubType === "BOND ISSUE" ? "deals" :
        tranSubType === "BANK LOAN" ? "loan" :
          tranType === "DERIVATIVE" ? "derivative" :
            tranType === "RFP" ? "rfp" :
              tranType === "OTHERS" ? "others" :
                tranType === "BUSINESSDEVELOPMENT" ? "bus-development" :
                  tranType === "MA-RFP" ? "marfp" : "loan"

    this.setState({
      loading: true,
    }, async () => {
      const res = await makeCopyOfTransaction(payload)
      if(res && res.data && res.data.done){
        const {newTransaction} = res.data
        toast("Transaction Created Successfully.", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.sendEmailAlertCreateTransaction(newTransaction, type, tranType)
      } else {
        toast(res.data.error || "Error in creating transaction. Please try again.", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.WARNING
        })
      }
      this.setState({
        loading: false,
        makeCopy: {
          issueName: "",
          projectName: "",
          emailMessage: "",
          tabs: [
            "summary",
            "details",
            "participants",
            // "check-track"
          ]
        },
        copyFrom: {}
      },async () =>{
        this.onGridOperations({sorted: null})
        this.cloneToggleModal()
      })
    })
  }

  sendEmailAlertCreateTransaction = async (newTransaction, type, tranType) => {
    const {svControls, makeCopy} = this.state
    const tranId = (newTransaction && newTransaction._id) || ""
    const emailPayload = {
      ...svControls,
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        ...this.state.email,
        url:
          tranType === "BUSINESSDEVELOPMENT"
            ? `bus-development/${tranId}`
            : `${type}/${tranId}/summary`,
        message: (makeCopy && makeCopy.emailMessage) || "",
        subject: `Create Transaction - ${makeCopy.issueName || makeCopy.projectName}`
      }
    }
    await createTranEmailAlert(emailPayload)
  }

  onChange = (event) => {
    const {makeCopy} = this.state
    let stateObject = {}
    const {name, value, checked, type} = event.target
    if(name === "tabs" && makeCopy && makeCopy.tabs){
      if(checked && makeCopy.tabs.indexOf(value) === -1){
        makeCopy.tabs.push(value)
      }
      if(!checked && makeCopy.tabs.indexOf(value) !== -1){
        const findIndex = makeCopy.tabs.indexOf(value)
        makeCopy.tabs.splice(findIndex,1)
      }
      if(checked && value === "check-track" && makeCopy.tabs.indexOf("participants") === -1){
        makeCopy.tabs.push("participants")
      }
      if(makeCopy.tabs.indexOf("participants") === -1 && makeCopy.tabs.indexOf("check-track") !== -1){
        const findIndex = makeCopy.tabs.indexOf("check-track")
        makeCopy.tabs.splice(findIndex,1)
      }
      stateObject = makeCopy
    }else {
      stateObject = {
        ...makeCopy,
        [name]: type === "checkbox" ? checked : value,
      }
    }
    this.setState({
      makeCopy: stateObject
    })
  }

  render() {
    const loading = () => <Loader />
    let { pageSize, entityList } = this.state
    const { search, pages, page, auth } = this.props
    const user = (auth && auth.loginDetails && auth.loginDetails.userEntities && auth.loginDetails.userEntities.length && auth.loginDetails.userEntities[0]) || {}
    if (this.props.listType === "project") {
      entityList = this.props.entityList
    }

    return (
      <React.Fragment>
        {this.state.loading && this.props.listType !== "project"
          ? loading()
          : null}
        {this.renderTableBox(search, entityList, pageSize, pages, page, user)}
        <CloneTransaction
          closeModal={this.cloneToggleModal}
          state={this.state}
          onChange={this.onChange}
          onSave={this.onSave}
        />
      </React.Fragment>
    )
  }
}

export default EntityPageGrid

// export default EntityPageGrid;

// export default connect(
//     mapDispatchToProps
// )(EntityPageFilter)
