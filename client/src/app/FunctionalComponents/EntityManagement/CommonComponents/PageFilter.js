import React from "react"
import { Multiselect, DropdownList } from "react-widgets"

const PageFilter = props => (
  <section className="accordions">
    <article className={props.expanded.pagefilter ? "accordion is-active" : "accordion"}>
      <div className="accordion-header toggle">
        <p
          onClick={event => {
            props.toggleButton(event, "pagefilter")
          }}
        >
          Page Filters as
        </p>
        <span>
          <i className={props.expanded.pagefilter ? "fas fa-chevron-up" : "fas fa-chevron-down"} />
        </span>
      </div>
      {props.expanded.pagefilter && <div className="accordion-body">
        <div className="accordion-content">
          <div className="columns">
            <div className="column">
              <div className="field has-addons">
              <div className="control w-100">
                <div className="select is-link is-fullwidth is-small third-party-select-hack" style={{height:'1.8rem'}}>
                  <DropdownList
                    filter
                    value={props.savedSearches}
                    data={props.savedSearchesList ? props.savedSearchesList : []}
                    valueField='value'
                    textField='label'
                    defaultValue={0}
                    onChange={(val) => {
                      const event = {
                        target: {
                          name: "savedSearches",
                          value: val
                        }
                      }
                      props.onChangeFilterPage(event)
                    }} />
                </div>
                </div>
                <div className="control">
                  <button className="button is-dark is-fullwidth is-small"
                    onClick={
                      props.handleDeletePref
                    }
                  >Delete</button>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="field has-addons">
                <div className="control">
                  <input className="input  is-link is-fullwidth is-small"
                    type="text"
                    name="searchname"
                    value={props.searchname}
                    placeholder="Name your search"
                    onChange={props.onChangeFilterPage}
                  />
                </div>
                <div className="control">
                  <button
                    className="button is-link is-small"
                    disabled={props.isSaving}
                    onClick={props.handleSavePref}
                  >
                    {
                      props.isSaving
                        ? "Saving..."
                        : "Go"
                    }
                  </button>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="select  is-link is-fullwidth is-small">
                <select
                  onChange={e => props.onPageSizeChange(Number(e.target.value))}
                  value={props.pageSize}
                >
                  {props.pageSizeOptions.map((option, i) => (
                    <option key={i} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="column">
              <p className="control has-icons-left">
                <input className="input  is-link is-fullwidth is-small" type="text"
                  placeholder="search client/prospect"
                  name="entitySearch"
                  value={props.filteredValue.entitySearch}
                  // onChange = {props.onChangeFilterPage}
                  onChange={e => props.onChangeFilter(e)}
                />

                <span className="icon is-small is-left has-background-black has-text-white">
                  <i className="fas fa-search" />
                </span>
              </p>
            </div>
            {props.listType === "client-prospect" && <div className="column">
              <div className="select  is-link is-fullwidth is-small">
                <select
                  name="entityType"
                  onChange={e => props.onChangeFilter(e)}
                  value={props.filteredValue.entityType}
                >
                  <option disabled="disabled" value="" hidden="hidden">Filter by...</option>
                  <option value="Client">Clients only</option>
                  <option value="Prospect">Prospects only</option>
                  <option value="ClentProspect">Clients &amp; Prospects</option>
                </select>
              </div>
            </div>}
            {props.listType === "third-party" && <div className="column">
              <div className="select  is-link is-fullwidth is-small third-party-select-hack">
                <DropdownList
                  value={props.filteredValue.entityType === "" ? "Select Market Role" : props.filteredValue.entityType}
                  data={props.marketRoleList ? props.marketRoleList : []}
                  textField='label'
                  defaultValue="Select Market Role"
                  onChange={(val) => {
                    const event = {
                      target: {
                        name: "entityType",
                        value: val
                      }
                    }
                    props.onChangeFilter(event)
                  }} />
              </div>
            </div>}
            <div className="column">
              <div className="select is-link is-fullwidth is-small tp-select-state">
                <Multiselect
                  filter
                  placeholder="Select State"
                  value={props.filteredValue.enityState}
                  name="enityState"
                  data={props.filteredValue.enityState && props.countryResult.length > 0 && props.countryResult[2]["United States"] ? [...props.countryResult[2]["United States"]] : []}
                  message="select State"
                  textField='state'
                  onChange={(val) => {
                    const event = {
                      target: {
                        name: "enityState",
                        value: val
                      }
                    }
                    props.onChangeFilter(event)
                  }} />
              </div>
            </div>
          </div>
        </div>
      </div>}
    </article>
    <br />
  </section>

)
export default PageFilter