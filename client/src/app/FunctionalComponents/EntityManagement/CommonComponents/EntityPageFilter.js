import React from "react"
import { connect } from "react-redux"
import { Multiselect, DropdownList } from "react-widgets"
import "react-table/react-table.css"
import { toast } from "react-toastify"
import {Link, withRouter} from "react-router-dom"
import {DebounceInput} from 'react-debounce-input'
import { getPicklistValues, saveEntityDetails, getUniqValueFromArray} from "GlobalUtils/helpers"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import {
  saveMasterListSearchPrefs,
  deleteMasterListSearchPrefs,
  fetchMasterListSearchPrefs,
  getEntityList,
  getUserList,
  changeUsersStatus
} from "AppState/actions/AdminManagement/admTrnActions"
import CONST from "GlobalUtils/consts"
import Loader from "../../../GlobalComponents/Loader"
import EntityPageGrid from "./EntityPageGrid"
import {Modal} from "../../TaskManagement/components/Modal"
import {getURLsForTenant} from "../../../StateManagement/actions/CreateTransaction"
import {SelectLabelInput} from "../../../GlobalComponents/TextViewBox"
import swal from "sweetalert";

export const ConvertToEntityModal = ({ closeModal, state, onChange, changeIssuerFlags, changeLegalRole, changeMarketRole, onSave }) => (
  <Modal
    closeModal={closeModal}
    modalState={state.entityModalState}
    title="Convert Migrated Data"
    saveModal={onSave}
    styleBody={{ minHeight: 300 }}
  >
    {
      state.selectedEnt && state.selectedEnt.length ?
        <div>
          <div className="columns">
            <div className="column">
              <div className="control field">
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Client"
                    checked={state.clientOrProspect === "Client"}
                    onChange={onChange}/>
                  &nbsp;Client
                </label>
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Prospect"
                    checked={state.clientOrProspect === "Prospect"}
                    onChange={onChange}/>
                  &nbsp;Prospect
                </label>
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Third Party"
                    checked={state.clientOrProspect === "Third Party"}
                    onChange={onChange}/>
                  &nbsp;Third Party
                </label>
                {state.error && state.error.clientOrProspect ?
                  <small className="has-text-danger">
                    &nbsp; Please select entity relationship type
                  </small>
                  : undefined
                }
              </div>
            </div>
          </div>
          <div className="columns">
            {
              state.clientOrProspect && state.clientOrProspect !== "Third Party" ?
                <div className="column is-one-third">
                  <p className="multiExpLblBlk">
                    Entity Type
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                  </p>
                  <div className="select is-small is-link">
                    <select value={state.entityType} name="entityType" onChange={onChange}>
                      <option value="">Select Entity Type</option>
                      {state.entityTypes.map(t => (
                        <option key={t} value={t}>
                          {t}
                        </option>
                      ))}
                    </select>
                    {state.error && state.error.entityType
                      ? <p className="has-text-danger">
                        Entity type should not be empty
                      </p>
                      : undefined
                    }
                  </div>
                </div>
                : null
            }
            {state.clientOrProspect !== "Third Party" && ["Governmental Entity / Issuer", "501c3 - Obligor"].includes(state.entityType)
              ? <div className="column">
                <p className="multiExpLblBlk">Issuer Type</p>
                <Multiselect
                  data={state.issuerFlagsList}
                  placeholder=" Select issuer types"
                  caseSensitive={false}
                  minLength={1}
                  value={state.issuerFlags}
                  filter="contains"
                  onChange={changeIssuerFlags}/>
                {state.error && state.error.issuerFlags ?
                  <p className="has-text-danger">
                    {state.error.issuerFlags}
                  </p> : undefined}
              </div>
              : undefined
            }
          </div>
          {state.clientOrProspect === "Third Party" ?
            <div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">Market Role</p>
                  <Multiselect
                    data={state.marketRolesList}
                    placeholder=" Select market role"
                    caseSensitive={false}
                    minLength={1}
                    value={state.marketRole}
                    filter="contains"
                    onChange={changeMarketRole}
                  />
                  {state.error && state.error.marketRole ?
                    <p className="has-text-danger">
                      {state.error.marketRole}
                    </p>
                    : undefined
                  }
                </div>
              </div>
              <div className="columns">
                {
                  state.marketRole.includes("Legal") ?
                    <div className="column">
                      <p className="multiExpLblBlk">Legal Type</p>
                      <Multiselect
                        data={state.legalTypes}
                        placeholder=" Select legal types"
                        caseSensitive={false}
                        minLength={1}
                        value={state.legalRole}
                        filter="contains"
                        onChange={changeLegalRole}
                      />
                      {state.error && state.error.legalRole ?
                        <p className="has-text-danger">
                          {state.error.legalRole}
                        </p>
                        : undefined
                      }
                    </div>
                    : undefined
                }
              </div>
            </div>
            : null
          }
        </div>
        : <div className="subtitle is-grouped-center"> Please Select at list one migrated entity  </div>
    }
  </Modal>
)

export const ConfirmationModal = ({ message, closeModal, modalState, onConfirmed }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" onClick={onConfirmed}>
              Yes
            </button>
            <button className="button is-light" onClick={closeModal}>
              No
            </button>
          </div>
        </footer>
      </div>
    </div>
  )
}

class EntityPageFilter extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
  }

  initialState() {
    return {
      expanded: false,
      loadingPicklists: true,
      entityModalState: false,
      clientOrProspect: "",
      error: {},
      marketRolesList: [],
      marketRole: [],
      legalTypes: [],
      legalRole: [],
      entityTypes: [],
      issuerFlags: [],
      issuerFlagsList: [],
      selectedEnt: [],
      countryResult: [],
      entityList: [],
      marketRoleList: [],
      pageSizeOptions: [5, 10, 20, 25, 50],
      pageSize: 25,
      filteredValue: {
        entityType: "",
        userType: "active",
        marketRole: [],
        entityState: [],
        entitySearch: "",
        defaultSearch: false
      },
      search: "",
      searchName: "",
      entityId: "",
      labelName: "",
      isSaving: false,
      savedSearches: "",
      savedSearchesList: [],
      savedSearchesData: [],
      viewList: true,
      loading: true,
      total: 0,
      pages: 0,
      modalState: false,
      page: {
        global: 0
      },
      jsonSheets: [],
      startXlDownload: false,
      userStateChange: "",
      selectedUsers: [],
      confirmAlert: CONST.confirmAlert
    };
  }

  async componentWillMount() {
    const { userEntities } = this.props.auth
    const { listType } = this.props
    let pickResult = []
    if(listType === "migratedentities"){
      pickResult = await getPicklistValues(["LKUPPARTICIPANTTYPE", "LKUPLEGALTYPE", "LKUPENTITYTYPE", "LKUPISSUERFLAG"])
    }
    console.log("result : ", pickResult)
    const participantTypesList = pickResult[0] && pickResult[0][1] ? pickResult[0][1] : []
    const legalTypes = pickResult[1] && pickResult[1][1] ? pickResult[1][1] : []
    const entityTypes = pickResult[2] && pickResult[2][1] ? pickResult[2][1] : []
    const issuerFlagsList = pickResult[3] && pickResult[3][1] ? pickResult[3][1] : []

    const marketRolesList = participantTypesList.filter(e => !legalTypes.includes(e))
    marketRolesList.unshift("Legal")

    if (listType !== "people") {
      await this.setNonPeopleData()
    }

    let entityList = {}
    let result = {}
    result = await this.getFilteredData(0)
    if (result && result.data.length > 0) {
      entityList = await result.data
    }
    this.setState({
      loading: false,
      marketRolesList,
      legalTypes,
      entityTypes,
      issuerFlagsList,
      gridTimeStamp: Date.now(),
      page: 0,
      entityList: result.data,
      total: result.total,
      pages: result.pages,
      expanded: false
    })
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.activeTab && nextProps.activeTab && this.props.activeTab !== nextProps.activeTab){
      this.setState({
        loading: true
      }, async () => {
        if (this.props.listType !== "people") {
          await this.setNonPeopleData()
        }
        let entityList = []
        const result = await this.getFilteredData(0)
        if (result && result.data.length > 0) {
          entityList = await result.data
        }

        this.setState({
          loading: false,
          entityList,
          total: (result && result.total) || 0,
          pages: (result && result.pages) || 0,
          expanded: false
        })
      })
    }
  }

  setNonPeopleData = async () => {
    let prefResult = {}
    let marketRoleList = []
    const filteredDefaultValue = {}
    const filteredValue = {}

    prefResult = fetchMasterListSearchPrefs(this.props.searchPref)
    prefResult = await prefResult
    const receivedDefault = await this.setSearchPref(
      prefResult,
      filteredDefaultValue,
      filteredValue
    )
    if (this.props.listType === "third-party") {
      const picklistArray = getPicklistValues([
        "LKUPMARKETROLE",
        // "LKUPPARTICIPANTTYPE"
      ])
      const [ participantType] = await picklistArray
      marketRoleList = ["Select All", ...participantType[1]]
    }

    this.setState({
      // countryResult,
      savedSearchesList: [
        ...(prefResult.data.length > 0
          ? prefResult.data.map(item => ({
            label: item.searchName,
            value: item._id
          }))
          : [])
      ],
      savedSearches: receivedDefault.filteredDefaultValue
        ? receivedDefault.filteredDefaultValue.searchName
        : "",
      searchName: receivedDefault.filteredDefaultValue
        ? receivedDefault.filteredDefaultValue.searchName
        : "",
      entityId: this.props.entityId,
      savedSearchesData: prefResult.data.length > 0 ? prefResult.data : [],
      loadingPicklists: false,
      filteredValue: receivedDefault.filteredValue,
      pageSize: receivedDefault.filteredValue.pageSizePref || 25,
      marketRoleList
    })
  };

  setSearchPref = async (prefResult, filteredDefaultValue, filteredValue) => {
    const defaultSearchValue = this.setSelectedDefaultValue(prefResult)
    filteredDefaultValue = defaultSearchValue.filteredDefaultValue
    filteredValue = defaultSearchValue.filteredValue
    prefResult.data.unshift(this.searchAllItem())
    return { prefResult, filteredDefaultValue, filteredValue }
  };

  setSelectedDefaultValue(prefResult) {
    let filteredValue = {}
    const filteredDefaultValue = prefResult.data.find(
      pref => JSON.parse(pref.searchPreference).defaultSearch === true
    )
    if (!filteredDefaultValue) {
      filteredValue = this.initialState().filteredValue
    } else {
      filteredValue = JSON.parse(filteredDefaultValue.searchPreference)
    }
    return { filteredDefaultValue, filteredValue }
  }

  getFilteredData = async (page, sorted, pageSize, backPagination) => {
    this.setState({ loading: true, page: {global: page}  })
    let result = {}
    const listType = this.props.listType
    const payload = this.constructPayload(listType, page, sorted, pageSize, backPagination)

    if (listType === "users" || listType === "migratedusers") {
      result = await getUserList(payload)
    } else if (listType === "people" && this.props.entityId !== "") {
      result = await getUserList(payload)
    } else if (listType === "third-party" || listType === "client-prospect" || listType === "migratedentities") {
      result = await getEntityList(payload)
    }

    const data = await result.data
    if (data) {
      const decoupledValue = this.decoupleResult(backPagination === "isExcel" ? data.pipeLineQueryResults : data.pipeLineQueryResults[0], backPagination)
      if(backPagination !== "isExcel") {
        this.setState({
          loading: false,
          entityList: decoupledValue.data || [],
          total: decoupledValue.total,
          pages: decoupledValue.pages,
        })
      } else {
        this.setState({ loading: false })
      }
      return decoupledValue
    }
    return {
      total: 0,
      pages: 0,
      data: []
    }
  };

  constructPayload = (listType, page, sorted, pageSize, backPagination) => {
    const entityTypes = []
    let marketRoles = []
    const { filteredValue } = this.state
    if(this.props.activeTab === "Migrated" || this.props.activeTab === "Undefined Users"){
      entityTypes.push("Undefined")
    } else if (
      filteredValue.entityType === "ClientProspect" ||
      (filteredValue.entityType === "" && listType === "client-prospect")
    ) {
      entityTypes.push("Client")
      entityTypes.push("Prospect")
    } else if (filteredValue.entityType === "Client") {
      entityTypes.push("Client")
    } else if (filteredValue.entityType === "Prospect") {
      entityTypes.push("Prospect")
    } else if (listType === "third-party") {
      entityTypes.push("Third Party")
      marketRoles =
        filteredValue.marketRole.indexOf("Select All") > -1
          ? []
          : filteredValue.marketRole
    } else if (
      listType === "users" ||
      (listType === "people" && filteredValue.marketRole)
    ) {
      entityTypes.push("Client")
      entityTypes.push("Prospect")
      entityTypes.push("Third Party")
      marketRoles = filteredValue.marketRole
    }

    let sortedFields = {}
    if (sorted && sorted[0]) {
      sortedFields[sorted[0].id] = sorted[0].desc ? -1 : 1
      this.setState({ sortedFields })
    } else {
      sortedFields = this.state.sortedFields
      if (sortedFields === undefined) {
        sortedFields = { entityName: 1 }
      }
    }

    if (marketRoles && (marketRoles.length === 1) & (marketRoles[0] === "")) {
      marketRoles = []
    }

    const pagination = {
      serverPerformPagination: backPagination === "isExcel" ? false : true,
      currentPage: page || 0,
      size: pageSize || this.state.pageSize,
      sortFields: sortedFields
    }

    let filters = {
      entityTypes: this.props.nav1 === "migratedentities" ? ["Undefined"] : entityTypes,
      userContactTypes: [],
      entityMarketRoleFlags: marketRoles,
      entityIssuerFlags: [],
      entityPrimaryAddressStates:
        filteredValue.entityState.indexOf("Select All") > -1
          ? []
          : filteredValue.entityState,
      freeTextSearchTerm: filteredValue.entitySearch
    }

    if (listType === "users" || listType === "people" || listType === "migratedusers") {
      filters = {
        userContactTypes: [],
        entityMarketRoleFlags: marketRoles,
        entityIssuerFlags: [],
        userPrimaryAddressStates:
          filteredValue.entityState.indexOf("Select All") > -1
            ? []
            : filteredValue.entityState,
        freeTextSearchTerm: filteredValue.entitySearch,
        entityId: this.props.entityId,
        activeFlag: listType === "people" ? "" : this.state.filteredValue.userType,
        entityTypes: listType === "migratedusers" || this.props.nav1 === "migratedentities" ? ["Undefined"] : ["Client", "Prospect", "Third Party"],
      }
    }

    const returnValue = {
      filters,
      pagination
    }

    return returnValue
  };

  decoupleResult = (result, backPagination) => ({
    total: backPagination === "isExcel" ? 0 : (result && result.metadata.length > 0 ? result.metadata[0].total : 0),
    pages: backPagination === "isExcel" ? 0 : (result && result.metadata.length > 0 ? result.metadata[0].pages : 0),
    data: backPagination === "isExcel" ? result : (result ? result.data : [])
  });

  handleGetPref = async savedSearches => {
    const { savedSearchesData, entityId } = this.state
    let { filteredValue } = this.state
    const { userEntities } = this.props.auth
    if (savedSearches !== "" && savedSearches.value !== "") {
      const filteredData = savedSearchesData.find(
        item => item._id === savedSearches.value
      )
      const { searchPreference } = filteredData
      filteredValue = JSON.parse(searchPreference)
    } else {
      filteredValue = {
        entityType: [],
        entityState: [],
        entitySearch: "",
        searchName: ""
      }
    }
    let result = {}
    this.setState(
      {
        filteredValue,
        entityList: [],
        total: 0,
        pages: 0,
        searchName:
          savedSearches.value !== "" && savedSearches.value !== 0
            ? savedSearches.label
            : "",
        loading: true,
        savedSearches,
        pageSize: filteredValue.pageSizePref || 25
      },
      async () => {
        result = await this.getFilteredData(
          0,
          null,
          filteredValue.pageSizePref
        )
        this.setState({
          entityList: result.data || [],
          total: result.total,
          pages: result.pages,
          searchName:
            savedSearches.value !== "" && savedSearches.value !== 0
              ? savedSearches.label
              : "",
          loading: false
        })
      }
    )
  };

  handleSavePref = async confirmed => {
    let {
      filteredValue,
      searchName,
      savedSearchesList,
      savedSearches,
      savedSearchesData
    } = this.state

    if (searchName !== "") {
      const defaultSearch = savedSearchesData.find(
        data => JSON.parse(data.searchPreference).defaultSearch === true
      )
      if (
        filteredValue.defaultSearch === true &&
        defaultSearch &&
        defaultSearch.searchName !== searchName
      ) {
        if (defaultSearch && !confirmed) {
          this.setState({
            modalState: true,
            modalMessage: "Do you want to change default search to this?"
          })
          return
        }
        if (defaultSearch && confirmed) {
          this.toggleModal()
          const defaultSavedSearchPreference = JSON.parse(
            defaultSearch.searchPreference
          )
          defaultSavedSearchPreference.defaultSearch = false
          savedSearchesData = await this.saveSearchPrefAndRestoreValues(
            defaultSearch.searchName,
            defaultSavedSearchPreference,
            savedSearchesList,
            savedSearchesData,
            false
          )
        }
      }
      this.setState({
        isSaving: true
      })
      savedSearchesData = await this.saveSearchPrefAndRestoreValues(
        searchName,
        filteredValue,
        savedSearchesList,
        savedSearchesData,
        true
      )
    }
  };

  handleDeletePref = async () => {
    let { savedSearches, entityId } = this.state
    let { savedSearchesList, savedSearchesData } = this.state
    const { userEntities } = this.props.auth
    if (
      savedSearches !== "" &&
      savedSearches.value !== "" &&
      savedSearches.value !== 0
    ) {
      if (!savedSearches.value && typeof(savedSearches) === "string") {
        savedSearches = savedSearchesList.find(
          item => item.label === savedSearches
        )
      }
      const result = await deleteMasterListSearchPrefs(savedSearches.value)
      if (result.status === 200) {
        savedSearchesList = savedSearchesList.filter(
          item => item.value !== savedSearches.value
        )
        savedSearchesData = savedSearchesData.filter(
          item => item._id !== savedSearches.value
        )
        this.setState(
          {
            savedSearchesList,
            savedSearches: "",
            savedSearchesData,
            filteredValue: this.initialState().filteredValue,
            searchName: ""
          },
          async () => {
            const result = await this.getFilteredData(0)
            this.setState({
              entityList: result.data || [],
              total: result.total,
              pages: result.pages,
              loading: false
            })
          }
        )
        toast("Successfully Deleted", {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        })
      } else
        toast("Something went wrong!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
    }
  };

  saveSearchPref = async (searchName, filteredValue) =>
    await saveMasterListSearchPrefs(
      this.props.searchPref,
      searchName,
      filteredValue
    );

  saveSearchPrefAndRestoreValues = async (
    searchName,
    filteredValue,
    savedSearchesList,
    savedSearchesData,
    needAlert
  ) => {
    const savePrefResult = await this.saveSearchPref(searchName, filteredValue)
    if (savePrefResult.status === 200) {
      if (!savePrefResult.data.data.nModified) {
        savedSearchesList.push({
          label: searchName,
          value: savePrefResult.data.data.upserted[0]._id
        })
        savedSearchesData.push({
          _id: savePrefResult.data.data.upserted[0]._id,
          searchName,
          searchPreference: JSON.stringify(filteredValue)
        })
        needAlert &&
          toast("Preference saved successfully!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
      } else
        needAlert &&
          toast("Preference updated successfully!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
      savedSearchesData = savedSearchesData.map(item => {
        if (item.searchName === searchName) {
          item.searchPreference = JSON.stringify(filteredValue)
        }
        this.setState({
          savedSearchesList,
          savedSearches: {
            label: searchName,
            value: item._id
          },
          savedSearchesData,
          searchName
        })
        return item
      })
    } else
      toast("Something went wrong!", {
        autoClose: 2000,
        type: toast.TYPE.ERROR
      })``
    this.setState({
      isSaving: false
    })
    return savedSearchesData
  };

  searchAllItem() {
    return {
      _id: 0,
      searchName: "Search All",
      searchPreference: JSON.stringify(this.initialState().filteredValue)
    }
  }

  toggleAccordion = e => {
    const { expanded } = this.state
    this.setState({
      expanded: !expanded
    })
  };

  onSearchPrefChange = async event => {
    const { name, value } = event.target
    const { filteredValue, savedSearchesData } = this.state

    const findValue = value.label || value
    const isExists = savedSearchesData.find(
      search => search.searchName === findValue
    )

    if (isExists && Object.keys(isExists).length) {
      const { searchPreference } = isExists
      filteredValue.defaultSearch = JSON.parse(searchPreference).defaultSearch
    } else {
      filteredValue.defaultSearch = false
    }

    if (name === "searchName" || name === "savedSearches") {
      this.setState({
        [name]: value
      })
    } else if (name === "defaultSearch") {
      filteredValue[name] = event.target.checked
      this.setState({
        filteredValue
      })
    } else {
      filteredValue[name] = value
      this.setState({
        filteredValue
      })
    }
    if (name === "savedSearches") {
      this.handleGetPref(value)
    }
  };

  onChangeFilter = async event => {
    const { filteredValue } = this.state
    const { name, value } = event.target

    if (typeof value === "string" || typeof value === "number") {
      filteredValue[name] = value
    } else if (Array.isArray(value)) {
      filteredValue[name] = value.indexOf("Select All") === 0 ? value.slice(1) : value.indexOf("Select All") > 0 ? [] : value
    }

    filteredValue.defaultSearch = !value
    this.setState({
      filteredValue
    })
    if (name === "entitySearch" && this.searchTimeout !== undefined) {
      clearTimeout(this.searchTimeout)
    }
    if (name === "entitySearch") {
      this.searchTimeout = setTimeout(
        () => this.setStateOnChangeFilter(filteredValue, name, value),
        800
      )
    } else {
      this.setStateOnChangeFilter(filteredValue, name, value)
    }
  };

  setStateOnChangeFilter = (filteredValue, name, value) => {
    this.setState(
      prevState => ({
        ...prevState,
        loading: true,
        filteredValue,
        searchName: "",
        savedSearches: "",
        pageSize: filteredValue.pageSizePref || 25
      }),
      async () => {
        const valueReturned = await this.getFilteredData(
          0,
          null,
          filteredValue.pageSizePref
        )
        const stateResult = valueReturned
        this.setState({
          filteredValue,
          entityList: stateResult.data || [],
          total: stateResult.total,
          pages: stateResult.pages,
          search: name === "entitySearch" ? value : "",
          loading: false
        })
      }
    )
  };

  toggleModal = () => {
    this.setState(prev => {
      const newState = !prev.modalState

      return { modalState: newState }
    })
  };

  renderFilter = () => (
    <section className="accordions">
      <article
        className={this.state.expanded ? "accordion is-active" : "accordion"}
      >
        {this.props.listType === "people"
          ? this.renderPeopleFilter()
          : this.renderAccordion()}
      </article>
    </section>
  );

  renderPeopleFilter = () => (
    <div className="columns">
      <div className="column">{this.renderSearchInput()}</div>
    </div>
  );

  renderAccordion = () => (
    <div>
      {this.renderAccordionHeader()}{" "}
      <div>{this.state.expanded && this.renderAccordionBody()}</div>{" "}
    </div>
  );

  renderAccordionBody = () => (
    <div className="accordion-body" style={{ padding: 20 }}>
      <div className="accordion-content">
        <div className="columns">
          <div className="column">{this.renderSearchInput()}</div>
          {
            this.props.listType === "client-prospect" ?
              <div className="column">
                {this.renderFilterByForClientProspectAndUsers()}
              </div>
              : (this.props.listType === "users" || this.props.listType === "migratedusers") ?
              <div className="column"> {this.renderFilterByForUsers()}</div>
              : null
          }

         {/* <div className="column">
            {
              this.props.listType === "client-prospect" ? this.renderFilterByForClientProspectAndUsers() :
                (this.props.listType === "users" || this.props.listType === "migratedusers")? this.renderFilterByForUsers() : null
            }
          </div> */}
          {(this.props.listType === "third-party" || /* this.props.listType === "migratedusers" || */
            this.props.nav1 === "migratedentities" /* this.props.listType === "users" || */) && (
            <div className="column">{this.renderSelectMarketRole()}</div>
          )}
          {/* <div className="column">{this.renderSelectState()}</div> */}
        </div>
        <div className="columns">
          <div className="column">{this.renderSelectSavedSearches()}</div>
          <div className="column">{this.renderSaveSearchElements()}</div>
        </div>
      </div>
    </div>
  );

  renderAccordionHeader = () => (
    <div className="accordion-header toggle" 
    onClick={event => {
      this.toggleAccordion(event)
    }}>
      <p>
        Filter {this.props.title ? this.props.title : "Page"} as
      </p>
      <span>
        <i
          className={
            this.state.expanded ? "fas fa-chevron-up" : "fas fa-chevron-down"
          }
        />
      </span>
    </div>
  );

  renderFilterByForClientProspectAndUsers = () => {
    const types = [
      {name: "Clients & Prospects only", value: "ClientProspect"},
      {name: "Clients only", value: "Client"},
      {name: "Prospects only", value: "Prospect"}
    ]
    const migratedTypes = [
      {name: "Undefined", value: "Undefined"}
    ]
    return (
      <span>
        <p className="multiExpLbl is-capitalized">Filter By</p>
        <div className="select  is-link is-fullwidth is-small">
          <select
            name="entityType"
            onChange={e => this.onChangeFilter(e)}
            value={this.state.filteredValue.entityType}
          >
            <option disabled="disabled" value="" hidden="hidden">
            Filter by...
            </option>
            {
              this.props.activeTab === "Migrated" ?
                migratedTypes.map(item => <option key={Math.random()} value={item.value}> {item.name} Only </option>) :
                types.map(item => <option key={Math.random()} value={item.value}> {item.name} </option>)
            }
            {this.state.marketRoleList.length > 0 &&
          this.state.marketRoleList.map(item => (
            <option key={Math.random()} value={item}>
              {item} Only
            </option>
          ))}
          </select>
        </div>
      </span>
    )
  }

  renderFilterByForUsers = () => {
    const types = [
      {name: "All Users", value: "all"},
      {name: "Active only", value: "active"},
      {name: "Inactive only", value: "inactive"}
    ]
    return (
      <span>
        <p className="multiExpLbl is-capitalized">Filter By</p>
        <div className="select  is-link is-fullwidth is-small">
          <select
            name="userType"
            onChange={e => this.onChangeFilter(e)}
            value={this.state.filteredValue.userType}
          >
            <option disabled="disabled" value="" hidden="hidden">Filter by...</option>
            {
              types.map(item => <option key={Math.random()} value={item.value}> {item.name} </option>)
            }
          </select>
        </div>
      </span>
    )
  }

  renderSelectMarketRole = () => (
    <span>
      <p className="multiExpLbl is-capitalized">Select Market Role </p>
      <div className="select  is-link is-fullwidth is-small third-party-select-hack" style={{height:"auto"}}>
        <Multiselect
          filter
          placeholder="Select Market Role"
          value={
            this.state.filteredValue.marketRole.length > 0
              ? this.state.filteredValue.marketRole
              : null
          }
          name="marketRole"
          data={this.state.marketRoleList ? this.state.marketRoleList : []}
          message="Select Market Role"
          textField="label"
          onChange={val => {
            const event = {
              target: {
                name: "marketRole",
                value: val
              }
            }
            this.onChangeFilter(event)
          }}
        />
      </div>
    </span>
  );

  renderSearchInput = () => {
    const { listType } = this.props
    const type = listType === "migratedentities" ? "migrated entities" : listType === "migratedusers" ? "migrated users" : listType
    return(
      <span>
        {this.props.listType !== "people" && (
          <p className="multiExpLbl is-capitalized">
            {`search ${ type }`}
          </p>
        )}
        <p className="control has-icons-left">
          <DebounceInput
            className="input is-fullwidth is-link is-small"
            type="text"
            placeholder={"Search " + listType}
            debounceTimeout={1000}
            name="entitySearch"
            value={this.state.filteredValue.entitySearch}
            onChange={e => this.onChangeFilter(e)}
          />
          <span className="icon is-small is-left has-background-black has-text-white">
            <i className="fas fa-search" />
          </span>
        </p>
      </span>
    )
  }

  renderSelectState = () => (
    <span>
      <p className="multiExpLbl is-capitalized">Select States</p>
      <div className="select is-link is-fullwidth is-small tp-select-state">
        <Multiselect
          filter
          placeholder="Select State"
          value={this.state.filteredValue.entityState}
          name="entityState"
          data={
            this.state.filteredValue.entityState &&
              this.state.countryResult.length > 0 &&
              this.state.countryResult[2]["United States"]
              ? [...this.state.countryResult[2]["United States"]]
              : []
          }
          message="select State"
          textField="state"
          onChange={val => {
            const event = {
              target: {
                name: "entityState",
                value: val
              }
            }
            this.onChangeFilter(event)
          }}
        />
      </div>
    </span>
  );

  renderSaveSearchElements = () => (
    <div className="columns entitypg">
      <ConfirmationModal
        closeModal={this.toggleModal}
        modalState={this.state.modalState}
        message={this.state.modalMessage}
        onConfirmed={() => this.handleSavePref(true)}
      />
      <div className="column">
        <p className="multiExpLbl is-capitalized">Name Search</p>
        <div className="field has-addons  is-fullwidth">
          <div className="control w-100">
            <input
              className="input  is-link is-fullwidth is-small"
              type="text"
              name="searchName"
              value={this.state.searchName}
              placeholder="Name your search"
              onChange={event => this.onSearchPrefChange(event)}
              title="Save this search criteria"
            />
          </div>
        </div>
      </div>
      <div className="column is-narrow">
        <div className="control">
          <p className="multiExpLbl is-capitalized">Default</p>
          <input
            className="checkbox is-link"
            type="checkbox"
            name="defaultSearch"
            disabled={!this.state.searchName}
            checked={this.state.filteredValue.defaultSearch}
            onChange={event => this.onSearchPrefChange(event)}
            title="Make this a default search"
          />
        </div>
      </div>
      <div className="column is-narrow">
        <div className="control">
          <p className="multiExpLbl is-capitalized">&nbsp;&nbsp;</p>
          <button
            className="button is-link is-small"
            disabled={this.state.isSaving}
            onClick={e => this.handleSavePref(false)}
          >
            {this.state.isSaving ? "Saving..." : "Save"}
          </button>
        </div>
      </div>
    </div>
  );

  renderSelectSavedSearches() {
    return (
      <span>
        <div className="columns entitypage">
          <div className="column">
            <p className="multiExpLbl is-capitalized">Saved Search</p>
            <div className="field has-addons">
              <div className="control w-100">
                <div
                  className="select is-link is-fullwidth is-small third-party-select-hack"
                  style={{ height: "1.8rem" }}
                >
                  <DropdownList
                    filter
                    value={this.state.savedSearches}
                    data={
                      this.state.savedSearchesList
                        ? this.state.savedSearchesList
                        : []
                    }
                    valueField="value"
                    textField="label"
                    defaultValue={0}
                    busy={this.state.loadingPickLists}
                    busySpinner={
                      <span className="fas fa-sync fa-spin is-link" />
                    }
                    onChange={val => {
                      const event = {
                        target: {
                          name: "savedSearches",
                          value: val
                        }
                      }
                      this.onSearchPrefChange(event)
                    }}
                  />
                </div>
              </div>

              <div className="control">
                <button
                  className="button is-dark is-fullwidth is-small"
                  onClick={e => this.handleDeletePref()}
                  title="Delete selected search"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
          <div className="column is-3" >
            <p className="multiExpLbl is-capitalized">Page size</p>
            <DropdownList
              filter={false}
              data={this.state.pageSizeOptions}
              value={
                this.state.filteredValue.pageSizePref === ""
                  ? "Select Page Size"
                  : this.state.filteredValue.pageSizePref
              }
              defaultValue={25}
              onChange={val => {
                const event = {
                  target: {
                    name: "pageSizePref",
                    value: val
                  }
                }
                this.onChangeFilter(event)
              }}
            />
          </div>
        </div>
      </span>
    )
  }

  changeField = (e) => {
    const { name, type } = e.target
    const value = type === "checkbox"
      ? e.target.checked
      : e.target.value
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      let issuerFlags = [...prevState.issuerFlags]
      if (name === "entityType") {
        if (!["Governmental Entity / Issuer", "501c3 - Obligor"].includes(value)) {
          issuerFlags = []
        }
      }
      return { [name]: value, issuerFlags, error }
    })
  }

  changeIssuerFlags = (issuerFlags) => {
    this.setState({ issuerFlags })
  }

  changeMarketRole = (marketRole) => {
    if (marketRole.includes("Legal")) {
      this.setState(prevState => {
        if (prevState.marketRole.includes("Legal")) {
          return { marketRole }
        }
        return { marketRole, legalRole: prevState.legalTypes }
      })
    } else {
      this.setState({ marketRole, legalRole: [] })
    }
  }

  changeLegalRole = (legalRole) => {
    this.setState({ legalRole })
  }

  convertEntityModalToggle = () => {
    this.setState({
      entityModalState: !this.state.entityModalState
    })
  }

  onConvertToEntity = () => {
    const {entityList} = this.state
    const selectedEnt = entityList.filter(ent => ent.selected)
    this.setState({
      entityModalState: true,
      selectedEnt
    })
  }

  onSave = async () => {
    const {clientOrProspect, issuerFlags, marketRole, legalRole, entityType, selectedEnt} = this.state
    const {location: { pathname }} = this.props
    const marketRoles = await getUniqValueFromArray([...marketRole, ...legalRole])
    if(!(selectedEnt && selectedEnt.length)) return
    const error = {}
    if(!clientOrProspect || (!entityType && clientOrProspect !== "Third Party")){
      if(!clientOrProspect){
        error.clientOrProspect = "Please provide a value"
      }
      if(!entityType && clientOrProspect !== "Third Party"){
        error.entityType = "Please provide a value"
      }
    }
    if(error && Object.keys(error).length){
      return this.setState({error})
    }

    this.setState({
      entityModalState: false,
      loading: true,
    }, () => {
      for(let i in selectedEnt) {
        const basicDetails = {
          entityType: clientOrProspect === "Third Party" ? selectedEnt[i].entityName : entityType,
          firmName: selectedEnt[i].entityName,
          msrbFirmName: selectedEnt[i].entityName,
          entityFlags: clientOrProspect === "Third Party" ?
            { marketRole: marketRoles } :
            { issuerFlags },
        }
        saveEntityDetails(`${selectedEnt[i].entityId}?type=migrated`, basicDetails, clientOrProspect, async () => {
          if (parseInt(i, 10) === (selectedEnt.length - 1)) {
            this.setState({
              clientOrProspect: "",
              issuerFlags: [],
              marketRole: [],
              legalRole: [],
              entityType: "",
              selectedEnt: [],
              error: {},
              loading: false,
            })
            toast("Successfully converted, migrated to entity", {
              autoClose: 2000,
              type: toast.TYPE.SUCCESS
            })
            /* if(auth && auth.userEntities && auth.userEntities.entityId){
              await this.props.getURLsForTenant(auth.userEntities.entityId)
            } */
            if(pathname.includes("admin")){
              if (clientOrProspect === "Third Party") {
                this.props.history.push("/admin-thirdparty")
              } else {
                this.props.history.push("/admin-cltprosp")
              }
            }else if (clientOrProspect === "Third Party") {
              this.props.history.push("/mast-thirdparty")
            } else {
              this.props.history.push("/mast-cltprosp")
            }
          }
        })
      }
    })
  }

  xlDownload = async  () => {
    const type = this.props.listType
    let result = await this.getFilteredData(0, [], 10, "isExcel")
    result = _.orderBy(result && result.data, ["entityName"], "asc") || []
    const labelName = type === "third-party" ? "Third Party" : type === "client-prospect" ? "Clients / Prospects" : type === "migratedentities" ? "Migrated Entities"
      : type === "users" ? "Users" : ""
    const clientProspect = []
    const jsonSheets = []
    if(result) {
      result && result.map((item) => {
        let data = {}
        if (type === "third-party" || type === "client-prospect") {
          data = {
            "Type": type === "third-party" ? item.entityFlagsAggregated : item.entityRelationshipType || "--",
            "Entity Name": item.entityName || "--",
            "City": item.entityPrimaryAddressCity || "--",
            "State": item.entityPrimaryAddressState || "--",
            "User Name": item.userFullName || "--",
            "Primary Contact Phone": item.userPrimaryPhoneNumber || "--"
          }
        } else if (type === "users") {
          data = {
            "Full Name": item.userFullName || "--",
            "Associated Entity": item.entityName || "--",
            "Primary Email": item.userPrimaryEmail || "--",
            "Primary Phone": item.userPrimaryPhoneNumber || "--",
            "Primary Address": item.userPrimaryAddressGoogle || "--"
          }
        }
        clientProspect.push(data)
      })

      if (type === "third-party" || type === "client-prospect") {
        const jsonClient = {
          name: type === "third-party" ? "Third-Party" : "Client-Prospect",
          headers: ["Type","Entity Name","City","State","User Name","Primary Contact Phone"],
          data: clientProspect,
        }
        jsonSheets.push(jsonClient)
      } else if (type === "users") {
        const jsonUser = {
          name: "Users",
          headers: ["Full Name","Associated Entity","Primary Email","Primary Phone","Primary Address"],
          data: clientProspect,
        }
        jsonSheets.push(jsonUser)
      }
    }

    this.setState({
      labelName,
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  onUserStateChange = async (e) => {
    const {confirmAlert, selectedUsers} = this.state
    let {value} = e && e.target
    value = value === "Active" ? "active" : value === "Inactive" ? "inactive" : ""
    const count = selectedUsers && selectedUsers.length
    if (value && count) {
      confirmAlert.text = `You want to change the status of ${count} Users?`
      swal(confirmAlert)
        .then(async(willConvert) => {
          if (willConvert) {
            const response = await changeUsersStatus({ids: selectedUsers, status : value })
            if (response && response.done) {
              await this.getFilteredData(0)
              this.setState({
                selectedUsers: []
              })
              toast("Your Users are Converted Successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
            } else {
              toast("Something went wrong in Convert User Status", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            }
          }
        })
    }
  }

  getSelectedUsers = (data) => {
    this.setState({ selectedUsers: data || []})
  }

  render() {
    const loading = () => <Loader />
    const { labelName, entityList, search, pages, page, userStateChange, selectedUsers } = this.state
    const { userEntities } = this.props.auth
    return (
      <React.Fragment>
        <ConvertToEntityModal
          closeModal={this.convertEntityModalToggle}
          state={this.state}
          onChange={this.changeField}
          onSave={this.onSave}
          changeIssuerFlags={this.changeIssuerFlags}
          changeLegalRole={this.changeLegalRole}
          changeMarketRole={this.changeMarketRole}
        />
        {this.state.loading ? loading() : null}

        <div className="columns overflow-auto">
          <div className="column is-pulled-left">
            <div className="field is-grouped" style={{justifyContent: "flex-start"}}>
              <div className={`${entityList.length ? "control" : "control isDisabled"}`} onClick={this.xlDownload}>
                <span className="has-link"><i className="far fa-2x fa-file-excel has-text-link"/></span>
              </div>
              <div style={{ display: "none" }}>
                <ExcelSaverMulti label={labelName || ""} startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
              </div>
            </div>
          </div>
          {
            this.props.searchPref === "adm-cltprosp" || this.props.searchPref === "adm-thirdparty" ?
              <div className="column is-pulled-right">
                <button
                  className="button is-link is-small"
                  onClick={this.props.addClientFirm}
                >
                  {this.props.searchPref === "adm-cltprosp" ? "Add New Client" : "Add New Third Party"}
                </button>
              </div> : null
          }
          {
            this.props.searchPref === "adm-users" ?
              <div className="column is-pulled-right">
                <div className="field is-grouped">
                  {
                    selectedUsers && selectedUsers.length ?
                      <div className="control">
                        <SelectLabelInput
                          placeholder="Convert User Status"
                          list={["Active", "Inactive"]}
                          name="userStateChange"
                          value={userStateChange || ""}
                          onChange={(e) => this.onUserStateChange(e)}
                        />
                      </div>: null
                  }
                  <button
                    className="button is-link is-small"
                    onClick={this.props.addNewUsers}
                  >
                    Add New Contact
                  </button>
                </div>
              </div> : null
          }
          {
            this.props.activeTab === "Migrated" ?
              <div className="column is-pulled-right">
                <button className="button is-link is-small" onClick={this.onConvertToEntity} > Convert Migrated To Entity </button>
              </div> : null
          }
          {
            this.props.nav1 === "mast-allcontacts" && userEntities && userEntities.relationshipToTenant && userEntities.relationshipToTenant === "Self" ?
              <div className="column is-pulled-right">
                <div className="field is-grouped">
                  {
                    selectedUsers && selectedUsers.length ?
                      <div className="control">
                        <SelectLabelInput
                          placeholder="Convert User Status"
                          list={["Active", "Inactive"]}
                          name="userStateChange"
                          value={userStateChange || ""}
                          onChange={(e) => this.onUserStateChange(e)}
                        />
                      </div>: null
                  }
                  <Link className="button is-link is-small" to="/addnew-contact">
                    Add New Contact
                  </Link>
                </div>
              </div> : null
          }
          {
            (this.props.nav1 === "mast-cltprosp" || this.props.nav1 === "mast-thirdparty") && this.props.activeTab !== "Migrated" && userEntities && userEntities.relationshipToTenant && userEntities.relationshipToTenant === "Self" ?
              <div className="column is-pulled-right">
                <Link className="button is-link is-small" to={this.props.nav1 === "mast-thirdparty" ? "/addnew-thirdparty" :"/addnew-client"}>
                  {this.props.nav1 === "mast-thirdparty" ? "Add New Third Party" :"Add New Client"}
                </Link>
              </div> : null
          }
        </div>

        {this.renderFilter()}
        <EntityPageGrid
          search={search}
          entityList={entityList}
          getSelectedUsers={this.getSelectedUsers}
          loading={this.state.loading}
          listType={this.props.listType}
          auth={this.props.auth}
          getFilteredData={this.getFilteredData}
          routeType={this.props.routeType}
          total={this.state.total}
          pages={this.state.pages}
          pageSize={this.state.pageSize}
          page={page.global}
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const mapDispatchToProps = dispatch => ({
  getURLsForTenant: tenantID => getURLsForTenant(dispatch, tenantID)
})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(EntityPageFilter))
