import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import "react-table/react-table.css"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}
const UsersList = ({
  onPageSizeChange,
  userslist,
  userType = "",
  pageSize,
  pageSizeOptions = [10, 20, 50, 100],
  search,
  updateContact
}) => {
  const entityUrl = (entityType, entityName, entityId, search) => {
    let url = ""
    switch (entityType) {
      case "Firm":
        url = ""
        break
      case "Client":
        url = `/clients-propects/${entityId}/entity`
        break
      default:
        url = `/thirdparties/${entityId}/entity`
        break
    }
    return (
      <Link to={url}>
        <small
          dangerouslySetInnerHTML={{
            __html: highlightSearch(entityName || "-", search)
          }}
        />
      </Link>
    )
  }

  const userUrl = (userType, userName, userId, search) => {
    let url = ""
    switch (userType) {
      case "Firm":
        url = `/admin-users/${userId}/users`
        break
      case "Client":
        url = `/contacts/${userId}/cltprops`
        break
      default:
        url = `/contacts/${userId}/thirdparties`
        break
    }
    return (
      <Link to={url}>
        <small
          dangerouslySetInnerHTML={{
            __html: highlightSearch(userName || "-", search)
          }}
        />
      </Link>
    )
  }
  const renderSwitch = (userType, item) => {
    switch (userType) {
      case "adminUsers":
        return (
          <Link to={`/admin-users/${item.userId}/users`}>
            <small
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.firstName || "-", search)
              }}
            />
          </Link>
        )
        break
      case "users":
        return userUrl("", item.firstName, item.userId, search)
        break

      default:
        return (
          <Link
            to=""
            onClick={event => {
              event.preventDefault()
              updateContact(event, item.userId)
            }}
          >
            <small
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.firstName || "-", search)
              }}
            />
          </Link>
        )
        break
    }
  }
  const columns = [
    {
      Header: "First Name",
      id: "firstName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd -striped">
            {renderSwitch(userType, item, search)}
          </div>
        )
      },
      sortMethod: (a, b) => a.firstName.localeCompare(b.firstName)
    },

    /* {
      Header: "Middle Name",
      id: "middleName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: highlightSearch(item.middleName || "-", search) }} />
        )
      },
      sortMethod: (a, b) => a.middleName.localeCompare(b.middleName)
    } , */
    {
      Header: "Last Name",
      id: "lastName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.lastName || "-", search)
            }}
          />
        )
      },
      sortMethod: (a, b) => a.lastName.localeCompare(b.lastName)
    },
    {
      Header: "Primary Email",
      id: "primaryEmail",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 150,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.primaryEmail || "-", search)
            }}
          />
        )
      },
      sortMethod: (a, b) =>
        a.primaryEmail !== undefined && b.primaryEmail !== undefined
          ? a.primaryEmail.localeCompare(b.primaryEmail)
          : ""
    },
    {
      Header: "Primary Phone",
      id: "phoneNumber",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 100,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="hpTablesTd wrap-cell-text"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(
                (item.phoneNumber && item.phoneNumber.toString()) || "-",
                search
              )
            }}
          />
        )
      },
      sortMethod: (a, b) =>
        a.phoneNumber !== undefined && b.phoneNumber !== undefined
          ? a.phoneNumber.localeCompare(b.phoneNumber)
          : ""
    },
    {
      Header: "Primary Address",
      id: "primaryAddress",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 250,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <p
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.primaryAddress || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.primaryAddress.localeCompare(b.primaryAddress)
    }
  ]
  if (userType === "users") {
    columns.splice(2, 0, {
      Header: "Associated Entity",
      id: "firmName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 150,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd -striped wrap-cell-text">
            {item.relationshipType === "Firm" ? (
              <small>{item.firmName}</small>
            ) : (
              entityUrl(
                item.relationshipType,
                item.firmName,
                item.entityId,
                search
              )
            )}{" "}
          </div>
        )
      },
      sortMethod: (a, b) => a.firmName.localeCompare(b.firmName)
    })

    /* columns.push({
      Header: "Associated Entity",
      id: "firmName",
      className: "multiExpTblVal",
      accessor: item => item,
      minWidth: 150,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd -striped wrap-cell-text">{
            item.relationshipType === "Firm"
              ? <small>{item.firmName}</small>
              : entityUrl(item.relationshipType, item.firmName, item.entityId, search)
          } </div>
        )
      },
      sortMethod: (a, b) => a.firmName.localeCompare(b.firmName)
    })*/
  }
  return (
    <div className="column box">
      <ReactTable
        columns={columns}
        data={userslist}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered overflow-auto"
        pageSize={pageSize || 10}
        // showPageSizeOptions={false}
        showPageJump
        // PaginationComponent={Pagination}
        minRows={2}
        onPageSizeChange={pageSize => {
          onPageSizeChange(pageSize)
        }}
      />
    </div>
  )
}

export default UsersList
