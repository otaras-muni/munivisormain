import React from "react"
import "react-confirm-alert/src/react-confirm-alert.css"
import { Redirect } from "react-router"
import MigratedEntitiesView from "./MigratedEntitiesView"


const MigratedEntitiesMain = props => {
  const { nav1, nav2, nav3 } = props
  if (nav2) {
    return (
      <MigratedEntitiesView {...props} nav1={nav1} id={nav2} navComponent={nav3} />
    )
  }
  return (
    <Redirect to="/mast-cltprosp"/>
  )
}

export default  MigratedEntitiesMain
