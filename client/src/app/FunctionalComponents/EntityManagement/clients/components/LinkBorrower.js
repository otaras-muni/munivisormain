import React from "react"
import dateFormat from "dateformat"
import { DropdownList } from "react-widgets"
import Autosuggest from "react-autosuggest"

const LinkBorrower = props => {
  const inputProps = (item, idx) => ({
    placeholder: "Firm Name",
    value: item.borOblFirmName,
    onChange: val => {
      const event = {
        target: {
          name: "borOblFirmName",
          value: val.target.value || ""
        }
      }
      props.onChangelinkBorrower(event, idx, [], "entityBorObl")
    },
    name: "borOblFirmName",
    title: "Firm Name",
    id: idx
  })
  // onBlur: onBlurInput

  const onSuggestionSelected = (
    event,
    { suggestion, suggestionValue, suggestionIndex, sectionIndex, method },
    id
  ) => {
    const e = {
      target: {
        name: "borOblFirmName",
        value: suggestionValue
      }
    }
    props.onChangelinkBorrower(e, id, [], "entityBorObl")
  }

  return (
    <article
      className={
        props.expanded.linkborrower ? "accordion is-active" : "accordion"
      }
    >
      <div className="accordion-header toggle">
        <p
          onClick={event => {
            props.toggleButton(event, "linkborrower")
          }}
        >
          Link Borrowers / Obligors
        </p>
        {props.canEdit && (
          <div className="field is-grouped">
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={event =>
                  props.addlinkBorrower(
                    event,
                    "linkborrower",
                    props.linkBorrower
                  )
                }
              >
                Add
              </button>
            </div>
            <div className="control">
              <button
                className="button is-light is-small"
                onClick={event =>
                  props.resetlinkBorrower(event, "linkborrower")
                }
              >
                Reset
              </button>
            </div>
          </div>
        )}
      </div>

      {props.expanded.linkborrower && (
        <div className="accordion-body">
          {props.canEdit && (
            <div className="accordion-body">
              <div className="accordion-content linkborrower">
                <div>
                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                    <thead>
                      <tr>
                        <th>
                          <p className="multiExpLbl">Relationship<span className='has-text-danger'>*</span></p>
                        </th>
                        <th>
                          <p className="multiExpLbl">Firm Name<span className='has-text-danger'>*</span></p>
                        </th>
                        <th>
                          <p className="multiExpLbl">Debt Type<span className='has-text-danger'>*</span></p>
                        </th>
                        <th>
                          <p className="multiExpLbl">Associated Cusip-6<span className='has-text-danger'>*</span></p>
                        </th>
                        <th>
                          <p className="multiExpLbl">Start Date<span className='has-text-danger'>*</span></p>
                        </th>
                        <th>
                          <p className="multiExpLbl">End Date<span className='has-text-danger'>*</span></p>
                        </th>
                        <th>
                          <p className="multiExpLbl">Delete</p>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {props.linkBorrower.map((item, idx) => (
                        <tr key={23 * idx}>
                          <td className="multiExpTblVal">
                            <div className="control">
                              <DropdownList
                                filter
                                name="borOblRel"
                                value={item.borOblRel}
                                data={
                                  props.relBorrowerObligor
                                    ? props.relBorrowerObligor[1]
                                    : []
                                }
                                message="Select Borrower / Obligor"
                                onChange={val => {
                                  const event = {
                                    target: {
                                      name: "borOblRel",
                                      value: val
                                    }
                                  }
                                  props.onChangelinkBorrower(
                                    event,
                                    idx,
                                    [],
                                    "entityBorObl"
                                  )
                                }}
                              />
                              {props.entityBorOblErrors &&
                                props.entityBorOblErrors[idx].borOblRel && (
                                  <small className="text-error">
                                    {props.entityBorOblErrors[idx].borOblRel}
                                  </small>
                                )}
                            </div>
                          </td>
                          <td className="multiExpTblVal firmProfile">
                            <div className="control complain-details rw-widget-container">
                              {/* <input
                              className="input is-small is-link"
                              type="text"
                              placeholder="Firm Name"
                              name="borOblFirmName"
                              value={item.borOblFirmName}
                              onChange={event => {
                                props.onChangelinkBorrower(
                                  event,
                                  idx,
                                  [],
                                  "entityBorObl"
                                )
                              }}
                            />
                            {props.entityBorOblErrors && props.entityBorOblErrors[idx].borOblFirmName && <small className="text-error">{ props.entityBorOblErrors[idx].borOblFirmName}</small>} */}

                              <Autosuggest
                                suggestions={props.suggestions || []}
                                onSuggestionsFetchRequested={
                                  props.getBorrowerList || []
                                }
                                onSuggestionsClearRequested={
                                  props.onBorrowerClear
                                }
                                onSuggestionSelected={(
                                  event,
                                  {
                                    suggestion,
                                    suggestionValue,
                                    suggestionIndex,
                                    sectionIndex,
                                    method
                                  }
                                ) =>
                                  onSuggestionSelected(
                                    event,
                                    {
                                      suggestion,
                                      suggestionValue,
                                      suggestionIndex,
                                      sectionIndex,
                                      method
                                    },
                                    idx
                                  )
                                }
                                getSuggestionValue={suggestion =>
                                  suggestion.firmName || ""
                                }
                                renderSuggestion={suggestion => (
                                  <div>{suggestion.firmName}</div>
                                )}
                                inputProps={inputProps(item, idx)}
                              />
                            </div>
                            {props.entityBorOblErrors &&
                            props.entityBorOblErrors[idx]
                              .borOblFirmName && (
                              <small className="text-error">
                                {
                                  props.entityBorOblErrors[idx]
                                    .borOblFirmName
                                }
                              </small>
                            )}
                          </td>
                          <td className="multiExpTblVal">
                            <div className="control">
                              <DropdownList
                                filter
                                value={item.borOblDebtType}
                                data={
                                  props.linkCusipDebType
                                    ? props.linkCusipDebType[1]
                                    : []
                                }
                                message="Select Debt Type"
                                onChange={val => {
                                  const event = {
                                    target: {
                                      name: "borOblDebtType",
                                      value: val
                                    }
                                  }
                                  props.onChangelinkBorrower(
                                    event,
                                    idx,
                                    [],
                                    "entityBorObl"
                                  )
                                }}
                              />
                              {props.entityBorOblErrors &&
                                props.entityBorOblErrors[idx]
                                  .borOblDebtType && (
                                  <small className="text-error">
                                    {
                                      props.entityBorOblErrors[idx]
                                        .borOblDebtType
                                    }
                                  </small>
                                )}
                            </div>
                          </td>
                          <td className="multiExpTblVal">
                            <div className="control">
                              <input
                                className="input is-small is-link"
                                type="text"
                                style={{ textTransform: "uppercase" }}
                                placeholder="Cusip-6"
                                name="borOblCusip6"
                                value={item.borOblCusip6}
                                onChange={event => {
                                  event.target.value = event.target.value.toUpperCase()
                                  if (event.target.value.length > 6) {
                                    return false
                                  }
                                  props.onChangelinkBorrower(
                                    event,
                                    idx,
                                    [],
                                    "entityBorObl"
                                  )
                                }}
                              />
                              {props.entityBorOblErrors &&
                                props.entityBorOblErrors[idx].borOblCusip6 && (
                                  <small className="text-error">
                                    {props.entityBorOblErrors[idx].borOblCusip6}
                                  </small>
                                )}
                            </div>
                          </td>
                          <td className="multiExpTblVal">
                            <div className="field">
                              <div className="control">
                                <input
                                  id="datepickerDemo"
                                  className="input is-small is-link"
                                  type="date"
                                  name="borOblStartDate"
                                  value={
                                    item.borOblStartDate
                                      ? dateFormat(
                                          item.borOblStartDate,
                                          "yyyy-mm-dd"
                                        )
                                      : ""
                                  }
                                  onChange={event => {
                                    props.onChangelinkBorrower(
                                      event,
                                      idx,
                                      [],
                                      "entityBorObl"
                                    )
                                  }}
                                />
                                {props.entityBorOblErrors &&
                                  props.entityBorOblErrors[idx]
                                    .borOblStartDate && (
                                    <small className="text-error">
                                      {
                                        props.entityBorOblErrors[idx]
                                          .borOblStartDate
                                      }
                                    </small>
                                  )}
                              </div>
                            </div>
                          </td>
                          <td className="multiExpTblVal">
                            <div className="field">
                              <div className="control">
                                <input
                                  id="datepickerDemo"
                                  className="input is-small is-link"
                                  type="date"
                                  name="borOblEndDate"
                                  value={
                                    item.borOblEndDate
                                      ? dateFormat(
                                          item.borOblEndDate,
                                          "yyyy-mm-dd"
                                        )
                                      : ""
                                  }
                                  onChange={event => {
                                    props.onChangelinkBorrower(
                                      event,
                                      idx,
                                      [],
                                      "entityBorObl"
                                    )
                                  }}
                                />
                                {props.entityBorOblErrors &&
                                  props.entityBorOblErrors[idx]
                                    .borOblEndDate && (
                                    <small className="text-error">
                                      {
                                        props.entityBorOblErrors[idx]
                                          .borOblEndDate
                                      }
                                    </small>
                                  )}
                              </div>
                            </div>
                          </td>
                          <td className="multiExpTblVal">
                            <div style={{ padding: "0px 10px" }}>
                              {idx >= 0 ? (
                                <span
                                  className="has-text-link fa-delete"
                                  style={{ cursor: "pointer" }}
                                  onClick={event => {
                                    props.deleteAddress(
                                      event,
                                      "entityBorObl",
                                      idx
                                    )
                                  }}
                                >
                                  <i className="far fa-trash-alt" />
                                </span>
                              ) : (
                                ""
                              )}
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                  <div className="columns">
                    <div className="column is-full">
                      <div className="field is-grouped-center">
                        <div className="control">
                          <button
                            className="button is-link"
                            onClick={() => props.onSave("linkBorrower")}
                          >
                            Save Borrowers / Obligors
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          {props.LinkBorrowerList.length > 0 && (
            <div className="box">
              <div className="overflow-auto">
                <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                  <thead>
                    <tr>
                      <th>
                        <p className="multiExpLbl">Relationship</p>
                      </th>
                      <th>
                        <p className="multiExpLbl">Firm Name</p>
                      </th>
                      <th>
                        <p className="multiExpLbl">Debt Type</p>
                      </th>
                      <th>
                        <p className="multiExpLbl">Associated Cusip-6</p>
                      </th>
                      <th>
                        <p className="multiExpLbl">Start Date</p>
                      </th>
                      <th>
                        <p className="multiExpLbl">End Date</p>
                      </th>
                      {props.canEdit && (
                        <th>
                          <p className="multiExpLbl">Update/Delete</p>
                        </th>
                      )}
                    </tr>
                  </thead>
                  <tbody>
                    {props.LinkBorrowerList.length > 0 &&
                      props.LinkBorrowerList.map((item, idx) => (
                        <tr key={123 * idx}>
                          <td className="multiExpTblVal">
                            <small>{item.borOblRel}</small>
                          </td>
                          <td className="multiExpTblVal">
                            <small>{item.borOblFirmName}</small>
                          </td>
                          <td className="multiExpTblVal">
                            <small>{item.borOblDebtType}</small>
                          </td>
                          <td className="multiExpTblVal">
                            <small>{item.borOblCusip6}</small>
                          </td>
                          <td className="multiExpTblVal">
                            <small>
                              {dateFormat(item.borOblStartDate, "yyyy-mm-dd")}
                            </small>
                          </td>
                          <td className="multiExpTblVal">
                            <small>
                              {dateFormat(item.borOblEndDate, "yyyy-mm-dd")}
                            </small>
                          </td>
                          {props.canEdit && (
                            <td className="multiExpTblVal">
                              <div className="field is-grouped">
                                <div
                                  className="control"
                                  onClick={event => {
                                    props.updateLinkBorrower(event, item._id)
                                  }}
                                >
                                  <a href="">
                                    <span className="has-text-link">
                                      <i className="fas fa-pencil-alt" />
                                    </span>
                                  </a>
                                </div>
                                <div
                                  className="control"
                                  onClick={event => {
                                    props.deleteLinkBorrower(event, item._id)
                                  }}
                                >
                                  <a href="">
                                    <span className="has-text-link">
                                      <i className="far fa-trash-alt" />
                                    </span>
                                  </a>
                                </div>
                              </div>
                            </td>
                          )}
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          )}
        </div>
      )}
    </article>
  )
}

export default LinkBorrower
