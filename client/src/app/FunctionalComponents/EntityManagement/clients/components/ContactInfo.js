import React from "react"
import NumberFormat from "react-number-format"
import { DropdownList } from "react-widgets"
import {
  TextLabelInput,
  DropDownInput,
  ZipCodeNumberInput
} from "../../../../GlobalComponents/TextViewBox"

const ContactInfo = props => (
  <article
    className={props.expanded.contactinfo ? "accordion is-active" : "accordion"}
  >
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "contactinfo")
        }}
      >
        Current / Previous Advisor Contact Information
      </p>
      {props.expanded.contactinfo ? (
        <i className="fas fa-chevron-up" />
      ) : (
        <i className="fas fa-chevron-down" />
      )}
    </div>
    {props.expanded.contactinfo && (
      <div className="accordion-body">
        <div className="accordion-content">
          <div className="columns">
            <DropDownInput
              filter
              style={{ fontSize: 12 }}
              value={props.firmDetails.firmLeadAdvisor}
              data={props.usersList}
              message="Select Lead Advisor"
              textField="label"
              valueField="value"
              key="name"
              label="Firm's Lead Advisor"
              defaultValue=""
              onChange={val => {
                const event = {
                  target: {
                    name: "firmLeadAdvisor",
                    value: val
                  }
                }
                props.onChangeFirmDetail(event)
              }}
              disabled={!props.canEdit}
              disableValue={props.firmDetails.firmLeadAdvisor}
              error={
                (props.errorFirmDetail &&
                  props.errorFirmDetail.firmLeadAdvisor) ||
                ""
              }
              busy={props.busy}
            />
          </div>
          <div className="columns">
            <TextLabelInput
              name="prevLeadAdvisor"
              type="text"
              value={props.firmDetails.prevLeadAdvisor || ""}
              placeholder="Enter Previous Lead Advisor"
              style={{ width: "100%" }}
              label="Previous Lead Advisor"
              onChange={props.onChangeFirmDetail}
              disabled={!props.canEdit || false}
              error={
                (props.errorFirmDetail &&
                  props.errorFirmDetail.prevLeadAdvisor) ||
                ""
              }
            />
            <TextLabelInput
              name="prevAdvisorFirm"
              value={props.firmDetails.prevAdvisorFirm || ""}
              type="text"
              placeholder="Enter Previous Advisor Firm"
              style={{ width: "100%" }}
              label="Previous Advisor Firm"
              onChange={props.onChangeFirmDetail}
              disabled={!props.canEdit || false}
              error={
                (props.errorFirmDetail &&
                  props.errorFirmDetail.prevAdvisorFirm) ||
                ""
              }
            />
            <TextLabelInput
              name="prevAdvisorContractExpire"
              type="date"
              value={props.firmDetails.prevAdvisorContractExpire || ""}
              placeholder="Enter Previous Lead Advisor"
              style={{ width: "100%" }}
              label="Previous Advisory Contract Expiry"
              onChange={props.onChangeFirmDetail}
              disabled={!props.canEdit || false}
              error={
                (props.errorFirmDetail &&
                  props.errorFirmDetail.prevAdvisorContractExpire) ||
                ""
              }
            />
          </div>
          <div className="columns">
            <TextLabelInput
              name="primaryContactNameInEmma"
              value={props.firmDetails.primaryContactNameInEmma || ""}
              type="text"
              placeholder="Enter Primary Contact Name in EMMA"
              style={{ width: "100%" }}
              label="Primary Contact Name in EMMA"
              onChange={props.onChangeFirmDetail}
              disabled={!props.canEdit || false}
              error={
                (props.errorFirmDetail &&
                  props.errorFirmDetail.primaryContactNameInEmma) ||
                ""
              }
            />
            <ZipCodeNumberInput
              format="+1 (###) ###-####"
              mask="_"
              className="input is-small is-link"
              name="primaryContactPhone"
              placeholder="Numbers only"
              label="Primary Contact Phone"
              value={
                props.firmDetails.primaryContactPhone
                  ? props.firmDetails.primaryContactPhone
                  : ""
              }
              onChange={props.onChangeFirmDetail}
              disabled={!props.canEdit}
            />
            <TextLabelInput
              name="primaryContactEmail"
              type="text"
              value={props.firmDetails.primaryContactEmail || ""}
              placeholder="Enter Primary Contact Phone"
              style={{ width: "100%" }}
              label="Primary Contact Email"
              onChange={props.onChangeFirmDetail}
              disabled={!props.canEdit || false}
              error={
                (props.errorFirmDetail &&
                  props.errorFirmDetail.primaryContactEmail) ||
                ""
              }
            />
          </div>
          <div className="columns">
            <div className="column is-full">
              <div className="field is-grouped-center">
                <div className="control">
                  <button
                    className="button is-link"
                    onClick={() => props.onSave("contact")}
                  >
                    Save Contact Information
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )}
  </article>
)
export default ContactInfo
