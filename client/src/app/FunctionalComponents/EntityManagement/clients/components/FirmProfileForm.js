import React from "react"
import { TextLabelInput, DropDownInput } from "../../../../GlobalComponents/TextViewBox"
import BorrowerList from "../../../../GlobalComponents/BorrowerList"

const FirmProfileForm = (props) => (
  <div className="accordion-body">
    <div className="accordion-content">
      <div className="columns firmProfile">
        <DropDownInput filter required
          data={props.entityTypeList}
          value={props.firmDetails.entityType ? { label: props.firmDetails.entityType, value: props.firmDetails.entityType } : props.entityTypeList ? props.entityTypeList[0] : []}
          style={{ fontSize: 12 }}
          message="Select Entity Type"
          textField="label"
          valueField="value"
          key="entityType"
          label="Entity Type"
          defaultValue=""
          onChange={props.getFirmDetails}
          disabled={props.readOnlyFeatures.entityType}
          disableValue={props.firmDetails.entityType}
          error={props.errorFirmDetail && props.errorFirmDetail.entityType || ""}
          busy={props.busy}
        />
        <div className="column complain-details">
          <p className="multiExpLbl">Entity Name<span className='has-text-danger'>*</span></p>
          <BorrowerList
            value={props.firmDetails.firmName}
            onChange={props.onChangeEntity}
            participantType={props.firmDetails.entityType}
            onError={props.onError}
          />
          <p className="has-text-danger text-error">{props.errorFirmDetail && props.errorFirmDetail.firmName || props.firmError }</p>

          {props.firmDetails.entityAliases &&
          props.firmDetails.entityAliases.length
            ? props.firmDetails.entityAliases.map((item, idx) => (
              <div className="field" key={parseInt(4 * idx, 10)}>
                {!props.canEdit ? <small>{item}</small>
                  : <div className="control d-flex">
                    <input
                      className="input is-small is-link"
                      name="entityAliases"
                      type="text"
                      placeholder="Enter Aliases"
                      value={item}
                      onChange={event => {
                        props.onChangeAliases(event, idx)
                      }}
                    />
                    <span
                      className="has-text-link fa-delete deleteadressaliases"
                      onClick={
                        () => {
                          props.deleteAliases(idx)
                        }
                      }>
                      <i className="far fa-trash-alt" />
                    </span>
                  </div>}
                {props.errorFirmDetail && props.errorFirmDetail.entityAliases[idx] && <small className="text-error">{props.errorFirmDetail.entityAliases[idx]}</small>}
              </div>
            ))
            : ""}
          {props.canEdit && <div className="field is-grouped">
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "aliases")
                }}
              >
                Add Alias
              </button>
            </div>
            <div className="control">
              <button
                className="button is-light is-small"
                onClick={props.resetAliases}
              >
                Cancel
              </button>
            </div>
          </div>}
        </div>
      </div>
      <hr />
      <div className="columns">
        <DropDownInput filter
          data={props.primarySectorsList[1] ? [...props.primarySectorsList[1]] : []}
          message="Select Primary Sectors"
          textField='label'
          valueField="value"
          defaultValue=""
          value={props.firmDetails.primarySectors}
          style={{ fontSize: 12 }}
          key="primarySectors"
          label="Primary Sectors"
          onChange={(val) => {
            const event = {
              target: {
                name: "primarySectors",
                value: val
              }
            }
            props.onChangeFirmDetail(event)
          }}
          disabled={!props.canEdit}
          disableValue={props.firmDetails.primarySectors}
          error={props.errorFirmDetail && props.errorFirmDetail.primarySectors || ""}
          busy={props.busy}
        />
        <DropDownInput filter
          data={props.primarySectorsList[1] && props.primarySectorsList[2][props.firmDetails.primarySectors] ? props.primarySectorsList[2][props.firmDetails.primarySectors] : []}
          message="Select Primary Sectors"
          textField='label'
          valueField="value"
          defaultValue=""
          value={props.firmDetails.secondarySectors}
          style={{ fontSize: 12 }}
          key="secondarySectors"
          label="Secondary Sectors"
          onChange={(val) => {
            const event = {
              target: {
                name: "secondarySectors",
                value: val
              }
            }
            props.onChangeFirmDetail(event)
          }}
          disabled={!props.canEdit}
          disableValue={props.firmDetails.secondarySectors}
          error={props.errorFirmDetail && props.errorFirmDetail.secondarySectors || ""}
          busy={props.busy}
        />
        <TextLabelInput
          name="taxId"
          value={props.firmDetails.taxId}
          placeholder="Enter Tax ID"
          style={{ width: "100%" }}
          label="Tax ID"
          onChange={props.onChangeFirmDetail}
          disabled={!props.canEdit || false}
          error={props.errorFirmDetail && props.errorFirmDetail.taxId ? props.errorFirmDetail.taxId : ""}
        />
      </div>
      <div className="columns">
        <div className="column is-full">
          <div className="field is-grouped-center">
            <div className="control">
              <button className="button is-link" onClick={() => props.onSave("client")}>Save Client
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default FirmProfileForm
