import React  from "react"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import DocUpload from "../../../docs/DocUpload"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import ImagePreview from "../../../../GlobalComponents/ImagePreview"
import DocModal from "../../../docs/DocModal";
import {SelectLabelInput, TextLabelInput} from "../../../../GlobalComponents/TextViewBox";

const AuditAndLogo = (props) => {

  const getBidBucket = (filename, docId) => {
    let { settings } = props
    settings = {
      ...settings,
      logo: {
        awsDocId: docId,
        fileName: filename
      }
    }
    props.onChange(settings, "logo")
  }

  const onChangeAuditFlag = e => {
    const {settings} = props
    const {name, value} = e.target
    settings[name] = value

    props.onChange(settings)
  }

  const onChangeAlign = e => {
    const {settings} = props
    const {name, value} = e.target
    settings[name] = value
    props.onChange(settings)
  }

  const onRemoveLogo = () => {
    const { settings } = props
    settings.logo.awsDocId = ""
    settings.logo.fileName = ""
    props.onChange(settings, "logo")
  }

  const onBlurInput = (e) => {
    const {user} = props
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    let change = ""
    if (name && value) {
      change = value === "no" ? "No activity log will be maintained by MuniVisor" :
        value === "supervisor" ? "Activity log will be only visible to Compliance Officer / Supervisory Principals" :
          value === "currentState" ? "Activity log will be visible to all eligible users of the firm" : value
      const audit = {
        userId: user && user.userId,
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `${name || "empty"} change to ${change || "empty"} in Firms`,
        date: new Date()
      }
      // auditLog.push(audit)
      props.getAuditChangeLog(audit)
    }
  }

  return (
    <div>
      {/* <div className="columns">
        <div className="column is-size-6">
          <label className="radio-button">No Activity log will be maintained by MuniVisor
            <input
              type="radio"
              name="auditFlag"
              value="no"
              checked={props.settings.auditFlag === "no"}
              onChange={onChangeAuditFlag}
              onBlur={(event) => onBlurInput(event)}
            />
            <span className="checkmark" />
          </label>
        </div>
        <div className="column is-size-6">
          <label className="radio-button">Activity log will be only visible to Compliance Officer / Supervisory Principals
            <input
              type="radio"
              name="auditFlag"
              value="supervisor"
              checked={props.settings.auditFlag === "supervisor"}
              onChange={onChangeAuditFlag}
              onBlur={(event) => onBlurInput(event)}
            />
            <span className="checkmark" />
          </label>
        </div>
        <div className="column is-size-6">
          <label className="radio-button">Activity log will be visible to all eligible users of the firm
            <input
              type="radio"
              name="auditFlag"
              value="currentState"
              checked={props.settings.auditFlag === "currentState"}
              onChange={onChangeAuditFlag}
              onBlur={(event) => onBlurInput(event)}
            />
            <span className="checkmark" />
          </label>
        </div>
      </div>
      <hr/> */}
      <div className="columns">
        <div className="column">
          <div className="title is-6 has-text-weight-semibold has-text-centered">
            Logo Configuration
          </div>
        </div>
      </div>
      <div className="columns">
        <SelectLabelInput
          label="Logo Location"
          name="location"
          list={props && props.logoLocation || []}
          onChange={onChangeAlign}
          value={props.settings.location || "Header"}
          onBlur={(event) => onBlurInput(event)}
        />
        <SelectLabelInput
          label="Logo Alignment"
          name="alignment"
          list={props && props.logoAlignment || []}
          onChange={onChangeAlign}
          value={props.settings.alignment || "Center"}
          onBlur={(event) => onBlurInput(event)}
        />
        <TextLabelInput
          label="Title"
          placeholder="Enter Title"
          name="title"
          onChange={onChangeAlign}
          value={props.settings.title || ""}
          onBlur={(event) => onBlurInput(event)}
        />
        <div className="column" style={{width: "unset"}}>
          <p className="multiExpLblBlk">Logo</p>
          {
            props.settings && props.settings.logo && props.settings.logo.awsDocId ?
              <div>
                <ImagePreview imagePreview docId={props.settings.logo.awsDocId} />
                <div>
                  <DocUpload
                    accept="image/x-png,image/jpeg"
                    bucketName={CONST.bucketName}
                    docId={props.settings.logo.awsDocId}
                    versionMeta={{ uploadedBy: `${props.user.userFirstName} ${props.user.userLastName}` }}
                    contextId={props.user.entityId}
                    contextType={ContextType.entityLogo || ""}
                    tenantId={props.user.entityId} />
                  <DocModal
                    onDeleteAll={onRemoveLogo}
                    selectedDocId={props.settings.logo.awsDocId}
                  />
                </div>
              </div> :
              <DocUpload
                accept="image/x-png,image/jpeg"
                bucketName={CONST.bucketName}
                versionMeta={{ uploadedBy: `${props.user.userFirstName} ${props.user.userLastName}` }}
                onUploadSuccess={getBidBucket}
                showFeedback
                contextId={props.user.entityId}
                contextType={ContextType.entityLogo || ""}
                tenantId={props.user.entityId}
              />
          }
        </div>
      </div>
    </div>
  )
}


export default AuditAndLogo
