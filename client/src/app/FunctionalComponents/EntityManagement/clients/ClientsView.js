import React from "react"
import { Redirect, NavLink } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
// import UpdateClientContainer from "./containers/UpdateClientContainer"
import ClientsProspects from "../../ClientsProspects/ClientsProspects"
import ClientContactList from "./../contacts/containers/ContactListContainer"
import BusinessActivity from "./../business-activity/containers/BusinessActivityListContainer"
import Contracts from "./containers/ContractUpdateContainer"
import Documents from "./../Documents"
import Audit from "../../../GlobalComponents/Audit"

const TABS = [
  { path: "entity", label: "Entity" },
  { path: "business-activity", label: "Business Activity" },
  { path: "contacts", label: "People" },
  { path: "documents", label: "Documents" },
  { path: "contracts", label: "Contracts" },
  { path: "activity-log", label: "Activity Log" }
]

const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

const ClientsView = props => {
  let { navComponent, audit, svControls } = props
  const { id, nav1, accessibleTabs } = props
  navComponent = navComponent || "details"
  const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
  if (!submitAudit) {
    const index = accessibleTabs.findIndex(tab => tab === "activity-log")
    if(index !== -1){
      accessibleTabs.splice(index, 1)
    }
  }
  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => {
      if(accessibleTabs.indexOf(t.path) === -1){
        return false
      }
      return (
        <li key={t.path}>
          <NavLink
            key={t.path}
            to={`/clients-propects/${id}/${t.path}`}
            activeStyle={activeStyle}
          >
            {" "}
            {t.label}{" "}
          </NavLink>
        </li>
      )
  })
  const renderViewSelection = (id, navComponent) => (
    <div className="tabs is-boxed">
      <ul>{renderTabs(TABS, navComponent, id)}</ul>
    </div>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(id, navComponent)}
          </div>
        </div>
      </div>
      <div id="main">
        <Switch condition={navComponent}>
          <Case value="entity">
            <ClientsProspects nav2={id} />
          </Case>
          <Case value="business-activity">
            <BusinessActivity />
          </Case>
          <Case value="contacts">
            <ClientContactList nav2={id} />
          </Case>
          <Case value="contracts">
            <Contracts nav2={id} />
          </Case>
          <Case value="documents">
            <Documents nav2={id} />
          </Case>
          <Case value="activity-log">
            <Audit nav1={nav1} nav2={id}/>
          </Case>
          <Default>
            <Redirect to="/client-list" />
          </Default>
        </Switch>
      </div>
    </div>
  )
}

export default ClientsView
