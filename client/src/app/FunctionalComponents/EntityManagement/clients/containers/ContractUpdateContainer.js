import React from "react"
import moment from "moment"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import CONST from "GlobalUtils/consts"
import { getPicklistByPicklistName, checkNavAccessEntitlrment } from "GlobalUtils/helpers"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import ScheduleTransactionAndRetainer from "../../../../GlobalComponents/ScheduleTransactionAndRetainer"
import {
  putcontracts,
  putcontractstatus,
  getcontracts,
  pullContractDocuments,
  putGenAuditLog
} from "../../../../StateManagement/actions/ClientProspectDashboard/actionCreators"
import { getFirmById } from "AppState/actions/AdminManagement/admTrnActions"
import Loader from "../../../../GlobalComponents/Loader"
import { DropDownInput } from "../../../../GlobalComponents/TextViewBox"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import connect from "react-redux/es/connect/connect"
import Audit from "../../../../GlobalComponents/Audit"
import {checkSupervisorControls} from "../../../../StateManagement/actions/CreateTransaction"

const cols = [
  [
    { name: "Contract ID" },
    { name: "Contract Name" },
    { name: "Contract Type" },
    { name: "Start Date" },
    { name: "End Date" },
    { name: "Updated On" },
    { name: "Updated By" },
    { name: "Action" },
    { name: "Status" }
  ]
]

class Contracts extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      contracts: [],
      contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
      sof: [cloneDeep(CONST.FAServiceFee.sof)],
      nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      documents: [],
      retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      tempSof: [cloneDeep(CONST.FAServiceFee.sof)],
      tempNonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      tempRetAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      contractObjectId: "",
      firmName: "",
      errorMessages: {},
      auditLogs: [],
      digitize: false,
      loading: true,
      newContract: false,
      viewOnly: true
    }
  }

  async componentWillMount() {
    const { nav2 } = this.props
    let picArray = getPicklistByPicklistName([
      "LKUPROLETYPES",
      "LKUPDESCRIPTIONTYPES",
      "LKUPDEALTYPES",
      "LKUPCONTRACTTYPES",
      "LKUPSECURITYTYPES",
      "LKUPTYPETYPES",
      "LKUPSTATUSTYPES"
    ])
    const res = await getcontracts(nav2)
    const firmDetailById = await getFirmById(this.props.nav2)
    if (res) {
      this.setState({
        contracts: res.data || [],
        auditLogs: (res.data && res.data.auditLogs) || []
      })
    }
    const picResult = await picArray
    const result = (picResult.length && picResult[1]) || {}
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit
    const svControls = await checkSupervisorControls()
    const status = result.LKUPSTATUSTYPES && result.LKUPSTATUSTYPES.map(e => e.label) || []
    this.setState({
      dropDown: {
        sofDesc: result.LKUPDESCRIPTIONTYPES || [],
        sofDealType: result.LKUPDEALTYPES || [],
        contractType: result.LKUPCONTRACTTYPES || [],
        securityType: result.LKUPSECURITYTYPES || [],
        role: result.LKUPROLETYPES || [],
        type: result.LKUPTYPETYPES || [],
        status
      },
      svControls,
      firmName: firmDetailById && firmDetailById.firmName,
      viewOnly,
      loading: false
    })
  }

  /* onRemove = async (contract) => {
    const {nav2} = this.props
    const {confirmAlert} = this.state
    confirmAlert.text = "You want to delete this contract?"
    swal(confirmAlert)
      .then((willDelete) => {
        if (willDelete) {
          deleteContracts(nav2, contract._id, (res) => {
            if (res && res.data && res.data.data) {
              toast("Entity Contract Removed successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
              this.setState({
                contracts: res.data.data || []
              })
            } else {
              toast("Something went wrong!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
            }
          })
        }
      })
  } */

  onEdit = contract => {
    this.setState({
      contractRef: {
        ...(contract && contract.contractRef)
      },
      notes: (contract && contract.notes) || "",
      nonTranFees: (contract && contract.nonTranFees) || [],
      retAndEsc: (contract && contract.retAndEsc) || [],
      documents: (contract && contract.documents) || [],
      sof: (contract && contract.sof) || [],
      digitize: (contract && contract.digitize) || false,
      contractObjectId: (contract && contract._id) || "",
      errorMessages: {}
    })
  }

  onSave = async (payload, callBack) => {
    const { nav2, user } = this.props
    const { contractObjectId } = this.state
    this.props.addAuditLog({
      userName: `${user.userFirstName} ${user.userLastName}`,
      log: contractObjectId
        ? `${payload.contractRef.contractName} Contracts Update`
        : `${payload.contractRef.contractName} Contracts Create`,
      date: new Date(),
      key: "contract"
    })
    if (contractObjectId) {
      payload._id = contractObjectId || ""
    }
    const res = await putcontracts(nav2, payload)
    if (res.data && res.data.contracts) {
      toast("Entity Contract updated successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      putGenAuditLog(res.data && res.data._id, this.props.auditLogs, res => {
        this.setState({ auditLogs: (res.data && res.data.auditLogs) || [] })
        this.props.updateAuditLog([])
      })
      this.setState(
        {
          contracts: (res.data && res.data) || [],
          contractObjectId: "",
          state: {},
          newContract: false,
          contractRef: cloneDeep(CONST.FAServiceFee.contractRef)
        },
        () => {
          callBack({
            status: true,
            state: {
              contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
              sof: [cloneDeep(CONST.FAServiceFee.sof)],
              documents: [],
              nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
              retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
              tempSof: [cloneDeep(CONST.FAServiceFee.sof)],
              tempNonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
              tempRetAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
              notes: "",
              digitize: false
            }
          })
        }
      )
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  onBlur = (event, change, contractName, state) => {
    const { user } = this.props
    this.props.addAuditLog({
      userName: `${user.userFirstName} ${user.userLastName}`,
      log: `${contractName || ""} contracts in ${change || ""} ${
        change === "Documents upload" ? "" : "change to"
      } ${event || ""}`,
      date: new Date(),
      key: "contract"
    })
    this.setState({ state })
  }

  onCancel = () => {
    this.setState({
      contractObjectId: "",
      contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
      sof: [cloneDeep(CONST.FAServiceFee.sof)],
      documents: [],
      newDocuments: [],
      nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      notes: "",
      digitize: false,
      state: {}
    })
  }

  onStatusChange = async (status, key, contract) => {
    const { nav2 } = this.props
    const res = await putcontractstatus(nav2, "status", {
      _id: contract._id,
      [status]: key
    })
    if (res) {
      this.setState({
        contracts: res.data || [],
        contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
        sof: [cloneDeep(CONST.FAServiceFee.sof)],
        nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
        retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
        documents: [],
        notes: ""
      })
    }
  }

  onAddNewContract = () => {
    this.setState(
      {
        newContract: true
      },
      () => this.onCancel()
    )
  }

  onDeleteDoc = async (documentId, callback) => {
    const { contractObjectId } = this.state
    const res = await pullContractDocuments(
      `${this.props.nav2}?contractId=${contractObjectId}&docId=${documentId}`
    )
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      // this.onAuditSave("document")
      this.setState(
        {
          documents: (res.data && res.data.documents) || []
        },
        () => {
          callback({
            status: true,
            documentsList: (res.data && res.data.documents) || []
          })
        }
      )
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  }

  render() {
    const {
      contracts,
      dropDown,
      newContract,
      digitize,
      errorMessages,
      contractRef,
      documents,
      sof,
      nonTranFees,
      retAndEsc,
      tempSof,
      tempNonTranFees,
      tempRetAndEsc,
      loading,
      notes,
      auditLogs,
      state,
      firmName,
      viewOnly,
      svControls
    } = this.state
    const { billingContract, audit } = this.props
    if (loading) {
      return <Loader />
    }
    const columns = viewOnly ? cols[0] && cols[0].filter(col => col.name !== "Action") : cols[0]
    const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false

    return (
      <div className="accordion-overflow">
        <div className="columns overflow-auto">
          <div className="column">
            <div className="tile is-ancestor">
              <div className="tile is-vertical is-8">
                <div className="tile">
                  <div className="tile is-parent is-vertical">
                    <article className="tile is-child">
                      <p className="title innerPgTitle">You selected...</p>
                      <p className="subtitle">{firmName || ""}</p>
                    </article>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {
            !viewOnly ?
              <div className="column is-pulled-right">
                <button className="button is-link" onClick={this.onAddNewContract}>
                  Add New Contract
                </button>
              </div> : null
          }
        </div>

        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="CONTRACTS LIBRARY"
              >
                {activeAccordions.includes(0) && (
                  <div className="tbl-auto">
                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                    <TableHeader cols={columns} />
                    <tbody>
                      {contracts.contracts &&
                        contracts.contracts.map((contract, i) => (
                          <tr key={i}>
                            <td className="emmaTablesTd">
                              <small>
                                {(contract &&
                                  contract.contractRef &&
                                  contract.contractRef.contractId) ||
                                  "---"}
                              </small>
                            </td>
                            <td
                              className="emmaTablesTd" /* style={{width: "70%"}} */
                            >
                              <small>
                                {(contract &&
                                  contract.contractRef &&
                                  contract.contractRef.contractName) ||
                                  "---"}
                              </small>
                            </td>
                            <td className="emmaTablesTd">
                              <small>
                                {(contract &&
                                  contract.contractRef &&
                                  contract.contractRef.contractType) ||
                                  "---"}
                              </small>
                            </td>
                            <td className="emmaTablesTd">
                              <small>
                                {contract &&
                                contract.contractRef &&
                                contract.contractRef.startDate
                                  ? moment(
                                    contract.contractRef.startDate
                                  ).format("MM-DD-YYYY")
                                  : ""}
                              </small>
                            </td>
                            <td className="emmaTablesTd">
                              <small>
                                {contract &&
                                contract.contractRef &&
                                contract.contractRef.endDate
                                  ? moment(contract.contractRef.endDate).format(
                                    "MM-DD-YYYY"
                                  )
                                  : ""}
                              </small>
                            </td>
                            <td className="emmaTablesTd">
                              <small>
                                {contract && contract.updatedAt
                                  ? moment(contract.updatedAt).format(
                                    "MM-DD-YYYY h:mm A"
                                  )
                                  : "--"}
                              </small>
                            </td>
                            <td className="emmaTablesTd">
                              <small>
                                {contract && contract.updatedBy
                                  ? `${contract.updatedBy.userFirstName ||
                                      "--"} ${contract.updatedBy.userLastName}`
                                  : "--"}
                              </small>
                            </td>
                            {
                              !viewOnly ?
                                <td className="emmaTablesTd">
                                  <div className="field is-grouped">
                                    <div className="control">
                                      {contract &&
                                      contract.status === "Deactivate" ? null : (
                                          <a onClick={() => this.onEdit(contract, i)}>
                                            <span className="has-text-link">
                                              <i className="fas fa-pencil-alt" />
                                            </span>
                                          </a>
                                        )}
                                    </div>
                                  </div>
                                </td> : null
                            }
                            <td>
                              <DropDownInput
                                data={dropDown.status || {}}
                                onChange={e =>
                                  this.onStatusChange("status", e, contract)
                                }
                                value={
                                  (contract && contract.status) || "Active"
                                }
                                disabled={viewOnly}
                              />
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
        {newContract || contractRef._id ? (
          <ScheduleTransactionAndRetainer
            contextId={this.props.nav2}
            dropDown={dropDown || {}}
            state={{
              contractRef,
              sof,
              nonTranFees,
              digitize,
              retAndEsc,
              documents,
              tempSof,
              tempNonTranFees,
              tempRetAndEsc,
              notes,
              errorMessages
            }}
            isRequired
            createdAt={(contracts && contracts.createdAt) || ""}
            onSave={this.onSave}
            onCancel={this.onCancel}
            onDeleteDoc={this.onDeleteDoc}
            onBlur={this.onBlur}
            states={state}
          />
        ) : null}
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              {(!billingContract && submitAudit) ? (
                <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{ overflowY: "scroll" }}
                >
                  {activeAccordions.includes(2) && (
                    <Audit auditLogs={auditLogs || []} />
                  )}
                </RatingSection>
              ) : null}
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  auth: state.auth || {},
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

const WrappedComponent = withAuditLogs(Contracts)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)

// export default  Contracts
