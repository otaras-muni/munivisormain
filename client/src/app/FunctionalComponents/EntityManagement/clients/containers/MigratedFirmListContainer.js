import React, { Component } from "react"
import "react-table/react-table.css"
import Fuse from "fuse.js"
import { withRouter, Link } from "react-router-dom"
import { connect } from "react-redux"
import EntityPageFilter from "./../../../EntityManagement/CommonComponents/EntityPageFilter"
import "./../../scss/entity.scss"

class MigratedFirmListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  async componentWillMount() {}

  async componentDidMount() {}

  fuse = e => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    const res = fuse.search(search)
    return res
  }

  render() {
    return (
      <EntityPageFilter
        listType= {this.props.listType || "client-prospect"}
        auth={this.props.auth}
        activeTab={this.props.activeTab}
        nav2={this.props.nav2 || ""}
        nav1={this.props.nav1 || ""}
        searchPref="mast-cltprosp-migrated"
        title="Clients / Prospects"
      />
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MigratedFirmListContainer)
)
