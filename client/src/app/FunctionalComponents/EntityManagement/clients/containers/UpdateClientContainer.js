import {validatClientsDetail} from "Validation/clients"
import {
  saveClientFirmDetail,
  getClientFirmDetailById,
  deleteLinkCusipBorrower,
  updateClientFirm,
  searchUsersList,
  getIssuerList
} from "AppState/actions/AdminManagement/admTrnActions"
import {confirmAlert} from "react-confirm-alert"
import "react-confirm-alert/src/react-confirm-alert.css"
import React from "react"
import {toast} from "react-toastify"
import {withRouter} from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import {connect} from "react-redux"
import {getPicklistValues, convertError, checkEmptyElObject, checkPlatformAdmin, checkNavAccessEntitlrment} from "GlobalUtils/helpers"
import FirmAddressListForm from "./../../CommonComponents/FirmAddressListForm"
import FirmProfileForm from "./../components/FirmProfileForm"
import EntityType from "./../components/EntityType"
import ContactInfo from "./../components/ContactInfo"
import LinkCusip from "./../components/LinkCusip"
import LinkBorrower from "./../components/LinkBorrower"
import Loader from "../../../../GlobalComponents/Loader"
import "./../../scss/entity.scss"

const dateCompare = (startDate, endDate) => {
  if (startDate > endDate) 
    return 1
  else if (startDate < endDate) 
    return -1
  return 0
}
const initialAddressError = (errorFirmDetail, address, initialFirmDetails) => {
  const arr = ["officeEmails", "officePhone", "officeFax"]
  arr.map(item => {
    if (address[item].length > 0) {
      for (let index = 1; index < address[item].length; index++) {
        errorFirmDetail
          .addresses[0][item]
          .push(initialFirmDetails.addresses[0][item][0])
      }
    }
  })
}

class UpdateClientContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.loading = false
    this.state.errorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.cleanErrorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.handleSubmit = this
      .handleSubmit
      .bind(this)
    this.submitted = false
  }

  async componentWillMount() {
    this.setState({loading: true})
    const action = await checkNavAccessEntitlrment(this.props.match.params.nav2)
    const { edit, view } = action || {}
    let {canEdit, canView} = this.state
    const isPlatformAdmin = await checkPlatformAdmin()
    if (isPlatformAdmin) {
      canEdit = true
    } else {
      canEdit = edit
      canView = view
    }
    this.setState({canEdit, canView})
    if (!canEdit && !canView) {
      this
        .props
        .history
        .push("/mast-cltprosp")
    }
  }

  async componentDidMount() {
    this.setState({loadingPickLists: true})
    let pickListArray = getPicklistValues([
      "LKUPCOUNTRY",
      "LKUPPRIMARYSECTOR",
      "LKUPISSUERFLAG",
      "LKUPDEBTTYPE",
      "LKUPCRMBORROWER",
      "LKUPENTITYTYPE",
      "LKUPFIRMNAME",
      "LKUPEMMAREFERENCEENTITIES"
    ])
    const {entityId} = this.props.auth.userEntities
    const {firmDetails} = this.state
    let usersList = searchUsersList(entityId, "")
    firmDetails.addresses[0].isPrimary = true
    // firmDetails.addresses[0].isActive = true
    firmDetails.entityBorObl = []
    usersList = await usersList
    usersList = usersList.map(item => `${item.firstName} ${item.lastName}`)
    const [countryResult,
      primarySectors,
      issuerFlagsList,
      linkCusipDebType,
      relBorrowerObligor,
      entityTypeListResult,
      maFirmList,
      entityFirmList] = await pickListArray
    const entityTypeList = [
      {
        label: "Select Entity Type",
        value: ""
      }
    ]
    entityTypeListResult[1].forEach(item => {
      entityTypeList.push({label: item, value: item})
    })

    this.setState(prevState => ({
      ...prevState,
      ...{
        countryResult,
        primarySectorsList: primarySectors,
        issuerFlagsList,
        linkCusipDebType,
        loading: false,
        firmDetails,
        usersList,
        relBorrowerObligor,
        entityId,
        entityTypeList,
        entityTypeListResult,
        maFirmList,
        entityFirmList,
        loadingPickLists: false
      }
    }))
    if (this.props.match.params.nav2 !== "new" && this.props.match.params.nav2.length === 24) {
      this
        .props
        .getClientFirmDetailById(this.props.match.params.nav2)
    } else {
      this
        .props
        .history
        .push("/mast-cltprosp")
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.admClientDetail.firmDetailById && Object.keys(nextProps.admClientDetail.firmDetailById).length > 0 && this.props.match.params.nav2 !== "new" && !nextProps.admClientDetail.updated) {
      let {errorFirmDetail, oldFirmDetails} = this.state
      oldFirmDetails = cloneDeep(nextProps.admClientDetail.firmDetailById)
      const firmDetails = cloneDeep(nextProps.admClientDetail.firmDetailById)
      const addressList = cloneDeep(nextProps.admClientDetail.firmDetailById.addresses)
      const LinkCusipList = firmDetails.entityLinkedCusips !== null
        ? firmDetails.entityLinkedCusips
        : []
      const LinkBorrowerList = firmDetails.entityBorObl !== null
        ? firmDetails.entityBorObl
        : []
      firmDetails.addresses = [
        this
          .initialState()
          .firmDetails
          .addresses[0]
      ]
      firmDetails.entityLinkedCusips = [
        ...this
          .initialState()
          .firmDetails
          .entityLinkedCusips
      ]
      firmDetails.entityBorObl = [
        ...this
          .initialState()
          .firmDetails
          .entityBorObl
      ]
      errorFirmDetail = cloneDeep(this.initialState().firmDetails)
      initialAddressError(errorFirmDetail, firmDetails.addresses[0], this.initialState().firmDetails)
      const oldErrorFirmDetail = cloneDeep(errorFirmDetail)
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        LinkCusipList,
        LinkBorrowerList,
        errorFirmDetail,
        readOnlyFeatures: {
          msrbFirmNameReadOnly: true,
          msrbIdReadOnly: true,
          registrantTypeReadOnly: true,
          firmName: true,
          entityType: true
        },
        loading: false,
        oldFirmDetails,
        oldErrorFirmDetail
      }))
    }
  }

  /** ********************************************** onChangeAliases *********************** */
  onChangeAliases = (e, key) => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails.entityAliases[key] = e.target.value
    const validator = {
      entityAliases: [e.target.value]
    }
    this.setState({firmDetails})
    if (this.submitted) {
      const errorData = validatClientsDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name) {
          errorFirmDetail[e.target.name][key] = `${err.context.label} Required.`
          this.setState({errorFirmDetail})
        } else {
          errorFirmDetail[e.target.name][key] = ""
          this.setState({errorFirmDetail})
        }
      } else {
        errorFirmDetail[e.target.name][key] = ""
        this.setState({errorFirmDetail})
      }
    }
  }

  /** ************************************************* */
  onChangeAddOns = (e, idx) => {
    const {firmAddOns} = this.state.firmDetails
    firmAddOns[idx].serviceEnabled = !firmAddOns[idx].serviceEnabled
    this.setState({firmAddOns})
  }

  /** ************************ this is for onchange ********************************* */

  onChangeEntity = (e, key) => {
    let {firmDetails} = this.state
    if (key === "borrower") {
      firmDetails = {
        ...firmDetails,
        firmName: e
      }
      this.setState({firmDetails})
    }
  }

  onChangeFirmDetail = (e, key, depFields, type = "addresses") => {
    const {firmDetails, errorFirmDetail} = this.state
    let keyFirmDetail
    let errorKeyFirmDetail
    let validator
    if (e.target.name === "firmName") {
      e.target.value = e.target.value !== null
        ? e.target.value.name
        : ""
    }
    if (typeof key !== "undefined") {
      const value = e.target.type === "checkbox"
        ? !firmDetails.addresses[key][e.target.name]
        : e.target.value
      if (e.target.name == "zip1" || e.target.name == "zip2") {
        keyFirmDetail = firmDetails.addresses[key].zipCode
        errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
        validator = {
          addresses: [
            {
              zipCode: {
                [e.target.name]: e.target.value
              }
            }
          ]
        }
      } else {
        keyFirmDetail = firmDetails[type][key]
        errorKeyFirmDetail = errorFirmDetail[type][key]
        validator = {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      }
      keyFirmDetail[e.target.name] = value
      if (depFields) {
        depFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    } else if (e.target.name === "clientType") {
      this.setState({clientType: e.target.value})
    } else {
      if (e.target.name === "primarySectors") 
        firmDetails.secondarySectors = ""
      if (e.target.name === "issuerFlags") {
        this.toggleCheckbox(e.target.value)
        validator = {
          entityFlags: {
            [e.target.name]: firmDetails.entityFlags[e.target.name]
          }
        }
        errorKeyFirmDetail = errorFirmDetail.entityFlags
      } else {
        firmDetails[e.target.name] = e.target.value
        validator = {
          [e.target.name]: e.target.value
        }
        errorKeyFirmDetail = errorFirmDetail
      }
      if (depFields) {
        depFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    }
    if (this.submitted) {
      if (e.target.name === "borOblStartDate" || e.target.name === "borOblEndDate") {
        if (firmDetails[type][key].borOblStartDate !== "" && firmDetails[type][key].borOblEndDate !== "") {
          const dateVal = dateCompare(firmDetails[type][key].borOblStartDate, firmDetails[type][key].borOblEndDate)
          switch (dateVal) {
            case 1:
              errorKeyFirmDetail[e.target.name] = e.target.name === "borOblStartDate"
                ? "Start Date is should be less Exit Date."
                : "End Date is should be Greate Thant Start Date."
              break
            default:
              errorKeyFirmDetail[e.target.name] = ""
              break
          }
        } else if (firmDetails[type][key][e.target.name] === "") {
          const dateVal = e.target.name === "borOblStartDate"
            ? "Start Date"
            : "End Date"
          errorKeyFirmDetail[e.target.name] = `${dateVal} Required`
        } else {
          errorKeyFirmDetail[e.target.name] = ""
        }
        this.setState({errorKeyFirmDetail})
        return
      }
      const errorData = validatClientsDetail(validator)

      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name || err.context.key === e.target.name) {
          errorKeyFirmDetail[e.target.name] = `${err.context.label} Required.`
          this.setState({errorFirmDetail})
        } else {
          errorKeyFirmDetail[e.target.name] = ""
          this.setState({errorFirmDetail})
        }
      } else if (e.target.name !== "clientType") {
        errorKeyFirmDetail[e.target.name] = ""
        this.setState({errorFirmDetail})
      }
    }
  }

  getFirmDetails = item => {
    const {firmDetails, readOnlyFeatures, errorFirmDetail, entityFirmList, maFirmList} = this.state
    let {entityFirmListData} = this.state
    if (item.value !== "") {
      firmDetails.entityType = item.label
      firmDetails.firmName = ""
      if (item.label === "Other Municipal Advisor") 
        entityFirmListData = [...maFirmList[1]]
      if (["501c3 - Obligor", "Governmental Entity / Issuer"].includes(item.label)) 
        entityFirmListData = entityFirmList[2][item.label]
      errorFirmDetail.entityType = ""
    } else {
      firmDetails.entityType = ""
      firmDetails.firmName = ""
      entityFirmListData = []
      if (this.submitted) {
        errorFirmDetail.entityType = "Entity Type Required"
      } else {
        errorFirmDetail.entityType = ""
      }
    }
    this.setState({firmDetails, readOnlyFeatures, errorFirmDetail, entityFirmListData})
  }

  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const {firmDetails} = this.state
    const {issuerFlags} = firmDetails.entityFlags
    if (issuerFlags.includes(label)) {
      issuerFlags.splice(issuerFlags.indexOf(label), 1)
    } else {
      issuerFlags.push(label)
    }
    this.setState(prevState => ({
      issuerFlags: {
        ...prevState.firmDetails.entityFlags,
        issuerFlags
      }
    }))
  }

  /** **************************************this is for initial default value********************* */
  initialState = () => ({
    firmDetails: {
      entityFlags: {
        issuerFlags: []
      },
      entityAliases: [],
      isMuniVisorClient: false,
      msrbFirmName: "",
      msrbRegistrantType: "",
      msrbId: "",
      firmName: "",
      firmType: "",
      taxId: "",
      primarySectors: "",
      secondarySectors: "",
      firmLeadAdvisor: "",
      prevLeadAdvisor: "",
      prevAdvisorFirm: "",
      prevAdvisorContractExpire: "",
      primaryContactNameInEmma: "",
      primaryContactPhone: "",
      primaryContactEmail: "",
      addresses: [
        {
          addressName: "",
          isPrimary: false,
          isHeadQuarter: false,
          website: "",
          officePhone: [
            {
              countryCode: "",
              phoneNumber: "",
              extension: ""
            }
          ],
          officeFax: [
            {
              faxNumber: ""
            }
          ],
          officeEmails: [
            {
              emailId: ""
            }
          ],
          addressLine1: "",
          addressLine2: "",
          country: "",
          state: "",
          city: "",
          zipCode: {
            zip1: "",
            zip2: ""
          }
        }
      ],
      entityLinkedCusips: [
        {
          debtType: "",
          associatedCusip6: ""
        }
      ],
      entityBorObl: [
        {
          borOblRel: "",
          borOblFirmName: "",
          borOblDebtType: "",
          borOblCusip6: "",
          borOblStartDate: "",
          borOblEndDate: ""
        }
      ]
    },
    clientType: "Client",
    addressList: [],
    LinkCusipList: [],
    LinkBorrowerList: [],
    firmList: [],
    expanded: {
      linkcusip: false,
      linkborrower: false,
      business: false,
      addresslist: false,
      entityname: true,
      entitytype: true,
      contactinfo: true
    },
    readOnlyFeatures: {
      msrbFirmNameReadOnly: false,
      msrbIdReadOnly: false
    },
    msrbRegTypeResult: [],
    updated: true,
    disabled: false,
    countryResult: [],
    annualRevenue: [],
    secondarySectors: [],
    primarySectorsList: [],
    issuerFlagsList: [],
    linkCusipDebType: [],
    isPrimaryAddress: "",
    isEntityExists: {
      msrbId: "",
      firmName: ""
    },
    oldFirmDetails: {},
    canEdit: false,
    canView: false,
    entityTypeList: [],
    issuer: [],
    suggestions: [],
    borrower: [],
    filteredBorrower: [],
    firmError: ""
  })

  onSaveAddress = addressList => {
    this.setState(prevState => ({
      ...prevState,
      firmDetails: {
        ...prevState.firmDetails,
        addresses: addressList
      },
      addressList
    }), () => this.handleSubmit("address"))
  }

  /** ************************ this is for submit form ********************************* */
  async handleSubmit(type) {
    this.submitted = true
    const {firmDetails, errorFirmDetail, firmError} = this.state
    if (firmError !== "") {
      errorFirmDetail.firmName = firmError
      this.setState({errorFirmDetail})
      return
    }
    const clientData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      entityType: firmDetails.entityType,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      firmType: firmDetails.firmType,
      taxId: firmDetails.taxId,
      primarySectors: firmDetails.primarySectors,
      secondarySectors: firmDetails.secondarySectors
    }

    const entityTypeData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      entityFlags: firmDetails.entityFlags
    }

    const contactData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      firmLeadAdvisor: firmDetails.firmLeadAdvisor,
      prevLeadAdvisor: firmDetails.prevLeadAdvisor,
      prevAdvisorFirm: firmDetails.prevAdvisorFirm,
      prevAdvisorContractExpire: firmDetails.prevAdvisorContractExpire
        ? firmDetails.prevAdvisorContractExpire
        : "",
      primaryContactNameInEmma: firmDetails.primaryContactNameInEmma,
      primaryContactPhone: firmDetails.primaryContactPhone,
      primaryContactEmail: firmDetails.primaryContactEmail
    }

    const linkCusipData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      entityLinkedCusips: firmDetails.entityLinkedCusips
    }
    const linkBorrowerData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      entityBorObl: firmDetails.entityBorObl
    }

    let firmDetailsData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      entityFlags: firmDetails.entityFlags,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      firmType: firmDetails.firmType,
      taxId: firmDetails.taxId,
      primarySectors: firmDetails.primarySectors,
      secondarySectors: firmDetails.secondarySectors,
      firmLeadAdvisor: firmDetails.firmLeadAdvisor,
      prevLeadAdvisor: firmDetails.prevLeadAdvisor,
      prevAdvisorFirm: firmDetails.prevAdvisorFirm,
      prevAdvisorContractExpire: firmDetails.prevAdvisorContractExpire
        ? firmDetails.prevAdvisorContractExpire
        : "",
      primaryContactNameInEmma: firmDetails.primaryContactNameInEmma,
      primaryContactPhone: firmDetails.primaryContactPhone,
      primaryContactEmail: firmDetails.primaryContactEmail,
      addresses: firmDetails.addresses,
      entityLinkedCusips: firmDetails.entityLinkedCusips,
      entityBorObl: firmDetails.entityBorObl
    }

    try {
      let addresses = []
      let entityLinkedCusips = []
      let entityBorObl = []
      firmDetailsData
        .addresses
        .forEach(address => {
          if (!checkEmptyElObject(address)) {
            addresses.push(address)
          }
        })
      firmDetailsData.addresses = addresses

      firmDetailsData
        .entityLinkedCusips
        .forEach(address => {
          if (!checkEmptyElObject(address)) {
            entityLinkedCusips.push(address)
          }
        })
      firmDetailsData.entityLinkedCusips = entityLinkedCusips

      firmDetailsData
        .entityBorObl
        .forEach(address => {
          if (!checkEmptyElObject(address)) {
            entityBorObl.push(address)
          }
        })
      firmDetailsData.entityBorObl = entityBorObl

      let {addressList, LinkCusipList, LinkBorrowerList} = this.state
      if (entityBorObl.length > 0) {
        entityBorObl = entityBorObl.sort((a, b) => a._id > b._id)
        LinkBorrowerList = LinkBorrowerList.filter(obj => obj._id != entityBorObl[0]._id)
      }
      if (entityLinkedCusips.length > 0) {
        entityLinkedCusips = entityLinkedCusips.sort((a, b) => a._id > b._id)
        LinkCusipList = LinkCusipList.length > 0
          ? LinkCusipList.filter(obj => obj._id != entityLinkedCusips[0]._id)
          : []
      }

      const mergeAddresses = [...addressList]
      let errorData
      if (type === "client") {
        errorData = validatClientsDetail(clientData, type)
        firmDetailsData = clientData
        /* if(clientData.firmName !== "") {
          errorFirmDetail.firmName = ""
          this.setState({errorFirmDetail})
        } */
      } else if (type === "contact") {
        errorData = validatClientsDetail(contactData, type)
        firmDetailsData = contactData
      } else if (type === "address") {
        const addressData = {
          _id: firmDetails._id
            ? firmDetails._id
            : "",
          addresses: mergeAddresses
        }
        errorData = validatClientsDetail(addressData, type)
      } else if (type === "linkCusip") {
        errorData = validatClientsDetail(linkCusipData, type)
        firmDetailsData.entityLinkedCusips = [
          ...LinkCusipList,
          ...entityLinkedCusips
        ]
      } else if (type === "linkBorrower") {
        errorData = validatClientsDetail(linkBorrowerData, type)
        firmDetailsData.entityBorObl = [
          ...LinkBorrowerList,
          ...entityBorObl
        ]
      } else {
        errorData = validatClientsDetail(firmDetailsData, type)
        firmDetailsData.entityLinkedCusips = [
          ...LinkCusipList,
          ...entityLinkedCusips
        ]
        firmDetailsData.entityBorObl = [
          ...LinkBorrowerList,
          ...entityBorObl
        ]
      }
      if (errorData.error || this.state.isEntityExists.firmName !== "" || this.state.isEntityExists.msrbId !== "") {
        if (errorData.error) {
          convertError(errorFirmDetail, errorData)
        }
        this.setState({errorFirmDetail})
      } else {
        this.setState({loading: true})
        const response = await updateClientFirm({firmDetails: firmDetailsData, mergeAddresses})
        if (typeof response.error !== "undefined" && response.error !== null) {
          this.setState({loading: false})
        } else {
          this.setState({
            firmDetails,
            errorFirmDetail,
            loading: false,
            readOnlyFeatures: {
              msrbFirmNameReadOnly: false,
              msrbIdReadOnly: false
            }
          })
          this.submitted = false
          if (type === "all") {
            this
              .props
              .history
              .push("/mast-cltprosp")
          }
          toast("Client / Prospect (" + type + ") has been Updated!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    }
  }

  /** ************************ this is for add new address ********************************* */
  addNewBussinessAddress = (event, type, addresses) => {
    event.preventDefault()
    let isEmpty = false
    addresses && addresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true
      }
    })
    if (!isEmpty) {
      switch (type) {
        case "address":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .addresses
              .push(this.initialState().firmDetails.addresses[0])
            errorFirmDetail
              .addresses
              .push(this.initialState().firmDetails.addresses[0])
            return {firmDetails, errorFirmDetail}
          })break
        case "linkcusip":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .entityLinkedCusips
              .push(this.initialState().firmDetails.entityLinkedCusips[0])
            errorFirmDetail
              .entityLinkedCusips
              .push(this.initialState().firmDetails.entityLinkedCusips[0])
            return {firmDetails, errorFirmDetail}
          })break
        case "linkborrower":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .entityBorObl
              .push(this.initialState().firmDetails.entityBorObl[0])
            errorFirmDetail
              .entityBorObl
              .push(this.initialState().firmDetails.entityBorObl[0])
            return {firmDetails, errorFirmDetail}
          })break
        default:
          console.log("Default")
      }
    } else {
      toast(`Already and empty ${type}!`, {
        autoClose: 2000,
        type: toast.TYPE.INFO
      })
    }
  }

  /** ************************ this is for reset form ********************************* */
  resetBussinessAddress = (e, type) => {
    e.preventDefault()
    const address = cloneDeep(this.initialState().firmDetails.addresses[0])
    address.isPrimary = true
    const {firmDetails} = this.state
    const {entityBorObl, entityLinkedCusips} = firmDetails
    switch (type) {
      case "address":
        this.setState(prevState => ({
          firmDetails: {
            ...prevState.firmDetails,
            addresses: [address]
          },
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            addresses: [
              this
                .initialState()
                .firmDetails
                .addresses[0]
            ]
          }
        }))break
      case "linkcusip":
        this.setState(prevState => ({
          firmDetails: {
            ...prevState.firmDetails,
            entityLinkedCusips: [
              this
                .initialState()
                .firmDetails
                .entityLinkedCusips[0]
            ]
          },
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            entityLinkedCusips: [
              this
                .initialState()
                .firmDetails
                .entityLinkedCusips[0]
            ]
          }
        }))break
      case "linkborrower":
        if (entityLinkedCusips.length > 0) {
          this.setState(prevState => ({
            firmDetails: {
              ...prevState.firmDetails,
              entityBorObl: [
                this
                  .initialState()
                  .firmDetails
                  .entityBorObl[0]
              ]
            },
            errorFirmDetail: {
              ...prevState.errorFirmDetail,
              entityBorObl: [
                this
                  .initialState()
                  .firmDetails
                  .entityBorObl[0]
              ]
            }
          }))
        }
        break
      default:
        console.log("Default")
    }
  }

  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = e => {
    e.preventDefault()
    const {oldFirmDetails} = this.state
    const {entityAliases} = oldFirmDetails
    this.setState(prevState => ({
      firmDetails: {
        ...prevState.firmDetails,
        entityAliases
      },
      errorFirmDetail: {
        ...prevState.errorFirmDetail,
        entityAliases: cloneDeep(this.initialState().firmDetails.entityAliases)
      }
    }))
  }

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type, key) => {
    e.preventDefault()
    this.setState(prevState => {
      const addMoreData = {
        ...prevState.firmDetails
      }
      const errorFirmDetail = {
        ...prevState.errorFirmDetail
      }
      if (type === "aliases") {
        addMoreData
          .entityAliases
          .push("")
        errorFirmDetail
          .entityAliases
          .push("")
      } else {
        addMoreData
          .addresses[key][type]
          .push(this.initialState().firmDetails.addresses[0][type][0])
        errorFirmDetail
          .addresses[key][type]
          .push(this.initialState().firmDetails.addresses[0][type][0])
      }
      return {addMoreData}
    })
  }

  /** *****************************************Toggle Button********************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const {expanded} = this.state
    Object
      .keys(expanded)
      .map(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
    this.setState({expanded})
  }

  /** *********************************************** update LinkCusip*********************************** */
  updateLinkCusipBorrower = (e, id, type = "entityBorObl") => {
    e.preventDefault()
    const {LinkCusipList, LinkBorrowerList} = this.state
    let cusipBorrowerVal
    if (type === "entityLinkedCusips") {
      cusipBorrowerVal = LinkCusipList.find(item => item._id == id)
    } else {
      cusipBorrowerVal = LinkBorrowerList.find(item => item._id == id)
    }
    this.setState(prevState => {
      const {errorFirmDetail, firmDetails} = prevState
      errorFirmDetail[type] = this
        .initialState()
        .firmDetails[type]
      firmDetails[type] = [cusipBorrowerVal]
      return {firmDetails, errorFirmDetail}
    })
  }

  deleteLinkCusipBorrower = async(e, id, type = "entityBorObl") => {
    e.preventDefault()
    const {firmDetails} = this.state
    let {LinkCusipList, LinkBorrowerList} = this.state
    let {entityLinkedCusips, entityBorObl} = firmDetails
    confirmAlert({
      customUI: ({onClose}) => (
        <div className="modal is-active">
          <div className="modal-background" role="presentation"/>
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title"/>
              <button className="delete" onClick={onClose}/>
            </header>
            <section>
              <div className="content custom-ui">
                <h1>Are you sure?</h1>
                <p>You want to delete this item?</p>
                <button onClick={onClose}>No</button>
                <button
                  onClick={async() => {
                  const response = await deleteLinkCusipBorrower(firmDetails._id, id, type)if (response.ok) {
                    if (type === "entityLinkedCusips") {
                      LinkCusipList = LinkCusipList.filter(item => item._id !== id)entityLinkedCusips = entityLinkedCusips.filter(item => item._id !== id)
                    } else {
                      LinkBorrowerList = LinkBorrowerList.filter(item => item._id !== id)entityBorObl = entityBorObl.filter(item => item._id !== id)
                    }
                    this.setState(prevState => ({
                      ...prevState,
                      LinkBorrowerList,
                      LinkCusipList,
                      firmDetails: {
                        ...prevState.firmDetails,
                        entityBorObl: entityBorObl.length > 0
                          ? entityBorObl
                          : this
                            .initialState()
                            .firmDetails
                            .entityBorObl,
                        entityLinkedCusips: entityLinkedCusips.length > 0
                          ? entityLinkedCusips
                          : this
                            .initialState()
                            .firmDetails
                            .entityLinkedCusips
                      }
                    }))
                  }
                  onClose()
                }}>
                  Yes, Delete it!
                </button>
              </div>
            </section>
          </div>
        </div>
      )
    })
  }

  getEntityIssuerList = async inputVal => {
    const {firmDetails} = this.state
    const {entityType} = firmDetails
    if (!inputVal || !entityType) {
      return {options: []}
    }
    const issuerList = await getIssuerList({inputVal, entityType})
    if (issuerList.status === 200 && issuerList.data) 
      return {options: issuerList.data}
    return {options: []}
  }

  onError = (errorFirmName) => {
    const firmError = errorFirmName.error || ""
    this.setState({firmError})
  }

  render() {
    const loading = () => <Loader/>
    const {
      firmDetails,
      entityTypeList,
      entityTypeListResult,
      LinkCusipList,
      LinkBorrowerList,
      usersList,
      countryResult,
      errorFirmDetail,
      firmList,
      numberOfEmployee,
      readOnlyFeatures,
      msrbRegTypeResult,
      primarySectorsList,
      addressList,
      canEdit,
      canView
    } = this.state
    console.log("======>>>>", entityTypeList)

    return (
      <section className="firms">
        {this.state.loading
          ? loading()
          : null}
        <section className="accordions">
          {firmDetails.entityType === "Governmental Entity / Issuer" && <EntityType
            expanded={this.state.expanded}
            toggleButton={this.toggleButton}
            onChangeIssuerFlag={this.onChangeFirmDetail}
            issuerFlagsList={this.state.issuerFlagsList[1]}
            entityFlags={this.state.firmDetails.entityFlags}
            errorFirmDetail={this.state.errorFirmDetail}
            canEdit={canEdit}
            canView={canView}
            onSave={this.handleSubmit}/>}
          <article
            className={this.state.expanded.entityname
            ? "accordion is-active"
            : "accordion"}>
            <div className="accordion-header toggle">
              <p
                onClick={event => {
                this.toggleButton(event, "entityname")
              }}>
                Client / Prospect
              </p>
            </div>
            {this.state.expanded.entityname && <FirmProfileForm
              firmDetails={firmDetails}
              onChangeFirmDetail={this.onChangeFirmDetail}
              errorFirmDetail={errorFirmDetail}
              firmList={firmList}
              getFirmDetails={this.getFirmDetails}
              addMore={this.addMore}
              onChangeAliases={this.onChangeAliases}
              resetAliases={this.resetAliases}
              readOnlyFeatures={readOnlyFeatures}
              msrbRegTypeResult={msrbRegTypeResult}
              numberOfEmployee={numberOfEmployee}
              primarySectorsList={primarySectorsList}
              deleteAliases={this.deleteAliases}
              clientType={this.state.clientType}
              updated={this.state.updated}
              disabled={this.state.disabled}
              msrbFirmListResult={this.state.msrbFirmListResult}
              isEntityExists={this.state.isEntityExists}
              entityTypeList={entityTypeList}
              entityTypeListResult={entityTypeListResult}
              maFirmList={this.state.maFirmList}
              entityFirmList={this.state.entityFirmList}
              entityFirmListData={this.state.entityFirmListData}
              getEntityFirmList={this.getEntityIssuerList}
              onClearRequested={this.onClearRequested}
              canEdit={canEdit}
              canView={canView}
              onChangeEntity={this.onChangeEntity}
              onSave={this.handleSubmit}
              suggestions={this.state.suggestions}
              getSuggestionValue={this.getSuggestionValue}
              onError={this.onError}
              firmError={this.state.firmError || ""}
              busy={this.state.loadingPickList}/>}
          </article>
          {firmDetails.entityType === "Governmental Entity / Issuer" && (<EntityType
            expanded={this.state.expanded}
            toggleButton={this.toggleButton}
            onChangeIssuerFlag={this.onChangeFirmDetail}
            issuerFlagsList={this.state.issuerFlagsList[1]}
            entityFlags={this.state.firmDetails.entityFlags}
            errorFirmDetail={this.state.errorFirmDetail}
            canEdit={canEdit}
            canView={canView}
            onSave={this.handleSubmit}
            busy={this.state.loadingPickLists}/>)}
          <ContactInfo
            expanded={this.state.expanded}
            toggleButton={this.toggleButton}
            errorFirmDetail={errorFirmDetail}
            firmDetails={firmDetails}
            onChangeFirmDetail={this.onChangeFirmDetail}
            usersList={usersList}
            canEdit={canEdit}
            canView={canView}
            onSave={this.handleSubmit}
            busy={this.state.loadingPickLists}/>
          <FirmAddressListForm
            key={parseInt(1 * 30, 10)}
            errorFirmDetail={this.state.errorFirmDetail.addresses[0]}
            countryResult={countryResult}
            canEdit={canEdit}
            canView={canView}
            busy={this.state.loadingPickLists}
            addressList={this.state.addressList}
            onSaveAddress={this.onSaveAddress}/>
          <LinkCusip
            expanded={this.state.expanded}
            toggleButton={this.toggleButton}
            linkCusip={this.state.firmDetails.entityLinkedCusips}
            onChangeLinkCusip={this.onChangeFirmDetail}
            addLinkCusip={this.addNewBussinessAddress}
            resetLinkCusip={this.resetBussinessAddress}
            LinkCusipList={LinkCusipList}
            updateLinkCusip={this.updateLinkCusipBorrower}
            deleteLinkCusip={this.deleteLinkCusipBorrower}
            entityLinkedCusipsErrors={this.state.errorFirmDetail.entityLinkedCusips}
            linkCusipDebType={this.state.linkCusipDebType}
            deleteAddress={this.deleteAddress}
            canEdit={canEdit}
            canView={canView}
            onSave={this.handleSubmit}/>
          <LinkBorrower
            expanded={this.state.expanded}
            toggleButton={this.toggleButton}
            linkBorrower={firmDetails.entityBorObl}
            onChangelinkBorrower={this.onChangeFirmDetail}
            addlinkBorrower={this.addNewBussinessAddress}
            resetlinkBorrower={this.resetBussinessAddress}
            LinkBorrowerList={LinkBorrowerList}
            relBorrowerObligor={this.state.relBorrowerObligor}
            updateLinkBorrower={this.updateLinkCusipBorrower}
            deleteLinkBorrower={this.deleteLinkCusipBorrower}
            entityBorOblErrors={this.state.errorFirmDetail.entityBorObl}
            linkCusipDebType={this.state.linkCusipDebType}
            deleteAddress={this.deleteAddress}
            canEdit={canEdit}
            canView={canView}
            onSave={this.handleSubmit}/> {canEdit && (
            <div className="columns">
              <div className="column is-full">
                <div className="field is-grouped-center">
                  <div className="control">
                    <button className="button is-link" onClick={() => this.handleSubmit("all")}>
                      Save All
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </section>
      </section>
    )
  }
}

const mapStateToProps = state => {
  const {admClientDetail, auth} = state
  return {admClientDetail, auth}
}

const mapDispatchToProps = {
  saveClientFirmDetail,
  getClientFirmDetailById
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UpdateClientContainer))
