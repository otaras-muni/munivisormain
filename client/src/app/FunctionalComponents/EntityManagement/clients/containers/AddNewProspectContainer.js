import {validatClientsDetail} from "Validation/clients"
import {
  checkDuplicateEntity,
  saveClientFirmDetail,
  getClientFirmDetailById,
  deleteLinkCusipBorrower,
  addClientFirm,
  updateClientFirm,
  getIssuerList,
  searchUsersList
} from "AppState/actions/AdminManagement/admTrnActions"
import React from "react"
import {toast} from "react-toastify"
import {withRouter} from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import {connect} from "react-redux"
import {getPicklistValues, convertError, checkEmptyElObject} from "GlobalUtils/helpers"
import FirmAddressListForm from "./../../CommonComponents/FirmAddressListForm"
import FirmProfileForm from "./../components/FirmProfileForm"
import EntityType from "./../components/EntityType"
import ContactInfo from "./../components/ContactInfo"
import LinkCusip from "./../components/LinkCusip"
import LinkBorrower from "./../components/LinkBorrower"
import Loader from "../../../../GlobalComponents/Loader"
import "./../../scss/entity.scss"

const dateCompare = (startDate, endDate) => {
  if (startDate > endDate) 
    return 1
  else if (startDate < endDate) 
    return -1
  return 0
}
class AddNewProspectContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.loading = false
    this.state.errorFirmDetail = cloneDeep(this.initialState().firmDetails)
    this.cleanErrorFirmDetail = cloneDeep(this.initialState().firmDetails)
    this.handleSubmit = this
      .handleSubmit
      .bind(this)
    this.submitted = false
    this.routeNavigate = this
      .routeNavigate
      .bind(this)
  }

  async componentWillMount() {
    this.setState({loading: true})
    const {entityId} = this.props.auth.userEntities
    let usersList = []
    const {firmDetails} = this.state
    firmDetails.addresses[0].isPrimary = true
    usersList = searchUsersList(entityId, "")
    usersList = await usersList
    usersList = usersList.map(item => `${item.firstName} ${item.lastName}`)

    this.setState(prevState => ({
      ...prevState,
      ...{
        loading: false,
        firmDetails,
        usersList,
        entityId,
        loadingPickLists: true
      }
    }))
  }

  async componentDidMount() {
    this.setState({loadingPickLists: true})
    const pickListArray = getPicklistValues([
      "LKUPCOUNTRY",
      "LKUPPRIMARYSECTOR",
      "LKUPISSUERFLAG",
      "LKUPDEBTTYPE",
      "LKUPCRMBORROWER",
      "LKUPENTITYTYPE",
      "LKUPFIRMNAME",
      "LKUPEMMAREFERENCEENTITIES"
    ])
    const issuerList = await getIssuerList({participantType: "Governmental Entity / Issuer", entityType: "Prospects/Clients"})
    if (issuerList.status === 200 && issuerList.data) {
      this.setState({borrower: issuerList.data})
    }
    const [countryResult,
      primarySectors,
      issuerFlagsList,
      linkCusipDebType,
      relBorrowerObligor,
      entityTypeListResult,
      maFirmList,
      entityFirmList] = await pickListArray
    const entityTypeList = [
      {
        label: "Select Entity Type",
        value: ""
      }
    ]
    entityTypeListResult[1].forEach(item => {
      entityTypeList.push({label: item, value: item})
    })

    this.setState(prevState => ({
      ...prevState,
      ...{
        countryResult,
        primarySectorsList: primarySectors,
        issuerFlagsList,
        linkCusipDebType,
        relBorrowerObligor,
        entityTypeList,
        entityTypeListResult,
        maFirmList,
        entityFirmList,
        loadingPickLists: false
      }
    }))
  }

  /** ********************************************** onChangeAliases *********************** */
  onChangeAliases = (e, key) => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails.entityAliases[key] = e.target.value
    const validator = {
      entityAliases: [e.target.value]
    }
    this.setState({firmDetails})
    if (this.submitted) {
      const errorData = validatClientsDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name) {
          errorFirmDetail[e.target.name][key] = `${err.context.label} Required.`
          this.setState({errorFirmDetail})
        } else {
          errorFirmDetail[e.target.name][key] = ""
          this.setState({errorFirmDetail})
        }
      } else {
        errorFirmDetail[e.target.name][key] = ""
        this.setState({errorFirmDetail})
      }
    }
  }

  /** ************************ this is for onchange ********************************* */
  onChangeEntity = (e, key) => {
    let {firmDetails} = this.state
    if (key === "borrower") {
      firmDetails = {
        ...firmDetails,
        firmName: e
      }
      this.setState({firmDetails})
    }
  }

  onChangeFirmDetail = (e, key, depFields, type = "addresses") => {
    const {firmDetails, errorFirmDetail, isEntityExists} = this.state
    let keyFirmDetail
    let errorKeyFirmDetail
    let validator
    if (typeof key !== "undefined") {
      const value = e.target.type === "checkbox"
        ? !firmDetails.addresses[key][e.target.name]
        : e.target.value
      if (e.target.name === "zip1" || e.target.name === "zip2") {
        keyFirmDetail = firmDetails.addresses[key].zipCode
        errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
        validator = {
          addresses: [
            {
              zipCode: {
                [e.target.name]: e.target.value
              }
            }
          ]
        }
      } else {
        keyFirmDetail = firmDetails[type][key]
        errorKeyFirmDetail = errorFirmDetail[type][key]
        validator = {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      }
      keyFirmDetail[e.target.name] = value
      if (depFields) {
        depFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    } else if (e.target.name === "clientType") {
      this.setState({clientType: e.target.value})
    } else {
      if (e.target.name === "primarySectors") 
        firmDetails.secondarySectors = ""
      if (e.target.name === "issuerFlags") {
        this.toggleCheckbox(e.target.value)
        validator = {
          entityFlags: {
            [e.target.name]: firmDetails.entityFlags[e.target.name]
          }
        }
        errorKeyFirmDetail = errorFirmDetail.entityFlags
      } else {
        firmDetails[e.target.name] = e.target.value
        validator = {
          [e.target.name]: e.target.value
        }
        errorKeyFirmDetail = errorFirmDetail
      }
      if (depFields) {
        depFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    }
    if (this.submitted) {
      if (e.target.name === "borOblStartDate" || e.target.name === "borOblEndDate") {
        if (firmDetails[type][key].borOblStartDate !== "" && firmDetails[type][key].borOblEndDate !== "") {
          const dateVal = dateCompare(firmDetails[type][key].borOblStartDate, firmDetails[type][key].borOblEndDate)
          switch (dateVal) {
            case 1:
              errorKeyFirmDetail[e.target.name] = e.target.name === "borOblStartDate"
                ? "Start Date is should be less Exit Date."
                : "End Date is should be Greate Thant Start Date."
              break
            default:
              errorKeyFirmDetail[e.target.name] = ""
              break
          }
        } else if (firmDetails[type][key][e.target.name] === "") {
          const dateVal = e.target.name === "borOblStartDate"
            ? "Start Date"
            : "End Date"
          errorKeyFirmDetail[e.target.name] = `${dateVal} Required`
        } else {
          errorKeyFirmDetail[e.target.name] = ""
        }
        this.setState({errorKeyFirmDetail})
        return
      }
      const errorData = validatClientsDetail(validator)

      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name || err.context.key === e.target.name) {
          console.log("====>>>", err.context.label)
          errorKeyFirmDetail[e.target.name] = `${err.context.label} Required.`
          this.setState({errorFirmDetail})
        } else {
          errorKeyFirmDetail[e.target.name] = ""
          this.setState({errorFirmDetail})
        }
      } else if (e.target.name !== "clientType") {
        errorKeyFirmDetail[e.target.name] = ""
        this.setState({errorFirmDetail})
      }
    }
  }

  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, key, idx) => {
    e.preventDefault()
    const addresses = this.state.firmDetails.addresses[key]
    const {errorFirmDetail} = this.state
    addresses[type][idx][e.target.name] = e.target.value
    const validator = {
      addresses: [
        {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      ]
    }
    this.setState({addresses})
    if (this.submitted) {
      const errorData = validatClientsDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorFirmDetail.addresses[key][type][idx][e.target.name] = `${err.context.label} Required.`
        this.setState({errorFirmDetail})
      } else {
        errorFirmDetail.addresses[key][type][idx][e.target.name] = ""
        this.setState({errorFirmDetail})
      }
    }
  };

  getFirmDetails = (item) => {
    const {firmDetails, readOnlyFeatures, errorFirmDetail, entityFirmList, maFirmList} = this.state
    let {entityFirmListData} = this.state
    if (item.value !== "") {
      firmDetails.entityType = item.label
      firmDetails.firmName = ""
      if (item.label === "Other Municipal Advisor") 
        entityFirmListData = [...maFirmList[1]]
      if (["501c3 - Obligor", "Governmental Entity / Issuer"].includes(item.label)) 
        entityFirmListData = entityFirmList[2][item.label]
      errorFirmDetail.entityType = ""
    } else {
      firmDetails.entityType = ""
      firmDetails.firmName = ""
      entityFirmListData = []
      if (this.submitted) {
        errorFirmDetail.entityType = "Entity Type Required"
      } else {
        errorFirmDetail.entityType = ""
      }
    }
    this.setState({firmDetails, readOnlyFeatures, errorFirmDetail, entityFirmListData})
  }

  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const {firmDetails} = this.state
    const {issuerFlags} = firmDetails.entityFlags
    if (issuerFlags.includes(label)) {
      issuerFlags.splice(issuerFlags.indexOf(label), 1)
    } else {
      issuerFlags.push(label)
    }
    this.setState(prevState => ({
      issuerFlags: {
        ...prevState.firmDetails.entityFlags,
        issuerFlags
      }
    }))
  }

  /** **************************************this is for initial default value********************* */
  initialState = () => ({
    firmDetails: {
      entityFlags: {
        issuerFlags: []
      },
      entityAliases: [],
      isMuniVisorClient: false,
      msrbFirmName: "",
      msrbRegistrantType: "",
      msrbId: "",
      firmName: "",
      firmType: "",
      entityType: "",
      taxId: "",
      primarySectors: "",
      secondarySectors: "",
      firmLeadAdvisor: "",
      prevLeadAdvisor: "",
      prevAdvisorFirm: "",
      prevAdvisorContractExpire: "",
      primaryContactNameInEmma: "",
      primaryContactPhone: "",
      primaryContactEmail: "",
      addresses: [
        {
          addressName: "",
          isPrimary: false,
          isHeadQuarter: false,
          /* isActive: true, */
          website: "",
          officePhone: [
            {
              countryCode: "",
              phoneNumber: "",
              extension: ""
            }
          ],
          officeFax: [
            {
              faxNumber: ""
            }
          ],
          officeEmails: [
            {
              emailId: ""
            }
          ],
          addressLine1: "",
          addressLine2: "",
          country: "",
          state: "",
          city: "",
          zipCode: {
            zip1: "",
            zip2: ""
          },
          formatted_address: "",
          url: "",
          location: {
            longitude: "",
            latitude: ""
          }
        }
      ],
      entityLinkedCusips: [
        {
          debtType: "",
          associatedCusip6: ""
        }
      ],
      entityBorObl: [
        {
          borOblRel: "",
          borOblFirmName: "",
          borOblDebtType: "",
          borOblCusip6: "",
          borOblStartDate: "",
          borOblEndDate: ""
        }
      ]
    },
    clientType: "Prospect",
    addressList: [],
    LinkCusipList: [],
    LinkBorrowerList: [],
    firmList: [],
    expanded: {
      linkcusip: false,
      linkborrower: false,
      business: false,
      addresslist: false,
      entityname: true,
      entitytype: true,
      contactinfo: true
    },
    readOnlyFeatures: {
      msrbFirmNameReadOnly: false,
      msrbIdReadOnly: false,
      registrantTypeReadOnly: false,
      firName: false
    },
    msrbRegTypeResult: [],
    countryResult: [],
    annualRevenue: [],
    secondarySectors: [],
    primarySectorsList: [],
    issuerFlagsList: [],
    linkCusipDebType: [],
    usersList: [],
    isPrimaryAddress: "",
    isEntityExists: {
      msrbId: "",
      firmName: ""
    },
    issuer: [],
    suggestions: [],
    borrower: [],
    filteredBorrower: [],
    firmError: ""
  })

  onSaveAddress = addressList => {
    this.setState(prevState => ({
      ...prevState,
      firmDetails: {
        ...prevState.firmDetails,
        addresses: addressList
      },
      addressList
    }), () => this.handleSubmit("address"))
  }

  /** ************************ this is for submit form ********************************* */

  async handleSubmit(type) {
    this.submitted = true
    const {firmDetails, errorFirmDetail, expanded, addressList, firmError} = this.state
    expanded.business = true
    if (firmError !== "") {
      errorFirmDetail.firmName = firmError
      this.setState({errorFirmDetail})
      return
    }

    const clientData = {
      entityType: firmDetails.entityType,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      firmType: firmDetails.firmType,
      taxId: firmDetails.taxId,
      primarySectors: firmDetails.primarySectors,
      secondarySectors: firmDetails.secondarySectors,
      entityFlags: firmDetails.entityType === "Governmental Entity / Issuer"
        ? firmDetails.entityFlags
        : {}
    }

    const contactData = {
      firmLeadAdvisor: firmDetails.firmLeadAdvisor,
      prevLeadAdvisor: firmDetails.prevLeadAdvisor,
      prevAdvisorFirm: firmDetails.prevAdvisorFirm,
      prevAdvisorContractExpire: firmDetails.prevAdvisorContractExpire
        ? firmDetails.prevAdvisorContractExpire
        : "",
      primaryContactNameInEmma: firmDetails.primaryContactNameInEmma,
      primaryContactPhone: firmDetails.primaryContactPhone,
      primaryContactEmail: firmDetails.primaryContactEmail
    }

    const addressData = {
      addresses: firmDetails.addresses
    }

    const linkCusipData = {
      entityLinkedCusips: firmDetails.entityLinkedCusips
    }
    const linkBorrowerData = {
      entityBorObl: firmDetails.entityBorObl
    }

    let firmDetailsData = {
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      firmType: firmDetails.firmType,
      entityType: firmDetails.entityType,
      taxId: firmDetails.taxId,
      primarySectors: firmDetails.primarySectors,
      secondarySectors: firmDetails.secondarySectors,
      firmLeadAdvisor: firmDetails.firmLeadAdvisor,
      prevLeadAdvisor: firmDetails.prevLeadAdvisor,
      prevAdvisorFirm: firmDetails.prevAdvisorFirm,
      prevAdvisorContractExpire: firmDetails.prevAdvisorContractExpire,
      primaryContactNameInEmma: firmDetails.primaryContactNameInEmma,
      primaryContactPhone: firmDetails.primaryContactPhone,
      primaryContactEmail: firmDetails.primaryContactEmail,
      addresses: firmDetails.addresses,
      entityLinkedCusips: firmDetails.entityLinkedCusips,
      entityBorObl: firmDetails.entityBorObl
    }
    if (firmDetails.entityType === "Governmental Entity / Issuer") 
      firmDetailsData.entityFlags = firmDetails.entityFlags
    try {
      const linkCusIp = []
      const entityBorObl = []
      firmDetailsData
        .entityLinkedCusips
        .forEach(address => {
          if (!checkEmptyElObject(address)) {
            linkCusIp.push(address)
          }
        })
      if (linkCusIp.length === 0) {
        firmDetailsData.entityLinkedCusips = []
      }

      firmDetailsData
        .entityBorObl
        .forEach(address => {
          if (!checkEmptyElObject(address)) {
            entityBorObl.push(address)
          }
        })
      if (entityBorObl.length === 0) {
        firmDetailsData.entityBorObl = []
      }

      let errorData
      if (type === "client") {
        errorData = validatClientsDetail(clientData, type)
        firmDetailsData = clientData
        /* if(clientData.firmName !== "") {
          errorFirmDetail.firmName = ""
          this.setState({errorFirmDetail})
        } */
      } else if (type === "contact") {
        errorData = validatClientsDetail(contactData, type)
        firmDetailsData = contactData
      } else if (type === "address") {
        errorData = validatClientsDetail(addressData, type)
        firmDetailsData = addressData
      } else if (type === "linkCusip") {
        errorData = validatClientsDetail(linkCusipData, type)
        firmDetailsData = linkCusipData
      } else if (type === "linkBorrower") {
        errorData = validatClientsDetail(linkBorrowerData, type)
        firmDetailsData = linkBorrowerData
      } else {
        errorData = validatClientsDetail(firmDetailsData, type)
      }

      const addresses = []
      const addr = firmDetailsData.addresses && firmDetailsData
        .addresses
        .forEach(items => {
          if (!checkEmptyElObject(items)) {
            addresses.push(items)
          }
        })
      firmDetailsData.addresses = addresses
      const mergeAddresses = [...addresses]

      if (errorData.error || this.state.isEntityExists.firmName !== "" || this.state.isEntityExists.msrbId !== "") {
        if (errorData.error) {
          convertError(errorFirmDetail, errorData)
        }
        this.setState({errorFirmDetail, expanded})
      } else {
        const {entityId} = this.props.auth.userEntities
        this.setState({loading: true})
        const state = {}
        let response
        if (type === "client") {
          response = await addClientFirm({firmDetails: firmDetailsData, entityId, clientType: "Prospect"})
          if (response) {
            this.setState({
              _id: response._id || ""
            })
          }
        } else if (type === "contact" || type === "address" || type === "linkCusip" || type === "linkBorrower" || type === "all") {
          firmDetailsData = {
            _id: this.state._id,
            ...firmDetailsData
          }
          response = await updateClientFirm({firmDetails: firmDetailsData, mergeAddresses})
        }

        if (typeof response.error !== "undefined" && response.error !== null) {
          if (response.error !== null) 
            this.setState({loading: false})
        } else {
          this.setState({
            firmDetails,
            errorFirmDetail,
            loading: false,
            readOnlyFeatures: {
              msrbFirmNameReadOnly: false,
              msrbIdReadOnly: false
            }
          })
          this.submitted = false
          toast("Prospect (" + type + ") has been Created!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
          if (type === "all") {
            this
              .props
              .history
              .push("/mast-cltprosp")
          }
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    }
  }

  /** ************************ this is for add new address ********************************* */
  addNewBussinessAddress = (event, type) => {
    event.preventDefault()
    const {firmDetails} = this.state
    const {addresses} = firmDetails
    let isEmpty = false
    addresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true
      }
    })
    if (!isEmpty) {
      switch (type) {
        case "address":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .addresses
              .push(this.initialState().firmDetails.addresses[0])
            errorFirmDetail
              .addresses
              .push(this.initialState().firmDetails.addresses[0])
            return {firmDetails, errorFirmDetail}
          })
          break
        case "linkcusip":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .entityLinkedCusips
              .push(this.initialState().firmDetails.entityLinkedCusips[0])
            errorFirmDetail
              .entityLinkedCusips
              .push(this.initialState().firmDetails.entityLinkedCusips[0])
            return {firmDetails, errorFirmDetail}
          })
          break
        case "linkborrower":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .entityBorObl
              .push(this.initialState().firmDetails.entityBorObl[0])
            errorFirmDetail
              .entityBorObl
              .push(this.initialState().firmDetails.entityBorObl[0])
            return {firmDetails, errorFirmDetail}
          })
          break
        default:
          console.log("Default")
      }
    } else {
      toast(`Already an empty ${type}!`, {
        autoClose: 2000,
        type: toast.TYPE.INFO
      })
    }
  }

  /** ************************ this is for reset form ********************************* */
  resetBussinessAddress = (e, type) => {
    e.preventDefault()
    const address = cloneDeep(this.initialState().firmDetails.addresses[0])
    address.isPrimary = true
    /* address.isActive = true */
    const {firmDetails} = this.state
    const {entityBorObl, entityLinkedCusips} = firmDetails
    switch (type) {
      case "address":
        this.setState(prevState => ({
          firmDetails: {
            ...prevState.firmDetails,
            addresses: [address]
          },
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            addresses: [
              this
                .initialState()
                .firmDetails
                .addresses[0]
            ]
          }
        }))
        break
      case "linkcusip":
        this.setState(prevState => ({
          firmDetails: {
            ...prevState.firmDetails,
            entityLinkedCusips: [
              this
                .initialState()
                .firmDetails
                .entityLinkedCusips[0]
            ]
          },
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            entityLinkedCusips: [
              this
                .initialState()
                .firmDetails
                .entityLinkedCusips[0]
            ]
          }
        }))
        break
      case "linkborrower":
        if (entityLinkedCusips.length > 0) {
          this.setState(prevState => ({
            firmDetails: {
              ...prevState.firmDetails,
              entityBorObl: [
                this
                  .initialState()
                  .firmDetails
                  .entityBorObl[0]
              ]
            },
            errorFirmDetail: {
              ...prevState.errorFirmDetail,
              entityBorObl: [
                this
                  .initialState()
                  .firmDetails
                  .entityBorObl[0]
              ]
            }
          }))
        }
        break
      default:
        console.log("Default")
    }
  }

  routeNavigate() {
    this
      .props
      .history
      .push("new")
  }

  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = e => {
    e.preventDefault()
    if (this.props.admClientDetail && this.props.admClientDetail.firmDetailById && !this.props.admClientDetail.updated && this.props.match.params.nav2 !== "new") {
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases: cloneDeep(this.props.admClientDetail.firmDetailById.entityAliases)
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: []
        }
      }))
    } else if (this.props.admClientDetail && this.props.admClientDetail.updatedFirmDetails && this.props.admClientDetail.updated && this.props.match.params.nav2 !== "new") {
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases: cloneDeep(this.props.admClientDetail.updatedFirmDetails.entityAliases)
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: []
        }
      }))
    } else {
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases: []
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: []
        }
      }))
    }
  }

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type, key) => {
    e.preventDefault()
    this.setState(prevState => {
      const addMoreData = {
        ...prevState.firmDetails
      }
      const errorFirmDetail = {
        ...prevState.errorFirmDetail
      }
      if (type === "aliases") {
        addMoreData
          .entityAliases
          .push("")
        errorFirmDetail
          .entityAliases
          .push("")
      } else {
        addMoreData
          .addresses[key][type]
          .push(this.initialState().firmDetails.addresses[0][type][0])
        errorFirmDetail
          .addresses[key][type]
          .push(this.initialState().firmDetails.addresses[0][type][0])
      }
      return {addMoreData}
    })
  }

  /** *****************************************Toggle Button********************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const {expanded} = this.state
    Object
      .keys(expanded)
      .map(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
    this.setState({expanded})
  }

  /** *********************************************** update LinkCusip*********************************** */
  updateLinkCusipBorrower = (e, id, type = "entityBorObl") => {
    e.preventDefault()
    const {LinkCusipList, LinkBorrowerList} = this.state
    let cusipBorrowerVal
    if (type === "entityLinkedCusips") {
      cusipBorrowerVal = LinkCusipList.find(item => item._id == id)
    } else {
      cusipBorrowerVal = LinkBorrowerList.find(item => item._id == id)
    }
    this.setState(prevState => {
      const {errorFirmDetail, firmDetails} = prevState
      errorFirmDetail[type] = this
        .initialState()
        .firmDetails[type]
      firmDetails[type] = [cusipBorrowerVal]
      return {firmDetails, errorFirmDetail}
    })
  }

  deleteLinkCusipBorrower = async(e, id, type = "entityBorObl") => {
    e.preventDefault()
    const {firmDetails} = this.state
    let {LinkCusipList, LinkBorrowerList} = this.state
    const response = await deleteLinkCusipBorrower(firmDetails._id, id, type)
    if (response.ok) {
      if (type === "entityLinkedCusips") {
        LinkCusipList = LinkCusipList.filter(item => item._id !== id)
      } else {
        LinkBorrowerList = LinkBorrowerList.filter(item => item._id !== id)
      }
      this.setState(prevState => ({
        ...prevState,
        LinkBorrowerList,
        LinkCusipList
      }))
    }
  }

  deleteAliases = idx => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails
      .entityAliases
      .splice(idx, 1)
    errorFirmDetail
      .entityAliases
      .splice(idx, 1)
    this.setState({firmDetails, errorFirmDetail})
  }

  onChangeAddressType = (e, idx) => {
    const {firmDetails} = this.state
    let {addressList} = this.state
    let {addresses} = firmDetails
    if (e.target.name === "isPrimary") {
      firmDetails.addresses[idx].isPrimary = !firmDetails.addresses[idx].isPrimary
    } else {
      firmDetails.addresses[idx].isPrimary = false
      firmDetails.addresses[idx].isHeadQuarter = true
    }
    if (this.submitted) {
      addressList = addressList.length > 0
        ? addressList.filter(obj => obj._id != addresses[0]._id)
        : []
      addresses = addresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [
        ...addressList,
        ...addresses
      ]
    }
    this.setState({firmDetails})
  }

  deleteAddress = (e, type, idx) => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails[type].splice(idx, 1)
    errorFirmDetail[type].splice(idx, 1)
    this.setState({firmDetails, errorFirmDetail})
  }

  checkDuplicateFirm = async e => {
    // const {value,name} = e.target
    const {isEntityExists, firmDetails} = this.state
    const {firmName} = firmDetails
    if (firmName.length > 0) {
      const result = await checkDuplicateEntity(firmName)
      if (result.success.isExist) {
        isEntityExists.firmName = "Firms Name is Already Exists"
      } else {
        isEntityExists.firmName = ""
      }
    } else {
      isEntityExists.firmName = ""
    }
    this.setState({isEntityExists})
  }

  getEntity = async() => {
    const {firmDetails} = this.state
    const {entityType} = firmDetails
    if (!entityType) {
      this.setState({suggestions: []})
    }
    const input = ""
    const issuerList = await getIssuerList({input, participantType: entityType, entityType: "Prospects/Clients"})
    if (issuerList.status === 200 && issuerList.data) {
      this.setState({issuer: issuerList.data})
    }
  }

  getEntityIssuerList = async inputVal => {
    const {issuer} = this.state
    const input = inputVal && inputVal
      .value
      .trim()
    if (!input) {
      this.setState({suggestions: []})
    }
    const firmName = issuer && issuer.filter(item => item.firmName.toLowerCase().includes(input.toLowerCase()))
    const firm = firmName && firmName.filter(item => item.firmName.toLowerCase())
    this.setState({
      suggestions: firm || []
    })
    console.log("===============firm========================>", firm)
  }

  onClearRequested = () => {
    this.setState({suggestions: []})
  }

  getBorrowerList = async inputVal => {
    const input = inputVal && inputVal
      .value
      .trim()
    this.getBorrower(input)
  }

  getBorrower = async inputVal => {
    const {borrower} = this.state
    const firmName = borrower && borrower.filter(item => item.firmName.toLowerCase().includes(inputVal.toLowerCase()))
    const firm = firmName && firmName.filter(item => item.firmName.toLowerCase())
    this.setState({
      filteredBorrower: firm || []
    })
    console.log("===============firm========================>", firm)
  }

  onBorrowerClear = () => {
    this.setState({filteredBorrower: []})
  }

  onError = (errorFirmName) => {
    const firmError = errorFirmName.error || ""
    this.setState({firmError})
  }

  render() {
    const loading = () => <Loader/>
    const {
      firmDetails,
      errorFirmDetail,
      usersList,
      firmList,
      _id,
      countryResult
    } = this.state
    return (
      <div id="main">
        <section className="firms">
          {this.state.loading
            ? loading()
            : null}
          <section className="accordions">
            {firmDetails.entityType === "Governmental Entity / Issuer" && (<EntityType
              expanded={this.state.expanded}
              toggleButton={this.toggleButton}
              onChangeIssuerFlag={this.onChangeFirmDetail}
              issuerFlagsList={this.state.issuerFlagsList[1]}
              entityFlags={firmDetails.entityFlags}
              errorFirmDetail={errorFirmDetail}
              canEdit
              canView={false}
              busy={this.state.loadingPickLists}/>)}
            <article
              className={this.state.expanded.entityname
              ? "accordion is-active"
              : "accordion"}>
              <div className="accordion-header toggle">
                <p
                  onClick={event => {
                  this.toggleButton(event, "entityname")
                }}>
                  Entity Name
                </p>
                {this.state.expanded.entityname
                  ? (<i className="fas fa-chevron-up"/>)
                  : (<i className="fas fa-chevron-down"/>)}
              </div>
              {this.state.expanded.entityname && <FirmProfileForm
                firmDetails={firmDetails}
                onChangeFirmDetail={this.onChangeFirmDetail}
                errorFirmDetail={errorFirmDetail}
                firmList={firmList}
                getFirmDetails={this.getFirmDetails}
                addMore={this.addMore}
                onChangeAliases={this.onChangeAliases}
                resetAliases={this.resetAliases}
                readOnlyFeatures={this.state.readOnlyFeatures}
                msrbRegTypeResult={this.state.msrbRegTypeResult}
                secondarySectors={this.state.secondarySectors}
                numberOfEmployee={this.state.numberOfEmployee}
                primarySectorsList={this.state.primarySectorsList}
                deleteAliases={this.deleteAliases}
                clientType={this.state.clientType}
                msrbFirmListResult={this.state.msrbFirmListResult}
                checkDuplicateFirm={this.checkDuplicateFirm}
                isEntityExists={this.state.isEntityExists}
                entityTypeList={this.state.entityTypeList}
                entityTypeListResult={this.state.entityTypeListResult}
                maFirmList={this.state.maFirmList}
                entityFirmList={this.state.entityFirmList}
                entityFirmListData={this.state.entityFirmListData}
                getEntityFirmList={this.getEntityIssuerList}
                onClearRequested={this.onClearRequested}
                suggestions={this.state.suggestions}
                getSuggestionValue={this.getSuggestionValue}
                canEdit
                canView={false}
                onChangeEntity={this.onChangeEntity}
                onSave={this.handleSubmit}
                onError={this.onError}
                firmError={this.state.firmError || ""}
                busy={this.state.loadingPickLists}/>}
            </article>
            {_id
              ? (
                <div>
                  <ContactInfo
                    expanded={this.state.expanded}
                    toggleButton={this.toggleButton}
                    errorFirmDetail={errorFirmDetail}
                    firmDetails={firmDetails}
                    onChangeFirmDetail={this.onChangeFirmDetail}
                    usersList={usersList}
                    canEdit
                    canView={false}
                    onSave={this.handleSubmit}
                    busy={this.state.loadingPickLists}/>
                  <FirmAddressListForm
                    key={parseInt(1 * 30, 10)}
                    errorFirmDetail={this.state.errorFirmDetail.addresses[0]}
                    countryResult={countryResult}
                    onChangeAddressType={this.onChangeAddressType}
                    canEdit={true}
                    canView={true}
                    busy={this.state.loadingPickList}
                    addressList={firmDetails.addresses}
                    onSaveAddress={this.onSaveAddress}/>
                  <LinkCusip
                    expanded={this.state.expanded}
                    toggleButton={this.toggleButton}
                    linkCusip={firmDetails.entityLinkedCusips}
                    onChangeLinkCusip={this.onChangeFirmDetail}
                    addLinkCusip={this.addNewBussinessAddress}
                    resetLinkCusip={this.resetBussinessAddress}
                    LinkCusipList={this.state.LinkCusipList}
                    updateLinkCusip={this.updateLinkCusipBorrower}
                    deleteLinkCusip={this.deleteLinkCusipBorrower}
                    entityLinkedCusipsErrors={errorFirmDetail.entityLinkedCusips}
                    linkCusipDebType={this.state.linkCusipDebType}
                    deleteAddress={this.deleteAddress}
                    canEdit
                    canView={false}
                    onSave={this.handleSubmit}
                    busy={this.state.loadingPickLists}/>
                  <LinkBorrower
                    expanded={this.state.expanded}
                    toggleButton={this.toggleButton}
                    linkBorrower={firmDetails.entityBorObl}
                    onChangelinkBorrower={this.onChangeFirmDetail}
                    addlinkBorrower={this.addNewBussinessAddress}
                    resetlinkBorrower={this.resetBussinessAddress}
                    LinkBorrowerList={this.state.LinkBorrowerList}
                    updateLinkBorrower={this.updateLinkCusipBorrower}
                    deleteLinkBorrower={this.deleteLinkCusipBorrower}
                    entityBorOblErrors={errorFirmDetail.entityBorObl}
                    linkCusipDebType={this.state.linkCusipDebType}
                    deleteAddress={this.deleteAddress}
                    relBorrowerObligor={this.state.relBorrowerObligor}
                    canEdit
                    canView={false}
                    onSave={this.handleSubmit}
                    getBorrowerList={this.getBorrowerList}
                    onBorrowerClear={this.onBorrowerClear}
                    suggestions={this.state.filteredBorrower}
                    getSuggestionValue={this.getSuggestionValue}
                    busy={this.state.loadingPickLists}/>
                </div>
              )
              : null}

            {_id
              ? (
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={() => this.handleSubmit("all")}>
                          Save All
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              )
              : null}
          </section>
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const {admClientDetail, auth} = state
  return {admClientDetail, auth}
}

const mapDispatchToProps = {
  saveClientFirmDetail,
  getClientFirmDetailById
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddNewProspectContainer))
