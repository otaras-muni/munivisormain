import React from "react"
import { connect } from "react-redux"
import "react-confirm-alert/src/react-confirm-alert.css"
import { Redirect } from "react-router"
import ClientsView from "./ClientsView"
import {checkSupervisorControls} from "../../../StateManagement/actions/CreateTransaction"

class ClientsMain extends React.Component {
  constructor() {
    super()
    this.state = {
      svControls: {}
    }
  }
  async componentDidMount() {
    const svControls = await checkSupervisorControls()
    this.setState({ svControls })
  }

  render () {
    const { nav1, nav2, nav3, nav, audit } = this.props
    const { svControls } = this.state
    if (nav2) {
      return (
        <ClientsView nav1={nav1} id={nav2} accessibleTabs={Object.keys((nav && nav[nav1]) || {})} navComponent={nav3} audit={audit} svControls={svControls}/>
      )
    }
    return (
      <Redirect to="/mast-cltprosp"/>
    )
  }
}

const mapStateToProps = state => ({
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

export default connect(mapStateToProps, null)(ClientsMain)
