import React from "react"
import { Redirect, NavLink } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
import ClientsProspects from "../../ClientsProspects/ClientsProspects"
import ClientContactList from "./../contacts/containers/ContactListContainer"
import BusinessActivity from "./../business-activity/containers/BusinessActivityListContainer"

const TABS = [
  { path: "entity", label: "Entity" },
  { path: "business-activity", label: "Business Activity" },
  { path: "contacts", label: "People" },
]

const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

const MigratedEntitiesView = props => {
  let { navComponent } = props
  const { id, nav1 } = props
  navComponent = navComponent || "details"
  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink
          key={t.path}
          to={`/migratedentities/${id}/${t.path}`}
          activeStyle={activeStyle}
        >
          {" "}
          {t.label}{" "}
        </NavLink>
      </li>
    ))
  const renderViewSelection = (id, navComponent) => (
    <div className="tabs is-boxed">
      <ul>{renderTabs(TABS, navComponent, id)}</ul>
    </div>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(id, navComponent)}
          </div>
        </div>
      </div>
      <div id="main">
        <Switch condition={navComponent}>
          <Case value="entity">
            <ClientsProspects {...props} nav2={id} nav1={nav1}/>
          </Case>
          <Case value="business-activity">
            <BusinessActivity nav2={id} nav1={nav1}/>
          </Case>
          <Case value="contacts">
            <ClientContactList nav2={id} nav1={nav1}/>
          </Case>
          <Default>
            <Redirect to="/client-list" />
          </Default>
        </Switch>
      </div>
    </div>
  )
}

export default MigratedEntitiesView
