import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import "react-table/react-table.css"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"

const BusinessActivityList = (props) => {
  const data = props && props.data && props.data.map(ent => {
    const newObject = {
      projectDescription: ent.tranAttributes.projectDescription,
      subType: ent.tranAttributes.subType,
      type: ent.tranAttributes.type,
      url: ent.url
    }
    return newObject
  })

  const columns = [
    {
      id: "type",
      Header: "Activity",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (`${item.type || props && props.type} - ${item.subType}` || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.subType || "").localeCompare(b.subType)
    },
    {
      id: "description",
      Header: "Project Description",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: item.projectDescription}} />
        )
      },
      sortMethod: (a, b) => (a.projectDescription || "").localeCompare(b.projectDescription)
    },
    {
      id: "link",
      Header: "",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div>
            <Link to={item.url} ><i className="fas fa-binoculars" /></Link>
          </div>
        )
      }
    }
  ]

  return (
    <div>
      <Accordion
        multiple
        activeItem={[0]}
        boxHidden
        render={({ activeAccordions, onAccordion }) => (
          <div>
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title={props && props.title}
            >
              {activeAccordions.includes(0) && (
                <div>
                  <div className="columns">
                    { props && props.loginDetails && props.loginDetails.relationshipToTenant && props.loginDetails.relationshipToTenant === "Self" ?
                      <div className="column">
                        <div className="control">
                          <Link className="button is-text is-small"
                            to={`/createTran/${props && props.link1}`}>{props && props.name1}</Link>
                        </div>
                      </div> : null
                    }
                    {
                      props.link2 && props && props.loginDetails && props.loginDetails.relationshipToTenant && props.loginDetails.relationshipToTenant === "Self" ?
                        <div className="column">
                          <div className="control">
                            <Link className="button is-text is-small" to={`/createTran/${props && props.link2}`}>{props && props.name2}</Link>
                          </div>
                        </div> : null
                    }

                    <div className="column">
                      <div className="control">
                        <Link className="button is-text is-small" to="/dash-projects">Project Dashboard</Link>
                        <i className="fas fa-columns" />
                      </div>
                    </div>
                  </div>
                  <ReactTable
                    columns={columns}
                    data={data || []}
                    showPaginationBottom
                    defaultPageSize={10}
                    pageSizeOptions={[5, 10, 20, 50, 100]}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    showPageJump
                    minRows={2}
                  />
                </div>
              )}
            </RatingSection>
          </div>
        )}
      />
    </div>
  )
}

export default BusinessActivityList
