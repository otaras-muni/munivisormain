import React, { Component } from "react"
import {
  getAllBusinessActivity,
  getFirmById
} from "AppState/actions/AdminManagement/admTrnActions"
import { toast } from "react-toastify"
import Switch, { Case } from "react-switch-case"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import BusinessActivityList from "./BusinessActivityList"
import Loader from "../../../../GlobalComponents/Loader"

class BusinessActivityContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activityList: [],
      newActivityList: [],
      firmName: "",
      loading: true
    }
  }

  async componentWillMount() {
    let firmDetailById = getFirmById(this.props.match.params.nav2)
    let response = getAllBusinessActivity(this.props.match.params.nav2)
    if (this.props.match.params.nav2 !== "new" && this.props.match.params.nav2.length === 24) {
      this.setState({
        loading: true
      })

      response = await response
      // Formatting the response to fit proper details
      const activityList = response && response.data

      const mapper = {
        transactions: { name: "Transactions", props: ["deals", "bankloans", "derivatives"] },
        projects: { name: "Projects", props: ["rfps", "others"] },
        marfps: { name: "MA RFPs", props: ["marfps"] },
        busdevs: { name: "Business Development", props: ["busdevs"] }

      }
      const groupedActivityList = Object.keys(mapper).reduce((acc, gp) => {
        const allGroupConsolidatedTransactions = mapper[gp].props.reduce((allGroupTrans, tranType) => [...allGroupTrans, ...activityList[tranType]], [])
        return { ...acc, [gp]: allGroupConsolidatedTransactions }
      }, {})

      firmDetailById = await firmDetailById

      if (firmDetailById !== null && !firmDetailById.error) {
        this.setState({
          firmName: firmDetailById.firmName,
          activityList: response.data,
          newActivityList: groupedActivityList,
          loading: false
        })
      } else {
        toast("Entity Not Found!", { autoClose: 2000, type: toast.TYPE.ERROR })
      }
    } else {
      this.props.history.push("/client-list")
    }
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }
  render() {
    const loading = () => <Loader />
    let { firmName, activityList, newActivityList } = this.state
    const { match, auth } = this.props
    const loginDetails = auth && auth.loginDetails && auth.loginDetails.userEntities && auth.loginDetails.userEntities.length && auth.loginDetails.userEntities && auth.loginDetails.userEntities[0] || {}
    console.log("loggedDetails:", loginDetails)
    return (
      <div className="tile is-ancestor business-activity" >
        {this.state.loading && loading()}
        <div className="tile is-vertical">
          <div className="tile">
            <div className="tile is-parent is-vertical">
              <article className="tile is-child">
                <p className="title innerPgTitle">You selected...</p>
                <p className="subtitle">{firmName}</p>
              </article>
              {
                Object.keys(newActivityList).map((item, idx) => (
                  <Switch condition={item}>
                    <Case value="transactions">
                      <BusinessActivityList
                        data={newActivityList[item]}
                        title="Transactions"
                        link1={`debt?name=${firmName || ""}&id=${match && match.params && match.params.nav2 || ""}`}
                        link2={`derivative?name=${firmName || ""}&id=${match && match.params && match.params.nav2 || ""}`}
                        name1="Add New Deal Issue / Bank Loans"
                        name2="Add New Derivative Transaction"
                        loginDetails={loginDetails}/>
                    </Case>
                    <Case value="projects">
                      <BusinessActivityList
                        data={newActivityList[item]}
                        title="Business Projects"
                        link1={`rfp?name=${firmName || ""}&id=${match && match.params && match.params.nav2 || ""}`}
                        name1="Add New RFP Project"
                        loginDetails={loginDetails}/>
                    </Case>
                    <Case value="marfps">
                      <BusinessActivityList
                        data={newActivityList[item]}
                        title="Municipal Advisor Request For Proposals"
                        type="MARFP"
                        link1={`marfp?name=${firmName || ""}&id=${match && match.params && match.params.nav2 || ""}`}
                        name1="Add New MA RFP"
                        loginDetails={loginDetails}/>
                    </Case>
                    <Case value="busdevs">
                      <BusinessActivityList
                        data={newActivityList[item]}
                        title="Business Development Activities"
                        link1={`businessDevelopment?name=${firmName || ""}&id=${match && match.params && match.params.nav2 || ""}`}
                        name1="Add New Business Development Activity"
                        loginDetails={loginDetails}/>
                    </Case>
                  </Switch>
                ))
              }
            </div>
          </div>
        </div>
      </div>)
  }
}
const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

export default withRouter(connect(mapStateToProps, {})(BusinessActivityContainer))
