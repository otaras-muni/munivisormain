import React from "react"
import { Redirect } from "react-router"
import ContactsView from "./ContactsView"


const ContacsMain = props => {
  const { nav2, nav3} = props
  if (nav2) {
    return (
      <ContactsView id={nav2} navComponent={nav3} />
    )
  }
  return (
    <Redirect to="/mast-allcontacts" />
  )
}

export default  ContacsMain
