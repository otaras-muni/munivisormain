import React, { Component } from "react"
import { toast } from "react-toastify"
import Switch, { Case, Default } from "react-switch-case"
import {
  entityUsersList,
  getFirmById
} from "AppState/actions/AdminManagement/admTrnActions"
import { withRouter, Link } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "../../../../GlobalComponents/Loader"
import "../../scss/entity.scss"
import UpdateContactContainer from "./UpdateContactContainer"
// import AddContactContainer from "./AddContactContainer"
import UsersNew from "../../../Users/Users"
import { checkPlatformAdmin, checkNavAccessEntitlrment } from "GlobalUtils/helpers"
import EntityPageFilter from "../../CommonComponents/EntityPageFilter"

class ClientContactListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firmName: "",
      filterUsersList: [],
      searchString: "",
      viewList: "",
      userId: "",
      entityId: "",
      loading: true,
      loadingPickLists: true,
      pageSize: 10,
      canEdit: false,
      canView: false
    }
  }

  async componentWillMount() {
    const { nav2, match } = this.props

    const action = await checkNavAccessEntitlrment(nav2)
    const { edit, view } = action

    let { canEdit, canView } = this.state
    const { entityId } = this.props.auth.userEntities
    const isPlatformAdmin = await checkPlatformAdmin()
    if (isPlatformAdmin) {
      canEdit = true
    } else {
      canEdit = edit
      canView = view
    }

    this.setState({
      canEdit,
      entityId,
      canView,
      nav2,
      nav1: match && match.params && match.params.nav1 || ""
    })
  }

  async componentDidMount() {
    const { entityId } = this.props.auth.userEntities
    let result = entityUsersList(entityId)
    let firmDetailById = getFirmById(this.props.nav2)
    if (this.props.nav2 !== "new" && this.props.nav2.length === 24) {
      const userList = []
      result = await result
      result.forEach(item => {
        if (item.entityId === this.props.nav2) {
          userList.push(item)
        }
      })
      firmDetailById = await firmDetailById
      if (firmDetailById !== null && !firmDetailById.error) {
        this.setState({
          firmName: firmDetailById.firmName,
          userList,
          filterUsersList: userList,
          loading: false,
          loadingPickLists: false
        })
      } else {
        toast("Entity Not Found!", { autoClose: 2000, type: toast.TYPE.ERROR })
      }
    }
  }

  getUsers = (userId, entityId) => {
    this.setState({
      viewList: "viewUsers",
      userId,
      entityId
    })
  }

  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map(item => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }

  changeSearchString = e => {
    const searchString = e.target.value
    if (searchString === "") {
      this.setState(prevState => {
        const { userList } = prevState
        return { filterUsersList: userList, searchString }
      })
    } else {
      this.setState({ searchString })
      this.performSearch()
    }
  }

  performSearch = () => {
    this.setState(prevState => {
      const searchUsers = this.searchUsers(
        prevState.searchString,
        prevState.filterUsersList,
        prevState.userList
      )
      return { filterUsersList: searchUsers }
    })
  }

  searchUsers = (searchString, filterUsersList, userList) => {
    searchString = searchString.trim().toLowerCase()
    const usersListRes = []
    userList.forEach(item => {
      if (
        item.firstName.toLowerCase().includes(searchString) ||
        item.middleName.toLowerCase().includes(searchString) ||
        item.lastName.toLowerCase().includes(searchString) ||
        (typeof item.primaryEmail !== "undefined" &&
          item.primaryEmail !== "" &&
          item.primaryEmail.toLowerCase().includes(searchString))
      ) {
        usersListRes.push(item)
      }
    })
    return usersListRes
  }

  handleKeyPressinSearch = e => {
    if (e.key === "Enter") {
      this.performSearch()
    }
  }

  addNewUsers = () => {
    this.setState({
      viewList: "addNewUsers"
    })
  }

  onSearchUsers = item => {
    this.setState({
      viewList: "viewUsers",
      userId: item.value
    })
  }

  render() {
    const loading = () => <Loader />
    const {
      firmName,
      canEdit,
      canView
    } = this.state
    return (
      <div>
        {this.state.loading ? loading() : null}
        <Switch condition={this.state.viewList}>
          <Case value="viewUsers">
            <UpdateContactContainer
              userId={this.state.userId}
              entityId={this.state.entityId}
              canEdit={canEdit}
              canView={canView}
            />
          </Case>
          <Case value="addNewUsers">
            <UsersNew
              entityId={this.props.nav2}
              auth={this.props.auth}
              nav1={this.state.nav1}
            />
          </Case>
          <Default>
            <div className="columns">
              <div className="column">
                <div className="tile is-ancestor">
                  <div className="tile is-vertical is-8">
                    <div className="tile">
                      <div className="tile is-parent is-vertical">

                        <article className="tile is-child">
                          <p className="title innerPgTitle">You selected...</p>
                          <p className="subtitle">{firmName || ""}</p>
                        </article>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {canEdit && (
                <div className="column is-pulled-right">
                  <button
                    className="button is-link"
                    onClick={event => {
                      event.preventDefault()
                      this.addNewUsers()
                    }}
                  >
                    Add new contact
                  </button>
                </div>
              )}
            </div>
            <EntityPageFilter
              listType="people"
              auth={this.props.auth}
              entityId={this.props.nav2 || ""}
              nav1={this.props.nav1 || ""}
              searchPref=""
              title=""
              routeType="adminUsers"
            />
          </Default>
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ClientContactListContainer)
)
