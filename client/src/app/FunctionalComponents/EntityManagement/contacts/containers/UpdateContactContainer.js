import React from "react"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { DropdownList } from "react-widgets"
import { withRouter } from "react-router-dom"
import { sendEmailAlert, saveUserDetail, getAllEntity, getThirdPartyFirmUserById, getUserDetailById, entityUsersList, addContactDetail, updateUserDetailById } from "AppState/actions/AdminManagement/admTrnActions"
import { validateUserDetail } from "Validation/userDetail"
import { convertError, getPicklistValues, checkEmptyElObject, checkNavAccessEntitlrment } from "GlobalUtils/helpers"
import UserAddress from "../../CommonComponents/UserAddress"
import UserContact from "../../CommonComponents/UserContact"
import ListAddresses from "../../CommonComponents/UserAddressList"
import Loader from "../../../../GlobalComponents/Loader"
import ContactType from "../../CommonComponents/ContactType"

const intialError = (errorUserDetail, userDetails, initialUserDetails) => {
  const arr = ["userEmails", "userPhone", "userFax"]
  arr.map(item => {
    if (userDetails[item].length > 0) {
      for (let index = 1; index < (userDetails[item].length); index++) {
        errorUserDetail[item].push(initialUserDetails[item][0])
      }
    }
  })
}
class UpdateUsersContainers extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.errorUserDetail = Object.assign({}, this.initialState().userDetails)
    this.cleanErrorUserDetail = Object.assign({}, this.initialState().userDetails)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
  }

  /** ************************ this is for onchange msrb firmname ********************************* */
  getUserDetails = (e) => {
    if (e.target.value != "") {
      this.props.getUserDetailById(e.target.value)
    } else {
      this.setState(prevState => ({
        ...prevState,
        userDetails: this.initialState().userDetails,
        addressList: [],
        errorUserDetail: this.cleanErrorUserDetail
      }))
    }
  }
  /** ***********************************This is for Initial State************************************* */

  initialState = () => ({
    userDetails: {
      entityId: "",
      userFlags: [], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
      userRole: "", // [ Admin, ReadOnly, Backup ]
      userEntitlement: "", // [ Global, Transaction ]
      userFirmName: "",
      userFirstName: "",
      userMiddleName: "",
      userLastName: "",
      userEmails: [{
        emailId: "",
        emailPrimary: true
      }],
      userPhone: [{
        phoneNumber: "",
        extension: "",
        phonePrimary: true
      }],
      userFax: [{
        faxNumber: "",
        faxPrimary: true
      }],
      userEmployeeID: "",
      userEmployeeType: "",
      userJobTitle: "",
      userManagerEmail: "",
      userDepartment: "",
      userCostCenter: "",
      userExitDate: "",
      userJoiningDate: "",
      userAddresses: [{
        addressName: "",
        addressType: "",
        isPrimary: false,
        isActive: true,
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: {
          zip1: "",
          zip2: ""
        },
        formatted_address: "",
        url: "",
        location: {
          longitude: "",
          latitude: ""
        },
        loadingPickLists: false
      }]
    },
    addressList: [],
    countryResult: [],
    searchUserState: {
      backspaceRemoves: false,
      firmValue: ""
    },
    searchFirm: {
      firmDropdown: [],
      firmValue: ""
    },
    searchUsers: {
      usersDropdown: [],
      usersValue: ""
    },
    usersList: [],
    filterUsersList: [],
    userRole: [],
    userFlags: [],
    userEntitlement: [],
    expanded: {
      business: false,
      addresslist: false,
      contactdetail: true,
      contacttype: true
    },
    loading: false,
    oldUsersDetails: {},
    userType: "",
    primaryEmails: {
      isPrimary: false,
      emailId: ""
    },
    disabled: false,
    canEdit: false,
    canView: false
  })
  async componentWillMount() {
    let picklistArray = getPicklistValues(["LKUPCOUNTRY", "LKUPUSERENTITLEMENT", "LKUPUSERFLAG", "LKUPUSERENTITLEMENT"])
    const action = await checkNavAccessEntitlrment(this.props.match.params.nav2)
    const { edit, view } = action || {}
    const canEdit = edit
    const canView = view

    if (!canEdit && !canView) {
      this.props.history.push("/mast-cltprosp")
    }
    this.setState({
      loading: true,
      canEdit,
      canView,
      loadingPickLists: true
    })
    let [countryResult, userRole, userFlags, userEntitlement] = await picklistArray
    this.setState((prevState) => ({
      ...prevState, ...{
        countryResult,
        userRole: userRole[1],
        userEntitlement: userEntitlement[1],
        userFlags: userFlags[1],
        loadingPickLists: false
      }
    }))
  }
  async componentDidMount() {
    const { userDetails } = this.state
    const { userEntities } = this.props.auth
    let usersList = entityUsersList(userEntities.entityId)
    let firmList = await getAllEntity()
    firmList.length > 0 ?
      firmList = firmList.map(item => ({ label: item.firmName, value: item.relatedEntityId, relationshipType: item.relationshipType }))
      : firmList = []
    userDetails.userFirmName = this.props.entityName
    usersList = await usersList
    const filterUsersList = usersList.map(item => ({ id: item.userId, label: `${item.firstName} ${item.lastName}` }))
    this.setState((prevState) => ({
      ...prevState, ...{
        userDetails,
        searchFirm: {
          firmDropdown: [{ label: "Select Firms", value: "", relationshipType: "" }, ...firmList]
        },
        usersList,
        filterUsersList,
        loading: false,
      }
    }))
    if (this.props.userId && this.props.userId.length === 24) {
      this.props.getUserDetailById(this.props.userId)
    } else {
      this.props.history.push(`/${this.props.match.params.nav1}/${this.props.match.params.nav2}/${this.props.match.params.nav3}`)
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.admUserDetail.userDetails &&
      Object.keys(nextProps.admUserDetail.userDetails).length > 0) {
      const { searchFirm, searchUsers, usersList } = this.state
      let { errorUserDetail, filterUsersList } = this.state
      const { firmDropdown } = searchFirm
      const { userDetails, primaryEmails } = cloneDeep(nextProps.admUserDetail)
      const addressList = cloneDeep(nextProps.admUserDetail.userDetails.userAddresses)
      userDetails.userAddresses = this.initialState().userDetails.userAddresses
      if (firmDropdown) {
        const firmName = firmDropdown.find(item => item.value === userDetails.entityId)
        userDetails.userFirmName = firmName && firmName.label ? firmName.label : ""
        errorUserDetail = cloneDeep(this.initialState().userDetails)
        intialError(errorUserDetail, userDetails, this.initialState().userDetails)
        searchUsers.usersValue = `${userDetails.userFirstName} ${userDetails.userLastName}`
        searchFirm.firmValue = firmName
        filterUsersList = usersList.filter(user => user.entityId === userDetails.entityId)
        filterUsersList = filterUsersList.map(item => ({ id: item.userId, label: `${item.firstName} ${item.lastName}` }))
        this.setState(prevState => ({
          ...prevState,
          userDetails,
          addressList,
          searchFirm,
          searchUsers,
          filterUsersList,
          errorUserDetail,
          loading: false,
          oldUsersDetails: cloneDeep(nextProps.admUserDetail.userDetails),
          isPrimaryAddress: "",
          isPrimaryEmailAddress: "",
          isPrimaryUserPhone: "",
          primaryEmails,
          disabled: true
        }))
      }
      this.submitted = false
    }
  }

  componentWillUpdate(nextState, nextProps) {
  }

  componentDidUpdate(nextState, nextProps) {
  }
  /** ************************ this is for submit form ********************************* */

  async handleSubmit(event) {
    event.preventDefault()
    this.submitted = true
    const isPrimaryAddress = false
    const { userDetails, errorUserDetail, usersList, search } = this.state
    const userDetailData = {
      _id: userDetails._id ? userDetails._id : "",
      entityId: userDetails.entityId,
      userFlags: userDetails.userFlags,
      userRole: userDetails.userEntitlement,
      userEntitlement: userDetails.userEntitlement,
      userFirmName: userDetails.userFirmName,
      userFirstName: userDetails.userFirstName,
      userMiddleName: userDetails.userMiddleName,
      userLastName: userDetails.userLastName,
      userEmails: userDetails.userEmails,
      userPhone: userDetails.userPhone,
      userFax: userDetails.userFax,
      userEmployeeID: userDetails.userEmployeeID,
      userEmployeeType: userDetails.userEmployeeID,
      userJobTitle: userDetails.userJobTitle,
      userManagerEmail: userDetails.userManagerEmail,
      userDepartment: userDetails.userDepartment,
      userCostCenter: userDetails.userCostCenter,
      userAddresses: userDetails.userAddresses
    }
    if (typeof this.state.searchFirm.firmValue !== "undefined" && this.state.searchFirm.firmValue.relationshipType === "Firm") {
      userDetailData.userExitDate = userDetails.userExitDate
      userDetailData.userJoiningDate = userDetails.userJoiningDate
    }
    try {
      let sortedData = []
      const { userAddresses } = userDetailData
      userAddresses.forEach(address => {
        if (!checkEmptyElObject(address)) {
          sortedData.push(address)
        }
      })
      userDetailData.userAddresses = sortedData
      let { addressList } = this.state
      if (sortedData.length > 0) {
        sortedData = sortedData.sort((a, b) => a._id > b._id)
        addressList = addressList.length > 0 ? addressList.filter((obj) => obj._id != sortedData[0]._id) : []
      }
      const mergeAddresses = [...addressList, ...sortedData]
      const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
      const errorData = validateUserDetail(userDetailData)
      if (errorData.error || filteredAddresses.length !== 1) {
        if (errorData.error) {
          convertError(errorUserDetail, errorData)
        }
        if (filteredAddresses.length === 0)
          // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
          toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
        else if (filteredAddresses.length > 1)
          // this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
          toast("Only One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
        else
          this.state.isPrimaryAddress = ""
        this.setState({ errorUserDetail })
      } else {
        this.setState({
          loading: true
        })
        const response = await updateUserDetailById(userDetailData, mergeAddresses)
        if (!response.error && response.isPrimaryAddress) {
          const { searchFirm, searchUsers, usersList } = this.state
          const { expanded, userDetails } = this.initialState()
          if (searchFirm.firmValue.value !== "") {
            userDetails.entityId = searchFirm.firmValue.value
            userDetails.userFirmName = searchFirm.firmValue.label
          }
          searchUsers.usersValue = ""
          this.setState({
            userDetails,
            errorUserDetail: cloneDeep(this.initialState().userDetails),
            loading: false,
            searchFirm,
            searchUsers,
            addressList: [],
            usersList,
            oldUsersDetails: {},
            expanded
          })
          this.submitted = false
          if (this.props.match.params.nav1 === "contacts") {
            this.props.history.push("/mast-allcontacts")
          } else {
            this.props.history.push(`/${this.props.match.params.nav1}/${this.props.match.params.nav2}/${this.props.match.params.nav3}`)
          }
          toast("Users Contact has been Updated!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        } else {
          if (response.error !== "" && typeof response.error === "string")
            toast(response.error, { autoClose: 2000, type: toast.TYPE.ERROR })
          if (typeof response.error === "object" && response.error !== null) {
            convertError(errorUserDetail, response)
          }
          if (typeof response.isPrimaryAddress !== "undefined" && !response.isPrimaryAddress) {
            // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
            toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
            this.setState({ errorUserDetail, loading: false })
          } else
            errorUserDetail.userAddresses[0].addressType = ""
          this.setState({ errorUserDetail, loading: false })
        }
      }
      // }
    } catch (error) {
      console.log("Expception===>>>", error)
    }
  }
  /** ************************ this is for add new address ********************************* */
  addNewUserAddress = (event) => {
    event.preventDefault()
    const { userDetails } = this.state
    const { userAddresses } = userDetails
    let isEmpty = false
    userAddresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true

      }
    })
    if (!isEmpty) {
      this.setState(prevState => {
        const userDetails = {
          ...prevState.userDetails
        }
        const errorUserDetail = {
          ...prevState.errorUserDetail
        }
        userDetails.userAddresses
          .push(this.initialState().userDetails.userAddresses[0])
        errorUserDetail.userAddresses
          .push(this.initialState().userDetails.userAddresses[0])
        return { userDetails, errorUserDetail }
      })
    } else {
      toast("Already and empty address!", { autoClose: 2000, type: toast.TYPE.INFO })
    }
  }
  /** ************************ this is for reset form ********************************* */
  resetUserAddress = e => {
    e.preventDefault()
    const userAddresses = this.initialState().userDetails.userAddresses[0]
    // userAddresses.addressType = "office"
    this.setState(prevState => ({
      userDetails: {
        ...prevState.userDetails,
        userAddresses: [userAddresses]
      },
      errorUserDetail: {
        ...prevState.errorUserDetail,
        userAddresses: [
          this.initialState().userDetails.userAddresses[0]
        ]
      }
    }))
  }

  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const userFlags = this.state.userDetails.userFlags
    if (userFlags.includes(label)) {
      userFlags.splice(userFlags.indexOf(label), 1)
    } else {
      userFlags.push(label)
    }
  }
  /** ************************ this is for onchange ********************************* */
  onChangeUserDetail = (e, key, deepFields) => {
    // e.preventDefault()
    const { userDetails, errorUserDetail } = this.state
    let { userAddresses } = userDetails
    let errorKeyUserDetail
    let validator
    try {
      if (typeof key !== "undefined") {
        if (e.target.name === "addressType") {
          userAddresses = userAddresses.map(item => {
            if (item.addressType === "office" && e.target.value === "office")
              item.addressType = ""
            return item
          })
        }
        const value = e.target.type === "checkbox" && (e.target.name === "zip2" || e.target.name === "zip1")
          ? !userDetails.userAddresses[key][e.target.name]
          : e.target.value
        if (e.target.name === "zip1" || e.target.name === "zip2") {
          userDetails.userAddresses[key].zipCode[e.target.name] = value
          errorKeyUserDetail = errorUserDetail.userAddresses[key].zipCode
          validator = {
            userAddresses: [
              {
                zipCode: {
                  [e.target.name]: e.target.value
                }
              }
            ]
          }
        } else {
          userDetails.userAddresses[key][e.target.name] = value
          errorKeyUserDetail = errorUserDetail.userAddresses[key]
          validator = {
            userAddresses: [
              {
                [e.target.name]: e.target.value
              }
            ]
          }
        }
      } else {
        if (e.target.name === "userFlags") {
          this.toggleCheckbox(e.target.value)
          validator = {
            [e.target.name]: userDetails[e.target.name]
          }
        } else {
          userDetails[e.target.name] = e.target.value
          validator = {
            [e.target.name]: e.target.value
          }
        }
        errorKeyUserDetail = errorUserDetail
      }
      if (deepFields) {
        deepFields.forEach(d => {
          userDetails.userAddresses[key][d] = ""
        })
      }
      this.setState({
        userDetails
      })
      if (this.submitted) {
        const errorData = validateUserDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          if (err.context.key === e.target.name) {
            errorKeyUserDetail[e.target.name] = `${err.context.label} Required.`
            this.setState({ errorUserDetail })
          } else {
            errorKeyUserDetail[e.target.name] = ""
            this.setState({ errorUserDetail })
          }
        } else {
          errorKeyUserDetail[e.target.name] = ""
          this.setState({ errorUserDetail })
        }
      }
    } catch (ex) {
      console.log("Ex<<<<=========>>>", errorKeyUserDetail, ex)
    }
  }
  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type) => {
    e.preventDefault()
    this.setState(prevState => {
      const addmore = this.initialState().userDetails[type][0]
      switch (type) {
        case "userEmails":
          addmore.emailPrimary = false
          break
        case "userPhone":
          addmore.phonePrimary = false
          break
        default:
          addmore.faxPrimary = false
          break
      }
      const addMoreData = {
        ...prevState.userDetails
      }
      const errorUserDetail = {
        ...prevState.errorUserDetail
      }
      addMoreData[type].push(addmore)
      errorUserDetail[type].push(this.initialState().userDetails[type][0])
      return { addMoreData }
    })
  }
  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, idx) => {
    // e.preventDefault();
    const { userDetails, errorUserDetail } = this.state
    let errorKeyUserDetail, validator
    if (e.target.type == "checkbox") {
      userDetails[type].forEach((item, index) => {
        if (index == idx) {
          userDetails[type][idx][e.target.name] = true
        } else
          userDetails[type][index][e.target.name] = false
      })
    } else {
      userDetails[type][idx][e.target.name] = e.target.value
    }
    validator = {
      [type]: [
        {
          [e.target.name]: e.target.value
        }
      ]
    }
    this.setState({
      userDetails
    })

    if (this.submitted) {
      const errorData = validateUserDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorUserDetail[type][idx][e.target.name] = `${err.context.label} Required.`
        this.setState({
          errorUserDetail
        })
      } else {
        errorUserDetail[type][idx][e.target.name] = ""
        this.setState({
          errorUserDetail
        })
      }
    }
  }
  // **************************function used to update business addresses *****************************
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this.state.addressList.find(item => item._id == id)
    this.setState(prevState => {
      const { errorUserDetail, expanded } = prevState
      const { userDetails } = prevState
      expanded.business = true
      errorUserDetail.userAddresses = this.cleanErrorUserDetail.userAddresses
      userDetails.userAddresses = [address]
      return { userDetails, errorUserDetail, expanded }
    })
  }
  /** ***************************************** Reset Form ************************************** */
  resetForm = (e) => {
    e.preventDefault()
    this.setState(prevState => ({
      userDetails: this.initialState().userDetails,
      errorUserDetail: this.initialState().userDetails
    }))
  }

  onSearchUsers = item => {
    this.setState({
      loading: true
    })
    if (item.id !== "") {
      this.props.getUserDetailById(item.id)
    }
  }
  onSearchFirms = (item) => {
    let { filterUsersList, usersList, errorUserDetail, userType, searchFirm } = this.state
    const { userDetails } = this.initialState()
    if (item.value !== "") {
      userDetails.userFirmName = item.label
      userDetails.entityId = item.value
      filterUsersList = usersList.filter(user => user.entityId === item.value)
      filterUsersList = filterUsersList.map(item => ({ id: item.userId, label: `${item.firstName} ${item.lastName}` }))
      if (this.submitted) {
        errorUserDetail.userFirmName = ""
        errorUserDetail.entityId = ""
      }
    } else {
      filterUsersList = usersList.map(item => ({ id: item.userId, label: `${item.firstName} ${item.lastName}` }))
      if (this.submitted) {
        errorUserDetail.userFirmName = "firm required"
        errorUserDetail.entityId = "firm required"
      }
    }
    this.setState(prevState => ({
      searchFirm: {
        ...prevState.searchFirm,
        firmValue: item
      },
      userDetails,
      filterUsersList,
      searchUsers: {
        usersValue: ""
      },
      addressList: [],
      oldUsersDetails: {},
      errorUserDetail
    }))
  }
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }
  deleteAliases = (type, idx) => {
    const { userDetails, errorUserDetail } = this.state
    let isPrimary = true
    let primaryKey = ""
    let message = ""
    switch (type) {
      case "userEmails":
        primaryKey = "emailPrimary"
        message = "Primary Email"
        break
      case "userPhone":
        primaryKey = "phonePrimary"
        message = "Primary Phone"
        break
      default:
        break
    }
    if (userDetails[type][idx][primaryKey]) {
      isPrimary = false
    }

    if (type === "userFax") {
      userDetails[type].splice(idx, 1)
      errorUserDetail[type].splice(idx, 1)
      this.setState({
        userDetails,
        errorUserDetail
      })
    }

    if (isPrimary) {
      userDetails[type].splice(idx, 1)
      errorUserDetail[type].splice(idx, 1)
      this.setState({
        userDetails,
        errorUserDetail
      })
    } else {
      toast(` ${message} can not be Deleted`, { autoClose: 2000, type: toast.TYPE.INFO })
    }
  }
  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = (e, type) => {
    e.preventDefault()
    const { oldUsersDetails } = this.state
    if (Object.keys(oldUsersDetails).length !== 0) {
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          [type]: cloneDeep(oldUsersDetails[type])
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          [type]: oldUsersDetails[type].map((item) => this.initialState().userDetails[type][0])
        }
      }))
    } else {
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          [type]: this.initialState().userDetails[type]
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          [type]: this.initialState().userDetails[type]
        }
      }))
    }
  }
  deleteAddress = (e, type, idx) => {
    const { userDetails, errorUserDetail } = this.state
    userDetails[type].splice(idx, 1)
    errorUserDetail[type].splice(idx, 1)
    this.setState({
      userDetails,
      errorUserDetail
    })
  }
  addNewUsers = (e) => {
    e.preventDefault()
    const { filterUsersList, searchFirm } = this.state
    const { userDetails, expanded } = cloneDeep(this.initialState())
    if (typeof searchFirm !== "undefined" && typeof searchFirm.firmValue !== "undefined" && searchFirm.firmValue.value !== "") {
      userDetails.entityId = searchFirm.firmValue.value
      userDetails.userFirmName = searchFirm.firmValue.label
    }
    this.setState(prevState => ({
      userDetails,
      filterUsersList,
      searchUsers: {
        usersValue: ""
      },
      addressList: [],
      oldUsersDetails: {},
      errorUserDetail: cloneDeep(this.initialState().userDetails),
      isPrimaryAddress: "",
      expanded
    }))
  }
  onChangeAddressType = (e, idx) => {
    const { userDetails } = this.state
    let { addressList, isPrimaryAddress } = this.state
    let { userAddresses } = userDetails
    if (e.target.name === "isPrimary") {
      userDetails.userAddresses[idx].isPrimary = !userDetails.userAddresses[idx].isPrimary
    }
    if (this.submitted) {
      addressList = addressList.length > 0 ? addressList.filter((obj) => obj._id != userAddresses[0]._id) : []
      userAddresses = userAddresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [...addressList, ...userAddresses]
      const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
      if (filteredAddresses.length === 0)
        // isPrimaryAddress  = "One Primary Office  Address  is Required"
        toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
      else if (filteredAddresses.length > 1)
        // isPrimaryAddress  = "Only One Primary Office  Address  is Required"
        toast("Only One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
      else {
        isPrimaryAddress = ""
      }
    }
    this.setState({
      userDetails,
      isPrimaryAddress
    })
  }
  checkAddressStatus = async (e, id, field) => {
    const { addressList } = this.state
    let addressIdx = ""
    addressList.forEach((address, idx) => {
      if (address._id == id) {
        addressIdx = idx
      }
    })
    if ((field === "isPrimary" && !addressList[addressIdx].isActive) || (field === "isActive" && addressList[addressIdx].isPrimary)) {
      toast("Primary Address Can not be Inactive.", { autoClose: 2000, type: toast.TYPE.WARNING })
    } else {
      addressList[addressIdx][field] = !addressList[addressIdx][field]
      if (field === "isPrimary") {
        addressList.forEach((address, idx) => {
          if (address._id === id)
            addressList[idx][field] = true
          else
            addressList[idx][field] = false
        })
      }
      this.setState(prevState => ({
        ...prevState,
        addressList
      }))
    }
  }
  getAddressDetails = (address = "", idx) => {
    if (address !== "") {
      const { userDetails, errorUserDetail } = this.state
      let id = ""
      if (userDetails.userAddresses[idx]._id !== "") {
        id = userDetails.userAddresses[idx]._id
      }
      userDetails.userAddresses[idx] = { ...this.initialState().userDetails.userAddresses[0] }
      userDetails.userAddresses[idx]._id = id
      if (address.addressLine1 !== "") {
        userDetails.userAddresses[idx].addressName = userDetails.userAddresses[idx].addressName !== "" ? userDetails.addresses[idx].addressName : address.addressLine1
        userDetails.userAddresses[idx].addressLine1 = address.addressLine1
        errorUserDetail.userAddresses[idx].addressName = ""
        errorUserDetail.userAddresses[idx].addressLine1 = ""
      }
      if (address.country !== "") {
        errorUserDetail.userAddresses[idx].country = ""
        userDetails.userAddresses[idx].country = address.country
      }
      if (address.state !== "") {
        errorUserDetail.userAddresses[idx].state = ""
        userDetails.userAddresses[idx].state = address.state
      }
      if (address.city !== "") {
        errorUserDetail.userAddresses[idx].city = ""
        userDetails.userAddresses[idx].city = address.city
      }
      if (address.zipcode !== "") {
        errorUserDetail.userAddresses[idx].country = ""
        userDetails.userAddresses[idx].zipCode.zip1 = address.zipcode
      }
      userDetails.userAddresses[idx].formatted_address = address.formatted_address
      userDetails.userAddresses[idx].url = address.url
      userDetails.userAddresses[idx].location.longitude = address.location.longitude.toString()
      userDetails.userAddresses[idx].location.latitude = address.location.latitude.toString()
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          ...userDetails
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          ...errorUserDetail
        }
      }))
    }
  }
  render() {
    const loading = () => <Loader />
    const { canEdit, canView } = this.state
    return (
      <div id="main">
        <div className="users">
          {this.state.loading ? loading() : null}
          <section className="accordions">
            {this.props.canEdit && <div className="columns">
              <div className="column is-full">
                <div className="field is-grouped">
                  {!["contacts", "admin-cltprosp", "admin-thirdparty", "clients-propects", "thirdparties"].includes(this.props.match.params.nav1) && <div className="control">
                    <button className="button is-link" onClick={this.addNewUsers}>
                      Add New Users
                      </button>
                  </div>}
                </div>
              </div>
            </div>}
            <p className="title innerPgTitle">Search by contact name to check if contact already exists in your database</p>
            <div className="columns">
              <div className="column">
                <DropdownList
                  filter="contains"
                  value={this.state.searchUsers.usersValue}
                  data={this.state.filterUsersList}
                  message="select Users"
                  textField='label'
                  valueField="value"
                  disabled={this.state.disabled}
                  busy={this.state.loadingPickLists}
                  busySpinner={
                    <span className="fas fa-sync fa-spin is-link" />
                  }
                  onChange={this.onSearchUsers} />
              </div>
            </div>
          </section>
          <section className="accordions">
            <ContactType
              {...this.state}
              toggleButton={this.toggleButton}
              onChangeUserDetail={this.onChangeUserDetail}
              canEdit={this.props.canEdit || canEdit}
              canView={this.props.canView || canView}
            />
            <UserContact
              {...this.state}
              addMore={this.addMore}
              onChangeAddMore={this.onChangeAddMore}
              onChangeUserDetail={this.onChangeUserDetail}
              userType={typeof this.state.searchFirm.firmValue !== "undefined" && this.state.searchFirm.firmValue.relationshipType === "Firm" ? "users" : ""}
              onSearchFirms={this.onSearchFirms}
              onSearchUsers={this.onSearchUsers}
              getUsersList={this.getUsersList}
              deleteAliases={this.deleteAliases}
              resetAliases={this.resetAliases}
              onSearch={this.onFilterUsers}
              addNewUsers={this.addNewUsers}
              toggleButton={this.toggleButton}
              canEdit={this.props.canEdit || canEdit}
              canView={this.props.canView || canView}
            />
            {this.state.addressList.length > 0 && <ListAddresses
              addressList={this.state.addressList}
              updateAddress={this.updateAddress}
              listAddressToggle={this.state.expanded.addresslist}
              toggleButton={this.toggleButton}
              checkAddressStatus={this.checkAddressStatus}
              canEdit={this.props.canEdit || canEdit}
              canView={this.props.canView || canView}
            />}
            <article className={this.state.expanded.business ? "accordion is-active" : "accordion"}>
              <div className="accordion-header">
                <p
                  onClick={event => {
                    this.toggleButton(event, "business")
                  }}
                >Address</p>
                {this.props.canEdit && <div className="field is-grouped">
                  <div className="control">
                    <button
                      className="button is-link is-small"
                      onClick={this.addNewUserAddress}>
                      Add
                      </button>
                  </div>
                  <div className="control">
                    <button
                      className="button is-light is-small"
                      onClick={this.resetUserAddress}>
                      Reset
                      </button>
                  </div>
                </div>}
              </div>
              {
                this.state.expanded.business &&
                <div className="accordion-body">
                  {this.state.userDetails.userAddresses.map((userAddress, idx) => (
                    <UserAddress
                      key={idx}
                      userAddress={userAddress}
                      idx={idx}
                      onChangeUserAddress={this.onChangeUserDetail}
                      errorUserDetail={this.state.errorUserDetail}
                      countryResult={this.state.countryResult}
                      deleteAddress={this.deleteAddress}
                      onChangeAddressType={this.onChangeAddressType}
                      getAddressDetails={this.getAddressDetails}
                      isPrimaryAddress={this.state.isPrimaryAddress}
                      canEdit={this.props.canEdit || canEdit}
                      canView={this.props.canView || canView}
                      busy={this.state.loadingPickLists}
                    />))
                  }
                </div>
              }
            </article>
            <br />
            {(this.props.canEdit || canEdit) && <div className="columns">
              <div className="column is-full">
                <div className="field is-grouped">
                  <div className="control">
                    <button className="button is-link is-small" onClick={this.handleSubmit}>
                      Save
                      </button>
                  </div>
                </div>
              </div>
            </div>}
          </section>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { admUserDetail, auth } = state
  return { admUserDetail, auth }
}
const mapDispatchToProps = {
  saveUserDetail,
  getThirdPartyFirmUserById,
  getUserDetailById
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UpdateUsersContainers))
