import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import { DropdownList } from "react-widgets"
import {
  saveUserDetail,
  getUserDetailById,
  findContactByName,
  searchUsersList,
  addContactDetail,
  entityUsersList,
  sendEmailAlert
} from "AppState/actions/AdminManagement/admTrnActions"
import { validateUserDetail } from "Validation/userDetail"
import { convertError, getPicklistValues, checkEmptyElObject } from "GlobalUtils/helpers"
import { withRouter } from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import UserAddress from "../../CommonComponents/UserAddress"
import UserContact from "../../CommonComponents/UserContact"
import Loader from "../../../../GlobalComponents/Loader"
import ContactType from "../../CommonComponents/ContactType"
import "./../../scss/entity.scss"

class UsersContainers extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState(),
      this.state.loading = false
    this.state.errorUserDetail = Object.assign({}, this.initialState().userDetails)
    this.cleanErrorUserDetail = Object.assign({}, this.initialState().userDetails)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
  }
  async componentWillMount() {
    const { userEntities } = this.props.auth
    let result = entityUsersList(userEntities.entityId)
    this.setState({
      loading: true,
      loadingPickLists: true
    })
    const usersList = []
    const { userDetails } = this.state
    userDetails.userFirmName = this.props.entityName
    userDetails.entityId = this.props.entityId
    // userDetails.userAddresses[0].addressType="office"
    result = await result
    result.forEach(item => {
      if (item.entityId === this.props.entityId) {
        usersList.push({ value: item.userId, label: `${item.firstName} ${item.middleName} ${item.lastName}` })
      }
    })
    this.setState((prevState) => ({
      ...prevState,
      ...{
        userDetails,
        userEntities,
        loading: false,
        usersList
      }
    }))
  }
  async componentDidMount() {
    const [countryResult, userRole, userFlags, userEntitlement] = await getPicklistValues(["LKUPCOUNTRY", "LKUPUSERENTITLEMENT", "LKUPUSERFLAG", "LKUPUSERENTITLEMENT"])

    this.setState((prevState) => ({
      ...prevState,
      ...{
        countryResult,
        userRole: userRole[1],
        userEntitlement: userEntitlement[1],
        userFlags: userFlags[1],
        loadingPickLists: false
      }
    }))
  }
  componentWillUpdate(nextState, nextProps) { }
  /** ************************ this is for submit form ********************************* */
  onChangeUserDetail = (e, key, deepFields) => {
    // e.preventDefault()
    const { userDetails, errorUserDetail } = this.state
    const { userAddresses } = userDetails
    let errorKeyUserDetail
    let validator
    try {
      if (typeof key !== "undefined") {
        const value = e.target.type === "checkbox" && (e.target.name === "zip2" || e.target.name === "zip1")
          ? !userDetails.userAddresses[key][e.target.name]
          : e.target.value
        if (e.target.name === "zip1" || e.target.name === "zip2") {
          userDetails.userAddresses[key].zipCode[e.target.name] = value
          errorKeyUserDetail = errorUserDetail.userAddresses[key].zipCode
          validator = {
            userAddresses: [
              {
                zipCode: {
                  [e.target.name]: e.target.value
                }
              }
            ]
          }
        } else {
          userDetails.userAddresses[key][e.target.name] = value
          errorKeyUserDetail = errorUserDetail.userAddresses[key]
          validator = {
            userAddresses: [
              {
                [e.target.name]: e.target.value
              }
            ]
          }
        }
      } else {
        if (e.target.name === "userFlags") {
          this.toggleCheckbox(e.target.value)
          validator = {
            [e.target.name]: userDetails[e.target.name]
          }
        } else {
          userDetails[e.target.name] = e.target.value
          validator = {
            [e.target.name]: e.target.value
          }
        }
        errorKeyUserDetail = errorUserDetail
      }
      this.setState({
        userDetails
      })
      if (this.submitted) {
        const errorData = validateUserDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          if (err.context.key === e.target.name) {
            errorKeyUserDetail[e.target.name] = `${err.context.label} Required.`
            this.setState({ errorUserDetail })
          } else {
            errorKeyUserDetail[e.target.name] = ""
            this.setState({ errorUserDetail })
          }
        } else {
          errorKeyUserDetail[e.target.name] = ""
          this.setState({ errorUserDetail })
        }
      }
    } catch (ex) {
      console.log("Ex<<<<=========>>>", errorKeyUserDetail, ex)
    }
  }
  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, idx) => {
    // e.preventDefault();
    const { userDetails, errorUserDetail } = this.state
    if (e.target.type === "checkbox") {
      userDetails[type].forEach((item, index) => {
        if (index === idx) {
          userDetails[type][idx][e.target.name] = true
        } else
          userDetails[type][index][e.target.name] = false
      })
    } else {
      userDetails[type][idx][e.target.name] = e.target.value
    }
    const validator = {
      [type]: [
        {
          [e.target.name]: e.target.value
        }
      ]
    }
    this.setState({ userDetails })

    if (this.submitted) {
      const errorData = validateUserDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorUserDetail[type][idx][e.target.name] = `${err.context.label} Required.`
        this.setState({ errorUserDetail })
      } else {
        errorUserDetail[type][idx][e.target.name] = ""
        this.setState({ errorUserDetail })
      }
    }
  }

  /** ***************************Function is used for searching firms******************* */
  onSearchFirms = item => {
    let { userDetail } = this.state
    userDetail = this.initialState().userDetails
    if (item) {
      userDetail.userFirmName = item.label
      userDetail.entityId = item.id
    }
    this.setState(prevState => ({
      searchFirm: {
        ...prevState.searchFirm,
        firmValue: item
      },
      userDetails: userDetail,
      errorUserDetail: this.cleanErrorUserDetail
    }))
    if (this.props.match.params.nav2 !== "new")
      this.props.history.push("/users/new/usersdetails")
  }
  onSearchUsers = item => {
    console.log("Item======>>", item)
    this.setState({
      searchUsers: {
        usersValue: item.value
      }
    })
  }

  getUsersList = async input => {
    const { userEntities } = this.props.auth
    let result, response
    if (!input) {
      return Promise.resolve({ options: [] })
    }
    if (userEntities.isMuniVisorClient) {
      result = await searchUsersList(userEntities.entityId, input)
      response = result.map((item, idx) => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))
    } else {
      result = await findContactByName({ input })
      response = result.map((item, idx) => ({ value: item.id, label: item.fullName }))
    }
    return { options: response }
    /* const response = result.map((item, idx) => ({value: item.id, label: item.fullName}))
    return {options: response} */
  }
  /** ************************ this is for onchange msrb firmname ********************************* */
  getUserDetails = e => {
    if (e.target.value != "") {
      this
        .props
        .getUserDetailById(e.target.value)
    } else {
      this.setState(prevState => ({
        ...prevState,
        userDetails: this.initialState().userDetails,
        addressList: [],
        errorUserDetail: this.cleanErrorUserDetail
      }))
    }
  }
  test = (props) => {
    Object
      .keys(props)
      .forEach(item => {
      })
  }
  initialState = () => ({
    userDetails: {
      entityId: "",
      userFlags: [], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
      userRole: "", // [ Admin, ReadOnly, Backup ]
      userEntitlement: "", // [ Global, Transaction ]
      userFirmName: "",
      userFirstName: "",
      userMiddleName: "",
      userLastName: "",
      userEmails: [{
        emailId: "",
        emailPrimary: true
      }],
      userPhone: [{
        phoneNumber: "",
        extension: "",
        phonePrimary: true
      }],
      userFax: [{
        faxNumber: "",
        faxPrimary: true
      }],
      userEmployeeID: "",
      userEmployeeType: "",
      userJobTitle: "",
      userManagerEmail: "",
      userDepartment: "",
      userCostCenter: "",
      userExitDate: "",
      userJoiningDate: "",
      userAddresses: [{
        addressName: "",
        isPrimary: false,
        isActive: true,
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: {
          zip1: "",
          zip2: ""
        },
        formatted_address: "",
        url: "",
        location: {
          longitude: "",
          latitude: ""
        }
      }]
    },
    addressList: [],
    countryResult: [],
    searchUserState: {
      backspaceRemoves: false,
      firmValue: ""
    },
    searchFirm: {
      firmDropdown: [],
      firmValue: ""
    },
    searchUsers: {
      usersDropdown: [],
      usersValue: ""
    },
    usersList: [],
    filterUsersList: [],
    userRole: [],
    userFlags: [],
    userEntitlement: [],
    expanded: {
      business: false,
      addresslist: false,
      contactdetail: true,
      contacttype: true
    },
    loading: false,
    oldUsersDetails: {},
    userType: "",
    primaryEmails: {
      isPrimary: false,
      emailId: ""
    }
  })

  async handleSubmit(event) {
    event.preventDefault()
    this.submitted = true
    const isPrimaryAddress = false
    const { userDetails, errorUserDetail } = this.state
    const userDetailData = {
      _id: userDetails._id ? userDetails._id : "",
      entityId: userDetails.entityId,
      userFlags: userDetails.userFlags,
      userRole: userDetails.userEntitlement,
      userEntitlement: userDetails.userEntitlement,
      userFirmName: userDetails.userFirmName,
      userFirstName: userDetails.userFirstName,
      userMiddleName: userDetails.userMiddleName,
      userLastName: userDetails.userLastName,
      userEmails: userDetails.userEmails,
      userPhone: userDetails.userPhone,
      userFax: userDetails.userFax,
      userEmployeeID: userDetails.userEmployeeID,
      userJobTitle: userDetails.userJobTitle,
      userManagerEmail: userDetails.userManagerEmail,
      userCostCenter: userDetails.userCostCenter,
      userAddresses: userDetails.userAddresses
    }
    try {
      const { userAddresses, userEmails, userPhone } = userDetailData
      const primaryAddress = userAddresses.filter(item => item.isPrimary)
      const primaryEmails = userEmails.filter(email => email.emailPrimary)
      const primaryUserPhone = userPhone.filter(phone => phone.phonePrimary)
      let isPrimaryAddress = true
      if (primaryAddress.length !== 1)
        isPrimaryAddress = false
      const errorData = validateUserDetail(userDetailData)
      if (errorData.error || !isPrimaryAddress || primaryEmails.length !== 1 || primaryUserPhone.length !== 1) {
        if (errorData.error) {
          convertError(errorUserDetail, errorData)
        }
        if (!isPrimaryAddress)
          // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
          toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
        if (primaryEmails.length !== 1)
          this.state.isPrimaryEmailAddress = "One Primary email is required."
        if (primaryUserPhone.length !== 1)
          this.state.isPrimaryUserPhone = "One Primary phone is required."
        this.setState({ errorUserDetail })
      } else {
        this.setState({
          loading: true
        })
        const response = await addContactDetail(userDetailData)
        if (!response.error && response.isPrimaryAddress) {
          this.setState({
            userDetails: cloneDeep(this.initialState().userDetails),
            errorUserDetail: cloneDeep(this.initialState().userDetails),
            loading: false
          })
          this.submitted = false
          sendEmailAlert(response.success._id)
          this.props.history.push(`/${this.props.match.params.nav1}/${this.props.match.params.nav2}/${this.props.match.params.nav3}`)
          toast("Users Contact has been Created!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        } else {
          if (response.duplicateExists && response.duplError.length > 0) {
            response.duplError.forEach(email => {
              errorUserDetail.userEmails[email.value].emailId = email.label
            })
          }
          if (response.error !== "" && typeof response.error === "string")
            toast(response.error, { autoClose: 2000, type: toast.TYPE.ERROR })
          if (typeof response.error === "object" && response.error !== null)
            convertError(errorUserDetail, response)
          if (typeof response.isPrimaryAddress !== "undefined" && !response.isPrimaryAddress) {
            // errorUserDetail.userAddresses[0].addressType  = "One Primary Office  Address  is Required"
            toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
          } else
            // errorUserDetail.userAddresses[0].addressType  = ""
            this.setState({ errorUserDetail, loading: false })
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    }
  }
  /** ************************ this is for add new address ********************************* */
  addNewUserAddress = (event) => {
    event.preventDefault()
    const { userDetails } = this.state
    const { userAddresses } = userDetails
    let isEmpty = false
    userAddresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true

      }
    })
    if (!isEmpty) {
      this.setState(prevState => {
        const userDetails = {
          ...prevState.userDetails
        }
        const errorUserDetail = {
          ...prevState.errorUserDetail
        }
        userDetails.userAddresses
          .push(this.initialState().userDetails.userAddresses[0])
        errorUserDetail.userAddresses
          .push(this.initialState().userDetails.userAddresses[0])
        return { userDetails, errorUserDetail }
      })
    } else {
      toast("Already and empty address!", { autoClose: 2000, type: toast.TYPE.INFO })
    }
  }
  /** ************************ this is for reset form ********************************* */
  resetUserAddress = e => {
    e.preventDefault()
    const userAddresses = this.initialState().userDetails.userAddresses[0]
    // userAddresses.addressType = "office"
    this.setState(prevState => ({
      userDetails: {
        ...prevState.userDetails,
        userAddresses: [userAddresses]
      },
      errorUserDetail: {
        ...prevState.errorUserDetail,
        userAddresses: [
          this.initialState().userDetails.userAddresses[0]
        ]
      }
    }))
  }
  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const { userFlags } = this.state.userDetails
    if (userFlags.includes(label)) {
      userFlags.splice(userFlags.indexOf(label), 1)
    } else {
      userFlags.push(label)
    }
  }

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type) => {
    e.preventDefault()
    this.setState(prevState => {
      const addmore = this.initialState().userDetails[type][0]
      switch (type) {
        case "userEmails":
          addmore.emailPrimary = false
          break
        case "userPhone":
          addmore.phonePrimary = false
          break
        default:
          addmore.faxPrimary = false
          break
      }
      const addMoreData = {
        ...prevState.userDetails
      }
      const errorUserDetail = {
        ...prevState.errorUserDetail
      }
      addMoreData[type].push(addmore)
      errorUserDetail[type].push(this.initialState().userDetails[type][0])
      return { addMoreData }
    })
  };

  // **************************function used to update business addresses
  // *****************************
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this.state.addressList.find(item => item._id == id)
    this.setState(prevState => {
      const { errorUserDetail, userDetails, expanded } = prevState
      expanded.business = true
      errorUserDetail.userAddresses = this.cleanErrorUserDetail.userAddresses
      userDetails.userAddresses = [address]
      return { userDetails, errorUserDetail, expanded }
    })
  }
  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }
  deleteAliases = (type, idx) => {
    const { userDetails, errorUserDetail } = this.state
    let isPrimary = true
    let primaryKey = ""
    let message = ""
    switch (type) {
      case "userEmails":
        primaryKey = "emailPrimary"
        message = "Primary Email"
        break
      case "userPhone":
        primaryKey = "phonePrimary"
        message = "Primary Phone"
        break
      default:
        break
    }
    if (userDetails[type][idx][primaryKey]) {
      isPrimary = false
    }

    if (type === "userFax") {
      userDetails[type].splice(idx, 1)
      errorUserDetail[type].splice(idx, 1)
      this.setState({
        userDetails,
        errorUserDetail
      })
    }

    if (isPrimary) {
      userDetails[type].splice(idx, 1)
      errorUserDetail[type].splice(idx, 1)
      this.setState({
        userDetails,
        errorUserDetail
      })
    } else {
      toast(` ${message} can not be Deleted`, { autoClose: 2000, type: toast.TYPE.INFO })
    }
  }
  resetAliases = (e, type) => {
    e.preventDefault()
    this.setState(prevState => ({
      userDetails: {
        ...prevState.userDetails,
        [type]: this.initialState().userDetails[type]
      },
      errorUserDetail: {
        ...prevState.errorUserDetail,
        [type]: this.initialState().userDetails[type]
      }
    }))
  }
  deleteAddress = (e, type, idx) => {
    const { userDetails, errorUserDetail } = this.state
    userDetails[type].splice(idx, 1)
    errorUserDetail[type].splice(idx, 1)
    this.setState({
      userDetails,
      errorUserDetail
    })
  }
  onChangeAddressType = (e, idx) => {
    const { userDetails } = this.state
    let { addressList, isPrimaryAddress } = this.state
    let { userAddresses } = userDetails
    if (e.target.name === "isPrimary") {
      userDetails.userAddresses[idx].isPrimary = !userDetails.userAddresses[idx].isPrimary
    }
    if (this.submitted) {
      addressList = addressList.length > 0 ? addressList.filter((obj) => obj._id != userAddresses[0]._id) : []
      userAddresses = userAddresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [...addressList, ...userAddresses]
      const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
      if (filteredAddresses.length === 0)
        // isPrimaryAddress  = "One Primary Office  Address  is Required"
        toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
      else if (filteredAddresses.length > 1)
        // isPrimaryAddress  = "Only One Primary Office  Address  is Required"
        toast("Only One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
      else {
        isPrimaryAddress = ""
      }
    }
    this.setState({
      userDetails,
      isPrimaryAddress
    })
  }
  checkAddressStatus = async (e, id, field) => {
    const { addressList } = this.state
    let addressIdx = ""
    addressList.forEach((address, idx) => {
      if (address._id == id) {
        addressIdx = idx
      }
    })
    if ((field === "isPrimary" && !addressList[addressIdx].isActive) || (field === "isActive" && addressList[addressIdx].isPrimary)) {
      toast("Primary Address Can not be Inactive.", { autoClose: 2000, type: toast.TYPE.WARNING })
    } else {
      addressList[addressIdx][field] = !addressList[addressIdx][field]
      if (field === "isPrimary") {
        addressList.forEach((address, idx) => {
          if (address._id === id)
            addressList[idx][field] = true
          else
            addressList[idx][field] = false
        })
      }
      this.setState(prevState => ({
        ...prevState,
        addressList
      }))
    }
  }
  getAddressDetails = (address = "", idx) => {
    if (address !== "") {
      const { userDetails, errorUserDetail } = this.state
      let id = ""
      if (userDetails.userAddresses[idx]._id !== "") {
        id = userDetails.userAddresses[idx]._id
      }
      userDetails.userAddresses[idx] = { ...this.initialState().userDetails.userAddresses[0] }
      userDetails.userAddresses[idx]._id = id
      if (address.addressLine1 !== "") {
        userDetails.userAddresses[idx].addressName = userDetails.userAddresses[idx].addressName !== "" ? userDetails.addresses[idx].addressName : address.addressLine1
        userDetails.userAddresses[idx].addressLine1 = address.addressLine1
        errorUserDetail.userAddresses[idx].addressName = ""
        errorUserDetail.userAddresses[idx].addressLine1 = ""
      }
      if (address.country !== "") {
        errorUserDetail.userAddresses[idx].country = ""
        userDetails.userAddresses[idx].country = address.country
      }
      if (address.state !== "") {
        errorUserDetail.userAddresses[idx].state = ""
        userDetails.userAddresses[idx].state = address.state
      }
      if (address.city !== "") {
        errorUserDetail.userAddresses[idx].city = ""
        userDetails.userAddresses[idx].city = address.city
      }
      if (address.zipcode !== "") {
        errorUserDetail.userAddresses[idx].country = ""
        userDetails.userAddresses[idx].zipCode.zip1 = address.zipcode
      }
      userDetails.userAddresses[idx].formatted_address = address.formatted_address
      userDetails.userAddresses[idx].url = address.url
      userDetails.userAddresses[idx].location.longitude = address.location.longitude.toString()
      userDetails.userAddresses[idx].location.latitude = address.location.latitude.toString()
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          ...userDetails
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          ...errorUserDetail
        }
      }))
    }
  }
  render() {
    const loading = () => <Loader />
    return (
      <div className="users">
        {this.state.loading ? loading() : null}
        <section className="accordions">
          <ContactType
            {...this.state}
            toggleButton={this.toggleButton}
            onChangeUserDetail={this.onChangeUserDetail}
            canEdit
            canView={false}
          />
          <UserContact
            {...this.state}
            addMore={this.addMore}
            onChangeAddMore={this.onChangeAddMore}
            onChangeUserDetail={this.onChangeUserDetail}
            userType="client"
            onSearchFirms={this.onSearchFirms}
            onSearchUsers={this.onSearchUsers}
            getUsersList={this.getUsersList}
            deleteAliases={this.deleteAliases}
            resetAliases={this.resetAliases}
            onSearch={this.onFilterUsers}
            addNewUsers={this.addNewUsers}
            toggleButton={this.toggleButton}
            canEdit
            canView={false}
          />
          <article className={this.state.expanded.business ? "accordion is-active" : "accordion"}>
            <div className="accordion-header">
              <p
                onClick={event => {
                  this.toggleButton(event, "business")
                }}
              >Address</p>
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={this.addNewUserAddress}>
                    Add
                          </button>
                </div>
                <div className="control">
                  <button
                    className="button is-light is-small"
                    onClick={this.resetUserAddress}>
                    Reset
                          </button>
                </div>
              </div>
            </div>
            {
              this.state.expanded.business &&
              <div className="accordion-body">
                {this.state.userDetails.userAddresses.map((userAddress, idx) => (
                  <UserAddress
                    key={idx}
                    userAddress={userAddress}
                    idx={idx}
                    onChangeUserAddress={this.onChangeUserDetail}
                    errorUserDetail={this.state.errorUserDetail}
                    countryResult={this.state.countryResult}
                    deleteAddress={this.deleteAddress}
                    onChangeAddressType={this.onChangeAddressType}
                    getAddressDetails={this.getAddressDetails}
                    isPrimaryAddress={this.state.isPrimaryAddress}
                    canEdit
                    canView={false}
                    busy={this.state.loadingPickLists}
                  />))
                }
              </div>
            }
          </article>
          <br />
          <div className="columns">
            <div className="column is-full">
              <div className="field is-grouped-center">
                <div className="control">
                  <button className="button is-link" onClick={this.handleSubmit}>
                    Save
                          </button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { admUserDetail, auth } = state
  return { admUserDetail, auth }
}
const mapDispatchToProps = {
  saveUserDetail,
  getUserDetailById
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UsersContainers))
