import React from "react"
import { DropdownList } from "react-widgets"

const SearchUsers = props => (
  <section className="accordions">
    <div className="tile is-ancestor">
      <div className="tile is-vertical is-8">
        <div className="tile">
          <div className="tile is-parent is-vertical">

            <article className="tile is-child">
              <p className="title innerPgTitle">You selected...</p>
              <p className="subtitle">{props.userFirmName}</p>
            </article>
          </div>
        </div>
      </div>
    </div>
    <p className="title innerPgTitle">Search by contact name to check if contact already exists in your database</p>
    <div className="columns">
      <div className="column">
        <DropdownList
          filter="contains"
          value={props.searchUsers.usersValue}
          data={props.usersList}
          message="select MSRB Firm Name"
          textField='label' valueField="value"
          onChange={props.onSearchUsers} />
      </div>
    </div>
  </section>
)

export default SearchUsers