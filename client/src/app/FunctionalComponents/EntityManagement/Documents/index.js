import React from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import {toast} from "react-toastify"
import {
  getEntityDocs,
  updateEntityDocs,
  saveEntityDocs,
  getFirmById,
  deleteEntityDoc
} from "AppState/actions/AdminManagement/admTrnActions"
import CONST, {ContextType} from "GlobalUtils/consts"
import Loader from "../../../GlobalComponents/Loader"
import  DocumentPage from "../../../GlobalComponents/DocumentPage"
import { checkPlatformAdmin, checkNavAccessEntitlrment, getAllTenantUserDetails } from "GlobalUtils/helpers"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"

class Documents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      documentsList: [],
      participants: [],
      firmName: "",
      loading: true,
      canEdit:false,
      canView:false
    }
  }

  async componentWillMount() {
    const { match, nav2} = this.props
    const {nav1} = match.params
    const action = await checkNavAccessEntitlrment(nav2)
    const {edit, view} = action || {}

    let {canEdit, canView} = this.state
    const firmDetailById = await getFirmById(nav2)
    const isPlatformAdmin = await checkPlatformAdmin()
    this.onParticipantsRefresh()
    if(isPlatformAdmin) {
      canEdit=true
    } else {
      canEdit = edit || false
      canView = view || false
    }
    this.setState({
      loading:true,
      canEdit,
      canView
    })
    if(!canEdit && !canView){
      const url = nav1==="thirdparties" ? "mast-thirdparty":"mast-cltprosp"
      this.props.history.push(`/${url}`)
    }
    this.setState({
      loading:true
    })
    getEntityDocs(this.props.nav2, (res)=> {
      if (res && res.status === 200) {
        this.setState({
          loading:false,
          firmName: firmDetailById && firmDetailById.firmName,
          documentsList: (res.data && res.data[0].entityDocuments) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          loading:false,
          documentsList: [],
        })
      }
    })
  }

  onDocSave = (docs, callback) => {
    saveEntityDocs(docs, (res)=> {
      if (res && res.status === 200) {
        toast("Documents has been inserted!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.saveAuditLogs()
        this.setState({
          documentsList: (res.data && res.data.entityDocuments) || [],
        }, () => {
          callback({
            status: true,
            documentsList: (res.data && res.data.entityDocuments) || [],
          })
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        [name]: value
      }
      type = "status"
    }else {
      document = {
        ...doc
      }
      type = "meta"
    }

    const res = await updateEntityDocs(this.props.nav2, `?type=${type}`, {_id: doc._id, ...document})
    if (res && res.status === 200) {
      toast("Document has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.saveAuditLogs()
      if(name){
        callback({
          status: true,
        })
      }else {
        this.setState({
          documentsList: (res.data && res.data.document && res.data.document.entityDocuments) || [],
        }, () => {
          callback({
            status: true,
            documentsList: (res.data && res.data.document && res.data.document.entityDocuments) || [],
          })
        })
      }
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onDeleteDoc = async (documentId, callback) => {
    const res = await deleteEntityDoc(this.props.nav2,documentId)
    if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.saveAuditLogs()
      callback({
        status: true,
        documentsList: (res.data && res.data[0].entityDocuments) || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  saveAuditLogs = async () => {
    const {auditLogs} = this.props
    if(auditLogs && auditLogs.length) {
      await this.props.submitAuditLogs(this.props.nav2)
    }
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await getAllTenantUserDetails()
    })
  }

  render() {
    const { documentsList, canEdit, canView, firmName, participants } = this.state
    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }
    return(
      <div>
        <div className="column">
          <div className="tile is-ancestor">
            <div className="tile is-vertical is-8">
              <div className="tile">
                <div className="tile is-parent is-vertical">

                  <article className="tile is-child">
                    <p className="title innerPgTitle">You selected...</p>
                    <p className="subtitle">{firmName || ""}</p>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DocumentPage {...this.props}
          onSave={this.onDocSave}
          isNotTransaction
          participants={participants || []}
          tranId={this.props.nav2}
          title="Upload Documents"
          nav1="clients-propects"
          pickCategory="LKUPDOCCATEGORIES"
          pickSubCategory="LKUPCORRESPONDENCEDOCS"
          pickAction="LKUPDOCACTION"
          documents={documentsList}
          contextType={this.props.contextType || ContextType.entity}
          onStatusChange={this.onStatusChange}
          onDeleteDoc={this.onDeleteDoc}
          isDisabled={!canEdit}
          canView = {canView}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Documents)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
