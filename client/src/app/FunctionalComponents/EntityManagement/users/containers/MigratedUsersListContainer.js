import React, { Component } from "react"
import "react-table/react-table.css"
import Fuse from "fuse.js"
import {Link, withRouter} from "react-router-dom"
import { connect } from "react-redux"
import EntityPageFilter from "./../../../EntityManagement/CommonComponents/EntityPageFilter"
import "./../../scss/entity.scss"

class MigratedUsersListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  fuse = e => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    const res = fuse.search(search)
    return res
  }

  render() {
    return (
      <div>
        <EntityPageFilter
          listType={this.props.listType || "users"}
          auth={this.props.auth}
          entityId=""
          activeTab={this.props.activeTab}
          nav1={this.props.nav1 || ""}
          searchPref="mast-users-migrated"
          title="Users"
          routeType="users"
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MigratedUsersListContainer)
)
