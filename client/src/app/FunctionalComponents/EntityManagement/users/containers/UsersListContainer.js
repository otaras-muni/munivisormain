import React, { Component } from "react"
import { withRouter, Link } from "react-router-dom"
import { connect } from "react-redux"
import "./../../scss/entity.scss"
import EntityPageFilter from "../../CommonComponents/EntityPageFilter"

class UsersListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: {
        pagefilter: false
      },
      loading: false,
      usersList: [],
      search: "",
      pageSizeOptions: [5, 10, 20, 25, 50, 100],
      pageSize: 25,
      countryResult: [],
      filteredValue: {
        entityType: "",
        enityState: [],
        entitySearch: "",
        groupBy: ""
      },
      searchname: "",
      entityId: "",
      isSaving: false,
      marketRoleList: []
    }
  }

  addNewUsers = () => {
    this.setState({
      viewList: "addNewUsers"
    })
  }

  render() {
    return (
      <div>
        <EntityPageFilter
          listType={this.props.listType || "users"}
          auth={this.props.auth}
          entityId=""
          activeTab={this.props.activeTab}
          nav1={this.props.nav1 || ""}
          searchPref="mast-users"
          title="Users"
          routeType="users"
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UsersListContainer)
)
