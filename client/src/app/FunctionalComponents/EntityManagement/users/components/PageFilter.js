import React from "react"
import { Multiselect, DropdownList } from "react-widgets"
import { Link } from "react-router-dom"

const PageFilter = props => (

  <section className="accordions">
    <article className={props.expanded.pagefilter ? "accordion is-active" : "accordion"}>
      <div className="accordion-header toggle">
        <p
          onClick={event => {
            props.toggleButton(event, "pagefilter")
          }}
        >
          Page Filters
        </p>
        <span>
          <i className={props.expanded.pagefilter ? "fas fa-chevron-up" : "fas fa-chevron-down"} onClick={event => {
            props.toggleButton(event, "pagefilter")
          }} />
        </span>
      </div>
      {props.expanded.pagefilter && <div className="accordion-body">
        <div className="accordion-content">
          <div className="columns">
            <div className="column">

              <div className="select is-fullwidth is-small is-link">
                <select
                  name="groupBy"
                  onChange={e => props.onChangeFilter(e)}
                  value={props.filteredValue.groupBy}
                >
                  <option disabled="" value="">Group by...</option>
                  <option value="entityname">Associated Entity Name</option>
                  <option value="state">State</option>
                </select>
                {props.loadingPickLists ? <div style={{ padding: 30 }} className="is-link"><span className="fas fa-sync fa-spin is-link" /></div> : null}
              </div>

            </div>
            <div className="column">

              <div className="select is-fullwidth is-small is-link">
                <select
                  name="entityType"
                  onChange={e => props.onChangeFilter(e)}
                  value={props.filteredValue.entityType}
                >
                  <option value="">Filter by...</option>
                  <option value="Client">Clients only</option>
                  <option value="Prospect">Prospects only</option>
                  <option value="ClentProspect">Clients &amp; Prospects</option>
                  {
                    props.marketRoleList.length > 0 && props.marketRoleList.map(item => (
                      <option key={Math.random()} value={item}>{item} Only</option>
                    ))
                  }
                </select>
              </div>

            </div>
            <div className="column">
              <div className="select is-link is-fullwidth is-small multi-select-hack">
                <Multiselect
                  filter
                  placeholder="Select State"
                  value={props.filteredValue.enityState}
                  name="enityState"
                  data={props.filteredValue.enityState && props.countryResult.length > 0 && props.countryResult[2]["United States"] ? [...props.countryResult[2]["United States"]] : []}
                  message="select State"
                  textField='state'
                  busy={props.loadingPickLists}
                  busySpinner={
                    <span className="fas fa-sync fa-spin is-link" />
                  }
                  onChange={(val) => {
                    const event = {
                      target: {
                        name: "enityState",
                        value: val
                      }
                    }
                    props.onChangeFilter(event)
                  }} />
              </div>
            </div>
            <div className="column">
              <div className="select is-fullwidth is-small is-link">
                <select
                  onChange={e => props.onPageSizeChange(Number(e.target.value))}
                  value={props.pageSize}
                >
                  <option value="">Results/Page</option>
                  {props.pageSizeOptions.map((option, i) => (
                    <option key={i} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="column">
              <button className="button is-link is-fullwidth is-small"
                disabled={props.isSaving}
                onClick={props.handleSavePref}>{
                  props.isSaving
                    ? "Saving..."
                    : "Save as my default view"
                }</button>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <p className="control has-icons-left">
                <input className="input  is-link is-fullwidth is-small" type="text"
                  placeholder="search by contact name or associated entity"
                  name="entitySearch"
                  value={props.filteredValue.entitySearch}
                  onChange={e => props.onChangeFilter(e)}
                //  onBlur={e => props.onChangeFilter(e)}
                />
                <span className="icon is-small is-left has-background-black has-text-white">
                  <i className="fas fa-search" />
                </span>
              </p>
            </div>
            <div className="column is-one-fifth">
              <Link className="button  is-link is-fullwidth is-small" to="/addnew-contact">Add new contact</Link>
            </div>
          </div>
        </div>
      </div>}
    </article>
  </section>
)
export default PageFilter