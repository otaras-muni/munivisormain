import React from "react"

const UserAddOns = (props) => (
  <article className="accordion is-active">
    <div className="accordion-header">
      <p>Firm Level</p>
    </div>
    <div className="accordion-body">
      <div className="accordion-content">

        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl">Add-On Name</p>
              </th>
              <th>
                <p className="multiExpLbl">Enabled</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {
              props.firmAddOns.map((element, idx) => (
                <tr key={idx}>
                  <td>
                    {element.serviceName}
                  </td>
                  <td>
                    <p className="emmaTablesTd">
                      <input type="checkbox"
                        name="addOns"
                        value={element.serviceName}
                        defaultChecked={element.serviceEnabled}
                        onClick={(event)=>{
                          props.onChangeAddOns(event, idx)
                        }}
                      />
                    </p>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

      </div>
    </div>
  </article>
)

export default UserAddOns