import React, { Component } from "react"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import {toast} from "react-toastify"
import swal from "sweetalert";
import {connect} from "react-redux"
import uuidv4 from "uuid"
import cloneDeep from "lodash.clonedeep"
import {
  delDealsRating, fetchDealsTransaction, addOrUpdateDealsRating, sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal";

// Get the deal ID based on the navigation item

// const Series = ["Series 1", "Series 2", "Series 3"]

const dealAgencyRatingsReference = {
  "Fitch":["AAA","AA","A+"],
  "Moodys":["AAA","AA","A+"],
  "S & P":["A1","A2","AA1","B1","BB1","CC2"]
}

const dealCEPRatingsReference = {
  "CEP1":["AA","AAA","A","B","C","CCC"],
  "CEP2":["AA","AAA","A","B","C","CCC"],
  "CEP3":["A1","A2","AA1","B1","BB1","CC2"]
}

const accordionColumns = [
  {
    sectionKeyInfo:"dealAgencyRatings",
    sectionHeader:"Deal Ratings",
    columnheaders:{
      "Series Code":"Series Code",
      "Rating Agency":"Rating Agency",
      "Long Term Rating":"Long Term Rating",
      "Long Term Outlook":"Long Term Outlook",
      "Short Term Rating":"Short Term Rating",
      "Short Term Outlook":"Short Term Outlook",
      "Delete":""
    } ,
    columns:{
      seriesId:"",
      ratingAgencyName: "",
      longTermRating: "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: "",
      saveEditFlag:0,
      dirty:true// 0 is initial, 1 means saved, 2 means edit
    },
  },
  {
    sectionKeyInfo:"dealCepRatings",
    sectionHeader:"Deal CEP Ratings",
    columnheaders:{
      "Series Code":"Series Code",
      "CEP":"CEP Provider",
      "CEP Type":"CEP Type",
      "Long Term Rating":"Long Term Rating",
      "Long Term Outlook":"Long Term Outlook",
      "Short Term Rating":"Short Term Rating",
      "Short Term Outlook":"Short Term Outlook",
      "Delete":""
    } ,

    columns:{
      seriesId:"",
      cepName: "",
      cepType: "",
      longTermRating: "",
      longTermOutlook: "",
      shortTermOutlook: "",
      shortTermRating: "",
      saveEditFlag:0, // 0 is initial, 1 means saved, 2 means edit,
      dirty:true

    },
  }
]

const cols = [
  [
    {name: "Series Code"},
    {name: "Rating Agency"},
    {name: "Long Term Rating"},
    {name: "Long Term Outlook"},
    {name: "Short Term Rating"},
    {name: "Short Term Outlook"},
    {name: "Edit/Delete"}],
  [
    {name: "Series Code"},
    {name: "CEP"},
    {name: "CEP Type"},
    {name: "Long Term Rating"},
    {name: "Long Term Outlook"},
    {name: "Short Term Rating"},
    {name: "Short Term Outlook"},
    {name: "Edit/Delete"}],
]

class SeriesDealRatings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: "",
      accordionData:{
        dealAgencyRatings: [],
        dealCepRatings:[]
      },
      errors:{
        dealAgencyRatings:[],
        dealCepRatings:[]
      },
      dropDown: {
        ratingTerm: {},
        ceps: {},
        cepTypes: []
      },
      accordions:[],
      seriesRatingsList: [],
      cepRatingsList: [],
      Series: [],
      isEditable: {},
      loading: true,
      isSaveRatingDisabled: false,
      isSaveCepRatingDisabled: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      },
      confirmAlert: CONST.confirmAlert
    }
  }

  getDealDetails = (dealId) => {
    fetchDealsTransaction("series", dealId, (res) => {
      if(res && res.dealIssueSeriesDetails) {
        const state = this.setAccordionState(res.dealIssueSeriesDetails)
        this.setState(prevState => ({
          ...state,
          email: {
            ...prevState.email,
            subject: `Transaction - ${res.dealIssueTranProjectDescription || res.dealIssueTranIssueName} - Notification`
          }
        }))
      } else {
        this.setState({
          loading: false,
        })
      }
    })
  }

  setAccordionState = (dealIssueSeriesDetails) => {
    let  isEditable = {}
    let seriesRatingsList = []
    let cepRatingsList = []
    dealIssueSeriesDetails.forEach((d) => {
      seriesRatingsList = seriesRatingsList.concat(d.seriesRatings)
      cepRatingsList = cepRatingsList.concat(d.cepRatings)
    })
    if(!seriesRatingsList.length){
      const row =  this.state.accordions.find( (acc) => acc.accIndex === "dealAgencyRatings")
      seriesRatingsList = [row.accDefaultRow]
      isEditable = {
        ...isEditable,
        dealAgencyRatings: 0
      }
    }
    if(!cepRatingsList.length){
      const row =  this.state.accordions.find( (acc) => acc.accIndex === "dealCepRatings")
      cepRatingsList = [row.accDefaultRow]
      isEditable = {
        ...isEditable,
        dealCepRatings: 0
      }
    }

    return {
      accordionData:{
        dealAgencyRatings: seriesRatingsList,
        dealCepRatings: cepRatingsList
      },
      seriesRatingsList,
      cepRatingsList,
      Series: dealIssueSeriesDetails,
      loading: false,
      isEditable,
    }
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName(["LKUPCEPRATINGS", "LKUPCREDITENHANCEMENTTYPE", "LKUPRATING"])
    console.log("RATING COMPONENT DEBUG", picResult)
    const result =  picResult[1] || {}
    const ratingTerm =  (picResult[2] && picResult[2].LKUPRATING) || {}
    const ceps = (picResult[2] && picResult[2].LKUPCEPRATINGS) || {}

    const accordionInitialConfig = accordionColumns.map((r) => ({accIndex:r.sectionKeyInfo,accOpen:false, accHeaderInfo:r.columnheaders, accDefaultRow:r.columns,accHeader:r.sectionHeader}))
    const accordionInitialData = accordionInitialConfig.reduce((finalvalue,{accIndex,accDefaultRow}) => {
      const a = []
      a.push({...accDefaultRow})
      return {...finalvalue,...{[accIndex]:a}}
    },{})

    const accordionInitialErrors = accordionInitialConfig.reduce((finalerrorvalues,{accIndex,accDefaultRow}) => {
      const a = []
      const {saveEditFlag,...errorInit} = {...accDefaultRow}
      a.push({...errorInit})
      return {...finalerrorvalues,...{[accIndex]:a}}
    },{})

    this.setState( (prevState) => ({
      ...prevState,
      dropDown: {
        ratingTerm,
        ceps,
        cepTypes: result.LKUPCREDITENHANCEMENTTYPE,
      },
      ...{ accordions:accordionInitialConfig, errors:accordionInitialErrors},
      userName: (this.props.user && this.props.user.userFirstName) || ""}),() => {
      const dealId = this.props.nav2
      if(dealId) {
        this.getDealDetails(dealId)
      }
    })
  }

  handleFieldChange = (aIndex,rowIndex,fieldPos, e, depFields) => {
    const {accordionData, userName} = this.state
    accordionData[aIndex][rowIndex][e.target.name] = e.target.type === "checkbox" ?  e.target.value : e.target.value
    this.props.addAuditLog({userName, log: `Deals Rating In ${aIndex}, ${e.target.name || "empty"} change to ${e.target.type === "checkbox" ? e.target.checked : e.target.value || "empty"} `, date: new Date(), key: aIndex})
    this.setState({accordionData})
  }

  toggleAccordion =(aIndex) => {
    const { multiple } = this.props
    const { accordions } = this.state
    if(multiple) {
      const newMultipleState = accordions.map ( ({accIndex,accOpen,...rest}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen,...rest} : {accIndex,accOpen,...rest})
      this.setState(  {accordions:newMultipleState} )
    } else {
      const newSingleState = accordions.map ( ({accIndex,accOpen,...rest}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen,...rest} : {accIndex,accOpen:false,...rest})
      this.setState(  {accordions:newSingleState} )
    }
  }

  addAccordionRow = (aIndex) => {
    const {userName} = this.state
    const [accordionDefaultData] = cloneDeep(this.state.accordions.filter( ({accIndex}) => accIndex === aIndex))

    if(!accordionDefaultData.accOpen) {
      this.toggleAccordion(aIndex)
    }

    const newAccordionDataState =  cloneDeep(this.state.accordionData)
    const newErrors = {...this.state.errors}
    const errorData = newErrors[aIndex]
    const accordionData = cloneDeep(newAccordionDataState[aIndex])
    const totalEmptyRows = accordionData.reduce ( (totalEmpty,rowdata) => {
      const rowEmpty = Object.values(rowdata).reduce( (isEmpty, field) => isEmpty && (field === "" || field === 0 || field === true),true)
      return totalEmpty + rowEmpty * 1
    },0)
    // Are there any rows that are already empty. If so don't insert a row
    if(totalEmptyRows === 0) {
      accordionData.push(cloneDeep(accordionDefaultData.accDefaultRow))
      const {saveEditFlag,...errorInit} = cloneDeep(accordionDefaultData.accDefaultRow)
      errorData.push(errorInit)

      this.setState(prevState => ({
        accordionData:{...newAccordionDataState,...{[aIndex]:accordionData}},
        errors:{...newErrors,...{[aIndex]:errorData}},
        isEditable: {
          ...prevState.isEditable,
          [aIndex]: accordionData.length ? accordionData.length-1 : 0,
        }
      }))
    }
    this.props.addAuditLog({userName, log: `Deals Rating In ${aIndex} add new row`, date: new Date(), key: aIndex})
    this.setState(prevState => prevState)
  }

  saveEditAccordionFlag = (id, aIndex, rowId, row) => {
    const newAccordionDataState =   cloneDeep(this.state.accordionData)
    const newErrors = { ...cloneDeep(this.state.errors) }
    const accordionData = [...newAccordionDataState[aIndex]]
    let isInValid = false
    if(row && Object.keys(row).length) {
      isInValid = Object.keys(row).some(key => (typeof(row[key]) === "string" && !row[key]))
    }

    if(isInValid){
      const newErrorInfo =  accordionData.map ( row => {
        const keys = Object.keys(row)
        const values = Object.values(row).reduce( (errorAcc, val,index)=> {
          const errorString = val === "" ? "Required" : ""
          return {...errorAcc,...{[keys[index]]:errorString}}
        },{})
        return values
      })
      this.setState({errors:{...newErrors,...{[aIndex]:newErrorInfo}}})
      return
    }

    const isDisabled = aIndex === "dealAgencyRatings" ? "isSaveRatingDisabled" : "isSaveCepRatingDisabled"
    const Toast = aIndex === "dealAgencyRatings" ? "Ratings" : "CEP Ratings"
    this.setState({
      [isDisabled]: true
    }, () => {
      addOrUpdateDealsRating(aIndex === "dealAgencyRatings" ? "dealAgencyRatings" : "dealCepRatings", this.props.nav2, row, (res) => {
        if (res && res.status === 200) {
          toast(`Deal ${Toast} has been updated!`, {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
          const {accordionData} = this.state
          accordionData[aIndex][rowId] = row
          const state = this.setAccordionState(res.data || {})
          this.setState({
            accordionData,
            ...state,
            [isDisabled]: false
          },async () => {
            this.props.submitAuditLogs(this.props.nav2)
            this.onCancel(aIndex)
          })
        } else {
          toast("Something went wrong!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
          this.setState({
            [isDisabled]: false
          })
          console.log(res)
        }
      })
    })
  }

  onEdit = (key, index) => {
    const {userName} = this.state
    if(this.state.isEditable[key]) { swal("Warning", `First save the current ${key === "dealAgencyRatings" ? "Deal Ratings" : "Deal CEP Ratings"}`, "warning"); return } ;
    this.props.addAuditLog({userName, log: `Deals Rating In ${key} one row edited`, date: new Date(), key})
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index,
      },
    })
  }

  onCancel = (aIndex) => {
    const newAccordionDataState =   cloneDeep(this.state.accordionData)
    const newErrors = {... cloneDeep(this.state.errors)}
    const accordionData = [... cloneDeep(newAccordionDataState[aIndex])]

    const newErrorInfo =  accordionData.map ( row => {
      const keys = Object.keys(row)
      const values = Object.values(row).reduce( (errorAcc, val,index)=> {
        const errorString = val === "" ? "Required" : ""
        return {...errorAcc,...{[keys[index]]:errorString}}
      },{})
      return values
    })

    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [aIndex]: "",
      },
      errors:{...newErrors,...{[aIndex]:newErrorInfo}}
    })
  }

  onRemoveRating = (type, series, ratingId, index) => {
    const {userName, errors, confirmAlert} = this.state
    //if(this.state.isEditable[type]) { swal("Warning", `First save the current ${type === "dealAgencyRatings" ? "Deal Ratings" : "Deal CEP Ratings"}`, "warning"); return }
    confirmAlert.text = `You want to delete this ${type === "dealAgencyRatings" ? "Deal Ratings" : "Deal CEP Ratings"}?`
    swal(confirmAlert)
      .then((willDelete) => {
        if (willDelete) {
          errors[type].splice(index, 1)
          if (series && ratingId) {
            this.props.addAuditLog({userName, log: `Deals Rating In ${type} one removed`, date: new Date(), key: type})
            const isDisabled = type === "dealAgencyRatings" ? "isSaveRatingDisabled" : "isSaveCepRatingDisabled"
            this.setState({
              [isDisabled]: true
            }, () => {
              delDealsRating(type, this.props.nav2, series, ratingId, (res) => {
                if (res && res.status === 200) {
                  toast(`${type === "dealAgencyRatings" ? "Deal Ratings" : "Deal CEP Ratings"} removed successfully!`, {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
                  const state = this.setAccordionState((res.data && res.data.dealIssueSeriesDetails) || {})
                  this.props.submitAuditLogs(this.props.nav2)
                  this.setState({
                    ...state,
                    [isDisabled]: false,
                    isEditable: {
                      ...this.state.isEditable,
                      [type]: "",
                    },
                  })
                } else {
                  toast("Something went wrong!", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
                  this.setState({
                    [isDisabled]: false
                  })
                }
              })
            })
          } else {
            const {accordionData} = this.state
            const auditLogs = this.props.auditLogs.filter(x => x.key !== type)
            this.props.updateAuditLog(auditLogs)
            accordionData[type].splice(index, 1)
            this.setState({
              accordionData,
              isEditable: {
                ...this.state.isEditable,
                [type]: "",
              },
            })
          }
        }
      })
  }

  resetAccordion = (aIndex) => {
    // const { seriesRatingsList, cepRatingsList, accordionData} = this.state
    const accordionData = cloneDeep(this.state.accordionData[aIndex])
    /*if(aIndex === "dealAgencyRatings") {
      accordionData = seriesRatingsList
    }else if(aIndex === "dealCepRatings") {
      accordionData = cepRatingsList
    }*/
    // const newAccordionDataState =  Object.assign({},this.state.accordionData)
    const newErrors = {...this.state.errors}
    // const accordionData = [...newAccordionDataState[aIndex]]
    const oldErrorData = [...newErrors[aIndex]]
    const newAccordionRowData = accordionData.filter ( row => row && (!row.dirty || row.dirty === undefined || row.dirty === false ))
    const newErrorData = oldErrorData.filter( (r, index) => accordionData[index] && (!accordionData[index].dirty || accordionData[index].dirty === undefined || accordionData[index].dirty === false ))
    // If flag is 1 then ensure thta there are values
    const auditLogs = this.props.auditLogs.filter(x=> x.key !== aIndex)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      accordionData: {
        ...this.state.accordionData,
        [aIndex]: newAccordionRowData
      },
      errors:{
        ...newErrors,
        [aIndex]:newErrorData
      },
      isEditable: {
        ...this.state.isEditable,
        [aIndex]: "",
      },
    })
  }

  renderAccordionHeaders = (aIndex) => {
    const [accordionDefaultData] = this.state.accordions.filter( ({accIndex}) => accIndex === aIndex)
    const fieldHeaders = Object.keys(accordionDefaultData.accHeaderInfo)
    return (
      <thead>
        <tr>
          {
            fieldHeaders.map ( (col) => (
              <th key={col}>
                <abbr title={accordionDefaultData.accHeaderInfo[col]}>{col}</abbr>
              </th>
            ))
          }
        </tr>
      </thead>
    )
  }

  renderBodyRevised =(aIndex) => {

    const { accordionData,errors, dropDown, Series } = this.state
    const {tranAction} = this.props;
    const valuesToRender = [...accordionData[aIndex]]
    const errorsToRender = [...errors[aIndex]]
    const isDisabled = aIndex === "dealAgencyRatings" ? this.state.isSaveRatingDisabled : this.state.isSaveCepRatingDisabled

    return valuesToRender.map( (row,rowIndex) => {
      let valSet = []
      const id = row._id || ""
      if(aIndex === "dealAgencyRatings"){
        valSet = ["seriesId", "ratingAgencyName", "longTermRating", "longTermOutlook", "shortTermOutlook", "shortTermRating"]
      } else {
        valSet = ["seriesId", "cepName", "cepType", "longTermRating", "longTermOutlook", "shortTermOutlook", "shortTermRating"]
      }
      const rowValues = []
      valSet.forEach(key => {
        rowValues.push(row[key])
      })

      const rowErrors = errorsToRender[rowIndex] || {}
      const flag = row.saveEditFlag
      const isEditable = this.state.isEditable[aIndex] === rowIndex ? 0 : 1

      let agencyRate = {}
      let cepRate = {}
      if(aIndex === "dealAgencyRatings"){
        const isExistsAgency = accordionData[aIndex].map(e => (e.seriesId === rowValues[0] && e.ratingAgencyName === rowValues[1]) || (e.seriesId === rowValues[0]) && e.ratingAgencyName)
        Object.keys(dropDown.ratingTerm).filter(e => {
          if(isExistsAgency.indexOf(e) === -1) {
            agencyRate = {
              ...agencyRate,
              [e]: dropDown.ratingTerm[e]
            }
          }
        })
      } else {
        const isExistsCep = accordionData[aIndex].map(e => (e.seriesId === rowValues[0] && e.cepName === rowValues[1]) || (e.seriesId === rowValues[0]) && e.cepName)
        Object.keys(dropDown.ceps).filter(e => {
          if(isExistsCep.indexOf(e) === -1) {
            cepRate = {
              ...cepRate,
              [e]: dropDown.ceps[e]
            }
          }
        })
      }

      switch(isEditable) {
      case 1:
        return <tbody key={uuidv4()}>
          <tr>
            {rowValues.map( (val,i) =>
              (<td key={uuidv4()}>
                {valSet[i] === "seriesId" ? <small>{Series.map((s) => s._id === val && s.seriesName)}</small> :<small>{val}</small>}
                {rowErrors[valSet[i]] ? <p className="text-error">{rowErrors[[valSet[i]]]}</p> : null }
              </td>))
            }
            {tranAction && tranAction.canEditTran ?
            <td>
              <span className={`has-text-link ${isDisabled ? "isDisabled" : ""}`} style={{cursor: "pointer"}} onClick={!isEditable ? () => this.saveEditAccordionFlag(id, aIndex, rowIndex, row) : () => this.onEdit(aIndex, rowIndex)}>
                {isEditable ? <i className="fas fa-pencil-alt" /> : <i className="far fa-save"/> }
              </span>{"   "}
              <span className={`has-text-link ${isDisabled ? "isDisabled" : ""}`} style={{cursor: "pointer"}} onClick={() => this.onRemoveRating(aIndex, rowValues[0], id, rowIndex)}>
                <i className="far fa-trash-alt"/>
              </span>
            </td> : null }
          </tr>
        </tbody>
      default: {
        return (
          <tbody key={uuidv4()}>
            <tr key={uuidv4()}>
              <td key={uuidv4()}>
                <div className="select is-small is-link">
                  <select value={rowValues[0]} name={valSet[0]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 0, e, [])}>
                    {["Pick", ...(Series || [])].map((s, i) => <option key={i} value={i === 0 ? "" : s._id}>{s.seriesName}</option>)}
                  </select>
                </div>
                {rowErrors[valSet[0]] ? <p className="text-error">{rowErrors[valSet[0]]}</p> : null}
              </td>
              <td key={uuidv4()}>
                <div className="select is-small is-link">
                  {aIndex === "dealAgencyRatings" ?
                    <select value={rowValues[1]} name={valSet[1]}
                      onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 1, e, [2, 3, 4, 5])}>
                      {["Pick", ...Object.keys(agencyRate)].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>)}
                    </select> :
                    <select value={rowValues[1]} name={valSet[1]}
                      onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 1, e, [2, 3, 4, 5])}>
                      {["Pick", ...Object.keys(cepRate)].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>)}
                    </select>}
                </div>
                {rowErrors[valSet[1]] ? <p className="text-error">{rowErrors[valSet[1]]}</p> : null}
              </td>
              <td key={uuidv4()}>
                <div className="select is-small is-link">
                  {aIndex === "dealAgencyRatings" ?
                    <select value={rowValues[2]} name={valSet[2]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 2, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ratingTerm[rowValues[1]] || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s} >{s}</option>) :
                        <option disabled value=""/>}
                    </select> :
                    <select value={rowValues[2]} name={valSet[2]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 2, e, [])}>
                      {["Pick", ...(dropDown.cepTypes || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>)}
                    </select>}
                </div>
                {rowErrors[valSet[2]] ? <p className="text-error">{rowErrors[valSet[2]]}</p> : null}
              </td>
              <td key={uuidv4()}>
                <div className="select is-small is-link">
                  {aIndex === "dealAgencyRatings" ?
                    <select value={rowValues[3]} name={valSet[3]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 3, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ratingTerm[rowValues[1]] || []) ].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select> :
                    <select value={rowValues[3]} name={valSet[3]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 3, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ceps[rowValues[1]] || []) ].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select>
                  }
                </div>
                {rowErrors[valSet[3]] ? <p className="text-error">{rowErrors[valSet[3]]}</p> : null}
              </td>
              <td key={uuidv4()}>
                <div className="select is-small is-link">
                  {aIndex === "dealAgencyRatings" ?
                    <select value={rowValues[4]} name={valSet[4]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 4, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ratingTerm[rowValues[1]] || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select> :
                    <select value={rowValues[4]} name={valSet[4]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 4, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ceps[rowValues[1]] || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select>}
                </div>
                {rowErrors[valSet[4]] ? <p className="text-error">{rowErrors[valSet[4]]}</p> : null}
              </td>
              <td key={uuidv4()}>
                <div className="select is-small is-link">
                  {aIndex === "dealAgencyRatings" ?
                    <select value={rowValues[5]} name={valSet[5]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 5, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ratingTerm[rowValues[1]] || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select> :
                    <select value={rowValues[5]} name={valSet[5]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 5, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ceps[rowValues[1]] || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select>}
                </div>
                {rowErrors[valSet[5]] ? <p className="text-error">{rowErrors[valSet[5]]}</p> : null}
              </td>
              {aIndex === "dealAgencyRatings" ? null :
                <td key={uuidv4()}>
                  <div className="select is-small is-link">
                    <select value={rowValues[6]} name={valSet[6]} onChange={(e) => this.handleFieldChange(aIndex, rowIndex, 6, e, [])}>
                      {rowValues[1] ? ["Pick", ...(dropDown.ceps[rowValues[1]] || [])].map((s, i) => <option key={s} value={i === 0 ? "" : s}>{s}</option>) :
                        <option disabled value=""/>}
                    </select>
                  </div>
                  {rowErrors[valSet[6]] ? <p className="text-error">{rowErrors[valSet[6]]}</p> : null}
                </td>}
              {tranAction && tranAction.canEditTran ?
              <td key={uuidv4()}>
                <span className={`has-text-link ${isDisabled ? "isDisabled" : ""}`} style={{cursor: "pointer"}} onClick={!isEditable ? () => this.saveEditAccordionFlag(id, aIndex, rowIndex, row) : () => this.onEdit(aIndex, rowIndex)} >
                  {isEditable ? <i className="fas fa-pencil-alt" /> : <i className="far fa-save"/> }
                </span>{"   "}
                <span className={`has-text-link ${isDisabled ? "isDisabled" : ""}`} style={{cursor: "pointer"}} onClick={() => this.onRemoveRating(aIndex, rowValues[0], id, rowIndex)}>
                  <i className="far fa-trash-alt"/>
                </span>
              </td>: null}
            </tr>
          </tbody>
        )
      }
      }
    })
  }

  handelToggle = () =>{
    this.setState({ modalState: true })
  }

  onModalSave = () => {
    const {email} = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice:true,
      emailParams: {
        url: window.location.pathname.replace('/',''),
        ...email,
      }
    }
    this.setState({
      modalState: false
    },async ()=>{
      await sendEmailAlert(emailPayload)
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  render() {
    const { accordions, modalState, email, accordionData } = this.state
    const {tranAction, participants} = this.props;
    let users = []
    if(email.category === "myfirm"){
      users = participants.filter(user => user.group === "Participants")
    }else if(email.category === "otherfirms"){
      users = participants.filter(user => user.group === "Others")
    }else if(email.category === "all") {
      users = participants
    }
    const isDisabled = (aIndex) => aIndex === "dealAgencyRatings" ? this.state.isSaveRatingDisabled : this.state.isSaveCepRatingDisabled
    const isAddDisabled = (aIndex) => {
      const  isDisabled = accordionData[aIndex].find(acc => acc.dirty)
      return !!isDisabled
    }

    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      accordions && <div>
        {tranAction && tranAction.canEditTran ?
        <button className="button is-link is-small" onClick={this.handelToggle} >Send Email Alert</button> : null }
        <SendEmailModal modalState={modalState} email={email} participants={users} onModalChange={this.onModalChange} onSave={this.onModalSave}/>
        <section className="accordions">
          { accordions.map ( ({accOpen,accIndex,accHeader}) => <article key={accIndex} className={ accOpen ? "accordion is-active" : "accordion"}>
            <div className="accordion-header">
              <p onClick={() => this.toggleAccordion(accIndex) } style={{width:"100%"}}>{accHeader}</p>
              {tranAction && tranAction.canEditTran ?
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link is-small" onClick={() => this.addAccordionRow(accIndex)} disabled={isDisabled(accIndex) || isAddDisabled(accIndex)}>Add</button>
                </div>
                <div className="control">
                  <button className="button is-light is-small" onClick={() => this.resetAccordion(accIndex)} disabled={isDisabled(accIndex)}>Reset</button>
                </div>
              </div> : null }
            </div>
            {accOpen && <div className="accordion-body">
              <div className="accordion-content">

                <div style={{overflowX:"auto"}}>
                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                    {this.renderAccordionHeaders(accIndex)}
                    {this.renderBodyRevised(accIndex)}
                  </table>
                </div>
              </div>
            </div>}
          </article>)}
        </section>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(SeriesDealRatings)

export default connect(mapStateToProps, null)(WrappedComponent)

