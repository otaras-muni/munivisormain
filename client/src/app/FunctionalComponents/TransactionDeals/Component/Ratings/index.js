import React, { Component } from "react"
import cloneDeep from "clone-deep"
import { toast } from "react-toastify"
import swal from "sweetalert"
import { connect } from "react-redux"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import {
  DealsAgencyRatingValidate,
  DealsCepRatingValidate
} from "../Validation/DealsRatingValidate"
import {
  addOrUpdateDealsRating,
  delDealsRating,
  sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import { fetchSupplierContacts } from "../../../../StateManagement/actions/TransactionDistribute"
import Loader from "../../../../GlobalComponents/Loader"
import CONST from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import EditableTable from "../../../../GlobalComponents/EditableTable"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"

class DealRatings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isEditable: {},
      tables: [
        {
          name: "Deal Ratings",
          key: "dealAgencyRatings",
          row: cloneDeep(CONST.DealRating.dealAgencyRatings) || {},
          header: [
            { name: "Series Code<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Rating Agency<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Long Term Rating<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Long Term Outlook<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Short Term Rating<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            {name: "Short Term Outlook<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>"},
            { name: "Action" }
          ],
          list: [],
          tbody: [
            {
              name: "seriesName",
              type: "dropdown",
              displayName: "Series Name",
              labelName: "seriesName",
              dropdownKey: "series"
            },
            {
              name: "ratingAgencyName",
              type: "dropdown",
              dropdownKey: "newRatingAgencyName",
              disabledDropdownKey: "newDisabledRatingAgencyName",
              labelName: "ratingAgencyName",
              displayName: "Rating Agency Name"
            },
            {
              name: "longTermRating",
              type: "dependedSelect",
              childKey: "ratingAgencyName",
              selectionKey: "ratingTerm"
            },
            {
              name: "longTermOutlook",
              type: "dependedSelect",
              childKey: "ratingAgencyName",
              selectionKey: "ratingTerm"
            },
            {
              name: "shortTermOutlook",
              type: "dependedSelect",
              childKey: "ratingAgencyName",
              selectionKey: "ratingTerm"
            },
            {
              name: "shortTermRating",
              type: "dependedSelect",
              childKey: "ratingAgencyName",
              selectionKey: "ratingTerm"
            },
            { name: "action" }
          ],
          handleError: payload => DealsAgencyRatingValidate(payload)
        },
        {
          name: "Deal CEP Ratings",
          key: "dealCepRatings",
          header: [
            { name: "Series Code<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "CEP Provider<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "CEP Type<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Long Term Rating<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Long Term Outlook<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Short Term Rating<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Short Term Outlook<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
            { name: "Action" }
          ],
          row: cloneDeep(CONST.DealRating.dealCepRatings) || {},
          list: [],
          tbody: [
            {
              name: "seriesName",
              type: "dropdown",
              labelName: "seriesName",
              displayName: "Series Name",
              dropdownKey: "series"
            },
            {
              name: "cepName",
              type: "lookup",
              labelName: "cepName",
              displayName: "CEP Name"
            },
            { name: "cepType", type: "select" },
            {
              name: "longTermRating",
              type: "dropdown",
              dropdownKey: "dealRating",
              disabledDropdownKey: "disableDealRating",
              labelName: "longTermRating",
              displayName: "Long Term Rating"
            },
            {
              name: "longTermOutlook",
              type: "dropdown",
              dropdownKey: "dealRating",
              disabledDropdownKey: "disableDealRating",
              labelName: "longTermOutlook",
              displayName: "Long Term Outlook"
            },
            {
              name: "shortTermOutlook",
              type: "dropdown",
              dropdownKey: "dealRating",
              disabledDropdownKey: "disableDealRating",
              labelName: "shortTermOutlook",
              displayName: "Short Term Outlook"
            },
            {
              name: "shortTermRating",
              type: "dropdown",
              dropdownKey: "dealRating",
              disabledDropdownKey: "disableDealRating",
              labelName: "shortTermRating",
              displayName: "Short Term Rating"
            },
            { name: "action" }
          ],
          handleError: payload => DealsCepRatingValidate(payload)
        }
      ],
      dropDown: {
        ratingTerm: {},
        ceps: {},
        ratingAgencyName: [],
        cepName: [],
        cepType: [],
        series: []
      },
      usersList: [],
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  async componentWillMount() {
    const { transaction } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPCEPRATINGS",
      "LKUPCREDITENHANCEMENTTYPE",
      "LKUPRATING",
      "LKUPPARTICIPANTTYPE"
    ])
    const result = picResult[1] || {}
    const ratingTerm = (picResult[2] && picResult[2].LKUPRATING) || {}
    const ceps = (picResult[2] && picResult[2].LKUPCEPRATINGS) || {}
    const ratingTermLevel1 = (result && result.LKUPRATING) || []

    await fetchSupplierContacts(
      transaction.dealIssueTranClientId,
      ["Credit Enhancement Provider", "Credit Enhancer"],
      res => {
        this.setState({
          suppliersList: res.suppliersList
        })
      }
    )
    if (transaction && transaction.dealIssueSeriesDetails) {
      const state = this.setAccordionState(transaction.dealIssueSeriesDetails)
      transaction.dealIssueSeriesDetails.forEach(series => {
        series.id = series._id
        series.name = series.seriesName
      })
      const ratingAgencyName = Object.keys(ratingTerm).map(agency => ({
        id: agency,
        name: agency
      }))
      const newRatingAgencyName = ratingTermLevel1 && ratingTermLevel1.map(alr => ({
        id: alr && alr.label,
        name: alr && alr.label
      }))
      const newDisabledRatingAgencyName = ratingTermLevel1 && ratingTermLevel1.filter(e => !e.included)
        .map(alr => ({
          id: alr && alr.label,
          name: alr && alr.label
        }))
      const cepRatings = ratingTerm && ratingTerm.Moodys.map(agency => ({
        id: agency && agency.label,
        name: agency && agency.label
      }))
      const disabledCepRatings = ratingTerm && ratingTerm.Moodys && ratingTerm.Moodys.filter(e => !e.included)
        .map(agency => ({
          id: agency && agency.label,
          name: agency && agency.label
        }))
      const cepProvider = this.state.suppliersList && this.state.suppliersList.map(agency => ({
        id: agency.id,
        name: agency.name
      }))

      const cepName = Object.keys(ceps).map(cep => ({ id: cep, name: cep }))
      this.setState(prevState => ({
        ...state,
        dropDown: {
          ratingTerm,
          ceps,
          ratingAgencyName,
          newRatingAgencyName,
          newDisabledRatingAgencyName,
          cepName,
          cepType: result.LKUPCREDITENHANCEMENTTYPE,
          series: transaction.dealIssueSeriesDetails,
          dealRating: cepRatings,
          disableDealRating: disabledCepRatings,
          cepProvider
        },
        userName: (this.props.user && this.props.user.userFirstName) || "",
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.dealIssueTranIssueName ||
            transaction.dealIssueTranProjectDescription} - Notification`
        }
      }))
    } else {
      this.setState({
        loading: false
      })
    }
  }

  setAccordionState = dealIssueSeriesDetails => {
    const { tables } = this.state
    let dealAgencyRatings = []
    let dealCepRatings = []
    dealIssueSeriesDetails.forEach(d => {
      dealAgencyRatings = dealAgencyRatings.concat(d.seriesRatings)
      dealCepRatings = dealCepRatings.concat(d.cepRatings)
    })
    tables.forEach(tbl => {
      tbl.list =
        tbl.key === "dealAgencyRatings"
          ? dealAgencyRatings
          : tbl.key === "dealCepRatings"
            ? dealCepRatings
            : []
    })
    return {
      tables,
      loading: false
    }
  };

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    const { tables } = this.state
    let series = ""
    tables.forEach(tbl => {
      if (!series) {
        series = tbl.list.find(series => series._id === removeId)
      }
    })
    delDealsRating(type, this.props.nav2, series.seriesId, removeId, res => {
      if (res && res.status === 200) {
        toast(
          `Removed ${
            type === "dealAgencyRatings" ? "agency rating" : "cep ratings"
          } successfully`,
          { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
        )
        const state = this.setAccordionState(res && res.data)
        this.props.submitAuditLogs(this.props.nav2)
        let list = []
        res &&
          res.data.forEach(d => {
            list =
              type === "dealAgencyRatings"
                ? list.concat(d.seriesRatings)
                : list.concat(d.cepRatings)
          })
        this.setState(
          {
            ...state
          },
          async () => {
            callback({
              status: true,
              list
            })
          }
        )
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  };

  onSave = (type, item, callback) => {
    console.log(type, item)
    const { tables } = this.state
    let list = []
    tables.forEach(tbl => {
      if (tbl.key === type) {
        list = cloneDeep(tbl.list)
      }
    })
    const alreadyExist = list.find(rating => rating._id === item._id)
    if(list.length){
      list = list.filter(d => d._id !== item._id)
    }
    let currItem = []
    if (type === "dealAgencyRatings") {
      currItem = list.filter(
        x =>
          x.seriesId === item.seriesId &&
          x.ratingAgencyName === item.ratingAgencyName
      )
    } else {
      currItem = list.filter(
        x => x.seriesId === item.seriesId && x.cepName === item.cepName
      )
    }

    if (currItem.length) {
      swal("Warning", "This pair of rating already exists.", "warning")
      return callback({
        status: false,
        duplicateData: true
      })
    }

    let query = this.props.nav2
    if(alreadyExist){
      query = `${query}?oldSeriesId=${alreadyExist.seriesId}`
    }

    addOrUpdateDealsRating(type, query, item, res => {
      if (res && res.status === 200) {
        toast(
          `Added ${
            type === "dealAgencyRatings" ? "agency rating" : "cep ratings"
          } successfully`,
          { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
        )
        const state = this.setAccordionState(res && res.data)
        this.props.submitAuditLogs(this.props.nav2)
        let list = []
        res &&
          res.data.forEach(d => {
            list =
              type === "dealAgencyRatings"
                ? list.concat(d.seriesRatings)
                : list.concat(d.cepRatings)
          })
        this.setState(
          {
            ...state,
          },
          async () => {
            callback({
              status: true,
              list
            })
          }
        )
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  };

  filterExistingAgencyAndCep = (key, item) => {
    const { tables, dropDown } = this.state
    let agencyRate = {}
    let cepRate = {}
    tables.forEach(tbl => {
      if (tbl.key === "dealAgencyRatings") {
        const isExistsAgency = tbl.list
          .filter(
            e =>
              (e.seriesId === item.seriesId &&
                e.ratingAgencyName === item.ratingAgencyName) ||
              e.seriesId === item.seriesId
          )
          .map(p => p.ratingAgencyName)
        agencyRate = Object.keys(dropDown.ratingTerm).filter(
          e => isExistsAgency.indexOf(e) === -1
        )
      } else {
        const isExistsCep = tbl.list
          .filter(
            e => e.seriesId === item.seriesId && e.cepName === item.cepName
          )
          .map(p => p.cepName)
        cepRate = Object.keys(dropDown.ceps).filter(
          e => isExistsCep.indexOf(e) === -1
        )
      }
    })
    const ratingAgencyName = Object.keys(agencyRate).map(agency => ({
      id: agency,
      name: agency
    }))
    const cepName = Object.keys(cepRate).map(cep => ({ id: cep, name: cep }))
    return key === "dealAgencyRatings" ? ratingAgencyName : cepName
  };

  onDropDownChange = (inputName, key, select, index, callback) => {
    if (inputName === "seriesName") {
      callback({
        status: true,
        object: {
          seriesId: select.id || "",
          seriesName: select.name || ""
        }
      })
    }
    if (inputName === "ratingAgencyName") {
      callback({
        status: true,
        object: {
          ratingAgencyName: select.id,
          longTermRating: "",
          longTermOutlook: "",
          shortTermOutlook: "",
          shortTermRating: ""
        }
      })
    }
    /* if (inputName === "cepName") {
      callback({
        status: true,
        object: {
          cepName: select.firmName,
          longTermRating: "",
          longTermOutlook: "",
          shortTermOutlook: "",
          shortTermRating: ""
        }
      });
    } */
    if (
      inputName === "longTermRating" ||
      inputName === "longTermOutlook" ||
      inputName === "shortTermOutlook" ||
      inputName === "shortTermRating"
    ) {
      callback({
        status: true,
        object: {
          [inputName]: select.name
        }
      })
    }
  };

  handelToggle = () =>{
    this.setState({ modalState: true })
  }

  onModalSave = () => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    this.setState({
      modalState: false
    },async ()=>{
      await sendEmailAlert(emailPayload)
    })
  };

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  };

  render() {
    const { modalState, email, dropDown, transaction, tables } = this.state
    const { tranAction, participants, onParticipantsRefresh } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        {tranAction.canEditTran ? (
          <button
            className="button is-link is-small"
            onClick={this.handelToggle}
          >
            Send Email Alert
          </button>
        ) : null}
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onModalSave}
        />
        <EditableTable
          {...this.props}
          transaction={transaction}
          tables={tables}
          dropDown={dropDown}
          onSave={this.onSave}
          onRemove={this.onRemove}
          onParentChange={this.onDropDownChange}
          onRowDropDown={this.filterExistingAgencyAndCep}
          tableStyle={{ overflowY: "unset" }}
          canSupervisorEdit={tranAction.canEditTran}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(DealRatings)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
