import React from "react"
import NumberFormat from "react-number-format"
import {
  SelectLabelInput,
  TextLabelInput
} from "../../../../../GlobalComponents/TextViewBox"
import BorrowerLookup from "Global/BorrowerLookup"
import ThirdPartyLookup from "Global/ThirdPartyLookup"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"


const ActivitySummary = ({ transaction = {}, securityType, onChange, onBlur, disabled, borrowerOrObligorName, guarantorName, onTextInputChange, tranTextChange, canEditTran }) => {

  const borrowerName = {
    _id: "",
    firmName: transaction.dealIssueBorrowerName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const guarantor = {
    _id: "",
    firmName: transaction.dealIssueGuarantorName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.dealIssueTransNotes || "--"

  return (
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          {transaction.dealIssueTranIssuerFirmName ? (
            <small style={{fontSize: 16}}>{getTranEntityUrl("Issuer", transaction.dealIssueTranIssuerFirmName, transaction.dealIssueTranIssuerId, "")}</small>
          ) : (
            <small>--</small>
          )}
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Issue name (as in OS)</p>
        </div>
        <div className="column">
          <TextLabelInput title="Issue Name" name="dealIssueTranIssueName" value={tranTextChange.dealIssueTranIssueName} placeholder="Issue Name" onBlur={onBlur}
            onChange={onTextInputChange} disabled={!canEditTran || transaction.dealIssueTranStatus === "Cancelled" || transaction.dealIssueTranStatus === "Closed" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">
            Project Description (internal)
          </p>
        </div>
        <div className="column">
          <TextLabelInput title="Project Name" name="dealIssueTranProjectDescription" value={tranTextChange.dealIssueTranProjectDescription} placeholder="Project Name"
            onBlur={onBlur} onChange={onTextInputChange} disabled={!canEditTran || transaction.dealIssueTranStatus === "Cancelled" || transaction.dealIssueTranStatus === "Closed" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrower or Obligor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput title="Borrower Or Obligor Name" disabled={disabled} list={borrowerOrObligorName || []} name="dealIssueBorrowerName" value={transaction.dealIssueBorrowerName || ""}
                            onChange={onChange} inputStyle={{ width: 300 }} /> */}
          <BorrowerLookup
            entityName={borrowerName}
            onChange={(e) => onChange({ target: { name: "dealIssueBorrowerName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.dealIssueBorrowerName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Guarantor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput title="Guarantor Name" disabled={disabled} list={guarantorName || []} name="dealIssueGuarantorName" value={transaction.dealIssueGuarantorName || ""}
            onChange={onChange} inputStyle={{ width: 300 }} /> */}
          <ThirdPartyLookup
            entityName={guarantor}
            onChange={(e) => onChange({ target: { name: "dealIssueGuarantorName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.dealIssueGuarantorName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule highlights</p>
        </div>
        <div className="column">
          <small>
            <p>---</p>
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Principal Amount / Par Value</p>
        </div>
        <div className="column">
          <small>
            {transaction.dealIssueParAmount ? (
              <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{ background: "transparent", border: "none" }}
                decimalScale={2} prefix="$" disabled value={transaction.dealIssueParAmount} />
            ) : ("---"
            )}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Security</p>
        </div>
        <div className="column">
          <SelectLabelInput title="Security Type" disabled={disabled} list={securityType || []} name="dealIssueSecurityType" value={transaction.dealIssueSecurityType || ""}
            onChange={onChange} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon / Rate Type:</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{"--"} Rate
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small>---</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Participants Firm</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">
            {transaction.dealIssueParticipants ?
              transaction.dealIssueParticipants.map((item, i) => (
                <p key={i}>
                  {item.dealPartContactId ? (
                    <div style={{fontSize: 15}}>{getTranUserUrl(item.dealPartType, `${item.dealPartType} : ${item.dealPartFirmName} (${item.dealPartContactName})`, item.dealPartContactId, "")}</div>
                  ) :
                    <div style={{fontSize: 15}}>{getTranEntityUrl(item.dealPartType, `${item.dealPartType} : ${item.dealPartFirmName}`, item.dealPartFirmId, "")}</div>
                  }
                </p>
              ))
              : null}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notes / Instructions</p>
        </div>
        <div className="column">
          <small>{actNotes || "--"}</small>
        </div>
      </div>
    </div>
  )
}


export default ActivitySummary
