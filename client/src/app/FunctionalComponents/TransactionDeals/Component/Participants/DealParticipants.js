import React from "react"
import swal from "sweetalert"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import { getPicklistByPicklistName, removeWhiteSpaces, generateDistributionPDF } from "GlobalUtils/helpers"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import Accordion from "../../../../GlobalComponents/Accordion"
import Participants from "./components/Participants"
import CONST from "../../../../../globalutilities/consts"
import {
  removeDealsParticipants,
  addOrUpdateDealsParticipants,
  sendEmailAlert,
  postUnderwriterCheckAddToDL
} from "../../../../StateManagement/actions/Transaction"
import {postCheckedAddToDL} from "../../../../StateManagement/actions/Common"
import UnderWriters from "./components/UnderWriters"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import { DealsPartUnderWritersValidate, DealsParticipantsValidate } from "../Validation/DealsParticipantsValidate"
import { fetchSupplierContacts } from "../../../../StateManagement/actions/TransactionDistribute"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import {DropDownInput} from "../../../../GlobalComponents/TextViewBox"

const mergeArray = (currentList, newItems) => {
  const newObj = newItems.find(part => part.isNew)
  if (newObj) {
    currentList.push(newObj)
  }
  return currentList
}

class DealParticipants extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cols: [
        { name: "Add to DL" },
        { name: "Firm Name<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
        { name: "Role in Syndicate<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
        { name: "Liability %" },
        { name: "Management Fee %" },
        { name: "Action" }],
      userName: "",
      dealIssueUnderwriters: [CONST.dealIssueUnderwriters],
      dealIssueParticipants: [CONST.dealIssueParticipants],
      isEditable: {},
      loading: true,
      dropDown: {
        firmName: [],
        syndicates: [],
        dealPartType: [],
        sortBy: ["Date", "Firm Name", "Type"],
        ascDsc: ["Ascending", "Descending"]
      },
      sortBy: "",
      ascDsc: "",
      searchText: "",
      searchList: [],
      underWritingFirms: [],
      filterUser: [],
      allFirms: [],
      participantsType: [],
      userList: [],
      errorMessages: {},
      isSaveUnderWriter: false,
      isSaveParticipants: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
        sendEmailTo: []
      },
      confirmAlert: CONST.confirmAlert,
      jsonSheets: [],
      startXlDownload: false,
      isRefresh: false,
    }
  }

  componentWillMount() {
    const dealId = this.props.nav2
    if (dealId) {
      this.getDealDetails(dealId)
    }
  }

  getDealDetails = (dealId, key) => {
    const { tranAction, transaction } = this.props
    if (transaction && (transaction.dealIssueParticipants || transaction.dealIssueUnderwriters)) {
      fetchSupplierContacts(transaction.dealIssueTranClientId, "Underwriting Services", (firms) => {
        const underWritingFirms = (firms && firms.suppliersList) || []
        if (key) {
          const isDisabled = key === "dealIssueUnderwriters" ? "isSaveUnderWriter" : "isSaveParticipants"
          let newPart = this.state[key].find(part => part.isNew)
          const list = (transaction && transaction[key] && transaction[key].length) ? cloneDeep(transaction[key]) : []
          newPart = newPart ? cloneDeep(newPart) : !list.length ? cloneDeep(CONST[key]) : ""
          if (newPart) {
            list.push(newPart)
          }

          this.setState(prevState => ({
            [key]: list,
            isEditable: {
              ...this.state.isEditable,
              [key]: (transaction[key] && !transaction[key].length) ? 0 : ""
            },
            underWritingFirms,
            [isDisabled]: false,
            email: {
              ...prevState.email,
              subject: `Transaction - ${transaction.dealIssueTranIssueName || transaction.dealIssueTranProjectDescription} - Notification`
            }
          }), () => {
            this.getFirmDetails()
            this.onSortByChange("Date", "Descending")
          })
        } else {
          this.setState(prevState => ({
            underWritingFirms,
            dealIssueUnderwriters: (transaction.dealIssueUnderwriters && transaction.dealIssueUnderwriters.length) ? transaction.dealIssueUnderwriters : tranAction.canEditTran ? [CONST.dealIssueUnderwriters] : [],
            dealIssueParticipants: (transaction.dealIssueParticipants && transaction.dealIssueParticipants.length) ? transaction.dealIssueParticipants : tranAction.canEditTran ? [CONST.dealIssueParticipants] : [],
            isEditable: {
              dealIssueParticipants: (transaction.dealIssueParticipants && !transaction.dealIssueParticipants.length) ? 0 : "",
              dealIssueUnderwriters: (transaction.dealIssueUnderwriters && !transaction.dealIssueUnderwriters.length) ? 0 : "",
            },
            email: {
              ...prevState.email,
              subject: `Transaction - ${transaction.dealIssueTranIssueName || transaction.dealIssueTranProjectDescription} - Notification`
            }
          }), () => {
            this.getFirmDetails()
            this.onSortByChange("Date", "Descending") })
        }
      })
    } else {
      this.setState({ loading: false, })
    }
  }

  async componentDidMount() {
    let result = await getPicklistByPicklistName(["LKUPUNDERWRITER", "LKUPUNDERWRITERROLE" /* , "LKUPPARTICIPANTTYPE" */])
    result = (result.length && result[1]) || {}
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        firmName: result.LKUPUNDERWRITER,
        syndicates: result.LKUPUNDERWRITERROLE,
        // dealPartType: result.LKUPPARTICIPANTTYPE,
      },
      userName: (this.props.user && this.props.user.userFirstName) || "",
    })
  }

  getFirmDetails = () => {
    const { transaction } = this.props
    const query = transaction && transaction.dealIssueTranIssuerId ? `?issuerId=${transaction.dealIssueTranIssuerId}` : ""
    fetchSupplierContacts(transaction.dealIssueTranClientId, `All${query}`, (allFirms) => {
      const userList = allFirms && allFirms.userList ? allFirms.userList.map(e => ({ ...e, name: `${e.userFirstName} ${e.userLastName}`, id: e._id })) : []
      let participantsType = []
      allFirms && allFirms.suppliersList && allFirms.suppliersList.forEach(ent => {
        ent.entityFlags && ent.entityFlags.marketRole && ent.entityFlags.marketRole.forEach(service => {
          if (participantsType.indexOf(service) === -1) participantsType.push(service)
        })
      })
      participantsType = participantsType.map(e => {
        const obj = {}
        obj.name = e
        obj.id = e
        return obj
      })

      participantsType.unshift({
        name: "Issuer",
        id: "Issuer",
        firmId: transaction.dealIssueTranIssuerId,
        firmName: transaction.dealIssueTranIssuerFirmName,
      }, {
        name: "Municipal Advisor",
        id: "Municipal Advisor",
        firmId: transaction.dealIssueTranClientId,
        firmName: transaction.dealIssueTranClientFirmName,
      })

      const partFirms = []
      allFirms && allFirms.suppliersList.forEach(ent => {
        ent.entityFlags && ent.entityFlags.marketRole && ent.entityFlags.marketRole.forEach(service => {
          if (ent._id === transaction.dealIssueTranClientId) {
            partFirms.push({ ...ent, entityFlags: "Municipal Advisor" })
          } if (ent._id === transaction.dealIssueTranIssuerId) {
            partFirms.push({ ...ent, entityFlags: "Issuer" })
          } else {
            partFirms.push({ ...ent, entityFlags: service })
          }
        })
      })
      this.setState({
        loading: false,
        isRefresh: false,
        allFirms: partFirms,
        userList,
        participantsType,
      }, () => this.addUserStatusInParticipants())
    })
  }

  addUserStatusInParticipants = () => {
    const {dealIssueParticipants, userList} = this.state
    if (dealIssueParticipants && userList && dealIssueParticipants.length && userList.length) {
      dealIssueParticipants && dealIssueParticipants.forEach(ent => {
        userList && userList.forEach(item => {
          if (ent.dealPartContactId === item._id) {
            ent.dealPartUserActive = item.userStatus
          }
        })
      })
    }
    this.setState({dealIssueParticipants})
  }

  distributePDFDownload = async () => {
    let { dealIssueParticipants } = this.state
    const { dealIssueUnderwriters } = this.state
    const { logo, transaction, user } = this.props
    const partTypeQueue = ["Issuer", "Bond Counsel", "Municipal Advisor"]

    const data = []
    const participantsData = []

    if(dealIssueParticipants && dealIssueParticipants.length || dealIssueUnderwriters && dealIssueUnderwriters.length){

      dealIssueParticipants.sort((a, b) => {
        const nameA = a && a.dealPartType && a.dealPartType.toLowerCase()
        const nameB = b && b.dealPartType && b.dealPartType.toLowerCase()
        if (nameA < nameB)
          return -1
        if (nameA > nameB)
          return 1
        return 0
      })

      partTypeQueue.forEach(partType => {
        dealIssueParticipants.forEach(d => {
          if(d && d.dealPartType === partType && d.addToDL){
            data.push({
              stack: [
                { text: d && d.dealPartType && d.dealPartType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
                { text: `Name: ${d.dealPartContactName || ""}`, bold: true, fontSize: 10},
                { text: `Address: ${d.dealPartContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
                { text: `Phone: ${d.dealPartContactPhone || ""}`, fontSize: 10},
                { text: `Fax: ${d.dealPartContactFax || ""}`, fontSize: 10},
                { text: `E-Mail: ${d.dealPartContactEmail || ""}`, fontSize: 10}
              ]
            })
          }
        })
      })

      dealIssueParticipants = dealIssueParticipants.filter(d => (d && partTypeQueue.indexOf(d.dealPartType) === -1  && d.addToDL))

      dealIssueUnderwriters.forEach(d => {
        if(d && d.dealPartContactAddToDL){
          data.push({
            stack: [
              { text: `${d && d.roleInSyndicate && d.roleInSyndicate.toUpperCase()}(Underwriter)`, decoration: "underline", bold: true, margin: [ 0, 10, 0, 0 ]},
              { text: `Firm Name: ${d.dealPartFirmName || ""}`, fontSize: 10},
            ]
          })
        }
      })

      dealIssueParticipants.forEach(d => {
        if(d && d.addToDL){
          data.push({
            stack: [
              { text: d && d.dealPartType && d.dealPartType.toUpperCase(), decoration: "underline", bold: true,  margin: [ 0, 10, 0, 0 ]},
              { text: `Name: ${d.dealPartContactName || ""}`, bold: true, fontSize: 10},
              { text: `Address: ${d.dealPartContactAddrLine1 || ""}`, margin: [ 0, 0, 5, 0 ], fontSize: 10},
              { text: `Phone: ${d.dealPartContactPhone || ""}`, fontSize: 10},
              { text: `Fax: ${d.dealPartContactFax || ""}`, fontSize: 10},
              { text: `E-Mail: ${d.dealPartContactEmail || ""}`, fontSize: 10}
            ]
          })
        }
      })
    }

    if(data && data.length){
      const stackData = []
      let duplicateData = []
      let dataIndex = 0
      data.forEach(d => {
        if(duplicateData.length < 2){
          dataIndex = dataIndex + 1
          const index = [dataIndex - 1]
          duplicateData.push(data[index])
          if(duplicateData.length === 1){
            stackData.push(duplicateData)
          }
        }
        if (duplicateData.length === 2){
          duplicateData = []
        }
      })
      if(stackData && stackData.length){
        stackData.forEach(stack => {
          if(stack && stack.length === 1){
            stack.push({stack: []})
          }
          participantsData.push({columns: stack}, {marginTop:10, text: [ "" ]})
        })
      }
      await generateDistributionPDF({
        logo,
        firmName: user && user.firmName && user.firmName.toUpperCase() || "",
        clientName: transaction && transaction.dealIssueTranIssuerFirmName && transaction.dealIssueTranIssuerFirmName.toUpperCase() || "",
        transactionType: "Deals",
        data: participantsData,
        fileName: `${transaction.dealIssueTranIssueName || transaction.dealIssueTranProjectDescription || ""} participants.pdf`
      })
    } else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  pdfDownload = () => {
    const { transaction, logo, user } = this.props
    const position = user && user.settings
    const { dealIssueUnderwriters, dealIssueParticipants } = this.state
    const pdfUnderWriter = []
    const pdfParticipants = []
    dealIssueUnderwriters && dealIssueUnderwriters.forEach((item) => {
      if(item.dealPartContactAddToDL === true){
        pdfUnderWriter.push([
          item.dealPartContactAddToDL ? "Yes" : "No",
          item.dealPartFirmName || "--",
          item.roleInSyndicate || "--",
          item.liabilityPerc || "--",
          item.managementFeePerc || "--"
        ])
      }
    })

    dealIssueParticipants && dealIssueParticipants.forEach((item) => {
      if(item.addToDL === true){
        const address1 = removeWhiteSpaces(item.dealPartContactAddrLine1 || "")
        pdfParticipants.push([
          item.dealPartType || "--",
          item.dealPartFirmName || "--",
          item.dealPartContactName || "--",
          item.dealPartContactPhone || "--",
          item.dealPartContactEmail || "--",
          item.addToDL ? "Yes" : "No",
          address1 || "--"
        ])
      }
    })
    const tableData = []
    if(pdfUnderWriter.length > 0){
      tableData.push({
        titles: ["Deal UnderWriter"],
        description: "",
        headers: ["Add to DL", "Firm name", "Role in Syndicate", "Liability %", "Management Fee %"],
        rows: pdfUnderWriter
      })
    }
    if(pdfParticipants.length > 0){
      tableData.push({
        titles: ["Deal Participants"],
        description: "",
        headers: ["Participant Type", "Firm name", "Contact Name", "Phone", "Email", "Add to DL", "Address"],
        rows: pdfParticipants
      })
    }
    if((pdfParticipants && pdfParticipants.length) || (pdfUnderWriter && pdfUnderWriter.length)){
      pdfTableDownload("", tableData, `${transaction.dealIssueTranIssueName || transaction.dealIssueTranProjectDescription || ""} participants.pdf`, logo, position)
    }else {
      toast("Please select add to DL!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  xlDownload = () => {
    const { transaction } = this.props
    const { dealIssueUnderwriters, dealIssueParticipants } = this.state
    const pdfUnderWriter = []
    const pdfParticipants = []
    dealIssueUnderwriters.map((item) => {
      const data = {
        "Add to DL": item.dealPartContactAddToDL || false,
        "Firm Name": item.dealPartFirmName || "--",
        "Part Type": item.dealPartType || "--",
        "Role in Syndicate": item.roleInSyndicate || "--",
        "Liability": item.liabilityPerc || "--",
        "Management Fee": item.managementFeePerc || "--",
      }
      pdfUnderWriter.push(data)
    })

    dealIssueParticipants.map((item) => {
      const data = {
        "Add to DL": item.addToDL || false,
        "Firm Name": item.dealPartFirmName || "--",
        "Part Type": item.dealPartType || "--",
        "Name": item.dealPartContactName || "--",
        "Email": item.dealPartContactEmail || "--",
        "Phone": item.dealPartContactPhone || "--",
        "Address": item.dealPartContactAddrLine1 || "--"
      }
      pdfParticipants.push(data)
    })
    const jsonSheets = [{
      name: "UnderWriter",
      headers: ["Add to DL", "Firm Name", "Part Type", "Role in Syndicate", "Liability", "Management Fee"],
      data: pdfUnderWriter,
    },
    {
      name: "Participants",
      headers: ["Add to DL", "Firm Name", "Part Type", "Name", "Email", "Phone", "Address"],
      data: pdfParticipants,
    }]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    })
  }

  onReset = (key) => {
    const { isEditable } = this.state
    const list = this.state[key].filter(item => !item.isNew)
    const auditLogs = this.props.auditLogs.filter(x => x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      loading: false,
      [key]: list || [cloneDeep(CONST[type])],
      isEditable: {
        ...isEditable,
        [key]: (list && !list.length) ? 0 : ""
      }
    }, () => {
      // this.getDealDetails(this.props.nav2, key)
    })
  }

  onAdd = (key) => {
    const { userName, isEditable } = this.state
    const insertRow = this.state[key]
    insertRow.unshift(CONST[key])
    this.props.addAuditLog({ userName, log: `${key === "dealIssueUnderwriters" ? "UnderWriting Team" : "Participants"} Add new Item`, date: new Date(), key })

    if (isEditable[key]) { swal("Warning", `First save the current ${key === "dealIssueUnderwriters" ? "UnderWriting Team" : "Participants"}`, "warning"); return }

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        // [key]: insertRow.length ? insertRow.length - 1 : 0,
        [key]: 0,
      }
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({ userName, log: `${change}`, date: new Date(), key: category })
  }

  actionButtons = (key, canEditTran) => {
    const isDisabled = key === "dealIssueUnderwriters" ? this.state.isSaveUnderWriter : this.state.isSaveParticipants
    const list = this.state[key]
    const isAddNew = (list && list.find(item => item.isNew)) || false
    if (!canEditTran) return
    return ( //eslint-disable-line
      <div className="field is-grouped">
        {
          key === "dealIssueParticipants" &&
            <div className="control is-link" onClick={() => this.getFirmDetails(this.setState({isRefresh: true}))}>
              <a title="Title refresh" className={this.state.isRefresh ? "fa fa-refresh fa-spin isDisabled" : "fa fa-refresh"}
                style={{
                  fontSize: 25,
                  padding: "2px 2px 3px 2px"
                }}
              />
            </div>
        }
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={isDisabled || !!isAddNew}>Add</button>
        </div>
        <div className="control">
          <button className="button is-light is-small" onClick={() => this.onReset(key)} disabled={isDisabled}>Reset</button>
        </div>
      </div>
    )
  }

  onEdit = (key, index, participant) => {
    this.props.addAuditLog({ userName: this.state.userName, log: `Deals participants In ${key === "dealIssueUnderwriters" ? "UnderWriting Team" : "Participants"} one row edited`, date: new Date(), key })
    if (this.state.isEditable[key]) { swal("Warning", `First save the current ${key === "dealIssueUnderwriters" ? "UnderWriting Team" : "Participants"}`, "warning"); return }
    let tempNewAddress = ""
    if (key === "dealIssueParticipants" && !participant.dealPartUserAddress && !participant.isNew) {
      tempNewAddress = participant.dealPartContactAddrLine1
    }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index,
      },
      tempNewAddress: tempNewAddress || ""
    })
  }

  onCancel = (key) => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: "",
      },
    })
  }

  onRemoveParticipants = (removeId, type, index, name, tableTitle) => {
    const dealId = this.props.dealId
    const { userName, isEditable, confirmAlert, sortBy, ascDsc } = this.state
    confirmAlert.text = `You want to delete this ${tableTitle}?`
    swal(confirmAlert)
      .then((willDelete) => {
        if (type && dealId && removeId) {
          const changeLog = type === "dealIssueUnderwriters" ? `Removed Underwriting Team ${name}` : `Removed Participant ${name}`
          this.props.addAuditLog({ userName, log: `${changeLog}`, date: new Date(), key: "dealIssueParticipants" })

          if (willDelete) {
            const isDisabled = type === "dealIssueUnderwriters" ? "isSaveUnderWriter" : "isSaveParticipants"
            this.setState({
              [isDisabled]: true
            }, () => {
              removeDealsParticipants(type, dealId, removeId, (res) => {
                if (res && res.status === 200) {
                  toast(`Deal ${type === "dealIssueUnderwriters" ? "Underwriting Team" : "Participants"} has been deleted!`, {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.SUCCESS,
                  })
                  this.props.submitAuditLogs(dealId)
                  let newPart = this.state[type].find(part => part.isNew)
                  const list = (res && res.data && res.data[type] && res.data[type].length) ? cloneDeep(res.data[type]) : []
                  newPart = newPart ? cloneDeep(newPart) : !list.length ? cloneDeep(CONST[type]) : ""
                  if (newPart) {
                    list.push(newPart)
                  }
                  this.setState({
                    [type]: list,
                    isEditable: {
                      ...isEditable,
                      [type]: (res && res.data && res.data[type] && !res.data[type].length) ? 0 : ""
                    },
                    errorMessages: {},
                    [isDisabled]: false
                  }, () => {
                    if(type === "dealIssueParticipants") {
                      this.addUserStatusInParticipants()
                      if (sortBy && ascDsc) {
                        this.onSortByChange(sortBy, ascDsc)
                      }
                    }
                  })
                } else {
                  toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
                  this.setState({
                    [isDisabled]: false
                  })
                }
              })
            })
          }
        } else {
          const { dealIssueUnderwriters, dealIssueParticipants } = this.state
          if (willDelete) {
            if (type === "dealIssueUnderwriters") {
              dealIssueUnderwriters.splice(index, 1)
            }
            if (type === "dealIssueParticipants") {
              dealIssueParticipants.splice(index, 1)
            }

            this.setState({
              dealIssueUnderwriters,
              dealIssueParticipants,
              isEditable: {
                ...isEditable,
                [type]: ""
              },
              errorMessages: {}
            })
          }
        }
      })
  }

  onCheckAddTODL = (e, item, type) => {
    const { dealIssueParticipants, dealIssueUnderwriters } = this.state
    const payload = {
      _id: item._id,
      addToDL: e.target.checked
    }
    if(type === "participant") {
      const partIndex = dealIssueParticipants.findIndex(part => part._id === item._id)
      dealIssueParticipants[partIndex].addToDL = e.target.checked
      this.setState({
        dealIssueParticipants
      }, async () => {
        const res = await postCheckedAddToDL("Deals", this.props.nav2, payload)
        if(res && res.status === 200 && res.data && res.data.updateParticipant) {
          console.log(res.data.updateParticipant)
          dealIssueParticipants[partIndex].addToDL = res.data.updateParticipant && res.data.updateParticipant.addToDL
          this.setState({
            dealIssueParticipants
          })
        }
      })
    }else if(type === "underWriter") {
      const unIndex = dealIssueUnderwriters.findIndex(under => under._id === item._id)
      dealIssueUnderwriters[unIndex].dealPartContactAddToDL = e.target.checked
      this.setState({
        dealIssueUnderwriters
      }, async () => {
        const res = await postUnderwriterCheckAddToDL(item._id, this.props.nav2, payload)
        if(res && res.status === 200 && res.data && res.data.updateUnderwriter) {
          console.log(res.data.updateUnderwriter)
          dealIssueUnderwriters[unIndex].dealPartContactAddToDL = res.data.updateUnderwriter && res.data.updateUnderwriter.dealPartContactAddToDL
          this.setState({
            dealIssueUnderwriters
          })
        }
      })
    }
  }

  onSave = (key, underPart, index) => {
    const { isEditable, email, dealIssueUnderwriters, sortBy, ascDsc } = this.state
    let liabilityTotalPerc = 0
    let managementFeeTotalPerc = 0
    dealIssueUnderwriters.forEach(underWriter => {
      liabilityTotalPerc += (underWriter.liabilityPerc ? parseInt(underWriter.liabilityPerc, 10) : 0)
      managementFeeTotalPerc += (underWriter.managementFeePerc ? parseInt(underWriter.managementFeePerc, 10) : 0)
    })

    if (liabilityTotalPerc > 100 || managementFeeTotalPerc > 100) return

    let isValid = false

    let errors = {}
    if (key === "dealIssueUnderwriters") {
      /* if (underPart && Object.keys(underPart).length) {
        isValid = Object.keys(underPart).some(key => (typeof (underPart[key]) === "string" && !underPart[key]))
      } */
      errors = DealsPartUnderWritersValidate(underPart)
    } else {
      delete underPart.dealPartContactAddToDL
      delete underPart.dealPartUserActive
      errors = DealsParticipantsValidate(underPart)
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        let message = "Required"
        if (err.path[0] === "managementFeePerc" || err.path[0] === "liabilityPerc") {
          message = "Required (must be less than or equal to 100)"
        }
        if (errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    if (isValid) {
      this.setState({
        errorMessages: {
          [key]: "All field is Required."
        }
      })
      return
    }
    if(key === "dealIssueParticipants" && !underPart.dealPartUserAddress){
      delete underPart.dealPartUserAddress
    }
    if(!underPart._id && key === "dealIssueParticipants"){
      underPart.createdDate = new Date()
    }
    const isDisabled = key === "dealIssueUnderwriters" ? "isSaveUnderWriter" : "isSaveParticipants"
    const Toast = key === "dealIssueUnderwriters" ? "Underwriting Team" : "Participants"
    this.setState({
      [isDisabled]: true
    }, () => {
      delete underPart.isNew
      addOrUpdateDealsParticipants(key === "dealIssueUnderwriters" ? "underwriters" : "participants", this.props.nav2, underPart, (res) => {
        if (res && res.status === 200) {
          toast(`Deal ${Toast} has been updated!`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.submitAuditLogs(this.props.nav2)

          let list = (res && res.data && res.data[key] && res.data[key].length) ? cloneDeep(res.data[key]) : []
          list = mergeArray(list, list.length ? cloneDeep(this.state[key]) : [cloneDeep(CONST[key])])

          this.setState({
            [key]: list,
            isEditable: {
              ...isEditable,
              [key]: (res.data && res.data[key] && !res.data[key].length) ? 0 : ""
            },
            errorMessages: {},
            [isDisabled]: false
          }, async () => {
            if(key === "dealIssueParticipants") {
              this.addUserStatusInParticipants()
              if (sortBy && ascDsc) {
                this.onSortByChange(sortBy, ascDsc)
              }
            }
          })
        } else {
          const error = (res && res.response && res.response.data && res.response.data.error) || "Something went wrong!"
          toast(error, { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            [isDisabled]: false
          })
        }
      })
    })
  }

  onModalSave = () => {
    const {  email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email,
      }
    }
    this.setState({
      modalState: false
    },async ()=>{
      await sendEmailAlert(emailPayload)
    })
  }

  handelToggle = () =>{
    this.setState({ modalState: true })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onSortByChange = (value, ascDsc) => {
    const {dealIssueParticipants, searchList, searchText} = this.state
    const sort = searchText ? searchList : dealIssueParticipants
    sort.sort(function(a, b){
      if(value === "Firm Name"){
        const nameA=a.dealPartFirmName.toLowerCase(), nameB=b.dealPartFirmName.toLowerCase()
        if (ascDsc === "Descending") {
          if (nameA > nameB)
            return -1
          if (nameA < nameB)
            return 1
          return 0
        } else {
          if (nameA < nameB)
            return -1
          if (nameA > nameB)
            return 1
          return 0
        }
      } else if(value === "Type"){
        const nameA=a.dealPartType.toLowerCase(), nameB=b.dealPartType.toLowerCase()
        if (ascDsc === "Descending") {
          if (nameA > nameB)
            return -1
          if (nameA < nameB)
            return 1
          return 0
        } else {
          if (nameA < nameB)
            return -1
          if (nameA > nameB)
            return 1
          return 0
        }
      } else if(value === "Date"){
        const dateA=new Date(a.createdDate), dateB=new Date(b.createdDate)
        if(ascDsc === "Descending"){
          return dateB-dateA
        } else {
          return dateA-dateB
        }
      }
    })
    this.setState({
      dealIssueParticipants,
      searchList,
      sortBy: value,
      ascDsc: ascDsc
    })
  }

  onSearchText = (e) => {
    const { dealIssueParticipants } = this.state
    this.setState({
      [e.target.name]: e.target.value
    }, () => {
      if (dealIssueParticipants.length) {
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const { searchText, dealIssueParticipants } = this.state
    if (searchText) {
      const searchList = dealIssueParticipants.filter(obj => ["dealPartType", "dealPartFirmName", "dealPartContactName", "dealPartContactPhone", "dealPartContactEmail", "dealPartContactAddrLine1", "dealPartContactAddrLine2", "dealParticipantState"].some(key => {
        return obj[key] && obj[key].toLowerCase().includes(searchText.toLowerCase())
      }))
      this.setState({
        searchList
      })
    }
  }

  render() {
    const { modalState, email, cols, loading, searchText, searchList, dealIssueUnderwriters, isSaveUnderWriter, isSaveParticipants, underWritingFirms, allFirms, userList, errorMessages, dropDown, participantsType, isEditable, ascDsc, sortBy, tempNewAddress } = this.state
    const dealIssueParticipants = searchText ? searchList : this.state.dealIssueParticipants
    const { tranAction, transaction, participants, onParticipantsRefresh } = this.props
    const columns = tranAction.canEditTran ? cloneDeep(cols) : cloneDeep(cols.filter(col => col.name !== "Action"))

    let liabilityTotalPerc = 0
    let managementFeeTotalPerc = 0
    dealIssueUnderwriters.forEach(underWriter => {
      liabilityTotalPerc += (underWriter.liabilityPerc ? parseInt(underWriter.liabilityPerc, 10) : 0)
      managementFeeTotalPerc += (underWriter.managementFeePerc ? parseInt(underWriter.managementFeePerc, 10) : 0)
    })
    if (tranAction.canEditTran) {
      liabilityTotalPerc > 100 ? columns.splice(3, 1, { name: "<b class='has-text-danger'>Liability %</b>" }) : columns.splice(3, 1, { name: "Liability %" })
      managementFeeTotalPerc > 100 ? columns.splice(4, 1, { name: "<b class='has-text-danger'>Management Fee %</b>" }) : columns.splice(4, 1, { name: "Management Fee %" })
    }

    const isDisabledDownload = (dealIssueUnderwriters && dealIssueUnderwriters.length && dealIssueUnderwriters[0] && dealIssueUnderwriters[0]._id) ||
                             (dealIssueParticipants && dealIssueParticipants.length || dealIssueParticipants[0] && dealIssueParticipants[0]._id)
    if (loading) {
      return <Loader />
    }
    return (
      <div className="participants">
        {tranAction && tranAction.canEditTran ?
          <div className="columns">
            <div className="column">
              <button className="button is-link is-small" onClick={this.handelToggle} >Send Email Alert</button>
            </div>
            <div className="column">
              <div className="field is-grouped">
                <div className={`${isDisabledDownload ? "control" : "control isDisabled"}`} onClick={this.distributePDFDownload}>
                  <span className="has-text-link">
                    <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                  </span>
                </div>
                {/* <div className={`${isDisabledDownload ? "control" : "control isDisabled"}`} onClick={this.pdfDownload}>
                  <span className="has-text-link">
                    <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
                  </span>
                </div> */}
                <div className={`${isDisabledDownload ? "control" : "control isDisabled"}`} onClick={this.xlDownload}>
                  <span className="has-text-link">
                    <i className="far fa-2x fa-file-excel has-text-link" title="Excel Download"/>
                  </span>
                </div>
                <div style={{ display: "none" }}>
                  <ExcelSaverMulti label={`${transaction.dealIssueTranIssueName || transaction.dealIssueTranProjectDescription || ""} participants`} startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag}
                    jsonSheets={this.state.jsonSheets} />
                </div>
              </div>
            </div>
          </div>
          : null}
        <SendEmailModal modalState={modalState} email={email} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onModalChange={this.onModalChange} onSave={this.onModalSave} />
        <Accordion multiple boxHidden
          activeItem={[0, 1]}
          render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(0)} title="Underwriting Team"
                actionButtons={this.actionButtons("dealIssueUnderwriters", tranAction.canEditTran || false)} style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
                  <div className="tbl-scroll">
                    <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                      <TableHeader cols={columns} />
                      <tbody>
                        {Array.isArray(dealIssueUnderwriters) && dealIssueUnderwriters.length ? dealIssueUnderwriters.map((underWriter, i) => {
                          const errors = (errorMessages && errorMessages.dealIssueUnderwriters && errorMessages.dealIssueUnderwriters[i.toString()]) || {}
                          const isExists = dealIssueUnderwriters.map(e => e.dealPartFirmId !== underWriter.dealPartFirmId && e.dealPartFirmId)
                          const firms = underWritingFirms.filter(e => isExists.indexOf(e.id) === -1)

                          return (
                            <UnderWriters
                              key={i}
                              category="dealIssueUnderwriters"
                              index={i}
                              errors={errors}
                              firms={firms}
                              dropDown={dropDown}
                              isEditable={isEditable}
                              underWriter={underWriter}
                              onItemChange={this.onItemChange}
                              onBlur={this.onBlur}
                              onRemove={this.onRemoveParticipants}
                              onEdit={this.onEdit}
                              onSave={this.onSave}
                              onCancel={this.onCancel}
                              tableTitle="Underwriting Team"
                              isSaveUnderWriter={isSaveUnderWriter}
                              canEditTran={tranAction.canEditTran || false}
                              onCheckAddTODL={this.onCheckAddTODL}
                              liabilityTotalPerc={liabilityTotalPerc}
                              managementFeeTotalPerc={managementFeeTotalPerc}/>
                          )
                        }) : null}
                      </tbody>
                    </table>
                  </div>
                }
              </RatingSection>
              <br/>
              <div className="columns">
                <div className="column is-half">
                  <p className="control has-icons-left">
                    <input className="input is-small is-link" type="text" name="searchText" placeholder="search" onChange={this.onSearchText} title="Search"/>
                    <span className="icon is-left has-background-dark">
                      <i className="fas fa-search" />
                    </span>
                  </p>
                </div>
                <div className="column">
                  <DropDownInput data={dropDown.sortBy} placeholder="Sort By" value={sortBy} onChange={(value) => this.onSortByChange(value)}/>
                </div>
                {sortBy &&
                <div className="column">
                  <DropDownInput data={dropDown.ascDsc} value={ascDsc || "Ascending"} onChange={(value) => this.onSortByChange(sortBy, value)}/>
                </div> }
              </div>
              <RatingSection onAccordion={() => onAccordion(1)} title=" Transaction Participants  "
                actionButtons={this.actionButtons("dealIssueParticipants", tranAction.canEditTran || false)} style={{ overflowY: "unset" }}>
                {activeAccordions.includes(1) &&
                  <div>
                    {Array.isArray(dealIssueParticipants) && dealIssueParticipants.length ? dealIssueParticipants.map((participant, i) => {
                      const errors = (errorMessages && errorMessages.dealIssueParticipants && errorMessages.dealIssueParticipants[i.toString()]) || {}
                      const users = userList.filter(e => e.entityId && participant.dealPartFirmId === e.entityId._id)
                      const isExistsUser = (dealIssueParticipants && dealIssueParticipants.filter(part => part.dealPartFirmId === participant.dealPartFirmId && part.dealPartType === participant.dealPartType).map(p => p.dealPartContactId)) || []
                      const filterUser = users.filter(user => isExistsUser.indexOf(user.id) === -1)
                      let partFirms = allFirms.filter(firm => firm.entityFlags === participant.dealPartType)
                      if(participant.dealPartType === "Underwriting Services"){
                        const underWritingFirm = dealIssueUnderwriters.map(f => f.dealPartFirmId)
                        partFirms = partFirms.filter(partF => underWritingFirm.indexOf(partF.id) !== -1)
                      }
                      const user = users.find(e => e.id === participant.dealPartContactId && e.userEmails)
                      const userEmails = (user && user.userEmails.map(u => u.emailId)) || []
                      const userPhones = (user && user.userPhone.map(u => u.phoneNumber)) || []
                      const userAddresses = user ? user.userAddresses.map(u => ({
                        ...u,
                        id: u._id,
                        name: u.addressName,
                      })) : []
                      const newAddress = {id: "", name: "Enter new Address"}
                      userAddresses && userAddresses.unshift(newAddress)

                      return (
                        <div key={i.toString()}>
                          <Participants
                            key={i.toString()}
                            category="dealIssueParticipants"
                            index={i}
                            firms={_.orderBy(partFirms, ["name"])}
                            userPhones={userPhones}
                            userAddresses={userAddresses}
                            userEmails={userEmails}
                            tableTitle="Deal Participants"
                            partType={_.orderBy(participantsType, ["name"])}
                            errors={errors}
                            userList={filterUser}
                            dropDown={dropDown}
                            isEditable={isEditable}
                            participant={participant}
                            onItemChange={this.onItemChange}
                            onBlur={this.onBlur}
                            onRemove={this.onRemoveParticipants}
                            onEdit={this.onEdit}
                            onSave={this.onSave}
                            onCancel={this.onCancel}
                            isSaveParticipants={isSaveParticipants}
                            canEditTran={tranAction.canEditTran || false}
                            onParticipantsRefresh={this.getFirmDetails}
                            onCheckAddTODL={this.onCheckAddTODL}
                            searchText={searchText}
                            tempNewAddress={tempNewAddress}
                          />
                          {i !== (dealIssueParticipants.length - 1) ? <hr /> : null}
                        </div>
                      )
                    }) : null}
                  </div>
                }
              </RatingSection>
            </div>
          } />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  logo: (state.logo && state.logo.firmLogo) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(DealParticipants)
export default connect(mapStateToProps, null)(WrappedComponent)

