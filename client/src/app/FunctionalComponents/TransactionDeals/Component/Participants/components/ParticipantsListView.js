import React from "react"
import TableHeader from "../../../../../GlobalComponents/TableHeader"

const ParticipantsListView = ({cols, participantsList=[], underwritersList=[], onRemoveParticipants, onEditParticipants}) => (
  <div>
    <div className="box" style={{overflowX: "auto"}}>
      <p className="title innerPgTitle">Underwriting Team</p>
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <TableHeader cols={cols[1]}/>
        <tbody>
          { underwritersList.map((underwriter, i) => (
            <tr key={i}>
              <td>
                <small>{underwriter.dealPartContactAddToDL ? "Yes" : "No"}</small>
              </td>
              <td>
                <small>{underwriter.dealPartFirmName}</small>
              </td>
              <td>
                <small>{underwriter.roleInSyndicate}</small>
              </td>
              <td>
                <small>{underwriter.liabilityPerc}%</small>
              </td>
              <td>
                <small>{underwriter.managementFeePerc}%</small>
              </td>
              <td>
                <div className="field is-grouped">
                  <div className="control">
                    <a href="">
                      <span className="has-text-link">
                        <i className="fas fa-pencil-alt"/>
                      </span>
                    </a>
                  </div>
                  <div className="control">
                    <a onClick={() => onRemoveParticipants(underwriter._id, "underwriters")}>
                      <span className="has-text-link">
                        <i className="far fa-trash-alt"/>
                      </span>
                    </a>
                  </div>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    <div className="box" style={{overflowX: "auto"}}>
      <p className="title innerPgTitle">Deal Participants</p>
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <TableHeader cols={cols[2]}/>
        <tbody>
          {
            participantsList.map((participant, i) => (
              <tr key={i}>
                <td>
                  <small>{participant.dealPartContactAddToDL ? "Yes" : "No"}</small>
                </td>
                <td>
                  <small>{participant.dealPartType}</small>
                </td>
                <td>
                  <small>{participant.dealPartFirmName}</small>
                </td>
                <td>
                  <small>{participant.dealPartContactName}</small>
                </td>
                <td>
                  <small>{participant.dealPartContactTitle || "---"}</small>
                </td>
                <td>
                  <a href="http://maps.google.com/?q=1200 Pennsylvania Ave SE, Washington, District of Columbia, 20003">
                    <small>{participant.dealPartContactAddrLine1} {participant.dealPartContactAddrLine2}</small>
                  </a>
                </td>
                <td>

                  <a href={`tel:${participant.dealPartContactPhone}`}>
                    <small>{participant.dealPartContactPhone}</small>
                  </a>
                </td>
                <td>
                  <a href="mailto: adi@otaras.com">
                    <small>{participant.dealPartContactEmail}</small>
                  </a>
                </td>
                <td>
                  <div className="field is-grouped">
                    <div className="control">
                      <a>
                        <span className="has-text-link">
                          <i className="fas fa-pencil-alt"/>
                        </span>
                      </a>
                    </div>
                    <div className="control">
                      <a onClick={() => onRemoveParticipants(participant._id, "participants")}>
                        <span className="has-text-link">
                          <i className="far fa-trash-alt"/>
                        </span>
                      </a>
                    </div>
                  </div>
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  </div>
)

export default ParticipantsListView
