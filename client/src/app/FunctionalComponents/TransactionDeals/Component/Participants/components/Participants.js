import React from "react"
import NumberFormat from "react-number-format"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"
import { SelectLabelInput, TextLabelInput } from "../../../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../../../GlobalComponents/DropDownListClear"
import ParticipantsSearchAddress from "../../../../../GlobalComponents/ParticipantsSearchAddress"


const Participants = ({ participant, index, partType, firms, userAddresses, userPhones, userEmails, userList, isEditable, onItemChange, onSave, isSaveParticipants,
  onCancel, onEdit, onRemove, errors = {}, onBlur, category, canEditTran, tableTitle, onParticipantsRefresh, onCheckAddTODL, searchText, tempNewAddress }) => {

  const onChange = (event, name) => {
    if (name && name === "dealPartType") {
      return onItemChange({
        ...participant,
        [name]: event.id,
        dealPartFirmId: event.id === "Municipal Advisor" || event.id === "Issuer" ? event.firmId : "",
        dealPartFirmName: event.id === "Municipal Advisor" || event.id === "Issuer" ? event.firmName : "",
        dealPartContactId: "",
        dealPartContactName: "",
        dealPartContactEmail: "",
        dealPartContactPhone: "",
        dealPartUserAddress: "",
        dealPartContactAddrLine1: "",
        dealPartContactAddrLine2: "",
        dealParticipantState: "",
        dealPartUserActive: ""
      }, category, index)
    }
    onItemChange({
      ...participant,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    }, category, index)
  }

  const onPartChange = (firm) => {
    onItemChange({
      ...participant,
      dealPartFirmId: firm.id,
      dealPartFirmName: firm.name,
      dealPartContactId: "",
      dealPartContactName: "",
      dealPartContactEmail: "",
      dealPartContactPhone: "",
      dealPartUserAddress: "",
      dealPartContactAddrLine1: "",
      dealPartContactAddrLine2: "",
      dealParticipantState: "",
      dealPartUserActive: ""
    }, category, index)
  }

  const onUserChange = (user) => {
    let email = ""
    let phone = ""
    let fax = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    if (user && Array.isArray(user.userFax) && user.userFax.length) {
      fax = user.userFax.find(e => e.faxPrimary)
      fax = fax ? fax.faxNumber : user.userFax[0].faxNumber
    }
    onItemChange({
      ...participant,
      dealPartContactId: Object.keys(user).length === 0 ? "" : user.id,
      dealPartContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
      dealPartContactEmail: email,
      dealPartContactPhone: phone,
      dealPartContactFax: fax,
      dealPartUserAddress: "",
      dealPartContactAddrLine1: "",
      dealPartContactAddrLine2: "",
      dealParticipantState: "",
      dealPartUserActive: user && user.userStatus === "inactive" ? "inactive" : "active"
    }, category, index)
  }

  const onAddressChange = (address) => {
    onItemChange({
      ...participant,
      dealPartUserAddress: address.id,
      dealPartContactAddrLine1: (!participant.isNew && address.name === "Enter new Address") ? tempNewAddress :
        address.addressLine1 ? `${address.addressLine1}, ${address.city ? `${address.city},` : ""} ${address.state ? `${address.state},` : ""} ${address.zipCode ? `${address.zipCode.zip1},` : ""} ${address.country}` : "",
      dealPartContactAddrLine2: address.addressLine2 || "",
      dealParticipantState: address.state || "",
      dealPartAddressGoogle: address.addressLine1 ? `${address.addressLine1 || ""} ${address.city || ""} ${address.state || ""} ${address.zipCode ? `${address.zipCode.zip1}` : ""}` : ""
    }, category, index)
  }

  const getAddressDetails = (address) => {
    ((address && Object.keys(address).length === 0) || typeof address === "undefined") ?
      onItemChange({
        ...participant,
        dealPartContactAddrLine1: "",
        dealPartContactAddrLine2: "",
        dealParticipantState: "",
        dealPartAddressGoogle: ""
      }, category, index) :
      onItemChange({
        ...participant,
        dealPartContactAddrLine1: `${address.formatted_address}` || "",
        dealPartContactAddrLine2: address.addressLine2 || "",
        dealParticipantState: address.state || "",
        dealPartAddressGoogle: address.addressLine1 ? `${address.addressLine1 || ""} ${address.city || ""} ${address.state || ""} ${address.zipcode || ""}` : ""
      }, category, index)
  }

  const onBlurInput = (event, name, title) => {
    if (event && event.target && event.target.title && event.target.value) {
      onBlur(category, `${event.target.title || name || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : name ? underWriter.dealPartFirmName : event.target.value || "empty"}`)
    }
    if (event && event.name && title) {
      onBlur(category, `user ${title} change to ${event.name}`)
    }
  }

  const highlightSearch = (string, searchQuery) => {
    const reg = new RegExp(
      searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
      "i"
    )
    return string.replace(reg, str => `<mark>${str}</mark>`)
  }

  const userAddress = participant.dealPartUserAddress ? userAddresses.find(addr => addr.id === participant.dealPartUserAddress) : ""

  isEditable = canEditTran ? (isEditable[category] === index) : ""

  const ListItem = ({ item }) => (
    <span style = {{color: item.userStatus === "inactive" ? "red" : ""}}>
      <strong>{item.name}</strong>
    </span>
  );

  return (
    <div>
      <div className="columns">
        <div className="column is-11">
          <div className="columns">
            <div className="column is-2">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Participant Type<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
                  {isEditable ?
                    // <DropdownList filter data={partType} value={participant.dealPartType} textField="name" valueField="id" onChange={(e) => onChange(e, "dealPartType")} onBlur={(e) => onBlurInput(e, "dealPartType", "Participant type")} disabled={!isEditable} />

                    <DropDownListClear
                      filter
                      data={partType}
                      value={participant.dealPartType}
                      textField="name"
                      valueField="id"
                      onChange={(e) => onChange(e, "dealPartType")}
                      onBlur={(e) => onBlurInput(e, "dealPartType", "Participant type")}
                      disabled={!isEditable}
                      isHideButton={participant.dealPartType && isEditable}
                      onClear={() => {onChange({}, "dealPartType")}}
                    />
                    : <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.dealPartType || "-",searchText)}}/>
                  }
                  {errors.dealPartType && <p className="text-error">{errors.dealPartType}</p>}
                </div>
              </div>
            </div>
            {/* <SelectLabelInput label="Participant Type" error= {errors.dealPartType || ""} name="dealPartType" value={participant.dealPartType} list={dropDown.dealPartType} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable}/> */}
            <div className="column">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Firm Name<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
                  {isEditable ?
                    // <DropdownList filter data={firms} value={participant.dealPartFirmName} textField="name" valueField="id" onChange={onPartChange} onBlur={(e) => onBlurInput(e, "dealPartFirmName", "Participant firm name")} disabled={!isEditable} />

                    <DropDownListClear
                      filter
                      data={firms}
                      value={participant.dealPartFirmName}
                      textField="name"
                      valueField="id"
                      onChange={onPartChange}
                      onBlur={(e) => onBlurInput(e, "dealPartFirmName", "Participant firm name")}
                      disabled={!isEditable}
                      isHideButton={participant.dealPartFirmName && isEditable}
                      onClear={() => {onPartChange({})}}
                    />
                    : <div>{getTranEntityUrl(participant.dealPartType, participant.dealPartFirmName, participant.dealPartFirmId, searchText)}</div>
                  }
                  {errors.dealPartFirmName && <p className="text-error">{errors.dealPartFirmName}</p>}
                  {participant.dealPartType && !participant.dealPartFirmName && isEditable ?
                    <div>
                      {/* <a className="has-text-link" style={{ fontSize: 12, padding: "0 0 8px 11px", marginTop: -10 }} target="_blank" href="/addnew-client" >Add New Client?</a> */}
                      <a className="has-text-link" style={{ fontSize: 12, padding: "0 0 8px 11px", marginTop: -10 }} target="_blank" href="/addnew-thirdparty" >Add New Third Party?</a>
                      <i className="fa fa-refresh" style={{ fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px" }} onClick={onParticipantsRefresh} />
                    </div> : null}
                </div>
              </div>
            </div>
            <div className="column">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Contact Name<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
                  {isEditable ?
                    // <DropdownList filter data={userList} value={participant.dealPartContactName} textField="name" valueField="id" onChange={onUserChange} onBlur={(e) => onBlurInput(e, "dealPartContactName", "Participant contact name")} disabled={!isEditable} />

                    <DropDownListClear
                      filter
                      data={userList}
                      value={participant.dealPartContactName}
                      textField="name"
                      valueField="id"
                      itemComponent={ListItem}
                      onChange={onUserChange}
                      onBlur={(e) => onBlurInput(e, "dealPartContactName", "Participant contact name")}
                      disabled={!isEditable}
                      isHideButton={participant.dealPartContactName && isEditable}
                      onClear={() => {onUserChange({})}}
                    />
                    : <div>{getTranUserUrl(participant.dealPartType, participant.dealPartContactName, participant.dealPartContactId, searchText, participant.dealPartUserActive)}</div>
                  }
                  {errors.dealPartContactName && <p className="text-error">{errors.dealPartContactName}</p>}
                </div>
                {participant.dealPartFirmName && !participant.dealPartContactName && isEditable ?
                  <div>
                    <a className="has-text-link" style={{ fontSize: 12, padding: "0 0 8px 11px", marginTop: -10 }} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                    <i className="fa fa-refresh" style={{ fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px" }} onClick={onParticipantsRefresh} />
                  </div> : null}
              </div>
            </div>
            <div className="column">
              <div className="is-small">
                <p className="multiExpLbl">Phone</p>
                <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  style={{width: 300, background: "transparent", border: "none"}}
                  // mask="_"
                  className="input is-small is-link"
                  name="dealPartContactPhone"
                  value={participant.dealPartContactPhone || ""}
                  disabled
                />
              </div>
            </div>
            {/* <SelectLabelInput label="Phone" error={errors.dealPartContactPhone && "Required/Valid"} list={userPhones || []} value={participant.dealPartContactPhone}
              name="dealPartContactPhone" onChange={onChange} onBlur={onBlurInput} disabled inputStyle={{ maxWidth: "100%", width: "100%" }} /> */}
            <SelectLabelInput label="Email" error={errors.dealPartContactEmail && "Required/Valid"} list={userEmails || []} value={participant.dealPartContactEmail}
              name="dealPartContactEmail" onChange={onChange} onBlur={onBlurInput} disabled inputStyle={{ maxWidth: "100%", width: "100%" }} />
          </div>
        </div>
        <div className="column">
          <div className="field is-grouped">
            <div className="control">
              <p className="multiExpLbl">Add to DL?</p>
              <label className="checkbox">
                <input type="checkbox" title="Participant Add To DL" name="addToDL" checked={participant.addToDL || false} disabled={!participant._id || !canEditTran} onChange={() => {}} onChange={() => { }} onClick={(e) => onCheckAddTODL(e, participant, "participant")} />
              </label>
            </div>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column is-11">
          <div className="columns">
            <div className="column is-2">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Addresses</p>
                  {isEditable ?
                    // <DropdownList filter data={userAddresses} value={userAddress ? userAddress.name : ""} textField="name" valueField="id" onChange={onAddressChange} disabled={!isEditable} />

                    <DropDownListClear
                      filter
                      data={userAddresses}
                      value={userAddress ? userAddress.name : ""}
                      textField="name"
                      valueField="id"
                      defaultValue="Enter new Address"
                      disabled={!isEditable}
                      isHideButton={userAddress.name && isEditable}
                      onChange={onAddressChange}
                      onClear={() => {onAddressChange({})}}
                    />
                    : <small>{userAddress ? (userAddress && userAddress.name) :
                      (participant && participant.dealPartContactAddrLine1 ? "New Address" : "")}</small>
                  }
                  {errors.dealPartUserAddress && <small className="text-error">{errors.dealPartUserAddress}</small>}
                </div>
              </div>
            </div>

            <div className="column is-5">
              {
                isEditable ?
                  <div className="control">
                    <div className="is-small">
                      <ParticipantsSearchAddress
                        idx={0}
                        getAddressDetails={getAddressDetails}
                        value={`${participant.dealPartContactAddrLine1 || ""}`}
                        isDisabled={!!participant.dealPartUserAddress}
                      />
                    </div>
                  </div> :
                  <div className="control">
                    <p className="multiExpLbl">Address</p>
                    <a href={`https://maps.google.com/?q=${participant.dealPartAddressGoogle ? participant.dealPartAddressGoogle : participant.dealPartContactAddrLine1}`} target="_blank">
                      <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.dealPartContactAddrLine1 ? `${participant.dealPartContactAddrLine1 || ""}` : "",searchText)}}/>
                      {/* <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.dealPartContactAddrLine2 ? `${participant.dealPartContactAddrLine2}${participant.dealParticipantState ? "," : ""}` : "",searchText)}}/>
                      <small dangerouslySetInnerHTML={{__html: highlightSearch((participant.dealParticipantState || ""),searchText)}}/> */}
                    </a>
                  </div>
              }
            </div>

            <div className="column is-5">
              <div className="control">
                <p className="multiExpLbl">Fax</p>
                <NumberFormat
                  title="Fax"
                  format="+1 (###) ###-####"
                  style={{width: 300, background: "transparent", border: "none"}}
                  // mask="_"
                  className="input is-small is-link"
                  name="dealPartContactPhone"
                  value={participant.dealPartContactFax || ""}
                  disabled
                />
              </div>
            </div>

            {/* {isEditable ?
              <TextLabelInput label="Address Line 1" error={errors.dealPartContactAddrLine1 || ""} name="dealPartContactAddrLine1" value={participant.dealPartContactAddrLine1} placeholder="Address Line 1" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable} />
              : null
            }
            {isEditable ?
              <TextLabelInput label="Address Line 2" error={errors.dealPartContactAddrLine2 || ""} name="dealPartContactAddrLine2" value={participant.dealPartContactAddrLine2} placeholder="Address Line 2" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable} />
              : null
            }
            {isEditable ?
              <TextLabelInput label="State" error={errors.dealParticipantState || ""} name="dealParticipantState" value={participant.dealParticipantState} placeholder="State" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable} />
              : null
            } */}
            {/* {!isEditable ?
              <div className="column">
                <p className="multiExpLbl">Address</p>
                <a href={`https://maps.google.com/?q=${participant.dealPartContactAddrLine1} ${participant.dealPartContactAddrLine2 || ""} ${participant.dealParticipantState || ""}`} target="_blank">
                  <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.dealPartContactAddrLine1 ? `${participant.dealPartContactAddrLine1}${(participant.dealPartContactAddrLine2 || participant.dealParticipantState) ? "," : ""}` : "",searchText)}}/>
                  <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.dealPartContactAddrLine2 ? `${participant.dealPartContactAddrLine2}${participant.dealParticipantState ? "," : ""}` : "",searchText)}}/>
                  <small dangerouslySetInnerHTML={{__html: highlightSearch((participant.dealParticipantState || ""),searchText)}}/>
                </a>
              </div> : null
            } */}
            {/* <div className="column" />
            {!isEditable ? <div className="column" /> : null}
            {!isEditable ? <div className="column" /> : null} */}
          </div>
        </div>
        {canEditTran ?
          <div className="column" style={{ marginTop: 18 }}>
            <div className="field is-grouped">
              <div className="control">
                <button onClick={isEditable ? () => onSave(category, participant, index) : () => onEdit(category, index, participant)} className={`${isSaveParticipants ? "borderless isDisabled" : "borderless"}`}>
                  <span className="has-text-link">
                    <i className={isEditable ? "far fa-save" : "fas fa-pencil-alt"} title={isEditable ? "Save" : "Edit"}/>
                  </span>
                </button>
              </div>
              <div className="control">
                <button onClick={isEditable ? () => onCancel(category) : () => onRemove(participant._id, category, index, participant.dealPartContactName, tableTitle)} className={`${isSaveParticipants ? "borderless isDisabled" : "borderless"}`}>
                  <span className="has-text-link">
                    <i className={isEditable ? "fa fa-times" : "far fa-trash-alt" } title={isEditable ? "Cancel" : "Delete"}/>
                  </span>
                </button>
              </div>
            </div>
            {participant.isNew && !isEditable ? <small className="text-error">New(Not Save)</small> : null}
          </div> : null}
      </div>
    </div>
  )
}


export default Participants
