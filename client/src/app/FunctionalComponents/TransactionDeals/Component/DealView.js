import React from "react"
import { connect } from "react-redux"
import { NavLink } from "react-router-dom"
import DealDetails from "./Details/DealDetails"
import DealParticipants from "./Participants/DealParticipants"
import Documents from "./Documents"
import {
  makeEligibleTabView
} from "GlobalUtils/helpers"
import Loader from "../../../GlobalComponents/Loader"
import {
  fetchDealsTransaction,
  fetchParticipantsAndOtherUsers
} from "../../../StateManagement/actions/Transaction"
import Audit from "../../../GlobalComponents/Audit"
import DealRatings from "./Ratings"
import Pricing from "./Pricing"
import Summary from "./Summary"
import CheckNTrack from "./CheckAndTrack/index"
import CONST, { activeStyle } from "../../../../globalutilities/consts"

const TABS = [
  { path: "summary", label: "Summary" },
  { path: "details", label: "Details" },
  { path: "participants", label: "Participants" },
  { path: "pricing", label: "Pricing" },
  { path: "ratings", label: "Rating" },
  { path: "documents", label: "Documents" },
  { path: "check-track", label: "Check-n-Track" },
  { path: "audit-trail", label: "Activity Log" }
]

class DealView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs: [],
      isSummary: false,
      loading: true,
      transaction: {},
      tranAction: {},
      checkLists: [
        {
          id: "CL1",
          type: "template1",
          data: [
            {
              title: "Catetgory 1",
              headers: ["List Item"],
              items: [
                { label: "A", values: [] },
                { label: "B", values: [] },
                { label: "C", values: [] },
                { label: "D", values: [] },
                { label: "E", values: [], assignedTo: "1" }
              ]
            }
          ]
        },
        {
          id: "CL2",
          type: "template3",
          data: [
            {
              title: "Category 2",
              headers: [
                "List Item",
                "Col Header (e.g. $/1000)",
                "Col Header (e.g. Amount)"
              ],
              items: [
                { label: "AA", values: [0, 0] },
                { label: "BB", values: [0, 0] },
                { label: "CC", values: [0, 0] },
                { label: "DD", values: [0, 0] }
              ]
            },
            {
              title: "Category 3",
              headers: [
                "List Item",
                "Col Header (e.g. $/1000)",
                "Col Header (e.g. Amount)"
              ],
              items: [
                { label: "AAAa", values: ["0.1", "2"] },
                { label: "BBB", values: ["0.1", "1"] },
                { label: "CCC", values: [0, "2"] },
                { label: "DDD", values: [0, "1"] },
                { label: "EEE", values: [0, "2"] }
              ]
            }
          ]
        }
      ]
    }
  }

  async componentWillMount() {
    const { user, nav, nav2, nav1, nav3, loginEntity } = this.props
    const transId = nav2
    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])
    if (transId) {
      fetchDealsTransaction(nav3, transId, async transaction => {
        if (transaction && transaction.dealIssueTranClientId) {
          const isEligible = await makeEligibleTabView(
            user,
            transId,
            TABS,
            allEligibleNav,
            transaction.dealIssueTranIssuerId,
            transaction.dealIssueTranClientId,
          )

          // if(transaction.dealIssueParticipants && loginEntity && loginEntity.relationshipToTenant !== "Self"){
          //   transaction.dealIssueParticipants = transaction.dealIssueParticipants.filter(part => part.dealPartFirmId === loginEntity.entityId)
          // }

          isEligible.tranAction.canEditTran = CONST.oppDecline.indexOf(transaction.dealIssueTranStatus) !== -1 ? false : isEligible.tranAction.canEditTran

          isEligible.tranAction.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.dealIssueTranStatus) === -1 && isEligible.tranAction.canEditDocument

          if (isEligible && isEligible.tranAction && (!isEligible.tranAction.canEditTran && !isEligible.tranAction.canViewTran)) {
            this.props.history.push("/dashboard")
          } else if (isEligible.tranAction.view.indexOf(nav3) === -1) {
            const view = isEligible.tranAction.view[0] || ""
            this.props.history.push(`/${nav1}/${nav2}/${view}`)
          } else {
            const participants = await fetchParticipantsAndOtherUsers(nav2)
            this.setState({
              transaction,
              loading: false,
              tranAction: isEligible.tranAction,
              tabs: isEligible.tabs,
              participants
            })
          }
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  onStatusChange = status => {
    const { tranAction } = this.state
    tranAction.canEditTran =
      CONST.oppDecline.indexOf(status) !== -1 ? false : tranAction.canEditTran
    this.setState({
      tranAction
    })
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderTabs = (tabs, dealId, option) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/deals/${dealId}/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  /* renderTabs = (tabs, dealId, option) => tabs.map(t =>
    <li key={t.path} className={option === t.path ? "is-active" : "inactive-tab"}>
      <Link to={`/deals/${dealId}/${t.path}`}>{t.label}</Link>
    </li>
  ) */

  renderViewSelection = (dealId, option) => (
    <div className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, dealId, option)}</ul>
    </div>
  )

  renderSelectedView = (dealId, option) => {
    const { tranAction, transaction, participants } = this.state
    switch (option) {
    case "details":
      return (
        <DealDetails
          {...this.props}
          participants={participants}
          tranAction={tranAction}
          dealId={dealId}
          transaction={transaction}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "participants":
      return (
        <DealParticipants
          {...this.props}
          participants={participants}
          dealId={dealId}
          transaction={transaction}
          tranAction={tranAction}
          {...this.props}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
          participants={participants}
          transaction={transaction}
          tranAction={tranAction}
        />
      )
    case "check-track":
      return (
        <CheckNTrack
          {...this.props}
          participants={participants}
          transaction={transaction}
          tranAction={tranAction}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "audit-trail":
      return (
        <Audit
          {...this.props}
          participants={participants}
          transaction={transaction}
          tranAction={tranAction}
        />
      )
    case "ratings":
      return (
        <DealRatings
          {...this.props}
          participants={participants}
          transaction={transaction}
          tranAction={tranAction}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "pricing":
      return (
        <Pricing
          {...this.props}
          participants={participants}
          transaction={transaction}
          tranAction={tranAction}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "summary":
      return (
        <Summary
          {...this.props}
          participants={participants}
          tranAction={tranAction}
          transaction={transaction}
          onParticipantsRefresh={this.onParticipantsRefresh}
          onStatusChange={this.onStatusChange}
        />
      )
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { option } = this.props
    const { transaction } = this.state
    const tranId = (transaction && transaction._id) || ""
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(tranId, option)}
            </div>
          </div>
        </div>
        {/* <section className="hero is-small is-link">
          <div className="hero-body">
            <div className="columns is-vcentered">
              <div className="column">
                <div className="subtitle">
                  <nav className="breadcrumb has-arrow-separator is-small is-right" aria-label="breadcrumbs">
                    <ul>
                      <li>
                        Deals
                      </li>
                      <li className="is-active">
                        {option}
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>

          <div className="hero-foot">
            <div className="container">
              {this.renderViewSelection(tranId, option)}
            </div>
          </div>
        </section> */}
        <section id="main">{this.renderSelectedView(tranId, option)}</section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(
  mapStateToProps,
  null
)(DealView)
