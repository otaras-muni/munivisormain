import React, { Component } from "react"

// Have a Header Fields with Abbreviation - Done
// Multiple Accordions ability to open and close - Done
// Ability to add fields and update the errors
// Save the Information on the statte Object - Done
// Audit Trail of this information

const defaultData = [{
  header:"Accordion1",
  items:[
    "Item 1.1",
    "Item 1.2",
    "Item 1.3"
  ]
},
{
  header:"Accordion2",
  items:[
    "Item 2.1",
    "Item 2.2",
    "Item 2.3",
    "Item 2.4",
    "Item 2.5"
  ]
}
]

const fieldDetails = [{
  accordion:"First Accordion",
  fields:[{
    colName:"Required ?",
    abbr:"Is this required",
    itemplaceholder:false,
    fieldName:"f1",
    initialValue:"",
    colParams:{type:"input",placeholder:"something"}
  },
  {
    colName:"Rating Provider",
    abbr:"Is this required",
    itemplaceholder:true,
    fieldName:"f2",
    initialValue:"",
    colParams:{type:"select",placeholder:"something",name:"fieldName",values:["S","M","F"], defaultSelected:"None", multi:false}
  }]
},{
  accordion:"Second Accordion",
  fields:[{
    colName:"Required ?",
    abbr:"Is this required",
    itemplaceholder:true,
    fieldName:"f3",
    initialValue:"",
    colParams:{type:"input",placeholder:"something"}
  },
  {
    colName:"Rating Yennappa Samachara",
    abbr:"Is this required",
    itemplaceholder:false,
    fieldName:"f4",
    initialValue:"this is initial",
    colParams:{type:"sinitialValueelect",placeholder:"something",values:["S","M","F"], defaultSelected:"None", multi:false}
  }]
}]

class DealRatings extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions:[],accordionData:[] }
    this.toggleAccordion = this.toggleAccordion.bind(this)
    this.renderAccordionHeaders = this.renderAccordionHeaders.bind(this)
    this.renderAccordionBody = this.renderAccordionBody.bind(this)
    this.renderAccordionRow = this.renderAccordionRow.bind(this)
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.addAccordionrow = this.addAccordionrow.bind(this)
    this.resetAccordion = this.resetAccordion.bind(this)
  }

  componentDidMount(){
    const accordionInitate = defaultData.map((r,i) => ({accIndex:i,accOpen:false}))
    const initialData = defaultData.map(({items},i) => {
      const {fields} = fieldDetails[i]
      return items.reduce( (finalData, item) => {
        const initialRowData = fields.reduce ( (row, {itemplaceholder,fieldName,initialValue}) => {
          const fieldValue = itemplaceholder ? item : initialValue
          return {...row,...{[fieldName]:fieldValue}}
        },{})
        initialRowData.dirty = false
        return [...finalData,initialRowData]
      },[])
    })
    this.setState({accordions:accordionInitate,accordionData:initialData})
  }


  toggleAccordion =(aIndex) => {
    const { multiple } = this.props
    const { accordions } = this.state
    if(multiple) {
      const newMultipleState = accordions.map ( ({accIndex,accOpen}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen} : {accIndex,accOpen})
      this.setState(  {accordions:newMultipleState} )
    } else {
      const newSingleState = accordions.map ( ({accIndex,accOpen}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen} : {accIndex,accOpen:false})
      this.setState(  {accordions:newSingleState} )
    }
  }

  handleFieldChange = (aIndex,rowIndex,fieldPos, e) => {
    const {fieldName} = fieldDetails[aIndex].fields[fieldPos]
    console.log("I am in handle field change",aIndex,rowIndex,fieldPos,e.target.value)
    const {accordionData:newAccordionData} = this.state
    newAccordionData[aIndex][rowIndex][fieldName] = e.target.value
    this.setState( {accordionData:newAccordionData})
  }

  addAccordionrow = (aIndex) => {
    // Open the accordion only if it is closed
    if(!this.state.accordions[aIndex].accOpen) {
      this.toggleAccordion(aIndex)
    }
    const {fields} = fieldDetails[aIndex]
    const rowToInsert = fields.reduce ( (row, {fieldName,initialValue}) => ({...row,...{[fieldName]:initialValue}}),{})
    rowToInsert.dirty = true
    const newState = Object.assign({},this.state)
    const {accordionData:newAccordionData} = newState
    newAccordionData[aIndex].push(rowToInsert)
    this.setState({accordionData:newAccordionData})
  }

  resetAccordion = (aIndex) => {
    const newState = Object.assign({},this.state)
    const {accordionData:newAccordionData} = newState
    const accordionRows = newAccordionData[aIndex]

    // find only those that are eligible for deletion
    const revisedRows = accordionRows.filter ( row => row.dirty === false )
    newAccordionData[aIndex] = revisedRows
    this.setState({accordionData:newAccordionData})
  }

  handdleSaveInformation = () => {
    // get the details of the state
  }

  updateAuditTrail =() => {
  }

  validateEntries = () => {
    // Read the state Objects
    // Push an array of errors
    // Display the array of errors / Change the
  }

  renderAccordionHeaders = (aIndex) => {
    const fieldsDetails = fieldDetails[aIndex]
    const {fields:fieldHeaders} = fieldsDetails
    return (
      <thead>
        <tr>
          {
            fieldHeaders.map ( ({colName,abbr}) => (
              <th key={colName}>
                <abbr title={abbr}>{colName}</abbr>
              </th>
            ))
          }
        </tr>
      </thead>
    )
  }

  renderAccordionBody =(aIndex) => {
    const singleAccordionDetail = this.state.accordionData[aIndex]
    return (<tbody>
      {
        singleAccordionDetail.map( (row, rowindex) => this.renderAccordionRow(aIndex,rowindex,row))
      }
    </tbody>)
  }

  renderAccordionRow = (aIndex,rowIndex,rowdata) => {
    const values = Object.values(rowdata)
    return (
      <tr key={`${aIndex}-${rowIndex}`}>
        <td>
          <input className="input is-small is-link" type="text" value = {values[0]} placeholder="Placeholder 123" onChange={(e) => this.handleFieldChange(aIndex,rowIndex,0,e)}/>
        </td>
        <td>
          <input className="input is-small is-link" type="text" value = {values[1]} placeholder="Placeholder 246" onChange={(e) => this.handleFieldChange(aIndex,rowIndex,1,e)}/>
        </td>
      </tr>)
  }

  render() {
    const { accordions } = this.state
    return (
      accordions && <div className="container">
        <section className="accordions box">
          { accordions.map ( ({accOpen,accIndex}) => <article key={accIndex} className={ accOpen ? "accordion is-active" : "accordion"}>
            <div className="accordion-header">
              <p onClick={() => this.toggleAccordion(accIndex) } style={{width:"100%"}}>Underwriters Discount</p>
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link is-small" onClick={() => this.addAccordionrow(accIndex)}>Add</button>
                </div>
                <div className="control">
                  <button className="button is-light is-small" onClick={() => this.resetAccordion(accIndex)}>Reset</button>
                </div>
              </div>
            </div>
            {accOpen && <div className="accordion-body">
              <div className="accordion-content">
                <div style={{overflowX:"auto"}}>
                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                    {this.renderAccordionHeaders(accIndex)}
                    {this.renderAccordionBody(accIndex)}
                  </table>
                </div>
              </div>
            </div>}
          </article>)}
        </section>
      </div>
    )
  }
}

export default DealRatings