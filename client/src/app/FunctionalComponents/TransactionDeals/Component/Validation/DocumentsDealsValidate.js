import Joi from "joi-browser"

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean().required().optional(),
  publicDate: Joi.string().required().optional(),
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean().required().optional(),
  emmaSendDate: Joi.string().required().optional(),
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().required(),
  docSubCategory: Joi.string().required(),
  docType: Joi.string().required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  markedPublic,
  sentToEmma,
  docAction: Joi.string().required(),
  _id: Joi.string().required().optional(),
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().required(),
  dealIssueDocuments: Joi.array().min(1).items(docListSchema).required(),
})

export const DocumentsDealsValidate = (inputTransDetail) => Joi.validate(inputTransDetail, DocDetailsDealsSchema, { abortEarly: false, stripUnknown:false })

