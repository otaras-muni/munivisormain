import Joi from "joi-browser"
import dateFormat from "dateformat";

const seriesPricingDataSchema = Joi.object().keys({
  term: Joi.number().min(0).max(50).label("Term").required(),
  maturityDate: Joi.date()/* .min(Joi.ref("callDate")) */.label("Maturity Date").required(),
  amount: Joi.number().min(0).label("Amount").required(),
  coupon: Joi.number().min(0).max(0.15).label("Coupon").required(),
  yield: Joi.number().min(0).max(0.10).label("Yield").required(),
  price: Joi.number().min(0).label("Price").required(),
  YTM: Joi.number().min(0).max(0.15).label("YTM").required(),
  cusip: Joi.string().uppercase().min(6).max(9).required().label("CUSIP").optional(),
  callDate: Joi.date().label("Call Date").required(),
  takeDown: Joi.number().min(0).label("Take Down").required(),
  insured: Joi.boolean().required().label("Insured").optional(),
  drop: Joi.boolean().label("Drop").required().optional(),
  _id: Joi.string().required().optional(),
})

const seriesPricingDetailsSchema = (minDate) => Joi.object().keys({
  dealSeriesPrincipal: Joi.number().min(0).label("Principal").required(),
  dealSeriesSecurityType: Joi.string().label("Security Type").required().optional(),
  dealSeriesSecurity: Joi.string().allow("", null).label("Security").optional(),
  dealSeriesSecurityDetails: Joi.string().allow("", null).label("Security Details").optional(),
  dealSeriesDatedDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate|| new Date(), "yyyy-mm-dd")) */.allow(null).label("Dated Date").optional(),
  dealSeriesSettlementDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate|| new Date(), "yyyy-mm-dd")) */.allow(null).label("Settlement Date").optional(),
  dealSeriesFirstCoupon: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate|| new Date(), "yyyy-mm-dd")) */.allow(null).label("First Coupon").optional(),
  dealSeriesPutDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate|| new Date(), "yyyy-mm-dd")) */.allow(null).label("Put Date").optional(),
  dealSeriesRecordDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate|| new Date(), "yyyy-mm-dd")) */.allow(null).label("Record Date").optional(),
  dealSeriesCallDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate|| new Date(), "yyyy-mm-dd")) */.allow(null).label("Call Date").optional(),
  dealSeriesFedTax: Joi.string().allow("", null).label("Fed Tax").optional(),
  dealSeriesStateTax: Joi.string().allow("", null).label("State Tax").optional(),
  dealSeriesPricingAMT: Joi.string().allow("", null).label("Pricing Amount").optional(),
  dealSeriesBankQualified: Joi.string().allow("", null).label("Bank Qualified").optional(),
  dealSeriesAccrueFrom: Joi.string().allow("", null).label("Accrue From").optional(),
  dealSeriesPricingForm: Joi.string().allow("", null).label("Pricing Form").optional(),
  dealSeriesCouponFrequency: Joi.string().allow("", null).label("Pricing Form").optional(),
  dealSeriesDayCount: Joi.string().allow("", null).label("Day Count").optional(),
  dealSeriesRateType: Joi.string().allow("", null).label("Rate Type").optional(),
  dealSeriesCallFeature: Joi.string().allow("", null).label("Call Feature").optional(),
  dealSeriesCallPrice: Joi.number().min(0).allow(null).label("Call Price").optional(),
  dealSeriesInsurance: Joi.string().allow("", null).label("Insurance").optional(),
  dealSeriesUnderwtiersInventory: Joi.string().allow("", null).label("Underwtiers Inventory").optional(),
  dealSeriesMinDenomination: Joi.number().min(0).allow(null).label("Minimum Denomination").optional(),
  dealSeriesSyndicateStructure: Joi.string().allow("", null).label("Syndicate Structure").optional(),
  dealSeriesGrossSpread: Joi.number().min(0).allow(null).label("Gross Spread").optional(),
  dealSeriesEstimatedTakeDown: Joi.number().min(0).allow(null).label("Estimated TakeDown").optional(),
  dealSeriesInsuranceFee: Joi.number().min(0).allow(null).label("Insurance Fee").optional(),
  _id: Joi.string().required().optional(),
})

const dealsPricingSchema = (minDate) => Joi.object().keys({
  seriesPricingDetails: seriesPricingDetailsSchema(minDate),
  seriesPricingData: Joi.array().items(seriesPricingDataSchema).optional(),
})

export const PricingDetailsValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, dealsPricingSchema(minDate),  { abortEarly: false, stripUnknown:false })
