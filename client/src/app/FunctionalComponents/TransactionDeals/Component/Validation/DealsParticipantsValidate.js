import Joi from "joi-browser"

const dealPartDistSchema = Joi.object().keys({
  dealPartFirmId: Joi.string().required().optional(),
  dealPartType: Joi.string().required(),
  dealPartFirmName: Joi.string().required().optional(),
  dealPartContactId: Joi.string().required().optional(),
  dealPartContactName: Joi.string().required(),
  dealPartContactTitle: Joi.string().allow("").optional(),
  dealPartContactEmail:  Joi.string().email().required(),
  dealPartUserAddress: Joi.string().allow("").optional(),
  dealPartContactAddrLine1: Joi.string().allow("").optional(),
  dealPartContactAddrLine2: Joi.string().allow("").optional(),
  dealPartAddressGoogle: Joi.string().allow("").optional(),
  dealParticipantState: Joi.string().allow("").optional(),
  dealPartContactPhone: Joi.string().allow("").optional(),
  dealPartContactFax: Joi.string().allow("").optional(),
  addToDL: Joi.boolean().optional(),
  createdDate: Joi.date().example(new Date("2016-01-01")).optional(),
  isNew: Joi.boolean().optional(),
  _id: Joi.string().required().optional(),
})

const dealUnderWritersSchema = Joi.object().keys({
  dealPartFirmId: Joi.string().required(),
  dealPartType: Joi.string().required(),
  dealPartFirmName: Joi.string().required(),
  roleInSyndicate: Joi.string().required(),
  liabilityPerc: Joi.number().allow(null, "").optional(),
  managementFeePerc: Joi.number().allow(null, "").optional(),
  dealPartContactAddToDL: Joi.boolean().required().optional(),
  isNew: Joi.boolean().optional(),
  _id: Joi.string().required().optional(),
})

export const DealsPartUnderWritersValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, dealUnderWritersSchema, { abortEarly: false, stripUnknown:false })
}

export const DealsParticipantsValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, dealPartDistSchema, { abortEarly: false, stripUnknown:false })
}
