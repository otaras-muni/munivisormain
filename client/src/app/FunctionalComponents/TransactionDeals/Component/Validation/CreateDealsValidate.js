import Joi from "joi-browser"
import dateFormat from "dateformat"

const transDetailSchema = (minDate, date) =>
  Joi.object().keys({
    dealIssueTranIssueName: Joi.string()
      .allow(["", null])
      .optional(),
    dealIssueTranProjectDescription: Joi.string()
      .required()
      .optional(),
    dealIssueTranIssuerId: Joi.string().optional(),
    dealIssueTranClientId: Joi.string().optional(),
    dealIssueTranName: Joi.string()
      .allow("")
      .optional(),
    // dealIssueBorrowerName: Joi.string().allow("").optional(),
    // dealIssueGuarantorName: Joi.string().allow("").optional(),
    dealIssueTranType: Joi.string()
      .allow("")
      .optional(),
    dealIssueTranSubType: Joi.string()
      .allow("")
      .optional(),
    dealIssueTranStatus: Joi.string()
      .allow("")
      .optional(),
    dealIssueTranState: Joi.string()
      .allow("")
      .optional(),
    dealIssueTranCounty: Joi.string()
      .allow("")
      .optional(),
    dealIssueTranPrimarySector: Joi.string()
      .required()
      .optional(),
    dealIssueTranSecondarySector: Joi.string()
      .allow("")
      .optional(),
    dealIssueofferingType: Joi.string()
      .allow("")
      .optional(),
    dealIssueSecurityType: Joi.string()
      .allow("")
      .optional(),
    dealIssueBankQualified: Joi.string()
      .allow("")
      .optional(),
    dealIssueCorpType: Joi.string()
      .allow("")
      .optional(),
    dealIssueParAmount: Joi.number().min(0).allow(null, "").optional(),
    dealIssuePricingDate:
      date === "historical"
        ? Joi.date().example(new Date("2016-01-01")).optional()
        : Joi.date()
          .example(new Date("2016-01-01"))
        /* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */
          .optional(),
    dealIssueExpAwardDate:
      date === "historical"
        ? Joi.date().example(new Date("2016-01-01")).optional()
        : Joi.date().example(new Date("2016-01-01")).min(Joi.ref("dealIssuePricingDate")).optional(),
    dealIssueActAwardDate:
      date === "historical"
        ? Joi.date().example(new Date("2016-01-01")).optional()
        : Joi.date().example(new Date("2016-01-01")).min(Joi.ref("dealIssueExpAwardDate")).optional(),
    dealIssueSdlcCreditPerc: Joi.number()
      .max(100)
      .min(0)
      .allow(null)
      .optional(),
    dealIssueEstimatedRev: Joi.number().min(0).allow(null, "").optional(),
    dealIssueUseOfProceeds: Joi.string()
      .allow("")
      .optional(),
    dealIssuePlaceholder: Joi.string()
      .allow("")
      .optional(),
    termsOfIssue: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    tic: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    bbri: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    nic: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    gsp: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    mgtf: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    exp: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    tkdn: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    other: Joi.number()
      .max(100)
      .min(0)
      .allow(null, "")
      .optional(),
    updateUser: Joi.string().optional(),
    updateDate: Joi.date().optional(),
    _id: Joi.string().required()
  })

export const CreateDealsValidate = (inputTransDetail, minDate, date) =>
  Joi.validate(inputTransDetail, transDetailSchema(minDate, date), {
    abortEarly: false,
    stripUnknown: false
  })
