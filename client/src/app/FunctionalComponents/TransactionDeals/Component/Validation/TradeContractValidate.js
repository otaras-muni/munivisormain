import Joi from "joi-browser";

const dealsTradingInformationSchema = Joi.object().keys({
  contractID: Joi.string().required(),
  contractStatus: Joi.string().required(),
  portfolio: Joi.string().required(),
  portfolioGroup: Joi.string().required(),
  counterParty: Joi.string().required(),
  securityGroup: Joi.string().required(),
  securityType: Joi.string().required(),
  brokerOrDealer: Joi.string().required(),
  tradeDate: Joi.string().required(),
  settlementDays: Joi.number().min(0).required(),
  settlementDate: Joi.string().required(),
  _id: Joi.string().required().optional(),
}).required()

const dealsContractInformationSchema = Joi.object().keys({
  standardContract: Joi.string().required(),
  receive: Joi.string().required(),
  currency: Joi.string().required(),
  notional: Joi.number().min(0).required(),
  fixedRate: Joi.number().min(0).required(),
  referenceRate: Joi.string().required(),
  spread: Joi.number().min(0).required(),
  initialRate: Joi.number().min(0).required(),
  effectiveDate: Joi.string().required(),
  tenor: Joi.number().min(0).required(),
  tenorType: Joi.string().required(),
  _id: Joi.string().required().optional(),
}).required()

const dealIssueLegSchema = Joi.object().keys({
  interest: Joi.string().required(),
  frequency: Joi.string().required(),
  maturityDate: Joi.string().required(),
  dayCount: Joi.string().required(),
  businessConvention: Joi.string().required(),
  bankHolidays: Joi.string().required(),
  endOfMonth: Joi.string().required(),
  securityGroup: Joi.string().required(),
  securityType: Joi.string().required(),
  currency: Joi.string().required(),
  notional: Joi.number().min(0).required(),
  fixedRate: Joi.number().min(0).required(),
  referenceRate: Joi.string().required(),
  spread: Joi.number().min(0).required(),
  initialRate: Joi.number().min(0).required(),
  effectiveDate: Joi.string().required(),
  _id: Joi.string().required().optional(),
}).required()

const dealIssueComplianceSchema = Joi.object().keys({
  status: Joi.string().required(),
  canRelease: Joi.string().required(),
  _id: Joi.string().required().optional(),
}).required()

const dealsContractSchema = Joi.object().keys({
  dealIssueTradingInformation:  dealsTradingInformationSchema,
  dealIssueContractInformation:  dealsContractInformationSchema,
  dealIssueReceiveLeg: dealIssueLegSchema,
  dealIssuePayLeg: dealIssueLegSchema,
  dealIssueCompliance: dealIssueComplianceSchema,
})

export const TradeContractValidate = (inputTransDistribute) => {
  return Joi.validate(inputTransDistribute, dealsContractSchema, { abortEarly: false, stripUnknown:false })
}

