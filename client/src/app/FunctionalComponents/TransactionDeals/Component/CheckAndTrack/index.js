import React from "react"
import {toast} from "react-toastify"
import ProcessChecklist from "../../../../GlobalComponents/ProcessChecklist"
import {putDealsTransaction, fetchDealsTransaction} from "../../../../StateManagement/actions/Transaction"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"

class CheckNTrack extends React.Component {
  constructor() {
    super()
    this.state = {
      checkLists: [],
      checklistId: "",
      participants: [],
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
  }

  componentWillMount() {
    const {transaction} = this.props
    if (transaction) {
      const participants = transaction && transaction.dealIssueParticipants && transaction.dealIssueParticipants.map(part => ({
        _id: part.dealPartContactId, // eslint-disable-line
        name: part.dealPartContactName,
        type: part.dealPartType
      })) || []
      // transaction.dealIssueTranAssignedTo && transaction.dealIssueTranAssignedTo.forEach(assign => { // eslint-disable-line
      //   participants.push({
      //     _id: assign._id, // eslint-disable-line
      //     name: assign.userFirstName,
      //     type: "Assginees"
      //   })
      // })
      this.setState(prevState => ({
        participants,
        checkLists: (transaction && transaction.dealChecklists) || [],
        loading: false,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.dealIssueTranIssueName || transaction.dealIssueTranProjectDescription} - Notification`
        }
      }))
    }else {
      this.setState({
        loading: false,
      })
    }
  }

  saveChecklist = (checkLists, checklistId) => {
    this.setState({
      checkLists,
      checklistId,
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const {email, checkLists, checklistId } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    let url = window.location.pathname.replace("/","")
    if(checklistId) {
      url = `${url}?cid=${checklistId}`
    }
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice:true,
      emailParams: {
        url,
        ...email,
      }
    }
    console.log("==============email send to ==============", emailParams)
    this.setState({
      modalState: false
    }, () => {
      putDealsTransaction(this.props.nav3, {dealChecklists: checkLists, _id: this.props.nav2}, (res)=> {
        if (res && res.status === 200) {
          toast("Deal check-n-track has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            checkLists: checkLists || [],
          }, async () => {
            await sendEmailAlert(emailParams)
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  render() {
    const {modalState, email, loading} = this.state
    const {participants, onParticipantsRefresh, transaction, tranAction} = this.props
    if(loading) {
      return <Loader/>
    }
    return (
      <div>
        <SendEmailModal modalState={modalState} email={email} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onModalChange={this.onModalChange} onSave={this.onConfirmationSave}/>
        <ProcessChecklist checklists={this.state.checkLists}
          totalThresholds={[0.20, 15000]}
          participants={this.state.participants || []}
          onSaveChecklist={this.saveChecklist}
          tenantId={transaction.dealIssueTranClientId}
          isDisabled={!tranAction.canTranStatusEditDoc}
        />
      </div>
    )
  }
}

export default CheckNTrack
