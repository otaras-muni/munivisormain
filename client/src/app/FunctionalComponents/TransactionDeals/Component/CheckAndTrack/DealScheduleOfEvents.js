import React, { Component } from "react"
import {toast} from "react-toastify"
import moment from "moment"
import CONST from "GlobalUtils/consts"
import { Multiselect } from "react-widgets"
import {putDealsCheckNTrack, removeDealsCheckInTrack} from "../../../../StateManagement/actions/Transaction";


const USERLIST = [
  { id: "City of Princeton", users: ["PR-U1", "PR-U2", "PR-U3", "PR-U4"] },
  {
    id: "City of South Brunswick",
    users: ["SB-U1", "SB-U2", "SB-U3", "SB-U4"]
  },

  {
    id: "Kansas Electric Company",
    users: ["KE-U1", "KE-U2", "KE-U3", "KE-U4"]
  },
  {
    id: "Ford and Associates",
    users: ["FAS-U1", "FAS-U2", "FAS-U3", "FAS-U4"]
  },
  { id: "PRAG", users: ["PRG-U1", "PRG-U2", "PRG-U3", "PRG-U4"] }
]

const defaultData = [{
  soeCategoryName:"Manage Deal Events",
  items:[
    "Draft Documents due to Board",
    "Comments Due on First Draft of Documents",
    "Distribute First Draft of POS",
    "Comments Due on First Draft of POS",
    "Distribute Second Draft of Documents",
    "Distribute Second Draft of POS",
    "Package to Rating Agencies and Bond Insurers"
  ]
}]

const fieldDetails = [{
  fields:[
    {
      colName:"Event",
      abbr:"Event",
      itemplaceholder:true,
      fieldName:"eventItem",
      initialValue:"",
    },
    {
      colName:"Responsible Party",
      abbr:"Responsible issue part(y/ies)",
      itemplaceholder:false,
      fieldName:"responsibleParties",
      initialValue:"",
    },
    {
      colName:"Start Date",
      abbr:"Start Date",
      itemplaceholder:false,
      fieldName:"eventStartDate",
      initialValue:"",
    },
    {
      colName:"End Date",
      abbr:"End Date",
      itemplaceholder:false,
      fieldName:"eventEndDate",
      initialValue:"",
    },
    {
      colName:"Status",
      abbr:"Status of the Item",
      itemplaceholder:false,
      fieldName:"eventStatus",
      initialValue:"",
    },
    {
      colName:"Drop",
      abbr:"Delete cost item",
      itemplaceholder:false,
      fieldName:"costItemDrop",
      initialValue:true,
    }
  ]
}]

class DealScheduleOfEvents extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions:[],dealIssueScheduleOfEvents:[],dealIssueCostOfIssuanceNotes:""}
    this.toggleAccordion = this.toggleAccordion.bind(this)
    this.renderAccordionHeaders = this.renderAccordionHeaders.bind(this)
    this.renderAccordionBody = this.renderAccordionBody.bind(this)
    this.renderAccordionRow = this.renderAccordionRow.bind(this)
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.addAccordionrow = this.addAccordionrow.bind(this)
    this.resetAccordion = this.resetAccordion.bind(this)
    this.removeAccordionRow = this.removeAccordionRow.bind(this)
    this.handleNotesChange = this.handleNotesChange.bind(this)
  }

  componentWillMount() {
    const scheduleOfEvents = defaultData.map(({items},i) => {
      const {fields} = fieldDetails[i]
      return items.reduce( (finalData, item) => {
        const initialRowData = fields.reduce ( (row, {itemplaceholder,fieldName,initialValue}) => {
          const fieldValue = itemplaceholder ? item : initialValue
          return {...row,...{[fieldName]:fieldValue}}
        },{})
        initialRowData.dirty = false
        return [...finalData,initialRowData]
      },[])
    })
    let {dealIssueCostOfIssuanceNotes} = this.props
    let items = []
    let dealIssueScheduleOfEvents = this.props.dealIssueScheduleOfEvents || []
    dealIssueScheduleOfEvents.forEach(event => {
      items.push(event.soeItems)
    })
    items = this.props.mergeCheckInTrack(items, 'scheduleOfEvents', scheduleOfEvents)
    this.setState({
      dealIssueScheduleOfEvents: items,
      dealIssueCostOfIssuanceNotes,
    })
  }

  componentDidMount(){
    const accordionInitate = defaultData.map((r,i) => ({accIndex:i,accOpen:false,accHeader:r.soeCategoryName}))
    this.setState({accordions:accordionInitate})
  }

  toggleAccordion =(aIndex) => {
    const { multiple } = this.props
    const { accordions } = this.state
    if(multiple) {
      const newMultipleState = accordions.map ( ({accIndex,accOpen,accHeader}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen,accHeader} : {accIndex,accOpen,accHeader})
      this.setState(  {accordions:newMultipleState} )
    } else {
      const newSingleState = accordions.map ( ({accIndex,accOpen,accHeader}) => accIndex === aIndex ? {accIndex,accOpen:!accOpen,accHeader} : {accIndex,accOpen:false,accHeader})
      this.setState(  {accordions:newSingleState} )
    }
  }

  handleFieldChange = (aIndex,rowIndex,fieldPos, e) => {
    const {fieldName} = fieldDetails[aIndex].fields[fieldPos]
    const {dealIssueScheduleOfEvents:newAccordionData} = this.state

    if(fieldName !== "responsibleParties"){
      const val =  e.target.name === "cleaveinput" ? e.target.rawValue : e.target.value
      newAccordionData[aIndex][rowIndex][fieldName] = val
    } else {
      newAccordionData[aIndex][rowIndex][fieldName] = e
    }

    this.setState( {dealIssueScheduleOfEvents:newAccordionData})
  }

  handleNotesChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
  }

  addAccordionrow = (aIndex) => {
    // Open the accordion only if it is closed
    if(!this.state.accordions[aIndex].accOpen) {
      this.toggleAccordion(aIndex)
    }
    const newAccordionData=[...this.state.dealIssueScheduleOfEvents]
    // Check if already there is a blank row
    const singleAccordionData = [...newAccordionData[aIndex]]
    const emptyRowExists = singleAccordionData.reduce( ( isAnyRowEmpty, row) => {
      const {dirty,costItemDrop,...maindata} = row
      const rowEmpty = Object.values(maindata).reduce( (isEmpty, field) => isEmpty && (field === ""),true)
      return isAnyRowEmpty || rowEmpty
    }, false)

    if(!emptyRowExists){
      const {fields} = fieldDetails[aIndex]
      const rowToInsert = fields.reduce ( (row, {fieldName,initialValue}) => ({...row,...{[fieldName]:initialValue}}),{})
      rowToInsert.dirty = true
      newAccordionData[aIndex].push(rowToInsert)
      this.setState({dealIssueScheduleOfEvents:newAccordionData})
    }
  }

  removeAccordionRow = (aIndex, rowId, type, trackId) => {
    const newAccordionData = [...this.state.dealIssueScheduleOfEvents]
    const singleAccordionDetail = [...this.state.dealIssueScheduleOfEvents[aIndex]]
    const newAccordionDetail = singleAccordionDetail.filter( (row, ind) => rowId !== ind )
    newAccordionData[aIndex] = newAccordionDetail
    const result  = confirm("Are you sure?, you want to remove this row !")
    if(!result) return
    if(trackId && type) {
      removeDealsCheckInTrack(this.props.nav2, type, trackId, "scheduleOfEvents", (res) => {
        if(res && res.status === 200) {
          toast("Deals checkntrack row removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
          this.setState({dealIssueScheduleOfEvents:newAccordionData})
        }else {
          toast("Something went wrong",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR })
        }
      })
    }else {
      toast("Deals checkntrack row removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
      this.setState({dealIssueScheduleOfEvents:newAccordionData})
    }
  }

  resetAccordion = (aIndex) => {
    const newState = Object.assign({},this.state)
    const {dealIssueScheduleOfEvents:newAccordionData} = newState
    const accordionRows = newAccordionData[aIndex]

    // find only those that are eligible for deletion
    const revisedRows = accordionRows.filter ( row => row.dirty === false )
    newAccordionData[aIndex] = revisedRows
    this.setState({dealIssueScheduleOfEvents:newAccordionData})
  }

  resetNotes =() => {
    this.setState({dealIssueCostOfIssuanceNotes:""})
  }

  resetForm =() => {
    const {accordions} = this.state
    accordions.forEach( acc => this.resetAccordion(acc.accIndex))
    this.resetNotes()
    toast("All data will be reset to the previous saved state",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
  }

  handleSaveInformation = () => {
    const { accordions, dealIssueCostOfIssuanceNotes } = this.state
    let inValid = []
    this.state.dealIssueScheduleOfEvents.forEach(initCheckTrack => {
      inValid.push(initCheckTrack.map(event => event.dirty && (!event.eventItem || !event.responsibleParties
        || !event.eventStartDate || !event.eventEndDate || !event.eventStatus)))
    })

    if(inValid && ( inValid[0].indexOf(true) !== -1 || inValid[0].indexOf(true) !== -1)) {
      toast("Please schedule of events new items added",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      return
    }

    const dealIssueScheduleOfEvents = this.state.dealIssueScheduleOfEvents.reduce( (finalObject,data,index) => {
      const revisedData = data.map( ({dirty,costItemDrop,...saveFields }) => saveFields)
      return [...finalObject, { soeCategoryName:accordions[index].accHeader,soeItems:revisedData}]
    },[])

    const payload = {
      dealIssueScheduleOfEvents,
      dealIssueCostOfIssuanceNotes
    }

    putDealsCheckNTrack(this.props.nav2, payload, (res)=> {
      if (res && res.status === 201) {
        toast("Check in track Schedule of Events updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
        this.props.getCheckInTrack()
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      }
    })
  }

  updateAuditTrail =() => {
  }

  renderAccordionHeaders = (aIndex) => {
    const fieldsDetails = fieldDetails[aIndex]
    const {fields:fieldHeaders} = fieldsDetails
    return (
      <thead>
        <tr>
          {
            fieldHeaders.map ( ({colName,abbr}) => (
              <th key={colName}>
                <abbr title={abbr}>{colName}</abbr>
              </th>
            ))
          }
        </tr>
      </thead>
    )
  }


  renderAccordionBody =(aIndex, accHeader) => {
    const singleAccordionDetail = [...this.state.dealIssueScheduleOfEvents[aIndex]]
    return (<tbody>
      {singleAccordionDetail.map( (row, rowindex) => this.renderAccordionRow(aIndex,rowindex,row, accHeader))}
    </tbody>)
  }

  renderAccordionRow = (aIndex,rowIndex,rowData,category) => {
    const values = Object.values(rowData)
    return (
      <tr key={`${aIndex}-${rowIndex}`}>
        <td>
          {
            rowData.dirty ?
              <input className="input is-small is-link" type="text" value = {rowData.eventItem} placeholder="Enter item" onChange={(e) => this.handleFieldChange(aIndex,rowIndex,0,e)}/>
              :
              rowData.eventItem
          }
        </td>
        <td>
          <Multiselect
            data={[{dealParticipantType:"Ford",dealPartyUserId:"Naveen"},{dealParticipantType:"Ford",dealPartyUserId:"Mayur"},{dealParticipantType:"City of NY",dealPartyUserId:"Umesh"},{dealParticipantType:"City of NY",dealPartyUserId:"Kapil"}]}
            value={rowData.responsibleParties || []}
            textField={({dealPartyUserId}) => dealPartyUserId}
            groupBy={({dealParticipantType}) => dealParticipantType}
            onChange={(e) => this.handleFieldChange(aIndex,rowIndex,1,e)}
          />
        </td>
        <td>
          <input className="input is-small is-link" type="date" value={(rowData.eventStartDate && moment(rowData.eventStartDate).format('YYYY-MM-DD')) || ""} onChange={(e) => this.handleFieldChange(aIndex,rowIndex,2,e)} name="dealSoeStartDate" />
        </td>
        <td>
          <input className="input is-small is-link" type="date" value={(rowData.eventEndDate && moment(rowData.eventEndDate).format('YYYY-MM-DD')) || ""} onChange={(e) => this.handleFieldChange(aIndex,rowIndex,3,e)} name="dealSoeEndDate" />
        </td>
        <td>
          <select value={rowData.eventStatus} className="input is-small is-link" onChange={(e) => this.handleFieldChange(aIndex,rowIndex,4,e)}>
            {["","A","B","C","D"].map( (s,i) => <option key={s} disabled={i===0} value={s}>{s}</option>)}
          </select>
        </td>
        <td>
          <span className="has-text-link"  style={{cursor: "pointer"}} onClick={() => this.removeAccordionRow(aIndex,rowIndex,category,rowData._id || '')}>
            <i className="far fa-trash-alt" />
          </span>
        </td>
      </tr>
    )
  }

  render() {
    const { accordions, dealIssueCostOfIssuanceNotes } = this.state
   /* const dealIssueScheduleOfEvents = this.state.dealIssueScheduleOfEvents.reduce( (finalObject,data,index) => {
      const revisedData = data.map( ({dirty,costItemDrop,...saveFields }) => saveFields)
      return [...finalObject, { soeCategoryName:accordions[index].accHeader,soeItems:revisedData}]
    },[])*/

    return (
      accordions && <div className="container dealSoE" >
        <section className="accordions box dealSoE">
          { accordions.map ( ({accOpen,accIndex,accHeader}) => <article key={accIndex} className={ accOpen ? "accordion is-active" : "accordion"}>
            <div className="accordion-header">
              <p onClick={() => this.toggleAccordion(accIndex) } style={{width:"100%"}}>{accHeader}</p>
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link is-small" onClick={() => this.addAccordionrow(accIndex)}>Add</button>
                </div>
                <div className="control">
                  <button className="button is-light is-small" onClick={() => this.resetAccordion(accIndex)}>Reset</button>
                </div>
              </div>
            </div>
            {accOpen && <div className="accordion-body">
              <div className="accordion-content">
                <div>
                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                    {this.renderAccordionHeaders(accIndex)}
                    {this.renderAccordionBody(accIndex, accHeader)}
                  </table>
                </div>
              </div>
            </div>}
          </article>)}

          <article className="accordion is-active">
            <div className="accordion-header toggle">
              <p>Notes/Instructions</p>
            </div>
            <div className="accordion-body">
              <div className="accordion-content">
                <div className="columns">
                  <div className="column is-full">
                    <div className="field">
                      <label className="label">
                        <abbr title="Notes and instructions by transaction participants">Notes/Instructions</abbr>
                      </label>
                      <div className="control">
                        <textarea className="textarea" name="dealIssueCostOfIssuanceNotes" value={dealIssueCostOfIssuanceNotes} onChange={this.handleNotesChange} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped">
                      <div className="control">
                        <button className="button is-link" onClick={() => this.handleSaveInformation() }
                        >Save</button>
                      </div>
                      <div className="control">
                        <button className="button is-link" onClick={() => this.resetForm()}>Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </section>
       {/* <pre>
          {JSON.stringify({dealIssueScheduleOfEvents,dealIssueCostOfIssuanceNotes},null,2)}
        </pre>*/}
      </div>
    )
  }
}

export default DealScheduleOfEvents
