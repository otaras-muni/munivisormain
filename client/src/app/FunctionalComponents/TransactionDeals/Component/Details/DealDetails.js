import React, { Component } from "react"
import { connect } from "react-redux"
import moment from "moment"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "../../../../GlobalComponents/Loader"
import { CreateDealsValidate } from "../Validation/CreateDealsValidate"
import {
  putDealsTransaction,
  postDealsNotes,
  sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import {
  TextLabelInput,
  SelectLabelInput,
  NumberInput
} from "../../../../GlobalComponents/TextViewBox"
import CONST, { states } from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import HistoricalData from "../../../../GlobalComponents/HistoricalData"
import SearchCountry from "../../../GoogleAddressForm/GoogleCountryComponent"
import TransactionNotes from "../../../../GlobalComponents/TransactionNotes"

const getInitialState = () => ({
  dealIssueTranIssueName: "",
  dealIssueTranProjectDescription: "",
  dealIssueTranIssuerId: "",
  dealIssueTranClientId: "",
  dealIssueTranName: "",
  // dealIssueBorrowerName: "",
  // dealIssueGuarantorName: "",
  // dealIssueTranType: "",
  dealIssueTranSubType:"",
  dealIssueTranStatus: "",
  dealIssueTranState: "",
  dealIssueTranCounty: "",
  dealIssueTranPrimarySector: "",
  dealIssueTranSecondarySector: "",
  dealIssueofferingType: "",
  dealIssueSecurityType: "",
  dealIssueBankQualified: "",
  dealIssueCorpType: "",
  dealIssueParAmount: null,
  dealIssuePricingDate: "",
  dealIssueExpAwardDate: "",
  dealIssueActAwardDate: "",
  dealIssueSdlcCreditPerc: 0,
  dealIssueEstimatedRev: "",
  dealIssueUseOfProceeds: "",
  dealIssuePlaceholder: "",
  termsOfIssue: 0,
  tic: 0,
  bbri: 0,
  nic: 0,
  gsp: 0,
  mgtf: 0,
  exp: 0,
  tkdn: 0,
  other: 0,
  updateUser: "",
  updateDate: "",
  _id: ""
})

// get the list of states for a given country
const getListOfStatesForCountry = (LIST, countryToBeSearched) =>
  LIST.reduce((allStatesForCountry, { countryName, states }) => {
    if (countryName === countryToBeSearched) {
      return states.reduce(
        (allStateNames, state) => [
          ...allStateNames,
          {
            stateFullName: state.stateFullName,
            stateShortName: state.stateShortName
          }
        ],
        []
      )
    }
    return allStatesForCountry
  }, [])

// get all the counties for a given state
const getListOfCounties = (LIST, countryToBeSearched, stateToBeSearched) =>
  LIST.reduce((allCountiesForState, country) => {
    if (country.countryName === countryToBeSearched) {
      return country.states.reduce(
        (allcounties, { stateShortName, stateFullName, counties }) =>
          stateShortName === stateToBeSearched ||
          stateFullName === stateToBeSearched
            ? [...allcounties, ...counties]
            : allcounties,
        []
      )
    }
    return allCountiesForState
  }, [])

class DealDetails extends Component {
  constructor() {
    super()
    this.state = {
      deals: {
        ...getInitialState()
      },
      tempDeals: {
        ...getInitialState()
      },
      userName: "",
      errorMessages: {},
      usersList: [],
      tranNotes: [],
      states: [...states],
      dropDown: {
        borrowerOrObligorName: [],
        guarantorName: [],
        tranType: [],
        tranStatus: [],
        primarySector: {},
        secondarySector: [],
        offeringType: [],
        securityType: [],
        bankQualified: [],
        corpType: [],
        useOfProceeds: [],
        stateCountry: {}
      },
      isSaveDisabled: false,
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      }
    }
  }

  async componentWillMount() {
    const { transaction, tranAction } = this.props

    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    this.setState(prevState => ({
      deals: {
        ...this.state.deals,
        ...transaction,
        dealIssuePricingDate:
          (transaction.dealIssuePricingDate &&
            moment(
              new Date(transaction.dealIssuePricingDate)
                .toISOString()
                .substring(0, 10)
            ).format("YYYY-MM-DD")) ||
          "",
        dealIssueExpAwardDate:
          (transaction.dealIssueExpAwardDate &&
            moment(
              new Date(transaction.dealIssueExpAwardDate)
                .toISOString()
                .substring(0, 10)
            ).format("YYYY-MM-DD")) ||
          "",
        dealIssueActAwardDate:
          (transaction.dealIssueActAwardDate &&
            moment(
              new Date(transaction.dealIssueActAwardDate)
                .toISOString()
                .substring(0, 10)
            ).format("YYYY-MM-DD")) ||
          "",
        dealIssueTranState: transaction.dealIssueTranState || userState,
        dealIssueTranCounty: transaction.dealIssueTranCounty || userCounty
      },
      tempDeals: {
        ...this.state.tempDeals,
        ...transaction,
        dealIssuePricingDate:
          (transaction.dealIssuePricingDate &&
            moment(
              new Date(transaction.dealIssuePricingDate)
                .toISOString()
                .substring(0, 10)
            ).format("YYYY-MM-DD")) ||
          "",
        dealIssueExpAwardDate:
          (transaction.dealIssueExpAwardDate &&
            moment(
              new Date(transaction.dealIssueExpAwardDate)
                .toISOString()
                .substring(0, 10)
            ).format("YYYY-MM-DD")) ||
          "",
        dealIssueActAwardDate:
          (transaction.dealIssueActAwardDate &&
            moment(
              new Date(transaction.dealIssueActAwardDate)
                .toISOString()
                .substring(0, 10)
            ).format("YYYY-MM-DD")) ||
          ""
      },
      tranNotes: transaction && transaction.tranNotes || [],
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.dealIssueTranIssueName ||
          transaction.dealIssueTranProjectDescription} - Notification`
      }
    }))
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName([
      "LKUPPRIMARYSECTOR",
      "LKUPFIRMNAME",
      "LKUPTRANSACTIONTYPE",
      "LKUPDEALTYPE",
      "LKUPSWAPTRANSTATUS",
      "LKUPCORPTYPE",
      "LKUPDEALOFFERING",
      "LKUPPARTICIPANTTYPE",
      "LKUPBANKQUALIFIED",
      "LKUPSECTYPEGENERAL",
      "LKUPUSEOFPROCEED",
      // "LKUPCOUNTRY"
    ])
    const result = (picResult.length && picResult[1]) || {}
    const primarySec = picResult[2].LKUPPRIMARYSECTOR || {}
    const stateCountry = /* (picResult[3] && picResult[3].LKUPCOUNTRY) || */ {}
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        primarySectors: result.LKUPPRIMARYSECTOR || [],
        secondarySectors: primarySec,
        stateCountry,
        borrowerOrObligorName: result.LKUPFIRMNAME,
        guarantorName: result.LKUPFIRMNAME,
        tranType: result.LKUPDEALTYPE,
        tranStatus: result.LKUPSWAPTRANSTATUS,
        offeringType: result.LKUPDEALOFFERING,
        securityType: result.LKUPSECTYPEGENERAL,
        bankQualified: result.LKUPBANKQUALIFIED,
        corpType: result.LKUPCORPTYPE,
        useOfProceeds: result.LKUPUSEOFPROCEED
      },
      userName: (this.props.user && this.props.user.userFirstName) || "",
      loading: false
    })
  }

  onChange = event => {
    this.setState({
      deals: {
        ...this.state.deals,
        [event.target.name]: event.target.value
      }
    })
  }

  onBlurInput = event => {
    const { userName } = this.state
    if (event.target.title && event.target.value) {
      this.props.addAuditLog({
        userName,
        log: `In Deal Details ${event.target.title || "empty"} change to ${event.target.value || "empty"}`,
        date: new Date(),
        key: "Deal Details"
      })
    }
  }

  onCancel = () => {
    const { transaction, tranAction } = this.props
    const userState = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.state) || "" : ""
    const userCounty = tranAction && tranAction.canTranStatusEditDoc ? (transaction && transaction.entityAddress && transaction.entityAddress.city) || "" : ""
    this.setState({
      deals: {
        ...this.state.tempDeals,
        dealIssueTranState: transaction.dealIssueTranState || userState,
        dealIssueTranCounty: transaction.dealIssueTranCounty || userCounty
      },
      errorMessages: {}
    })
  }

  onSubmit = () => {
    const { dealMigration } = this.state.deals
    const deals = cloneDeep(this.state.deals)
    // let newDeal = {}
    const initialDeals = {
      ...getInitialState(),
      dealIssueTranClientId: this.props.user.entityId || ""
    }
    Object.keys(deals).forEach(key => {
      if (!initialDeals.hasOwnProperty(key)) {
        delete deals[key]
      }
    })
    deals.updateDate = new Date()
    deals.updateUser = (this.props.user && this.props.user.userId) || ""
    /* Object.keys(deals).forEach(key => {
       if(deals[key]){
         newDeal[key] = deals[key]
       }
    }) */
    const remField = [
      "dealIssueTranIssuerId",
      "dealIssueTranStatus",
      "dealIssueTranStatus",
      "dealIssuePricingDate",
      "dealIssueExpAwardDate",
      "dealIssueActAwardDate"
    ]
    remField.forEach(key => {
      if (!deals[key]) {
        delete deals[key]
      }
    })
    console.log("======", deals)
    const errors = CreateDealsValidate(
      deals,
      this.state.deals.created_at,
      dealMigration && dealMigration.onboardingStatus
    )

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      this.setState({ errorMessages })
      return
    }

    this.setState({
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    // console.log("==============email send to ==============", emailParams)

    const deals = cloneDeep(this.state.deals)
    const initialDeals = {
      ...getInitialState(),
      dealIssueTranClientId: this.props.user.entityId || ""
    }
    Object.keys(deals).forEach(key => {
      if (!initialDeals.hasOwnProperty(key)) {
        delete deals[key]
      }
    })
    deals.updateDate = new Date()
    deals.updateUser = (this.props.user && this.props.user.userId) || ""

    /* let newDeal = {}
    Object.keys(deals).forEach(key => {
      if(deals[key]){
        newDeal[key] = deals[key]
      }
    })
    */

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        const remField = [
          "dealIssueTranIssuerId",
          "dealIssueTranStatus",
          "dealIssueTranStatus",
          "dealIssuePricingDate",
          "dealIssueExpAwardDate",
          "dealIssueActAwardDate"
        ]
        remField.forEach(key => {
          if (!deals[key]) {
            delete deals[key]
          }
        })
        putDealsTransaction("details", deals, res => {
          if (res && res.status === 200) {
            toast("Deal transaction has been updated!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(this.props.nav2)
            this.setState(
              {
                errorMessages: {},
                deals: cloneDeep(this.state.deals),
                tempDeals: deals,
                isSaveDisabled: false
              },
              async () => {
                await sendEmailAlert(emailPayload)
              }
            )
          } else {
            toast("Something went wrong!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  getCountryDetails = (cityStateCountry = "") => {
    const { deals } = this.state
    if (cityStateCountry && cityStateCountry.state) {
      deals.dealIssueTranState = cityStateCountry.state.trim()
    }else {
      deals.dealIssueTranState = ""
    }
    if (cityStateCountry && cityStateCountry.city) {
      deals.dealIssueTranCounty = cityStateCountry.city.trim()
    }else {
      deals.dealIssueTranCounty = ""
    }
    this.setState({
      deals
    })
  }

  onNotesSave = (payload, callback) => {
    postDealsNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add Deals Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  render() {
    const {
      modalState,
      email,
      deals,
      errorMessages,
      loading,
      isSaveDisabled,
      tranNotes,
      dropDown
    } = this.state
    const { tranAction, participants, onParticipantsRefresh } = this.props
    if (loading) {
      return <Loader />
    }
    return (
      <section className="accordions">
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onConfirmationSave}
        />
        <article className="accordion is-active">
          <div className="accordion-header toggle">
            <p>Key Transaction Parameters</p>
          </div>

          <div className="accordion-body">
            <div className="accordion-content">
              <div className="columns">
                <SearchCountry
                  idx={0}
                  label="State/City"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.dealIssueTranState || errorMessages.dealIssueTranCounty || ""}
                  value={
                    deals.dealIssueTranCounty && deals.dealIssueTranState ?
                      `${deals.dealIssueTranCounty}, ${deals.dealIssueTranState}` :
                      deals.dealIssueTranState ?
                        `${deals.dealIssueTranState}` :
                        deals.dealIssueTranCounty ?
                          `${deals.dealIssueTranCounty}` : ""
                  }
                  getCountryDetails={this.getCountryDetails}
                />
                <SelectLabelInput
                  label="Transaction Type"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.dealIssueTranSubType || ""}
                  list={dropDown.tranType}
                  name="dealIssueTranSubType"
                  value={deals.dealIssueTranSubType}
                  onChange={this.onChange}
                />
                {/*
                <div className="column is-one-fourth">
                  <p className="multiExpLbl">
                    State<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <div className="control">
                    <div
                      className="select is-small is-link"
                      style={{ width: "100%" }}
                    >
                      <select
                        name="dealIssueTranState"
                        title="State"
                        value={deals.dealIssueTranState || ""}
                        onChange={this.onChange}
                        onBlur={this.onBlurInput}
                        disabled={!tranAction.canEditTran}
                        style={{ width: "100%" }}
                      >
                        <option value="">Pick State</option>
                        {dropDown.stateCountry &&
                          Object.keys(
                            dropDown.stateCountry["United States"]
                          ).map((key, i) => (
                            <option key={i} value={key}>
                              {key}
                            </option>
                          ))}
                      </select>
                      {errorMessages.dealIssueTranState && (
                        <small className="text-error">
                          {errorMessages.dealIssueTranState || ""}
                        </small>
                      )}
                    </div>
                  </div>
                </div>

                <div className="column">
                  <p className="multiExpLbl">County</p>
                  <div
                    className="select is-small is-link"
                    style={{ width: "100%" }}
                  >
                    <select
                      name="dealIssueTranCounty"
                      title="County"
                      value={deals.dealIssueTranCounty || ""}
                      onChange={this.onChange}
                      onBlur={this.onBlurInput}
                      disabled={!tranAction.canEditTran}
                      style={{ width: "100%" }}
                    >
                      <option value="">Pick County</option>
                      {dropDown.stateCountry &&
                      dropDown.stateCountry["United States"] &&
                      dropDown.stateCountry["United States"][
                        deals.dealIssueTranState
                      ]
                        ? dropDown.stateCountry["United States"][
                          deals.dealIssueTranState
                        ].map((country, i) => (
                          <option key={country} value={country}>
                            {country}
                          </option>
                        ))
                        : null}
                    </select>
                    {errorMessages.dealIssueTranCounty && (
                      <small className="text-error">
                        {errorMessages.dealIssueTranCounty || ""}
                      </small>
                    )}
                  </div>
                </div>
                */}
                <div className="column">
                  <p className="multiExpLbl">
                    Primary Sector<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <div className="control">
                    <div
                      className="select is-small is-link"
                      style={{ width: "100%" }}
                    >
                      <select
                        value={deals.dealIssueTranPrimarySector || ""}
                        title="Primary Sector"
                        onChange={this.onChange}
                        name="dealIssueTranPrimarySector"
                        onBlur={this.onBlurInput}
                        disabled={!tranAction.canEditTran}
                        style={{ width: "100%" }}
                      >
                        <option value="" disabled="">
                          Pick Primary Sector
                        </option>
                        {dropDown && dropDown.primarySectors && dropDown.primarySectors.map(t => (
                          <option key={t && t.label} value={t && t.label} disabled={!t.included}>
                            {t && t.label}
                          </option>
                        ))}
                      </select>
                      {errorMessages &&
                        errorMessages.dealIssueTranPrimarySector && (
                        <small className="text-error">
                          {errorMessages.dealIssueTranPrimarySector}
                        </small>
                      )}
                    </div>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Secondary Sector</p>
                  <div
                    className="select is-small is-link"
                    style={{ width: "100%" }}
                  >
                    <select
                      title="Secondary Sector"
                      name="dealIssueTranSecondarySector"
                      value={deals.dealIssueTranSecondarySector}
                      onChange={this.onChange}
                      onBlur={this.onBlurInput}
                      disabled={!tranAction.canEditTran}
                      style={{ width: "100%" }}
                    >
                      <option value="" disabled="">
                        Pick Secondary Sector
                      </option>
                      {deals.dealIssueTranPrimarySector &&
                      dropDown.secondarySectors[deals.dealIssueTranPrimarySector]
                        ? dropDown.secondarySectors[
                          deals.dealIssueTranPrimarySector
                        ].map(sector => (
                          <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                            {sector && sector.label}
                          </option>
                        ))
                        : null}
                    </select>
                    {errorMessages &&
                      errorMessages.dealIssueTranSecondarySector && (
                      <small className="text-error">
                        {errorMessages.dealIssueTranSecondarySector}
                      </small>
                    )}
                  </div>
                </div>
              </div>

              <div className="columns">
                <SelectLabelInput
                  label="Use of Proceeds"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.dealIssueUseOfProceeds || ""}
                  list={dropDown.useOfProceeds}
                  name="dealIssueUseOfProceeds"
                  value={deals.dealIssueUseOfProceeds}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
                <SelectLabelInput
                  label="Offering Type"
                  className="column is-3"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.dealIssueofferingType || ""}
                  list={dropDown.offeringType}
                  name="dealIssueofferingType"
                  value={deals.dealIssueofferingType}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />

                {/*<SelectLabelInput*/}
                  {/*label="Security Type"*/}
                  {/*disabled={!tranAction.canEditTran}*/}
                  {/*error={errorMessages.dealIssueSecurityType || ""}*/}
                  {/*list={dropDown.securityType}*/}
                  {/*name="dealIssueSecurityType"*/}
                  {/*value={deals.dealIssueSecurityType}*/}
                  {/*onChange={this.onChange}*/}
                  {/*onBlur={this.onBlurInput}*/}
                {/*/>*/}
                <SelectLabelInput
                  label="Bank Qualified"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.dealIssueBankQualified || ""}
                  name="dealIssueBankQualified"
                  list={dropDown.bankQualified}
                  value={deals.dealIssueBankQualified}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
                <SelectLabelInput
                  label="Corp Type"
                  disabled={!tranAction.canEditTran}
                  error={errorMessages.dealIssueCorpType || ""}
                  list={dropDown.corpType}
                  name="dealIssueCorpType"
                  value={deals.dealIssueCorpType}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
              </div>

              <div className="columns">
                <NumberInput
                  label="Par Amount"
                  prefix="$"
                  disabled={!tranAction.canEditTran}
                  error={(errorMessages && errorMessages.dealIssueParAmount) ? "Required(must be larger than or equal to 0)" : ""}
                  name="dealIssueParAmount"
                  placeholder="$"
                  value={deals.dealIssueParAmount}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
                <TextLabelInput
                  label="Pricing Date"
                  disabled={!tranAction.canEditTran}
                  error={(errorMessages.dealIssuePricingDate) || ""}
                  name="dealIssuePricingDate"
                  type="date"
                  // value={deals.dealIssuePricingDate ? moment(new Date(deals.dealIssuePricingDate).toISOString().substring(0, 10)).format("YYYY-MM-DD"): ""}
                  value={(deals.dealIssuePricingDate === "" || !deals.dealIssuePricingDate) ? null : new Date(deals.dealIssuePricingDate)}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
                <TextLabelInput
                  label="Expected Award Date"
                  disabled={!tranAction.canEditTran}
                  error={(errorMessages.dealIssueExpAwardDate && "Required (must be larger than or equal to  Pricing Date)") || ""}
                  name="dealIssueExpAwardDate"
                  type="date"
                  // value={deals.dealIssueExpAwardDate ? moment(new Date(deals.dealIssueExpAwardDate).toISOString().substring(0, 10)).format("YYYY-MM-DD"): ""}
                  value={(deals.dealIssueExpAwardDate === "" || !deals.dealIssueExpAwardDate) ? null : new Date(deals.dealIssueExpAwardDate)}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
                <TextLabelInput
                  label="Actual Award Date"
                  disabled={!tranAction.canEditTran}
                  error={(errorMessages.dealIssueActAwardDate && "Required (must be larger than or equal to Exp. Award Date)") || ""}
                  name="dealIssueActAwardDate"
                  type="date"
                  // value={deals.dealIssueActAwardDate ? moment(new Date(deals.dealIssueActAwardDate).toISOString().substring(0, 10)).format("YYYY-MM-DD"): ""}
                  value={(deals.dealIssueActAwardDate === "" || !deals.dealIssueActAwardDate) ? null : new Date(deals.dealIssueActAwardDate)}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
              </div>

              <div className="columns">
                <NumberInput
                  label="SDLC Credit %"
                  className="column is-3"
                  suffix="%"
                  disabled={!tranAction.canEditTran}
                  error={(errorMessages.dealIssueSdlcCreditPerc && "(must be larger than 0% and less than or equal to 100%)") || ""}
                  name="dealIssueSdlcCreditPerc"
                  placeholder="%"
                  value={deals.dealIssueSdlcCreditPerc}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
                <NumberInput
                  label="Estimated Revenue"
                  className="column is-3"
                  prefix="$"
                  disabled={!tranAction.canEditTran}
                  error={(errorMessages && errorMessages.dealIssueEstimatedRev) ? "Required(must be larger than or equal to 0)" : ""}
                  name="dealIssueEstimatedRev"
                  placeholder="$"
                  value={deals.dealIssueEstimatedRev}
                  onChange={this.onChange}
                  onBlur={this.onBlurInput}
                />
              </div>
            </div>
          </div>
        </article>
        <br />
        <Accordion
          multiple
          boxHidden
          activeItem={[0, 1]}
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Other transaction related reporting fields"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <div className="columns">
                      <NumberInput
                        label="Terms Of Issue %"
                        suffix="%"
                        name="termsOfIssue"
                        placeholder="%"
                        disabled={!tranAction.canEditTran}
                        value={deals.termsOfIssue}
                        onChange={this.onChange}
                        onBlur={this.onBlurInput}
                        error={
                          (errorMessages.termsOfIssue &&
                            "(must be larger than 0% and less than or equal to 100%)") ||
                          ""
                        }
                      />
                      <NumberInput
                        label="TIC %"
                        suffix="%"
                        name="tic"
                        placeholder="%"
                        value={deals.tic}
                        disabled={!tranAction.canEditTran}
                        onChange={this.onChange}
                        onBlur={this.onBlurInput}
                        error={
                          (errorMessages.tic &&
                            "(must be larger than 0% and less than or equal to 100%)") ||
                          ""
                        }
                      />
                      <NumberInput
                        label="BBRI %"
                        suffix="%"
                        name="bbri"
                        placeholder="%"
                        value={deals.bbri}
                        disabled={!tranAction.canEditTran}
                        onChange={this.onChange}
                        onBlur={this.onBlurInput}
                        error={
                          (errorMessages.bbri &&
                            "(must be larger than 0% and less than or equal to 100%)") ||
                          ""
                        }
                      />
                      <NumberInput
                        label="NIC %"
                        suffix="%"
                        name="nic"
                        placeholder="%"
                        value={deals.nic}
                        disabled={!tranAction.canEditTran}
                        onChange={this.onChange}
                        onBlur={this.onBlurInput}
                        error={
                          (errorMessages.nic &&
                            "(must be larger than 0% and less than or equal to 100%)") ||
                          ""
                        }
                      />
                    </div>
                    {deals.dealIssueofferingType === "Negotiated" ? (
                      <div>
                        <div className="columns">
                          <b>Gross Spread / Components</b>
                        </div>
                        <div className="columns">
                          <NumberInput
                            label="G/Sp %"
                            suffix="%"
                            name="gsp"
                            placeholder="%"
                            disabled={!tranAction.canEditTran}
                            value={deals.gsp}
                            onChange={this.onChange}
                            onBlur={this.onBlurInput}
                            error={
                              (errorMessages.gsp &&
                                "(must be larger than 0% and less than or equal to 100%)") ||
                              ""
                            }
                          />
                          <NumberInput
                            label="MgtF %"
                            suffix="%"
                            name="mgtf"
                            placeholder="%"
                            value={deals.mgtf}
                            disabled={!tranAction.canEditTran}
                            onChange={this.onChange}
                            onBlur={this.onBlurInput}
                            error={
                              (errorMessages.mgtf &&
                                "(must be larger than 0% and less than or equal to 100%)") ||
                              ""
                            }
                          />
                          <NumberInput
                            label="Exp %"
                            suffix="%"
                            name="exp"
                            placeholder="%"
                            disabled={!tranAction.canEditTran}
                            value={deals.exp}
                            onChange={this.onChange}
                            onBlur={this.onBlurInput}
                            error={
                              (errorMessages.exp &&
                                "must be larger than 0% and less than or equal to 100%)") ||
                              ""
                            }
                          />
                          <NumberInput
                            label="Tkdn %"
                            suffix="%"
                            name="tkdn"
                            placeholder="%"
                            disabled={!tranAction.canEditTran}
                            value={deals.tkdn}
                            onChange={this.onChange}
                            onBlur={this.onBlurInput}
                            error={
                              (errorMessages.tkdn &&
                                "(must be larger than 0% and less than or equal to 100%)") ||
                              ""
                            }
                          />
                        </div>
                        <div className="columns">
                          <NumberInput
                            label="Other %"
                            suffix="%"
                            className="column is-3"
                            name="other"
                            placeholder="%"
                            disabled={!tranAction.canEditTran}
                            value={deals.other}
                            onChange={this.onChange}
                            onBlur={this.onBlurInput}
                            error={
                              (errorMessages.other &&
                                "(must be larger than 0% and less than or equal to 100%)") ||
                              ""
                            }
                          />
                        </div>
                      </div>
                    ) : null}
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
        {tranAction && tranAction.canEditTran ? (
          <div className="columns" style={{ marginTop: "25px" }}>
            <div className="column is-full">
              <div className="field is-grouped-center">
                <div className="control">
                  <button
                    className="button is-link"
                    onClick={this.onSubmit}
                    disabled={isSaveDisabled}
                    title="Save"
                  >
                    Save
                  </button>
                </div>
                <div className="control">
                  <button className="button is-light" onClick={this.onCancel} title="Cancel">
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        { deals.OnBoardingDataMigrationHistorical ? <HistoricalData data={(deals && deals.historicalData) || {}}/> : null}
        <TransactionNotes
          notesList={tranNotes || []}
          canEditTran={tranAction && tranAction.canEditTran || false}
          onSave={this.onNotesSave}
        />
      </section>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(DealDetails)

export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
