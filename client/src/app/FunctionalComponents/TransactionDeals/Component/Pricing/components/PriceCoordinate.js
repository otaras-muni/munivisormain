import React from "react"
import moment from "moment"
import {NumberInput, TextLabelInput, SelectLabelInput} from "../../../../../GlobalComponents/TextViewBox"

const PriceCoordinate = ({price = {}, dropDown, onChangeItem, errors = {}, onBlur, category, canEditTran}) => {

  const onChange = (event) => {
    onChangeItem({
      ...price,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value || null,
    }, category)
  }

  const onBlurInput = (event) => {
    if(event && event.target && event.target.title && event.target.value) {
      onBlur(category, `Pricing Information ${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  return (
    <div>

      <div className="columns">
        <NumberInput prefix="$" disabled={!canEditTran} label="Principal" required error= {errors.dealSeriesPrincipal || ""} name="dealSeriesPrincipal" value={price.dealSeriesPrincipal} placeholder="$" onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Security Type" required disabled={!canEditTran} error= {errors.dealSeriesSecurityType || ""} list={dropDown.securityType} name="dealSeriesSecurityType" value={price.dealSeriesSecurityType} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput disabled={!canEditTran} label="Security" error= {errors.dealSeriesSecurity || ""} name="dealSeriesSecurity" value={price.dealSeriesSecurity || ""} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput disabled={!canEditTran} label="Security Details" error= {errors.dealSeriesSecurityDetails || ""} name="dealSeriesSecurityDetails" value={price.dealSeriesSecurityDetails || ""} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <TextLabelInput label="Dated Date" disabled={!canEditTran} error= {errors.dealSeriesDatedDate || ""} placeholder="$" name="dealSeriesDatedDate" type="date" /* value={(price.dealSeriesDatedDate && moment(new Date(price.dealSeriesDatedDate).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} */ value={(price.dealSeriesDatedDate === "" || !price.dealSeriesDatedDate) ? null : new Date(price.dealSeriesDatedDate)} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="Settlement Date" disabled={!canEditTran} error= {errors.dealSeriesSettlementDate || ""} name="dealSeriesSettlementDate" type="date" /* value={(price.dealSeriesSettlementDate && moment(new Date(price.dealSeriesSettlementDate).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} */ value={(price.dealSeriesSettlementDate === "" || !price.dealSeriesSettlementDate) ? null : new Date(price.dealSeriesSettlementDate)} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="First Coupon" disabled={!canEditTran} error= {errors.dealSeriesFirstCoupon || ""} name="dealSeriesFirstCoupon" type="date" /* value={(price.dealSeriesFirstCoupon && moment(new Date(price.dealSeriesFirstCoupon).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} */ value={(price.dealSeriesFirstCoupon === "" || !price.dealSeriesFirstCoupon) ? null : new Date(price.dealSeriesFirstCoupon)} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="Put Date" disabled={!canEditTran} error= {errors.dealSeriesPutDate || ""} name="dealSeriesPutDate" type="date" /* value={(price.dealSeriesPutDate && moment(new Date(price.dealSeriesPutDate).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} */ value={(price.dealSeriesPutDate === "" || !price.dealSeriesPutDate) ? null : new Date(price.dealSeriesPutDate)} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <TextLabelInput label="Record Date" disabled={!canEditTran} error= {errors.dealSeriesRecordDate || ""}  type="date" name="dealSeriesRecordDate" /* value={(price.dealSeriesRecordDate && moment(new Date(price.dealSeriesRecordDate).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} */ value={(price.dealSeriesRecordDate === "" || !price.dealSeriesRecordDate) ? null : new Date(price.dealSeriesRecordDate)} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Fed Tax" disabled={!canEditTran} error= {errors.dealSeriesFedTax || ""} list={dropDown.fadTax} name="dealSeriesFedTax" value={price.dealSeriesFedTax} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="State Tax" disabled={!canEditTran} error= {errors.dealSeriesStateTax || ""} list={dropDown.stateTax} name="dealSeriesStateTax" value={price.dealSeriesStateTax} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="AMT" disabled={!canEditTran} error= {errors.dealSeriesPricingAMT || ""} list={dropDown.amt} name="dealSeriesPricingAMT" value={price.dealSeriesPricingAMT} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <SelectLabelInput label="Bank Qualified" disabled={!canEditTran} error= {errors.dealSeriesBankQualified || ""} list={dropDown.bankQualified} name="dealSeriesBankQualified" value={price.dealSeriesBankQualified} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Accrue From" disabled={!canEditTran} error= {errors.dealSeriesAccrueFrom || ""} list={dropDown.accrueFrom} name="dealSeriesAccrueFrom" value={price.dealSeriesAccrueFrom} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="From" disabled={!canEditTran} error= {errors.dealSeriesPricingForm || ""} list={dropDown.form} name="dealSeriesPricingForm" value={price.dealSeriesPricingForm} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Coupon Frequency" disabled={!canEditTran}  error= {errors.dealSeriesCouponFrequency || ""} list={dropDown.couponFrequency} name="dealSeriesCouponFrequency" value={price.dealSeriesCouponFrequency} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <SelectLabelInput label="Day Count" disabled={!canEditTran} error= {errors.dealSeriesDayCount || ""} list={dropDown.dayCount} name="dealSeriesDayCount" value={price.dealSeriesDayCount} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Rate Type" disabled={!canEditTran} error= {errors.dealSeriesRateType || ""} list={dropDown.rateType} name="dealSeriesRateType" value={price.dealSeriesRateType} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Call Feature" disabled={!canEditTran} error= {errors.dealSeriesCallFeature || ""} list={dropDown.callFeatures} name="dealSeriesCallFeature" value={price.dealSeriesCallFeature} onChange={onChange} onBlur={onBlurInput}/>
        <TextLabelInput label="Call Date" disabled={!canEditTran} error= {errors.dealSeriesCallDate || ""} name="dealSeriesCallDate" type="date" /* value={(price.dealSeriesCallDate && moment(new Date(price.dealSeriesCallDate).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} */ value={(price.dealSeriesCallDate === "" || !price.dealSeriesCallDate) ? null : new Date(price.dealSeriesCallDate)} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <NumberInput prefix="$" label="Call Price" disabled={!canEditTran} error= {errors.dealSeriesCallPrice || ""} name="dealSeriesCallPrice"  placeholder="$" value={price.dealSeriesCallPrice || ""}  onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Insurance" disabled={!canEditTran} error= {errors.dealSeriesInsurance || ""} list={dropDown.insurance} name="dealSeriesInsurance" value={price.dealSeriesInsurance || ""} onChange={onChange} onBlur={onBlurInput}/>
        <SelectLabelInput label="Underwriter's Inventory" disabled={!canEditTran} error= {errors.dealSeriesUnderwtiersInventory || ""} list={dropDown.underwriterInventory} name="dealSeriesUnderwtiersInventory" value={price.dealSeriesUnderwtiersInventory} onChange={onChange} onBlur={onBlurInput}/>
        <NumberInput label="Minimum Denomination" disabled={!canEditTran} error= {errors.dealSeriesMinDenomination || ""} name="dealSeriesMinDenomination" value={price.dealSeriesMinDenomination} onChange={onChange} onBlur={onBlurInput}/>
      </div>

      <div className="columns">
        <SelectLabelInput label="Syndicate Structure" disabled={!canEditTran} error= {errors.dealSeriesSyndicateStructure || ""} list={dropDown.syndicateStructure} name="dealSeriesSyndicateStructure" value={price.dealSeriesSyndicateStructure} onChange={onChange} onBlur={onBlurInput}/>
        <NumberInput prefix="$" label="Gross Spread" disabled={!canEditTran} error= {errors.dealSeriesGrossSpread || ""} name="dealSeriesGrossSpread" value={price.dealSeriesGrossSpread || ""} placeholder="$/1000" onChange={onChange} onBlur={onBlurInput}/>
        <NumberInput prefix="$" label="Estimated Takedown" disabled={!canEditTran} error= {errors.dealSeriesEstimatedTakeDown || ""} name="dealSeriesEstimatedTakeDown" value={price.dealSeriesEstimatedTakeDown || ""} placeholder="$/1000" onChange={onChange} onBlur={onBlurInput}/>
        <NumberInput prefix="$" label="Insurance Fee" disabled={!canEditTran} error= {errors.dealSeriesInsuranceFee || ""} name="dealSeriesInsuranceFee" value={price.dealSeriesInsuranceFee || ""} placeholder="bps" onChange={onChange} onBlur={onBlurInput}/>
      </div>
    </div>
  )}

export default PriceCoordinate
