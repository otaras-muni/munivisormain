import React from "react"
import swal from "sweetalert"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import { connect } from "react-redux"
import { getPicklistByPicklistName, checkEmptyElObject } from "GlobalUtils/helpers"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import Accordion from "../../../../GlobalComponents/Accordion"
import CONST from "../../../../../globalutilities/consts"
import Series from "./components/Series"
import {
  fetchDealsTransaction,
  putDealsTransaction,
  fetchSeriesDetailsBySeriesId,
  putDealsPricingDetails,
  putSeriesDetails,
  removeDealSeries,
  sendEmailAlert
} from "../../../../StateManagement/actions/Transaction"
import Tabs from "../../../../GlobalComponents/Tabs"
import PriceCoordinate from "./components/PriceCoordinate"
import PricingData from "./components/PricingData"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import { PricingDetailsValidate } from "../../../TransactionDeals/Component/Validation/PricingDeatailsValidation"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import DocExcelReader from "../../../../GlobalComponents/DocExcelReader"

class Pricing extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cols: [
        [
          {name: "Series Code<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>"},
          {name: "Description"},
          {name: "Tag"},
          {name: "Edit/Delete"}],
        [
          { name: "Term" },
          { name: "Maturity Date" },
          { name: "Amount  " },
          { name: "Coupon" },
          { name: "Yield" },
          { name: "Price" },
          { name: "YTM" },
          { name: "CUSIP" },
          { name: "Call Date" },
          { name: "Takedown" },
          { name: "Insured" },
          { name: "Drop" },
          { name: "Action" }
        ]
      ],
      userName: "",
      dealIssueSeriesDetails: [CONST.dealIssueSeriesDetails],
      errorMessages: {},
      seriesPricingDetails: CONST.seriesPricingDetails,
      seriesPricingData: [CONST.seriesPricingData],
      dealSeriesList: [],
      dropDown: {
        securityType: [],
        callFeatures: [],
        fadTax: [],
        stateTax: [],
        amt: [],
        bankQualified: [],
        accrueFrom: [],
        form: [],
        couponFrequency: [],
        dayCount: [],
        rateType: [],
        insurance: [],
        underwriterInventory: [],
        syndicateStructure: []
      },
      activeTab: "",
      loading: true,
      isEditable: "",
      isSaveDisabled: false,
      isSavePricingDisabled: false,
      activeItem: [],
      modalState: false,
      createdDate: "",
      email: {
        category: "",
        message: "",
        subject: ""
      },
      confirmAlert: CONST.confirmAlert,
      seriesloading: true,
      jsonSheet: [
        {
          term: 32,
          maturityDate: "2018-10-17",
          amount: 25,
          coupon: 0.02,
          yield: 0.03,
          price: 3,
          YTM: 0.03,
          cusip: "3DFSF434",
          callDate: "2018-10-11",
          takeDown: 32,
          insured: false,
          drop: false
        },
        {
          term: 15,
          maturityDate: "2018-10-18",
          amount: 23,
          coupon: 0.02,
          yield: 0.03,
          price: 3,
          YTM: 0.03,
          cusip: "3DFSF435",
          callDate: "2018-10-12",
          takeDown: 32,
          insured: false,
          drop: false
        }
      ]
    }
  }

  componentWillMount() {
    const { transaction, nav2 } = this.props
    if (transaction && transaction._id) {
      if (transaction && transaction.dealIssueSeriesDetails) {
        this.setState(
          {
            dealIssueSeriesDetails: transaction.dealIssueSeriesDetails,
            dealSeriesList: transaction.dealIssueSeriesDetails,
            activeTab:
              (transaction.dealIssueSeriesDetails &&
                transaction.dealIssueSeriesDetails.length &&
                transaction.dealIssueSeriesDetails[0]._id) ||
              "",
            createdDate: transaction.created_at,
            email: {
              ...this.state.email,
              subject: `Transaction - ${transaction.dealIssueTranIssueName ||
                transaction.dealIssueTranProjectDescription} - Notification`
            }
          },
          () => {
            this.getActiveSeriesDetails()
          }
        )
      } else {
        this.setState({
          isEditable: 0,
          loading: false
        })
      }
    }
  }

  getDealDetails = dealId => {
    fetchDealsTransaction("pricing", dealId, res => {
      if (res && res.dealIssueSeriesDetails) {
        this.setState(
          {
            dealIssueSeriesDetails: res.dealIssueSeriesDetails || [],
            dealSeriesList: res.dealIssueSeriesDetails || [],
            activeTab:
              (res.dealIssueSeriesDetails &&
                res.dealIssueSeriesDetails.length &&
                res.dealIssueSeriesDetails[0]._id) ||
              "",
            isSaveDisabled: false
          },
          () => {
            this.getActiveSeriesDetails()
          }
        )
      } else {
        this.setState({
          isEditable: 0,
          loading: false
        })
      }
    })
  }

  async componentDidMount() {
    let result = await getPicklistByPicklistName([
      "LKUPTOBECONFIRMED",
      "LKUPCALLFEATURE",
      "LKUPFEDTAX",
      "LKUPSTATETAX",
      "LKUPAMT",
      "LKUPBANKQUALIFIED",
      "LKUPACCRUEFROM",
      "LKUPFORM",
      "LKUPCOUPONFREQUENCY",
      "LKUPDAYCOUNT",
      "LKUPRATETYPE",
      "LKUPSECTYPEGENERAL",
      "LKUPINSURANCE",
      "LKUPYESNO",
      "LKUPSYNDSTRUCT"
    ])
    result = (result.length && result[1]) || {}

    this.setState({
      dropDown: {
        ...this.state.dropDown,
        securityType: result.LKUPSECTYPEGENERAL,
        callFeatures: result.LKUPCALLFEATURE,
        fadTax: result.LKUPFEDTAX,
        stateTax: result.LKUPSTATETAX,
        amt: result.LKUPAMT,
        bankQualified: result.LKUPBANKQUALIFIED,
        accrueFrom: result.LKUPACCRUEFROM,
        form: result.LKUPFORM,
        couponFrequency: result.LKUPCOUPONFREQUENCY,
        dayCount: result.LKUPDAYCOUNT,
        rateType: result.LKUPRATETYPE,
        insurance: result.LKUPINSURANCE,
        underwriterInventory: result.LKUPYESNO,
        syndicateStructure: result.LKUPSYNDSTRUCT
      },
      userName: (this.props.user && this.props.user.userFirstName) || "",
      loading: false
    })
  }

  onChangeItem = (item, category, index) => {
    let items = this.state[category]
    if (category === "seriesPricingDetails") {
      items = item
    } else {
      items[index] = item
    }

    this.setState({
      [category]: items
    }, () => {
      if(category === "seriesPricingData"){
        this.principalTotal()
      }
    })
  }

  onReset = key => {
    const auditLogs = this.props.auditLogs.filter(x => x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      [key]: [...this.state.tempSeriesPricingData],
      isEditable: "",
      errorMessages: {}
    })
  }

  onAdd = key => {
    const { userName, isEditable } = this.state
    const insertRow = this.state[key]

    if (isEditable) {
      swal("Warning", "First save the current series", "warning")
      return
    }

    insertRow.unshift(CONST[key])
    this.props.addAuditLog({userName, log: "Deal series Add new Item", date: new Date(), key})
    this.setState({
      [key]: insertRow,
      isEditable: insertRow.length ? 0 : 0
    },() => {
      if(key === "seriesPricingData" || key === "dealIssueSeriesDetails"){
        this.setState({
          activeItem: [0,1]
        })
      }
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({
      userName,
      log: `Deal series ${change}`,
      date: new Date(),
      key: category
    })
  }

  actionButtons = (key, isDisabled) => {
    const canEditTran =
      (this.props.tranAction && this.props.tranAction.canEditTran) || false
    if (!canEditTran) return
    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key)}
            disabled={isDisabled || false}
          >
            Add
          </button>
        </div>
        {key === "dealIssueSeriesDetails" ? null : (
          <div className="control">
            <button
              className="button is-light is-small"
              onClick={() => this.onReset(key)}
              disabled={isDisabled || false}
            >
              Reset
            </button>
          </div>
        )}
      </div>
    )
  }
  onSeriesSave = index => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    console.log("==============email send to ==============", emailPayload)
    const series = this.state.dealIssueSeriesDetails[index]
    let isValid = false
    if (this.state.dealIssueSeriesDetails.length) {
      isValid = Object.keys(series).some(
        key => typeof series[key] === "string" && !series[key]
      )
    }

    if (!series.seriesName) {
      this.setState({
        errorMessages: {
          series: "Series Code is Required."
        }
      })
      return
    }

    const dealSeries = this.state.dealIssueSeriesDetails.map((item) => item.seriesName)

    const isDuplicate = dealSeries.some((item, idx) => dealSeries.indexOf(item) !== idx)

    if (isDuplicate) {
      this.setState({
        errorMessages: {
          series: "Series Code must be unique."
        }
      })
      return
    }

    if (series && series._id) {
      this.setState(
        {
          isSaveDisabled: true,
          loading: true
        },
        () => {
          putSeriesDetails(this.props.nav2, series, res => {
            if (res && res.status === 200) {
              this.props.submitAuditLogs(this.props.nav2)
              toast("Deal series has been updated!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.setState(
                {
                  errorMessages: {},
                  isEditable: "",
                  loading: false
                },
                async () => {
                  this.getDealDetails(this.props.nav2)
                }
              )
            } else {
              toast("Something went wrong!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false,
                loading: false
              })
            }
          })
        }
      )
    } else {
      const dealSeriesDetails = {
        _id: this.props.nav2,
        dealIssueSeriesDetails: [series]
      }
      this.setState(
        {
          isSaveDisabled: true,
          loading: true
        },
        () => {
          putDealsTransaction("series", dealSeriesDetails, res => {
            if (res && res.status === 200) {
              toast("Deal series has been added!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  errorMessages: {},
                  isEditable: "",
                  isSaveDisabled: false,
                  loading: false
                },
                () => {
                  this.getDealDetails(this.props.nav2)
                }
              )
            } else {
              toast("Something went wrong!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false,
                loading: false
              })
            }
          })
        }
      )
    }
  }

  getActiveSeriesDetails = () => {
    const { activeTab } = this.state
    const { transaction } = this.props
    if (!activeTab) return
    fetchSeriesDetailsBySeriesId(this.props.nav2, activeTab, res => {
      if (res) {
        const seriesPricing = res.dealIssueSeriesDetails && res.dealIssueSeriesDetails.seriesPricingData
          && Array.isArray(res.dealIssueSeriesDetails.seriesPricingData)
          && res.dealIssueSeriesDetails.seriesPricingData.map(item => ({
            ...item,
            coupon: item.coupon * 100,
            yield: item.yield * 100,
            YTM: item.YTM * 100
          }))
        if(transaction && transaction.dealIssueSecurityType){
          CONST.seriesPricingDetails.dealSeriesSecurityType = transaction.dealIssueSecurityType
        }
        this.setState({
          loading: false,
          seriesloading: false,
          seriesPricingDetails: res.dealIssueSeriesDetails ? res.dealIssueSeriesDetails.seriesPricingDetails ? res.dealIssueSeriesDetails.seriesPricingDetails : CONST.seriesPricingDetails : CONST.seriesPricingDetails,
          seriesPricingData: res.dealIssueSeriesDetails
            ? seriesPricing && seriesPricing.length
              ? seriesPricing
              : [CONST.seriesPricingData]
            : [CONST.seriesPricingData],
          tempSeriesPricingData: res.dealIssueSeriesDetails
            ? res.dealIssueSeriesDetails.seriesPricingData &&
            res.dealIssueSeriesDetails.seriesPricingData.length
              ? [...res.dealIssueSeriesDetails.seriesPricingData]
              : [CONST.seriesPricingData]
            : [CONST.seriesPricingData]
        })
      }
    })
  }

  onTab = activeTab => {
    this.setState(
      {
        activeTab,
        seriesloading: true,
        loading: true,
        errorMessages: {},
      }, () => {
        this.getActiveSeriesDetails()
      }
    )
  }

  onEditSeries = index => {
    if (this.state.isEditable) {
      swal("Warning", "First save the current series", "warning")
      return
    }
    this.setState({
      isEditable: index
    })
  }

  onRemove = (seriesId, index, seriesName) => {
    const { confirmAlert } = this.state
    confirmAlert.text = "You want to delete this Series?"
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        if (seriesId) {
          this.props.addAuditLog({
            userName: this.state.userName,
            log: `${seriesName} series was removed`,
            date: new Date(),
            key: "dealIssueSeriesDetails"
          })
          this.setState(
            {
              isSaveDisabled: true
            },
            () => {
              removeDealSeries(this.props.nav2, seriesId, res => {
                if (res && res.status === 200) {
                  toast("Deal series was removed!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.SUCCESS
                  })
                  this.props.submitAuditLogs(this.props.nav2)
                  this.setState(
                    {
                      isEditable: "",
                      errorMessages: {},
                      isSaveDisabled: false
                    },
                    () => {
                      this.getDealDetails(this.props.nav2)
                    }
                  )
                } else {
                  toast("Something went wrong!", {
                    autoClose: CONST.ToastTimeout,
                    type: toast.TYPE.ERROR
                  })
                  this.setState({
                    isSaveDisabled: false
                  })
                }
              })
            }
          )
        } else {
          const { dealIssueSeriesDetails } = this.state
          dealIssueSeriesDetails.splice(index, 1)
          this.setState({
            dealIssueSeriesDetails,
            isEditable: "",
            errorMessages: {}
          })
        }
      }
    })
  }

  onSave = () => {
    const {
      activeTab,
      seriesPricingDetails,
      seriesPricingData,
      createdDate
    } = this.state
    let isValid = false
    const dealIssueSeriesDetails = {
      seriesPricingDetails
    }

    const pricingData = []
    if (seriesPricingData && Object.keys(seriesPricingData).length) {
      seriesPricingData.forEach(series => {
        const data = {
          ...series,
          coupon: series.coupon === "" ? series.coupon : series.coupon / 100,
          yield: series.yield === "" ? series.yield : series.yield / 100,
          YTM: series.YTM === "" ? series.YTM : series.YTM / 100,
        }
        isValid = Object.keys(series).some(key => series[key])
        if (isValid) {
          pricingData.push(data)
        }
      })
    }
    if (pricingData.length) {
      dealIssueSeriesDetails.seriesPricingData = pricingData
    }
    const errors = PricingDetailsValidate(dealIssueSeriesDetails, createdDate)
    if (errors && errors.error) {
      const errorMessages = {}
      console.log("=======>", errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) {
          //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) {
            //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] =  err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] = {
            [err.path[1]]: {
              [err.path[2]]: err.message
            }
          }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: err.message
          }
        }
      })

      if(errorMessages) {
        const seriesPricingDetails = errorMessages && errorMessages.seriesPricingDetails
        const toastMessage = seriesPricingDetails && seriesPricingDetails.dealSeriesPrincipal && seriesPricingDetails && seriesPricingDetails.dealSeriesSecurityType ?
          "Principal & Security Type cannot be blank in Pricing Information" :
          seriesPricingDetails && seriesPricingDetails.dealSeriesPrincipal ? "Principal cannot be blank in Pricing Information" :
            seriesPricingDetails && seriesPricingDetails.dealSeriesSecurityType ? "Security Type cannot be blank in Pricing Information" : errorMessages && errorMessages.seriesPricingData ? Object.values(errorMessages.seriesPricingData).length ? "Some Field Required" : "" : ""
        toast(toastMessage, { autoClose: 4000, type: toast.TYPE.WARNING })
      }
      this.setState({ errorMessages, activeItem: [0, 1] })
      return
    }

    this.setState(
      {
        isSavePricingDisabled: true,
        loading: true,
      },
      () => {
        putDealsPricingDetails(
          this.props.nav2,
          activeTab,
          dealIssueSeriesDetails,
          res => {
            if (res && res.status === 200) {
              toast("Deal pricing has been updated!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  errorMessages: {},
                  isSavePricingDisabled: false,
                  createdDate: "",
                  activeItem: [],
                  loading: false,
                }
              )
            } else {
              toast("Something went wrong!", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSavePricingDisabled: false,
                activeItem: [],
                loading: false,
              })
            }
          }
        )
      }
    )
  }

  handelToggle = () =>{
    this.setState({ modalState: true })
  }

  onModalSave = () => {
    const { email } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }
    this.setState({
      modalState: false
    },async ()=>{
      await sendEmailAlert(emailPayload)
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onExcelRead = docSeries => {
    let { seriesPricingData } = this.state
    const pricingData = []
    if(docSeries && Array.isArray(docSeries)){

      const booleanValues = ["insured", "drop"]
      docSeries.forEach(series => {
        booleanValues.forEach(key => {
          if(typeof series[key] !== "boolean"){
            series[key] = false
          }
        })
      })

      docSeries = docSeries.map(item => ({
        ...item,
        coupon: item.coupon * 100,
        yield: item.yield * 100,
        YTM: item.YTM * 100
      }))

      seriesPricingData = seriesPricingData.concat(docSeries)
      seriesPricingData.forEach(item => {
        if (!checkEmptyElObject(item)) {
          pricingData.push(item)
        }
      })
      this.setState({
        seriesPricingData: pricingData,
        errorMsg: ""
      }, () => this.principalTotal())
    }else {
      this.setState({
        errorMsg: docSeries
      })
    }
  }

  principalTotal = () => {
    const { seriesPricingData, seriesPricingDetails } = this.state
    if(seriesPricingData && seriesPricingData.length){
      let number = 0
      seriesPricingData.forEach(s => {
        number += parseFloat(s.amount || 0)
      })
      seriesPricingDetails.dealSeriesPrincipal = number
      this.setState({
        seriesPricingDetails
      })
    } else {
      seriesPricingDetails.dealSeriesPrincipal = ""
      this.setState({
        seriesPricingDetails
      })
    }
  }

  onRemovePricingData = index => {
    const seriesPricingData = cloneDeep(this.state.seriesPricingData)
    seriesPricingData.splice(index, 1)
    this.setState({
      seriesPricingData,
      isEditable: "",
      errorMessages: {}
    }, () => this.principalTotal())
  }

  render() {
    const {
      seriesloading,
      jsonSheet,
      modalState,
      email,
      loading,
      cols,
      dealIssueSeriesDetails,
      errorMessages,
      dropDown,
      activeItem,
      dealSeriesList,
      isSaveDisabled,
      isSavePricingDisabled,
      activeTab,
      seriesPricingDetails,
      seriesPricingData,
      isEditable,
      errorMsg
    } = this.state
    const { tranAction, participants, onParticipantsRefresh } = this.props
    const canEditTran =
      (this.props.tranAction && this.props.tranAction.canEditTran) || false
    let dealSeriesTabs = dealSeriesList.filter(series => series._id)
    dealSeriesTabs = dealSeriesTabs.map(series => ({
      title: <span>{series.seriesName}</span>,
      value: series._id
    }))

    if (loading) {
      return <Loader />
    }

    return (
      <div className="pricing">
        {tranAction && tranAction.canEditTran ? (
          <button
            className="button is-small is-link"
            onClick={this.handelToggle}
          >
            Send Email Alert
          </button>
        ) : null}
        <SendEmailModal
          modalState={modalState}
          email={email}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onModalSave}
        />
        <Accordion
          multiple
          boxHidden
          isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [0]}
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Add Series"
                actionButtons={this.actionButtons(
                  "dealIssueSeriesDetails",
                  isSaveDisabled
                )}
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(0) && (
                  <div className="overflow-auto">
                    <table
                      className="table is-bordered is-striped is-hoverable is-fullwidth"
                      style={{ marginBottom: 0 }}
                    >
                      <TableHeader cols={cols[0]} />
                      {dealIssueSeriesDetails.map((series, index) => {
                        const errors =
                          errorMessages.dealIssueSeriesDetails &&
                          errorMessages.dealIssueSeriesDetails[index.toString()]
                        return (
                          <Series
                            key={index.toString()}
                            index={index}
                            series={series}
                            errors={errors}
                            isEditable={isEditable}
                            onBlur={this.onBlur}
                            category="dealIssueSeriesDetails"
                            canEditTran={canEditTran}
                            onChangeItem={this.onChangeItem}
                            onRemove={this.onRemove}
                            onEditSeries={this.onEditSeries}
                            onSeriesSave={this.onSeriesSave}
                            isSaveDisabled={isSaveDisabled}
                          />
                        )
                      })}
                    </table>
                    <small className="text-error">
                      {errorMessages.series || ""}
                    </small>
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
        {dealSeriesTabs.length ? (
          <Tabs
            activeTab={activeTab}
            tabs={dealSeriesTabs}
            type="deal"
            onTab={this.onTab}
          />
        ) : null}
        {dealSeriesTabs.length && !seriesloading ? (
          <Accordion
            multiple
            boxHidden
            isRequired={!!activeItem.length}
            activeItem={activeItem.length ? activeItem : [0]}
            render={({ activeAccordions, onAccordion }) => (
              <div>
                <RatingSection
                  onAccordion={() => onAccordion(0)}
                  title="Pricing Information"
                >
                  {activeAccordions.includes(0) && (
                    <PriceCoordinate
                      dropDown={dropDown}
                      category="seriesPricingDetails"
                      errors={
                        (errorMessages && errorMessages.seriesPricingDetails) ||
                        {}
                      }
                      price={seriesPricingDetails}
                      onChangeItem={this.onChangeItem}
                      onBlur={this.onBlur}
                      canEditTran={canEditTran}
                    />
                  )}
                </RatingSection>
                <RatingSection
                  onAccordion={() => onAccordion(1)}
                  title="Pricing Data"
                  actionButtons={this.actionButtons(
                    "seriesPricingData",
                    isSavePricingDisabled
                  )}
                >
                  {activeAccordions.includes(1) && (
                    <div>
                      <DocExcelReader
                        onExcelRead={this.onExcelRead}
                        jsonSheet={jsonSheet}
                        notShowSample={true}
                      />
                      <a
                        className="is-size-7"
                        href={"https://s3.amazonaws.com/munivisor-platform-static-documents/Pricing+Sample.xlsx"}>
                        Sample excel
                      </a>
                      <small className="text-error">
                        {errorMsg}
                      </small>
                      {/* <table
                        className="table is-bordered is-striped is-hoverable is-fullwidth"
                        style={{ marginBottom: 0 }}
                      >
                        <TableHeader cols={cols[1]} /> */}
                      <div className="margin-topTen">
                        {seriesPricingData.map((priceData, index) => {
                        const errors = errorMessages.seriesPricingData && errorMessages.seriesPricingData[index.toString()]
                          return (
                            <PricingData
                              key={index.toString()}
                              category="seriesPricingData"
                              index={index}
                              length={seriesPricingData && seriesPricingData.length}
                              errors={errors}
                              priceData={priceData}
                              onChangeItem={this.onChangeItem}
                              onBlur={this.onBlur}
                              canEditTran={canEditTran}
                              onRemove={this.onRemovePricingData}
                            />
                        )
                        })}
                      </div>
                      {/* </table> */}
                    </div>
                  )}
                </RatingSection>
                {canEditTran ? (
                  <div className="columns" style={{ marginTop: 10 }}>
                    <div className="column is-full">
                      <div className="field is-grouped-center">
                        <div className="control">
                          <button
                            className="button is-link"
                            onClick={this.onSave}
                            disabled={isSavePricingDisabled}
                            title="Save"
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            )}
          />
        ) : null}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(Pricing)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
