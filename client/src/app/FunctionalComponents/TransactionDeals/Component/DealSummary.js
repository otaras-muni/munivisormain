import React from "react"

const DealSummary = props => {
  const { deal = {} } = props
  // let dropdownClass = "dropdown tag is-small is-link"
  let dropdownElement
  const toggleDropdown = () => {
    dropdownElement.classList.toggle("is-active")
  }

  return (
    <div className="column is-vcentered">
      <div className="tags has-addons">
        <span className="tag is-success">{deal.tranStatus}</span>
        <span className="tag is-dark">{deal.tranName}</span>
        <span className="dropdown tag is-small is-link" onClick={toggleDropdown}
          ref={el => {dropdownElement = el}} aria-haspopup="true" aria-controls="dropdown-menu1">
          <div className="dropdown-trigger">Summary
            <i className="fas fa-angle-down" aria-hidden="true" />
          </div>
          <div className="dropdown-menu" id="dropdown-menu1" role="menu">
            <div className="dropdown-content">
              <div className="dropdown-item">
                <div className="columns">
                  <div className="column">
                    <label className="dashExpLbl">Issuer Name</label>
                    <p className="dashExpLblVal">{deal.tranIssuerId}
                    </p>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <label className="dashExpLbl">Transaction Name</label>
                    <p className="dashExpLblVal">{deal.tranName}</p>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <label className="dashExpLbl">Date Hired</label>
                    <p className="dashExpLblVal">{deal.tranDateHired}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl">Start Date</label>
                    <p className="dashExpLblVal">{deal.tranStartDate}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl">End Date</label>
                    <p className="dashExpLblVal">{deal.tranExpectedEndDate}</p>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <label className="dashExpLbl">Transaction Type</label>
                    <p className="dashExpLblVal">{deal.tranType}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl"> Purpose of Request </label>
                    <p className="dashExpLblVal">{deal.tranPurposeOfRequest}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl"> Assigned to </label>
                    <p className="dashExpLblVal">{deal.tranAssignedTo && deal.tranAssignedTo[0]}</p>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <label className="dashExpLbl">State</label>
                    <p className="dashExpLblVal">{deal.tranState}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl">County</label>
                    <p className="dashExpLblVal">{deal.tranCounty}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl">Primary Sector</label>
                    <p className="dashExpLblVal">{deal.tranPrimarySector}</p>
                  </div>
                  <div className="column">
                    <label className="dashExpLbl">Secondary Sector</label>
                    <p className="dashExpLblVal">{deal.tranSecondarySector}</p>
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-full">
                    <label className="dashExpLbl">Notes / Instructions</label>
                    <p className="dashExpLblVal">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </span>
        <span className="tag">
          <a href="#">
            <span className="has-text-link" title="Link related transactions">
              <i className="fas fa-2x fa-link" />
            </span>
          </a>
        </span>
      </div>
    </div>
  )
}

export default DealSummary
