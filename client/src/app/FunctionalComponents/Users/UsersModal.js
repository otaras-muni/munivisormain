import React, { Component } from "react"
import { toast } from "react-toastify"
import { Multiselect, Combobox } from "react-widgets"
import MaskedInput from "react-text-mask"
// import moment from "moment"

import { Modal } from "Global/BulmaModal"
import Loader from "Global/Loader"
import { crmSchema, validateValue } from "Validation/common"

import {
  getPicklistValues, getEntityUserDetails, saveEntityUserDetails,
  sendUserOnboardingEmail, getAllRelatedEntites, getEntityDetails,
  getCalendarDateAsString, checkNavAccessEntitlrment} from "GlobalUtils/helpers"

import FirmAddressListForm from "../EntityManagement/CommonComponents/FirmAddressListForm"

const defaultValues = {
  isMuniVisorClient: false
}

const toastTimeout = 1000

const accessTypes = {
  Firm: ["global-edit", "global-readonly", "tran-edit", "tran-readonly",
    "global-readonly-tran-edit"],
  Client: ["tran-edit", "tran-readonly"],
  Prospect: ["tran-edit", "tran-readonly"],
  "Third Party": ["tran-edit", "tran-readonly"]
}

const getInitialAddress = () => (
  [
    {
      addressName: "",
      isPrimary: false,
      isHeadQuarter: false,
      /* isActive: true, */
      website: "",
      officePhone: [
        {
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }
      ],
      officeFax: [
        {
          faxNumber: ""
        }
      ],
      officeEmails: [
        {
          emailId: ""
        }
      ],
      addressLine1: "",
      addressLine2: "",
      country: "",
      state: "",
      city: "",
      zipCode: {
        zip1: "",
        zip2: ""
      },
      formatted_address: "",
      url: "",
      location: {
        longitude: "",
        latitude: ""
      }
    }
  ]
)

const getInitialState = () => ({
  expanded: {
    userBasicSection: true,
    otherDetailsSection: false
  },
  viewOnly: true,
  entity: {},
  currentUser: {},
  userId: "",
  userEmailId: "",
  userStatus: "active",
  userFlags: [],
  userEmails: [],
  userPhone: [],
  userFax: [],
  userEmployeeID: "",
  userJobTitle: "",
  userManagerEmail: "",
  userJoiningDate: null,
  userExitDate: null,
  userCostCenter: "",
  userEntitlement: "",
  userFirstName: "",
  userMiddleName: "",
  userLastName: "",
  userAddresses: getInitialAddress(),
  modalState: false,
  modalIndex: -1,
  modalType: "",
  error: { addresses: getInitialAddress() },
  generalError: "",
  updateData: {}
})

class UsersModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      countryResult: [], userFlagsList: [], relatedEntites: [], entityAddress: "",
      ...getInitialState()
    }
    this.saveCallback = this.saveCallback.bind(this)
    this.getEntityDetailsCallback = this.getEntityDetailsCallback.bind(this)
    this.getUserData = this.getUserData.bind(this)
    this.getUserDetailsCallback = this.getUserDetailsCallback.bind(this)
    this.savePrimaryDetails = this.savePrimaryDetails.bind(this)
    this.saveOtherDetails = this.saveOtherDetails.bind(this)
    this.changeUserStatus = this.changeUserStatus.bind(this)
    this.changeEntity = this.changeEntity.bind(this)
    this.entityGroupHeading = this.entityGroupHeading.bind(this)
    this.changeField = this.changeField.bind(this)
    this.changeContactTypes = this.changeContactTypes.bind(this)
    this.addOtherEmail = this.addOtherEmail.bind(this)
    this.resetOtherEmail = this.resetOtherEmail.bind(this)
    this.addPhone = this.addPhone.bind(this)
    this.resetPhone = this.resetPhone.bind(this)
    this.addFax = this.addFax.bind(this)
    this.resetFax = this.resetFax.bind(this)
    this.onSaveAddress = this.onSaveAddress.bind(this)
    this.saveAddressChanges = this.saveAddressChanges.bind(this)
  }

  async componentDidMount() {
    const picklistPromise = getPicklistValues(["LKUPCOUNTRY", "LKUPUSERFLAG"])
    const { entityId, nav2 } = this.props

    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit

    const relatedEntitesResult = getAllRelatedEntites()
    const result = await picklistPromise
    const relatedEntitesList = (await relatedEntitesResult) || []
    const relatedEntites = relatedEntitesList.map(e => ({
      entityId: e.relatedEntityId,
      entityName: e.firmName,
      entityType: e.relationshipType
    }))
    let entity = {}
    if (entityId) {
      const [entityName, entityType] = this.getEntityNameAndType(relatedEntites, entityId)
      entity = { entityId, entityName, entityType }
    }
    console.log("result : ", result)
    const countryResult = result[0] || []
    const userFlagsList = result[1] && result[1][1] ? result[1][1] : []
    this.setState({
      entity, countryResult, userFlagsList, userId: nav2 || "",
      relatedEntites, viewOnly, waiting: false
    }, this.getUserData)
  }

  async componentWillReceiveProps(nextProps) {
    const { nav2 } = nextProps
    if (nav2 !== this.props.nav2) {
      const action = await checkNavAccessEntitlrment(nav2)
      const viewOnly = action && nav2 && !action.edit
      this.setState({ userId: nav2 || "", viewOnly }, this.getUserData)
    }
  }

  onSaveAddress(userAddresses) {
    console.log("addressList : ", userAddresses)
    this.setState({ userAddresses }, this.saveAddressChanges)
  }

  getEntityNameAndType(entities = [], entityId) {
    const idx = entities.findIndex(e => (e.entityId === entityId))
    if (idx > -1) {
      const { entityName, entityType } = entities[idx]
      return [entityName, entityType]
    }
    return []
  }

  getAccessLevelList(entity) {
    entity = entity || this.state.entity
    const { entityType } = entity || {}
    console.log("entity, entityType : ", entity, entityType)
    return (accessTypes[entityType] || [])
  }

  getEntityDetailsCallback(err, res) {
    if(err) {
      this.setState({ waiting: false, generalError: "Error in getting entity data"})
    } else {
      console.log("res : ", res)
      const { addresses } = res[0] || {}
      if(addresses && addresses.length) {
        const entityAddress = addresses.filter(a => a.isPrimary)[0] || addresses[0]
        this.setState({ entityAddress })
      }
    }
  }

  getUserDetailsCallback(err, res) {
    if (err) {
      this.setState({ waiting: false, generalError: "Error in getting user data" })
    } else {
      console.log("res : ", res)
      const currentUser = res || {}
      const { relatedEntites, entityAddress } = this.state
      const { userFirstName, userMiddleName, userLastName, userEmails, userStatus,
        userEntitlement, userFlags, userPhone, userFax, userAddresses,
        userJobTitle, userManagerEmail, userJoiningDate, userEmployeeID,
        userExitDate, userCostCenter } = currentUser

      const userEmailId = userEmails && userEmails[0] && userEmails[0].emailId
      const entityId = (currentUser && currentUser.entityId) || ""
      let entity
      if (entityId) {
        if(!entityAddress) {
          getEntityDetails(entityId, this.getEntityDetailsCallback)
        }
        const [entityName, entityType] = this.getEntityNameAndType(relatedEntites, currentUser.entityId)
        entity = { entityId, entityName, entityType }
      }

      const validUserEntitlements = this.getAccessLevelList(entity)

      console.log("validUserEntitlements : ", validUserEntitlements)

      this.setState({
        waiting: false,
        generalError: "",
        currentUser,
        entity,
        userStatus: (userStatus === "inactive" ? "inactive" : "active"),
        userEmailId,
        userFirstName,
        userMiddleName,
        userLastName,
        userEmails,
        userPhone,
        userFax,
        userJobTitle,
        userManagerEmail,
        userJoiningDate,
        userEmployeeID,
        userExitDate,
        userCostCenter,
        userEntitlement: (validUserEntitlements.includes(userEntitlement) ?
          userEntitlement : ""),
        userFlags,
        userAddresses,
        error: { addresses: getInitialAddress() }
      })
    }
  }

  getUserData() {
    const { userId } = this.state
    if (userId) {
      this.setState({ waiting: true })
      getEntityUserDetails(userId, this.getUserDetailsCallback)
    }
  }


  toggleExpand(val) {
    console.log("toggle : ", val)
    // e.preventDefault()
    this.setState(prevState => {
      const expanded = { ...prevState.expanded }
      Object.keys(expanded).forEach(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
      return { expanded }
    })
  }

  toggleModal(modalType, modalIndex) {
    this.setState(prev => {
      const modalState = !prev.modalState
      return { modalState, modalType, modalIndex }
    })
  }

  checkNonEmptyErrorKey(error, keys) {
    if (!error) {
      return false
    }
    if (Object.getPrototypeOf(error).constructor.name === "Object") {
      keys = keys || Object.keys(error).filter(k => k !== "userAddresses")
      if (!keys.length) {
        return false
      }
      let nonEmptyError = false
      keys.some(k => {
        if (this.checkNonEmptyErrorKey(error[k])) {
          nonEmptyError = true
          return true
        }
      })
      return nonEmptyError
    }
    return true
  }

  checkEmptyArray(arr) {
    let result = -1
    arr.some((e, i) => {
      if(!e) {
        result = i
        return true
      }
    })
    return result
  }

  checkAnyError(keys) {
    const { error } = this.state
    return this.checkNonEmptyErrorKey(error, keys)
  }

  changeUserStatus() {
    this.setState(prevState => {
      let userStatus = "inactive"
      const prevStatus = prevState.userStatus
      if (prevStatus === "inactive") {
        userStatus = "active"
      }
      console.log("userStatus : ", userStatus)
      return { userStatus }
    })
  }

  entityGroupHeading({ item }) {
    return (
      <span className="title innerContactTitle has-text-info">{item}</span>
    )
  }

  changeEntity(entity) {
    console.log("entity: ", entity)
    this.setState({ entity })
  }

  changeField(e) {
    const { name, value } = e.target
    this.setState(prevState => {
      const error = { ...prevState.error }
      error[name] = ""
      let validationResult
      if(value && name === "userEmailId") {
        validationResult = validateValue(crmSchema, "emailId", value)
        error.userEmailId = (validationResult && validationResult.error) ?
          "Please enter a valid email" : ""
      }
      if(value && name === "userManagerEmail") {
        validationResult = validateValue(crmSchema, "emailId", value)
        error.userManagerEmail = (validationResult && validationResult.error) ?
          "Please enter a valid email" : ""
      }
      if (name === "userExitDate") {
        const { userJoiningDate } = prevState
        console.log("value1 : ", value)
        if (
          userJoiningDate &&
          value &&
          new Date(userJoiningDate) > new Date(value)
        ) {
          console.log("1")
          error.userExitDate =
            "Exit date cannot be before the joining date"
        } else {
          console.log("2")
          error.userExitDate = ""
        }
      }
      if (name === "userJoiningDate") {
        const { userExitDate } = prevState
        console.log("value2 : ", value)
        if (
          userExitDate &&
          value &&
          new Date(userExitDate) < new Date(value)
        ) {
          console.log("3")
          error.userJoiningDate =
            "Joining date cannot be after the exit date"
        } else {
          console.log("4")
          error.userJoiningDate = ""
        }
      }
      return { [name]: value, error }
    })
  }

  changeContactTypes(userFlags) {
    this.setState({ userFlags })
  }

  changeOtherEmail(i, e) {
    const { value } = e.target
    let validationResult
    if(value) {
      validationResult = validateValue(crmSchema, "emailId", value)
    }
    // console.log("validationResult : ", validationResult)
    const j = i + 1
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userEmails = { ...error.userEmails }
      error.userEmails[j] = (validationResult && validationResult.error) ?
        "Please enter a valid email" : ""
      const userEmails = [...prevState.userEmails]
      userEmails[j] = { ...prevState.userEmails[j] }
      userEmails[j].emailId = value
      userEmails[j].emailPrimary = false
      return { userEmails, error }
    })
  }

  addOtherEmail() {
    this.setState(prevState => {
      const keys = ["userEmails"]
      if (this.checkAnyError(keys)) {
        return {}
      }
      const error = { ...prevState.error }
      error.userEmails = {}
      const userEmails = [...prevState.userEmails]
      const otherEmails = userEmails.slice(1)
      const len = otherEmails.length
      if (len) {
        const lastEmail = otherEmails[len-1]
        if (lastEmail && lastEmail.emailId) {
          userEmails.push({ emailId: "", emailPrimary: false })
        } else {
          error.userEmails[len] = "Please provide a value  here before adding another email"
        }
      } else {
        userEmails.push({ emailId: "", emailPrimary: false })
      }
      return { userEmails, error }
    })
  }

  resetOtherEmail() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userEmails = {}
      const { currentUser } = prevState
      let userEmails = [...prevState.userEmails]
      if (!currentUser || !currentUser.userEmails || currentUser.userEmails.length < 2) {
        return { userEmails: userEmails.slice(0, 1), error }
      }
      userEmails = [...currentUser.userEmails]
      return { userEmails, error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removeOtherEmail(i) {
    this.setState(prevState => {
      const userEmails = [ ...prevState.userEmails ]
      userEmails.splice(i, 1)
      const error = { ...prevState.error }
      error.userEmails = { ...error.userEmails }
      error.userEmails[i] = ""
      return { userEmails, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  changePhone(i, e) {
    const { name, type } = e.target
    let value = type === "checkbox" ? e.target.checked : e.target.value
    console.log("i, name, val : ", i, name, value)
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userPhone = { ...error.userPhone }
      error.userPhone[i] = ""
      if (name === "phoneNumber") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length < 11) {
          error.userPhone[i] = "Phone number should have 10 digits"
        }
      }
      if (name === "extension") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length > 8) {
          error.userPhone[i] = "Ext should have max 8 digits"
        }
      }
      const userPhone = [...prevState.userPhone]
      if(name === "phonePrimary") {
        const pi = userPhone.findIndex(p => p.phonePrimary)
        if(pi > -1) {
          userPhone[pi].phonePrimary = false
        }
      }
      userPhone[i] = { ...userPhone[i] }
      userPhone[i][name] = value
      return { userPhone, error }
    })
  }

  addPhone() {
    this.setState(prevState => {
      const userPhone = [...prevState.userPhone]
      const len = userPhone.length
      const keys = ["userPhone"]
      const error = { ...prevState.error }
      error.userPhone = {}
      if (this.checkAnyError(keys)) {
        return {}
      }
      if (len) {
        const lastPhone = userPhone[len-1]
        if (lastPhone && lastPhone.phoneNumber) {
          userPhone.push({ phoneNumber: "", extension: "", phonePrimary: false })
        } else {
          error.userPhone[len-1] = "Please provide a value here before adding another phone"
        }
      } else {
        userPhone.push({ phoneNumber: "", extension: "", phonePrimary: false })
      }
      return { userPhone, error }
    })
  }

  resetPhone() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userPhone = {}
      const { currentUser } = prevState
      if (!currentUser || !currentUser.userPhone || currentUser.userPhone.length < 1) {
        return { userPhone: [{ phoneNumber: "", extension: "", phonePrimary: false }], error }
      }
      return { userPhone: [...currentUser.userPhone], error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removePhone(i) {
    this.setState(prevState => {
      const userPhone = [ ...prevState.userPhone ]
      userPhone.splice(i, 1)
      const error = { ...prevState.error }
      error.userPhone = { ...error.userPhone }
      error.userPhone[i] = ""
      return { userPhone, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  changeFax(i, e) {
    const { name, type } = e.target
    let value = type === "checkbox" ? e.target.checked : e.target.value
    console.log("i, name, val : ", i, name, value)
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userFax = { ...error.userFax }
      error.userFax[i] = ""
      if (name === "faxNumber") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length < 11) {
          error.userFax[i] = "Fax number should have 10 digits"
        }
      }
      const userFax = [...prevState.userFax]
      if(name === "faxPrimary") {
        const pi = userFax.findIndex(p => p.faxPrimary)
        if(pi > -1) {
          userFax[pi].faxPrimary = false
        }
      }
      userFax[i] = { ...userFax[i] }
      userFax[i][name] = value
      return { userFax, error }
    })
  }

  addFax() {
    this.setState(prevState => {
      const userFax = [...prevState.userFax]
      const len = userFax.length
      const error = { ...prevState.error }
      error.userFax = {}
      const keys = ["userFax"]
      if (this.checkAnyError(keys)) {
        return {}
      }

      if (len) {
        const lastFax = userFax[len-1]
        if (lastFax && lastFax.faxNumber) {
          userFax.push({ faxNumber: "", faxPrimary: false })
        } else {
          error.userFax[len-1] = "Please provide a value  here before adding another fax"
        }
      } else {
        userFax.push({ faxNumber: "", faxPrimary: false })
      }
      return { userFax, error }
    })
  }

  resetFax() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userFax = {}
      const { currentUser } = prevState
      if (!currentUser || !currentUser.userFax || currentUser.userFax.length < 1) {
        return { userFax: [{ faxNumber: "", faxPrimary: false }], error }
      }
      return { userFax: [...currentUser.userFax], error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removeFax(i) {
    this.setState(prevState => {
      const userFax = [ ...prevState.userFax ]
      userFax.splice(i, 1)
      const error = { ...prevState.error }
      error.userFax = { ...error.userFax }
      error.userFax[i] = ""
      return { userFax, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  showErrorToast(err) {
    toast(err, { autoClose: toastTimeout, type: toast.TYPE.WARNING })
  }

  saveCallback(err, res) {
    const { afterSaveUser } = this.props
    if (!err) {
      const { _id } = res
      const { userId } = this.state
      if (_id) {
        if (!userId) {
          sendUserOnboardingEmail(_id, this.sendEmailCallback)
        }
        this.setState({ userId: _id })
        // this.props.history.push(`/add-new-taks/${_id}`)
      }
      if(afterSaveUser) {
        afterSaveUser(_id)
      } else {
        this.getUserData()
      }
      toast("Changes Saved", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      this.setState({ waiting: false })
      this.showErrorToast("Error in saving changes")
    }
  }

  sendEmailCallback(err, res) {
    if (res && res.data && res.data.done && res.data.type === "Self") {
      toast("Email sent successfully", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else if (res && res.data && !res.data.done && res.data.type === "Self" ) {
      toast(res.data.message, { autoClose: toastTimeout, type: toast.TYPE.ERROR })
    }
    else if (res && res.data && !res.data.done && ["Client","Third Party","Prospect"].includes(res.data.type) ) {
      toast("No emails will be sent to Client, Prospect and Third Party Users", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    }
    else {
      toast(res.data.message, { autoClose: toastTimeout, type: toast.TYPE.ERROR })
    }
  }


  savePrimaryDetails() {
    this.setState({ waiting: true })
    const { userId, entity, userFirstName, userMiddleName, userLastName, userEmails,
      userEntitlement, userEmailId, userFlags, userStatus } = this.state
    const requiredFields = ["userFirstName", "userLastName", "userEntitlement",
      "userEmailId"]
    let emptyRequiredField = false
    requiredFields.forEach(d => {
      if(!this.state[d]) {
        emptyRequiredField = true
        this.setState(prevState => {
          const error = { ...prevState.error }
          error[d] = "Please provide a value"
          return { error, waiting: false }
        })
      }
    })
    if(emptyRequiredField) {
      this.setState({ waiting: false })
      this.showErrorToast("Please fix highlighted errors")
      return
    }
    const { entityId } = entity
    const emails = [{ emailId: userEmailId, emailPrimary: true }, ...userEmails.slice(1)]
    const basicDetails = {
      entityId, userFirstName,
      userMiddleName, userLastName, userEntitlement, userFlags,
      userEmails: emails, userStatus, ...defaultValues
    }

    const keys = [...requiredFields]
    if(this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      this.showErrorToast("Please fix highlighted errors")
      return
    }

    const { hideContactType, primaryContact } = this.props
    if(hideContactType && primaryContact) {
      basicDetails.userFlags = ["Primary Contact"]
    }
    saveEntityUserDetails(userId, basicDetails, this.saveCallback)
  }

  saveOtherDetails() {
    this.setState({ waiting: true })
    const checkArrays = [
      {key: "userEmails", field: "emailId"},
      {key: "userPhone", field: "phoneNumber"},
      {key: "userFax", field: "faxNumber"},
    ]
    let emptyArrayError
    checkArrays.some(a => {
      const arr = this.state[a.key].map(d => d[a.field])
      console.log("arr : ", arr)
      const emptyIdx = this.checkEmptyArray(arr)
      console.log("emptyIdx : ", emptyIdx)
      if(emptyIdx > -1) {
        this.setState(prevState => {
          const error = { ...prevState.error }
          error[a.key] = { ...error[a.key] }
          error[a.key][emptyIdx] = "Please provide a value"
          return { error }
        })
        emptyArrayError = true
        return true
      }
    })
    if(emptyArrayError) {
      this.setState({ waiting: false })
      this.showErrorToast("Please fix highlighted errors")
      return
    }
    const { userId, userEmails, userPhone, userFax, userEmployeeID,
      userJobTitle, userManagerEmail, userJoiningDate, entity,
      userExitDate, userCostCenter } = this.state
    let otherDetails = {
      userEmails,
      userPhone,
      userFax
    }
    if(entity && entity.entityType === "Firm") {
      otherDetails = { ...otherDetails, userEmployeeID,
        userJobTitle, userManagerEmail, userJoiningDate,
        userExitDate, userCostCenter }
    }
    const keys = ["userEmails", "userPhone", "userFax", "userManagerEmail",
      "userJoiningDate", "userExitDate"]
    if(this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      this.showErrorToast("Please fix highlighted errors")
      return
    }
    saveEntityUserDetails(userId, otherDetails, this.saveCallback)
  }

  saveAddressChanges() {
    const { userId, userAddresses } = this.state
    const updateData = { userAddresses: userAddresses || [] }
    saveEntityUserDetails(userId, updateData, this.saveCallback)
  }


  renderSection() {
    const { expanded, viewOnly } = this.state
    return (
      <article className={expanded.entityTypeSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <p onClick={this.toggleExpand.bind(this, "entityTypeSection")}>
            Entity Type</p>
        </div>
        {expanded.entityTypeSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content">
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={(() => { })}>
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderFirmUserExtraDetails() {
    const { userEmployeeID, userJobTitle, userManagerEmail, userJoiningDate,
      userExitDate, userCostCenter, error } = this.state
    return (
      <div>
        <hr />
        <div className="columns">
          <div className="column">
            <p className="multiExpLblBlk">
                Employee ID
            </p>
            <input
              className="input is-small is-link"
              type="text"
              name="userEmployeeID"
              placeholder="Emplyee Id"
              value={userEmployeeID}
              onChange={this.changeField}
            />
            {error && error.userEmployeeID ?
              <p className="has-text-danger">
                {error.userEmployeeID}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
              Job Title
            </p>
            <input
              className="input is-small is-link"
              type="text"
              name="userJobTitle"
              placeholder="Job Title"
              value={userJobTitle}
              onChange={this.changeField}
            />
            {error && error.userJobTitle ?
              <p className="has-text-danger">
                {error.userJobTitle}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
              {"Manager's Email"}
            </p>
            <input
              className="input is-small is-link"
              type="email"
              name="userManagerEmail"
              placeholder="Manager's Email"
              value={userManagerEmail}
              onChange={this.changeField}
            />
            {error && error.userManagerEmail ?
              <p className="has-text-danger">
                {error.userManagerEmail}
              </p>
              : undefined
            }
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <p className="multiExpLblBlk">
                Joining Date
            </p>
            <input
              className="input is-small is-link"
              type="date"
              name="userJoiningDate"
              placeholder="Joining Date"
              value={getCalendarDateAsString(userJoiningDate)}
              onChange={this.changeField}
            />
            {error && error.userJoiningDate ?
              <p className="has-text-danger">
                {error.userJoiningDate}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
                Exit Date
            </p>
            <input
              className="input is-small is-link"
              type="date"
              name="userExitDate"
              placeholder="Joining Date"
              value={getCalendarDateAsString(userExitDate)}
              onChange={this.changeField}
            />
            {error && error.userExitDate ?
              <p className="has-text-danger">
                {error.userExitDate}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
              Cost Center
            </p>
            <input
              className="input is-small is-link"
              type="email"
              name="userCostCenter"
              placeholder="Cost Center"
              value={userCostCenter}
              onChange={this.changeField}
            />
            {error && error.userCostCenter ?
              <p className="has-text-danger">
                {error.userCostCenter}
              </p>
              : undefined
            }
          </div>
        </div>
      </div>
    )
  }

  renderContactDetails() {
    const { expanded, viewOnly, error, userId, userEmails, userPhone,
      userFax, entity, modalType, modalIndex, modalState } = this.state
    if (!userId) {
      return null
    }
    const { entityType }  = entity || {}
    const otherEmails = userEmails ? userEmails.slice(1) : []
    if (!otherEmails.length) {
      otherEmails.push({ emailId: "", emailPrimary: false })
    }
    const phones = [...userPhone]
    if (!phones.length) {
      phones.push({ phoneNumber: "", extension: "", phonePrimary: false })
    }

    const fax = [...userFax]
    if (!fax.length) {
      fax.push({ faxNumber: "", faxPrimary: false })
    }
    return (
      <article className={expanded.otherDetailsSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <p onClick={this.toggleExpand.bind(this, "otherDetailsSection")}>
            Other Details</p>
        </div>
        {expanded.otherDetailsSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                      Other Emails
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  {modalType === "email" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Email ${(otherEmails[modalIndex] && otherEmails[modalIndex].emailId) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removeOtherEmail.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                  }
                  {
                    otherEmails.map((e, i) => {
                      const { emailId } = e
                      return (
                        <div key={i} className="columns">
                          <div className="column">
                            <div className="field is-grouped">
                              <input
                                className="input is-small is-link"
                                type="email"
                                placeholder="johnsmith@example.com"
                                value={emailId}
                                onChange={this.changeOtherEmail.bind(this, i)}
                              />
                              <span className="has-text-link"
                                onClick={this.toggleModal.bind(this, "email", i+1)}>
                                <i className="far fa-trash-alt" />
                              </span>
                            </div>
                            {error && error.userEmails && error.userEmails[i + 1] ?
                              <p className="has-text-danger">
                                {error.userEmails[i + 1]}
                              </p>
                              : undefined
                            }
                          </div>
                        </div>
                      )
                    })
                  }
                  <div>
                    <div className="field is-grouped is-pulled-left">
                      <div className="control">
                        <button className="button is-link is-small" type="button"
                          onClick={this.addOtherEmail}>Add More</button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small" type="button"
                          onClick={this.resetOtherEmail}>Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                      Phone
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  {modalType === "phone" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Phone ${(phones[modalIndex] && phones[modalIndex].phoneNumber) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removePhone.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                  }
                  {
                    phones.map((e, i) => {
                      const { phoneNumber, extension, phonePrimary } = e
                      return (
                        <div key={i}>
                          <div className="field is-grouped-left">
                            <div className="control">
                              <MaskedInput
                                mask={["+", "1", "(", /[0-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                                className="input is-small is-link is-fullwidth"
                                type="text"
                                name="phoneNumber"
                                placeholder="Phone Number"
                                value={phoneNumber}
                                onChange={this.changePhone.bind(this, i)}
                              />
                            </div>
                            <div className="control">
                              <MaskedInput
                                mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                className="input is-small is-link"
                                type="text"
                                name="extension"
                                size={8}
                                placeholder="Ext"
                                value={extension}
                                onChange={this.changePhone.bind(this, i)}
                              />
                            </div>
                            {!phonePrimary &&
                              <span className="has-text-link"
                                onClick={this.toggleModal.bind(this, "phone", i)}>
                                <i className="far fa-trash-alt" />
                              </span>
                            }
                          </div>
                          {error && error.userPhone && error.userPhone[i] ?
                            <p className="has-text-danger">
                              {error.userPhone[i]}
                            </p>
                            : undefined
                          }
                          <div className="field is-grouped-left">
                            <p className="multiExpLblBlk">
                              is primary
                              <input
                                type="checkbox"
                                name="phonePrimary"
                                checked={!!phonePrimary}
                                onChange={this.changePhone.bind(this, i)}
                              />
                            </p>
                          </div>
                        </div>
                      )
                    })
                  }
                  <div>
                    <div className="field is-grouped is-pulled-left">
                      <div className="control">
                        <button className="button is-link is-small" type="button"
                          onClick={this.addPhone}>Add More</button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small" type="button"
                          onClick={this.resetPhone}>Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                      Fax
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  {modalType === "fax" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Fax ${(fax[modalIndex] && fax[modalIndex].faxNumber) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removeFax.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                  }
                  {
                    fax.map((e, i) => {
                      const { faxNumber, faxPrimary } = e
                      return (
                        <div key={i}>
                          <div className="field is-grouped-left">
                            <MaskedInput
                              mask={["+", "1", "(", /[1-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                              className="input is-small is-link"
                              name="faxNumber"
                              type="text"
                              placeholder="fax"
                              value={faxNumber}
                              onChange={this.changeFax.bind(this, i)}
                            />
                            {!faxPrimary &&
                              <span className="has-text-link"
                                onClick={this.toggleModal.bind(this, "fax", i)}>
                                <i className="far fa-trash-alt" />
                              </span>
                            }
                          </div>
                          {error && error.userFax && error.userFax[i] ?
                            <p className="has-text-danger">
                              {error.userFax[i]}
                            </p>
                            : undefined
                          }
                          <div className="field is-grouped-left">
                            <p className="multiExpLblBlk">
                              is primary
                              <input
                                type="checkbox"
                                name="faxPrimary"
                                checked={!!faxPrimary}
                                onChange={this.changeFax.bind(this, i)}
                              />
                            </p>
                          </div>
                        </div>
                      )
                    })
                  }
                  <div>
                    <div className="field is-grouped is-pulled-left">
                      <div className="control">
                        <button className="button is-link is-small" type="button"
                          onClick={this.addFax}>Add More</button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small" type="button"
                          onClick={this.resetFax}>Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {entityType === "Firm" &&
                this.renderFirmUserExtraDetails()
              }
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={this.saveOtherDetails}>
                          Save Other Details
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderBasicDetails() {
    const { expanded, entity, userFlags, userFlagsList, userEntitlement, userStatus,
      userFirstName, userMiddleName, userLastName, userEmailId, relatedEntites,
      error, viewOnly, currentUser } = this.state
    const { entityId, hideContactType, hideUserStatusToggle } = this.props
    const { userLoginCredentials } = currentUser
    const [currentEntityName] = entityId ? [entity.entityName] :
      this.getEntityNameAndType(relatedEntites, currentUser.entityId)
    return (
      <article className={expanded.userBasicSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <p onClick={this.toggleExpand.bind(this, "userBasicSection")}>
            Primary Details</p>
        </div>
        {expanded.userBasicSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content" >
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Firm Name
                    {!currentEntityName &&
                      <span className="icon has-text-danger">
                        <i className="fas fa-asterisk extra-small-icon" />
                      </span>
                    }
                  </p>
                  {currentEntityName ?
                    <p className="is-size-7">{currentEntityName}</p> :
                    <Combobox
                      busy={!(relatedEntites && relatedEntites.length)}
                      data={relatedEntites}
                      textField='entityName'
                      caseSensitive={false}
                      minLength={1}
                      filter='contains'
                      onChange={this.changeEntity}
                      groupBy='entityType'
                      groupComponent={this.entityGroupHeading}
                    />
                  }
                </div>
                <div className="column">
                  {/* <p className="multiExpLblBlk">Active/Inactive</p>
                  <div className="select is-small is-link">
                    <select
                      value={userStatus || "active"}
                      name="userEntitlement"
                      onChange={this.changeField}
                    >
                      {activeStatus.map(t => (
                        <option key={t} value={t}>
                          {t}
                        </option>
                      ))}
                    </select>
                  </div> */}
                  {!hideUserStatusToggle &&
                    <div className="switchContainer is-pulled-right">
                      <span className="filterText">{userStatus !== "inactive" ? "Active" : "Inactive"}</span>
                      <label className="customswitch" style={{ marginLeft: "10px" }}>
                        <input
                          type="checkbox"
                          onChange={this.changeUserStatus}
                          checked={userStatus !== "inactive"}
                        />
                        <span className="customslider round" />
                      </label>
                    </div>
                  }
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    First Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <input
                    className="input is-small is-link"
                    type="text"
                    name="userFirstName"
                    placeholder="John"
                    value={userFirstName}
                    onChange={this.changeField}
                  />
                  {error && error.userFirstName ?
                    <p className="has-text-danger">
                      {error.userFirstName}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                      Middle Name
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  <input
                    className="input is-small is-link"
                    type="text"
                    name="userMiddleName"
                    placeholder="M"
                    value={userMiddleName}
                    onChange={this.changeField}
                  />
                  {error && error.userMiddleName ?
                    <p className="has-text-danger">
                      {error.userMiddleName}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                    Last Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <input
                    className="input is-small is-link"
                    type="text"
                    name="userLastName"
                    placeholder="Smith"
                    value={userLastName}
                    onChange={this.changeField}
                  />
                  {error && error.userLastName ?
                    <p className="has-text-danger">
                      {error.userLastName}
                    </p>
                    : undefined
                  }
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Primary Email
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <input
                    className="input is-small is-link"
                    type="email"
                    disabled={userLoginCredentials && userLoginCredentials !== "created"}
                    name="userEmailId"
                    placeholder="johnsmith@example.com"
                    value={userEmailId}
                    onChange={this.changeField}
                  />
                  {error && error.userEmailId ?
                    <p className="has-text-danger">
                      {error.userEmailId}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  <div>
                    <p className="multiExpLblBlk">
                      System Access Level
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                    </p>
                    <div className="select is-small is-link">
                      <select
                        value={userEntitlement}
                        name="userEntitlement"
                        onChange={this.changeField}
                      >
                        <option value="">Select access level</option>
                        {this.getAccessLevelList().map(t => (
                          <option key={t} value={t}>
                            {t}
                          </option>
                        ))}
                      </select>
                      {error && error.userEntitlement ?
                        <p className="has-text-danger">
                          {error.userEntitlement}
                        </p>
                        : undefined
                      }
                    </div>
                  </div>
                </div>
                {!hideContactType && <div className="column">
                  <p className="multiExpLblBlk">
                      Contact Type
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  <Multiselect
                    data={userFlagsList}
                    placeholder=" Select contact types"
                    caseSensitive={false}
                    minLength={1}
                    value={userFlags}
                    filter="contains"
                    onChange={this.changeContactTypes}
                  />
                  {error && error.userFlags ?
                    <p className="has-text-danger">
                      {error.userFlags}
                    </p>
                    : undefined
                  }
                </div>}
              </div>
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={this.savePrimaryDetails}>
                          Save Primary Details
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderAddress() {
    const { userId, waiting, countryResult, userAddresses, error,
      viewOnly, entityAddress } = this.state

    if (!userId) {
      return null
    }

    return (
      <FirmAddressListForm
        key={parseInt(1 * 30, 10)}
        countryResult={countryResult}
        errorFirmDetail={error.addresses[0]}
        onChangeAddressType={this.onChangeAddressType}
        canEdit={!viewOnly}
        canView
        busy={waiting}
        addressList={userAddresses}
        onSaveAddress={this.onSaveAddress}
        isUserAddress
        officeAddress={entityAddress || getInitialAddress()[0]}
      />
    )
  }

  render() {
    const { showAddress, showOtherDetails } = this.props
    const { waiting, generalError } = this.state
    if (generalError) {
      return (
        <div id="main">
          <strong className="has-text-danger">{generalError}</strong>
        </div>
      )
    }
    return (
      <div>
        {waiting && <Loader />}
        <section className="accordions">
          {this.renderBasicDetails()}
          {showOtherDetails && this.renderContactDetails()}
          {showAddress && this.renderAddress()}
        </section>
      </div>
    )
  }
}

export default UsersModal
