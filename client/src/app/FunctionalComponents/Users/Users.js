import React, { Component } from "react"
import swal from "sweetalert"
import { toast } from "react-toastify"
import {connect} from "react-redux"
import {Link} from "react-router-dom"
import { Multiselect, Combobox } from "react-widgets"
import MaskedInput from "react-text-mask"
import { Modal } from "Global/BulmaModal"
import Loader from "Global/Loader"
import { crmSchema, validateValue } from "Validation/common"
import PhoneInput, {formatPhoneNumber, isValidPhoneNumber} from "react-phone-number-input"
import {
  getPicklistValues, getEntityUserDetails, saveEntityUserDetails,
  sendUserOnboardingEmail, getAllRelatedEntites, getEntityDetails,
  getCalendarDateAsString, sendOnBoardingEmailForMigratedUsers,
  checkNavAccessEntitlrment, updateAuditLog} from "GlobalUtils/helpers"

import FirmAddressListForm from "../EntityManagement/CommonComponents/FirmAddressListForm"
import HistoricalData from "../../GlobalComponents/HistoricalData"
import NotesReactTable from "../../GlobalComponents/NotesReactTable"
import {checkSupervisorControls} from "../../StateManagement/actions/CreateTransaction"
import RatingSection from "../../GlobalComponents/RatingSection"
import Accordion from "../../GlobalComponents/Accordion"
import {urlHelper} from "../../../globalutilities/helpers"
import {TextLabelInput} from "../../GlobalComponents/TextViewBox"

// Save Modal Definition
export const SaveModal = ({ message, closeModal, savemodalState, onConfirmed }) => {
  if (!savemodalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" autoFocus onClick={onConfirmed} >Yes</button>
            <button className="button" onClick={closeModal} >No</button>
          </div>
        </footer>
      </div>
    </div>
  )
}

const defaultValues = {
  isMuniVisorClient: false
}


const toastTimeout = 1000

const accessTypes = {
  Firm: ["global-edit", "global-readonly", "tran-edit", "tran-readonly",
    "global-readonly-tran-edit"],
  Client: ["tran-edit", "tran-readonly"],
  Undefined: ["tran-edit", "tran-readonly"],
  Prospect: ["tran-edit", "tran-readonly"],
  "Third Party": ["tran-edit", "tran-readonly"]
}

const getInitialAddress = () => (
  [
    {
      addressName: "",
      isPrimary: false,
      isHeadQuarter: false,
      /* isActive: true, */
      website: "",
      officePhone: [
        {
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }
      ],
      officeFax: [
        {
          faxNumber: ""
        }
      ],
      officeEmails: [
        {
          emailId: ""
        }
      ],
      addressLine1: "",
      addressLine2: "",
      country: "",
      state: "",
      city: "",
      zipCode: {
        zip1: "",
        zip2: ""
      },
      formatted_address: "",
      url: "",
      location: {
        longitude: "",
        latitude: ""
      }
    }
  ]
)

const getInitialState = () => ({
  expanded: {
    userBasicSection: true,
    otherDetailsSection: false,
    userNotes: false,
  },
  viewOnly: true,
  entity: {},
  currentUser: {},
  userId: "",
  userEmailId: "",
  userStatus: "active",
  userFlags: [],
  notes: [],
  userEmails: [],
  userPhone: [],
  userFax: [],
  userEmployeeID: "",
  userJobTitle: "",
  userManagerEmail: "",
  userJoiningDate: null,
  userExitDate: null,
  userCostCenter: "",
  userEntitlement: "tran-edit",
  userFirstName: "",
  userMiddleName: "",
  userLastName: "",
  userAddresses: getInitialAddress(),
  modalState: false,
  savemodalState: false,
  edit: false,
  modalIndex: -1,
  modalType: "",
  error: { addresses: getInitialAddress() },
  generalError: "",
  updateData: {},
  firmDetails: {},
  userFlagsListDisabled: []
})

class UsersNew extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userFlagsList: [], relatedEntites: [], entityAddress: "", changeLog: [],
      primaryContactId: "", ...getInitialState()
    }
    this.saveCallback = this.saveCallback.bind(this)
    this.getEntityDetailsCallback = this.getEntityDetailsCallback.bind(this)
    this.getUserData = this.getUserData.bind(this)
    this.getUserDetailsCallback = this.getUserDetailsCallback.bind(this)
    this.saveOtherDetails = this.saveOtherDetails.bind(this)
    this.changeUserStatus = this.changeUserStatus.bind(this)
    this.changeEntity = this.changeEntity.bind(this)
    this.entityGroupHeading = this.entityGroupHeading.bind(this)
    this.changeField = this.changeField.bind(this)
    this.changeContactTypes = this.changeContactTypes.bind(this)
    this.renderTag = this.renderTag.bind(this)
    this.addOtherEmail = this.addOtherEmail.bind(this)
    this.resetOtherEmail = this.resetOtherEmail.bind(this)
    this.addPhone = this.addPhone.bind(this)
    this.resetPhone = this.resetPhone.bind(this)
    this.addFax = this.addFax.bind(this)
    this.resetFax = this.resetFax.bind(this)
    this.onSaveAddress = this.onSaveAddress.bind(this)
    this.saveAddressChanges = this.saveAddressChanges.bind(this)
  }

  async componentDidMount() {
    const picklistPromise = getPicklistValues(["LKUPUSERFLAG"])
    const { entityId, nav2 } = this.props

    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit

    const relatedEntitesResult = getAllRelatedEntites()
    const result = await picklistPromise
    const relatedEntitesList = (await relatedEntitesResult) || []
    const relatedEntites = relatedEntitesList.map(e => ({
      entityId: e.relatedEntityId,
      entityName: e.firmName,
      entityType: e.relationshipType
    }))
    let entity = {}
    let userFlagsListDisabled = []
    if (entityId) {
      getEntityDetails(entityId, this.getEntityDetailsCallback)
      const [entityName, entityType] = this.getEntityNameAndType(relatedEntites, entityId)
      entity = { entityId, entityName, entityType }
      userFlagsListDisabled = this.userFlagsDisabledView(entityType)
      console.log("entity details", entity)
    }
    console.log("result : ", result)
    let userFlagsList = result[0] && result[0][1] ? result[0][1] : []
    const disabledUserFlags = userFlagsList && userFlagsList.filter(f => !f.included).map(e => e.label)
    userFlagsList = userFlagsList && userFlagsList.map(e => e.label)
    this.setState({
      entity, userFlagsList, userId: nav2 || "",
      relatedEntites, userFlagsListDisabled, viewOnly, disabledUserFlags, waiting: false
    }, this.getUserData)
  }

  async componentWillReceiveProps(nextProps) {
    const { nav2 } = nextProps
    if (nav2 !== this.props.nav2) {
      const action = await checkNavAccessEntitlrment(nav2)
      const viewOnly = action && nav2 && !action.edit
      this.setState({ userId: nav2 || "", viewOnly }, this.getUserData)
    }
  }

  onSaveAddress(userAddresses) {
    console.log("addressList : ", userAddresses)
    this.setState({ userAddresses }, this.saveAddressChanges)
  }

  getEntityNameAndType(entities = [], entityId) {
    const idx = entities.findIndex(e => (e.entityId === entityId))
    if (idx > -1) {
      const { entityName, entityType } = entities[idx]
      return [entityName, entityType]
    }
    return []
  }

  getAccessLevelList(entity) {
    console.log("entity : ", entity)
    entity = entity || this.state.entity
    let { entityType } = entity || {}
    console.log("entity, entityType : ", entity, entityType)
    if(this.props.nav3 === "migratedentities"){
      entityType = "Undefined"
    }
    return (accessTypes[entityType] || [])
  }

  getEntityDetailsCallback(err, res) {
    if(err) {
      this.setState({ waiting: false, generalError: "Error in getting entity data"})
    } else {
      console.log("res : ", res)
      const firmDetails = res[0] || {}
      const { addresses, primaryContactId = "" } = res[0] || {}
      if(addresses && addresses.length) {
        const entityAddress = addresses.filter(a => a.isPrimary)[0] || addresses[0]
        this.setState({ entityAddress })
      }
      this.setState(prevState => {
        const userFlags = [ ...prevState.userFlags ]
        const userFlagsList = [ ...prevState.userFlagsList ]
        const { userId } = prevState
        if(primaryContactId !== userId && primaryContactId) {
          userFlagsList // = userFlagsList.filter(f => f !== PRIMARY_CONTACT)
          userFlags  // = userFlags.filter(f => f !== PRIMARY_CONTACT)
        }
        // else if(!userFlags.includes(PRIMARY_CONTACT)) {
        //   userFlags.push(PRIMARY_CONTACT)
        // }
        return { userFlags, userFlagsList, primaryContactId, firmDetails }
      })
    }
  }

  getUserDetailsCallback(err, res) {
    if (err) {
      this.setState({ waiting: false, generalError: "Error in getting user data" })
    } else {
      console.log("res : ", res)
      const currentUser = res || {}
      const { relatedEntites, entityAddress } = this.state
      const { userFirstName, userMiddleName, userLastName, userEmails, userStatus,
        userEntitlement, userFlags, userPhone, userFax, userAddresses,
        userJobTitle, userManagerEmail, userJoiningDate, userEmployeeID,
        userExitDate, userCostCenter, relationshipType, notes } = currentUser

      const userEmailId = userEmails && userEmails[0] && userEmails[0].emailId
      const entityId = (currentUser && currentUser.entityId) || ""
      let entity
      if (entityId) {
        if(!entityAddress) {
          getEntityDetails(entityId, this.getEntityDetailsCallback)
        }
        const [entityName, entityType] = this.getEntityNameAndType(relatedEntites, currentUser.entityId)
        entity = { entityId, entityName, entityType }
      }


      const validUserEntitlements = this.getAccessLevelList(entity)
      const userFlagsListDisabled = this.userFlagsDisabledView(entity.entityType)

      console.log("validUserEntitlements : ", validUserEntitlements)

      this.setState({
        waiting: false,
        generalError: "",
        userFlagsListDisabled,
        currentUser,
        entity,
        userStatus: (userStatus === "inactive" ? "inactive" : "active"),
        userEmailId,
        userFirstName,
        userMiddleName,
        userLastName,
        userEmails,
        userPhone,
        userFax,
        userJobTitle,
        userManagerEmail,
        userJoiningDate,
        userEmployeeID,
        userExitDate,
        userCostCenter,
        notes: (notes || []) ,
        userEntitlement: (validUserEntitlements.includes(userEntitlement) ?
          userEntitlement : ""),
        userFlags,
        userAddresses,
        error: { addresses: getInitialAddress() }
      })
    }
  }

  getUserData() {
    const { userId } = this.state
    if (userId) {
      this.setState({ waiting: true })
      getEntityUserDetails(userId, this.getUserDetailsCallback)
    }
  }

  toggleSaveModal = () => {
    this.setState(prev => {
      const newState = !prev.savemodalState

      return { savemodalState: newState }
    })
  }

  showModal = (operation) => {
  this.setState({
    savemodalState: true,
      onConfirmed: operation === "primary" ? (() => this.savePrimaryDetails())
        : ( (operation === "other") ?  (() => this.saveOtherDetails()) : (() => this.onSaveNotes())),
      modalMessage: operation === "primary" ? "Are you sure you want to save the primary contact details?"
        : ((operation === "other") ? "Are you sure you want to save the other contact details?"
          : "Are you sure you want to save notes?")
  })
}

  toggleExpand(val) {
    console.log("toggle : ", val)
    // e.preventDefault()
    this.setState(prevState => {
      const expanded = { ...prevState.expanded }
      Object.keys(expanded).forEach(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
      return { expanded }
    })
  }

  toggleModal(modalType, modalIndex) {
    this.setState(prev => {
      const modalState = !prev.modalState
      return { modalState, modalType, modalIndex }
    })
  }

  checkNonEmptyErrorKey(error, keys) {
    if (!error) {
      return false
    }
    if (Object.getPrototypeOf(error).constructor.name === "Object") {
      keys = keys || Object.keys(error).filter(k => k !== "userAddresses")
      if (!keys.length) {
        return false
      }
      let nonEmptyError = false
      keys.some(k => {
        if (this.checkNonEmptyErrorKey(error[k])) {
          nonEmptyError = true
          return true
        }
      })
      return nonEmptyError
    }
    return true
  }

  checkDupInArray(arr) {
    console.log("arr : ", arr)
    let result = [-1, -1]
    arr.some((e, i) => {
      if(e) {
        const dupIdx = arr.slice(i + 1).findIndex(d => d === e)
        if(dupIdx > -1) {
          result = [i, dupIdx+i+1]
          return true
        }
      }
    })
    console.log("dupResult : ", result)
    return result
  }

  checkEmptyArray(arr) {
    let result = -1
    arr.some((e, i) => {
      if(!e) {
        result = i
        return true
      }
    })
    return result
  }

  checkAnyError(keys) {
    const { error } = this.state
    return this.checkNonEmptyErrorKey(error, keys)
  }

  changeUserStatus() {
    const { userFlags, userStatus
    } = this.state
    const errorMessage = "Primary users cannot be inactive. In order to iactivate, make other contact as primary instead of this one."
    if (userFlags.includes("Primary Contact") && userStatus === "active") { swal("Warning", errorMessage, "warning"); return }
    this.setState(prevState => {
      let userStatus = "inactive"
      const prevStatus = prevState.userStatus
      if (prevStatus === "inactive") {
        userStatus = "active"
      }
      console.log("userStatus : ", userStatus)
      return { userStatus }
    })
  }

  entityGroupHeading({ item }) {
    return (
      <span className="title innerContactTitle has-text-info">{item}</span>
    )
  }

  changeEntity = (entity) => {
    console.log("entity: ", entity)
    const userFlagsListDisabled = this.userFlagsDisabledView(entity.entityType)
    this.setState({ entity, userFlagsListDisabled, userFlags: [] })
    if(entity && entity.entityId) {
      getEntityDetails(entity.entityId, this.getEntityDetailsCallback)
    }
  }

  userFlagsDisabledView = (entityType) => {
    let userFlagsListDisabled = []
    const CRM = ["Client", "Prospect", "Third Party"]
    const tenants = ["Firm", "Self", "Municipal Advisor", "TENANT"]
    if(CRM.indexOf(entityType) !== -1){
      userFlagsListDisabled = ["Series 50", "Compliance Officer", "Supervisory Principal"]
    }else if(tenants.indexOf(entityType) !== -1){
      userFlagsListDisabled = ["Dist Member", "EMMA Contact", "MSRB Contact", "Finance Contact", "Procurement Contact"]
    }
    return userFlagsListDisabled
  }

  changeField(e) {
    const { name, value } = e.target
    this.setState(prevState => {
      const error = { ...prevState.error }
      error[name] = ""
      let validationResult
      if(value && name === "userEmailId") {
        validationResult = validateValue(crmSchema, "emailId", value)
        error.userEmailId = (validationResult && validationResult.error) ?
          "Please enter a valid email" : ""
      }
      if(value && name === "userManagerEmail") {
        validationResult = validateValue(crmSchema, "emailId", value)
        error.userManagerEmail = (validationResult && validationResult.error) ?
          "Please enter a valid email" : ""
      }
      if (name === "userExitDate") {
        const { userJoiningDate } = prevState
        console.log("value1 : ", value)
        if (
          userJoiningDate &&
          value &&
          new Date(userJoiningDate) > new Date(value)
        ) {
          console.log("1")
          error.userExitDate =
            "Exit date cannot be before the joining date"
        } else {
          console.log("2")
          error.userExitDate = ""
        }
      }
      if (name === "userJoiningDate") {
        const { userExitDate } = prevState
        console.log("value2 : ", value)
        if (
          userExitDate &&
          value &&
          new Date(userExitDate) < new Date(value)
        ) {
          console.log("3")
          error.userJoiningDate =
            "Joining date cannot be after the exit date"
        } else {
          console.log("4")
          error.userJoiningDate = ""
        }
      }
      return { [name]: value, error }
    })
  }

  changeContactTypes(values) {
    console.log("userFlags values : ", values)
    this.setState(prevState => {
      let userFlags = [ ...prevState.userFlags ]
      console.log("userFlags 1 : ", userFlags)
      const userFlagsList = [ ...prevState.userFlagsList ]
      const { userId, primaryContactId } = prevState
      if(primaryContactId !== userId && primaryContactId) {
        userFlagsList // .filter(f => f !== PRIMARY_CONTACT)
        userFlags = values // .filter(f => f !== PRIMARY_CONTACT)
      } else {
        // userFlags = [ ...new Set([PRIMARY_CONTACT, ...values]) ]
        userFlags = values
      }
      console.log("userFlags 2 : ", userFlags)
      return { userFlags, userFlagsList }
    })
    // this.setState({ userFlags })
  }

  changeOtherEmail(i, e) {
    const { value } = e.target
    let validationResult
    if(value) {
      validationResult = validateValue(crmSchema, "emailId", value)
    }
    // console.log("validationResult : ", validationResult)
    const j = i + 1
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userEmails = { ...error.userEmails }
      error.userEmails[j] = (validationResult && validationResult.error) ?
        "Please enter a valid email" : ""
      const userEmails = [...prevState.userEmails]
      userEmails[j] = { ...prevState.userEmails[j] }
      userEmails[j].emailId = value
      userEmails[j].emailPrimary = false
      return { userEmails, error }
    })
  }

  addOtherEmail() {
    this.setState(prevState => {
      const keys = ["userEmails"]
      if (this.checkAnyError(keys)) {
        return {}
      }
      const error = { ...prevState.error }
      error.userEmails = {}
      const userEmails = [...prevState.userEmails]
      const otherEmails = userEmails.slice(1)
      const len = otherEmails.length
      if (len) {
        const lastEmail = otherEmails[len-1]
        if (lastEmail && lastEmail.emailId) {
          userEmails.push({ emailId: "", emailPrimary: false })
        } else {
          error.userEmails[len] = "Please provide a value  here before adding another email"
        }
      } else {
        userEmails.push({ emailId: "", emailPrimary: false })
      }
      return { userEmails, error }
    })
  }

  resetOtherEmail() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userEmails = {}
      const { currentUser } = prevState
      let userEmails = [...prevState.userEmails]
      if (!currentUser || !currentUser.userEmails || currentUser.userEmails.length < 2) {
        return { userEmails: userEmails.slice(0, 1), error }
      }
      userEmails = [...currentUser.userEmails]
      return { userEmails, error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removeOtherEmail(i) {
    this.setState(prevState => {
      const userEmails = [ ...prevState.userEmails ]
      userEmails.splice(i, 1)
      const error = { ...prevState.error }
      error.userEmails = {}
      return { userEmails, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  changePhone(i, e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value.trim()
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userPhone = { ...error.userPhone }
      error.userPhone[i] = ""
      if (name === "phoneNumber") {
        // value = value.replace(/[^0-9]/g, "")
        if(value.length < 17) {
          error.userPhone[i] = "Phone number should have 10 digits"
        }
      }
      /* if (name === "extension") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length > 8) {
          error.userPhone[i] = "Ext should have max 8 digits"
        }
      } */
      const userPhone = [...prevState.userPhone]
      if(name === "phonePrimary") {
        const pi = userPhone.findIndex(p => p.phonePrimary)
        if(pi > -1) {
          userPhone[pi].phonePrimary = false
        }
      }
      userPhone[i] = { ...userPhone[i] }
      userPhone[i][name] = value
      return { userPhone, error }
    })
  }

  addPhone() {
    this.setState(prevState => {
      const userPhone = [...prevState.userPhone]
      const len = userPhone.length
      const keys = ["userPhone"]
      const error = { ...prevState.error }
      error.userPhone = {}
      if (this.checkAnyError(keys)) {
        return {}
      }
      if (len) {
        const lastPhone = userPhone[len-1]
        if (lastPhone && lastPhone.phoneNumber) {
          userPhone.push({ phoneNumber: "", extension: "", phonePrimary: false })
        } else {
          error.userPhone[len-1] = "Please provide a value here before adding another phone"
        }
      } else {
        userPhone.push({ phoneNumber: "", extension: "", phonePrimary: false })
      }
      return { userPhone, error }
    })
  }

  resetPhone() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userPhone = {}
      const { currentUser } = prevState
      if (!currentUser || !currentUser.userPhone || currentUser.userPhone.length < 1) {
        return { userPhone: [{ phoneNumber: "", extension: "", phonePrimary: false }], error }
      }
      return { userPhone: [...currentUser.userPhone], error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removePhone(i) {
    this.setState(prevState => {
      const userPhone = [ ...prevState.userPhone ]
      userPhone.splice(i, 1)
      const error = { ...prevState.error }
      error.userPhone = { ...error.userPhone }
      error.userPhone[i] = ""
      return { userPhone, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  changeFax(i, e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value.trim()
    console.log("i, name, val : ", i, name, value)
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userFax = { ...error.userFax }
      error.userFax[i] = ""
      if (name === "faxNumber") {
        // value = value.replace(/[^0-9]/g, "")
        if(value.length < 17) {
          error.userFax[i] = "Fax number should have 10 digits"
        }
      }
      const userFax = [...prevState.userFax]
      if(name === "faxPrimary") {
        const pi = userFax.findIndex(p => p.faxPrimary)
        if(pi > -1) {
          userFax[pi].faxPrimary = false
        }
      }
      userFax[i] = { ...userFax[i] }
      userFax[i][name] = value
      return { userFax, error }
    })
  }

  addFax() {
    this.setState(prevState => {
      const userFax = [...prevState.userFax]
      const len = userFax.length
      const error = { ...prevState.error }
      error.userFax = {}
      const keys = ["userFax"]
      if (this.checkAnyError(keys)) {
        return {}
      }

      if (len) {
        const lastFax = userFax[len-1]
        if (lastFax && lastFax.faxNumber) {
          userFax.push({ faxNumber: "", faxPrimary: false })
        } else {
          error.userFax[len-1] = "Please provide a value  here before adding another fax"
        }
      } else {
        userFax.push({ faxNumber: "", faxPrimary: false })
      }
      return { userFax, error }
    })
  }

  resetFax() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userFax = {}
      const { currentUser } = prevState
      if (!currentUser || !currentUser.userFax || currentUser.userFax.length < 1) {
        return { userFax: [{ faxNumber: "", faxPrimary: false }], error }
      }
      return { userFax: [...currentUser.userFax], error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removeFax(i) {
    this.setState(prevState => {
      const userFax = [ ...prevState.userFax ]
      userFax.splice(i, 1)
      const error = { ...prevState.error }
      error.userFax = { ...error.userFax }
      error.userFax[i] = ""
      return { userFax, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  showErrorToast(err) {
    toast(err, { autoClose: toastTimeout, type: toast.TYPE.WARNING })
  }

  saveCallback = async (err, res, primaryDetails) => {
    if (!err) {
      const { _id } = res
      const { userId, firmDetails } = this.state
      const { user } = this.props
      if (_id) {
        if (!userId && firmDetails && firmDetails.relationshipType === "Self") {
          sendUserOnboardingEmail(_id, this.sendEmailCallback)
        }
        this.setState({ userId: _id })
        // this.props.history.push(`/add-new-taks/${_id}`)
      }
      if(userId === user.userId && primaryDetails){
        await checkSupervisorControls(primaryDetails)
      }
      this.getUserData()
      toast("Changes Saved", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      const { data } = err && err.response
      if( data && data.error ) {
        if ( data && data.duplError ) {
          const duplicateEmailIds = data.duplError.map( ({email}) => email).join(",")
          this.showErrorToast(`Please remove duplicate Email Ids - ${duplicateEmailIds}`)
        } else {
          this.showErrorToast(data.error)
        }
      } else {
        this.showErrorToast("Error in saving changes")
      }
      this.setState({ waiting: false })
    }
  }

  sendEmailCallback(err, res) {
    if (res && res.data && res.data.done) {
      toast("Email sent successfully", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else if (res && res.data && !res.data.done) {
      toast(res.data.message, { autoClose: toastTimeout, type: toast.TYPE.ERROR })
    }
    // else if (res && res.data && !res.data.done && ["Client","Third Party","Prospect"].includes(res.data.type) ) {
    //   toast("No emails will be sent to Client, Prospect and Third Party Users", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    // }
    else {
      toast(res.data.message, { autoClose: toastTimeout, type: toast.TYPE.ERROR })
    }
  }

  savePrimaryDetails = async () => {
    let { userEntitlement } = this.state
    if(!this.state.userEntitlement){
      userEntitlement = "tran-edit"
    }
    this.setState({ waiting: true, userEntitlement, savemodalState: false })
    const { userId, entity, userFirstName, userMiddleName, userLastName, userEmails,
      userEmailId, userFlags, userStatus, changeLog, currentUser } = this.state
    const { nav1 } = this.props
    const requiredFields = ["userFirstName", "userLastName", /* "userEntitlement", */ "userEmailId"]
    let emptyRequiredField = false
    requiredFields.forEach(d => {
      if(!this.state[d]) {
        emptyRequiredField = true
        this.setState(prevState => {
          const error = { ...prevState.error }
          error[d] = "Please provide a value"
          return { error, waiting: false }
        })
      }
    })
    if(emptyRequiredField) {
      this.setState({ waiting: false })
      this.showErrorToast("Please fix highlighted errors")
      return
    }
    const { entityId, entityName } = entity
    const emails = [{ emailId: userEmailId, emailPrimary: true }, ...userEmails.slice(1)]
    const basicDetails = {
      entityId, userFirstName, userFirmName: entityName,
      userMiddleName, userLastName, userEntitlement, userFlags,
      userEmails: emails, userStatus, ...defaultValues
    }
    const keys = [...requiredFields]
    if(this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      this.showErrorToast("Please fix highlighted errors")
      return
    }
    if(!(currentUser && currentUser._id) && (nav1 && nav1 === "thirdparties")) {
      const { userEntities: { userId, userFirstName, userLastName } } = this.props.auth
      const userName = `${userFirstName} ${userLastName}`
      changeLog.push({
        userId,
        userName,
        log: "New User Created",
        date: new Date()
      })
    }
    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
    saveEntityUserDetails(userId, basicDetails, (err, res) => this.saveCallback(err, res, true))
  }

  saveOtherDetails() {
    this.setState({
      error: {},
      waiting: true,
      savemodalState: false
    }, () => {
      const checkArrays = [
        {key: "userEmails", field: "emailId"},
        {key: "userPhone", field: "phoneNumber"},
        {key: "userFax", field: "faxNumber"},
      ]
      let emptyArrayError
      checkArrays.some(a => {
        const arr = this.state[a.key].map(d => d[a.field])
        console.log("arr : ", arr)
        const emptyIdx = this.checkEmptyArray(arr)
        console.log("emptyIdx : ", emptyIdx)
        if(emptyIdx > -1) {
          this.setState(prevState => {
            const error = { ...prevState.error }
            error[a.key] = { ...error[a.key] }
            error[a.key][emptyIdx] = "Please provide a value"
            return { error }
          })
          emptyArrayError = true
          return true
        }
      })

      const  phoneFaxValid = [
        {key: "userFax", field: "faxNumber", label: "Fax number"},
        {key: "userPhone", field: "phoneNumber", label: "Phone number"}
      ]
      phoneFaxValid.forEach(pfKey => {
        const valArr = this.state[pfKey.key]
        const inValidArr = valArr.filter(arr => arr && arr[pfKey.field] && arr[pfKey.field].trim().length < 8)
        if(inValidArr && inValidArr.length) {
          inValidArr.forEach(inVal => {
            const errIdx = valArr.findIndex(val => val[pfKey.field] === inVal[pfKey.field])
            this.setState(prevState => {
              const error = { ...prevState.error }
              error[pfKey.key] = { ...error[pfKey.key] }
              error[pfKey.key][errIdx] = `${pfKey.label} should have 10 digits`
              return { error }
            })
          })
          emptyArrayError = true
        }
      })

      if(emptyArrayError) {
        this.setState({ waiting: false })
        this.showErrorToast("Please fix highlighted errors")
        return
      }
      const { userId, userEmails, userPhone, userFax, userEmployeeID,
        userJobTitle, userManagerEmail, userJoiningDate, entity,
        userExitDate, userCostCenter } = this.state
      const error = {}
      const types = ["userEmails", "userPhone", "userFax"]
      types.forEach(t => {
        const keys = this.state[t]
        const key = t === "userEmails" ? "emailId" : t === "userPhone" ? "phoneNumber" : t === "userFax" ? "faxNumber" : ""
        const dupResult = this.checkDupInArray(keys.map(d => d[key]))
        if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1){
          error[t] = { ...error[t] }
          error[t][dupResult[0]] = "Duplicate values are not allowed"
          error[t][dupResult[1]] = "Duplicate values are not allowed"
        }
      })
      if((error && error.userEmails) || (error && error.userPhone) || (error && error.userFax)){
        return this.setState({error, waiting: false})
      }
      let otherDetails = {
        userEmails,
        userPhone,
        userFax
      }
      otherDetails = { ...otherDetails, userEmployeeID,
        userJobTitle, userManagerEmail, userJoiningDate,
        userExitDate, userCostCenter }
      const keys = ["userEmails", "userPhone", "userFax", "userManagerEmail",
        "userJoiningDate", "userExitDate"]
      if(this.checkAnyError(keys)) {
        this.setState({ waiting: false })
        this.showErrorToast("Please fix highlighted errors")
        return
      }
      otherDetails && otherDetails.userPhone && otherDetails.userPhone.length && otherDetails.userPhone.forEach((item, idx) => {
        const value = item && item.phoneNumber && item.phoneNumber.includes("+") || false
        const Internationals = item.phoneNumber && formatPhoneNumber(value ? item.phoneNumber : `+1${item.phoneNumber}`, "International") || "International"
        if(!value && item.phoneNumber){
          otherDetails.userPhone[idx].phoneNumber = Internationals || ""
        }
      })
      otherDetails && otherDetails.userFax && otherDetails.userFax.length && otherDetails.userFax.forEach((item, idx) => {
        const value = item && item.faxNumber && item.faxNumber.includes("+") || false
        const Internationals = item.faxNumber && formatPhoneNumber(value ? item.faxNumber : `+1${item.faxNumber}`, "International") || "International"
        if(!value && item.faxNumber){
          otherDetails.userFax[idx].faxNumber = Internationals || ""
        }
      })
      if(otherDetails){
        const types = ["userPhone", "userFax"]
        types.map(t => {
          if(otherDetails && otherDetails[t] && otherDetails[t].length){
            const primaryType = t === "userPhone" ? "phonePrimary" : "faxPrimary"
            const primary = otherDetails[t].filter(f => f[primaryType]) || []
            if(!primary.length){
              otherDetails[t][0][primaryType] = true
            }
          }
        })
      }
      saveEntityUserDetails(userId, otherDetails, this.saveCallback)
    })
  }

  saveAddressChanges() {
    this.setState({ waiting: true })
    const { userId, userAddresses } = this.state
    const updateData = { userAddresses: userAddresses || [] }
    saveEntityUserDetails(userId, updateData, this.saveCallback)
  }

  renderTag({ item }) {
    const className = ""  // item === PRIMARY_CONTACT ? "rw-multiselect-tag-no-clear" : ""
    return  (
      <span className={className}>{item}</span>
    )
  }

  sendOnBoardingEmail = () => {
    const { userId } = this.state
    const { nav2 } = this.props
    this.setState({
      loading: true
    }, async () => {
      const result = await sendOnBoardingEmailForMigratedUsers(nav2 || userId)
      this.setState({
        loading: false
      })
      if(result && result.done){
        toast(result.message, { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
      }else {
        toast(result.message, { autoClose: toastTimeout, type: toast.TYPE.ERROR })
      }
    })
  }

  onPhoneNumberChange = (e, nameProp, idx, key) => {
    const { businessAddress } = this.state
    let { userPhone, userFax } = this.state
    const Internationals = e && formatPhoneNumber(e, "International") || ""
    const errors = Internationals ? (isValidPhoneNumber(Internationals) ? undefined : "Invalid phone number") : ""
    if(nameProp === "userPhone"){
      userPhone = [...userPhone, ]
      if(userPhone && Array.isArray(userPhone)){
        if(userPhone.length){
          userPhone[idx] = {
            ...userPhone[idx],
            [key]: Internationals
          }
        }else {
          userPhone.push({
            [key]: Internationals
          })
        }
      }
    } else if(nameProp === "userFax"){
      userFax = [...userFax, ]
      if(userFax && Array.isArray(userFax)){
        if(userFax.length){
          userFax[idx] = {
            ...userFax[idx],
            [key]: Internationals
          }
        }else {
          userFax.push({
            [key]: Internationals
          })
        }
      }
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress,
      userPhone,
      userFax,
      error: {
        ...prevState.error,
        [nameProp]: {
          ...prevState.error[nameProp],
          [idx]: errors || ""
        }
      }
    }))
  }

  onSaveNotes = () => {
    const { notes, note, userId, edit, noteId } = this.state
    const { user } = this.props
    if(note){
      if(edit){
        const index = notes.findIndex(f => f._id === noteId)
        notes[index] = {
          note,
          createdAt: new Date(),
          updatedAt: new Date(),
          updatedByName: `${user && user.userFirstName || ""} ${user && user.userLastName || ""}`,
          updatedById: user && user.userId || "" ,
        }
      } else {
        notes.push({
          note,
          createdAt: new Date(),
          updatedAt: new Date(),
          updatedByName: `${user && user.userFirstName || ""} ${user && user.userLastName || ""}`,
          updatedById: user && user.userId || "" ,
        })
      }
      const payload = {
        notes
      }
      saveEntityUserDetails(userId, payload, (err, res) => this.saveNotesCallback(err, res, true))
    }
  }

  onNotesChange = (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  saveNotesCallback = async (err, res) => {
    if (!err) {
      this.setState({
        notes: res && res.notes || [],
        note: "",
        savemodalState: false,
        edit: false,
        noteId: ""
      })
      toast("Changes Saved", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      const { data } = err && err.response
      if( data && data.error ) {
        this.showErrorToast("Error in saving changes")
      }
      this.setState({ waiting: false })
    }
  }

  onEditNotes = (note) => {
    this.setState({
      noteId: note._id || "",
      edit: true,
      note: note && note.note || ""
    })
  }

  renderSection() {
    const { expanded, viewOnly } = this.state
    return (
      <article className={expanded.entityTypeSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <p onClick={this.toggleExpand.bind(this, "entityTypeSection")}>
            Entity Type</p>
        </div>
        {expanded.entityTypeSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content">
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={(() => { })}>
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderFirmUserExtraDetails() {
    const { userEmployeeID, userJobTitle, userManagerEmail, userJoiningDate,
      userExitDate, userCostCenter, error } = this.state
    return (
      <div>
        <hr />
        <div className="columns">
          <div className="column">
            <p className="multiExpLblBlk">
                Employee ID
            </p>
            <input
              className="input is-small is-link"
              type="text"
              name="userEmployeeID"
              placeholder="Emplyee Id"
              value={userEmployeeID}
              onChange={this.changeField}
            />
            {error && error.userEmployeeID ?
              <p className="text-error has-text-danger">
                {error.userEmployeeID}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
              Job Title
            </p>
            <input
              className="input is-small is-link"
              type="text"
              name="userJobTitle"
              placeholder="Job Title"
              value={userJobTitle}
              onChange={this.changeField}
            />
            {error && error.userJobTitle ?
              <p className="text-error has-text-danger">
                {error.userJobTitle}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
              {"Manager's Email"}
            </p>
            <input
              className="input is-small is-link"
              type="email"
              name="userManagerEmail"
              placeholder="Manager's Email"
              value={userManagerEmail}
              onChange={this.changeField}
            />
            {error && error.userManagerEmail ?
              <p className="text-error has-text-danger">
                {error.userManagerEmail}
              </p>
              : undefined
            }
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <p className="multiExpLblBlk">
                Joining Date
            </p>
            {/* <input
              className="input is-small is-link"
              type="date"
              name="userJoiningDate"
              placeholder="Joining Date"
              value={getCalendarDateAsString(userJoiningDate)}
              onChange={this.changeField}
            /> */}

            <TextLabelInput
              type="date"
              name="userJoiningDate"
              value={(userJoiningDate === "" || !userJoiningDate) ? null : new Date(userJoiningDate)}
              onChange={this.changeField}
            />

            {error && error.userJoiningDate ?
              <p className="text-error has-text-danger">
                {error.userJoiningDate}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
                Exit Date
            </p>
            {/* <input
              className="input is-small is-link"
              type="date"
              name="userExitDate"
              placeholder="Joining Date"
              value={getCalendarDateAsString(userExitDate)}
              onChange={this.changeField}
            /> */}

            <TextLabelInput
              type="date"
              name="userExitDate"
              value={(userExitDate === "" || !userExitDate) ? null : new Date(userExitDate)}
              onChange={this.changeField}
            />

            {error && error.userExitDate ?
              <p className="text-error has-text-danger">
                {error.userExitDate}
              </p>
              : undefined
            }
          </div>
          <div className="column">
            <p className="multiExpLblBlk">
              Cost Center
            </p>
            <input
              className="input is-small is-link"
              type="email"
              name="userCostCenter"
              placeholder="Cost Center"
              value={userCostCenter}
              onChange={this.changeField}
            />
            {error && error.userCostCenter ?
              <p className="text-error has-text-danger">
                {error.userCostCenter}
              </p>
              : undefined
            }
          </div>
        </div>
      </div>
    )
  }

  renderContactDetails() {
    const { expanded, viewOnly, error, userId, userEmails, userPhone,
      userFax, entity, modalType, modalIndex, modalState } = this.state
    if (!userId) {
      return null
    }
    const { entityType }  = entity || {}
    const otherEmails = userEmails ? userEmails.slice(1) : []
    if (!otherEmails.length) {
      otherEmails.push({ emailId: "", emailPrimary: false })
    }
    const phones = [...userPhone]
    if (!phones.length) {
      phones.push({ phoneNumber: "", extension: "", phonePrimary: false })
    }

    const fax = [...userFax]
    if (!fax.length) {
      fax.push({ faxNumber: "", faxPrimary: false })
    }
    return (
      <article className={expanded.otherDetailsSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <button className="borderless btn-accordion" onClick={this.toggleExpand.bind(this, "otherDetailsSection")}>
            <p>Other Details</p>
          </button>
        </div>
        {expanded.otherDetailsSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                      Other Emails
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  {modalType === "email" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Email ${(otherEmails[modalIndex] && otherEmails[modalIndex].emailId) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removeOtherEmail.bind(this, modalIndex)}>Delete</button>
                          </div>
                          {/* <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div> */}
                        </div>
                      </div>
                    </Modal>
                  }
                  {
                    otherEmails.map((e, i) => {
                      const { emailId } = e
                      return (
                        <div key={i} className="columns">
                          <div className="column">
                            <div className="field is-grouped-left">
                              <div className="control"><input
                                className="input is-small is-link"
                                type="email"
                                placeholder="johnsmith@example.com"
                                value={emailId}
                                onChange={this.changeOtherEmail.bind(this, i)}
                              /> </div>
                              {!viewOnly ?
                                <span className="has-text-link"
                                  onClick={this.toggleModal.bind(this, "email", i + 1)}>
                                  <i className="far fa-trash-alt"/>
                                </span> : null
                              }
                            </div>
                            {error && error.userEmails && error.userEmails[i + 1] ?
                              <p className="text-error has-text-danger">
                                {error.userEmails[i + 1]}
                              </p>
                              : undefined
                            }
                          </div>
                        </div>
                      )
                    })
                  }
                  {!viewOnly ?
                    <div>
                      <div className="field is-grouped is-pulled-left">
                        <div className="control">
                          <button className="button is-link is-small" type="button"
                            onClick={this.addOtherEmail}>Add More
                          </button>
                        </div>
                        <div className="control">
                          <button className="button is-light is-small" type="button"
                            onClick={this.resetOtherEmail}>Cancel
                          </button>
                        </div>
                      </div>
                    </div> : null
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                      Phone
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  {modalType === "phone" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Phone ${(phones[modalIndex] && phones[modalIndex].phoneNumber) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removePhone.bind(this, modalIndex)}>Delete</button>
                          </div>
                          {/* <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div> */}
                        </div>
                      </div>
                    </Modal>
                  }
                  {
                    phones.map((e, i) => {
                      const { phoneNumber, extension, phonePrimary } = e
                      const value = phoneNumber && phoneNumber.includes("+") || false
                      const Internationals = phoneNumber && formatPhoneNumber(value ? phoneNumber : `+1${phoneNumber}`, "International") || "International"
                      return (
                        <div key={i}>
                          <div className="field is-grouped-left">
                            <div className="control">
                              {/* <ZipCodeNumberDisableInput
                                format="+1 (###) ###-####"
                                mask="_"
                                className="input is-small is-link"
                                name="phoneNumber"
                                size="15"
                                value={phoneNumber || ""}
                                onChange={this.changePhone.bind(this, i)}
                              /> */}
                              <PhoneInput
                                value={ Internationals || "" }
                                onChange={event => {this.onPhoneNumberChange(event,"userPhone",i, "phoneNumber")}}
                              />
                              {/* <MaskedInput
                                mask={["+", "1", "(", /[0-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                                className="input is-small is-link is-fullwidth"
                                type="text"
                                name="phoneNumber"
                                placeholder="Phone Number"
                                value={phoneNumber}
                                onChange={this.changePhone.bind(this, i)}
                              /> */}
                            </div>
                            <div className="control">
                              <MaskedInput
                                mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                className="input is-small is-link"
                                type="text"
                                name="extension"
                                size={8}
                                placeholder="Ext"
                                value={extension}
                                onChange={this.changePhone.bind(this, i)}
                              />
                            </div>
                            {!viewOnly ?
                              <span>
                                {!phonePrimary &&
                                  <span className="has-text-link"
                                    onClick={this.toggleModal.bind(this, "phone", i)}>
                                    <i className="far fa-trash-alt"/>
                                  </span>
                                }
                              </span> : null
                            }
                          </div>
                          {error && error.userPhone && error.userPhone[i] ?
                            <p className="text-error has-text-danger">
                              {error.userPhone[i]}
                            </p>
                            : undefined
                          }
                          <div className="field is-grouped-left">
                            <p className="multiExpLblBlk">
                              is primary
                              <input
                                type="checkbox"
                                name="phonePrimary"
                                checked={!!phonePrimary}
                                onChange={this.changePhone.bind(this, i)}
                              />
                            </p>
                          </div>
                        </div>
                      )
                    })
                  }
                  {!viewOnly ?
                    <div>
                      <div className="field is-grouped is-pulled-left">
                        <div className="control">
                          <button className="button is-link is-small" type="button"
                            onClick={this.addPhone}>Add More
                          </button>
                        </div>
                        <div className="control">
                          <button className="button is-light is-small" type="button"
                            onClick={this.resetPhone}>Cancel
                          </button>
                        </div>
                      </div>
                    </div> : null
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                      Fax
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  {modalType === "fax" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Fax ${(fax[modalIndex] && fax[modalIndex].faxNumber) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.removeFax.bind(this, modalIndex)}>Delete</button>
                          </div>
                          {/* <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div> */}
                        </div>
                      </div>
                    </Modal>
                  }
                  {
                    fax.map((e, i) => {
                      const { faxNumber, faxPrimary } = e
                      const value = faxNumber && faxNumber.includes("+") || false
                      const Internationals = faxNumber && formatPhoneNumber(value ? faxNumber : `+1${faxNumber}`, "International") || "International"
                      return (
                        <div key={i}>
                          <div className="field is-grouped-left">
                            <div className="control">
                              {/* <ZipCodeNumberDisableInput
                                format="+1 (###) ###-####"
                                mask="_"
                                className="input is-small is-link"
                                name="faxNumber"
                                size="15"
                                placeholder="fax"
                                value={faxNumber || ""}
                                onChange={this.changeFax.bind(this, i)}
                              /> */}
                              <PhoneInput
                                value={ Internationals || "" }
                                onChange={event => {this.onPhoneNumberChange(event,"userFax",i, "faxNumber")}}
                              />
                            </div>
                            {!viewOnly ?
                              <span>
                                {!faxPrimary &&
                                  <span className="has-text-link"
                                    onClick={this.toggleModal.bind(this, "fax", i)}>
                                    <i className="far fa-trash-alt"/>
                                  </span>
                                }
                              </span> : null
                            }
                          </div>
                          {error && error.userFax && error.userFax[i] ?
                            <p className="text-error has-text-danger">
                              {error.userFax[i]}
                            </p>
                            : undefined
                          }
                          <div className="field is-grouped-left">
                            <p className="multiExpLblBlk">
                              is primary
                              <input
                                type="checkbox"
                                name="faxPrimary"
                                checked={!!faxPrimary}
                                onChange={this.changeFax.bind(this, i)}
                              />
                            </p>
                          </div>
                        </div>
                      )
                    })
                  }
                  {!viewOnly ?
                    <div>
                      <div className="field is-grouped is-pulled-left">
                        <div className="control">
                          <button className="button is-link is-small" type="button"
                            onClick={this.addFax}>Add More
                          </button>
                        </div>
                        <div className="control">
                          <button className="button is-light is-small" type="button"
                            onClick={this.resetFax}>Cancel
                          </button>
                        </div>
                      </div>
                    </div> : null
                  }
                </div>
              </div>
              {
                this.renderFirmUserExtraDetails()
              }
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={()=>this.showModal("other")}>
                          Save Other Details
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderBasicDetails() {
    const { expanded, entity, userFlags, userEntitlement, userStatus, disabledUserFlags,
      userFirstName, userMiddleName, userLastName, userEmailId, relatedEntites,
      error, viewOnly, currentUser, firmDetails, userFlagsListDisabled } = this.state
    const { entityId, nav3 } = this.props
    const { userLoginCredentials } = currentUser
    const [currentEntityName] = entityId ? [entity.entityName] :
      this.getEntityNameAndType(relatedEntites, currentUser.entityId)

    const userFlagsList = this.state.userFlagsList.filter(val => userFlagsListDisabled.indexOf(val) === -1)

    const sendOnboardingEmail1 = ( nav3 !== "migratedentities") && currentUser && currentUser.OnBoardingDataMigrationHistorical && currentUser.userLoginCredentials  !== "Done"
    const sendOnboardingEmail2 = currentUser && (currentUser.userLoginCredentials === "created" || currentUser.userLoginCredentials === "validate")

    return (
      <article className={expanded.userBasicSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <button className="borderless btn-accordion" onClick={this.toggleExpand.bind(this, "userBasicSection")}>
            <p>Primary Details</p>
          </button>
        </div>
        {expanded.userBasicSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content" >
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Firm Name
                    {!currentEntityName &&
                      <span className="icon has-text-danger">
                        <i className="fas fa-asterisk extra-small-icon" />
                      </span>
                    }
                  </p>
                  {currentEntityName || (firmDetails && firmDetails.relationshipType === "Undefined") ?
                    <Link to={urlHelper(
                      "entities",
                      firmDetails._id || "",
                      firmDetails.relationshipType || ""
                    )} className="is-size-7">{currentEntityName || (firmDetails && firmDetails.firmName) || ""}</Link> :
                    <Combobox
                      busy={!(relatedEntites && relatedEntites.length)}
                      data={relatedEntites}
                      textField='entityName'
                      caseSensitive={false}
                      minLength={1}
                      filter='contains'
                      onChange={this.changeEntity}
                      groupBy='entityType'
                      groupComponent={this.entityGroupHeading}
                    />
                  }
                </div>
                {
                  (sendOnboardingEmail1 || sendOnboardingEmail2) ?
                    <div className="column">
                      <div className="control">
                        <button className="button is-link is-small" onClick={this.sendOnBoardingEmail}>Send On Boarding Email</button>
                      </div>
                    </div>
                    : null
                }
                <div className="column">
                  <div className="switchContainer is-pulled-right">
                    <span className="filterText">{userStatus !== "inactive" ? "Active" : "Inactive"}</span>
                    <label className="switch" style={{ marginLeft: "10px" }}>
                      <input name="userstatus"
                        type="checkbox"
                        onChange={this.changeUserStatus}
                        checked={userStatus !== "inactive"}
                      />
                      <span className="customslider round" />
                    </label>
                  </div>
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    First Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <input
                    className="input is-small is-link"
                    type="text"
                    name="userFirstName"
                    placeholder="John"
                    value={userFirstName}
                    onChange={this.changeField}
                  />
                  {error && error.userFirstName ?
                    <p className="text-error has-text-danger">
                      {error.userFirstName}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                      Middle Name
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  <input
                    className="input is-small is-link"
                    type="text"
                    name="userMiddleName"
                    placeholder="M"
                    value={userMiddleName}
                    onChange={this.changeField}
                  />
                  {error && error.userMiddleName ?
                    <p className="text-error has-text-danger">
                      {error.userMiddleName}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">
                    Last Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <input
                    className="input is-small is-link"
                    type="text"
                    name="userLastName"
                    placeholder="Smith"
                    value={userLastName}
                    onChange={this.changeField}
                  />
                  {error && error.userLastName ?
                    <p className="text-error has-text-danger">
                      {error.userLastName}
                    </p>
                    : undefined
                  }
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Primary Email
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <input
                    className="input is-small is-link"
                    type="email"
                    disabled={userLoginCredentials && userLoginCredentials !== "created"}
                    name="userEmailId"
                    placeholder="johnsmith@example.com"
                    value={userEmailId}
                    onChange={this.changeField}
                  />
                  {error && error.userEmailId ?
                    <p className="text-error has-text-danger">
                      {error.userEmailId}
                    </p>
                    : undefined
                  }
                </div>
                { !viewOnly ?
                  <div className="column">
                    <div>
                      <p className="multiExpLblBlk">
                        System Access Level
                        <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                      </p>
                      <div className="select is-small is-link">
                        <select
                          value={userEntitlement}
                          name="userEntitlement"
                          onChange={this.changeField}
                        >
                          <option value="">Select access level</option>
                          {this.getAccessLevelList().map(t => (
                            <option key={t} value={t}>
                              {t}
                            </option>
                          ))}
                        </select>
                        {error && error.userEntitlement ?
                          <p className="text-error has-text-danger">
                            {error.userEntitlement}
                          </p>
                          : undefined
                        }
                      </div>
                    </div>
                  </div> : null
                }
                <div className="column">
                  <p className="multiExpLblBlk">
                      Contact Type
                    {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                  </p>
                  <Multiselect
                    data={userFlagsList}
                    disabled={disabledUserFlags || userFlagsListDisabled}
                    placeholder=" Select contact types"
                    caseSensitive={false}
                    minLength={1}
                    value={userFlags}
                    filter="contains"
                    tagComponent={this.renderTag}
                    onChange={this.changeContactTypes}
                  />
                  {error && error.userFlags ?
                    <p className="text-error has-text-danger">
                      {error.userFlags}
                    </p>
                    : undefined
                  }
                </div>
              </div>
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={()=>this.showModal("primary")}>
                          Save Primary Details
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderHistoricalData = () => {
    const {currentUser} = this.state
    const data = currentUser.historicalData || {}
    return <HistoricalData data={data}/>
  }

  renderAddress() {
    const { userId, waiting, userAddresses, error,
      viewOnly, entityAddress } = this.state

    if (!userId) {
      return null
    }

    return (
      <FirmAddressListForm
        key={parseInt(1 * 30, 10)}
        errorFirmDetail={error && error.addresses && error.addresses[0] || {}}
        onChangeAddressType={this.onChangeAddressType}
        canEdit={!viewOnly}
        canView
        busy={waiting}
        addressList={userAddresses}
        onSaveAddress={this.onSaveAddress}
        isUserAddress
        officeAddress={entityAddress || getInitialAddress()[0]}
      />
    )
  }

  renderNotes() {
    const { userId, notes, note, viewOnly } = this.state

    if (!userId) {
      return null
    }

    return (
      <Accordion multiple
        activeItem={[]}
        boxHidden
        render={({activeAccordions, onAccordion}) =>
          <div autoFocus tabIndex={0}>
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title="Notes"
            >
              {activeAccordions.includes(0) && (
                <div>
                  <NotesReactTable
                    notes={notes}
                    onEditNotes={this.onEditNotes}
                    canEdit={viewOnly || false}
                  />
                  <div className="columns">
                    <div className="column is-full">
                      <p className="multiExpLbl">Notes</p>
                      <div className="control">
                        <textarea className="textarea" name="note" value={note || ""} disabled={viewOnly || false} onChange={(e) => this.onNotesChange(e)}/>
                      </div>
                    </div>
                  </div>
                  { !viewOnly ?
                    <div className="columns">
                      <div className="column">
                        <button className="button is-link is-small center" disabled={!note}
                          onClick={()=>this.showModal("notes")}>Save
                        </button>
                      </div>
                    </div> : null
                  }
                </div>
              )}
            </RatingSection>
          </div>
        }
      />

    )
  }

  render() {
    const { waiting, generalError, currentUser, savemodalState  } = this.state
    if (generalError) {
      return (
        <div id="main">
          <strong className="text-error has-text-danger">{generalError}</strong>
        </div>
      )
    }
    return (
      <div id="main">
        {waiting && <Loader />}
        <section className="accordions">
        <SaveModal
            closeModal={this.toggleSaveModal}
            savemodalState={this.state.savemodalState}
            message={this.state.modalMessage}
            onConfirmed={this.state.onConfirmed}
          />
          {this.renderBasicDetails()}
          {this.renderContactDetails()}
          {this.renderAddress()}
          {this.renderNotes()}
          {currentUser && currentUser.OnBoardingDataMigrationHistorical ? this.renderHistoricalData() : null}
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})
export default connect( mapStateToProps, null)(UsersNew)
