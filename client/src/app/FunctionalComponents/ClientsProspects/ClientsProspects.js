import React, { Component } from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import { Multiselect, Combobox } from "react-widgets"
import MaskedInput from "react-text-mask"

import { Modal } from "Global/BulmaModal"
import EntityLookup from "Global/EntityLookup"
import BorrowerLookup from "Global/BorrowerLookup"
import Loader from "Global/Loader"

import {
  getPicklistValues,
  getEntityDetails,
  saveEntityDetails,
  getUsersByFlags,
  getEntityUsers,
  getCalendarDateAsString,
  updateAuditLog,
  getEntityAddressChangeLog,
  getUniqValueFromArray,
  checkNavAccessEntitlrment
} from "GlobalUtils/helpers"
import UserModal from "../Users/UsersModal"
import {SelectLabelInput, TextLabelInput} from "../../GlobalComponents/TextViewBox"
import FirmAddressListForm from "../EntityManagement/CommonComponents/FirmAddressListForm"
																				   
import HistoricalData from "../../GlobalComponents/HistoricalData"
import Notes from "../../GlobalComponents/Notes"

// Save Modal Definition
export const SaveModal = ({ message, closeModal, modalState, onConfirmed }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" autoFocus onClick={onConfirmed} >Yes</button>
            <button className="button is-light" onClick={closeModal} >No</button>
          </div>
        </footer>
      </div>
    </div>
  )
}

const defaultValues = {
  isMuniVisorClient: false
}

const userFieldsRequired = {
  "userFirstName": 1,
  "userLastName": 1,
  "userLoginCredentials.userEmailId": 1,
  "userPhone": 1
}

const ToastTimeout = 1000

const getInitialAddress = () => ([
  {
    addressName: "",
    isPrimary: false,
    isHeadQuarter: false,
    /* isActive: true, */
    website: "",
    officePhone: [
      {
        countryCode: "",
        phoneNumber: "",
        extension: ""
      }
    ],
    officeFax: [
      {
        faxNumber: ""
      }
    ],
    officeEmails: [
      {
        emailId: ""
      }
    ],
    addressLine1: "",
    addressLine2: "",
    country: "",
    state: "",
    city: "",
    zipCode: {
      zip1: "",
      zip2: ""
    },
    formatted_address: "",
    url: "",
    location: {
      longitude: "",
      latitude: ""
    }
  }
])

const getInitialState = () => ({
  expanded: {
    entityTypeSection: true,
    advisorSection: false,
    cusipSection: false,
    borrowerSection: false
  },
  advisorSelectOpen: false,
  primaryContactSelectOpen: false,
  viewOnly: true,
  entityId: "",
  entityName: "",
  entityAliases: [],
  aliasIndex: -1,
  issuerFlags: [],
  clientOrProspect: "Client",
  primarySectors: "",
  secondarySectors: "",
  taxId: "",
  firmLeadAdvisor: "",
  primaryContact: "",
  addresses: getInitialAddress(),
  entityLinkedCusips: [],
  entityBorObl: [],
  modalState: false,
  modalIndex: -1,
  modalType: "",
  error: {
    addresses: getInitialAddress()
  },
  changeLog: [],
  generalError: "",
  updateData: {}
})

class ClientsProspects extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...getInitialState(),
      waiting: true,
      marketRolesList: [],
      marketRole: [],
      legalTypes: [],
      legalRole: [],
      advisorsList: [],
      primarySectorsList: [],
      secondarySectorsList: {},
      issuerFlagsList: [],
      countryResult: [],
      debtTypes: [],
      borrowerRelationshipTypes: [],
      currentEntity: {},
      entityTypes: [],
      users: [],
      edit: false,
      notes: [],
    }
    this.onChangeEntityName = this
      .onChangeEntityName
      .bind(this)
    this.showAddUserModal = this
      .showAddUserModal
      .bind(this)
    this.getSavedUserDetails = this
      .getSavedUserDetails
      .bind(this)
    this.changePrimaryContact = this
      .changePrimaryContact
      .bind(this)
    this.changeField = this
      .changeField
      .bind(this)
    this.saveBasicDetails = this
      .saveBasicDetails
      .bind(this)
    this.saveAdvisorContactDetails = this
      .saveAdvisorContactDetails
      .bind(this)
    this.saveCallback = this
      .saveCallback
      .bind(this)
    this.changeIssuerFlags = this
      .changeIssuerFlags
      .bind(this)
    this.changeAdvisor = this
      .changeAdvisor
      .bind(this)
    this.onSaveAddress = this
      .onSaveAddress
      .bind(this)
    this.saveAddressChanges = this
      .saveAddressChanges
      .bind(this)
    this.toggleAdvisorSelect = this
      .toggleAdvisorSelect
      .bind(this)
    this.togglePrimaryContactSelect = this
      .togglePrimaryContactSelect
      .bind(this)
    this.getEntityData = this
      .getEntityData
      .bind(this)
    this.getEntityDetailsCallback = this
      .getEntityDetailsCallback
      .bind(this)
    this.addCusip = this
      .addCusip
      .bind(this)
    this.resetCusip = this
      .resetCusip
      .bind(this)
    this.addAlias = this
      .addAlias
      .bind(this)
    this.cancelAlias = this
      .cancelAlias
      .bind(this)
    this.saveCusipDetails = this
      .saveCusipDetails
      .bind(this)
    this.addBorrower = this
      .addBorrower
      .bind(this)
    this.resetBorrower = this
      .resetBorrower
      .bind(this)
    this.saveBorrowerDetails = this
      .saveBorrowerDetails
      .bind(this)
    this.saveBasicChangeLog = this
      .saveBasicChangeLog
      .bind(this)
    this.saveAdvisorContactChangeLog = this
      .saveAdvisorContactChangeLog
      .bind(this)
    this.saveCusipChangeLog = this
      .saveCusipChangeLog
      .bind(this)
    this.saveBorrowerChangeLog = this
      .saveBorrowerChangeLog
      .bind(this)
    this.saveAddressChangeLog = this
      .saveAddressChangeLog
      .bind(this)
    // this.toggleModal = this.toggleModal.bind(this)
  }

  componentWillMount() {
    if (this.state.modalState) {
      this.setState({
        modalState: false
      });
  }
  }

  toggleSaveModal = () => {
    this.setState(prev => {
      const newState = !prev.modalState

      return { modalState: newState }
    })
  }

  showModal = (operation) => {
  this.setState({
    modalState: true,
    onConfirmed: operation === "primary" ? this.saveBasicDetails
                                 : ((operation ==="Advisor/Contact") ? this.saveAdvisorContactDetails
                                 :((operation ==="CUSIPs") ? this.saveCusipDetails : this.saveBorrowerDetails)),
    modalMessage: operation === "primary" ? "Are you sure you want to save the entity details?"
    : ((operation ==="Advisor/Contact") ? "Are you sure you want to save Advisor/Contact?"
    : ((operation ==="CUSIPs") ? "Are you sure you want to save CUSIPs?" : "Are you sure you want to save Borrowers/Obligors?"))
  })
}

  async componentDidMount() {
    console.log("did mount")
    const { nav1, nav2 } = this.props
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit
    const clientOrProspect = nav1 === "addnew-prospect"
      ? "Prospect"
      : "Client"
    let users = []
    let usersReult

    if (!viewOnly && nav2 && nav1 !== "migratedentities") {
      usersReult = getEntityUsers(nav2, userFieldsRequired)
    }
    const getAdvisors = getUsersByFlags(["Series 50"])
    const result = await getPicklistValues([
      "LKUPENTITYTYPE",
      "LKUPPRIMARYSECTOR",
      "LKUPISSUERFLAG",
      // "LKUPCOUNTRY",
      "LKUPDEBTTYPE",
      "LKUPCRMBORROWER",
      "LKUPLEGALTYPE",
      "LKUPPARTICIPANTTYPE"
    ])
    console.log("result : ", result)
    const entityTypes = result[0] && result[0][1]
      ? result[0][1]
      : []
    const primarySectorsList = result[1] && result[1][1]
      ? result[1][1]
      : []
    const secondarySectorsList = result[1] && result[1][2]
      ? result[1][2]
      : {}
    let issuerFlagsList = result[2] && result[2][1]
      ? result[2][1]
      : []
    // const countryResult = result[3] || []
    const debtTypes = result[3] && result[3][1]
      ? result[3][1]
      : []
    const borrowerRelationshipTypes = result[4] && result[4][1]
      ? result[4][1]
      : []

    const disabledIssuerFlags = issuerFlagsList && issuerFlagsList.filter(f => !f.included).map(e => e.label)
    issuerFlagsList = issuerFlagsList && issuerFlagsList.map(e => e.label)

    let legalTypes = result[5] && result[5][1] ? result[5][1] : []
    const disabledLegalTypes = legalTypes && legalTypes.filter(f => !f.included).map(e => e.label)
    legalTypes = legalTypes && legalTypes.map(e => e.label)

    let participantTypesList = result[6] && result[6][1] ? result[6][1] : []
    const disabledMarketRole = participantTypesList && participantTypesList.filter(f => !f.included).map(e => e.label)
    participantTypesList = participantTypesList && participantTypesList.map(e => e.label)

    const marketRolesList = participantTypesList.filter(e => !legalTypes.includes(e))
    marketRolesList.unshift("Legal")

    console.log("entityTypes : ", entityTypes)
    const advisors = (await getAdvisors) || []
    const advisorsList = advisors.map(e => ({ _id: e._id, name: `${e.userFirstName} ${e.userLastName}` }))
    if (usersReult) {
      users = (await usersReult) || []
      users = users.map(u => ({
        primaryContactId: u._id,
        primaryContactEmail: u.userEmailId,
        primaryContactName: `${u.userFirstName} ${u.userLastName}`,
        primaryContactPhone: u.userPhone && u.userPhone.length
          ? u.userPhone[0].phoneNumber
          : ""
      }))
    }
    this.setState({
      entityTypes,
      primarySectorsList,
      secondarySectorsList,
      // countryResult,
      issuerFlagsList,
      disabledIssuerFlags,
      disabledMarketRole,
      disabledLegalTypes,
      advisorsList,
      debtTypes,
      borrowerRelationshipTypes,
      marketRolesList, legalTypes,
      users,
      entityId: nav2 || "",
      clientOrProspect,
      viewOnly,
      waiting: false
    }, this.getEntityData)
  }

  async componentWillReceiveProps(nextProps) {
    const { nav1, nav2 } = nextProps
    if (nav1 !== this.props.nav1) {
      const clientOrProspect = nav1 === "addnew-prospect"
        ? "Prospect"
        : "Client"
      this.setState({
        clientOrProspect
      }, this.getEntityData)
    }
    if (nav2 !== this.props.nav2 && nav1 !== "migratedentities") {
      const action = await checkNavAccessEntitlrment(nav2)
      const viewOnly = action && nav2 && !action.edit
      let users = await getEntityUsers(nav2, userFieldsRequired)
      users = users.map(u => ({
        primaryContactId: u._id,
        primaryContactEmail: u.userEmailId,
        primaryContactName: `${u.userFirstName} ${u.userLastName}`,
        primaryContactPhone: u.userPhone && u.userPhone.length
          ? u.userPhone[0].phoneNumber
          : ""
      }))
      this.setState({
        entityId: nav2 || "",
        viewOnly,
        users
      }, this.getEntityData)
    }
  }

  onSaveAddress(addressList) {
    console.log("addressList : ", addressList)
    this.setState({
      addressList
    }, this.saveAddressChanges)
  }

  onChangeEntityName(entityName) {
    // this.setState({ entityName })
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      error.entityName = ""
      return { entityName, error }
    })
  }

  // onChangeBorrowerName(i, borrowerName) {
  //   console.log("i, borrowerName : ", i, borrowerName)
	onChangeBorrowerName(i, elm) {
    console.log("i, borrowerName : ", i)
	const { name, value } = elm.target											 
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      error.entityBorObl = {}
      // error.entityBorObl[i] = {
      //   ...error.entityBorObl[i]
      // }
      // error.entityBorObl[i].borOblFirmName = ""
      const entityBorObl = [...prevState.entityBorObl]
      entityBorObl[i] = {
        ...entityBorObl[i]
      }
      // entityBorObl[i].borOblFirmName = borrowerName.firmName
	  entityBorObl[i].borOblFirmName = value
      return { entityBorObl, error }
    })
  }

  async getSavedUserDetails() {
    const { entityId } = this.state
    if (!entityId) {
      return
    }
    let users = await getEntityUsers(entityId, userFieldsRequired)
    users = users.map(u => ({
      primaryContactId: u._id,
      primaryContactEmail: u.userEmailId,
      primaryContactName: `${u.userFirstName} ${u.userLastName}`,
      primaryContactPhone: u.userPhone && u.userPhone.length
        ? u.userPhone[0].phoneNumber
        : ""
    }))
    this.setState({ users, primaryContact: users[0] })
  }

  getEntityDetailsCallback(err, res) {
    const { advisorsList } = this.state
    if (err) {
      this.setState({ waiting: false, generalError: "Error in getting entity data" })
    } else {
      console.log("res : ", res)
      const currentEntity = res[0] || {}
      if(currentEntity.relationshipType !== "Client" && currentEntity.relationshipType !== "Prospect" && currentEntity.relationshipType !== "Undefined") {
        this.setState({ generalError: "Wrong Entity Type provided for ${currentEntity.firmName}", waiting: false })
        return
      }
      const {
        entityType,
        firmName,
        taxId,
        addresses,
        entityFlags,
        entityAliases,
        primarySectors,
        secondarySectors,
        firmLeadAdvisor,
        relationshipType,
        entityLinkedCusips,
        entityBorObl,
        primaryContactId,
        notes
        // primaryContactFirstName,
        // primaryContactLastName,
        // primaryContactEmail
      } = currentEntity
      const { users } = this.state
      let  primaryContact = ""
      if(primaryContactId && users.length) {
        const idx = users.findIndex(e => e.primaryContactId === primaryContactId)
        if(idx > -1) {
          primaryContact = { ...users[idx] }
        }
      }
      // const primaryContact = primaryContactId
      //   ? {
      //     primaryContactId,
      //     primaryContactName: `${primaryContactFirstName} ${primaryContactLastName}`,
      //     primaryContactEmail
      //   }
      //   : ""

      const advisor = advisorsList.filter(e => e._id === firmLeadAdvisor)
      console.log("advisor : ", advisor)
      this.setState({
        waiting: false,
        generalError: "",
        currentEntity,
        entityAliases,
        aliasIndex: -1,
        clientOrProspect: relationshipType,
        entityType,
        entityName: {
          _id: "",
          firmName,
          participantType: entityType,
          isExisting: true
        },
        issuerFlags: (entityFlags && entityFlags.issuerFlags) || [],
        primarySectors,
        secondarySectors,
        taxId,
        firmLeadAdvisor: advisor,
        primaryContact,
        addresses,
        entityLinkedCusips,
        entityBorObl,
        notes: notes || [],
        error: {
          addresses: getInitialAddress()
        }
      })
    }
  }

  getEntityData() {
    const { entityId } = this.state
    if (entityId) {
      this.setState({ waiting: true })
      getEntityDetails(entityId, this.getEntityDetailsCallback)
    }
  }

  toggleExpand(val) {
    console.log("toggle : ", val)
    // e.preventDefault()
    this.setState(prevState => {
      const expanded = {
        ...prevState.expanded
      }
      Object
        .keys(expanded)
        .forEach(item => {
          if (item === val) {
            expanded[item] = !expanded[item]
          }
        })
      return { expanded }
    })
  }

  showAddUserModal() {
    this.toggleModal("addUser", -1)
  }

  changePrimaryContact(values) {
    const primaryContact = values.slice(-1)[0]
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      error.primaryContact = ""
      return { primaryContact, error }
    }, this.togglePrimaryContactSelect)
  }

  changeField(e) {
    const { name, type } = e.target
    const value = type === "checkbox"
      ? e.target.checked
      : e.target.value
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      error[name] = ""
      if (name === "taxId" && value.replace(/[^0-9]/g, "").length !== 9) {
        error.taxId = "Tax Id should have 9 digits"
      }
      let issuerFlags = [...prevState.issuerFlags]
      const { currentEntity } = prevState
      if (name === "entityType") {
        if (!["Governmental Entity / Issuer", "501c3 - Obligor"].includes(value)) {
          issuerFlags = []
        } else if (!issuerFlags.length && currentEntity.entityFlags && currentEntity.entityFlags.issuerFlags) {
          issuerFlags = [...currentEntity.entityFlags.issuerFlags]
        }
      }
      return { [name]: value, issuerFlags, error }
    })
  }

  changeIssuerFlags(issuerFlags) {
    this.setState({ issuerFlags })
  }

  toggleAdvisorSelect() {
    this.setState(prevState => ({
      advisorSelectOpen: !prevState.advisorSelectOpen
    }))
  }

  togglePrimaryContactSelect() {
    this.setState(prevState => ({
      primaryContactSelectOpen: !prevState.primaryContactSelectOpen
    }))
  }

  changeAdvisor(values) {
    const firmLeadAdvisor = values.slice(-1)
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      error.firmLeadAdvisor = ""
      return { firmLeadAdvisor, error }
    }, this.toggleAdvisorSelect)
  }

  toggleModal(modalType, modalIndex) {
    this.setState(prev => {
      if (prev.modalType === "addUser" && !modalType) {
        this.getSavedUserDetails()
      }
      const modalState = !prev.modalState
      return { modalState, modalType, modalIndex }
    })
  }

  changeCusipField(i, e) {
    const { name, value } = e.target
    this.setState(prevState => {
      const entityLinkedCusips = [...prevState.entityLinkedCusips]
      const error = {
        ...prevState.error
      }
      error.entityLinkedCusips = {}
      // error.entityLinkedCusips[i] = {
      //   ...error.entityLinkedCusips[i]
      // }
      // error.entityLinkedCusips[i][name] = ""
      entityLinkedCusips[i] = {
        ...entityLinkedCusips[i]
      }
      entityLinkedCusips[i][name] = value
      if (name === "associatedCusip6") {
        if (value) {
          if (value.length < 6) {
            error.entityLinkedCusips[i] = {
              ...error.entityLinkedCusips[i]
            }
            error.entityLinkedCusips[i].associatedCusip6 = "Please provide 6 characters CUSIP"
          } else if(/[^a-zA-Z0-9]/.test(value)){
            error.entityLinkedCusips[i] = {
              ...error.entityLinkedCusips[i]
            }
            error.entityLinkedCusips[i].associatedCusip6 = "special character not allowed"
          } else {
            entityLinkedCusips[i].associatedCusip6 = value.substring(0, 6)
          }
        }
      }
      return { entityLinkedCusips, error }
    })
  }

  removeCusip(i) {
    this.setState(prevState => {
      const entityLinkedCusips = [...prevState.entityLinkedCusips]
      entityLinkedCusips.splice(i, 1)
      const error = {
        ...prevState.error
      }
      error.entityLinkedCusips = {}
      return { entityLinkedCusips, modalIndex: -1, modalType: "", modalState: false, error }
    })
  }

  checkDupInArray(arr) {
    console.log("arr : ", arr)
    let result = [-1, -1]
    arr.some((e, i) => {
      if(e) {
        const dupIdx = arr.slice(i + 1).findIndex(d => d === e)
        if(dupIdx > -1) {
          result = [i, dupIdx+i+1]
          return true
        }
      }
    })
    console.log("dupResult : ", result)
    return result
  }

  checkEmptyArray(arr) {
    let result = -1
    arr.some((e, i) => {
      if(!e) {
        result = i
        return true
      }
    })
    return result
  }

  checkEmptyRows(rows) {
    let isEmpty
    rows.some((d, i) => {
      Object
        .keys(d)
        .some(k => {
          if(k !== "debtDescription"){
            if (!d[k]) {
              isEmpty = [i, k]
              return true
            }
          }
        })
      if (isEmpty) {
        return true
      }
    })
    console.log("isEmpty : ", isEmpty)
    return isEmpty
  }

  addCusip(e) {
    e.stopPropagation()
    this.setState(prevState => {
      const expanded = {
        ...prevState.expanded,
        cusipSection: true
      }
      const entityLinkedCusips = [...prevState.entityLinkedCusips]
      const error = {
        ...prevState.error
      }
      const [ei,
        ek] = this.checkEmptyRows(entityLinkedCusips) || []
      if (ek) {
        error.entityLinkedCusips = {
          ...error.entityLinkedCusips
        }
        error.entityLinkedCusips[ei] = {
          ...error.entityLinkedCusips[ei]
        }
        error.entityLinkedCusips[ei][ek] = "Please provide a value here or reset before adding another row"
        return { error }
      }
      error.entityLinkedCusips = {}

      entityLinkedCusips.unshift({ debtType: "", debtDescription: "", associatedCusip6: "" })
      return { entityLinkedCusips, expanded, error }
    })
  }

  resetCusip(e) {
    e.stopPropagation()
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityLinkedCusips = currentEntity
        .entityLinkedCusips
        .map(e => ({
          ...e
        }))
      const error = {
        ...prevState.error
      }
      error.entityLinkedCusips = {}
      return { entityLinkedCusips, modalType: "", modalIndex: -1, modalState: false, error }
    })
  }

  changeBorrowerField(i, e) {
    const { name, value } = e.target
    this.setState(prevState => {
      const entityBorObl = [...prevState.entityBorObl]
      const error = {
        ...prevState.error
      }
      error.entityBorObl = {}
      // error.entityBorObl[i] = {
      //   ...error.entityBorObl[i]
      // }
      // error.entityBorObl[i][name] = ""
      entityBorObl[i] = {
        ...entityBorObl[i]
      }
      entityBorObl[i][name] = value
      if (name === "borOblCusip6") {
        if (value) {
          if (value.length < 6) {
            error.entityBorObl[i] = {
              ...error.entityBorObl[i]
            }
            error.entityBorObl[i].borOblCusip6 = "Please provide 6 characters CUSIP"
          } else if(/[^a-zA-Z0-9]/.test(value)){
            error.entityBorObl[i] = {
              ...error.entityBorObl[i]
            }
            error.entityBorObl[i].borOblCusip6 = "special character not allowed"
          } else {
            entityBorObl[i].borOblCusip6 = value.substring(0, 6)
          }
        }
      }
      if (name === "borOblEndDate") {
        console.log("value1 : ", value)
        if (entityBorObl[i].borOblStartDate && value && new Date(entityBorObl[i].borOblStartDate) > new Date(value)) {
          console.log("1")
          error.entityBorObl[i] = {
            ...error.entityBorObl[i]
          }
          error.entityBorObl[i].borOblEndDate = "End date cannot be before the start date"
        } else {
          console.log("2")
          error.entityBorObl[i] = {
            ...error.entityBorObl[i]
          }
          error.entityBorObl[i].borOblEndDate = ""
        }
      }
      if (name === "borOblStartDate") {
        console.log("value2 : ", value)
        if (entityBorObl[i].borOblEndDate && value && new Date(entityBorObl[i].borOblEndDate) < new Date(value)) {
          console.log("3")
          error.entityBorObl[i] = {
            ...error.entityBorObl[i]
          }
          error.entityBorObl[i].borOblStartDate = "Start date cannot be after the end date"
        } else {
          console.log("4")
          error.entityBorObl[i] = {
            ...error.entityBorObl[i]
          }
          error.entityBorObl[i].borOblStartDate = ""
        }
      }
      return { entityBorObl, error }
    })
  }

  removeBorrower(i) {
    this.setState(prevState => {
      const entityBorObl = [...prevState.entityBorObl]
      entityBorObl.splice(i, 1)
      const error = {
        ...prevState.error
      }
      error.entityBorObl = {}
      return { entityBorObl, modalIndex: -1, modalType: "", modalState: false, error }
    })
  }

  addBorrower(e) {
    e.stopPropagation()
    this.setState(prevState => {
      const expanded = {
        ...prevState.expanded,
        borrowerSection: true
      }
      const entityBorObl = [...prevState.entityBorObl]
      // if (this.checkEmptyRows(entityBorObl)) {
      //   return {}
      // }
      const error = {
        ...prevState.error
      }
      const [ei,
        ek] = this.checkEmptyRows(entityBorObl.map(d => ({
        borOblRel: d.borOblRel,
        borOblFirmName: d.borOblFirmName,
        // borOblCusip6: d.borOblCusip6
      }))) || []
      if (ek) {
        error.entityBorObl = {
          ...error.entityBorObl
        }
        error.entityBorObl[ei] = {
          ...error.entityBorObl[ei]
        }
        error.entityBorObl[ei][ek] = "Please provide a value here or reset before adding another row"
        return { error }
      }

      error.entityBorObl = {}

      entityBorObl.unshift({
        borOblRel: "",
        borOblFirmName: "",
        borOblDebtType: "",
        borOblCusip6: "",
        borOblStartDate: null,
        borOblEndDate: null
      })
      return { entityBorObl, expanded, error }
    })
  }

  resetBorrower(e) {
    e.stopPropagation()
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityBorObl = currentEntity
        .entityBorObl
        .map(e => ({
          ...e
        }))
      const error = {
        ...prevState.error
      }
      error.entityBorObl = {}
      return { entityBorObl, modalType: "", modalIndex: -1, modalState: false, error }
    })
  }

  editAlias(aliasIndex) {
    this.setState({ aliasIndex })
  }

  cancelEditAlias(i) {
    console.log(" cancel i : ", i)
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityAliases = [...prevState.entityAliases]
      const newItems = currentEntity && currentEntity.entityAliases && currentEntity.entityAliases.length
        ? (entityAliases.length - currentEntity.entityAliases.length)
        : 0
      entityAliases[i] = currentEntity.entityAliases[(i - newItems)]
      return ({ entityAliases, aliasIndex: -1 })
    })
  }

  addAlias() {
    console.log("in addAlias")
    this.setState(prevState => {
      const error = {
        ...prevState.error
      }
      const entityAliases = [...prevState.entityAliases]
      if (entityAliases.length) {
        const emptyAliasIdx = this.checkEmptyArray(entityAliases)
        if(emptyAliasIdx > -1) {
          error.entityAliases = { ...error.entityAliases }
          error.entityAliases[emptyAliasIdx] = "Please provide a value here or reset before adding another Alias"
          return { error }
        }
      }
      error.entityAliases = ""
      entityAliases.unshift("")
      console.log("entityAliases : ", entityAliases)
      return { entityAliases, error }
    })
  }

  cancelAlias() {
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityAliases = [...(currentEntity.entityAliases || [])]
      const error = {
        ...prevState.error
      }
      error.entityAliases = ""
      return {
        entityAliases,
        modalType: "",
        modalIndex: -1,
        modalState: false,
        error,
        aliasIndex: -1
      }
    })
  }

  changeAlias(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const entityAliases = [...prevState.entityAliases]
      const error = {
        ...prevState.error
      }
      error.entityAliases = ""
      entityAliases[i] = value
      return { entityAliases, error }
    })
  }

  removeAlias(i) {
    this.setState(prevState => {
      const entityAliases = [...prevState.entityAliases]
      entityAliases.splice(i, 1)
      const error = {
        ...prevState.error
      }
      error.entityAliases = ""
      return { entityAliases, modalIndex: -1, modalType: "", modalState: false, error }
    })
  }

  arrayDiffs(oldArr=[], newArr=[]) {
    const removed = []
    const added = []
    oldArr.forEach(e => {
      if(e && !newArr.includes(e) && !removed.includes(e)) {
        removed.push(e)
      }
    })
    newArr.forEach(e => {
      if(e && !oldArr.includes(e) && !added.includes(e)) {
        added.push(e)
      }
    })
    return [removed, added]
  }

  saveBasicChangeLog(oldEntity, newEntity={}, relType) {
    const { auth } = this.props
    const { entityId } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const primitiveFields = {
      "entityType": "Entity Type",
      "firmName": "Firm Name",
      "msrbRegistrantType": "MSRB Registrant Type",
      "msrbId": "MSRB Id",
      "taxId": "Tax Id",
      "businessStructure": "Business Structure",
      "numEmployees": "Number of Employees",
      "annualRevenue": "Annual Revenue",
      "primarySectors": "Primary Sector",
      "secondarySectors": "Secondary Sector"
    }
    const changeLog = []
    if(relType !== oldEntity.relationshipType) {
      changeLog.push({
        userId,
        userName,
        log: oldEntity.relationshipType ?
          `Changed Relationship Type from ${oldEntity.relationshipType} to ${relType}`
          : `Added ${newEntity.firmName} as ${relType}`,
        date
      })
    }
    Object.keys(newEntity).forEach(k => {
      if(primitiveFields[k] && (oldEntity[k] !== newEntity[k])) {
        changeLog.push({
          userId,
          userName,
          log: oldEntity[k] ?
            `Changed ${primitiveFields[k]} from ${oldEntity[k]} to ${newEntity[k]}`
            : `Added ${newEntity[k]} as ${primitiveFields[k]}`,
          date
        })
      }
    })
    const oldFlags = (oldEntity.entityFlags && oldEntity.entityFlags.issuerFlags) || []
    const newFlags = (newEntity.entityFlags && newEntity.entityFlags.issuerFlags) || []
    const [removedFlags, addedFlags] = this.arrayDiffs(oldFlags, newFlags)
    if(removedFlags && removedFlags.length) {
      removedFlags.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Removed issuer type ${f}`,
          date
        })
      })
    }
    if(addedFlags && addedFlags.length) {
      addedFlags.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Added issuer type ${f}`,
          date
        })
      })
    }
    const oldAliases = (oldEntity.entityAliases) || []
    const newAliases = (newEntity.entityAliases) || []
    const [removedAliases, addedAliases] = this.arrayDiffs(oldAliases, newAliases)
    if(removedAliases && removedAliases.length) {
      removedAliases.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Removed alias ${f}`,
          date
        })
      })
    }
    if(addedAliases && addedAliases.length) {
      addedAliases.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Added alias ${f}`,
          date
        })
      })
    }
    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  saveAdvisorContactChangeLog(oldEntity, newEntity={}) {
    const { auth } = this.props
    const { entityId, advisorsList, users } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const changeLog = []

    let firmLeadAdvisorOld = oldEntity.firmLeadAdvisor
    let firmLeadAdvisorNew = newEntity.firmLeadAdvisor
    firmLeadAdvisorOld = advisorsList.filter(a => a._id === firmLeadAdvisorOld)[0]
    firmLeadAdvisorOld = firmLeadAdvisorOld && firmLeadAdvisorOld.name
    firmLeadAdvisorNew = advisorsList.filter(a => a._id === firmLeadAdvisorNew)[0]
    firmLeadAdvisorNew = firmLeadAdvisorNew && firmLeadAdvisorNew.name
    if(firmLeadAdvisorOld !== firmLeadAdvisorNew) {
      changeLog.push({
        userId,
        userName,
        log: firmLeadAdvisorOld ?
          (firmLeadAdvisorNew ? `Changed Lead Advisor from ${firmLeadAdvisorOld} to ${firmLeadAdvisorNew}`
            : `Removed ${firmLeadAdvisorOld} as Lead Advisor`)
          : `Added ${firmLeadAdvisorNew} as Lead Advisor`,
        date
      })
    }

    let contactOld = oldEntity.primaryContactId
    let contactNew = newEntity.primaryContactId
    contactOld = users.filter(a => a.primaryContactId === contactOld)[0]
    contactOld = contactOld && contactOld.primaryContactName
    contactNew = users.filter(a => a.primaryContactId === contactNew)[0]
    contactNew = contactNew && contactNew.primaryContactName
    if(contactOld !== contactNew) {
      changeLog.push({
        userId,
        userName,
        log: contactOld ?
          (contactNew ? `Changed Primary Contact from ${contactOld} to ${contactNew}`
            : `Removed ${contactOld} as Primary Contact`)
          : `Added ${contactNew} as Primary Contact`,
        date
      })
    }

    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  saveCusipChangeLog(oldEntity, newEntity={}) {
    const { auth } = this.props
    const { entityId } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const changeLog = []

    const fields = {
      debtType: "Debt Type",
      debtDescription: "Debt Description",
      associatedCusip6: "CUSIP"
    }

    const oldArr = [ ...oldEntity.entityLinkedCusips ].reverse()
    const newArr = [ ...newEntity.entityLinkedCusips ].reverse()

    const oldLen = oldArr.length
    const newLen = newArr.length

    const lenDiff = newLen - oldLen

    if(lenDiff > 0) {
      const newItems = newArr.slice(oldLen)
      newItems.forEach(e => {
        let values = ""
        Object.keys(fields).forEach(f => {
          if(e[f]) {
            values += ` ${fields[f]}: ${e[f]}`
          }
        })
        changeLog.push({
          userId,
          userName,
          log: `Added new linked cusip with values - ${values}`,
          date
        })
      })
    }

    newArr.slice(0, oldLen).forEach((e, i) => {
      Object.keys(fields).forEach(f => {
        if(newArr[i][f] !== oldArr[i][f]) {
          changeLog.push({
            userId,
            userName,
            log: `Changed value of ${fields[f]} from ${oldArr[i][f] || "blank"} to ${newArr[i][f] || "No value"} for a linked cusip with values`,
            date
          })
        }
      })
    })

    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  saveBorrowerChangeLog(oldEntity, newEntity={}) {
    const { auth } = this.props
    const { entityId } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const changeLog = []

    const fields = {
      borOblRel: "Relationship",
      borOblFirmName: "Firm Name",
      borOblDebtType: "Debt Type",
      borOblCusip6: "CUSIP",
      borOblStartDate: "Start Date",
      borOblEndDate: "End Date",
    }

    const oldArr = [ ...oldEntity.entityBorObl ].reverse()
    const newArr = [ ...newEntity.entityBorObl ].reverse()

    const oldLen = oldArr.length
    const newLen = newArr.length

    const lenDiff = newLen - oldLen

    if(lenDiff > 0) {
      const newItems = newArr.slice(oldLen)
      newItems.forEach(e => {
        let values = ""
        Object.keys(fields).forEach(f => {
          if(e[f]) {
            values += ` ${fields[f]}: ${e[f]}`
          }
        })
        changeLog.push({
          userId,
          userName,
          log: `Added new borrower/obligor with values - ${values}`,
          date
        })
      })
    }

    newArr.slice(0, oldLen).forEach((e, i) => {
      Object.keys(fields).forEach(f => {
        if(newArr[i][f] !== oldArr[i][f]) {
          changeLog.push({
            userId,
            userName,
            log: `Changed value of ${fields[f]} from ${oldArr[i][f] || "blank"} to ${newArr[i][f] || "No value"} for a borrower/obligor with values`,
            date
          })
        }
      })
    })

    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  saveAddressChangeLog(oldEntity, newEntity={}) {
    const { auth } = this.props
    const { entityId } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const changeLog = getEntityAddressChangeLog(oldEntity.addresses, newEntity.addresses)
    console.log("changeLog : ", changeLog)
    changeLog.forEach(e => ({
      userId,
      userName,
      ...e,
      date
    }))
    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  saveCallback = async (err, res) => {
    const {auth} = this.props
    if (!err) {
      const { _id } = res
      if (_id) {
        this.setState({ entityId: _id })
        // this.props.history.push(`/add-new-taks/${_id}`)
      }
      if(this.props.nav1 === "migratedentities"){
        if(auth && auth.userEntities && auth.userEntities.entityId){
          // await this.props.getURLsForTenant(auth.userEntities.entityId)
        }
        if(this.state.clientOrProspect === "Third Party"){
          this.props.history.push("/mast-thirdparty")
        }else {
          this.props.history.push("/mast-cltprosp")
        }
      }
      this.getEntityData()
      toast("Changes Saved", {
        autoClose: ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
    } else {
      this.setState({ waiting: false })
      toast("Error in saving changes", {
        autoClose: ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  checkNonEmptyErrorKey(error, keys) {
    if (!error) {
      return false
    }
    if (Object.getPrototypeOf(error).constructor.name === "Object") {
      keys = keys || Object
        .keys(error)
        .filter(k => k !== "addresses")
      if (!keys.length) {
        return false
      }
      let nonEmptyError = false
      keys.some(k => {
        if (this.checkNonEmptyErrorKey(error[k])) {
          nonEmptyError = true
          return true
        }
      })
      return nonEmptyError
    }
    return true
  }

  checkAnyError(keys) {
    const { error } = this.state
    return this.checkNonEmptyErrorKey(error, keys)
  }

  saveBasicDetails = async () => {
    this.setState({ waiting: true, modalState: false })
    const {
      entityId,
      clientOrProspect,
      entityType,
      entityName,
      issuerFlags,
      primarySectors,
      secondarySectors,
      taxId,
      entityAliases,
      currentEntity,
      legalRole,
      marketRole
    } = this.state
    const keys = [
      "entityType",
      "entityName",
      "issuerFlags",
      "primarySectors",
      "secondarySectors",
      "taxId"
    ]
    if(clientOrProspect === "Third Party"){
      keys.splice(0,1)
    }
    if (this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      return
    }
    const requiredFields = clientOrProspect === "Third Party" ? ["entityName"] : ["entityType", "entityName"]
    let emptyRequiredField = false
    requiredFields.forEach(d => {
      if (!this.state[d]) {
        emptyRequiredField = true
        this.setState(prevState => {
          const error = {
            ...prevState.error
          }
          error[d] = "Please provide a value"
          return { error, waiting: false }
        })
      }
    })
    if (emptyRequiredField) {
      this.setState({ waiting: false })
      return
    }
    const emptyAliasIdx = this.checkEmptyArray(entityAliases)
    console.log("emptyAliasIdx : ", emptyAliasIdx)
    if(emptyAliasIdx > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityAliases = { ...error.entityAliases }
        error.entityAliases[emptyAliasIdx] = "Please provide a value"
        return { error, waiting: false }
      })
      return
    }
    const dupResult = this.checkDupInArray(entityAliases)
    if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityAliases = { ...error.entityAliases }
        error.entityAliases[dupResult[0]] = "Duplicate values are not allowed"
        error.entityAliases[dupResult[1]] = "Duplicate values are not allowed"
        return { error, waiting: false }
      })
      return
    }
    const firmName = entityName
      ? entityName.firmName || ""
      : ""
    const basicDetails = {
      entityType: clientOrProspect === "Third Party" ? firmName : entityType,
      firmName,
      entityAliases,
      msrbFirmName: firmName,
      entityFlags: clientOrProspect === "Third Party" ?
        { marketRole: await getUniqValueFromArray([...marketRole, ...legalRole]) } :
        { issuerFlags },
      primarySectors,
      secondarySectors,
      taxId,
      ...defaultValues
    }
    this.saveBasicChangeLog(currentEntity, basicDetails, clientOrProspect)
    saveEntityDetails(this.props.nav1 === "migratedentities" && clientOrProspect !== "Undefined" ? `${entityId}?type=migrated` : entityId, basicDetails, clientOrProspect, this.saveCallback)
  }

  saveAdvisorContactDetails() {
    this.setState({ waiting: true, modalState: false })
    const { entityId, firmLeadAdvisor, primaryContact, currentEntity } = this.state
    const firmLeadAdvisorId = firmLeadAdvisor && firmLeadAdvisor[0]
      ? firmLeadAdvisor[0]._id
      : ""
    const updateData = {
      firmLeadAdvisor: firmLeadAdvisorId,
      primaryContactId: (primaryContact && primaryContact.primaryContactId) || null
    }
    // if((!currentEntity.firmLeadAdvisor && !newValue) ||
    // (currentEntity.firmLeadAdvisor === newValue)) {   return }
    this.saveAdvisorContactChangeLog(currentEntity, updateData)
    saveEntityDetails(entityId, updateData, "", this.saveCallback)
  }

  saveAddressChanges() {
    console.log("in saveAddressChanges")
    this.setState({ waiting: true })
    const { auth } = this.props
    const { entityId, addressList, currentEntity } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const updateData = { addresses: addressList || [] }
    updateAuditLog(entityId, [{
      userId,
      userName,
      log: "Currently Address list updated",
      date
    }])
    // this.saveAddressChangeLog(currentEntity, updateData)
    saveEntityDetails(entityId, updateData, "", this.saveCallback)
  }

  saveCusipDetails(e) {
    e.stopPropagation()
    this.setState({ waiting: true, modalState: false })
    const { entityId, entityLinkedCusips, currentEntity } = this.state
    if (!entityLinkedCusips || !entityLinkedCusips.length) {
      this.setState({ waiting: false })
      return
    }
    const keys = ["entityLinkedCusips"]
    if (this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      return
    }
    const requiredFields = ["debtType", "associatedCusip6"]
    let emptyRequiredField = false
    entityLinkedCusips.forEach((c, i) => {
      requiredFields.forEach(d => {
        if (!entityLinkedCusips[i][d]) {
          emptyRequiredField = true
          this.setState(prevState => {
            const error = {
              ...prevState.error
            }
            error.entityLinkedCusips = {
              ...error.entityLinkedCusips
            }
            error.entityLinkedCusips[i] = {
              ...error.entityLinkedCusips[i]
            }
            error.entityLinkedCusips[i][d] = "Please provide a value"
            return { error }
          })
        }
      })
    })
    if (emptyRequiredField) {
      this.setState({ waiting: false })
      return
    }
    const require = ["associatedCusip6"]
    let emptyRequired = false
    entityLinkedCusips.forEach((c, i) => {
      require.forEach(d => {
        if (/[^a-zA-Z0-9]/.test(entityLinkedCusips[i][d])) {
          emptyRequired = true
          this.setState(prevState => {
            const error = {
              ...prevState.error
            }
            error.entityLinkedCusips = {
              ...error.entityLinkedCusips
            }
            error.entityLinkedCusips[i] = {
              ...error.entityLinkedCusips[i]
            }
            error.entityLinkedCusips[i][d] = "special character not allowed"
            return { error }
          })
        }
      })
    })
    if (emptyRequired) {
      this.setState({ waiting: false })
      return
    }
    const dupResult = this.checkDupInArray(entityLinkedCusips.map(d => d.associatedCusip6))
    if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityLinkedCusips = { ...error.entityLinkedCusips }
        error.entityLinkedCusips[dupResult[0]] = { ...error.entityLinkedCusips[dupResult[0]] }
        error.entityLinkedCusips[dupResult[0]].associatedCusip6 = "Duplicate values are not allowed"
        error.entityLinkedCusips[dupResult[1]] = { ...error.entityLinkedCusips[dupResult[1]] }
        error.entityLinkedCusips[dupResult[1]].associatedCusip6 = "Duplicate values are not allowed"
        return { error, waiting: false }
      })
      return
    }
    const updateData = {
      entityLinkedCusips
    }
    this.saveCusipChangeLog(currentEntity, updateData)
    saveEntityDetails(entityId, updateData, "", this.saveCallback)
  }

  saveBorrowerDetails(e) {
    e.stopPropagation()
    this.setState({ waiting: true, modalState: false })
    const { entityId, entityBorObl, currentEntity } = this.state
    if (!entityBorObl || !entityBorObl.length) {
      this.setState({ waiting: false })
      return
    }
    const keys = ["entityBorObl"]
    if (this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      return
    }
    const requiredFields = ["borOblRel", "borOblFirmName"]
    let emptyRequiredField = false
    entityBorObl.forEach((c, i) => {
      requiredFields.forEach(d => {
        if (!entityBorObl[i][d]) {
          emptyRequiredField = true
          this.setState(prevState => {
            const error = {
              ...prevState.error
            }
            error.entityBorObl = {
              ...error.entityBorObl
            }
            error.entityBorObl[i] = {
              ...error.entityBorObl[i]
            }
            error.entityBorObl[i][d] = "Please provide a value"
            return { error }
          })
        }
      })
    })
    if (emptyRequiredField) {
      this.setState({ waiting: false })
      return
    }
    const require = ["borOblCusip6"]
    let emptyRequired = false
    entityBorObl.forEach((c, i) => {
      require.forEach(d => {
        if (/[^a-zA-Z0-9]/.test(entityBorObl[i][d])) {
          emptyRequired = true
          this.setState(prevState => {
            const error = {
              ...prevState.error
            }
            error.entityBorObl = {
              ...error.entityBorObl
            }
            error.entityBorObl[i] = {
              ...error.entityBorObl[i]
            }
            error.entityBorObl[i][d] = "special character not allowed"
            return { error }
          })
        }
      })
    })
    if (emptyRequired) {
      this.setState({ waiting: false })
      return
    }
    const dupCheckFields = ["borOblFirmName", "borOblCusip6"]
    let dupField = false
    dupCheckFields.some(f => {
      const dupResult = this.checkDupInArray(entityBorObl.map(d => d[f]))
      if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
        this.setState(prevState => {
          const error = { ...prevState.error }
          error.entityBorObl = { ...error.entityBorObl }
          error.entityBorObl[dupResult[0]] = { ...error.entityBorObl[dupResult[0]] }
          error.entityBorObl[dupResult[0]][f] = "Duplicate values are not allowed"
          error.entityBorObl[dupResult[1]] = { ...error.entityBorObl[dupResult[1]] }
          error.entityBorObl[dupResult[1]][f] = "Duplicate values are not allowed"
          return { error }
        })
        dupField = true
        return true
      }
    })
    if (dupField) {
      this.setState({ waiting: false })
      return
    }
    const updateData = {
      entityBorObl
    }
    this.saveBorrowerChangeLog(currentEntity, updateData)
    saveEntityDetails(entityId, updateData, "", this.saveCallback)
  }

  changeMarketRole = (marketRole) => {
    if (marketRole.includes("Legal")) {
      this.setState(prevState => {
        if (prevState.marketRole.includes("Legal")) {
          return { marketRole }
        }
        return { marketRole, legalRole: prevState.legalTypes }
      })
    } else {
      this.setState({ marketRole, legalRole: [] })
    }
  }

  changeLegalRole = (legalRole) => {
    this.setState({ legalRole })
  }

  renderBasicDetails() {
    const {
      entityId,
      expanded,
      clientOrProspect,
      entityTypes,
      entityType,
      entityAliases,
      entityName,
      issuerFlags,
      primarySectors,
      primarySectorsList,
      secondarySectors,
      secondarySectorsList,
      issuerFlagsList,
      disabledIssuerFlags,
      disabledMarketRole,
      disabledLegalTypes,
      taxId,
      error,
      currentEntity,
      modalState,
      modalType,
      modalIndex,
      aliasIndex,
      viewOnly,
      marketRole,
      marketRolesList,
      legalTypes,
      legalRole
    } = this.state
    const entityLookUpViewOnly = this.props.nav1 === "migratedentities" ?
      !!entityId && !(entityName && entityName.firmName && entityName.firmName.includes("Unknown Firm")) : !!entityId
    return (
      <article
        className={expanded.entityTypeSection
          ? "accordion is-active"
          : "accordion"}>
        <div className="accordion-header toggle">
          <p
            onClick={this
              .toggleExpand
              .bind(this, "entityTypeSection")}>
            Entity Type</p>
        </div>
        {expanded.entityTypeSection
          ? <div
            className={viewOnly
              ? "accordion-body view-only-mode"
              : "accordion-body"}>
            <div className="accordion-content">
              <div className="control field">
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Client"
                    disabled={viewOnly || false}
                    checked={clientOrProspect === "Client"}
                    onChange={this.changeField} />
                  Client
                </label>
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Prospect"
                    disabled={viewOnly || false}
                    checked={clientOrProspect === "Prospect"}
                    onChange={this.changeField} />
                  Prospect
                </label>
                {
                  this.props.nav1 === "migratedentities" ?
                    <label className="radio">
                      <input
                        className="radio"
                        name="clientOrProspect"
                        type="radio"
                        value="Third Party"
                        disabled={viewOnly || false}
                        checked={clientOrProspect === "Third Party"}
                        onChange={this.changeField} />
                      Third Party
                    </label> : null
                }
              </div>
              <div className="columns">
                {
                  this.props.nav1 === "migratedentities" && (clientOrProspect === "Prospect" || clientOrProspect === "Client") ?
                    <div className="column is-one-third">
                      <p className="multiExpLblBlk">
                        Entity Type
                        <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                      </p>
                      <SelectLabelInput
                        placeholder="Select Entity Type"
                        list={entityTypes || []}
                        name="entityType"
                        value={entityType || ""}
                        disabled={viewOnly || false}
                        error={error.entityType || ""}
                        onChange={this.changeField}
                      />
                    </div>
                    : null
                }
                {
                  this.props.nav1 !== "migratedentities" && clientOrProspect !== "Third Party" ?
                    <div className="column is-one-third">
                      <p className="multiExpLblBlk">
                        Entity Type
                        <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                      </p>
                      <SelectLabelInput
                        placeholder="Select Entity Type"
                        list={entityTypes || []}
                        name="entityType"
                        value={entityType || ""}
                        disabled={viewOnly || false}
                        error={error.entityType || ""}
                        onChange={this.changeField}
                      />
                    </div>
                    : null
                }
                {["Governmental Entity / Issuer", "501c3 - Obligor"].includes(entityType)
                  ? <div className="column">
                    <p className="multiExpLblBlk">Issuer Type</p>
                    <Multiselect
                      data={issuerFlagsList || []}
                      placeholder=" Select issuer types"
                      caseSensitive={false}
                      minLength={1}
                      value={issuerFlags}
                      filter="contains"
                      disabled={disabledIssuerFlags || viewOnly || false}
                      onChange={this.changeIssuerFlags} /> {error && error.issuerFlags
                      ? <p className="has-text-danger">
                        {error.issuerFlags}
                      </p>
                      : undefined
                    }
                  </div>
                  : undefined
                }
                <div className="column">
                  <p className="multiExpLblBlk">
                    Entity Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>

                  <EntityLookup
                    entityName={entityName}
                    onChange={this.onChangeEntityName}
                    participantType={entityType}
                    viewOnly={entityLookUpViewOnly}
                    entityType="Prospects/Clients"
                    error={(error && error.entityName) || ""}
                    isRequired={false}
                    isHide={entityName.firmName && !entityLookUpViewOnly}
                  />
                  {modalType === "alias" && <Modal
                    closeModal={this.toggleModal.bind(this, "", -1)}
                    modalState={modalState}
                    showBackground
                    title={`Delete Alias ${entityAliases[modalIndex]}`}>
                    <div>
                      <div className="field is-grouped">
                        <div className="control">
                          <button
                            className="button is-link is-small"
                            type="button"
                            autoFocus
                            onClick={this.removeAlias.bind(this, modalIndex)}>Delete</button>
                        </div>
                        <div className="control">
                          <button
                            className="button is-light is-small"
                            type="button"
                            onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                        </div>
                      </div>
                    </div>
                  </Modal>
                  }
                  {currentEntity && currentEntity.entityAliases && currentEntity.entityAliases.length
                    ? <h6 className="title is-6">Aliases</h6>
                    : undefined
                  }
                  {entityAliases.map((a, i) => {
                    if (!(currentEntity && currentEntity.entityAliases && currentEntity.entityAliases.length) || (i < (entityAliases.length - currentEntity.entityAliases.length))) {
                      return (
                        <div key={i} className="block">
                          <div className="field is-grouped">
                            <input
                              className="input is-small is-link"
                              type="text"
                              placeholder="Enter alias"
                              value={a}
                              custom={i}
                              onChange={this.changeAlias.bind(this, i)} />
                              <button 
                                className="button is-small borderless" 
                                aria-label={`Delete alias ${i+1}`}
                                onClick={this.toggleModal.bind(this, "alias", i)}>
                                <span className="has-text-danger">
                                  <i className="far fa-trash-alt" />
                                </span>
                              </button>
                          </div>
                          {error && error.entityAliases && error.entityAliases[i]
                            ? <p className="text-error has-text-danger">
                              {error.entityAliases[i]}
                            </p>
                            : undefined
                          }
                        </div>
                      )
                    }
                    if (aliasIndex === i) {
                      return (
                        <div key={i} className="block">
                          <div className="field is-grouped">
                            <input
                              className="input is-small is-link"
                              type="text"
                              placeholder="alias"
                              value={a}
                              onChange={this.changeAlias.bind(this, i)} />
                            <span className="has-text-danger">
                              <i className="fa fa-times"
                                onClick={this.cancelEditAlias.bind(this, i)} />
                            </span>
                          </div>
                          {error && error.entityAliases && error.entityAliases[i]
                            ? <p className="has-text-danger">
                              {error.entityAliases[i]}
                            </p>
                            : undefined
                          }
                        </div>
                      )
                    }
                    return (
                      <span className="level span-with-border-radius" key={i}>
                        {a}
                        { !viewOnly ?
                          <span className="has-text-danger is-pulled-right">
                            <i className="fa fa-pencil"
                              onClick={this.editAlias.bind(this, i)}/>
                          </span> : null
                        }
                      </span>
                    )
                  })
                  }
                  { !viewOnly ?
                    <div className="column">
                      <div className="level-right">
                        <div className="field is-grouped">
                          <div className="control">
                            <button
                              className="button is-link is-small"
                              type="button"
                              onClick={this.addAlias}>Add Alias
                            </button>
                          </div>
                          <div className="control">
                            <button
                              className="button is-light is-small"
                              type="button"
                              onClick={this.cancelAlias}>Cancel
                            </button>
                          </div>
                        </div>
                      </div>
                    </div> : null
                  }
                </div>
              </div>
              { this.props.nav1 === "migratedentities" && clientOrProspect === "Third Party" ?
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLblBlk">Market Role</p>
                    <Multiselect
                      data={marketRolesList || []}
                      placeholder=" Select market role"
                      caseSensitive={false}
                      minLength={1}
                      value={marketRole}
                      filter="contains"
                      disabled={disabledMarketRole || viewOnly || false}
                      onChange={this.changeMarketRole}
                    />
                    {error && error.marketRole ?
                      <p className="has-text-danger">
                        {error.marketRole}
                      </p>
                      : undefined
                    }
                  </div>
                  {
                    marketRole.includes("Legal") ?
                      <div className="column">
                        <p className="multiExpLblBlk">Legal Type</p>
                        <Multiselect
                          data={legalTypes || []}
                          placeholder=" Select legal types"
                          caseSensitive={false}
                          minLength={1}
                          value={legalRole}
                          filter="contains"
                          disabled={disabledLegalTypes || viewOnly || false}
                          onChange={this.changeLegalRole}
                        />
                        {error && error.legalRole ?
                          <p className="has-text-danger">
                            {error.legalRole}
                          </p>
                          : undefined
                        }
                      </div>
                      : undefined
                  }
                </div>
                : null
              }
              { this.props.nav1 !== "migratedentities" ?
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLblBlk">Primary Sector</p>
                    <div className="select is-small is-link">
                      <select
                        value={primarySectors}
                        name="primarySectors"
                        disabled={viewOnly || false}
                        onChange={this.changeField}>
                        <option value="">Select Primary Sector</option>
                        {primarySectorsList.map(t => (
                          <option key={t.label} value={t.label} disabled={!t.included}>
                            {t.label}
                          </option>
                        ))}
                      </select>
                      {error && error.primarySectors
                        ? <p className="has-text-danger">
                          {error.primarySectors}
                        </p>
                        : undefined
                      }
                    </div>
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">Secondary Sector</p>
                    <div className="select is-small is-link">
                      <select
                        value={secondarySectors}
                        name="secondarySectors"
                        disabled={viewOnly || false}
                        onChange={this.changeField}>
                        <option value="">Select Secondary Sector</option>
                        {primarySectors && secondarySectorsList && secondarySectorsList[primarySectors] && secondarySectorsList[primarySectors].map(t => (
                          <option key={t.label} value={t.label} disabled={!t.included}>
                            {t.label}
                          </option>
                        ))
                        }
                      </select>
                      {error && error.secondarySectors
                        ? <p className="has-text-danger">
                          {error.secondarySectors}
                        </p>
                        : undefined
                      }
                    </div>
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">Tax Id</p>
                    <div>
                      <MaskedInput
                        mask={[/\d/,/\d/,/\d/,"-",/\d/,/\d/,"-",/\d/,/\d/,/\d/,/\d/]}
                        className="input is-small is-link"
                        type="text"
                        name="taxId"
                        placeholder="Enter Tax ID"
                        value={taxId}
                        disabled={viewOnly || false}
                        onChange={this.changeField} /> {error && error.taxId
                        ? <p className="has-text-danger">
                          {error.taxId}
                        </p>
                        : undefined
                      }
                    </div>
                  </div>
                </div>
                : null
              }
              {!viewOnly && <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button className="button is-link" onClick={()=>this.showModal("primary")} disabled={clientOrProspect === "Undefined"}>
                        Save {this.props.nav1 === "migratedentities" ? "Migrated" : clientOrProspect}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderAdvisorDetails() {
    const {
      entityId,
      expanded,
      advisorsList,
      firmLeadAdvisor,
      error,
      advisorSelectOpen,
      viewOnly,
      users,
      primaryContact,
      primaryContactSelectOpen,
      modalType,
      modalState,
      userEmailId,
      userFirstName,
      userMiddleName,
      userLastName,
      userPhone
    } = this.state
    const { primaryContactPhone, primaryContactEmail } = primaryContact || {}
    if (!entityId) {
      return null
    }
    return (
      <article
        className={expanded.advisorSection
          ? "accordion is-active"
          : "accordion"}>
        <div className="accordion-header toggle">
            <button className="borderless btn-accordion" onClick={this
                .toggleExpand
                .bind(this, "advisorSection")}>
              <p> Advisor/Primary Contact Information</p>
            </button>
        </div>
        {expanded.advisorSection
          ? <div
            className={viewOnly
              ? "accordion-body view-only-mode"
              : "accordion-body"}>
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Lead Advisor
                  </p>
                  <Multiselect
                    data={advisorsList}
                    textField="name"
                    placeholder=" Select lead advisor"
                    caseSensitive={false}
                    minLength={1}
                    open={advisorSelectOpen}
                    onToggle={this.toggleAdvisorSelect}
                    value={firmLeadAdvisor || []}
                    filter="contains"
                    disabled={viewOnly || false}
                    onChange={this.changeAdvisor} /> {error && error.firmLeadAdvisor
                    ? <p className="has-text-danger">
                      {error.firmLeadAdvisor}
                    </p>
                    : undefined
                  }
                </div>
                <div className="column">
                  {(users && users.length)
                    ? <div>
                      <p className="multiExpLblBlk">
                        Primary Contact
                      </p>
                      <Multiselect
                        data={users}
                        textField="primaryContactName"
                        placeholder="Select primary contact"
                        caseSensitive={false}
                        minLength={1}
                        open={primaryContactSelectOpen}
                        onToggle={this.togglePrimaryContactSelect}
                        value={primaryContact ? [primaryContact] : []}
                        disabled={viewOnly || false}
                        filter="contains"
                        onChange={this.changePrimaryContact} />
                    </div>
                    : undefined
                  }
                </div>
              </div>
              <div className="columns">
                {modalType === "addUser" && <Modal
                  closeModal={this
                    .toggleModal
                    .bind(this, "", -1)}
                  modalState={modalState}
                  contentClass="container"
                  showBackground
                  title="Add Primary Contact User">
                  <div>
                    <UserModal
                      entityId={entityId}
                      afterSaveUser={this.toggleModal.bind(this, "", -1)}
                      hideContactType
                      primaryContact
                      hideUserStatusToggle />
                  </div>
                </Modal>
                }
                {viewOnly
                  ? undefined
                  : (users && users.length)
                    ? undefined
                    : <div className="column">
                      <a className="has-text-link" onClick={this.showAddUserModal}>
                          No users found. Add Primary contact user
                      </a>
                    </div>
                }
              </div>
              {primaryContactEmail && <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Primary Contact Email
                  </p>
                  <p className="is-size-7">{primaryContactEmail}</p>
                </div>
                {primaryContactPhone && <div className="column">
                  <p className="multiExpLblBlk">
                    Primary Contact Phone
                  </p>
                  <p className="is-size-7">{primaryContactPhone}</p>
                </div>}
              </div>
              }
              {!viewOnly && <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button className="button is-link" onClick={()=>this.showModal("Advisor/Contact")}>
                        Save Advisor/Contact
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderAddress() {
    const {
      entityId,
      waiting,
      countryResult,
      addresses,
      error,
      viewOnly
    } = this.state

    if (!entityId) {
      return null
    }

    return (<FirmAddressListForm
      countryResult={countryResult}
      errorFirmDetail={error.addresses[0]}
      onChangeAddressType={this.onChangeAddressType}
      canEdit={!viewOnly}
      canView
      busy={waiting}
      addressList={addresses}
      onSaveAddress={this.onSaveAddress} />)
  }

  renderCusipTable() {
    const {
      entityLinkedCusips,
      debtTypes,
      modalState,
      modalType,
      modalIndex,
      currentEntity,
      error,
      viewOnly
    } = this.state
    if (!entityLinkedCusips || !entityLinkedCusips.length) {
      return null
    }

    return (
      <div className="overflow-auto">
        {modalType === "cusip" && <Modal
          closeModal={this
            .toggleModal
            .bind(this, "", -1)}
          modalState={modalState}
          showBackground
          title={`Delete Cusip ${entityLinkedCusips[modalIndex] && entityLinkedCusips[modalIndex].associatedCusip6}`}>
          <div>
            <div className="field is-grouped">
              <div className="control">
                <button
                  className="button is-link is-small"
                  type="button"
                  onClick={this
                    .removeCusip
                    .bind(this, modalIndex)}>Delete</button>
              </div>
              <div className="control">
                <button
                  className="button is-light is-small"
                  type="button"
                  onClick={this
                    .toggleModal
                    .bind(this, "", -1)}>Cancel</button>
              </div>
            </div>
          </div>
        </Modal>
        }
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl">
                  Debt Type
                  <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                </p>
              </th>
              <th>
                <p className="multiExpLbl">
                  Debt Description
                </p>
              </th>
              <th>
                <p className="multiExpLbl">
                  Cusip
                  <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                </p>
              </th>
              {((entityLinkedCusips && entityLinkedCusips.length) > (currentEntity && currentEntity.entityLinkedCusips && currentEntity.entityLinkedCusips.length))
                ? <th>
                  <p className="multiExpLbl">Delete</p>
                </th>
                : undefined
              }
            </tr>
          </thead>
          <tbody>
            {entityLinkedCusips.map((e, i) => (
              <tr key={i}>
                <td className="multiExpTblVal">
                  <SelectLabelInput
                    placeholder="Select Debt Type"
                    list={debtTypes || []}
                    name="debtType"
                    value={e.debtType || ""}
                    disabled={viewOnly || false}
                    onChange={this.changeCusipField.bind(this, i)}
                  />
                  {error && error.entityLinkedCusips && error.entityLinkedCusips[i] && error.entityLinkedCusips[i].debtType
                    ? <p className="has-text-danger">
                      {error.entityLinkedCusips[i].debtType}
                    </p>
                    : undefined
                  }
                </td>
                <td className="multiExpTblVal">
                  <input
                    className="input is-small is-link"
                    name="debtDescription"
                    type="text"
                    placeholder="description"
                    value={e.debtDescription}
                    disabled={viewOnly || false}
                    onChange={this
                      .changeCusipField
                      .bind(this, i)} /> {error && error.entityLinkedCusips && error.entityLinkedCusips[i] && error.entityLinkedCusips[i].debtDescription
                    ? <p className="has-text-danger">
                      {error.entityLinkedCusips[i].debtDescription}
                    </p>
                    : undefined
                  }
                </td>
                <td className="multiExpTblVal">
                  <input
                    className="input is-small is-link"
                    name="associatedCusip6"
                    type="text"
                    placeholder="Associated CUSIP"
                    value={e.associatedCusip6}
                    disabled={viewOnly || false}
                    onChange={this
                      .changeCusipField
                      .bind(this, i)} /> {error && error.entityLinkedCusips && error.entityLinkedCusips[i] && error.entityLinkedCusips[i].associatedCusip6
                    ? <p className="has-text-danger">
                      {error.entityLinkedCusips[i].associatedCusip6}
                    </p>
                    : undefined
                  }
                </td>
                {((entityLinkedCusips && entityLinkedCusips.length) > (currentEntity && currentEntity.entityLinkedCusips && currentEntity.entityLinkedCusips.length))
                  ? <td className="multiExpTblVal">
                    {currentEntity && currentEntity.entityLinkedCusips && currentEntity.entityLinkedCusips.length
                      ? (i < (entityLinkedCusips.length - currentEntity.entityLinkedCusips.length))
                        ? <span className="has-text-danger">
                          <i
                            className="far fa-trash-alt"
                            onClick={this
                              .toggleModal
                              .bind(this, "cusip", i)} />
                        </span>
                        : <span className="has-text-danger" title="Can't delete saved record">
                          <i className="fa fa-ban" />
                        </span>

                      
                      : 
                      <button className="borderless"
                      onClick={this
                        .toggleModal
                        .bind(this, "cusip", i)}>
                      <span className="has-text-danger">
                        <i className="far fa-trash-alt"/>
                      </span>
                      </button>
                    }
                  </td>
                  : undefined
                }
              </tr>
            ))
            }
          </tbody>
        </table>
      </div>
    )
  }

  renderCusips() {
    const { entityId, expanded, viewOnly, entityLinkedCusips } = this.state
    if (!entityId) {
      return null
    }
    return (
      <article
        className={expanded.cusipSection
          ? "accordion is-active"
          : "accordion"}>
        <div
          className="accordion-header toggle"
          onClick={this
            .toggleExpand
            .bind(this, "cusipSection")}>
          <div className="level-left">
            <p>LINK CUSIP</p>
          </div>
          { !viewOnly ?
            <div className="level-right">
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className="button is-link is-small"
                    type="button"
                    onClick={this.addCusip}>Add
                  </button>
                </div>
                <div className="control">
                  <button
                    className="button is-light is-small"
                    type="button"
                    onClick={this.resetCusip}>Reset
                  </button>
                </div>
              </div>
            </div> : null
          }
        </div>
        {expanded.cusipSection
          ? <div
            className={viewOnly
              ? "accordion-body view-only-mode"
              : "accordion-body"}>
            <div className="accordion-content">
              {this.renderCusipTable()}
              {!viewOnly && <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      {
                        entityLinkedCusips && entityLinkedCusips.length ?
                        <button className="button is-link" onClick={()=>this.showModal("CUSIPs")}>
                          Save CUSIPs
                        </button> : null
                      }
                    </div>
                  </div>
                </div>
              </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderBorrowerTable() {
    const {
      entityBorObl,
      modalState,
      modalIndex,
      modalType,
      currentEntity,
      borrowerRelationshipTypes,
      debtTypes,
      viewOnly,
      error
    } = this.state
    if (!entityBorObl || !entityBorObl.length) {
      return null
    }
    return (
      <div>
        {modalType === "borrower" && <Modal
          closeModal={this
            .toggleModal
            .bind(this, "", -1)}
          modalState={modalState}
          showBackground
          title={`Delete Borrower/Obligor ${entityBorObl[modalIndex] && entityBorObl[modalIndex].borOblFirmName}`}>
          <div>
            <div className="field is-grouped">
              <div className="control">
                <button
                  className="button is-link is-small"
                  type="button"
                  onClick={this
                    .removeBorrower
                    .bind(this, modalIndex)}>Delete</button>
              </div>
              <div className="control">
                <button
                  className="button is-light is-small"
                  type="button"
                  onClick={this
                    .toggleModal
                    .bind(this, "", -1)}>Cancel</button>
              </div>
            </div>
          </div>
        </Modal>
        }
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl">Relationship
                  <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                </p>
              </th>
              <th>
                <p
                  className="multiExpLbl"
                  style={{
                    minWidth: "250px"
                  }}>Firm Name<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
              </th>
              <th>
                <p className="multiExpLbl">Debt Type</p>
              </th>
              <th>
                <p className="multiExpLbl">Associated Cusip-6</p>
              </th>
              <th>
                <p className="multiExpLbl">Start Date</p>
              </th>
              <th>
                <p className="multiExpLbl">End Date</p>
              </th>
              {((entityBorObl && entityBorObl.length) > (currentEntity && currentEntity.entityBorObl && currentEntity.entityBorObl.length))
                ? <th>
                  <p className="multiExpLbl">Delete</p>
                </th>
                : undefined
              }
            </tr>
          </thead>
          <tbody>
            {entityBorObl.map((e, i) => {
              const borrowerName = {
                _id: "",
                firmName: e.borOblFirmName,
                participantType: "Governmental Entity / Issuer",
                isExisting: true
              }
              return (
                <tr key={i}>
                  <td className="multiExpTblVal">
                    <SelectLabelInput
                      placeholder="Select Relationship"
                      list={borrowerRelationshipTypes || []}
                      name="borOblRel"
                      value={e.borOblRel || ""}
                      disabled={viewOnly || false}
                      onChange={this.changeBorrowerField.bind(this, i)}
                    />
                    {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblRel
                      ? <p className="has-text-danger">
                        {error.entityBorObl[i].borOblRel}
                      </p>
                      : undefined
                    }
                  </td>

                  {/* <td className="multiExpTblVal dropdown-on-top">
                    <BorrowerLookup
                      entityName={borrowerName}		 
                      notEditable={viewOnly || false}
                      onChange={this
												 
                        .onChangeBorrowerName
                        .bind(this, i)}
                      type={e.borOblRel}
                      isHide={e.borOblFirmName}
                    />
                    {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblFirmName
                      ? <p className="has-text-danger">
                        {error.entityBorObl[i].borOblFirmName}
                      </p>
                      : undefined
                    }
                  </td> */}
				  
				          <td className="multiExpTblVal">
                    <input
                      type="text"
                      className="input is-small is-link"
                      name="firmName"
                      placeholder="Firm Name"
                      value={e.borOblFirmName}
                      onChange={ this.onChangeBorrowerName.bind(this, i)}/>
                    {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblFirmName
                      ? <p className="has-text-danger">
                        {error.entityBorObl[i].borOblFirmName}
                      </p>
                      : undefined
                    }
                  </td>
                  
                  <td className="multiExpTblVal">
                    <div className="select is-small is-link">
                      <SelectLabelInput
                        placeholder="Select Debt Type"
                        list={debtTypes || []}
                        name="borOblDebtType"
                        value={e.borOblDebtType || ""}
                        disabled={viewOnly || false}
                        onChange={this.changeBorrowerField.bind(this, i)}
                      />
                      {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblDebtType
                        ? <p className="has-text-danger">
                          {error.entityBorObl[i].borOblDebtType}
                        </p>
                        : undefined
                      }
                    </div>
                  </td>
				  
                  <td className="multiExpTblVal">
                    <input
                      className="input is-small is-link"
                      name="borOblCusip6"
                      type="text"
                      placeholder="CUSIP-6"
                      value={e.borOblCusip6}
                      disabled={viewOnly || false}
                      onChange={this
                        .changeBorrowerField
                        .bind(this, i)} /> {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblCusip6
                      ? <p className="has-text-danger">
                        {error.entityBorObl[i].borOblCusip6}
                      </p>
                      : undefined
                    }
                  </td>
                  <td className="multiExpTblVal">
                    {/* <input
                      className="input is-small is-link"
                      type="date"
                      name="borOblStartDate"
                      value={getCalendarDateAsString(e.borOblStartDate)}
                      disabled={viewOnly || false}
                      onChange={this.changeBorrowerField.bind(this, i)}
                    /> */}
                    <TextLabelInput
                      type="date"
                      name="borOblStartDate"
                      value={(e.borOblStartDate === "" || !e.borOblStartDate) ? null : new Date(e.borOblStartDate)}
                      disabled={viewOnly || false}
                      onChange={this.changeBorrowerField.bind(this, i)}
                    />
                    {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblStartDate ?
                      <p className="has-text-danger">{error.entityBorObl[i].borOblStartDate}</p>
                      : undefined
                    }
                  </td>
                  <td className="multiExpTblVal">
                    {/* <input
                      className="input is-small is-link"
                      type="date"
                      name="borOblEndDate"
                      value={getCalendarDateAsString(e.borOblEndDate)}
                      disabled={viewOnly || false}
                      onChange={this.changeBorrowerField.bind(this, i)}
                    /> */}
                    <TextLabelInput
                      type="date"
                      name="borOblEndDate"
                      value={(e.borOblEndDate === "" || !e.borOblEndDate) ? null : new Date(e.borOblEndDate)}
                      disabled={viewOnly || false}
                      onChange={this.changeBorrowerField.bind(this, i)}
                    />
                    {error && error.entityBorObl && error.entityBorObl[i] && error.entityBorObl[i].borOblEndDate ?
                      <p className="has-text-danger">{error.entityBorObl[i].borOblEndDate}</p>
                      : undefined
                    }
                  </td>
                  {((entityBorObl && entityBorObl.length) > (currentEntity && currentEntity.entityBorObl && currentEntity.entityBorObl.length))
                    ? <td>
                      {currentEntity && currentEntity.entityBorObl && currentEntity.entityBorObl.length
                        ? (i < (entityBorObl.length - currentEntity.entityBorObl.length))
                          ? <span className="has-text-danger">
                            <i
                              className="far fa-trash-alt"
                              onClick={this
                                .toggleModal
                                .bind(this, "borrower", i)} />
                          </span>
                          : <span className="has-text-danger" title="Can't delete saved record">
                            <i className="fa fa-ban" />
                          </span>
                        : 
                        <button className="borderless"
                          onClick={this
                          .toggleModal
                          .bind(this, "borrower", i)}>
                            <span className="has-text-danger">
                              <i className="far fa-trash-alt" />
                            </span>
                        </button>
                      }
                    </td>
                    : undefined
                    
                  }
                </tr>
              )
            })
            }
          </tbody>
        </table>
      </div>
    )
  }

  renderBorrowers() {
    const { entityId, expanded, viewOnly, entityBorObl } = this.state
    if (!entityId) {
      return null
    }
    return (
      <article
        className={expanded.borrowerSection
          ? "accordion is-active"
          : "accordion"}>
        <div
          className="accordion-header toggle"
          onClick={this
            .toggleExpand
            .bind(this, "borrowerSection")}>
          <div className="level-left">
            <p>LINK BORROWERS / OBLIGORS</p>
          </div>
          { !viewOnly ?
            <div className="level-right">
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className="button is-link is-small"
                    type="button"
                    onClick={this.addBorrower}>Add
                  </button>
                </div>
                <div className="control">
                  <button
                    className="button is-light is-small"
                    type="button"
                    onClick={this.resetBorrower}>Reset
                  </button>
                </div>
              </div>
            </div> : null
          }
        </div>
        {expanded.borrowerSection
          ? <div
            className={viewOnly
              ? "accordion-body view-only-mode"
              : "accordion-body"}>
            <div className="accordion-content">
              {this.renderBorrowerTable()}
              {!viewOnly && <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      {
                        (entityBorObl && entityBorObl.length) ?
                        <button className="button is-link" onClick={()=>this.showModal("Borrowers/Obligors")}>
                          Save Borrowers/Obligors
                        </button> : null
                      }
                    </div>
                  </div>
                </div>
              </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderHistoricalData = () => {
    const {currentEntity} = this.state
    const data = currentEntity.historicalData || {}
    return <HistoricalData data={data}/>
  }

  render() {
    const { waiting, generalError, currentEntity, entityId, notes, viewOnly } = this.state
    const { nav1, auth } = this.props
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    if (generalError) {
      return (
        <div id="main">
          <strong className="has-text-danger">{generalError}</strong>
        </div>
      )
    }
    return (
      <div id="main">
        {waiting && <Loader />}
        <section className="accordions">
          <SaveModal
            closeModal={this.toggleSaveModal}
            modalState={this.state.modalState}
            message={this.state.modalMessage}
            onConfirmed={this.state.onConfirmed}
          />
          {this.renderBasicDetails()}
          {nav1 !== "migratedentities" ? this.renderAdvisorDetails() : null}
          {this.renderAddress()}
          {nav1 !== "migratedentities" ? this.renderCusips() : null}
          {nav1 !== "migratedentities" ? this.renderBorrowers() : null}
          {currentEntity && currentEntity.OnBoardingDataMigrationHistorical ? this.renderHistoricalData() : null}
          <Notes
            userId={userId}
            userFirstName={userFirstName}
            userLastName={userLastName}
            entityId={entityId}
            saveCallback={this.saveCallback}
            waiting={waiting}
            notes={notes}
            canEdit={viewOnly || false}
          />
        </section>
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default connect(mapStateToProps, null)(ClientsProspects)
