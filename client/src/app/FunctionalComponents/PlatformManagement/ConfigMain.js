import React from "react"
import { Link, NavLink, withRouter } from "react-router-dom"
import { connect } from "react-redux"

import AuditLog from "Global/AuditLog"
import Picklists from "Global/Picklists"
import ReferenceDataMain from "./ReferenceDataMain"
import ChecklistsMain from "./ChecklistsMain"
import NotificationsMain from "./NotificationsMain"
import ConfigNotification from "./ConfigNotification";
import {checkSupervisorControls} from "../../StateManagement/actions/CreateTransaction";
import Loader from "../../GlobalComponents/Loader";
import Audit from "../../GlobalComponents/Audit";

const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

class ConfigMain extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      TABS: [
        {path: "reference-data", label: "Picklists"},
        {path: "checklists", label: "Checklists"},
        {path: "notifications", label: "Notifications"},
        {path: "audit", label: "Activity Log"}
      ],
      loading: true
    }
  }

  async componentWillMount() {
    const {audit} = this.props
    const { TABS } = this.state
    const svControls = await checkSupervisorControls()
    const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
    if (!submitAudit) {
      const index = TABS.findIndex(tab => tab.path === "audit")
      if(index !== -1){
        TABS.splice(index, 1)
      }
    }
    this.setState({ loading: false })
  }

  renderTabs = (tabs, nav2) =>
    tabs.map(t => (
      <li
        key={t.path}
        className={t.path === nav2 ? "is-active" : "inactive-tab"}
      >
        <NavLink to={`/configuration/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderViewSelection = nav2 => {
    const { TABS } = this.state
    return (
      <nav className="tabs is-boxed">
        <ul>{this.renderTabs(TABS, nav2)}</ul>
      </nav>
    )
  }

  renderSelectedView = (nav2, nav3) => {
    switch (nav2) {
    case "reference-data":
      return <Picklists option={nav3} />
    case "checklists":
      return <ChecklistsMain option={nav3} />
      // case "notifications":
      //   return <NotificationsMain option={nav3} />
    case "notifications":
      return <ConfigNotification {...this.props} option={nav3} />
    case "audit":
      return <div id="main"><Audit nav1={this.props.nav1} nav2="config"/></div>
    default:
      return <p>{nav2}</p>
    }
  }

  render () {
    let { nav2, nav3 } = this.props
    nav2 = nav2 || "reference-data"
    if (this.state.loading) {
      return <Loader />
    }
    return (
      <div>
        <section className="hero is-small">
          <div className="hero-foot">
            <div className="container">{this.renderViewSelection(nav2)}</div>
          </div>
        </section>
        {this.renderSelectedView(nav2, nav3)}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(mapStateToProps, null)(ConfigMain)
