import React, { Component } from "react"

import qs from "qs"

import ChecklistTemplate1 from "./ChecklistTemplate1"
import ChecklistTemplate2 from "./ChecklistTemplate2"
import ChecklistTemplate3 from "./ChecklistTemplate3"
import ChecklistTemplate4 from "./ChecklistTemplate4"
import ChecklistDetails1 from "./ChecklistDetails1"
import ChecklistDetails3 from "./ChecklistDetails3"
import ChecklistDetails4 from "./ChecklistDetails4"

const TEMPLATES = {
  1: ChecklistTemplate1,
  2: ChecklistTemplate2,
  3: ChecklistTemplate3,
  4: ChecklistTemplate4
}

class ChecklistTemplateSelection extends Component {
  constructor(props) {
    super(props)
    this.state = { selectedTemplateType: "", newListParams: {} }
  }

  selectTemplateType(selectedTemplateType) {
    const newListParams = qs.parse(this.props.query, { ignoreQueryPrefix: true }) || {}
    console.log("newListParams : ", newListParams)
    this.setState(prevState => {
      if(prevState.selectedTemplateType) {
        return { selectedTemplateType: "", newListParams }
      }
      return { selectedTemplateType, newListParams }
    })
  }

  renderTemplateType(type, selected) {
    // console.log("type : ", type)
    const Component = TEMPLATES[type]
    return(
      <div key={type}>
        <div className="field is-grouped-left">
          <div className="control">
            <p className="title innerPgTitle" style={{paddingTop: "5px"}}>
              {`Template ${type}`}
            </p>
          </div>
          <div className="control">
            <button className="button is-info is-small is-rounded"
                    onClick={this.selectTemplateType.bind(this, type)}>
              {this.state.selectedTemplateType ? "Show all templates" : "Select"}</button>
          </div>
        </div>
        <Component selected={!selected} />
      </div>
    )
  }

  renderTemplateTypes(selectedTemplateType) {
    if(selectedTemplateType) {
      return this.renderTemplateType(selectedTemplateType, true)
    }
    return [1,3,4].map(e => this.renderTemplateType(e, false))
  }

  renderSelectedTemplate(selectedTemplateType, newListParams) {
    const { userId, userName, token, configChecklists } = this.props
    newListParams = { ...newListParams }
    newListParams.type = `template${selectedTemplateType}`
    switch(selectedTemplateType) {
    case 1:
      return (
        <ChecklistDetails1
          userId={userId}
          userName={userName}
          token={token}
          newListParams={newListParams}
          configChecklists={configChecklists}
        />
      )
    case 2:
      return <p>Template2 is not available for selection yet</p>
    case 3:
      return (
        <ChecklistDetails3
          userId={userId}
          userName={userName}
          token={token}
          newListParams={newListParams}
          configChecklists={configChecklists}
        />
      )
    case 4:
      return (
        <ChecklistDetails4
          userId={userId}
          userName={userName}
          token={token}
          newListParams={newListParams}
          configChecklists={configChecklists}
        />
      )
    default:
      return null
    }
  }

  render() {
    const { selectedTemplateType, newListParams } = this.state
    return (
      <section>
        <p className="title innerPgTitle">Choose a template and start adding items to the checklist</p>
        <hr/>
        {this.renderTemplateTypes(selectedTemplateType)}
        {this.renderSelectedTemplate(selectedTemplateType, newListParams)}
      </section>
    )
  }
}

export default ChecklistTemplateSelection
