import React from "react"

const ChecklistTemplate2 = props => {
  const { selected } = props
  return (
    <section className="accordions">

      <article className="accordion is-active">
        <div className="accordion-header toggle">
          <div className="field is-grouped">
            <div className="control">
              <p>Title</p>
            </div>
            {selected &&
              <div className="control">
                <a>
                  <span className="has-text-link">
                    <i className="fas fa-pencil-alt" />
                  </span>
                </a>
              </div>
            }
          </div>
          {selected &&
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link is-small">Add List Item</button>
              </div>
              <div className="control">
                <button className="button is-light is-small">Reset</button>
              </div>
            </div>
          }
        </div>
        <div className="accordion-body">
          <div className="accordion-content">

            <div>

              <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>
                      <div className="field is-grouped">
                        <div className="control">
                          <p className="emmaTablesTh">List Item</p>
                        </div>
                        {selected &&
                          <div className="control">
                            <a>
                              <span className="has-text-link">
                                <i className="fas fa-pencil-alt" />
                              </span>
                            </a>
                          </div>
                        }
                      </div>
                    </th>
                    <th>
                      <p className="emmaTablesTh">Priority</p>
                    </th>
                    <th>
                      <p className="emmaTablesTh">NA</p>
                    </th>
                    <th>
                      <p className="emmaTablesTh">1</p>
                    </th>
                    <th>
                      <p className="emmaTablesTh">2</p>
                    </th>
                    <th>
                      <p className="emmaTablesTh">3</p>
                    </th>
                    <th>
                      <p className="emmaTablesTh">4</p>
                    </th>
                    <th>
                      <p className="emmaTablesTh">5</p>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <input className="input is-small is-link" type="text" placeholder="Checklist Item"/>
                    </td>
                    <td>
                      <div className="select is-small is-link">
                        <select>
                          <option value="" disabled selected>Pick priority</option>
                          <option>LKUPHIGHMEDLOW</option>
                          <option>High</option>
                        </select>
                      </div>
                    </td>
                    <td>
                      <label className="radio">
                        <input type="radio"/>
                      </label>
                    </td>
                    <td>
                      <label className="radio">
                        <input type="radio"/>
                      </label>
                    </td>
                    <td>
                      <label className="radio">
                        <input type="radio"/>
                      </label>
                    </td>
                    <td>
                      <label className="radio">
                        <input type="radio"/>
                      </label>
                    </td>
                    <td>
                      <label className="radio">
                        <input type="radio"/>
                      </label>
                    </td>
                    <td>
                      <label className="radio">
                        <input type="radio"/>
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

          </div>
        </div>

      </article>

    </section>
  )
}

export default ChecklistTemplate2
