import React, { Component } from "react"
import { connect } from "react-redux"

import { getConfigChecklist, saveConfigChecklist } from "../../StateManagement/actions"

class ChecklistDetails2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      accordions: {}, expanded: ["Add a title"], validationError: {},
      accordionTitleToChange: "", keyChanges: {}, removedKeys: [],
      changeLog: [], removedRows: [], generalError: "", itemHeaders: {}, itemHeaderToChange: "",
      newChecklistName: "", newChecklistAttributedTo: "",
      newChecklistBestPractise: "", newChecklistNotes: ""
    }
    this.validateSubmition = this.validateSubmition.bind(this)
    this.saveAccordions = this.saveAccordions.bind(this)
    this.addAccordion = this.addAccordion.bind(this)
    this.resetAllAcccordions = this.resetAllAcccordions.bind(this)
    this.saveData = this.saveData.bind(this)
    this.logAndSave = this.logAndSave.bind(this)
    this.saveConfigInDB = this.saveConfigInDB.bind(this)
    this.changeAccordionTitle = this.changeAccordionTitle.bind(this)
    this.changeItemHeader = this.changeItemHeader.bind(this)
    this.handleKeyPressItemHeader = this.handleKeyPressItemHeader.bind(this)
    this.handleKeyPressTitle = this.handleKeyPressTitle.bind(this)
    this.changeNewChecklistFields = this.changeNewChecklistFields.bind(this)
    this.onBlur = this.onBlur.bind(this)
  }

  componentDidMount() {
    const { config, configChecklists, cid, itemHeaders } = this.props
    const idx = configChecklists.findIndex(c => c.id === cid)
    const checklist = idx ? { ...configChecklists[idx] } : {}
    console.log("setting data in componentDidMount")
    // this.setDataInState(config)
    if (!config || !Object.keys(config).length) {
      console.log("getting db data")
      // this.props.getConfigChecklist()
      this.setDataInState(config, checklist, itemHeaders)
    } else {
      this.setDataInState(config, checklist, itemHeaders)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { config, configChecklists, cid, itemHeaders } = nextProps
    const idx = configChecklists.findIndex(c => c.id === cid)
    const checklist = idx >= 0 ? { ...configChecklists[idx] } : {}
    console.log("setting data in componentWillReceiveProps")
    this.setDataInState(config, checklist, itemHeaders)
  }


  componentWillUnmount() {
    console.log("in componentWillUnmount")
    const { accordions, itemHeaders, changeLog, newChecklistName, newChecklistAttributedTo,
      newChecklistBestPractise, newChecklistNotes } = this.state
    const { token, cid, configChecklists } = this.props
    const checklists = [...configChecklists]
    const idx = checklists.findIndex(c => c.id === cid)
    const checklist = { ...checklists[idx] }
    checklist.name = newChecklistName
    checklist.attributedTo = newChecklistAttributedTo
    checklist.bestPractice = newChecklistBestPractise
    checklist.notes = newChecklistNotes
    checklist.data = accordions
    checklist.itemHeaders = itemHeaders
    checklists[idx] = checklist
    this.props.saveConfigChecklist(token, checklists, changeLog, "redux-store")
  }

  setaccordionTitleToChange(accordionTitleToChange, e) {
    this.setState({ accordionTitleToChange })
  }

  setDataInState(data, checklist = {}, itemHeaders = {}) {
    if (!data || !Object.keys(data).length) {
      data = this.initialAccordionsData()
    }
    itemHeaders = { ...itemHeaders }
    Object.keys(itemHeaders).forEach(k => {
      if (!itemHeaders[k] || !itemHeaders[k][0]) {
        itemHeaders[k] = ["List Item"]
      }
    })
    const newChecklistName = checklist.name || ""
    const newChecklistAttributedTo = checklist.attributedTo || ""
    const newChecklistBestPractise = checklist.bestPractice || ""
    const newChecklistNotes = checklist.notes || ""
    this.setState({
      accordions: data, newChecklistName, newChecklistAttributedTo,
      newChecklistBestPractise, newChecklistNotes, itemHeaders, validationError: {}
    })
  }

  removeAccordion(key) {
    this.setState(prevState => {
      const accordions = {}
      const keys = Object.keys(prevState.accordions).filter(k => k !== key)
      keys.forEach(k => {
        accordions[k] = prevState.accordions[k]
      })
      const removedKeys = [...prevState.removedKeys]
      removedKeys.push(key)
      return { accordions, removedKeys }
    })
  }

  changeNewChecklistFields(e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  onBlur(e, id, name) {
    const { removedRows } = this.state
    const { value } = e.target
    const row = `${id || ""} in ${name || ""} change to ${value || ""}`
    removedRows.push(row)
    this.setState({
      removedRows
    })
  }

  initialAccordionsData() {
    return {
      "Add a title": [
        {
          label: ""
        }
      ]
    }
  }

  initialItemHeader() {
    return {
      "Add a title": ["List Item"]
    }
  }

  addAccordion() {
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      const itemHeaders = { ...prevState.itemHeaders }
      const numKeys = Object.keys(accordions).length
      accordions[`Add Title ${numKeys}`] = this.initialAccordionsData()["Add a title"]
      itemHeaders[`Add Title ${numKeys}`] = this.initialItemHeader()["Add a title"]
      return { accordions, itemHeaders }
    })
  }

  updateKeyChanges(prevKeyChanges, oldKey, newKey) {
    console.log("prevKeyChanges : ", JSON.stringify(prevKeyChanges))
    console.log("oldKey : ", oldKey)
    console.log("newKey : ", newKey)

    let newKeyChanges = { ...prevKeyChanges }
    const oldKeys = Object.keys(prevKeyChanges)

    if (!oldKeys.length) {
      newKeyChanges[oldKey] = newKey
      return newKeyChanges
    }

    if (newKeyChanges.hasOwnProperty(newKey)) {
      newKeyChanges = {}
      console.log("changed to original value !!")
      oldKeys.forEach(k => {
        if (k !== newKey) {
          newKeyChanges[k] = prevKeyChanges[k]
        }
      })
      return newKeyChanges
    }
    let foundOldKey = false
    oldKeys.some(k => {
      if (prevKeyChanges[k] === oldKey) {
        console.log("found match for key : ", k)
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        foundOldKey = true
        newKeyChanges[k] = newKey
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        return true
      }
    })
    if (!foundOldKey) {
      console.log("match not found : ", oldKey)
      newKeyChanges[oldKey] = newKey
    }

    return newKeyChanges
  }

  changeAccordionTitle(e) {
    const newKey = e.target.value
    console.log("newKey : ", newKey)
    this.setState(prevState => {
      const { accordionTitleToChange } = prevState

      if (accordionTitleToChange === newKey) {
        return {}
      }

      const accordionsKeys = Object.keys(prevState.accordions)
      if (accordionsKeys.includes(newKey)) {
        return { generalError: "Duplicate Title" }
      }

      const prevKeyChanges = { ...prevState.keyChanges }
      let keyChanges = {}
      const accordions = {}
      const itemHeaders = {}
      accordionsKeys.forEach(k => {
        console.log("k : ", k)
        console.log("accordionTitleToChange : ", accordionTitleToChange)
        if (k === accordionTitleToChange) {
          console.log("doing new key")
          accordions[newKey] = prevState.accordions[accordionTitleToChange]
          itemHeaders[newKey] = prevState.itemHeaders[accordionTitleToChange]
          keyChanges = this.updateKeyChanges(prevKeyChanges, accordionTitleToChange, newKey)
        } else {
          console.log("doing old key")
          accordions[k] = prevState.accordions[k]
          itemHeaders[k] = prevState.itemHeaders[k]
        }
      })
      return { accordions, itemHeaders, accordionTitleToChange: newKey, keyChanges, generalError: "" }
    })
  }

  setItemHeaderToChange(itemHeaderToChange) {
    // this.setState(prevState => {
    //   const itemHeaders = { ...prevState.itemHeaders }
    //   if(!itemHeaders[itemHeaderToChange] || !itemHeaders[itemHeaderToChange][0]) {
    //     itemHeaders[itemHeaderToChange] = ["List Item"]
    //   }
    //   return { itemHeaders, itemHeaderToChange }
    // })
    this.setState({ itemHeaderToChange })
  }

  changeItemHeader(e) {
    const { value } = e.target
    this.setState(prevState => {
      const itemHeaders = { ...prevState.itemHeaders }
      const { itemHeaderToChange } = prevState
      itemHeaders[itemHeaderToChange][0] = value
      return { itemHeaders }
    })
  }

  handleKeyPressItemHeader(e) {
    if (e.key === "Enter") {
      console.log("val : ", e.target.value)
      this.changeItemHeader(e.target.value)
    }
  }

  handleKeyPressTitle(e) {
    if (e.key === "Enter") {
      this.setaccordionTitleToChange("")
    }
  }

  saveConfigInDB() {
    const { accordions, itemHeaders, changeLog, newChecklistName,
      newChecklistAttributedTo, newChecklistBestPractise,
      newChecklistNotes } = this.state
    const { token, cid, configChecklists } = this.props
    const checklists = [...configChecklists]
    const idx = checklists.findIndex(c => c.id === cid)
    const checklist = { ...checklists[idx] }
    checklist.name = newChecklistName
    checklist.attributedTo = newChecklistAttributedTo
    checklist.bestPractice = newChecklistBestPractise
    checklist.notes = newChecklistNotes
    checklist.data = accordions
    checklist.itemHeaders = itemHeaders
    checklists[idx] = checklist
    console.log("changeLog : ", changeLog)
    this.props.saveConfigChecklist(token, checklists, changeLog)
  }

  newItemLog(e) {
    let logMessage = ""
    if (!e) {
      return logMessage
    }
    Object.keys(e).forEach(k => {
      if (k !== "_id") {
        logMessage += ` ${k} - ${e[k]}`
      }
    })
    return logMessage
  }

  checkElemChange(newItem, oldItem) {
    let elemChange = ""
    Object.keys(newItem).forEach(k => {
      if (newItem[k] !== oldItem[k]) {
        elemChange += ` ${k} - ${oldItem[k]} to ${newItem[k]}`
      }
    })
    return elemChange
  }

  logAndSave() {
    const userName = "Anon"
    const date = new Date()
    // let dateString = date.toLocaleString();
    const oldData = this.props.config
    const { accordions, keyChanges, removedKeys, removedRows } = this.state
    const changeLog = []
    const newData = {}

    removedKeys.forEach(k => {
      changeLog.push({ userName, log: `removed checklist with title ${k}`, date })
    })

    removedRows.forEach(row => {
      changeLog.push({ userName, log: row, date })
    })

    Object.keys(accordions).forEach(k => {
      console.log("k : ", k)
      let oldKey
      Object.keys(keyChanges).some(c => {
        console.log("c : ", c)
        if ((keyChanges[c] === k) && oldData.hasOwnProperty(c)) {
          oldKey = c
          changeLog.push({ userName, log: `changed checklist Title ${c} to ${k}`, date })
          return true
        }
      })
      console.log("oldKey : ", oldKey)
      if (oldKey) {
        newData[oldKey] = accordions[k]
      } else {
        newData[k] = accordions[k]
      }
    })
    console.log("oldData : ", oldData)
    console.log("newData : ", newData)

    Object.keys(newData).forEach(n => {
      const newItems = newData[n]
      const oldItems = oldData[n]
      if (oldItems) {
        const len = oldItems.length
        newItems.forEach((e, i) => {
          if (i < len) {
            const elemChange = this.checkElemChange(e, oldItems[i])
            if (elemChange) {
              changeLog.push({ userName, log: `changed item${i + 1} in checklist with title ${n} - ${elemChange}`, date })
            }
          } else {
            changeLog.push({ userName, log: `added following item in checklist with title ${n} - ${this.newItemLog(e)}`, date })
          }
        })
      } else {
        let newListMessage = `added new checklist with title ${n} with below items : `
        newItems.forEach((e, i) => { newListMessage += ` |${i + 1}: ${this.newItemLog(e)}|` })
        changeLog.push({ userName, log: newListMessage, date })
      }
    })

    this.setState({ changeLog, accordionTitleToChange: "" }, this.saveConfigInDB)
  }

  saveData() {
    const { accordions, validationError } = this.state
    const errKeys = Object.keys(validationError)
    console.log("errKeys : ", errKeys)
    let save = true
    if (errKeys.length) {
      let error = false
      errKeys.some(i => {
        const itemKeys = Object.keys(validationError[i])
        console.log("itemKeys : ", itemKeys)
        if (!itemKeys.length) {
          return true
        }
        itemKeys.some(j => {
          const subItemKeys = Object.keys(validationError[i][j])
          console.log("subItemKeys : ", subItemKeys)
          if (!subItemKeys.length) {
            return true
          }
          subItemKeys.some(k => {
            console.log("subItem : ", validationError[i][j][k])
            if (validationError[i][j][k]) {
              error = true
              return true
            }
          })
          if (error) {
            return true
          }
        })
        if (error) {
          return true
        }
      })
      if (error) {
        save = false
      }
    }

    if (save) {
      this.logAndSave()
    }
  }

  validateSubmition() {
    this.setState(prevState => {
      const { accordions, validationError } = prevState
      const newErr = { ...validationError }
      Object.keys(accordions).forEach(a => {
        Object.keys(accordions[a]).forEach(k => {
          if (!accordions[a][k].label) {
            newErr[a] = { ...newErr[a] }
            newErr[a][k] = { ...newErr[a][k] }
            newErr[a][k].label = "No input provided"
          }
        })
      })
      return { validationError: newErr }
    }, this.saveData)
  }

  saveAccordions() {
    this.validateSubmition()
  }

  handleClick(key, e) {
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  }

  changeLabel(key, i, e) {
    const { value } = e.target
    console.log(" key: ", key)
    console.log(" i: ", i)
    console.log(" val: ", value)
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key][i] = { ...accordions[key][i], label: value }
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i], label: "" }
      return { accordions, validationError }
    })
  }

  addAcccordionItem(key, e) {
    this.setState(prevState => {
      const expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key] = accordions[key].filter(e => e && e.label)
      accordions[key].push({
        label: ""
      })
      return { accordions, expanded }
    })
  }

  resetAcccordion(key, e) {
    this.setState((prevState, props) => {
      const expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      if (props.config[key]) {
        accordions[key] = [...props.config[key]]
      } else {
        accordions[key] = this.initialAccordionsData()["Add a title"]
      }
      return { accordions, expanded, validationError: {} }
    })
    // this.setState({ accordions: data });
  }

  resetAllAcccordions() {
    const { token } = this.props
    this.props.getConfigChecklist(token)
    this.setState({ changeLog: [], removedKeys: [], removedRows: [], keyChanges: {} })
  }

  renderTitleInputFieldWithIcon(key) {
    if (this.state.accordionTitleToChange === key) {
      return (
        <div className="field is-grouped-left">
          <div className="control">
            <input autoFocus
              className="input is-small is-link"
              type="text"
              value={key}
              onKeyPress={this.handleKeyPressTitle}
              onChange={this.changeAccordionTitle} />
          </div>
          <div className="control">
            <span className="icon is-small is-right">
              <i className="fa fa-times" onClick={this.setaccordionTitleToChange.bind(this, "")} />
            </span>
          </div>
        </div>
      )
    }
    return (
      <div className="field is-grouped-left">
        <div className="control">
          <p>{key} </p>
        </div>
        <div className="control">
          <a>
            <span className="has-text-link">
              <i className="fas fa-pencil-alt" onClick={this.setaccordionTitleToChange.bind(this, key)} />
            </span>
          </a>
        </div>
      </div>
    )

  }

  renderHeader(key) {
    return (
      <div className="accordion-header toggle" style={{ display: "initial" }}>
        <div className="columns is-gapless is-multiline">
          <div className="column is-fullwidth">
            {this.renderTitleInputFieldWithIcon(key)}
          </div>
          <div className="column is-2">
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link is-small"
                  onClick={this.addAcccordionItem.bind(this, key)}>Add List Item</button>
              </div>
              <div className="control">
                <button className="button is-light is-small"
                  onClick={this.resetAcccordion.bind(this, key)}>Reset</button>
              </div>
              <div className="control is-size-4">
                <i className="fa fa-trash-o"
                  onClick={this.removeAccordion.bind(this, key)} />
              </div>
              <div className="control is-size-4">
                <i className={this.state.expanded.includes(key) ?
                  "fa fa-minus-circle" : "fa fa-plus-circle"}
                  onClick={this.handleClick.bind(this, key)} />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderAccordionBodyItem(e, key, i) {
    return (
      <tr key={key + i}>
        <td>
          <input
            className="input is-small is-link"
            type="text"
            value={e.label}
            onChange={this.changeLabel.bind(this, key, i)}
          />
          {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].label ?
            <strong>{this.state.validationError[key][i].label}</strong>
            : undefined
          }
        </td>
      </tr>
    )
  }

  renderAccordionBody(key, accordion) {
    const { itemHeaders, itemHeaderToChange } = this.state
    return (
      <div className="tbl-scroll">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
          <tr>
            <th>
              {itemHeaderToChange === key ?
                <div className="field is-grouped">
                  <div className="control">
                    <input
                      className="input is-small is-link"
                      type="text"
                      value={itemHeaders[key][0]}
                      onKeyPress={this.handleKeyPressItemHeader}
                      onChange={this.changeItemHeader} />
                  </div>
                  <div className="control">
                    <span className="icon is-small is-right">
                      <i className="fa fa-times" onClick={this.setItemHeaderToChange.bind(this, "")} />
                    </span>
                  </div>
                </div> :
                <div className="field is-grouped">
                  <div className="control">
                    <p className="emmaTablesTh">
                      {itemHeaders[key][0]}
                    </p>
                  </div>
                  <div className="control">
                    <a>
                      <span className="has-text-link">
                        <i className="fas fa-pencil-alt"
                           onClick={this.setItemHeaderToChange.bind(this, key)} />
                      </span>
                    </a>
                  </div>
                </div>
              }
            </th>
          </tr>
          </thead>
          <tbody>
          {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
          </tbody>
        </table>
      </div>
    )
  }

  renderAccordion(key, accordion) {
    return (
      <section className="accordions" key={key}>
        <article className="accordion is-active" >
          {this.renderHeader(key)}
          {this.state.expanded.includes(key) ?
            this.renderAccordionBody(key, accordion)
            : undefined
          }
        </article>
      </section>
    )
  }

  renderAccordions(accordions) {
    return Object.keys(accordions).map(a => this.renderAccordion(a, accordions[a]))
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Chk List Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Attributed to</p>
        </th>
        <th>
          <p className="emmaTablesTh">Best Practice</p>
        </th>
        <th>
          <p className="emmaTablesTh">Notes</p>
        </th>
      </tr>
    )
  }

  renderChecklistBasicDetails() {
    const { newChecklistName, newChecklistAttributedTo,
      newChecklistBestPractise, newChecklistNotes } = this.state
    return (
      <tr>
        <td className="emmaTablesTd">
          <small>{this.props.cid}</small>
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder="Checklist name"
            name="newChecklistName"
            value={newChecklistName}
            onBlur={(e) => this.onBlur(e, this.props.cid, "Chk List Name")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder="Defaults to firmname"
            name="newChecklistAttributedTo"
            value={newChecklistAttributedTo}
            onBlur={(e) => this.onBlur(e, this.props.cid, "Attributed to")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder=""
            name="newChecklistBestPractise"
            value={newChecklistBestPractise}
            onBlur={(e) => this.onBlur(e, this.props.cid, "Best Practice")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
        <td className="emmaTablesTd">
          <input className="input is-small is-link"
            type="text"
            placeholder=""
            name="newChecklistNotes"
            value={newChecklistNotes}
            onBlur={(e) => this.onBlur(e, this.props.cid, "Notes")}
            onChange={this.changeNewChecklistFields}
          />
        </td>
      </tr>
    )
  }

  render() {
    return (
      <div>
        <div className="box">
          <div className="tbl-scroll">
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
              {this.renderTableHeader()}
              </thead>
              <tbody>
              {this.renderChecklistBasicDetails()}
              </tbody>
            </table>
          </div>
        </div>
        {this.renderAccordions(this.state.accordions)}
        <strong>{this.state.generalError}</strong>
        <div className="field is-grouped-center">
          <div className="control">
            <button className="button is-link"
              onClick={this.addAccordion}>Add a new checklist</button>
          </div>
          <div className="control">
            <button className="button is-link"
              onClick={this.resetAllAcccordions}>Undo All Changes</button>
          </div>
          <div className="control">
            <button className="button is-link"
              onClick={this.saveAccordions}>Save</button>
          </div>
        </div>
      </div>
    )
  }

}

// const mapStateToProps = ({ configChecklist }) => {
//   console.log("configChecklist in state : ", configChecklist)
//   return { config: configChecklist }
// }

export default connect(null, { getConfigChecklist, saveConfigChecklist })(ChecklistDetails2)
