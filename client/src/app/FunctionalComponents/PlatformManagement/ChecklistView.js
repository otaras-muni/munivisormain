import React from "react"
import ChecklistStart from "./ChecklistStart"
import ChecklistDetails1 from "./ChecklistDetails1"
import ChecklistDetails2 from "./ChecklistDetails2"
import ChecklistDetails3 from "./ChecklistDetails3"
import ChecklistDetails4 from "./ChecklistDetails4"

const ChecklistView = props => {
  const { userId, userName, start, configChecklists, token, checklist, cid } = props
  if(start) {
    return <ChecklistStart checklist={checklist} />
  }
  switch(checklist.type) {
  case "template1":
    return <ChecklistDetails1
      token={token}
      cid={cid}
      userId={userId}
      userName={userName}
      itemHeaders={checklist.itemHeaders}
      config={checklist.data}
      configChecklists={configChecklists} />
  case "template2":
    return <ChecklistDetails2
      token={token}
      cid={cid}
      userId={userId}
      userName={userName}
      itemHeaders={checklist.itemHeaders}
      config={checklist.data}
      configChecklists={configChecklists} />
  case "template3":
    return <ChecklistDetails3
      token={token}
      cid={cid}
      userId={userId}
      userName={userName}
      itemHeaders={checklist.itemHeaders}
      config={checklist.data}
      configChecklists={configChecklists} />
  case "template4":
    return <ChecklistDetails4
      token={token}
      cid={cid}
      userId={userId}
      userName={userName}
      itemHeaders={checklist.itemHeaders}
      config={checklist.data}
      configChecklists={configChecklists} />
  default:
    return null
  }
}

export default ChecklistView
