import React from "react"

const ChecklistTemplate3 = props => {
  const { selected } = props
  return (
    <section className="accordions">
      <article className="accordion is-active">
        <div className="accordion-header toggle">
          <div className="field is-grouped">
            <div className="control">
              <p>Category</p>
            </div>
            {selected &&
            <div className="column">
              <div className="control">
                <a>
                  <span className="has-text-link">
                    <i className="fas fa-pencil-alt" />
                  </span>
                </a>
              </div>
            </div>
            }
          </div>
          {selected &&
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link is-small">Add List Item</button>
              </div>
              <div className="control">
                <button className="button is-light is-small">Reset</button>
              </div>
            </div>
          }
        </div>
        <div className="accordion-body">
          <div className="accordion-content">
            <div>
              <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>
                      <div className="field is-grouped">
                        <div className="control">
                          <p className="emmaTablesTh">List Item</p>
                        </div>
                        {selected &&
                          <div className="control">
                            <a>
                              <span className="has-text-link">
                                <i className="fas fa-pencil-alt" />
                              </span>
                            </a>
                          </div>
                        }
                      </div>
                    </th>
                    <th>
                      <div className="field is-grouped">
                        <div className="control">
                          <p className="emmaTablesTh">Col Header (e.g. $/1000)</p>
                        </div>
                        {selected &&
                          <div className="control">
                            <a>
                              <span className="has-text-link">
                                <i className="fas fa-pencil-alt" />
                              </span>
                            </a>
                          </div>
                        }
                      </div>
                    </th>
                    <th>
                      <div className="field is-grouped">
                        <div className="control">
                          <p className="emmaTablesTh">Col Header (e.g. Amount)</p>
                        </div>
                        {selected &&
                          <div className="control">
                            <a>
                              <span className="has-text-link">
                                <i className="fas fa-pencil-alt" />
                              </span>
                            </a>
                          </div>
                        }
                      </div>
                    </th>
                    <th>
                      <p className="emmaTablesTh">Drop Item</p>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="soeExpLblVal">
                      <input className="input is-small is-link" type="text" placeholder="Checklist Item"/>
                    </td>
                    <td>
                      <input className="input is-small is-link" type="text" placeholder="3.28350"/>
                    </td>
                    <td>
                      <input className="input is-small is-link" type="text" placeholder="200,556.25"/>
                    </td>
                    <td>
                      <a>
                        <span className="has-text-link">
                          <i className="far fa-trash-alt" />
                        </span>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </article>
    </section>
  )
}

export default ChecklistTemplate3
