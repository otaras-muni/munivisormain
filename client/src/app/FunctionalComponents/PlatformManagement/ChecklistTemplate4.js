import React from "react"
import {TextLabelInput} from "../../GlobalComponents/TextViewBox";

const ChecklistTemplate4 = () => (
  <section className="accordions">

    <article className="accordion is-active">
      <div className="accordion-header toggle">
        <div className="field is-grouped">
          <div className="control">
            <p>Category</p>
          </div>
          <div className="column">
            <div className="control">
              <a>
                <span className="has-text-link">
                  <i className="fas fa-pencil-alt" />
                </span>
              </a>
            </div>
          </div>
        </div>
        <div className="field is-grouped">
          <div className="control">
            <button className="button is-link is-small">Add List Item</button>
          </div>
          <div className="control">
            <button className="button is-light is-small">Reset</button>
          </div>
        </div>
      </div>

      <div className="accordion-body">
        <div className="accordion-content">

          <div>

            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                <tr>
                  <th>
                    <div className="field is-grouped">
                      <div className="control">
                        <p className="emmaTablesTh">List Item</p>
                      </div>
                      <div className="control">
                        <a>
                          <span className="has-text-link">
                            <i className="fas fa-pencil-alt" />
                          </span>
                        </a>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="field is-grouped">
                      <div className="control">
                        <p className="emmaTablesTh">Responsible Party</p>
                      </div>
                      <div className="control">
                        <a>
                          <span className="has-text-link">
                            <i className="fas fa-pencil-alt" />
                          </span>
                        </a>
                      </div>
                    </div>
                  </th>
                  <th>
                    <p className="emmaTablesTh">Start Date</p>
                  </th>
                  <th>
                    <p className="emmaTablesTh">End Date</p>
                  </th>
                  <th>
                    <p className="emmaTablesTh">Status</p>
                  </th>
                  <th>
                    <p className="emmaTablesTh">Drop</p>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="soeExpLblVal">
                    <input className="input is-small is-link" type="text" placeholder="Checklist Item"/>
                  </td>
                  <td>
                    <input className="input is-small is-link" type="text" placeholder="Picklist from CRM"/>
                  </td>
                  <td>
                    <TextLabelInput disabled />
                    {/* <input id="datepickerDemo" className="input is-small is-link" type="date"/> */}
                  </td>
                  <td>
                    {/* <input id="datepickerDemo" className="input is-small is-link" type="date"/> */}
                    <TextLabelInput disabled />
                  </td>
                  <td>
                    <div className="select is-small is-link">
                      <select>
                        <option value="" disabled selected>Pick</option>
                        <option>LKUPSOEEVENTSTATUS</option>
                        <option>Planned</option>
                      </select>
                    </div>
                  </td>
                  <td>
                    <a>
                      <span className="has-text-link">
                        <i className="far fa-trash-alt" />
                      </span>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>

          </div>

        </div>
      </div>

    </article>
  </section>
)

export default ChecklistTemplate4
