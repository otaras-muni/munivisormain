import React, { Component } from "react"
import { insertNotification, getNotification } from "../../../globalutilities/helpers"
import { toast } from "react-toastify"
import { updateAuditLog } from "GlobalUtils/helpers"
import CONST from "../../../globalutilities/consts"
import Accordion from "../../GlobalComponents/Accordion"
import RatingSection from "../../GlobalComponents/RatingSection"

class ConfigNotification extends Component {
  constructor(props) {
    super(props)
    this.state = {
      self: false,
      prospect: false,
      thirdParty: false,
      disabled: false
    }
  }

  async componentDidMount() {
    const resGetNotification = await getNotification()
    const notifications = (resGetNotification && resGetNotification.notifications) || {}
    this.setState({
      self: notifications.self,
      prospect: notifications.client,
      thirdParty: notifications.thirdParty,
    })
  }

  handleChange = (event) => {
    const target = event.target
    const value = target.type === "checkbox" ? target.checked : target.value
    this.setState({
      [target.name]: value
    })
  }

  handleSubmit = () => {
    const {self, prospect, thirdParty,} = this.state
    const {user} = this.props
    const {userId, userFirstName, userLastName} = user
    const notification = {
      self,
      client: prospect,
      thirdParty,
    }
    this.setState({
      disabled: true
    }, async ()=>{
      const res = await insertNotification(notification)
      if(res && res.status === 200){
        const selfMsg = self ? "Firm users level" : ""
        const clMsg = prospect ? "Client/Prospects users level" : ""
        const tpMsg = thirdParty ? "Third party users level" : ""
        const changelog = { userId, userName: `${userFirstName} ${userLastName}`, log: `Set notifications configuration at ${selfMsg} ${clMsg ? `, ${clMsg}` : ""} ${tpMsg ? `, ${tpMsg}` : ""}`, date: new Date() }
        await updateAuditLog("config", [changelog])
        toast("Changes Saved", { autoClose: CONST.toastTimeout, type: toast.TYPE.SUCCESS })
        this.setState({ disabled: false })
      }else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR })
        this.setState({ disabled: false })
      }
    })
  }

  render() {
    const {self, prospect, thirdParty, disabled} = this.state
    return (
      <div id="main">
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title="Email Notification Configuration"
            >
              {activeAccordions.includes(0) && (
                <div>
                  <div className="columns">
                    <div className="column">
                      <label className="checkbox-button"> Send emails to only firm users
                        <input type="checkbox"
                          name="self"
                          checked={self}
                          onClick={this.handleChange}/>
                        <span className="checkmark"/>
                      </label>
                    </div>
                    <div className="column">
                      <label className="checkbox-button"> Send emails to users of clients and prospects
                        <input type="checkbox"
                          name="prospect"
                          checked={prospect}
                          onClick={this.handleChange}/>
                        <span className="checkmark"/>
                      </label>
                    </div>
                    <div className="column">
                      <label className="checkbox-button"> Send emails to users of Third Party market professionals
                        <input type="checkbox"
                          name="thirdParty"
                          checked={thirdParty}
                          onClick={this.handleChange}/>
                        <span className="checkmark"/>
                      </label>
                    </div>
                  </div>
                  <hr/>
                  <button className="button is-link"
                    disabled={disabled}
                    onClick={this.handleSubmit}>Save</button>
                </div>
              )}
            </RatingSection>
          )}
        />
      </div>
    )
  }
}

export default ConfigNotification

