import React, { Component } from "react"
import { withRouter } from "react-router-dom"

import { connect } from "react-redux"
import { Multiselect, Combobox } from "react-widgets"

import { NOTIFICATIONS_MODES, TIME_UNITS } from "GlobalUtils/consts"
import { getConfigNotifications, addNewNotification, saveConfigNotifications } from "../../StateManagement/actions"
import Loader from "../../GlobalComponents/Loader"

class NotificationsMain extends Component {
  constructor(props) {
    super(props)
    this.state={selectedId: "", selectedAction: "", modes: [], changeLog: [],
      searchString: "",  timeTriggerValue: 0, timeTriggerUnit: "",
      stringToMatch: "", frequencyValue: 0, frequencyUnit: "", limit: 0,
      isActive: true, recurring: false, defaultOptIn: true, canUserChange: true,
      name: "", description: "", view: "", configNotifications: [] }
    this.changeNotificationFields = this.changeNotificationFields.bind(this)
    this.changeModes = this.changeModes.bind(this)
    this.addNewNotification = this.addNewNotification.bind(this)
    this.changeTriggerTimeUnit = this.changeTriggerTimeUnit.bind(this)
    this.changeFrequencyUnit = this.changeFrequencyUnit.bind(this)
    this.changeSearchString = this.changeSearchString.bind(this)
    this.handleKeyPressinSearch = this.handleKeyPressinSearch.bind(this)
    this.copyNotification = this.copyNotification.bind(this)
    this.saveNotification = this.saveNotification.bind(this)
  }

  componentDidMount() {
    this.props.getConfigNotifications(this.props.token)
    this.setNotificationsInState(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.setNotificationsInState(nextProps)
  }

  setNotificationsInState({ configNotifications }) {
    configNotifications = configNotifications || []
    this.setState({ configNotifications })
  }

  saveNotification() {
    this.setState(prevState => {
      const {selectedId, name, description, modes, timeTriggerUnit,
        recurring, timeTriggerValue, frequencyValue, limit, defaultOptIn,
        canUserChange, frequencyUnit, isActive } = prevState
      const configNotifications = [ ...prevState.configNotifications ]
      const idx = configNotifications.findIndex(e => e.id ===selectedId)
      const timeTrigger = {}
      const frequency = {}
      timeTrigger.value = timeTriggerValue
      timeTrigger.unit =  timeTriggerUnit
      frequency.value = frequencyValue
      frequency.unit = frequencyUnit
      configNotifications[idx] = { id: selectedId, name, description, modes,
        recurring, timeTrigger, frequency, limit, defaultOptIn, canUserChange,
        isActive }
      return { configNotifications }
    }, this.saveInDB)
  }

  saveInDB() {
    const { configNotifications, changeLog } = this.state
    const { token } = this.props
    this.props.saveConfigNotifications(token, configNotifications, changeLog)
    this.setState({selectedId: "", selectedAction: "", modes: [], changeLog: [],
      searchString: "",  timeTriggerValue: 0, timeTriggerUnit: "",
      stringToMatch: "", frequencyValue: 0, frequencyUnit: "", limit: 0,
      isActive: true, recurring: false, defaultOptIn: true, canUserChange: true,
      name: "", description: "", view: ""})
  }

  changeTriggerTimeUnit(timeTriggerUnit) {
    this.setState({ timeTriggerUnit })
  }

  changeFrequencyUnit(frequencyUnit) {
    this.setState({ frequencyUnit })
  }

  changeNotificationFields(e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    this.setState({ [name]: value })
  }

  changeModes(modes) {
    this.setState({ modes })
  }

  changeSearchString(e) {
    const searchString = e.target.value
    if(!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString })
    }
  }

  handleKeyPressinSearch(e) {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  performAction() {
    const { selectedId, selectedAction } = this.state
    switch(selectedAction) {
    case "edit" :
      this.setState({ view: "edit" })
      break
    case "copy" :
      this.copyNotification(selectedId)
      break
    default:
      console.log("unknown action")
      break
    }
  }

  addNewNotification() {
    this.setState(prevState => {
      const configNotifications = [...prevState.configNotifications]
      const ids = configNotifications.map(c => +c.id.split("N")[1])
      ids.sort((a, b) => b-a)
      const newListId = `N${(ids[0] || 0) + 1}`
      const newNotification = {
        id: newListId,
        name: "",
        description: "",
        modes: [],
        recurring: false,
        timeTrigger: {
          value: 0,
          unit: ""
        },
        frequency: {
          value: 0,
          unit: ""
        },
        limit: 0,
        defaultOptIn: true,
        canUserChange: true,
        isActive: true
      }
      configNotifications.push(newNotification)
      return { configNotifications, selectedId: newListId, selectedAction: "edit",
        searchString: "",  timeTriggerValue: 0, timeTriggerUnit: "", modes: [],
        stringToMatch: "", frequencyValue: 0, frequencyUnit: "", limit: 0,
        isActive: true, recurring: false, defaultOptIn: true, canUserChange: true,
        name: "", description: "", view: "edit" }
    })
  }

  copyNotification(id) {
    this.setState(prevState => {
      const configNotifications = [...prevState.configNotifications]
      const ids = configNotifications.map(c => +c.id.split("N")[1])
      ids.sort((a, b) => b-a)
      const newListId = `N${(ids[0] || 0) + 1}`
      const idx = configNotifications.findIndex(n => n.id === id)
      configNotifications.push({ ...configNotifications[idx], id: newListId })
      return { selectedId: newListId, configNotifications,
        selectedAction: "edit", view: "edit" }
    })
  }

  changeSelectedAction(n, e) {
    console.log("n : ", n)
    const { id, name, description, modes, recurring, timeTrigger, frequency,
      limit, defaultOptIn, canUserChange, isActive } = n
    const timeTriggerValue = timeTrigger.value
    const timeTriggerUnit = timeTrigger.unit
    const frequencyValue = frequency.value
    const frequencyUnit = frequency.unit
    const selectedAction = e.target.value
    this.setState({ selectedId: id, name, description, modes, recurring,
      limit, defaultOptIn, canUserChange, isActive,
      timeTriggerValue, timeTriggerUnit, frequencyValue, frequencyUnit,
      selectedAction }, this.performAction)
  }

  renderTableHeader1() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Modes</p>
        </th>
        <th>
          <p className="emmaTablesTh">Recurring</p>
        </th>
        <th>
          <p className="emmaTablesTh">Limit</p>
        </th>
        <th>
          <p className="emmaTablesTh">Default Opt In</p>
        </th>
      </tr>
    )
  }

  renderTableHeader2() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">Trigger Value</p>
        </th>
        <th>
          <p className="emmaTablesTh">Trigger Unit</p>
        </th>
        <th>
          <p className="emmaTablesTh">Frequency Value</p>
        </th>
        <th>
          <p className="emmaTablesTh">Frequency Unit</p>
        </th>
        <th>
          <p className="emmaTablesTh">Can User Change?</p>
        </th>
        <th>
          <p className="emmaTablesTh">Is Active</p>
        </th>
        <th>
          <p className="emmaTablesTh">Action</p>
        </th>
      </tr>
    )
  }

  renderNotificationsRows1(n) {
    return (
      <tbody>
        <tr>
          <td className="emmaTablesTd">
            {n.id}
          </td>
          <td className="emmaTablesTd">
            {n.name}
          </td>
          <td className="emmaTablesTd">
            {n.modes.toString()}
          </td>
          <td className="emmaTablesTd">
            {n.recurring ? "Yes" : "No"}
          </td>
          <td className="emmaTablesTd">
            {n.limit}
          </td>
          <td className="emmaTablesTd">
            {n.defaultOptIn ? "Yes" : "No"}
          </td>
        </tr>
      </tbody>
    )
  }

  renderNotificationsRows2(n) {
    const { selectedId, selectedAction } = this.state
    return (
      <tbody>
        <tr>
          <td className="emmaTablesTd">
            {n.timeTrigger.value}
          </td>
          <td className="emmaTablesTd">
            {n.timeTrigger.unit}
          </td>
          <td className="emmaTablesTd">
            {n.frequency.value}
          </td>
          <td className="emmaTablesTd">
            {n.frequency.unit}
          </td>
          <td className="emmaTablesTd">
            {n.canUserChange ? "Yes" : "No"}
          </td>
          <td className="emmaTablesTd">
            {n.isActive ? "Yes" : "No"}
          </td>
          <td>
            <div className="select is-small is-link">
              <select value={ selectedId === n.id ?  selectedAction : "" }
                onChange={this.changeSelectedAction.bind(this, n)}>
                <option value="" disabled>Action</option>
                <option value="edit">Edit</option>
                <option value="copy">Make a copy</option>
              </select>
            </div>
          </td>
        </tr>
      </tbody>
    )
  }

  renderNotificationsEdit1(n) {
    const { modes, recurring, name, limit, defaultOptIn } = this.state
    return (
      <tbody>
        <tr>
          <td className="emmaTablesTd">
            {n.id}
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder="Name"
              name="name"
              value={name}
              onChange={this.changeNotificationFields}
            />
          </td>
          <td className="emmaTablesTd">
            <Multiselect
              data={NOTIFICATIONS_MODES}
              caseSensitive={false}
              minLength={1}
              value={modes}
              filter="contains"
              onChange={this.changeModes}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="checkbox"
              name="recurring"
              type="checkbox"
              checked={!!recurring}
              onChange={this.changeNotificationFields} />
          </td>
          <td className="emmaTablesTd">
            <input className="input"
              name="limit"
              style={{maxWidth: "50px"}}
              type="number"
              value={limit}
              onChange={this.changeNotificationFields} />
          </td>
          <td className="emmaTablesTd">
            <input className="checkbox"
              name="defaultOptIn"
              type="checkbox"
              checked={!!defaultOptIn}
              onChange={this.changeNotificationFields} />
          </td>
        </tr>
      </tbody>
    )
  }

  renderNotificationsEdit2(n) {
    const { timeTriggerValue, timeTriggerUnit, frequencyValue, frequencyUnit,
      canUserChange, isActive, selectedId, selectedAction } = this.state
    return (
      <tbody>
        <tr>
          <td className="emmaTablesTd">
            <input className="input"
              style={{maxWidth: "50px"}}
              name="timeTriggerValue"
              type="number"
              value={timeTriggerValue}
              onChange={this.changeNotificationFields} />
          </td>
          <td className="emmaTablesTd">
            <Combobox
              style={{ "maxWidth": "150px" }}
              data={TIME_UNITS}
              caseSensitive={false}
              minLength={1}
              value={timeTriggerUnit}
              onChange={this.changeTriggerTimeUnit}
              filter='contains'
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input"
              style={{maxWidth: "50px"}}
              name="frequencyValue"
              type="number"
              value={frequencyValue}
              onChange={this.changeNotificationFields} />
          </td>
          <td className="emmaTablesTd">
            <Combobox
              style={{ "maxWidth": "150px" }}
              data={TIME_UNITS}
              caseSensitive={false}
              minLength={1}
              value={frequencyUnit}
              onChange={this.changeFrequencyUnit}
              filter='contains'
            />
          </td>
          <td className="emmaTablesTd">
            <input className="checkbox"
              name="canUserChange"
              type="checkbox"
              checked={!!canUserChange}
              onChange={this.changeNotificationFields} />
          </td>
          <td className="emmaTablesTd">
            <input className="checkbox"
              name="isActive"
              type="checkbox"
              checked={!!isActive}
              onChange={this.changeNotificationFields} />
          </td>
          <td>
            <div className="select is-small is-link">
              <select value={ selectedId === n.id ?  selectedAction : "" }
                onChange={this.changeSelectedAction.bind(this, n)}>
                <option value="" disabled>Action</option>
                <option value="edit">Edit</option>
                <option value="copy">Make a copy</option>
              </select>
            </div>
          </td>
        </tr>
      </tbody>
    )
  }

  renderNotificationsRows(notifications) {
    const { view, selectedId, description } = this.state
    const rows = notifications.map(n => (
      <div className="box" key={n.id}>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            {this.renderTableHeader1()}
          </thead>
          { view === "edit" && selectedId === n.id ?
            this.renderNotificationsEdit1(n || []) :
            this.renderNotificationsRows1(n || [])}
        </table>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            {this.renderTableHeader2()}
          </thead>
          { view === "edit" && selectedId === n.id ?
            this.renderNotificationsEdit2(n || []) :
            this.renderNotificationsRows2(n || [])}
        </table>
        { view === "edit" && selectedId === n.id ?
          <input className="input is-small is-link"
            type="text"
            placeholder="Description"
            name="description"
            value={description}
            onChange={this.changeNotificationFields} /> :
          <p>{n.description}</p>
        }
        { view === "edit" && selectedId === n.id ?
          <button onClick={this.saveNotification}>Save</button>
          : undefined
        }
      </div>
    ))

    return rows
  }

  renderNotifications(notifications) {
    return (
      <section className="container">
        <section className="accordions box">
          <p className="title innerPgTitle">Search notification to clone/copy</p>
          <div className="columns">
            <div className="column">
              <p className="control has-icons-left">
                <input className="input is-small is-link"
                  type="text"
                  placeholder="search by name or description"
                  value={this.state.searchString}
                  onChange={this.changeSearchString}
                  onKeyPress={this.handleKeyPressinSearch}
                />
                <span className="icon is-left has-background-dark">
                  <i className="fas fa-search" />
                </span>
              </p>
            </div>
            <div className="column">
              <button className="button is-link is-small"
                onClick={this.addNewNotification}>Add new notification</button>
            </div>
          </div>
          <div className="box" >
            {this.renderNotificationsRows(notifications)}
          </div>
        </section>
      </section>
    )
  }

  render() {
    const { stringToMatch, configNotifications } = this.state
    // const { configNotifications } = this.props
    const matchingLists = stringToMatch ?
      configNotifications.filter(n =>
        n.name.toLowerCase().includes(stringToMatch.toLowerCase()) ||
        n.description.toLowerCase().includes(stringToMatch.toLowerCase())
      ) : configNotifications

    if(configNotifications) {
      return (
        <div id="main">
          {this.renderNotifications(matchingLists || [])}
        </div>
      )
    }
    return <Loader />
  }
}

// export default NotificationsMain

const mapStateToProps = ({ configNotifications, auth: { token} }) => ({
  configNotifications,
  token
})

export default connect(mapStateToProps, { getConfigNotifications,
  addNewNotification, saveConfigNotifications })(withRouter(NotificationsMain))
