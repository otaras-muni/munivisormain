import React, { Component } from "react"
import { Link, withRouter } from "react-router-dom"
import { checkListsList } from "GlobalUtils/helpers"

import { connect } from "react-redux"

import qs from "qs"

import { getConfigChecklist, addNewChecklist, saveConfigChecklist } from "../../StateManagement/actions"
import Loader from "../../GlobalComponents/Loader"
import ChecklistTemplateSelection from "./ChecklistTemplateSelection"
import ChecklistView from "./ChecklistView"

class ChecklistsMain extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedListId: "", selectedAction: "", newListId: "",
      newChecklistName: "", newChecklistAttributedTo: "", searchString: "",
      newChecklistBestPractise: "", newChecklistNotes: "", stringToMatch: ""
    }
    this.addNewChecklist = this.addNewChecklist.bind(this)
    this.changeNewChecklistFields = this.changeNewChecklistFields.bind(this)
    this.changeSearchString = this.changeSearchString.bind(this)
    this.handleKeyPressinSearch = this.handleKeyPressinSearch.bind(this)
  }

  async componentDidMount() {
    const configChecklists = await checkListsList(this.props.token)
    // this.props.getConfigChecklist(this.props.token)
    this.setState({configChecklists})
  }

  componentWillUnmount() {
    const { userId, userName, token, firmName } = this.props
    const { configChecklists } = this.state
    const { newListId, newChecklistName, newChecklistAttributedTo,
      newChecklistBestPractise, newChecklistNotes } = this.state
    console.log("unmount : ", newListId)
    if (newListId) {
      const newChecklist = {
        id: newListId,
        name: newChecklistName,
        attributedTo: newChecklistAttributedTo || firmName,
        bestPractice: newChecklistBestPractise,
        notes: newChecklistNotes,
        lastUpdated: {
          date: new Date().toISOString(),
          by: "user"
        },
        itemHeaders: { "Add a title": ["List Item", "Responsible Party"]},
        data: {
          "Add a title": [{}]
        }
      }
      const checklists = [...configChecklists.slice(0, -1), newChecklist]
      const changeLog = [{
        userId,
        userName,
        log: `added a new checklist with id ${newListId}`,
        date: new Date()
      }]
      // this.props.saveConfigChecklist(token, checklists, changeLog, "redux-store")
    }
  }

  changeNewChecklistFields(e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  changeSearchString(e) {
    const searchString = e.target.value
    if (!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString })
    }
  }

  handleKeyPressinSearch(e) {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  performAction() {
    const { selectedListId, selectedAction } = this.state
    switch (selectedAction) {
    case "edit":
      this.props.history.push(`/configuration/checklists/${selectedListId}`)
      break
    case "copy":
      console.log("copying : ", selectedListId)
      this.copyChecklist(selectedListId)
      break
    default:
      console.log("unknown action")
      break
    }
  }

  addNewChecklist() {
    if (!this.state.newListId) {
      const configChecklists = [...this.state.configChecklists]
      const ids = configChecklists.map(c => +c.id.split("CL")[1])
      ids.sort((a, b) => b - a)
      const newListId = `CL${(ids[0] || 0) + 1}`
      const newChecklist = {
        id: newListId,
        name: "",
        attributedTo: "",
        bestPractice: "",
        notes: "",
        lastUpdated: {
          date: new Date().toISOString(),
          by: "user"
        },
        data: {
          "Add a title": [{}]
        }
      }
      configChecklists.push(newChecklist)
      // this.props.addNewChecklist(configChecklists)
      this.setState({ newListId, newChecklist, configChecklists })
    }
  }

  copyChecklist(id) {
    if (!this.state.newListId) {
      const configChecklists = [...this.state.configChecklists]
      const checklistToCopy = configChecklists.filter(c => c.id === id)[0] || {}
      const ids = configChecklists.map(c => +c.id.split("CL")[1])
      ids.sort((a, b) => b - a)
      const newListId = `CL${ids[0] + 1}`
      const newChecklist = {
        ...checklistToCopy,
        id: newListId,
        lastUpdated: {
          date: new Date().toISOString(),
          by: "user"
        }
      }
      configChecklists.push(newChecklist)
      // this.props.addNewChecklist(configChecklists)
      this.props.history.push(`/configuration/checklists/${newListId}`)
    }
  }

  changeSelectedAction(selectedListId, e) {
    console.log("selectedListId : ", selectedListId)
    console.log("e : ", e)
    const selectedAction = e.target.value
    this.setState({ selectedListId, selectedAction }, this.performAction)
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Chk List Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Attributed to</p>
        </th>
        <th>
          <p className="emmaTablesTh">Best Practice</p>
        </th>
        <th>
          <p className="emmaTablesTh">Notes</p>
        </th>
        <th>
          <p className="emmaTablesTh">Last Updated</p>
        </th>
        <th>
          <p className="emmaTablesTh">Action</p>
        </th>
      </tr>
    )
  }

  renderChecklistsRows(checklists) {
    let { newListId, newChecklistName, newChecklistAttributedTo,
      newChecklistBestPractise, newChecklistNotes } = this.state
    newChecklistAttributedTo = newChecklistAttributedTo || this.props.firmName
    const currentChecklists = newListId ? checklists.slice(0, -1) : checklists
    const newChecklist = newListId ? checklists.slice(-1)[0] : {}
    const rows = currentChecklists.map(d =>
      <tr key={d._id || d.id}>
        <td className="emmaTablesTd">
          <Link to={`/configuration/checklists/${d.id}`}>{d.id}</Link>
        </td>
        <td className="emmaTablesTd">
          <small>{d.name}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.attributedTo}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.bestPractice}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.notes}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{new Date(d.lastUpdated.date).toLocaleString()} {d.lastUpdated.by}</small>
        </td>
        <td>
          <div className="select is-small is-link">
            <select value={this.state.selectedAction} onChange={this.changeSelectedAction.bind(this, d.id)}>
              <option value="" disabled>Action</option>
              <option value="edit">Edit</option>
              {/*<option value="copy">Make a copy</option>*/}
            </select>
          </div>
        </td>
      </tr>
    )
    if (newListId) {
      const query = qs.stringify({
        newListId,
        newChecklistName,
        newChecklistAttributedTo,
        newChecklistBestPractise,
        newChecklistNotes
      })
      rows.unshift(
        <tr key={newListId}>
          <td className="emmaTablesTd">
            <small>{newListId}</small>
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder="Checklist name"
              name="newChecklistName"
              value={newChecklistName}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder="Defaults to firmname"
              name="newChecklistAttributedTo"
              value={newChecklistAttributedTo}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder=""
              name="newChecklistBestPractise"
              value={newChecklistBestPractise}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <input className="input is-small is-link"
              type="text"
              placeholder=""
              name="newChecklistNotes"
              value={newChecklistNotes}
              onChange={this.changeNewChecklistFields}
            />
          </td>
          <td className="emmaTablesTd">
            <small>{new Date(newChecklist.lastUpdated.date).toLocaleString()} {newChecklist.lastUpdated.by}</small>
          </td>
          <td className="emmaTablesTd" style={{paddingTop: "10px"}}>
            <div className="is-small is-link">
              <Link to={`/configuration/checklists/${newListId}?${query}`}>Save & Update</Link>
            </div>
          </td>
        </tr>
      )
    }
    return rows
  }

  renderChecklists(checklists) {
    return (
      <section className="container">
        <section className="box">
          <p className="title innerPgTitle">Search checklists to clone/copy</p>
          <div className="columns">
            <div className="column">
              <p className="control has-icons-left">
                <input className="input is-small is-link"
                  type="text"
                  placeholder="search by checklist name"
                  value={this.state.searchString}
                  onChange={this.changeSearchString}
                  onKeyPress={this.handleKeyPressinSearch}
                />
                <span className="icon is-left has-background-dark">
                  <i className="fas fa-search" />
                </span>
              </p>
            </div>
            <div className="column">
              <button className="button is-link is-small" onClick={this.addNewChecklist}>Add new checklist</button>
            </div>
          </div>
          <div className="box overflow-auto" >
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                {this.renderTableHeader()}
              </thead>
              <tbody>
                {this.renderChecklistsRows(checklists)}
              </tbody>
            </table>
          </div>
        </section>
      </section>
    )
  }

  render() {
    const { stringToMatch, configChecklists } = this.state
    const { userId, userName, token, option, location: { search } } = this.props
    // console.log("option : ", option)
    const matchingLists = stringToMatch ?
      configChecklists.filter(c => c.name.toLowerCase().includes(stringToMatch.toLowerCase())) : configChecklists
    if (option) {
      // console.log("search : ", search)
      if (search) {
        return (
          <div className="container box overflow-auto">
            <ChecklistTemplateSelection
              token={token}
              userId={userId}
              userName={userName}
              configChecklists={configChecklists}
              query={this.props.location.search}
            />
          </div>
        )
      }
      const checklist = configChecklists && configChecklists.length && configChecklists.filter(c => c.id === option)[0]
      if (checklist) {
        const { data } = checklist
        let oldList = true
        Object.keys(data).some(k => {
          if (data[k].length === 1 && !data[k][0]) {
            oldList = false
            return true
          }
        })
        return (
          <div className="container box overflow-auto">
            <ChecklistView start={!oldList}
              cid={option}
              userId={userId}
              userName={userName}
              token={token}
              checklist={checklist}
              configChecklists={configChecklists} />
          </div>
        )
      }
    }

    if (configChecklists) {
      return (
        <div id="main">
          {this.renderChecklists(matchingLists || [])}
        </div>
      )
    }

    return <Loader />
  }
}

const mapStateToProps = ({ configChecklists, auth: { token,
  loginDetails: { userEntities } } }) => {
  const { userId, firmName, msrbFirmName, userFirstName, userLastName } = userEntities[0]
  return {
    userId,
    userName: `${userFirstName} ${userLastName}`,
    configChecklists,
    token,
    firmName: firmName || msrbFirmName || ""
  }
}

export default connect(mapStateToProps, {
  getConfigChecklist,
  addNewChecklist, saveConfigChecklist
})(withRouter(ChecklistsMain))
