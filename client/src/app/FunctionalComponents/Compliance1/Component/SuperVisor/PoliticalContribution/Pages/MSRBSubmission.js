import React, { Component } from "react"
import {withRouter} from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import { connect} from "react-redux"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import Documents from "../../../../../../GlobalComponents/DocumentPage"
import Loader from "../../../../../../GlobalComponents/Loader"
import CONST, {ContextType} from "../../../../../../../globalutilities/consts"
import {
  fetchMsrbSubmissionDocuments, pullContributeDetailsDocuments,
  putContributeDetailsDocuments,
  putMsrbSubmissionDocuments,
  putPoliContributionAuditLogs
} from "../../../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import {SelectLabelInput, TextLabelBox} from "../../../../../../GlobalComponents/TextViewBox"
import Audit from "../../../../../../GlobalComponents/Audit"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../../../GlobalComponents/Accordion"

class People extends Component {
  constructor(props) {
    super(props)

    this.state = {
      documentsList: [],
      dropDown: {
        polContribQuarterOfDisclosureFirm: [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        polContribYearOfDisclosureFirm: [],
      },
      disclosureInfo: {
        year: "",
        quarter: ""
      },
      docDetails: {},
      docLoading: false,
      auditLogs: []
    }
  }

  async componentDidMount() {
    const polContribYearOfDisclosureFirm = []
    let min = 2015, max = min + 10
    for (let i = min; i <= max; i++) { polContribYearOfDisclosureFirm.push(i) }
    this.setState ({
      dropDown: {
        ...this.state.dropDown,
        polContribYearOfDisclosureFirm,
      }
    })
  }

  onDocSave = ( docs, callback) => {
    console.log(docs)
    const {docDetails} = this.state
    putMsrbSubmissionDocuments(`?year=${docDetails.year}&quarter=${docDetails.quarter}`, docs, (res)=> {
      if (res && res.status === 200) {
        toast("Documents has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.msrbSubmissions) || [],
        })
        this.onAuditSave("documents")
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    const { disclosureInfo} = this.state
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc
      }
      type = "updateMeta"
    }
    putMsrbSubmissionDocuments(`?details=${type}&year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}`, document, (res) => {
      if (res && res.status === 200) {
        toast("Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if(name){
          callback({
            status: true,
          })
        }else {
          callback({
            status: true,
            documentsList: (res.data && res.data.msrbSubmissions) || [],
          })
        }
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const {docDetails} = this.state
    pullContributeDetailsDocuments( docDetails._id, documentId, "msrbSubmissions", (res) => {
      if (res && res.status === 200) {
        toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        callback({
          status: true,
          documentsList: (res.data && res.data.msrbSubmissions) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onAuditSave = (key) => {
    const {disclosureInfo} = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "Political Contributions and Prohibitions"
        log.superVisorSubSection = "MSRB Submissions"
      })
      putPoliContributionAuditLogs(`?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}`, auditLogs, (res)=> {
        if(res && res.status === 200) {
          this.setState({
            docLoading: false,
            auditLogs: res.data.auditLogs || []
          })
        }
      })
      const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      this.props.updateAuditLog(remainLogs)
    }
  }

  onChange = (e) => {
    const disclosureInfo = this.state.disclosureInfo
    disclosureInfo[e.target.name] = e.target.value
    this.setState({
      disclosureInfo
    })
  }

  fetchDocuments =() => {
    const {disclosureInfo} = this.state
    this.setState({
      docLoading: true
    }, () => {
      fetchMsrbSubmissionDocuments(`?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}`, (res) => {
        this.setState({
          docDetails: res || {},
          documentsList: (res && res.msrbSubmissions && res.msrbSubmissions.length) ? cloneDeep(res.msrbSubmissions) : [],
          auditLogs: (res && res.auditLogs && res.auditLogs.length) ? cloneDeep(res.auditLogs) : [],
          docLoading: false,
        })
      })
    })
  }

  render() {
    const { documentsList, disclosureInfo, dropDown, docDetails, docLoading, auditLogs } = this.state
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Political Contributions & Prohibitions",
    }
    const value = (val) => {
      const quarter = [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}]
      const qt = quarter.find(q => q.value === parseInt(val,10))
      return qt.name
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Report Quarter</p>
            <div className="select is-small is-link">
              <select name="quarter" value={disclosureInfo.quarter || ""} onChange={this.onChange}>
                <option value="">Pick</option>
                {
                  dropDown.polContribQuarterOfDisclosureFirm.map((q) => (
                    <option key={q.value} value={q.value}>{q.name}</option>
                  ))
                }
              </select>
            </div>
          </div>
          <SelectLabelInput label="Report Year" list={dropDown.polContribYearOfDisclosureFirm || []} name="year" value={disclosureInfo.year || ""} onChange={this.onChange}/>
          <div className="column">
            <div className="field is-grouped" style={{paddingTop: 20}}>
              <div className="control">
                <button className="button is-link is-small" onClick={this.fetchDocuments} disabled={!disclosureInfo.year || !disclosureInfo.quarter}>Search</button>
              </div>
            </div>
          </div>
        </div>
        {
          docLoading ? <Loader/> :
            <div>
              {
                docDetails.finAdvisorEntityId ?
                  <div className="box">
                    <div className="columns">
                      <TextLabelBox label="Report Quarter" value={docDetails.quarter ? value(docDetails.quarter) : ""}/>
                      <TextLabelBox label="Report Year" value={docDetails.year || ""}/>
                    </div>
                    <Documents {...this.props} onSave={this.onDocSave} tranId={docDetails._id} title="Documents" pickCategory="LKUPDOCCATS" pickSubCategory="LKUPCORRESPONDENCEDOCS"
                      pickAction="LKUPCOMPLIANCEDOCACTION" pickType="LKUPPOLDOCTYPE" staticField={staticField || {}} documents={documentsList} category="msrbSubmissions" onStatusChange={this.onStatusChange}
                      contextType={ContextType.supervisor.msrbSubmissions}  tableStyle={{fontSize: "smaller"}} onDeleteDoc={this.onDeleteDoc}/>
                    <Accordion multiple activeItem={[0]} boxHidden render={({activeAccordions, onAccordion}) =>
                      <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{overflowY: "scroll", fontSize: "smaller"}}>
                        {activeAccordions.includes(2) &&
                        <Audit auditLogs={auditLogs || []}/>
                        }
                      </RatingSection>
                    }/>
                  </div>
                  :null
              }
            </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(People)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
