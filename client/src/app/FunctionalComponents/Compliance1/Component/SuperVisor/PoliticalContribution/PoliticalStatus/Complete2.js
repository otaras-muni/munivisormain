import React, { Component } from "react"
import queryString from "query-string"
import {toast} from "react-toastify"
import Loader from "Global/Loader"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import Questions from "../../../../../../GlobalComponents/Questions"
import CONST from "../../../../../../../globalutilities/consts"
import AffirmationAndSubmission from "../components/AffirmationAndSubmission"
import {
  getPoliContributionsDetails,
  postPoliContributions,
  putQuestionAnswes
} from "../../../../../../StateManagement/actions/Supervisor"

class Complete2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      negativeAnswers: CONST.Section.negativeAnswers,
      positiveAnswers: CONST.Section.positiveAnswers,
      disclosureInfo : CONST.SuperVisor.disclosureInfo || {},
      loading: true,
      isSaveDisabled: false,
    }
  }

  async componentWillMount() {
    const params = queryString.parse(this.props.history.location.search)
    getPoliContributionsDetails(`${params.disclosureFor}?year=${params.year}&quarter=${params.quarter}`, (res) => {
      this.setState(prevState => ({
        disclosureInfo: {
          ...prevState.disclosureInfo,
          ...params,
          affirmationNotes: res.affirmationNotes || ""
        },
        params,
        contDetails: res || {},
        loading: false,
        negativeAnswers: res && res.negativeAnswers && res.negativeAnswers.length ? res.negativeAnswers : CONST.Section.negativeAnswers,
        positiveAnswers: res && res.positiveAnswers && res.positiveAnswers.length ? res.positiveAnswers : CONST.Section.positiveAnswers,
      }))
    })
  }

  onChange = (e) => {
    this.setState({
      disclosureInfo: {
        ...this.state.disclosureInfo,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    })
  }

  onSaveAndRecord = (key) => {
    const {contDetails, negativeAnswers, positiveAnswers} = this.state
    let payload = {}
    if(key && typeof key === "string") {

      const newQue = this.state[key]
      const isExists = newQue.find(q => q.sectionHeader === "Others" && !q.isNew)
      if(isExists){
        const existsIndex = newQue.findIndex(q => q.sectionHeader === "Others" && !q.isNew)
        const newObj = newQue.find(q => q.sectionHeader === "Others" && q.isNew)
        if(newObj){
          isExists.sectionItems = isExists.sectionItems.concat(newObj.sectionItems || [])
          newQue[existsIndex] = isExists
          newQue.splice(newQue.length-1, 1)
        }
      }
      payload = {
        [key]: newQue
      }

    }else {

      const isExistsNegAns = negativeAnswers.find(q => q.sectionHeader === "Others" && !q.isNew)
      if(isExistsNegAns){
        const existsIndex = negativeAnswers.findIndex(q => q.sectionHeader === "Others" && !q.isNew)
        const newObj = negativeAnswers.find(q => q.sectionHeader === "Others" && q.isNew)
        if(newObj){
          isExistsNegAns.sectionItems = isExistsNegAns.sectionItems.concat(newObj.sectionItems || [])
          negativeAnswers[existsIndex] = isExistsNegAns
          negativeAnswers.splice(negativeAnswers.length-1, 1)
        }
      }

      const isExistsPosAns = positiveAnswers.find(q => q.sectionHeader === "Others" && !q.isNew)
      if(isExistsPosAns){
        const existsIndex = positiveAnswers.findIndex(q => q.sectionHeader === "Others" && !q.isNew)
        const newObj = positiveAnswers.find(q => q.sectionHeader === "Others" && q.isNew)
        if(newObj) {
          isExistsPosAns.sectionItems = isExistsPosAns.sectionItems.concat(newObj.sectionItems || [])
          positiveAnswers[existsIndex] = isExistsPosAns
          positiveAnswers.splice(positiveAnswers.length-1, 1)
        }
      }

      payload = {negativeAnswers, positiveAnswers}
    }
    if(contDetails && contDetails.contributeId && contDetails.userOrEntityId) {
      this.setState({
        isSaveDisabled: true
      }, () => {
        putQuestionAnswes(`${contDetails.contributeId._id}?userOrEntityId=${contDetails.userOrEntityId}`, payload, (res) => {
          if(res && res.status === 200) {
            toast("Answer successfully updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            let upState = {}
            if(key) {
              upState = {
                [key]: res.data && res.data[key] && res.data[key].length ? res.data[key] : CONST.Section[key],
              }
            }else {
              upState = {
                negativeAnswers: res.data && res.data.negativeAnswers && res.data.negativeAnswers.length ? res.data.negativeAnswers : CONST.Section.negativeAnswers,
                positiveAnswers: res.data && res.data.positiveAnswers && res.data.positiveAnswers.length ? res.data.positiveAnswers : CONST.Section.positiveAnswers,
              }
            }
            this.setState({
              isSaveDisabled: false,
              ...upState,
            }, () => this.props.history.push("/dashboard"))
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      })
    }
  }

  onSave = () => {
    const {disclosureInfo} = this.state
    if(disclosureInfo && disclosureInfo.quarter && disclosureInfo.year && disclosureInfo.disclosureFor) {
      this.setState({
        isSaveDisabled: true
      }, () => {
        postPoliContributions(disclosureInfo, (res) => {
          if(res && res.status === 200) {
            toast("Details successfully updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            this.setState({
              isSaveDisabled: false
            }, () => this.props.history.push("/dashboard"))
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          }
        })
      })
    }
  }

  onChangeItem = (key, rows) => {
    this.setState({
      [key]: rows
    })
  }

  render() {
    const { negativeAnswers, positiveAnswers, disclosureInfo, contDetails, isSaveDisabled } = this.state
    if (this.state.loading) {
      return <Loader/>
    }
    let queValue = []
    negativeAnswers && negativeAnswers.length && negativeAnswers.forEach((word) => {
      word.sectionItems.length ? word.sectionItems.forEach((item) => {
        queValue.push(!!item.sectionAffirm)
      }) : queValue.push(!!word.sectionAffirm)
    })
    let isQueDisabled = queValue.indexOf(true) !== -1

    positiveAnswers && positiveAnswers.length && positiveAnswers.forEach((word) => {
      word.sectionItems.length ? word.sectionItems.forEach((item) => {
        queValue.push(!!item.sectionAffirm)
      }) : queValue.push(!!word.sectionAffirm)
    })
    isQueDisabled = queValue.indexOf(true) !== -1
    return (
      <div id="main">
        <div>
          <Accordion multiple
            activeItem={[0]} boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                <Questions
                  title="Section 1" onSave={this.onSaveAndRecord}
                  bodyTitle="Disclosure is required if your response to any of the below questions is NO."
                  questions={negativeAnswers} category="negativeAnswers" onChangeItem={this.onChangeItem}
                />
                <Questions
                  title="Section 2" onSave={this.onSaveAndRecord}
                  bodyTitle="Disclosure is required if your response to any of the below questions is Yes."
                  questions={positiveAnswers} category="positiveAnswers" onChangeItem={this.onChangeItem}
                />
                <RatingSection onAccordion={() => onAccordion(0)} title="Affirmation and Submission Comments">
                  {activeAccordions.includes(0) &&
                  <AffirmationAndSubmission user={this.props.user} item={disclosureInfo} onChange={this.onChange} category="complete" disable={isQueDisabled} />
                  }
                  <div className="column is-full">
                    <div className="field is-grouped">
                      <div className="control">
                        <button className="button is-link is-small" onClick={this.onSave} disabled={!contDetails._id || isSaveDisabled || isQueDisabled}>Save</button>
                      </div>
                      <div className="control">
                        <button className="button is-link is-small" onClick={this.onSaveAndRecord} disabled={!contDetails._id || isSaveDisabled || !isQueDisabled}>Save & record disclosure</button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small" >Cancel</button>
                      </div>
                    </div>
                  </div>
                </RatingSection>
              </div>
            }
          />
        </div>
        <hr />
        <Disclaimer/>
      </div>
    )
  }
}

export default Complete
