import React, { Component } from "react"
import {withRouter} from "react-router-dom"
import { connect} from "react-redux"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import {fetchAllPoliticalDisclosure} from "../../../../../../StateManagement/actions/Supervisor"
import DisclodureFormModal from "../../../../../../GlobalComponents/DisclodureFormModal"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import DashboardTable from "../../GiftsAndGrautities/components/DashboardTable"
import Accordion from "../../../../../../GlobalComponents/Accordion"

const cols = [
  {name: "Name"},
  {name: "Type"},
  {name: "Quarter"},
  {name: "Year"},
  {name: "Link"},
  {name: "Status"}]

class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      search: {
        searchText: "",
        dateRange: "",
        groupBy: "",
      },
      disclosureInfo: {},
      path1: "", path2: "",
      dropDown: {
        dateRange: ["Last month", "Last 3 months", "Last 6 months", "Last 1 year", "All"],
        groupBy: ["Group by recipient", "Group by recipient's employer"],
      },
      disclosures: [],
      allDisclosures: [],
      loading: true,
      modalState: false
    }
  }

  async componentDidMount(){
    const {svControls} = this.props
    const res = await fetchAllPoliticalDisclosure(svControls.supervisor ? "?type=supervisor" : "")
    if(res){
      this.setState({
        disclosures: res || [],
        allDisclosures: res || [],
        loading:false,
      })
    }else {
      this.setState({
        loading:false,
      })
    }

  }

  onSearchChange = (e) => {
    const {allDisclosures, search} = this.state
    const { name, value } = e.target
    search[name] = value
    this.setState({
      search,
    },() => {
      if(allDisclosures.length){
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const {search, allDisclosures} = this.state
    if(search && search.searchText) {
      const disclosures = allDisclosures.filter(obj => ["userFirstName", "userLastName", "quarter", "year", "status"].some(key => {
        /* if(key === "date") {
          return moment(obj[key]).format("MM.DD.YYYY").toLowerCase().includes(search.searchText.toLowerCase())
        } */
        if(key === "quarter" || key === "year"){
          return obj.contributeId[key] && obj.contributeId[key].toString().toLowerCase().includes(search.searchText.toLowerCase())
        }
        return obj[key] && obj[key].toLowerCase().includes(search.searchText.toLowerCase())
      }))
      this.setState({
        disclosures
      })
    }else {
      this.setState({
        disclosures: allDisclosures
      })
    }
  }

  onAction = (disclosure) => {
    let queryStatus = ""
    if(disclosure.status === "Pre-approval") queryStatus = `&status=Pre-approval`
    if(disclosure.status === "Pending" && disclosure.cac && !disclosure.hasOwnProperty("noDisclosure")){
      this.setState({
        disclosureInfo: {
          year: disclosure.contributeId.year,
          quarter: disclosure.contributeId.quarter,
          disclosureFor: disclosure.userOrEntityId
        },
        path1: `/compliance/complete?disclosureFor=${disclosure.userOrEntityId}&year=${disclosure.contributeId.year}&quarter=${disclosure.contributeId.quarter}`,
        path2: `/compliance/cmp-sup-political/disclosure?disclosureFor=${disclosure.userOrEntityId}&year=${disclosure.contributeId.year}&quarter=${disclosure.contributeId.quarter}${queryStatus}`,
        modalState: true
      })
    }else if(disclosure.noDisclosure) {
      this.props.history.push(`/compliance/complete?disclosureFor=${disclosure.userOrEntityId}&year=${disclosure.contributeId.year}&quarter=${disclosure.contributeId.quarter}`)
    }else {
      this.props.history.push(`/compliance/cmp-sup-political/disclosure?disclosureFor=${disclosure.userOrEntityId}&year=${disclosure.contributeId.year}&quarter=${disclosure.contributeId.quarter}${queryStatus}`)
    }
  }

  toggleModal = () => {
    this.setState((prev) => {
      const newState = !prev.modalState
      return { modalState: newState, path1: "", path2: "" }
    })
  }

  render() {
    const { disclosures, modalState, search, loading, path1, path2, disclosureInfo} = this.state
    return (
      <div>
        <div className="columns">
          <div className="column is-half">
            <p className="control has-icons-left">
              <input className="input is-small is-link" type="text" name="searchText" placeholder="search recipient" value={search.searchText || ""} onChange={this.onSearchChange}/>
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <DisclodureFormModal toggleModal={this.toggleModal} modalState={modalState} path1={path1} path2={path2} info={disclosureInfo} type="political"/>
        </div>
        <div>
          <DashboardTable
            disclosures={disclosures}
            onAction={this.onAction}
            search={search.searchText || ""}
          />
        </div>
        {/* {
          loading ? <Loader/> :
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <TableHeader cols={cols}/>
              <tbody>
                {
                  disclosures.map((disclosure,i) => (
                    <tr key={i}>
                      <td className="multiExpTblVal" style={{maxWidth: 300}}>
                        <p>{`${disclosure.userFirstName} ${disclosure.userLastName}`}</p>
                      </td>
                      <td className="multiExpTblVal" style={{maxWidth: 300}}>
                        <p>G-37 Quartetly Obligation</p>
                      </td>
                      <td className="multiExpTblVal" style={{maxWidth: 300}}>
                        <p>{disclosure.contributeId && disclosure.contributeId.quarter || "---"}</p>
                      </td>
                      <td className="multiExpTblVal">
                        <p>{disclosure.contributeId && disclosure.contributeId.year || "---"}</p>
                      </td>
                      <td className="multiExpTblVal">
                        <a className="text-overflow" onClick={() => this.onAction(disclosure)} >Disclosure</a>
                      </td>
                      <td className="multiExpTblVal">
                        <p>{disclosure.status || "---"}</p>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
        } */}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Dashboard)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
