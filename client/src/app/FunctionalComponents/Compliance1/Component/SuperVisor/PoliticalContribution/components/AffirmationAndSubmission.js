import React from "react"
import {DropdownList} from "react-widgets"

const AffirmationAndSubmission = ({user, onChange, dropDown, item={}, disable, noDisclosure, svControls, currentYear, currentQuarter}) => (
  <div>
    <div className="columns">
      <div className="column">
        <p className="multiExpLbl ">Disclosure for</p>
        <div className="control">
          <div style={{width: "150px", fontSize: 12}}>
            {svControls.supervisor ?
              <DropdownList filter value={item.disclosureFor || []} data={dropDown.users || []} textField="name" valueField="id" key="name" onChange={(user) => onChange(user, "disclosureFor")}  disabled={svControls && !svControls.supervisor}/> :
              <p>{`${user.userFirstName} ${user.userLastName}`}</p> }
          </div>
        </div>
      </div>
      <div className="column">
        <p className="multiExpLbl">Report Quarter</p>
        <div className="select is-small is-link">
          <select name="quarter" value={item.quarter || ""} onChange={onChange}>
            <option value="">Pick</option>
            {dropDown.quarterOfDisclosureFirm.map((q) => <option key={q.value} value={q.value} disabled={q.value > currentQuarter}>{q.name}</option>)}
          </select>
        </div>
      </div>
      <div className="column">
        <p className="multiExpLbl">Report Year</p>
        <div className="select is-small is-link">
          <select name="year" value={item.year || ""} onChange={onChange}>
            <option value="">Pick</option>
            {dropDown.yearOfDisclosureFirm.map((value) => <option key={value} value={value} disabled={value > currentYear}>{value}</option>)}
          </select>
        </div>
      </div>
      {/* <SelectLabelInput label="Report Year" error= "" list={dropDown.yearOfDisclosureFirm} name="year" value={item.year || ""} onChange={onChange} /> */}
    </div>
    {
      noDisclosure ?
        <div>
          <div className="columns">
            <div className="column is-full">
              <div className="control">
                <textarea className="textarea" onChange={onChange} value={item.affirmationNotes} name="affirmationNotes" placeholder="" disabled={disable}/>
              </div>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <label className="checkbox">
                <p className="multiExpLbl">
                  <input type="checkbox" name="G37Obligation" checked={item.G37Obligation || false} onClick={onChange} onChange={onChange} disabled={disable}/>I confirm that I have no disclosures to be made. I
                  confirm completion of my G-37 obligation for the period.
                </p>
              </label>
            </div>
          </div>
        </div>
        : <div className="title innerPgTitle"><strong> Disclosure already exists for this quarter  </strong> </div>
    }

  </div>
)
export default AffirmationAndSubmission
