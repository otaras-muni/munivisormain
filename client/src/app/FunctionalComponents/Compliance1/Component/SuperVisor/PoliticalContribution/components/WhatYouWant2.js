import React from "react"
import {Link} from "react-router-dom"
import { SelectLabelInput } from "../../../../../../GlobalComponents/TextViewBox"

const WhatYouWant2 = ({dropDown, item={}, onChange, isSaveDisabled}) => (
  <div>
    <div className="columns">
      <SelectLabelInput label="Disclosure for" error= "" list={dropDown.polContribOfDisclosureFirm} name="disclosureFor" value={item.disclosureFor || ""} onChange={onChange} />
      <div className="column">
        <p className="multiExpLbl">Report Quarter</p>
        <div className="select is-small is-link">
          <select name="quarter" value={item.quarter || ""} onChange={onChange}>
            <option value="">Pick</option>
            {
              dropDown.polContribQuarterOfDisclosureFirm.map((q) => (
                <option key={q.value} value={q.value}>{q.name}</option>
              ))
            }
          </select>
        </div>
      </div>
      <SelectLabelInput label="Report Year" error= "" list={dropDown.polContribYearOfDisclosureFirm} name="year" value={item.year || ""} onChange={onChange} />
    </div>
    <div className="columns">
      <div className="column">
        <p className="title innerPgTitle">
          <input type="checkbox" name="G37Obligation" checked={item.G37Obligation || false} onClick={onChange} disabled={!item.quarter || !item.year || !item.disclosureFor}/>
          <strong>that you have not made any political contributions last quarter and complete your G-37 obligation.</strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <p className="title innerPgTitle">
          <Link to={`/compliance/complete?disclosureFor=${item.disclosureFor}&year=${item.year}&quarter=${item.quarter}`}>
            <button className="button is-warning is-small" value="Confirm" disabled={isSaveDisabled || !item.disclosureFor || !item.year || !item.quarter || !item.G37Obligation || false}>Complete</button>
          </Link> <strong>the survey and follow instructions if you made political contribution(s) last quarter.</strong>
        </p>
      </div>
    </div>
  </div>

)
export default WhatYouWant2
