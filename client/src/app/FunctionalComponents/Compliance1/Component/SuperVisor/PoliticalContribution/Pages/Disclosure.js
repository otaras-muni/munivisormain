import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import { DropdownList } from "react-widgets"
import moment from "moment"
import * as qs from 'query-string'
import { getPicklistByPicklistName, updateCACPoliticalActionStatus, getToken } from "GlobalUtils/helpers"
import EditableTable from "../../../../../../GlobalComponents/EditableTable"
import DocumentPage from "../../../../../../GlobalComponents/DocumentPage"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Audit from "../../../../../../GlobalComponents/Audit"
import { DropDownInput, SelectLabelInput } from "../../../../../../GlobalComponents/TextViewBox"
import Loader from "../../../../../../GlobalComponents/Loader"
import CONST, { ContextType } from "../../../../../../../globalutilities/consts"
import { fetchAssigned, fetchIssuers } from "../../../../../../StateManagement/actions/CreateTransaction/index"
import {
  putPolContribMuniEntity, getPoliContributionsDetails, putContributeDetailsAuditLogs, pullDisclosure,
  putContributeDetailsDocuments, postPoliContributions, pullsvCmplDocuments, pullPqDocuments
} from "../../../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import {
  EntityByStatePolContribution, PaymentPolPartiesValidate, BallotContribValidate,
  ReimbursementsValidate, BallotApprovedContribValidate
} from "../../Validation/PolContributionValidate"
import TableHeader from "../../../../../../GlobalComponents/TableHeader"


const cols = [
  { name: "User" },
  { name: "Notes/Instruction" },
  { name: "Time" }]

class Disclosure extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tables: [
        {
          name: "Contributions made to officials of a municipal entity (list by state)",
          key: "contribMuniEntityByState",
          row: cloneDeep(CONST.SuperVisor.contribMuniEntityByState) || {},
          header: [
            /* { name: "Username" }, */
            { name: "State<span class='has-text-danger'>*</span>" },
            { name: "Official's name, title & address<span class='has-text-danger'>*</span>" },
            { name: "Contributor category<span class='has-text-danger'>*</span>" },
            { name: "Amount<span class='has-text-danger'>*</span>" },
            { name: "Date<span class='has-text-danger'>*</span>" },
            { name: "Rule G-37(j) Exemption (if any)" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            /* { name: "userName", type: "text" }, */
            { name: "state", placeholder: "State", type: "select" },
            { name: "userAddressConsolidated", placeholder: "Details", type: "text" },
            { name: "contributorCategory", placeholder: "Category", type: "select" },
            { name: "contributionAmount", placeholder: "$", type: "number", prefix: "$" },
            { name: "contributionDate", placeholder: "Date", type: "date" },
            { name: "exemptionG37RuleExemption", placeholder: "Exemption", type: "text" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => EntityByStatePolContribution(payload, minDate)
        },
        {
          name: "Payments made to political parties of states or political subdivisions (list by state)",
          key: "paymentsToPartiesByState",
          row: cloneDeep(CONST.SuperVisor.paymentsToPartiesByState) || {},
          header: [
            { name: "State<span class='has-text-danger'>*</span>" },
            { name: "Political party name<span class='has-text-danger'>*</span>" },
            { name: "Political party address<span class='has-text-danger'>*</span>" },
            { name: "Contributor category<span class='has-text-danger'>*</span>" },
            { name: "Amount<span class='has-text-danger'>*</span>" },
            { name: "Date<span class='has-text-danger'>*</span>" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            { name: "state", placeholder: "State", type: "select" },
            { name: "partyName", placeholder: "Political Party Name", type: "text", parentEvent: true, keyValue: "politicalPartyDetails", remainVal: ["partyAddress"] },
            { name: "partyAddress", placeholder: "Political Party Address", type: "text", parentEvent: true, keyValue: "politicalPartyDetails", remainVal: ["partyName"] },
            { name: "contributorCategory", placeholder: "Category", type: "select" },
            { name: "paymentAmount", placeholder: "$", type: "number", prefix: "$" },
            { name: "paymentDate", placeholder: "Payment Date", type: "date" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => PaymentPolPartiesValidate(payload, minDate)
        },
        {
          name: "Contributions made to bond ballot campaigns (list by state)",
          key: "ballotContribByState",
          row: cloneDeep(CONST.SuperVisor.ballotContribByState) || {},
          header: [
            { name: "State<span class='has-text-danger'>*</span>" },
            { name: "Bond ballot campaign details<span class='has-text-danger'>*</span>" },
            { name: "Contributor category<span class='has-text-danger'>*</span>" },
            { name: "Amount<span class='has-text-danger'>*</span>" },
            { name: "Date<span class='has-text-danger'>*</span>" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            { name: "state", placeholder: "State", type: "select" },
            { name: "ballotDetail", placeholder: "Political Party", type: "text" },
            { name: "contributorCategory", placeholder: "Category", type: "select" },
            { name: "contributionAmount", placeholder: "$", type: "number", prefix: "$" },
            { name: "contributionDate", placeholder: "Contribute Date", type: "date" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => BallotContribValidate(payload, minDate)
        },
        {
          name: "Reimbursement for Contributions (incl. third party making such payments or reimbursements)",
          key: "reimbursements",
          row: cloneDeep(CONST.SuperVisor.reimbursements) || {},
          header: [
            /* { name: "Person" }, */
            { name: "State" },
            { name: "Bond ballot campaign details" },
            { name: "3rd party name and address" },
            { name: "3rd party category" },
            { name: "Amount" },
            { name: "Date" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            /* { name: "thirdPartyDetails", type: "dropdown", labelName1: "userFirstName", labelName2: "userLastName", dropdownKey: "users" }, */
            { name: "state", placeholder: "State", type: "select" },
            { name: "ballotDetail", placeholder: "Ballot Details", type: "text" },
            { name: "userAddressConsolidated", placeholder: "Details", type: "text"},
            { name: "thirdPartyCategory", placeholder: "Category", type: "select" },
            { name: "reimbursementAmt", placeholder: "$", type: "number", prefix: "$" },
            { name: "reimbursementDate", placeholder: "Contribute Date", type: "date" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => ReimbursementsValidate(payload, minDate)
        },
        {
          name: "Ballot-Approved Offerings",
          key: "ballotApprovedOfferings",
          row: cloneDeep(CONST.SuperVisor.ballotApprovedOfferings) || {},
          header: [
            { name: "Full Name of Municipal Entity<span class='has-text-danger'>*</span>" },
            { name: "Full Issue Description<span class='has-text-danger'>*</span>" },
            { name: "Reportable Date of Selection<span class='has-text-danger'>*</span>" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            { name: "municipalEntityName", type: "dropdown", labelName: "municipalEntityName", dropdownKey: "entityList" },
            { name: "processDescription", placeholder: "Ballot Details", type: "text" },
            { name: "reportablesaleDate", placeholder: "Details", type: "date" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => BallotApprovedContribValidate(payload, minDate)
        },
      ],
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      preApprovalQuarter: "",
      preApprovalYear: "",
      dropDown: {
        polContribQuarterOfDisclosureFirm: [{ name: "First", value: 1 }, { name: "Second", value: 2 }, { name: "Third", value: 3 }, { name: "Fourth", value: 4 }],
        polContribYearOfDisclosureFirm: [],
        polContribOfDisclosureFirm: ["Self", "Someone else", "Firm"],
        supervisorAction: ["Approved", "Rejected", "Canceled"],
        usersList: [],
        entityList: [],
        contributorCategory: [],
        allSuggestions: []
      },
      isSaveDisabled: {},
      disclosureInfo: CONST.SuperVisor.disclosureInfo || {},
      contributor: {
        complainantDetails: {
          ...cloneDeep(CONST.complaint.complainantDetails),
          entityId: (this.props.user && this.props.user.entityId) || "",
          entityName: (this.props.user && this.props.user.firmName) || "",
        }
      },
      documentsList: [],
      disclosureDetails: {},
      loading: true,
      supervisorNotesShow: false,
      status: "",
      supervisorNote: ""
    }
  }

  async componentDidMount() {
    const queryString = qs.parse(location.search)
    const picResult = await getPicklistByPicklistName(["LKUPCONTRIBUTORCAT", "LKUPSTATE" /* "LKUPCOUNTRY"*/])
    const result = (picResult.length && picResult[1]) || {}
    // const stateCountry =  (picResult[3] && picResult[3].LKUPCOUNTRY) || {}

    let preApprovalQuarter = queryString.quarter || ""
    let preApprovalYear = queryString.year || ""
    const polContribYearOfDisclosureFirm = []
    let min = 2015, max = min + 10
    for (let i = min; i <= max; i++) { polContribYearOfDisclosureFirm.push(i) }
    const { disclosureInfo, dropDown } = this.state
    let { currentYear, currentQuarter } = this.state
    const statusFlag = (queryString.year > currentYear) || ((queryString.quarter > currentQuarter) && (queryString.year === currentYear)) || queryString.status === "Pre-approval"

    if (statusFlag && !queryString.year && !queryString.quarter) {
      if (currentQuarter === 4) {
        preApprovalQuarter = 1
        preApprovalYear = currentYear + 1
      } else {
        preApprovalQuarter = currentQuarter + 1
        preApprovalYear = currentYear
      }
      // dropDown.supervisorAction = ["Approved", "Rejected", "Canceled"]
    }

    this.setState(prevState => ({
      dropDown: {
        ...this.state.dropDown,
        contributorCategory: result.LKUPCONTRIBUTORCAT || [],
        thirdPartyCategory: result.LKUPCONTRIBUTORCAT || [],
        state: result.LKUPSTATE || [],
        polContribYearOfDisclosureFirm,
        // supervisorAction: ["Approved", "Rejected", "Canceled"],
        // stateCountry,
        // organizations
      },
      disclosureInfo: {
        ...disclosureInfo,
        year: currentQuarter === 4 && statusFlag ? preApprovalYear : queryString.year || currentYear,
        quarter: statusFlag ? preApprovalQuarter : queryString.quarter || currentQuarter,
        disclosureFor: queryString.disclosureFor || this.props.user.userId || "",
        status: statusFlag ? "Pre-approval" : ""
      },
      preApprovalQuarter,
      preApprovalYear,
      disclosureDetails: {},
      tempTable: cloneDeep(prevState.tables) || [],
      status: statusFlag ? "Pre-approval" : "",
    }), () => {
      this.getEntitiesAndUsers()
    })
  }

  getEntitiesAndUsers = () => {
    fetchIssuers(this.props.user.entityId, (res) => {
      fetchAssigned(this.props.user.entityId, (users) => {
        this.setState({
          dropDown: {
            ...this.state.dropDown,
            usersList: users.usersList || [],
            entityList: res.issuerList || [],
          },
          loading: false,
        }, () => {
          this.onSearch()
        })
      })
    })
  }

  onRemove = (type, removeId, callback) => {
    const { tables, disclosureDetails } = this.state
    const contributeId = disclosureDetails._id
    pullDisclosure(type, contributeId, removeId, (res) => {
      if (res && res.status === 200) {
        toast("Removed Political Contribution Disclosure successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        tables.forEach(tbl => {
          if (tbl.key === type) {
            tbl.list = res.data[type] || []
          }
        })
        this.setState({
          tables
        }, () => {
          callback({
            status: true,
            list: res.data[type],
          })
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onChange = (e, inputName) => {
    if (inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
        }
      }, () => this.onSearch())
    } else {
      const { name, value, title} = e.target
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [name]: e.target.type === "checkbox" ? e.target.checked : value
        }
      }, () => {
        if (name === "disclosureFor" || name === "quarter" || name === "year") {
          this.onSearch()
        } else {
          if (name === "status") {
            this.props.addAuditLog({userName: this.props.user.userFirstName, log: `change ${title} ${value}`, date: new Date(), key: "details" })
            this.onDisclosureSave(true)
          }
        }
      })
    }
  }

  onChangeItem = (value, name) => {
    this.setState({
      [name]: value,
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    let userAdd = ""
    if (user && Array.isArray(user.userAddresses) && user.userAddresses.length) {
      userAdd = user.userAddresses.find(e => e.primaryAddress)
      userAdd = userAdd ? userAdd.userAddresses.addressLine1 : user.userAddresses[0].addressLine1
    }
    /* if (inputName === "contributionReceipientDetails") {
      this.props.addAuditLog({ userName: this.props.user.userFirstName, log: `In Disclosure ${user.name}`, date: new Date(), key })
      callback({
        status: true,
        object: {
          userId: user.id,
          contributionReceipientDetails: {
            userId: user.id,
            userFirstName: user.userFirstName,
            userLastName: user.userLastName,
            userAddressConsolidated: userAdd
          },
        },
      })
    } */
    if (inputName === "thirdPartyDetails") {
      callback({
        status: true,
        object: {
          userId: user.id,
          thirdPartyDetails: {
            userId: user.id,
            userFirstName: user.userFirstName,
            userLastName: user.userLastName,
            userAddressConsolidated: userAdd
          },
        },
      })
    }
    if (inputName === "municipalEntityName") {
      callback({
        status: true,
        userId: user.id,
        object: {
          entityId: user.id,
          municipalEntityName: user.name,
        },
      })
    }
    if (inputName === "partyName" || inputName === "partyAddress") {
      callback({
        status: true,
        childKey: "politicalPartyDetails",
        childObject: {
          [inputName]: select.target.value
        },
      })
    }
  }

  onSearch = () => {
    const { disclosureInfo, tables, status, currentYear, currentQuarter } = this.state
    if (disclosureInfo && disclosureInfo.disclosureFor && disclosureInfo.year && disclosureInfo.quarter) {
      const query1 = status === "Pre-approval" ? `&status=Pre-approval` : ""
      const query = `${disclosureInfo.disclosureFor}?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}${query1}`

      this.setState({
        isSearchDisabled: true
      }, () => {
        getPoliContributionsDetails(query, (res) => {
          if (res) {
            if (!res.message || res.message !== "not found") {
              tables.forEach(tbl => {
                tbl.list = (res && res[tbl.key]) || []
                /* if (tbl.key === "contribMuniEntityByState") {
                  tbl.list = res[tbl.key] ? res[tbl.key].map(data => ({
                    ...data,
                    userId: (data.contributionReceipientDetails && data.contributionReceipientDetails.userId) || ""
                  })) : []
                } else if (tbl.key === "reimbursements") {
                  tbl.list = res[tbl.key] ? res[tbl.key].map(data => ({
                    ...data,
                    userId: (data.thirdPartyDetails && data.thirdPartyDetails.userId) || ""
                  })) : []
                } else {
                  tbl.list = (res && res[tbl.key]) || []
                } */
              })
            }

            this.setState(prevState => ({
              disclosureDetails: res || {},
              disclosureInfo: {
                ...prevState.disclosureInfo,
                status: typeof res.status === "boolean" ? "" : res.status || prevState.status,
              },
              contributor: {
                ...prevState.contributor
              },
              contributeId: res.contributeId || "",
              documentsList: res.contributionsRelatedDocuments || [],
              tables: (!res.message || res.message !== "not found") ? tables : cloneDeep(prevState.tempTable) || [],
              isSearchDisabled: false,
              auditLogs: (res && res.auditLogs) || [],
            }))
          } else {
            toast("something went wrong", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            this.setState({
              isSearchDisabled: false
            })
          }
        })
      })
    }
  }

  onSave = (type, item, callback) => {
    const { disclosureInfo, disclosureDetails, tables } = this.state
    // const contributeId = disclosureDetails.contributeId._id // eslint-disable-line
    // const queryString = `${contributeId}` ? `${contributeId}?userOrEntityId=${disclosureDetails.userOrEntityId}&type=${type}` : ""
    if (disclosureInfo && disclosureInfo.disclosureFor && disclosureInfo.year && disclosureInfo.quarter && type) {
      const queryString = `${disclosureInfo.year}?quarter=${disclosureInfo.quarter}&userOrEntityId=${disclosureInfo.disclosureFor}&type=${type}`
      putPolContribMuniEntity(queryString, item, (res) => {
        if (res && res.status === 200) {
          toast("Political contribution disclosure updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          res = (res && res.data) || {}
          const list = (res && res[type]) || []
          tables.forEach(tbl => {
            if (tbl.key === type) {
              tbl.list = list || []
            }
          })
          this.setState({
            tables,
            contributeId: (res.contributeId) || "",
            disclosureInfo: {
              ...disclosureInfo,
              status: res.status || ""
            },
            disclosureDetails: {
              ...this.state.disclosureDetails,
              ...res,
            },
          }, () => {
            this.onAuditSave(type)
            callback({
              status: true,
              list,
            })
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    }

  }

  onAuditSave = (key) => {
    const { user } = this.props
    const { disclosureDetails } = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)

    if (disclosureDetails && disclosureDetails._id) {
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Political Contributions and Prohibitions"
          log.superVisorSubSection = "Disclosure"
        })
        putContributeDetailsAuditLogs(disclosureDetails._id, auditLogs, (res) => {
          if (res && res.status === 200) {
            this.setState({
              auditLogs: (res.data && res.data.auditLogs) || []
            })
          }
        })
        const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
        this.props.updateAuditLog(remainLogs)
      }
    }
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    const { disclosureDetails } = this.state
    if (disclosureDetails && disclosureDetails._id) {
      putContributeDetailsDocuments("", disclosureDetails._id, docs, (res) => {
        if (res && res.status === 200) {
          toast("Disclosure Related Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("documents")
          callback({
            status: true,
            documentsList: (res.data && res.data.contributionsRelatedDocuments) || [],
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    }
  }

  onStatusChange = async (e, doc, callback) => {
    const { disclosureDetails } = this.state
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc
      }
      type = "updateMeta"
    }
    putContributeDetailsDocuments(`?details=${type}`, disclosureDetails._id, document, (res) => {
      if (res && res.status === 200) {
        toast("Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if(name){
          callback({
            status: true,
          })
        }else {
          callback({
            status: true,
            documentsList: (res.data && res.data.contributionsRelatedDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const {disclosureDetails} = this.state
    pullDisclosure("contributionsRelatedDocuments", disclosureDetails._id, documentId, (res) => {
      if (res && res.status === 200) {
        toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        callback({
          status: true,
          documentsList: (res.data && res.data.contributionsRelatedDocuments) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDisclosureSave = (flag) => {
    const { disclosureInfo, supervisorNote } = this.state
    const { svControls, user } = this.props
    if (supervisorNote) {
      if (!supervisorNote) return
      disclosureInfo.notes = {
        userName: `${user.userFirstName} ${user.userLastName}`,
        note: supervisorNote,
        supervisor: svControls.supervisor,
      }
      this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `add new notes ${supervisorNote} by ${user.userFirstName} ${user.userLastName}`, date: new Date(), key: "notes" })
      delete disclosureInfo.status
    }
    this.setState(prevState => ({
      isSaveDisabled: {
        ...prevState.isSaveDisabled,
        details: true
      }
    }), () => {
      delete disclosureInfo.G37Obligation
      delete disclosureInfo.affirmationNotes
      postPoliContributions(!flag ? { ...disclosureInfo, type: "notes" } : disclosureInfo, (res) => {
        if (res && res.status === 200) {
          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState(prevState => ({
            disclosureDetails: res.data || {},
            isSaveDisabled: {
              ...prevState.isSaveDisabled,
              details: false
            },
            disclosureInfo: {
              ...disclosureInfo,
              status: (res.data && res.data.status) || ""
            },
            contributeId: (res.data && res.data.contributeId) || "",
            supervisorNote: ""
          }),() => {
            this.onAuditSave(flag ? "details" : "notes")
          })
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onMakeDisclosureSave = (status) => {
    const { disclosureInfo } = this.state
    const {user} = this.props
    delete disclosureInfo.G37Obligation
    delete disclosureInfo.affirmationNotes
    disclosureInfo.status = status
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${status === "Complete" ? "Make Disclosure" : "Send for Pre-approval"}`, date: new Date(), key: "MakeDisclosure" })
    this.setState(prevState => ({
      isSaveDisabled: {
        ...prevState.isSaveDisabled,
        details: true
      }
    }), () => {
      postPoliContributions(disclosureInfo, async (res) => {
        if (res && res.status === 200) {
          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("MakeDisclosure")
          try{
            await updateCACPoliticalActionStatus(getToken(), {...disclosureInfo, type: "political"})
          } catch (err) {
            console.log(err)
          }
          this.props.history.push("/compliance/cmp-sup-political/dashboard")
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onUsersListRefresh = () => {
    fetchAssigned(this.props.user.entityId, (users) => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        }
      })
    })
  }

  getQuarterDate = (quarter, year) => {
    return {
      start: moment(moment().month(parseInt(quarter, 10)*3 - 3).year(parseInt(year, 10)).startOf('month')).format("YYYY-MM-DD"),
      end: moment(moment().month(parseInt(quarter, 10)*3 - 1).year(parseInt(year, 10)).endOf('month')).format("YYYY-MM-DD")
    }
  }
  render() {
    const { loading, contributeId, supervisorNotesShow, dropDown, disclosureDetails, currentQuarter, currentYear, tables, auditLogs, documentsList, status, supervisorNote, disclosureInfo, isSearchDisabled, isSaveDisabled } = this.state
    const { svControls, user } = this.props
    let isMakeDisclose = 0
    let quarterDate = ""
    if(disclosureInfo && disclosureInfo.quarter && disclosureInfo.year){
      quarterDate = this.getQuarterDate(disclosureInfo.quarter, disclosureInfo.year)
    }
    tables.forEach(tbl => { isMakeDisclose = isMakeDisclose + tbl.list.length })
    // let canEdit = status !== "Pre-approval" ? (parseInt(disclosureInfo.quarter, 10) < currentQuarter) ? !(parseInt(disclosureInfo.quarter, 10) < currentQuarter) : !(parseInt(disclosureInfo.year) < currentYear) : true
    const flag = (parseInt(disclosureInfo.year,10) > currentYear) || ((parseInt(disclosureInfo.quarter,10) > currentQuarter) && (parseInt(disclosureInfo.year,10) === currentYear))
    let canEdit = disclosureDetails && disclosureDetails.userOrEntityId  && user ?  disclosureDetails.userOrEntityId === user.userId && disclosureDetails.status === "Pending" : true
    const isDisclose = disclosureDetails && disclosureDetails.userOrEntityId  && user && disclosureDetails.userOrEntityId === user.userId && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved")
    /* if (disclosureDetails && user && disclosureDetails.userOrEntityId === user.userId && disclosureInfo.status !== "Pending") {
      canEdit = false
    } */
    canEdit = svControls.supervisor && disclosureInfo.disclosureFor !== user.userId ? false : canEdit
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Political Contributions Prohibitions",
    }
    const value = (val) => {
      const quarter = [{ name: "First", value: 1 }, { name: "Second", value: 2 }, { name: "Third", value: 3 }, { name: "Fourth", value: 4 }]
      const qt = quarter.find(q => q.value === parseInt(val, 10))
      return qt.name
    }
    if (loading) {
      return <Loader />
    }
    return (
      <div>
        <div>
          <Accordion multiple activeItem={[0]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              <div className="columns">
                <div className="column">
                  <p>
                    <small>Rule G-37 Snapshot by NAMA</small>
                    <a href="https://s3.amazonaws.com/munidocs/5ba7c1c030580f39ec3b9b1a/ENTITYDOCS/ENTITYDOCS_5ba7c1d030580f39ec3b9daa/aaa_1537849129037.json" download><i className="fas fa-info-circle" /></a>
                  </p>
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl ">Disclosure for</p>
                  <div className="control">
                    <div className="select is-link is-small is-fullwidth select-arrow-hack">
                      {/* <DropDownInput label="Disclosure for" filter data={dropDown.usersList || []} value={disclosureInfo.disclosureFor || []} onChange={(user) => this.onChange(user, "disclosureFor")} disabled={!svControls.supervisor}/> */}
                      {svControls.supervisor ?
                        <DropdownList filter value={disclosureInfo.disclosureFor || []} data={dropDown.usersList || []} textField="name" valueField="id" key="name" onChange={(user) => this.onChange(user, "disclosureFor")} disabled={!svControls.supervisor} className="is-link" /> :
                        <p>{`${user.userFirstName} ${user.userLastName}`}</p> }
                      {svControls.supervisor &&
                        <div>
                          <a className="has-text-link" style={{ fontSize: 12, marginTop: -10 }} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                          <i className="fa fa-refresh" style={{ fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px" }} onClick={this.onUsersListRefresh} />
                        </div>}
                    </div>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Report Quarter</p>
                  <div className="select is-small is-fullwidth is-link">
                    <select name="quarter" value={disclosureInfo.quarter || ""} onChange={this.onChange}>
                      <option value="">Pick</option>
                      {dropDown.polContribQuarterOfDisclosureFirm.map((q) => <option key={q.value} value={q.value} disabled={status === "Pre-approval" ?
                        !(currentYear < disclosureInfo.year) ? q.value <= currentQuarter : false
                        : q.value > currentQuarter}>{q.name}</option>)}
                    </select>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Report Year</p>
                  <div className="select is-small is-fullwidth is-link">
                    <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange} className="is-link">
                      <option value="">Pick</option>
                      {dropDown.polContribYearOfDisclosureFirm.map((value) => <option key={value} value={value} disabled={status === "Pre-approval" ? currentQuarter === 4 ? value <= currentYear : value < currentYear : value > currentYear}>{value}</option>)}
                    </select>
                  </div>
                </div>
                {
                  contributeId && svControls.supervisor && disclosureDetails.status === "Pre-approval" &&
                  <SelectLabelInput title="Disclosure status" label="Supervisor's Action" error="" list={dropDown.supervisorAction} name="status" value={disclosureInfo.status} onChange={this.onChange} disabled={disclosureInfo.status === "Complete"} />
                }
              </div>
              <br />
              {
                isSearchDisabled ? <Loader /> :
                  <div>
                    {
                      disclosureDetails.noDisclosure ?
                        <div className="title innerPgTitle"><strong> No disclosure for this quarter  </strong></div> :
                        <div>
                          <EditableTable {...this.props} tables={tables} canSupervisorEdit={canEdit} contextType="SVOBLIGATIONS" dropDown={dropDown}
                            onSave={this.onSave} onRemove={this.onRemove} onParentChange={this.onDropDownChange} minDate={(quarterDate && quarterDate.start) || ""} maxDate={(quarterDate && quarterDate.end) || ""}/>
                          {contributeId &&
                          <DocumentPage {...this.props} isNotTransaction onSave={this.onDocSave} tranId={this.props.nav2} title="Related Documents" pickCategory="LKUPDOCCATS"
                            pickSubCategory="LKUPCORRESPONDENCEDOCS" onStatusChange={this.onStatusChange} pickAction="LKUPDOCACTION" pickType="LKUPPOLCONTDOCTYPE"
                            staticField={staticField} category="contributionsRelatedDocuments" documents={documentsList} onDeleteDoc={this.onDeleteDoc}
                            contextType={ContextType.supervisor.poliContributeRelated} tableStyle={{ fontSize: "smaller" }} isDisabled={!canEdit} />}
                          <br />
                          <a className="button is-link is-small" onClick={() => this.setState({ supervisorNotesShow: !supervisorNotesShow })} >{`${supervisorNotesShow ? "Hide" : "Show"}`} All Notes</a>

                          {supervisorNotesShow && Array.isArray(disclosureDetails.notes) &&
                            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                              <TableHeader cols={cols} />
                              <tbody>
                                {
                                  disclosureDetails.notes.length ? disclosureDetails.notes.map((note, i) => (
                                    <tr key={i}>
                                      <td className="emmaTablesTd">
                                        <small>{`${note && note.userName || ""} ${note && note.supervisor ? "(Supervisor)" : ""}`}</small>
                                      </td>
                                      <td className="emmaTablesTd" style={{ width: "70%" }}>
                                        <small>{note && note.note}</small>
                                      </td>
                                      <td className="emmaTablesTd">
                                        <small>{note && note.createdDate ? moment(note.createdDate).format("MM-DD-YYYY h:mm A") : ""}</small>
                                      </td>
                                    </tr>
                                  )) : null
                                }
                              </tbody>
                            </table>
                          }

                          {
                            canEdit || (svControls.supervisor && (disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) ?
                              <div className="columns">
                                <div className="column is-full">
                                  <p className="multiExpLbl">Supervisor Notes/Instructions</p>
                                  <div className="control">
                                    <textarea className="textarea" value={supervisorNote || ""} name="supervisorNote"  onChange={(e) => this.onChangeItem(e.target.value, e.target.name)} />
                                  </div>
                                </div>
                              </div> : null
                          }


                          <div className="column is-full">
                            <div className="field is-grouped">
                              <div className="control">
                                {
                                  canEdit || (svControls.supervisor && (disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) ?
                                    <button className="button is-link is-small" onClick={() => this.onDisclosureSave(false)} disabled={isSaveDisabled.details}>Save</button>
                                    : null
                                }
                              </div>
                              {
                                isDisclose ?
                                  <div className="control">
                                    {status === "Pre-approval" && flag && disclosureDetails.status === "Pending" ?
                                      <button className="button is-link is-small" disabled={isSaveDisabled.details || !isMakeDisclose || false}
                                        onClick={() => this.onMakeDisclosureSave("Pre-approval")}>Send for Pre-approval</button>
                                      : !flag && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved") && <button className="button is-link" disabled={isSaveDisabled.details || !isMakeDisclose || false}
                                        onClick={() => this.onMakeDisclosureSave("Complete")} >Make disclosure</button> }
                                  </div> : null
                              }
                            </div>
                          </div>

                          <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{ overflowY: "scroll" }}>
                            {activeAccordions.includes(2) &&
                            <Audit auditLogs={auditLogs || []} />
                            }
                          </RatingSection>

                        </div>
                    }
                  </div>
              }
            </div>
          }
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Disclosure)
export default connect(mapStateToProps, null)(WrappedComponent)
