import React, { Component } from "react"
import {toast} from "react-toastify"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import WhatYouWant from "../components/WhatYouWant"
import CONST from "../../../../../../../globalutilities/consts"
import { postPoliContributions } from "../../../../../../StateManagement/actions/Supervisor"

class Information extends Component {
  constructor(props) {
    super(props)
    this.state = {
      disclosureInfo : CONST.SuperVisor.disclosureInfo || {},
      isSaveDisabled: true
    }
  }

  onChange = (e) => {
    const name = e.target.name
    this.setState({
      disclosureInfo: {
        ...this.state.disclosureInfo,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }, () => {
      if(name === "G37Obligation"){
        this.postDisclosureObligation()
      }
    })
  }

  postDisclosureObligation = () => {
    const {disclosureInfo} = this.state
    delete disclosureInfo.affirmationNotes
    postPoliContributions(disclosureInfo, (res) => {
      if(res && res.status === 200) {
        this.setState({
          isSaveDisabled: false
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      }
    })
  }

  render() {
    const { disclosureInfo, isSaveDisabled } = this.state
    return (
      <div>
        <div>
          <Accordion multiple
            activeItem={[0]} boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                <RatingSection onAccordion={() => onAccordion(0)} title="What do you want to do?" category="whatyouwant">
                  {activeAccordions.includes(0) &&
                    <WhatYouWant item={disclosureInfo} isSaveDisabled={isSaveDisabled} category="polConInfo" onChange={this.onChange} />
                  }
                </RatingSection>
              </div>
            }
          />
        </div>
      </div>
    )
  }
}

export default Information
