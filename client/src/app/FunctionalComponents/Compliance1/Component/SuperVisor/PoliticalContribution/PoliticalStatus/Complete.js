import React, { Component } from "react"
import moment from "moment"
import {toast} from "react-toastify"
import {withRouter} from "react-router-dom"
import { connect } from "react-redux"
import * as qs from "query-string"
import Loader from "../../../../../../GlobalComponents/Loader"
import { updateCACPoliticalActionStatus, getToken } from "GlobalUtils/helpers"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import AffirmationAndSubmission from "../components/AffirmationAndSubmission"
import CONST from "../../../../../../../globalutilities/consts"
import {getPoliContributionsDetails, postPoliContributions} from "../../../../../../StateManagement/actions/Supervisor"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import {fetchAssigned} from "../../../../../../StateManagement/actions/CreateTransaction"

class Complete extends Component {
  constructor(props) {
    super(props)
    const yearOfDisclosureFirm = []
    const min = 2015
    const max = min + 10
    for (let i = min; i<=max; i++) { yearOfDisclosureFirm.push(i) }
    this.state = {
      disclosureInfo : {
        ...CONST.SuperVisor.disclosureInfo,
      } || {},
      loading: true,
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      dropDown: {
        quarterOfDisclosureFirm: [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        yearOfDisclosureFirm,
        users: [],
      },
      contDetails: {},
      isSaveDisabled: false,
    }
  }

  componentDidMount() {
    const queryString = qs.parse(location.search)
    const {currentYear, currentQuarter} = this.state
    const {user} = this.props
    const flag = (parseInt(queryString.year,10) > currentYear) || ((parseInt(queryString.quarter,10) > currentQuarter) && (parseInt(queryString.year,10) === currentYear))
    const year = queryString.year || currentYear
    const quarter = queryString.quarter || currentQuarter
    const disclosureFor = queryString.disclosureFor || user.userId || ""
    if(!flag){
      fetchAssigned(user.entityId, (users)=> {
        this.setState(prevState => ({
          disclosureInfo: {
            ...prevState.disclosureInfo,
            year,quarter, disclosureFor,
          },
          dropDown: {
            ...prevState.dropDown,
            users: users.usersList || [],
          }
        }), () => {
          this.getDisclosureInfo()
        })
      })
    }else {
      this.props.history.push("/compliance/cmp-sup-political/dashboard")
    }
  }

  onChange = (e, inputName) => {
    if(inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
          // disclosureName: `${e.userFirstName} ${e.userLastName}`
        }
      }, () => this.getDisclosureInfo())
    }else {
      const {name} = e.target
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
        }
      }, () => {
        if(name === "disclosureFor" || name === "quarter" || name === "year") this.getDisclosureInfo()
      })
    }
  }

  onCancel = () => {
    this.setState(prevState => ({
      disclosureInfo:{
        ...prevState.disclosureInfo,
        affirmationNotes: "",
        G37Obligation: ""
      }
    }))
  }

  onSave = () => {
    const {disclosureInfo} = this.state
    // if(!disclosureInfo.affirmationNotes) return
    disclosureInfo.status = "Complete"
    disclosureInfo.noDisclosure = true
    this.setState({
      isSaveDisabled: true
    }, () => {
      postPoliContributions(disclosureInfo, async (res) => {
        if(res && res.status === 200) {
          toast("Affirmation and Submission Comments updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          try{
            await updateCACPoliticalActionStatus(getToken(), {...disclosureInfo, type: "political"})
          } catch (err) {
            console.log(err)
          }
          this.setState({
            isSaveDisabled: false,
          },() => {
            this.props.history.push("/compliance/cmp-sup-political/dashboard")
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  getDisclosureInfo = () => {
    const {disclosureInfo} = this.state
    if(disclosureInfo && disclosureInfo.disclosureFor && disclosureInfo.year && disclosureInfo.quarter){
      getPoliContributionsDetails(`${disclosureInfo.disclosureFor}?year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}`, (res) => {
        this.setState(prevState => ({
          disclosureInfo: {
            ...prevState.disclosureInfo,
            ...disclosureInfo,
            affirmationNotes: res.affirmationNotes || "",
            G37Obligation: res.G37Obligation || ""
          },
          contDetails: res || {},
          loading: false,
        }))
      })
    }
  }

  render() {
    const {disclosureInfo, contDetails, currentQuarter, currentYear, isSaveDisabled, dropDown, loading} = this.state
    let canEdit = true
    const noDisclosure = contDetails && contDetails.hasOwnProperty("noDisclosure") ? contDetails.noDisclosure : true
    if (contDetails.status === "Complete") {
      canEdit = false
    }

    if (loading) {
      return <Loader/>
    }
    return (
      <div id="main">
        <div>
          <Accordion multiple activeItem={[0]} boxHidden render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(0)} title="Affirmation & Submission Comments">
                {activeAccordions.includes(0) &&
                 <AffirmationAndSubmission user={this.props.user} disable={!canEdit || false} svControls={this.props.svControls} currentQuarter={currentQuarter}
                   currentYear={currentYear} dropDown={dropDown} item={disclosureInfo} onChange={this.onChange}
                   noDisclosure={noDisclosure}/>
                }
                {
                  canEdit && noDisclosure &&
                  <div className="is-full">
                    <div className="field is-grouped">
                      <div className="control">
                        <button className="button is-link is-small" onClick={this.onSave} disabled={!disclosureInfo.G37Obligation || isSaveDisabled || false}>Save</button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small" onClick={this.onCancel}>Cancel</button>
                      </div>
                    </div>
                  </div>
                }
              </RatingSection>
            </div>
          }/>
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

export default withRouter(connect(mapStateToProps, null)(Complete))
