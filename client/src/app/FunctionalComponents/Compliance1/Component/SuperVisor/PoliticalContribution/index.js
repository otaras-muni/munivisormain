import React from "react"
import {Link, withRouter} from "react-router-dom"
import Loader from "Global/Loader"
import Disclosure from "./Pages/Disclosure"
import Dashboard from "./Pages/Dashboard"
import Information from "./Pages/Information"
import MSRBSubmission from "./Pages/MSRBSubmission"
import Disclaimer from "../../../../../GlobalComponents/Disclaimer"

const tabs = [{
  title: <span>Information</span>,
  value: 1,
  path: "information",
},{
  title: <span>Dashboard</span>,
  value: 2,
  path: "dashboard",
},{
  title: <span>Disclosure</span>,
  value: 3,
  path: "disclosure",
},{
  title: <span>MSRB Filing Information</span>,
  value: 4,
  path: "msrbsubmission",
}]

class PoliticalContribution extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSummary: false,
      loading: true,
    }
  }

  componentWillMount() {
    this.setState({
      loading: false,
    })
  }

  renderSelectedView = () => {
    switch(this.props.nav3 || "information") {
      case "information" :
        return <Information {...this.props} />
      case "dashboard" :
        return <Dashboard {...this.props} />
      case "disclosure" :
        return <Disclosure {...this.props} />
      case "msrbsubmission" :
        return <MSRBSubmission {...this.props} />
      default:
        return this.props.nav3
    }
  }

  render() {
    const activeTab = tabs.find(x => x.path === (this.props.nav3 || "information") )
    const loading = (
      <Loader/>
    )

    if (this.state.loading || !activeTab) {
      return loading
    }

    return(
      <section>
        <div className="tabs">
          <ul>
            {
              tabs.map(tab=>(
                <li key={tab.value} className={`${tab.value === parseInt(activeTab.value) && "is-active"}`}>
                  <Link to={`/compliance/cmp-sup-political/${tab.path}`} role="button" className="tabSecLevel" >{tab.title}</Link>
                </li>
              ))
            }
          </ul>
        </div>
        {this.renderSelectedView()}
        <hr/>
        <Disclaimer/>
      </section>
    )
  }
}

export default withRouter(PoliticalContribution)
