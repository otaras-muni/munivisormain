import React from "react"
import Loader from "./../../../../GlobalComponents/Loader"
import SuperVisorTabs from "./SuperVisorTabs"
import SidebarView from "./SidebarView"
import BusinessConduct from "./BusinessConduct"
import GeneralAdmin from "./GeneralAdmin"
import ProfessionQualifications from "./ProfessionQualifications"
import ClientComplaints from "./ClientComplaints"
import SupervisoryObligations from "./SupervisoryObligations"
import PoliticalContribution from "./PoliticalContribution"
import GiftsAndGratuities from "./GiftsAndGrautities"

class SuperVisor extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSideBarOpen: true,

      tiles: [
        { path: "cmp-sup-general", label: "General Administration Activities", row: 1 },
        { path: "cmp-sup-prof", label: "Professional Qualifications", row: 1 },
        { path: "cmp-sup-busconduct", label: "Business Conduct Activities", row: 1 },
        { path: "cmp-sup-gifts", label: "Gifts and Gratuities", row: 1 },
        { path: "cmp-sup-political", label: "Political Contributions and Prohibitions", row: 2 },
        { path: "cmp-sup-complaints", label: "Client Complaints", row: 2 },
        { path: "cmp-sup-compobligations", label: "Supervisory and Compliance Obligations", row: 2 },
        { path: "", label: "", row: 2 },
      ]
    }
  }
  handleSideBarToggle = () => {
    this.setState({
      isSideBarOpen: !this.state.isSideBarOpen,
    })
  }
  renderSelectedView = (option) => {
    switch (option) {
      case "cmp-sup-prof":
        return <ProfessionQualifications {...this.props} />
      case "cmp-sup-gifts":
        return <GiftsAndGratuities {...this.props} />
      case "cmp-sup-political":
        return <PoliticalContribution {...this.props} />
      /* case "cmp-sup-nonsolicit" : */
      case "cmp-sup-complaints":
        return <ClientComplaints {...this.props} />
      case "cmp-sup-compobligations":
        return <SupervisoryObligations {...this.props} />
      case "cmp-sup-busconduct":
        return <BusinessConduct {...this.props} />
      case "cmp-sup-general":
        return <GeneralAdmin {...this.props} />
      default:
        return option
    }
  }

  render() {
    const { nav2 } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div id="main">
        <div className="overflow-auto" style={{ padding: 12 }}>
          {nav2 === "supervisor" ?
            <SuperVisorTabs {...this.props} TILES={this.state.tiles} /> :

            <div style={{ minWidth: 150 }}>
              <div className="switchContainer">
                <span className="filterText"> Side Menu</span>
                <label className="customswitch" style={{ "marginLeft": "10px" }}>
                  <input type="checkbox" onChange={this.handleSideBarToggle} checked={this.state.isSideBarOpen} />
                  <span className="customslider round" />
                </label>
              </div>
              <div className="columns">
                <SidebarView {...this.props} showSidebar={this.state.isSideBarOpen} superVisorCategories={this.state.tiles} selectedSuperVisorCategory={nav2} />
                <div className="column">
                  {this.renderSelectedView(nav2)}
                </div>
              </div>
            </div>
          }
        </div>
      </div>
    )
  }
}

export default SuperVisor
