import React from "react"
import {DropdownList} from "react-widgets"

const AffirmationAndSubmission = ({onChange, item={}, dropDown, disable, currentYear, currentQuarter, user, svControls, noDisclosure}) => (
  <div>
    <div className="columns">
      {/* <SelectLabelInput label="Disclosure for" list={dropDown.users} name="disclosureFor" value={item.disclosureFor || ""} onChange={onChange}/> */}
      <div className="column">
        <p className="multiExpLbl ">Disclosure for</p>
        <div className="control">
          <div style={{width: "150px", fontSize: 12}}>
            {svControls.supervisor ?
              <DropdownList filter value={item.disclosureFor || []} data={dropDown.users || []} textField="name" valueField="id" key="name" onChange={(user) => onChange(user, "disclosureFor")} />
              : <span>{`${user.userFirstName} ${user.userLastName}` || []}</span>}
          </div>
        </div>
      </div>
      <div className="column">
        <p className="multiExpLbl">Report Quarter</p>
        <div className="select is-small is-link">
          <select name="quarter" value={item.quarter || ""} onChange={onChange}>
            <option value="">Pick</option>
            {
              dropDown.quarterOfDisclosureFirm.map((q) => (
                <option key={q.value} value={q.value} disabled={q.value > currentQuarter}>{q.name}</option>
              ))
            }
          </select>
        </div>
      </div>
      <div className="column">
        <p className="multiExpLbl">Report Year</p>
        <div className="select is-small is-link">
          <select name="year" value={item.year || ""} onChange={onChange}>
            <option value="">Pick</option>
            {dropDown.yearOfDisclosureFirm.map((value) => <option key={value} value={value} disabled={value > currentYear}>{value}</option>)}
          </select>
        </div>
      </div>
      {/* <SelectLabelInput label="Report Year" list={dropDown.yearOfDisclosureFirm} name="year" value={item.year || ""} onChange={onChange}/> */}
    </div>
    {
      noDisclosure ?
        <div>
          <div className="columns">
            <div className="column is-full">
              <div className="control">
                <textarea className="textarea" onChange={onChange} value={item.affirmationNotes} name="affirmationNotes" placeholder="" disabled={disable}/>
              </div>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <label className="checkbox">
                <p className="multiExpLbl">
                  <input type="checkbox" name="giftsAffirmCompletionOfG20ForPeriod" checked={item.giftsAffirmCompletionOfG20ForPeriod || false} onClick={onChange} onChange={onChange}/>
                  I confirm that I have not made gifts or gratuities in relation to the municipal securities or municipal advisory activities. I have no disclosures to be made. I confirm completion of my G-20 obligation for the period.
                </p>
              </label>
            </div>
          </div>
        </div> : <div className="title innerPgTitle"><strong> Disclosure already exists for this quarter  </strong> </div>
    }

  </div>
)
export default AffirmationAndSubmission
