import React from "react"
import {Link} from "react-router-dom"

const WhatYouWant = () => (
  <div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
        <a className="link-withouthover link-hover" href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-20+Gifts%2C+Gratuities%2C+Non-Cash+Compensation+and+Expenses+of+Issuance.pdf" download target="_blank">See MSRB Rule G-20</a>
        <strong>Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/gifts-complete" className="link-withouthover link-hover" target="_blank">Confirm</Link>
          <strong> your G-20 obligation if you have No disclosures to make. </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/cmp-sup-gifts/disclosure?status=Pre-approval" className="link-withouthover link-hover" target="_blank">Pre-approval</Link>
          <strong> from your supervisor before you make a gift. </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/cmp-sup-gifts/disclosure" className="link-withouthover link-hover" target="_blank">Complete</Link>
          <strong> G-20 disclosure obligation. </strong>
        </p>
      </div>
    </div>
  </div>

)
export default WhatYouWant
