import React, { Component } from "react"
import {toast} from "react-toastify"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import WhatYouWant from "../components/WhatYouWant"
import CONST from "../../../../../../../globalutilities/consts"
import { postGiftsGratuitiesInfo } from "../../../../../../StateManagement/actions/Supervisor"

class Information extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notes: CONST.CAC.giftsGrautities,
      dropDown: {
        quarterOfDisclosureFirm: [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        yearOfDisclosureFirm: [2017, 2018],
        disclosureFirm: ["Self", "Firm"]
      },
      disclosureInfo : CONST.SuperVisor.giftDisclosureInfo || {},
      isSaveDisabled: true
    }
  }

  onChange = (e) => {
    const name = e.target.name
    this.setState({
      disclosureInfo: {
        ...this.state.disclosureInfo,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }, () => {
      if(name === "G20Obligation"){
        this.postDisclosure()
      }
    })
  }

  postDisclosure = () => {
    const {disclosureInfo} = this.state
    delete disclosureInfo.affirmationNotes
    postGiftsGratuitiesInfo(disclosureInfo, (res) => {
      if(res && res.status === 200) {
        this.setState({
          isSaveDisabled: false
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      }
    })
  }

  render() {
    const { dropDown, disclosureInfo, isSaveDisabled } = this.state
    return (
      <div>
        <div>
          <Accordion multiple
            activeItem={[0]} boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                {/*<ComplianceAction
                  title="Compliance Action Center (CAC)"
                  bodyTitle="Use the Compliance Action Center (CAC) for proactive compliance management. You can set up alerts, notifications and/or reminders for requirements
                  similar to those listed below. It is our best effort to have most rules set up to enable a headstart! You may choose to update those based on your specific requirements.
                  You may also choose to add new rules with immediate effect!"
                  notes={notes}
                />*/}
                <RatingSection onAccordion={() => onAccordion(0)} title="What do you want to do?" category="whatyouwant">
                  {activeAccordions.includes(0) &&
                    <WhatYouWant dropDown={dropDown} item={disclosureInfo} isSaveDisabled={isSaveDisabled} category="polConInfo" onChange={this.onChange} />
                  }
                </RatingSection>
              </div>
            }
          />
        </div>
      </div>
    )
  }
}

export default Information
