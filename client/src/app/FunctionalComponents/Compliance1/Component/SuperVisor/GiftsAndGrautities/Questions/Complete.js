import React, { Component } from "react"
import moment from "moment/moment"
import Loader from "Global/Loader"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import * as qs from "query-string"
import { updateCACPoliticalActionStatus, getToken } from "GlobalUtils/helpers"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import AffirmationAndSubmission from "../components/AffirmationAndSubmission"
import { fetchAssigned } from "../../../../../../StateManagement/actions/CreateTransaction"
import {
  getGiftsGrautitiesDetails, postGiftsGratuitiesInfo
} from "../../../../../../StateManagement/actions/Supervisor"
import CONST from "../../../../../../../globalutilities/consts"

class Complete extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      disclosureInfo: {
        ...CONST.SuperVisor.disclosureInfo,
        giftsAffirmCompletionOfG20ForPeriod: false,
      } || {},
      tempDisclosureInfo: {
        ...CONST.SuperVisor.disclosureInfo,
        giftsAffirmCompletionOfG20ForPeriod: false,
      } || {},
      loading: true,
      contDetails: {},
      isSaveDisabled: false,
    }
  }

  componentWillMount() {
    const queryString = qs.parse(location.search)
    const { currentYear, currentQuarter } = this.state
    const { user } = this.props
    const flag = (parseInt(queryString.year, 10) > currentYear) || ((parseInt(queryString.quarter, 10) > currentQuarter) && (parseInt(queryString.year, 10) === currentYear))
    const year = queryString.year || currentYear
    const quarter = queryString.quarter || currentQuarter
    const disclosureFor = queryString.disclosureFor || user.userId || ""

    const yearOfDisclosureFirm = []
    const min = 2015
    const max = min + 10
    for (let i = min; i <= max; i++) { yearOfDisclosureFirm.push(i) }

    if (!flag) {
      fetchAssigned(user.entityId, (users) => {
        this.setState(prevState => ({
          disclosureInfo: {
            ...prevState.disclosureInfo,
            year, quarter, disclosureFor,
          },
          dropDown: {
            ...prevState.dropDown,
            users: users.usersList || [],
            quarterOfDisclosureFirm: [{ name: "First", value: 1 }, { name: "Second", value: 2 }, { name: "Third", value: 3 }, { name: "Fourth", value: 4 }],
            yearOfDisclosureFirm,
          },
          loading: false,
        }), () => {
          this.getDisclosureInfo()
        })
      })
    } else {
      this.props.history.push("/compliance/cmp-sup-gifts/dashboard")
    }
  }

  getDisclosureInfo = () => {
    const { disclosureInfo } = this.state
    if (disclosureInfo && disclosureInfo.disclosureFor && disclosureInfo.year && disclosureInfo.quarter) {
      getGiftsGrautitiesDetails(`?disclosureFor=${disclosureInfo.disclosureFor}&year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}`, (res) => {
        this.setState(prevState => ({
          disclosureInfo: {
            ...prevState.disclosureInfo,
            affirmationNotes: res && res.affirmationNotes || "",
            giftsAffirmCompletionOfG20ForPeriod: res && res.giftsAffirmCompletionOfG20ForPeriod || false
          },
          tempDisclosureInfo: {
            ...prevState.disclosureInfo,
            affirmationNotes: res && res.affirmationNotes || "",
            giftsAffirmCompletionOfG20ForPeriod: res && res.giftsAffirmCompletionOfG20ForPeriod || false
          },
          contDetails: res || {},
          loading: false,
        }))
      })
    }
  }

  onChange = (e, inputName) => {
    if (inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
          // disclosureName: `${e.userFirstName} ${e.userLastName}`
        }
      }, () => this.getDisclosureInfo())
    } else {
      const { name } = e.target
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
        }
      }, () => {
        if (name === "disclosureFor" || name === "quarter" || name === "year") this.getDisclosureInfo()
      })
    }
  }

  onCancel = () => {
    this.setState({
      disclosureInfo: cloneDeep(this.state.tempDisclosureInfo)
    })
  }

  onSave = () => {
    const { disclosureInfo } = this.state
    // if(!disclosureInfo.affirmationNotes) return
    disclosureInfo.status = "Complete"
    disclosureInfo.noDisclosure = true
    this.setState({
      isSaveDisabled: true
    }, () => {
      postGiftsGratuitiesInfo(disclosureInfo, async (res) => {
        toast("Affirmation and Submission Comments updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        if (res && res.status === 200) {
          try {
            await updateCACPoliticalActionStatus(getToken(), { ...disclosureInfo, type: "gifts" })
          } catch (err) {
            console.log(err)
          }
          this.setState({
            isSaveDisabled: false,
            tempDisclosureInfo: disclosureInfo
          }, () =>
              this.props.history.push("/compliance/cmp-sup-gifts/dashboard"))
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  render() {
    const { disclosureInfo, isSaveDisabled, dropDown, currentQuarter, currentYear, contDetails } = this.state
    const { user, svControls } = this.props
    let canEdit = true
    const noDisclosure = contDetails && contDetails.hasOwnProperty("noDisclosure") ? contDetails.noDisclosure : true
    if (contDetails.status === "Complete") {
      canEdit = false
    }
    if (this.state.loading) {
      return <Loader />
    }
    return (
      <div id="main">
        <div>
          <Accordion multiple
            activeItem={[0]} boxHidden
            render={({ activeAccordions, onAccordion }) =>
              <div>
                <RatingSection onAccordion={() => onAccordion(0)} title="Affirmation and Submission Comments">
                  {activeAccordions.includes(0) &&
                    <AffirmationAndSubmission item={disclosureInfo} onChange={this.onChange} dropDown={dropDown} disable={!canEdit}
                      currentQuarter={currentQuarter} currentYear={currentYear} user={user || {}} svControls={svControls || ""} noDisclosure={noDisclosure} />
                  }
                  {
                    canEdit && noDisclosure &&
                    <div className="column is-full">
                      <div className="field is-grouped">
                        <div className="control">
                          <button className="button is-link is-small" onClick={this.onSave}
                            disabled={!disclosureInfo.giftsAffirmCompletionOfG20ForPeriod || isSaveDisabled || false}>Save
                          </button>
                        </div>
                        <div className="control">
                          <button className="button is-light is-small" onClick={this.onCancel}>Cancel</button>
                        </div>
                      </div>
                    </div>
                  }
                </RatingSection>
              </div>
            }
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

export default Complete
