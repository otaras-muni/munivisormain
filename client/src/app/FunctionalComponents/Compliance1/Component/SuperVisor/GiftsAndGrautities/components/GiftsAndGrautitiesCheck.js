import React from "react"

const GiftsAndGrautitiesCheck = ({item={}, onCheck, state, status, canEdit}) => (
  <div>
    <div className="columns">
      <div className="column">
        <p>
          <small>{item.titleText}
          </small>
          <a href="http://www.msrb.org/Rules-and-Interpretations/MSRB-Rules/General/Rule-G-20.aspx" target="new">
            <i className="fas fa-info-circle" />
          </a>
        </p>
      </div>
    </div>

    <div className="columns">
      <div className="column">
        <label className="checkbox">
          <p className="multiExpLbl">
            <input type="checkbox" name={item.key} title={item.title} checked={state[item.key] || false} onClick={onCheck} onChange={onCheck} disabled={!canEdit} /> {item.boldLine}
          </p>
        </label>
      </div>
    </div>
  </div>
)
export default GiftsAndGrautitiesCheck
