import React from "react"
import {Link} from "react-router-dom"
import { SelectLabelInput } from "../../../../../../GlobalComponents/TextViewBox"

const WhatYouWant = ({dropDown, item={}, onChange, isSaveDisabled}) => (
  <div>
    <div className="columns">
      <SelectLabelInput label="Disclosure for" error= "" list={dropDown.disclosureFirm} name="disclosureFor" value={item.disclosureFor || ""} onChange={onChange} />
      <div className="column">
        <p className="multiExpLbl">Report Quarter</p>
        <div className="select is-small is-link">
          <select name="quarter" value={item.quarter || ""} onChange={onChange}>
            <option value="">Pick</option>
            {
              dropDown.quarterOfDisclosureFirm.map((q) => (
                <option key={q.value} value={q.value}>{q.name}</option>
              ))
            }
          </select>
        </div>
      </div>
      <SelectLabelInput label="Report Year" error= "" list={dropDown.yearOfDisclosureFirm} name="year" value={item.year || ""} onChange={onChange} />
    </div>
    <div className="columns">
      <div className="column">
        <p className="title innerPgTitle">
          <input type="checkbox" name="G20Obligation" checked={item.G20Obligation || false} onClick={onChange} disabled={!item.quarter || !item.year || !item.disclosureFor}/>
          <strong>Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance</strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <p className="title innerPgTitle">
          <Link to={`/compliance/gifts-complete?disclosureFor=${item.disclosureFor}&year=${item.year}&quarter=${item.quarter}`}>
            <button className="button is-warning is-small" value="Confirm" disabled={isSaveDisabled || !item.disclosureFor || !item.year || !item.quarter || !item.G20Obligation || false}>Complete</button>
          </Link><strong> your G-20 obligation if you have NO disclosures to make.</strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <p className="title innerPgTitle">
          <Link to={`/compliance/cmp-sup-gifts/disclosure?disclosureFor=${item.disclosureFor}&year=${item.year}&quarter=${item.quarter}`}>
            <button className="button is-warning is-small" value="Confirm" disabled={isSaveDisabled || !item.disclosureFor || !item.year || !item.quarter || !item.G20Obligation || false}>Complete</button>
          </Link><strong> G-20 disclosure obligation.</strong>
        </p>
      </div>
    </div>
  </div>

)
export default WhatYouWant
