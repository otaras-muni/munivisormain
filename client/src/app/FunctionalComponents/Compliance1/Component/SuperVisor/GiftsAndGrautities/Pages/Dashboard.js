import React, { Component } from "react"
import {withRouter} from "react-router-dom"
import { connect} from "react-redux"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import {fetchAllGifts} from "../../../../../../StateManagement/actions/Supervisor"
import moment from "moment"
import DisclodureFormModal from "../../../../../../GlobalComponents/DisclodureFormModal"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import DashboardTable from "../components/DashboardTable"

const cols = [
  {name: "Name"},
  {name: "Type"},
  {name: "Quarter"},
  {name: "Year"},
  {name: "Link"},
  {name: "Status"}]

const dateRanges = [
  {name: "Last month", value: 1},
  {name: "Last 3 months", value: 3},
  {name: "Last 6 months", value: 6},
  {name: "Last 1 year", value: 12},
  {name: "All", value: 0},
]

class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      search: {
        searchText: "",
        dateRange: "",
        groupBy: "",
      },
      dropDown: {
        dateRange: ["Last month", "Last 3 months", "Last 6 months", "Last 1 year", "All"],
        groupBy: ["Group by recipient", "Group by recipient's employer"],
      },
      giftsList: [],
      allGifts: [],
      loading: true,
      modalState: false,
      disclosureInfo: {},
      path1: "", path2: "",
    }
  }

  async componentDidMount(){
    const {svControls} = this.props
    const res = await fetchAllGifts(svControls.supervisor ? "?type=supervisor" : "")
    if(res){
      this.setState({
        giftsList: res || [],
        allGifts: res || [],
        loading:false,
      })
    }else {
      this.setState({
        loading:false,
      })
    }
  }

  onSearchChange = (e) => {
    const {allGifts, search} = this.state
    const { name, value } = e.target
    search[name] = value
    this.setState({
      search,
    },() => {
      if(allGifts.length){
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const {search, allGifts} = this.state
    if(search && search.searchText) {
      const giftsList = allGifts.filter(obj => ["userFirstName", "userLastName", "quarter", "year", "status"].some(key => {
        /* if(key === "giftDate") {
          return moment(obj[key]).format("MM.DD.YYYY").toLowerCase().includes(search.searchText.toLowerCase())
        } */
        if(key === "giftValue"){
          return obj[key] && obj[key].toString().toLowerCase().includes(search.searchText.toLowerCase())
        }
        if(key === "quarter" || key === "year"){
          return obj[key] && obj[key].toString().toLowerCase().includes(search.searchText.toLowerCase())
        }
        return obj[key] && obj[key].toLowerCase().includes(search.searchText.toLowerCase())
      }))
      this.setState({
        giftsList
      })
    }else {
      this.setState({
        giftsList: allGifts
      })
    }
  }

  onDateRangeSearch = () => {
    const {search, allGifts, giftsList} = this.state
    const gifts = search && search.searchText ? giftsList : allGifts
    if(search && search.dateRange){
      const d = new Date()

      let cuurentDate = moment(d).format("MM-DD-YYYY") // `${d.getDate()}-${d.getMonth()+1}-${d.getFullYear()}`
      cuurentDate = moment(cuurentDate).unix()

      const previous = dateRanges.find(range => range.name === search.dateRange)
      let dateRange = new Date(d.getFullYear(), d.getMonth() - previous.value, d.getDate())
      dateRange = moment(dateRange).unix()

      const giftsList = search.dateRange === "All" ? gifts : gifts.filter(obj => ["giftDate"].some(key => moment(obj[key]).unix() > dateRange && moment(obj[key]).unix() < cuurentDate))

      this.setState({
        giftsList
      })
    }
  }

  onAction = (gift) => {
    let queryStatus = ""
    if(gift.status === "Pre-approval") queryStatus = "&status=Pre-approval"

    if(gift.status === "Pending" && gift.cac && !gift.hasOwnProperty("noDisclosure")){
      this.setState({
        disclosureInfo: {
          year: gift.year,
          quarter: gift.quarter,
          disclosureFor: gift.userOrEntityId
        },
        path1: `/compliance/gifts-complete?disclosureFor=${gift.userOrEntityId}&year=${gift.year}&quarter=${gift.quarter}`,
        path2: `/compliance/cmp-sup-gifts/disclosure?disclosureFor=${gift.userOrEntityId}&year=${gift.year}&quarter=${gift.quarter}${queryStatus}`,
        modalState: true
      })
    }else if(gift.noDisclosure) {
      this.props.history.push(`/compliance/gifts-complete?disclosureFor=${gift.userOrEntityId}&year=${gift.year}&quarter=${gift.quarter}`)
    }else {
      this.props.history.push(`/compliance/cmp-sup-gifts/disclosure?disclosureFor=${gift.userOrEntityId}&year=${gift.year}&quarter=${gift.quarter}${queryStatus}`)
    }
  }

  toggleModal = () => {
    this.setState((prev) => {
      const newState = !prev.modalState
      return { modalState: newState, path1: "", path2: "" }
    })
  }

  render() {
    const { search, loading, giftsList, modalState, path1, path2, disclosureInfo } = this.state
    return (
      <div>
        <div className="columns">
          <div className="column is-half">
            <p className="control has-icons-left">
              <input className="input is-small is-link" type="text" name="searchText" placeholder="search recipient" value={search.searchText || ""} onChange={this.onSearchChange}/>
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <DisclodureFormModal toggleModal={this.toggleModal} modalState={modalState} path1={path1} path2={path2} info={disclosureInfo} type="political"/>
        </div>
        <div>
          <DashboardTable
            giftsList={giftsList}
            onAction={this.onAction}
            search={search.searchText || ""}
          />
        </div>

        {/* {
          loading ? <Loader/> :
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <TableHeader cols={cols}/>
              <tbody>
                {
                  giftsList.map((giftsList,i) => (
                    <tr key={i}>
                      <td className="multiExpTblVal" style={{maxWidth: 300}}>
                        <p>{giftsList && `${giftsList.userFirstName} ${giftsList.userLastName}`}</p>
                      </td>
                      <td className="multiExpTblVal" style={{maxWidth: 300}}>
                        <p>G-20 Quartetly Obligation</p>
                      </td>
                      <td className="multiExpTblVal" style={{maxWidth: 300}}>
                        <p>{giftsList && giftsList.quarter || "---"}</p>
                      </td>
                      <td className="multiExpTblVal">
                        <p>{giftsList && giftsList.year || "---"}</p>
                      </td>
                      <td className="multiExpTblVal">
                        <a className="text-overflow" onClick={() => this.onAction(giftsList)} >Disclosure</a>
                      </td>
                      <td className="multiExpTblVal">
                        <p>{giftsList && giftsList.status || "---"}</p>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
        } */}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Dashboard)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
