import React from "react"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify"
import { connect} from "react-redux"
import Accordion from "../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../GlobalComponents/RatingSection"
import withAuditLogs from "../../../../../GlobalComponents/withAuditLogs"
import EditableTable from "../../../../../GlobalComponents/EditableTable"
import Disclaimer from "../../../../../GlobalComponents/Disclaimer"
import Audit from "../../../../../GlobalComponents/Audit"
import CONST from "../../../../../../globalutilities/consts"
import Loader from "../../../../../GlobalComponents/Loader"
import {fetchBcaDetails, putBusConduct, pullBusConduct, putBcaAuditLogs} from "../../../../../StateManagement/actions/Supervisor"
import {fetchAssigned} from "../../../../../StateManagement/actions/CreateTransaction"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import {BusinessConductValidate} from "../Validation/BusinessConductValidate"

class BusinessConduct extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      tables: [ {
        name:"Contravention of restrictions imposed on associated persons",
        key: "bcaContraDetails",
        row: cloneDeep(CONST.SuperVisor.bcaContraDetails) || {},
        header: [
          {name: "Person"},
          /*{name: "First Name"},
          {name: "Last Name"},
          {name: "Email"},
          {name: "Phone"},*/
          {name: "Violation<span class='has-text-danger'>*</span>"},
          /*{name: "Violation Date"},*/
          {name: "Notes<span class='has-text-danger'>*</span>"},
          {name: "Action"}
        ],
        list: [],
        tbody: [
          {name: "userId", type: "dropdown", labelName1: "userFirstName", labelName2: "userLastName", dropdownKey: "users"},
          /*{name: "userFirstName", placeholder: "First Name", type: "text"},
          {name: "userLastName", placeholder: "Last Name", type: "text"},
          {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
          {name: "userPrimaryPhone", type: "phone"},*/
          {name: "violationType", type: "select"},
          /*{name: "violataionDate", type: "date"},*/
          {name: "violationNotes", placeholder: "Notes", type: "text"},
          {name: "action"},
        ],
        handleError: (payload) => BusinessConductValidate(payload)
      }],
      dropDown:{
        violationType: []
      },
      notes: CONST.CAC.busConduct,
      auditLogs: [],
      loading: true,
    }
  }

  async componentWillMount() {
    const {tables} = this.state
    const {svControls} = this.props
    let result = await getPicklistByPicklistName(["LKUPCONDUCTVIOLATION"])
    result = (result.length && result[1]) || {}
    fetchBcaDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (tbl.key === "bcaContraDetails" && res && res.bcaContraDetails) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          violationType: result.LKUPCONDUCTVIOLATION || ["Suspension", "Revocation of registration", "Revocation of bar", "Unfair practice",
            "Deceptive, dishonesty or unfair practice", "Other"],
        },
        loading: false,
        tables,
        auditLogs: (res && res.auditLogs) || []
      },() => {
        this.getUsers()
      })
    })
  }

  onSave = (type, item, callback) => {
    console.log(type, item)
    putBusConduct(item, (res)=> {
      if (res && res.status === 200) {
        toast("Added Business Conduct successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    pullBusConduct(removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed Business Conduct successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        loading: false,
      })
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    let email = ""
    let phone = ""
    if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In Bussiness conduct select ${user.name} person`, date: new Date(), key })
    callback({
      status: true,
      object: {
        userId: user.id,
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        userPrimaryEmailId: email,
        userPrimaryPhone: phone,
      },
    })
  }

  onAuditSave = (key) => {
    const {user} = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Business Conduct"
        log.superVisorSubSection = "Contravention of restrictions imposed on associated persons"
      })
      putBcaAuditLogs(auditLogs, (res)=> {
        if(res && res.status === 200) {
          this.setState({
            auditLogs: (res.data && res.data.auditLogs) || []
          })
        }
      })
      const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      this.props.updateAuditLog(remainLogs)
    }
  }

  render() {
    const { dropDown, tables, auditLogs} = this.state
    const {svControls} = this.props
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }
    return (
      <div id="main" style={{marginTop: 0}}>
        <p>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
          <a href="../../../../../../public/docs/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
        </p>
        <div>
          <Accordion multiple
            activeItem={[0]}
            boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                {/*<ComplianceAction
                  title="Compliance Action Center (CAC)"
                  bodyTitle="Use the Compliance Action Center (CAC) for proactive compliance management. You can set up alerts, notifications and/or reminders
                  for requirements similar to those listed below. It is our best effort to have most rules set up to enable a headstart! You may choose to update
                  those based on your specific requirements. You may also choose to add new rules with immediate effect!"
                  notes={notes}
                />*/}
                <EditableTable {...this.props} tables={tables} canSupervisorEdit={svControls.canSupervisorEdit} dropDown={dropDown} onSave={this.onSave} onRemove={this.onRemove}
                               onParentChange={this.onDropDownChange}/>
                <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{overflowY: "scroll"}}>
                  {activeAccordions.includes(2) &&
                  <Audit auditLogs={auditLogs || []}/>
                  }
                </RatingSection>
              </div>
            }
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(BusinessConduct)
export default connect(mapStateToProps, null)(WrappedComponent)
