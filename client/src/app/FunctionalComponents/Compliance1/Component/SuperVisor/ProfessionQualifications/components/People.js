import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import EditableTable from "../../../../../../GlobalComponents/EditableTable"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Audit from "../../../../../../GlobalComponents/Audit"
import {
  fetchPqDetails,
  putPqPersonsOrTraining,
  pullPqPersonsOrTraining,
  putPqAuditLogs,
  fetchSeriesPassedAndValid
} from "../../../../../../StateManagement/actions/Supervisor"
import { fetchAssigned } from "../../../../../../StateManagement/actions/CreateTransaction"
import { PqQualifiedPersonsValidate, PqTrainingProgramsValidate } from "../../Validation/ProfQulificationsValidate"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "Global/Loader"
import CONST from "../../../../../../../globalutilities/consts"

class People extends Component {
  constructor(props) {
    super(props)

    this.state = {
      accordions: [1, 2, 3, 4, 5].map((r) => ({ accIndex: r, accOpen: false })),
      isEditable: {},
      tables: [
        {
          name: "Qualified Associated Persons",
          key: "pqQualifiedPersons",
          row: cloneDeep(CONST.SuperVisor.pqQualifiedPersons),
          header: [
            { name: "Person<span class='has-text-danger'>*</span>" },
            /*{name: "First Name"},
            {name: "Last Name"},
            {name: "Email"},
            {name: "Phone"},*/
            { name: "Role<span class='has-text-danger'>*</span>" },
            { name: "Last Training Attended on<span class='has-text-danger'>*</span>" },
            { name: "Series 50 Passed On " },
            { name: "Series 50 Valid Till" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            { name: "userId", type: "dropdown", labelName1: "userFirstName", labelName2: "userLastName", dropdownKey: "users" },
            /*{name: "userFirstName", placeholder: "First Name", type: "text"},
            {name: "userLastName", placeholder: "Last Name", type: "text"},
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},*/
            { name: "userOrgRole", placeholder: "Role", type: "select" },
            { name: "userLastTrainingAttendDate", placeholder: "Pick Date", type: "date" },
            { name: "userSeries50PassedOnDate", placeholder: "Pick Date", type: "date" },
            { name: "userSeries50ValidEndDate", placeholder: "Pick Date", type: "date" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => PqQualifiedPersonsValidate(payload, minDate)
        },
        {
          name: "Training Programs",
          key: "pqTrainingPrograms",
          header: [
            { name: "Program Name<span class='has-text-danger'>*</span>" },
            { name: "Conducted on" },
            { name: "Conducted by<span class='has-text-danger'>*</span>" },
            { name: "Attendance<span class='has-text-danger'>*</span>" },
            { name: "Notes<span class='has-text-danger'>*</span>" },
            { name: "Action" }
          ],
          row: cloneDeep(CONST.SuperVisor.pqTrainingPrograms),
          list: [],
          tbody: [
            { name: "trgProgramName", placeholder: "Program Name", type: "text" },
            { name: "trgOrganizationDate", placeholder: "Conducted On", type: "date" },
            { name: "trgConductedBy", placeholder: "Conducted By", type: "dropdown", dropdownKey: "usersList" },
            { name: "trgAttendees", placeholder: "Attendance", type: "multiselect", dropdownKey: "usersList" },
            { name: "trgNotes", placeholder: "Notes", type: "text" },
            { name: "action" },
          ],
          handleError: (payload, minDate) => PqTrainingProgramsValidate(payload, minDate)
        },
      ],
      notes: CONST.CAC.profQulifications,
      dropDown: {},
      auditLogs: [],
      createdAt: "",
      loading: true,
    }
  }

  async componentWillMount() {
    const { tables } = this.state
    const { svControls } = this.props
    let result = await getPicklistByPicklistName(["LKUPROLEWITHENTITY"])
    result = (result.length && result[1]) || {}
    fetchPqDetails("", (res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t => {
        t.header.splice(-1, 1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          userOrgRole: result.LKUPROLEWITHENTITY || [],
        },
        tables,
        createdAt: res && res.createdAt || "",
        auditLogs: (res && res.auditLogs) || []
      }, () => {
        this.getUsers()
      })
    })
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    })
  }

  onSave = (type, item, callback) => {
    putPqPersonsOrTraining(type, item, (res) => {
      if (res && res.status === 200) {
        toast(`Updated Professional Qualifications successfully`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
          createdAt: ""
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    pullPqPersonsOrTraining(type, removeId, (res) => {
      if (res && res.status === 200) {
        toast(`Removed Professional Qualifications successfully`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users) => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        loading: false,
      })
    })
  }

  onDropDownChange = async (inputName, key, select, index, callback) => {
    if (key === "pqQualifiedPersons" && inputName === "userId") {
      const user = select
      let email = ""
      let phone = ""
      if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }
      this.props.addAuditLog({ userName: this.props.user.userFirstName, log: `In Professional Qualifications ${key} select ${user.name} person`, date: new Date(), key })
      const userDetails = await fetchSeriesPassedAndValid(user.id)
      callback({
        status: true,
        object: {
          userId: user.id,
          userFirstName: user.userFirstName,
          userLastName: user.userLastName,
          userPrimaryEmailId: email,
          userPrimaryPhone: phone,
          userSeries50PassedOnDate: (userDetails && userDetails.series50PassDate) || "",
          userSeries50ValidEndDate: (userDetails && userDetails.series50ValidityEndDate) || ""
        },
      })
    } else if (inputName === "trgConductedBy") {
      this.props.addAuditLog({ userName: this.props.user.userFirstName, log: `In Professional Qualifications ${key} select ${select.name} person`, date: new Date(), key })
      callback({
        status: true,
        object: {
          trgConductedBy: {
            id: select.id,
            name: select.name,
            userId: select.id,
            userFirstName: select.name,
            userLastName: select.userLastName
          }
        },
      })
    } else if (inputName === "trgAttendees") {
      callback({
        status: true,
        object: {
          trgAttendees: select.map(item => ({
            id: item.id,
            name: item.name,
            userId: item.id,
            userFirstName: item.name,
            userLastName: item.userLastName
          })),
        },
      })
    }
  }

  onAuditSave = (key) => {
    const { user } = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    const table = this.state.tables.find(tbl => tbl.key === key)
    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Professional Qualifications"
        log.superVisorSubSection = table.name
      })
      putPqAuditLogs(auditLogs, (res) => {
        if (res && res.status === 200) {
          this.setState({
            auditLogs: (res.data && res.data.auditLogs) || []
          })
        }
      })
      const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      this.props.updateAuditLog(remainLogs)
    }
  }

  renderKeyDetails() {
    const { dropDown, auditLogs, tables, createdAt } = this.state
    const { svControls } = this.props
    return (
      <div>
        <div style={{ marginLeft: 10, marginRight: 10 }}>
          <Accordion multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) =>
              <div>
                {/*<ComplianceAction
                  title="CAC"
                  bodyTitle="Use the Compliance Action Center (CAC) for proactive compliance management. You can set up alerts, notifications and/or reminders
                  for requirements similar to those listed below. It is our best effort to have most rules set up to enable a headstart! You may choose to update
                  those based on your specific requirements. You may also choose to add new rules with immediate effect!"
                  notes={notes}
                />*/}
                <div>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
                  <a href="../../../../../../../public/docs/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle" /></a>
                </div>
                <div>
                  <EditableTable {...this.props} tables={tables} minDate={createdAt} canSupervisorEdit={svControls.canSupervisorEdit} dropDown={dropDown} onSave={this.onSave} onRemove={this.onRemove}
                    onParentChange={this.onDropDownChange} />
                </div>
                <div>
                  <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{ fontSize: "smaller" }}>
                    {activeAccordions.includes(2) &&
                      <Audit auditLogs={auditLogs || []} />
                    }
                  </RatingSection>
                </div>
              </div>
            }
          />
        </div>
        <div style={{ marginLeft: 10, marginRight: 10 }}>
          <hr />
          <Disclaimer />
        </div>
      </div>
    )
  }

  render() {
    const { accordions } = this.state

    if (this.state.loading) {
      return <Loader />
    }
    return (
      accordions && <div>
        {this.renderKeyDetails(1)}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(People)
export default connect(mapStateToProps, null)(WrappedComponent)
