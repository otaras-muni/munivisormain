import React, { Component } from "react"
import cloneDeep from "clone-deep"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import EditableTable from "../../../../../../GlobalComponents/EditableTable"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Audit from "../../../../../../GlobalComponents/Audit"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "../../../../../../GlobalComponents/Loader"
import CONST from "../../../../../../../globalutilities/consts"
import { fetchAssigned } from "../../../../../../StateManagement/actions/CreateTransaction"
import {
  fetchResponsibleDetails,
  putSupervisoryAuditLogs,
  putSupervisory,
  pullSupervisory
} from "../../../../../../StateManagement/actions/Supervisor"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import {
  ResponsibleSupervisoryValidate,
  AnnualCertificationValidate
} from "../../Validation/SupervisoryObligation"

class People extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isEditable: {},
      tables: [
        {
          name: "Persons Responsible for Supervision",
          key: "responsibleSupervision",
          row: cloneDeep(CONST.SuperVisor.responsibleSupervision) || {},
          header: [
            { name: "Person" },
            /*{name: "First Name"},
            {name: "Last Name"},
            {name: "Email"},
            {name: "Phone"},
            {name: "Role in Firm"},*/
            { name: "Designation<span class='has-text-danger'>*</span>" },
            { name: "Active?<span class='has-text-danger'>*</span>" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            {
              name: "userId",
              type: "dropdown",
              labelName1: "userFirstName",
              labelName2: "userLastName",
              dropdownKey: "users"
            },
            /*{name: "userFirstName", placeholder: "First Name", type: "text"},
            {name: "userLastName", placeholder: "Last Name", type: "text"},
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "userRoleFirm", placeholder: "User Role", type: "label"},*/
            {
              name: "userComplianceDesignation",
              placeholder: "Designation",
              type: "select"
            },
            { name: "userActive", placeholder: "Active", type: "select" },
            { name: "action" }
          ],
          handleError: payload => ResponsibleSupervisoryValidate(payload),
          children: this.renderChildren()
        },
        {
          name: "Annual Certifications",
          key: "annualCertification",
          row: cloneDeep(CONST.SuperVisor.annualCertification) || {
            certifiedOn: "",
            certifiedBy: {
              userId: "",
              userFirstName: "",
              userEntityId: "",
              userLastName: "",
              userPrimaryEmailId: ""
            },
            certifierDesignation: "",
            docCategory: "", // Default to "Supervisory Obligations"
            docSubCategory: "", // Default to "Annual Certification"
            docAWSFileLocation: "",
            docFileName: "",
            docNotes: ""
          },
          header: [
            { name: "Certified on<span class='has-text-danger'>*</span>" },
            { name: "Certified by" },
            { name: "Designation<span class='has-text-danger'>*</span>" },
            { name: "Upload File<span class='has-text-danger'>*</span>" },
            { name: "Filename" },
            { name: "Notes" },
            { name: "Action" }
          ],
          list: [],
          dropDown: {
            userIdKey: "certifiedBy.userId"
          },
          tbody: [
            { name: "certifiedOn", type: "date" },
            {
              name: "certifiedBy",
              type: "dropdown",
              labelName: "userFirstName",
              dropdownKey: "users"
            },
            {
              name: "certifierDesignation",
              placeholder: "Designation",
              type: "select"
            },
            { name: "doc", labelName: "docAWSFileLocation", type: "document" },
            {
              name: "docAWSFileLocation",
              type: "file",
              documentIdLabel: "_id"
            },
            { name: "docNotes", placeholder: "Notes", type: "text" },
            { name: "action" }
          ],
          handleError: (payload, minDate) =>
            AnnualCertificationValidate(payload, minDate)
        }
      ],
      notes: CONST.CAC.svcoPeople,
      dropDown: {
        userActive: [],
        userComplianceDesignation: [],
        certifierDesignation: [],
        usersList: []
      },

      loading: true
    }
  }

  async componentWillMount() {
    const { tables } = this.state
    const { svControls } = this.props
    let result = await getPicklistByPicklistName([
      "LKUPOBLIDESIGNATION",
      "LKUPYESNO"
    ])
    result = (result.length && result[1]) || {}
    fetchResponsibleDetails(res => {
      !svControls.canSupervisorEdit
        ? tables.forEach(t => {
            t.header.splice(-1, 1)
          })
        : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState(
        {
          dropDown: {
            ...this.state.dropDown,
            userActive: result.LKUPYESNO || [],
            userComplianceDesignation: result.LKUPOBLIDESIGNATION || [],
            certifierDesignation: result.LKUPOBLIDESIGNATION || []
          },
          loading: false,
          contextId: (res && res._id) || "",
          auditLogs: (res && res.auditLogs) || []
        },
        () => {
          this.getUsers()
        }
      )
    })
  }

  renderChildren = () => {
    return (
      <div className="columns">
        <div className="column">
          <p className="multiExpTblVal">
            Municipal advisor must designate firm contacts via Form A-12 and is
            required to amend Form A-12 to update the designated contacts as
            changes occur. Use the
            <a href="cacMonitor.html" target="new">
              Compliance Action Center
            </a>{" "}
            to set up reminders/alerts/notifications.
          </p>
        </div>
      </div>
    )
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, users => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || []
        },
        loading: false
      })
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    if (inputName === "certifiedBy") {
      const user = select
      let email = ""
      if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      this.props.addAuditLog({
        userName: this.props.user.userFirstName,
        log: `In Annual Certification select certified by ${user.name}`,
        date: new Date(),
        key
      })
      callback({
        status: true,
        object: {
          certifiedBy: {
            id: user.id,
            name: user.name,
            userEntityId: user.entityId,
            userId: user.id,
            userFirstName: user.userFirstName,
            userLastName: user.userLastName,
            userPrimaryEmailId: email
          }
        }
      })
    } else if (inputName === "doc") {
      callback({
        status: true,
        object: {
          docCategory: "Supervisory Obligations",
          docSubCategory: "Annual Certification",
          docAWSFileLocation: (select && select.docAWSFileLocation) || "",
          docFileName: (select && select.docFileName) || ""
        }
      })
    } else {
      const user = select
      let email = ""
      let phone = ""
      if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }

      this.props.addAuditLog({
        userName: this.props.user.userFirstName,
        log: `In Persons Responsible for Supervision select ${
          user.name
        } person`,
        date: new Date(),
        key
      })
      callback({
        status: true,
        object: {
          userId: user.id,
          userFirstName: user.userFirstName,
          userLastName: user.userLastName,
          userPrimaryEmailId: email,
          userPrimaryPhone: phone,
          userRoleFirm: user.userFlags && user.userFlags.toString(),
          userEntityId: user.entityId
        }
      })
    }
  }

  onRemove = (type, removeId, callback) => {
    pullSupervisory(type, removeId, res => {
      if (res && res.status === 200) {
        toast("Removed Supervisory Obligation Admin successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    putSupervisory(type, item, res => {
      if (res && res.status === 200) {
        toast("Added Supervisory Obligation Admin successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onAuditSave = key => {
    const { user } = this.props
    if (key === "responsibleSupervision") {
      const auditLogs = this.props.auditLogs.filter(log => log.key === key)
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Supervisory Obligation"
          log.superVisorSubSection = "Persons Responsible for Supervision"
        })
        putSupervisoryAuditLogs(auditLogs, res => {
          if (res && res.status === 200) {
            this.setState({
              auditLogs: (res.data && res.data.auditLogs) || []
            })
          }
        })
        const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
        this.props.updateAuditLog(remainLogs)
      }
    } else if (key === "annualCertification") {
      const auditLogs = this.props.auditLogs.filter(log => log.key === key)
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.superVisorModule = "Supervisory Obligation"
          log.superVisorSubSection = "Annual Certifications"
        })
        putSupervisoryAuditLogs(auditLogs, res => {
          if (res && res.status === 200) {
            this.setState({
              auditLogs: (res.data && res.data.auditLogs) || []
            })
          }
        })
        const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
        this.props.updateAuditLog(remainLogs)
      }
    } else {
      const auditLogs = this.props.auditLogs.filter(log => log.key === key)
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.superVisorModule = "Supervisory Obligation"
          log.superVisorSubSection = "Documents"
        })
        putSupervisoryAuditLogs(auditLogs, res => {
          if (res && res.status === 200) {
            this.setState({
              auditLogs: (res.data && res.data.auditLogs) || []
            })
          }
        })
        const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
        this.props.updateAuditLog(remainLogs)
      }
    }
  }

  onDeleteDoc = async (type, versionId, documentId, callback) => {
    this.props.addAuditLog({
      userName: `${this.props.user.userFirstName} ${
        this.props.user.userLastName
      }`,
      log: `document removed from Supervisory Obligation`,
      date: new Date(),
      key: "annualCertification"
    })
    pullSupervisory(type, documentId, res => {
      if (res && res.status === 200) {
        toast("Removed Supervisory Obligation Admin successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  render() {
    const { dropDown, tables, auditLogs, contextId } = this.state
    const { svControls, nav2 } = this.props
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div>
          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                {/*<ComplianceAction
                   title="Compliance Action Center (CAC)"
                   bodyTitle="Use the Compliance Action Center (CAC) for proactive compliance management. You can set up alerts, notifications and/or reminders
                    for requirements similar to those listed below. It is our best effort to have most rules set up to enable a headstart! You may choose to update
                    those based on your specific requirements. You may also choose to add new rules with immediate effect!"
                   boldTitle="Annual Certification"
                   notes={this.state.notes}
                 />*/}
                <EditableTable
                  {...this.props}
                  tables={tables}
                  canSupervisorEdit={svControls.canSupervisorEdit}
                  contextType="SVOBLIGATIONS"
                  contextId={nav2}
                  dropDown={dropDown}
                  onSave={this.onSave}
                  onRemove={this.onRemove}
                  onParentChange={this.onDropDownChange}
                  deleteDoc={this.onDeleteDoc}
                />
                <RatingSection
                  onAccordion={() => onAccordion(2)}
                  title="Activity Log"
                  style={{ overflowY: "scroll" }}
                >
                  {activeAccordions.includes(2) && (
                    <Audit auditLogs={auditLogs || []} />
                  )}
                </RatingSection>
              </div>
            )}
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(People)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
