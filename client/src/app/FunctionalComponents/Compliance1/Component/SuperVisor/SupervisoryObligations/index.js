import React, { Component } from "react"
import Loader from "Global/Loader"
import {HeadStart} from "./components/HeadStart"
import People from "./components/People"
import Documents from "./components/Documents"

class SupervisoryCompliance extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      tabActiveIndex: this.props.activeIndex || 0,
      TABS: [
        /*{ label: "HeadStart",row:1,Component: HeadStart},*/
        { label: "People",row:1,Component:People},
        { label: "Documents",row:1,Component:Documents},
      ]
    }
    this.identifyComponentToRender = this.identifyComponentToRender.bind(this)
  }

  componentWillMount() {
    const {TABS} = this.state
    const {activeIndex} = this.props
    const {Component} = TABS[activeIndex || 0]


    this.setState({
      loading: false,
      TABS,
      tabActiveIndex:activeIndex || 0,
      SelectedComponent:Component
    })
  }

  identifyComponentToRender(tab, ind){
    this.setState({SelectedComponent:tab.Component,tabActiveIndex:ind})
  }

  renderTabContents() {
    const {TABS,tabActiveIndex} =this.state
    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        TABS.map ( (t,i) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(t,i)}>
            <a className="tabSecLevel">
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedTabComponent() {
    const {SelectedComponent} = this.state
    return <SelectedComponent {...this.props} />
  }

  render() {

    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="column">
        <div className="tabs">
          {this.renderTabContents()}
        </div>
        {this.renderSelectedTabComponent()}
      </div>
    )
  }
}

export default SupervisoryCompliance
