import Joi from "joi-browser"
import dateFormat from "dateformat"

const keyDetailsSchema = minDate =>
  Joi.object().keys({
    productCode: Joi.string().required(),
    problemCode: Joi.string().required(),
    complaintStatus: Joi.string().required(),
    dateReceived: Joi.date().required(),
    dateOfActivity: Joi.date()
      .allow("")
      .optional(),
    activityDetails: Joi.array()
      .min(0)
      .required()
      .optional(),
    complaintDescription: Joi.string()
      .allow("")
      .optional(),
    actionAgainstComplaint: Joi.string()
      .allow("")
      .optional(),
    createdDate: Joi.date()
      .required()
      .optional(),
    _id: Joi.string()
      .required()
      .optional()
  })

export const KeyDetailsValidate = (inputTransDistribute, minDate) =>
  Joi.validate(inputTransDistribute, keyDetailsSchema(minDate), {
    abortEarly: false,
    stripUnknown: false
  })

const complainantDetailsSchema = Joi.object().keys({
  entityId: Joi.string().required(),
  entityName: Joi.string().required(),
  userId: Joi.string()
    .allow("")
    .required(),
  userFirstName: Joi.string().required(),
  userMiddleName: Joi.string().allow(""),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string()
    .email()
    .required(),
  userPrimaryPhone: Joi.string().required(),
  userFax: Joi.string().required(),
  userAddressConsolidated: Joi.object()
    .keys({
      country: Joi.string().required(),
      state: Joi.string().required(),
      city: Joi.string().required(),
      zipCode: {
        zip1: Joi.string().required(),
        zip2: Joi.string().required()
      },
      addressName: Joi.string().required(),
      addressType: Joi.string()
        .allow("")
        .required(),
      addressLine1: Joi.string().required(),
      addressLine2: Joi.string()
        .allow("")
        .optional()
    })
    .required(),
  _id: Joi.string()
    .required()
    .optional()
})

export const ComplainantDetailsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, complainantDetailsSchema, {
    abortEarly: false,
    stripUnknown: false
  })

const assocPersonDetailsSchema = Joi.object().keys({
  userId: Joi.string()
    .allow("")
    .optional(),
  userFirstName: Joi.string()
    .allow("")
    .optional(),
  userMiddleName: Joi.string()
    .allow("")
    .optional(),
  userLastName: Joi.string()
    .allow("")
    .optional(),
  userPrimaryEmailId: Joi.string()
    .email()
    .required(),
  userPrimaryPhone: Joi.string().required(),
  userEntityId: Joi.string()
    .allow("")
    .optional(),
  userEntityName: Joi.string().required(),
  userPrimaryAddress: Joi.string().required(),
  _id: Joi.string()
    .required()
    .optional()
})

export const AssocPersonDetailsValidate = inputTransDistribute =>
  Joi.validate(inputTransDistribute, assocPersonDetailsSchema, {
    abortEarly: false,
    stripUnknown: false
  })
