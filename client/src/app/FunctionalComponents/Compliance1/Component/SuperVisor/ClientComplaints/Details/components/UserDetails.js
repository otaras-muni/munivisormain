import React from "react"
import Autosuggest from "react-autosuggest"
import NumberFormat from "react-number-format"
import {
  TextLabelInput,
  DropDownInput
} from "../../../../../../../GlobalComponents/TextViewBox"
import CONST from "../../../../../../../../globalutilities/consts"

const UserDetails = ({
  dropDown,
  errors,
  item,
  onChangeItem,
  canSupervisorEdit,
  userAddresses,
  onBlur,
  category,
  onSave,
  onCancel,
  onFetchRequested,
  onClearRequested,
  onOrganizationSelect,
  isSaveDisabled,
  isDisclosure
}) => {
  const onChange = event => {
    const inArray = [
      "addressName",
      "addressLine1",
      "addressLine2",
      "country",
      "state",
      "city",
      "zip1",
      "zip2"
    ]
    let newItem = {}
    if (inArray.indexOf(event.target.name) !== -1) {
      if (event.target.name === "zip1" || event.target.name === "zip2") {
        newItem = {
          ...item,
          complainantDetails: {
            ...item.complainantDetails,
            userAddressConsolidated: {
              ...item.complainantDetails.userAddressConsolidated,
              zipCode: {
                ...item.complainantDetails.userAddressConsolidated.zipCode,
                [event.target.name]:
                  event.target.type === "checkbox"
                    ? event.target.checked
                    : event.target.value
              }
            }
          }
        }
      } else {
        newItem = {
          ...item,
          complainantDetails: {
            ...item.complainantDetails,
            userAddressConsolidated: {
              ...item.complainantDetails.userAddressConsolidated,
              [event.target.name]:
                event.target.type === "checkbox"
                  ? event.target.checked
                  : event.target.value
            }
          }
        }
      }
    } else {
      newItem = {
        ...item,
        complainantDetails: {
          ...item.complainantDetails,
          [event.target.name]:
            event.target.type === "checkbox"
              ? event.target.checked
              : event.target.value
        }
      }
    }
    onChangeItem(newItem, category)
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onOrgChange = (firm, name) => {
    if (name === "addressName") {
      delete firm.isActive
      delete firm.isPrimary
      onChangeItem(
        {
          ...item,
          complainantDetails: {
            ...item.complainantDetails,
            userAddressConsolidated: {
              ...item.complainantDetails.userAddressConsolidated,
              ...firm,
              country: "United States"
            }
          }
        },
        category
      )
    } else {
      onChangeItem(
        {
          ...item,
          complainantDetails: {
            ...CONST.complaint.complainantDetails,
            entityId: firm.id,
            entityName: firm.name
          }
        },
        category
      )
      if (firm && firm.id) {
        onOrganizationSelect(firm.id)
      }
    }
  }
  const complainantDetails = item.complainantDetails

  const onChangeSelect = (e, { newValue }) => {
    onChangeItem(
      {
        ...item,
        complainantDetails: {
          ...item.complainantDetails,
          userFirstName: newValue,
          userId: "",
          userMiddleName: "",
          userLastName: "",
          userPrimaryEmailId: "",
          userPrimaryPhone: "",
          userFax: "",
          userAddressConsolidated: {
            country: "",
            state: "",
            city: "",
            zipCode: {
              zip1: "",
              zip2: ""
            },
            addressName: "",
            addressType: "",
            addressLine1: "",
            addressLine2: ""
          }
        }
      },
      category
    )
  }

  const onSuggestionSelected = (
    event,
    { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
  ) => {
    const user = suggestion || {}
    let email = ""
    let phone = ""
    let fax = ""
    let address = {}
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    if (user && Array.isArray(user.userFax) && user.userFax.length) {
      fax = user.userFax.find(e => e.faxPrimary)
      fax = fax ? fax.faxNumber : user.userFax[0].faxNumber
    }
    if (
      user &&
      Array.isArray(user.userAddresses) &&
      user.userAddresses.length
    ) {
      address = user.userAddresses[0]
      delete address._id
      delete address.isActive
      delete address.isPrimary
    }

    onChangeItem(
      {
        ...item,
        complainantDetails: {
          ...item.complainantDetails,
          userId: user.id || "",
          userFirstName: suggestionValue || "",
          userMiddleName: user.userMiddleName || "",
          userLastName: user.userLastName || "",
          userPrimaryEmailId: email || "",
          userPrimaryPhone: phone || "",
          userFax: fax || "",
          userAddressConsolidated: {
            ...item.complainantDetails.userAddressConsolidated,
            ...address,
            country: "United States"
          }
        }
      },
      category
    )
  }

  const inputProps = {
    placeholder: "First Name",
    value: complainantDetails.userFirstName,
    onChange: onChangeSelect,
    name: "userFirstName",
    title: "First Name",
    onBlur: onBlurInput
  }

  return (
    <div className="complain-details">
      <div className="columns">
        {!isDisclosure ? (
          <DropDownInput
            label="Complainant’s Organization Name"
            required
            error={errors.entityId || ""}
            filter
            data={dropDown.organizations}
            value={complainantDetails.entityName || ""}
            onChange={onOrgChange}
            disabled={!canSupervisorEdit}
          />
        ) : null}
      </div>

      <div className="columns ">
        {canSupervisorEdit ? (
          <div className="column firmProfile">
            <p className="multiExpLbl">First Name</p>
            <div className="control complain-details rw-widget-container">
              <Autosuggest
                suggestions={dropDown.suggestions || []}
                onSuggestionsFetchRequested={onFetchRequested}
                onSuggestionsClearRequested={onClearRequested}
                onSuggestionSelected={onSuggestionSelected}
                getSuggestionValue={suggestion => suggestion.name || ""}
                renderSuggestion={suggestion => <div>{suggestion.name}</div>}
                inputProps={inputProps}
              />
            </div>
            {errors.userFirstName ? (
              <p className="text-error">{errors.userFirstName}</p>
            ) : null}
          </div>
        ) : (
          <div className="column">
            <p className="multiExpLbl">
              First Name<span className="has-text-danger">*</span>
            </p>
            <small>{}</small>
          </div>
        )}
        <TextLabelInput
          label="Middle Name"
          error={errors.userMiddleName || ""}
          name="userMiddleName"
          placeholder="Middle Name"
          value={complainantDetails.userMiddleName}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!canSupervisorEdit}
        />
        <TextLabelInput
          label="Last Name"
          required
          error={errors.userLastName || ""}
          name="userLastName"
          placeholder="Last Name"
          value={complainantDetails.userLastName || ""}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!canSupervisorEdit}
        />
      </div>

      <div className="columns ">
        <div className="column ">
          <p className="multiExpLbl">
            Email<span className="has-text-danger">*</span>
          </p>
          <TextLabelInput
            title="Email"
            error={errors.userPrimaryEmailId || ""}
            name="userPrimaryEmailId"
            placeholder="Email"
            value={complainantDetails.userPrimaryEmailId || ""}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!canSupervisorEdit}
          />
        </div>

        <div className="column">
          <p className="multiExpLbl">
            Phone<span className="has-text-danger">*</span>
          </p>
          <NumberFormat
            title="Phone"
            format="+1 (###) ###-####"
            mask="_"
            className="input is-small is-link"
            name="userPrimaryPhone"
            placeholder="+1 (###) ###-####"
            value={complainantDetails.userPrimaryPhone}
            onBlur={event => onBlurInput(event)}
            onChange={event => onChange(event)}
            disabled={!canSupervisorEdit}
          />
          {errors.userPrimaryPhone && (
            <small className="text-error">{errors.userPrimaryPhone}</small>
          )}
        </div>

        <div className="column">
          <p className="multiExpLbl">
            Fax<span className="has-text-danger">*</span>
          </p>
          <NumberFormat
            title="Fax"
            format="+1 ###-###-####"
            mask="_"
            className="input is-small is-link"
            name="userFax"
            placeholder="+1 ###-###-####"
            value={complainantDetails.userFax || ""}
            onBlur={event => onBlurInput(event)}
            onChange={event => onChange(event)}
            disabled={!canSupervisorEdit}
          />
          {errors.userFax && (
            <small className="text-error">{errors.userFax}</small>
          )}
        </div>
      </div>

      <div className="columns ">
        {userAddresses.length ? (
          <DropDownInput
            label="Address Name"
            required
            error={errors.addressName || ""}
            filter
            data={userAddresses}
            value={complainantDetails.userAddressConsolidated.addressName || ""}
            onChange={addr => onOrgChange(addr, "addressName")}
            disabled={!canSupervisorEdit}
          />
        ) : (
          <TextLabelInput
            label="Address Name"
            required
            error={errors.addressName || ""}
            name="addressName"
            placeholder="Address Name"
            value={complainantDetails.userAddressConsolidated.addressName || ""}
            onChange={onChange}
            onBlur={onBlurInput}
            disabled={!canSupervisorEdit}
          />
        )}
        <TextLabelInput
          label="Address Line 1"
          required
          error={errors.addressLine1 || ""}
          name="addressLine1"
          placeholder="Address Line 1"
          value={complainantDetails.userAddressConsolidated.addressLine1 || ""}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!canSupervisorEdit}
        />
        <TextLabelInput
          label="Address Line 2"
          error={errors.addressLine2 || ""}
          name="addressLine2"
          placeholder="Address Line 2"
          value={complainantDetails.userAddressConsolidated.addressLine2 || ""}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!canSupervisorEdit}
        />
      </div>

      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">
            Country<span className="has-text-danger">*</span>
          </p>
          <div className="control">
            <div className="select is-small is-link">
              <select
                title="Country"
                name="country"
                value={complainantDetails.userAddressConsolidated.country || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              >
                <option value="">Pick Country</option>
                {dropDown.stateCountry &&
                  Object.keys(dropDown.stateCountry).map((country, i) => (
                    <option key={i} value={country}>
                      {country}
                    </option>
                  ))}
              </select>
              {errors.country && <p className="text-error">{errors.country}</p>}
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">
            State<span className="has-text-danger">*</span>
          </p>
          <div className="control">
            <div className="select is-small is-link">
              <select
                title="State"
                name="state"
                value={complainantDetails.userAddressConsolidated.state || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              >
                <option value="">Pick State</option>
                {dropDown.stateCountry &&
                  complainantDetails.userAddressConsolidated &&
                  complainantDetails.userAddressConsolidated.country &&
                  Object.keys(
                    dropDown.stateCountry[
                      complainantDetails.userAddressConsolidated.country
                    ]
                  ).map((state, i) => (
                    <option key={i} value={state}>
                      {state}
                    </option>
                  ))}
              </select>
              {errors.state && <p className="text-error">{errors.state}</p>}
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">
            City<span className="has-text-danger">*</span>
          </p>
          <div className="control">
            <div className="select is-small is-link">
              <select
                title="City"
                name="city"
                value={complainantDetails.userAddressConsolidated.city || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              >
                <option value="">Pick City</option>
                {dropDown.stateCountry &&
                complainantDetails.userAddressConsolidated &&
                complainantDetails.userAddressConsolidated.country &&
                complainantDetails.userAddressConsolidated.state &&
                dropDown.stateCountry[
                  complainantDetails.userAddressConsolidated.country
                ] &&
                dropDown.stateCountry[
                  complainantDetails.userAddressConsolidated.country
                ][complainantDetails.userAddressConsolidated.state]
                  ? dropDown.stateCountry[
                      complainantDetails.userAddressConsolidated.country
                    ][complainantDetails.userAddressConsolidated.state].map(
                      (city, i) => (
                        <option key={i} value={city}>
                          {city}
                        </option>
                      )
                    )
                  : null}
              </select>
              {errors.city && <p className="text-error">{errors.city}</p>}
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl ">
            Zip Code<span className="has-text-danger">*</span>
          </p>
          <div className="field is-grouped">
            <div className="control">
              <TextLabelInput
                title="Zip Code 1"
                error={errors.zip1 || ""}
                name="zip1"
                placeholder="Zip-1"
                value={
                  complainantDetails.userAddressConsolidated.zipCode.zip1 || ""
                }
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              />
            </div>
            <div className="control">
              <TextLabelInput
                title="Zip Code 2"
                error={errors.zip2 || ""}
                name="zip2"
                placeholder="Zip-2"
                value={
                  complainantDetails.userAddressConsolidated.zipCode.zip2 || ""
                }
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canSupervisorEdit}
              />
            </div>
          </div>
        </div>
      </div>
      {canSupervisorEdit ? (
        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped">
              <div className="control">
                <button
                  className="button is-link is-small"
                  onClick={onSave}
                  disabled={isSaveDisabled.complainantDetails || false}
                >
                  Save
                </button>
              </div>
              <div className="control">
                <button className="button is-light is-small" onClick={onCancel}>
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  )
}

export default UserDetails
