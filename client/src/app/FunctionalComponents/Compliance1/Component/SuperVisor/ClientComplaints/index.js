import React from "react"
import {Link, withRouter} from "react-router-dom"
import Loader from "Global/Loader"
import ClientComplaintsList from "./List/ComplaintsList"
import ClientComplaintsDetails from "./Details/ComplaintsDetails"
import Disclaimer from "../../../../../GlobalComponents/Disclaimer"

const tabs = [{
  title: <span>Complaints List</span>,
  value: 1,
  path: "list",
},{
  title: <span>Complaints Detail</span>,
  value: 2,
  path: "details",
}]

class ClientComplaintsMain extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSummary: false,
      loading: true,
    }
  }

  componentWillMount() {
    this.setState({
      loading: false,
    })
  }

  renderSelectedView = () => {
    switch(this.props.nav3 || "list") {
    case "list" :
      return <ClientComplaintsList {...this.props} />
    case "details" :
      return <ClientComplaintsDetails {...this.props} />
    default:
      return this.props.nav3
    }
  }

  render() {
    const activeTab = tabs.find(x => x.path === (this.props.nav3 || "list") )
    console.log('this.props.nav3=================',this.props.nav3)
    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }

    return(
      <section>
        <div className="tabs">
          <ul>
            {
              tabs.map(tab=>(
                <li key={tab.value} className={`${tab.value === parseInt(activeTab.value) && "is-active"}`}>
                  <Link to={`/compliance/cmp-sup-complaints/${tab.path}`} role="button" className="tabSecLevel" >{tab.title}</Link>
                </li>
              ))
            }
          </ul>
        </div>
        {/*<Tabs activeTab={activeTab && activeTab.value} type="rfp" transactionId={this.props.nav2} tabs={tabs} link={this.props.nav3}/>*/}
        {this.renderSelectedView()}
        <hr/>
        <Disclaimer/>
      </section>
    )
  }
}

export default withRouter(ClientComplaintsMain)
