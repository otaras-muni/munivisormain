import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Documents from "../../../../../../GlobalComponents/DocumentPage"
import {
  fetchUserTransactions,
  fetchAssigned,
  fetchEntNotEqSelf
} from "../../../../../../StateManagement/actions/CreateTransaction/index"
import {
  fetchComplaintDetailsById,
  putComplaintDetails,
  pullCmplAssocPersonDetails,
  putsvCmplDocuments,
  pullsvCmplDocuments,
  putCmplDetailsAuditLogs
} from "../../../../../../StateManagement/actions/Supervisor"
import {
  AssocPersonDetailsValidate,
  ComplainantDetailsValidate,
  KeyDetailsValidate
} from "../../Validation/ClientComplaintsValidate"
import Loader from "Global/Loader"
import EditableTable from "../../../../../../GlobalComponents/EditableTable"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import KeyDetail from "./components/KeyDetail"
import UserDetails from "./components/UserDetails"
import Audit from "../../../../../../GlobalComponents/Audit"
import CONST from "../../../../../../../globalutilities/consts"

const relatedActivityDropdown = [
  { id: 1, name: "Naveen" },
  { id: 2, name: "Deepak" },
  { id: 3, name: "Kapil" }
]

class ClientComplaintsDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      complaint: CONST.complaint,
      tempComplaint: CONST.complaint,
      dropDown: {
        compProdCodes: [],
        compProblemCodes: [],
        compDocTypes: [],
        compDocActions: [],
        stateCountry: [],
        suggestions: [],
        allSuggestions: [],
        usersList: [],
        transactions: []
      },
      tables: [
        {
          name: "Associated person(s) identified in the complaint",
          key: "complainantAssocPersonDetails",
          row: {
            userFirstName: "",
            userMiddleName: "",
            userLastName: "",
            userEntityName: "",
            userPrimaryEmailId: "",
            userPrimaryPhone: "",
            userPrimaryAddress: ""
          },
          header: [
            { name: "Person" },
            /*{name: "First Name"},
            {name: "Middle Name"},
            {name: "Last Name"},*/
            { name: "Associated Entity<span class='has-text-danger'>*</span>" },
            { name: "Primary Email<span class='has-text-danger'>*</span>" },
            { name: "Primary Phone<span class='has-text-danger'>*</span>" },
            { name: "Primary Address<span class='has-text-danger'>*</span>" },
            { name: "Action" }
          ],
          list: [],
          tbody: [
            {
              name: "userId",
              type: "dropdown",
              labelName: "userFirstName",
              dropdownKey: "users"
            },
            /*{name: "userFirstName", placeholder: "First Name", type: "text"},
            {name: "userMiddleName", placeholder: "Middle Name", type: "text"},
            {name: "userLastName", placeholder: "Last Name", type: "text"},*/
            {
              name: "userEntityName",
              placeholder: "Associated Entity",
              type: "text"
            },
            { name: "userPrimaryEmailId", placeholder: "Email", type: "text" },
            { name: "userPrimaryPhone", placeholder: "Phone", type: "phone" },
            {
              name: "userPrimaryAddress",
              placeholder: "Address",
              type: "text"
            },
            { name: "action" }
          ],
          handleError: payload => AssocPersonDetailsValidate(payload)
        }
      ],
      errorMessages: {},
      auditLogs: [],
      documentsList: [],
      isSaveDisabled: {
        keyDetails: false,
        complainantDetails: false
      },
      isLoading: true
    }
  }

  componentWillMount() {
    const comId = this.props.nav4 || ""
    const { tables } = this.state
    const { svControls } = this.props
    if (comId) {
      fetchComplaintDetailsById(comId, res => {
        const comEntityId =
          (res && res.complainantDetails && res.complainantDetails.entityId) ||
          ""
        const queryString = comEntityId
          ? `${this.props.user.entityId},${comEntityId}`
          : this.props.user.entityId
        fetchAssigned(queryString, users => {
          !svControls.canSupervisorEdit
            ? tables.forEach(t => {
                t.header.splice(-1, 1)
              })
            : null
          tables.forEach(tbl => {
            tbl.list = (res && res[tbl.key]) || []
          })
          const usersList = users.usersList.filter(
            u => u.entityId === this.props.user.entityId
          )
          const allSuggestions = users.usersList.filter(
            u => u.entityId === comEntityId
          )

          if (res) {
            this.setState(prevState => ({
              complaint: {
                ...prevState.complaint,
                keyDetails: {
                  ...prevState.complaint.keyDetails,
                  ...res.keyDetails
                },
                complainantDetails: {
                  ...prevState.complaint.complainantDetails,
                  ...res.complainantDetails
                }
              },
              tempComplaint: {
                ...prevState.complaint,
                keyDetails: {
                  ...prevState.complaint.keyDetails,
                  ...res.keyDetails
                },
                complainantDetails: {
                  ...prevState.complaint.complainantDetails,
                  ...res.complainantDetails
                }
              },
              dropDown: {
                ...prevState.dropDown,
                usersList,
                allSuggestions
              },
              tables,
              documentsList:
                res && res.complaintDocuments && res.complaintDocuments.length
                  ? res.complaintDocuments
                  : [],
              auditLogs:
                res && res.auditLogs && res.auditLogs.length
                  ? res.auditLogs
                  : []
            }))
          }
        })
      })
    }
  }

  async componentDidMount() {
    this.getPickLists()
    this.getTransactions()
    fetchEntNotEqSelf(this.props.user.entityId, organizations => {
      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          organizations
        },
        isLoading: false
      }))
    })
  }

  getTransactions = async () => {
    const res = await fetchUserTransactions(this.props.user.entityId)
    let transactions = []
    if (res.transactions && Array.isArray(res.transactions)) {
      transactions = res.transactions.map(tran => ({
        ...tran,
        name:
          tran.rfpTranProjectDescription ||
          tran.dealIssueTranProjectDescription ||
          tran.actTranProjectDescription ||
          tran.actProjectName ||
          "",
        id: tran._id, // eslint-disable-line
        group: tran.actTranType
          ? tran.actTranType
          : tran.actType
          ? tran.actType
          : tran.rfpTranType === "RFP"
          ? tran.rfpTranType
          : tran.dealIssueTranSubType === "Bond Issue"
          ? "Deal"
          : ""
      }))
    }
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        transactions
      }
    })
  }

  getPickLists = async () => {
    const picResult = await getPicklistByPicklistName([
      "LKUPCOUNTRY",
      "LKUPCMPLPRODCODES",
      "LKUPCMPLPROBLEMCODES",
      "LKUPCOMPLAINTDOCTYPE",
      "LKUPCOMPLAINTDOCACTION"
    ])
    const result = (picResult.length && picResult[1]) || {}
    const stateCountry = (picResult[3] && picResult[3].LKUPCOUNTRY) || {}
    this.setState(prevState => ({
      dropDown: {
        ...prevState.dropDown,
        compProdCodes: result.LKUPCMPLPRODCODES || [],
        compProblemCodes: result.LKUPCMPLPROBLEMCODES || [],
        compDocTypes: result.LKUPCOMPLAINTDOCTYPE || [],
        compDocActions: result.LKUPCOMPLAINTDOCACTION || [],
        stateCountry
      }
    }))
  }

  onOrganizationSelect = entityId => {
    fetchAssigned(entityId, users => {
      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          allSuggestions: users.usersList || []
        }
      }))
    })
  }

  onChangeItem = (item, category) => {
    this.setState({
      [category]: item
    })
  }

  onBlur = (category, change) => {
    const userName = (this.props.user && this.props.user.userFirstName) || ""
    this.props.addAuditLog({
      userName,
      log: `${change}`,
      date: new Date(),
      key: category
    })
  }

  onCancel = key => {
    let tempComplaint = cloneDeep(this.state.tempComplaint)
    if (key === "keyDetails") {
      this.setState({
        complaint: tempComplaint,
        errorMessages: {}
      })
    } else if (key === "complainantDetails") {
      this.setState({
        complaint: tempComplaint,
        errorMessages: {}
      })
    }
  }

  onSave = key => {
    const { complaint, tables } = this.state
    const comId = this.props.nav4
    console.log(complaint[key])
    let errors = {}
    if (key === "keyDetails") {
      errors = KeyDetailsValidate(
        complaint[key],
        complaint.keyDetails.createdDate
      )
    } else if (key === "complainantDetails") {
      errors = ComplainantDetailsValidate(complaint[key])
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, [key]: errorMessages }
      }))
      return
    }

    this.setState(
      {
        isSaveDisabled: {
          ...this.state.isSaveDisabled,
          [key]: true
        }
      },
      () => {
        putComplaintDetails(key, comId, complaint[key], res => {
          if (res && res.status === 200) {
            toast(
              `Added ${
                key === "keyDetails"
                  ? "key details"
                  : key === "complainantDetails"
                  ? "Complainant’s name and address"
                  : "Associated person(s)"
              } successfully`,
              { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
            )
            this.onAuditSave("complaint")
            if (res.data && res.data._id) {
              /*this.props.identifyComponentToRender("Compliance Detail", res.data._id)*/
              tables.forEach(tbl => {
                tbl.list = (res && res[tbl.key]) || []
              })
            }

            if (
              complaint &&
              complaint.keyDetails &&
              complaint.keyDetails.createdDate
            ) {
              this.setState(prevState => ({
                complaint: {
                  ...prevState.complaint,
                  [key]: (res.data && res.data[key]) || {}
                },
                isSaveDisabled: {
                  ...prevState.isSaveDisabled,
                  [key]: false
                },
                errorMessages: {
                  ...prevState.errorMessages,
                  [key]: {}
                },
                tables
              }))
            } else {
              this.props.history.push("/compliance/cmp-sup-complaints")
            }
          } else {
            toast("Something went wrong!", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState(prevState => ({
              isSaveDisabled: {
                ...prevState.isSaveDisabled,
                [key]: false
              }
            }))
          }
        })
      }
    )
  }

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState(prevState => ({
      dropDown: {
        ...prevState.dropDown,
        suggestions: this.getSuggestions(value)
      }
    }))
  }

  getSuggestions = value => {
    const inputValue = value.trim().toLowerCase()
    const inputLength = inputValue.length
    return inputLength === 0
      ? []
      : this.state.dropDown && this.state.dropDown.allSuggestions
      ? this.state.dropDown.allSuggestions.filter(
          item => item.name.toLowerCase().slice(0, inputLength) === inputValue
        )
      : []
  }

  onSuggestionsClearRequested = () => {
    this.setState(prevState => ({
      dropDown: {
        ...prevState.dropDown,
        suggestions: []
      }
    }))
  }

  onAuditSave = key => {
    const { user } = this.props
    const comId = this.props.nav4
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if (auditLogs.length && comId) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Client Complaints"
        log.superVisorSubSection = `${
          key === "keyDetails"
            ? "key details"
            : key === "complainantDetails"
            ? "Complainant’s name and address"
            : "Associated person(s)"
        }`
      })
      putCmplDetailsAuditLogs(comId, auditLogs, res => {
        if (res && res.status === 200) {
          this.setState({
            auditLogs: (res.data && res.data.auditLogs) || []
          })
        }
      })
      const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      this.props.updateAuditLog(remainLogs)
    }
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    let email = ""
    let phone = ""
    let address = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    if (
      user &&
      Array.isArray(user.userAddresses) &&
      user.userAddresses.length
    ) {
      address = `${user.userAddresses[0].addressLine1},${
        user.userAddresses[0].addressLine2
      }`
    }

    this.props.addAuditLog({
      userName: this.props.user.userFirstName,
      log: `In General admin select ${user.name} person`,
      date: new Date(),
      key
    })
    callback({
      status: true,
      object: {
        userId: user.id,
        userEntityId: user.entityId,
        userEntityName: user.userFirmName,
        userFirstName: user.userFirstName,
        userMiddleName: user.userMiddleName,
        userLastName: user.userLastName,
        userPrimaryEmailId: email,
        userPrimaryPhone: phone,
        userPrimaryAddress: address
      }
    })
  }

  onCmplAssocPersonDetailsSave = (type, item, callback) => {
    console.log(type, item)
    const comId = this.props.nav4
    if (comId) {
      putComplaintDetails(type, comId, item, res => {
        if (res && res.status === 200) {
          toast(
            `Added Associated person(s) identified in the complaint successfully`,
            { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
          )
          this.onAuditSave(type)
          callback({
            status: true,
            list: (res.data && res.data[type]) || []
          })
        } else {
          toast("Something went wrong!", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
          callback({
            status: false
          })
        }
      })
    }
  }

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    const comId = this.props.nav4
    pullCmplAssocPersonDetails(type, comId, removeId, res => {
      if (res && res.status === 200) {
        toast("Removed Associated person(s) successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    const comId = this.props.nav4
    putsvCmplDocuments("", comId, docs, res => {
      if (res && res.status === 200) {
        toast("Documents has been updated!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        callback({
          status: true,
          documentsList: (res.data && res.data.complaintDocuments) || []
        })
        this.onAuditSave("documents")
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    } else {
      document = {
        ...doc
      }
      type = "updateMeta"
    }
    const comId = this.props.nav4
    putsvCmplDocuments(`details=${type}`, comId, document, res => {
      if (res && res.status === 200) {
        toast("Document status has been updated!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.onAuditSave("document")
        if (name) {
          callback({
            status: true
          })
        } else {
          callback({
            status: true,
            documentsList: (res.data && res.data.complaintDocuments) || []
          })
        }
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = async (documentId, callback) => {
    const comId = this.props.nav4
    const res = await pullsvCmplDocuments(`?docId=${documentId}&comId=${comId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.props.submitAuditLogs(this.props.nav2)
      callback({
        status: true,
        documentsList: (res.data && res.data.complaintDocuments) || []
      })
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  }

  render() {
    const {
      isLoading,
      complaint,
      auditLogs,
      documentsList,
      dropDown,
      tables,
      errorMessages,
      isSaveDisabled
    } = this.state
    const { svControls } = this.props
    const comId = this.props.nav4
    let userAddresses = []
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Client Complaints"
    }
    if (
      complaint &&
      complaint.complainantDetails &&
      complaint.complainantDetails.userId &&
      dropDown &&
      dropDown.allSuggestions.length
    ) {
      const user =
        dropDown.allSuggestions.find(
          u => u._id === complaint.complainantDetails.userId
        ) || {}
      userAddresses =
        user && user.userAddresses
          ? user.userAddresses.map(addr => ({
              ...addr,
              id: addr._id,
              name: addr.addressName
            }))
          : []
    }

    // const loading = () => <Loader />

    if (isLoading) {
      return <Loader/>
    }
    return (
      <div>
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Key Detail"
              >
                {activeAccordions.includes(0) && (
                  <KeyDetail
                    dropDown={dropDown}
                    item={complaint || {}}
                    canSupervisorEdit={svControls.canSupervisorEdit}
                    errors={errorMessages.keyDetails || {}}
                    onAssignedSelect={this.onAssignedSelect}
                    onChangeItem={this.onChangeItem}
                    category="complaint"
                    onBlur={this.onBlur}
                    onSave={() => this.onSave("keyDetails")}
                    isSaveDisabled={isSaveDisabled}
                    onCancel={() => this.onCancel("keyDetails")}
                  />
                )}
              </RatingSection>
              {comId ? (
                <div>
                  <RatingSection
                    onAccordion={() => onAccordion(1)}
                    title="Complainant's Name and Address"
                  >
                    {activeAccordions.includes(1) && (
                      <UserDetails
                        dropDown={dropDown}
                        item={complaint || {}}
                        canSupervisorEdit={svControls.canSupervisorEdit}
                        errors={errorMessages.complainantDetails || {}}
                        userAddresses={userAddresses}
                        onChangeItem={this.onChangeItem}
                        category="complaint"
                        onBlur={this.onBlur}
                        onSave={() => this.onSave("complainantDetails")}
                        onCancel={() => this.onCancel("complainantDetails")}
                        onClearRequested={this.onSuggestionsClearRequested}
                        onFetchRequested={this.onSuggestionsFetchRequested}
                        getSuggestionValue={this.getSuggestionValue}
                        onOrganizationSelect={this.onOrganizationSelect}
                        isSaveDisabled={isSaveDisabled}
                      />
                    )}
                  </RatingSection>
                  <EditableTable
                    {...this.props}
                    dropDown={dropDown}
                    tables={tables}
                    canSupervisorEdit={svControls.canSupervisorEdit}
                    onSave={this.onCmplAssocPersonDetailsSave}
                    onRemove={this.onRemove}
                    onParentChange={this.onDropDownChange}
                  />

                  <Documents
                    {...this.props}
                    onSave={this.onDocSave}
                    tranId={this.props.nav2}
                    title="Related documents"
                    pickCategory="LKUPDOCCATS"
                    pickSubCategory="LKUPCORRESPONDENCEDOCS"
                    onStatusChange={this.onStatusChange}
                    pickAction="LKUPDOCACTION"
                    pickType="LKUPCOMPLAINTDOCTYPE"
                    staticField={staticField}
                    documents={documentsList}
                    category="complaintDocuments"
                    contextType="SVCLIENTCOMPLAINTS"
                    tableStyle={{ fontSize: "smaller" }}
                    onDeleteDoc={this.onDeleteDoc}
                  />

                  <RatingSection
                    onAccordion={() => onAccordion(2)}
                    title="Activity Log"
                    style={{ overflowY: "scroll" }}
                  >
                    {activeAccordions.includes(2) && (
                      <Audit auditLogs={auditLogs || []} />
                    )}
                  </RatingSection>
                </div>
              ) : null}
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(ClientComplaintsDetails)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
