import React from "react"
import moment from "moment"
import {Link} from "react-router-dom"
import {
  fetchComplaintDetails
} from "../../../../../../StateManagement/actions/Supervisor/index"
import Loader from "Global/Loader"
import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import TableHeader from "../../../../../../GlobalComponents/TableHeader"
import CONST from "../../../../../../../globalutilities/consts"

const cols = [
  {name: "Client Name"},
  {name: "Description"},
  {name: "Received Date "},
  {name: "Last Updated"},
  {name: "Product Code <a href=\"../../../../../../../public/docs/G8-Complaints-Prod-Problem-Codes.pdf\" download target=\"_blank\"><i class=\"fas fa-info-circle\"/></a>"},
  {name: "Problem Code <a href=\"../../../../../../../public/docs/G8-Complaints-Prod-Problem-Codes.pdf\" download target=\"_blank\"><i class=\"fas fa-info-circle\"/></a>"},
  {name: "Details"},
  {name: "Status"}]

class ClientComplaintsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading:true,
      complainList: [],
      allCmplaints: [],
      searchText: "",
      notes: CONST.CAC.svComplaintDetails,
      jsonSheets: [],
      startXlDownload: false
    }
  }

  componentWillMount(){
    fetchComplaintDetails((complainList) => {
      complainList  = complainList.map(complain => ({
        ...complain,
        ...complain.keyDetails
      }))
      this.setState({
        complainList,
        allCmplaints: complainList,
        isLoading:false,
      })
    })
  }

  onSearchText = (e) => {
    const {allCmplaints} = this.state
    this.setState({
      [e.target.name]: e.target.value
    },() => {
      if(allCmplaints.length){
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const {searchText, allCmplaints} = this.state
    if(searchText) {
      const complainList = allCmplaints.filter(obj => ["finAdvisorEntityName", "complaintDescription", "lastUpdateDate", "dateReceived", "productCode", "problemCode", "complaintStatus"].some(key => {
        if(key === "dateReceived") {
          return moment(obj[key]).format("MM.DD.YYYY hh:mm A").toLowerCase().includes(searchText.toLowerCase())
        }
        return obj[key] && obj[key].toLowerCase().includes(searchText.toLowerCase())
      }))
      this.setState({
        complainList
      })
    }
  }

  pdfDownload = () => {
    const {complainList} = this.state
    const pdfComplainList = []
    complainList && complainList.forEach((item) => {
      pdfComplainList.push([
        item.finAdvisorEntityName || "--",
        item.keyDetails.complaintDescription || "--",
        item.keyDetails.dateReceived ? moment(item.keyDetails.dateReceived).format("MM.DD.YYYY") : "" || "--",
        item.lastUpdateDate ? moment(item.lastUpdateDate).format("MM.DD.YYYY") : "" || "--",
        item.keyDetails.productCode || "--",
        item.keyDetails.problemCode || "--",
        item.keyDetails.complaintStatus || "--"
      ])
    })
    const tableData = []
    if(pdfComplainList.length > 0){
      tableData.push({
        titles:["Complaints List"],
        description: "",
        headers: ["Client Name","Description","Received Date","Last Updated","Product Code","Problem Code","Status"],
        rows: pdfComplainList
      })
    }
    pdfTableDownload("", tableData, "complainList.pdf")
  }

  xlDownload = () => {
    const {complainList} = this.state

    const exlComplainList = []

    complainList && complainList.map((item) =>  {
      const data = {
        "Client Name": item.finAdvisorEntityName || "--",
        "Description": item.keyDetails.complaintDescription || "--",
        "Received Date": item.keyDetails.dateReceived ? moment(item.keyDetails.dateReceived).format("MM.DD.YYYY") : "" || "--",
        "Last Updated": item.lastUpdateDate ? moment(item.lastUpdateDate).format("MM.DD.YYYY") : "" || "--",
        "Product Code": item.keyDetails.productCode || "--",
        "Problem Code": item.keyDetails.problemCode || "--",
        "Status": item.keyDetails.complaintStatus || "--"
      }
      exlComplainList.push(data)
    })
    const jsonSheets = [ {
      name: "Participants",
      headers: ["Client Name","Description","Received Date","Last Updated","Product Code","Problem Code","Status"],
      data: exlComplainList,
    } ]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  render() {
    const {isLoading, complainList} = this.state
    const {svControls} = this.props
    const loading = () => <Loader/>

    if (isLoading) {
      return loading()
    }

    return (
      <div>
        {/* <ComplianceAction
          title="Compliance Action Center (CAC)"
          bodyTitle="Use the Compliance Action Center (CAC) for proactive compliance management. You can set up alerts, notifications and/or reminders
                  for requirements similar to those listed below. It is our best effort to have most rules set up to enable a headstart! You may choose to update
                  those based on your specific requirements. You may also choose to add new rules with immediate effect!"
          notes={this.state.notes}
        /> */}
        <p>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
          <a href="../../../../../../../public/docs/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
        </p><br/>
        <div className="columns">
          <div className="column">
            <p className="control has-icons-left">
              <input className="input is-small is-link" type="text" placeholder="search" name="searchText" onChange={this.onSearchText}/>
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="field is-grouped">
              <div className="control" title="Excel" onClick={this.xlDownload}>
                <span className="has-text-link">
                  <i className="far fa-2x fa-file-excel has-text-link" />
                </span>
              </div>
              <div style={{ display: "none" }}>
                <ExcelSaverMulti label="XML" startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
              </div>
              <div className="control" title="PDF" onClick={this.pdfDownload}>
                <span className="has-text-link">
                  <i className="far fa-2x fa-file-pdf has-text-link" />
                </span>
              </div>
            </div>
          </div>
          {
            svControls.canSupervisorEdit ?
              <div className="column">
                <Link className="button is-link is-small" to="/compliance/cmp-sup-complaints/details">Add a
                  Complaint</Link>
                {/* <a className="button is-link is-small" onClick={() => this.props.identifyComponentToRender("Compliance Detail")}>Add a Complaint</a> */}
              </div>
              : null
          }
        </div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <TableHeader cols={cols}/>
          <tbody>
            {
              complainList.map((complain,i) => (
                <tr key={i}>
                  <td className="emmaTablesTd" style={{maxWidth: 300}}>
                    <Link className="text-overflow" to={`/clients-propects/${complain.finAdvisorEntityId}/entity`}>{complain.finAdvisorEntityName || ""}</Link>
                  </td>
                  <td className="emmaTablesTd" style={{maxWidth: 300}}>
                    <p>{complain.keyDetails.complaintDescription || "---"}</p>
                  </td>
                  <td className="emmaTablesTd" onClick={() => this.props.history.push(`/compliance/cmp-sup-complaints/details/${complain._id}`)}>
                    <p>{complain.keyDetails.dateReceived ? moment(complain.keyDetails.dateReceived).format("MM.DD.YYYY") : "" }</p>
                  </td>
                  <td className="emmaTablesTd">
                    <p>{complain.lastUpdateDate ? moment(complain.lastUpdateDate).format("MM.DD.YYYY") : "" }</p>
                  </td>
                  <td className="emmaTablesTd">
                    <p>{complain.keyDetails.productCode || ""}</p>
                  </td>
                  <td className="emmaTablesTd">
                    <p>{complain.keyDetails.problemCode || ""}</p>
                  </td>
                  <td className="emmaTablesTd">
                    <Link className="text-overflow" to={`/compliance/cmp-sup-complaints/details/${complain._id}`}>Details</Link>
                  </td>
                  <td className="emmaTablesTd">
                    <p>{complain.keyDetails.complaintStatus || ""}</p>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    )
  }
}

export default ClientComplaintsList
