import React, { Component } from "react"
import cloneDeep from "clone-deep"
import {toast} from "react-toastify";
import { connect} from "react-redux"
import EditableTable from "../../../../../../GlobalComponents/EditableTable"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import {PeopleQualifiedValidate, PeopleDesignateValidate} from "../../Validation/GeneralAdminValidation"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import Audit from "../../../../../../GlobalComponents/Audit"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Loader from "../../../../../../GlobalComponents/Loader"
import CONST from "../../../../../../../globalutilities/consts"
import {fetchAssigned} from "../../../../../../StateManagement/actions/CreateTransaction";
import {
  fetchDesignateDetails, putGenAdmin, putGenAuditLogs, pullGenAdmin
} from "../../../../../../StateManagement/actions/Supervisor";
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs";

class People extends Component {
  constructor(props) {
    super(props)

    this.state = { accordions:[1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false})),
      designateFirmContacts: [CONST.SuperVisor.gaDesignateFirmContacts],
      qualifiedAssPersons: [CONST.SuperVisor.gaQualifiedAssPersons],
      isEditable: {},
      tables: [
        {
          name:"Designate Firm Contacts",
          key: "gaDesignateFirmContacts",
          row: cloneDeep(CONST.SuperVisor.gaDesignateFirmContacts) || {},
          header: [
            {name: "Person<span class='has-text-danger'>*</span>"},
            /*{name: "First Name"},
            {name: "Last Name"},*/
            {name: "Email<span class='has-text-danger'>*</span>"},
            {name: "Phone<span class='has-text-danger'>*</span>"},
            {name: "Address<span class='has-text-danger'>*</span>"},
            {name: "Contact Type<span class='has-text-danger'>*</span>"},
            {name: "Action"}
          ],
          list: [],
          tbody: [
            {name: "userId", type: "dropdown", labelName: "userFirstName", dropdownKey: "users"},
            /*{name: "userFirstName", placeholder: "First Name", type: "text"},
            {name: "userLastName", placeholder: "Last Name", type: "text"},*/
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "userAddress", placeholder: "Address", type: "text"},
            {name: "userContactType", placeholder: "Contact Type", type: "select"},
            {name: "action"},
          ],
          handleError: (payload) => PeopleDesignateValidate(payload),
          children: this.renderChildren()
        },
        {
          name:"Qualified Associated Persons",
          key: "gaQualifiedAssPersons",
          header: [
            {name: "Person<span class='has-text-danger'>*</span>"},
            /*{name: "First Name"},
            {name: "Last Name"},*/
            {name: "Email<span class='has-text-danger'>*</span>"},
            {name: "Phone<span class='has-text-danger'>*</span>"},
            {name: "Professional Fee Paid on<span class='has-text-danger'>*</span>"},
            {name: "Series 50 Passed On<span class='has-text-danger'>*</span>"},
            {name: "Series 50 Valid Till<span class='has-text-danger'>*</span>"},
            {name: "Action"}
          ],
          row:  cloneDeep(CONST.SuperVisor.gaQualifiedAssPersons) || {},
          list: [],
          tbody: [
            {name: "userId", type: "dropdown", labelName: "userFirstName", dropdownKey: "users"},
            /*{name: "userFirstName", placeholder: "First Name", type: "text"},
            {name: "userLastName", placeholder: "Last Name", type: "text"},*/
            {name: "userPrimaryEmailId", placeholder: "Email", type: "text"},
            {name: "userPrimaryPhone", placeholder: "Phone", type: "phone"},
            {name: "profFeePaidOn", placeholder: "Address", type: "date"},
            {name: "series50PassDate", placeholder: "Contact Type", type: "date"},
            {name: "series50ValidityEndDate", placeholder: "Contact Type", type: "date"},
            {name: "action"},
          ],
          // handleError: (payload, minDate) => PeopleQualifiedValidate(payload, minDate)
        },
      ],
      notes: CONST.CAC.genAdmin,
      dropDown:{},
      usersList: [],
      loading: true,
    }
  }

  async componentWillMount() {
    const {tables} = this.state
    const {svControls} = this.props
    console.log("===================>", svControls.canSupervisorEdit)
    let result = [] //await getPicklistByPicklistName(["LKUPCONDUCTVIOLATION"])
    result = (result.length && result[1]) || {}
    fetchDesignateDetails((res) => {
      !svControls.canSupervisorEdit ? tables.forEach(t =>{
        t.header.splice(-1,1)
      }) : null
      tables.forEach(tbl => {
        tbl.list = (res && res[tbl.key]) || []
      })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          userContactType: ["Designated contact A-12", "Designated contact EMMA", "Designated contact SEC", "Designated contact FINRA",
            "Other (please specify)"],
        },
        loading: false,
        tables,
        auditLogs: (res && res.auditLogs) || [],
        transaction: res,
      },() => {
        this.getUsers()
      })
    })
  }

  async componentDidMount(){
    const accordionInitiate = [1,2,3,4,5].map((r) => ({accIndex:r,accOpen:false}))
    console.log("ACCINITIAL STATE", accordionInitiate)
    this.setState({
      accordions:accordionInitiate,
      isLoading:false
    })
  }

  renderChildren = () => {
    return (
      <div className="columns">
        <div className="column">
          <p className="multiExpTblVal">
            Municipal advisor must designate firm contacts via Form A-12 and is required to amend Form A-12 to update
            the designated
            contacts as changes occur. Use the
            <a href="cacMonitor.html" target="new">Compliance Action Center</a> to
            set up reminders/alerts/notifications.
          </p>
        </div>
      </div>
    )
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        },
        loading: false,
      })
    })
  }

  onRemove = (type, removeId, callback) => {
    console.log(type, removeId)
    pullGenAdmin(type, removeId, (res) => {
      if(res && res.status === 200) {
        toast(`Removed ${type === "gaDesignateFirmContacts" ? "Designate Firm Contacts" : "Qualified Associated Persons"} successfully`,{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onSave = (type, item, callback) => {
    console.log(type, item)
    putGenAdmin(type, item, (res)=> {
      if (res && res.status === 200) {
        toast(`Added ${type === "gaDesignateFirmContacts" ? "Designate Firm Contacts" : "Qualified Associated Persons"} successfully`,{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    let email = ""
    let phone = ""
    let userAdd = ""
    console.log("=================================>", user)
    if (key === "gaDesignateFirmContacts") {

      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }

      if(user && Array.isArray(user.userAddresses) && user.userAddresses.length) {
        userAdd = user.userAddresses.find(e => e.primaryAddress)
        userAdd = userAdd ? userAdd.userAddresses.addressLine1 :user.userAddresses[0].addressLine1
      }

      // this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
      callback({
        status: true,
        object: {
          userId: user.id,
          userFirstName: `${user.userFirstName} ${user.userLastName}`,
          userLastName: user.userLastName,
          userPrimaryEmailId: email,
          userPrimaryPhone: phone,
          userAddress: userAdd
        },
      })
    } else {
      if(user && Array.isArray(user.userEmails) && user.userEmails.length) {
        email = user.userEmails.filter(e => e.emailPrimary)
        email = email.length ? email[0].emailId : user.userEmails[0].emailId
      }
      if(user && Array.isArray(user.userPhone) && user.userPhone.length) {
        phone = user.userPhone.find(e => e.phonePrimary)
        phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
      }
      this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In General admin select ${user.name} person`, date: new Date(), key })
      callback({
        status: true,
        object: {
          userId: user.id,
          userFirstName: `${user.userFirstName} ${user.userLastName}`,
          userLastName: user.userLastName,
          userPrimaryEmailId: email,
          userPrimaryPhone: phone,
        },
      })
    }


  }

  onAuditSave = (key) => {
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "General Admin"
        log.superVisorSubSection = `${key === "gaDesignateFirmContacts" ? "Designate Firm Contacts" : "Qualified Associated Persons"}`
      })
      putGenAuditLogs(auditLogs, (res)=> {
        if(res && res.status === 200) {
          this.setState({
            auditLogs: (res.data && res.data.auditLogs) || []
          })
        }
      })
      const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      this.props.updateAuditLog(remainLogs)
    }
  }

  renderKeyDetails() {
    const { dropDown, auditLogs, transaction} = this.state
    const {svControls} = this.props
    console.log("===========canEdit==========>", svControls.canSupervisorEdit)
    const tables = this.state.tables.map(item => ({
      ...item,
      handleError:  item.key === "gaDesignateFirmContacts" ? (payload) => PeopleDesignateValidate(payload) : (payload, minDate) => PeopleQualifiedValidate(payload, minDate)
    }))

    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div>
          <Accordion multiple
            activeItem={[0]}
            boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                {/*<ComplianceAction
                  title="Compliance Action Center (CAC)"
                  bodyTitle="Use the Compliance Action Center (CAC) for proactive compliance management. You can set up alerts, notifications and/or reminders
                  for requirements similar to those listed below. It is our best effort to have most rules set up to enable a headstart! You may choose to update
                  those based on your specific requirements. You may also choose to add new rules with immediate effect!"
                  notes={notes}
                />*/}
                <p><small>Rule G-44 based recommended sections in compliance policy and supervisory procedure document</small>
                  <a href="../../../../../../../public/docs/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf" download target="_blank"><i className="fas fa-info-circle"/></a>
                </p>
                <EditableTable {...this.props} canSupervisorEdit={svControls.canSupervisorEdit} transaction={transaction} tables={tables} dropDown={dropDown} onSave={this.onSave} onRemove={this.onRemove} onParentChange={this.onDropDownChange}/>
                <RatingSection onAccordion={() => onAccordion(2)} title="Activity Log" style={{overflowY: "scroll"}}>
                  {activeAccordions.includes(2) &&
                  <Audit auditLogs={auditLogs || []}/>
                  }
                </RatingSection>
              </div>
            }
          />
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }

  render() {
    const { accordions, isLoading } = this.state
    const loading = () => <Loader/>

    if (isLoading) {
      return loading()
    }
    return (
      accordions && <div>
        {this.renderKeyDetails(1)}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(People)
export default connect(mapStateToProps, null)(WrappedComponent)
