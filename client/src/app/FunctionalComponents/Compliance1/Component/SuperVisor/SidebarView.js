import React, { Component } from "react"

class SidebarView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //isSideBarOpen: props.isSideBarOpen,
      superVisorCategories: []
    }
  }

  componentWillMount() {
    const { superVisorCategories, selectedSuperVisorCategory } = this.props
    const activeIndex = superVisorCategories.findIndex(x => x.path === selectedSuperVisorCategory)
    const removedElement = superVisorCategories.splice(activeIndex, 1)[0]
    superVisorCategories.splice(0, 0, removedElement)
    this.setState({
      superVisorCategories
    })
  }

  // handleSideBarToggle = () => {
  //   this.setState({
  //     isSideBarOpen: !this.state.isSideBarOpen,
  //   })
  // }

  render() {

    const { selectedSuperVisorCategory, user, showSidebar } = this.props
    const { superVisorCategories } = this.state
    const iconClass = showSidebar ? "left" : "right"

    return showSidebar ? (

      <div className="column box is-one-quarter" style={{ overflowX: "hidden", overflowY: "hidden", width: 335, "marginTop": "20px", "marginLeft": "20px", "padding": "0px", borderRadius: "0px", height: "fit-content" }} >
        {/* <div className="facet-title">
           <p className="title innerPgTitle">{(user && user.firmName) || ""}</p>
           <span
             className={`fas fa-lg fa-angle-double-${iconClass}`}
             onClick={this.handleSideBarToggle}
           />
           <span>Hello</span>
        </div> */}
        <header className="card-header hero is-link">
          <p className="multiExpLbl has-text-white" style={{ justifyContent: "center", "padding": "15px" }}>System to <u>implement &amp; enforce</u> compliance policies &amp; supervisory procedures.</p>
        </header>
        <br />
        {
          superVisorCategories.map((data) => {
            const to = data.path === "cac" ? `/${data.path}/${data.sidebarPath}` : ""
            return(
              <div key={data.path} className="column">
                <a className={selectedSuperVisorCategory === data.path ? "button is-link is-fullwidth" : data.label ? "button is-fullwidth" : ""}
                  onClick={() => this.props.history.push(to || `/compliance/${data.path}/${data.sidebarPath}`)}>
                  <p>
                    <small>{data.label}</small>
                  </p>
                </a>
              </div>
            )
          })
        }
      </div>
    ) : (
      null)
    {/* <div>
        <span
          className={`fas fa-lg fa-angle-double-${iconClass}`}
          onClick={this.handleSideBarToggle}
        />

      </div>  */}
  }

}

export default SidebarView
