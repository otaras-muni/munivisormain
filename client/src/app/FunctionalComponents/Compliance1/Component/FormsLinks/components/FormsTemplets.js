import React from "react"
import Disclaimer from "../../../../../GlobalComponents/Disclaimer"
import RatingSection from "../../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../../GlobalComponents/Accordion"

class FormsTemplets extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }


  actionButtons = () => (
    <div className="field is-grouped">
      <div className="control">
        <button className="button is-link is-small">Add new form/template</button>
      </div>
    </div>
  )


  render() {

    return (
      <div id="main">

        <Accordion multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(2)} title="Search Forms &amp; Templates">
                {activeAccordions.includes(2) &&
                  <div className="accordion-body" >
                    <div className="accordion-content" style={{ padding: 0 }}>

                      <div className="accordion-body">
                        <div className="accordion-content">

                          <div className="columns">
                            <div className="column">
                              <p className="control has-icons-left">
                                <input className="input is-small is-link" type="text" placeholder="search" />
                                <span className="icon is-left has-background-dark">
                                  <i className="fas fa-search" />
                                </span>
                              </p>
                            </div>
                          </div>
                          <div className="overflow-auto">
                            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                              <thead>
                                <tr>
                                  <th>
                                    <p className="multiExpLbl ">Form Name</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Filename</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Category</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Reviewer/Approver</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Last Updated</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Last Reviewed</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Status</p>
                                  </th>
                                  <th>
                                    <p className="multiExpLbl ">Action</p>
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td className="multiExpTblVal">
                                    Form A-12
                              </td>
                                  <td className="multiExpTblVal">A-12-2018.pdf</td>
                                  <td className="multiExpTblVal">
                                    MSRB
                              </td>
                                  <td className="multiExpTblVal">

                                  </td>
                                  <td className="multiExpTblVal">

                                  </td>
                                  <td className="multiExpTblVal">
                                    4.5.2018
                              </td>
                                  <td className="multiExpTblVal">
                                    OK to use
                              </td>
                                  <td className="multiExpTblVal">
                                    <div className="field is-grouped">
                                      <div className="control">
                                        <a href="">
                                          <span className="has-text-link">
                                            <i className="fas fa-pencil-alt" />
                                          </span>
                                        </a>
                                      </div>
                                      <div className="control">
                                        <a href="">
                                          <span className="has-text-link">
                                            <i className="fas fa-download" />
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td className="multiExpTblVal">
                                    Form MA-I
                              </td>
                                  <td className="multiExpTblVal">MA-I-2018.pdf</td>
                                  <td className="multiExpTblVal">
                                    SEC
                              </td>
                                  <td className="multiExpTblVal">

                                  </td>
                                  <td className="multiExpTblVal">

                                  </td>
                                  <td className="multiExpTblVal">
                                    4.5.2018
                              </td>
                                  <td className="multiExpTblVal">
                                    OK to use
                              </td>
                                  <td className="multiExpTblVal">
                                    <div className="field is-grouped">
                                      <div className="control">
                                        <a href="">
                                          <span className="has-text-link">
                                            <i className="fas fa-pencil-alt" />
                                          </span>
                                        </a>
                                      </div>
                                      <div className="control">
                                        <a href="">
                                          <span className="has-text-link">
                                            <i className="fas fa-download" />
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td className="multiExpTblVal">
                                    Template: Compliance policies &amp; procedures
                              </td>
                                  <td className="multiExpTblVal">WCP-WSP.docx</td>
                                  <td className="multiExpTblVal">
                                    Compliance Officer
                              </td>
                                  <td className="multiExpTblVal">
                                    Alex Rancho
                              </td>
                                  <td className="multiExpTblVal">
                                    3.30.2018
                              </td>
                                  <td className="multiExpTblVal">
                                    4.5.2018
                              </td>
                                  <td className="multiExpTblVal">
                                    Under Review
                              </td>
                                  <td className="multiExpTblVal">
                                    <div className="field is-grouped">
                                      <div className="control">
                                        <a href="">
                                          <span className="has-text-link">
                                            <i className="fas fa-pencil-alt" />
                                          </span>
                                        </a>
                                      </div>
                                      <div className="control">
                                        <a href="">
                                          <span className="has-text-link">
                                            <i className="fas fa-download" />
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>

                        </div>
                      </div>

                    </div>
                  </div>

                }
              </RatingSection>
            </div>
          }
        />

        <Accordion multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(2)} title="Manage Form / Template" actionButtons={this.actionButtons} >
                {activeAccordions.includes(2) &&
                  <div className="accordion-body">
                    <div className="accordion-content">

                      <div className="columns">
                        <div className="column">
                          <p className="multiExpLbl">Form or Template Name</p>
                          <input className="input is-small is-link" type="text"
                            placeholder="Template: Compliance policies &amp; procedures" />
                        </div>
                        <div className="column">
                          <p className="multiExpLbl">Filename</p>
                          <div className="field is-grouped-left">
                            <div className="control">
                              <p className="multiExpTblVal">WCP-WSP.docx</p>
                            </div>
                            <div className="control">
                              <div className="file is-small is-link">
                                <label className="file-label">
                                  <input className="file-input" type="file" name="resume" />
                                  <span className="file-cta">
                                    <span className="file-icon">
                                      <i className="fas fa-upload" />
                                    </span>
                                    <span className="file-label">
                                      New version…
                                     </span>
                                  </span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="column">
                          <p className="multiExpLbl ">Category</p>
                          <div className="select is-small is-fullwidth is-link">
                            <select>
                              <option value="" disabled="">Pick...</option>
                              <option>LKUPFORMSRCTYPE</option>
                              <option selected="">Compliance Officer</option>
                              <option>MSRB</option>
                              <option>SEC</option>
                              <option>FINRA</option>
                              <option>SIFMA</option>
                              <option>NFMA</option>
                              <option>NAMA</option>
                              <option>GFOA</option>
                              <option>Internal</option>
                              <option>Other (please specify)</option>
                            </select>
                          </div>
                        </div>
                        <div className="column">
                          <p className="multiExpLbl">Other Category (please specify)</p>
                          <input className="input is-small is-link" type="text" placeholder="Other Source Type" />
                        </div>
                      </div>

                      <div className="columns">
                        <div className="column">
                          <p className="multiExpLbl ">Status (What do you want to do?)</p>
                          <div className="select is-small is-link">
                            <select>
                              <option value="" disabled="">Action</option>
                              <option>LKUPWSPDOCACTION</option>
                              <option>Mark as archived</option>
                              <option>Mark as work in progress</option>
                              <option>Send for review...</option>
                              <option selected="">Mark as under review</option>
                              <option>Reviewed and OK</option>
                              <option>Mark as baselined (OK to use)</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div className="columns">
                        <div className="column">
                          <p className="multiExpLbl">Would you like to notify someone about your action? If yes, please choose
                            recipients
                            below..
                           </p>
                          <input className="input is-small is-link margin-topTen" type="text" placeholder="Accountable owner to complete any action" />
                          <input className="input is-small is-link margin-topTen" type="text" placeholder="TO LIST: multi-select (from CRM)" />
                          <input className="input is-small is-link margin-topTen" type="text" placeholder="CC LIST: multi-select (from CRM)" />
                          <input className="input is-small is-link margin-topTen" type="text" placeholder="Subject (limit 120 characters)" />
                        </div>
                      </div>
                      <div className="columns">
                        <div className="column">
                          <p className="multiExpLbl">Reference rule</p>
                          <input className="input is-small is-link topTen" type="text" placeholder="MSRB Rule A-12(j) and (k)" />
                        </div>
                      </div>

                      <div className="columns">
                        <div className="column is-full">
                          <div className="field">
                            <p className="multiExpLbl ">
                            </p><p className="multiExpLbl "
                              title="Notes and instructions by transaction participants">Notes/Instructions</p>
                            <p />
                            <div className="control">
                              <textarea className="textarea is-link" placeholder="" />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="columns">
                        <div className="column is-full">
                          <div className="field is-grouped-center">
                            <div className="control">
                              <button className="button is-link">Save</button>
                            </div>
                            <div className="control">
                              <button className="button is-light">Cancel</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <hr />

                      <div className="columns">
                        <div className="column is-right">
                          <p className="multiExpLbl">Activity Log</p>
                        </div>
                      </div>
                      <div className="columns">
                        <div className="column is-full">
                          <p className="dashExpLbl">Willy updated 6.8.2018 10:13</p>
                          <p className="multiExpTblVal">Notes...</p>
                        </div>
                      </div>
                      <div className="columns">
                        <div className="column is-full">
                          <p className="dashExpLbl">Jill Smith updated 6.8.2018 10:10</p>
                          <p className="multiExpTblVal">Notes...</p>
                        </div>
                      </div>
                      <div className="columns">
                        <div className="column is-full">
                          <p className="dashExpLbl">Shawn Smith created 6.7.2018 12:12</p>
                          <p className="multiExpTblVal">Notes...</p>
                        </div>
                      </div>

                    </div>
                  </div>
                }
              </RatingSection>
            </div>
          }
        />
        <hr />
        <Disclaimer />

      </div>
    )
  }
}

export default FormsTemplets
