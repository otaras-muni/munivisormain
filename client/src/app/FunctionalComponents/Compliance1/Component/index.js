import React from "react"
import { connect } from "react-redux"
import { Link, NavLink } from "react-router-dom"
import { activeStyle } from "../../../../globalutilities/consts"
import { getAllTenantUserDetails } from "../../../../globalutilities/helpers"
import { checkSupervisorControls } from "../../../StateManagement/actions/CreateTransaction"
import Complete from "./SuperVisor/PoliticalContribution/PoliticalStatus/Complete"
import GiftsComplete from "./SuperVisor/GiftsAndGrautities/Questions/Complete"
import SuperVisor from "./SuperVisor"
import PoliciesProcedures from "./PoliciesProcedures"
import FormsLinks from "./FormsLinks"
import Loader from "../../../GlobalComponents/Loader"
import CheckStepTwo from "./SuperVisor/PoliticalContribution/PoliticalStatus/CheckStepTwo"
import CheckStepOne from "./SuperVisor/PoliticalContribution/PoliticalStatus/CheckStepOne"

class ComplianceView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      TABS: [
        { path: "supervisor", label: "SuperVisor" },
        { path: "comp-policies-procs", label: "Policies & Procedures" },
        // { path: "formsLinks", label: "Forms & Links" }
      ],
      supervisor: [
        "cmp-sup-general",
        "cmp-sup-prof",
        "cmp-sup-busconduct",
        "cmp-sup-gifts",
        "cmp-sup-political",
        /* "cmp-sup-nonsolicit", */ "cmp-sup-complaints",
        "cmp-sup-compobligations"
      ],
      politicalSub: [
        "complete",
        "gifts-complete",
        "check-step-one",
        "check-step-two"
      ],
      svControls: {},
      participants: [],
      loading: true,
      tiles: [
        { path: "cmp-sup-general", label: "Genral adminstration Activities" },
        { path: "cmp-sup-prof", label: "Prfessional Qualifications" },
        { path: "cmp-sup-busconduct", label: "Business Conduct Activities" },
        { path: "cmp-sup-gifts", label: "Gifts and Gratuities" },
        {
          path: "cmp-sup-political",
          label: "Political Contributions and Prohibitions"
        },
        { path: "cmp-sup-complaints", label: "Client Complaints" },
        {
          path: "cmp-sup-compobligations",
          label: "Supervisory and Compliance Obligations"
        }
      ]
    }
  }

  componentWillMount = async () => {
    const { option } = this.props
    const svControls = await checkSupervisorControls()
    /* svControls.canSupervisorEdit = svControls.supervisor && svControls.edit */
    svControls.canSupervisorEdit = true
    console.log("=================== svControls =======>", svControls)
    this.setState(
      {
        svControls,
        loading: false
      },
      () => this.onParticipantsRefresh()
    )
    if (!option) {
      this.props.history.push("/compliance/supervisor")
    }
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await getAllTenantUserDetails()
    })
  }

  renderTabs = (tabs, option) =>
    tabs.map(t => {
      console.log("The selected option is", option)
      return (
        <li
          key={t.path}
          className={t.path === option ? "is-active" : "inactive-tab"}
        >
          <NavLink to={`/compliance/${t.path}`} activeStyle={activeStyle}>
            {t.label}
          </NavLink>
        </li>
      )
    })

  /* renderTabs = (tabs, option) => tabs.map(t =>
    <li key={t.path} className={option === t.path ? "is-active" : "inactive-tab"}>
      <NavLink activeStyle={activeStyle} to={`/compliance/${t.path}`}>{t.label}</NavLink>
    </li>
  ) */

  renderViewSelection = (compId, option) => (
    <div className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.TABS, compId, option)}</ul>
    </div>
  )

  renderSelectedView = option => {
    switch (option) {
      case "supervisor":
      case "cmp-sup-general":
      case "cmp-sup-prof":
      case "cmp-sup-busconduct":
      case "cmp-sup-gifts":
      case "cmp-sup-political":
      /*  case "cmp-sup-nonsolicit" : */
      case "cmp-sup-complaints":
      case "cmp-sup-compobligations":
        return (
          <SuperVisor
            {...this.props}
            onParticipantsRefresh={this.onParticipantsRefresh}
            participants={this.state.participants}
            svControls={this.state.svControls}
          />
        )
      case "gifts-complete":
        return (
          <GiftsComplete
            {...this.props}
            onParticipantsRefresh={this.onParticipantsRefresh}
            participants={this.state.participants}
            svControls={this.state.svControls}
          />
        )
      case "complete":
        return <Complete svControls={this.state.svControls} />
      case "check-step-one":
        return <CheckStepOne />
      case "check-step-two":
        return <CheckStepTwo />
      case "comp-policies-procs":
        return (
          <PoliciesProcedures
            {...this.props}
            onParticipantsRefresh={this.onParticipantsRefresh}
            participants={this.state.participants}
            svControls={this.state.svControls}
          />
        )
      case "formsLinks":
        return <FormsLinks {...this.props} />
      default:
        return <p>{option}</p>
    }
  }

  render() {
    let { option } = this.props
    const index = this.state.supervisor.indexOf(option)
    const poliSub = this.state.politicalSub.indexOf(option)
    option = index !== -1 ? "supervisor" : option
    const activeTab = this.state.tiles.find(
      option => option.path === this.props.nav2
    )
    const mainTab = this.state.TABS.find(
      option => option.path === this.props.nav2
    )

    if (this.state.loading) {
      return <Loader />
    }
    return (
      <div>
        {/* {
          poliSub === -1 ?
            <section className="hero is-small is-link">
             {/*  <div className="hero-body">
                <div className="columns is-vcentered" style={{marginTop: 23}}>
                  <div className="column">
                    <div className="subtitle">
                      <nav className="breadcrumb has-arrow-separator is-small is-right" aria-label="breadcrumbs">
                        <ul>
                          <li>
                            Compliance
                          </li>
                          {
                            !activeTab && mainTab ?
                              <li className="is-active">
                                {mainTab.label}
                              </li> : null
                          }
                          { activeTab ?
                            <li>
                              <a style={{cursor: "pointer"}} onClick = {() => this.props.history.push("/compliance/supervisor")}>
                                SuperVisOr
                              </a>
                            </li>: null}

                          { activeTab ?
                            <li className="is-active">
                              {activeTab.label}
                            </li>: null}

                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
              <div className="hero-foot">
                <div className="container">
                  {this.renderViewSelection(option)}
                </div>
              </div>
            </section>
            :null
        } */}
        {/* {poliSub === -1 ? (
          <div className="hero is-link">
            <div className="hero-foot hero-footer-padding">
              <div className="container">
                {this.renderViewSelection(option)}
              </div>
            </div>
          </div>
        ) : null} */}
        <section>{this.renderSelectedView(option)}</section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(
  mapStateToProps,
  null
)(ComplianceView)
