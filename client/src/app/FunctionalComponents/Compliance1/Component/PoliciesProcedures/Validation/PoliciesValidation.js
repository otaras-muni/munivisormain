import Joi from "joi-browser"

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().required().optional(),
  docSubCategory: Joi.string().required().optional(),
  docType: Joi.string().required(),
  docAWSFileLocation: Joi.string().required(),
  docFileName: Joi.string().required().optional(),
  lastReviewedOn: Joi.date().example(new Date("2016-01-01")).allow(null).optional(),
  lastUpdateBy: Joi.string().allow("").optional(),
  reviewer: Joi.string().allow("").optional(),
  subject: Joi.string().allow("").optional(),
  notes: Joi.string().allow("").optional(),
  reviewerRole: Joi.string().allow("").optional(),
  // docActions: Joi.object().keys({
  //  actionDate: Joi.string().required().optional(),
  //  actionType: Joi.string().required().optional(),
  // }),
  status: Joi.string().required(),
  history: Joi.array().optional(),
  auditLogs: Joi.array().optional(),
  docCommunication: Joi.object({
    owners: Joi.array().min(1).required(),
    toRecipients: Joi.array().optional(),
    ccRecipients: Joi.array().optional()
    // communicationSubject: Joi.string().required().optional(),
    // communicationNotes: Joi.string().required().optional()
  }),
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  _id: Joi.string().required().optional(),
  docReviewers: Joi.array().optional(),
  lastUpdatedDate: Joi.date().optional()
})

export const PoliciesValidation = inputTransDetail =>
  Joi.validate(inputTransDetail, docListSchema, {abortEarly: false, stripUnknown: false})
