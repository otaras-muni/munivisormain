import React from "react"
import {muniApiBaseURL, FRONTENDURL} from "GlobalUtils/consts"
import {getPicklistByPicklistName, getDocDetails} from "GlobalUtils/helpers"
import {connect} from "react-redux"
import cloneDeep from "lodash.clonedeep"
import moment from "moment"
import swal from "sweetalert"
import axios from "axios"
import {toast} from "react-toastify"
import * as qs from "query-string"
import {getS3FileGetURL} from "../../../../StateManagement/actions/index"
import Accordion from "../../../../GlobalComponents/Accordion"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, {ContextType} from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TableHeader from "../../../../GlobalComponents/TableHeader"
import {putPoliciesProcedures, getPoliciesProcedures, putPoliProAuditLogs, getSupervisor} from "../../../../StateManagement/actions/Supervisor/Supervisor"
import {PoliciesValidation} from "./Validation/PoliciesValidation"
import EstablishDocumentation from "./components/EstablishDocumentation"
import {checkSupervisorControls, fetchAssigned} from "../../../../StateManagement/actions/CreateTransaction"
import DocLink from "../../../docs/DocLink"
import {DocModal} from "../../../docs/DocModal"
import {sendComplianceEmail} from "../../../../StateManagement/actions/Transaction"
import DocModalDetails from "../../../docs/DocModalDetails"
import {pullPqDocument} from "../../../../StateManagement/actions/Supervisor"
import {PoliciesSendEmailModal} from "../../../../GlobalComponents/PoliciesSendEmailModal"
import {getToken, getFile, getAllTenantUserDetails} from "../../../../../globalutilities/helpers"
import PoliciesList from "./components/PoliciesList"

const col = [
  [
    {
      name: "Document Type"
    }, {
      name: "Filename"
    }, {
      name: "Reviewer/Approver"
    }, {
      name: "Reviewer/Approver Role"
    }, {
      name: "Created"
    }, {
      name: "Last Updated On"
    }, {
      name: "Last Reviewed On"
    }, {
      name: "Last Updated By"
    }, {
      name: "Status"
    }, {
      name: "Actions"
    }
  ],
  [
    {
      name: "Uploaded by"
    }, {
      name: "Filename"
    }, {
      name: "FileSize"
    }, {
      name: "upload date"
    }, {
      name: "Action"
    }
  ],
  [
    {
      name: "User Name"
    }, {
      name: "Firm Name"
    }, {
      name: "History"
    }, {
      name: "Date"
    }
  ]
]

class PoliciesProcedures extends React.Component {
  constructor(props) {
    super(props)
    const {user} = this.props
    this.state = {
      bucketName: CONST.bucketName,
      loading: true,
      isSaveDisabled: false,
      fileName: "",
      auditLogs: [],
      participants: [],
      errorMessages: {},
      documentsList: [],
      searchList: [],
      svControls: {},
      docCommunicationDetails: {
        ...cloneDeep(CONST.CREATE.DocCommunicationDetails),
        createdBy: (user && user.userId) || "",
        createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
      },
      category: "",
      dropDown: {
        category: [],
        subCategory: [],
        docType: [],
        docActions: []
      },
      showModal: false,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: ""
      },
      confirmAlert: CONST.confirmAlert
    }
  }

  async componentWillMount() {
    const queryString = qs.parse(location.search)
    this.supervisorControls()
    this.getUsers()
    this.getSuperviosrList()
    this.onParticipantsRefresh()
    getPoliciesProcedures(res => {
      this.setState({
        documentsList: (res && res.svPoliciesAndProcedures) || [],
        // auditLogs: (res && res.auditLogs) || []
      }, () => {
        if (this.state.documentsList && queryString && queryString.activityId) {
          const document = this
            .state
            .documentsList
            .filter(doc => doc._id === queryString.activityId)
          if (document && document.length) {
            if (document[0] && document[0].docAWSFileLocation) {
              this.getDocInfo(document[0].docAWSFileLocation, "duplicateFile")
            }
            document[0]
              .docCommunication
              .owners
              .forEach(item => {
                item.id = item.userId
                item.name = `${item.userFirstName} ${item.userLastName}`
              })
            document[0]
              .docCommunication
              .ccRecipients
              .forEach(item => {
                item.id = item.userId
                item.name = `${item.userFirstName} ${item.userLastName}`
              })
            document[0]
              .docCommunication
              .toRecipients
              .forEach(item => {
                item.id = item.userId
                item.name = `${item.userFirstName} ${item.userLastName}`
              })
            this.setState({
              docCommunicationDetails: document[0],
              auditLog: document[0].auditLogs,
              duplicateStatus: document[0] && document[0].status
            })
          } else {
            this
              .props
              .history
              .push("/dashboard")
          }
        }
      })
    })
  }

  async componentDidMount() {
    const {user} = this.props
    let result = await getPicklistByPicklistName(["LKUPWSPDOCTYPE", "LKUPWSPDOCACTION"])
    result = (result.length && result[1]) || {}
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        docType: result.LKUPWSPDOCTYPE || [],
        docActions: result.LKUPWSPDOCACTION || []
      },
      userName: (user && user.userFirstName) || "",
      loading: false
    })
    this.onSubNoteChange()
  }

  supervisorControls = async() => {
    const svControls = await checkSupervisorControls()
    this.setState({svControls})
  }

  getSuperviosrList = async() => {
    const supervisor = await getSupervisor()
    if (supervisor && supervisor.data) {
      supervisor && supervisor
        .data
        .forEach(item => {
          item.id = item._id || ""
          item.name = `${item.userFirstName} ${item.userLastName}` || ""
        })
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          supervisorList: (supervisor && supervisor.data) || []
        }
      })
    }
  }

  onChange = (item, index, category, change) => {
    const {userName} = this.state
    const items = this.state[category]
    items.splice(index, 1, item)
    this
      .props
      .addAuditLog({userName, log: `In ${category} ${change}`, date: new Date(), key: category})
    this.setState({[category]: items})
  }

  getBidBucket = (filename, docId, index) => {
    const {docCommunicationDetails} = this.state
    const {user} = this.props
    docCommunicationDetails.docAWSFileLocation = docId
    docCommunicationDetails.docFileName = filename
    this
      .props
      .addAuditLog({
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: docCommunicationDetails._id
          ? `Upload Version ${filename}`
          : `Upload New File ${filename}`,
        date: new Date(),
        key: "docCommunicationDetails"
      })
    this.getDocInfo(docId)
    this.setState({
      docCommunicationDetails
    }, () => this.onSubNoteChange())
  }

  onModalChange = (state, name) => {
    const {docCommunicationDetails, subNote} = this.state
    if (state && state.modalState === false) {
      if (state && state.modalState === false) {
        this
          .props
          .updateAuditLog([])
      }
    } else {
      const cc = docCommunicationDetails
        .docCommunication
        .ccRecipients
        .concat(docCommunicationDetails.docCommunication.owners)
      const sendEmail = cc.concat(docCommunicationDetails.docCommunication.toRecipients)
      const url = `${FRONTENDURL}/comp-polandproc?activityId=${docCommunicationDetails._id}`
      state.email.sendEmailTo = sendEmail
      if (docCommunicationDetails._id) {
        state.email.message = `${subNote.Message} <a href=${url} >${url}</a>`
      } else {
        state.email.message = `${subNote.Message}`
      }
      state.email.subject = subNote.Subject
      if (name === "message") {
        state = {
          email: {
            ...this.state.email,
            ...state
          }
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onSave = () => {
    const {docCommunicationDetails} = this.state
    const {user} = this.props
    delete docCommunicationDetails.docCommunication.communicationSubject
    delete docCommunicationDetails.docCommunication.communicationNotes
    const errors = PoliciesValidation(docCommunicationDetails)

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors
        .error
        .details
        .forEach(err => {
          errorMessages[err.context.key] = "Required" || err.message
        })
      console.log(errorMessages)
      this.setState({errorMessages})
      return
    }
    if (docCommunicationDetails && docCommunicationDetails.docFileName) {
      if (!docCommunicationDetails._id) {
        this
          .props
          .addAuditLog({userName: `${user.userFirstName} ${user.userLastName}`, log: "Create New Documents", date: new Date(), key: "docCommunicationDetails"})
      } else {
        this
          .props
          .addAuditLog({userName: `${user.userFirstName} ${user.userLastName}`, log: "Update Documents", date: new Date(), key: "docCommunicationDetails"})
      }
    }
    this.setState({
      isSaveDisabled: true,
      modalState: true,
      policies: "policies"
    }, () => this.onSubNoteChange())
    // }
  }

  onModalSave = () => {
    const {transaction, user} = this.props
    const {docCommunicationDetails, subNote, dropDown, duplicateStatus} = this.state
    const userName = `${user.userFirstName} ${user.userLastName}`
    const historyArray = []
    let duplicateHistory = {}
    const emailPayload = {
      tranId: (transaction && transaction._id) || "",
      type: "Policies & Procedures",
      sendEmailUserChoice: true,
      emailParams: {
        url: window
          .location
          .pathname
          .replace("/", ""),
        subject: "Document Action",
        ...this.state.email
      }
    }
    if (duplicateStatus !== docCommunicationDetails.status) {
      duplicateHistory = {
        userName,
        firmName: user && user.firmName,
        date: moment(new Date()).format("MM.DD.YYYY hh:mm A"),
        log: `Status Change To ${docCommunicationDetails.status}`
      }
      historyArray.push(duplicateHistory)
    }
    docCommunicationDetails.history = historyArray
      ? docCommunicationDetails.history !== undefined
        ? docCommunicationDetails
          .history
          .concat(historyArray)
        : historyArray
      : docCommunicationDetails.history
    if (docCommunicationDetails.status === "Reviewed and Approved") {
      const userId = user && user.userId
      const reviewerUser = dropDown && dropDown
        .supervisorList
        .filter(a => a.id === userId)
      docCommunicationDetails.lastReviewedOn = new Date()
      docCommunicationDetails.reviewer = userName
      docCommunicationDetails.reviewerRole = reviewerUser && reviewerUser[0].role
    }
    docCommunicationDetails.lastUpdatedDate = new Date()
    docCommunicationDetails.lastUpdateBy = userName
    // docCommunicationDetails.auditLogs = auditLog !== undefined  ?
    // auditLog.concat(this.props.auditLogs) : this.props.auditLogs
    this.setState(pre => ({
      modalState: !pre.modalState,
      loading: true
    }), async() => {
      putPoliciesProcedures(docCommunicationDetails, response => {
        if (response && response.status === 200) {
          if (!docCommunicationDetails._id) {
            const length = response.data.svPoliciesAndProcedures.length
            const url = `${FRONTENDURL}/comp-polandproc?activityId=${response.data && response.data.svPoliciesAndProcedures[length - 1]._id}`
            emailPayload.emailParams.message = `${subNote.Message} <a href=${url} >${url}</a>`
          }
          toast("Policies And Procedures add Succesfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS
          })
          // this.onAuditSave("docCommunicationDetails")
          this.setState({
            documentsList: (response.data && response.data.svPoliciesAndProcedures) || [],
            docCommunicationDetails: {
              ...cloneDeep(CONST.CREATE.DocCommunicationDetails),
              createdBy: (user && user.userId) || "",
              createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
            },
            duplicateFile: "",
            objectName: "",
            subNote: {},
            docTable: {},
            errorMessages: "",
            duplicateStatus: "",
            fileName: "",
            isSaveDisabled: false,
            loading: false
          }, async() => {
            await sendComplianceEmail(emailPayload)
            this
              .props
              .updateAuditLog([])
            this.onSubNoteChange()
          })
        } else {
          toast("Something went wrong!", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
        }
      })
    })
  }

  onCancel = () => {
    const {user} = this.props
    this.setState({
      docCommunicationDetails: {
        ...cloneDeep(CONST.CREATE.DocCommunicationDetails),
        createdBy: (user && user.userId) || "",
        createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
      },
      errorMessages: "",
      dataItems: ""
    }, () => this.onSubNoteChange())
  }

  downloadFile = () => {
    this
      .downloadAnchor
      .click()
  }

  handleDocDetails = res => {
    this.setState({
      showModal: !this.state.showModal,
      doc: res || {}
    })
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, res => {
      this.setState({
        ...res
      }, callback.bind(this, bucketName, fileName))
    })
  }

  onChangeItem = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  getUsers = () => {
    fetchAssigned(this.props.user.entityId, users => {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || []
        },
        loading: false
      })
    })
  }

  onChangeItems = (item, category) => {
    this.setState({
      [category]: item
    }, () => this.onSubNoteChange())
  }

  onNotesChange = state => {
    if (state && state.notes) {
      this.setState(prevState => ({
        subNote: {
          ...prevState.subNote,
          ...state
        }
      }))
    } else {
      this.setState(prevState => ({
        subNote: {
          ...prevState.subNote,
          ...state
        }
      }))
    }
  }

  onSubNoteChange = () => {
    const {user} = this.props
    const {docCommunicationDetails, fileName} = this.state
    const status = docCommunicationDetails.status || ""
    let message = {}
    const sub = status === "Reviewed and Approved"
      ? "Document reviewed and approved"
      : status === "Review in Progress"
        ? "Submitted document for approval"
        : status === "Send for Review"
          ? "Document status changed"
          : "New Document Uploaded"
    const note = status === "Reviewed and Approved"
      ? "has approved the compliance document. The status of the document is"
      : status === "Review in Progress"
        ? "is submitting the document for approval. The status of the document is"
        : status === "Send for Review"
          ? "Document status changed"
          : `uploaded a new document.
       The status of the document is`
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    if (docCommunicationDetails._id && fileName !== this.state.duplicateFile) {
      message = {
        Subject: `Category: Compliance, Sub Category: Policy & Procedures, Document Type: ${docCommunicationDetails.docType || ""} - New Version of the document uploaded`,
        Message: `User ${userName} uploaded a new version of the document. The status of the document is ${status || ""}. The URL is given below :`
      }
    } else {
      message = {
        Subject: `Category: Compliance, Sub Category: Policy & Procedures, Document Type: ${docCommunicationDetails.docType || ""} - ${sub || ""}`,
        Message: `User ${userName} ${note || ""} "${status || ""}". The URL is given below :`
      }
    }
    this.setState({subNote: message})
  }

  onBlur = (change, category) => {
    const {user} = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this
      .props
      .addAuditLog({userName, log: `${change}`, date: new Date(), key: category})
  }

  onSearchText = e => {
    const {documentsList} = this.state
    this.setState({
      [e.target.name]: e.target.value
    }, () => {
      if (documentsList.length) {
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const {searchText, documentsList} = this.state
    if (searchText) {
      const searchList = documentsList.filter(obj => [
        "docType",
        "docFileName",
        "docCategory",
        "docSubCategory",
        "LastUpdatedDate",
        "lastUpdatedDate"
      ].some(key => {
        if (key === "LastUpdatedDate" || key === "lastUpdatedDate") {
          return moment(obj[key])
            .format("MM.DD.YYYY hh:mm A")
            .toLowerCase()
            .includes(searchText.toLowerCase())
        }
        return obj[key]
          .toLowerCase()
          .includes(searchText.toLowerCase())
      }))
      this.setState({searchList})
    }
  }

  onAuditSave = key => {
    const auditLogs = this
      .props
      .auditLogs
      .filter(log => log.key === key)
    if (auditLogs.length) {
      auditLogs.forEach(log => {
        log.superVisorModule = "Policies & Procedures"
        log.superVisorSubSection = "Establish & Maintain Documentation"
      })
      putPoliProAuditLogs(auditLogs, res => {
        if (res && res.status === 200) {
          this.setState({
            auditLogs: (res.data && res.data.auditLogs) || []
          })
        }
      })
      const remainLogs = this
        .props
        .auditLogs
        .filter(log => log.key !== key)
      this
        .props
        .updateAuditLog(remainLogs)
    }
  }

  deleteDoc = _id => {
    console.log("_id : ", _id)
    this.setState(prevState => {
      const docIds = [...prevState.docIds].filter(e => e._id !== _id)
      return {docIds}
    })
  }

  onDeleteDoc = async document => {
    const {user} = this.props
    const {docsId} = this.state
    const res = await pullPqDocument(`?docId=${docsId || document._id}`)
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.onAuditSave("document")
      this.setState({
        status: true,
        documentsList: (res.data && res.data.svPoliciesAndProcedures) || [],
        docCommunicationDetails: {
          ...cloneDeep(CONST.CREATE.DocCommunicationDetails),
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        }
      })
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      this.setState({status: false})
    }
  }

  onEdit = doc => {
    if (doc && doc.docAWSFileLocation) {
      this.getDocInfo(doc.docAWSFileLocation, "duplicateFile")
    }
    doc
      .docCommunication
      .owners
      .forEach(item => {
        item.id = item.userId
        item.name = `${item.userFirstName} ${item.userLastName}`
      })
    doc
      .docCommunication
      .ccRecipients
      .forEach(item => {
        item.id = item.userId
        item.name = `${item.userFirstName} ${item.userLastName}`
      })
    doc
      .docCommunication
      .toRecipients
      .forEach(item => {
        item.id = item.userId
        item.name = `${item.userFirstName} ${item.userLastName}`
      })
    this.setState({
      docCommunicationDetails: doc,
      duplicateStatus: doc && doc.status,
      auditLog: doc && doc.auditLogs || [],
      errorMessages: {}
    }, () => this.onSubNoteChange())
  }

  getDocInfo = async(_id, duplicateFiles) => {
    const {docCommunicationDetails, fileName} = this.state
    const res = await getDocDetails(_id || this.state.docTable._id)
    console.log("res : ", res)
    const [error,
      doc] = res
    if (error) {
      this.setState({waiting: false, error})
    } else {
      const {contextId, contextType, tenantId, name} = doc
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      this.setState({
        docTable: doc, error: "", objectName, waiting: false,
        // fileName: name,
        duplicateFile: doc && doc.originalName
      }, () => {
        if (docCommunicationDetails && docCommunicationDetails.status === "Reviewed and Approved") {
          const file = fileName === ""
            ? this.state.duplicateFile
            : fileName
          if (this.state.duplicateFile !== file) {
            this.setState({
              docCommunicationDetails: {
                ...docCommunicationDetails,
                status: "Work in Progress"
              }
            })
          }
        }
        this.onSubNoteChange()
      })
    }
    if (duplicateFiles) {
      this.setState({
        fileName: doc && doc.originalName
      }, () => this.onSubNoteChange())
    }
  }

  onParticipantsRefresh = async() => {
    this.setState({participants: await getAllTenantUserDetails()})
  }

  onClickDownload = (versionId, name, fileName) => {
    console.log("in onClickDownload : ", versionId, name)
    const {docTable} = this.state
    const {contextId, contextType, tenantId} = docTable
    const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
    this.setState({
      versionId,
      objectName,
      fileNames: fileName
    }, this.downloadFile)
  }

  onClickDelete = (versionId, name, fileName) => {
    if (this.state.docTable) {
      const {docTable} = this.state
      const {contextId, contextType, tenantId} = docTable
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      this.setState({
        deleteVersion: versionId,
        objectName
      }, () => {
        if (this.state.deleteVersion) {
          const {confirmAlert} = this.state
          confirmAlert.text = `You want to delete this ${fileName} File?`
          swal(confirmAlert).then(willDelete => {
            if (this.state.deleteVersion) {
              if (willDelete) {
                this.deleteDocVersion(fileName)
              }
            }
          })
        }
      })
    }
  }

  deleteDocVersion(fileName) {
    const {user} = this.props
    const versionId = this.state.deleteVersion
    console.log("in onClickDelete : ", versionId)
    const {docTable, objectName, bucketName} = this.state
    if ((!docTable && !docTable.name) || !versionId) {
      this.setState({error: "No bucket/file name/versionId provided", deleteVersion: ""})
      return
    }
    const {contextId, contextType, tenantId} = docTable
    this.setState({waiting: true})
    let files
    if (versionId === "All") {
      files = docTable
        .meta
        .versions
        .map(e => ({Key: `${tenantId}/${contextType}/${contextType}_${contextId}/${e.name}`, VersionId: e.versionId}))
    } else {
      files = [
        {
          Key: objectName,
          VersionId: versionId
        }
      ]
    }
    axios.post(`${muniApiBaseURL}/s3/delete-s3-object`, {
      bucketName,
      files
    }, {
      headers: {
        "Authorization": getToken()
      }
    }).then(res => {
      console.log("res : ", res)
      if (res.error) {
        const error = `Error in deleting ${versionId}`
        this.setState({error, deleteVersion: ""})
      } else if (docTable.meta.versions.length > 1 && versionId !== "All") {
        axios.post(`${muniApiBaseURL}/docs/update-versions`, {
          _id: docTable._id,
          versions: [versionId],
          option: "remove"
        }, {
          headers: {
            "Authorization": getToken()
          }
        }).then(res => {
          console.log("version removed ok : ", res)
          this.setState({
            error: "",
            deleteVersion: "",
            waiting: false
          }, this.getDocInfo, this.props.addAuditLog({userName: `${user.userFirstName} ${user.userLastName}`, log: `Remove File ${fileName}`, date: new Date(), key: "docCommunicationDetails"}))
        }).catch(err => {
          console.log("err in deleting version : ", err)
          this.setState({error: "err in deleting version", deleteVersion: ""})
        })
      }
    }).catch(err => {
      console.log("err : ", err)
      this.setState({error: "Error!!", deleteVersion: "", waiting: false})
    })
  }

  render() {
    const {
      errorMessages,
      loading,
      dropDown,
      searchText,
      searchList,
      docCommunicationDetails,
      bucketName,
      modalState,
      email,
      doc,
      docTable,
      objectName,
      versionId,
      fileNames,
      svControls,
      duplicateFile,
      fileName,
      duplicateStatus,
      subNote,
      participants
    } = this.state
    const documentsList = searchText
      ? searchList
      : this.state.documentsList
    const {user} = this.props

    if (loading) {
      return <Loader/>
    }
    return (
      <div id="main">
        <section className="container has-text-centered">
          <p className="title is-small">{(user && user.firmName) || ""}</p>
          <p className="multiExpLbl">
            <u>Establish &amp; maintain</u> compliance policies &amp; supervisory procedures.</p>
        </section>
        <hr/>
        <PoliciesSendEmailModal
          isNotTransaction
          modalState={modalState}
          email={email}
          participants={participants}
          documentFlag
          onParticipantsRefresh={this.onParticipantsRefresh}
          onModalChange={this.onModalChange}
          onSave={this.onModalSave}/>
        <div>
          <Accordion
            multiple
            activeItem={[0, 1, 2, 3]}
            boxHidden
            render={({activeAccordions, onAccordion}) => (
              <div className="columns">
                <div className="column">
                  <RatingSection onAccordion={() => onAccordion(0)} title="Audit">
                    {activeAccordions.includes(0) && (
                      <div>
                        <div className="columns">
                          <div className="column">
                            <p className="control has-icons-left">
                              <input
                                className="input is-small is-link"
                                type="text"
                                placeholder="search"
                                name="searchText"
                                onChange={this.onSearchText}/>
                              <span className="icon is-left has-background-dark">
                                <i className="fas fa-search"/>
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="overflow-auto">
                          <PoliciesList
                            list={documentsList || []}
                            onEdit={this.onEdit}
                            onDeleteDoc={this.onDeleteDoc}
                          />
                          {/* <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                          <TableHeader cols={col[0]}/>
                          <tbody>
                            {documentsList.map((doc, i) => (
                              <tr key={i}>
                                <td className="multiExpTblVal">
                                  {doc.docType}
                                </td>
                                <td className="multiExpTblVal">
                                  <div
                                    className="field is-grouped"
                                    style={{
                                    justifyContent: "start"
                                  }}>
                                    <DocLink docId={doc.docAWSFileLocation}/>
                                    <DocModal
                                      onDeleteAll={() => this.onDeleteDoc(doc, i)}
                                      documentId={doc._id}
                                      selectedDocId={doc.docAWSFileLocation}/>
                                  </div>
                                </td>
                                <td className="multiExpTblVal">
                                  {doc.reviewer || "---"}
                                </td>
                                <td className="multiExpTblVal">
                                  {doc.reviewerRole || "---"}{" "}
                                </td>
                                <td>{doc.createdUserName || "---"}</td>
                                <td className="multiExpTblVal">
                                  {doc.lastUpdatedDate
                                    ? moment(doc.lastUpdatedDate).format("MM.DD.YYYY hh:mm A")
                                    : ""}
                                </td>
                                <td className="multiExpTblVal">
                                  {doc.lastReviewedOn
                                    ? moment(doc.lastReviewedOn).format("MM.DD.YYYY hh:mm A")
                                    : "" || "---"}
                                </td>
                                <td className="multiExpTblVal">
                                  {doc.lastUpdateBy || "---"}
                                </td>
                                <td className="multiExpTblVal">
                                  {doc.status || "---"}
                                </td>
                                <td>
                                  <span className="has-text-link">
                                    <a className="fas fa-pencil-alt" onClick={() => this.onEdit(doc)}/>
                                  </span>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table> */}
                        </div>
                      </div>
                    )}
                  </RatingSection>
                  <RatingSection
                    onAccordion={() => onAccordion(1)}
                    title="Establish & Maintain Documentation">
                    {activeAccordions.includes(1) && (
                      <div>
                        <EstablishDocumentation
                          user={user}
                          contextType={ContextType.supervisor.poliProducers}
                          dropDown={dropDown}
                          contextId={user.userId}
                          bucketName={bucketName}
                          getBidBucket={this.getBidBucket}
                          errors={errorMessages}
                          tenantId={user.entityId}
                          onFileAction={this.onFileAction}
                          item={docCommunicationDetails}
                          category="docCommunicationDetails"
                          docHeader={col[1]}
                          onClickDelete={this.onClickDelete}
                          onChangeItems={this.onChangeItems}
                          onNotesChange={this.onNotesChange}
                          onBlur={this.onBlur}
                          deleteDoc={this.onDeleteDoc}
                          docTable={docTable}
                          svControls={svControls}
                          duplicateFile={duplicateFile}
                          fileName={fileName}
                          subNote={subNote}
                          duplicateStatus={duplicateStatus}
                          onDownloadFile={this.onClickDownload}/>
                        <div className="columns is-centered">
                          <div className="field is-grouped">
                            <div className="column control">
                              {docCommunicationDetails && (
                                <button className="button is-link is-small" onClick={this.onSave}>
                                  Save
                                </button>
                              )}
                            </div>
                            <div className="column control">
                              <button className="button is-light is-small" onClick={this.onCancel}>
                                Cancel
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </RatingSection>
                  {docCommunicationDetails._id
                    ? <RatingSection
                      onAccordion={() => onAccordion(2)}
                      title="Status History"
                      style={{overflowY: "scroll"}}>
                      {activeAccordions.includes(2) && (
                        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                          <TableHeader cols={col[2]}/>
                          <tbody>
                            {docCommunicationDetails
                              .history
                              .map((log, i) => (
                                <tr key={i}>
                                  <td className="multiExpTblVal">{log.userName || "--"}</td>
                                  <td className="multiExpTblVal">{log.firmName || "--"}</td>
                                  <td className="multiExpTblVal">{log.log || "--"}</td>
                                  <td className="multiExpTblVal">{log.date || "--"}</td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                      )}
                    </RatingSection>
                    : null}
                  {/* {docCommunicationDetails._id ? <RatingSection onAccordion={() => onAccordion(3)} title="Activity Log" style={{ overflowY: "scroll" }} > {activeAccordions.includes(3) && ( <Audit auditLogs={auditLog || []} /> )} </RatingSection> : null } */}
                </div>
              </div>
            )}/>
        </div>
        <br/>
        <hr/>
        <Disclaimer/> {this.state.showModal
          ? (<DocModalDetails
            showModal={this.state.showModal}
            closeDocDetails={this.handleDocDetails}
            documentId={doc.documentId}
            onDeleteAll={this.onDeleteDoc}
            docMetaToShow={[]}
            versionMetaToShow={[]}
            docId={doc._id}/>)
          : null}
        <a
          className="hidden-download-anchor"
          style={{display: "none"}}
          onClick={() => getFile(`?objectName=${objectName}&versionId=${versionId}`, fileNames)}
          ref={el => {this.downloadAnchor = el}}
          download={fileNames}>
          hidden
        </a>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(PoliciesProcedures)
export default connect(mapStateToProps, null)(WrappedComponent)
