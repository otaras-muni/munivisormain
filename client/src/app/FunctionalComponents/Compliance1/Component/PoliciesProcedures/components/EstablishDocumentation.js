import React from "react"
import {
  SelectLabelInput,
  MultiSelect
} from "../../../../../GlobalComponents/TextViewBox"
import CKEditor from "react-ckeditor-component"
import DocUpload from "../../../../docs/DocUpload"
import DocLink from "../../../../docs/DocLink"
import DocModal from "../../../../docs/DocModal"
import TableHeader from "../../../../../GlobalComponents/TableHeader"
import moment from "moment"

const EstablishDocumentation = ({
  user,
  contextType,
  dropDown,
  bucketName,
  getBidBucket,
  errors = {},
  deleteDoc,
  onChangeItems,
  onNotesChange,
  item,
  category,
  onBlur,
  contextId,
  tenantId,
  docHeader,
  docTable,
  onClickDelete,
  onDownloadFile,
  svControls,
  duplicateFile,
  fileName,
  subNote,
  duplicateStatus
}) => {
  const onChange = e => {
    let newItem = {}
    if (e.target.name === "communicationNotes" || e.target.name === "communicationSubject") {
      newItem = {
        ...item,
        docCommunication: {
          ...item.docCommunication,
          [e.target.name]: e.target.value
        }
      }
    } else if (e.target.name === "actionType") {
      newItem = {
        ...item,
        docActions: {
          [e.target.name]: e.target.value,
          actionDate: new Date().toString()
        }
      }
    } else {
      newItem = {
        ...item,
        [e.target.name]: e.target.value
      }
    }
    onChangeItems(newItem, category)
  }

  const onEditorChange = (e, name) => {
    if(name === "Subject"){
      onNotesChange({
        [name]: e.target.value
      })
    } else {
      onNotesChange({
        [name]: name === "notes" || "Message" ? e.editor.getData() : e.target.value
      })
    }
  }

  const onAssignedSelect = (items, name) => {
    onChangeItems({
      ...item,
      docCommunication: {
        ...item.docCommunication,
        [name]: items.map(item => {
          const user = item
          let email = ""
          if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
            email = user.userEmails.filter(e => e.emailPrimary)
            email = email.length ? email[0].emailId : user.userEmails[0].emailId
          }
          return {
            id: item.id || item._id,
            name: item.name || `${item.userFirstName} ${item.userLastName}`,
            userId: item.id || item._id,
            userFirstName: item.userFirstName,
            userEntityId: item.entityId,
            userLastName: item.userLastName,
            userPrimaryEmailId: email,
            sendEmailTo: email,
            role: item.role
          }
        })
      }
    },category)
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(`${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`,category)
    }
  }

  const disableValue = ["Review in Progress", "Reviewed and Approved"]

  return (
    <div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl ">Document category</p>
          <p className="multiExpTblVal">Compliance</p>
        </div>
        <div className="column">
          <p className="multiExpLbl ">Sub-Category</p>
          <p className="multiExpTblVal">Policy &amp; Procedures</p>
        </div>
        <div className="column">
          <p className="multiExpLbl ">Document Type
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          <SelectLabelInput
            title="Document Type"
            list={dropDown.docType}
            onBlur={onBlurInput}
            error={errors.docType}
            name="docType"
            value={(item && item.docType) || ""}
            onChange={onChange}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl ">Upload File
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          {item._id ? (
            <DocUpload
              bucketName={bucketName}
              docMeta={{
                category: item.docCategory,
                subCategory: item.docSubCategory,
                type: item.docType
              }}
              docId={item.docAWSFileLocation}
              versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
              showFeedback
              contextId={contextId}
              onUploadSuccess={getBidBucket}
              contextType={contextType}
              tenantId={tenantId}
              user={user}
            />
          ) : (
            <DocUpload
              bucketName={bucketName}
              onUploadSuccess={getBidBucket}
              versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
              showFeedback
              contextType={contextType || "DEALS"}
              tenantId={user.entityId}
              contextId={contextId}
              user={user}
            />
          )}
          {errors.docAWSFileLocation && (
            <p className="text-error">{errors.docAWSFileLocation}</p>
          )}
        </div>
        <div className="column">
          <p className="multiExpLbl ">Filename</p>
          {item.docAWSFileLocation ? (
            <div className="field is-grouped" style={{ justifyContent: "end" }}>
              <DocLink docId={item.docAWSFileLocation} />
              {item._id ? (
                <DocModal
                  onDeleteAll={() => deleteDoc(item)}
                  selectedDocId={item.docAWSFileLocation}
                />
              ) : null}
            </div>
          ) : null}
        </div>
        <div className="column">
          <p className="multiExpLbl ">Status
            <span className="icon has-text-danger">
              <i className="fas fa-asterisk extra-small-icon"/>
            </span>
          </p>
          {duplicateStatus === "Reviewed and Approved" ? duplicateFile !== fileName ?
            <div className="select is-small is-link">
              <SelectLabelInput
                disableValue={svControls && !svControls.supervisor ? disableValue : null}
                title="Status"
                list={dropDown.docActions}
                onBlur={onBlurInput}
                name="status"
                value={(item && item.status) || ""}
                onChange={onChange}
              />
              <p className="text-error">{errors.status}</p>
            </div> :
            <small>{item && item.status}</small> : <div className="select is-small is-link">
            <SelectLabelInput
              disableValue={svControls && !svControls.supervisor ? disableValue : null}
              title="Status"
              list={dropDown.docActions}
              onBlur={onBlurInput}
              name="status"
              value={(item && item.status) || ""}
              onChange={onChange}
            />
            <p className="text-error">{errors.status}</p>
          </div>
             }
        </div>
      </div>
      {item._id ? (
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <TableHeader cols={docHeader} />
          <tbody>
            {docTable &&
              docTable.meta &&
              docTable.meta.versions.map((doc, i) => (
                <tr key={i}>
                  <td className="multiExpTblVal">{doc.uploadedBy || "--"}</td>
                  <td className="multiExpTblVal">{doc.originalName || ""}</td>
                  <td className="multiExpTblVal">{doc.size || ""}</td>
                  <td className="multiExpTblVal">
                    {doc.uploadDate ? moment(doc.uploadDate).format("MM-DD-YYYY h:mm A") : "" || "-"}
                  </td>
                  <td>
                    <div className="field is-grouped">
                      <span>
                        <span className="icon has-text-info">
                          <i
                            className="fa fa-download"
                            onClick={() => onDownloadFile(doc.versionId, doc.name, doc.originalName)}
                          />
                        </span>
                        <span className="icon has-text-danger">
                          <i
                            className="fa fa-trash-o"
                            onClick={() => onClickDelete(doc.versionId, doc.name, doc.originalName)}
                          />
                        </span>
                      </span>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      ) : null}
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">
            Would you like to notify someone about your action? If yes, please
            choose recipients below..
          </p>
          <MultiSelect
            filter
            data={dropDown.supervisorList}
            value={(item.docCommunication && item.docCommunication.owners) || []}
            placeholder="Accountable owner to complete action"
            onChange={item => onAssignedSelect(item, "owners")}
            error={errors.owners || ""}
            style={{ maxWidth: "unset" }}
            isFull
          />
          <MultiSelect
            filter
            data={dropDown.usersList}
            value={(item.docCommunication && item.docCommunication.toRecipients) || []}
            placeholder="TO"
            onChange={item => onAssignedSelect(item, "toRecipients")}
            error={errors.toRecipients || ""}
            style={{ maxWidth: "unset" }}
            isFull
          />
          <MultiSelect
            filter
            data={dropDown.usersList}
            value={(item.docCommunication && item.docCommunication.ccRecipients) || []}
            placeholder="CC"
            onChange={item => onAssignedSelect(item, "ccRecipients")}
            error={errors.ccRecipients || ""}
            style={{ maxWidth: "unset" }}
            isFull
          />
          <div className="column is-full">
            <input
              title="Subject"
              className="input is-small is-link"
              value={(subNote && subNote.Subject) || ""}
              // onBlur={onBlurInput}
              type="text"
              placeholder="Subject (limit 120 characters)"
              name="Subject"
              onChange={(e) => onEditorChange(e, "Subject")}
            />
            {/* <p className="text-error">{errors.communicationSubject}</p> */}
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column is-full">
          <div className="field">
            <p className="multiExpLbl " />
            <p className="multiExpLbl " title="Notes and instructions by transaction participants">
              Notes/Instructions
            </p>
            <p />
            <div className="control">
              {/* <textarea className="textarea" value={(item.docCommunication && item.docCommunication.communicationNotes) || ""} onBlur={onBlurInput} placeholder="" name="communicationNotes" onChange={onChange}/>
              <p className="text-error">{errors.communicationNotes}</p> */}
              <CKEditor
                activeClass="p10"
                content={(subNote && subNote.Message) || "" }
                events={{
                  /* "blur": this.onBlur,
                  "afterPaste": this.afterPaste, */
                  change: e => onEditorChange(e, "Message")
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default EstablishDocumentation
