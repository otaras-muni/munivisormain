import React from "react"
import ReactTable from "react-table"
import "react-table/react-table.css"
import moment from "moment"
import DocLink from "../../../../docs/DocLink";
import {DocModal} from "../../../../docs/DocModal";


const PoliciesList = ({
  list,
  onDeleteDoc,
  onEdit,
  pageSizeOptions = [5, 10, 20, 50, 100],
}) => {
  const columns = [
    {
      id: "docType",
      Header: "Document Type",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.docType || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.docType || "").localeCompare((b.docType || ""))
    },
    {
      id: "file",
      Header: "Filename",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div
            className="field is-grouped"
            style={{
              justifyContent: "start"
            }}>
            <DocLink docId={item.docAWSFileLocation}/>
            <DocModal
              onDeleteAll={() => onDeleteDoc(item)}
              documentId={item._id}
              selectedDocId={item.docAWSFileLocation}/>
          </div>
        )
      },
      sortMethod: (a, b) => (a.docFileName || "").localeCompare((b.docFileName || ""))
    },
    {
      id: "reviewer",
      Header: "Reviewer/Approver",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.reviewer || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.reviewer || "").localeCompare((b.reviewer || ""))
    },
    {
      id: "reviewerRole",
      Header: "Reviewer/Approver Role",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.reviewerRole || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.reviewerRole || "").localeCompare((b.reviewerRole || ""))
    },
    {
      id: "createdUserName",
      Header: "Created",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.createdUserName) }} />
        )
      },
      sortMethod: (a, b) => (a.createdUserName || "").localeCompare((b.createdUserName || ""))
    },
    {
      id: "lastUpdatedDate",
      Header: "Last Updated On",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        const data = (item.lastUpdatedDate) ? moment(item.lastUpdatedDate).format("MM.DD.YYYY hh:mm A") : "-" || "-"
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (data) }} />
        )
      },
      sortMethod: (a, b) => (a.lastUpdatedDate || "").localeCompare((b.lastUpdatedDate || ""))
    },
    {
      id: "lastReviewedOn",
      Header: "Last Reviewed On",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        const data = (item.lastReviewedOn) ? moment(item.lastReviewedOn).format("MM.DD.YYYY hh:mm A") : "-" || "-"
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (data) }} />
        )
      },
      sortMethod: (a, b) => (a.lastReviewedOn || "").localeCompare((b.lastReviewedOn || ""))
    },
    {
      id: "lastUpdateBy",
      Header: "Last Updated By",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.lastUpdateBy || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.lastUpdateBy || "").localeCompare((b.lastUpdateBy || ""))
    },
    {
      id: "status",
      Header: "Status",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.status || "-") }} />
        )
      },
      sortMethod: (a, b) => (a.status || "").localeCompare((b.status || ""))
    },
    {
      id: "recordDate",
      Header: "Actions",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => { // eslint-disable-line
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div className="control">
              <span className="has-text-link">
                <a className="fas fa-pencil-alt" onClick={() => onEdit(item)}/>
              </span>
            </div>
          </div>
        )
      },
    },
  ]

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={list}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default PoliciesList
