import React, { Component } from "react"
import ComplianceView from "./Component"

class Compliance extends Component {
  render() {
    const { nav2 } = this.props
    return (
      <div>
        <ComplianceView {...this.props} option={nav2}/>
      </div>
    )
  }
}

export default Compliance
