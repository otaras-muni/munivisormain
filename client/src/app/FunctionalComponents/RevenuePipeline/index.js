import React, { Component } from "react"
import {Link} from "react-router-dom"
import { connect } from "react-redux"
import { Multiselect} from "react-widgets"
import moment from "moment"
import cloneDeep from "lodash.clonedeep"
import { getPicklistByPicklistName, transactionUrl } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/Loader"
import {fetchAllRevenuePipeline} from "../../StateManagement/actions/Common"
import { fetchIssuers, fetchLeadAdvisor } from "../../StateManagement/actions/CreateTransaction"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import RatingSection from "../../GlobalComponents/RatingSection"
import Accordion from "../../GlobalComponents/Accordion"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

class RevenuePipeline extends Component {
  constructor(props) {
    super(props)
    this.state = {
      revenue: {},
      loading: true,
      tableView: true,
      filterBy: {
        searchTerm: "",
        status: [
          "In Progress",
          "Lost",
          "Cancel/Dropped",
          "Closed",
          "Executed but not closed",
          "Pre-execution",
          "Pre-Pricing",
        ],
        client: [],
        leadManager: [],
        createdAt: "currentYear",
        type: ["All"]

      },
      dropDown: {
        transactionTypes: [
          {id: "All", name: "All"},
          {id: "Deals", name: "Debt / Bond Issue"},
          {id: "BankLoans", name: "Debt / Bank Loan"},
          {id: "RFP", name: "Manage Client RFP"},
          {id: "ActMaRFP", name: "MA-RFP"},
          {id: "Derivatives", name: "Derivatives"},
          {id: "Others", name: "Other Activity"},
        ],
        times: ["Last months", "Last 3 months", "Last 6 months", "Last 1 year"],
        status: [], // ["Open and/or Active", "Closed and/or Canceled", "All activity"],
        clients: [],
        leadManager: [],
      },
      jsonSheets: [],
      startXlDownload: false
    }
  }

  componentDidMount() {
    this.getRevenuePipeline()
  }

  getRevenuePipeline = async () => {
    const {user} = this.props
    this.searchRevenuePipeline()
    const leadManagers = await fetchLeadAdvisor(user && user.entityId)
    const picResult = await getPicklistByPicklistName(["LKUPSWAPTRANSTATUS"])
    const result = (picResult.length && picResult[1]) || {}
    const disabledStatus = result.LKUPSWAPTRANSTATUS && result.LKUPSWAPTRANSTATUS.filter(f => !f.included).map(e => e.label) || []
    const status = result.LKUPSWAPTRANSTATUS && result.LKUPSWAPTRANSTATUS.map(e => e.label) || []

    fetchIssuers(user.entityId, (res) => {

      res.issuerList.forEach(issuer => {
        issuer.clientId = issuer.id || ""
        issuer.clientName = issuer.name || ""
      })

      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          clients: res.issuerList,
          status,
          disabledStatus,
          leadManager: leadManagers || []
        },
        loading: false,
      }))
    })
  }

  searchRevenuePipeline = async () => {
    const filterBy = cloneDeep(this.state.filterBy)
    if(filterBy.createdAt){
      switch(filterBy.createdAt) {
      case "currentYear":
        filterBy.createdAt = moment().startOf("year").format()
        break
      case "lastMonth":
        filterBy.createdAt = moment().subtract(1, "months").date(1).format()
        break
      case "last3Months":
        filterBy.createdAt = moment().subtract(3, "months").date(1).format()
        break
      case "last6Months":
        filterBy.createdAt = moment().subtract(6, "months").date(1).format()
        break
      case "last12Months":
        filterBy.createdAt = moment().subtract(12, "months").date(1).format()
        break
      case "last24Months":
        filterBy.createdAt = moment().subtract(24, "months").date(1).format()
        break
      case "last36Months":
        filterBy.createdAt = moment().subtract(36, "months").date(1).format()
        break
      default:
        filterBy.createdAt = ""
        break
      }
    }

    if(filterBy && filterBy.client && filterBy.client.length){
      filterBy.client = filterBy.client.map(cl => cl.id)
    }

    if(filterBy && filterBy.leadManager && filterBy.leadManager.length){
      filterBy.leadManager = filterBy.leadManager.map(cl => cl.id)
    }

    const revenue = await fetchAllRevenuePipeline(filterBy)
    this.setState({
      revenue,
      loading: false
    })
  }

  onChange = e => {
    const {filterBy} = this.state
    const { name, value } = e.target

    this.setState({
      filterBy: {
        ...filterBy,
        [name]: value
      },
      loading: true,
    },() => this.searchRevenuePipeline())
  }

  viewToggle = () => {
    this.setState(prevState => ({
      tableView: !prevState.tableView
    }))
  }

  onMultiSelect = (name, value) => {
    const {filterBy} = this.state
    this.setState({
      filterBy: {
        ...filterBy,
        [name]: value
      },
      loading: true,
    },() => this.searchRevenuePipeline())
  }

  onSelectTranTypes = (data, metaData) => {
    const {filterBy} = this.state
    data = data.map(val => val.id)
    if(filterBy.type.indexOf("All") === -1 && metaData.action === "insert" && metaData.dataItem.id === "All"){
      data = ["All"]
    }else if(filterBy.type.indexOf("All") !== -1 && metaData.action === "insert" && metaData.dataItem.id !== "All"){
      data = data.filter(val => val !== "All")
    }else if(!data.length && metaData.action === "remove"){
      data = ["All"]
    }

    this.setState({
      filterBy: {
        ...filterBy,
        type: data
      },
      loading: true,
    },() => this.searchRevenuePipeline())
  }

  xlDownload = () => {
    const {revenue} = this.state
    const xlOpportunity = []
    const xlInProgress = []
    const xlInvoiced = []
    const xlPaid = []

    revenue && revenue.opportunities && revenue.opportunities.map((item, id) => {
      const data = {
        "Issue Description": item.activityDescription || "--",
        "Client Name": item.activityClientName || "--",
        "Lead Advisor": item.activityLeadAdvisorName || "--",
        "Estimated Revenue": item.activityEstimatedRev || "--",
        "Total Expense": item.totalOverallExpenses || "--",
        "Total": revenue.opportunities && revenue.opportunities.length-1 === id ? revenue.totalOpportunities : "" || "",
      }
      xlOpportunity.push(data)
    })

    revenue && revenue.inProgress && revenue.inProgress.map((item, id) => {
      const data = {
        "Issue Description": item.activityDescription || "--",
        "Client Name": item.activityClientName || "--",
        "Lead Advisor": item.activityLeadAdvisorName || "--",
        "Estimated Revenue": item.activityEstimatedRev || "--",
        "Total Expense": item.totalOverallExpenses || "--",
        "Total": revenue.inProgress && revenue.inProgress.length-1 === id ? revenue.totalInProgress : "" || "",
      }
      xlInProgress.push(data)
    })

    revenue && revenue.invoiced && revenue.invoiced.map((item, id) => {
      const data = {
        "Issue Description": item.activityDescription || "--",
        "Client Name": item.activityClientName || "--",
        "Lead Advisor": item.activityLeadAdvisorName || "--",
        "Estimated Revenue": item.activityEstimatedRev || "--",
        "Total Expense": item.totalOverallExpenses || "--",
        "Total": revenue.invoiced && revenue.invoiced.length-1 === id ? revenue.totalInvoiced : "" || "",
      }
      xlInvoiced.push(data)
    })

    revenue && revenue.paid && revenue.paid.map((item, id) => {
      const data = {
        "Issue Description": item.activityDescription || "--",
        "Client Name": item.activityClientName || "--",
        "Lead Advisor": item.activityLeadAdvisorName || "--",
        "Estimated Revenue": item.activityEstimatedRev || "--",
        "Total Expense": item.totalOverallExpenses || "--",
        "Total": revenue.paid && revenue.paid.length-1 === id ? revenue.totalPaid : "" || "",
      }
      xlPaid.push(data)
    })

    const jsonSheets = [{
      name: "Opportunity",
      headers: ["Issue Description","Client Name","Lead Advisor","Estimated Revenue","Total Expense", "Total"],
      data: xlOpportunity,
    },
    {
      name: "In Progress",
      headers: ["Issue Description","Client Name","Lead Advisor","Estimated Revenue","Total Expense", "Total"],
      data: xlInProgress,
    },
    {
      name: "Invoiced",
      headers: ["Issue Description","Client Name","Lead Advisor","Estimated Revenue","Total Expense", "Total"],
      data: xlInvoiced,
    },
    {
      name: "Paid",
      headers: ["Issue Description","Client Name","Lead Advisor","Estimated Revenue","Total Expense", "Total"],
      data: xlPaid,
    }]
    this.setState({
      jsonSheets,
      startXlDownload: true
    }, () => this.resetXLDownloadFlag)
  }

  resetXLDownloadFlag = () => {
    this.setState({ startXlDownload: false })
  }

  render() {
    const { revenue, tableView, filterBy, dropDown } = this.state
    const { opportunities, inProgress, invoiced, paid, totalOpportunities, totalInProgress, totalInvoiced, totalPaid  } = revenue

    const loading = () => <Loader />
    return (
      <div id="main">
        {this.state.loading ? loading() : null}
        <Accordion
          multiple
          activeItem={[1]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              {
                <div className="columns">
                  <div className="column">
                    <div className="field is-grouped" style={{justifyContent: "flex-start"}}>
                      <div className="control" onClick={this.xlDownload}>
                        <span className="has-link"><i className="far fa-2x fa-file-excel has-text-link"/></span>
                      </div>
                      <div style={{ display: "none" }}>
                        <ExcelSaverMulti label="Revenue Pipeline" startDownload={this.state.startXlDownload} afterDownload={this.resetXLDownloadFlag} jsonSheets={this.state.jsonSheets}/>
                      </div>
                    </div>
                  </div>
                </div>
              }
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Search"
              >
                {activeAccordions.includes(0) && (
                  <div className="revenue">
                    <div className="columns">
                      <div className="column">
                        <p className="multiExpLbl ">
                          Status
                        </p>
                        <div className="control" style={{fontSize: 12}}>
                          <Multiselect
                            value={filterBy.status || ""}
                            data={dropDown.status || []}
                            disabled={dropDown.disabledStatus || ["Won", "Cancelled", "On Hold"]}
                            filter
                            onChange={status =>
                              this.onMultiSelect("status", status)
                            }
                          />
                        </div>
                      </div>

                      <div className="column">
                        <p className="multiExpLbl ">
                          Client Name
                        </p>
                        <div className="control" style={{fontSize: 12}}>
                          <Multiselect
                            filter
                            value={filterBy.client || ""}
                            data={dropDown.clients || []}
                            message="select issuer"
                            textField="name"
                            valueField="id"
                            key="name"
                            groupBy={({ relType }) => relType}
                            onChange={issuer =>
                              this.onMultiSelect("client", issuer)
                            }
                          />
                        </div>
                      </div>

                      <div className="column">
                        <p className="multiExpLblBlk" >Search</p>
                        <p className="control has-icons-left">
                          <input className="input customInput css-dnqvel ep3169p0 is-link" placeholder="Search.." name="searchTerm" value={filterBy.searchTerm || ""} onChange={this.onChange}/>
                          <span className="icon is-left searchIcon"><i className="fas fa-search" /></span>
                        </p>
                      </div>
                    </div>

                    <div className="columns">
                      <div className="column">
                        <p className="multiExpLbl ">
                          Transaction Type or Subtype
                        </p>
                        <div className="control" style={{fontSize: 12}}>
                          <Multiselect
                            filter
                            message="Select Transaction Type"
                            textField="name"
                            valueField="id"
                            key="name"
                            value={filterBy.type || ""}
                            data={dropDown.transactionTypes || []}
                            onChange={this.onSelectTranTypes
                              /* type => this.onMultiSelect("type", type.map(val => val.id)) */
                            }
                          />
                        </div>
                      </div>

                      <div className="column">
                        <p className="multiExpLbl ">
                          Created time
                        </p>
                        <div className="control">
                          <div className="select is-small is-link">
                            <select name="createdAt" onChange={this.onChange} value={filterBy.createdAt || ""}>
                              <option value="currentYear">Current Year</option>
                              <option value="lastMonth">Last Month</option>
                              <option value="last3Months">Last 3 Months</option>
                              <option value="last6Months">Last 6 Months</option>
                              <option value="last12Months">Last 12 Months</option>
                              <option value="last24Months">Last 24 Months</option>
                              <option value="last36Months">Last 36 Months</option>
                              <option value="">All</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div className="column">
                        <p className="multiExpLbl ">
                          Lead Manager
                        </p>
                        <div className="control" style={{fontSize: 12}}>
                          <Multiselect
                            filter
                            value={filterBy.leadManager || ""}
                            data={dropDown.leadManager || []}
                            message="select issuer"
                            textField="name"
                            valueField="id"
                            key="name"
                            groupBy={({ relType }) => relType}
                            onChange={issuer =>
                              this.onMultiSelect("leadManager", issuer)
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Revenue Pipeline"
              >
                {activeAccordions.includes(1) && (
                  <div className="revenue">
                    <div className="columns" style={{ marginTop: 5 }}>
                      {/* <div className="tabs">
                        <li className={tableView ? "is-active" : ""} onClick={this.viewToggle}>
                          <a className="tabSecLevel">
                            <span>Board</span>
                          </a>
                        </li>
                        <li className={!tableView ? "is-active" : ""} onClick={this.viewToggle}>
                          <a className="tabSecLevel">
                            <span>Board</span>
                          </a>
                        </li>
                      </div> */}
                    </div>
                    <br/>
                    {
                      tableView ?
                        <div className="padder">
                          <div className="columns table-mobile-view">
                            <div className="column table-header">
                              Opportunity
                            </div>
                            <div className="column table-header">
                              Deals (In Progress)
                            </div>
                            <div className="column table-header">
                              Invoiced
                            </div>
                            <div className="column table-header">
                              Paid
                            </div>
                          </div>
                          <div className="columns table-mobile-view">
                            <div className="column table-data">
                              {
                                opportunities && opportunities.length ? opportunities.map(opportunity => (
                                  <div key={opportunity.activityId} className="box" style={{fontSize: 12, margin: 10, padding: "5px 16px"}}>
                                    <article className="media">
                                      <div className="media-content">
                                        <div className="content">
                                          <p>
                                            {transactionUrl(opportunity.activityTranType, opportunity.activityTranSubType, opportunity.activityDescription, opportunity.activityId, filterBy.searchTerm)}
                                          </p>
                                          <div>
                                            <strong>Client Name: </strong>
                                            <Link
                                              to={`/clients-propects/${opportunity.activityTranClientId || ""}/entity` || ""}
                                              dangerouslySetInnerHTML={{
                                                __html: highlightSearch(opportunity.activityClientName || "-", "")
                                              }}
                                            />
                                          </div>
                                          <div>
                                            <strong>Lead Advisor: </strong>
                                            {
                                              opportunity.activityLeadAdvisorId ?
                                                <Link
                                                  to={`/admin-users/${opportunity.activityLeadAdvisorId}/users` || ""}
                                                  dangerouslySetInnerHTML={{
                                                    __html: highlightSearch(opportunity.activityLeadAdvisorName || "-", "")
                                                  }}
                                                /> : "-"
                                            }
                                          </div>
                                          <div>
                                            <strong>Estimated Revenue: </strong>
                                            ${typeof opportunity.activityEstimatedRev === "number" ? opportunity.activityEstimatedRev && opportunity.activityEstimatedRev.toLocaleString() || 0 : 0}
                                          </div>
                                        </div>
                                      </div>
                                    </article>
                                  </div>
                                )) : null
                              }
                            </div>
                            <div className="column table-data">
                              {
                                inProgress && inProgress.length ? inProgress.map(invoice => (
                                  <div key={invoice.activityId} className="box" style={{fontSize: 12, margin: 10, padding: "5px 16px"}}>
                                    <article className="media">
                                      <div className="media-content">
                                        <div className="content">
                                          <p>
                                            {transactionUrl(invoice.activityTranType, invoice.activityTranSubType, invoice.activityDescription, invoice.activityId, filterBy.searchTerm)}
                                          </p>
                                          <div>
                                            <strong>Client Name: </strong>
                                            <Link
                                              to={`/clients-propects/${invoice.activityTranClientId || ""}/entity` || ""}
                                              dangerouslySetInnerHTML={{
                                                __html: highlightSearch(invoice.activityClientName || "-", "")
                                              }}
                                            />
                                          </div>
                                          <div>
                                            <strong>Lead Advisor: </strong>
                                            {
                                              invoice.activityLeadAdvisorId ?
                                                <Link
                                                  to={`/admin-users/${invoice.activityLeadAdvisorId}/users` || ""}
                                                  dangerouslySetInnerHTML={{
                                                    __html: highlightSearch(invoice.activityLeadAdvisorName || "-", "")
                                                  }}
                                                /> : "-"
                                            }
                                          </div>
                                          <div>
                                            <strong>Estimated Revenue: </strong>
                                            {`$${typeof invoice.activityEstimatedRev === "number" ? invoice.activityEstimatedRev && invoice.activityEstimatedRev.toLocaleString() || 0 : 0}`}
                                          </div>
                                          { invoice.invoiceStatus ?
                                            <div>
                                              <strong>Total Expense: </strong>
                                              <strong>{ `$${invoice.totalOverallExpenses && invoice.totalOverallExpenses.toLocaleString() || 0}` }</strong>
                                            </div>
                                            : null }
                                        </div>
                                      </div>
                                    </article>
                                  </div>
                                )) : null
                              }
                            </div>
                            <div className="column table-data">
                              {
                                invoiced && invoiced.length ? invoiced.map(invoice => (
                                  <div key={invoice.activityId} className="box" style={{fontSize: 12, margin: 10, padding: "5px 16px"}}>
                                    <article className="media">
                                      <div className="media-content">
                                        <div className="content">
                                          <p>
                                            {transactionUrl(invoice.activityTranType, invoice.activityTranSubType, invoice.activityDescription, invoice.activityId, filterBy.searchTerm)}
                                          </p>
                                          <div>
                                            <strong>Client Name: </strong>
                                            <Link
                                              to={`/clients-propects/${invoice.activityTranClientId || ""}/entity` || ""}
                                              dangerouslySetInnerHTML={{
                                                __html: highlightSearch(invoice.activityClientName || "-", "")
                                              }}
                                            />
                                          </div>
                                          <div>
                                            <strong>Lead Advisor: </strong>
                                            {
                                              invoice.activityLeadAdvisorId ?
                                                <Link
                                                  to={`/admin-users/${invoice.activityLeadAdvisorId}/users` || ""}
                                                  dangerouslySetInnerHTML={{
                                                    __html: highlightSearch(invoice.activityLeadAdvisorName || "-", "")
                                                  }}
                                                /> : "-"
                                            }
                                          </div>
                                          <div>
                                            <strong>Estimated Revenue: </strong>
                                            {`$${typeof invoice.activityEstimatedRev === "number" ? invoice.activityEstimatedRev && invoice.activityEstimatedRev.toLocaleString() || 0 : 0}`}
                                          </div>
                                          <div>
                                            <strong>Actual Revenue: </strong>
                                            <strong>${invoice.totalOverallExpenses && invoice.totalOverallExpenses.toLocaleString() || 0}</strong>
                                          </div>
                                        </div>
                                      </div>
                                    </article>
                                  </div>
                                )) : null
                              }
                            </div>
                            <div className="column table-data">
                              {
                                paid && paid.length ? paid.map(invoice => (
                                  <div key={invoice.activityId} className="box" style={{fontSize: 12, margin: 10, padding: "5px 16px"}}>
                                    <article className="media">
                                      <div className="media-content">
                                        <div className="content">
                                          <p>
                                            {transactionUrl(invoice.activityTranType, invoice.activityTranSubType, invoice.activityDescription, invoice.activityId, filterBy.searchTerm)}
                                          </p>
                                          <div>
                                            <strong>Client Name: </strong>
                                            <Link
                                              to={`/clients-propects/${invoice.activityTranClientId || ""}/entity` || ""}
                                              dangerouslySetInnerHTML={{
                                                __html: highlightSearch(invoice.activityClientName || "-", "")
                                              }}
                                            />
                                          </div>
                                          <div>
                                            <strong>Lead Advisor: </strong>
                                            {
                                              invoice.activityLeadAdvisorId ?
                                                <Link
                                                  to={`/admin-users/${invoice.activityLeadAdvisorId}/users` || ""}
                                                  dangerouslySetInnerHTML={{
                                                    __html: highlightSearch(invoice.activityLeadAdvisorName || "-", "")
                                                  }}
                                                /> : "-"
                                            }
                                          </div>
                                          <div>
                                            <strong>Estimated Revenue: </strong>
                                            {`$${typeof invoice.activityEstimatedRev === "number" ? invoice.activityEstimatedRev && invoice.activityEstimatedRev.toLocaleString() || 0 : 0}`}
                                          </div>
                                          <div>
                                            <strong>Actual Revenue: </strong>
                                            <strong>${invoice.totalOverallExpenses && invoice.totalOverallExpenses.toLocaleString() || 0}</strong>
                                          </div>
                                        </div>
                                      </div>
                                    </article>
                                  </div>
                                )) : null
                              }
                            </div>
                          </div>
                          <div className="columns table-mobile-view" >
                            <div className="column table-data total">
                              ${totalOpportunities && totalOpportunities.toLocaleString() || 0}
                            </div>
                            <div className="column table-data total">
                              ${totalInProgress && totalInProgress.toLocaleString() || 0}
                            </div>
                            <div className="column table-data total">
                              ${totalInvoiced && totalInvoiced.toLocaleString() || 0}
                            </div>
                            <div className="column table-data total">
                              ${totalPaid && totalPaid.toLocaleString() || 0}
                            </div>
                          </div>
                        </div>
                        :
                        <div> Board view under construction </div>
                    }
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>

    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userInfo: auth.userEntities,
  user: (auth && auth.userEntities) || {},
  token: auth.token,
})

export default connect(mapStateToProps, null )(RevenuePipeline)
