import React, { Component } from "react"
import axios from "axios"
import qs from "qs"
import mime from "mime"
import fileSize from "file-size"

import { Modal } from "Global/BulmaModal"
import { muniApiBaseURL } from "GlobalUtils/consts"
import Loader from "../../GlobalComponents/LineLoader"
import { getToken, checkInvalidFiles } from "GlobalUtils/helpers"
import moment from "moment";

class DocUpload extends Component {
  constructor(props) {
    super(props)
    this.state = {
      error: "",
      message: "",
      inProgress: false,
      file: {},
      showModal: false,
      docId: "",
      modalState: false
    }
    this.clickUpload = this.clickUpload.bind(this)
    this.onClickUpload = this.onClickUpload.bind(this)
    this.onChangeUpload = this.onChangeUpload.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.uploadFileFromState = this.uploadFileFromState.bind(this)
    this.uploadNewFile = this.uploadNewFile.bind(this)
  }

  onClickUpload(event) {
    event.target.value = null
  }

  onChangeUpload(event) {
    console.log(" in onChangeUpload")
    this.setState({ inProgress: true })
    const file = event.target.files[0]
    console.log("fileName1 : ", file.name)
    if (!file) {
      console.log("No file")
      this.setState({ error: "No file", inProgress: false })
      return
    }
    const { contextId, contextType, folderId, tenantId, docId, docMeta } = this.props
    // console.log("tags : ", tags)
    // bucketName = bucketName || "munidocs"
    const invalidFile = checkInvalidFiles(file, "single")
    if(invalidFile){
      this.setState({
        error: invalidFile || "",
        inProgress: false
      })
      return
    }
    if (docId) {
      const docFilter = { _id: docId }
      console.log("docFilter : ", docFilter)
      axios
        .get(`${muniApiBaseURL}/docs`, { params: { ...docFilter },headers:{"Authorization":getToken()} })
        .then(res => {
          console.log("res : ", res)
          const doc = (res && res.data[0]) || {}
          const { _id } = doc
          if (_id) {
            this.setState(
              { showModal: false, docId: _id, file },
              this.uploadFileFromState
            )
          } else {
            this.setState({
              error: "No matching document found to add version to",
              showModal: false,
              docId: "",
              inProgress: false
            })
          }
        })
    } else {
      const docFilter = {
        contextId,
        contextType,
        folderId,
        tenantId,
        originalName: file.name
      }
      Object.keys(docMeta || {}).forEach(k => {
        docFilter[`meta.${k}`] = docMeta[k]
      })
      console.log("docFilter : ", docFilter)
      axios
        .get(`${muniApiBaseURL}/docs`, { params: { ...docFilter },headers:{"Authorization":getToken()} })
        .then(res => {
          console.log("res : ", res)
          const doc = (res && res.data[0]) || {}
          const { _id } = doc
          if (_id) {
            console.log("_id : ", _id)
            this.setState({
              showModal: true,
              modalState: true,
              docId: _id,
              file
            })
          } else {
            this.setState({ showModal: false, docId: "", file: {} })
            this.uploadFile(file)
          }
        })
    }
  }

  uploadFile(file) {
    const { bucketName, contextId, contextType, tenantId, tags } = this.props
    let fileName = file.name
    const extnIdx = fileName.lastIndexOf(".")
    if (extnIdx > -1) {
      fileName = `${fileName.substr(
        0,
        extnIdx
      )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
    }
    console.log("fileName2 : ", fileName)
    const contentType = file.type
    if (!fileName) {
      this.setState({ error: "No file name provided", inProgress: false })
      return
    }
    let filePath
    if (tenantId && contextId && contextType) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const opType = "upload"
    const options = { contentType, tags }

    axios
      .post(`${muniApiBaseURL}/s3/get-signed-url`, {
        opType,
        bucketName,
        fileName: filePath,
        options
      }, {headers:{"Authorization":getToken()}})
      .then(res => {
        console.log("res : ", res)
        this.uploadWithSignedUrl(bucketName, res.data.url, file, fileName, tags)
      })
      .catch(err => {
        console.log("err : ", err)
        this.setState({
          error: "Error in getting signed url for upload",
          message: "",
          inProgress: false
        })
      })
  }

  uploadFileFromState() {
    this.toggleModal(true)
    const { file } = this.state
    this.uploadFile(file)
  }

  uploadNewFile() {
    this.setState({ docId: "" }, this.uploadFileFromState)
  }

  toggleModal(inProgress) {
    console.log("inProgress : ", inProgress)
    this.setState(prev => {
      const inProgressValue = inProgress === true
      console.log("inProgressValue : ", inProgressValue)
      const newState = !prev.modalState
      return {
        showModal: false,
        modalState: newState,
        inProgress: inProgressValue
      }
    })
  }

  uploadWithSignedUrl(bucketName, signedS3Url, file, fileName, tags) {
    console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      bucketName,
      fileName,
      file.type
    )
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("no tagging")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = () => {
      console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        console.log("file uploaded ok")
        this.setState({
          error: "",
          message: `Last upload successful for "${fileName}" at ${new Date()}`
        })
        this.updateDocsDB(bucketName, file, fileName)
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded", xhr.status, xhr.responseText, err)
      this.setState({
        message: "",
        error: "Error in uploading to S3",
        inProgress: false
      })
    }
    xhr.send(file)
  }

  clickUpload() {
    // event.preventDefault();
    // console.log(" in clickUpload: ", event);
    // if(this.props.dealId === "0") {
    //   this.setState({ message: "",
    //     error: "Please save the new deal before uploading documents",
    //     inProgress: false })
    //   return
    // }
    this.uploadFileInput.click()
  }

  updateDocsDB(bucketName, file, fileName) {
    // let size = fileSize(file.size).human('jedec');
    const {
      contextId,
      contextType,
      folderId,
      tenantId,
      docMeta,
      versionMeta
    } = this.props
    const type = mime.getExtension(file.type)
    const meta = { ...docMeta }
    // let fileName = file.name
    let filePath
    if (tenantId && contextType && contextId) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
      // meta = { ...meta }
    }
    console.log("bucketName, fileName : ", bucketName, fileName)
    axios
      .post(`${muniApiBaseURL}/s3/get-s3-object-versions`, {
        bucketName,
        fileName: filePath
      },{headers:{"Authorization":getToken()}})
      .then(res => {
        if (res.data.error) {
          console.log("err in getting version : ", res.error)
          this.setState({
            message: "",
            error: "Error in getting S3 versions",
            inProgress: false
          })
        } else {
          console.log("got versions : ", res)
          // let latestVersion = res.Versions.filter(v => v.IsLatest)[0];
          let versionId
          let uploadDate
          let size
          res.data.result.Versions.some(v => {
            if (v.IsLatest) {
              versionId = v.VersionId
              uploadDate = v.LastModified
              size = fileSize(v.Size).human("jedec")
              return true
            }
          })
          if (versionId) {
            const { docId } = this.state
            console.log("docId : ", docId)
            // meta.versions = [{ versionId,  uploadDate, size }]
            // const docFilter = { name: file.name }
            // docFilter.tenantId = tenantId || null
            // docFilter.contextType = contextType || null
            // docFilter.contextId = contextId || null
            // console.log("docFilter : ", docFilter)
            if (docId) {
              axios
                .get(`${muniApiBaseURL}/docs`, { params: { _id: docId },headers:{"Authorization":getToken()} })
                .then(res => {
                  console.log("res : ", res)
                  const doc = (res && res.data[0]) || {}
                  const { _id } = doc
                  if (_id) {
                    // const updatedDoc = { ...doc }
                    // updatedDoc.meta = { ...doc.meta, ...meta }
                    // updatedDoc.meta.versions = [ { versionId,  uploadDate, size },
                    //   ...doc.meta.versions ]
                    // console.log("updatedDoc : ", updatedDoc)
                    // axios.put(`${muniApiBaseURL}/doc/${_id}`, updatedDoc)
                    //   .then(res => {
                    //     console.log("updated ok in docs db : ", res)
                    //     this.setState({ message: "", error: "", inProgress: false })
                    //     if(this.props.onUploadSuccess) {
                    //       this.props.onUploadSuccess(file.name, _id, this.props.bidIndex, res.meta)
                    //     }
                    //   }).catch(err => {
                    //     console.log("err in updating in docs db : ", err)
                    //     this.setState({ message: "", error: "Error in updating docs DB", inProgress: false })
                    //   })
                    axios
                      .post(`${muniApiBaseURL}/docs/update-versions`, {
                        _id,
                        versions: [
                          {
                            versionId,
                            name: fileName,
                            originalName: file.name,
                            uploadDate,
                            size,
                            type,
                            ...versionMeta
                          }
                        ],
                        name: fileName,
                        originalName: file.name,
                        option: "add",
                        updatedAt: moment(new Date()).format("MM/DD/YYYY hh:mm A")
                      },{headers:{"Authorization":getToken()}})
                      .then(res => {
                        console.log("updated versions ok in docs db : ", res)
                        this.setState({
                          message: "",
                          error: "",
                          inProgress: false
                        })
                        if (this.props.onUploadSuccess) {
                          this.props.onUploadSuccess(
                            file.name,
                            _id,
                            this.props.bidIndex,
                            res.meta
                          )
                        }
                      })
                      .catch(err => {
                        console.log(
                          "err in updating versions in docs db : ",
                          err
                        )
                        this.setState({
                          message: "",
                          error: "Error in updating versions in docs DB",
                          inProgress: false
                        })
                      })
                  } else {
                    meta.versions = [
                      {
                        versionId,
                        name: fileName,
                        originalName: file.name,
                        uploadDate,
                        size,
                        type,
                        ...versionMeta
                      }
                    ]
                    const doc = {
                      name: fileName,
                      originalName: file.name,
                      meta,
                      contextType,
                      folderId,
                      contextId,
                      tenantId
                    }
                    axios
                      .post(`${muniApiBaseURL}/docs`, doc,{headers:{"Authorization":getToken()}})
                      .then(res => {
                        console.log("inserted ok in docs db : ", res)
                        this.setState({
                          message: "",
                          error: "",
                          inProgress: false
                        })
                        if (this.props.onUploadSuccess) {
                          this.props.onUploadSuccess(
                            file.name,
                            res.data._id,
                            this.props.bidIndex,
                            res.meta
                          )
                        }
                      })
                      .catch(err => {
                        console.log("err in inserting in docs db : ", err)
                        this.setState({
                          message: "",
                          error: "Error in inserting in docs DB",
                          inProgress: false
                        })
                      })
                  }
                })
                .catch(err => {
                  console.log("err in getting docs : ", err)
                  this.setState({
                    message: "",
                    error: "Error in getting docs",
                    inProgress: false
                  })
                })
            } else {
              meta.versions = [
                {
                  versionId,
                  name: fileName,
                  originalName: file.name,
                  uploadDate,
                  size,
                  ...versionMeta
                }
              ]
              const doc = {
                name: fileName,
                originalName: file.name,
                meta,
                contextType,
                folderId,
                contextId,
                tenantId
              }
              axios
                .post(`${muniApiBaseURL}/docs`, doc,{headers:{"Authorization":getToken()}})
                .then(res => {
                  console.log("inserted ok in docs db : ", res)
                  this.setState({ message: "", error: "", inProgress: false })
                  if (this.props.onUploadSuccess) {
                    this.props.onUploadSuccess(
                      file.name,
                      res.data._id,
                      this.props.bidIndex,
                      res.meta
                    )
                  }
                })
                .catch(err => {
                  console.log("err in inserting in docs db : ", err)
                  this.setState({
                    message: "",
                    error: "Error in inserting in docs DB",
                    inProgress: false
                  })
                })
            }
          } else {
            console.log("No version error")
            this.setState({
              message: "",
              error: "S3 version error",
              inProgress: false
            })
          }
        }
      })
      .catch(err => {
        console.log("err in getting version : ", err)
        this.setState({
          message: "",
          error: "Error in getting S3 versions",
          inProgress: false
        })
      })
  }

  render() {
    const { disabled, responseOrQuestion, docId, showFeedback, accept } = this.props
    const { showModal } = this.state
    if (showModal) {
      return (
        <Modal
          closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title="New file or new version ?"
        >
          <p className="multiExpGrpLbl">
            We have found an existing file with the same name and upload options
            selected by you.
          </p>
          <p className="multiExpGrpLbl">
            Please confirm if you want to upload a new version to the existing
            file or upload as a separate new file?
          </p>
          <div className="field is-grouped">
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={this.uploadFileFromState}
              >
                New Version
              </button>
            </div>
            <div className="control">
              <button
                className="button is-light is-small"
                onClick={this.uploadNewFile}
              >
                New File
              </button>
            </div>
          </div>
        </Modal>
      )
    }
    if (!this.state.inProgress) {
      return (
        <div style={{display: "table-cell"}}>
          <input
            type="file"
            ref={el => {
              this.uploadFileInput = el
            }}
            accept={accept}
            style={{ display: "none" }}
            onClick={this.onClickUpload}
            onChange={this.onChangeUpload}
            disabled={disabled}
          />
          {this.state.error && <small className="has-text-danger">{this.state.error}</small>}
          {responseOrQuestion ? (
            <div
              className={`file has-name ${disabled ? "disabled" : ""}`}
              style={{ cursor: disabled ? "not-allowed" : "pointer" }}
              onClick={this.clickUpload}
            >
              <label className="file-label">
                <span className="file-cta">
                  <span className="file-icon" >
                    <i className="fas fa-paperclip"/>
                  </span>
                </span>
              </label>
            </div>
          ) : (
            <div autoFocus tabIndex={0}
              className={`file is-small ${disabled ? "disabled" : ""}`}
              style={{ cursor: disabled ? "not-allowed" : "pointer" }}
              onClick={this.clickUpload}
            >
              <span className="file-cta" title="Upload new version">
                <span className="file-icon pointer">
                  <i className="fas fa-upload" />
                </span>
                <span className="file-label">
                  {docId ? "" : "Choose a file…"}
                </span>
              </span>
            </div>
          )}
          {showFeedback && this.state.message ? (
            <div>
              <br />
              <p className="bg-success">{this.state.message}</p>
            </div>
          ) : (
            undefined
          )}
        </div>
      )
    }
    return <Loader />
  }
}

export default DocUpload
