import React, { Component } from "react"

import Box from "grommet/components/Box"
import Table from "grommet/components/Table"
import TableRow from "grommet/components/TableRow"

import { FormControl, Modal, Button, ControlLabel } from "react-bootstrap"

import { eSignTableHeaders } from "../utils/config"
import { b64toBlob } from "../utils/helper_functions"

class DocuSignForm extends Component {
  constructor(props) {
    super(props)
    this.state = { showModal: true, signerEmail: "", emailSubject: "",
      emailBlurb: "", error: "" }
    this.hideDocDetails = this.hideDocDetails.bind(this)
    this.changeSignerEmail = this.changeSignerEmail.bind(this)
    this.changeEmailSubject = this.changeEmailSubject.bind(this)
    this.changeEmailBlurb = this.changeEmailBlurb.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  hideDocDetails() {
    // this.setState({ showModal: false });
    this.setState({ showModal: false }, this.props.closeDSDetails)
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.showDSModal && !this.state.showModal) {
      this.setState({ showModal: true, error: "" })
    }
  }

  changeSignerEmail(event) {
    this.setState({ signerEmail: event.target.value, error: "" })
  }

  changeEmailSubject(event) {
    this.setState({ emailSubject: event.target.value, error: "" })
  }

  changeEmailBlurb(event) {
    this.setState({ emailBlurb: event.target.value, error: "" })
  }

  onSubmit() {
    const { signerEmail, emailSubject, emailBlurb } = this.state

    if(!signerEmail || !emailSubject || !emailBlurb) {
      this.setState({ error: "Incomplete options provided"})
      return
    }

    this.props.updateDocuSignOptions({ signerEmail, emailSubject, emailBlurb })
    this.setState({ showModal: false }, this.props.submitDocuSignForm)
  }

  downloadFile(name, data) {
    this.downloadAnchor.download = `${name  }.pdf`
    // console.log("csv : ", csv);
    const blob = b64toBlob(data, "application/pdf")
    // let blob = new Blob(new Buffer(data, 'base64'));
    // console.log("blob : ", blob);
    this.downloadAnchor.href = window.URL.createObjectURL(blob)
    // console.log("anchor : ", this.downloadAnchor);
    this.downloadAnchor.click()
  }

  renderTableHeaders(eSign) {
    const headers = Object.keys(eSign).map((h, i) => {
      const field = eSignTableHeaders.filter(e => e.name === h)[0]
      if(field && eSign[h]) {
        return <th key={h+i}><ControlLabel>{field.label}</ControlLabel></th>
      }
    })
    if(eSign.fileData) {
      headers.push(
        <th key="download-h"><ControlLabel>"Download"</ControlLabel></th>
      )
    }
    return headers
  }

  renderDocsRows(eSign) {
    const cells = Object.keys(eSign).map((c, i) => {
      const field = eSignTableHeaders.filter(e => e.name === c)[0]
      if(field && eSign[c]) {
        return (
          <td key={c+i}>
            {eSign[c]}
          </td>
        )
      }
    })
    if(eSign.fileData) {
      const fileName = this.props.fileName
      cells.push(
        <td key="download-d">
          <i className="fa fa-download link-pointer"
            onClick={this.downloadFile.bind(this, fileName, eSign.fileData)} />
        </td>
      )
    }
    return (
      <TableRow>
        {cells}
      </TableRow>
    )
  }

  renderStatus() {
    const eSign = this.props.eSign
    if(eSign) {
      return (
        <Table responsive>
          <thead>
            <tr>
              {this.renderTableHeaders(eSign)}
            </tr>
          </thead>
          <tbody>
            {this.renderDocsRows(eSign)}
          </tbody>
        </Table>
      )
    } 
    return <p className="bg-info text-center">None</p>
    

  }

  render() {
    return (
      <Modal show={this.state.showModal} onHide={this.hideDocDetails}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.fileName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Box direction="column"
            colorIndex="light-2"
            align="stretch"
            justify="center"
            margin="small"
            pad={{"between": "small"}}>
            <Box direction="column"
              colorIndex="light-1"
              align="center"
              justify="center"
              wrap
              pad="small" >
              <strong className="text-center">Past requests</strong>
              <br />
              {this.renderStatus()}
              <br />
              <strong className="text-center">Send a new signature request</strong>
              <br />
              <FormControl type="email"
                value={this.state.signerEmail}
                placeholder="Signer Email"
                onChange={this.changeSignerEmail} />
              <FormControl type="email"
                value={this.state.emailSubject}
                placeholder="Email Subject"
                onChange={this.changeEmailSubject} />
              <FormControl componentClass="textarea"
                value={this.state.emailBlurb}
                placeholder="Email Message"
                onChange={this.changeEmailBlurb} />
              <Button onClick={this.onSubmit}>Submit</Button>
              {this.state.error ?
                <p className="text-center bg-danger">{this.state.error}</p>
                : undefined
              }
              <a  className="hidden-download-anchor"
                ref={ el => { this.downloadAnchor = el } }
                href="#" download />
            </Box>
          </Box>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.hideDocDetails}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default DocuSignForm
