import React, { Component } from "react"
import axios from "axios"
import moment from "moment"

import { muniApiBaseURL } from "GlobalUtils/consts"
import { Modal } from "Global/BulmaModal"
import {getToken} from "../../../globalutilities/helpers";

class DocDetails extends Component {
  constructor(props) {
    super(props)
      this.state = { modalState: this.props.showModal, deletedVersion: "", error: "" }
    this.toggleModal = this.toggleModal.bind(this)
  }

  onClickDownload(versionId) {
    console.log("in onClickDownload : ", versionId)
    const { doc, bucketName, contextId, contextType, tenantId } = this.props
    if(!bucketName || (!doc && !doc.name) || !versionId) {
      this.setState({ error: "No bucket/file name/versionId provided" })
      return
    }

    let fileName = doc.name
    if(tenantId && contextId && contextType) {
      fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const opType = "download"
    const options = { versionId }

    axios.post(`${muniApiBaseURL}s3/get-signed-url`, { opType, bucketName, fileName, options }, {headers:{"Authorization":getToken()}})
      .then(res => {
        console.log("res : ", res)
        this.setState({ error: "" })
        this.downloadFile(res.data.url)
      })
      .catch(err => {
        console.log("err : ", err)
        this.setState({ error: "Error in getting signed url for download" })
      })
  }

  onClickDelete(versionId) {
    console.log("in onClickDelete : ", versionId)
    const { doc, bucketName, contextId, contextType, tenantId } = this.props
    if(!bucketName || (!doc && !doc.name) || !versionId) {
      this.setState({ error: "No bucket/file name/versionId provided", deletedVersion: "" })
      return
    }

    let fileName = doc.name
    if(tenantId && contextId && contextType) {
      fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const files = [{ Key: fileName, VersionId: versionId }]
    axios.post(`${muniApiBaseURL}s3/delete-s3-object`, { bucketName, files }, {headers:{"Authorization":getToken()}})
      .then(res => {
        console.log("res : ", res)
        if(res.error) {
          const error = `Error in deleting ${versionId}`
          this.setState({ error, deletedVersion: "" })
        } else if(doc.meta.versions.length > 1) {
          const updatedDoc = { ...doc }
          updatedDoc.meta = { ...doc.meta }
          updatedDoc.meta.versions = doc.meta.versions.filter(v => v.versionId !== versionId)
          axios.put(`${muniApiBaseURL}doc/${doc._id}`, updatedDoc)
            .then(res => {
              console.log("version removed ok : ", res)
              this.setState({ error: "", deletedVersion: versionId })
            }).catch(err => {
              console.log("err in deleting version : ", err)
              this.setState({ error: "err in deleting version", deletedVersion: "" })
            })
        } else {
          axios.delete(`${muniApiBaseURL}/doc/${doc._id}`)
            .then(res => {
              console.log("document removed ok : ", res)
              this.setState({ error: "", deletedVersion: versionId })
            })
            .catch(err => {
              console.log("err in deleting document : ", err)
              this.setState({ error: "err in deleting document" })
            })
        }
      }).catch(err => {
        console.log("err : ", err)
        this.setState({ error: "Error!!", deletedVersion: "" })
      })
  }

  downloadFile(signedS3Url) {
    const { downloadAnchor } = this.props
    // console.log("anchor : ", downloadAnchor);
    downloadAnchor.href = signedS3Url
    // console.log("anchor : ", downloadAnchor);
    downloadAnchor.click()
  }

  toggleModal() {
    this.setState((prev) => {
      const newState = !prev.modalState
      return { modalState: newState }
    }, this.props.closeDocDetails)
  }

  render() {
    const { doc } = this.props
    if(doc && doc.name) {
      return (
        <Modal
          closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title={doc.name}>
          <div>
            <span><strong>type : </strong></span>
            <span>{doc.meta.type}</span>
          </div>
          <div>
            <span><strong>status : </strong></span>
            <span>{doc.status}</span>
          </div>
          <br />
          <div>
            {doc.meta.versions.map((v, i) => (
              <div key={i}>
                <div>
                  <span><strong>version : </strong></span>
                  <span>{v.versionId} </span>
                  <div>
                    <span>
                      <strong>Actions : </strong>
                      <span className="btn btn-default link-pointer">
                        <i className="fa fa-download"
                          onClick={this.onClickDownload.bind(this, v.versionId)}  /></span>
                      <span className="btn btn-danger link-pointer">
                        <i className="fa fa-trash-o"
                          onClick={this.onClickDelete.bind(this, v.versionId)}  /></span>
                    </span>
                  </div>
                </div>
                <div>
                  <span><strong>size : </strong></span>
                  <span>{v.size}</span>
                </div>
                <div>
                  <span><strong>upload date : </strong></span>
                  <span>{moment(v.uploadDate).toString()}</span>
                </div>
                <br />
              </div>
            ))}
            {this.state.error ?
              <p className="bg-danger">{this.state.error}</p>
              : undefined
            }
          </div>
        </Modal>
      )
    }
    return null
  }
}

export default DocDetails
