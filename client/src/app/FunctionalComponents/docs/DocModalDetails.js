import React, { Component } from "react"
import axios from "axios"
// import moment from "moment"

import { getDocDetails, getToken, getFile } from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts"
import { Modal } from "Global/BulmaModal"
import Loader from "Global/Loader"

class DocModalDetails extends Component {
  constructor(props) {
    super(props)
    this.state = { modalState: this.props.showModal, deleteVersion: "",
      error: "", doc: {}, waiting: true, objectName: "", versionId: "" }
    this.toggleModal = this.toggleModal.bind(this)
    this.deleteDocVersion = this.deleteDocVersion.bind(this)
    this.cancelDelete = this.cancelDelete.bind(this)
  }

  async componentDidMount() {
    await this.getDocInfo(this.props.docId)
  }

  async componentWillReceiveProps(nextProps) {
    if(nextProps.docId !== this.props.docId) {
      await this.getDocInfo(nextProps.docId)
    }
  }

  onClickDownload(versionId, name) {
    console.log("in onClickDownload : ", versionId, name)
    const { doc } = this.state
    const { contextId, contextType, tenantId } = doc
    const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
    this.setState({ versionId, objectName }, this.downloadFile)
  }

  onClickDelete(versionId, name) {
    const { doc } = this.state
    const { contextId, contextType, tenantId } = doc
    const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
    this.setState({ deleteVersion: versionId, objectName })
  }

  getUploadDate(v) {
    const { uploadDate, name } = v
    console.log("uploadDate : ", uploadDate)
    if(uploadDate && new Date(uploadDate).toLocaleString() !== "Invalid Date") {
      return new Date(uploadDate)
    }
    const extnIdx = name.lastIndexOf(".")
    const name1 = name.substr(0, extnIdx)
    const idx1 = name1.lastIndexOf("_")
    const timestamp = name1.substr(idx1+1)
    if(timestamp) {
      return new Date(+timestamp)
    }
    return ""
  }

  async getDocInfo(_id) {
    if(!_id) {
      _id = this.props.docId
    }
    const res = await getDocDetails(_id)
    console.log("res : ", res)
    const [error, doc] = res
    if(error) {
      this.setState({ waiting: false, error })
    } else {
      const { contextId, contextType, tenantId, name } = doc
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      this.setState({ doc, error: "", objectName, waiting: false, fileName: name })
    }
  }

  downloadFile() {
    this.downloadAnchor.click()
  }

  cancelDelete() {
    this.setState({ deleteVersion: "" })
  }

  deleteDocVersion() {
    const versionId = this.state.deleteVersion
    console.log("in onClickDelete : ", versionId)
    const { doc, objectName } = this.state
    const { bucketName, documentId } = this.props
    if((!doc && !doc.name) || !versionId) {
      this.setState({ error: "No bucket/file name/versionId provided", deleteVersion: "" })
      return
    }

    const { contextId, contextType, tenantId } = doc

    this.setState({ waiting: true })

    // let fileName = doc.name
    // if(tenantId && contextId && contextType) {
    //   fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    // }
    let files
    if(versionId === "All") {
      files = doc.meta.versions.map(e => ({
        Key: `${tenantId}/${contextType}/${contextType}_${contextId}/${e.name}`,
        VersionId: e.versionId
      }))
    } else {
      files = [{ Key: objectName, VersionId: versionId }]
    }

    axios.post(`${muniApiBaseURL}s3/delete-s3-object`, { bucketName, files }, {headers:{"Authorization":getToken()}})
      .then(res => {
        console.log("res : ", res)
        if(res.error) {
          const error = `Error in deleting ${versionId}`
          this.setState({ error, deleteVersion: "" })
        } else if(doc.meta.versions.length > 1 && versionId !== "All") {
          // const updatedDoc = { ...doc }
          // updatedDoc.meta = { ...doc.meta }
          // updatedDoc.meta.versions = doc.meta.versions.filter(v => v.versionId !== versionId)
          // axios.put(`${muniApiBaseURL}docs/${doc._id}`, updatedDoc)
          //   .then(res => {
          //     console.log("version removed ok : ", res)
          //     this.setState({ error: "", deleteVersion: "", waiting: false })
          //     if(doc.meta.versions.length === 1) {
          //       this.setState({ error: "", deleteVersion: "", waiting: false }, this.toggleModal)
          //     } else {
          //       this.setState({ error: "", deleteVersion: "" }, this.getDocInfo)
          //     }
          //   }).catch(err => {
          //     console.log("err in deleting version : ", err)
          //     this.setState({ error: "err in deleting version", deleteVersion: "" })
          //   })
          axios.post(`${muniApiBaseURL}docs/update-versions`, { _id: doc._id,
            versions: [versionId], option: "remove"},{headers:{"Authorization":getToken()}})
            .then(res => {
              console.log("version removed ok : ", res)
              this.setState({ error: "", deleteVersion: "", waiting: false }, this.getDocInfo)
            }).catch(err => {
              console.log("err in deleting version : ", err)
              this.setState({ error: "err in deleting version", deleteVersion: "" })
            })
        } else {
          axios.delete(`${muniApiBaseURL}docs/${doc._id}`,{headers:{"Authorization":getToken()}})
            .then(res => {
              console.log("document removed ok : ", res)
              this.setState({ error: "", deleteVersion: "", waiting: false }, this.toggleModal)
              this.props.onDeleteAll(doc._id, documentId, doc.originalName || "")
            })
            .catch(err => {
              console.log("err in deleting document : ", err)
              this.setState({ error: "err in deleting document", waiting: false })
            })
        }
      }).catch(err => {
        console.log("err : ", err)
        this.setState({ error: "Error!!", deleteVersion: "", waiting: false })
      })
  }

  toggleModal() {
    this.setState((prev) => {
      const newState = !prev.modalState
      return { modalState: newState }
    }, this.props.closeDocDetails)
  }

  render() {
    const { deleteVersion, waiting, error, doc, versionId, fileName, objectName } = this.state
    const { docMetaToShow, versionMetaToShow, nav1 } = this.props
    const isTransaction = nav1 === "loan" || nav1 === "derivative" || nav1 === "marfp" || nav1 === "others" || nav1 === "deals"
    const tranFirmId = nav1 === "deals" ? "dealIssueTranClientId" : (nav1 === "loan" || nav1 === "derivative" || nav1 === "marfp" || nav1 === "others") ? "actTranFirmId" : ""
    const userFirmLogin = isTransaction && this.props.tranAction.canEditDocument && this.props.transaction[tranFirmId] === this.props.user.entityId
    const thirdPartyLogin = isTransaction && this.props.tranAction.canEditDocument && this.props.user.userId === this.props.doc.createdBy
    if(waiting) {
      return (
        <Modal closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title={doc.name}>
          <Loader />
        </Modal>
      )
    }
    if(deleteVersion) {
      return (
        <Modal
          closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title={doc.name}>
          <strong>Delete this version</strong>
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small"
                onClick={this.deleteDocVersion}>Yes</button>
            </div>
            <div className="control">
              <button className="button is-link is-small"
                onClick={this.cancelDelete}>No</button>
            </div>
          </div>
        </Modal>
      )
    }
    if(doc && doc.name) {
      const docVersions = [ ...doc.meta.versions ]
      docVersions.sort((a, b) => {
        const date1 = this.getUploadDate(a)
        const date2 = this.getUploadDate(b)
        return (new Date(date2) - new Date(date1))
      })
      return (
        <Modal
          closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title={doc.originalName}>
          <div>
            {/* <span><strong>File Type : </strong></span>
            <span>{doc.meta.type}</span> */}
            {docMetaToShow && docMetaToShow.length ?
              docMetaToShow.map((e, i) => {
                if(doc.meta && doc.meta[e]) {
                  return (
                    <span key={e+i}>
                      <strong>{` ${e} : `}</strong>
                      {doc.meta[e]}
                    </span>
                  )
                }
              })
              : undefined
            }
          </div>
          {/* {userFirmLogin || thirdPartyLogin ?
            <div>
              <span><strong>Delete All Versions : </strong></span>
              <span className="icon has-text-danger">
                <i className="fa fa-trash-o"
                  onClick={this.onClickDelete.bind(this, "All")}  />
              </span>
            </div>
            : !isTransaction ?
              <div>
                <span><strong>Delete All Versions : </strong></span>
                <span className="icon has-text-danger">
                  <i className="fa fa-trash-o"
                    onClick={this.onClickDelete.bind(this, "All")}  />
                </span>
              </div> : null
          } */}
          {doc.status &&
            <div>
              <span><strong>status : </strong></span>
              <span>{doc.status}</span>
            </div>
          }
          <br />
          <div>
            {docVersions.map((v, i) => {
              const uploadDate = this.getUploadDate(v)
              return (
                <div key={i}>
                  <div>
                    <span><strong>version : </strong></span>
                    <span>{v.versionId} </span>
                    <span><strong>Name : </strong></span>
                    <span>{v.originalName} </span>
                    <div className="field is-grouped">
                      <span>
                        <strong>Actions : </strong>
                        <span className="icon has-text-info">
                          <i className="fa fa-download"
                            onClick={this.onClickDownload.bind(this, v.versionId, v.name)} title="Download" /></span>
                        {/* {userFirmLogin || thirdPartyLogin ?
                          <span className="icon has-text-danger">
                            <i className="fa fa-trash-o"
                              onClick={this.onClickDelete.bind(this, v.versionId, v.name)}  /></span> :
                          !isTransaction ?
                            <span className="icon has-text-danger">
                              <i className="fa fa-trash-o"
                                onClick={this.onClickDelete.bind(this, v.versionId, v.name)}  />
                            </span> : null
                        } */}
                      </span>
                    </div>
                  </div>
                  <div>
                    <span><strong>size : </strong></span>
                    <span>{v.size}</span>
                  </div>
                  {
                    uploadDate &&
                  <div>
                    <span><strong>upload date : </strong></span>
                    <span>{this.getUploadDate(v).toLocaleString()}</span>
                  </div>
                  }
                  {versionMetaToShow && versionMetaToShow.length ?
                    <div>
                      {versionMetaToShow.map((e, i) => {
                        if(v && v[e]) {
                          return (
                            <span key={e+i}>
                              <strong>{` ${e} : `}</strong>
                              {v[e]}
                            </span>
                          )
                        }
                      })}
                    </div>
                    : undefined
                  }
                  <br />
                </div>
              )})}
            {error ?
              <p className="bg-danger">{error}</p>
              : undefined
            }
          </div>
          <a  className="hidden-download-anchor"
            style={{ display: "none" }}
            ref={ el => { this.downloadAnchor = el } }
            onClick={() => getFile(`?objectName=${objectName}&versionId=${versionId}`, fileName)}
            download={fileName} >
              hidden
          </a>
        </Modal>
      )
    }
    return null
  }
}

export default DocModalDetails
