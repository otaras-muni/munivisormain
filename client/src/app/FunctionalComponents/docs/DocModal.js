import React, { Component } from "react"
import DocModalDetails from "./DocModalDetails"
import ShareDocModalDetails from "./ShareDocModalDetails"

export class DocModal extends Component {
  constructor(props) {
    super(props)
    this.state = { showModal: false }
    this.showDocDetails = this.showDocDetails.bind(this)
    this.closeDocDetails = this.closeDocDetails.bind(this)
  }

  showDocDetails() {
    const { selectedDocId } = this.props
    this.setState({ showModal: true, selectedDocId })
  }

  closeDocDetails() {
    this.setState({ showModal: false, selectedDocId: "" })
  }

  render() {
    const { showModal, selectedDocId } = this.state
    if(showModal) {
      return (
        <div>
          { this.props.myDocuments ?
            <ShareDocModalDetails showModal={this.state.showModal}
              closeDocDetails={this.closeDocDetails}
              documentId={this.props.documentId || ""}
              onDeleteAll={this.props.onDeleteAll}
              docMetaToShow={this.props.docMetaToShow}
              versionMetaToShow={this.props.versionMetaToShow}
              transaction={this.props.transaction}
              tranAction={this.props.tranAction}
              user={this.props.user}
              nav1={this.props.nav1}
              doc={this.props.doc}
              isDisable={this.props.isDisable}
              svControls={this.props.svControls}
              contextType={this.props.contextType}
              docId={selectedDocId}/>
            :
            <DocModalDetails showModal={this.state.showModal}
              closeDocDetails={this.closeDocDetails}
              documentId={this.props.documentId || ""}
              onDeleteAll={this.props.onDeleteAll}
              docMetaToShow={this.props.docMetaToShow}
              versionMetaToShow={this.props.versionMetaToShow}
              transaction={this.props.transaction}
              tranAction={this.props.tranAction}
              user={this.props.user}
              nav1={this.props.nav1}
              doc={this.props.doc}
              docId={selectedDocId}/>
          }
        </div>
      )
    }
    return (
      <div>
        <span className="icon has-text-info" style={{ cursor: "pointer" }} title="Show version">
          <i className="fas fa-plus link-pointer "
            onClick={this.showDocDetails} />
        </span>
      </div>
    )
  }
}

export default DocModal
