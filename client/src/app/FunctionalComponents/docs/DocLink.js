import React, { Component } from "react"
import { muniApiBaseURL } from "GlobalUtils/consts"
import { getDocDetails, getFile, getPlateFormDocDetails } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/LineLoader"

class DocLink extends Component {
  constructor(props) {
    super(props)
    this.state = { fileName: "", waiting: true, error: "", objectName: "" }
  }

  async componentDidMount() {
    await this.getDocInfo(this.props.docId)
  }

  async componentWillReceiveProps(nextProps) {
    if((nextProps.docId !== this.props.docId) ||
      (nextProps.name !== this.props.name)) {
      await this.getDocInfo(nextProps.docId)
    }
  }

  async getDocInfo(_id) {
    const { plateForm } = this.props
    let res = {}
    if(plateForm){
      res = await getPlateFormDocDetails(_id)
    } else {
      res = await getDocDetails(_id)
    }
    console.log("res : ", res)
    const [error, doc] = res
    if(error) {
      this.setState({ error, waiting: false })
    } else {
      let err = ""
      let objectName = ""
      const { contextId, contextType, tenantId, name } = doc
      if(plateForm && tenantId && contextType && name && contextType === "INVOICES") {
        objectName = `${contextType}/${name}`
      } else if(tenantId && contextId && contextType && name) {
        objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      } else {
        err = "invalid doc details"
      }
      this.setState({ fileName: doc.originalName, waiting: false, error: err, objectName })
    }
  }

  render() {
    const { fileName, waiting, error, objectName } = this.state
    const { plateForm } = this.props

    return (
      <div className="multiExpTblVal">

        { fileName && plateForm ?

          <a
            className="fa fa-download"
            onClick={() => getFile(`?objectName=${encodeURIComponent(objectName)}`, fileName)}
            title="download"
          /> :
          <a
            onClick={() => getFile(`?objectName=${encodeURIComponent(objectName)}`, fileName)}
            download={fileName}>{fileName}</a>
        }

        {(waiting || !fileName) && <span><Loader /></span>}
        {error && <strong>{error}</strong>}
      </div>
    )
  }
}

export default DocLink
