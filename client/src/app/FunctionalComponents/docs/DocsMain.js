import React, { Component } from "react"
import axios from "axios"
import moment from "moment"
import { connect } from "react-redux"

import { getDocs } from "../../StateManagement/actions"

import CONST, { docTableHeaders, muniApiBaseURL } from "GlobalUtils/consts"
import  { getToken } from "GlobalUtils/helpers"
import DocDetails from "./DocDetails"
import DocUpload from "./DocUpload"
// import DocuSignForm from "./DocuSignForm"

const arraysWithSameElements = (arr1, arr2) => {
  // console.log("arr1 : ", arr1);
  // console.log("arr2 : ", arr2);
  if(!arr1 || !arr2) {
    return false
  }
  if(arr1.length !== arr2.length) {
    return false
  }
  let res = true
  arr1.some(e => {
    if(!arr2.includes(e)) {
      res = false
      return true
    }
  })
  // console.log("res : ", res);
  return res
}

class DocsMain extends Component {
  constructor(props) {
    super(props)
    this.state = { bucketName: CONST.bucketName, fileName: "", signedS3Url: "",
      showModal: false, showDSModal: false, selectedDocId: "", showSigneeInput: "",
      waiting: false }
    this.downloadFile = this.downloadFile.bind(this)
    this.closeDocDetails = this.closeDocDetails.bind(this)
    this.closeDSDetails = this.closeDSDetails.bind(this)
    this.docuSignOptions = { signerEmail: "", emailSubject: "", emailBlurb: "" }
  }

  componentDidMount() {
    this.props.getDocs()
  }

  componentWillReceiveProps(nextProps) {
    const prevDocs = this.props.docs.map(d => d._id)
    const nextDocs = nextProps.docs.map(d => d._id)
    if(!arraysWithSameElements(prevDocs, nextDocs)) {
      this.props.getDocs()
    }
  }

  onClickShowSigneeInput(_id) {
    this.setState({ showSigneeInput: _id, showDSModal: true })
  }

  onClickDownload(fileName) {
    this.getS3FileGetURL(fileName, this.downloadFile)
  }

  onClickGetSignatures(fileName) {
    // this.setState({ waiting: true })
    // this.getS3FileGetURL(fileName, this.docuSign)
  }

  getS3FileGetURL(fileName, callback) {
    const { bucketName } = this.state
    console.log("====>>>",bucketName)
    if(!bucketName || !fileName) {
      this.setState({ signedS3Url: "", showModal: false, showDSModal: false, signerEmail: "",
        showSigneeInput: "", error: "No bucket/file name provided", waiting: false })
      return
    }
    const { contextId, contextType, tenantId } = this.props
    if(tenantId && contextId && contextType) {
      fileName = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const opType = "download"
    axios.post(`${muniApiBaseURL}/s3/get-signed-url`, { opType, bucketName, fileName }, {headers:{"Authorization":getToken()}})
      .then(res => {
        const signedS3Url = res.data.url
        console.log("res : ", signedS3Url)
        this.setState({ signedS3Url, showModal: false, showDSModal: false, error: "", fileName: "" },
          callback.bind(this, bucketName, fileName))
      })
      .catch(err => {
        console.log("err : ", err)
        this.setState({ signedS3Url: "", showModal: false, showDSModal: false, signerEmail: "",
          showSigneeInput: "", error: "Error!!", waiting: false })
      })
  }

  // docuSign(bucketName, fileName) {
  //   if (!bucketName || !fileName) {
  //     console.log("no bucketName or fileName")
  //     return
  //   }
  //   console.log("bucketName : ", bucketName)
  //   console.log("fileName : ", fileName)
  //   console.log("docuSignOptions : ", this.docuSignOptions)
  //
  //   const docId = this.state.showSigneeInput
  //   const { signerEmail, emailSubject, emailBlurb } = this.docuSignOptions
  //   if(!signerEmail || !emailSubject || !emailBlurb) {
  //     console.log("Incomplete info")
  //     return
  //   }
  //   this.docuSignOptions.replyEmailAddressOverride = Meteor.user().emails[0].address
  //   this.docuSignOptions.replyEmailNameOverride = this.props.myProfile.name
  //
  //   Meteor.call("request_signature_on_document", bucketName, fileName, this.docuSignOptions, (err, res) => {
  //     if(err) {
  //       console.log("err : ", err)
  //       this.setState({ signerEmail: "", showSigneeInput: "", waiting: false })
  //     } else {
  //       console.log("res : ", res)
  //       const { envelopeId, status, statusDateTime } = res
  //       let sent = null
  //       if(status === "sent") {
  //         sent = statusDateTime
  //       }
  //       const eSign = { envelopeId, status, statusDateTime, sent, signerEmail }
  //       const meta = { eSign }
  //
  //       Meteor.call("docs.update", docId, { meta }, (err1, res1) => {
  //         if(err1) {
  //           console.log("err in updating in docs db : ", err1)
  //           this.setState({ signerEmail: "", showSigneeInput: "", waiting: false })
  //         } else {
  //           console.log("updated ok in docs db : ", res1)
  //           this.setState({ signerEmail: "", showSigneeInput: "", waiting: false })
  //         }
  //       })
  //     }
  //   })
  // }

  downloadFile() {
    // console.log("anchor : ", this.downloadAnchor);
    this.downloadAnchor.click()
  }

  updateDocuSignOptions(options) {
    console.log("options : ", options)
    this.docuSignOptions = options
  }

  showDocDetails(selectedDocId) {
    this.setState({ showModal: true, showDSModal: false, selectedDocId })
  }

  closeDocDetails() {
    this.setState({ showModal: false, showDSModal: false, selectedDocId: "" })
  }

  closeDSDetails() {
    this.setState({ showModal: false, showDSModal: false, showSigneeInput: "" })
  }

  renderTableHeaders() {
    return this.props.rowCells.map((h, i) => {
      const field = docTableHeaders.filter(e => e.name === h)[0]
      if(field) {
        return <td key={h+i}><strong>{field.label}</strong></td>
      }
    })
  }

  renderDocRowCells(data, field) {
    switch(field) {
    case "name":
      return data.name
    case "uploadDate":
      return moment(data.meta.versions[0].uploadDate).toString()
    case "download":
      return (
        <i className="fa fa-download link-pointer"
          onClick={this.onClickDownload.bind(this, data.name)} />
      )
    case "docuSign":
      return (
        <div>
          <i className="fa fa-pencil-square-o link-pointer"
            onClick={this.onClickShowSigneeInput.bind(this, data._id)} />
          {/* {this.state.showSigneeInput === data._id ?
            <DocuSignForm fileName={data.name}
              showDSModal={this.state.showDSModal}
              closeDSDetails={this.closeDSDetails}
              eSign={data.meta.eSign}
              submitDocuSignForm={this.onClickGetSignatures.bind(this, data.name)}
              updateDocuSignOptions={this.updateDocuSignOptions.bind(this)} />
            : undefined
          } */}
        </div>
      )
    case "share":
      return (
        <i className="fa fa-share-alt" />
      )
    case "details":
      return (
        <i className="fa fa-plus link-pointer"
          onClick={this.showDocDetails.bind(this, data._id)} />
      )
    default:
      return undefined
    }
  }

  renderDocsRows() {
    if(this.props.rowCells && this.props.rowCells.length) {
      let docs = [...this.props.docs]
      const { tenantId, contextType, contextId, fileName } = this.props
      if(tenantId && contextId && contextType && fileName) {
        docs = docs.filter(d => d.tenantId === tenantId && d.contextType === contextType && d.contextId === contextId && d.name === fileName)
      }
      if(docs && docs.length) {
        return docs.map((d, i) => (
          <tr key={i}>
            {this.props.rowCells.map((c, j) => (
              <td key={c+i+j}>
                {this.renderDocRowCells(d, c)}
              </td>
            ))}
          </tr>
        ))
      }
    }
  }

  render() {
    if(this.state.waiting) {
      return (
        <h4>Loading!!</h4>
      )
    }
    return (
      <div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              {this.renderTableHeaders()}
            </tr>
          </thead>
          <tbody>
            {this.renderDocsRows()}
          </tbody>
        </table>
        {this.props.showUploadButton ?
          <DocUpload bucketName={this.state.bucketName}
            onUploadSuccess={this.props.getDocs.bind(this)}
            showFeedback />
          : true
        }
        <a  className="hidden-download-anchor"
          style={{ display: "none" }}
          ref={ el => { this.downloadAnchor = el } }
          href={this.state.signedS3Url} download>
            click here to download S3 file securely
        </a>
        {this.state.error ?
          <p className="bg-danger">{this.state.error}</p>
          : undefined
        }
        {this.state.showModal ?
          <DocDetails showModal={this.state.showModal}
            contextType={this.props.contextType}
            tenantId={this.props.tenantId}
            contextId={this.props.contextId}
            closeDocDetails={this.closeDocDetails}
            bucketName={this.state.bucketName}
            downloadAnchor={this.downloadAnchor}
            doc={this.props.docs.filter(d => d._id === this.state.selectedDocId)[0]}/>
          : undefined
        }
      </div>
    )

  }
}

const mapStateToProps = ({ docs }) => {
  console.log("docs in state : ", docs)
  return { docs }
}

export default connect(mapStateToProps, { getDocs })(DocsMain)
