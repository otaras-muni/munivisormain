import React from "react"
import { DropdownList } from "react-widgets"

const LinkCusip = props => (
  <article
    className={props.expanded.linkcusip ? "accordion is-active" : "accordion"}
  >
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "linkcusip")
        }}
      >
        Link Cusip
      </p>
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={event =>
              props.addLinkCusip(event, "linkcusip", [...props.linkCusip])
            }
          >
            {" "}
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={event => props.resetLinkCusip(event, "linkcusip")}
          >
            {" "}
            Reset
          </button>
        </div>
      </div>
    </div>
    {props.expanded.linkcusip && (
      <div className="accordion-body">
        <div className="accordion-content">
          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <thead>
              <tr>
                <th>
                  <p className="multiExpLbl">Debt Type</p>
                </th>
                <th>
                  <p className="multiExpLbl">Cusip</p>
                </th>
                <th>
                  <p className="multiExpLbl">Delete</p>
                </th>
              </tr>
            </thead>
            <tbody>
              {props.linkCusip.map((item, idx) => (
                <tr key={30 * idx}>
                  <td className="multiExpTblVal">
                    <div className="control">
                      <DropdownList
                        filter
                        value={item.debtType}
                        data={
                          props.linkCusipDebType
                            ? props.linkCusipDebType[1]
                            : []
                        }
                        message="Select Debt Type"
                        busy={props.busy}
                        busySpinner={
                          <span className="fas fa-sync fa-spin is-link" />
                        }
                        onChange={val => {
                          const event = {
                            target: {
                              name: "debtType",
                              value: val
                            }
                          }
                          props.onChangeLinkCusip(
                            event,
                            idx,
                            [],
                            "entityLinkedCusips"
                          )
                        }}
                      />
                      {props.entityLinkedCusipsErrors &&
                        props.entityLinkedCusipsErrors[idx].debtType && (
                          <small className="text-error">
                            {props.entityLinkedCusipsErrors[idx].debtType}
                          </small>
                        )}
                    </div>
                  </td>
                  <td className="multiExpTblVal">
                    <div>
                      <input
                        className="input is-small is-link"
                        style={{ textTransform: "uppercase" }}
                        type="text"
                        placeholder="Cusip-6"
                        name="associatedCusip6"
                        value={item.associatedCusip6}
                        onChange={event => {
                          event.target.value = event.target.value.toUpperCase()
                          if (event.target.value.length > 6) {
                            return false
                          }
                          props.onChangeLinkCusip(
                            event,
                            idx,
                            [],
                            "entityLinkedCusips"
                          )
                        }}
                      />
                      {props.entityLinkedCusipsErrors &&
                        props.entityLinkedCusipsErrors[idx]
                          .associatedCusip6 && (
                          <small className="text-error">
                            {
                              props.entityLinkedCusipsErrors[idx]
                                .associatedCusip6
                            }
                          </small>
                        )}
                    </div>
                  </td>
                  <td className="multiExpTblVal">
                    <div style={{ padding: "0.75rem" }}>
                      {idx >= 0 ? (
                        <span
                          className="has-text-link fa-delete"
                          style={{ cursor: "pointer" }}
                          onClick={() => {
                            props.deleteAddress(
                              event,
                              "entityLinkedCusips",
                              idx
                            )
                          }}
                        >
                          <i className="far fa-trash-alt" />
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {props.LinkCusipList.length > 0 && (
          <div className="box">
            <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                <tr>
                  <th>
                    <p className="multiExpLbl">Debt Type</p>
                  </th>
                  <th>
                    <p className="multiExpLbl">Cusip</p>
                  </th>
                  <th>
                    <p className="multiExpLbl">Edit/Delete</p>
                  </th>
                </tr>
              </thead>
              <tbody>
                {props.LinkCusipList.map((item, idx) => (
                  <tr key={20 * idx}>
                    <td className="multiExpTblVal">
                      <small>{item.debtType}</small>
                    </td>
                    <td className="multiExpTblVal">
                      <small>{item.associatedCusip6}</small>
                    </td>
                    <td className="multiExpTblVal">
                      <div className="field is-grouped">
                        <div
                          className="control"
                          onClick={event => {
                            props.updateLinkCusip(
                              event,
                              item._id,
                              "entityLinkedCusips"
                            )
                          }}
                        >
                          <a href="">
                            <span className="has-text-link">
                              <i className="fas fa-pencil-alt" />
                            </span>
                          </a>
                        </div>
                        <div
                          className="control"
                          onClick={event => {
                            props.deleteLinkCusip(
                              event,
                              item._id,
                              "entityLinkedCusips"
                            )
                          }}
                        >
                          <a href="">
                            <span className="has-text-link">
                              <i className="far fa-trash-alt" />
                            </span>
                          </a>
                        </div>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    )}
  </article>
)
export default LinkCusip
