import React from "react"
import { DropdownList } from "react-widgets"
import AsyncCreatableSelect from "react-select/lib/AsyncCreatable"

const FirmProfileForm = props => (
  <div className="accordion-body">
    <div className="accordion-content">
      {!props.updated && (
        <div className="columns">
          <div className="column is-size-5">
            <p>
              <input
                type="radio"
                name="clientType"
                value="Client"
                checked={props.clientType === "Client"}
                onChange={props.onChangeFirmDetail}
                disabled={props.readOnlyFeatures.clientTypeDisabled}
              />
              &nbsp;&nbsp;Client
            </p>
          </div>
          <div className="column is-size-5">
            <p>
              <input
                type="radio"
                name="clientType"
                value="Prospect"
                checked={props.clientType === "Prospect"}
                onChange={props.onChangeFirmDetail}
                disabled={props.readOnlyFeatures.clientTypeDisabled}
              />
              &nbsp;&nbsp;Prospect
            </p>
          </div>
        </div>
      )}
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Entity Type</p>
          <div className="control">
            <DropdownList
              filter
              value={
                props.firmDetails.entityType
                  ? {
                      label: props.firmDetails.entityType,
                      value: props.firmDetails.entityType
                    }
                  : props.entityTypeList[0]
              }
              data={props.entityTypeList}
              message="Entity Type"
              textField="label"
              valueField="value"
              defaultValue=""
              onChange={props.getFirmDetails}
              disabled={props.readOnlyFeatures.entityType}
              busy={props.busy}
              busySpinner={<span className="fas fa-sync fa-spin is-link" />}
            />
          </div>
          {props.errorFirmDetail && props.errorFirmDetail.entityType && (
            <small className="text-error">
              {props.errorFirmDetail.entityType}
            </small>
          )}
        </div>
        <div className="column">
          <p className="multiExpLbl">Entity Name</p>
          <div className="field">
            <AsyncCreatableSelect
              multi={false}
              cacheOptions={false}
              value={{ firmName: props.firmDetails.firmName }}
              onChange={val => {
                const event = {
                  target: {
                    name: "firmName",
                    value: val
                  }
                }
                props.onChangeFirmDetail(event)
              }}
              labelKey="firmName"
              loadOptions={props.getEntityFirmList}
              backspaceRemoves
              allowCreateWhileLoadingboolean={false}
              isDisabled={props.readOnlyFeatures.firmName}
            />
            {props.errorFirmDetail && props.errorFirmDetail.firmName && (
              <small className="text-error">
                {props.errorFirmDetail.firmName}
              </small>
            )}
          </div>
          {props.firmDetails.entityAliases &&
          props.firmDetails.entityAliases.length
            ? props.firmDetails.entityAliases.map((item, idx) => (
                <div className="field" key={idx}>
                  <div className="control d-flex">
                    <input
                      className="input is-small is-link"
                      name="entityAliases"
                      type="text"
                      placeholder="Enter Aliases"
                      value={item}
                      onChange={event => {
                        props.onChangeAliases(event, idx)
                      }}
                    />
                    <span
                      className="has-text-link fa-delete deleteadressaliases"
                      onClick={() => {
                        props.deleteAliases(idx)
                      }}
                    >
                      <i className="far fa-trash-alt" />
                    </span>
                  </div>
                  {props.errorFirmDetail &&
                    props.errorFirmDetail.entityAliases[idx] && (
                      <small className="text-error">
                        {props.errorFirmDetail.entityAliases[idx]}
                      </small>
                    )}
                </div>
              ))
            : ""}
          <div className="field is-grouped">
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "aliases")
                }}
              >
                Add Alias
              </button>
            </div>
            <div className="control">
              <button
                className="button is-light is-small"
                onClick={props.resetAliases}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Primary Sectors</p>
          <div className="field">
            <div className="control">
              <DropdownList
                filter
                value={props.firmDetails.primarySectors}
                data={
                  props.primarySectorsList[1]
                    ? [...props.primarySectorsList[1]]
                    : []
                }
                message="Select Primary Sectors"
                textField="label"
                valueField="value"
                defaultValue=""
                busy={props.busy}
                busySpinner={<span className="fas fa-sync fa-spin is-link" />}
                onChange={val => {
                  const event = {
                    target: {
                      name: "primarySectors",
                      value: val
                    }
                  }
                  props.onChangeFirmDetail(event)
                }}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.primarySectors && (
              <small className="text-error">
                {props.errorFirmDetail.primarySectors}
              </small>
            )}
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">Secondary Sectors</p>
          <div className="field">
            <div className="control">
              <DropdownList
                filter
                value={props.firmDetails.secondarySectors}
                data={
                  props.firmDetails.primarySectors &&
                  props.primarySectorsList[2] &&
                  props.primarySectorsList[2][props.firmDetails.primarySectors]
                    ? [
                        ...props.primarySectorsList[2][
                          props.firmDetails.primarySectors
                        ]
                      ]
                    : []
                }
                message="Secondary Sectors"
                textField="label"
                valueField="value"
                defaultValue=""
                busy={props.busy}
                busySpinner={<span className="fas fa-sync fa-spin is-link" />}
                onChange={val => {
                  const event = {
                    target: {
                      name: "secondarySectors",
                      value: val
                    }
                  }
                  props.onChangeFirmDetail(event)
                }}
              />
            </div>
            {props.errorFirmDetail &&
              props.errorFirmDetail.secondarySectors && (
                <small className="text-error">
                  {props.errorFirmDetail.secondarySectors}
                </small>
              )}
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">Tax ID</p>
          <div className="control">
            <input
              className="input is-small is-link"
              name="taxId"
              type="text"
              placeholder="Enter Tax ID"
              value={props.firmDetails.taxId}
              onChange={props.onChangeFirmDetail}
            />
          </div>
          {props.errorFirmDetail && props.errorFirmDetail.taxId && (
            <small className="text-error">{props.errorFirmDetail.taxId}</small>
          )}
        </div>
      </div>
    </div>
  </div>
)
export default FirmProfileForm
