import React from "react"
import dateFormat from "dateformat"
import { DropdownList } from "react-widgets"
import { DropDownSelect } from "../../../../GlobalComponents/TextViewBox"

const LinkBorrower = props => (
  <article
    className={
      props.expanded.linkborrower ? "accordion is-active" : "accordion"
    }
  >
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "linkborrower")
        }}
      >
        Link Borrowers / Obligors
      </p>
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={event =>
              props.addlinkBorrower(event, "linkborrower", props.linkBorrower)
            }
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={event => props.resetlinkBorrower(event, "linkborrower")}
          >
            Reset
          </button>
        </div>
      </div>
    </div>

    {props.expanded.linkborrower && (
      <div className="accordion-body">
        <div className="accordion-body">
          <div className="accordion-content linkborrower">
            <div className="overflow-auto">
              <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>
                      <p className="multiExpLbl">Relationship</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Firm Name</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Debt Type</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Associated Cusip-6</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Start Date</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">End Date</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Delete</p>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {props.linkBorrower.map((item, idx) => (
                    <tr key={23 * idx}>
                      <td className="multiExpTblVal">
                        <div className="control">
                          <DropDownSelect
                            name="borOblRel"
                            value={item.borOblRel}
                            data={
                              props.relBorrowerObligor
                                ? props.relBorrowerObligor[1]
                                : []
                            }
                            message="Select Borrower / Obligor"
                            busy={props.busy}
                            busySpinner={
                              <span className="fas fa-sync fa-spin is-link" />
                            }
                            onChange={val => {
                              const event = {
                                target: {
                                  name: "borOblRel",
                                  value: val.value
                                }
                              }
                              props.onChangelinkBorrower(
                                event,
                                idx,
                                [],
                                "entityBorObl"
                              )
                            }}
                          />
                          {props.entityBorOblErrors &&
                            props.entityBorOblErrors[idx].borOblRel && (
                              <small className="text-error">
                                {props.entityBorOblErrors[idx].borOblRel}
                              </small>
                            )}
                        </div>
                      </td>
                      <td className="multiExpTblVal">
                        <div className="control" style={{ width: "250px" }}>
                          <input
                            className="input is-small is-link"
                            type="text"
                            placeholder="Firm Name"
                            name="borOblFirmName"
                            value={item.borOblFirmName}
                            onChange={event => {
                              props.onChangelinkBorrower(
                                event,
                                idx,
                                [],
                                "entityBorObl"
                              )
                            }}
                          />
                          {props.entityBorOblErrors &&
                            props.entityBorOblErrors[idx].borOblFirmName && (
                              <small className="text-error">
                                {props.entityBorOblErrors[idx].borOblFirmName}
                              </small>
                            )}
                        </div>
                      </td>
                      <td className="multiExpTblVal">
                        <div className="control" style={{ width: "200px" }}>
                          <DropDownSelect
                            value={item.borOblDebtType}
                            data={
                              props.linkCusipDebType
                                ? props.linkCusipDebType[1]
                                : []
                            }
                            message="Select Debt Type"
                            busy={props.busy}
                            busySpinner={
                              <span className="fas fa-sync fa-spin is-link" />
                            }
                            onChange={val => {
                              const event = {
                                target: {
                                  name: "borOblDebtType",
                                  value: val.value
                                }
                              }
                              props.onChangelinkBorrower(
                                event,
                                idx,
                                [],
                                "entityBorObl"
                              )
                            }}
                          />
                          {props.entityBorOblErrors &&
                            props.entityBorOblErrors[idx].borOblDebtType && (
                              <small className="text-error">
                                {props.entityBorOblErrors[idx].borOblDebtType}
                              </small>
                            )}
                        </div>
                      </td>
                      <td className="multiExpTblVal">
                        <div style={{ width: "100px" }}>
                          <input
                            className="input is-small is-link"
                            type="text"
                            style={{ textTransform: "uppercase" }}
                            placeholder="Cusip-6"
                            name="borOblCusip6"
                            value={item.borOblCusip6}
                            onChange={event => {
                              event.target.value = event.target.value.toUpperCase()
                              if (event.target.value.length > 6) {
                                return false
                              }
                              props.onChangelinkBorrower(
                                event,
                                idx,
                                [],
                                "entityBorObl"
                              )
                            }}
                          />
                          {props.entityBorOblErrors &&
                            props.entityBorOblErrors[idx].borOblCusip6 && (
                              <small className="text-error">
                                {props.entityBorOblErrors[idx].borOblCusip6}
                              </small>
                            )}
                        </div>
                      </td>
                      <td className="multiExpTblVal">
                        <div className="field">
                          <div
                            className="control"
                            style={{ maxWidth: "125px" }}
                          >
                            <input
                              id="datepickerDemo"
                              className="input is-small is-link"
                              type="date"
                              name="borOblStartDate"
                              value={
                                item.borOblStartDate
                                  ? dateFormat(
                                      item.borOblStartDate,
                                      "yyyy-mm-dd"
                                    )
                                  : ""
                              }
                              onChange={event => {
                                props.onChangelinkBorrower(
                                  event,
                                  idx,
                                  [],
                                  "entityBorObl"
                                )
                              }}
                            />
                            {props.entityBorOblErrors &&
                              props.entityBorOblErrors[idx].borOblStartDate && (
                                <small className="text-error">
                                  {
                                    props.entityBorOblErrors[idx]
                                      .borOblStartDate
                                  }
                                </small>
                              )}
                          </div>
                        </div>
                      </td>
                      <td className="multiExpTblVal">
                        <div className="field">
                          <div
                            className="control"
                            style={{ maxWidth: "125px" }}
                          >
                            <input
                              id="datepickerDemo"
                              className="input is-small is-link"
                              type="date"
                              name="borOblEndDate"
                              value={
                                item.borOblEndDate
                                  ? dateFormat(item.borOblEndDate, "yyyy-mm-dd")
                                  : ""
                              }
                              onChange={event => {
                                props.onChangelinkBorrower(
                                  event,
                                  idx,
                                  [],
                                  "entityBorObl"
                                )
                              }}
                            />
                            {props.entityBorOblErrors &&
                              props.entityBorOblErrors[idx].borOblEndDate && (
                                <small className="text-error">
                                  {props.entityBorOblErrors[idx].borOblEndDate}
                                </small>
                              )}
                          </div>
                        </div>
                      </td>
                      <td className="multiExpTblVal">
                        <div style={{ padding: "0px 10px" }}>
                          {idx >= 0 ? (
                            <span
                              className="has-text-link fa-delete"
                              style={{ cursor: "pointer" }}
                              onClick={event => {
                                props.deleteAddress(event, "entityBorObl", idx)
                              }}
                            >
                              <i className="far fa-trash-alt" />
                            </span>
                          ) : (
                            ""
                          )}
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {props.LinkBorrowerList.length > 0 && (
          <div className="box">
            <div className="overflow-auto">
              <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>
                      <p className="multiExpLbl">Relationship</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Firm Name</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Debt Type</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Associated Cusip-6</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Start Date</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">End Date</p>
                    </th>
                    <th>
                      <p className="multiExpLbl">Update / Delete</p>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {props.LinkBorrowerList.length > 0 &&
                    props.LinkBorrowerList.map((item, idx) => (
                      <tr key={123 * idx}>
                        <td className="multiExpTblVal">
                          <small>{item.borOblRel}</small>
                        </td>
                        <td className="multiExpTblVal">
                          <small>{item.borOblFirmName}</small>
                        </td>
                        <td className="multiExpTblVal">
                          <small>{item.borOblDebtType}</small>
                        </td>
                        <td className="multiExpTblVal">
                          <small>{item.borOblCusip6}</small>
                        </td>
                        <td className="multiExpTblVal">
                          <small>
                            {dateFormat(item.borOblStartDate, "yyyy-mm-dd")}
                          </small>
                        </td>
                        <td className="multiExpTblVal">
                          <small>
                            {dateFormat(item.borOblEndDate, "yyyy-mm-dd")}
                          </small>
                        </td>
                        <td className="multiExpTblVal">
                          <div className="field is-grouped">
                            <div
                              className="control"
                              onClick={event => {
                                props.updateLinkBorrower(event, item._id)
                              }}
                            >
                              <a href="">
                                <span className="has-text-link">
                                  <i className="fas fa-pencil-alt" />
                                </span>
                              </a>
                            </div>
                            <div
                              className="control"
                              onClick={event => {
                                props.deleteLinkBorrower(event, item._id)
                              }}
                            >
                              <a href="">
                                <span className="has-text-link">
                                  <i className="far fa-trash-alt" />
                                </span>
                              </a>
                            </div>
                          </div>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        )}
      </div>
    )}
  </article>
)
export default LinkBorrower
