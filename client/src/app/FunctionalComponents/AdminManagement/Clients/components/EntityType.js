import React from "react"

const EntityType = props => (
  <article className={props.expanded.entitytype ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "entitytype")
        }}
      >Issuer Classification</p>
    </div>
    { props.expanded.entitytype && <div className="accordion-body">
      <div className="accordion-content">
        <p className="title innerPgTitle">Please check all that apply</p>
        <div className="column">
          {props.issuerFlagsList && props.issuerFlagsList.map(item => (
            <div className="column" key={Math.random()} style={{display:"inline-block"}}>
              <h1>
                <p className="multiExpLbl" title={`Is ${item}?`}> 
                  {item}
                  <input
                    type="checkbox"
                    name="issuerFlags"
                    value={item}
                    onChange={
                      (event)=>{
                        event.target.name = "issuerFlags"
                        event.target.value = item
                        props.onChangeIssuerFlag(event)
                      }}
                    checked={!!props.entityFlags.issuerFlags.includes(item)}
                  />
                </p>
              </h1>
            </div>
          ))}
        </div>
        {props.errorFirmDetail &&  props.errorFirmDetail.entityFlags.issuerFlags && <small className="text-error">{props.errorFirmDetail.entityFlags.issuerFlags}</small>}                     
      </div>
    </div>}
  </article>
)
export default EntityType