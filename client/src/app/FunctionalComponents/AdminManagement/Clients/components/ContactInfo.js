import React from "react"
import NumberFormat from "react-number-format"
import { DropdownList } from "react-widgets"

const ContactInfo = props => (
  <article className={props.expanded.contactinfo ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "contactinfo")
        }}
      >Current/Previous Advisor Contact Information</p>{
        props.expanded.contactinfo ? <i className="fas fa-chevron-up" style={{ cursor: "pointer" }} />
          : <i className="fas fa-chevron-down" style={{ cursor: "pointer" }} />
      } <br />
    </div>
    {props.expanded.contactinfo && <div className="accordion-body">
      <div className="accordion-content">
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Firm's Lead Advisor</p>
            <div className="control">
              <DropdownList filter
                value={props.firmDetails.firmLeadAdvisor}
                data={props.usersList} message="select MSRB Firm Name"
                textField='label' valueField="value" defaultValue=""
                busy={props.busy}
                busySpinner={
                  <span className="fas fa-sync fa-spin is-link" />
                }
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "firmLeadAdvisor",
                      value: val
                    }
                  }
                  props.onChangeFirmDetail(event)
                }} />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.firmLeadAdvisor && <small className="text-error">{props.errorFirmDetail.firmLeadAdvisor}</small>}
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Previous Lead Advisor</p>
            <div className="control">
              <input className="input is-small is-link"
                type="text"
                placeholder="search by name"
                name="prevLeadAdvisor"
                value={props.firmDetails.prevLeadAdvisor ? props.firmDetails.prevLeadAdvisor : ""}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.prevLeadAdvisor && <small className="text-error">{props.errorFirmDetail.prevLeadAdvisor}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Previous Advisor Firm</p>
            <div className="control">
              <input className="input is-small is-link"
                type="text"
                placeholder="search by firm name"
                name="prevAdvisorFirm"
                value={props.firmDetails.prevAdvisorFirm ? props.firmDetails.prevAdvisorFirm : ""}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.prevAdvisorFirm && <small className="text-error">{props.errorFirmDetail.prevAdvisorFirm}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Previous Advisory Contract Expiry</p>
            <div className="control">
              <input id="datepickerDemo"
                className="input is-small is-link"
                type="date"
                name="prevAdvisorContractExpire"
                value={props.firmDetails.prevAdvisorContractExpire ? props.firmDetails.prevAdvisorContractExpire : ""}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.prevAdvisorContractExpire && <small className="text-error">{props.errorFirmDetail.prevAdvisorContractExpire}</small>}
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Primary Contact Name in EMMA</p>
            <div className="control">
              <input className="input is-small is-link"
                type="text"
                placeholder=""
                name="primaryContactNameInEmma"
                value={props.firmDetails.primaryContactNameInEmma ? props.firmDetails.primaryContactNameInEmma : ""}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.primaryContactNameInEmma && <small className="text-error">{props.errorFirmDetail.primaryContactNameInEmma}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Primary Contact Phone</p>
            <div className="control">
              <NumberFormat
                format="+1 (###) ###-####"
                mask="_"
                className="input is-small is-link"
                name="primaryContactPhone"
                placeholder="Numbers only"
                size="15"
                value={props.firmDetails.primaryContactPhone ? props.firmDetails.primaryContactPhone : ""}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.primaryContactPhone && <small className="text-error">{props.errorFirmDetail.primaryContactPhone}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl"> Primary Contact Email</p>
            <div className="control">
              <input className="input is-small is-link"
                type="text"
                placeholder=""
                name="primaryContactEmail"
                value={props.firmDetails.primaryContactEmail ? props.firmDetails.primaryContactEmail : ""}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.primaryContactEmail && <small className="text-error">{props.errorFirmDetail.primaryContactEmail}</small>}
          </div>
        </div>
      </div>
    </div>}
  </article>
)
export default ContactInfo