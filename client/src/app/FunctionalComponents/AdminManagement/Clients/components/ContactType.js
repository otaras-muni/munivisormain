import React from "react"

const capitalizeWords = (str) => str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())

const ContactType = (props) => (
  <article className={props.expanded.contacttype ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "contacttype")
        }}>Contact Type &amp; System Access Settings</p>
    </div>
    {props.expanded.contacttype && <div className="accordion-body">
      <div className="accordion-content">

        <p className="title innerPgTitle">Select Contact Type</p>

        <div className="columns" style={{ display: "inline-block" }}>
          {props.loadingPickLists ? <div style={{ padding: 30 }} className="is-link"><span className="fas fa-sync fa-spin is-link" /></div> : props.userFlags.map((item, idx) => (
            <div className="column" key={Math.random()} style={{ display: "inline-block" }}>
              <h1>
                <p className="emmaTablesTd" title={`${item}`}>
                  {" "}
                  {item}
                  <input
                    type="checkbox"
                    name="userFlags"
                    value={item}
                    onChange={props.onChangeUserDetail}
                    checked={!!props.userDetails.userFlags.includes(item)}
                  />
                </p>
              </h1>
            </div>
          ))}
        </div>
        {props.errorUserDetail && props.errorUserDetail.userFlags && <small className="text-error">{props.errorUserDetail.userFlags}</small>}
        <hr />

        <p className="title innerPgTitle">Select Contact's System Access Level</p>
        <div className="columns">
          {props.loadingPickLists ? <div style={{ padding: 30 }} className="is-link"><span className="fas fa-sync fa-spin is-link" /></div> : props.userEntitlement.map((item, idx) => (
            <div className="column" key={Math.random()}>
              <h1>
                <p className="emmaTablesTd" title={`Is ${capitalizeWords((item.split("-")).join(" "))}?`}>
                  {" "}
                  {capitalizeWords((item.split("-")).join(" "))}
                  <input
                    type="checkbox"
                    name="userEntitlement"
                    value={item}
                    checked={props.userDetails.userEntitlement === item}
                    onChange={props.onChangeUserDetail}
                  />
                </p>
              </h1>
            </div>
          ))}
          <br />
        </div>
        {props.errorUserDetail && props.errorUserDetail.userEntitlement && <small className="text-error">{props.errorUserDetail.userEntitlement}</small>}
      </div>
    </div>}

  </article>
)
export default ContactType