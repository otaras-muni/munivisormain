import React from "react"
import { NavLink,Redirect } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
import ClientsProspects from "../../ClientsProspects/ClientsProspects"
// import ClientsFirms from "./containers/ClientsFirmContainers"
import Documents from "../../EntityManagement/Documents"
import ContactList from "../../EntityManagement/FirmLists/ContactListContainer"

const TABS = [
  { path: "entity", label: "Client Entity" },
  { path: "contacts", label: "Client Contacts" },
  { path: "documents", label: "Client Documents" }
]
const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

const ClientsView = props => {
  let { id, navComponent } = props
  navComponent = navComponent || "details"
  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink key={t.path} to={`/admin-cltprosp/${id}/${t.path}`} activeStyle={activeStyle}> {t.label} </NavLink>
      </li>
    ))

  const renderViewSelection = (id, navComponent) => (
    <nav className="tabs is-boxed">
      <ul>{renderTabs(TABS, navComponent, id)}</ul>
    </nav>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(id, navComponent)}
          </div>
        </div>
      </div>
      <div id = "main">
        <Switch condition={navComponent}>
          <Case value="entity">
            <ClientsProspects nav2={id} />
          </Case>
          <Case value="contacts">
            <ContactList nav2={id} />
          </Case>
          <Case value="documents">
            <Documents nav2 ={id} />
          </Case>
          <Default>
            <Redirect to="/admin-cltprosp"/>
          </Default>
        </Switch>
      </div>
    </div>
  )
}

export default ClientsView
