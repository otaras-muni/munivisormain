import {validatClientsDetail} from "Validation/clients"
import {
  saveClientFirmDetail,
  getClientFirmDetailById,
  deleteLinkCusipBorrower,
  updateClientFirm,
  searchUsersList,
  clientProspectFirmList,
  checkDuplicateEntity,
  addClientFirm,
  checkPlatFormAdmin,
  getAllUserFirmList,
  getIssuerList
} from "AppState/actions/AdminManagement/admTrnActions"
import {confirmAlert} from "react-confirm-alert"
import "react-confirm-alert/src/react-confirm-alert.css"
import React from "react"
import {toast} from "react-toastify"
import {withRouter} from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import {connect} from "react-redux"
import {DropdownList} from "react-widgets"
import {getPicklistValues, convertError, checkEmptyElObject} from "GlobalUtils/helpers"
import FirmAddressForm from "./../../CommonComponents/FirmAddressForm"
import FirmProfileForm from "./../components/FirmProfileForm"
import EntityType from "./../components/EntityType"
import ContactInfo from "./../components/ContactInfo"
import FirmAddressList from "./../../CommonComponents/FirmAddressList"
import LinkCusip from "./../components/LinkCusip"
import LinkBorrower from "./../components/LinkBorrower"
import Loader from "../../../../GlobalComponents/Loader"
import "./../../scss/entity.scss"

const dateCompare = (startDate, endDate) => {
  if (startDate > endDate) 
    return 1
  else if (startDate < endDate) 
    return -1
  return 0
}
const initialAddressError = (errorFirmDetail, address, initialFirmDetails) => {
  const arr = ["officeEmails", "officePhone", "officeFax"]
  arr.map(item => {
    if (address[item].length > 0) {
      for (let index = 1; index < address[item].length; index++) {
        errorFirmDetail
          .addresses[0][item]
          .push(initialFirmDetails.addresses[0][item][0])
      }
    }
  })
}
class ClientsFirms extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.loading = true
    this.state.loadingCheckLists = true
    this.state.errorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.cleanErrorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.handleSubmit = this
      .handleSubmit
      .bind(this)
    this.submitted = false
  }

  async componentWillMount() {
    let isPlatFormAdmin = checkPlatFormAdmin()
    this.setState({loading: true})
    const {userEntities} = this.props.auth
    const {firmDetails} = this.state
    let {searchFirmName, entityId} = this.state
    let usersList = []
    let clientList = []
    firmDetails.addresses[0].isPrimary = true
    firmDetails.addresses[0].isActive = true

    isPlatFormAdmin = await isPlatFormAdmin

    if (!isPlatFormAdmin) {
      usersList = searchUsersList(userEntities.entityId, "")
      clientList = await clientProspectFirmList(userEntities.entityId)
      clientList = clientList.map(item => ({label: item.firmName, value: item.firmId, firmType: item.firmType}))
      usersList = await usersList
      usersList = usersList.map(item => `${item.firstName} ${item.lastName}`)
      searchFirmName = userEntities.firmName
      entityId = userEntities.entityId
    }
    this.setState(prevState => ({
      ...prevState,
      ...{
        firmDetails,
        usersList,
        clientList,
        isPlatFormAdmin,
        entityId,
        searchFirmName,
        loading: false
      }
    }))
    if (this.props.clientId && this.props.clientId.length === 24) {
      this
        .props
        .getClientFirmDetailById(this.props.clientId)
    }
  }
  async componentDidMount() {
    this.setState({loadingCheckLists: true})
    let firmList = getAllUserFirmList()
    const [countryResult,
      primarySectors,
      issuerFlagsList,
      linkCusipDebType,
      relBorrowerObligor,
      entityTypeListResult,
      maFirmList,
      entityFirmList] = await getPicklistValues([
      "LKUPCOUNTRY",
      "LKUPPRIMARYSECTOR",
      "LKUPISSUERFLAG",
      "LKUPDEBTTYPE",
      "LKUPCRMBORROWER",
      "LKUPENTITYTYPE",
      "LKUPFIRMNAME",
      "LKUPEMMAREFERENCEENTITIES"
    ])
    const entityTypeList = [
      {
        label: "Select Entity Type",
        value: ""
      }
    ]
    entityTypeListResult[1].forEach(item => {
      entityTypeList.push({label: item, value: item})
    })

    firmList = await firmList
    firmList = firmList.map(item => ({label: item.firmName, value: item._id}))

    this.setState(prevState => ({
      ...prevState,
      ...{
        firmList: [
          {
            label: "Select Firms",
            value: ""
          },
          ...firmList
        ],
        countryResult,
        primarySectorsList: primarySectors,
        issuerFlagsList,
        linkCusipDebType,
        relBorrowerObligor,
        entityTypeList,
        entityTypeListResult,
        maFirmList,
        loadingCheckLists: false,
        entityFirmList
      }
    }))
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.admClientDetail.firmDetailById && Object.keys(nextProps.admClientDetail.firmDetailById).length > 0 && !nextProps.admClientDetail.updated) {
      let {errorFirmDetail, oldFirmDetails} = this.state
      oldFirmDetails = cloneDeep(nextProps.admClientDetail.firmDetailById)
      const firmDetails = {
        ...cloneDeep(nextProps.admClientDetail.firmDetailById)
      }
      const addressList = cloneDeep(nextProps.admClientDetail.firmDetailById.addresses)
      const LinkCusipList = firmDetails.entityLinkedCusips !== null
        ? firmDetails.entityLinkedCusips
        : []
      const LinkBorrowerList = firmDetails.entityBorObl !== null
        ? firmDetails.entityBorObl
        : []
      firmDetails.addresses = [
        this
          .initialState()
          .firmDetails
          .addresses[0]
      ]
      firmDetails.entityLinkedCusips = [
        ...this
          .initialState()
          .firmDetails
          .entityLinkedCusips
      ]
      firmDetails.entityBorObl = [
        ...this
          .initialState()
          .firmDetails
          .entityBorObl
      ]
      errorFirmDetail = cloneDeep(this.initialState().firmDetails)
      initialAddressError(errorFirmDetail, firmDetails.addresses[0], this.initialState().firmDetails)
      const oldErrorFirmDetail = cloneDeep(errorFirmDetail)
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        LinkCusipList,
        LinkBorrowerList,
        errorFirmDetail,
        readOnlyFeatures: {
          msrbFirmNameReadOnly: true,
          msrbIdReadOnly: true,
          registrantTypeReadOnly: true,
          clientTypeDisabled: true,
          firmName: true
        },
        loading: false,
        oldFirmDetails,
        expanded: cloneDeep(this.initialState().expanded),
        oldErrorFirmDetail,
        isPrimaryAddress: "",
        addClientProspect: true,
        updated: true
      }))
    }
  }
  componentWillUpdate(nextState, nextProps) {}
  componentDidUpdate(prevProps, prevState) {}

  /** ********************************************** onChangeAliases *********************** */
  onChangeAliases = (e, key) => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails.entityAliases[key] = e.target.value
    const validator = {
      entityAliases: [e.target.value]
    }
    this.setState({firmDetails})
    if (this.submitted) {
      const errorData = validatClientsDetail(validator)
      if (errorData.error !== null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name) {
          errorFirmDetail[e.target.name][key] = `${err.context.label} Required.`
          this.setState({errorFirmDetail})
        } else {
          errorFirmDetail[e.target.name][key] = ""
          this.setState({errorFirmDetail})
        }
      } else {
        errorFirmDetail[e.target.name][key] = ""
        this.setState({errorFirmDetail})
      }
    }
  }
  /** ************************************************* */
  onChangeAddOns = (e, idx) => {
    const {firmDetails} = this.state
    const {firmAddOns} = firmDetails
    firmAddOns[idx].serviceEnabled = !firmAddOns[idx].serviceEnabled
    firmDetails.firmAddOns = firmAddOns
    this.setState({firmDetails})
  }
  /** ************************ this is for onchange ********************************* */
  onChangeFirmDetail = (e, key, deepFields, type = "addresses") => {
    // e.preventDefault()
    const {firmDetails, errorFirmDetail, isEntityExists} = this.state
    let keyFirmDetail = {}
    let errorKeyFirmDetail = {}
    let validator = []
    if (e.target.name === "firmName") {
      if (e.target.value !== null) {
        e.target.value = e.target.value.firmName
      } else {
        isEntityExists.firmName = ""
        e.target.value = ""
      }
    }
    if (typeof key !== "undefined") {
      const value = e.target.type === "checkbox"
        ? !firmDetails.addresses[key][e.target.name]
        : e.target.value
      if (e.target.name === "zip1" || e.target.name === "zip2") {
        keyFirmDetail = firmDetails.addresses[key].zipCode
        errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
        validator = {
          addresses: [
            {
              zipCode: {
                [e.target.name]: e.target.value
              }
            }
          ]
        }
      } else {
        keyFirmDetail = firmDetails[type][key]
        errorKeyFirmDetail = errorFirmDetail[type][key]
        validator = {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      }
      keyFirmDetail[e.target.name] = value
      if (deepFields) {
        deepFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    } else if (e.target.name === "clientType") {
      this.setState({clientType: e.target.value})
    } else {
      if (e.target.name === "primarySectors") 
        firmDetails.secondarySectors = ""
      if (e.target.name === "issuerFlags") {
        this.toggleCheckbox(e.target.value)
        validator = {
          entityFlags: {
            [e.target.name]: firmDetails.entityFlags[e.target.name]
          }
        }
        errorKeyFirmDetail = errorFirmDetail.entityFlags
      } else {
        firmDetails[e.target.name] = e.target.value
        validator = {
          [e.target.name]: e.target.value
        }
        errorKeyFirmDetail = errorFirmDetail
      }
      if (deepFields) {
        deepFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    }
    if (this.submitted) {
      if (e.target.name === "borOblStartDate" || e.target.name === "borOblEndDate") {
        if (firmDetails[type][key].borOblStartDate !== "" && firmDetails[type][key].borOblEndDate !== "") {
          const dateVal = dateCompare(firmDetails[type][key].borOblStartDate, firmDetails[type][key].borOblEndDate)
          switch (dateVal) {
            case 1:
              errorKeyFirmDetail[e.target.name] = e.target.name === "borOblStartDate"
                ? "Start Date is should be less Exit Date."
                : "End Date is should be Greate Thant Start Date."
              break
            default:
              errorKeyFirmDetail[e.target.name] = ""
              break
          }
        } else if (firmDetails[type][key][e.target.name] === "") {
          const dateVal = e.target.name === "borOblStartDate"
            ? "Start Date"
            : "End Date"
          errorKeyFirmDetail[e.target.name] = `${dateVal} Required`
        } else {
          errorKeyFirmDetail[e.target.name] = ""
        }
        this.setState({errorKeyFirmDetail})
        return
      }
      const errorData = validatClientsDetail(validator)

      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name || err.context.key === e.target.name) {
          errorKeyFirmDetail[e.target.name] = `${err.context.label} Required.`
          this.setState({errorFirmDetail})
        } else {
          errorKeyFirmDetail[e.target.name] = ""
          this.setState({errorFirmDetail})
        }
      } else if (e.target.name !== "clientType") {
        errorKeyFirmDetail[e.target.name] = ""
        this.setState({errorFirmDetail})
      }
    }
  }

  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, key, idx) => {
    e.preventDefault()
    const {firmDetails} = this.state
    const {addresses} = firmDetails
    const {errorFirmDetail} = this.state
    addresses[key][type][idx][e.target.name] = e.target.value
    const validator = {
      addresses: [
        {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      ]
    }
    firmDetails.addresses = addresses
    this.setState({firmDetails})
    if (this.submitted) {
      const errorData = validatClientsDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorFirmDetail.addresses[key][type][idx][e.target.name] = `${
        err.context.label} Required.`
        this.setState({errorFirmDetail})
      } else {
        errorFirmDetail.addresses[key][type][idx][e.target.name] = ""
        this.setState({errorFirmDetail})
      }
    }
  }
  onChangeAddressType = (e, idx) => {
    const {firmDetails} = this.state
    let {addressList} = this.state
    let {addresses} = firmDetails
    if (e.target.name === "isPrimary") {
      firmDetails.addresses[idx].isPrimary = !firmDetails.addresses[idx].isPrimary
    } else {
      firmDetails.addresses[idx].isPrimary = false
      firmDetails.addresses[idx].isHeadQuarter = true
    }
    if (this.submitted) {
      addressList = addressList.length > 0
        ? addressList.filter(obj => obj._id != addresses[0]._id)
        : []
      addresses = addresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [
        ...addressList,
        ...addresses
      ]
      const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
      if (filteredAddresses.length === 0) 
        // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
        toast("One Primary  Address  is Required!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
      else if (filteredAddresses.length > 1) 
        // this.state.isPrimaryAddress  = "Only One Primary Office  Address  is
        // Required"
        toast("One Primary  Address  is Required!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
      else 
        this.state.isPrimaryAddress = ""
    }
    this.setState({firmDetails})
  }

  onSearchClient = (item, metaData) => {
    metaData
      .originalEvent
      .preventDefault()
    this.setState({loading: true})
    if (item.value !== "") {
      this
        .props
        .getClientFirmDetailById(item.value)
      this.setState({searchClientFirmName: item.label, clientType: item.firmType})
    } else {
      const {firmDetails, readOnlyFeatures} = cloneDeep(this.initialState())
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList: [],
        LinkCusipList: [],
        LinkBorrowerList: [],
        errorFirmDetail: cloneDeep(this.initialState().firmDetails),
        loading: false,
        oldFirmDetails: {},
        searchClientFirmName: "",
        clientType: "Client",
        readOnlyFeatures
      }))
    }
  }

  getFirmDetails = item => {
    const {firmDetails, readOnlyFeatures, errorFirmDetail, entityFirmList, maFirmList} = this.state
    let {entityFirmListData} = this.state
    if (item.value !== "") {
      firmDetails.entityType = item.label
      firmDetails.firmName = ""
      if (item.label === "Other Municipal Advisor") 
        entityFirmListData = [...maFirmList[1]]
      if (["501c3 - Obligor", "Governmental Entity / Issuer"].includes(item.label)) 
        entityFirmListData = entityFirmList[2][item.label]
      errorFirmDetail.entityType = ""
    } else {
      firmDetails.entityType = ""
      firmDetails.firmName = ""
      entityFirmListData = []
      if (this.submitted) {
        errorFirmDetail.entityType = "Entity Type Required"
      } else {
        errorFirmDetail.entityType = ""
      }
    }
    this.setState({firmDetails, readOnlyFeatures, errorFirmDetail, entityFirmListData})
  }

  /** **************************************this is for initial default value********************* */
  initialState = () => ({
    firmDetails: {
      entityFlags: {
        issuerFlags: []
      },
      entityAliases: [],
      isMuniVisorClient: false,
      entityType: "",
      firmName: "",
      firmType: "",
      taxId: "",
      primarySectors: "",
      secondarySectors: "",
      firmLeadAdvisor: "",
      prevLeadAdvisor: "",
      prevAdvisorFirm: "",
      prevAdvisorContractExpire: "",
      primaryContactNameInEmma: "",
      primaryContactPhone: "",
      primaryContactEmail: "",
      addresses: [
        {
          addressName: "",
          isPrimary: false,
          isHeadQuarter: false,
          isActive: true,
          website: "",
          officePhone: [
            {
              countryCode: "",
              phoneNumber: "",
              extension: ""
            }
          ],
          officeFax: [
            {
              faxNumber: ""
            }
          ],
          officeEmails: [
            {
              emailId: ""
            }
          ],
          addressLine1: "",
          addressLine2: "",
          country: "",
          state: "",
          city: "",
          zipCode: {
            zip1: "",
            zip2: ""
          },
          formatted_address: "",
          url: "",
          location: {
            longitude: "",
            latitude: ""
          }
        }
      ],
      entityLinkedCusips: [
        {
          debtType: "",
          associatedCusip6: ""
        }
      ],
      entityBorObl: [
        {
          borOblRel: "",
          borOblFirmName: "",
          borOblDebtType: "",
          borOblCusip6: "",
          borOblStartDate: "",
          borOblEndDate: ""
        }
      ]
    },
    clientType: "Client",
    addressList: [],
    LinkCusipList: [],
    LinkBorrowerList: [],
    firmList: [],
    expanded: {
      linkcusip: false,
      linkborrower: false,
      business: false,
      addresslist: false,
      entityname: true,
      entitytype: true,
      contactinfo: false
    },
    readOnlyFeatures: {
      msrbFirmNameReadOnly: false,
      msrbIdReadOnly: false,
      registrantTypeReadOnly: false,
      clientTypeDisabled: false,
      firmName: false
    },
    entityTypeList: [],
    msrbRegTypeResult: [],
    updated: false,
    disabled: false,
    countryResult: [],
    annualRevenue: [],
    secondarySectors: [],
    primarySectorsList: [],
    issuerFlagsList: [],
    linkCusipDebType: [],
    isPrimaryAddress: "",
    isEntityExists: {
      msrbId: "",
      firmName: ""
    },
    oldFirmDetails: {},
    clientList: [],
    searchClientFirmName: "",
    searchFirmName: "",
    entityId: "",
    firmError: "",
    entityFirmListData: [],
    updateClientProspect: false,
    addClientProspect: true
  })

  /** ************************ this is for submit form ********************************* */
  async handleSubmit(event) {
    // event.preventDefault()
    this.submitted = true
    const {firmDetails, errorFirmDetail} = this.state
    const firmDetailsData = {
      _id: firmDetails._id
        ? firmDetails._id
        : "",
      // entityFlags:firmDetails.entityFlags,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName: firmDetails.msrbFirmName
        ? firmDetails.msrbFirmName
        : firmDetails.firmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      firmType: firmDetails.firmType,
      taxId: firmDetails.taxId,
      entityType: firmDetails.entityType,
      primarySectors: firmDetails.primarySectors,
      secondarySectors: firmDetails.secondarySectors,
      firmLeadAdvisor: firmDetails.firmLeadAdvisor,
      prevLeadAdvisor: firmDetails.prevLeadAdvisor,
      prevAdvisorFirm: firmDetails.prevAdvisorFirm,
      prevAdvisorContractExpire: firmDetails.prevAdvisorContractExpire
        ? firmDetails.prevAdvisorContractExpire
        : "",
      primaryContactNameInEmma: firmDetails.primaryContactNameInEmma,
      primaryContactPhone: firmDetails.primaryContactPhone,
      primaryContactEmail: firmDetails.primaryContactEmail,
      addresses: firmDetails.addresses,
      entityLinkedCusips: firmDetails.entityLinkedCusips,
      entityBorObl: firmDetails.entityBorObl
    }
    if (firmDetails.entityType === "Governmental Entity / Issuer") 
      firmDetailsData.entityFlags = firmDetails.entityFlags
    try {
      if (firmDetailsData._id !== "") {
        let addresses = []
        let entityLinkedCusips = []
        let entityBorObl = []
        firmDetailsData
          .addresses
          .forEach(address => {
            if (!checkEmptyElObject(address)) {
              addresses.push(address)
            }
          })
        firmDetailsData.addresses = addresses
        firmDetailsData
          .entityLinkedCusips
          .forEach(address => {
            if (!checkEmptyElObject(address)) {
              entityLinkedCusips.push(address)
            }
          })
        firmDetailsData.entityLinkedCusips = entityLinkedCusips
        firmDetailsData
          .entityBorObl
          .forEach(address => {
            if (!checkEmptyElObject(address)) {
              entityBorObl.push(address)
            }
          })
        firmDetailsData.entityBorObl = entityBorObl
        let {addressList, LinkCusipList, LinkBorrowerList} = this.state
        if (addresses.length > 0) {
          addresses = addresses.sort((a, b) => a._id > b._id)
          addressList = addressList.length > 0
            ? addressList.filter(obj => obj._id != addresses[0]._id)
            : []
        }
        if (entityBorObl.length > 0) {
          entityBorObl = entityBorObl.sort((a, b) => a._id > b._id)
          LinkBorrowerList = LinkBorrowerList.filter(obj => obj._id != entityBorObl[0]._id)
        }
        if (entityLinkedCusips.length > 0) {
          entityLinkedCusips = entityLinkedCusips.sort((a, b) => a._id > b._id)
          LinkCusipList = LinkCusipList.length > 0
            ? LinkCusipList.filter(obj => obj._id != entityLinkedCusips[0]._id)
            : []
        }

        const mergeAddresses = [
          ...addressList,
          ...addresses
        ]
        const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
        const mergeEntityLinkedCusips = [
          ...LinkCusipList,
          ...entityLinkedCusips
        ]
        const mergeEntityBorObl = [
          ...LinkBorrowerList,
          ...entityBorObl
        ]

        const errorData = validatClientsDetail(firmDetailsData)
        if (errorData.error || filteredAddresses.length !== 1) {
          if (errorData.error) {
            convertError(errorFirmDetail, errorData)
          }
          if (filteredAddresses.length === 0) {
            // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
            toast("One Primary  Address  is Required!", {
              autoClose: 2000,
              type: toast.TYPE.ERROR
            })
          } else if (filteredAddresses.length > 1) {
            toast("Only One Primary  Address  is Required!", {
              autoClose: 2000,
              type: toast.TYPE.ERROR
            })
          } else 
            this.state.isPrimaryAddress = ""
          this.setState({errorFirmDetail})
        } else {
          this.setState({loading: true})
          const response = await updateClientFirm({firmDetails: firmDetailsData, mergeAddresses, mergeEntityLinkedCusips, mergeEntityBorObl})
          if ((typeof response.error !== "undefined" && response.error !== null) || (typeof response.isPrimaryAddress !== "undefined" && !response.isPrimaryAddress)) {
            if (response.error !== null) 
              convertError((errorFirmDetail.errorData.error = response.error))
            if (!response.isPrimaryAddress) 
              toast("One Primary Address  is Required!", {
                autoClose: 2000,
                type: toast.TYPE.ERROR
              })
            else 
              this.state.isPrimaryAddress = ""
            this.setState({loading: false})
          } else {
            this.submitted = false
            toast("Client Firms has been Updated!", {
              autoClose: 2000,
              type: toast.TYPE.SUCCESS
            })
            this
              .props
              .getClientFirmDetailById(firmDetailsData._id)
          }
        }
      } else {
        const {entityId} = this.state
        if (entityId !== "") {
          const entityBorObl = []
          let addresses = []
          addresses = firmDetailsData
            .addresses
            .filter(item => item.isPrimary)
          firmDetailsData
            .entityBorObl
            .forEach(address => {
              if (!checkEmptyElObject(address)) {
                entityBorObl.push(address)
              }
            })
          if (entityBorObl.length === 0) {
            firmDetailsData.entityBorObl = []
          }
          const errorData = validatClientsDetail(firmDetailsData)
          if (errorData.error || addresses.length !== 1 || this.state.isEntityExists.firmName !== "" || this.state.isEntityExists.msrbId !== "") {
            if (errorData.error) {
              convertError(errorFirmDetail, errorData)
            }
            if (addresses.length !== 1) 
              toast("One Primary Office  Address  is Required!", {
                autoClose: 2000,
                type: toast.TYPE.ERROR
              })
            this.setState({errorFirmDetail})
          } else {
            this.setState({loading: true})
            const response = await addClientFirm({firmDetails: firmDetailsData, entityId, clientType: this.state.clientType})
            if ((typeof response.error !== "undefined" && response.error !== null) || (typeof response.isPrimaryAddress !== "undefined" && !response.isPrimaryAddress)) {
              if (response.error !== null) 
                convertError((errorFirmDetail.errorData.error = response.error))
              if (!response.isPrimaryAddress) 
                toast("One Primary Office  Address  is Required!", {
                  autoClose: 2000,
                  type: toast.TYPE.ERROR
                })
              else 
                this.state.isPrimaryAddress = ""
              this.setState({loading: false})
            } else {
              const {clientList} = this.state
              const {expanded, readOnlyFeatures} = this.initialState()
              let {firmDetails} = this.initialState()
              firmDetails = cloneDeep(this.initialState().firmDetails)
              clientList.push({label: response.firmName, value: response._id, firmType: this.state.clientType})
              this.setState(prevState => ({
                ...prevState,
                firmDetails,
                addressList: [],
                LinkCusipList: [],
                LinkBorrowerList: [],
                errorFirmDetail: cloneDeep(this.initialState().firmDetails),
                loading: false,
                oldFirmDetails: {},
                readOnlyFeatures,
                clientList,
                searchClientFirmName: "",
                expanded,
                clientType: "Client"
              }))
              this.submitted = false
              toast("Client Firms has been Created!", {
                autoClose: 2000,
                type: toast.TYPE.SUCCESS
              })
              this
                .props
                .history
                .push("/admin-cltprosp")
            }
          }
        } else {
          this.setState({firmError: "Select Firm"})
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    }
  }

  /** ************************ this is for add new address ********************************* */
  addNewBussinessAddress = (event, type, addresses) => {
    event.preventDefault()
    let isEmpty = false
    addresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true
      }
    })
    if (!isEmpty) {
      switch (type) {
        case "address":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .addresses
              .push(this.initialState().firmDetails.addresses[0])
            errorFirmDetail
              .addresses
              .push(this.initialState().firmDetails.addresses[0])
            return {firmDetails, errorFirmDetail}
          })break
        case "linkcusip":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .entityLinkedCusips
              .push(this.initialState().firmDetails.entityLinkedCusips[0])
            errorFirmDetail
              .entityLinkedCusips
              .push(this.initialState().firmDetails.entityLinkedCusips[0])
            return {firmDetails, errorFirmDetail}
          })break
        case "linkborrower":
          this.setState(prevState => {
            const firmDetails = {
              ...prevState.firmDetails
            }
            const errorFirmDetail = {
              ...prevState.errorFirmDetail
            }
            firmDetails
              .entityBorObl
              .push(this.initialState().firmDetails.entityBorObl[0])
            errorFirmDetail
              .entityBorObl
              .push(this.initialState().firmDetails.entityBorObl[0])
            return {firmDetails, errorFirmDetail}
          })break
        default:
          console.log("Default")
      }
    } else {
      toast(`Already and empty ${type}!`, {
        autoClose: 2000,
        type: toast.TYPE.INFO
      })
    }
  }
  /** ************************ this is for reset form ********************************* */
  resetBussinessAddress = (e, type) => {
    e.preventDefault()
    const address = cloneDeep(this.initialState().firmDetails.addresses[0])
    address.isPrimary = true
    address.isActive = true
    const {firmDetails} = this.state
    const {entityBorObl, entityLinkedCusips} = firmDetails
    switch (type) {
      case "address":
        this.setState(prevState => ({
          firmDetails: {
            ...prevState.firmDetails,
            addresses: [address]
          },
          errorFirmDetail: {
            ...prevState.errorFirmDetail,
            addresses: [
              this
                .initialState()
                .firmDetails
                .addresses[0]
            ]
          }
        }))break
      case "linkcusip":
        if (entityLinkedCusips.length > 0) {
          this.setState(prevState => ({
            firmDetails: {
              ...prevState.firmDetails,
              entityLinkedCusips: [
                this
                  .initialState()
                  .firmDetails
                  .entityLinkedCusips[0]
              ]
            },
            errorFirmDetail: {
              ...prevState.errorFirmDetail,
              entityLinkedCusips: [
                this
                  .initialState()
                  .firmDetails
                  .entityLinkedCusips[0]
              ]
            },
            isPrimaryAddress: ""
          }))
        }
        break
      case "linkborrower":
        if (entityBorObl.length > 0) {
          this.setState(prevState => ({
            firmDetails: {
              ...prevState.firmDetails,
              entityBorObl: [
                this
                  .initialState()
                  .firmDetails
                  .entityBorObl[0]
              ]
            },
            errorFirmDetail: {
              ...prevState.errorFirmDetail,
              entityBorObl: [
                this
                  .initialState()
                  .firmDetails
                  .entityBorObl[0]
              ]
            }
          }))
        }
        break
      default:
        console.log("Default")
    }
  }

  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = e => {
    e.preventDefault()
    const {oldFirmDetails, firmDetails} = this.state
    if (Object.keys(oldFirmDetails).length > 0) {
      const {entityAliases} = cloneDeep(oldFirmDetails)
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: oldFirmDetails
            .entityAliases
            .map(item => this.initialState().firmDetails.entityAliases)
        }
      }))
    } else {
      const {entityAliases} = cloneDeep(this.initialState().firmDetails)
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: cloneDeep(this.initialState().firmDetails.entityAliases)
        }
      }))
    }
  }

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type, key) => {
    e.preventDefault()
    this.setState(prevState => {
      const addMoreData = {
        ...prevState.firmDetails
      }
      const errorFirmDetail = {
        ...prevState.errorFirmDetail
      }
      if (type === "aliases") {
        addMoreData
          .entityAliases
          .push("")
        errorFirmDetail
          .entityAliases
          .push("")
      } else {
        addMoreData
          .addresses[key][type]
          .push(this.initialState().firmDetails.addresses[0][type][0])
        errorFirmDetail
          .addresses[key][type]
          .push(this.initialState().firmDetails.addresses[0][type][0])
      }
      return {addMoreData, errorFirmDetail}
    })
  }

  // **************************function used to update business addresses
  // *****************************
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this
      .state
      .addressList
      .find(item => item._id === id)
    this.setState(prevState => {
      let {errorFirmDetail} = prevState
      const {firmDetails, expanded, oldErrorFirmDetail} = prevState
      errorFirmDetail = this
        .initialState()
        .firmDetails
      firmDetails.addresses = [address]
      const arr = ["officeEmails", "officePhone", "officeFax"]
      arr.forEach(item => {
        if (address[item].length > 1) {
          for (let i = 1; i < address[item].length; i++) {
            errorFirmDetail
              .addresses[0][item]
              .push(this.initialState().firmDetails.addresses[0][item][0])
          }
        }
      })
      expanded.business = true
      oldErrorFirmDetail.addresses = errorFirmDetail.addresses
      return {firmDetails, errorFirmDetail, expanded}
    })
  }
  /** *****************************************Toggle Button********************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const {expanded} = this.state
    Object
      .keys(expanded)
      .map(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
    this.setState({expanded})
  }
  /** *********************************************** update LinkCusip*********************************** */
  updateLinkCusipBorrower = (e, id, type = "entityBorObl") => {
    e.preventDefault()
    const {LinkCusipList, LinkBorrowerList} = this.state
    let cusipBorrowerVal
    if (type === "entityLinkedCusips") {
      cusipBorrowerVal = LinkCusipList.find(item => item._id == id)
    } else {
      cusipBorrowerVal = LinkBorrowerList.find(item => item._id == id)
    }
    this.setState(prevState => {
      const {errorFirmDetail, firmDetails} = prevState
      errorFirmDetail[type] = this
        .initialState()
        .firmDetails[type]
      firmDetails[type] = [cusipBorrowerVal]
      return {firmDetails, errorFirmDetail}
    })
  }

  deleteLinkCusipBorrower = async(e, id, type = "entityBorObl") => {
    e.preventDefault()
    const {firmDetails} = this.state
    let {LinkCusipList, LinkBorrowerList} = this.state
    let {entityLinkedCusips, entityBorObl} = firmDetails
    confirmAlert({
      customUI: ({onClose}) => (
        <div className="custom-ui">
          <h1>Are you sure?</h1>
          <p>You want to delete this item?</p>
          <button onClick={onClose}>No</button>
          <button
            onClick={async() => {
            const response = await deleteLinkCusipBorrower(firmDetails._id, id, type)if (response.ok) {
              if (type === "entityLinkedCusips") {
                LinkCusipList = LinkCusipList.filter(item => item._id !== id)entityLinkedCusips = entityLinkedCusips.filter(item => item._id !== id)
              } else {
                LinkBorrowerList = LinkBorrowerList.filter(item => item._id !== id)entityBorObl = entityBorObl.filter(item => item._id !== id)
              }
              this.setState(prevState => ({
                ...prevState,
                LinkBorrowerList,
                LinkCusipList,
                firmDetails: {
                  ...prevState.firmDetails,
                  entityBorObl: entityBorObl.length > 0
                    ? entityBorObl
                    : this
                      .initialState()
                      .firmDetails
                      .entityBorObl,
                  entityLinkedCusips: entityLinkedCusips.length > 0
                    ? entityLinkedCusips
                    : this
                      .initialState()
                      .firmDetails
                      .entityLinkedCusips
                }
              }))
            }
            onClose()
          }}>
            Yes, Delete it!
          </button>
        </div>
      )
    })
  }
  deleteAliases = idx => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails
      .entityAliases
      .splice(idx, 1)
    errorFirmDetail
      .entityAliases
      .splice(idx, 1)
    this.setState({firmDetails, errorFirmDetail})
  }
  deleteAddressAliases = (e, type, key, idx) => {
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails
      .addresses[key][type]
      .splice(idx, 1)
    errorFirmDetail
      .addresses[key][type]
      .splice(idx, 1)
    this.setState({firmDetails, errorFirmDetail})
  }
  resetAddressAliases = (e, type, key, id) => {
    if (id !== "") {
      this.setState(prevState => {
        const oldFirmDetails = cloneDeep({
          ...prevState.oldFirmDetails
        })
        const {firmDetails, errorFirmDetail} = prevState
        const oldErrorFirmDetail = cloneDeep({
          ...prevState.oldErrorFirmDetail
        })
        const {addresses} = oldFirmDetails
        const address = addresses.filter(item => item._id === id)
        firmDetails.addresses[key][type] = address[0][type]
        errorFirmDetail.addresses[key][type] = cloneDeep(oldErrorFirmDetail.addresses[key][type])
        return {firmDetails, errorFirmDetail}
      })
    } else {
      const {firmDetails, errorFirmDetail} = this.state
      firmDetails.addresses[key][type] = this
        .initialState()
        .firmDetails
        .addresses[0][type]
      errorFirmDetail.addresses[key][type] = this
        .initialState()
        .firmDetails
        .addresses[0][type]
      this.setState({firmDetails, errorFirmDetail})
    }
  }
  checkDuplicateFirm = async e => {
    // const {value,name} = e.target
    const {isEntityExists, firmDetails} = this.state
    const {firmName} = firmDetails
    if (firmName.length > 0) {
      const result = await checkDuplicateEntity(firmName)
      if (result.success.isExist) {
        isEntityExists.firmName = "Firms Name is Already Exists"
      } else {
        isEntityExists.firmName = ""
      }
    } else {
      isEntityExists.firmName = ""
    }
    this.setState({isEntityExists})
  }
  addNewFirm = e => {
    e.preventDefault()
    const {firmDetails} = cloneDeep(this.initialState())
    this.setState(prevState => ({
      ...prevState,
      firmDetails,
      addressList: [],
      LinkCusipList: [],
      LinkBorrowerList: [],
      errorFirmDetail: cloneDeep(this.initialState().firmDetails),
      loading: false,
      oldFirmDetails: {},
      searchClientFirmName: "",
      clientType: "Client",
      readOnlyFeatures: {
        registrantTypeReadOnly: false,
        msrbFirmNameReadOnly: false,
        msrbId: false,
        clientTypeDisabled: false
      }
    }))
  }

  onSearchFirm = async(item, metaData) => {
    metaData
      .originalEvent
      .preventDefault()
    let {clientList, usersList} = this.state
    const {firmDetails, readOnlyFeatures} = cloneDeep(this.initialState())
    this.setState({loading: true})
    if (item.value !== "") {
      clientList = await clientProspectFirmList(item.value)
      clientList = clientList.map(item => ({label: item.firmName, value: item.firmId, firmType: item.firmType}))
      usersList = await searchUsersList(item.value, "")
      usersList = usersList.map(item => `${item.firstName} ${item.lastName}`)
      this.setState({
        firmDetails,
        addressList: [],
        LinkBorrowerList: [],
        LinkCusipList: [],
        searchFirmName: item.label,
        searchClientFirmName: "",
        errorFirmDetail: cloneDeep(this.initialState().firmDetails),
        clientList,
        usersList,
        loading: false,
        readOnlyFeatures,
        entityId: item.value,
        firmError: ""
      })
    } else {
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList: [],
        LinkBorrowerList: [],
        LinkCusipList: [],
        searchClientFirmName: "",
        errorFirmDetail: cloneDeep(this.initialState().firmDetails),
        loading: false,
        searchFirmName: "",
        clientList: [],
        usersList: [],
        loading: false,
        readOnlyFeatures,
        entityId: "",
        firmError: ""
      }))
    }
  }
  checkAddressStatus = async(e, id, field) => {
    const {addressList} = this.state
    let addressIdx = ""
    addressList.forEach((address, idx) => {
      if (address._id == id) {
        addressIdx = idx
      }
    })
    if ((field === "isPrimary" && !addressList[addressIdx].isActive) || (field === "isActive" && addressList[addressIdx].isPrimary)) {
      toast("Primary Address Can not be Inactive.", {
        autoClose: 2000,
        type: toast.TYPE.WARNING
      })
    } else {
      addressList[addressIdx][field] = !addressList[addressIdx][field]
      if (field === "isPrimary") {
        addressList.forEach((address, idx) => {
          if (address._id === id) 
            addressList[idx][field] = true
          else 
            addressList[idx][field] = false
        })
      }
      this.setState(prevState => ({
        ...prevState,
        addressList
      }))
    }
  }
  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const {firmDetails} = this.state
    const {issuerFlags} = firmDetails.entityFlags
    if (issuerFlags.includes(label)) {
      issuerFlags.splice(issuerFlags.indexOf(label), 1)
    } else {
      issuerFlags.push(label)
    }
    this.setState(prevState => ({
      issuerFlags: {
        ...prevState.firmDetails.entityFlags,
        issuerFlags
      }
    }))
  }

  getAddressDetails = (address = "", idx) => {
    if (address !== "") {
      const {firmDetails, errorFirmDetail} = this.state
      let id = ""
      if (firmDetails.addresses[idx]._id !== "") {
        id = firmDetails.addresses[idx]._id
      }
      firmDetails.addresses[idx] = {
        ...this
          .initialState()
          .firmDetails
          .addresses[0]
      }
      firmDetails.addresses[idx]._id = id
      if (address.addressLine1 !== "") {
        firmDetails.addresses[idx].addressName = firmDetails.addresses[idx].addressName !== ""
          ? firmDetails.addresses[idx].addressName
          : address.addressLine1
        firmDetails.addresses[idx].addressLine1 = address.addressLine1
        errorFirmDetail.addresses[idx].addressName = ""
        errorFirmDetail.addresses[idx].addressLine1 = ""
      }
      if (address.country !== "") {
        errorFirmDetail.addresses[idx].country = ""
        firmDetails.addresses[idx].country = address.country
      }
      if (address.state !== "") {
        errorFirmDetail.addresses[idx].state = ""
        firmDetails.addresses[idx].state = address.state
      }
      if (address.city !== "") {
        errorFirmDetail.addresses[idx].city = ""
        firmDetails.addresses[idx].city = address.city
      }
      if (address.zipcode !== "") {
        errorFirmDetail.addresses[idx].zipCode.zip1 = ""
        firmDetails.addresses[idx].zipCode.zip1 = address.zipcode
      }
      firmDetails.addresses[idx].formatted_address = address.formatted_address
      firmDetails.addresses[idx].url = address.url
      firmDetails.addresses[idx].location = {
        ...address.location
      }

      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...firmDetails
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        }
      }))
    }
  }
  addUpdateClientProspect = (e, type) => {
    e.preventDefault()
    const {searchFirm} = this.state
    const {firmDetails, expanded} = cloneDeep(this.initialState())
    this.setState(prevState => ({
      firmDetails, searchFirmName: "", searchClientFirmName: "", addressList: [],
      // clientList:[],
      oldUsersDetails: {},
      readOnlyFeatures: {
        clientTypeDisabled: false
      },
      errorFirmDetail: {
        ...this
          .initialState()
          .firmDetails
      },
      expanded,
      updateClientProspect: type === "updateClientProspect",
      addClientProspect: type === "addClientProspect",
      updated: false,
      entityId: "",
      firmError: ""
    }))
  }
  getEntityIssuerList = async inputVal => {
    const {firmDetails} = this.state
    const {entityType} = firmDetails
    if (!inputVal || !entityType) {
      return {options: []}
    }
    const issuerList = await getIssuerList({inputVal, participantType: entityType, entityType: "Prospects/Clients"})
    if (issuerList.status === 200 && issuerList.data) 
      return {options: issuerList.data}
    return {options: []}
  }
  render() {
    const loading = () => <Loader/>
    const {
      firmDetails,
      LinkCusipList,
      LinkBorrowerList,
      usersList,
      countryResult,
      errorFirmDetail,
      firmList,
      numberOfEmployee,
      readOnlyFeatures,
      msrbRegTypeResult,
      primarySectorsList,
      addressList,
      clientType
    } = this.state
    console.log("clientType===>>>", clientType)
    return (
      <div>
        <section className="firms">
          {this.state.loading
            ? loading()
            : null}
          {/*           <div className="columns">
            <div className="column is-full">
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link" onClick={(e)=>{this.addUpdateClientProspect(e,"addClientProspect")}}>
                                Add Firm
                  </button>
                </div>
                <div className="control">
                  <button className="button is-link" onClick={(e)=>{this.addUpdateClientProspect(e,"updateClientProspect")}}>
                                Update Firm
                  </button>
                </div>
              </div>
            </div>
          </div>  */}
          {this.state.isPlatFormAdmin && (
            <div className="columns">
              <div className="column">
                <DropdownList
                  filter="contains"
                  value={this.state.searchFirmName}
                  data={firmList}
                  groupBy={item => item.firmType}
                  message="select Firm Name"
                  textField="label"
                  valueField="value"
                  onChange={this.onSearchFirm}
                  busy={this.state.loadingCheckLists}
                  busySpinner={< span className = "fas fa-sync fa-spin is-link" />}/> {this.state.firmError && (
                  <small className="text-error">{this.state.firmError}</small>
                )}
              </div>
            </div>
          )}
          {/*           <div className="columns"  style={{display:this.state.updateClientProspect ? "block" : "none"}}>
            <div className="column">
              <DropdownList
                filter="contains"
                value={this.state.searchClientFirmName}
                data={this.state.clientList}
                groupBy={item => item.firmType}
                message="select MSRB Firm Name"
                textField='label' valueField="value"
                onChange={this.onSearchClient} />
            </div>
          </div> */}
          {this.state.addClientProspect
            ? (
              <section className="accordions">
                <article
                  className={this.state.expanded.entityname
                  ? "accordion is-active"
                  : "accordion"}>
                  <div className="accordion-header toggle">
                    <p
                      onClick={event => {
                      this.toggleButton(event, "entityname")
                    }}>
                      Client / Issuer
                    </p>
                    {this.state.expanded.entityname
                      ? (<i
                        className="fas fa-chevron-up"
                        style={{
                        cursor: "pointer"
                      }}/>)
                      : (<i
                        className="fas fa-chevron-down"
                        style={{
                        cursor: "pointer"
                      }}/>)}{" "}
                    <br/>
                  </div>
                  {this.state.expanded.entityname && (<FirmProfileForm firmDetails={firmDetails} onChangeFirmDetail={this.onChangeFirmDetail} errorFirmDetail={errorFirmDetail} getFirmDetails={this.getFirmDetails} addMore={this.addMore} onChangeAliases={this.onChangeAliases} resetAliases={this.resetAliases} readOnlyFeatures={readOnlyFeatures} msrbRegTypeResult={msrbRegTypeResult} numberOfEmployee={numberOfEmployee} primarySectorsList={primarySectorsList} deleteAliases={this.deleteAliases} clientType={this.state.clientType} updated={this.state.updated} disabled={this.state.disabled} msrbFirmListResult={this.state.msrbFirmListResult} // checkDuplicateFirm = {this.checkDuplicateFirm}
  isEntityExists={this.state.isEntityExists} entityTypeList={this.state.entityTypeList} entityTypeListResult={this.state.entityTypeListResult} maFirmList={this.state.maFirmList} entityFirmList={this.state.entityFirmList} entityFirmListData={this.state.entityFirmListData} getEntityFirmList={this.getEntityIssuerList} busy={this.state.loadingCheckLists}/>)}
                </article>
                {firmDetails.entityType === "Governmental Entity / Issuer" && (<EntityType
                  expanded={this.state.expanded}
                  toggleButton={this.toggleButton}
                  onChangeIssuerFlag={this.onChangeFirmDetail}
                  issuerFlagsList={this.state.issuerFlagsList[1]}
                  entityFlags={this.state.firmDetails.entityFlags}
                  errorFirmDetail={this.state.errorFirmDetail}
                  busy={this.state.loadingCheckLists}/>)}
                <ContactInfo
                  expanded={this.state.expanded}
                  toggleButton={this.toggleButton}
                  errorFirmDetail={errorFirmDetail}
                  firmDetails={firmDetails}
                  onChangeFirmDetail={this.onChangeFirmDetail}
                  usersList={usersList}
                  busy={this.state.loadingCheckLists}/> {addressList.length > 0 && (<FirmAddressList
                  addressList={addressList}
                  updateAddress={this.updateAddress}
                  listAddressToggle={this.state.expanded.addresslist}
                  toggleButton={this.toggleButton}
                  checkAddressStatus={this.checkAddressStatus}
                  busy={this.state.loadingCheckLists}/>)}
                <article
                  className={this.state.expanded.business
                  ? "accordion is-active"
                  : "accordion"}>
                  <div className="accordion-header">
                    <p
                      onClick={event => {
                      this.toggleButton(event, "business")
                    }}>
                      Business Address
                    </p>
                    <div className="field is-grouped">
                      <div className="control">
                        <button
                          className="button is-link is-small"
                          onClick={event => {
                          this.addNewBussinessAddress(event, "address", [...firmDetails.addresses])
                        }}>
                          Add
                        </button>
                      </div>
                      <div className="control">
                        <button
                          className="button is-light is-small"
                          onClick={event => {
                          this.resetBussinessAddress(event, "address")
                        }}>
                          Reset
                        </button>
                      </div>
                    </div>
                  </div>
                  {this.state.expanded.business && (
                    <div className="accordion-body">
                      {firmDetails
                        .addresses
                        .map((businessAddress, idx) => (<FirmAddressForm
                          key={idx}
                          businessAddress={businessAddress}
                          idx={idx}
                          onChangeBusinessAddress={this.onChangeFirmDetail}
                          addMore={this.addMore}
                          onChangeAddMore={this.onChangeAddMore}
                          errorFirmDetail={errorFirmDetail.addresses[idx]}
                          countryResult={countryResult}
                          resetAddressAliases={this.resetAddressAliases}
                          deleteAddressAliases={this.deleteAddressAliases}
                          onChangeAddressType={this.onChangeAddressType}
                          deleteAddress={this.deleteAddress}
                          isPrimaryAddress={this.state.isPrimaryAddress}
                          getAddressDetails={this.getAddressDetails}
                          busy={this.state.loadingCheckLists}/>))}
                    </div>
                  )}
                </article>
                <LinkCusip
                  expanded={this.state.expanded}
                  toggleButton={this.toggleButton}
                  linkCusip={this.state.firmDetails.entityLinkedCusips}
                  onChangeLinkCusip={this.onChangeFirmDetail}
                  addLinkCusip={this.addNewBussinessAddress}
                  resetLinkCusip={this.resetBussinessAddress}
                  LinkCusipList={LinkCusipList}
                  updateLinkCusip={this.updateLinkCusipBorrower}
                  deleteLinkCusip={this.deleteLinkCusipBorrower}
                  entityLinkedCusipsErrors={errorFirmDetail.entityLinkedCusips}
                  linkCusipDebType={this.state.linkCusipDebType}
                  deleteAddress={this.deleteAddress}
                  busy={this.state.loadingCheckLists}/>
                <LinkBorrower
                  expanded={this.state.expanded}
                  toggleButton={this.toggleButton}
                  linkBorrower={firmDetails.entityBorObl}
                  onChangelinkBorrower={this.onChangeFirmDetail}
                  addlinkBorrower={this.addNewBussinessAddress}
                  resetlinkBorrower={this.resetBussinessAddress}
                  LinkBorrowerList={LinkBorrowerList}
                  updateLinkBorrower={this.updateLinkCusipBorrower}
                  deleteLinkBorrower={this.deleteLinkCusipBorrower}
                  entityBorOblErrors={errorFirmDetail.entityBorObl}
                  linkCusipDebType={this.state.linkCusipDebType}
                  deleteAddress={this.deleteAddress}
                  relBorrowerObligor={this.state.relBorrowerObligor}
                  busy={this.state.loadingCheckLists}/>
                <div
                  className="columns"
                  style={{
                  marginTop: "10px"
                }}>
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={this.handleSubmit}>
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            )
            : ("")}
        </section>
      </div>
    )
  }
}
const mapStateToProps = state => {
  const {admClientDetail, auth} = state
  return {admClientDetail, auth}
}

const mapDispatchToProps = {
  saveClientFirmDetail,
  getClientFirmDetailById
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClientsFirms))
