import React from "react"
import ManagementConsole from "./ManagementConsole"
import "./scss/admin.scss"

const AdminManagement = props => {
  const { nav2, nav3 } = props
  return (
    <ManagementConsole links = {props.links}/>
  )
}

export default AdminManagement