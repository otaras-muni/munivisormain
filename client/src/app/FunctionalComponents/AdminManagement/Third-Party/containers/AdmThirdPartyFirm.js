import React from "react"
import {
  withRouter
} from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import {
  connect
} from "react-redux"
import {
  getAllThirdPartyFirmList,
  getThirdPartyFirmDetailById,
  saveThirdPartyFirmDetail,
  searchFirms,
  thirdPartyFirmList,
  addThirdPartyFirm,
  updateThirdPartyFirm,
  checkPlatFormAdmin,
  getAllUserFirmList,
  getIssuerList
} from "AppState/actions/AdminManagement/admTrnActions"
import {
  validateThirdPartyDetail
} from "Validation/thirdParty"
import {DropdownList, Multiselect} from "react-widgets"
import {getPicklistValues,convertError,checkEmptyElObject} from "GlobalUtils/helpers"
import ThirdPartyAddressForm from "../../CommonComponents/FirmAddressForm"
import ListAddresses from "../../CommonComponents/FirmAddressList"
import ThirdPartyDetail from "../components/ThirdPartyDetails"
import Loader from "../../../../GlobalComponents/Loader"

const initialAddressError =(errorFirmDetail,address,initialFirmDetails)=>{
  const arr = ["officeEmails","officePhone","officeFax"]
  arr.map(item=>{
    if(address[item].length>0){
      for (let index = 1; index <(address[item].length); index++) {
        errorFirmDetail.addresses[0][item].push(initialFirmDetails.addresses[0][item][0])
      }
    }   
  })
}
class AdmThirdPartyFirm extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.errorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.cleanErrorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
  }

  async componentWillMount() {
    this.setState({
      loading:true
    })      
  }
  async componentDidMount() {
    const {userEntities} = this.props.auth
    const {firmDetails} = this.state
    let {entityId, searchTenetsFirm} = this.state
    let thirdPartyList=[]
    firmDetails.addresses[0].isPrimary = true
    firmDetails.addresses[0].isActive = true
    const isPlatFormAdmin = await checkPlatFormAdmin()
    let firmList = await getAllUserFirmList()   
    firmList = firmList.map(item=>({label:item.firmName,value:item._id}))    
    const [msrbFirmList,countryResult, msrbRegTypeResult, businessStructure, annualRevenue,numberOfEmployee,marketRole] = await getPicklistValues(["LKUPMSRBFIRMS","LKUPCOUNTRY", "LKUPREGISTRANTTYPE", "LKUPBUSSTRUCT", "LKUPUANNUALREVENUE","LKUPNUMOFPEOPLE","LKUPPARTICIPANTTYPE"])  
    const msrbFirmListResult = [{label:"Select  Entity",value:""}]
    msrbRegTypeResult[2]["Third Party"].forEach(item => {
      Object.keys(msrbFirmList[3][item]).forEach(firm => {
        msrbFirmListResult.push({label:firm,value:msrbFirmList[3][item][firm][0],registrantType:item})
      })
    })
    if(!isPlatFormAdmin){
      thirdPartyList = await thirdPartyFirmList(userEntities.entityId)
      thirdPartyList = thirdPartyList.map(item=>({label:item.firmName,value:item.firmId})) 
      searchTenetsFirm = userEntities.firmName
      entityId  =  userEntities.entityId 
    }
    this.setState((prevState) => ({ ...prevState,
      ...{
        countryResult,       
        msrbRegTypeResult:msrbRegTypeResult[2]["Third Party"],
        businessStructure,
        annualRevenue,
        numberOfEmployee,
        marketRole,
        loading:false,
        firmDetails,
        firmList:[{label:"Select Firms",value:""},...firmList],
        msrbFirmListResult,
        isPlatFormAdmin,
        entityId,
        searchTenetsFirm,
        thirdPartyList:[{label:"Select Third Party Firms",value:""},...thirdPartyList]
      }
    }))
    if(this.props.nav2 && this.props.nav2.length===24){           
      this.props.getThirdPartyFirmDetailById(this.props.nav2)
    } 
  }
  componentWillReceiveProps(nextProps) {

    if (nextProps.admThirdParty.firmDetailById &&
      Object.keys(nextProps.admThirdParty.firmDetailById).length > 0 &&     
      !nextProps.admThirdParty.updated
    ) {
      let {errorFirmDetail, oldFirmDetails} = this.state
      oldFirmDetails = cloneDeep(nextProps.admThirdParty.firmDetailById)
      const firmDetails = cloneDeep(nextProps.admThirdParty.firmDetailById)
      const addressList = cloneDeep(nextProps.admThirdParty.firmDetailById.addresses)
      firmDetails.addresses = cloneDeep(this.initialState().firmDetails.addresses)
      errorFirmDetail =cloneDeep(this.initialState().firmDetails)  
      initialAddressError(errorFirmDetail,firmDetails.addresses[0],this.initialState().firmDetails)   
      const oldErrorFirmDetail = cloneDeep(errorFirmDetail)
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        errorFirmDetail,
        readOnlyFeatures: {
          msrbFirmNameReadOnly: true,
          msrbIdReadOnly: true,
          firmName:true,
          registrantTypeReadOnly:true
        },
        loading:false,
        oldFirmDetails,
        oldErrorFirmDetail,
        addThirdParty:true
      }))
    }    
  } 
  shouldComponentUpdate(){
    return true
  }
  componentWillUpdate(nextState, nextProps) {}
  /** ************************ this is for submit form ********************************* */
  componentDidUpdate(nextState, nextProps) {
  }
  getAddressDetails = (address="",idx)=>{
    if(address!==""){
      const {firmDetails,errorFirmDetail} = this.state
      let id="" 
      if(firmDetails.addresses[idx]._id!==""){
        id = firmDetails.addresses[idx]._id
      }
      firmDetails.addresses[idx] ={...this.initialState().firmDetails.addresses[0]}
      firmDetails.addresses[idx]._id = id
      if(address.addressLine1!==""){
        firmDetails.addresses[idx].addressName = firmDetails.addresses[idx].addressName!=="" ?firmDetails.addresses[idx].addressName :address.addressLine1 
        firmDetails.addresses[idx].addressLine1 = address.addressLine1
        errorFirmDetail.addresses[idx].addressName = ""
        errorFirmDetail.addresses[idx].addressLine1 = ""
      }
      if(address.country!==""){
        errorFirmDetail.addresses[idx].country = ""
        firmDetails.addresses[idx].country = address.country
      }
      if(address.state!==""){
        errorFirmDetail.addresses[idx].state = ""
        firmDetails.addresses[idx].state = address.state
      }
      if(address.city!==""){
        errorFirmDetail.addresses[idx].city = ""
        firmDetails.addresses[idx].city = address.city
      }
      if(address.zipcode!==""){
        errorFirmDetail.addresses[idx].zipCode.zip1 = ""
        firmDetails.addresses[idx].zipCode.zip1 = address.zipcode
      }  
      firmDetails.addresses[idx].formatted_address = address.formatted_address
      firmDetails.addresses[idx].url = address.url
      firmDetails.addresses[idx].location = {...address.location}
      
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...firmDetails          
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        } 
      }))
    }
  }
  /** ********************************************** onChangeAliases *********************** */
  onChangeAliases = (e, key) => {
    const {
      firmDetails,
      errorFirmDetail
    } = this.state
    firmDetails.entityAliases[key] = e.target.value
    const validator = {
      entityAliases: [e.target.value]
    }
    this.setState({
      firmDetails
    })
    if (this.submitted) {
      const errorData = validateThirdPartyDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name) {
          errorFirmDetail[e.target.name][key] = "Required" || err.message
          this.setState({
            errorFirmDetail
          })
        } else {
          errorFirmDetail[e.target.name][key] = ""
          this.setState({
            errorFirmDetail
          })
        }
      } else {
        errorFirmDetail[e.target.name][key] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }

   /** ************************ this is for onchange ********************************* */
   onChangeFirmDetail = (e, key, deepFields) => {
     const {
       firmDetails,
       isEntityExists
     } = this.state
     const {
       errorFirmDetail
     } = this.state
     let keyFirmDetail={}
     let errorKeyFirmDetail={}
     let validator
     if(e.target.name==="firmName"){     
       if(e.target.value!==null){         
         e.target.value = e.target.value.firmName
       }else{
         isEntityExists.firmName=""
         e.target.value=""       
       }
     }      
     if (typeof key !== "undefined") {
       const value = e.target.type === "checkbox" ?!firmDetails.addresses[key][e.target.name] :e.target.value
       if (e.target.name == "zip1" || e.target.name == "zip2") {
         keyFirmDetail = firmDetails.addresses[key].zipCode
         errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
         validator = {
           addresses: [{
             zipCode: {
               [e.target.name]: e.target.value
             }
           }]
         }
       } else {
         keyFirmDetail = firmDetails.addresses[key]
         errorKeyFirmDetail = errorFirmDetail.addresses[key]
         validator = {
           addresses: [{
             [e.target.name]: e.target.value
           }]
         }
       }
       keyFirmDetail[e.target.name] = value/*  */
       if(deepFields){
         deepFields.forEach ( d => {
           keyFirmDetail[d] = ""
         }) 
       }     
       this.setState(prevState => ({
         ...prevState,
         firmDetails
       }))
     } else{
       if(e.target.name==="marketRole"){
         keyFirmDetail = firmDetails.entityFlags
         errorKeyFirmDetail = errorFirmDetail.entityFlags
         validator = {          
           entityFlags: {
             [e.target.name]: e.target.value
           }         
         }
       }else{
         keyFirmDetail = firmDetails
         errorKeyFirmDetail = errorFirmDetail
         validator = {
           [e.target.name]: e.target.value
         }
       }          
       if(deepFields){
         deepFields.forEach ( d => {
           firmDetails.addresses[key][d] = ""
         }) 
       }  
       keyFirmDetail[e.target.name] = e.target.value
       this.setState(prevState => ({
         ...prevState,
         firmDetails
       }))
     }
     if (this.submitted) {
       const errorData = validateThirdPartyDetail(validator)
       if (errorData.error != null) {
         const err = errorData.error.details[0]
         if (err.context.key === e.target.name) {
           errorKeyFirmDetail[e.target.name] = `${err.context.label} Required.`           
           this.setState({
             errorFirmDetail
           })
         } else {
           errorKeyFirmDetail[e.target.name] = ""
           this.setState({
             errorFirmDetail
           })
         }
       } else {
         errorKeyFirmDetail[e.target.name] = ""
         this.setState({
           errorFirmDetail
         })
       }
     }
   }

  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, key, idx) => {
    e.preventDefault()
    const addresses = this.state.firmDetails.addresses[key]
    const errorFirmDetail = this.state.errorFirmDetail
    let errorKeyFirmDetail, validator
    addresses[type][idx][e.target.name] = e.target.value
    validator = {
      addresses: [{
        [type]: [{
          [e.target.name]: e.target.value
        }]
      }]
    }
    this.setState({
      addresses
    })

    if (this.submitted) {
      const errorData = validateThirdPartyDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorFirmDetail.addresses[key][type][idx][e.target.name] ="Required" || err.message
        this.setState({
          errorFirmDetail
        })
      } else {
        errorFirmDetail.addresses[key][type][idx][e.target.name] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }
  // **************************function used to update business addresses *****************************
  /** ************************************************************************** */
  getItemValue(item){
    return `${item.value} - ${item.label}`
  }
  /** ************************************************************** *********** */ 
  getFirmDetails = item => { 
    const {firmDetails, readOnlyFeatures,errorFirmDetail} = this.state 
    if(item.value!==""){
      firmDetails.firmName =item.label,
      firmDetails.msrbFirmName = item.label,
      readOnlyFeatures.firmName = true
      readOnlyFeatures.msrbFirmNameReadOnly = true
      errorFirmDetail.firmName = ""
      errorFirmDetail.msrbId = ""
    }else{
      firmDetails.firmName =""
      readOnlyFeatures.firmName = false
      if(this.submitted){
        errorFirmDetail.firmName = "FirmName Required."
      }else{
        errorFirmDetail.firmName = ""
      }      
    }
    this.setState({
      firmDetails,
      readOnlyFeatures
    })
  } 
  async handleSubmit(event) {
    event.preventDefault()
    this.submitted = true
    const {firmDetails,errorFirmDetail} = this.state
    const  firmDetailsData= {
      _id:firmDetails._id ? firmDetails._id:"",
      entityFlags:firmDetails.entityFlags,
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: false,
      msrbFirmName:firmDetails.msrbFirmName ? firmDetails.msrbFirmName : firmDetails.firmName,
      taxId:firmDetails.taxId,      
      firmName:firmDetails.firmName,
      addresses: firmDetails.addresses
    }    
    try {
      if(firmDetailsData._id===""){
        const {entityId} = this.state
        if(entityId!==""){        
          let {addresses} = firmDetailsData      
          addresses  = addresses.filter(item=>item.isPrimary)
          const errorData = validateThirdPartyDetail(firmDetailsData)
          if (errorData.error || addresses.length!==1 || this.state.isEntityExists.firmName!=="" || this.state.isEntityExists.msrbId!=="") {
            if(errorData.error){
              convertError(errorFirmDetail, errorData)          
            }
            if(addresses.length!==1)
              // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
              toast("One Primary Address  is Required !",{ autoClose:3000, type: toast.TYPE.ERROR})              
            this.setState({errorFirmDetail})
          } else {
            this.setState({
              loading:true
            })       
            const result = await   addThirdPartyFirm({firmDetails:firmDetailsData,entityId})
            const {thirdPartyList} =this.state
            const {expanded} = this.initialState()
            thirdPartyList.push({label:result.firmName,value:result._id})
            if(!result.error){
              this.setState({
                firmDetails:cloneDeep(this.initialState().firmDetails),
                errorFirmDetail:cloneDeep(this.initialState().firmDetails),
                loading:false,
                readOnlyFeatures:{
                  msrbFirmNameReadOnly:false,
                  msrbIdReadOnly:false
                },
                thirdPartyList,
                expanded,
                oldFirmDetails:{}
              })
              this.submitted = false 
              toast("Third Party Firms has been Created!",{ autoClose:2000, type: toast.TYPE.SUCCESS}) 
              // this.props.getThirdPartyFirmDetailById(firmDetailsData._id)                         
              this.props.history.push("/admin-thirdparty")
            }else{
              toast("Something Went Wrong !",{ autoClose:2000, type: toast.TYPE.DANGER})
            }
          }
        }else{
          this.setState({
            firmError:"Select Firm"
          })
        }
      }else{        
        let addresses=[]      
        let {addressList} = this.state 
        firmDetailsData.addresses.forEach(address => {
          if(!checkEmptyElObject(address))
          {   
            addresses.push(address)
          }
        })
        firmDetailsData.addresses = addresses
        if(addresses.length>0){
          addresses = addresses.sort((a, b) => a._id > b._id)
          addressList = addressList.length > 0 ?  addressList.filter((obj) => obj._id != addresses[0]._id) : []
        } 
        const mergeAddresses = [...addressList, ...addresses]
        const filteredAddresses  = mergeAddresses.filter(item=>item.isPrimary)      
        const errorData = validateThirdPartyDetail(firmDetailsData)
        if (errorData.error || filteredAddresses.length!==1 || this.state.isEntityExists.firmName!=="" || this.state.isEntityExists.msrbId!=="") {
          if(errorData.error){
            convertError(errorFirmDetail, errorData)          
          }
          if(filteredAddresses.length ===0)
            toast("One Primary Address  is Required !",{ autoClose:3000, type: toast.TYPE.ERROR})              
          else if(filteredAddresses.length >1)            
            toast("Only One Primary Address  is Required!",{ autoClose:3000, type: toast.TYPE.ERROR})              
          else
            this.state.isPrimaryAddress  = ""
          this.setState({errorFirmDetail})
        } else {    
          this.setState({
            loading:true
          })
          const result = await updateThirdPartyFirm({firmDetails:firmDetailsData,mergeAddresses})        
          if((typeof result.error!=="undefined" && result.error!==null) || (typeof result.isPrimaryAddress!=="undefined" && !result.isPrimaryAddress)){     
            if(result.error!==null)   
              convertError(errorFirmDetail.errorData.error=result.error)   
            if(!result.isPrimaryAddress)
              toast("One Primary Address is Required !",{ autoClose:3000, type: toast.TYPE.ERROR})              
            else
              this.state.isPrimaryAddress  = ""
            this.setState({loading:false})
          }else{
            this.setState({
              firmDetails:cloneDeep(this.initialState().firmDetails),
              errorFirmDetail:cloneDeep(this.initialState().firmDetails),
              loading:false,
              readOnlyFeatures:{
                msrbFirmNameReadOnly:false,
                msrbIdReadOnly:false
              }
            })
            this.submitted = false             
            toast("Third Party Firms has been Update!",{ autoClose:2000, type: toast.TYPE.SUCCESS}) 
            this.props.getThirdPartyFirmDetailById(firmDetailsData._id)         
          } 
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    } 
  }
  /** ************************ this is for add new address ********************************* */
  addNewBussinessAddress = (event)=>{
    event.preventDefault()
    const {firmDetails} = this.state
    const {addresses} = firmDetails    
    let isEmpty = false
    addresses.forEach(address => {
      if(checkEmptyElObject(address))
      {  
        isEmpty = true
              
      }             
    })    
    if(!isEmpty){
      this.setState(prevState => {
        const firmDetails = { ...prevState.firmDetails
        }
        const errorFirmDetail = { ...prevState.errorFirmDetail
        }
        firmDetails.addresses.push(this.initialState().firmDetails.addresses[0])
        errorFirmDetail.addresses.push(this.initialState().firmDetails.addresses[0])
        return {
          firmDetails,
          errorFirmDetail
        }
      })
    }else{
      toast("Already and empty address!",{ autoClose:2000, type: toast.TYPE.INFO})  
    }
  }
  /** ************************ this is for reset form ********************************* */
  resetBussinessAddress = (e) => {
    e.preventDefault()
    const address = cloneDeep(this.initialState().firmDetails.addresses[0])
    address.isPrimary = true
    address.isActive = true
    this.setState(prevState => ({
      firmDetails: {
        ...prevState.firmDetails,
        addresses: [address]
      },
      errorFirmDetail: {
        ...prevState.errorFirmDetail,
        addresses: [this.initialState().firmDetails.addresses[0]]
      },
      inputAddresses:"tsst"
    }))
  } 

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type, key) => {
    e.preventDefault()
    this.setState(prevState => {
      const addMoreData = { ...prevState.firmDetails
      }
      const errorFirmDetail = { ...prevState.errorFirmDetail
      }
      if (type === "aliases") {        
        addMoreData.entityAliases.push("")
        errorFirmDetail.entityAliases.push("")        
        prevState.resetAliases.push(addMoreData.entityAliases.length-1)
      } else {
        addMoreData.addresses[key][type].push(this.initialState().firmDetails
          .addresses[0][type][0])
        errorFirmDetail.addresses[key][type].push(this.initialState().firmDetails
          .addresses[0][type][0])

      }
      return {
        addMoreData
      }
    })
  }
  // **************************function used to update business addresses *****************************
  //* **************************************function used to update business addresses *****************************
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this.state.addressList.find(item => item._id == id)
    this.setState(prevState => {
      let { errorFirmDetail} = prevState
      const {firmDetails, expanded,oldErrorFirmDetail} = prevState
      errorFirmDetail = this.initialState().firmDetails
      firmDetails.addresses = [address]
      const arr = ["officeEmails", "officePhone", "officeFax"]
      arr.forEach(item => {
        if (address[item].length > 1) {
          for (let i = 1; i < address[item].length; i++) {
            errorFirmDetail.addresses[0][item].push(
              this.initialState().firmDetails.addresses[0][item][0]
            )
          }
        }
      })
      expanded.business = true
      oldErrorFirmDetail.addresses = errorFirmDetail.addresses
      return { firmDetails, errorFirmDetail, expanded}
    })
  }
  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = (e) => {
    e.preventDefault()
    const {oldFirmDetails, firmDetails} = this.state
    if(Object.keys(oldFirmDetails).length>0){
      const {entityAliases} = cloneDeep(oldFirmDetails)
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases:oldFirmDetails.entityAliases.map((item)=>this.initialState().firmDetails.entityAliases)
        }
      }))
    }else{
      const {entityAliases} = cloneDeep(this.initialState().firmDetails)
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases:cloneDeep(this.initialState().firmDetails.entityAliases)
        }
      }))
    }
  }
  /** **************************************this is for initial default value********************* */
  initialState = () => ({
    firmDetails: {
      entityAliases: [],
      isMuniVisorClient: false,
      entityFlags: {
        marketRole: []
      },
      taxId:"",
      firmName: "",
      addresses: [{
        addressName: "",
        isPrimary: false,
        isHeadQuarter: false,
        isActive: true,
        website: "",
        officePhone: [{
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }],
        officeFax: [{
          faxNumber: ""
        }],
        officeEmails: [{
          emailId: ""
        }],
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: {
          zip1: "",
          zip2: ""
        }
      }]
    },
    addressList: [],
    resetAliases:[],
    readOnlyFeatures: {
      msrbFirmNameReadOnly: false,
      msrbIdReadOnly: false,
      registrantTypeReadOnly:false,
      firmName:false
    },
    msrbRegTypeResult:[],
    countryResult:[],
    annualRevenue:[],
    numberOfEmployee:[],
    businessStructure:[],
    marketRole:[],
    searchValue:"",
    autocompleteData: [],
    expanded: {
      business: false,
      addOns:false,
      addresslist:false,
      entityname:true,
      entitytype:true
    }, 
    isPrimaryAddress :"",
    isEntityExists:{
      msrbId: "",
      firmName: ""
    },
    oldFirmDetails:{},
    thirdPartyList:[],
    searchFirmName:"",
    searchTenetsFirm:"",
    entityId:"",
    firmError:"",
    updateThirdParty:false,
    addThirdParty:true,
    inputAddresses:""
  })
retrieveDataAsynchronously = (e)=>{

}

onSelectAutoComplete = (e)=>{

}
onChangeAutoComplete = (e)=>{
  this.setState({
    searchValue: e.target.value
  })
}

handleKeyPressinSearch=async (e)=>{
  if (e.key === "Enter" && e.target.value!=="") {
    const result = await this.props.searchFirms(e.target.value)
    this.setState(prevState => ({      
      ...prevState,
      autocompleteData:result
    }))
  }else{
    this.setState(prevState => ({      
      ...prevState,
      autocompleteData:[]
    }))
  }
}
toggleButton = (e, val) => {
  e.preventDefault()
  const { expanded } = this.state
  Object.keys(expanded).map((item)=>{
    if(item === val){
      expanded[item] = !expanded[item]
    }
  })
  this.setState({
    expanded
  })
}
deleteAliases=(idx)=>{
  const {firmDetails, errorFirmDetail} = this.state
  firmDetails.entityAliases.splice(idx, 1)
  errorFirmDetail.entityAliases.splice(idx, 1)
  this.setState({
    firmDetails,
    errorFirmDetail
  })    
}
deleteAddressAliases = (e, type, key, idx)=>{
  const {firmDetails, errorFirmDetail} = this.state
  firmDetails.addresses[key][type].splice(idx, 1)
  errorFirmDetail.addresses[key][type].splice(idx, 1)
  this.setState({
    firmDetails,
    errorFirmDetail
  })
}
/* resetAddressAliases =(e, type, key ,id)=>{ 
  const {firmDetails,errorFirmDetail} = this.state 
  if(id!==""){
    if(this.props.admThirdParty && this.props.admThirdParty.firmDetailById && !this.props.admThirdParty.updated && this.props.match.params.nav2!=="new"){
      const {addresses} = cloneDeep(this.props.admThirdParty.firmDetailById)
      const address = addresses.filter(item=>item._id===id)      
      firmDetails.addresses[key][type]= address[0][type]
      errorFirmDetail.addresses[key][type] =address[0][type].map((item)=>this.initialState().firmDetails.addresses[0][type][0])
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...firmDetails          
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        } 
      }))
    }
    if(this.props.admThirdParty && this.props.admThirdParty.updatedFirmDetails && this.props.admThirdParty.updated && this.props.match.params.nav2!=="new"){
      const {addresses} = cloneDeep(this.props.admThirdParty.updatedFirmDetails)
      const address = addresses.filter(item=>item._id===id)        
      firmDetails.addresses[key][type]= address[0][type] 
      errorFirmDetail.addresses[key][type] = address[0][type].map((item)=>this.initialState().firmDetails.addresses[0][type][0])
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...firmDetails          
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        } 
      }))
    }
  }else{
    firmDetails.addresses[key][type]= this.initialState().firmDetails.addresses[0][type]
    errorFirmDetail.addresses[key][type] = this.initialState().firmDetails.addresses[0][type]
    this.setState({
      firmDetails,
      errorFirmDetail
    })    
  }
} */
resetAddressAliases =(e, type, key ,id)=>{
  if(id!==""){
    this.setState(prevState => {
      const oldFirmDetails =cloneDeep({ ...prevState.oldFirmDetails })
      const {firmDetails, errorFirmDetail} = prevState
      const oldErrorFirmDetail = cloneDeep({ ...prevState.oldErrorFirmDetail })     
      const {addresses} = oldFirmDetails
      const address = addresses.filter(item=>item._id===id)      
      firmDetails.addresses[key][type]= address[0][type]
      errorFirmDetail.addresses[key][type] = cloneDeep(oldErrorFirmDetail.addresses[key][type]) 
      return {firmDetails, errorFirmDetail}
    })
  }else{
    const {firmDetails, errorFirmDetail} = this.state
    firmDetails.addresses[key][type]= this.initialState().firmDetails.addresses[0][type]
    errorFirmDetail.addresses[key][type] = this.initialState().firmDetails.addresses[0][type]
    this.setState({
      firmDetails,
      errorFirmDetail
    })    
  }        
}
onChangeAddressType = (e,idx)=>{ 
  const {firmDetails} = this.state
  let {addressList} = this.state
  let {addresses} = firmDetails
  if(e.target.name==="isPrimary"){
    firmDetails.addresses[idx].isPrimary = !firmDetails.addresses[idx].isPrimary
  }else{
    firmDetails.addresses[idx].isPrimary = false
    firmDetails.addresses[idx].isHeadQuarter = true     
  }   
  if(this.submitted){      
    addressList = addressList.length > 0 ?  addressList.filter((obj) => obj._id != addresses[0]._id) : []   
    addresses = addresses.sort((a, b) => a._id > b._id)      
    const mergeAddresses = [...addressList, ...addresses]
    const filteredAddresses  = mergeAddresses.filter(item=>item.isPrimary)  
    if(filteredAddresses.length ===0){
      // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
      toast("One Primary Address  is Required !",{ autoClose:3000, type: toast.TYPE.ERROR})              
    }
    else if(filteredAddresses.length >1){
      // this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
      toast("Only One Primary Address  is Required !",{ autoClose:3000, type: toast.TYPE.ERROR})              
    }else
      this.state.isPrimaryAddress  = ""
  }
  this.setState({
    firmDetails
  })
}
onSearchThirdParty = (item)=>{ 
  const {firmDetails,readOnlyFeatures} = cloneDeep(this.initialState())
  if(item.value!==""){  
    this.props.getThirdPartyFirmDetailById(item.value)
    this.setState({
      searchFirmName:item.label
    })
  }else{
    this.setState(prevState => ({
      ...prevState,
      firmDetails,
      addressList:[],
      errorFirmDetail:cloneDeep(this.initialState().firmDetails),
      loading:false,
      oldFirmDetails:{},
      searchFirmName:"",
      readOnlyFeatures
    }))  
  }
}
checkDuplicateFirm = async(e)=>{
  e.preventDefault()
  const {value,name} = e.target
  const {entityId} = this.props.auth.userEntities 
  const {isEntityExists} = this.state
  if(value.length>0){
    const result = await checkDuplicateEntity(value, entityId)
    if(result.success.isExist){
      isEntityExists[name] = "Firms Name is Already Exists"
    }else{        
      isEntityExists[name] = ""
    }         
    this.setState({
      isEntityExists
    })
  }
}
deleteAddress = (e,type,idx)=>{
  const {firmDetails, errorFirmDetail} = this.state
  firmDetails[type].splice(idx, 1)
  errorFirmDetail[type].splice(idx, 1)
  this.setState({
    firmDetails,
    errorFirmDetail
  })     
}
addNewFirm = (e)=>{
  e.preventDefault()
  const {firmDetails} = cloneDeep(this.initialState())
  firmDetails.entityBorObl = []
  this.setState(prevState => ({
    ...prevState,
    firmDetails,
    addressList:[],
    errorFirmDetail:cloneDeep(this.initialState().firmDetails),
    loading:false,
    oldFirmDetails:{},
    searchFirmName:"",
    readOnlyFeatures:{
      registrantTypeReadOnly:false,
      msrbFirmNameReadOnly:false,
      msrbId:false
    }
  }))  
}
onSearchFirm = async(item, metaData)=>{ 
  metaData.originalEvent.preventDefault()
  let {thirdPartyList} = this.state
  const {firmDetails,readOnlyFeatures} = cloneDeep(this.initialState())
  firmDetails.entityBorObl = []
  this.setState({
    loading:true
  })
  if(item.value!==""){  
    thirdPartyList = await thirdPartyFirmList(item.value)
    thirdPartyList = thirdPartyList.map(item=>({label:item.firmName,value:item.firmId})) 
    this.setState({
      firmDetails,
      addressList:[],
      searchTenetsFirm:item.label,
      searchFirmName:"",
      errorFirmDetail:cloneDeep(this.initialState().firmDetails),
      thirdPartyList:[{label:"Select Third Firms",value:""},...thirdPartyList],
      loading:false,
      readOnlyFeatures,
      entityId:item.value,
      firmError:""
    })
  }else{
    this.setState(prevState => ({
      ...prevState,
      firmDetails,
      addressList:[],
      searchTenetsFirm:"",
      errorFirmDetail:cloneDeep(this.initialState().firmDetails),
      loading:false,
      searchFirmName:"",
      thirdPartyList:[],
      loading:false,
      readOnlyFeatures,
      entityId:"",
      firmError:""
    }))
  }
} 
 
checkAddressStatus = async(e,id,field)=>{
  const {addressList} = this.state
  let addressIdx=""
  addressList.forEach((address,idx) => {
    if(address._id==id){
      addressIdx = idx
    }
  })       
  if((field==="isPrimary" && !addressList[addressIdx].isActive)|| (field==="isActive" && addressList[addressIdx].isPrimary)){
    toast("Primary Address Can not be Inactive.",{ autoClose:2000, type: toast.TYPE.WARNING})
  }else{     
    addressList[addressIdx][field] = !addressList[addressIdx][field]  
    if(field==="isPrimary"){
      addressList.forEach((address,idx) => {
        if(address._id===id)
          addressList[idx][field] = true
        else
          addressList[idx][field] = false
      })                        
    }                
    this.setState(prevState => ({
      ...prevState,
      addressList
    })) 
  }   
}
addUpdateThirdParty = (e,type)=>{
  e.preventDefault()
  const {searchFirm} = this.state     
  const {firmDetails,expanded} = cloneDeep(this.initialState() )
  this.setState(prevState => ({
    firmDetails,        
    addressList:[],
    oldUsersDetails:{},
    readOnlyFeatures:{
      registrantTypeReadOnly:false,
      msrbFirmNameReadOnly:false,
      msrbId:false,
      clientTypeDisabled:false,
      firmName:false
    },
    errorUserDetail:cloneDeep(this.initialState().firmDetails),
    expanded,
    updateThirdParty:(type==="updateThirdParty"),
    addThirdParty:(type==="addThirdParty")
  }))
} 
getEntityIssuerList = async(inputVal)=>{
  if (!inputVal) {
    return({ options: [] })
  }   
  const issuerList = await getIssuerList({inputVal, entityType:"Third Party"})    
  if(issuerList.status===200 && issuerList.data)
    return { options:issuerList.data }
  return { options: [] }
} 
render() {
  const loading = () => <Loader/>
  if(this.state.loading) {
    return loading()
  } 
  const {firmDetails,countryResult, errorFirmDetail, thirdPartyList, readOnlyFeatures, 
    msrbRegTypeResult, addressList, marketRole,expanded, firmList} = this.state
  return (     
    <section className="firms">
      {/*       <div className="columns">
        <div className="column is-full">
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link" onClick={(e)=>{this.addUpdateThirdParty(e,"addThirdParty")}}>
                              Add Firm
              </button>
            </div>  
            <div className="control">
              <button className="button is-link" onClick={(e)=>{this.addUpdateThirdParty(e,"updateThirdParty")}}>
                              Update Firm
              </button>
            </div>        
          </div>
        </div>             
      </div>  */}
      {this.state.isPlatFormAdmin && <div className="columns">
        <div className="column">
          <DropdownList 
            filter="contains"
            value={this.state.searchTenetsFirm}
            data={firmList} 
            groupBy={item => item.firmType}
            message="select MSRB Firm Name" 
            textField='label' valueField="value"
            onChange={this.onSearchFirm}                 
          />
          {this.state.firmError && <small className="text-error">{this.state.firmError}</small>}
        </div>
      </div>}           
      <div className="columns"  style={{display:this.state.updateThirdParty ? "block" : "none"}}>
        <div className="column">
          <DropdownList 
            filter="contains"
            value={this.state.searchFirmName}
            data={thirdPartyList} 
            message="select Firm Name" 
            textField='label' valueField="value"
            onChange={this.onSearchThirdParty} />
        </div>
      </div>
      {
        this.state.addThirdParty
          ?
          <section className="accordions">
            <article className={this.state.expanded.entitytype ? "accordion is-active" : "accordion"}>
              <div className="accordion-header toggle">
                <p
                  onClick={event => {
                    this.toggleButton(event, "entitytype")
                  }}
                >Entity Type</p>
                {
                  this.state.expanded.entitytype ? <i className="fas fa-chevron-up" style={{ cursor: "pointer" }} />
                  : <i className="fas fa-chevron-down" style={{ cursor: "pointer" }} />
               } <br />
                
              </div>
              {this.state.expanded.entitytype &&
              <div className = "accordion-body"> 
                <div className = "accordion-content">
                  <div className="columns">
                    <div className="column">
                      <p className="title innerPgTitle">Market Role / Participant Type</p>
                      <div className="control">
                        <Multiselect
                          name = "marketRole"
                          data={this.state.marketRole[1] ? this.state.marketRole[1]:[]}
                          value={firmDetails.entityFlags.marketRole}
                          onChange={(val)=>{
                            const event={
                              target:{
                                name:"marketRole",
                                value:val
                              }}
                            this.onChangeFirmDetail(event)
                          }}
                        />
                      </div>
                      {this.state.errorFirmDetail && this.state.errorFirmDetail.entityFlags.marketRole && <small className="text-error">{this.state.errorFirmDetail.entityFlags.marketRole}</small>}
                    </div>
                  </div>
                </div>
              </div>}
            </article>            
            <article className={this.state.expanded.entityname ? "accordion is-active" : "accordion"}>
              <div className="accordion-header toggle">
                <p
                  onClick={event => {
                    this.toggleButton(event, "entityname")
                  }}
                >Entity Name</p>{
                  this.state.expanded.entityname ? <i className="fas fa-chevron-up" style={{ cursor: "pointer" }} />
                  : <i className="fas fa-chevron-down" style={{ cursor: "pointer" }} />
               } <br />
              </div>
              {this.state.expanded.entityname && <ThirdPartyDetail
                firmDetails = {firmDetails}
                errorFirmDetail = {errorFirmDetail}
                onChangeFirmDetail = {this.onChangeFirmDetail}
                addMore = {this.addMore}
                getFirmDetails = {this.getFirmDetails}
                resetAliases={this.resetAliases}
                onChangeAliases={this.onChangeAliases}
                readOnlyFeatures={readOnlyFeatures}                                                       
                msrbRegTypeResult ={msrbRegTypeResult}
                marketRole = {marketRole}
                autocompleteData = {this.state.autocompleteData}
                searchValue = {this.state.searchValue}
                onSelectAutoComplete = {this.onSelectAutoComplete}
                onChangeAutoComplete = {this.onChangeAutoComplete}
                handleKeyPressinSearch = {this.handleKeyPressinSearch}
                getItemValue = {this.getItemValue}
                deleteAliases= {this.deleteAliases}
                updated= {this.state.updated}
                disabled = {this.state.disabled}
                msrbFirmListResult={this.state.msrbFirmListResult}
                getEntityFirmList = {this.getEntityIssuerList}
                checkDuplicateFirm = {this.checkDuplicateFirm}
                isEntityExists = {this.state.isEntityExists}
                canEdit
                cavView={false}
              />} 
            </article>                    
            {addressList.length>0 && <ListAddresses 
              addressList = {addressList}
              updateAddress = {this.updateAddress}
              listAddressToggle = {expanded.addresslist}
              toggleButton = {this.toggleButton}
              checkAddressStatus = {this.checkAddressStatus}
            />}           
            <article className={this.state.expanded.business ? "accordion is-active" : "accordion"}>
              <div className="accordion-header">
                <p
                  onClick={event => {
                    this.toggleButton(event, "business")
                  }}
                >Business Address</p>
                <div className = "field is-grouped">
                  <div className = "control">
                    <button className = "button is-link is-small"onClick = {this.addNewBussinessAddress}> Add </button> 
                  </div> 
                  <div className = "control">
                    <button className = "button is-light is-small"
                      onClick = {this.resetBussinessAddress} > Reset </button> 
                  </div> 
                </div> 
              </div> 
              {expanded.business && <div className = "accordion-body" > 
                {
                  firmDetails.addresses.map((businessAddress, idx) => ( 
                    <ThirdPartyAddressForm 
                      key={idx}
                      businessAddress={businessAddress}
                      idx={idx}
                      onChangeBusinessAddress={this.onChangeFirmDetail}
                      addMore={this.addMore}
                      onChangeAddMore={this.onChangeAddMore}
                      errorFirmDetail={
                        errorFirmDetail.addresses[idx]
                      }
                      countryResult={countryResult}
                      resetAddressAliases = {this.resetAddressAliases}   
                      deleteAddressAliases = {this.deleteAddressAliases} 
                      onChangeAddressType = {this.onChangeAddressType}
                      deleteAddress = {this.deleteAddress}
                      isPrimaryAddress = {this.state.isPrimaryAddress}
                      getAddressDetails = {this.getAddressDetails}
                      inputAddresses = {this.state.inputAddresses}
                    />
                  ))
                } 
              </div>} 
            </article> 
            <br></br>
            <div className = "columns">
              <div className = "column is-full" >
                <div className = "field is-grouped-center" >
                  <div className = "control" >
                  
                    <button className = "button is-link"
                      onClick = {this.handleSubmit}> Save </button> 
                  </div> 
                  <div className = "control" >
                    <button className = "button is-light"> Cancel </button> </div> 
                </div> 
              </div> 
            </div> 
          </section>
          :""
      } 
    </section> 
  )
}
}
const mapStateToProps = (state) => {
  const {admThirdParty,auth} = state
  return { admThirdParty,auth}
}
const mapDispatchToProps = {
  getThirdPartyFirmDetailById,
  saveThirdPartyFirmDetail,
  getAllThirdPartyFirmList,
  searchFirms
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(
  AdmThirdPartyFirm))
