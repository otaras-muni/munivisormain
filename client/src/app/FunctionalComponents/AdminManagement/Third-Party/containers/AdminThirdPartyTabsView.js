import React, { Component } from "react"
import "react-table/react-table.css"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import ThirdPartyFirmListContainer from "./ThirdPartyFirmListContainer"
import MigratedFirmListContainer from "../../../EntityManagement/clients/containers/MigratedFirmListContainer"
import "../../scss/entity.scss"

class AdminClientFirmTabsView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabActiveIndex: 0,
      TABS: [
        { label: "Third Party" },
        { label: "Migrated Entities" },
      ]
    }
  }

  identifyComponentToRender = (index) => {
    this.setState({
      tabActiveIndex:index
    })
  }

  renderTabContents = () => {
    const {TABS,tabActiveIndex} =this.state
    const { auth} = this.props
    const relationshipToTenant = (auth && auth.userEntities && auth.userEntities.relationshipToTenant ) || ""
    let NEWTABS = []
    if ( relationshipToTenant === "Self") {
      NEWTABS = [...TABS]
    } else {
      NEWTABS= [ { label: "Clients / Prospects" }]
    }
    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        NEWTABS.map ( (t,index) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(index)}>
            <a className="tabSecLevel">
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedView = (option) => {
    switch (option) {
    case 0 :
      return <ThirdPartyFirmListContainer listType="client-prospect" activeTab="Clients / Prospects" />
    case 1 :
      return <MigratedFirmListContainer listType="migratedentities" activeTab="Migrated" />
    default:
      return option
    }
  }

  render() {
    return (
      <div id="main">
        <div className="tabs">
          {this.renderTabContents()}
        </div>
        {this.renderSelectedView(this.state.tabActiveIndex)}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

export default withRouter(connect(mapStateToProps,null)(AdminClientFirmTabsView))
