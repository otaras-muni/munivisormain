import React from "react"
import { Redirect } from "react-router"
import ThirdPartyFirmList from "./containers/ThirdPartyFirmListContainer"
import ThirdPartyView from "./ThirdPartyView"


const FirmsMain = props => {
  const { nav2, nav3 } = props
  if (nav2) {
    return (
      <ThirdPartyView id={nav2} navComponent={nav3} />
    )
  }
  return (
    <ThirdPartyFirmList userType="admin"/>
  )
}

export default  FirmsMain
