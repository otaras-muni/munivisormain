import React from "react"

const ListAddresses = (props) => (
  <article className="accordion is-active">
    <div className="accordion-header toggle">
      <p>Manage Available Addresses</p>
    </div>

    <div className="accordion-body">
      <div className="accordion-content">

        <div style={{ overflowX: "auto" }}>

          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <thead>
              <tr>
                <th>
                  <p className="multiExpLbl">Address Name</p>
                </th>
                <th>
                  <p className="multiExpLbl">Is Primary?</p>
                </th>
                <th>
                  <p className="multiExpLbl">Is Headquarter?</p>
                </th>
                <th>
                  <p className="multiExpLbl">Inactive</p>
                </th>
                <th>
                  <p className="multiExpLbl">Update</p>
                </th>
              </tr>
            </thead>
            <tbody>
              {
                props.addressList ?
                  props.addressList.map((address, idx) => (
                    <tr key={idx}>
                      <td>{address.addressName}</td>
                      <td>
                        <p className="emmaTablesTd">
                          <input type="checkbox" checked={address.isPrimary} />
                        </p>
                      </td>
                      <td>
                        <p className="emmaTablesTd">
                          <input type="checkbox" checked={address.isHeadQuarter} />
                        </p>
                      </td>
                      <td>
                        <p className="emmaTablesTd">
                          <input type="checkbox" checked={address.isActive} />
                        </p>
                      </td>
                      <td>
                        <a href="#">
                          <span className="has-text-link">
                            <i className="fas fa-pencil-alt is-small" onClick={
                              (event) => {
                                event.preventDefault()
                                props.updateAddress(event, address._id)
                              }} />
                          </span>
                        </a>
                      </td>
                    </tr>
                  ))
                  : ""
              }
            </tbody>
          </table>
        </div>

      </div>

    </div>

  </article>
)

export default ListAddresses