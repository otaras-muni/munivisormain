import React from "react"
import {
  FormGroup
} from "react-bootstrap"

const state = [
  {
    value: "",
    label: "Pick State"
  },
  {
    value: "lkupcity",
    label: "LKUPCITY"
  },
  {
    value: "Tampa",
    label: "Tampa"
  }
]

const city = [
  {
    value: "",
    label: "Pick City"
  },
  {
    value: "lkupcity",
    label: "LKUPCITY"
  },
  {
    value: "Tampa",
    label: "Tampa"
  }
]

const country = [
  {
    value: "",
    label: "Pick Country"
  },
  {
    value: "lkupcity",
    label: "LKUPCITY"
  },
  {
    value: "Tampa",
    label: "Tampa"
  }
]

const SelectOption = (props) => (
  <select name={props.title}
    onChange={
      (event) => {
        props.onChangeBusinessAddress(event, props.idx)
      }
    }
    value={props.currentValue}
  >
    {props.items.map((item, index) => (
      <option value={item.value} key={index} >{item.label}</option>
    ))
    }
  </select>
)

// Admin transaction form
const ThirdPartyAddressForm = (props) => (
  <div className="accordion-content" key={props.idx}>
    <div className="tile is-ancestor">

      <div className="tile is-12 is-vertical is-parent">

        <div className="columns">
          <div className="column is-half">
            <p className="multiExpLbl ">Address Name</p>
            <div className="control">
              <FormGroup className={props.errorFirmDetail.addressName != "" ? "has-error" : ""}>
                <input className="input is-small is-link" name="addressName" type="text" value={props.businessAddress.addressName} placeholder="Address identifier"
                  onChange={
                    (event) => {
                      props.onChangeBusinessAddress(event, props.idx)
                    }
                  }
                />
                {props.errorFirmDetail.addressName != "" ? <span className="help-block">{props.errorFirmDetail.addressName}</span> : ""}
              </FormGroup>
            </div>
          </div>
          <div className="column">
            <p className="emmaTablesTd">
              <input name="isPrimary" type="checkbox" value={props.businessAddress.isPrimary}
                defaultChecked={props.businessAddress.isPrimary}
                onClick={
                  (event) => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }
                }
              /> Is Primary?
            </p>
          </div>
          <div className="column">
            <p className="emmaTablesTd">
              <input name="isHeadQuarter" type="checkbox" value={props.businessAddress.isHeadQuarter}
                defaultChecked={props.businessAddress.isHeadQuarter}
                onClick={
                  (event) => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }
                }
              /> Is Headquarter?
            </p>
          </div>
        </div>

        <div className="columns">
          {/*                         <div className="column is-half">
                           <p className="multiExpLbl">Firm / Organization Name</p>
                            <FormGroup className={props.errorFirmDetail.firmName != '' ? 'has-error' : ''}>
                                <div className="control">
                                    <input name="firmName" className="input is-small is-link" type="text" value={props.businessAddress.firmName} placeholder="Enter Firm Name"
                                        onChange={
                                            (event) => {
                                                props.onChangeBusinessAddress(event, props.idx)
                                            }
                                        }
                                    />
                                </div>
                                {props.errorFirmDetail.firmName != '' ? <span className="help-block">{props.errorFirmDetail.firmName}</span> : ''}
                            </FormGroup>
                        </div> */}
          <div className="column">
            <p className="multiExpLbl ">Website(s)</p>
            <FormGroup className={props.errorFirmDetail.website != "" ? "has-error" : ""}>
              <div className="control">
                <input className="input is-small is-link" name="website" type="text" placeholder="Enter Website URL" value={props.businessAddress.website}
                  onChange={
                    (event) => {
                      props.onChangeBusinessAddress(event, props.idx)
                    }
                  }
                />
              </div>
              {props.errorFirmDetail.website != "" ? <span className="help-block">{props.errorFirmDetail.website}</span> : ""}
            </FormGroup>
          </div>
        </div>

        <div className="columns">
          <div className="column is-third">
            <p className="multiExpLbl ">Office Phone</p>
            {
              props.businessAddress.officePhone.map((item, idx) => (
                <div className="field is-grouped" key={idx}>
                  <FormGroup className={props.errorFirmDetail.officePhone[idx].phoneNumber != "" ? "has-error" : ""}>
                    <div className="control">
                      <input className="input is-small is-link" name="phoneNumber" size="10" type="text" placeholder="Numbers only"
                        value={item.phoneNumber}
                        onChange={
                          (event) => {
                            props.onChangeAddMore(event, "officePhone", props.idx, idx)
                          }
                        }
                      />
                    </div>
                    {props.errorFirmDetail.officePhone[idx].phoneNumber != "" ? <span className="help-block">{props.errorFirmDetail.officePhone[idx].phoneNumber}</span> : ""}
                  </FormGroup>
                  <FormGroup className={props.errorFirmDetail.officePhone[idx].extension != "" ? "has-error" : ""}>
                    <div className="control deleteadressaliases">
                      <input className="input is-small is-link" name="extension" size="5" type="text" placeholder="Ext"
                        value={item.extension}
                        onChange={
                          (event) => {
                            props.onChangeAddMore(event, "officePhone", props.idx, idx)
                          }
                        }
                      />
                    </div>
                    {props.errorFirmDetail.officePhone[idx].extension != "" ? <span className="help-block">{props.errorFirmDetail.officePhone[idx].extension}</span> : ""}
                  </FormGroup>
                </div>
              ))
            }
            <div className="control">
              <button className="button is-link is-small"
                onClick={
                  (event) => {
                    props.addMore(event, "officePhone", props.idx)
                  }
                }>Add More</button>
            </div>
          </div>
          <div className="column">
            <p className="multiExpLbl">Office Fax</p>
            {
              props.businessAddress.officeFax.map((item, idx) => (
                <div className="field" key={idx}>
                  <FormGroup className={props.errorFirmDetail.officeFax[idx].faxNumber != "" ? "has-error" : ""}>
                    <div className="control d-flex">
                      <input className="input is-small is-link" name="faxNumber" size="10" type="text" placeholder="Numbers only"
                        value={item.faxNumber}
                        onChange={
                          (event) => {
                            props.onChangeAddMore(event, "officeFax", props.idx, idx)
                          }
                        }
                      />
                    </div>
                    {props.errorFirmDetail.officeFax[idx].faxNumber != "" ? <span className="help-block">{props.errorFirmDetail.officeFax[idx].faxNumber}</span> : ""}
                  </FormGroup>
                </div>
              ))
            }
            <div className="control">
              <button className="button is-link is-small"
                onClick={
                  (event) => {
                    props.addMore(event, "officeFax", props.idx)
                  }
                }
              >Add More</button>
            </div>
          </div>

          <div className="column">
            <p className="multiExpLbl">Office Email</p>
            {
              props.businessAddress.officeEmails.map((item, idx) => (
                <div className="field" key={idx}>
                  <FormGroup className={props.errorFirmDetail.officeEmails[idx].emailId != "" ? "has-error" : ""}>
                    <div className="control">
                      <input className="input is-small is-link" name="emailId" type="email" placeholder="e.g. alexsmith@gmail.com"
                        value={item.emailId
                        }
                        onChange={
                          (event) => {
                            props.onChangeAddMore(event, "officeEmails", props.idx, idx)
                          }
                        }
                      />
                    </div>
                    {props.errorFirmDetail.officeEmails[idx].emailId != "" ? <span className="help-block">{props.errorFirmDetail.officeEmails[idx].emailId}</span> : ""}
                  </FormGroup>
                </div>
              ))
            }
            <div className="control">
              <button className="button is-link is-small"
                onClick={
                  (event) => {
                    props.addMore(event, "officeEmails", props.idx)
                  }
                }
              >Add More</button>
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-half">
            <p className="multiExpLbl">Address Line 1</p>
            <FormGroup className={props.errorFirmDetail.addressLine1 != "" ? "has-error" : ""}>
              <div className="control">
                <input className="input is-small is-link" name="addressLine1" type="text" placeholder="address line 1" value={props.businessAddress.addressLine1}
                  onChange={
                    (event) => {
                      props.onChangeBusinessAddress(event, props.idx)
                    }
                  }
                />
              </div>
              {props.errorFirmDetail.addressLine1 != "" ? <span className="help-block">{props.errorFirmDetail.addressLine1}</span> : ""}
            </FormGroup>
          </div>
          <div className="column">
            <p className="multiExpLbl">Address Line 2</p>
            <FormGroup className={props.errorFirmDetail.addressLine2 != "" ? "has-error" : ""}>
              <div className="control">
                <input className="input is-small is-link" type="text" name="addressLine2" placeholder="address line 2" value={props.businessAddress.addressLine2}
                  onChange={
                    (event) => {
                      props.onChangeBusinessAddress(event, props.idx)
                    }
                  }
                />
              </div>
              {props.errorFirmDetail.addressLine2 != "" ? <span className="help-block">{props.errorFirmDetail.addressLine2}</span> : ""}
            </FormGroup>
          </div>
        </div>

        <div className="columns">
          <div className="column is-one-fourth">
            <p className="multiExpLbl">Country</p>
            <FormGroup className={props.errorFirmDetail.country != "" ? "has-error" : ""}>
              <div className="control">
                <div className="select">
                  <SelectOption
                    idx={props.idx}
                    items={country}
                    title="country"
                    currentValue={props.businessAddress.country}
                    onChangeBusinessAddress={props.onChangeBusinessAddress}
                  />
                </div>
              </div>
              {props.errorFirmDetail.country != "" ? <span className="help-block">{props.errorFirmDetail.country}</span> : ""}
            </FormGroup>
          </div>
          <div className="column">
            <p className="multiExpLbl">State</p>
            <FormGroup className={props.errorFirmDetail.state != "" ? "has-error" : ""}>
              <div className="control">
                <div className="select">
                  <SelectOption
                    idx={props.idx}
                    items={state}
                    title="state"
                    currentValue={props.businessAddress.state}
                    onChangeBusinessAddress={props.onChangeBusinessAddress}
                  />
                </div>
              </div>
              {props.errorFirmDetail.state != "" ? <span className="help-block">{props.errorFirmDetail.state}</span> : ""}
            </FormGroup>
          </div>
          <div className="column">
            <p className="multiExpLbl">City</p>
            <FormGroup className={props.errorFirmDetail.city != "" ? "has-error" : ""}>
              <div className="control">
                <div className="select">
                  <SelectOption
                    idx={props.idx}
                    items={city}
                    title="city"
                    currentValue={props.businessAddress.city}
                    onChangeBusinessAddress={props.onChangeBusinessAddress}

                  />
                </div>
              </div>
              {props.errorFirmDetail.city != "" ? <span className="help-block">{props.errorFirmDetail.city}</span> : ""}
            </FormGroup>
          </div>
          <div className="column">
            <p className="multiExpLbl">Zip Code</p>
            <div className="field is-grouped">
              <FormGroup className={props.errorFirmDetail.zipCode.zip1 != "" ? "has-error" : ""}>
                <div className="control">
                  <input className="input is-small is-link" name="zip1" size="5" type="text" placeholder="Zip-1" value={props.businessAddress.zipCode.zip1}
                    onChange={
                      (event) => {
                        props.onChangeBusinessAddress(event, props.idx)
                      }
                    }
                  />
                </div>
                {props.errorFirmDetail.zipCode.zip1 != "" ? <span className="help-block">{props.errorFirmDetail.zipCode.zip1}</span> : ""}
              </FormGroup>
              <FormGroup className={props.errorFirmDetail.zipCode.zip2 != "" ? "has-error" : ""}>
                <div className="control">
                  <input className="input is-small is-link" name="zip2" size="5" type="text" placeholder="Zip-2" value={props.businessAddress.zipCode.zip2}
                    onChange={
                      (event) => {
                        props.onChangeBusinessAddress(event, props.idx)
                      }
                    }
                  />
                </div>
                {props.errorFirmDetail.zipCode.zip2 != "" ? <span className="help-block">{props.errorFirmDetail.zipCode.zip2}</span> : ""}
              </FormGroup>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
)
export default ThirdPartyAddressForm