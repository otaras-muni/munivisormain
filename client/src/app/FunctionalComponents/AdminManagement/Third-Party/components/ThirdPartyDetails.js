import React from "react"
import {TextLabelInput, AsyncCreatableSelectInput} from "../../../../GlobalComponents/TextViewBox"

const ThirdPartyDetails = props => (
  <div className="accordion-body">
    <div className="accordion-content">
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">
            Entity Name
          </p>
          <AsyncCreatableSelectInput
            multi={false}
            cacheOptions={false}
            value={{"firmName":props.firmDetails.firmName}}
            onChange={(val)=>{
              const event={
                target:{
                  name:"firmName",
                  value:val
                }}
              props.onChangeFirmDetail(event)
            }}
            // groupBy={(row) =>row.participantType}
            labelKey="firmName"
            className="field"
            loadOptions={props.getEntityFirmList||[]}
            backspaceRemoves
            allowCreateWhileLoadingboolean={false}
            disabled={props.readOnlyFeatures.firmName}
            disableValue={props.firmDetails.firmName}
            onBlur={props.checkDuplicateFirm}
            error={props.errorFirmDetail && props.errorFirmDetail.firmName ||props.isEntityExists && props.isEntityExists.firmName }
          />
          {props.firmDetails.entityAliases &&
          props.firmDetails.entityAliases.length
            ? props.firmDetails.entityAliases.map((item, idx) => (
              <div className="field is-grouped-left">
                {!props.canEdit ?<small>{item}</small>
                  :<div className="control deletealiases">
                    <input
                      className="input is-small is-link"
                      name="entityAliases"
                      type="text"
                      placeholder="Enter Aliases"
                      value={item}
                      onChange={event => {
                        console.log("Event ")
                        props.onChangeAliases(event, idx)
                      }}
                    />
                    <span className="has-text-link fa-delete"
                      onClick = {()=>{
                        props.deleteAliases(idx)
                      }}
                    >
                      <i className="far fa-trash-alt" />
                    </span>
                  </div>}
                {props.errorFirmDetail &&  props.errorFirmDetail.entityAliases[idx] && <small className="text-error">{props.errorFirmDetail.entityAliases[idx]}</small>}
              </div>
            ))
            : ""}
          {props.canEdit ? <div className="field is-grouped-left">
            <div className="control">
              <button
                className="button is-small is-link"
                onClick={event => {
                  props.addMore(event, "aliases")
                }}
              >
                Add Alias
              </button>
            </div>
            <div className="control">
              <button
                className="button is-small is-light"
                onClick={props.resetAliases}
              >
                Cancel
              </button>
            </div>
          </div>
            :""}
        </div>
        <TextLabelInput
          name="taxId"
          value={props.firmDetails.taxId}
          placeholder="Enter Tax ID"
          style={{width: "100%"}}
          label="Tax ID"
          onChange={props.onChangeFirmDetail}
          disabled={!props.canEdit || false}
          error={props.errorFirmDetail &&  props.errorFirmDetail.taxId ? props.errorFirmDetail.taxId:""}
        />
      </div>
    </div>
  </div>
)
export default ThirdPartyDetails
