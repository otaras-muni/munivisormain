import React from "react"
import Switch, { Case, Default } from "react-switch-case"
import { NavLink , Redirect} from "react-router-dom"
// import AdmThirdPartyFirm from "./containers/AdmThirdPartyFirm"
import ThirdPartyNew from "../../ThirdParty/ThirdParty"
import AdmThirdPartyContacts from "./containers/AdmThirdPartyContacts"
import Documents from "./../../EntityManagement/Documents"
import ContactList from "./../../EntityManagement/FirmLists/ContactListContainer"

const TABS = [
  { path: "entity", label: "3rd Party Firm" },
  { path: "contacts", label: "3rd Party Contacts" },
  { path: "documents", label: "3rd Party Documents" }
]
const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

const ThirdPartyView = props => {
  let { id, navComponent } = props
  navComponent = navComponent || "details"
  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink key={t.path} to={`/admin-thirdparty/${id}/${t.path}`} activeStyle={activeStyle}> {t.label} </NavLink>
      </li>
    ))
  const renderViewSelection = (id, navComponent) => (
    <nav className="tabs is-boxed">
      <ul>
        {renderTabs(TABS, navComponent, id)}
      </ul>
    </nav>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(id, navComponent)}
          </div>
        </div>
      </div>
      <div id="main">
        <Switch condition={navComponent}>
          <Case value='entity'>
            <ThirdPartyNew nav2= {id}/>
          </Case>
          <Case value='contacts'>
            <ContactList nav2={id} />
          </Case>
          <Case value='documents'>
            <Documents nav2 ={id} />
          </Case>
          <Default>
            <Redirect to="/admin-thirdparty" />
          </Default>
        </Switch>
      </div>
    </div>
  )
}

export default ThirdPartyView
