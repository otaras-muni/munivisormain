import React from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import { DropdownList } from "react-widgets"
import ReactTooltip from "react-tooltip"
import {toast} from "react-toastify"
import { confirmAlert } from "react-confirm-alert"
import "react-confirm-alert/src/react-confirm-alert.css"
import {
  getPicklistByPicklistName,
  getAllTenantUserEmails,
  getAllTenantEntityNames,
  updateAuditLog,
  checkEmptyElObject,
  getUniqValueFromArray
} from "GlobalUtils/helpers"
import CONST from "../../../../../globalutilities/consts"
import { addGridData, setStepIndex, setTypeOfFileUpload } from "../../../../StateManagement/actions/AdminManagement/admMigrationTools"
import {fetchDMObject, postJSONData, putDMMappingObject} from "../../../../StateManagement/actions/Common"
import Loader from "../../../../GlobalComponents/Loader"
import {addDocInDB, getSignedUrl} from "../../../../StateManagement/actions/docs_actions"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import {fetchLeadAdvisor} from "../../../../StateManagement/actions/CreateTransaction"
import {removeWhiteSpaces} from "../../../../../globalutilities/helpers"

class MapColumns extends React.Component {

  constructor(props) {
    super(props)
    const headers = Object.keys((props.gridData && props.gridData[0]) || {}).filter(key => key.toLowerCase().search("empty") === -1)
    this.state = {
      ...props,
      fileColumnHeaders: headers,
      mappedColumnObj: {},
      errorCount: {},
      category: "Users/Entities",
      userAddress: "",
      loading: true,
      UpdatedGridData: {},
      inspectData: [],
      validatedData: [],
      inValidData: [],
      leadAdviors: [],
      dbColumnHeaders: [],
    }
  }

  async componentWillMount() {
    const { dbColumnHeaders, nav2, user }= this.props
    this.getMappingObject(this.state.category)
    this.setState({
      loading: true
    })
    const emails = await getAllTenantUserEmails()
    const firmNames = await getAllTenantEntityNames()
    firmNames.push(user.firmName)
    const dbSecondlevelDBLookupData = firmNames

    const dbLookupColumns = {
      "firmEmailId": emails,
      "userEmail1": emails,
      "userEmail2": emails,
      "emailId": emails,
      "firmName": firmNames,
    }

    // Get picklist values
    let pickListKeys = [...new Set(this.props.dbColumnHeaders.filter(columnConfig => columnConfig.pickListKey)
      .map(columnConfig => columnConfig.pickListKey))]

    if(nav2 === "deal-files"){
      pickListKeys.push("LKUPPARTICIPANTTYPE")
      pickListKeys.push("LKUPUNDERWRITERROLE")
      pickListKeys.push("LKUPOTHERSTRAN")
    }
    pickListKeys = await getUniqValueFromArray(pickListKeys)
    const pickListData = pickListKeys.length ? await getPicklistByPicklistName(pickListKeys) : []
    const pickListResultArray = (pickListData.length && pickListData[1]) || {}
    const pickListResultObject = (pickListData.length && pickListData[2]) || {}

    if(nav2 === "deal-files"){
      pickListResultArray.LKUPCOUNTRY =  (pickListData[2] && pickListData[2].LKUPCOUNTRY && pickListData[2].LKUPCOUNTRY["United States"]) || []
      pickListResultArray.LKUPPRIMARYSECTOR = (pickListData.length && pickListData[2] && pickListData[2].LKUPPRIMARYSECTOR) || {}
      pickListResultArray.LKUPDEBTTRAN = [
        ...((pickListData.length && pickListData[1] && pickListData[1].LKUPDEBTTRAN) || []),
        ...((pickListData.length && pickListData[1] && pickListData[1].LKUPOTHERSTRAN) || [])
      ]
      pickListResultArray.LKUPPARTICIPANTTYPE.forEach((val,i) => {
        const isExists = dbColumnHeaders.find(col => col.displayName === `Participants  - ${val}`)
        const isExistsAlias = dbColumnHeaders.find(col => col.displayName === `${val} - Alias`)
        if(!isExists){
          dbColumnHeaders.push({
            fieldName: `dealPartFirmName${i}`,
            displayName: `Participants  - ${val}`,
            staticName: val,
            pickListKey: "LKUPPARTICIPANTTYPE"
          })
        }
        if(!isExistsAlias){
          dbColumnHeaders.push({
            fieldName: `dealPartFirmAlias${i}`,
            relatedTo: `dealPartFirmName${i}`,
            displayName: `${val} - Alias`,
            staticName: val,
          })
        }
      })
      pickListResultArray.LKUPUNDERWRITERROLE.forEach((val,i) => {
        const isExists = dbColumnHeaders.find(col => col.displayName === `Underwriting Team  - ${val}`)
        const isExistsAlias = dbColumnHeaders.find(col => col.displayName === `${val} - Alias`)
        if(!isExists){
          dbColumnHeaders.push({
            fieldName: `underwriters${i}`,
            displayName: `Underwriting Team - ${val}`,
            staticName: val,
            pickListKey: "LKUPUNDERWRITERROLE"
          })
        }
        if(!isExistsAlias){
          dbColumnHeaders.push({
            fieldName: `underwritersAlias${i}`,
            relatedTo: `underwriters${i}`,
            displayName: `${val} - Alias`,
            staticName: val,
          })
        }
      })
      dbColumnHeaders.sort((a, b) => {
        const nameA = (a && a.displayName && a.displayName.toUpperCase()) || ""
        const nameB = (b && b.displayName && b.displayName.toUpperCase()) || ""
        if (nameA < nameB) {
          return -1
        }
        if (nameA > nameB) {
          return 1
        }
        return 0
      })
    }
    this.setState({
      loading: false,
      dbLookupColumns,
      pickListResultArray, // eslint-disable-line
      pickListResultObject, // eslint-disable-line
      dbSecondlevelDBLookupData, // eslint-disable-line
      dbColumnHeaders
    })
  }

  componentDidUpdate(prevProps) {
    const {gridData} = this.props
    if(gridData && typeof gridData === "object" && Object.keys(this.props.gridData).length !== Object.keys(prevProps.gridData || {}).length){
      const headers = Object.keys((this.props.gridData && this.props.gridData[0]) || {}).filter(key => key.toLowerCase().search("empty") === -1)
      this.setState({
        fileColumnHeaders: headers
      })
    }
  }

  filterRowsWithInvalidData = () => {
    let {dbColumnHeaders} = this.props
    const {category, dbLookupColumns, mappedColumnObj} = this.state

    if(category === "Users"){
      dbColumnHeaders = this.props.UserColumnConfig
    }else if(category === "Entities"){
      dbColumnHeaders = this.props.EntityColumnConfig
    }

    const gridData = cloneDeep(this.state.UpdatedGridData)
    let errors = {}
    const validatedData = []
    const inValidData = []
    const unKnownFirm = `Unknown Firm ${Math.random().toString(16).toUpperCase().substring(2, 10)}`

    Object.keys(gridData || {}).forEach((rowKey) => {
      const rowData = gridData[rowKey]
      let isValid = true
      let message = ""
      // console.log(rowData)
      const addressMandatoryFields = [/* "Address Line 1", "Country", "State", "Zip Code" */]
      // const addressValidateFields = ["Company Email Id", "Company Phone", "Company Fax"]
      const addressValidateFields = ["Company Email Id", "Business Primary Phone Number", "Business Phone Number 1", "Business Phone Number 2", "Business Phone Number 3",
        "Business Phone Number 4", "Company Fax", "Zip Code"]
      const phones1 = ["officePhone", "officePhone1", "officePhone2", "officePhone3", "officePhone4", "officePhone5"]
      const phones2 = ["address2officePhone", "address2officePhone1", "address2officePhone2", "address2officePhone3", "address2officePhone4", "address2officePhone5"]
      // let address1Validate = false
      // let address2Validate = false
      // let address3Validate = false
      const address1 = {
        firmAddressLine1: removeWhiteSpaces(rowData.firmAddressLine1),
        firmAddressLine2: removeWhiteSpaces(rowData.firmAddressLine2),
        firmCountry: removeWhiteSpaces(rowData.firmCountry),
        firmState: removeWhiteSpaces(rowData.firmState),
        firmCity: removeWhiteSpaces(rowData.firmCity),
        firmZipCode: removeWhiteSpaces(rowData.firmZipCode),
        firmWebsite: removeWhiteSpaces(rowData.firmWebsite),
        firmEmailId: removeWhiteSpaces(rowData.firmEmailId),
        officePhone: removeWhiteSpaces(rowData.officePhone),
        officeFax: removeWhiteSpaces(rowData.officeFax),
      }
      const address2 = {
        address2AddressLine1: removeWhiteSpaces(rowData.address2AddressLine1),
        address2AddressLine2: removeWhiteSpaces(rowData.address2AddressLine2),
        address2Country: removeWhiteSpaces(rowData.address2Country),
        address2State: removeWhiteSpaces(rowData.address2State),
        address2City: removeWhiteSpaces(rowData.address2City),
        address2ZipCode: removeWhiteSpaces(rowData.address2ZipCode),
        address2Website: removeWhiteSpaces(rowData.address2Website),
        address2EmailId: removeWhiteSpaces(rowData.address2EmailId),
        address2officePhone: removeWhiteSpaces(rowData.address2officePhone),
        address2officeFax: removeWhiteSpaces(rowData.address2officeFax),
      }
      const address3 = {
        userAddressLine1: removeWhiteSpaces(rowData.userAddressLine1),
        userAddressLine2: removeWhiteSpaces(rowData.userAddressLine2),
        userCountry: removeWhiteSpaces(rowData.userCountry),
        userState: removeWhiteSpaces(rowData.userState),
        userCity: removeWhiteSpaces(rowData.userCity),
        userZipCode: removeWhiteSpaces(rowData.userZipCode),
      }
      /* if(!checkEmptyElObject(address1)){
        address1Validate = Object.keys(address1 || {}).some(key => address1[key])
      }
      if(!checkEmptyElObject(address2)){
        address2Validate = Object.keys(address2 || {}).some(key => address2[key])
      }
      if(!checkEmptyElObject(address3)){
        address3Validate = Object.keys(address3 || {}).some(key => address3[key])
      } */
      Object.keys(rowData || {}).forEach((key) => {
        isValid = true
        message = ""
        const columnHeader = key
        const dbColumn = dbColumnHeaders.filter(column => column.fieldName === columnHeader)[0]
        const isAddress1 = [...Object.keys(address1 || {}), ...phones1].indexOf(columnHeader) !== -1
        const isAddress2 = [...Object.keys(address2 || {}), ...phones2].indexOf(columnHeader) !== -1
        const excelKeys = Object.keys(mappedColumnObj || {})
        const excelLabelIndex = Object.keys(mappedColumnObj || {}).findIndex(exKey => dbColumn && mappedColumnObj && mappedColumnObj[exKey] === dbColumn.fieldName)
        const excelLabel = excelKeys[excelLabelIndex]
        if(dbColumn && dbColumn.type === "string" && rowData[key]){
          rowData[key] = rowData[key].toString()
        }

        if (dbColumn && dbColumn.mandatory && !rowData[dbColumn.fieldName] && isValid) {
          rowData[key] = dbColumn.fieldName === "emailId" ?
            `user${Math.random().toString(16).toUpperCase().substring(2, 10)}@tobeupdated.com` : dbColumn.fieldName === "firmName" ?
              unKnownFirm :`Unknown ${dbColumn.displayName}`
          // isValid = false
          // message = `${excelLabel} (${dbColumn.displayName}) is mandatory`
        }
        if (dbColumn && (dbColumn.regexString || dbColumn.regExpFunction) && isValid) {
          const regexString = new RegExp(dbColumn.regexString, "g")
          if(addressValidateFields.indexOf(dbColumn.displayName) !== -1 && rowData[key] && (dbColumn.regexString || dbColumn.regExpFunction)){
            if(dbColumn.regexString && !regexString.test(rowData[key]) && !dbColumn.regExpFunction){
              isValid = false
              message = `${excelLabel} (${isAddress1 ? "Address 1" : isAddress2 ? "Address 2" : "Address"} - ${dbColumn.displayName}) is invalid`
            }else if(dbColumn.regExpFunction){
              const isValidPhoneZip = dbColumn.regExpFunction(removeWhiteSpaces(rowData[key]))
              if(!isValidPhoneZip.valid){
                isValid = false
                message = `${excelLabel} (${isAddress1 ? "Address 1" : isAddress2 ? "Address 2" : "Address"} - ${dbColumn.displayName}) is invalid`
              } else {
                rowData[key] = isValidPhoneZip.data
              }
            }
          }else if (addressValidateFields.indexOf(dbColumn.displayName) === -1 && rowData[key] && (dbColumn.regexString || dbColumn.regExpFunction)) {
            if(dbColumn.regexString && !regexString.test(rowData[key]) && !dbColumn.regExpFunction){
              isValid = false
              message = `${excelLabel} (${dbColumn.displayName}) is invalid`
            }else if(dbColumn.regExpFunction) {
              const isValidPhoneZip = dbColumn.regExpFunction(removeWhiteSpaces(rowData[key]))
              if (!isValidPhoneZip.valid) {
                isValid = false
                message = `${excelLabel} (${dbColumn.displayName}) is invalid`
              } else {
                rowData[key] = isValidPhoneZip.data
              }
            }
          }
        }

        if (dbColumn && dbColumn.duplicateCheck && rowData[dbColumn.fieldName] && isValid) {
          const dataArray = Object.values(gridData).map(data => data[dbColumn.fieldName] ? data[dbColumn.fieldName].toLowerCase() : data[dbColumn.fieldName])
          const emailsArray = []
          const emailFields = ["emailId", "userEmail1", "userEmail2"]
          if(emailFields.indexOf(dbColumn.fieldName) !== -1){
            Object.values(gridData).forEach(data => data.emailId && emailsArray.push(typeof data.emailId === "string" ? data.emailId.toLowerCase() : data.emailId))
            Object.values(gridData).forEach(data => data.userEmail1 && emailsArray.push(typeof data.userEmail1 === "string" ? data.userEmail1.toLowerCase() : data.userEmail1))
            Object.values(gridData).forEach(data => data.userEmail2 && emailsArray.push(typeof data.userEmail2 === "string" ? data.userEmail2.toLowerCase() : data.userEmail2))
          }
          if(emailFields.indexOf(dbColumn.fieldName) !== -1 && emailsArray.filter(email => rowData[key] && email === rowData[key].toLowerCase()).length > 1){
            isValid = false
            message = `${excelLabel} (${dbColumn.displayName}) is duplicate entry`
          }else if (dataArray.filter(data => rowData[key] && data === rowData[key].toLowerCase()).length > 1) {
            isValid = false
            message = `${excelLabel} (${dbColumn.displayName}) is duplicate entry`
          }
        }

        if (dbColumn && dbColumn.checkDB && rowData[dbColumn.fieldName] && isValid) {
          const dbLookupData = dbLookupColumns[dbColumn.fieldName]
          if (dbLookupData && dbLookupData.indexOf(rowData[key]) !== -1) {
            isValid = false
            message = `${excelLabel} (${dbColumn.displayName}) already present in database`
          }
        }

        if(dbColumn && dbColumn.checkExistsInDB && rowData[dbColumn.fieldName] && isValid){
          const dbLookupData = dbLookupColumns[dbColumn.fieldName].map(val => val.toLowerCase())
          console.log("===============dbLookupData===================", dbLookupData)
          if (dbLookupData && dbLookupData.indexOf(rowData[key].toLowerCase()) === -1) {
            isValid = false
            message = `${excelLabel} (${dbColumn.displayName}) does not exists in database`
          }else {
            rowData[key] = dbLookupColumns[dbColumn.fieldName].find(val => val.toLowerCase() === rowData[key].toLowerCase())
          }
        }

        /* if(dbColumn && dbColumn.staticLookupData && dbColumn.staticLookupData.length){
          const dbLookupData = dbColumn.staticLookupData
          if (dbLookupData && dbLookupData.indexOf(rowData[key]) === -1) {
            isValid = false
            message = `${dbColumn.displayName} should be Client, Prospect or Third Party.`
          }
        } */
        /* if((address1Validate && isAddress1) || (address2Validate && !isAddress1)){
          if(dbColumn && addressMandatoryFields.indexOf(dbColumn.displayName) !== -1 && !rowData[dbColumn.fieldName]){
            isValid = false
            message = `${excelLabel} (${isAddress1 ? "Address 1" : "Address 2" } - ${dbColumn.displayName}) field is mandatory`
          }
        } */

        if (dbColumn && dbColumn.length && rowData[dbColumn.fieldName] && isValid && dbColumn.length < rowData[dbColumn.fieldName].length) {
          isValid = false
          message = `${excelLabel} (${dbColumn.displayName}) characters length should be less than 256 characters`
        }

        if(!isValid){
          if(errors.hasOwnProperty(rowKey)){ // eslint-disable-line
            errors[rowKey] = {
              ...errors[rowKey],
              [dbColumn.fieldName]: message
            }
          }else {
            errors = {
              ...errors,
              [rowKey]: {
                [dbColumn.fieldName]: message
              }
            }
          }
        }
      })
    })
    const errorsIndex = Object.keys(errors || {}).map(key => parseInt(key, 10))
    const inspectData = []
    console.log("--------gridData-------------", gridData)
    Object.keys(gridData || {}).forEach(keyIndex => {
      if(errorsIndex.indexOf(parseInt(keyIndex, 10)) === -1){
        validatedData.push(gridData[keyIndex])
        inspectData.push(gridData[keyIndex]. _otherData) // eslint-disable-line
      }else {
        const error = errors[keyIndex]
        const errorString = []
        Object.keys(error || {}).forEach((key, i) => {
          errorString.push(` ${i+1 }. ${ error[key]}`)
        })

        let modifyField = gridData[keyIndex]
        modifyField = {
          ...modifyField,
          _otherData: {
            ...modifyField._otherData, // eslint-disable-line
            Errors: errorString.toString()
          }
        }
        inValidData.push(modifyField)
        inspectData.push(modifyField. _otherData) // eslint-disable-line
      }
    })

    const errKeys = []
    const allErr = []
    const errorCount = {}
    Object.keys(errors || {}).forEach(key => {
      allErr.push(errors[key] || {})
      Object.keys(errors[key] || {}).forEach(subKey => {
        if(errKeys.indexOf(subKey) === -1){
          errKeys.push(subKey)
        }
      })
    })
    allErr.forEach(errObj => {
      errKeys.forEach(subKey => {
        if(errObj[subKey]){
          const keyErr = allErr.filter(err => err[subKey] === errObj[subKey])
          const dbColumn = dbColumnHeaders.find(column => column.fieldName === subKey)
          if(dbColumn && !errorCount.hasOwnProperty(dbColumn.displayName)){ // eslint-disable-line
            errorCount[dbColumn.displayName] = keyErr.length
          }
        }
      })
    })

    console.log("*****errors****", errorCount)
    console.log("*****validatedData****", validatedData)
    console.log("*****inspectData****", inspectData)
    console.log("--------gridData", gridData)

    this.setState({
      inspectData,
      validatedData,
      inValidData,
      errorCount,
      loading: false
    },() => {
      this.putMappingObject(this.state.category)
      if(errors && Object.keys(errors || {}).length){
        this.showErrorModal(inspectData)
        if(this.props.file){
          const blob = this.excelDownloadHandler(inspectData, "Records", true)
          const file = new File([blob], this.props.file.name || "Records")
          this.uploadFile(file)
        }
      }else {
        this.props.setTypeOfFileUpload({validatedData, selectedStepIndex: this.state.stepIndex + 1})
        // this.props.setStepIndex(this.state.stepIndex + 1)
      }
    })

  }

  filterDealsRowsWithInvalidData = () => {
    const {dbColumnHeaders, pickListResultArray, mappedColumnObj} = this.state
    const gridData = cloneDeep(this.state.UpdatedGridData)
    let errors = {}
    const validatedData = []
    const inValidData = []

    Object.keys(gridData || {}).forEach((rowKey) => {
      const rowData = gridData[rowKey]
      let isValid = true
      let message = ""

      Object.keys(rowData || {}).forEach((key) => {
        isValid = true
        message = ""
        const columnHeader = key
        const dbColumn = dbColumnHeaders.filter(column => column.fieldName === columnHeader)[0]
        const excelKeys = Object.keys(mappedColumnObj || {})
        const excelLabelIndex = Object.keys(mappedColumnObj || {}).findIndex(exKey => dbColumn && mappedColumnObj && mappedColumnObj[exKey] === dbColumn.fieldName)
        const excelLabel = excelKeys[excelLabelIndex]

        if(dbColumn && dbColumn.type === "string" && rowData[key]){
          rowData[key] = rowData[key].toString()
        }

        if (dbColumn && dbColumn.mandatory && !rowData[dbColumn.fieldName] && isValid) {
          isValid = false
          message = `${excelLabel} (${dbColumn.displayName}) is mandatory`
        }

        if (dbColumn && dbColumn.regexString && rowData[key] && isValid) {
          const regexString = new RegExp(dbColumn.regexString, "g")
          if (!regexString.test(rowData[key])) {
            isValid = false
            message = `${excelLabel} (${dbColumn.displayName}) is invalid`
          }
        }

        const nCheckPicklist = ["LKUPCOUNTRY", "LKUPPARTICIPANTTYPE", "LKUPUNDERWRITERROLE"]
        if(dbColumn && dbColumn.pickListKey && rowData[key] && nCheckPicklist.indexOf(dbColumn.pickListKey) === -1){
          let picklist = []
          if(pickListResultArray.LKUPPRIMARYSECTOR && dbColumn.pickListKey === "SECONDARYSECTOR" && rowData && rowData.dealIssueTranPrimarySector){

            picklist = pickListResultArray.LKUPPRIMARYSECTOR[rowData.dealIssueTranPrimarySector] ? pickListResultArray.LKUPPRIMARYSECTOR[rowData.dealIssueTranPrimarySector].map(val => val.toLowerCase()) : []

          }else  if(pickListResultArray.LKUPPRIMARYSECTOR && dbColumn.pickListKey === "LKUPPRIMARYSECTOR"){

            picklist = Object.keys(pickListResultArray[dbColumn.pickListKey] || {}).map(val => val.toLowerCase())

          }else {
            picklist = pickListResultArray[dbColumn.pickListKey].map(val => val.toLowerCase())
          }
          // const picklist = pickListResultArray[dbColumn.pickListKey].map(val => val.toLowerCase())
          const findIndex = picklist.indexOf(rowData[key].toLowerCase())
          if(findIndex === -1){
            isValid = false
            message = `${excelLabel} (${dbColumn.displayName}) does not exist in the valid set of configured picklists`
          }
        }

        if (dbColumn && dbColumn.length && rowData[key] && isValid && dbColumn.length < rowData[key].length) {
          isValid = false
          message = `${excelLabel} (${dbColumn.displayName}) characters length should be less than 256 characters`
        }

        if(!isValid){
          if(errors.hasOwnProperty(rowKey)){ // eslint-disable-line
            errors[rowKey] = {
              ...errors[rowKey],
              [dbColumn.fieldName]: message
            }
          }else {
            errors = {
              ...errors,
              [rowKey]: {
                [dbColumn.fieldName]: message
              }
            }
          }
        }
      })
    })
    const errorsIndex = Object.keys(errors || {}).map(key => parseInt(key, 10))
    const inspectData = []

    Object.keys(gridData || {}).forEach(keyIndex => {
      if(errorsIndex.indexOf(parseInt(keyIndex, 10)) === -1){
        validatedData.push(gridData[keyIndex])
        inspectData.push(gridData[keyIndex]. _otherData) // eslint-disable-line
      }else {
        const error = errors[keyIndex]
        const errorString = []
        Object.keys(error || {}).forEach((key, i) => {
          errorString.push(` ${i+1 }. ${ error[key]}`)
        })

        let modifyField = gridData[keyIndex]
        modifyField = {
          ...modifyField,
          _otherData: {
            ...modifyField._otherData, // eslint-disable-line
            Errors: errorString.toString()
          }
        }
        inValidData.push(modifyField)
        inspectData.push(modifyField. _otherData) // eslint-disable-line
      }
    })

    const errKeys = []
    const allErr = []
    const errorCount = {}
    Object.keys(errors || {}).forEach(key => {
      allErr.push(errors[key])
      Object.keys(errors[key] || {}).forEach(subKey => {
        if(errKeys.indexOf(subKey) === -1){
          errKeys.push(subKey)
        }
      })
    })
    allErr.forEach(errObj => {
      errKeys.forEach(subKey => {
        if(errObj[subKey]){
          const keyErr = allErr.filter(err => err[subKey] === errObj[subKey])
          const dbColumn = dbColumnHeaders.find(column => column.fieldName === subKey)
          if(dbColumn && !errorCount.hasOwnProperty(dbColumn.displayName)){ // eslint-disable-line
            errorCount[dbColumn.displayName] = keyErr.length
          }
        }
      })
    })

    console.log("*****errors****", errorCount)
    console.log("*****inspectData****", inspectData)

    this.setState({
      inspectData,
      validatedData,
      inValidData,
      errorCount,
      loading: false
    },() => {
      this.putMappingObject("Deals")
      if(errors && Object.keys(errors || {}).length){
        this.showErrorModal(inspectData)
        if(this.props.file){
          const blob = this.excelDownloadHandler(inspectData, "Records with Issues", true)
          const file = new File([blob], this.props.file.name || "Records with Issues")
          this.uploadFile(file)
        }
      }else {
        this.props.setTypeOfFileUpload({validatedData})
        // this.props.setStepIndex(this.state.stepIndex + 1)
      }
    })

  }

  getMappingObject = async (type) => {
    let {svMappingObj} = this.state
    const {nav2} = this.props
    const leadAdviors = await fetchLeadAdvisor(this.props.user.entityId)
    if(!svMappingObj) {
      svMappingObj = await fetchDMObject(type)
      let mappedColumnObj = {}
      let columnObj = {}
      if(nav2 === "deal-files"){
        columnObj = cloneDeep((svMappingObj && svMappingObj.Deals) || {})
      }else {
        columnObj = cloneDeep((svMappingObj && svMappingObj[type]) || {})
      }
      Object.keys(columnObj || {}).forEach(key => {
        if(this.state.fileColumnHeaders.indexOf(key) !== -1){
          mappedColumnObj = {
            ...mappedColumnObj,
            [key]: columnObj[key]
          }
        }
      })
      this.setState({
        svMappingObj,
        mappedColumnObj,
        leadAdviors
      })
    }else {
      this.setState({
        mappedColumnObj: cloneDeep((svMappingObj && svMappingObj[type]) || {}),
        leadAdviors
      })
    }
  }

  putMappingObject = async (type) => {
    if(type){
      const {svMappingObj, mappedColumnObj} = this.state
      const newMappedColumnObj = await putDMMappingObject(type, svMappingObj, mappedColumnObj)
      this.setState({
        svMappingObj: newMappedColumnObj,
        mappedColumnObj: (newMappedColumnObj && newMappedColumnObj[type]) || {}
      })
    }
  }

  excelDownloadHandler = async (data, name, fileUpload) => {
    console.log("*******dataArray**********", data)
    if(data && data.length){
      const headers = Object.keys(data[0])
      const payload = {
        dataArray: [{
          name,
          data,
          headers,
        }]
      }
      if(fileUpload) {
        const blob = await postJSONData(payload, name, true)
        return blob
      }
      await postJSONData(payload, name)
    }else {
      console.log("Pleas pass the data list")
    }
  }

  showErrorModal = () => {
    const {inspectData, validatedData, inValidData} = this.state
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className='custom-ui'>
          <div style={{ margin: "20px 0px", fontWeight: "500" }} className='body'>
            <div className="column">
              Your file has errors, Please review the column named <b><em >errors</em></b> and repeat the process again.
            </div>
            <div className="column" style={{ display: "inline-block", fontWeight: 500, textAlign: "center" }}>
              <a className="action-button excel-upload has-text-link" onClick={() => this.excelDownloadHandler(inspectData, "Records with Issues")}>
                <i className="fa fa-download" title="Download Records" style={{fontSize: 70}}/>
              </a>
              <div className="btn-label">Total {(inspectData && inspectData.length) || 0} Records</div>
              <div className="btn-label">{(validatedData && validatedData.length) || 0} Correct And {(inValidData && inValidData.length) || 0} Records with Issues</div>
            </div>
          </div>
          <button onClick={() => {
            this.setState({
              loading: false
            })
            onClose()
            this.props.setStepIndex(this.state.stepIndex - 1)
          }}>Ok</button>
        </div>
      )
    })
  }

  /**
   * Update column mapping
   */
  updateColumnMappingHandler = (key, value) => {
    let { mappedColumnObj } = this.state

    if(this.props.nav2 === "deal-files"){
      const neObj = {}
      Object.keys(mappedColumnObj || {}).forEach(key => {
        neObj[mappedColumnObj[key]] = key
      })
      delete neObj[value]

      mappedColumnObj = {}
      Object.keys(neObj || {}).forEach(newKey => {
        mappedColumnObj[neObj[newKey]] = newKey
      })
      mappedColumnObj[key] = value
    }else {
      const oldKey = Object.keys(mappedColumnObj || {}).find(oldKey => mappedColumnObj[oldKey] === key)
      delete mappedColumnObj[oldKey]
      mappedColumnObj[value] = key
    }
    this.setState({
      mappedColumnObj
    })
  }

  uploadFile = async (file) => {
    const { bucketName, contextId, contextType, tenantId } = this.props
    const tags = {
      status : "fail"
    }
    let fileName = file.name
    const extnIdx = fileName.lastIndexOf(".")
    if (extnIdx > -1) {
      fileName = `${fileName.substr(
        0,
        extnIdx
      )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
    }
    const contentType = file.type
    if (!fileName) {
      return console.log("No file name provided")
    }
    let filePath
    if (tenantId && contextId && contextType) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const opType = "upload"
    const options = { contentType, tags }

    const res = await getSignedUrl({  // eslint-disable-line
      opType,
      bucketName,
      fileName: filePath,
      options
    })
    if (res) {
      console.log("res : ", res)
      this.uploadWithSignedUrl(bucketName, res.data.url, file, fileName, tags)
    }else {
      console.log("err : ", res)
    }
  }

  uploadWithSignedUrl(bucketName, signedS3Url, file, fileName, tags) {
    console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      bucketName,
      fileName,
      file.type
    )
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("no tagging")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = () => {
      console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        console.log("file uploaded ok")
        console.log(`Last upload successful for "${fileName}" at ${new Date()}`)
        this.updateDocsDB(bucketName, file, fileName)
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded", xhr.status, xhr.responseText, err)
    }
    xhr.send(file)
  }

  updateDocsDB = async (bucketName, file, fileName) => {
    const {validatedData, inValidData, errorCount} = this.state
    const {
      contextId,
      contextType,
      folderId,
      tenantId,
      user
    } = this.props
    const doc = {
      name: fileName,
      originalName: file.name,
      contextType,
      folderId,
      contextId,
      tenantId
    }
    const res = await addDocInDB(doc)
    if(res){
      console.log("inserted ok in docs db : ", res)
      const audits = [
        { userName: `${user.userFirstName} ${user.userLastName}`, log: `Migration document ${file.name} upload for ${this.state.category || ""}`, type: this.props.nav2, errorCount,
          uploadedData: `Found ${(validatedData && validatedData.length) || 0} Correct And ${(inValidData && inValidData.length) || 0} Records with Issues`, fileId: res.data._id, status: "Fail", date: new Date(), key: "migration" }
      ]
      await updateAuditLog("Data Migration", audits)
    }else {
      console.log("err in inserting in docs db : ", res)
    }
  }

  checkColumnMapping = (columnData, index) => {
    const options = this.state.fileColumnHeaders
    const { mappedColumnObj, dbColumnHeaders } = this.state
    const key = Object.keys(mappedColumnObj || {}).find(key => mappedColumnObj[key] === columnData.fieldName)
    const mappedKeys = Object.keys(mappedColumnObj || {}).filter(val => val !== columnData.displayName)
    const filterDbColumnHeaders = options.filter(name => mappedKeys.indexOf(name) === -1)

    options.sort((a, b) => {
      const nameA = (a && a.toUpperCase()) || ""
      const nameB = (b && b.toUpperCase()) || ""
      if (nameA < nameB) {
        return -1
      }
      if (nameA > nameB) {
        return 1
      }
      return 0
    })
    const defaultValue = key || ""
    const columnArray = (
      <div className="mapping-input" key={index.toString()}>
        <div>
          <div className="multiExpLblBlk">
            {columnData.displayName} :
            {
              columnData.mandatory ?
                <span>
                &nbsp;&nbsp;
                  <a data-tip data-for="tooltipInfo">
                    <i className="fa fa-asterisk" style={{ color: "#3982CC" }} />
                  </a>
                  <ReactTooltip id="tooltipInfo" place="right" type="info" effect="solid">
                    <span>This is mandatory field, please select an option for it.</span>
                  </ReactTooltip>
                </span>
                : null
            }
            &nbsp;&nbsp;
            <span>
              {
                Object.values(this.state.mappedColumnObj).findIndex(value => value === columnData.fieldName) !== -1 ?
                  <i className="fa fa-times-circle" style={{ cursor: "pointer", color: "red" }}
                    onClick={
                      () => {
                        const { mappedColumnObj } = this.state
                        const key = Object.keys(mappedColumnObj || {}).find(key => mappedColumnObj[key] === columnData.fieldName)
                        delete mappedColumnObj[key]
                        this.setState({
                          mappedColumnObj
                        })
                      }
                    } />
                  : null
              }
            </span>
          </div>
          <div className="is-small  is-fullwidth is-link">
            <DropdownList
              filter='contains'
              data={filterDbColumnHeaders}
              value={defaultValue}
              placeholder='Select a value'
              onChange={(value) => this.updateColumnMappingHandler(columnData.fieldName, value)}
            />
          </div>
        </div>
      </div>
    )
    return columnArray
  }

  checkTranColumnMapping = (field, index) => {
    const { mappedColumnObj, dbColumnHeaders,  } = this.state
    const mappedValues = Object.values(mappedColumnObj).filter(val => val !== mappedColumnObj[field])
    const filterDbColumnHeaders = dbColumnHeaders.filter(col => mappedValues.indexOf(col.fieldName) === -1)
    const defaultValue = mappedColumnObj[field] || ""
    return (
      <div className="mapping-input" key={index.toString()}>
        <div>
          <div className="multiExpLblBlk">
            {field} :
            &nbsp;&nbsp;
            <span>
              {
                Object.values(mappedColumnObj).findIndex(value => value === mappedColumnObj[field]) !== -1 ?
                  <i className="fa fa-times-circle" style={{cursor: "pointer", color: "red"}}
                    onClick={
                      () => {
                        const key = Object.keys(mappedColumnObj || {}).find(key => mappedColumnObj[key] === mappedColumnObj[field])
                        delete mappedColumnObj[key]
                        this.setState({
                          mappedColumnObj
                        })
                      }
                    }/>
                  : null
              }
            </span>
          </div>
          <div className="is-small  is-fullwidth is-link">
            <DropdownList
              filter='contains'
              data={filterDbColumnHeaders}
              textField="displayName"
              valueField="fieldName"
              value={defaultValue}
              placeholder='Select a value'
              onChange={(value) => this.updateColumnMappingHandler(field, value.fieldName)}
            />
          </div>
        </div>
      </div>
    )
  }

  cancelStepHandler = () => {
    this.props.setTypeOfFileUpload({
      gridData: null,
      interimStorageKey: null,
      inspectData: null,
      validatedData: null,
      uploadedFileType: null,
      userAddress: null,
      mappedColumnObj: null,
      dbColumnHeaders: null,
      nonExistsPickListValue: null,
      nonExistsLeadAdv: null,
      dbLookupColumns: null,
      leadAdviors: null,
      file: null,
    })
    this.props.history.goBack()
  }

  validateMappedColumns = () => {
    let {dbColumnHeaders} = this.props
    const {category} = this.state

    if(category === "Users"){
      dbColumnHeaders = this.props.UserColumnConfig
    }else if(category === "Entities"){
      dbColumnHeaders = this.props.EntityColumnConfig
    }

    const mandatoryColumns = dbColumnHeaders.filter(column => column.mandatory).map(data => data.fieldName)
    const mappedColumnObj = Object.values(this.state.mappedColumnObj)
    const address1 = [
      "firmAddressLine1",
      "firmCountry",
      "firmState",
      "firmZipCode",
    ]
    const address2 = [
      "address2AddressLine1",
      "address2Country",
      "address2State",
      "address2ZipCode",
    ]

    if (mandatoryColumns.every(val => mappedColumnObj.includes(val)) &&
      (address1.filter(e => mappedColumnObj.indexOf(e) !== -1).length === 4 || address1.filter(e => mappedColumnObj.indexOf(e) !== -1).length === 0) &&
      (address2.filter(e => mappedColumnObj.indexOf(e) !== -1).length === 4 || address2.filter(e => mappedColumnObj.indexOf(e) !== -1).length === 0)) {
      return true
    }
    const mandatoryColumnsNames = dbColumnHeaders.filter(column => column.mandatory).map(data => data.displayName)
    this.showWarningModal(
      <div>
        <div>Please check and map values for below mandatory columns,</div>
        <ul style={{ textAlign: "left", marginTop: "10px" }}>
          {
            mandatoryColumnsNames.map((columnName, i) => <li key={i}>- {columnName}</li>)
          }
        </ul>
        {
          address1.filter(e => mappedColumnObj.indexOf(e) !== -1).length > 0 ?
            <div>
              <div>Address 1</div>
              <ul style={{ textAlign: "left", marginTop: "10px" }}>
                {
                  address1.map(val => {
                    const col = this.props.dbColumnHeaders.find(column => column.fieldName === val)
                    return <li>- {col.displayName}</li>
                  })
                }
              </ul>
            </div>
            : null
        }
        {
          address2.filter(e => mappedColumnObj.indexOf(e) !== -1).length > 0 ?
            <div>
              <div>Address 2</div>
              <ul style={{ textAlign: "left", marginTop: "10px" }}>
                {
                  address2.map(val => {
                    const col = this.props.dbColumnHeaders.find(column => column.fieldName === val)
                    return <li key={val}>- {col.displayName}</li>
                  })
                }
              </ul>
            </div>
            : null
        }
      </div>
    )
    return false
  }

  validateDealsMappedColumns = () => {
    const {dbColumnHeaders} = this.state
    const mappedColumnObj = Object.values(this.state.mappedColumnObj)
    const mandatoryColumns = dbColumnHeaders.filter(column => column.mandatory).map(data => data.fieldName)
    if (mandatoryColumns.every(val => mappedColumnObj.includes(val))) return true
    const mandatoryColumnsNames = dbColumnHeaders.filter(column => column.mandatory).map(data => data.displayName)

    this.showWarningModal(
      <div>
        <div>Please check and map values for below mandatory columns,</div>
        <ul style={{ textAlign: "left", marginTop: "10px" }}>
          {
            mandatoryColumnsNames.map(columnName => <li key={columnName}>- {columnName}</li>)
          }
        </ul>
      </div>
    )
    return false
  }

  nextStepHandler = () => {
    this.setState({
      loading: true
    },() => {
      window.setTimeout(() => this.stepHandle(), 1000)
    })
  }

  stepHandle = () => {
    const validate = this.props.nav2 === "deal-files" ? this.validateDealsMappedColumns(): this.validateMappedColumns()
    if (validate) {
      const { mappedColumnObj, userAddress, category, dbColumnHeaders, pickListResultArray, dbLookupColumns, leadAdviors, validatedData } = this.state
      const gridData = this.props.gridData || {}
      const nonExistsPickListValue = {}
      let nonExistsLeadAdv = {}
      // Update grid data according to mapped db column headers
      const UpdatedGridData = {}
      Object.keys(gridData || {}).forEach((k, i) => {
        UpdatedGridData[i] = {}
        Object.keys(gridData[i] || {}).forEach(key => {
          if (mappedColumnObj[key]) {
            let leadAdv = ""
            const dbColumn = dbColumnHeaders.find(column => column.fieldName === mappedColumnObj[key])
            if(this.props.nav2 === "deal-files" && gridData[i][key]){
              const nCheckPicklist = ["LKUPCOUNTRY", "LKUPPARTICIPANTTYPE", "LKUPUNDERWRITERROLE", "LKUPPRIMARYSECTOR", "SECONDARYSECTOR"]
              if(dbColumn && dbColumn.pickListKey && nCheckPicklist.indexOf(dbColumn.pickListKey) === -1){
                const picklist = pickListResultArray[dbColumn.pickListKey].map(val => val.toLowerCase())
                const findIndex = picklist.indexOf(gridData[i][key].toLowerCase())
                if(findIndex === -1){
                  // gridData[i][key] = ""
                  /* if(nonExistsPickListValue.hasOwnProperty(dbColumn.pickListKey)){ // eslint-disable-line
                    nonExistsPickListValue[dbColumn.pickListKey].push(gridData[i][key])
                  }else {
                    nonExistsPickListValue = {
                      ...nonExistsPickListValue,
                      [dbColumn.pickListKey]: [gridData[i][key]]
                    }
                  } */
                }else {
                  gridData[i][key] =  pickListResultArray[dbColumn.pickListKey][findIndex]
                }
              }
              if(dbColumn && dbColumn.fieldName === "dealIssueTranAssignedTo"){
                const leadEmail = gridData[i][key]
                leadAdviors.forEach(lead => {
                  const primaryEmail = lead.userEmails.find(email => email.emailId.toLowerCase() === leadEmail.toLowerCase())
                  if(primaryEmail){
                    leadAdv = lead
                  }
                })
                if(!leadAdv && !nonExistsLeadAdv.hasOwnProperty(gridData[i][key])){ // eslint-disable-line
                  nonExistsLeadAdv = {
                    ...nonExistsLeadAdv,
                    [gridData[i][key]]: {}
                  }
                }
              }
            }
            if(leadAdv){
              UpdatedGridData[i][mappedColumnObj[key]] = leadAdv.name
              UpdatedGridData[i].dealIssueTranAssigneeId = leadAdv.id
            }else {
              UpdatedGridData[i][mappedColumnObj[key]] = gridData[i][key]
            }
          }
          (UpdatedGridData[i]._otherData || (UpdatedGridData[i]._otherData = {}))[key] = gridData[i][key] // eslint-disable-line
        })
      })
      console.log("========UpdatedGridData===========", UpdatedGridData)
      this.setState({
        UpdatedGridData,
        // loading: true
      },async () => {
        if (this.props.nav2 === "deal-files") {
          await this.filterDealsRowsWithInvalidData(UpdatedGridData)
        } else {
          await this.filterRowsWithInvalidData(UpdatedGridData)
        }
        if(this.state.inValidData && this.state.inValidData.length === 0){
          this.props.setTypeOfFileUpload({
            inspectData: UpdatedGridData,
            uploadedFileType: this.props.nav2 === "deal-files" ? "Deals" : category,
            userAddress: userAddress || {},
            dbColumnHeaders,
            mappedColumnObj,
            nonExistsPickListValue: nonExistsPickListValue || {},
            nonExistsLeadAdv: nonExistsLeadAdv || {},
            dbLookupColumns,
            leadAdviors,
            selectedStepIndex: this.state.stepIndex + 1
          })
        }
      })
      // this.props.setStepIndex(this.state.stepIndex + 1)
    }
  }

  showWarningModal = (message) => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className='custom-ui'>
          <div style={{ margin: "20px 0px", fontWeight: "500" }} className='body'>
            {message || "Something went wrong, please contact Otaras support team."}
          </div>
          <button onClick={() => {
            this.setState({ loading: false })
            onClose()
          }}>Ok</button>
        </div>
      )
    })
  }

  onChange = (event) => {
    let state = {}
    if(event.target.name === "category"){
      state = {
        [event.target.name]: event.target.value,
        userAddress: "",
      }
      this.getMappingObject(event.target.value)
    }else {
      state = {
        [event.target.name]: event.target.value
      }
    }

    this.setState({
      ...state
    })
  }

  saveMapping = async () => {
    const {nav2} = this.props
    await this.putMappingObject(nav2 === "user-data" ? this.state.category : nav2 === "deal-files" ? "Deals" : "")
    toast("Mapping saved successfully.", {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.SUCCESS
    })
  }

  render() {
    const {category, userAddress, loading, fileColumnHeaders} = this.state
    const {nav2,dbColumnHeaders} = this.props
    let {UserColumnConfig} = this.props
    if(UserColumnConfig && UserColumnConfig.length && category !== "Users"){
      UserColumnConfig = UserColumnConfig.filter(val => val.fieldName !== "firmName")
    }

    return (
      <div className="stepContainer">
        { loading ? <Loader /> : null }
        <div className="stepBody" style={{marginTop: 0}}>
          {
            nav2 === "user-data" ?
              <div>
                <div className="columns">
                  <div className="column has-text-weight-bold">
                    &nbsp;&nbsp;which type of file upload?
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="control">
                      <label className="radio-button">Entities/Contacts Combined
                        <input type="radio" name="category" value="Users/Entities" checked={category === "Users/Entities" || false} onClick={this.onChange} onChange={this.onChange}/>
                        <span className="checkmark" />
                      </label>
                    </div>
                  </div>
                  <div className="column">
                    <div className="control">
                      <label className="radio-button">Entities
                        <input type="radio" name="category" value="Entities" checked={category === "Entities" || false} onClick={this.onChange} onChange={this.onChange}/>
                        <span className="checkmark" />
                      </label>
                    </div>
                  </div>
                  <div className="column">
                    <div className="control">
                      <label className="radio-button">Users
                        <input type="radio" name="category" value="Users" checked={category === "Users" || false} onClick={this.onChange} onChange={this.onChange}/>
                        <span className="checkmark" />
                      </label>
                    </div>
                  </div>
                </div>
              </div> : null
          }
          <div className="infoDiv">
            <i className="fa fa-info-circle" style={{ margin: "10px" }} />
            Please verify and select appropriate column names to be mapped with existing fields in the database.
          </div>
          <br/>
          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                {
                  nav2 === "user-data" && category === "Users/Entities" ?
                    <div>
                      <RatingSection
                        onAccordion={() => onAccordion(0)}
                        title="Entities"
                      >
                        {activeAccordions.includes(0) && (
                          <div>
                            <div className="columns">
                              <div className="column">
                                {Object.values(this.props.EntityColumnConfig).filter(column => !column.subTitle).map((column, index) =>
                                  this.checkColumnMapping(column, index))
                                }
                              </div>
                            </div>
                            {
                              ["Address 1", "Address 2"].map(val => (
                                <div key={val}>
                                  <p className="stepLabel">
                                    <b>{val}</b>
                                  </p>
                                  {Object.values(this.props.EntityColumnConfig).filter(column => column.subTitle === val).map((column, index) =>
                                    this.checkColumnMapping(column, index))
                                  }
                                </div>
                              ))
                            }
                          </div>
                        )}
                      </RatingSection>
                      <RatingSection
                        onAccordion={() => onAccordion(1)}
                        title="Users"
                      >
                        {activeAccordions.includes(1) && (
                          <div>
                            {Object.values(UserColumnConfig).filter(column => !column.subTitle).map((column, index) => this.checkColumnMapping(column, index))}
                            <div className="columns">
                              <div className="column">
                                Which address you want to attach as a users primary address?
                              </div>
                              <div className="column">
                                <label className="checkbox-button">
                                  Address 1
                                  <input
                                    type="checkbox"
                                    name="userAddress"
                                    value="address1"
                                    checked={userAddress === "address1" || false}
                                    onClick={this.onChange}
                                    onChange={this.onChange}
                                  />
                                  <span className="checkmark" />
                                </label>
                              </div>
                              <div className="column">
                                <label className="checkbox-button">
                                  Address 2
                                  <input
                                    type="checkbox"
                                    name="userAddress"
                                    value="address2"
                                    checked={userAddress === "address2" || false}
                                    onClick={this.onChange}
                                    onChange={this.onChange}
                                  />
                                  <span className="checkmark" />
                                </label>
                              </div>
                            </div>
                            {
                              ["Address"].map(val => (
                                <div key={val}>
                                  <p className="stepLabel">
                                    <b>{val}</b>
                                  </p>
                                  {Object.values(this.props.dbColumnHeaders).filter(column => column.subTitle === val).map((column, index) =>
                                    this.checkColumnMapping(column, index))
                                  }
                                </div>
                              ))
                            }
                          </div>
                        )}
                      </RatingSection>
                    </div>
                    : null
                }
                {
                  nav2 === "user-data" && category === "Users" ?
                    <RatingSection
                      onAccordion={() => onAccordion(0)}
                      title="Users"
                    >
                      {activeAccordions.includes(0) && (
                        <div>
                          <div className="columns">
                            <div className="column">
                              {Object.values(this.props.UserColumnConfig).filter(column => !column.subTitle).map((column, index) => this.checkColumnMapping(column, index))}
                            </div>
                          </div>
                          {
                            ["Address"].map(val => (
                              <div key={val}>
                                <p className="stepLabel">
                                  <b>{val}</b>
                                </p>
                                {Object.values(this.props.UserColumnConfig).filter(column => column.subTitle === val).map((column, index) =>
                                  this.checkColumnMapping(column, index))
                                }
                              </div>
                            ))
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null }

                {
                  nav2 === "user-data" && category === "Entities" ?
                    <RatingSection
                      onAccordion={() => onAccordion(0)}
                      title="Entities"
                    >
                      {activeAccordions.includes(0) && (
                        <div>
                          <div className="columns">
                            <div className="column">
                              {Object.values(this.props.EntityColumnConfig).filter(column => !column.subTitle).map((column, index) =>
                                this.checkColumnMapping(column, index))
                              }
                            </div>
                          </div>
                          {
                            ["Address 1", "Address 2"].map(val => (
                              <div key={val}>
                                <p className="stepLabel">
                                  <b>{val}</b>
                                </p>
                                {Object.values(this.props.EntityColumnConfig).filter(column => column.subTitle === val).map((column, index) =>
                                  this.checkColumnMapping(column, index))
                                }
                              </div>
                            ))
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null}

                {
                  nav2 === "deal-files" ?
                    <RatingSection
                      onAccordion={() => onAccordion(0)}
                      title="Transactions"
                    >
                      {activeAccordions.includes(0) && (
                        <div>
                          <div className="columns">
                            <div className="column">
                              {Object.values(fileColumnHeaders).map((column, index) =>
                                this.checkTranColumnMapping(column, index))
                              }
                            </div>
                          </div>
                        </div>
                      )}
                    </RatingSection>
                    : null}
              </div>
            )} />
        </div>
        <div className='stepFooter'>
          <button className="button is-pulled-left" onClick={this.cancelStepHandler}>Cancel</button>
          <div className="is-full">
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-info is-pulled-right" onClick={this.saveMapping}>Save Mapping</button>
              </div>
              <div className="control">
                <button className="button is-info is-pulled-right" onClick={this.nextStepHandler} disabled={loading}>Next</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  doesFileHasHeaders: state.admMigrationToolsReducer.doesFileHasHeaders,
  gridData: state.admMigrationToolsReducer.gridData,
  file: state.admMigrationToolsReducer.file,
  interimStorageKey: state.admMigrationToolsReducer.interimStorageKey,
  user: (state.auth && state.auth.userEntities) || {}
})

const mapDispatchToProps = {
  updateGridColumnHeaders: addGridData,
  setStepIndex,
  setTypeOfFileUpload
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MapColumns))
