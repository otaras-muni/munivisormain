import React from "react"
import { connect } from "react-redux"
import { withRouter, Link } from "react-router-dom"
import Loader from "../../../../GlobalComponents/Loader"
import {setStepIndex, setTypeOfFileUpload} from "../../../../StateManagement/actions/AdminManagement/admMigrationTools"
import "react-table/react-table.css"

class Summary extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  componentWillMount () {
    window.setTimeout(() => this.setState({
      loading: false
    }), 1000)
  }
  /**
   * Cancel button click handler
   */
  okayStepHandler = () => {
    this.props.setTypeOfFileUpload({
      uploadResult: null,
      gridData: null,
      interimStorageKey: null,
      inspectData: null,
      validatedData: null,
      uploadedFileType: null,
      userAddress: null,
      mappedColumnObj: null,
      dbColumnHeaders: null,
      nonExistsPickListValue: null,
      nonExistsLeadAdv: null,
      dbLookupColumns: null,
      leadAdviors: null,
      file: null,
    })
    this.props.setStepIndex(0)
  }

  /**
   * Component render method
   */
  render() {
    const {uploadResult, uploadedFileType} = this.props
    const loading = () => <Loader />
    if (this.state.loading) {
      return loading()
    }
    return (
      <div className='stepContainer'>
        <div className="inputFileContainer stepBody">
          <label className="has-text-primary  has-text-weight-semibold is-size-3">
            {uploadedFileType || ""} Data Successfully Uploaded
          </label>
          <div className="returnFileName" style={{marginTop: 20}}>
            {
              uploadedFileType === "Users/Entities" ?
                `${uploadResult && uploadResult.newInsertedEntities || 0} Entities and ${uploadResult && uploadResult.newInsertedUsers || 0} users Uploaded` :
                uploadedFileType === "Entities" ?
                  `${uploadResult && uploadResult.newInsertedEntities || 0} Entities Uploaded` :
                  uploadedFileType === "Users" ?
                    `${uploadResult && uploadResult.newInsertedUsers || 0 } Users Uploaded` :
                    uploadedFileType === "Deals" ?
                      `${uploadResult && `${uploadResult.transactionsCount || 0} Deals, ${uploadResult.transactionsDuplicateCount || 0} Dupliacte Deals, ${uploadResult.entitiesCount || 0} Entities, ${uploadResult.thirdPartiesCount || 0} Third parties uploaded` } And ${uploadResult.leadManagers || 0} Lead Managers` : ""
            }
          </div>
          <Link to="/admin-migtools">Check log status</Link>
        </div>
        <div className='stepFooter'>
          <button className="button is-info is-pulled-right" onClick={this.okayStepHandler}>Back to upload</button>
        </div>
      </div >
    )
  }
}

const mapStateToProps = state => ({
  uploadResult: state.admMigrationToolsReducer.uploadResult,
  uploadedFileType: state.admMigrationToolsReducer.uploadedFileType,
})

const mapDispatchToProps = {
  setStepIndex,
  setTypeOfFileUpload
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Summary))
