import React from "react"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { confirmAlert } from "react-confirm-alert"
import { DropdownList } from "react-widgets"
import cloneDeep from "lodash.clonedeep"
import { emailRegex } from "Validation/common"
import {
  getPicklistByPicklistName,
  uploadMigrationData,
  setDataClarity,
  updateAuditLog,
  getUniqValueFromArray
} from "GlobalUtils/helpers"
import CONST, { sortNameStateList } from "GlobalUtils/consts.js"
import Loader from "../../../../GlobalComponents/Loader"
import { setStepIndex, setValidatedData, setTypeOfFileUpload } from "../../../../StateManagement/actions/AdminManagement/admMigrationTools"
import {addDocInDB, getSignedUrl} from "../../../../StateManagement/actions/docs_actions"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import SearchCountry from "../../../GoogleAddressForm/GoogleCountryComponent"
import {TextLabelInput} from "../../../../GlobalComponents/TextViewBox"


class DataGrid extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props,
      loading: false,
      modifiedData: [],
      offeringTypes: {},
      issuerTypes: [],
      issueTypes: [],
      useOfProceed: [],
      stateList: [],
      notMatchedStates: {},
      uploadResult: {},
      nonExistsPickListValue: {},
      nonExistsLeadAdv: {},
      errors: {},
      selectedIssuerType: {},
      selectedPurposeTypes: {},
      selectedIssueTypes: {},
      ignoreIssuerType: false,
      ignorePurposeType: false,
      ignoreIssueType: false,
    }
  }

  /**
   * Async willMount hook
   */
  async componentWillMount() {
    const {validatedData, nonExistsLeadAdv, nav2} = this.props
    const nonExistsPickListValue = cloneDeep(this.props.nonExistsPickListValue)
    const pickValue = {}
    this.setState({
      loading: true
    })

    // Get picklist values
    const picklistsKeys = ["LKUPCOUNTRY"]
    if(nav2 === "deal-files"){
      picklistsKeys.push("LKUPDEBTTRAN") // Debt
      picklistsKeys.push("LKUPOTHERSTRAN") // Others
      /* picklistsKeys.push("LKUPDERIVATIVETRAN") // Derivative
      picklistsKeys.push("LKUPPARTICIPANTTYPE") // RFP
      picklistsKeys.push("LKUPBUSDEVTRAN") // Business Development
      picklistsKeys.push("LKUPPRIMARYSECTOR")
      picklistsKeys.push("LKUPSECTYPEGENERAL")
      picklistsKeys.push("LKUPUSEOFPROCEED") */
    }
    const picResult = await getPicklistByPicklistName(picklistsKeys)

    const result = (picResult.length && picResult[1]) || {}
    const stateList =  (picResult[2] && picResult[2].LKUPCOUNTRY && picResult[2].LKUPCOUNTRY["United States"]) || []
    stateList.push("Puerto Rico")

    let debtSubTypes = []
    let otherSubTypes = []
    /*
     let derivativeSubTypes = []
     let rfpSubTypes = []
     let otherSubTypes = []
     let busDevSubTypes = []
     let marfpSubTypes = []
    */
    if(nav2 === "deal-files"){
      debtSubTypes =  (result.LKUPDEBTTRAN || []).map(type => ({
        name: type,
        groupBy: "Debt",
        id: type
      }))
      otherSubTypes = (result.LKUPOTHERSTRAN || []).map(type => ({
        name: type,
        groupBy: "Others",
        id: type
      }))
      /* derivativeSubTypes =  (result.LKUPDERIVATIVETRAN || []).map(type => ({
         name: type,
         groupBy: "Derivative",
         id: type
       }))
       rfpSubTypes =  (result.LKUPPARTICIPANTTYPE || []).map(type => ({
         name: type,
         groupBy: "RFP",
         id: type
       }))
       otherSubTypes = (result.LKUPOTHERSTRAN || []).map(type => ({
         name: type,
         groupBy: "Others",
         id: type
       }))
       busDevSubTypes =  (result.LKUPBUSDEVTRAN || []).map(type => ({
         name: type,
         groupBy: "Business Development",
         id: type
       }))
       marfpSubTypes =  [{
         name: "MA-RFP",
         groupBy: "MA-RFP",
         id: "MA-RFP"
       }]
       pickValue.issuerTypes = (result.LKUPPRIMARYSECTOR || [])
       pickValue.issueTypes = (result.LKUPSECTYPEGENERAL || [])
       pickValue.useOfProceed = (result.LKUPUSEOFPROCEED || []) */
      pickValue.tranTypes = [ ...debtSubTypes, ...otherSubTypes, /* , ...derivativeSubTypes, ...rfpSubTypes, ...otherSubTypes, ...busDevSubTypes, ...marfpSubTypes */]
    }

    let notMatchedStates = {}
    const modifiedData = []
    // let offeringTypes = []

    if(nav2 === "user-data" ){
      Object.keys(validatedData || {}).forEach(keyIndex => {
        const inspect = validatedData[keyIndex]
        const st = stateList.find(state => state && inspect.firmState && state.toLowerCase() === inspect.firmState.toLowerCase()) || ""
        if(st){
          inspect.firmState = st || ""
        }
        /* stateList.forEach(state => {
          if(state && inspect.firmState && state.toLowerCase() === inspect.firmState.toLowerCase()){
            inspect.firmState = state
          }
        }) */

        const findResult1 = stateList.findIndex(data => data && inspect.firmState && data.toLowerCase() === inspect.firmState.toLowerCase())
        if(findResult1 === -1 && inspect.firmState){
          const sortName = Object.keys(sortNameStateList || {}).find(key => key && inspect.firmState && key.toLowerCase() === inspect.firmState.toLowerCase())
          if(sortName) {
            inspect.firmState = sortNameStateList[inspect.firmState.toUpperCase()]
          }else {
            notMatchedStates = {
              ...notMatchedStates,
              [inspect.firmState]: ""
            }
          }
        }

        const findResult2 = stateList.findIndex(data => data && inspect.address2State && data.toLowerCase() === inspect.address2State.toLowerCase())
        if(findResult2 === -1 && inspect.address2State){
          const sortName = Object.keys(sortNameStateList || {}).find(key => key && inspect.address2State && key.toLowerCase() === inspect.address2State.toLowerCase())
          if(sortName) {
            inspect.firmState = sortNameStateList[inspect.address2State.toUpperCase()]
          }else {
            notMatchedStates = {
              ...notMatchedStates,
              [inspect.address2State]: ""
            }
          }
        }

        const findResult3 = stateList.findIndex(data => data && inspect.userState && data.toLowerCase() === inspect.userState.toLowerCase())
        if(findResult3 === -1 && inspect.userState){
          const sortName = Object.keys(sortNameStateList || {}).find(key => key && inspect.userState && key.toLowerCase() === inspect.userState.toLowerCase())
          if(sortName) {
            inspect.userState = sortNameStateList[inspect.userState.toUpperCase()]
          }else {
            notMatchedStates = {
              ...notMatchedStates,
              [inspect.userState]: ""
            }
          }
        }
        modifiedData.push(inspect)
      })
    }

    if(nav2 === "deal-files"){
      Object.keys(validatedData || {}).forEach(keyIndex => {
        const inspect = validatedData[keyIndex]
        /* stateList.forEach(state => {
          if(state && inspect.dealIssueTranState && state.toLowerCase() === inspect.dealIssueTranState.toLowerCase()){
            inspect.dealIssueTranState = state
          }
        }) */
        const st = stateList.find(state => state && inspect.dealIssueTranState && state.toLowerCase() === inspect.dealIssueTranState.toLowerCase()) || ""
        if(st){
          inspect.dealIssueTranState = st || ""
        }

        const findResult1 = stateList.findIndex(data => data && inspect.dealIssueTranState && data.toLowerCase() === inspect.dealIssueTranState.toLowerCase())
        if(findResult1 === -1 && inspect.dealIssueTranState){
          const sortName = Object.keys(sortNameStateList || {}).find(key => key && inspect.dealIssueTranState && key.toLowerCase() === inspect.dealIssueTranState.toLowerCase())
          if(sortName) {
            inspect.dealIssueTranState = sortNameStateList[inspect.dealIssueTranState.toUpperCase()]
          }else {
            notMatchedStates = {
              ...notMatchedStates,
              [inspect.dealIssueTranState]: ""
            }
          }
        }
        // offeringTypes.push(inspect.dealIssueofferingType)
        modifiedData.push(inspect)
      })

      /* const allOfferingTypes = await getUniqValueFromArray(offeringTypes)
      offeringTypes = {}
      Object.keys(nonExistsPickListValue || {}).forEach(async key => {
        nonExistsPickListValue[key] = await getUniqValueFromArray(nonExistsPickListValue[key])
      })
      allOfferingTypes.forEach(type => {
        offeringTypes = {
          ...offeringTypes,
          [type]: ""
        }
      }) */
    }

    console.log("1. notMatchedStates", notMatchedStates)
    console.log("2. modifiedData", modifiedData)
    console.log("3. stateList", stateList)
    console.log("4. sortNameStateList", sortNameStateList)
    console.log("5. nonExistsPickListValue", nonExistsPickListValue)
    console.log("6. nonExistsLeadAdv", nonExistsLeadAdv)
    Object.keys(nonExistsLeadAdv || {}).forEach(name => {
      nonExistsLeadAdv[name].oldUser = false
    })

    this.setState({
      loading: false,
      modifiedData,
      nonExistsLeadAdv,
      // offeringTypes,
      nonExistsPickListValue,
      stateList,
      notMatchedStates,
      ...pickValue,
    },() => {
      if(notMatchedStates && !Object.keys(notMatchedStates || {}).length && nav2 !== "deal-files"){
        this.nextStepHandler()
      }
    })
  }

  showWarningModal = (message) => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className='custom-ui'>
          <div style={{ margin: "20px 0px", fontWeight: "500" }} className='body'>
            {message || "Something went wrong, please contact Otaras support team."}
          </div>
          <button onClick={() => {
            onClose()
          }}>Ok</button>
        </div>
      )
    })
  }


  /**
   * Method to show confirmation modal
   * It takes text message as an input which is optional
   */
  showConfirmationModal = async () => {
    const{uploadedFileType, userAddress, mappedColumnObj, dbColumnHeaders, nav2, user, } = this.props
    let partTypes = dbColumnHeaders.filter(partType => partType.pickListKey === "LKUPPARTICIPANTTYPE").map(field => field.fieldName)
    partTypes = _.uniqBy(partTypes)
    let UWTypes = dbColumnHeaders.filter(partType => partType.pickListKey === "LKUPUNDERWRITERROLE").map(field => field.fieldName)
    UWTypes = _.uniqBy(UWTypes)
    const {modifiedData, notMatchedStates, tranTypes, offeringTypes, nonExistsPickListValue, selectedIssuerType, selectedIssueTypes,
      selectedPurposeTypes, ignoreIssueType, ignorePurposeType, ignoreIssuerType, nonExistsLeadAdv} = this.state
    let clients = []
    const leadManagers = {}
    let thirdParties = {}
    let thirdPartiesList = {}

    if(nav2 === "deal-files"){
      clients = modifiedData.map(client => {
        let entityAliases = []
        if(client && client.issuerAliases && typeof client.issuerAliases === "string"){
          entityAliases = client.issuerAliases.split(";")
          entityAliases = entityAliases.map(val => val && typeof val === "string" ? val.trim() : val)
        }
        return {
          name: client.dealIssueTranClientFirmName,
          entityType: client.clientEntityType,
          issuerType: client.clientIssuerType,
          entityAliases
        }
      })
      const newClients = []
      clients.forEach(clt => {
        const exCltIndex = newClients.findIndex(c => c.name.toLowerCase() === clt.name.toLowerCase())
        if(exCltIndex !== -1){
          newClients[exCltIndex].entityAliases = _.uniqBy([...newClients[exCltIndex].entityAliases, ...clt.entityAliases])
        }else {
          clt.entityAliases =_.uniqBy(clt.entityAliases)
          newClients.push(clt)
        }
      })
      clients = _.uniqBy(newClients, "name")
      thirdParties = {}
      thirdPartiesList = {}
      Object.keys(notMatchedStates || {}).forEach(key => {
        modifiedData.forEach(data => {
          if(data.dealIssueTranState && key && data.dealIssueTranState.toLowerCase() === key.toLowerCase()){
            data.dealIssueTranState = notMatchedStates[key]
          }
        })
      })

      if(!ignoreIssuerType){
        Object.keys(selectedIssuerType || {}).forEach(key => {
          modifiedData.forEach(data => {
            if(data.dealIssueTranPrimarySector && key && data.dealIssueTranPrimarySector.toLowerCase() === key.toLowerCase()){
              data.dealIssueTranPrimarySector = selectedIssuerType[key]
            }
          })
        })
      }

      if(!ignoreIssueType){
        Object.keys(selectedIssueTypes || {}).forEach(key => {
          modifiedData.forEach(data => {
            if(data.dealIssueSecurityType && key && data.dealIssueSecurityType.toLowerCase() === key.toLowerCase()){
              data.dealIssueSecurityType = selectedIssueTypes[key]
            }
          })
        })
      }

      if(!ignorePurposeType){
        Object.keys(selectedPurposeTypes).forEach(key => {
          modifiedData.forEach(data => {
            if(data.dealIssueUseOfProceeds && key && data.dealIssueUseOfProceeds.toLowerCase() === key.toLowerCase()){
              data.dealIssueUseOfProceeds = selectedPurposeTypes[key]
            }
          })
        })
      }

      Object.keys(modifiedData || {}).forEach(rowKey => {
        const data = modifiedData[rowKey]
        Object.keys(data || {}).forEach(typeKey => {
          const entity = {
            name: data[typeKey],
            entityAliases: []
          }
          if(partTypes.indexOf(typeKey) !== -1 && data[typeKey]){
            const partConfig = dbColumnHeaders.find(val => val.fieldName === typeKey)
            const aliasConfig = dbColumnHeaders.find(val => val.relatedTo === typeKey)
            const aliasMap = Object.values(mappedColumnObj).indexOf(aliasConfig.fieldName) !== -1
            if(aliasMap && aliasConfig && aliasConfig.relatedTo && data[aliasConfig.relatedTo] && typeof data[aliasConfig.relatedTo] === "string"){
              entity.entityAliases = data[aliasConfig.fieldName].split(";")
              entity.entityAliases = entity.entityAliases.map(val => val && typeof val === "string" ? val.trim() : val)
            }
            if(thirdParties.hasOwnProperty(partConfig.staticName)){
              thirdParties[partConfig.staticName].push(entity)
            }else {
              thirdParties = {
                ...thirdParties,
                [partConfig.staticName]: [entity]
              }
            }
          }
          if(UWTypes.indexOf(typeKey) !== -1 && data[typeKey]){
            const UWConfig = dbColumnHeaders.find(val => val.fieldName === typeKey)
            const aliasConfig = dbColumnHeaders.find(val => val.relatedTo === typeKey)
            const aliasMap = Object.values(mappedColumnObj).indexOf(aliasConfig.fieldName) !== -1
            if(aliasMap && aliasConfig && aliasConfig.relatedTo && data[aliasConfig.relatedTo] && typeof data[aliasConfig.relatedTo] === "string"){
              entity.entityAliases = data[aliasConfig.fieldName].split(";")
              entity.entityAliases = entity.entityAliases.map(val => val && typeof val === "string" ? val.trim() : val)
            }
            if(thirdParties.hasOwnProperty("Underwriting Services")){
              thirdParties["Underwriting Services"].push(entity)
            }else {
              thirdParties = {
                ...thirdParties,
                "Underwriting Services": [entity]
              }
            }
            data[`dealSyndicate${typeKey}`] = UWConfig.staticName
          }
        })
        if(data.dealIssueTranAssignedTo){
          const lead = nonExistsLeadAdv[data.dealIssueTranAssignedTo]
          if(lead){
            if(lead.assigneeId && lead.oldUser){
              data.dealIssueTranAssigneeId = lead.assigneeId
              data.dealIssueTranAssignedTo = lead.assigneeName
            }else {
              leadManagers[data.dealIssueTranAssignedTo] = lead
            }
          }
        }
      })

      Object.keys(thirdParties || {}).forEach(async (partyKey) => {
        thirdParties[partyKey] = _.uniqBy(thirdParties[partyKey], "name")
        // thirdParties[partyKey] = await getUniqValueFromArray(thirdParties[partyKey])
        const newThirdParties = []
        thirdParties[partyKey].forEach(th => {
          const exThIndex = newThirdParties.findIndex(c => c.name === th.name)
          if(exThIndex !== -1){
            newThirdParties[exThIndex].entityAliases = _.uniqBy([...newThirdParties[exThIndex].entityAliases, ...th.entityAliases])
          }else {
            th.entityAliases =_.uniqBy(th.entityAliases)
            newThirdParties.push(th)
          }
        })
        thirdParties[partyKey] = _.uniqBy(newThirdParties, "name")

        thirdParties[partyKey].forEach(party => {
          const pName = typeof party === "string" ? party : party.name
          if(thirdPartiesList.hasOwnProperty(pName.toLowerCase()) && thirdPartiesList[pName.toLowerCase()].marketRole.indexOf(partyKey) === -1){
            thirdPartiesList[pName.toLowerCase()].marketRole.push(partyKey)
            thirdPartiesList[pName.toLowerCase()].entityAliases = _.uniqBy([...thirdPartiesList[pName.toLowerCase()].entityAliases, ...party.entityAliases])
          }else {
            thirdPartiesList = {
              ...thirdPartiesList,
              [pName.toLowerCase()]: {
                ...(typeof party === "string" ? { name: pName } : party),
                marketRole: [partyKey]
              }
            }
          }
        })

      })
      /* Object.keys(nonExistsPickListValue).forEach(async key => {
        nonExistsPickListValue[key] = await getUniqValueFromArray(nonExistsPickListValue[key])
      }) */
    }else {
      Object.keys(notMatchedStates || {}).forEach(key => {
        modifiedData.forEach(data => {
          if(data.firmState && key && data.firmState.toLowerCase() === key.toLowerCase()){
            data.firmState = notMatchedStates[key]
          }

          if(data.address2State && key && data.address2State.toLowerCase() === key.toLowerCase()){
            data.address2State = notMatchedStates[key]
          }
        })
      })
    }
    console.log("5. Final Modified Data", modifiedData)
    console.log("Not Exists Lead Managers...", leadManagers)

    if(!(modifiedData && modifiedData.length)){
      return toast("No data found", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }

    confirmAlert({
      customUI: ({ onClose }) => (
        <div className='custom-ui'>
          <div style={{ margin: "20px 0px", fontWeight: "500" }} className='body'>Are you sure, you want to proceed with {(modifiedData && modifiedData.length) || 0} valid data?</div>
          <button onClick={() => { this.setState({loading: false}, () => onClose())} }>No</button>
          <button onClick={() => {
            onClose()
            this.setState({
              loading: true
            }, async () => {
              const payload = await setDataClarity(uploadedFileType, modifiedData, userAddress, mappedColumnObj, tranTypes, offeringTypes, user, partTypes, UWTypes)
              if(nav2 === "user-data"){
                // const payload = await setDataClarity(uploadedFileType, modifiedData, userAddress, mappedColumnObj)
                console.log("33. payload", payload)
                const result = await uploadMigrationData(uploadedFileType, payload)
                if (result && result.data && result.data.done) {
                  toast(result.data.message, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
                  if(this.props.file){
                    this.setState({
                      uploadResult: (result.data && result.data.data) || {}
                    },() => {
                      this.props.setTypeOfFileUpload({uploadResult: (result.data && result.data.data) || {} })
                      return this.uploadFile(this.props.file)
                    })
                  }
                  this.props.setTypeOfFileUpload({uploadResult: (result.data && result.data.data) || {}, selectedStepIndex: this.state.stepIndex + 1 })
                  this.setState({
                    loading: false,
                    uploadResult: (result.data && result.data.data) || {}
                  })
                }else {
                  this.setState({
                    loading: false
                  })
                  toast(result.data.message, { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
                }
              }else {
                if(!ignoreIssuerType){
                  delete nonExistsPickListValue.LKUPPRIMARYSECTOR
                }
                if(!ignoreIssueType){
                  delete nonExistsPickListValue.LKUPSECTYPEGENERAL
                }
                if(!ignorePurposeType){
                  delete nonExistsPickListValue.LKUPUSEOFPROCEED
                }
                payload.clients = clients
                payload.thirdPartiesList = thirdPartiesList
                payload.picklists = nonExistsPickListValue
                payload.leadManagers = leadManagers
                console.log("DM Deals payload", payload)
                const result = await uploadMigrationData(uploadedFileType, payload)
                if (result && result.data && result.data.done) {
                  toast(result.data.message, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
                  if(this.props.file){
                    this.setState({
                      uploadResult: (result.data && result.data.data) || {}
                    },() => {
                      this.props.setTypeOfFileUpload({uploadResult: (result.data && result.data.data) || {} })
                      return this.uploadFile(this.props.file)
                    })
                  }
                  this.props.setTypeOfFileUpload({uploadResult: (result.data && result.data.data) || {}, selectedStepIndex: this.state.stepIndex + 1 })
                  this.setState({
                    loading: false,
                    uploadResult: (result.data && result.data.data) || {}
                  })
                }else {
                  this.setState({
                    loading: false
                  })
                  toast(result.data.message, { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
                }
              }
            })
          }}>Yes</button>
        </div>
      )
    })
  }

  cancelStepHandler = () => {
    this.props.setTypeOfFileUpload({
      gridData: null,
      interimStorageKey: null,
      inspectData: null,
      validatedData: null,
      uploadedFileType: null,
      userAddress: null,
      mappedColumnObj: null,
      dbColumnHeaders: null,
      nonExistsPickListValue: null,
      nonExistsLeadAdv: null,
      dbLookupColumns: null,
      leadAdviors: null,
      file: null,
    })
    this.props.history.goBack()
  }

  nextStepHandler = () => {
    this.setState({
      loading: true
    },() => {
      window.setTimeout(() => this.stepHandle(), 1000)
    })
  }

  stepHandle = async () => {
    const { nav2 } = this.props
    const valid = await this.leadAdvValidate()
    if(!valid) return this.setState({loading: false})

    const { notMatchedStates, offeringTypes, nonExistsPickListValue, selectedIssuerType, selectedPurposeTypes, selectedIssueTypes, ignorePurposeType, ignoreIssueType, ignoreIssuerType } = this.state
    if(Object.keys(notMatchedStates || {}).length !== Object.values(notMatchedStates || {}).filter(val => val).length ||
      Object.keys(offeringTypes || {}).length !== Object.values(offeringTypes || {}).filter(val => val).length ||
      (nonExistsPickListValue && typeof nonExistsPickListValue === "object" && 
        (nonExistsPickListValue && nonExistsPickListValue.LKUPPRIMARYSECTOR && nonExistsPickListValue.LKUPPRIMARYSECTOR.length !== Object.values(selectedIssuerType).length && !ignoreIssuerType) ||
        (nonExistsPickListValue && nonExistsPickListValue.LKUPSECTYPEGENERAL && nonExistsPickListValue.LKUPSECTYPEGENERAL.length !== Object.values(selectedIssueTypes).length && !ignoreIssueType) ||
        (nonExistsPickListValue && nonExistsPickListValue.LKUPUSEOFPROCEED && nonExistsPickListValue.LKUPUSEOFPROCEED.length !== Object.values(selectedPurposeTypes).length && !ignorePurposeType)) ){
      this.showWarningModal(
        <div>
          <div>Please check and map values for below mandatory fields</div>
          <div>
            <div>State</div>
            <ul style={{ textAlign: "left", marginTop: "10px" }}>
              {
                Object.keys(notMatchedStates || {}).map((columnName,index) => <li key={index.toString()}>- {columnName}</li>)
              }
            </ul>
          </div>
          {
            nav2 === "deal-files" ?
              <div>
                <div>Activity type</div>
                <ul style={{ textAlign: "left", marginTop: "10px" }}>
                  {
                    Object.keys(offeringTypes || {}).map((columnName,index) => <li key={index.toString()}>- {columnName}</li>)
                  }
                </ul>
              </div>
              :null
          }
          {
            nav2 === "deal-files" && ((nonExistsPickListValue && nonExistsPickListValue.LKUPSECTYPEGENERAL) || []).length ?
              <div>
                <div>Issuer type</div>
                <ul style={{ textAlign: "left", marginTop: "10px" }}>
                  {
                    ((nonExistsPickListValue && nonExistsPickListValue.LKUPPRIMARYSECTOR) || []).map((columnName,index) => <li key={index.toString()}>- {columnName}</li>)
                  }
                </ul>
              </div>
              :null
          }
          {
            nav2 === "deal-files" && ((nonExistsPickListValue && nonExistsPickListValue.LKUPSECTYPEGENERAL) || []).length ?
              <div>
                <div>Issue type</div>
                <ul style={{ textAlign: "left", marginTop: "10px" }}>
                  {
                    ((nonExistsPickListValue && nonExistsPickListValue.LKUPSECTYPEGENERAL) || []).map((columnName,index) => <li key={index.toString()}>- {columnName}</li>)
                  }
                </ul>
              </div>
              :null
          }
          {
            nav2 === "deal-files" && ((nonExistsPickListValue && nonExistsPickListValue.LKUPSECTYPEGENERAL) || []).length ?
              <div>
                <div>Use of Proceeds (Purpose)</div>
                <ul style={{ textAlign: "left", marginTop: "10px" }}>
                  {
                    ((nonExistsPickListValue && nonExistsPickListValue.LKUPUSEOFPROCEED) || []).map((columnName,index) => <li key={index.toString()}>- {columnName}</li>)
                  }
                </ul>
              </div>
              :null
          }
        </div>
      )
      return this.setState({loading: false})
    }
    this.showConfirmationModal()
  }

  leadAdvValidate = async () => {
    const {nonExistsLeadAdv} = this.state
    const {nav2} = this.props
    let valid = true
    if(nav2 === "deal-files" && nonExistsLeadAdv && Object.keys(nonExistsLeadAdv || {}).length){
      const errors = {}
      const leadNames = Object.keys(nonExistsLeadAdv || {})
      for (const i in leadNames) {
        const lead = nonExistsLeadAdv[leadNames[i]]
        const errorMessages = {}
        if(lead.oldUser){
          const error = this.onChangeValidation("assigneeId", lead.assigneeId || "")
          if(error){
            errorMessages.assigneeId = error
          }
        }else {
          const reqField = ["userFirstName", "userLastName", "emailId"]
          reqField.forEach(field => {
            const error = this.onChangeValidation(field, lead[field] || "")
            if(error){
              errorMessages[field] = error
            }
          })
        }
        if(errorMessages && Object.keys(errorMessages || {}).length){
          errors[leadNames[i]] = errorMessages
        }
      }

      if(errors && Object.keys(errors || {}).length) {
        this.setState({ errors, activeItem: [0,1,2,3,4,5] })
        valid = false
      }
    }
    return valid
  }

  uploadFile = async (file) => {
    const { bucketName, contextId, contextType, tenantId } = this.props
    const tags = {
      status : "Success"
    }
    let fileName = file.name
    const extnIdx = fileName.lastIndexOf(".")
    if (extnIdx > -1) {
      fileName = `${fileName.substr(
        0,
        extnIdx
      )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
    }
    const contentType = file.type
    if (!fileName) {
      return toast("No file name provided", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
    let filePath
    if (tenantId && contextId && contextType) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const opType = "upload"
    const options = { contentType, tags }

    const res = await getSignedUrl({  // eslint-disable-line
      opType,
      bucketName,
      fileName: filePath,
      options
    })
    if (res) {
      // console.log("res : ", res)
      this.uploadWithSignedUrl(bucketName, res.data.url, file, fileName, tags)
    }else {
      console.log("err : ", res)
    }
  }

  uploadWithSignedUrl(bucketName, signedS3Url, file, fileName, tags) {
    /* console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      bucketName,
      fileName,
      file.type
    ) */
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("No tagging required any more")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = async () => {
      // console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        // console.log("file uploaded ok")
        // console.log(`Last upload successful for "${fileName}" at ${new Date()}`)
        await this.updateDocsDB(bucketName, file, fileName)
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded", xhr.status, xhr.responseText, err)
    }
    xhr.send(file)
  }

  updateDocsDB = async (bucketName, file, fileName) => {
    const {
      contextId,
      contextType,
      folderId,
      tenantId,
      user,
      uploadedFileType
    } = this.props
    const {uploadResult} = this.state
    const doc = {
      name: fileName,
      originalName: file.name,
      contextType,
      folderId,
      contextId,
      tenantId
    }
    const res = await addDocInDB(doc)
    if(res){
      console.log("inserted ok in docs db : ", res)
      const log = uploadedFileType === "Users/Entities" ?
        `${uploadResult && uploadResult.newInsertedEntities || 0} Entities and ${uploadResult && uploadResult.newInsertedUsers || 0} users Uploaded` :
        uploadedFileType === "Entities" ?
          `${uploadResult && uploadResult.newInsertedEntities || 0} Entities Uploaded` :
          uploadedFileType === "Users" ?
            `${uploadResult && uploadResult.newInsertedUsers || 0 } Users Uploaded` :
            uploadedFileType === "Deals" ?
              `${uploadResult && `${uploadResult.transactionsCount || 0} Deals, ${uploadResult.transactionsDuplicateCount || 0} Dupliacte Deals, ${uploadResult.entitiesCount || 0} Entities, ${uploadResult.thirdPartiesCount || 0} Third parties uploaded` } And ${uploadResult.leadManagers || 0} Lead Managers` : ""

      const audits = [
        { userName: `${user.userFirstName} ${user.userLastName}`, log: `Migration document ${file.name} upload for ${uploadedFileType || ""}`, type: this.props.nav2, uploadedData: log,
          fileId: res.data._id, status: "Success", date: new Date(), key: "migration" }
      ]
      await updateAuditLog("Data Migration", audits)
      this.props.setStepIndex(this.state.stepIndex + 1)
    }else {
      console.log("err in inserting in docs db : ", res)
    }
  }

  stateMappingHandler = (key, value) => {
    const { notMatchedStates } = this.state
    notMatchedStates[key] = (value && value.state.trim()) || ""
    this.setState({
      notMatchedStates
    })
  }

  activityTypeColumnMapping = (key, value) => {
    const { offeringTypes } = this.state
    offeringTypes[key] = value
    this.setState({
      offeringTypes
    })
  }

  onChangeValidation = (name, value) => {
    const {dbLookupColumns} = this.props
    const regexString = new RegExp(emailRegex, "g")
    let error = ""
    switch (name) {
    case "emailId": if (!value) {
      error = "Required"
    } else if (value && !regexString.test(value)) {
      error = "Please enter valid email"
    }else if(dbLookupColumns && dbLookupColumns.emailId && dbLookupColumns.emailId.indexOf(value) !== -1){
      error = "Email already exists"
    }
      break
    case "userFirstName": if (!value) {
      error = "Required"
    }
      break
    case "userLastName": if (!value) {
      error = "Required"
    }
      break
    case "assigneeId": if (!value) {
      error = "Required"
    }
      break
    default:
      error = ""
    }

    return error
  }

  onChange = (event, key, name, value) => {
    const {nonExistsLeadAdv} = this.state
    let {errors} = this.state
    if(name === "assigneeId"){
      nonExistsLeadAdv[key] = {
        ...nonExistsLeadAdv[key],
        [name]: value.id,
        assigneeName: value.name
      }
    }else {
      nonExistsLeadAdv[key] = {
        ...nonExistsLeadAdv[key],
        [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value
      }
    }
    if((event && event.target && event.target.name) || name){
      const error = this.onChangeValidation((event && event.target && event.target.name) || name || "", (event && event.target && event.target.value) || (value && value.id) || "")
      errors = {
        ...errors,
        [key]: {
          ...errors[key],
          [(event && event.target && event.target.name) || name]: error
        }
      }
    }

    this.setState({
      nonExistsLeadAdv,
      errors
    })
  }

  setColumnMapping = (stateKey, key, value) => {
    const replaceObj = this.state[stateKey]
    replaceObj[key] = value
    this.setState({
      [stateKey]: replaceObj
    })
  }

  ignoreMapping = event => {
    const { name, checked } = event.target
    this.setState({
      [name]: checked
    })
  }

  render() {
    const {loading, modifiedData, nav2, nonExistsPickListValue, issuerTypes, useOfProceed, issueTypes, selectedIssuerType, selectedIssueTypes, selectedPurposeTypes,
      notMatchedStates, stateList, tranTypes, offeringTypes, ignoreIssuerType, ignoreIssueType, ignorePurposeType, errors, nonExistsLeadAdv} = this.state
    const {leadAdviors} = this.props
    const uniqIssuerTypes = []
    const uniqIssueTypes = []
    const uniqPurposeTypes = []
    if(nonExistsPickListValue && nonExistsPickListValue.LKUPPRIMARYSECTOR) {
      nonExistsPickListValue.LKUPPRIMARYSECTOR.forEach(val => {
        if (uniqIssuerTypes.indexOf(val) === -1) {
          uniqIssuerTypes.push(val)
        }
      })
    }
    if(nonExistsPickListValue && nonExistsPickListValue.LKUPSECTYPEGENERAL) {
      nonExistsPickListValue.LKUPSECTYPEGENERAL.forEach(val => {
        if (uniqIssueTypes.indexOf(val) === -1) {
          uniqIssueTypes.push(val)
        }
      })
    }
    if(nonExistsPickListValue && nonExistsPickListValue.LKUPUSEOFPROCEED) {
      nonExistsPickListValue.LKUPUSEOFPROCEED.forEach(val => {
        if (uniqPurposeTypes.indexOf(val) === -1) {
          uniqPurposeTypes.push(val)
        }
      })
    }

    return (
      <div className="stepContainer">
        { loading ? <Loader /> : null }
        <div className="stepBody" style={{marginTop: 0, minHeight: "85%"}}>
          {
            Object.keys(notMatchedStates || {}).length ?
              <div className="columns">
                <div className="column">
                  <div className="infoDiv">
                    <i className="fa fa-info-circle" style={{margin: 10}}/>
                    {nav2 === "deal-files" ? "Please verify state, activity type and mapped with existing value in the database." : "Please verify state and mapped with existing value in the database."}
                  </div>
                </div>
              </div>
              : <div className="columns">
                <div className="column">
                  <div className="infoDiv">
                    Your data is valid
                  </div>
                </div>
              </div>
          }

          <Accordion
            multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <div>
                {
                  notMatchedStates && Object.keys(notMatchedStates || {}).length ?
                    <RatingSection
                      onAccordion={() => onAccordion(0)}
                      title="State Mapping"
                    >
                      {activeAccordions.includes(0) && (
                        <div>
                          {
                            Object.keys(notMatchedStates || {}).map((key,index) => (
                              <div className="columns" key={index.toString()}>
                                <div className="column is-5" style={{ display: "inline-block", fontWeight: 500, textAlign: "right" }}>{key} :</div>
                                <div className="column is-3" style={{ display: "inline-block" }}>
                                  <SearchCountry
                                    idx={key}
                                    label="Search state"
                                    value={(notMatchedStates && notMatchedStates[key]) || ""}
                                    getCountryDetails={(value) => this.stateMappingHandler(key , value)}
                                    disabled={false}
                                    dataMigrationview
                                  />
                                  {/* <DropdownList
                                    filter='contains'
                                    data={stateList}
                                    value={(notMatchedStates && notMatchedStates[key]) || ""}
                                    placeholder='Select a value'
                                    onChange={(value) => this.stateMappingHandler(key, value)}
                                  /> */}
                                </div>
                              </div>
                            ))
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null
                }

                {
                  nav2 === "deal-files" && Object.keys(offeringTypes || {}).length ?
                    <RatingSection
                      onAccordion={() => onAccordion(1)}
                      title="Activity type"
                    >
                      {activeAccordions.includes(1) && (
                        <div>
                          {
                            Object.keys(offeringTypes || {}).map((key,index) => (
                              <div className="columns" key={index.toString()}>
                                <div className="column is-5" style={{ display: "inline-block", fontWeight: 500, textAlign: "right" }}>{key || ""} :</div>
                                <div className="column is-3" style={{ display: "inline-block" }}>
                                  <DropdownList
                                    filter='contains'
                                    data={tranTypes}
                                    textField="name"
                                    valueField="id"
                                    groupBy="groupBy"
                                    disabled={["Other (please specify)"]}
                                    value={(offeringTypes && offeringTypes[key]) || ""}
                                    placeholder='Select a value'
                                    onChange={value => this.activityTypeColumnMapping(key, value.name)}
                                  />
                                </div>
                              </div>
                            ))
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null
                }

                {
                  nav2 === "deal-files" && nonExistsPickListValue && Object.keys(nonExistsPickListValue || {}).length
                  && nonExistsPickListValue.LKUPPRIMARYSECTOR && nonExistsPickListValue.LKUPPRIMARYSECTOR.length ?
                    <RatingSection
                      onAccordion={() => onAccordion(2)}
                      title="Issuer Type"
                    >
                      {activeAccordions.includes(2) && (
                        <div>
                          <div className="columns">
                            <div className="column"/>
                            <div className="column">
                              <div className="hpTablesTd wrap-cell-text" style={{marginLeft: "30%"}}>
                                <label className="checkbox-button">
                                  If you want to ignore.
                                  <input
                                    type="checkbox"
                                    name="ignoreIssuerType"
                                    checked={ignoreIssuerType}
                                    onChange={this.ignoreMapping}
                                    onBlur={this.ignoreMapping}
                                  /><span className="checkmark"/>
                                </label>
                              </div>
                            </div>
                            <div className="column"/>
                          </div>
                          {
                            nav2 === "deal-files" && Object.keys(nonExistsPickListValue || {}).length && !ignoreIssuerType ?
                              uniqIssuerTypes.map((key,index) => (
                                <div className="columns" key={index.toString()}>
                                  <div className="column is-5" style={{ display: "inline-block", fontWeight: 500, textAlign: "right" }}>{key || ""} :</div>
                                  <div className="column is-3" style={{ display: "inline-block" }}>
                                    <DropdownList
                                      filter='contains'
                                      data={issuerTypes}
                                      textField="name"
                                      valueField="id"
                                      groupBy="groupBy"
                                      disabled={["Other"]}
                                      value={(selectedIssuerType && selectedIssuerType[key]) || ""}
                                      placeholder='Select a value'
                                      onChange={value => this.setColumnMapping("selectedIssuerType", key, value)}
                                    />
                                  </div>
                                </div>
                              ))
                              : null
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null
                }

                {
                  nav2 === "deal-files" && nonExistsPickListValue && Object.keys(nonExistsPickListValue || {}).length
                  && nonExistsPickListValue.LKUPSECTYPEGENERAL && nonExistsPickListValue.LKUPSECTYPEGENERAL.length ?
                    <RatingSection
                      onAccordion={() => onAccordion(3)}
                      title="Issue Type"
                    >
                      {activeAccordions.includes(3) && (
                        <div>
                          <div className="columns">
                            <div className="column"/>
                            <div className="column">
                              <div className="hpTablesTd wrap-cell-text" style={{marginLeft: "30%"}}>
                                <label className="checkbox-button">
                                  If you want to ignore.
                                  <input
                                    type="checkbox"
                                    name="ignoreIssueType"
                                    checked={ignoreIssueType}
                                    onChange={this.ignoreMapping}
                                    onBlur={this.ignoreMapping}
                                  /><span className="checkmark"/>
                                </label>
                              </div>
                            </div>
                            <div className="column"/>
                          </div>
                          {
                            nav2 === "deal-files" && Object.keys(nonExistsPickListValue || {}).length && !ignoreIssueType ?
                              uniqIssueTypes.map((key,index) => (
                                <div className="columns" key={index.toString()}>
                                  <div className="column is-5" style={{ display: "inline-block", fontWeight: 500, textAlign: "right" }}>{key || ""} :</div>
                                  <div className="column is-3" style={{ display: "inline-block" }}>
                                    <DropdownList
                                      filter='contains'
                                      data={issueTypes}
                                      textField="name"
                                      valueField="id"
                                      groupBy="groupBy"
                                      disabled={["Other"]}
                                      value={(selectedIssueTypes && selectedIssueTypes[key]) || ""}
                                      placeholder='Select a value'
                                      onChange={value => this.setColumnMapping("selectedIssueTypes", key, value)}
                                    />
                                  </div>
                                </div>
                              ))
                              : null
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null
                }

                {
                  nav2 === "deal-files" && nonExistsPickListValue && Object.keys(nonExistsPickListValue || {}).length
                  && nonExistsPickListValue.LKUPUSEOFPROCEED && nonExistsPickListValue.LKUPUSEOFPROCEED.length ?
                    <RatingSection
                      onAccordion={() => onAccordion(4)}
                      title="Use of Proceeds (Purpose)"
                    >
                      {activeAccordions.includes(4) && (
                        <div>
                          <div className="columns">
                            <div className="column"/>
                            <div className="column">
                              <div className="hpTablesTd wrap-cell-text" style={{marginLeft: "30%"}}>
                                <label className="checkbox-button">
                                  If you want to ignore.
                                  <input
                                    type="checkbox"
                                    name="ignorePurposeType"
                                    checked={ignorePurposeType}
                                    onChange={this.ignoreMapping}
                                    onBlur={this.ignoreMapping}
                                  /><span className="checkmark"/>
                                </label>
                              </div>
                            </div>
                            <div className="column"/>
                          </div>
                          {
                            nav2 === "deal-files" && Object.keys(nonExistsPickListValue || {}).length && !ignorePurposeType ?
                              uniqPurposeTypes.map((key,index) => (
                                <div className="columns" key={index.toString()}>
                                  <div className="column is-5" style={{ display: "inline-block", fontWeight: 500, textAlign: "right" }}>{key || ""} :</div>
                                  <div className="column is-3" style={{ display: "inline-block" }}>
                                    <DropdownList
                                      filter='contains'
                                      data={useOfProceed}
                                      textField="name"
                                      valueField="id"
                                      groupBy="groupBy"
                                      disabled={["Other"]}
                                      value={(selectedPurposeTypes && selectedPurposeTypes[key]) || ""}
                                      placeholder='Select a value'
                                      onChange={value => this.setColumnMapping("selectedPurposeTypes", key, value)}
                                    />
                                  </div>
                                </div>
                              ))
                              : null
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null
                }

                {
                  nav2 === "deal-files" && Object.keys(nonExistsLeadAdv || {}).length ?
                    <RatingSection
                      onAccordion={() => onAccordion(5)}
                      title="Lead Advisor"
                    >
                      {activeAccordions.includes(5) && (
                        <div>
                          {
                            Object.keys(nonExistsLeadAdv || {}).map((key,index) => {
                                const errMsg = (errors && errors[key]) || {}
                                const item = nonExistsLeadAdv[key] || {}
                                return (
                                  <div key={index.toString()}>
                                    <div className="columns">
                                      <div className="column is-2">{key || ""} :</div>
                                      {leadAdviors && Array.isArray(leadAdviors) && leadAdviors.length ?
                                        <div className="column is-5">
                                          <div className="hpTablesTd wrap-cell-text" style={{marginLeft: "30%", display: "inline-block"}}>
                                            <label className="checkbox-button">
                                              Do you want to choose from existing users?
                                              <input
                                                type="checkbox"
                                                name="oldUser"
                                                checked={item.oldUser || false}
                                                onChange={(e) => this.onChange(e, key)}
                                                onBlur={(e) => this.onChange(e, key)}
                                              /><span className="checkmark"/>
                                            </label>
                                          </div>
                                        </div>
                                        : null}
                                      { item.oldUser ?
                                        <div className="column is-4">
                                          <DropdownList
                                            filter
                                            value={item.assigneeId || []}
                                            data={this.props.leadAdviors || []}
                                            message="Search"
                                            textField="name"
                                            valueField="id"
                                            key="name"
                                            onChange={user => this.onChange("", key, "assigneeId", user)}
                                          />
                                          {errMsg.assigneeId ? <small className="is-small text-error has-text-danger ">{errMsg.assigneeId}</small> : null}
                                        </div>
                                        : null }
                                    </div>
                                    { !item.oldUser ?
                                      <div className="columns">
                                        <TextLabelInput
                                          className="column"
                                          placeholder="First Name e.g. John"
                                          // label="First Name"
                                          error={errMsg.userFirstName || ""}
                                          name="userFirstName"
                                          value={item.userFirstName || ""}
                                          onChange={e => this.onChange(e, key)}
                                        />
                                        <TextLabelInput
                                          className="column"
                                          placeholder="Last Name e.g. Doe"
                                          // label="Last Name"
                                          error={errMsg.userLastName || ""}
                                          name="userLastName"
                                          value={item.userLastName || ""}
                                          onChange={e => this.onChange(e, key)}
                                        />
                                        <TextLabelInput
                                          className="column"
                                          placeholder="Primary email e.g. johndoe@otaras.com"
                                          // label="Primary email"
                                          error={errMsg.emailId || ""}
                                          name="emailId"
                                          value={item.emailId || ""}
                                          onChange={e => this.onChange(e, key)}
                                        />
                                      </div> : null }
                                    <hr/>
                                  </div>
                                )
                              })
                          }
                        </div>
                      )}
                    </RatingSection>
                    : null
                }
              </div>
            )}
          />

        </div>
        <div className='stepFooter'>
          <button className="button is-pulled-left" onClick={this.cancelStepHandler}>Cancel</button>
          <button className="button is-info is-pulled-right" onClick={this.nextStepHandler} disabled={loading}>Proceed With Valid Data</button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  gridData: state.admMigrationToolsReducer.gridData,
  interimStorageKey: state.admMigrationToolsReducer.interimStorageKey,
  inspectData: state.admMigrationToolsReducer.inspectData,
  validatedData: state.admMigrationToolsReducer.validatedData,
  uploadedFileType: state.admMigrationToolsReducer.uploadedFileType,
  userAddress: state.admMigrationToolsReducer.userAddress,
  mappedColumnObj: state.admMigrationToolsReducer.mappedColumnObj,
  dbColumnHeaders: state.admMigrationToolsReducer.dbColumnHeaders,
  nonExistsPickListValue: state.admMigrationToolsReducer.nonExistsPickListValue,
  nonExistsLeadAdv: state.admMigrationToolsReducer.nonExistsLeadAdv,
  dbLookupColumns: state.admMigrationToolsReducer.dbLookupColumns,
  leadAdviors: state.admMigrationToolsReducer.leadAdviors,
  file: state.admMigrationToolsReducer.file,
  user: (state.auth && state.auth.userEntities) || {}
})

const mapDispatchToProps = {
  setStepIndex,
  setValidatedData,
  setTypeOfFileUpload
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DataGrid))
