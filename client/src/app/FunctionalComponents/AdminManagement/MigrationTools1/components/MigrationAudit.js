import React from "react"
import ReactTable from "react-table"
import moment from "moment"
import "react-table/react-table.css"
import DocLink from "../../../docs/DocLink"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const MigrationAudit = ({
  logs,
  pageSizeOptions = [5, 10, 20, 50, 100],
  search,
  openModal
}) => {
  const columns = [
    {
      Header: "USER",
      id: "userName",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            {item.userName}
          </div>
        )
      },
      sortMethod: (a, b) => a.userName.localeCompare(b.userName)
    },
    {
      id: "log",
      Header: "ACTION",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            {item.log}
          </div>
        )
      },
      sortMethod: (a, b) => a.log.localeCompare(b.log)
    },
    {
      id: "uploadedData",
      Header: "UPLOADED DATA",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            {item.uploadedData || ""}
            { item.status === "Fail" ?
              <a
                className="control is-pulled-right"
                onClick={() => openModal(item)}
                title="View errors count"
              >
                <span className="has-text-link">
                  <i className="far fa-eye"/>
                </span>
              </a>
              : null}
          </div>
        )
      },
      sortMethod: (a, b) => a.uploadedData && b.uploadedData && a.uploadedData.localeCompare(b.uploadedData || "")
    },
    {
      id: "status",
      Header: "STATUS",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            {item.status}
          </div>
        )
      },
      sortMethod: (a, b) => a.status.localeCompare(b.status)
    },
    {
      id: "fileId",
      Header: "FILE",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            <DocLink docId={item.fileId} />
          </div>
        )
      }
    },
    {
      id: "ip",
      Header: "IP",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            {item.ip}
          </div>
        )
      },
      sortMethod: (a, b) => a.ip.localeCompare(b.ip)
    },
    {
      id: "date",
      Header: "TIME",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{wordBreak: "break-all"}}>
            {item.date ? moment(item.date).format("MM-DD-YYYY h:mm A"): "" || "-"}
          </div>
        )
      },
      sortMethod: (a, b) => a.timeStamp - b.timeStamp
    }
  ]
  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={logs}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default MigrationAudit
