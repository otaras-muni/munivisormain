import React from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { confirmAlert } from "react-confirm-alert"
import { removeWhiteSpaces } from "GlobalUtils/helpers"
import {readDMExcel} from "../../../../../../src/app/StateManagement/actions/Common"

import "react-confirm-alert/src/react-confirm-alert.css"

import {
  setStepIndex,
  addGridData,
  setFile,
  setTypeOfFileUpload
} from "../../../../StateManagement/actions/AdminManagement/admMigrationTools"
import Loader from "../../../../GlobalComponents/Loader"
import Accordion from "../../../../GlobalComponents/Accordion"

const ALLOWED_FILE_FORMATS = [".xlsx", ".xls", ".csv", ".CSV"]

class FileUploader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props,
      fileName: null,
      file: null,
      fileError: null,
      loading: false,
    }
  }

  /**
   * Confirm alert to check file has headers
   */
  confirmFileHasHeaders = (files) => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className='custom-ui'>
          <div style={{ margin: "20px 0px", fontWeight: "500" }} className='body'>Does this file has column headers?</div>
          <button onClick={() => {
            onClose(this.uploadFileHandler(files, false))
          }}>No</button>
          <button onClick={() => {
            onClose(this.uploadFileHandler(files, true))
          }}>Yes</button>
        </div>
      )
    })
  }

  /**
   * File upload handler
   */
  uploadFileHandler = (files, hasHeaders = true) => {
    this.setState({
      loading: true
    },() => {
      window.setTimeout(() => this.uploadFile(files, hasHeaders), 1000)
    })
  }

  uploadFile = (files, hasHeaders = true) => {
    const fileName = files[0].name
    if (ALLOWED_FILE_FORMATS.indexOf(fileName.match(/\.([^\.]+)$/)[0]) === -1) {
      this.setState({
        fileName: null,
        fileError: "Invalid file type",
        loading: false
      })
      return
    }
    const reader = new FileReader()
    reader.onload = async (e) => {
      // read workbook
      const bstr = e.target.result

      const payload = {}
      payload.bstr = bstr
      payload.hasHeaders = hasHeaders

      const gridData = await readDMExcel(payload)

      this.props.onLoadGridData(gridData && gridData.data)
    }
    reader.readAsBinaryString(files[0])

    this.setState({
      fileName,
      file: files[0],
      fileError: null,
      loading: false
    })
  }

  /**
   * Cancel button click handler
   */
  cancelStepHandler = () => {
    this.props.setTypeOfFileUpload({
      gridData: null,
      interimStorageKey: null,
      inspectData: null,
      validatedData: null,
      uploadedFileType: null,
      userAddress: null,
      mappedColumnObj: null,
      dbColumnHeaders: null,
      nonExistsPickListValue: null,
      nonExistsLeadAdv: null,
      dbLookupColumns: null,
      leadAdviors: null,
      file: null,
    })
    this.props.history.goBack()
  }

  /**
   * Next button click handler
   */
  nextStepHandler = () => {
    this.setState({
      loading: true
    },() => {
      window.setTimeout(() => this.stepHandle(), 1000)
    })
  }

  stepHandle = () => {
    const {file} = this.state
    if (!this.state.fileName) {
      this.showWarningModal()
    } else {
      this.props.setStepIndex(this.state.stepIndex + 1)
      this.props.setFile(file)
    }
  }

  /**
   * Method to show warning modal
   */
  showWarningModal = () => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className='custom-ui'>
          <div style={{ margin: "20px 0px", fontWeight: "500" }} className='body'>Please select a file.</div>
          <button onClick={() => {
            this.setState({
              loading: false
            })
            onClose()
          }}>Ok</button>
        </div>
      )
    })
  }

  /**
   * Component render method
   */
  render() {
    const {loading} = this.state
    return (
      <div className='stepContainer'>
        { loading ? <Loader /> : null }
        <div className="stepBody" style={{marginTop: 0, minHeight: "85%"}}>
          <Accordion multiple
            activeItem={[0]}
            boxHidden
            render={({activeAccordions, onAccordion}) =>
              <div>
                <article className="accordion is-active">
                  <div className="accordion-header">
                    <p onClick={onAccordion || null}>Instruction</p>
                  </div>
                  <div className="accordion-body">
                    <div className="accordion-content">
                     As much as we like to get you started on MuniVisor, we consider this an opportunity to
                     review and rectify your historical data as needed! Before you import your data,
                     you’ll need to prepare your data. This means organizing your data outside of MuniVisor
                     before beginning the import process.
                     Your data is what will help you create meaningful, helpful, and personalized experiences
                     for your business.
                     Please download the attached file to
                      <a
                        href="https://s3.amazonaws.com/munivisor-platform-static-documents/Data_Migration_Helper.pdf"
                        target="_blank" download> Learn more</a>
                     .
                    </div>
                  </div>
                </article>
              </div>
            }/>
          <div className="inputFileContainer" style={{top: "30%"}}>
            <input className="inputFile" type="file" id="my-file" accept={ALLOWED_FILE_FORMATS.join()} onClick={(e) => e.target.value = null} onChange={(e) => this.uploadFileHandler(e.target.files)} />
            <label className="inputFileTrigger" htmlFor="my-file">
              <i className="fa fa-upload" style={{ margin: "0px 5px" }} />Select a file (excel/csv)
            </label>
            <div className="returnFileName" style={{ marginTop: "20px" }}>
              {this.state.fileName ? <div>Selected file: {this.state.fileName}</div> : !this.state.fileError ? <i>No file chosen</i> : null}
              {this.state.fileError ? <div style={{ color: "red" }}>{this.state.fileError}</div> : null}
            </div>
          </div>
        </div>
        <div className='stepFooter'>
          <button className="button is-pulled-left" onClick={this.cancelStepHandler}>Cancel</button>
          <button className="button is-info is-pulled-right" onClick={this.nextStepHandler}>Next</button>
        </div>
      </div >
    )
  }
}

const mapDispatchToProps = {
  setStepIndex,
  onLoadGridData: addGridData,
  setFile,
  setTypeOfFileUpload
}

export default withRouter(connect(null, mapDispatchToProps)(FileUploader))
