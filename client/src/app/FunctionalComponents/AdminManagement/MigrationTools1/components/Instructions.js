import React from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import Accordion from "../../../../GlobalComponents/Accordion"

import {setStepIndex, setTypeOfFileUpload} from "../../../../StateManagement/actions/AdminManagement/admMigrationTools"

class Instructions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props
    }
  }

  /**
   * Cancel button click handler
   */
  cancelStepHandler = () => {
    this.props.setTypeOfFileUpload({
      gridData: null,
      interimStorageKey: null,
      inspectData: null,
      validatedData: null,
      uploadedFileType: null,
      userAddress: null,
      mappedColumnObj: null,
      dbColumnHeaders: null,
      nonExistsPickListValue: null,
      nonExistsLeadAdv: null,
      dbLookupColumns: null,
      leadAdviors: null,
      file: null,
    })
    this.props.history.goBack()
  }

  /**
   * Next button click handler
   */
  nextStepHandler = () => {
    this.props.setStepIndex(this.state.stepIndex + 1)
  }

  /**
   * Component render method
   */
  render() {
    return (
      <div className='stepContainer'>
        <div className='stepBody' style={{marginTop: 0, minHeight: "85%"}}>

          <Accordion multiple
            activeItem={[0]}
            boxHidden
            render={({ activeAccordions, onAccordion }) =>
              <div>
                <article className="accordion is-active">
                  <div className="accordion-header">
                    <p onClick={onAccordion || null}>Instruction</p>
                  </div>
                  <div className="accordion-body">
                    <div className="accordion-content">
                      As much as we like to get you started on MuniVisor, we consider this an opportunity to review and rectify your historical data as needed! Before you import your data,
                      you’ll need to prepare your data. This means organizing your data outside of MuniVisor before beginning the import process.
                      Your data is what will help you create meaningful, helpful, and personalized experiences for your business.
                      Please download the attached file to
                      <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Data_Migration_Helper.pdf" target="_blank" download> Learn more</a>
                      .
                    </div>
                  </div>
                </article>
              </div>
            }
          />
        </div>
        <div className='stepFooter'>
          <button className="button is-pulled-left" onClick={this.cancelStepHandler}>Cancel</button>
          <button className="button is-info is-pulled-right" onClick={this.nextStepHandler}>Next</button>
        </div>
      </div >
    )
  }
}

const mapDispatchToProps = {
  setStepIndex,
  setTypeOfFileUpload
}

export default withRouter(connect(null, mapDispatchToProps)(Instructions))
