import { emailRegex } from "Validation/common"
import {regexTestsForPhoneFax, regexTextsForZipCode} from "GlobalUtils/helpers"
/**
 * Config object for user data entity
 * It supports below properties,
 *  fieldName: String (mandatory),
 *  displayName: String (mandatory),
 *  mandatory: Boolean (optional),
 *  duplicateCheck: Boolean (optional),
 *  checkDB: String (optional),
 *  regexString: String (optional),
 *  isDriver: Boolean (optional),
 *  dependendsOn: String (optional) fieldName of the driver field, e.g. 'state'
 *  pickListKey: String (Picklist key) e.g. LKUPSTATECITYBYCODE, LKUPUNDERWRITER
 */

export const ColumnMapConfig = [
  {
    fieldName: "userFirstName",
    displayName: "First Name",
    type: "string",
    mandatory: true,
    length: 256
  },
  {
    fieldName: "userMiddleName",
    displayName: "Middle Name",
    type: "string",
    length: 256
  },
  {
    fieldName: "userLastName",
    displayName: "Last Name",
    type: "string",
    length: 256
  },
  {
    fieldName: "emailId",
    displayName: "Primary Email Id",
    type: "string",
    mandatory: true,
    duplicateCheck: true,
    checkDB: true,
    regexString: emailRegex,
    length: 256
  },
  {
    fieldName: "userEmail1",
    displayName: "User Email 1",
    type: "string",
    duplicateCheck: true,
    checkDB: true,
    regexString: emailRegex,
    length: 256
  },
  {
    fieldName: "userEmail2",
    displayName: "User Email 2",
    type: "string",
    duplicateCheck: true,
    checkDB: true,
    regexString: emailRegex,
    length: 256
  },
  {
    fieldName: "userPhone",
    displayName: "Primary Phone Number",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userPhone2",
    displayName: "Phone Number - 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userPhone3",
    displayName: "Phone Number - 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userPhone4",
    displayName: "Phone Number - 3",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userAddressLine1",
    displayName: "Address Line 1",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "userAddressLine2",
    displayName: "Address Line 2",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "userCountry",
    displayName: "Country",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "userState",
    displayName: "State",
    type: "string",
    isDriver: true,
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "userCity",
    displayName: "City",
    dependendsOn: "firmState",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "userZipCode",
    displayName: "Zip Code",
    regExpFunction: (val) => regexTextsForZipCode(val),
    subTitle: "Address",
    length: 256
  },

  {
    fieldName: "firmName",
    displayName: "Firm Name",
    type: "string",
    mandatory: true,
    checkForUnique: true,
    staticLookup: true,
    length: 256
  },
  {
    fieldName: "firmAddressLine1",
    displayName: "Address Line 1",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmAddressLine2",
    displayName: "Address Line 2",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmCountry",
    displayName: "Country",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmState",
    displayName: "State",
    type: "string",
    isDriver: true,
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmCity",
    displayName: "City",
    dependendsOn: "firmState",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmZipCode",
    displayName: "Zip Code",
    regExpFunction: (val) => regexTextsForZipCode(val),
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmWebsite",
    displayName: "Company Website",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmEmailId",
    displayName: "Company Email Id",
    regexString: emailRegex,
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "officeFax",
    displayName: "Company Fax",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone",
    displayName: "Business Primary Phone Number",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone2",
    displayName: "Business Phone Number 1",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone3",
    displayName: "Business Phone Number 2",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone4",
    displayName: "Business Phone Number 3",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone5",
    displayName: "Business Phone Number 4",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2AddressLine1",
    displayName: "Address Line 1",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2AddressLine2",
    displayName: "Address Line 2",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2Country",
    displayName: "Country",
    type: "string",
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2State",
    displayName: "State",
    type: "string",
    isDriver: true,
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2City",
    displayName: "City",
    dependendsOn: "firmState",
    type: "string",
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2ZipCode",
    displayName: "Zip Code",
    regExpFunction: (val) => regexTextsForZipCode(val),
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2Website",
    displayName: "Company Website",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2EmailId",
    displayName: "Company Email Id",
    regexString: emailRegex,
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2officeFax",
    displayName: "Company Fax",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone",
    displayName: "Business Primary Phone Number",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone2",
    displayName: "Business Phone Number 1",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone3",
    displayName: "Business Phone Number 2",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone4",
    displayName: "Business Phone Number 3",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone5",
    displayName: "Business Phone Number 4",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
]

export const UserColumnConfig = [
  {
    fieldName: "userFirstName",
    displayName: "First Name",
    type: "string",
    mandatory: true,
    length: 256
  },
  {
    fieldName: "userMiddleName",
    displayName: "Middle Name",
    type: "string",
    length: 256
  },
  {
    fieldName: "userLastName",
    displayName: "Last Name",
    type: "string",
    length: 256
  },
  {
    fieldName: "emailId",
    displayName: "Primary Email Id",
    mandatory: true,
    type: "string",
    duplicateCheck: true,
    checkDB: true,
    regexString: emailRegex,
    length: 256
  },
  {
    fieldName: "userEmail1",
    displayName: "User Email 1",
    type: "string",
    duplicateCheck: true,
    checkDB: true,
    regexString: emailRegex,
    length: 256
  },
  {
    fieldName: "userEmail2",
    displayName: "User Email 2",
    type: "string",
    duplicateCheck: true,
    checkDB: true,
    regexString: emailRegex,
    length: 256
  },
  {
    fieldName: "firmName",
    displayName: "Firm Name",
    type: "string",
    mandatory: true,
    checkExistsInDB: true,
    length: 256
  },
  {
    fieldName: "userJobTitle",
    displayName: "Job Title",
    type: "string",
    length: 256
  },
  {
    fieldName: "userDepartment",
    displayName: "Department",
    type: "string",
    length: 256
  },
  {
    fieldName: "userPhone",
    displayName: "Primary Phone Number",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userPhone2",
    displayName: "Phone Number - 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userPhone3",
    displayName: "Phone Number - 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "userPhone4",
    displayName: "Phone Number - 3",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "firmAddressLine1",
    displayName: "Address Line 1",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "firmAddressLine2",
    displayName: "Address Line 2",
    subTitle: "Address",
    type: "string",
    length: 256
  },
  {
    fieldName: "firmCountry",
    displayName: "Country",
    subTitle: "Address",
    type: "string",
    length: 256
  },
  {
    fieldName: "firmState",
    displayName: "State",
    isDriver: true,
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "firmCity",
    displayName: "City",
    dependendsOn: "State",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "firmZipCode",
    displayName: "Zip Code",
    regExpFunction: (val) => regexTextsForZipCode(val),
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "firmWebsite",
    displayName: "Company Website",
    type: "string",
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "firmEmailId",
    displayName: "Company Email Id",
    regexString: emailRegex,
    subTitle: "Address",
    length: 256
  },
  {
    fieldName: "officeFax",
    displayName: "Company Fax",
    subTitle: "Address",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone",
    displayName: "Business Primary Phone Number",
    subTitle: "Address",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone2",
    displayName: "Business Phone Number 1",
    subTitle: "Address",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone3",
    displayName: "Business Phone Number 2",
    subTitle: "Address",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone4",
    displayName: "Business Phone Number 3",
    subTitle: "Address",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone5",
    displayName: "Business Phone Number 4",
    subTitle: "Address",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
]

export const EntityColumnConfig = [
  {
    fieldName: "firmName",
    displayName: "Firm Name",
    checkForUnique: true,
    mandatory: true,
    checkDB: true,
    type: "string",
    staticLookup: true,
    length: 256
  },
  {
    fieldName: "firmAddressLine1",
    displayName: "Address Line 1",
    subTitle: "Address 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "firmAddressLine2",
    displayName: "Address Line 2",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmCountry",
    displayName: "Country",
    subTitle: "Address 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "firmState",
    displayName: "State",
    isDriver: true,
    subTitle: "Address 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "firmCity",
    displayName: "City",
    dependendsOn: "State",
    subTitle: "Address 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "firmZipCode",
    displayName: "Zip Code",
    regExpFunction: (val) => regexTextsForZipCode(val),
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmWebsite",
    displayName: "Company Website",
    type: "string",
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "firmEmailId",
    displayName: "Company Email Id",
    checkDB: true,
    regexString: emailRegex,
    subTitle: "Address 1",
    length: 256
  },
  {
    fieldName: "officePhone",
    displayName: "Business Primary Phone Number",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone2",
    displayName: "Business Phone Number 1",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone3",
    displayName: "Business Phone Number 2",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone4",
    displayName: "Business Phone Number 3",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officePhone5",
    displayName: "Business Phone Number 4",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "officeFax",
    displayName: "Company Fax",
    subTitle: "Address 1",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },

  {
    fieldName: "address2AddressLine1",
    displayName: "Address Line 1",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2AddressLine2",
    displayName: "Address Line 2",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2Country",
    displayName: "Country",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2State",
    displayName: "State",
    isDriver: true,
    type: "string",
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2City",
    displayName: "City",
    dependendsOn: "firmState",
    type: "string",
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2ZipCode",
    displayName: "Zip Code",
    regExpFunction: (val) => regexTextsForZipCode(val),
    subTitle: "Address 2",
    length: 256
  },
  {
    fieldName: "address2Website",
    displayName: "Company Website",
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2EmailId",
    displayName: "Company Email Id",
    regexString: emailRegex,
    subTitle: "Address 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "address2officeFax",
    displayName: "Company Fax",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone",
    displayName: "Business Primary Phone Number",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone2",
    displayName: "Business Phone Number 1",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone3",
    displayName: "Business Phone Number 2",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone4",
    displayName: "Business Phone Number 3",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
  {
    fieldName: "address2officePhone5",
    displayName: "Business Phone Number 4",
    subTitle: "Address 2",
    regExpFunction: (val) => regexTestsForPhoneFax(val),
    length: 256
  },
]
