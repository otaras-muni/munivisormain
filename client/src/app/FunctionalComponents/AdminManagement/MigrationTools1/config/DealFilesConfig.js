import { emailRegex } from "Validation/common"
import {regexTestsForEmail} from "GlobalUtils/helpers"
/**
 * Config object for deal file entity
 * It supports below properties,
 *  fieldName: String (mandatory),
 *  displayName: String (mandatory),
 *  mandatory: Boolean (optional),
 *  duplicateCheck: Boolean (optional),
 *  regexString: String (optional),
 *  isDriver: Boolean (optional),
 *  dependendsOn: String (optional) fieldName of the driver field, e.g. 'state'
 *  pickListKey: String (Picklist key) e.g. LKUPSTATECITYBYCODE, LKUPUNDERWRITER
 */

export const ColumnMapConfig = [
  {
    fieldName: "dealIssueTranClientFirmName",
    displayName: "Client Name",
    mandatory: true,
    type: "string",
    length: 256
  },
  {
    fieldName: "issuerAliases",
    displayName: "Client Alias",
    type: "string",
    length: 256
  },
  {
    fieldName: "clientIssuerType",
    displayName: "Client Type",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealIssueTranProjectDescription",
    displayName: "Issue Name",
    mandatory: true,
    regexString: "^(?!\\s*$).+",
    length: 256
  },
  {
    fieldName: "dealIssueTranStatus",
    displayName: "Status",
    pickListKey: "LKUPSWAPTRANSTATUS",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealIssueofferingType",
    displayName: "Offering Type",
    type: "string",
    pickListKey: "LKUPDEALOFFERING",
    length: 256
  },
  {
    fieldName: "dealIssueTranAssignedTo",
    displayName: "Lead Advisor",
    // regexString: "emailRegex",
    regexFunction: (val) => regexTestsForEmail(val),
    type: "string",
    checkDB: true,
    length: 256
  },
  {
    fieldName: "dealIssueBorrowerName",
    displayName: "Borrower Name",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealIssueParAmount",
    displayName: "Issue Size (Par Amount) (USD)",
    regexString: "^[+-]?\\d+(\\.\\d+)?$",
    length: 256
  },
  {
    fieldName: "dealIssueTranPrimarySector",
    displayName: "Primary Sector",
    type: "string",
    pickListKey: "LKUPPRIMARYSECTOR",
    length: 256
  },
  {
    fieldName: "dealIssueTranSecondarySector",
    displayName: "Secondary Sector",
    pickListKey: "SECONDARYSECTOR",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealIssueTranSubType",
    displayName: "Transaction Sub Type",
    pickListKey: "LKUPDEBTTRAN",
    type: "string",
    length: 256
  },
  {
    fieldName: "clientEntityType",
    displayName: "Entity Type",
    pickListKey: "LKUPENTITYTYPE",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealIssuePricingDate",
    displayName: "Pricing Date",
    regexString: "^(0[1-9]|1[0-2])\/(0[1-9]|1\\d|2\\d|3[01])\/\\d{4}$",
    length: 256
  },
  {
    fieldName: "dealIssueActAwardDate",
    displayName: "Closing Date",
    regexString: "^(0[1-9]|1[0-2])\/(0[1-9]|1\\d|2\\d|3[01])\/\\d{4}$",
    length: 256
  },
  {
    fieldName: "termsOfIssue",
    displayName: "Term of Issue",
    regexString: "^[+-]?\\d+(\\.\\d+)?$",
    length: 256
  },
  {
    fieldName: "dealIssueSecurityType",
    displayName: "Security Type",
    type: "string",
    pickListKey: "LKUPSECTYPEGENERAL",
    length: 256
  },
  {
    fieldName: "dealIssueTranState",
    displayName: "State",
    isDriver: true,
    type: "string",
    pickListKey: "LKUPCOUNTRY",
    length: 256
  },
  /* {
    fieldName: "dealIssueTranFirmLeadAdvisorName",
    displayName: "Lead Banker",
    length: 256
  }, */
  {
    fieldName: "dealIssueUseOfProceeds",
    displayName: "Use of Proceeds",
    type: "string",
    pickListKey: "LKUPUSEOFPROCEED",
    length: 256,
  },
  /* {
    fieldName: "seriesName",
    displayName: "Series Name - 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealSeriesPrincipal",
    displayName: "Series Principal  - 1",
    regexString: "^[+-]?\\d+(\\.\\d+)?$",
    length: 256
  },
  {
    fieldName: "dealSeriesSecurityType",
    displayName: "Series Security Type - 1",
    type: "string",
    pickListKey: "LKUPSECTYPEGENERAL",
    length: 256
  },
  {
    fieldName: "dealRatingSP",
    displayName: "Series Rating - S & P - 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealRatingFitch",
    displayName: "Series Rating - Fitch - 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealRatingKroll",
    displayName: "Series Rating - Kroll - 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealRatingMoodys",
    displayName: "Series Rating -  Moodys - 1",
    type: "string",
    length: 256
  },
  {
    fieldName: "seriesName2",
    displayName: "Series Name - 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealSeriesPrincipal2",
    displayName: "Series Principal  - 2",
    regexString: "^[+-]?\\d+(\\.\\d+)?$",
    length: 256
  },
  {
    fieldName: "dealSeriesSecurityType2",
    displayName: "Series Security Type - 2",
    type: "string",
    pickListKey: "LKUPSECTYPEGENERAL",
    length: 256
  },
  {
    fieldName: "dealRatingSP2",
    displayName: "Series Rating - S & P - 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealRatingFitch2",
    displayName: "Series Rating - Fitch - 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealRatingKroll2",
    displayName: "Series Rating - Kroll - 2",
    type: "string",
    length: 256
  },
  {
    fieldName: "dealRatingMoodys2",
    displayName: "Series Rating -  Moodys - 2",
    type: "string",
    length: 256
  }, */
  {
    fieldName: "tic",
    displayName: "TIC %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },{
    fieldName: "bbri",
    displayName: "BBRI %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },
  {
    fieldName: "nic",
    displayName: "NIC %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },{
    fieldName: "gsp",
    displayName: "G/Sp %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },
  {
    fieldName: "mgtf",
    displayName: "MgtF %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },{
    fieldName: "exp",
    displayName: "Exp %",
    // regexString: "^[+-]?\\d+(\\.\\d+)?$",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },
  {
    fieldName: "tkdn",
    displayName: "Tkdn %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  },
  {
    fieldName: "other",
    displayName: "Other %",
    regexString: "^(\\d*\\.)?\\d+$",
    length: 256
  }
]
