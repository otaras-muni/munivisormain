import React from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { ContextType } from "GlobalUtils/consts.js"
import Instructions from "../components/Instructions"
import FileUploader from "../components/FileUploader"
import DataGrid from "../components/DataGrid"
import MapColumns from "../components/MapColumns"
import Summary from "../components/Summary"
import { ColumnMapConfig } from "../config/DealFilesConfig"
import { addGridData, setStepIndex, setInterimStorageKey } from "../../../../StateManagement/actions/AdminManagement/admMigrationTools"

import "react-confirm-alert/src/react-confirm-alert.css"

const bucketName = process.env.S3BUCKET

class DealFiles extends React.Component {

  constructor(props) {
    super(props)
    this.gridData = null
    const configProps = {
      dbColumnHeaders: ColumnMapConfig,
      bucketName,
      contextId: this.props.user.userId,
      contextType: ContextType.migrationTools,
      tenantId: this.props.user.entityId
    }
    this.state = {
      isSideBarOpen: true,
      steps: [
        /* {
          label: "Instructions",
          body: React.createElement(Instructions, { stepIndex: 0, nav2: "deal-files" })
        }, */
        {
          label: "Instructions And Upload file",
          body: React.createElement(FileUploader, { stepIndex: 0, nav2: "deal-files" })
        },
        {
          label: "Map columns",
          body: React.createElement(MapColumns, { stepIndex: 1, ...configProps, nav2: "deal-files" })
        },
        {
          label: "State Mapping And Proceed With Valid data",
          body: React.createElement(DataGrid, { stepIndex: 2, ...configProps, nav2: "deal-files" })
        },
        {
          label: "Server Response",
          body: React.createElement(Summary, { stepIndex: 3, ...configProps, nav2: "deal-files" })
        }
      ]
    }
  }

  async componentWillMount() {
    this.props.setStepIndex(0)
  }

  /**
   * Sidebar toggle handler
   */
  handleSideBarToggle = () => {
    this.setState({
      isSideBarOpen: !this.state.isSideBarOpen,
    })
  }

  /**
   * Component render method
   */
  render() {
    const { isSideBarOpen } = this.state
    const iconClass = isSideBarOpen ? "left" : "right"
    return (
      <React.Fragment>
        <div className="column stepWizardWrapper">
          <div className={`column is-${isSideBarOpen ? "2" : "1"}`} style={{ display: "inline-block", borderRight: "1px solid #d4cccc" }}>
            <div style={{ textAlign: "right" }}>
              <span
                className={`fas fa-lg fa-angle-double-${iconClass}`}
                style={{ cursor: "pointer" }}
                onClick={this.handleSideBarToggle}
              />
            </div>
            <ul>
              {this.state.steps.map((step, index) => (
                <li key={index.toString()} className={index === this.props.selectedStepIndex ? "stepLabelSelected" : "stepLabel"}>{isSideBarOpen ? `Step ${index + 1}: ${step.label}` : `Step ${index + 1}`}</li>
              ))}
            </ul>
          </div>
          <div className={`column is-${isSideBarOpen ? "10" : "11"}`} style={{ display: "flex", flexDirection: "column" }}>
            <div style={{ flex: "1", overflow: "auto" }}>
              {this.state.steps[this.props.selectedStepIndex] ? this.state.steps[this.props.selectedStepIndex].body : null}
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  gridData: state.admMigrationToolsReducer.gridData,
  selectedStepIndex: state.admMigrationToolsReducer.selectedStepIndex,
  user: (state.auth && state.auth.userEntities) || {}
})

const mapDispatchToProps = {
  setGridData: addGridData,
  setStepIndex,
  setInterimStorageKey
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps, null)(DealFiles))
