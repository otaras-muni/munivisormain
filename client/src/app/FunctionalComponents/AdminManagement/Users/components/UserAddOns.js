import React from "react"

const UserAddOns = (props) => (
  <article className="accordion is-active">
    <div className="accordion-header">
      <p>Users Level</p>
    </div>
    <div className={props.viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
      <div className="accordion-content">

        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl " title="">Add-On Name</p>
              </th>
              <th>
                <p className="multiExpLbl " title="">Enabled</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {
              props.usersAddOnsList.map((item, idx) => (
                <tr key={idx}>
                  <td>
                    {item}
                  </td>
                  <td>
                    <p className="emmaTablesTd">
                      <input
                        type="checkbox"
                        name="addOns"
                        value={item}
                        onChange={
                          (event)=>{
                            event.target.name = "addOns"
                            event.target.value = item
                            props.toggleCheckbox(event)
                          }}
                        checked={!!props.userAddOns.includes(item)}
                      />
                    </p>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

      </div>
    </div>
  </article>
)

export default UserAddOns
