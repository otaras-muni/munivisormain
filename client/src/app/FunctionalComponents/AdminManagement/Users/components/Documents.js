import React, { Component } from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import { withRouter } from "react-router-dom"
import DocumentPage from "../../../../GlobalComponents/DocumentPage"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import {
  postUserDocument,
  pullUserDocument,
  putUserDocumentStatus
} from "../../../../StateManagement/actions/AdminManagement/admTrnActions"

class Document extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      documentsList: [],
    }
  }

  async componentWillMount() {
    const { userDetails } = this.props
    this.setState({
      documentsList: (userDetails && userDetails.userDocuments) || [],
      loading: false,
    })
  }

  onDocSave = async (docs, callback) => {
    console.log(docs)
    const { userId } = this.props

    if (userId) {
      const res = await postUserDocument(userId, docs)
      if (res && res.status === 200) {
        toast("Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.userDocuments) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    }
  }

  onStatusChange = async (e, doc, callback) => {
    const { userId } = this.props
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        [name]: value
      }
      type = "status"
    } else {
      document = {
        ...doc
      }
      type = "meta"
    }

    if (userId) {
      const res = await putUserDocumentStatus(userId, `?type=${type}`, { _id: doc._id, ...document })
      if (res && res.status === 200) {
        toast("Document has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        if (name) {
          callback({
            status: true,
          })
        } else {
          callback({
            status: true,
            documentsList: (res.data && res.data.userDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    }
  }

  onDeleteDoc = async (documentId, callback) => {
    const { userId } = this.props

    if (userId) {
      const res = await pullUserDocument(userId, documentId)
      if (res && res.status === 200) {
        toast("Document removed successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.userDocuments) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    }
  }

  render() {
    const { documentsList } = this.state
    const { userId } = this.props
    const loading = () => <Loader />
    /* const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Supervisory & Compliance Obligations",
    } */
    return (
      <div>
        {this.state.loading ? loading() : null}
        <DocumentPage {...this.props} isNotTransaction onSave={this.onDocSave} title="Documents" pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS"
          onStatusChange={this.onStatusChange} pickAction="LKUPCOMPLIANCEDOCACTION" pickType="LKUPSACODOCTYPE" documents={documentsList} category="userDocuments" contextType={ContextType.user}
          tranId={userId} onDeleteDoc={this.onDeleteDoc} activeItem={[]} />

      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Document)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
