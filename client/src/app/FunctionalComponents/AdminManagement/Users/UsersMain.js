import React from "react"
import { Redirect } from "react-router"
import UsersView from "./UsersView"
import UsersList from "./containers/UsersListContainer"

const UsersMain = props => {
  const { nav2, nav3 } = props
  if (nav2) {
    return (
      <UsersView userId={nav2} navComponent={nav3} />
    )
  }
  return (
    <UsersList />
  )
}

export default  UsersMain
