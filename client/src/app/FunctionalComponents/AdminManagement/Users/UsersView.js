import React from "react"
import { NavLink, Redirect } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
// import UsersContainers from "./../Users/containers/UsersContainers"
import UsersNew from "../../Users/Users"
import UsersAddOns from "./containers/UsersAddOnsContainer"

const TABS = [
  { path: "users", label: "Users Details" },
  // { path: "add-ons", label: "Add Ons" }
]
const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}
const UsersView = props => {
  let { navComponent } = props
  navComponent = navComponent || "details"
  const {userId} = props
  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink key={t.path} to={`/admin-users/${id}/${t.path}`} activeStyle={activeStyle}> {t.label} </NavLink>
      </li>
    ))
  const renderViewSelection = (userId, navComponent) => (
    <nav className="tabs is-boxed">
      <ul>
        {renderTabs(TABS,navComponent, userId)}
      </ul>
    </nav>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(userId, navComponent)}
          </div>
        </div>
      </div>
      <Switch condition={navComponent}>
        <Case value="users">
          <div id="main">
            <UsersNew nav2 = {userId} />
          </div>
        </Case>
        <Case value="add-ons">
          <UsersAddOns options={navComponent} nav2 = {userId} userId = {userId} />
        </Case>
        <Default>
          <Redirect to="/admin-users"/>
        </Default>
      </Switch>
    </div>
  )
}

export default UsersView
