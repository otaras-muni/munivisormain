import React, { Component } from "react"
import { toast } from "react-toastify"
import Switch, { Case, Default } from "react-switch-case"
import {
  getFirmById,
  entityUsersList
} from "AppState/actions/AdminManagement/admTrnActions"
import { withRouter, Link } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "../../../../GlobalComponents/Loader"
import "../../scss/entity.scss"
import UsersContainer from "./UsersContainers"
import UsersNew from "../../../Users/Users"
import EntityPageFilter from "../../../EntityManagement/CommonComponents/EntityPageFilter"

class UserstListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firmName: "",
      filterUsersList: [],
      searchString: "",
      userList: [],
      viewList: "",
      userId: "",
      entityId: "",
      search: ""
    }
  }

  componentWillMount() {
    this.setState({
      loading: true
    })
  }

  async componentDidMount() {
    const { entityId } = this.props.auth.userEntities
    if (entityId !== "" && entityId.length === 24) {
      let firmDetailById = getFirmById(entityId)
      firmDetailById = await firmDetailById
      if (firmDetailById !== null && !firmDetailById.error) {
        this.setState({
          firmName: firmDetailById.firmName,
          entityId
        })
      } else {
        toast("Entity Not Found!", { autoClose: 2000, type: toast.TYPE.ERROR })
      }
    }
    this.setState({
      loading: false
    })
  }

  addNewUsers = () => {
    this.setState({
      viewList: "addNewUsers"
    })
  }

  render() {
    const loading = () => <Loader />
    return (
      <div className="users-list-container">
        {this.state.loading ? loading() : null}
        <Switch condition={this.state.viewList}>
          <Case value="viewUsers">
            <UsersContainer
              userId={this.state.userId}
              entityId={this.state.entityId}
            />
          </Case>
          <Case value="addNewUsers">
            <UsersNew
              entityId={this.state.entityId}
            />
          </Case>
          <Default>
            <EntityPageFilter
              listType="users"
              auth={this.props.auth}
              entityId={this.props.auth.userEntities.entityId || ""}
              nav1={this.props.nav1 || ""}
              searchPref="adm-users"
              title="Users"
              routeType="adminUsers"
              addNewUsers={this.addNewUsers}
            />
          </Default>
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserstListContainer)
)
