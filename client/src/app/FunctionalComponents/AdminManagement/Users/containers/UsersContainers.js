import React from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import { DropdownList } from "react-widgets"
import { saveUserDetail, getFirmUserById, checkPlatFormAdmin, getUserDetailById, getAllUserFirmList, findContactByName, searchUsersList, updateUserDetailById, addContactDetail, sendEmailAlert } from "AppState/actions/AdminManagement/admTrnActions"
import { validateUserDetail } from "Validation/userDetail"
import { convertError, getPicklistValues, checkEmptyElObject } from "GlobalUtils/helpers"
import { withRouter } from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import UserAddress from "../../CommonComponents/UserAddress"
import UserContact from "../../CommonComponents/UserContact"
import ContactType from "../../CommonComponents/ContactType"
import ListAddresses from "../../CommonComponents/UserAddressList"
import Loader from "../../../../GlobalComponents/Loader"
import Documents from "../components/Documents"
import FirmAddressListForm from "../../../EntityManagement/CommonComponents/FirmAddressListForm"

const intialError = (errorUserDetail, userDetails, initialUserDetails) => {
  const arr = ["userEmails", "userPhone", "userFax"]
  arr.map(item => {
    if (userDetails[item].length > 0) {
      for (let index = 1; index < (userDetails[item].length); index++) {
        errorUserDetail[item].push(initialUserDetails[item][0])
      }
    }
  })
}
class UsersContainers extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.loading = true
    this.state.loadingPickLists = true
    this.state.errorUserDetail = cloneDeep(this.initialState().userDetails)
    this.cleanErrorUserDetail = cloneDeep(this.initialState().userDetails)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
  }
  async componentWillMount() {
    //[countryResult, userRole, userFlags, userEntitlement]
    let firmList = getAllUserFirmList()
    let isPlatFormAdmin = checkPlatFormAdmin()

    const { userEntities } = this.props.auth
    userEntities.isMuniVisorClient = true
    const { searchFirm, userDetails } = this.state
    let usersList = []
    let filterUsersList = []
    firmList = await firmList
    firmList = firmList.map(item => ({ value: item._id, label: item.firmName }))
    searchFirm.firmDropdown = [{ "label": "Select Firm", "value": "" }, ...firmList]
    isPlatFormAdmin = await isPlatFormAdmin
    if (userEntities.isMuniVisorClient && !isPlatFormAdmin) {
      searchFirm.firmValue = { id: userEntities.entityId, label: userEntities.firmName }
      userDetails.entityId = userEntities.entityId
      userDetails.userFirmName = userEntities.firmName
      usersList = await searchUsersList(userEntities.entityId, "")
      filterUsersList = usersList.map((item, idx) => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))
    }
    this.setState((prevState) => ({
      ...prevState,
      ...{
        userEntities,
        searchFirm,
        usersList,
        filterUsersList,
        firmDisable: !!userEntities.isMuniVisorClient,
        isPlatFormAdmin,
        loading: false
      }
    }))
  }
  async componentDidMount() {
    let pickList = getPicklistValues(["LKUPCOUNTRY", "LKUPUSERENTITLEMENT", "LKUPUSERFLAG", "LKUPUSERENTITLEMENT"])

    if (this.props.userId) {
      this.props.getUserDetailById(this.props.userId)
    }
    let [countryResult, userRole, userFlags, userEntitlement] = await pickList
    this.setState((prevState) => ({
      ...prevState,
      ...{
        countryResult,
        userRole: userRole[1],
        userEntitlement: userEntitlement[1],
        userFlags: userFlags[1],
        loadingPickLists: false
      }
    }))
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.admUserDetail.userDetails &&
      Object.keys(nextProps.admUserDetail.userDetails).length > 0 &&
      !nextProps.admUserDetail.updated
    ) {
      const { searchFirm, searchUsers, usersList } = this.state
      let { errorUserDetail, filterUsersList } = this.state
      const { firmDropdown } = searchFirm
      const { userDetails, primaryEmails } = cloneDeep(nextProps.admUserDetail)
      const addressList = cloneDeep(nextProps.admUserDetail.userDetails.userAddresses)
      userDetails.userAddresses = this.initialState().userDetails.userAddresses
      // if(firmDropdown){
      // const firmName = firmDropdown.find(item => item.value === userDetails.entityId)
      // userDetails.userFirmName = firmName && firmName.label ? firmName.label : ""
      errorUserDetail = cloneDeep(this.initialState().userDetails)
      intialError(errorUserDetail, userDetails, this.initialState().userDetails)
      searchUsers.usersValue = `${userDetails.userFirstName} ${userDetails.userLastName}`
      // searchFirm.firmValue = firmName
      filterUsersList = usersList.filter(user => user.entityId === userDetails.entityId)
      filterUsersList = filterUsersList.map(item => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))
      this.setState(prevState => ({
        ...prevState,
        userDetails,
        addressList,
        searchFirm,
        searchUsers,
        filterUsersList,
        errorUserDetail,
        loading: false,
        oldUsersDetails: cloneDeep(nextProps.admUserDetail.userDetails),
        addUsers: true,
        primaryEmails,
        isPrimaryAddress: ""
      }))
      // }
      // this.props.getFirmName(userDetails.userFirmName)
      this.submitted = false
    }
  }
  componentWillUpdate(nextState, nextProps) { }
  /** ************************ this is for onchange ********************************* */
  onChangeUserDetail = (e, key, deepFields) => {
    // e.preventDefault()
    const { userDetails, errorUserDetail } = this.state
    const { userAddresses } = userDetails
    let errorKeyUserDetail
    let validator
    try {
      if (typeof key !== "undefined") {
        const value = e.target.type === "checkbox" && (e.target.name === "zip2" || e.target.name === "zip1")
          ? !userDetails.userAddresses[key][e.target.name]
          : e.target.value
        if (e.target.name === "zip1" || e.target.name === "zip2") {
          userDetails.userAddresses[key].zipCode[e.target.name] = value
          errorKeyUserDetail = errorUserDetail.userAddresses[key].zipCode
          validator = {
            userAddresses: [
              {
                zipCode: {
                  [e.target.name]: e.target.value
                }
              }
            ]
          }
        } else {
          userDetails.userAddresses[key][e.target.name] = value
          errorKeyUserDetail = errorUserDetail.userAddresses[key]
          validator = {
            userAddresses: [
              {
                [e.target.name]: e.target.value
              }
            ]
          }
        }
      } else {
        if (e.target.name === "userFlags") {
          this.toggleCheckbox(e.target.value)
          validator = {
            [e.target.name]: userDetails[e.target.name]
          }
        } else {
          userDetails[e.target.name] = e.target.value
          validator = {
            [e.target.name]: e.target.value
          }
        }
        errorKeyUserDetail = errorUserDetail
      }
      if (deepFields) {
        deepFields.forEach(d => {
          userDetails.userAddresses[key][d] = ""
        })
      }
      this.setState({
        userDetails
      })
      if (this.submitted) {
        const errorData = validateUserDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          if (err.context.key === e.target.name) {
            errorKeyUserDetail[e.target.name] = `${err.context.label} Required.`
            this.setState({ errorUserDetail })
          } else {
            errorKeyUserDetail[e.target.name] = ""
            this.setState({ errorUserDetail })
          }
        } else {
          errorKeyUserDetail[e.target.name] = ""
          this.setState({ errorUserDetail })
        }
      }
    } catch (ex) {
      console.log("Ex<<<<=========>>>", errorKeyUserDetail, ex)
    }
  }
  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, idx) => {
    // e.preventDefault();
    const { userDetails, errorUserDetail } = this.state
    if (e.target.type === "checkbox") {
      userDetails[type].forEach((item, index) => {
        if (index === idx) {
          userDetails[type][idx][e.target.name] = true
        } else
          userDetails[type][index][e.target.name] = false
      })
    } else {
      userDetails[type][idx][e.target.name] = e.target.value
    }
    const validator = {
      [type]: [
        {
          [e.target.name]: e.target.value
        }
      ]
    }
    this.setState({ userDetails })

    if (this.submitted) {
      const errorData = validateUserDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorUserDetail[type][idx][e.target.name] = `${err.context.label} Required.`
        this.setState({ errorUserDetail })
      } else {
        errorUserDetail[type][idx][e.target.name] = ""
        this.setState({ errorUserDetail })
      }
    }
  }

  /** ***************************Function is used for searching firms******************* */
  onSearchFirms = async (item) => {
    let { filterUsersList, usersList, errorUserDetail, searchUsers, updateUsers, addUsers } = this.state
    const { userDetails } = this.initialState()
    if (item.value !== "") {
      userDetails.userFirmName = item.label
      userDetails.entityId = item.value
      usersList = await searchUsersList(item.value, "")
      filterUsersList = usersList.map(item => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))
      if (this.submitted) {
        errorUserDetail.userFirmName = ""
        errorUserDetail.entityId = ""
      }
    } else {
      filterUsersList = []
      if (this.submitted && !updateUsers) {
        errorUserDetail.userFirmName = "firm required"
        errorUserDetail.entityId = "firm required"
      }
    }
    this.setState(prevState => ({
      searchFirm: {
        ...prevState.searchFirm,
        firmValue: item
      },
      userDetails,
      usersList,
      filterUsersList,
      searchUsers: {
        usersValue: ""
      },
      addressList: [],
      oldUsersDetails: {},
      errorUserDetail,
      addUsers: !updateUsers
    }))
  }
  onSearchUsers = item => {
    this.setState({
      loading: true
    })
    if (item.value !== "") {
      this.props.getUserDetailById(item.value)
    }
  }
  getUsersList = async input => {
    const { userEntities } = this.props.auth
    let result, response
    if (!input) {
      return Promise.resolve({ options: [] })
    }
    if (userEntities.isMuniVisorClient) {
      result = await searchUsersList(userEntities.entityId, input)
      response = result.map((item, idx) => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))
    } else {
      result = await findContactByName({ input })
      response = result.map((item, idx) => ({ value: item.id, label: item.fullName }))
    }
    return { options: response }
  }
  /** ************************ this is for onchange msrb firmname ********************************* */
  getUserDetails = e => {
    if (e.target.value !== "") {
      this
        .props
        .getUserDetailById(e.target.value)
    } else {
      this.setState(prevState => ({
        ...prevState,
        userDetails: this
          .initialState()
          .userDetails,
        addressList: [],
        errorUserDetail: this.cleanErrorUserDetail
      }))
    }
  }
  test = (props) => {
    Object
      .keys(props)
      .forEach(item => {
      })
  }
  initialState = () => ({
    userDetails: {
      entityId: "",
      userFlags: [], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
      userRole: "", // [ Admin, ReadOnly, Backup ]
      userEntitlement: "", // [ Global, Transaction ]
      userFirmName: "",
      userFirstName: "",
      userMiddleName: "",
      userLastName: "",
      userEmails: [
        {
          emailId: "",
          emailPrimary: true
        }
      ],
      userPhone: [
        {
          phoneNumber: "",
          extension: "",
          phonePrimary: true
        }
      ],
      userFax: [
        {
          faxNumber: "",
          faxPrimary: true
        }
      ],
      userEmployeeID: "",
      userExitDate: "",
      userJobTitle: "",
      userManagerEmail: "",
      userJoiningDate: "",
      userCostCenter: "",
      userAddresses: [{
        addressName: "",
        addressType: "",
        isPrimary: false,
        isActive: true,
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: {
          zip1: "",
          zip2: ""
        },
        formatted_address: "",
        url: "",
        location: {
          longitude: "",
          latitude: ""
        }
      }]
    },
    addressList: [],
    countryResult: [],
    searchFirm: {
      firmDropdown: [],
      firmValue: ""
    },
    searchUserState: {
      backspaceRemoves: false,
      firmValue: ""
    },
    firmDisable: false,
    searchUsers: {
      usersDropdown: [],
      usersValue: ""
    },
    usersList: [],
    filterUsersList: [],
    userRole: [],
    userFlags: [],
    userEntitlement: [],
    expanded: {
      business: false,
      addresslist: false,
      contactdetail: true,
      contacttype: true
    },
    isPrimaryAddress: "",
    isPrimaryEmailAddress: "",
    isPrimaryUserPhone: "",
    oldUsersDetails: {},
    canIsPrimary: false,
    updateUsers: false,
    addUsers: true,
    primaryEmails: {
      isPrimary: false,
      emailId: ""
    }
  })

  onSaveAddress = addressList => {
    this.setState(
      prevState => ({
        ...prevState,
        firmDetails: {
          ...prevState.firmDetails,
          addresses: addressList
        },
        addressList
      }),
      () => this.handleSubmit()
    )
  }

  async handleSubmit(event) {
    event && event.preventDefault()
    this.submitted = true
    const isPrimaryAddress = false
    const { userDetails, errorUserDetail } = this.state
    const { userEntities } = this.props.auth
    try {
      const userDetailData = {
        _id: userDetails._id ? userDetails._id : "",
        entityId: userDetails.entityId,
        userFlags: userDetails.userFlags,
        userRole: userDetails.userEntitlement,
        userEntitlement: userDetails.userEntitlement,
        userFirmName: userDetails.userFirmName,
        userFirstName: userDetails.userFirstName,
        userMiddleName: userDetails.userMiddleName,
        userLastName: userDetails.userLastName,
        userEmails: userDetails.userEmails,
        userPhone: userDetails.userPhone,
        userFax: userDetails.userFax,
        userEmployeeID: userDetails.userEmployeeID,
        userExitDate: userDetails.userExitDate,
        userJobTitle: userDetails.userJobTitle,
        userManagerEmail: userDetails.userManagerEmail,
        userJoiningDate: userDetails.userJoiningDate,
        userCostCenter: userDetails.userCostCenter,
        userAddresses: userDetails.userAddresses
      }
      if (userDetailData._id !== "") {
        let sortedData = []
        const { userAddresses } = userDetailData
        userAddresses.forEach(address => {
          if (!checkEmptyElObject(address)) {
            sortedData.push(address)
          }
        })
        userDetailData.userAddresses = sortedData
        let { addressList } = this.state
        if (sortedData.length > 0) {
          sortedData = sortedData.sort((a, b) => a._id > b._id)
          addressList = addressList.length > 0 ? addressList.filter((obj) => obj._id != sortedData[0]._id) : []
        }
        const mergeAddresses = [...addressList, ...sortedData]
        const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
        const errorData = validateUserDetail(userDetailData)
        if (errorData.error || filteredAddresses.length !== 1) {
          if (errorData.error) {
            convertError(errorUserDetail, errorData)
          }
          if (filteredAddresses.length === 0)
            // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
            toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
          else if (filteredAddresses.length > 1)
            // this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
            toast("Only One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
          else
            this.state.isPrimaryAddress = ""
          this.setState({ errorUserDetail })
        } else {
          this.setState({
            loading: true
          })
          const result = await updateUserDetailById(userDetailData, mergeAddresses)
          if (!result.error && result.isPrimaryAddress) {
            this.submitted = false
            toast("Users Contact has been Updated!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
            this.props.getUserDetailById(userDetailData._id)
          } else {
            this.setState({ loading: false })
            if (result.error)
              convertError(errorUserDetail, result)
            if (!result.isPrimaryAddress) {
              // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
              toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
              this.setState({ errorUserDetail, loading: false })
            } else {
              this.state.isPrimaryAddress = ""
              this.setState({ errorUserDetail, loading: false })
            }
          }
        }
      } else {
        const { userAddresses, userEmails, userPhone } = userDetailData
        const primaryAddress = userAddresses.filter(item => item.isPrimary)
        const primaryEmails = userEmails.filter(email => email.emailPrimary)
        const primaryUserPhone = userPhone.filter(phone => phone.phonePrimary)
        let isPrimaryAddress = true
        if (primaryAddress.length !== 1)
          isPrimaryAddress = false
        const errorData = validateUserDetail(userDetailData)
        if (errorData.error || !isPrimaryAddress || primaryEmails.length !== 1 || primaryUserPhone.length !== 1) {
          if (errorData.error) {
            convertError(errorUserDetail, errorData)
          }
          if (!isPrimaryAddress)
            // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
            toast("One Primary Address  is Required !", { autoClose: 3000, type: toast.TYPE.ERROR })
          if (primaryEmails.length !== 1)
            this.state.isPrimaryEmailAddress = "One Primary email is required."
          if (primaryUserPhone.length !== 1)
            this.state.isPrimaryUserPhone = "One Primary phone is required."
          this.setState({ errorUserDetail })
        } else {
          this.setState({
            loading: true
          })
          const response = await addContactDetail(userDetailData)
          if (!response.error && response.isPrimaryAddress && !response.duplicateExists) {
            const { usersList, searchFirm, searchUsers, filterUsersList } = this.state
            searchUsers.usersValue = ""
            const userDetails = cloneDeep(this.initialState().userDetails)
            userDetails.entityId = userEntities.entityId
            userDetails.userFirmName = userEntities.firmName
            usersList.push({ entityId: response.success.entityId, firmName: response.success.userFirmName, firstName: response.success.userFirstName, lastName: response.success.userLastName, userId: response.success._id })
            filterUsersList.push({ value: response.success._id, label: `${response.success.userFirstName} ${response.success.userLastName}` })
            this.setState({
              loading: false
            })
            this.setState({
              userDetails,
              errorUserDetail: cloneDeep(this.initialState().userDetails),
              loading: false,
              searchFirm,
              searchUsers,
              usersList,
              filterUsersList,
              oldUsersDetails: {}
            })
            this.submitted = false
            sendEmailAlert(response.success._id)
            this.props.history.push("/admin-users")
            toast("Users Contact has been Created!", { autoClose: 2000, type: toast.TYPE.SUCCESS })
          } else {
            if (response.duplicateExists && response.duplError.length > 0) {
              response.duplError.forEach(email => {
                errorUserDetail.userEmails[email.value].emailId = email.label
              })
            }
            if (response.error !== "" && typeof response.error === "string")
              toast(response.error, { autoClose: 2000, type: toast.TYPE.ERROR })
            if (typeof response.error === "object" && !response.error !== null)
              convertError(errorUserDetail, response)
            if (typeof response.isPrimaryAddress !== "undefined" && !response.isPrimaryAddress) {
              // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
              toast("One Primary Address  is Required !", { autoClose: 3000, type: toast.TYPE.ERROR })
            } else
              this.state.isPrimaryAddress = ""
            this.setState({ errorUserDetail, loading: false })
          }
        }
      }
    } catch (ex) {
      console.log("=========>>>", ex)
    }
  }
  /** ************************ this is for add new address ********************************* */
  addNewUserAddress = (event) => {
    event.preventDefault()
    const { userDetails } = this.state
    const { userAddresses } = userDetails
    let isEmpty = false
    userAddresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true

      }
    })
    if (!isEmpty) {
      this.setState(prevState => {
        const userDetails = {
          ...prevState.userDetails
        }
        const errorUserDetail = {
          ...prevState.errorUserDetail
        }
        userDetails.userAddresses
          .push(this.initialState().userDetails.userAddresses[0])
        errorUserDetail.userAddresses
          .push(this.initialState().userDetails.userAddresses[0])
        return { userDetails, errorUserDetail }
      })
    } else {
      toast("Already and empty address!", { autoClose: 2000, type: toast.TYPE.INFO })
    }
  }
  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = label => {
    const { userFlags } = this.state.userDetails
    if (userFlags.includes(label)) {
      userFlags.splice(userFlags.indexOf(label), 1)
    } else {
      userFlags.push(label)
    }
  }

  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type) => {
    e.preventDefault()
    this.setState(prevState => {
      const addmore = this.initialState().userDetails[type][0]
      switch (type) {
        case "userEmails":
          addmore.emailPrimary = false
          break
        case "userPhone":
          addmore.phonePrimary = false
          break
        default:
          addmore.faxPrimary = false
          break
      }
      const addMoreData = {
        ...prevState.userDetails
      }
      const errorUserDetail = {
        ...prevState.errorUserDetail
      }
      addMoreData[type].push(addmore)
      errorUserDetail[type].push(this.initialState().userDetails[type][0])
      return { addMoreData }
    })
  }
  /** ********************************************************************************** */
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this.state.addressList.find(item => item._id == id)
    this.setState(prevState => {
      const { errorUserDetail, userDetails, expanded } = prevState
      expanded.business = true
      errorUserDetail.userAddresses = this.cleanErrorUserDetail.userAddresses
      userDetails.userAddresses = [address]
      return { userDetails, errorUserDetail, expanded }
    })
  }
  /** ******************************************************************************** */
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map((item) => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }
  deleteAliases = (type, idx) => {
    const { userDetails, errorUserDetail } = this.state
    let isPrimary = true
    let primaryKey = ""
    let message = ""
    switch (type) {
      case "userEmails":
        primaryKey = "emailPrimary"
        message = "Primary Email"
        break
      case "userPhone":
        primaryKey = "phonePrimary"
        message = "Primary Phone"
        break
      default:
        break
    }
    if (userDetails[type][idx][primaryKey]) {
      isPrimary = false
    }

    if (type === "userFax") {
      userDetails[type].splice(idx, 1)
      errorUserDetail[type].splice(idx, 1)
      this.setState({
        userDetails,
        errorUserDetail
      })
    }

    if (isPrimary) {
      userDetails[type].splice(idx, 1)
      errorUserDetail[type].splice(idx, 1)
      this.setState({
        userDetails,
        errorUserDetail
      })
    } else {
      toast(` ${message} can not be Deleted`, { autoClose: 2000, type: toast.TYPE.INFO })
    }
  }
  /** ************************ this is for reset form ********************************* */
  resetUserAddress = e => {
    // e.preventDefault()
    const userAddresses = this.initialState().userDetails.userAddresses[0]
    this.state.isPrimaryAddress = ""
    this.setState(prevState => ({
      userDetails: {
        ...prevState.userDetails,
        userAddresses: [userAddresses]
      },
      errorUserDetail: {
        ...prevState.errorUserDetail,
        userAddresses: [
          this.initialState().userDetails.userAddresses[0]
        ]
      }
    }))
  }
  onFilterUsers = (item, metaData) => {
  }
  addNewUsers = () => {
    const { usersList, searchUsers, searchFirm } = this.state
    searchUsers.usersValue = ""
    const userDetails = cloneDeep(this.initialState().userDetails)
    userDetails.entityId = searchFirm.firmValue.id
    userDetails.userFirmName = searchFirm.firmValue.label
    this.setState({
      userDetails,
      errorUserDetail: cloneDeep(this.initialState().userDetails),
      loading: false,
      searchUsers,
      usersList,
      addressList: []
    })
  }
  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  resetAliases = (e, type) => {
    e.preventDefault()
    const { oldUsersDetails } = this.state
    if (Object.keys(oldUsersDetails).length !== 0) {
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          [type]: cloneDeep(oldUsersDetails[type])
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          [type]: oldUsersDetails[type].map((item) => this.initialState().userDetails[type][0])
        }
      }))
    } else {
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          [type]: this.initialState().userDetails[type]
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          [type]: this.initialState().userDetails[type]
        }
      }))
    }
  }
  addUpdateUsers = (e, type) => {
    e.preventDefault()
    const { searchFirm } = this.state
    const { userDetails, expanded } = cloneDeep(this.initialState())
    if (searchFirm.firmValue !== "" || (searchFirm.firmValue.id !== "" && searchFirm.firmValue.label !== "")) {
      userDetails.entityId = searchFirm.firmValue.id
      userDetails.userFirmName = searchFirm.firmValue.label
    }
    this.setState(prevState => ({
      userDetails,
      searchUsers: {
        usersValue: ""
      },
      addressList: [],
      oldUsersDetails: {},
      errorUserDetail: cloneDeep(this.initialState().userDetails),
      expanded,
      updateUsers: (type === "updateUsers"),
      addUsers: (type === "addUsers")
    }))
  }
  onChangeAddressType = (e, idx) => {
    const { userDetails } = this.state
    let { addressList, isPrimaryAddress } = this.state
    let { userAddresses } = userDetails
    if (e.target.name === "isPrimary") {
      userDetails.userAddresses[idx].isPrimary = !userDetails.userAddresses[idx].isPrimary
    }
    if (this.submitted) {
      addressList = addressList.length > 0 ? addressList.filter((obj) => obj._id != userAddresses[0]._id) : []
      userAddresses = userAddresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [...addressList, ...userAddresses]
      const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
      if (filteredAddresses.length === 0)
        // isPrimaryAddress  = "One Primary Office  Address  is Required"
        toast("One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
      else if (filteredAddresses.length > 1)
        // isPrimaryAddress  = "Only One Primary Office  Address  is Required"
        toast("Only One Primary Address  is Required!", { autoClose: 2000, type: toast.TYPE.ERROR })
      else {
        isPrimaryAddress = ""
      }
    }
    this.setState({
      userDetails,
      isPrimaryAddress
    })
  }
  checkAddressStatus = async (e, id, field) => {
    const { addressList } = this.state
    let addressIdx = ""
    addressList.forEach((address, idx) => {
      if (address._id == id) {
        addressIdx = idx
      }
    })
    if ((field === "isPrimary" && !addressList[addressIdx].isActive) || (field === "isActive" && addressList[addressIdx].isPrimary)) {
      toast("Primary Address Can not be Inactive.", { autoClose: 2000, type: toast.TYPE.WARNING })
    } else {
      addressList[addressIdx][field] = !addressList[addressIdx][field]
      if (field === "isPrimary") {
        addressList.forEach((address, idx) => {
          if (address._id === id)
            addressList[idx][field] = true
          else
            addressList[idx][field] = false
        })
      }
      this.setState(prevState => ({
        ...prevState,
        addressList
      }))
    }
  }
  getAddressDetails = (address = "", idx) => {
    if (address !== "") {
      const { userDetails, errorUserDetail } = this.state
      let id = ""
      if (userDetails.userAddresses[idx]._id !== "") {
        id = userDetails.userAddresses[idx]._id
      }
      userDetails.userAddresses[idx] = { ...this.initialState().userDetails.userAddresses[0] }
      userDetails.userAddresses[idx]._id = id
      if (address.addressLine1 !== "") {
        userDetails.userAddresses[idx].addressName = userDetails.userAddresses[idx].addressName !== "" ? userDetails.addresses[idx].addressName : address.addressLine1
        userDetails.userAddresses[idx].addressLine1 = address.addressLine1
        errorUserDetail.userAddresses[idx].addressName = ""
        errorUserDetail.userAddresses[idx].addressLine1 = ""
      }
      if (address.country !== "") {
        errorUserDetail.userAddresses[idx].country = ""
        userDetails.userAddresses[idx].country = address.country
      }
      if (address.state !== "") {
        errorUserDetail.userAddresses[idx].state = ""
        userDetails.userAddresses[idx].state = address.state
      }
      if (address.city !== "") {
        errorUserDetail.userAddresses[idx].city = ""
        userDetails.userAddresses[idx].city = address.city
      }
      if (address.zipcode !== "") {
        errorUserDetail.userAddresses[idx].country = ""
        userDetails.userAddresses[idx].zipCode.zip1 = address.zipcode
      }
      userDetails.userAddresses[idx].formatted_address = address.formatted_address
      userDetails.userAddresses[idx].url = address.url
      userDetails.userAddresses[idx].location.longitude = address.location.longitude.toString()
      userDetails.userAddresses[idx].location.latitude = address.location.latitude.toString()
      this.setState(prevState => ({
        userDetails: {
          ...prevState.userDetails,
          ...userDetails
        },
        errorUserDetail: {
          ...prevState.errorUserDetail,
          ...errorUserDetail
        }
      }))
    }
  }
  deleteAddress = (e, type, idx) => {
    const { userDetails, errorUserDetail } = this.state
    userDetails[type].splice(idx, 1)
    errorUserDetail[type].splice(idx, 1)
    this.setState({
      userDetails,
      errorUserDetail
    })
  }
  render() {
    const loading = () => <Loader />
    return (
      <section className="accordions">
        {"hi"}
        {this.state.loading ? loading() : null}
        <ContactType
          {...this.state}
          toggleButton={this.toggleButton}
          onChangeUserDetail={this.onChangeUserDetail}
        />
        <UserContact
          {...this.state}
          addMore={this.addMore}
          onChangeAddMore={this.onChangeAddMore}
          userDetails={this.state.userDetails}
          onChangeUserDetail={this.onChangeUserDetail}
          errorUserDetail={this.state.errorUserDetail}
          userType="users"
          onSearchFirms={this.onSearchFirms}
          onSearchUsers={this.onSearchUsers}
          getUsersList={this.getUsersList}
          deleteAliases={this.deleteAliases}
          resetAliases={this.resetAliases}
          addNewUsers={this.addNewUsers}
          toggleButton={this.toggleButton}
        />
        {/* {this.state.addressList.length > 0 && <ListAddresses
          addressList={this.state.addressList}
          updateAddress={this.updateAddress}
          listAddressToggle={this.state.expanded.addresslist}
          toggleButton={this.toggleButton}
          checkAddressStatus={this.checkAddressStatus}
        />}
        <article className={this.state.expanded.business ? "accordion is-active" : "accordion"}>
          <div className="accordion-header">
            <p
              onClick={
                event => {
                  this.toggleButton(event, "business")
                }}> Address</p>
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link is-small" onClick={this.addNewUserAddress}>Add</button>
              </div>
              <div className="control">
                <button className="button is-light is-small"
                  onClick={this.resetUserAddress}
                >Reset</button>
              </div>
            </div>
          </div>
          {this.state.expanded.business && <div className="accordion-body">
            {
              this.state.userDetails.userAddresses.map((userAddress, idx) => (
                <UserAddress
                  key={idx}
                  userAddress={userAddress}
                  idx={idx}
                  onChangeUserAddress={this.onChangeUserDetail}
                  errorUserDetail={this.state.errorUserDetail}
                  countryResult={this.state.countryResult}
                  deleteAddress={this.deleteAddress}
                  onChangeAddressType={this.onChangeAddressType}
                  getAddressDetails={this.getAddressDetails}
                  isPrimaryAddress={this.state.isPrimaryAddress}
                  busy={this.state.loadingPickLists}
                />
              ))
            }
          </div>}
        </article> */}
        <FirmAddressListForm
          key={parseInt(1 * 30, 10)}
          countryResult={this.state.countryResult}
          errorFirmDetail={this.state.errorUserDetail}
          onChangeAddressType={this.onChangeAddressType}
          canEdit={this.state.canEdit}
          canView
          busy={this.state.loadingPickLists}
          addressList={this.state.addressList}
          onSaveAddress={this.onSaveAddress}
          isUserAddress={true}
          officeAddress={this.state.addressList[0]}
        />
        <Documents {...this.props} userDetails={this.state.userDetails} />
        <br />
        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped-center">
              <div className="control">
                <button className="button is-link" onClick={this.handleSubmit}>Save</button>
              </div>
              <div className="control">
                <button className="button is-light">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = state => {
  const { admUserDetail, admFirmDetails, auth } = state
  return { admUserDetail, admFirmDetails, auth }
}
const mapDispatchToProps = {
  saveUserDetail,
  getUserDetailById
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UsersContainers))
