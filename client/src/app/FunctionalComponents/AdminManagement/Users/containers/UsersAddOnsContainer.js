import {
  searchUsersList,
  getUserDetailById,
  getAllTenetsEntity,
  checkPlatFormAdmin,
  saveUserAddOns
} from "AppState/actions/AdminManagement/admTrnActions"

import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { DropdownList } from "react-widgets"
import { getPicklistValues, checkNavAccessEntitlrment } from "GlobalUtils/helpers"
import UsersAddOns from "./../components/UserAddOns"
import Loader from "../../../../GlobalComponents/Loader"

class UsersAddOnsContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      usersAddOnsList: [],
      usersDetails: {
        userAddOns: []
      },
      usersList: [],
      usersName: "",
      firmName: "",
      errors: {
        firm: "",
        user: ""
      }
    }
    this.submitted = false
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async componentWillMount() {
    const { nav2 } = this.props
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit
    this.setState({
      loading: true,
      viewOnly
    })
  }
  async componentDidMount() {
    const { userEntities } = this.props.auth
    let { usersList } = this.state
    // userEntities.isMuniVisorClient = true
    let firmList = getAllTenetsEntity()
    let picklistArray = getPicklistValues(["LKUPUSERSADDONS"])
    let isPlatFormAdmin = checkPlatFormAdmin()
    isPlatFormAdmin = await isPlatFormAdmin
    if (!isPlatFormAdmin) {
      usersList = await searchUsersList(userEntities.entityId, "")
      usersList = usersList.map((item, idx) => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))
    }
    let [usersAddOnsList] = await picklistArray
    firmList = await firmList
    this.setState({
      usersList: [{ value: "", label: "Select Users" }, ...usersList],
      firmList: [{ "label": "Select Firm", "value": "" }, ...firmList.map(item => ({ "label": item.firmName, "value": item._id }))],
      usersAddOnsList: usersAddOnsList[1],
      isPlatFormAdmin,
      loading: false
    })
    if (this.props.userId && this.props.userId.length === 24) {
      this.props.getUserDetailById(this.props.userId)
    }
  }

  async componentWillReceiveProps(nextProps) {
    const { nav2 } = nextProps
    if (nextProps.admUserDetail.userDetails &&
      Object.keys(nextProps.admUserDetail.userDetails).length > 0 &&
      !nextProps.admUserDetail.updated
    ) {
      const usersDetails = cloneDeep(nextProps.admUserDetail.userDetails)
      usersDetails.userAddOns = usersDetails.userAddOns ? usersDetails.userAddOns : []
      this.setState(prevState => ({
        usersDetails,
        usersName: { value: usersDetails._id, label: `${usersDetails.userFirstName} ${usersDetails.userLastName}` }
      }))
    }
    if (nav2 !== this.props.nav2) {
      const action = await checkNavAccessEntitlrment(nav2)
      const viewOnly = action && nav2 && !action.edit
      this.setState({ viewOnly })
    }
  }
  componentWillUnmount() {

  }
  onSearchUsers = item => {
    const { errors } = this.state
    if (item.value !== "") {
      errors.user = ""
      this.setState({
        usersName: item,
        loading: true,
        errors
      })
      this.props.getUserDetailById(item.value)
    } else {
      if (this.submitted) {
        errors.user = "Select User"
      }
      this.setState({
        usersDetails: {
          userAddOns: []
        },
        usersName: "",
        errors
      })
    }
  }
  onChangeFirm = async (item) => {
    const { errors } = this.state
    if (item.value !== "") {
      errors.firm = ""
      let usersList = await searchUsersList(item.value, "")
      usersList = [{ "label": "Select Users", "value": "" }, ...usersList.map((item, idx) => ({ value: item.userId, label: `${item.firstName} ${item.lastName}` }))]
      this.setState({
        usersDetails: {
          userAddOns: []
        },
        firmName: item,
        usersList,
        errors
      })
    } else {
      if (this.submitted) {
        errors.firm = "Select Firm"
      }
      this.setState({
        firmName: item,
        usersList: [{ "label": "Select Users", "value": "" }],
        errors
      })
    }
  }
  /** ************************************************************************************* */
  async handleSubmit(event) {
    event.preventDefault()
    this.submitted = true
    try {
      const { usersDetails, usersName, firmName } = this.state
      const { userAddOns } = usersDetails
      if (usersName !== "") {
        const response = await saveUserAddOns(userAddOns, usersDetails._id)
        if (response.status === 200)
          toast("AddOns has added", { autoClose: 2000, type: toast.TYPE.SUCCESS })
        else
          toast("Something Wrong!", { autoClose: 2000, type: toast.TYPE.ERROR })
      } else {
        this.setState({
          errors: {
            firm: firmName !== "" ? "" : "Select Firm",
            user: usersName !== "" ? "" : "Select User"
          }
        })
      }
    } catch (error) {
      console.log("========>>>", error)
    }
  }
  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = (e) => {
    const { usersDetails } = this.state
    const { userAddOns } = usersDetails
    const { value } = e.target
    if (userAddOns.includes(value)) {
      userAddOns.splice(userAddOns.indexOf(value), 1)
    } else {
      userAddOns.push(value)
    }
    this.setState((prevState) => ({
      ...prevState, ...{
        ...usersDetails,
        userAddOns
      }
    }))
  }

  render() {
    const loading = () => <Loader />
    const { usersAddOnsList, usersDetails, usersList, usersName, firmName, firmList, viewOnly } = this.state
    const { userAddOns } = usersDetails
    return (
      <div id="main">
        {this.state.loading ? loading() : null}
        <section className="container">
          <div className="tile is-ancestor">
            <div className="tile is-vertical is-8">
              <div className="tile">
                <div className="tile is-parent is-vertical">
                  <article className="tile is-child">
                    <p className="title innerPgTitle">You selected...</p>
                    <p className="subtitle">{usersDetails.userFirstName}</p>
                  </article>
                </div>
              </div>
            </div>
          </div>
          <section className="accordions">
            {this.state.isPlatFormAdmin && <div className="columns">
              <div className="column">
                <p className="title innerPgTitle">Associated Entity / Firm </p>
                <div className="control">
                  <DropdownList
                    filter="contains"
                    value={firmName}
                    data={firmList}
                    message="select Firm Name"
                    textField='label'
                    valueField="value"
                    onChange={this.onChangeFirm}
                    disabled={!this.state.isPlatFormAdmin}
                  />
                </div>
                {this.state.errors && <small className="text-error">{this.state.errors.firm}</small>}
              </div>
            </div>}
            {/*             <div className="columns">
              <div className="column">
                <p className="title innerPgTitle">Associated Users </p>
                <div className="control">
                  <DropdownList
                    filter="contains"
                    value={usersName}
                    data={usersList}
                    message="Select Users Name"
                    textField='label'
                    valueField="value"
                    onChange={this.onSearchUsers}
                  />
                </div>
                {this.state.errors && <small className="text-error">{this.state.errors.user}</small>}
              </div>
            </div> */}
            <UsersAddOns
              usersAddOnsList={usersAddOnsList}
              toggleCheckbox={this.toggleCheckbox}
              userAddOns={userAddOns}
              viewOnly={viewOnly}
            />
            <hr />
            {
              !viewOnly ?
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped">
                      <div className="control">
                        <button className="button is-link is-small" onClick={this.handleSubmit}>
                          Save
                        </button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
                :null
            }
          </section>
        </section>
      </div>
    )
  }
}
const mapStateToProps = state => {
  const { auth, admUserDetail } = state
  return { auth, admUserDetail }
}
const mapDispatchToProps = {
  getUserDetailById
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UsersAddOnsContainer))
