import React from "react"

const ListAddresses = (props) => (
  <article className={props.listAddressToggle ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "addresslist")
        }}
      >Manage Available Addresses</p>
    </div>
    <div className="accordion-body">
      { props.listAddressToggle &&
      <div className="accordion-content">
        <div style={{ overflowX: "auto" }}>

          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <thead>
              <tr>
                <th>
                  <p className="multiExpLbl">Address Name</p>
                </th>
                {/*  <th>
                  <p className="multiExpLbl">Residence?</p>
                </th> */}
                <th>
                  <p className="multiExpLbl ">Primary?</p>
                </th>
                <th>
                  <p className="multiExpLbl ">Inactive</p>
                </th>
                <th>
                  <p className="multiExpLbl ">Update</p>
                </th>
              </tr>
            </thead>
            <tbody>
              {
                props.addressList ?
                  props.addressList.map((address, idx) => (
                    <tr key={idx}>
                      <td className="multiExpTblVal">{address.addressName}</td>
                      {/*                       <td>
                        <p className="emmaTablesTd">
                          <input type="checkbox"
                            value = {address.isResidence}
                            checked={address.isResidence}
                            onChange = {
                              (event)=>{
                                props.checkAddressStatus(event, address._id,"isResidence")
                              }
                            }
                          />
                        </p>
                      </td> */}
                      <td className="multiExpTblVal">
                        <p className="emmaTablesTd">
                          <input type="checkbox"
                            value = {address.isPrimary}
                            checked={address.isPrimary}
                            onChange = {
                              (event)=>{
                                props.checkAddressStatus(event, address._id,"isPrimary")
                              }
                            }
                          />
                        </p>
                      </td>
                      <td className="multiExpTblVal">
                        <p className="emmaTablesTd">
                          <input type="checkbox"
                            value = {address.isActive}
                            checked={!address.isActive}
                            onChange = {
                              (event)=>{
                                props.checkAddressStatus(event, address._id,"isActive")
                              }
                            }
                          />
                        </p>
                      </td>
                      <td className="multiExpTblVal">
                        <a href="#">
                          <span className="has-text-link">
                            <i className="fas fa-pencil-alt is-small" onClick={
                              (event) => {
                                event.preventDefault()
                                props.updateAddress(event, address._id)
                              }} />
                          </span>
                        </a>
                      </td>
                    </tr>
                  ))
                  : ""
              }
            </tbody>
          </table>
        </div>
      </div>}

    </div>

  </article>

)

export default ListAddresses