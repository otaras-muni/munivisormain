import React from "react"
import { DropdownList } from "react-widgets"

const FirmProfileForm = props => (
  <div className="accordion-body">
    <div className="accordion-content">
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">
            MSRB Firm Name
          </p>
          <div className="control">
            <DropdownList
              value={props.firmDetails.msrbFirmName ? { label: props.firmDetails.msrbFirmName, value: props.firmDetails._id } : (props.msrbFirmList && props.msrbFirmList[0]) || []}
              data={props.msrbFirmList}
              message="select MSRB Firm Name"
              textField='label' valueField="value" defaultValue=""
              onChange={props.getFirmDetails}
              disabled={props.readOnlyFeatures.msrbFirmNameReadOnly}
              busy={props.busy}
              busySpinner={
                <span className="fas fa-sync fa-spin is-link" />
              } />
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">
            MSRB Registrant Type
          </p>
          <div className="control">
            <DropdownList
              value={props.firmDetails.msrbRegistrantType}
              data={props.msrbRegTypeResult}
              valueField="0"
              defaultValue={props.msrbRegTypeResult[2] ? props.msrbRegTypeResult[2][0] : []}
              disabled={props.readOnlyFeatures.registrantTypeReadOnly}
              busy={props.busy}
              busySpinner={
                <span className="fas fa-sync fa-spin is-link" />
              }
            onChange={(val) => {
              const event = {
                target: {
                  name: "msrbRegistrantType",
                  value: val
                }
              }
              props.onChangeFirmDetail(event)
            }}
            />
          </div>
          {props.errorFirmDetail && props.errorFirmDetail.msrbRegistrantType && <small className="text-error">{props.errorFirmDetail.msrbRegistrantType}</small>}
        </div>
        <div className="column">
          <p className="multiExpLbl">MSRB ID</p>
          <div className="field">
            <div className="control">
              <input
                className="input is-small is-link"
                name="msrbId"
                type="text"
                placeholder="K0525"
                value={props.firmDetails.msrbId}
                onChange={props.onChangeFirmDetail}
                disabled={props.readOnlyFeatures.msrbIdReadOnly}
                onBlur={props.checkDuplicateFirm}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.msrbId && <small className="text-error">{props.errorFirmDetail.msrbId}</small>}
            {props.isEntityExists && props.isEntityExists.msrbId && <small className="text-error">{props.isEntityExists.msrbId}</small>}
          </div>
        </div>
      </div>
      <hr />
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">
            Firm Name (if not MSRB registered)
          </p>
          <div className="field">
            <div className="control">
              <input
                className="input is-small is-link"
                name="firmName"
                type="text"
                placeholder="Enter Firm Name"
                value={props.firmDetails.firmName}
                onChange={props.onChangeFirmDetail}
                disabled={props.readOnlyFeatures.firmName}
                onBlur={props.checkDuplicateFirm}
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.firmName && <small className="text-error">{props.errorFirmDetail.firmName}</small>}
            {props.isEntityExists && props.isEntityExists.firmName && <small className="text-error">{props.isEntityExists.firmName}</small>}
          </div>

          {props.firmDetails.entityAliases &&
            props.firmDetails.entityAliases.length
            ? props.firmDetails.entityAliases.map((item, idx) => (
              <div className="field is-grouped-left" key={idx}>
                <div className="control d-flex">
                  <input
                    className="input is-small is-link"
                    name="entityAliases"
                    type="text"
                    placeholder="Enter Aliases"
                    value={item}
                    onChange={event => {
                      props.onChangeAliases(event, idx)
                    }}
                  />
                  <span className="has-text-link fa-delete deleteadressaliases"
                    onClick={() => {
                      props.deleteAliases(idx)
                    }}
                  >
                    <i className="far fa-trash-alt" />
                  </span>
                </div>
                {props.errorFirmDetail && props.errorFirmDetail.entityAliases[idx] && <small className="text-error">{props.errorFirmDetail.entityAliases[idx]}</small>}
              </div>
            ))
            : ""}
          <div className="field is-grouped-left">
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "aliases")
                }}
              >
                Add Alias
              </button>
            </div>
            <div className="control">
              <button
                className="button is-light is-small"
                onClick={props.resetAliases}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>

      <hr />

      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Tax ID</p>
          <div className="control">
            <input
              className="input is-small is-link"
              name="taxId"
              type="text"
              placeholder="Enter Tax ID"
              value={props.firmDetails.taxId}
              onChange={props.onChangeFirmDetail}
            />
          </div>
          {props.errorFirmDetail && props.errorFirmDetail.taxId && <small className="text-error">{props.errorFirmDetail.taxId}</small>}
        </div>
        <div className="column">
          <p className="multiExpLbl">Business Structure</p>
          <div className="field">
            <div className="control">
              <DropdownList filter value={props.firmDetails.businessStructure} data={props.businessStructure[1]} message="Select Business Addresses" textField='label' valueField="value" defaultValue=""
                busy={props.busy}
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "businessStructure",
                      value: val
                    }
                  }
                  props.onChangeFirmDetail(event)
                }} />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.businessStructure && <small className="text-error">{props.errorFirmDetail.businessStructure}</small>}
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">Number of Employees</p>
          <div className="field">
            <div className="control">
              <DropdownList filter value={props.firmDetails.numEmployees} data={props.numberOfEmployee[1]} message="Number Of Employee" textField='label' valueField="value" defaultValue=""
                busy={props.busy}
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "numEmployees",
                      value: val
                    }
                  }
                  props.onChangeFirmDetail(event)
                }} />
            </div>
          </div>
        </div>
        <div className="column">
          <p className="multiExpLbl">Annual Revenue</p>
          <div className="field">
            <div className="control">
              <DropdownList filter value={props.firmDetails.annualRevenue} data={props.annualRevenue[1]} message="Number Of Employee" textField='label' valueField="value" defaultValue=""
                busy={props.busy}
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "annualRevenue",
                      value: val
                    }
                  }
                  props.onChangeFirmDetail(event)
                }} />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)
export default FirmProfileForm
