import React from "react"
import { DropdownList } from "react-widgets"
import NumberFormat from "react-number-format"
import SearchAddress from "./../../GoogleAddressForm/GoogleAddressFormComponent"

const AdmTrnAddressForm = (props) => (
  <div className="accordion-content" key={props.idx} style={{ borderBottom: "1px solid #8393a8" }}>
    <div className="tile is-ancestor">
      <div className="tile is-12 is-vertical is-parent">
        <SearchAddress idx={props.idx} getAddressDetails={props.getAddressDetails} inputAddresses={props.inputAddresses} />
        <div className="columns">
          <div className="column is-half">
            <p className="multiExpLbl">Address Name</p>
            <div className="control">
              <input className="input is-small is-link" name="addressName" type="text" value={props.businessAddress.addressName ? props.businessAddress.addressName : ""} placeholder="Address identifier"
                onChange={
                  (event) => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }
                }
              />
              {props.errorFirmDetail && props.errorFirmDetail.addressName && <small className="text-error">{props.errorFirmDetail.addressName}</small>}
            </div>
          </div>
          <div className="column is-half">
            <div className="column">
              <div className="column is-half" style={{ display: "inline-block" }}>
                <p className="multiExpLbl">
                  <input name="isPrimary" type="checkbox" value={props.businessAddress.isPrimary}
                    checked={props.businessAddress.isPrimary}
                    onChange={
                      (event) => {
                        props.onChangeAddressType(event, props.idx)
                      }
                    }
                  /> Is Primary?
                </p>
              </div>
              {/*               <div className="is-half" style={{display:"inline-block"}}>
                <p className="multiExpLbl">
                  <input name="isHeadQuarter" type="checkbox" value={props.businessAddress.isHeadQuarter}
                    checked={props.businessAddress.isHeadQuarter}
                    onChange={
                      (event) => {
                        props.onChangeAddressType(event, props.idx)
                      }
                    }
                  /> Is Headquarter?
                </p>
              </div> */}
              <div style={{ padding: "0.75rem", float: "right" }}>
                {props.idx > 0
                  ? <span className="has-text-link fa-delete" style={{ cursor: "pointer" }}
                    onClick={() => {
                      props.deleteAddress(event, "addresses", props.idx)
                    }}
                  >
                    <i className="far fa-trash-alt" />
                  </span>
                  : ""
                }
              </div>
            </div>
            {props.isPrimaryAddress && <small className="text-error">{props.isPrimaryAddress}</small>}
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Website(s)</p>
            <div className="control">
              <input className="input is-small is-link" name="website" type="text" placeholder="Enter Website URL" value={props.businessAddress.website ? props.businessAddress.website : ""}
                onChange={
                  (event) => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }
                }
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.website && <small className="text-error">{props.errorFirmDetail.website}</small>}
          </div>
        </div>

        <div className="columns">
          <div className="column is-third">
            <p className="multiExpLbl">Office Phone</p>
            {
              props.businessAddress.officePhone.map((item, idx) => (
                <div className="field is-grouped-left" key={2 * idx * idx}>
                  <div>
                    <div className="control" style={{ marginRight: "5px" }}>
                      <NumberFormat
                        format="+1 (###) ###-####"
                        mask="_"
                        className="input is-small is-link"
                        placeholder="Numbers Only"
                        name="phoneNumber"
                        size="15"
                        value={item.phoneNumber ? item.phoneNumber : ""}
                        onChange={
                          (event) => {
                            props.onChangeAddMore(event, "officePhone", props.idx, idx)
                          }
                        } />
                      {props.errorFirmDetail && props.errorFirmDetail.officePhone[idx].phoneNumber && <span className="text-error">{props.errorFirmDetail.officePhone[idx].phoneNumber}</span>}
                    </div>
                  </div>
                  <div>
                    <div className="control">
                      <NumberFormat
                        format="####"
                        className="input is-small is-link"
                        name="extension"
                        placeholder="Ext"
                        size="8"
                        value={item.extension ? item.extension : ""}
                        onChange={
                          (event) => {
                            props.onChangeAddMore(event, "officePhone", props.idx, idx)
                          }
                        } />
                      {idx > 0
                        ? <span className="has-text-link fa-delete deleteadressaliases"
                          onClick={() => {
                            props.deleteAddressAliases(event, "officePhone", props.idx, idx)
                          }}
                        >
                          <i className="far fa-trash-alt" />
                        </span>
                        : ""
                      }
                      {props.errorFirmDetail && props.errorFirmDetail.officePhone[idx].extension && <small className="text-error">{props.errorFirmDetail.officePhone[idx].extension}</small>}
                    </div>
                  </div>
                </div>
              ))
            }
            <div className="control">
              <button className="button is-link is-small"
                onClick={
                  (event) => {
                    props.addMore(event, "officePhone", props.idx)
                  }
                }>Add More</button>
              <button
                className="button is-light is-small"
                onClick={event => {
                  props.resetAddressAliases(event, "officePhone", props.idx, props.businessAddress._id ? props.businessAddress._id : "")
                }}
              >
                Cancel
              </button>
            </div>
          </div>
          <div className="column">
            <p className="multiExpLbl">Office Fax</p>
            {
              props.businessAddress.officeFax.map((item, idx) => (
                <div className="field" key={3 * idx * idx}>
                  <div className="control d-flex">
                    <NumberFormat
                      format="+1 (###) ###-####"
                      mask="_"
                      className="input is-small is-link"
                      name="faxNumber"
                      value={item.faxNumber ? item.faxNumber : ""}
                      onChange={
                        (event) => {
                          props.onChangeAddMore(event, "officeFax", props.idx, idx)
                        }
                      } />
                    {idx > 0
                      ? <span className="has-text-link fa-delete deleteadressaliases"
                        onClick={() => {
                          props.deleteAddressAliases(event, "officeFax", props.idx, idx)
                        }}
                      >
                        <i className="far fa-trash-alt" />
                      </span>
                      : ""
                    }
                  </div>
                  {props.errorFirmDetail && props.errorFirmDetail.officeFax[idx].faxNumber && <small className="text-error">{props.errorFirmDetail.officeFax[idx].faxNumber}</small>}
                </div>
              ))
            }
            <div className="control">
              <button className="button is-link is-small"
                onClick={
                  (event) => {
                    props.addMore(event, "officeFax", props.idx)
                  }
                }
              >Add More</button>
              <button
                className="button is-light is-small"
                onClick={event => {
                  props.resetAddressAliases(event, "officeFax", props.idx, props.businessAddress._id ? props.businessAddress._id : "")
                }}
              >
                Cancel
              </button>
            </div>
          </div>

          <div className="column">
            <p className="multiExpLbl">Office Email</p>
            {
              props.businessAddress.officeEmails.map((item, idx) => (
                <div className="field" key={4 * idx * idx}>
                  <div className="control d-flex">
                    <input className="input is-small is-link" name="emailId" type="email" placeholder="e.g. alexsmith@gmail.com"
                      value={item.emailId ? item.emailId : ""}
                      onChange={
                        (event) => {
                          props.onChangeAddMore(event, "officeEmails", props.idx, idx)
                        }
                      }
                    />
                    {idx > 0
                      ? <span className="has-text-link fa-delete deleteadressaliases"
                        onClick={() => {
                          props.deleteAddressAliases(event, "officeEmails", props.idx, idx)
                        }}
                      >
                        <i className="far fa-trash-alt" />
                      </span>
                      : ""
                    }
                  </div>
                  {props.errorFirmDetail && props.errorFirmDetail.officeEmails[idx].emailId && <small className="text-error">{props.errorFirmDetail.officeEmails[idx].emailId}</small>}
                </div>
              ))
            }
            <div className="control">
              <button className="button is-link is-small"
                onClick={
                  (event) => {
                    props.addMore(event, "officeEmails", props.idx)
                  }
                }
              >Add More</button>
              <button
                className="button is-light is-small"
                onClick={event => {
                  props.resetAddressAliases(event, "officeEmails", props.idx, props.businessAddress._id ? props.businessAddress._id : "")
                }}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-half">
            <p className="multiExpLbl">Address Line 1</p>
            <div className="control">
              <input className="input is-small is-link" name="addressLine1" type="text" placeholder="address line 1" value={props.businessAddress.addressLine1 ? props.businessAddress.addressLine1 : ""}
                onChange={
                  (event) => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }
                }
              />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.addressLine1 && <small className="text-error">{props.errorFirmDetail.addressLine1}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Address Line 2</p>
            <div className="control">
              <input className="input is-small is-link" type="text" name="addressLine2" placeholder="address line 2" value={props.businessAddress.addressLine2 ? props.businessAddress.addressLine2 : ""}
                onChange={
                  (event) => {
                    props.onChangeBusinessAddress(event, props.idx)
                  }
                }
              />
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-one-fourth">
            <p className="multiExpLbl">Country</p>
            <div className="control">
              <DropdownList filter value={props.businessAddress.country} data={props.countryResult[1] ? [...props.countryResult[1]] : [""]} message="select Registrant Type" textField='country'
                busy={props.busy}
                busySpinner={
                  <span className="fas fa-sync fa-spin is-link" />
                }
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "country",
                      value: val
                    }
                  }
                  props.onChangeBusinessAddress(event, props.idx, ["state", "city"])
                }} />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.country && <small className="text-error">{props.errorFirmDetail.country}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">State</p>
            <div className="control">
              <DropdownList filter value={props.businessAddress.state} data={props.businessAddress.country && props.countryResult[2][props.businessAddress.country] ? [...props.countryResult[2][props.businessAddress.country]] : []}
                message="select Registrant Type"
                textField='state'
                busy={props.busy}
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "state",
                      value: val
                    }
                  }
                  props.onChangeBusinessAddress(event, props.idx, ["city"])
                }} />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.state && <small className="text-error">{props.errorFirmDetail.state}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">City</p>
            <div className="control">
              <DropdownList filter value={props.businessAddress.city} data={(props.businessAddress.country && props.businessAddress.state) && (props.countryResult[3][props.businessAddress.country] && props.countryResult[3][props.businessAddress.country][props.businessAddress.state]) ? [...props.countryResult[3][props.businessAddress.country][props.businessAddress.state]] : []} message="select Registrant Type" textField='city'
                busy={props.busy}
                busySpinner={
                  <span className="fas fa-sync fa-spin is-link" />
                }
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "city",
                      value: val
                    }
                  }
                  props.onChangeBusinessAddress(event, props.idx, [])
                }} />
            </div>
            {props.errorFirmDetail && props.errorFirmDetail.city && <small className="text-error">{props.errorFirmDetail.city}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Zip Code</p>
            <div className="field is-grouped-left">
              <div className="control d-flex">
                <div >
                  <input
                    type="text"
                    className="input is-small is-link"
                    placeholder="Zip1"
                    name="zip1"
                    minLength="5"
                    maxLength="5"
                    value={props.businessAddress.zipCode.zip1 ? props.businessAddress.zipCode.zip1 : ""}
                    onChange={
                      (event) => {
                        props.onChangeBusinessAddress(event, props.idx)
                      }
                    } />
                </div>
                {props.errorFirmDetail && props.errorFirmDetail.zipCode.zip1 && <small className="text-error">{props.errorFirmDetail.zipCode.zip1}</small>}
              </div>
              <div className="control d-flex">
                <div>
                  <input
                    type="text"
                    className="input is-small is-link"
                    placeholder="Zip2"
                    name="zip2"
                    minLength="4"
                    maxLength="4"
                    value={props.businessAddress.zipCode.zip2 ? props.businessAddress.zipCode.zip2 : ""}
                    onChange={
                      (event) => {
                        props.onChangeBusinessAddress(event, props.idx)
                      }
                    } />
                </div>
                {props.errorFirmDetail && props.errorFirmDetail.zipCode.zip2 && <small className="text-error">{props.errorFirmDetail.zipCode.zip2}</small>}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)
export default AdmTrnAddressForm