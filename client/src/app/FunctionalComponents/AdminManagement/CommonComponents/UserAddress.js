import React from "react"
import {
  FormGroup
} from "react-bootstrap"

import { DropdownList } from "react-widgets"

import NumberFormat from "react-number-format"
import SearchAddress from "./../../GoogleAddressForm/GoogleAddressFormComponent"
const UserAddress = (props) => (
  <div className="accordion-content" style={{ borderBottom: "1px solid #8393a8" }} key={props.idx}>
    <div className="tile is-ancestor">
      <div className="tile is-12 is-vertical is-parent">
        <SearchAddress idx={props.idx} getAddressDetails={props.getAddressDetails} />
        <div className="columns">
          <div className="column is-one-third">
            <p className="multiExpLbl">Address Name</p>
            <div className="control">
              <input className="input is-small is-link" type="text" placeholder="Address identifier"
                name="addressName"
                value={props.userAddress.addressName}
                onChange={(event) => {
                  props.onChangeUserAddress(event, props.idx)
                }}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].addressName && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].addressName}</small>}
          </div>
          <div className="column is-two-third">
            <div className="column is-one-fourth" style={{ display: "inline-block" }}>
              <p className="emmaTablesTd">
                <input
                  type="checkbox"
                  name="isPrimary"
                  value={props.userAddress.isPrimary}
                  checked={props.userAddress.isPrimary}
                  onChange={
                    (event) => {
                      props.onChangeAddressType(event, props.idx)
                    }
                  }
                /> Use Primary Office Address?
              </p>
              {props.isPrimaryAddress && <small className="text-error">{props.isPrimaryAddress}</small>}
            </div>
            {/* <div className="column is-one-fourth" style={{display:"inline-block"}}>
              <p className="emmaTablesTd">
                <input
                  type="checkbox"
                  name="isHeadQuarter"
                  value={props.userAddress.isHeadQuarter}
                  checked={props.userAddress.isHeadQuarter}
                  onChange={
                    (event) => {
                      props.onChangeAddressType(event, props.idx)
                    }
                  }
                /> Use Headquarter Address?
              </p>
            </div>
            <div className="column is-one-fourth" style={{display:"inline-block"}}>
              <p className="emmaTablesTd">
                <input
                  type="checkbox"
                  name="isResidence"
                  value={props.userAddress.isResidence}
                  checked={props.userAddress.isResidence}
                  onChange={
                    (event) => {
                      props.onChangeAddressType(event, props.idx)
                    }
                  }
                /> Use Residence Address?
              </p>
            </div> */}
            <div className="column is-one-fourth" style={{ padding: "0.75rem", float: "right" }}>
              {props.idx > 0
                ? <span className="has-text-link fa-delete" style={{ cursor: "pointer" }}
                  onClick={() => {
                    props.deleteAddress(event, "userAddresses", props.idx)
                  }}
                >
                  <i className="far fa-trash-alt" />
                </span>
                : ""
              }
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-half">
            <p className="multiExpLbl">Address Line 1</p>
            <div className="control">
              <input className="input is-small is-link" type="text" placeholder="address line 1"
                name="addressLine1"
                value={props.userAddress.addressLine1}
                onChange={(event) => {
                  props.onChangeUserAddress(event, props.idx)
                }}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].addressLine1 && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].addressLine1}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Address Line 2</p>
            <div className="control">
              <input className="input is-small is-link" type="text" placeholder="address line 2"
                name="addressLine2"
                value={props.userAddress.addressLine2}
                onChange={(event) => {
                  props.onChangeUserAddress(event, props.idx)
                }}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].addressLine2 && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].addressLine2}</small>}
          </div>
        </div>

        <div className="columns">
          <div className="column is-one-fourth">
            <p className="multiExpLbl">Country</p>
            <div className="control">
              <DropdownList filter value={props.userAddress.country} data={props.countryResult[1] ? [...props.countryResult[1]] : [""]} message="select Country" textField='country'
                busy={props.busy}
                busySpinner={
                  <span className="fas fa-sync fa-spin is-link" />
                }
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "country",
                      value: val
                    }
                  }
                  props.onChangeUserAddress(event, props.idx, ["state", "city"])
                }} />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].country && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].country}</small>}

          </div>
          <div className="column">
            <p className="multiExpLbl">State</p>
            <div className="control">
              <DropdownList filter value={props.userAddress.state} data={props.userAddress.country && props.countryResult[2][props.userAddress.country] ? [...props.countryResult[2][props.userAddress.country]] : []}
                message="select Country"
                textField='state'
                busy={props.busy}
                busySpinner={
                  <span className="fas fa-sync fa-spin is-link" />
                }
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "state",
                      value: val
                    }
                  }
                  props.onChangeUserAddress(event, props.idx, ["city"])
                }} />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].state && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].state}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">City</p>
            <div className="control">
              <DropdownList filter value={props.userAddress.city} data={(props.userAddress.state && props.userAddress.country) && (props.countryResult[3][props.userAddress.country] && props.countryResult[3][props.userAddress.country][props.userAddress.state]) ? [...props.countryResult[3][props.userAddress.country][props.userAddress.state]] : []}
                message="select City"
                textField='city'
                busy={props.busy}
                busySpinner={
                  <span className="fas fa-sync fa-spin is-link" />
                }
                onChange={(val) => {
                  const event = {
                    target: {
                      name: "city",
                      value: val
                    }
                  }
                  props.onChangeUserAddress(event, props.idx, [])
                }} />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].city && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].city}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl">Zip Code</p>
            <div className="field is-grouped-left">
              <div className="control">
                <div>
                  <NumberFormat
                    format="#####"
                    className="input is-small is-link"
                    placeholder="Zip1"
                    name="zip1"
                    size="5"
                    value={props.userAddress.zipCode.zip1}
                    onChange={
                      (event) => {
                        props.onChangeUserAddress(event, props.idx)
                      }
                    } />
                </div>
                {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].zipCode.zip1 && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].zipCode.zip1}</small>}
              </div>
              <div className="control">
                <div>
                  <NumberFormat
                    format="####"
                    className="input is-small is-link"
                    placeholder="Zip2"
                    name="zip2"
                    size="5"
                    value={props.userAddress.zipCode.zip2}
                    onChange={
                      (event) => {
                        props.onChangeUserAddress(event, props.idx)
                      }
                    } />
                </div>
                {props.errorUserDetail && props.errorUserDetail.userAddresses[props.idx].zipCode.zip2 && <small className="text-error">{props.errorUserDetail.userAddresses[props.idx].zipCode.zip2}</small>}
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

)

export default UserAddress