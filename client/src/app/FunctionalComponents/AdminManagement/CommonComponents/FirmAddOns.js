import React from "react"
import includes from "array-includes"

const FirmAddOns = (props) => (

  <article className="accordion is-active">
    <div className="accordion-header">
      <p>Firm Level</p>
    </div>
    <div className="accordion-body">
      <div className="accordion-content">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl" title="">Add-On Name</p>
              </th>
              <th>
                <p className="multiExpLbl" title="">Enabled</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {
              props.firmAddonsList.map((item, idx) => (
                <tr key={idx}>
                  <td>
                    {item}
                  </td>
                  <td>
                    <p className="emmaTablesTd">
                      <input
                        type="checkbox"
                        name="addOns"
                        value={item}
                        onChange={
                          (event)=>{
                            event.target.name = "addOns"
                            event.target.value = item
                            props.toggleCheckbox(event)
                          }}
                        checked={!!props.firmAddOns.includes(item)}
                      />
                    </p>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

      </div>
    </div>
  </article>
)

export default FirmAddOns