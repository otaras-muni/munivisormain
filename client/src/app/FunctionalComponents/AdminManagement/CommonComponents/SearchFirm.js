import React from "react"

const issuerFlags = [
  "State Issuer",
  "State Authority",
  "County Issuer",
  "Conduit",
  "Local Authority",
  "Locality",
  "Other"
]
const SearchFirm = props => (
  <div>
    <div className="column">
      {props.issuerFlagsList && props.issuerFlagsList.map(item => (
        <div className="column" key={Math.random()} style={{display:"inline-block"}}>
          <h1>
            <p className="emmaTablesTd" title={`Is ${item}?`}>
              {item}
              <input
                type="checkbox"
                name="issuerFlags"
                value={item}
                onChange={
                  (event)=>{
                    event.target.name = "issuerFlags"
                    event.target.value = item
                    props.onChangeIssuerFlag(event)
                  }}
                checked={!!props.firmDetails.entityFlags.issuerFlags.includes(item)}
              />
            </p>
          </h1>
        </div>
      ))}
    </div>
    {props.errorFirmDetail &&  props.errorFirmDetail.entityFlags.issuerFlags && <small className="text-error">{props.errorFirmDetail.entityFlags.issuerFlags}</small>}                     
    <hr />
  </div>
)

export default SearchFirm
