import React from "react"
import Switch, { Case, Default } from "react-switch-case"
import NumberFormat from "react-number-format"
import moment from "moment"

const UserContact = props => (
  <article className={props.expanded.contactdetail ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "contactdetail")
        }}>Contact Detail</p>
    </div>
    {props.expanded.contactdetail && <div className="accordion-body">
      <div className="accordion-content">
        <div className="columns">
          <div className="column is-two-third">
            <p className="multiExpLbl "> Firm Name
            <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
            </p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="Firms Name"
                name="userFirmName"
                value={props.userDetails.userFirmName ? props.userDetails.userFirmName : ""}
                onChange={props.onChangeUserDetail}
                disabled
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userFirmName && <small className="text-error">{props.errorUserDetail.userFirmName}</small>}
          </div>
        </div>
        <hr />
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl ">
              First Name<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
            </p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="First Name"
                name="userFirstName"
                value={props.userDetails.userFirstName ? props.userDetails.userFirstName : ""}
                onChange={props.onChangeUserDetail}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userFirstName && <small className="text-error">{props.errorUserDetail.userFirstName}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Middle Name</p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="Middle Name"
                name="userMiddleName"
                value={props.userDetails.userMiddleName ? props.userDetails.userMiddleName : ""}
                onChange={props.onChangeUserDetail}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userMiddleName && <small className="text-error">{props.errorUserDetail.userMiddleName}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Last Name
            <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
            </p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="Last Name"
                name="userLastName"
                value={props.userDetails.userLastName ? props.userDetails.userLastName : ""}
                onChange={props.onChangeUserDetail}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userLastName && <small className="text-error">{props.errorUserDetail.userLastName}</small>}
          </div>
        </div>

        <hr />

        <div className="columns">
          <div className="column is-third">
            <p className="multiExpLbl ">Email
            <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
            </p>
            {props.userDetails.userEmails.map((item, idx) => (
              <div key={2 * idx}>
                <div className="field">
                  <div className="control d-flex">
                    {props.primaryEmails.isPrimary && props.primaryEmails.emailId === item.emailId ? <small>{item.emailId}</small>
                      : <input
                        className="input is-small is-link"
                        name="emailId"
                        type="email"
                        placeholder="e.g. alexsmith@gmail.com"
                        value={item.emailId ? item.emailId : ""}
                        onChange={event => {
                          props.onChangeAddMore(event, "userEmails", idx)
                        }}
                      />}
                    {idx > 0
                      ? <span className="has-text-link fa-delete deleteadressaliases"
                        onClick={() => {
                          props.deleteAliases("userEmails", idx)
                        }}
                      >
                        <i className="far fa-trash-alt" />
                      </span>
                      : ""
                    }
                  </div>
                  {props.primaryEmails.isPrimary && props.primaryEmails.emailId === item.emailId && <p className="emmaTablesTd">
                    {" "}
                    Is Primary
                    <input
                      type="checkbox"
                      name="emailPrimary"
                      disabled
                      value={item.emailPrimary}
                      checked={item.emailPrimary ? item.emailPrimary : ""}
                      onChange={event => {
                        props.onChangeAddMore(event, "userEmails", idx)
                      }}
                    />
                  </p>}
                  {!props.primaryEmails.isPrimary && <p className="emmaTablesTd">
                    {" "}
                    Is Primary
                    <input
                      type="checkbox"
                      name="emailPrimary"
                      value={item.emailPrimary}
                      checked={item.emailPrimary ? item.emailPrimary : ""}
                      onChange={event => {
                        props.onChangeAddMore(event, "userEmails", idx)
                      }}
                    />
                  </p>}
                </div>
                <span>
                  {props.errorUserDetail && props.errorUserDetail.userEmails[idx].emailId && <small className="text-error">{props.errorUserDetail.userEmails[idx].emailId}</small>}
                </span>
              </div>
            ))}
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "userEmails")
                }}
              >
                Add More
              </button>
              <button
                className="button is-light is-small"
                onClick={event => {
                  props.resetAliases(event, "userEmails")
                }}
              >
                Cancel
              </button>
            </div>
            {props.isPrimaryEmailAddress && <small className="text-error">{props.isPrimaryEmailAddress}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Phone</p>
            {props.userDetails.userPhone.map((item, idx) => (
              <div key={3 * idx}>
                <div className="field is-grouped-left" style={{ marginBottom: "0px" }}>
                  <div className="control">
                    <NumberFormat
                      format="+1 (###) ###-####"
                      mask="_"
                      className="input is-small is-link is-fullwidth"
                      name="phoneNumber"
                      placeholder="Numbers only"
                      value={item.phoneNumber ? item.phoneNumber : ""}
                      onChange={event => {
                        props.onChangeAddMore(event, "userPhone", idx)
                      }} />
                  </div>
                  {props.errorUserDetail && props.errorUserDetail.userPhone[idx].phoneNumber && <small className="text-error">{props.errorUserDetail.userPhone[idx].phoneNumber}</small>}
                  <div className="control">
                    <NumberFormat
                      format="####"
                      className="input is-small is-link"
                      name="extension"
                      placeholder="Ext"
                      size="5"
                      value={item.extension ? item.extension : ""}
                      onChange={event => {
                        props.onChangeAddMore(event, "userPhone", idx)
                      }} />
                    {idx > 0
                      ? <span className="has-text-link fa-delete deleteadressaliases"
                        onClick={() => {
                          props.deleteAliases("userPhone", idx)
                        }}
                      >
                        <i className="far fa-trash-alt" />
                      </span>
                      : ""
                    }
                  </div>
                  {props.errorUserDetail && props.errorUserDetail.userPhone[idx].extension && <small className="text-error">{props.errorUserDetail.userPhone[idx].extension}</small>}
                </div>
                <div className="field is-grouped-left" style={{ marginBottom: "12px" }}>
                  <p className="emmaTablesTd">
                    {" "}
                    Is Primary
                    <input
                      type="checkbox"
                      name="phonePrimary"
                      value={item.phonePrimary ? item.phonePrimary : ""}
                      checked={item.phonePrimary}
                      onChange={event => {
                        props.onChangeAddMore(event, "userPhone", idx)
                      }}
                    />
                  </p>
                </div>
              </div>
            ))}
            <div className="control" >
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "userPhone")
                }}
              >
                Add More
              </button>
              <button
                className="button is-light is-small"
                onClick={event => {
                  props.resetAliases(event, "userPhone")
                }}
              >
                Cancel
              </button>
            </div>
            {props.isPrimaryUserPhone && <small className="text-error">{props.isPrimaryUserPhone}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Fax</p>
            {props.userDetails.userFax.map((item, idx) => (
              <div key={4 * idx}>
                <div className="field">
                  <div className="control d-flex">
                    <NumberFormat
                      format="+1 (###) ###-####"
                      mask="_"
                      className="input is-small is-link"
                      name="faxNumber"
                      size="10"
                      type="text"
                      placeholder="Numbers only"
                      value={item.faxNumber ? item.faxNumber : ""}
                      onChange={event => {
                        props.onChangeAddMore(event, "userFax", idx)
                      }} />
                    {idx > 0
                      ? <span className="has-text-link fa-delete deleteadressaliases"
                        onClick={() => {
                          props.deleteAliases("userFax", idx)
                        }}
                      >
                        <i className="far fa-trash-alt" />
                      </span>
                      : ""
                    }
                  </div>
                  <p className="emmaTablesTd">
                    {" "}
                    Is Primary
                    <input
                      type="checkbox"
                      name="faxPrimary"
                      value={item.faxPrimary}
                      checked={item.faxPrimary ? item.faxPrimary : ""}
                      onChange={event => {
                        props.onChangeAddMore(event, "userFax", idx)
                      }}
                    />
                  </p>
                </div>
                <span>
                  {props.errorUserDetail && props.errorUserDetail.userFax[idx].faxNumber && <small className="text-error">{props.errorUserDetail.userFax[idx].faxNumber}</small>}
                </span>
              </div>
            ))}
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={event => {
                  props.addMore(event, "userFax")
                }}
              >
                Add More
              </button>
              <button
                className="button is-light is-small"
                onClick={event => {
                  props.resetAliases(event, "userFax")
                }}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>

        <hr />

        <div className="columns">
          <div className="column">
            <p className="multiExpLbl ">Employee ID</p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="Employee ID"
                name="userEmployeeID"
                value={props.userDetails.userEmployeeID ? props.userDetails.userEmployeeID : ""}
                onChange={props.onChangeUserDetail}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userEmployeeID && <small className="text-error">{props.errorUserDetail.userEmployeeID}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Job Title</p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="Job Title"
                name="userJobTitle"
                value={props.userDetails.userJobTitle ? props.userDetails.userJobTitle : ""}
                onChange={props.onChangeUserDetail}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userJobTitle && <small className="text-error">{props.errorUserDetail.userJobTitle}</small>}
          </div>
          <div className="column">
            <p className="multiExpLbl ">Manager's Email</p>
            <div className="field">
              <div className="control">
                <input
                  className="input is-small is-link"
                  type="email"
                  placeholder="e.g. alexsmith@gmail.com"
                  name="userManagerEmail"
                  value={props.userDetails.userManagerEmail ? props.userDetails.userManagerEmail : ""}
                  onChange={props.onChangeUserDetail}
                />
              </div>
              {props.errorUserDetail && props.errorUserDetail.userManagerEmail && <small className="text-error">{props.errorUserDetail.userManagerEmail}</small>}
            </div>
          </div>
        </div>

        <hr />
        <div className="columns">
          <Switch condition={props.userType}>
            <Case value="users">
              <div className="column">
                <p className="multiExpLbl ">Joining Date</p>
                <div className="control">
                  <input
                    id="datepickerDemo"
                    className="input is-small is-link"
                    type="date"
                    name="userJoiningDate"
                    value={props.userDetails.userJoiningDate ? moment(props.userDetails.userJoiningDate).format("YYYY-MM-DD") : ""}
                    onChange={props.onChangeUserDetail}
                  />
                </div>
                {props.errorUserDetail && props.errorUserDetail.userJoiningDate && <small className="text-error">{props.errorUserDetail.userJoiningDate}</small>}
              </div>
              <div className="column">
                <p className="multiExpLbl ">Exit Date</p>
                <div className="control">
                  <input
                    id="datepickerDemo"
                    className="input is-small is-link"
                    type="date"
                    name="userExitDate"
                    value={props.userDetails.userExitDate ? moment(props.userDetails.userExitDate).format("YYYY-MM-DD") : ""}
                    onChange={props.onChangeUserDetail}
                  />
                </div>
                {props.errorUserDetail && props.errorUserDetail.userExitDate && <small className="text-error">{props.errorUserDetail.userExitDate}</small>}
              </div>
            </Case>
            <Default>
              <div className="column">
                <p className="multiExpLbl ">Department</p>
                <div className="control">
                  <input
                    className="input is-small is-link"
                    type="text"
                    placeholder="Department"
                    name="userDepartment"
                    value={props.userDetails.userDepartment ? props.userDetails.userDepartment : ""}
                    onChange={props.onChangeUserDetail}
                  />
                </div>
                {props.errorUserDetail && props.errorUserDetail.userDepartment && <small className="text-error">{props.errorUserDetail.userDepartment}</small>}
              </div>
              <div className="column">
                <p className="multiExpLbl ">Employee Type</p>
                <div className="control">
                  <input
                    className="input is-small is-link"
                    type="text"
                    placeholder="Employee Type"
                    name="userEmployeeType"
                    value={props.userDetails.userEmployeeType ? props.userDetails.userEmployeeType : ""}
                    onChange={props.onChangeUserDetail}
                  />
                </div>
                {props.errorUserDetail && props.errorUserDetail.userEmployeeType && <small className="text-error">{props.errorUserDetail.userEmployeeType}</small>}
              </div>
            </Default>
          </Switch>
          <div className="column">
            <p className="multiExpLbl ">Cost Center</p>
            <div className="control">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="Cost Center"
                name="userCostCenter"
                value={props.userDetails.userCostCenter ? props.userDetails.userCostCenter : ""}
                onChange={props.onChangeUserDetail}
              />
            </div>
            {props.errorUserDetail && props.errorUserDetail.userCostCenter && <small className="text-error">{props.errorUserDetail.userCostCenter}</small>}
          </div>
        </div>
      </div>
    </div>}
  </article>
)

export default UserContact
