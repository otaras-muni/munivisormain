import React from "react"
import { NavLink, Redirect } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
// import AdmAddTenFirm from "./containers/AdmAddTenFirm"
import AuditLog from "Global/AuditLog"
import FirmNew from "../../Firm/Firm"
import FirmAddOns from "./containers/FirmAddOnsContainer"

const TABS = [
  { path: "firms", label: "Entity" },
  { path: "activity-log", label: "Activity Log" }
  // { path: "add-ons", label: "Add Ons" }
]
const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}
const FirmsView = props => {
  const { firmId, audit, svControls } = props
  let { navComponent } = props
  navComponent = navComponent || "details"

  const submitAudit = (audit === "no") ? false : (audit === "currentState") ? true : (audit === "supervisor" && (svControls && svControls.supervisor)) || false
  if (!submitAudit) {
    const index = TABS.findIndex(tab => tab.path === "activity-log")
    if(index !== -1){
      TABS.splice(index, 1)
    }
  } else if (submitAudit && TABS.length === 1) {
    TABS.push({ path: "activity-log", label: "Activity Log" })
  }
  const renderTabs = (tabs, navComponent, id) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink
          key={t.path}
          to={`/admin-firms/${firmId}/${t.path}`}
          activeStyle={activeStyle}
        >
          {" "}
          {t.label}{" "}
        </NavLink>
      </li>
    ))

  const renderViewSelection = (firmId, navComponent) => (
    <nav className="tabs is-boxed">
      <ul>{renderTabs(TABS, navComponent, firmId)}</ul>
    </nav>
  )
  return (
    <div>
      <div className="hero is-link">
        <div className="hero-foot hero-footer-padding">
          <div className="container">
            {renderViewSelection(firmId, navComponent)}
          </div>
        </div>
      </div>
      <div id="main">
        <Switch condition={navComponent}>
          <Case value="firms">
            <FirmNew options={navComponent} nav2={firmId} />
          </Case>
          <Case value="add-ons">
            <FirmAddOns options={navComponent} firmId={firmId} />
          </Case>
          <Case value="activity-log">
            <AuditLog type={firmId} />
          </Case>
          <Default>
            <Redirect to="/not-found" />
          </Default>
        </Switch>
      </div>
    </div>
  )
}

export default FirmsView
