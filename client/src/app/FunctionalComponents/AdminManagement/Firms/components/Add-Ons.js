import React from "react"

const AddOns = (props) => (
  <article className={props.addOnsToggle ? "accordion is-active" : "accordion"}>
    <div className="accordion-header">
      <p
        onClick={event => {
          props.toggleButton(event, "addOns")
        }}
      >Firm Level</p>
    </div>
    <div className="accordion-body">
      {props.addOnsToggle &&
      <div className="accordion-content">

        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl " title="">Add-On Name</p>
              </th>
              <th>
                <p className="multiExpLbl " title="">Enabled</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {
              props.firmAddOns.map((element, idx) => (
                <tr key={idx}>
                  <td>
                    {element.serviceName}
                  </td>
                  <td>
                    <p className="emmaTablesTd">
                      <input type="checkbox"
                        name="addOns"
                        value={element.serviceName}
                        checked={element.serviceEnabled}
                        onChange={(event)=>{
                          props.onChangeAddOns(event, idx)
                        }}
                      />
                    </p>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

      </div>}
    </div>
  </article>
)

export default AddOns