import React from "react"
import {
  FormGroup
} from "react-bootstrap"

const msrbRegCat = [
  { label: "Pick MSRB Registrant Type", vlaue: "" },
  { label: "Issuer", value: "Issuer" },
  { label: "Broker Dealer Only", value: "Broker Dealer Only" },
  { label: "Municipal Securities Dealer", value: "Municipal Securities Dealer" },
  { label: "Municipal Advisor Only", value: "Municipal Advisor Only" },
  { label: "Broker and Municipal Advisor", value: "Broker and Municipal Advisor" }]

const emplCat = [
  { label: "Pick Number of Employees", value: "" },
  { label: "< 5", value: "< 5" },
  { value: "5 to 20", label: "5 to 20" },
  { value: "20 +", label: "20 +" }]
const revRef = [
  { label: "Pick Annual Revenue", value: "" },
  { label: "Less than 5 M", value: "Less than 5 M" },
  { label: "5M to 20M", value: "5M to 20M" },
  { label: "20M +", value: "20M +" }]
const LKUPBUSSTRUCT = [
  { label: "Pick Business Structure", value: "" },
  { label: "Sole Proprietorship", value: "Sole Proprietorship" },
  { label: "General Partnership", value: "General Partnership" },
  { label: "Limited Partnership", value: "Limited Partnership" },
  { label: "LLC", value: "LLC" },
  { label: "LLP", value: "LLP" },
  { label: "LLLP", value: "LLLP" },
  { label: "Corporation Private", value: "Corporation Private" },
  { label: "Corporation Public", value: "Corporation Public" },
  { label: "Corporation Non Profit", value: "Corporation Non Profit" },
  { label: "Trust", value: "Trust" },
  { label: "Joint Venture", value: "Joint Venture" },
  { label: "Tenants in Common", value: "Tenants in Common" },
  { label: "Municipality", value: "Municipality" },
  { label: "Governmental or Quasi", value: "Governmental or Quasi" },
  { label: "NA", value: "NA" }
]
const SelectOption = (props) => (
  <select name={props.title}
    onChange={
      (event) => {
        props.onChangeFirmDetail(event)
      }
    }
    value={props.currentValue}
  >
    {props.items.map((item, index) => (
      <option value={item.value} key={index}>{item.label}</option>
    ))
    }
  </select>
)
const AdmTrnFirm = (props) => (
  <div>
    <div className="columns">
      <div className="column">
        <p className="multiExpLbl">MSRB Firm Name</p>
        <div className="control">
          <div className="select">
            <select name="msrbFirmName"
              onChange={
                (event) => {
                  props.getFirmDetails(event)
                }
              }
              value={props.firmDetails._id}
            >
              {props.firmList.map((item, index) => (
                <option value={item.value} key={index}>{item.label}</option>
              ))
              }
            </select>
          </div>
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small">Add Alias</button>
            </div>
            <div className="control">
              <button className="button is-light is-small">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <p className="multiExpLbl">MSRB Registrant Type</p>
        <FormGroup className={props.errorFirmDetail.msrbRegistrantType != "" ? "has-error" : ""}>
          <div className="control">
            <div className="select">
              <SelectOption
                items={msrbRegCat}
                title="msrbRegistrantType"
                currentValue={props.firmDetails.msrbRegistrantType}
                onChangeFirmDetail={props.onChangeFirmDetail}
              />
            </div>
          </div>
          {props.errorFirmDetail.msrbRegistrantType != "" ? <span className="help-block">{props.errorFirmDetail.msrbRegistrantType}</span> : ""}
        </FormGroup>
      </div>
      <div className="column">
        <p className="multiExpLbl">MSRB ID</p>
        <div className="field">
          <FormGroup className={props.errorFirmDetail.msrbId != "" ? "has-error" : ""}>
            <div className="control">
              <input className="input is-small is-link" name="msrbId" type="text" placeholder="K0525"
                value={props.firmDetails.msrbId}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail.msrbId != "" ? <span className="help-block">{props.errorFirmDetail.msrbId}</span> : ""}
          </FormGroup>
        </div>
      </div>
    </div>
    <hr />
    <div className="columns">
      <div className="column">
        <p className="multiExpLbl">Firm Name (if not MSRB registered)</p>
        <div className="field">
          <FormGroup className={props.errorFirmDetail.firmName != "" ? "has-error" : ""}>
            <div className="control">
              <input className="input is-small is-link" name="firmName" type="text" placeholder="Enter Firm Name"
                value={props.firmDetails.firmName}
                onChange={props.onChangeFirmDetail}
              />
            </div>
            {props.errorFirmDetail.firmName != "" ? <span className="help-block">{props.errorFirmDetail.firmName}</span> : ""}
          </FormGroup>
        </div>

        {
          (props.firmDetails.entityAliases && props.firmDetails.entityAliases.length)
            ?
            props.firmDetails.entityAliases.map((item, idx) => (
              <div className="field is-grouped" key={idx}>
                <FormGroup className={props.errorFirmDetail.entityAliases[idx] != "" ? "has-error" : ""}>
                  <div className="control">
                    <input className="input is-small is-link" name="entityAliases" type="text" placeholder="Enter Aliases"
                      value={item}
                      onChange={(event) => {
                        props.onChangeAliases(event, idx)
                      }
                      }
                    />
                  </div>
                  {props.errorFirmDetail.entityAliases[idx] != "" ? <span className="help-block">{props.errorFirmDetail.entityAliases[idx]}</span> : ""}
                </FormGroup>
              </div>
            ))
            : ""
        }
        <div className="field is-grouped">
          <div className="control">
            <button className="button is-link is-small"
              onClick={
                (event) => {
                  props.addMore(event, "aliases")
                }
              }>Add More</button>
          </div>
          <div className="control">
            <button className="button is-light is-small"
              onClick={props.resetAliases}
            >Cancel</button>
          </div>
        </div>
      </div>
    </div>

    <hr />

    <div className="columns">
      <div className="column">
        <p className="multiExpLbl">Tax ID</p>
        <FormGroup className={props.errorFirmDetail.taxId != "" ? "has-error" : ""}>
          <div className="control">
            <input className="input is-small is-link" name="taxId" type="text" placeholder="Enter Tax ID"
              value={props.firmDetails.taxId}
              onChange={props.onChangeFirmDetail}
            />
          </div>
          {props.errorFirmDetail.taxId != "" ? <span className="help-block">{props.errorFirmDetail.taxId}</span> : ""}
        </FormGroup>
      </div>
      <div className="column">
        <p className="multiExpLbl">Business Structure</p>
        <div className="field">
          <FormGroup className={props.errorFirmDetail.businessStructure != "" ? "has-error" : ""}>
            <div className="control">
              <div className="select">
                <SelectOption
                  items={LKUPBUSSTRUCT}
                  title="businessStructure"
                  currentValue={props.firmDetails.businessStructure}
                  onChangeFirmDetail={props.onChangeFirmDetail}
                />
              </div>
            </div>
            {props.errorFirmDetail.businessStructure != "" ? <span className="help-block">{props.errorFirmDetail.businessStructure}</span> : ""}
          </FormGroup>
        </div>
      </div>
      <div className="column">
        <p className="multiExpLbl">Number of Employees</p>
        <div className="field">
          <FormGroup className={props.errorFirmDetail.numEmployees != "" ? "has-error" : ""}>
            <div className="control">
              <div className="select">
                <SelectOption
                  items={emplCat}
                  title="numEmployees"
                  currentValue={props.firmDetails.numEmployees}
                  onChangeFirmDetail={props.onChangeFirmDetail}
                />
              </div>
            </div>
            {props.errorFirmDetail.numEmployees != "" ? <span className="help-block">{props.errorFirmDetail.numEmployees}</span> : ""}
          </FormGroup>
        </div>
      </div>
      <div className="column">
        <p className="multiExpLbl">Annual Revenue</p>
        <div className="field">
          <FormGroup className={props.errorFirmDetail.annualRevenue != "" ? "has-error" : ""}>
            <div className="control">
              <div className="select">
                <SelectOption
                  items={revRef}
                  title="annualRevenue"
                  currentValue={props.firmDetails.annualRevenue}
                  onChangeFirmDetail={props.onChangeFirmDetail}
                />
              </div>
            </div>
            {props.errorFirmDetail.annualRevenue != "" ? <span className="help-block">{props.errorFirmDetail.annualRevenue}</span> : ""}
          </FormGroup>
        </div>
      </div>
    </div>
    <hr />
  </div>
)
export default AdmTrnFirm