import React from "react"
import UserAddress from "../components/UserAddress"
import UserContact from "../components/UserContact"
import ListAddresses from "../components/ThirdPartyListAddress"
import { connect } from "react-redux"
import {
  FormGroup
} from "react-bootstrap"
import {
  saveUserDetail,
  getUserDetailById,
  getAllThirdPartyList
} from "AppState/actions/AdminManagement/admTrnActions"

class ThirdPartyContacts extends React.Component {
  constructor(props) {
    super(props)

    this.state = this.initialState()
    this.state.errorUserDetail = Object.assign({}, this.initialState().userDetails)
    this.cleanErrorUserDetail = Object.assign({}, this.initialState().userDetails)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
    this.logAndSave = this.logAndSave.bind(this)


  }

    /** ***********************************This is for Initial State************************************* */

    initialState = () => ({
      userDetails: {
        userId: String, // Should we make this the same as the primary email
        userFlags: [String], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
        userRole: String, // [ Admin, ReadOnly, Backup ]
        userEntitlement: String, // [ Global, Transaction ]
        userFirstName: String,
        userMiddleName: String,
        userLastName: String,
        userEmails: [{
          emailId: String,
          emailPrimary: Boolean,
        }],
        userEmployeeID: String,
        userJobTitle: String,
        userManagerEmail: String,
        userJoiningDate: Date,
        userExitDate: Date,
        userCostCenter: String,
        addresses: [{
          addressName: String,
          addressType: String, // Office address, headquarter or residence
          addressLine1: String,
          addressLine2: String,
          country: String,
          state: String,
          city: String,
          zipCode: { zip1: String, zip2: String }
        }],
        userAddOns: [{
          serviceType: String,
          serviceEnabled: String
        }],
        userLoginCredentials: [{
          _id: String,
          userId: String, // Should come from the user Collection
          userEmailId: String, // We can get rid of this if we are able to just use the email ID.
          password: String,  // This is the salted password.
          passwordConf: String,
          passwordResetString: String,
          passwordResetDate: Date
        }],
        userAddDate: Date,
        userUpdateDate: Date
      }
    })

    componentDidMount() {
      console.log("Component Did Mount==>>>")
      this.props.getAllThirdPartyList()
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.thirdPartyUserDetails.userDetails) {
        const addressList = nextProps.thirdPartyUserDetails.userDetails.addresses
        const thirdPartyList = this.state.thirdPartyList
        nextProps.thirdPartyUserDetails.userDetails.addresses = this.initialState().userDetails.addresses
        console.log("nextProps.thirdPartyUserDetails.userDetails==>>", nextProps.thirdPartyUserDetails.userDetails)
        this.setState(prevState => ({
          ...prevState,
          userDetails: nextProps.thirdPartyUserDetails.userDetails,
          addressList,
          thirdPartyList: prevState.thirdPartyList.push({ "label": nextProps.thirdPartyUserDetails.userDetails.msrbFirmName, value: nextProps.thirdPartyUserDetails.userDetails._id }),
          errorUserDetail: this.initialState().userDetails
        }))
        console.log("addressList==>>", addressList)
      }
      if (nextProps.thirdPartyUserDetails.thirdPartyList) {
        this.setState(prevState => ({
          ...prevState,
          thirdPartyList: nextProps.thirdPartyUserDetails.thirdPartyList
        }))
      }
      if (nextProps.thirdPartyUserDetails.error) {
        const errorUserDetail = this.state.errorUserDetail
        const arr = ["zipCode", "userEmails"]
        const error = nextProps.thirdPartyUserDetails.error
        error.details.forEach((item) => {
          arr.includes(item.path[2])
            ? errorUserDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]] = item.message
            : errorUserDetail[item.path[0]][item.path[1]][item.path[2]] = item.message
        })
        this.setState((prevState) => ({
          ...prevState,
          errorUserDetail
        }))
      }
    }

    componentDidUpdate(nextState, nextProps) {

    }
    componentWillUpdate(nextState, nextProps) {
    }
    /** ************************ this is for submit form ********************************* */

    handleSubmit(event) {
      event.preventDefault()
      this.submitted = true
      const userDetails = this.state.userDetails
      const thirdPartyList = this.state.thirdPartyList
      const errorUserDetail = this.state.errorUserDetail
      try {
        console.log("userDetails==>>>", userDetails)
        const errorData = validateUserDetail(userDetails)
        if (errorData.error) {
          console.log("errorData.error==>>", errorData.error.details)
          errorData.error.details.forEach((item) => {
            switch (item.path.length) {
            case 1:
              errorUserDetail[item.path[0]] = item.message
              break
            case 3:
              errorUserDetail[item.path[0]][item.path[1]][item.path[2]] = item.message
              break
            case 4:
              errorUserDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]] = item.message
              break
            case 5:
              errorUserDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]][item.path[4]] = item.message
              break
            }
            this.setState({
              errorUserDetail
            })
          })
        } else {
          console.log("UserDetails==>>", userDetails)
          this.props.saveUserDetail({ userDetails })
        }

      } catch (ex) {
        console.log("=======>>>", ex)
      }
    }
    /** ***********************this is for login and save the firm details data*********************************** */
    logAndSave() {
      const userName = "Anon"
      const date = new Date()
      console.log("this.props.thirdPartyUserDetails==>>", this.props.thirdPartyUserDetails)
      const oldData = this.props.thirdPartyUserDetails.userDetails
      const { userDetails } = this.state
      const changeLog = []
    }
    /** ************************ this is for add new address ********************************* */
    addNewUserAddress(event) {
      event.preventDefault()
      const initialState = this.initialState()
      this.setState(prevState => {
        const userDetails = { ...prevState.userDetails }
        const errorUserDetail = { ...prevState.errorUserDetail }
        userDetails.addresses.push(this.initialState().userDetails.addresses[0])
        errorUserDetail.addresses.push(this.initialState().userDetails.addresses[0])
        return { userDetails, errorUserDetail }
      })
    }
    /** ************************ this is for reset form ********************************* */
    resetUserAddress = (e) => {
      e.preventDefault()
      this.setState({
        userDetails: {
          addresses: [this.initialState().userDetails.addresses[0]]
        },
        errorUserDetail: {
          addresses: [this.initialState().userDetails.addresses[0]]
        }
      })
    }
    /** ************************ this is for onchange msrb firmname ********************************* */
    getUserDetails = (e) => {
      if (e.target.value != "") {
        this.props.getUserDetailById(e.target.value)
      } else {
        this.setState(prevState => ({
          ...prevState,
          userDetails: this.initialState().userDetails,
          addressList: [],
          errorUserDetail: this.cleanErrorUserDetail
        }))
      }
    }

    /** ************************ this is for onchange ********************************* */
    onChangeUserDetail = (e, key, ) => {
      // e.preventDefault()
      const userDetails = this.state.userDetails
      const errorUserDetail = this.state.errorUserDetail
      let keyUserDetail, errorKeyUserDetail
      let validator
      if (typeof key !== "undefined") {
        const value = (e.target.type === "checkbox" ? (!userDetails.addresses[key][e.target.name]) : e.target.value)
        if (e.target.name == "zip1" || e.target.name == "zip2") {
          keyUserDetail = userDetails.addresses[key].zipCode
          errorKeyUserDetail = errorUserDetail.addresses[key].zipCode
          validator = {
            addresses: [
              {
                zipCode: { [e.target.name]: e.target.value }
              }
            ]
          }
        } else {
          keyUserDetail = userDetails.addresses[key]
          errorKeyUserDetail = errorUserDetail.addresses[key]
          validator = {
            addresses: [
              { [e.target.name]: e.target.value }
            ]
          }
        }
        keyUserDetail[e.target.name] = value
      } else {
        userDetails[e.target.name] = e.target.value
        validator = { [e.target.name]: e.target.value }
        errorKeyUserDetail = errorUserDetail
      }
      if(deepFields){
        deepFields.forEach ( d => {
          userDetails.userAddresses[key][d] = ""
        })
      }
      this.setState({
        userDetails
      })
      if (this.submitted) {
        const errorData = validateUserDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          if (err.context.key === e.target.name) {
            errorKeyUserDetail[e.target.name] = "Required" || err.message
            this.setState({
              errorUserDetail
            })
          } else {
            errorKeyUserDetail[e.target.name] = ""
            this.setState({
              errorUserDetail
            })
          }
        } else {
          errorKeyUserDetail[e.target.name] = ""
          this.setState({
            errorUserDetail
          })
        }
      }
    }

    /** **************************this is function is used for addMore function ***************** */
    addMore = (e, type, key) => {
      e.preventDefault()
      this.setState(prevState => {
        const addMoreData = { ...prevState.userDetails.addresses[key] }
        const errorUserDetail = { ...prevState.errorUserDetail.addresses[key] }
        addMoreData[type].push(this.initialState().userDetails.addresses[0][type][0])
        errorUserDetail[type].push(this.initialState().userDetails.addresses[0][type][0])
        return { addMoreData }
      })
    }
    /** ************************ this is for onchange add more ********************************* */
    onChangeAddMore = (e, type, key, idx) => {
      e.preventDefault()
      const addresses = this.state.userDetails.addresses[key]
      const errorUserDetail = this.state.errorUserDetail
      let errorKeyUserDetail, validator
      addresses[type][idx][e.target.name] = e.target.value
      validator = {
        addresses: [
          {
            [type]: [
              {
                [e.target.name]: e.target.value
              }
            ]
          }
        ]
      }
      this.setState({
        addresses
      })

      if (this.submitted) {
        const errorData = validateUserDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          errorUserDetail.addresses[key][type][idx][e.target.name] ="Required" || err.message
          this.setState({
            errorUserDetail
          })
        } else {
          errorUserDetail.addresses[key][type][idx][e.target.name] = ""
          this.setState({
            errorUserDetail
          })
        }
      }
    }
    // **************************function used to update business addresses *****************************
    updateAddress = (e, id) => {
      e.preventDefault()
      const address = this.state.addressList.find((item) => item._id == id)
      this.setState(prevState => {
        const { errorUserDetail, userDetails } = prevState
        userDetails.addresses = [address]
        const arr = ["userEmails"]
        arr.forEach(item => {
          console.log("Address==>>", address[item])
          if (address[item].length > 1) {
            for (let i = 1; i < address[item].length; i++) {
              console.log("Item==>>", this.initialState().userDetails.addresses[0][item][0])
              errorUserDetail.addresses[0][item].push(this.initialState().userDetails.addresses[0][item][0])
            }
          }

        })
        return { userDetails, errorUserDetail }
      })

    }

    render() {
      return (
        <div id="main">
          <section className="container">
            <section className="accordions box">
              <UserContact />
              <ListAddresses addressList={this.state.addressList} updateAddress={this.updateAddress} />
              <hr />

              <article className="accordion is-active">

                <div className="accordion-header">
                  <p>Address</p>
                  <div className="field is-grouped">
                    <div className="control">
                      <button className="button is-link is-small" onClick={this.addNewUserAddress.bind(this)}>Add</button>
                    </div>
                    <div className="control">
                      <button className="button is-light is-small" onClick={this.resetUserAddress.bind(this)}>Reset</button>
                    </div>
                  </div>
                </div>
                <div className="accordion-body">
                  {
                    this.state.userDetails.addresses.map((userAddress, idx) => (
                      <UserAddress
                        userAddress={userAddress}
                        idx={idx}
                        onChangeUserAddress={this.onChangeUserDetail}
                        addMore={this.addMore}
                        onChangeAddMore={this.onChangeAddMore}
                        errorUserDetail={this.state.errorUserDetail.addresses[idx]}
                      />
                    ))
                  }
                </div>

              </article>

              <br />
              <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button className="button is-link" onClick={this.handleSubmit}>Save</button>
                    </div>
                    <div className="control">
                      <button className="button is-light">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </section>
        </div>
      )
    }


}

const mapStateToProps = (state) => {
  const thirdPartyUserDetails = state.thirdPartyUserDetails
  return { thirdPartyUserDetails }
}
const mapDispatchToProps = {
  saveUserDetail,
  getUserDetailById,
  getAllThirdPartyList
}

export default connect(mapStateToProps, mapDispatchToProps) (ThirdPartyContacts)
