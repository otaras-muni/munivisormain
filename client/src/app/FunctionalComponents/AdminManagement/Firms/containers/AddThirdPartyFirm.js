import React from "react"
import ThirdPartyAddressForm from "../components/AdmTrnAddressForm"

import ThirdParty from "../components/ThirdPartyDetails"
import ListAddresses from "../components/ListAddresses"
import { connect } from "react-redux"
import {
  saveFirmDetail,
  getFirmDetailById,
  saveThirdPartyDetail
} from "AppState/actions/AdminManagement/admTrnActions"
import {
  validateFirmDetail
} from "Validation/firmDetail"
import ThirdPartyDetail from "../components/ThirdPartyDetails"

class AddThirdPartyFirm extends React.Component {

  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.errorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.cleanErrorFirmDetail = Object.assign({}, this.initialState().firmDetails)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
  }

    /** **************************************this is for initial default value********************* */
    initialState = () => ({
      firmDetails: {
        entityAliases: [],
        isMuniVisorClient: false,
        entityFlags: {
          marketRole: [],
          issuerFlags: []
        },
        msrbRegistrantType: "",
        msrbId: "",
        firmName: "",
        addresses: [
          {
            addressName: "",
            isPrimary: true,
            isHeadQuarter: true,
            isActive: true,
            website: "",
            officePhone: [
              {
                countryCode: "",
                phoneNumber: "",
                extension: ""
              }
            ],
            officeFax: [
              {
                faxNumber: ""
              }
            ],
            officeEmails: [{ emailId: "" }],
            addressLine1: "",
            addressLine2: "",
            county: "",
            country: "",
            state: "",
            city: "",
            zipCode: { zip1: "", zip2: "" }
          }
        ],
        firmAddOns: [
          {
            serviceName: "test",
            serviceEnabled: false
          }
        ]
      },
      addressList: [],
      firmList: []
    })
    componentWillMount() {
    }
    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.admFirmDetails.firmDetails) {
        const addressList = Object.assign({}, nextProps.admFirmDetails.firmDetails.addresses)
        nextProps.admFirmDetails.firmDetails.addresses = this.initialState().firmDetails.addresses
        this.setState(prevState => ({
          ...prevState,
          firmDetails: nextProps.admFirmDetails.firmDetails
        }))
      }
      if (nextProps.admFirmDetails.firmList) {
        this.setState(prevState => ({
          ...prevState,
          firmList: nextProps.admFirmDetails.firmList
        }))
      }
      if (nextProps.admFirmDetails.error) {
        const errorFirmDetail = this.state.errorFirmDetail
        const arr = ["zipCode", "officeFax", "officeEmails"]
        const error = nextProps.admFirmDetails.error
        error.details.forEach((item) => {
          arr.includes(item.path[2])
            ? errorFirmDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]] = item.message
            : errorFirmDetail[item.path[0]][item.path[1]][item.path[2]] = item.message
        })
        this.setState((prevState) => ({
          ...prevState,
          errorFirmDetail
        }))
      }
    }

    componentDidUpdate(nextState, nextProps) {

    }
    componentWillUpdate(nextState, nextProps) {
    }
    /** ************************ this is for submit form ********************************* */

    handleSubmit(event) {
      event.preventDefault()
      this.submitted = true
      const firmDetails = this.state.firmDetails
      firmDetails.msrbFirmName = firmDetails.firmName
      const firmList = this.state.firmList
      const errorFirmDetail = this.state.errorFirmDetail
      try {
        const errorData = validateFirmDetail(firmDetails)
        if (errorData.error) {
          errorData.error.details.forEach((item) => {
            switch (item.path.length) {
            case 1:
              errorFirmDetail[item.path[0]] = item.message
              break
            case 2:
              errorFirmDetail[item.path[0]][item.path[1]] = item.message
              break
            case 3:
              errorFirmDetail[item.path[0]][item.path[1]][item.path[2]] = item.message
              break
            case 4:
              errorFirmDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]] = item.message
              break
            case 5:
              errorFirmDetail[item.path[0]][item.path[1]][item.path[2]][item.path[3]][item.path[4]] = item.message
              break
            }
            this.setState({
              errorFirmDetail
            })
          })
        } else {
          this.props.saveFirmDetail({ firmDetails })
          this.submitted = false
        }

      } catch (ex) {
        console.log("=======>>>", ex)
      }
    }
    /** ************************ this is for add new address ********************************* */
    addNewBussinessAddress(event) {
      event.preventDefault()
      const initialState = this.initialState()
      this.setState(prevState => {
        const firmDetails = { ...prevState.firmDetails }
        const errorFirmDetail = { ...prevState.errorFirmDetail }
        firmDetails.addresses.push(this.initialState().firmDetails.addresses[0])
        errorFirmDetail.addresses.push(this.initialState().firmDetails.addresses[0])
        return { firmDetails, errorFirmDetail }
      })
    }
    /** ************************ this is for reset form ********************************* */
    resetBussinessAddress = (e) => {
      e.preventDefault()
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          addresses: [this.initialState().firmDetails.addresses[0]]
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          addresses: [this.initialState().firmDetails.addresses[0]]
        }
      }))
    }

    /** ***************************function for select multiple options**************** */
    onSelectMultipleOptions = (value) => {
      const firmDetails = this.state.firmDetails
      const errorFirmDetail = this.state.errorFirmDetail
      let keyFirmDetail, errorKeyFirmDetail
      firmDetails.entityFlags.marketRole = value
      this.setState({
        firmDetails
      })
    }

    /** ************************ this is for onchange ********************************* */
    onChangeFirmDetail = (e, key, selectedOption) => {
      const firmDetails = this.state.firmDetails
      const errorFirmDetail = this.state.errorFirmDetail
      let keyFirmDetail, errorKeyFirmDetail
      let validator
      if (typeof key !== "undefined") {
        const value = (e.target.type === "checkbox" ? (!firmDetails.addresses[key][e.target.name]) : e.target.value)
        if (e.target.name == "zip1" || e.target.name == "zip2") {
          keyFirmDetail = firmDetails.addresses[key].zipCode
          errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
          validator = {
            addresses: [
              {
                zipCode: { [e.target.name]: e.target.value }
              }
            ]
          }
        } else {
          keyFirmDetail = firmDetails.addresses[key]
          errorKeyFirmDetail = errorFirmDetail.addresses[key]
          validator = {
            addresses: [
              { [e.target.name]: e.target.value }
            ]
          }
        }
        keyFirmDetail[e.target.name] = value
        this.setState({
          firmDetails
        })
      } else {
        firmDetails[e.target.name] = e.target.value
        validator = { [e.target.name]: e.target.value }
        errorKeyFirmDetail = errorFirmDetail
        this.setState({
          firmDetails
        })
      }
      if (this.submitted) {
        const errorData = validateFirmDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          if (err.context.key === e.target.name) {
            errorKeyFirmDetail[e.target.name] = "Required" || err.message
            this.setState({
              errorFirmDetail
            })
          } else {
            errorKeyFirmDetail[e.target.name] = ""
            this.setState({
              errorFirmDetail
            })
          }
        } else {
          errorKeyFirmDetail[e.target.name] = ""
          this.setState({
            errorFirmDetail
          })
        }
      }
    }

    /** **************************this is function is used for addMore function ***************** */
    addMore = (e, type, key) => {
      e.preventDefault()
      this.setState(prevState => {
        const addMoreData = { ...prevState.firmDetails }
        const errorFirmDetail = { ...prevState.errorFirmDetail }
        if (type === "aliases") {
          addMoreData.entityAliases.push("")
          errorFirmDetail.entityAliases.push("")
        } else {
          addMoreData.addresses[key][type].push(this.initialState().firmDetails.addresses[0][type][0])
          errorFirmDetail.addresses[key][type].push(this.initialState().firmDetails.addresses[0][type][0])

        }
        return { addMoreData }
      })
    }
    /** ************************ this is for onchange add more ********************************* */
    onChangeAddMore = (e, type, key, idx) => {
      e.preventDefault()
      const addresses = this.state.firmDetails.addresses[key]
      const errorFirmDetail = this.state.errorFirmDetail
      let errorKeyFirmDetail, validator
      addresses[type][idx][e.target.name] = e.target.value
      validator = {
        addresses: [
          {
            [type]: [
              {
                [e.target.name]: e.target.value
              }
            ]
          }
        ]
      }
      this.setState({
        addresses
      })

      if (this.submitted) {
        const errorData = validateFirmDetail(validator)
        if (errorData.error != null) {
          const err = errorData.error.details[0]
          errorFirmDetail.addresses[key][type][idx][e.target.name] ="Required" || err.message
          this.setState({
            errorFirmDetail
          })
        } else {
          errorFirmDetail.addresses[key][type][idx][e.target.name] = ""
          this.setState({
            errorFirmDetail
          })
        }
      }
    }
    // **************************function used to update business addresses *****************************
    updateAddress = (e, id) => {
      e.preventDefault()
      const address = this.state.addressList.find((item) => item._id == id)
      this.setState(prevState => {
        let { errorFirmDetail, firmDetails } = prevState
        errorFirmDetail = this.initialState().firmDetails
        firmDetails.addresses = [address]
        const arr = ["officeEmails", "officePhone", "officeFax"]
        arr.forEach(item => {
          if (address[item].length > 1) {
            for (let i = 1; i < address[item].length; i++) {
              errorFirmDetail.addresses[0][item].push(this.initialState().firmDetails.addresses[0][item][0])
            }
          }

        })
        return { firmDetails, errorFirmDetail }
      })

    }

    render() {
      return (
        <div id="main">
          <section className="container">
            <section className="accordions box">
              <ThirdPartyDetail
                firmDetails={this.state.firmDetails}
                errorFirmDetail={this.state.errorFirmDetail}
                onChangeFirmDetail={this.onChangeFirmDetail}
                onSelectMultipleOptions={this.onSelectMultipleOptions}
                addMore={this.addMore}
              />
              <hr />
              <ListAddresses addressList={this.state.addressList} updateAddress={this.updateAddress} />
              <hr />
              <article className="accordion is-active">
                <div className="accordion-header">
                  <p>Business Address</p>
                  <div className="field is-grouped">
                    <div className="control">
                      <button className="button is-link is-small" onClick={this.addNewBussinessAddress.bind(this)}>Add</button>
                    </div>
                    <div className="control">
                      <button className="button is-light is-small" onClick={this.resetBussinessAddress.bind(this)}>Reset</button>
                    </div>
                  </div>
                </div>
                <div className="accordion-body">
                  {
                    this.state.firmDetails.addresses.map((businessAddress, idx) => (
                      <ThirdPartyAddressForm
                        businessAddress={businessAddress}
                        idx={idx}
                        onChangeBusinessAddress={this.onChangeFirmDetail}
                        addMore={this.addMore}
                        onChangeAddMore={this.onChangeAddMore}
                        errorFirmDetail={this.state.errorFirmDetail.addresses[idx]}
                      />
                    ))
                  }
                </div>
              </article>
              <div className="columns">
                <div className="column is-full">
                  <div className="field is-grouped-center">
                    <div className="control">
                      <button className="button is-link" onClick={this.handleSubmit}>Save</button>
                    </div>
                    <div className="control">
                      <button className="button is-light">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </section>
        </div>
      )
    }
}

const mapStateToProps = (state) => {
  const admFirmDetails = state.admFirmDetails
  return { admFirmDetails }
}

const mapDispatchToProps = {
  saveFirmDetail,
  getFirmDetailById,
  saveThirdPartyDetail
}

export default connect(mapStateToProps, mapDispatchToProps)(AddThirdPartyFirm)