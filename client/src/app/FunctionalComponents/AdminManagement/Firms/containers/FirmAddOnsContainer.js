import {
  getAllTenetsEntity,
  saveAddOns,
  checkPlatFormAdmin,
  getFirmDetailById
} from "AppState/actions/AdminManagement/admTrnActions"

import cloneDeep from "lodash.clonedeep"
import React, { Component } from "react"

import { toast } from "react-toastify"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { DropdownList } from "react-widgets"
import { getPicklistValues } from "GlobalUtils/helpers"
import FirmAddOns from "./../../CommonComponents/FirmAddOns"

import Loader from "../../../../GlobalComponents/Loader"

class FirmAddOnsContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firmAddonsList: [],
      firmList: [],
      firmName: "",
      loading: false,
      isPlatFormAdmin: false,
      firmDetails: {
        firmAddOns: []
      },
      error: ""
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async componentWillMount() {
    let firmList = getAllTenetsEntity()
    let isPlatFormAdmin = checkPlatFormAdmin()
    this.setState({
      loading: true
    })
    const { userEntities } = this.props.auth
    userEntities.isMuniVisorClient = true
    firmList = await firmList
    isPlatFormAdmin = await isPlatFormAdmin
    this.setState({
      firmList: [
        { label: "Select Firm", value: "" },
        ...firmList.map(item => ({ label: item.firmName, value: item._id }))
      ],
      loading: false,
      isPlatFormAdmin
    })
    if (userEntities.isMuniVisorClient && !isPlatFormAdmin) {
      this.props.getFirmDetailById(userEntities.entityId)
    }
  }

  async componentDidMount() {
    this.setState({
      loadingPickLists: true
    })
    const [firmAddOnsList] = await getPicklistValues(["LKUPFIRMADDONS"])
    this.setState({
      loading: false,
      firmAddonsList: firmAddOnsList[1],
      loadingPickLists: false
    })
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.admFirmDetails.firmDetailById &&
      Object.keys(nextProps.admFirmDetails.firmDetailById).length > 0 &&
      !nextProps.admFirmDetails.updated
    ) {
      const firmDetails = cloneDeep(nextProps.admFirmDetails.firmDetailById)
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        firmName: { label: firmDetails.firmName, value: firmDetails._id },
        loading: false,
        error: ""
      }))
    }
  }
  componentWillUnmount() {}
  /** ************************************************************************************* */
  async handleSubmit(event) {
    event.preventDefault()
    const { firmDetails, firmName } = this.state
    const { firmAddOns } = firmDetails
    if (firmName !== "") {
      const response = await saveAddOns(firmAddOns, firmDetails._id)
      if (response.status === 200)
        toast("AddOns has added", { autoClose: 2000, type: toast.TYPE.SUCCESS })
      else
        toast("Something Wrong!", { autoClose: 2000, type: toast.TYPE.ERROR })
    } else {
      this.setState({
        error: "Select Firm"
      })
    }
  }
  /** *********************************** this one is for toggle checkbox ********************************** */
  toggleCheckbox = e => {
    const { firmDetails } = this.state
    const { firmAddOns } = firmDetails
    const { value } = e.target
    if (firmAddOns.includes(value)) {
      firmAddOns.splice(firmAddOns.indexOf(value), 1)
    } else {
      firmAddOns.push(value)
    }
    this.setState(prevState => ({
      ...prevState,
      ...{
        ...firmDetails,
        firmAddOns
      }
    }))
  }

  onChangeFirm = item => {
    if (item.value !== "") {
      this.setState({
        firmName: item,
        loading: true,
        error: ""
      })
      this.props.getFirmDetailById(item.value)
    } else {
      this.setState({
        error: "",
        firmDetails: {
          firmAddOns: []
        },
        firmName: ""
      })
    }
  }
  render() {
    const loading = () => <Loader />
    const { firmAddonsList, firmDetails, firmList, firmName } = this.state
    const { firmAddOns } = firmDetails
    return (
      <section className="accordions">
        {this.state.loading ? loading() : null}
        <div className="columns">
          <div className="column">
            <p className="title innerPgTitle">Associated Entity / Firm</p>
            <div className="control">
              <DropdownList
                filter="contains"
                value={firmName}
                data={firmList}
                message="select Firm Name"
                textField="label"
                valueField="value"
                onChange={this.onChangeFirm}
                disabled={!this.state.isPlatFormAdmin}
                busy={this.state.loadingPickLists}
                busySpinner={<span className="fas fa-sync fa-spin is-link" />}
              />
            </div>
            {this.state.error && (
              <small className="text-error">{this.state.error}</small>
            )}
          </div>
        </div>
        <FirmAddOns
          firmAddonsList={firmAddonsList}
          toggleCheckbox={this.toggleCheckbox}
          firmAddOns={firmAddOns}
        />
        <div className="columns">
          <div className="column is-full">
            <div className="field is-grouped-center">
              <div className="control">
                <button className="button is-link" onClick={this.handleSubmit}>
                  Save
                </button>
              </div>
              <div className="control">
                <button className="button is-light">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
const mapStateToProps = state => {
  const { auth, admFirmDetails } = state
  return { auth, admFirmDetails }
}
const mapDispatchToProps = {
  getFirmDetailById
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FirmAddOnsContainer)
)
