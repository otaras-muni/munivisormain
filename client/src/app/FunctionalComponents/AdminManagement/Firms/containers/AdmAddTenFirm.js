import React from "react"
import { DropdownList } from "react-widgets"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import { validateFirmDetail } from "Validation/firmDetail"
import {
  getPicklistValues,
  convertError,
  checkEmptyElObject,
  getToken
} from "GlobalUtils/helpers"
import {
  saveFirmDetail,
  addTenetsFirm,
  updateTenetsFirm,
  updateFirmLogoAndAuditSetting,
  getFirmDetailById,
  getAllFirmList,
  checkPlatFormAdmin,
  getAllUserFirmList,
  checkDuplicateEntity,
  changeAddressStatus
} from "AppState/actions/AdminManagement/admTrnActions"
import {
  checkAuth,
} from "../../../../StateManagement/actions"
import AdmTrnAddressForm from "./../../CommonComponents/FirmAddressForm"
import AdmTrnFirm from "./../../CommonComponents/FirmProfileForm"
import ListAddresses from "./../../CommonComponents/FirmAddressList"
import Loader from "../../../../GlobalComponents/Loader"
import AuditAndLogo from "../../../EntityManagement/clients/components/AuditAndLogo"

let errorData = { error: null }

const initialAddressError = (errorFirmDetail, address, initialFirmDetails) => {
  const arr = ["officeEmails", "officePhone", "officeFax"]
  arr.map(item => {
    if (address[item].length > 0) {
      for (let index = 1; index < address[item].length; index++) {
        errorFirmDetail.addresses[0][item].push(
          initialFirmDetails.addresses[0][item][0]
        )
      }
    }
  })
}

class AdmAddTenFirm extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.state.loading = false
    this.state.loadingPickLists = false
    this.state.errorFirmDetail = Object.assign(
      {},
      this.initialState().firmDetails
    )
    this.cleanErrorFirmDetail = Object.assign(
      {},
      this.initialState().firmDetails
    )
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitted = false
  }

  async componentWillMount() {
    let isPlatFormAdmin = checkPlatFormAdmin()

    const { userEntities } = this.props.auth
    userEntities.isMuniVisorClient = true
    const { firmDetails } = this.state
    firmDetails.addresses[0].isPrimary = true

    this.setState({
      loading: true
    })

    isPlatFormAdmin = await isPlatFormAdmin

    this.setState(prevState => ({
      ...prevState,
      ...{
        firmDetails,
        isPlatFormAdmin: isPlatFormAdmin
      }
    }))

    if (userEntities.isMuniVisorClient && !isPlatFormAdmin) {
      this.props.getFirmDetailById(userEntities.entityId)
    } else {
      this.setState({
        loading: false,
        loadingPickLists: false
      })
    }
  }

  async componentDidMount() {
    this.setState({
      loadingPickLists: true
    })
    let firmList = getAllUserFirmList()

    const [
      msrbFirmList,
      countryResult,
      msrbRegTypeResult,
      businessStructure,
      annualRevenue,
      numberOfEmployee
    ] = await getPicklistValues([
      "LKUPMSRBFIRMS",
      "LKUPCOUNTRY",
      "LKUPREGISTRANTTYPE",
      "LKUPBUSSTRUCT",
      "LKUPANNUALREVENUE",
      "LKUPNUMOFPEOPLE"
    ])

    const msrbFirmListResult = [{ label: "Select MSRB Firm", value: "" }]

    msrbRegTypeResult[2]["Tenants / Firms"].forEach(item => {
      Object.keys(msrbFirmList[3][item]).forEach(firm => {
        msrbFirmListResult.push({
          label: firm,
          value: msrbFirmList[3][item][firm][0],
          registrantType: item
        })
      })
    })

    firmList = await firmList
    firmList =
      firmList.length > 0 &&
      firmList.map(item => ({ label: item.firmName, value: item._id }))

    this.setState(prevState => ({
      ...prevState,
      ...{
        countryResult,
        msrbRegTypeResult: msrbRegTypeResult[2]["Tenants / Firms"]
          ? msrbRegTypeResult[2]["Tenants / Firms"]
          : [],
        businessStructure,
        annualRevenue,
        numberOfEmployee,
        msrbFirmListResult,
        firmList,
        loadingPickLists: false
      }
    }))
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.admFirmDetails.firmDetailById &&
      Object.keys(nextProps.admFirmDetails.firmDetailById).length > 0 &&
      !nextProps.admFirmDetails.updated
    ) {
      let { errorFirmDetail, oldFirmDetails } = this.state
      const firmDetails = cloneDeep(nextProps.admFirmDetails.firmDetailById)
      oldFirmDetails = cloneDeep(nextProps.admFirmDetails.firmDetailById)
      const addressList = cloneDeep(
        nextProps.admFirmDetails.firmDetailById.addresses
      )
      const settings = nextProps.admFirmDetails.firmDetailById.settings || {}
      firmDetails.addresses = [this.initialState().firmDetails.addresses[0]]
      // firmDetails.addresses[0].isPrimary = true
      errorFirmDetail = cloneDeep(this.initialState().firmDetails)
      initialAddressError(
        errorFirmDetail,
        firmDetails.addresses[0],
        this.initialState().firmDetails
      )
      const oldErrorFirmDetail = cloneDeep(errorFirmDetail)
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        errorFirmDetail,
        readOnlyFeatures: {
          msrbFirmNameReadOnly: true,
          msrbIdReadOnly: true,
          registrantTypeReadOnly: true,
          clientTypeDisabled: true,
          firmName: true
        },
        loading: false,
        oldErrorFirmDetail,
        isPrimaryAddress: "",
        oldFirmDetails,
        settings: settings && Object.keys(settings).length ? settings : prevState.settings
      }))
    }
    if (
      nextProps.admFirmDetails.updatedFirmDetails &&
      Object.keys(nextProps.admFirmDetails.updatedFirmDetails).length > 0 &&
      nextProps.admFirmDetails.updated
    ) {
      const currentAddresses = cloneDeep(this.state.firmDetails.addresses[0])
      const firmDetails = cloneDeep(nextProps.admFirmDetails.updatedFirmDetails)
      let { errorFirmDetail } = this.state
      const addressList = cloneDeep(
        nextProps.admFirmDetails.updatedFirmDetails.addresses
      )
      firmDetails.addresses = [currentAddresses]
      errorFirmDetail = cloneDeep(this.initialState().firmDetails)
      initialAddressError(
        errorFirmDetail,
        currentAddresses,
        this.initialState().firmDetails
      )

      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        errorFirmDetail
      }),async () => {
        await this.props.checkAuth(getToken())
      })

      toast("Firms has been updated!", {
        autoClose: 2000,
        type: toast.TYPE.SUCCESS
      })
    }
    if (
      nextProps.admFirmDetails.newFirmDetails &&
      Object.keys(nextProps.admFirmDetails.newFirmDetails).length > 0 &&
      nextProps.admFirmDetails.firmAdded
    ) {
      const firmDetails = cloneDeep(nextProps.admFirmDetails.newFirmDetails)
      const { firmList } = this.state
      firmList.push({
        label: firmDetails.msrbFirmName,
        value: firmDetails._id
      })
      this.setState(prevState => ({
        ...prevState,
        firmDetails: this.initialState().firmDetails,
        addressList: [],
        errorFirmDetail: this.cleanErrorFirmDetail
      }))
      this.props.history.push(`/firms/${firmDetails._id}/firmsdetails`)
    }

    if (nextProps.admFirmDetails.firmList) {
      this.setState(prevState => ({
        ...prevState,
        firmList: nextProps.admFirmDetails.firmList
      }))
    }
    if (nextProps.admFirmDetails.error) {
      const { errorFirmDetail } = this.state.errorFirmDetail
      const arr = ["zipCode", "officeFax", "officeEmails"]
    }
  }
  componentWillUpdate(nextState, nextProps) {}
  componentDidUpdate(prevProps, prevState) {}

  componentWillUnmount() {}
  /** ********************************************** onChangeAliases *********************** */
  onChangeAliases = (e, key) => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails.entityAliases[key] = e.target.value
    const validator = {
      entityAliases: [e.target.value]
    }
    this.setState({
      firmDetails
    })
    if (this.submitted) {
      const errorData = validateFirmDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.path[0] === e.target.name) {
          errorFirmDetail[e.target.name][key] = "Required" || err.message
          this.setState({
            errorFirmDetail
          })
        } else {
          errorFirmDetail[e.target.name][key] = ""
          this.setState({
            errorFirmDetail
          })
        }
      } else {
        errorFirmDetail[e.target.name][key] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }
  /** ************************************************* */
  onChangeAddOns = (e, idx) => {
    const { firmAddOns } = this.state.firmDetails
    firmAddOns[idx].serviceEnabled = !firmAddOns[idx].serviceEnabled
    this.setState({
      firmAddOns
    })
  }
  /** ************************ this is for onchange msrb firmname ********************************* */
  onChangeFirmDetail = (e, key, deepFields) => {
    const { firmDetails } = this.state
    const { errorFirmDetail } = this.state
    let keyFirmDetail, errorKeyFirmDetail
    let validator
    if (typeof key !== "undefined") {
      const value =
        e.target.type === "checkbox"
          ? !firmDetails.addresses[key][e.target.name]
          : e.target.value
      if (e.target.name == "zip1" || e.target.name == "zip2") {
        keyFirmDetail = firmDetails.addresses[key].zipCode
        errorKeyFirmDetail = errorFirmDetail.addresses[key].zipCode
        validator = {
          addresses: [
            {
              zipCode: {
                [e.target.name]: e.target.value
              }
            }
          ]
        }
      } else {
        keyFirmDetail = firmDetails.addresses[key]
        errorKeyFirmDetail = errorFirmDetail.addresses[key]
        validator = {
          addresses: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      }
      keyFirmDetail[e.target.name] = value /*  */
      if (deepFields) {
        deepFields.forEach(d => {
          keyFirmDetail[d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    } else {
      firmDetails[e.target.name] = e.target.value
      validator = {
        [e.target.name]: e.target.value
      }
      errorKeyFirmDetail = errorFirmDetail
      if (deepFields) {
        deepFields.forEach(d => {
          firmDetails.addresses[key][d] = ""
        })
      }
      this.setState(prevState => ({
        ...prevState,
        firmDetails
      }))
    }
    if (this.submitted) {
      errorData = validateFirmDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        if (err.context.key === e.target.name) {
          errorKeyFirmDetail[e.target.name] = `${err.context.label} Required.`
          this.setState({
            errorFirmDetail
          })
        } else {
          errorKeyFirmDetail[e.target.name] = ""
          this.setState({
            errorFirmDetail
          })
        }
      } else {
        errorKeyFirmDetail[e.target.name] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }

  /** ************************ this is for onchange add more ********************************* */
  onChangeAddMore = (e, type, key, idx) => {
    e.preventDefault()
    const { firmDetails, errorFirmDetail } = this.state
    const addresses = firmDetails.addresses[key]
    addresses[type][idx][e.target.name] = e.target.value
    const validator = {
      addresses: [
        {
          [type]: [
            {
              [e.target.name]: e.target.value
            }
          ]
        }
      ]
    }
    this.setState({
      addresses
    })

    if (this.submitted) {
      errorData = validateFirmDetail(validator)
      if (errorData.error != null) {
        const err = errorData.error.details[0]
        errorFirmDetail.addresses[key][type][idx][e.target.name] = `${
          err.context.label
        } Required.`.replace(/['"]+/g, "")
        this.setState({
          errorFirmDetail
        })
      } else {
        errorFirmDetail.addresses[key][type][idx][e.target.name] = ""
        this.setState({
          errorFirmDetail
        })
      }
    }
  }

  getFirmDetails = item => {
    const { firmDetails, readOnlyFeatures, errorFirmDetail } = this.state
    if (item.value !== "") {
      firmDetails.firmName = item.label
      firmDetails.msrbId = item.value
      firmDetails.msrbFirmName = item.label
      readOnlyFeatures.firmName = true
      readOnlyFeatures.msrbIdReadOnly = true
      firmDetails.msrbRegistrantType = item.registrantType
      readOnlyFeatures.registrantTypeReadOnly = true
      errorFirmDetail.firmName = ""
      errorFirmDetail.msrbId = ""
      errorFirmDetail.msrbRegistrantType = ""
    } else {
      firmDetails.firmName = ""
      firmDetails.msrbId = ""
      firmDetails.msrbFirmName = ""
      readOnlyFeatures.firmName = false
      readOnlyFeatures.msrbIdReadOnly = false
      readOnlyFeatures.registrantTypeReadOnly = false
      firmDetails.msrbRegistrantType = ""
      if (this.submitted) {
        errorFirmDetail.firmName = "FirmName Required."
      } else {
        errorFirmDetail.firmName = ""
      }
    }
    this.setState({
      firmDetails,
      readOnlyFeatures,
      errorFirmDetail
    })
  }

  /** **************************************this is for initial default value********************* */
  initialState = () => ({
    firmDetails: {
      entityAliases: [],
      isMuniVisorClient: true,
      msrbFirmName: "",
      msrbRegistrantType: "",
      msrbId: "",
      firmName: "",
      taxId: "",
      businessStructure: "",
      numEmployees: "",
      annualRevenue: "",
      addresses: [
        {
          addressName: "",
          isPrimary: false,
          isHeadQuarter: false,
          isActive: true,
          website: "",
          officePhone: [
            {
              countryCode: "",
              phoneNumber: "",
              extension: ""
            }
          ],
          officeFax: [
            {
              faxNumber: ""
            }
          ],
          officeEmails: [
            {
              emailId: ""
            }
          ],
          addressLine1: "",
          addressLine2: "",
          country: "",
          state: "",
          city: "",
          zipCode: {
            zip1: "",
            zip2: ""
          },
          formatted_address: "",
          url: "",
          location: {
            longitude: "",
            latitude: ""
          }
        }
      ]
    },
    oldFirmDetails: {},
    addressList: [],
    firmList: [],
    resetAliases: [],
    readOnlyFeatures: {
      msrbFirmNameReadOnly: false,
      msrbIdReadOnly: false,
      registrantTypeDisable: false,
      registrantTypeReadOnly: false,
      msrbId: false
    },
    msrbRegTypeResult: [],
    countryResult: [],
    annualRevenue: [],
    numberOfEmployee: [],
    businessStructure: [],
    expanded: {
      business: false,
      addOns: false,
      addresslist: false,
      entityname: true
    },
    isPlatFormAdmin: false,
    searchFirmName: "",
    isPrimaryAddress: "",
    isEntityExists: {
      msrbId: "",
      firmName: ""
    },
    settings: {
      auditFlag: "no",
      logo: {
        awsDocId: "",
        fileName: ""
      }
    }
  })

  /** ************************ this is for submit form ********************************* */
  async handleSubmit(event) {
    event.preventDefault()
    this.submitted = true
    const { firmDetails, errorFirmDetail, settings } = this.state
    const firmDetailsData = {
      _id: firmDetails._id ? firmDetails._id : "",
      entityAliases: firmDetails.entityAliases,
      isMuniVisorClient: true,
      msrbFirmName: firmDetails.msrbFirmName,
      msrbRegistrantType: firmDetails.msrbRegistrantType,
      msrbId: firmDetails.msrbId,
      firmName: firmDetails.firmName,
      taxId: firmDetails.taxId,
      businessStructure: firmDetails.businessStructure,
      numEmployees: firmDetails.numEmployees,
      annualRevenue: firmDetails.annualRevenue,
      addresses: firmDetails.addresses,
      settings
    }
    try {
      if (firmDetailsData._id !== "") {
        let addressData = []
        const { addresses } = firmDetailsData
        addresses.forEach(address => {
          if (!checkEmptyElObject(address)) {
            addressData.push(address)
          }
        })
        firmDetailsData.addresses = addressData
        let { addressList } = this.state
        if (addressData.length > 0) {
          addressData = addressData.sort((a, b) => a._id > b._id)
          addressList =
            addressList.length > 0
              ? addressList.filter(obj => obj._id != addressData[0]._id)
              : []
        }
        const mergeAddresses = [...addressList, ...addressData]
        const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
        const errorData = validateFirmDetail(firmDetailsData)
        if (
          errorData.error ||
          filteredAddresses.length !== 1 ||
          this.state.isEntityExists.firmName !== "" ||
          this.state.isEntityExists.msrbId !== ""
        ) {
          if (errorData.error) {
            convertError(errorFirmDetail, errorData)
          }
          if (filteredAddresses.length === 0)
            // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
            toast("One Primary Address  is Required!", {
              autoClose: 2000,
              type: toast.TYPE.ERROR
            })
          else if (filteredAddresses.length > 1)
            // this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
            toast("Only One Primary Address is Required!", {
              autoClose: 2000,
              type: toast.TYPE.ERROR
            })
          else this.state.isPrimaryAddress = ""
          this.setState({ errorFirmDetail })
        } else {
          this.setState({
            loading: true
          })
          const response = await updateTenetsFirm({
            firmDetails: firmDetailsData,
            mergeAddresses
          })
          if (
            (typeof response.error !== "undefined" &&
              response.error !== null) ||
            (typeof response.isPrimaryAddress !== "undefined" &&
              !response.isPrimaryAddress)
          ) {
            if (response.error !== null)
              convertError((errorFirmDetail.errorData.error = response.error))
            if (!response.isPrimaryAddress)
              // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
              toast("One Primary Address is Required!", {
                autoClose: 2000,
                type: toast.TYPE.ERROR
              })
            else this.state.isPrimaryAddress = ""
            this.setState({ loading: false })
          } else {
            this.submitted = false
            await  this.props.checkAuth(getToken())
            toast("Firms has been Updated!", {
              autoClose: 2000,
              type: toast.TYPE.SUCCESS
            })
            this.props.getFirmDetailById(firmDetailsData._id)
          }
        }
      } else {
        let { addresses } = firmDetailsData
        addresses = addresses.filter(item => item.isPrimary)
        const errorData = validateFirmDetail(firmDetailsData)
        if (
          errorData.error ||
          addresses.length !== 1 ||
          this.state.isEntityExists.firmName !== "" ||
          this.state.isEntityExists.msrbId !== ""
        ) {
          if (errorData.error) {
            convertError(errorFirmDetail, errorData)
          }
          if (addresses.length !== 1)
            // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
            toast("One Primary Address  is Required!", {
              autoClose: 2000,
              type: toast.TYPE.ERROR
            })
          this.setState({ errorFirmDetail })
        } else {
          const { entityId } = this.props.auth.userEntities
          this.setState({
            loading: true
          })
          const response = await addTenetsFirm({ firmDetails: firmDetailsData })
          if (
            (typeof response.error !== "undefined" &&
              response.error !== null) ||
            (typeof response.isPrimaryAddress !== "undefined" &&
              !result.isPrimaryAddress)
          ) {
            if (response.error !== null)
              convertError((errorFirmDetail.errorData.error = response.error))
            if (!response.isPrimaryAddress)
              this.state.isPrimaryAddress =
                "One Primary Office Address is Required"
            else this.state.isPrimaryAddress = ""
            this.setState({ loading: false })
          } else {
            const { firmList } = this.state
            const { expanded, readOnlyFeatures } = this.initialState()
            let { firmDetails } = this.initialState()
            firmDetails = cloneDeep(this.initialState().firmDetails)
            firmDetails.entityBorObl = []
            firmList.push({ label: response.firmName, value: response._id })
            this.setState(prevState => ({
              ...prevState,
              firmDetails,
              addressList: [],
              errorFirmDetail: cloneDeep(this.initialState().firmDetails),
              loading: false,
              oldFirmDetails: {},
              readOnlyFeatures,
              firmList,
              searchFirmName: "",
              expanded,
              isPrimaryAddress: ""
            }))
            this.submitted = false
            toast("Firms has been Created!", {
              autoClose: 2000,
              type: toast.TYPE.SUCCESS
            })
          }
        }
      }
    } catch (ex) {
      console.log("=======>>>", ex)
    }
  }

  /** ************************ this is for add new address ********************************* */
  addNewBussinessAddress(event) {
    event.preventDefault()
    const { firmDetails } = this.state
    const { addresses } = firmDetails
    let isEmpty = false
    addresses.forEach(address => {
      if (checkEmptyElObject(address)) {
        isEmpty = true
      }
    })
    if (!isEmpty) {
      this.setState(prevState => {
        const firmDetails = {
          ...prevState.firmDetails
        }
        const errorFirmDetail = {
          ...prevState.errorFirmDetail
        }
        firmDetails.addresses.push(this.initialState().firmDetails.addresses[0])
        errorFirmDetail.addresses.push(
          this.initialState().firmDetails.addresses[0]
        )
        return {
          firmDetails,
          errorFirmDetail
        }
      })
    } else {
      toast("Already and empty address!", {
        autoClose: 2000,
        type: toast.TYPE.INFO
      })
    }
  }
  /** ************************ this is for reset form ********************************* */
  resetBussinessAddress = e => {
    e.preventDefault()
    const address = cloneDeep(this.initialState().firmDetails.addresses[0])
    address.isPrimary = true
    address.isActive = true
    this.setState(prevState => ({
      firmDetails: {
        ...prevState.firmDetails,
        addresses: [address]
      },
      errorFirmDetail: {
        ...prevState.errorFirmDetail,
        addresses: [this.initialState().firmDetails.addresses[0]]
      },
      isPrimaryAddress: ""
    }))
  }
  /** **************************Reset Aliases on Cancel buttons Click*********************** */
  /*  resetAliases = (e) => {
    e.preventDefault()
    if(this.props.admFirmDetails && this.props.admFirmDetails.firmDetailById &&  !this.props.admFirmDetails.updated && this.props.match.params.nav2!=="new"){
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases: cloneDeep(this.props.admFirmDetails.firmDetailById.entityAliases)
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases:[]
        }
      }))
    }else if(this.props.admFirmDetails && this.props.admFirmDetails.updatedFirmDetails && this.props.admFirmDetails.updated && this.props.match.params.nav2!=="new"){
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases: cloneDeep(this.props.admFirmDetails.updatedFirmDetails.entityAliases)
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases:[]
        }
      }))
    }else{
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases: []
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases:[]
        }
      }))
    }
  } */
  resetAliases = e => {
    e.preventDefault()
    const { oldFirmDetails, firmDetails } = this.state
    if (Object.keys(oldFirmDetails).length > 0) {
      const { entityAliases } = cloneDeep(oldFirmDetails)
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: oldFirmDetails.entityAliases.map(
            item => this.initialState().firmDetails.entityAliases
          )
        }
      }))
    } else {
      const { entityAliases } = cloneDeep(this.initialState().firmDetails)
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          entityAliases
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          entityAliases: cloneDeep(
            this.initialState().firmDetails.entityAliases
          )
        }
      }))
    }
  }
  /** **************************this is function is used for addMore function ***************** */
  addMore = (e, type, key) => {
    e.preventDefault()
    this.setState(prevState => {
      const { firmDetails, errorFirmDetail } = prevState
      if (type === "aliases") {
        firmDetails.entityAliases.push("")
        errorFirmDetail.entityAliases.push("")
        prevState.resetAliases.push(firmDetails.entityAliases.length)
      } else {
        firmDetails.addresses[key][type].push(
          this.initialState().firmDetails.addresses[0][type][0]
        )
        errorFirmDetail.addresses[key][type].push(
          this.initialState().firmDetails.addresses[0][type][0]
        )
      }
      return {
        firmDetails,
        errorFirmDetail
      }
    })
  }

  // **************************function used to update business addresses *****************************
  updateAddress = (e, id) => {
    e.preventDefault()
    const address = this.state.addressList.find(item => item._id === id)
    this.setState(prevState => {
      let { errorFirmDetail } = prevState
      const { firmDetails, expanded, oldErrorFirmDetail } = prevState
      errorFirmDetail = this.initialState().firmDetails
      firmDetails.addresses = [address]
      const arr = ["officeEmails", "officePhone", "officeFax"]
      arr.forEach(item => {
        if (address[item].length > 1) {
          for (let i = 1; i < address[item].length; i++) {
            errorFirmDetail.addresses[0][item].push(
              this.initialState().firmDetails.addresses[0][item][0]
            )
          }
        }
      })
      expanded.business = true
      oldErrorFirmDetail.addresses = errorFirmDetail.addresses
      return { firmDetails, errorFirmDetail, expanded }
    })
  }
  resetAll = e => {
    e.preventDefault()
    if (
      this.props.admFirmDetails &&
      this.props.admFirmDetails.firmDetailById &&
      this.props.match.params.nav2 !== "new"
    ) {
      const firmDetails = cloneDeep(this.props.admFirmDetails.firmDetailById)
      const addressList = cloneDeep(
        this.props.admFirmDetails.firmDetailById.addresses
      )

      firmDetails.addresses = this.initialState().firmDetails.addresses
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        errorFirmDetail: this.initialState().firmDetails
      }))
    } else if (
      this.props.admFirmDetails &&
      this.props.admFirmDetails.updatedFirmDetails &&
      this.props.match.params.nav2 !== "new"
    ) {
      const firmDetails = cloneDeep(this.props.admFirmDetails.firmDetailById)
      const addressList = cloneDeep(
        this.props.admFirmDetails.firmDetailById.addresses
      )

      firmDetails.addresses = this.initialState().firmDetails.addresses
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList,
        errorFirmDetail: this.initialState().firmDetails
      }))
    } else {
      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...this.initialState().firmDetails
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...this.initialState().firmDetails
        }
      }))
    }
  }
  toggleButton = (e, val) => {
    e.preventDefault()
    const { expanded } = this.state
    Object.keys(expanded).map(item => {
      if (item === val) {
        expanded[item] = !expanded[item]
      }
    })
    this.setState({
      expanded
    })
  }
  deleteAliases = idx => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails.entityAliases.splice(idx, 1)
    errorFirmDetail.entityAliases.splice(idx, 1)
    this.setState({
      firmDetails,
      errorFirmDetail
    })
  }
  deleteAddressAliases = (e, type, key, idx) => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails.addresses[key][type].splice(idx, 1)
    errorFirmDetail.addresses[key][type].splice(idx, 1)
    this.setState({
      firmDetails,
      errorFirmDetail
    })
  }
  deleteAddress = (e, type, idx) => {
    const { firmDetails, errorFirmDetail } = this.state
    firmDetails[type].splice(idx, 1)
    errorFirmDetail[type].splice(idx, 1)
    this.setState({
      firmDetails,
      errorFirmDetail
    })
  }
  resetAddressAliases = (e, type, key, id) => {
    if (id !== "") {
      this.setState(prevState => {
        const oldFirmDetails = cloneDeep({ ...prevState.oldFirmDetails })
        const { firmDetails, errorFirmDetail } = prevState
        const oldErrorFirmDetail = cloneDeep({
          ...prevState.oldErrorFirmDetail
        })
        const { addresses } = oldFirmDetails
        const address = addresses.filter(item => item._id === id)
        firmDetails.addresses[key][type] = address[0][type]
        errorFirmDetail.addresses[key][type] = cloneDeep(
          oldErrorFirmDetail.addresses[key][type]
        )
        return { firmDetails, errorFirmDetail }
      })
    } else {
      const { firmDetails, errorFirmDetail } = this.state
      firmDetails.addresses[key][
        type
      ] = this.initialState().firmDetails.addresses[0][type]
      errorFirmDetail.addresses[key][
        type
      ] = this.initialState().firmDetails.addresses[0][type]
      this.setState({
        firmDetails,
        errorFirmDetail
      })
    }
  }
  onChangeAddressType = (e, idx) => {
    let { firmDetails, addressList } = this.state
    let { addresses } = firmDetails
    if (e.target.name == "isPrimary") {
      firmDetails.addresses[idx].isPrimary = !firmDetails.addresses[idx]
        .isPrimary
    } else {
      firmDetails.addresses[idx].isHeadQuarter = !firmDetails.addresses[idx]
        .isHeadQuarter
    }
    /*     if(e.target.name==="isPrimary"){
      firmDetails.addresses.forEach((item, index) => {
        if (index == idx) {
          firmDetails.addresses[idx].isPrimary = true
          firmDetails.addresses[idx].isHeadQuarter = false
        }else
          firmDetails.addresses[index].isPrimary = false
      })
    }else{
      firmDetails.addresses[idx].isPrimary = false
      firmDetails.addresses[idx].isHeadQuarter = true
    } */
    if (this.submitted) {
      addressList =
        addressList.length > 0
          ? addressList.filter(obj => obj._id != addresses[0]._id)
          : []
      addresses = addresses.sort((a, b) => a._id > b._id)
      const mergeAddresses = [...addressList, ...addresses]
      const filteredAddresses = mergeAddresses.filter(item => item.isPrimary)
      if (filteredAddresses.length === 0)
        // this.state.isPrimaryAddress  = "One Primary Office  Address  is Required"
        toast("One Primary Address  is Required!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
      else if (filteredAddresses.length > 1)
        // this.state.isPrimaryAddress  = "Only One Primary Office  Address  is Required"
        toast("Only One Primary Address  is Required!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
      else this.state.isPrimaryAddress = ""
    }
    this.setState({
      firmDetails
    })
  }

  checkDuplicateFirm = async e => {
    e.preventDefault()
    const { value, name } = e.target
    const { isEntityExists } = this.state
    if (value.length > 0) {
      const result = await checkDuplicateEntity(value, "")
      if (result.success.isExist) {
        isEntityExists[name] = "Firms Name is Already Exists"
      } else {
        isEntityExists[name] = ""
      }
      this.setState({
        isEntityExists
      })
    }
  }
  onSearchFirm = (item, metaData) => {
    metaData.originalEvent.preventDefault()
    this.setState({
      loading: true
    })
    if (item.value !== "") {
      this.props.getFirmDetailById(item.value)
      this.setState({
        searchFirmName: item.label
      })
    } else {
      const { firmDetails, readOnlyFeatures } = cloneDeep(this.initialState())
      firmDetails.entityBorObl = []
      this.setState(prevState => ({
        ...prevState,
        firmDetails,
        addressList: [],
        errorFirmDetail: cloneDeep(this.initialState().firmDetails),
        loading: false,
        searchFirmName: "",
        readOnlyFeatures
      }))
    }
  }
  addNewFirm = e => {
    e.preventDefault()
    const { firmDetails } = cloneDeep(this.initialState())
    firmDetails.entityBorObl = []
    this.setState(prevState => ({
      ...prevState,
      firmDetails,
      addressList: [],
      LinkCusipList: [],
      LinkBorrowerList: [],
      errorFirmDetail: cloneDeep(this.initialState().firmDetails),
      loading: false,
      oldFirmDetails: {},
      searchFirmName: "",
      clientType: "Client",
      readOnlyFeatures: {
        registrantTypeReadOnly: false,
        msrbFirmNameReadOnly: false,
        msrbId: false,
        clientTypeDisabled: false
      }
    }))
  }

  checkAddressStatus = async (e, id, field) => {
    const { addressList } = this.state
    let addressIdx = ""
    addressList.forEach((address, idx) => {
      if (address._id == id) {
        addressIdx = idx
      }
    })
    if (
      (field === "isPrimary" && !addressList[addressIdx].isActive) ||
      (field === "isActive" && addressList[addressIdx].isPrimary)
    ) {
      toast("Primary Address Can not be Inactive.", {
        autoClose: 2000,
        type: toast.TYPE.WARNING
      })
    } else {
      addressList[addressIdx][field] = !addressList[addressIdx][field]
      if (field === "isPrimary") {
        addressList.forEach((address, idx) => {
          if (address._id === id) addressList[idx][field] = true
          else addressList[idx][field] = false
        })
      }
      this.setState(prevState => ({
        ...prevState,
        addressList
      }))
    }
  }
  getAddressDetails = (address = "", idx) => {
    if (address !== "") {
      const { firmDetails, errorFirmDetail } = this.state
      let id = ""
      if (firmDetails.addresses[idx]._id !== "") {
        id = firmDetails.addresses[idx]._id
      }
      firmDetails.addresses[idx] = {
        ...this.initialState().firmDetails.addresses[0]
      }
      firmDetails.addresses[idx]._id = id
      if (address.addressLine1 !== "") {
        firmDetails.addresses[idx].addressName =
          firmDetails.addresses[idx].addressName !== ""
            ? firmDetails.addresses[idx].addressName
            : address.addressLine1
        firmDetails.addresses[idx].addressLine1 = address.addressLine1
        errorFirmDetail.addresses[idx].addressName = ""
        errorFirmDetail.addresses[idx].addressLine1 = ""
      }
      if (address.country !== "") {
        errorFirmDetail.addresses[idx].country = ""
        firmDetails.addresses[idx].country = address.country
      }
      if (address.state !== "") {
        errorFirmDetail.addresses[idx].state = ""
        firmDetails.addresses[idx].state = address.state
      }
      if (address.city !== "") {
        errorFirmDetail.addresses[idx].city = ""
        firmDetails.addresses[idx].city = address.city
      }
      if (address.zipcode !== "") {
        errorFirmDetail.addresses[idx].zipCode.zip1 = ""
        firmDetails.addresses[idx].zipCode.zip1 = address.zipcode
      }
      firmDetails.addresses[idx].formatted_address = address.formatted_address
      firmDetails.addresses[idx].url = address.url
      firmDetails.addresses[idx].location = { ...address.location }

      this.setState(prevState => ({
        firmDetails: {
          ...prevState.firmDetails,
          ...firmDetails
        },
        errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        }
      }))
    }
  }

  onAuditAndLogoChange = async (settings, type) => {
    const { firmDetails } = this.state
    if(firmDetails._id){
      const payload = {
        settings
      }
      payload._id = firmDetails._id
      if(type === "logo") {
        const res = await updateFirmLogoAndAuditSetting(payload)
        if(res && res.status === 200){
          toast("Firm has been updated!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
          this.setState({
            settings
          })
        }else {
          toast("Something went wrong!", {
            autoClose: 2000,
            type: toast.TYPE.ERROR
          })
        }
      }else {
        this.setState({
          settings
        })
      }
    }
  }

  render() {
    const {settings} = this.state
    const {auth} = this.props
    const loading = () => <Loader />
    if (this.state.loading) {
      return loading()
    }
    return (
      <section className="accordions">
        {this.state.isPlatFormAdmin && (
          <div className="columns">
            <div className="column is-full">
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={this.addNewFirm}
                  >
                    Add New Firm
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.isPlatFormAdmin && (
          <div className="columns">
            <div className="column">
              <DropdownList
                filter="contains"
                value={this.state.searchFirmName}
                data={this.state.firmList}
                groupBy={item => item.firmType}
                message="select MSRB Firm Name"
                textField="label"
                valueField="value"
                onChange={this.onSearchFirm}
                busy={this.state.loadingPickLists}
                busySpinner={<span className="fas fa-sync fa-spin is-link" />}
              />
            </div>
          </div>
        )}
        <article
          className={
            this.state.expanded.entityname ? "accordion is-active" : "accordion"
          }
        >
          <div className="accordion-header toggle">
            <p
              onClick={event => {
                this.toggleButton(event, "entityname")
              }}
            >
              Firms
            </p>
            {this.state.expanded.entityname ? (
              <i className="fas fa-chevron-up" style={{ cursor: "pointer" }} />
            ) : (
              <i
                className="fas fa-chevron-down"
                style={{ cursor: "pointer" }}
              />
            )}{" "}
            <br />
          </div>
          {this.state.expanded.entityname && (
            <AdmTrnFirm
              firmDetails={this.state.firmDetails}
              onChangeFirmDetail={this.onChangeFirmDetail}
              errorFirmDetail={this.state.errorFirmDetail}
              msrbFirmList={this.state.msrbFirmListResult}
              getFirmDetails={this.getFirmDetails}
              addMore={this.addMore}
              onChangeAliases={this.onChangeAliases}
              resetAliases={this.resetAliases}
              readOnlyFeatures={this.state.readOnlyFeatures}
              msrbRegTypeResult={this.state.msrbRegTypeResult}
              businessStructure={this.state.businessStructure}
              numberOfEmployee={this.state.numberOfEmployee}
              annualRevenue={this.state.annualRevenue}
              deleteAliases={this.deleteAliases}
              checkDuplicateFirm={this.checkDuplicateFirm}
              isEntityExists={this.state.isEntityExists}
              busy={this.state.loadingPickLists}
            />
          )}
        </article>

        {this.state.expanded.entityname ?
          <AuditAndLogo user={auth.userEntities || {}} settings={settings} onChange={this.onAuditAndLogoChange}/>
          : null }

        {this.state.addressList.length > 0 && (
          <ListAddresses
            addressList={this.state.addressList}
            updateAddress={this.updateAddress}
            listAddressToggle={this.state.expanded.addresslist}
            toggleButton={this.toggleButton}
            checkAddressStatus={this.checkAddressStatus}
          />
        )}
        <article
          className={
            this.state.expanded.business ? "accordion is-active" : "accordion"
          }
        >
          <div className="accordion-header">
            <p
              onClick={event => {
                this.toggleButton(event, "business")
              }}
            >
              Business Address
            </p>

            <div className="field is-grouped">
              <div className="control">
                <button
                  className="button is-link is-small"
                  onClick={this.addNewBussinessAddress.bind(this)}
                >
                  Add
                </button>
              </div>
              <div className="control">
                <button
                  className="button is-light is-small"
                  onClick={this.resetBussinessAddress.bind(this)}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>

          {this.state.expanded.business && (
            <div className="accordion-body">
              {this.state.firmDetails.addresses.map((businessAddress, idx) => (
                <AdmTrnAddressForm
                  key={idx}
                  businessAddress={businessAddress}
                  idx={idx}
                  onChangeBusinessAddress={this.onChangeFirmDetail}
                  addMore={this.addMore}
                  onChangeAddMore={this.onChangeAddMore}
                  errorFirmDetail={this.state.errorFirmDetail.addresses[idx]}
                  countryResult={this.state.countryResult}
                  resetAddressAliases={this.resetAddressAliases}
                  deleteAddressAliases={this.deleteAddressAliases}
                  deleteAddress={this.deleteAddress}
                  onChangeAddressType={this.onChangeAddressType}
                  isPrimaryAddress={this.state.isPrimaryAddress}
                  getAddressDetails={this.getAddressDetails}
                  busy={this.state.loadingPickLists}
                />
              ))}
            </div>
          )}
        </article>
        <div className="columns">
          <div className="column is-full" style={{ marginTop: "25px" }}>
            <div className="field is-grouped-center">
              <div className="control">
                <button className="button is-link" onClick={this.handleSubmit}>
                  Save
                </button>
              </div>
              <div className="control">
                <button className="button is-light" onClick={this.resetAll}>
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = state => {
  const { admFirmDetails, auth } = state
  return { admFirmDetails, auth }
}
const mapDispatchToProps = {
  saveFirmDetail,
  getAllFirmList,
  getFirmDetailById,
  checkAuth
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AdmAddTenFirm)
)
