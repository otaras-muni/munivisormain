import React from "react"
import { connect } from "react-redux"
import { Redirect } from "react-router"
import {checkPlatFormAdmin} from "AppState/actions/AdminManagement/admTrnActions"
import FirmsView from "./FirmsView"
import {checkSupervisorControls} from "../../../StateManagement/actions/CreateTransaction"

class FirmsMain extends React.Component {
  constructor() {
    super()
    this.state = {
      svControls: {}
    }
  }

  async componentWillMount() {
    const svControls = await checkSupervisorControls()
    this.setState({ svControls})
  }

  render () {
    const { nav2, nav3, loginDetails, audit } = this.props
    const { svControls } = this.state
    const {userEntities} = loginDetails
    const {entityId} = userEntities[0]
    if (nav2) {
      return (
        <FirmsView firmId={nav2} navComponent={nav3} audit={audit} svControls={svControls}/>
      )
    }
    return (
      <Redirect to={`/admin-firms/${entityId}/firms`}/>
    )
  }
}
const mapStateToProps = state => ({
  audit: (state.auth && state.auth.userEntities && state.auth.userEntities.settings && state.auth.userEntities.settings.auditFlag) || ""
})

export default connect(mapStateToProps, null)(FirmsMain)
