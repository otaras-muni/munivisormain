import React from "react"
import { Link } from "react-router-dom"

const ManagementConsole = props => (
  <div id="main">
    <section className="container">
      <div className="tile is-ancestor">
        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-firms">
              <i className="fas fa-7x fa-building" />
              <hr />
              <p className="title mgmtConTitle">My Firm</p>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-users">
              <i className="fas fa-7x fa-users" />
              <hr />
              <p className="title mgmtConTitle">Users</p>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-addnew-client-prospects">
              <i className="fas fa-7x fa-handshake" />
              <hr />
              <p className="title mgmtConTitle">Clients</p>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-thirdparty">
              <i className="fab fa-7x fa-keycdn" />
              <hr />
              <p className="title mgmtConTitle">3rd Party</p>
            </Link>
          </article>
        </div>
      </div>

      <div className="tile is-ancestor">
        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/configuration">
              <i className="fab fa-7x fa-whmcs" />
              <hr />
              <p className="title mgmtConTitle">Configuration</p>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/configuration">
              <i className="fas fa-7x fa-wrench" />
              <hr />
              <p className="title mgmtConTitle">Migration Tools</p>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/usage">
              <i className="fas fa-7x fa-battery-full" />
              <hr />
              <p className="title mgmtConTitle">Usage</p>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin/billing">
              <i className="fas fa-7x fa-credit-card" />
              <hr />
              <p className="title mgmtConTitle">Billing</p>
            </Link>
          </article>
        </div>
      </div>
    </section>
  </div>
)

export default ManagementConsole
