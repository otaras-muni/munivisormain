import React from "react"
import EntityResultSet from "./components/EntityResultSet"
import TranResultSet from "./components/TranResultSet"
import UserEntityResultSet from "./components/UserEntityResult"
import TaskResultSet from "../Dashboard/components/TaskResultSet"
import DocumentResultSet from "../Document/components/TransactionResultSet"

const tranResultComponentId = [
  "mainSearch",
  "dateFilter",
  "parAmountFilter",
  "stateFilter",
  "countyFilter",
  "primarySectorFilter",
  "secondarySectorFilter",
  "statusFilter",
  "useOfProceedsFilter"
]


const entitiesResultComponentId = [
  "mainSearch",
  "marketRole"
]

const userResultComponentId = [
  "mainSearch",
  "entitiesFilter",
  "userStateFilter"
]

const tasksResultComponentId = [
  "mainSearch",
  "taskDueDateFilter",
  "taskTypeFilter"
]

const documentssResultComponentId = [
  "mainSearch",
  "docTranTypeFilter",
  "docTypeFilter"
]

export default {
  transactions: {
    react: tranResultComponentId,
    resultSet: (data, activeGroup) => <TranResultSet data={data} activeGroup={activeGroup} />
  },
  entities: {
    react: entitiesResultComponentId,
    resultSet: (data) => <EntityResultSet data={data} />
  },
  users: {
    react: userResultComponentId,
    resultSet: (data) => <UserEntityResultSet data={data} />
  },
  tasks: {
    react: tasksResultComponentId,
    resultSet: (data) => <TaskResultSet items={data} />
  },
  documents: {
    react: documentssResultComponentId,
    resultSet: (data) => <DocumentResultSet items={data} />
  }
}
