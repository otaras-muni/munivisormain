import React from "react"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import Loader from "Global/Loader"
import { getPicklistValues } from "GlobalUtils/helpers"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX, ELASTIC_SEARCH_TYPES } from "../../../../../constants"
import { getHeaders } from "../../../../../globalutilities"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType = "must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "dealIssueTranState.raw": value
                }
              },
              {
                match: {
                  "rfpTranState.raw": value
                }
              },
              {
                match: {
                  "actTranState.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class StateFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: [],
      selectedCountry: "",
      loading:true
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps, prevState) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.stateFilter || {})) !== JSON.stringify((oldPref.stateFilter || {}))) {
      this.props.setQuery({
        query: (pref.stateFilter || {}).query || {},
        value: (pref.stateFilter || {}).value || ""
      })
    }

    if (pref.countyFilter) {
      this.setSelectedCounty(pref.countyFilter.value)
    }
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  setSelectedCounty = (selectedCountry) => {
    if (this.state.selectedCountry !== selectedCountry) {
      this.setState({
        selectedCountry,
        loading: true,
      }, () => {
        this.getOptions()
      })
    }
  }

  getOptions = async () => {
    const { pref } = this.props
    const countryValue = this.state.selectedCountry || ""

    const [countries] = await getPicklistValues(["LKUPCOUNTRY"])
    let states = []

    if (countryValue) {
      states = countries[3]["United States"][this.state.selectedCountry]
    } /* else {
      const stateList = (countries ? countries[1] : []).map(item => countries[2][item])
      states = stateList.reduce((state, item) => [...state, ...item], [])
    } */
    if (!this.isCancelled) {
      this.setState({
        options: _.uniq(states),
        loading: false
      })
    }
  }

  // getOptions = async () => {
  //   const [state] = await getPicklistValues(["LKUPSTATECOUNTY"])
  //   console.log('state', state)
  //   axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.MARFPS},${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.RFPS}/_search`, {
  //     "size": 0,
  //     "aggs": {
  //       "deal_options": {
  //         "terms": {
  //           "field": "dealIssueTranState.raw"
  //         }
  //       },
  //       "rfp_options": {
  //         "terms": {
  //           "field": "rfpTranState.raw"
  //         }
  //       },
  //       "loan_options": {
  //         "terms": {
  //           "field": "bankLoanSummary.actTranState.raw"
  //         }
  //       },
  //       "marfps_options": {
  //         "terms": {
  //           "field": "maRfpSummary.actState.raw"
  //         }
  //       },
  //       "other_options": {
  //         "terms": {
  //           "field": "actTranState.raw"
  //         }
  //       }
  //     }
  //   }, {headers: getHeaders()}).then(res => {
  //     const dealoptions = res.data.aggregations.deal_options.buckets
  //     const rfpoptions = res.data.aggregations.rfp_options.buckets
  //     const loanOptions = res.data.aggregations.loan_options.buckets
  //     const marfpOptions = res.data.aggregations.marfps_options.buckets
  //     const othersOptions = res.data.aggregations.other_options.buckets

  //     const result = _.uniqBy([...dealoptions, ...rfpoptions, ...loanOptions, ...marfpOptions, ...othersOptions], "key")
  //     const options = result.reduce((acc, state) => state.key ? [...acc, state.key] : [...acc], [])
  //     if(!this.isCancelled) {
  //       this.setState({
  //         options,
  //       })
  //     }
  //   })
  // }

  handleQueryChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const stateFilter = pref.stateFilter || {}
    stateFilter.queryType = stateFilter.queryType || "must"
    stateFilter.value = value
    stateFilter.query = prepareQuery(stateFilter.value, stateFilter.queryType)

    this.props.setQuery({
      query: stateFilter.query,
      value: stateFilter.value,
    })

    this.props.handleFilterChange("stateFilter", stateFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const stateFilter = pref.stateFilter || {}
    stateFilter.queryType = value
    stateFilter.value = stateFilter.value || ""
    stateFilter.query = prepareQuery(stateFilter.value, stateFilter.queryType)

    this.props.setQuery({
      query: stateFilter.query,
      value: stateFilter.value,
    })

    this.props.handleFilterChange("stateFilter", stateFilter)
  }

  render() {
    const { pref } = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px" }}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).stateFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).stateFilter || {}).value || ""}>
            <option value="">County</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
            {this.state.options && this.state.options["United States"] && this.state.options["United States"][this.state.selectedCountry]
              ? this.state.options["United States"][this.state.selectedCountry].map((country, i) => (
                <option key={country} value={country}>
                  {country}
                </option>
              ))
              : null}
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default StateFilter
