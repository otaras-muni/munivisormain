import React from "react"
import moment from "moment"
import styled from "styled-components"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType = "must") => {
  let query = {}
  switch (value) {
    case "today":
      query = {
        bool: {
          [queryType]: {
            range: {
              "createdAt": {
                lte: moment().format("YYYY-MM-DD"),
                gte: moment().format("YYYY-MM-DD")
              }
            }
          }
        }
      }
      break
    case "lastWeek":
      query = {
        bool: {
          [queryType]: {
            range: {
              "createdAt": {
                lte: moment().subtract(1, "week").endOf("week").format(),
                gte: moment().subtract(1, "week").startOf("week").format()
              }
            }
          }
        }
      }
      break
    case "lastMonth":
      query = {
        bool: {
          [queryType]: {
            range: {
              createdAt: {
                lte: moment().format(),
                gte: moment().subtract(1, "months").date(1).format()
              }
            }
          }
        }
      }
      break
    case "last3Months":
      query = {
        bool: {
          [queryType]: {
            range: {
              createdAt: {
                lte: moment().format(),
                gte: moment().subtract(3, "months").date(1).format()
              }
            }
          }
        }
      }
      break
    case "last6Months":
      query = {
        bool: {
          [queryType]: {
            range: {
              createdAt: {
                lte: moment().format(),
                gte: moment().subtract(6, "months").date(1).format()
              }
            }
          }
        }
      }
      break
    case "last12Months":
      query = {
        bool: {
          [queryType]: {
            range: {
              createdAt: {
                lte: moment().format(),
                gte: moment().subtract(12, "months").date(1).format()
              }
            }
          }
        }
      }
      break
    case "last24Months":
      query = {
        bool: {
          [queryType]: {
            range: {
              createdAt: {
                lte: moment().format(),
                gte: moment().subtract(24, "months").date(1).format()
              }
            }
          }
        }
      }
      break
    case "last36Months":
      query = {
        bool: {
          [queryType]: {
            range: {
              createdAt: {
                lte: moment().format(),
                gte: moment().subtract(36, "months").date(1).format()
              }
            }
          }
        }
      }
      break
    default:
      query = {}
      break
  }

  return query
}

class DateFilter extends React.Component {

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.dateFilter || {})) !== JSON.stringify((oldPref.dateFilter || {}))) {
      this.props.setQuery({
        query: (pref.dateFilter || {}).query || {},
        value: (pref.dateFilter || {}).value || ""
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const dateFilter = pref.dateFilter || {}
    dateFilter.queryType = dateFilter.queryType || "must"
    dateFilter.value = value
    dateFilter.query = prepareQuery(dateFilter.value, dateFilter.queryType)

    this.props.setQuery({
      query: dateFilter.query,
      value: dateFilter.value,
    })

    this.props.handleFilterChange("dateFilter", dateFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const dateFilter = pref.dateFilter || {}
    dateFilter.queryType = value
    dateFilter.value = dateFilter.value || ""
    dateFilter.query = prepareQuery(dateFilter.value, dateFilter.queryType)

    this.props.setQuery({
      query: dateFilter.query,
      value: dateFilter.value,
    })

    this.props.handleFilterChange("dateFilter", dateFilter)
  }

  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px" }}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).dateFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).dateFilter || {}).value || ""}>
            <option value="">Date Range</option>
            <option value="today">Today</option>
            <option value="lastWeek">Last Week</option>
            <option value="lastMonth">Last Month</option>
            <option value="last3Months">Last 3 Months</option>
            <option value="last6Months">Last 6 Months</option>
            <option value="last12Months">Last 12 Months</option>
            <option value="last24Months">Last 24 Months</option>
            <option value="last36Months">Last 36 Months</option>
            <option value="all">All</option>
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default DateFilter
