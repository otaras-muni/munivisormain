import React from "react"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX, ELASTIC_SEARCH_TYPES } from "../../../../../constants"
import { getHeaders } from "../../../../../globalutilities"
import {getPicklistValues} from "GlobalUtils/helpers"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "dealIssueTranStatus.raw": value
                }
              },
              {
                match: {
                  "rfpTranStatus.raw": value
                }
              },
              {
                match: {
                  "bankLoanSummary.actTranStatus.raw": value
                }
              },
              {
                match: {
                  "derivativeSummary.tranStatus.raw": value
                }
              },
              {
                match: {
                  "actOppStatus.raw": value
                }
              },
              {
                match: {
                  "actTranStatus.raw": value
                }
              },
              {
                match: {
                  "actStatus.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class StatusFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.statusFilter || {})) !== JSON.stringify((oldPref.statusFilter || {}))) {
      this.props.setQuery({
        query: (pref.statusFilter || {}).query || {},
        value: (pref.statusFilter || {}).value || ""
      })
    }
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  getOptions = async () => {
    const [status] = await getPicklistValues(["LKUPSWAPTRANSTATUS"])
    if(!this.isCancelled) {
      this.setState({
        options: status[1],
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const statusFilter = pref.statusFilter || {}
    statusFilter.queryType = statusFilter.queryType || "must"
    statusFilter.value = value
    statusFilter.query = prepareQuery(statusFilter.value, statusFilter.queryType)

    this.props.setQuery({
      query: statusFilter.query,
      value: statusFilter.value,
    })

    this.props.handleFilterChange("statusFilter", statusFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const statusFilter = pref.statusFilter || {}
    statusFilter.queryType = value
    statusFilter.value = statusFilter.value || ""
    statusFilter.query = prepareQuery(statusFilter.value, statusFilter.queryType)

    this.props.setQuery({
      query: statusFilter.query,
      value: statusFilter.value,
    })

    this.props.handleFilterChange("statusFilter", statusFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).statusFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).statusFilter || {}).value || ""}>
            <option value="">Status</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default StatusFilter
