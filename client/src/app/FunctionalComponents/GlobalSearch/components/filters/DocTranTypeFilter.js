/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import styled from "styled-components"
import { TRANSACTION_TYPES } from "../../../Document/components/TransactionResultSet"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "docContext.raw": value
                }
              },
            ]
          }
        }
      }
    }
  }


  return query
}

class DocTranTypeFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const { setQuery, pref: newPref } = this.props
    const pref = { ...newPref }

    if (JSON.stringify((pref.docTranTypeFilter || {})) !== JSON.stringify((oldPref.docTranTypeFilter || {}))) {
      setQuery({
        query: (pref.docTranTypeFilter || {}).query || {},
        value: (pref.docTranTypeFilter || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = () => {
    this.setState({
      options: Object.keys(TRANSACTION_TYPES).map(item => TRANSACTION_TYPES[item]),
    })
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const docTranTypeFilter = pref.docTranTypeFilter || {}
    docTranTypeFilter.queryType = docTranTypeFilter.queryType || "must"
    docTranTypeFilter.value = value
    docTranTypeFilter.query = prepareQuery(docTranTypeFilter.value, docTranTypeFilter.queryType)

    this.props.setQuery({
      query: docTranTypeFilter.query,
      value: docTranTypeFilter.value,
    })

    this.props.handleFilterChange("docTranTypeFilter", docTranTypeFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const docTranTypeFilter = pref.docTranTypeFilter || {}
    docTranTypeFilter.queryType = value
    docTranTypeFilter.value = docTranTypeFilter.value || ""
    docTranTypeFilter.query = prepareQuery(docTranTypeFilter.value, docTranTypeFilter.queryType)

    this.props.setQuery({
      query: docTranTypeFilter.query,
      value: docTranTypeFilter.value,
    })

    this.props.handleFilterChange("docTranTypeFilter", docTranTypeFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).docTranTypeFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} style={{ maxWidth: "200px" }} value={((pref || {}).docTranTypeFilter || {}).value || ""}>
            <option value="">Transaction Type</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default DocTranTypeFilter
