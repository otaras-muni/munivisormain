/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import styled from "styled-components"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "relatedActivityDetails.activityType.raw": value
                }
              },
              {
                match: {
                  "relatedActivityDetails.activitySubType.raw": value
                }
              },
              {
                match: {
                  "taskDetails.taskType.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class taskTypeFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.taskTypeFilter || {})) !== JSON.stringify((oldPref.taskTypeFilter || {}))) {
      this.props.setQuery({
        query: (pref.taskTypeFilter || {}).query || {},
        value: (pref.taskTypeFilter || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = () => {
    const options = [
      "Debt",
      "Managed Client RFP",
      "Derivative",
      "Bank Loan",
      "marfp",
      "Compliance"
    ]
    this.setState({
      options,
    })
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const taskTypeFilter = pref.taskTypeFilter || {}
    taskTypeFilter.queryType = taskTypeFilter.queryType || "must"
    taskTypeFilter.value = value
    taskTypeFilter.query = prepareQuery(taskTypeFilter.value, taskTypeFilter.queryType)

    this.props.setQuery({
      query: taskTypeFilter.query,
      value: taskTypeFilter.value,
    })

    this.props.handleFilterChange("taskTypeFilter", taskTypeFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const taskTypeFilter = pref.taskTypeFilter || {}
    taskTypeFilter.queryType = value
    taskTypeFilter.value = taskTypeFilter.value || ""
    taskTypeFilter.query = prepareQuery(taskTypeFilter.value, taskTypeFilter.queryType)

    this.props.setQuery({
      query: taskTypeFilter.query,
      value: taskTypeFilter.value,
    })

    this.props.handleFilterChange("taskTypeFilter", taskTypeFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-small is-link" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).taskTypeFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-small is-link">
          <select onChange={this.handleQueryChange} style={{ maxWidth: "200px" }} value={((pref || {}).taskTypeFilter || {}).value || ""}>
            <option value="">Transaction Type</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default taskTypeFilter
