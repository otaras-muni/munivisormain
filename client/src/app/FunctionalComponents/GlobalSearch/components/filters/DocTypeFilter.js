/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX, DOCUMENT_TYPE } from "../../../../../constants"
import { getHeaders } from "../../../../../globalutilities"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "userAddresses.state.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class DocTypeFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.docTypeFilter || {})) !== JSON.stringify((oldPref.docTypeFilter || {}))) {
      this.props.setQuery({
        query: (pref.docTypeFilter || {}).query || {},
        value: (pref.docTypeFilter || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = async () => {
    axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/${DOCUMENT_TYPE}/_search`, {
      "size": 0,
      "aggs": {
        "doc_types": {
          "terms": {
            "field": "docType.raw"
          }
        },
      }
    }, {headers: getHeaders()}).then(res => {
      const docTypeOptions = res.data.aggregations.doc_types.buckets

      const result = _.uniqBy([...docTypeOptions], "key")
      const options = result.map(state => state.key)
      if(!this.isCancelled) {
        this.setState({
          options,
        })
      }
    })
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const docTypeFilter = pref.docTypeFilter || {}
    docTypeFilter.queryType = docTypeFilter.queryType || "must"
    docTypeFilter.value = value
    docTypeFilter.query = prepareQuery(docTypeFilter.value, docTypeFilter.queryType)

    this.props.setQuery({
      query: docTypeFilter.query,
      value: docTypeFilter.value,
    })

    this.props.handleFilterChange("docTypeFilter", docTypeFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const docTypeFilter = pref.docTypeFilter || {}
    docTypeFilter.queryType = value
    docTypeFilter.value = docTypeFilter.value || ""
    docTypeFilter.query = prepareQuery(docTypeFilter.value, docTypeFilter.queryType)

    this.props.setQuery({
      query: docTypeFilter.query,
      value: docTypeFilter.value,
    })

    this.props.handleFilterChange("docTypeFilter", docTypeFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).docTypeFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} style={{ maxWidth: "200px" }} value={((pref || {}).docTypeFilter || {}).value || ""}>
            <option value="">Document Type</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default DocTypeFilter
