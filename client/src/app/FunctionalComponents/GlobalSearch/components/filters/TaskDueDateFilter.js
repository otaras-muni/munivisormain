import React from "react"
import moment from "moment"
import styled from "styled-components"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType = "must") => {
  let query = {}
  switch (value) {
  case "today":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format("YYYY-MM-DD"),
              gte: moment().format("YYYY-MM-DD")
            }
          }
        }
      }
    }
    break
  case "lastWeek":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().subtract(1, "week").endOf("week").format(),
              gte: moment().subtract(1, "week").startOf("week").format()
            }
          }
        }
      }
    }
    break
  case "lastMonth":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format(),
              gte: moment().subtract(1, "months").date(1).format()
            }
          }
        }
      }
    }
    break
  case "last3Months":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format(),
              gte: moment().subtract(3, "months").date(1).format()
            }
          }
        }
      }
    }
    break
  case "last6Months":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format(),
              gte: moment().subtract(6, "months").date(1).format()
            }
          }
        }
      }
    }
    break
  case "last12Months":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format(),
              gte: moment().subtract(12, "months").date(1).format()
            }
          }
        }
      }
    }
    break
  case "last24Months":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format(),
              gte: moment().subtract(24, "months").date(1).format()
            }
          }
        }
      }
    }
    break
  case "last36Months":
    query = {
      bool: {
        [queryType]: {
          range: {
            "taskDetails.taskEndDate": {
              lte: moment().format(),
              gte: moment().subtract(36, "months").date(1).format()
            }
          }
        }
      }
    }
    break
  default:
    query = {}
    break
  }

  return query
}

class TaskDueDateFilter extends React.Component {

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.taskDueDateFilter || {})) !== JSON.stringify((oldPref.taskDueDateFilter || {}))) {
      this.props.setQuery({
        query: (pref.taskDueDateFilter || {}).query || {},
        value: (pref.taskDueDateFilter || {}).value || ""
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const taskDueDateFilter = pref.taskDueDateFilter || {}
    taskDueDateFilter.queryType = taskDueDateFilter.queryType || "must"
    taskDueDateFilter.value = value
    taskDueDateFilter.query = prepareQuery(taskDueDateFilter.value, taskDueDateFilter.queryType)

    this.props.setQuery({
      query: taskDueDateFilter.query,
      value: taskDueDateFilter.value,
    })

    this.props.handleFilterChange("taskDueDateFilter", taskDueDateFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const taskDueDateFilter = pref.taskDueDateFilter || {}
    taskDueDateFilter.queryType = value
    taskDueDateFilter.value = taskDueDateFilter.value || ""
    taskDueDateFilter.query = prepareQuery(taskDueDateFilter.value, taskDueDateFilter.queryType)

    this.props.setQuery({
      query: taskDueDateFilter.query,
      value: taskDueDateFilter.value,
    })

    this.props.handleFilterChange("taskDueDateFilter", taskDueDateFilter)
  }

  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-small is-link" style={{ marginRight: "10px" }}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).taskDueDateFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-small is-link">
          <select onChange={this.handleQueryChange} value={((pref || {}).taskDueDateFilter || {}).value || ""}>
            <option value="">Due Date</option>
            <option value="today">Today</option>
            <option value="lastWeek">Last Week</option>
            <option value="lastMonth">Last Month</option>
            <option value="last3Months">Last 3 Months</option>
            <option value="last6Months">Last 6 Months</option>
            <option value="last12Months">Last 12 Months</option>
            <option value="last24Months">Last 24 Months</option>
            <option value="last36Months">Last 36 Months</option>
            <option value="all">All</option>
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default TaskDueDateFilter
