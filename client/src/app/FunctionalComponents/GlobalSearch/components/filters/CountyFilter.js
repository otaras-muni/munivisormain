import React from "react"
import styled from "styled-components"
import Loader from "Global/Loader"
import {getPicklistValues} from "GlobalUtils/helpers"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "dealIssueTranCounty.raw": value
                }
              },
              {
                match: {
                  "rfpTranCounty.raw": value
                }
              },
              {
                match: {
                  "bankLoanSummary.actTranCounty.raw": value
                }
              },
              {
                match: {
                  "maRfpSummary.actCounty.raw": value
                }
              },
              {
                match: {
                  "actTranCounty.actCounty.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class CountyFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: [],
      loading:true
    }
  }

  async componentDidMount() {
    const [county] = await getPicklistValues(["LKUPCOUNTRY"])
    if(!this.isCancelled) {
      this.setState({
        options: county[3],
        loading: false
      })
    }
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.countyFilter || {})) !== JSON.stringify((oldPref.countyFilter || {}))) {
      this.props.setQuery({
        query: (pref.countyFilter || {}).query || {},
        value: (pref.countyFilter || {}).value || ""
      })
    }
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const countyFilter = pref.countyFilter || {}
    countyFilter.queryType = countyFilter.queryType || "must"
    countyFilter.value = value
    countyFilter.query = prepareQuery(countyFilter.value, countyFilter.queryType)

    this.props.setQuery({
      query: countyFilter.query,
      value: countyFilter.value,
    })

    this.props.handleFilterChange("countyFilter", countyFilter)
    this.props.handleFilterChange("stateFilter", {})
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const countyFilter = pref.countyFilter || {}
    countyFilter.queryType = value
    countyFilter.value = countyFilter.value || ""
    countyFilter.query = prepareQuery(countyFilter.value, countyFilter.queryType)

    this.props.setQuery({
      query: countyFilter.query,
      value: countyFilter.value,
    })

    this.props.handleFilterChange("countyFilter", countyFilter)
  }

  render() {
    const { pref } = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).countyFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).countyFilter || {}).value || ""}>
            <option value="">State</option>
            {this.state.options && this.state.options["United States"] &&  Object.keys(this.state.options["United States"]).length ?
              Object.keys(this.state.options["United States"]).map((key, i) => (
                <option key={i} value={key}>
                  {key}
                </option>
              )): null}
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default CountyFilter
