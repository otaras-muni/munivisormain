import React from "react"
import styled from "styled-components"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}
  switch(value) {
  case "lessThan10MUsd":
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                range: {
                  dealIssueParAmount: {
                    lte: 10000000,
                  }
                }
              },
              {
                range: {
                  "bankLoanTerms.parAmount": {
                    lte: 10000000
                  }
                }
              },
              {
                range: {
                  "actTranParAmount": {
                    lte: 10000000
                  }
                }
              }
            ]
          }
        }
      }
    }
    break
  case "11mTo20M":
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                range: {
                  dealIssueParAmount: {
                    lte: 20000000,
                    gte: 11000000
                  }
                }
              },
              {
                range: {
                  "bankLoanTerms.parAmount": {
                    lte: 20000000,
                    gte: 11000000
                  }
                }
              },
              {
                range: {
                  "actTranParAmount": {
                    lte: 20000000,
                    gte: 11000000
                  }
                }
              }
            ]
          }
        }
      }
    }
    break
  case "21MTo50M":
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                range: {
                  dealIssueParAmount: {
                    lte: 50000000,
                    gte: 21000000
                  }
                }
              },
              {
                range: {
                  "bankLoanTerms.parAmount": {
                    lte: 50000000,
                    gte: 21000000
                  }
                }
              },
              {
                range: {
                  "actTranParAmount": {
                    lte: 50000000,
                    gte: 21000000
                  }
                }
              }
            ]
          }
        }
      }
    }
    break
  case "51MTo100M":
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                range: {
                  dealIssueParAmount: {
                    lte: 100000000,
                    gte: 51000000
                  }
                }
              },
              {
                range: {
                  "bankLoanTerms.parAmount": {
                    lte: 100000000,
                    gte: 51000000
                  }
                }
              },
              {
                range: {
                  "actTranParAmount": {
                    lte: 100000000,
                    gte: 51000000
                  }
                }
              }
            ]
          }
        }
      }
    }
    break
  case "greaterThan100M":
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                range: {
                  dealIssueParAmount: {
                    gte: 100000000
                  }
                }
              },
              {
                range: {
                  "bankLoanTerms.parAmount": {
                    gte: 100000000
                  }
                }
              },
              {
                range: {
                  "actTranParAmount": {
                    gte: 100000000
                  }
                }
              }
            ]
          }
        }
      }
    }
    break
  default:
    query = {}
    break
  }

  return query
}

class ParAmountFilter extends React.Component {


  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.parAmountFilter || {})) !== JSON.stringify((oldPref.parAmountFilter || {}))) {
      this.props.setQuery({
        query: (pref.parAmountFilter || {}).query || {},
        value: (pref.parAmountFilter || {}).value || ""
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const parAmountFilter = pref.parAmountFilter || {}
    parAmountFilter.queryType = parAmountFilter.queryType || "must"
    parAmountFilter.value = value
    parAmountFilter.query = prepareQuery(parAmountFilter.value, parAmountFilter.queryType)

    this.props.setQuery({
      query: parAmountFilter.query,
      value: parAmountFilter.value,
    })

    this.props.handleFilterChange("parAmountFilter", parAmountFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const parAmountFilter = pref.parAmountFilter || {}
    parAmountFilter.queryType = value
    parAmountFilter.value = parAmountFilter.value || ""
    parAmountFilter.query = prepareQuery(parAmountFilter.value, parAmountFilter.queryType)

    this.props.setQuery({
      query: parAmountFilter.query,
      value: parAmountFilter.value,
    })

    this.props.handleFilterChange("parAmountFilter", parAmountFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).parAmountFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).parAmountFilter || {}).value || ""}>
            <option value="">Par Amount</option>
            <option value="lessThan10MUsd">Less than USD 10M</option>
            <option value="11mTo20M">USD 11M - 20M</option>
            <option value="21MTo50M">USD 21M - 50M</option>
            <option value="51MTo100M">USD 51M - 100M</option>
            <option value="greaterThan100M">Greater than 100M</option>
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default ParAmountFilter
