import React from "react"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX, ELASTIC_SEARCH_TYPES } from "../../../../../constants"
import { getHeaders } from "../../../../../globalutilities"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "dealIssueUseOfProceeds.raw": value
                }
              },
              {
                match: {
                  "bankLoanSummary.actTranUseOfProceeds.raw": value
                }
              },
              {
                match: {
                  "maRfpSummary.actUseOfProceeds.raw": value
                }
              },
              {
                match: {
                  "actTranUseOfProceeds.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class UseOfProceedsFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.useOfProceedsFilter || {})) !== JSON.stringify((oldPref.useOfProceedsFilter || {}))) {
      this.props.setQuery({
        query: (pref.useOfProceedsFilter || {}).query || {},
        value: (pref.useOfProceedsFilter || {}).value || ""
      })
    }
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  getOptions = () => {
    axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.MARFPS}/_search`, {
      "size": 0,
      "aggs": {
        "deal_options": {
          "terms": {
            "field": "dealIssueUseOfProceeds.raw"
          }
        },
        "bankloan_options": {
          "terms": {
            "field": "bankLoanSummary.actTranUseOfProceeds.raw"
          }
        },
        "marfp_options": {
          "terms": {
            "field": "maRfpSummary.actUseOfProceeds.raw"
          }
        },
        "others_options": {
          "terms": {
            "field": "actTranUseOfProceeds.raw"
          }
        }
      }
    }, {headers: getHeaders()}).then(res => {
      const dealOptions = res.data.aggregations.deal_options.buckets
      const bankLoanOptions = res.data.aggregations.bankloan_options.buckets
      const marfpOptions = res.data.aggregations.marfp_options.buckets
      const othersOptions = res.data.aggregations.others_options.buckets

      const result = _.uniqBy([...dealOptions, ...bankLoanOptions, ...marfpOptions, ...othersOptions], "key")
      const filtered = result.filter(state => state.key !== "")
      const options = filtered.map(state => state.key)
      if(!this.isCancelled) {
        this.setState({
          options,
        })
      }
    })
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const useOfProceedsFilter = pref.useOfProceedsFilter || {}
    useOfProceedsFilter.queryType = useOfProceedsFilter.queryType || "must"
    useOfProceedsFilter.value = value
    useOfProceedsFilter.query = prepareQuery(useOfProceedsFilter.value, useOfProceedsFilter.queryType)

    this.props.setQuery({
      query: useOfProceedsFilter.query,
      value: useOfProceedsFilter.value,
    })

    this.props.handleFilterChange("useOfProceedsFilter", useOfProceedsFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const useOfProceedsFilter = pref.useOfProceedsFilter || {}
    useOfProceedsFilter.queryType = value
    useOfProceedsFilter.value = useOfProceedsFilter.value || ""
    useOfProceedsFilter.query = prepareQuery(useOfProceedsFilter.value, useOfProceedsFilter.queryType)

    this.props.setQuery({
      query: useOfProceedsFilter.query,
      value: useOfProceedsFilter.value,
    })

    this.props.handleFilterChange("useOfProceedsFilter", useOfProceedsFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).useOfProceedsFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).useOfProceedsFilter || {}).value || ""}>
            <option value="">Use of Proceeds</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default UseOfProceedsFilter
