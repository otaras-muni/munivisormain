import React from "react"
import { connect } from "react-redux"
import { updateGlobalSearchPref } from "../../../../StateManagement/actions"
import { ELASTIC_SEARCH_TYPES } from "../../../../../constants"

const mapDispatchToProps = dispatch => ({
  updatePrefs: (filter, data) => dispatch(updateGlobalSearchPref(filter, data))
})

class TypeFilter extends React.Component {
  handleQueryChange = e => {
    const { target } = e
    this.props.updatePrefs("tranTypeQuery", target.value)
  }
  render() {
    return (
      <div className="select is-fullwidth is-link is-small">
        <select
          onChange={this.handleQueryChange}
          defaultValue={this.props.defaultValue}
          id="tran-types"
        >
          <option value={`${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DERIVITIVE}`}>Deals / Bank Loans / Derivatives</option>
          <option value={`${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.OTHERS},${ELASTIC_SEARCH_TYPES.BUSINESSDEVELOPMENT}`}>RFPs / Others / Business Development</option>
          <option value={`${ELASTIC_SEARCH_TYPES.MARFPS}`}>MARFPs</option>
        </select>
      </div>
    )
  }
}

export default connect(
  null,
  mapDispatchToProps
)(TypeFilter)
