import React from "react"
import styled from "styled-components"
import { getPicklistValues } from "GlobalUtils/helpers"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType = "must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "dealIssueTranPrimarySector.raw": value
                }
              },
              {
                match: {
                  "rfpTranPrimarySector.raw": value
                }
              },
              {
                match: {
                  "actTranPrimarySector.raw": value
                }
              },
              {
                match: {
                  "actPrimarySector.raw": value
                }
              },
              {
                match: {
                  "actTranPrimarySector.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class PrimarySectorFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.primarySectorFilter || {})) !== JSON.stringify((oldPref.primarySectorFilter || {}))) {
      this.props.setQuery({
        query: (pref.primarySectorFilter || {}).query || {},
        value: (pref.primarySectorFilter || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = async () => {
    const [primarySectors] = await getPicklistValues(["LKUPPRIMARYSECTOR"])
    if (!this.isCancelled) {
      this.setState({
        options: primarySectors ? primarySectors[1] : []
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const primarySectorFilter = pref.primarySectorFilter || {}
    primarySectorFilter.queryType = primarySectorFilter.queryType || "must"
    primarySectorFilter.value = value
    primarySectorFilter.query = prepareQuery(primarySectorFilter.value, primarySectorFilter.queryType)

    this.props.setQuery({
      query: primarySectorFilter.query,
      value: primarySectorFilter.value,
    })

    this.props.handleFilterChange("primarySectorFilter", primarySectorFilter)
    this.props.handleFilterChange("secondarySectorFilter", {})
  }

  handleQueryTypeChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const primarySectorFilter = pref.primarySectorFilter || {}
    primarySectorFilter.queryType = value
    primarySectorFilter.value = primarySectorFilter.value || ""
    primarySectorFilter.query = prepareQuery(primarySectorFilter.value, primarySectorFilter.queryType)

    this.props.setQuery({
      query: primarySectorFilter.query,
      value: primarySectorFilter.value,
    })

    this.props.handleFilterChange("primarySectorFilter", primarySectorFilter)
  }

  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px" }}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).primarySectorFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).primarySectorFilter || {}).value || ""}>
            <option value="">Primary Sector</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default PrimarySectorFilter
