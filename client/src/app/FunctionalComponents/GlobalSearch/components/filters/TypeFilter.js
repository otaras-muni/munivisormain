import React from "react"
import { connect } from "react-redux"
import { updateGlobalSearchPref } from "../../../../StateManagement/actions"

const mapDispatchToProps = dispatch => ({
  updatePrefs: (filter, data) => dispatch(updateGlobalSearchPref(filter, data))
})

const transactionTypes = {
  transactions: "Transactions",
  entities: "Entities",
  users: "Users",
  documents: "Documents",
  tasks: "Tasks"
}

class TypeFilter extends React.Component {
  handleQueryChange = e => {
    const { target } = e
    if (target.value === "transactions") {
      setTimeout(() => {
        const element = document.getElementById("tran-types")
        element.click()
      }, 1000)
    }

    this.props.updatePrefs("typeQuery", target.value)
  }
  render() {
    return (
      <div className="select is-fullwidth is-link is-small">
        <select
          onChange={this.handleQueryChange}
          defaultValue={this.props.defaultValue}
        >
          <option value="">Filter With</option>
          {Object.keys(transactionTypes).map(key => (
            <option key={key} value={key}>
              {transactionTypes[key]}
            </option>
          ))}
        </select>
      </div>
    )
  }
}

export default connect(
  null,
  mapDispatchToProps
)(TypeFilter)
