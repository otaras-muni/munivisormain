/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX } from "../../../../../constants"
import { getHeaders } from "../../../../../globalutilities"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "userAddresses.state.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class UserStateFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.userStateFilter || {})) !== JSON.stringify((oldPref.userStateFilter || {}))) {
      this.props.setQuery({
        query: (pref.userStateFilter || {}).query || {},
        value: (pref.userStateFilter || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = async () => {
    const res = await axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/entityusers/_search`, {
      "size": 0,
      "aggs": {
        "state_options": {
          "terms": {
            "field": "userAddresses.state.raw"
          }
        }
      }
    }, {headers: getHeaders()})

    const dealoptions = await res.data.aggregations.state_options.buckets
    const result = _.uniqBy([...dealoptions], "key")
    const options = result.reduce((acc, state) => state.key ? [...acc, state.key] : [...acc], [])
    if(!this.isCancelled) {
      this.setState({
        options,
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const userStateFilter = pref.userStateFilter || {}
    userStateFilter.queryType = userStateFilter.queryType || "must"
    userStateFilter.value = value
    userStateFilter.query = prepareQuery(userStateFilter.value, userStateFilter.queryType)

    this.props.setQuery({
      query: userStateFilter.query,
      value: userStateFilter.value,
    })

    this.props.handleFilterChange("userStateFilter", userStateFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const userStateFilter = pref.userStateFilter || {}
    userStateFilter.queryType = value
    userStateFilter.value = userStateFilter.value || ""
    userStateFilter.query = prepareQuery(userStateFilter.value, userStateFilter.queryType)

    this.props.setQuery({
      query: userStateFilter.query,
      value: userStateFilter.value,
    })

    this.props.handleFilterChange("userStateFilter", userStateFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-small is-link" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).userStateFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-small is-link">
          <select onChange={this.handleQueryChange} style={{ maxWidth: "200px" }} value={((pref || {}).userStateFilter || {}).value || ""}>
            <option value="">State</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default UserStateFilter
