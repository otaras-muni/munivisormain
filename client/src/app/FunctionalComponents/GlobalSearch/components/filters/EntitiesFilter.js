/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import styled from "styled-components"
import axios from "axios"
import { connect } from "react-redux"
import _ from "lodash"
import { muniApiBaseURL } from "../../../../../globalutilities/consts"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "entityId.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

const mapStateToProps = (state) => ({
  auth: state.auth
})

class EntitiesFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.entitiesFilter || {})) !== JSON.stringify((oldPref.entitiesFilter || {}))) {
      this.props.setQuery({
        query: (pref.entitiesFilter || {}).query || {},
        value: (pref.entitiesFilter || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = async () => {
    const res = await axios({
      method: "GET",
      url: `${muniApiBaseURL}common/getalltenantentitydetails`,
      headers: { Authorization: this.props.auth.token },
    })

    const data = await res.data.entities
    const entities = data.map(item => ({ id: item.entityId, name: item.name}))
    this.setState({
      options: _.uniqBy(entities, "id")
    })
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const entitiesFilter = pref.entitiesFilter || {}
    entitiesFilter.queryType = entitiesFilter.queryType || "must"
    entitiesFilter.value = value
    entitiesFilter.query = prepareQuery(entitiesFilter.value, entitiesFilter.queryType)

    this.props.setQuery({
      query: entitiesFilter.query,
      value: entitiesFilter.value,
    })

    this.props.handleFilterChange("entitiesFilter", entitiesFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const entitiesFilter = pref.entitiesFilter || {}
    entitiesFilter.queryType = value
    entitiesFilter.value = entitiesFilter.value || ""
    entitiesFilter.query = prepareQuery(entitiesFilter.value, entitiesFilter.queryType)

    this.props.setQuery({
      query: entitiesFilter.query,
      value: entitiesFilter.value,
    })

    this.props.handleFilterChange("entitiesFilter", entitiesFilter)
  }
  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).entitiesFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} style={{ maxWidth: "200px" }} value={((pref || {}).entitiesFilter || {}).value || ""}>
            <option value="">Entities Filter</option>
            {
              this.state.options.map(state => (
                <option key={state.id} value={state.id}>{state.name}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default connect(mapStateToProps)(EntitiesFilter)
