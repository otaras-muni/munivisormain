import React from "react"
import moment from "moment"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { numberWithCommas } from "../../../../globalutilities/helpers"
import dateFormat from "../../../../globalutilities/dateFormat"

import RfpTranGraph from "./tranGraphs/RfpTranGrap"

const RfpResultSet = ({ data, allUrls, hasSubComponent }) => {
  const others = {}

  if (hasSubComponent) {
    others.SubComponent = row => (
      <RfpTranGraph row={row.original} allUrls={allUrls} />
    )
  }

  return (
    <ReactTable
      data={data}
      columns={[
        {
          id: "issuer",
          Header: "Issuer Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={allUrls[item.rfpTranIssuerId || item.actTranFirmId || item.actIssuerClient].entity || allUrls[item.rfpTranIssuerId || item.actTranFirmId || item.actIssuerClient].firms || ""} dangerouslySetInnerHTML={{ __html: (item.rfpTranIssuerFirmName || item.actTranFirmName || item.actIssuerClientEntityName || "-") }} />
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpTranIssuerFirmName || a.actTranFirmName || a.actTranFirmName || "").localeCompare((b.rfpTranIssuerFirmName || b.actTranFirmName || b.actTranFirmName || ""))
        },
        {
          id: "issueDescription",
          Header: "Issue Description/Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={allUrls[item._id].summary || allUrls[item._id].default || ""} dangerouslySetInnerHTML={{ __html: (item.rfpTranIssueName || item.rfpTranProjectDescription || item.actTranProjectDescription || item.actProjectName || "-") }} />


              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpTranIssueName || a.rfpTranProjectDescription || a.actTranProjectDescription || a.actProjectName || "").localeCompare((b.rfpTranIssueName || b.rfpTranProjectDescription || b.actTranProjectDescription || b.actProjectName || ""))
        },
        {
          id: "type",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.rfpTranType || item.actTranType || item.actType || "-") }} />
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpTranType || a.actTranType || a.actType || "").localeCompare((b.rfpTranType || b.actTranType || b.actType || ""))
        },
        {
          id: "principalAmount",
          Header: ()=> (<span title="Principal Amount($)">Principal Amount($)</span>),
          accessor: item => item,
          Cell: row => { // eslint-disable-line
            return (
              <div className="hpTablesTd wrap-cell-text">
                {numberWithCommas(row.value.actTranParAmount || "-")}
              </div>
            )
          },
          maxWidth: 150,
        },
        {
          id: "leadManager",
          Header: "Lead Manager",
          accessor: item => item,
          Cell: row => { // eslint-disable-line
            const item = row.value
            let manager = {
              rfpSelEvalContactId: "",
              rfpSelEvalContactName: "-"
            }
            if (item.actTranFirmLeadAdvisorName) {
              manager = {
                rfpSelEvalContactName: item.actTranFirmLeadAdvisorName,
                rfpSelEvalContactId: item.actTranFirmLeadAdvisorId
              }
            } else if (item.rfpEvaluationTeam) {
              const findResult = item.rfpEvaluationTeam.find(f => f.rfpSelEvalContactId === item.rfpTranAssignedTo[0])
              if (findResult) {
                manager = findResult
              }
            } else if (item.actLeadFinAdvClientEntityName) {
              manager = {
                rfpSelEvalContactName: item.actLeadFinAdvClientEntityName,
                rfpSelEvalContactId: item.actLeadFinAdvClientEntityId
              }
            }

            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={manager.rfpSelEvalContactId ? (allUrls[manager.rfpSelEvalContactId] || {}).users : "/"}>{manager.rfpSelEvalContactName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 150,
        },
        {
          id: "couponType",
          Header: "Coupon Type",
          accessor: item => item,
          Cell: row => { // eslint-disable-line
            return (
              <div className="hpTablesTd wrap-cell-text">
                -
              </div>
            )
          },
          maxWidth: 150,
        },
        {
          id: "purposeSector",
          Header: "Purpose/Sector",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <p dangerouslySetInnerHTML={{ __html: (item.rfpTranPrimarySector || item.actTranPrimarySector || item.actPrimarySector || "-") }} />

              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpTranPrimarySector || a.actTranPrimarySector || a.actPrimarySector || "").localeCompare((b.rfpTranPrimarySector || b.actTranPrimarySector || b.actPrimarySector || ""))
        },
        {
          id: "startDate",
          Header: "Start Date",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                {dateFormat(
                  item.rfpTranStartDate || item.actTranStartDate
                )}
              </div>
            )
          },
          sortMethod: (a, b) => {
            a = moment(
              a.rfpTranStartDate || a.actTranStartDate
            )
            b = moment(
              b.rfpTranStartDate || b.actTranStartDate
            )
            return a.diff(b)
          },
          maxWidth: 200
        },
        {
          id: "endDate",
          Header: "End Date",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                {dateFormat(
                  item.rfpTranExpectedEndDate || item.actTranExpectedEndDate
                )}
              </div>
            )
          },
          sortMethod: (a, b) => {
            a = moment(
              a.rfpTranExpectedEndDate || a.actTranExpectedEndDate
            )
            b = moment(
              b.rfpTranExpectedEndDate || b.actTranExpectedEndDate
            )
            return a.diff(b)
          },
          maxWidth: 200
        },
        {
          id: "status",
          Header: "Status",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.rfpTranStatus || item.actTranStatus || item.actStatus || "-") }} />
            )
          },
          sortMethod: (a, b) => (a.rfpTranStatus || a.actTranStatus || a.actStatus || "").localeCompare((b.rfpTranStatus || b.actTranStatus || b.actStatus || "")),
          maxWidth: 150
        },
      ]}
      showPagination={false}
      // PaginationComponent={Pagination}
      pageSize={data.length}
      minRows={2}
      className="-striped -highlight is-bordered"
      {...others}
    />
  )
}

export default RfpResultSet
