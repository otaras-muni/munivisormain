import React from "react"
import styled from "styled-components"
import { Link } from "react-router-dom"
import ReactTable from "react-table"

export const appendTranIds = (data = [], row, allUrls) => (
  data.map(item => ({ ...item, docLink: allUrls[row._id].documents || "" }))
)

const Container = styled.div`
  padding: 20px;
`

const documents = [
  {
    id: "fileName",
    Header: "File Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          <Link to={item.docLink}>{item.docFileName || "-"}</Link>
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docFileName || "").localeCompare((b.docFileName || ""))
  },
  {
    id: "docType",
    Header: "Document Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.docType || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docType || "").localeCompare((b.docType || ""))
  },
  {
    id: "docCategory",
    Header: "Category",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.docCategory || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docCategory || "").localeCompare((b.docCategory || ""))
  },
  {
    id: "docSubCategory",
    Header: "Sub Category",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.docSubCategory || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docSubCategory || "").localeCompare((b.docSubCategory || ""))
  }
]

const TranGraph = ({ row, allUrls }) => (
  <Container>
    <b> Associated Participants </b>
    <ReactTable
      data={row.dealIssueParticipants || row.bankLoanParticipants || row.derivativeParticipants || []}
      columns={[
        {
          id: "participantName",
          Header: "Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[(item.dealPartContactId || item.partContactId)].users || allUrls[(item.dealPartContactId || item.partContactId)].thirdparties || "/"}>{item.dealPartContactName || item.partContactName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.dealPartContactName || a.partContactName || "").localeCompare((b.dealPartContactName || b.partContactName || ""))
        },
        {
          id: "participantFirmName",
          Header: "Firm Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[(item.dealPartFirmId || item.partFirmId)].entity || allUrls[item.dealPartFirmId || item.partFirmId].firms || ""}>{item.dealPartFirmName || item.partFirmName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.dealPartFirmName || a.partFirmName || "").localeCompare((b.dealPartFirmName || b.partFirmName || ""))
        },
        {
          id: "participantType",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.dealPartType || item.partType || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.dealPartType || a.partType || "").localeCompare((b.dealPartType || b.partType || ""))
        },
        {
          id: "participantState",
          Header: "State",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.dealParticipantState || item.participantState || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.dealParticipantState || a.participantState || "").localeCompare((b.dealParticipantState || b.participantState || ""))
        },
      ]}
      minRows={2}
      pageSize={(row.dealIssueParticipants || row.bankLoanParticipants || row.derivativeParticipants || []).length}
      showPagination={false}
      style={{
        maxHeight: "300px"
      }}
    />
    <br />
    <b> Associated Documents </b>
    <ReactTable
      data={appendTranIds(row.dealIssueDocuments || row.bankLoanDocuments || row.derivativeDocuments || [], row, allUrls)}
      columns={documents}
      minRows={2}
      pageSize={appendTranIds(row.dealIssueDocuments || row.bankLoanDocuments || row.derivativeDocuments || [], row, allUrls).length}
      showPagination={false}
      style={{
        maxHeight: "300px"
      }}
    />
  </Container>
)

export default TranGraph
