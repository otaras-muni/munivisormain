import React from "react"
import moment from "moment"
import { Link } from "react-router-dom"
import ReactTable from "react-table"

import getParAmount from "../../../../globalutilities/amountDecorator"
import { leadManager } from "./TranResultSet"
import { numberWithCommas } from "../../../../globalutilities/helpers"
import dateFormat from "../../../../globalutilities/dateFormat"

import DealsTranGraph from "./tranGraphs/DealsTranGraph"

const DealsResultSet = ({ data, allUrls, hasSubComponent }) => {
  const others = {}

  if (hasSubComponent) {
    others.SubComponent = row => (
      <DealsTranGraph row={row.original} allUrls={allUrls} />
    )
  }
  return (
    <ReactTable
      data={data}
      columns={[
        {
          id: "issuer",
          Header: "Issuer Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value

            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={`${(allUrls[item.dealIssueTranIssuerId || item.actTranClientId] || {}).entity || ""}`} dangerouslySetInnerHTML={{ __html: (item.dealIssueTranIssuerFirmName || item.actTranClientName || "-") }} />

              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.dealIssueTranIssuerFirmName || a.actTranClientName || "").localeCompare((b.dealIssueTranIssuerFirmName || b.actTranClientName || ""))
        },
        {
          id: "description",
          Header: ()=> (<span title="Issue Description/Name">Issue Description/Name</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value

            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={(allUrls[item._id] || {}).summary || ""} dangerouslySetInnerHTML={{ __html: ((item.dealIssueTranIssueName || item.dealIssueTranProjectDescription) || (item.actTranIssueName || item.actTranProjectDescription)) }} />


              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => ((a.dealIssueTranIssueName || a.dealIssueTranProjectDescription) || (a.actTranIssueName || a.actTranProjectDescription || "")).localeCompare(b.dealIssueTranIssueName || (b.actTranIssueName || b.actTranProjectDescription || ""))
        },
        {
          id: "type",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <p dangerouslySetInnerHTML={{ __html: (item.dealIssueTranType || (`${item.actTranType || item.actType || '-'} / ${item.actTranSubType || item.actSubType || '-'}`)) }} />

              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.dealIssueTranType || (`${a.actTranType} / ${a.actTranSubType}`)).localeCompare((b.dealIssueTranType || (`${b.actTranType} / ${b.actTranSubType}`)))
        },
        {
          id: "principalAmount",
          Header: ()=> (<span title="Principal Amount($)">Principal Amount($)</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value
            const amt = getParAmount(item)
            return (
              <div className="hpTablesTd wrap-cell-text">
                {numberWithCommas(amt > 0 ? amt : "-")}
              </div>
            )
          },
          maxWidth: 150,
          sortMethod: (a, b) => getParAmount(a) - getParAmount(b)
        },
        {
          id: "leadManager",
          Header: ()=> (<span title="Lead Manager">Lead Manager</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value
            const val = item.dealIssueParticipants
              ? leadManager(item.dealIssueParticipants, "dealPartContactName", "dealPartContactId", "dealPartType")
              : leadManager((item.derivativeParticipants || item.bankLoanParticipants), "partContactName", "partContactId", "partType")
            return (
              <Link to={(allUrls[val.id] || {}).users || ""} className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{
                __html: (val ? val.name : "-")
              }} />
            )
          },
          sortMethod: (a, b) => {
            const aval = a.dealIssueParticipants
              ? leadManager(a.dealIssueParticipants, "dealPartContactName", "dealPartContactId", "dealPartType")
              : leadManager((a.derivativeParticipants || a.bankLoanParticipants), "partContactName", "partContactId", "partType")
            const bval = b.dealIssueParticipants
              ? leadManager(b.dealIssueParticipants, "dealPartContactName", "dealPartContactId", "dealPartType")
              : leadManager((b.derivativeParticipants || b.bankLoanParticipants), "partContactName", "partContactId", "partType")

            return (aval || {}).name.localeCompare((bval || {}).name)
          },
          maxWidth: 150
        },
        {
          id: "couponType",
          Header: ()=> (<span title="Coupon Type">Coupon Type</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">

                <p dangerouslySetInnerHTML={{
                  __html: (((item.dealIssueSeriesDetails || {}).seriesPricingDetails || {}).dealSeriesRateType ||
                    (item.bankLoanTerms || {}).paymentType ||
                    (item.derivTradeClientPayLeg ? `client: ${(item.derivTradeClientPayLeg || {}).paymentType || "-"} / dealer: ${(item.derivTradeDealerPayLeg || {}).paymentType || "-"}` : "-"))
                }} />

              </div>
            )
          },
          sortMethod: (a, b) => (((a.dealIssueSeriesDetails || {}).seriesPricingDetails || {}).dealSeriesRateType ||
            (a.bankLoanTerms || {}).paymentType ||
            a.derivTradeClientPayLeg ? `client: ${(a.derivTradeClientPayLeg || {}).paymentType || ""} / dealer: ${(a.derivTradeDealerPayLeg || {}).paymentType || ""}` : "").localeCompare((((b.dealIssueSeriesDetails || {}).seriesPricingDetails || {}).dealSeriesRateType ||
              (b.bankLoanTerms || {}).paymentType ||
              b.derivTradeClientPayLeg ? `client: ${(b.derivTradeClientPayLeg || {}).paymentType || ""} / dealer: ${(b.derivTradeDealerPayLeg || {}).paymentType || ""}` : "")),
          maxWidth: 150
        },
        {
          id: "purposeSector",
          Header: ()=> (<span title="Purpose/Sector">Purpose/Sector</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <p dangerouslySetInnerHTML={{ __html: (item.dealIssueTranPrimarySector || item.actTranPrimarySector || "-") }} />
              </div>
            )
          },
          sortMethod: (a, b) => (a.dealIssueTranPrimarySector || a.actTranPrimarySector || "").localeCompare(b.dealIssueTranPrimarySector || b.actTranPrimarySector || ""),
          maxWidth: 150
        },
        {
          id: "pricingDate",
          Header: ()=> (<span title="Expected Pricing Date">Expected Pricing Date</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                {dateFormat(
                  item.dealIssuePricingDate ||
                  (item.bankLoanSummary || {}).actTranClosingDate ||
                  (item.derivativeSummary || {}).tranTradeDate
                )}
              </div>
            )
          },
          sortMethod: (a, b) => {
            a = moment(
              a.dealIssuePricingDate ||
              (a.bankLoanSummary || {}).actTranClosingDate ||
              (a.derivativeSummary || {}).tranTradeDate
            )
            b = moment(
              b.dealIssuePricingDate ||
              (b.bankLoanSummary || {}).actTranClosingDate ||
              (b.derivativeSummary || {}).tranTradeDate
            )
            return a.diff(b)
          },
          maxWidth: 200
        },
        {
          id: "closingDate",
          Header: ()=> (<span title="Expected Closing Date">Expected Closing Date</span>),
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                {dateFormat(
                  item.dealIssueTranExpectedEndDate ||
                  (item.bankLoanSummary || {}).actTranClosingDate ||
                  (item.derivativeSummary || {}).tranEndDate
                )}
              </div>
            )
          },
          sortMethod: (a, b) => {
            a = moment(
              a.dealIssueTranExpectedEndDate ||
              (a.bankLoanSummary || {}).actTranClosingDate ||
              (a.derivativeSummary || {}).tranEndDate
            )
            b = moment(
              b.dealIssueTranExpectedEndDate ||
              (b.bankLoanSummary || {}).actTranClosingDate ||
              (b.derivativeSummary || {}).tranEndDate
            )
            return a.diff(b)
          },
          maxWidth: 200
        },
        {
          id: "status",
          Header: "Status",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{
                __html: (item.dealIssueTranStatus ||
                  (item.bankLoanSummary || {}).actTranStatus ||
                  (item.derivativeSummary || {}).tranStatus ||
                  item.actTranStatus || item.actStatus || "-")
              }} />
            )
          },
          sortMethod: (a, b) => (a.dealIssueTranStatus ||
            (a.bankLoanSummary || {}).actTranStatus ||
            (a.derivativeSummary || {}).tranStatus || a.actTranStatus || a.actStatus || "").localeCompare((b.dealIssueTranStatus ||
              (b.bankLoanSummary || {}).actTranStatus ||
              (b.derivativeSummary || {}).tranStatus || b.actTranStatus || b.actStatus || "")),
          maxWidth: 150
        },
      ]}
      showPagination={false}
      // PaginationComponent={Pagination}
      pageSize={data.length}
      minRows={2}
      className="-striped -highlight is-bordered"
      {...others}
    />
  )
}

export default DealsResultSet
