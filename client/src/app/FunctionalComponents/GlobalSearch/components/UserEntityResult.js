import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import UserGraph from "./userGraphs"

const userColumns = [
  {
    id: "userFirstName",
    Header: "First Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          <Link to={`/addnew-contact/${item._id}`} dangerouslySetInnerHTML={{__html: (item.userFirstName || "-")}} />
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.userFirstName || "").localeCompare((b.userFirstName || ""))
  },
  {
    id: "userLastName",
    Header: "Last Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (item.userLastName || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.userLastName || "").localeCompare((b.userLastName || ""))
  },
  {
    id: "phone",
    Header: "Phone Number",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const phones = item.userPhone.filter(i => i.phonePrimary === true)[0] || {}
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (phones.phoneNumber || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => {
      const aPhone = a.userPhone.filter(i => i.phonePrimary === true)[0] || {}
      const bPhone = b.userPhone.filter(i => i.phonePrimary === true)[0] || {}
      return (aPhone.phoneNumber || "").localeCompare((bPhone.phoneNumber || ""))
    }
  },
  {
    id: "email",
    Header: "Email",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const email = item.userEmails.filter(i => i.emailPrimary === true)[0] || {}
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (email.emailId || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => {
      const aemail = a.userEmails.filter(i => i.emailPrimary === true)[0] || {}
      const bemail = b.userEmails.filter(i => i.emailPrimary === true)[0] || {}
      return (aemail.emailId || "").localeCompare((bemail.emailId || ""))
    }
  },
]

const UserEntityResultSet = ({ data }) => (
  <div style={{ marginTop: "20px"}}>
    <ReactTable
      data={data}
      pageSize={data.length}
      columns={userColumns}
      showPagination={false}
      minRows={2}
      className="-striped -highlight is-bordered"
      SubComponent={row => (
        <UserGraph row={row.original} />
      )}
    />
  </div>
)

export default UserEntityResultSet
