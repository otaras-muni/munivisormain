import React, { Component } from "react"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import { Multiselect } from "react-widgets"
import MaskedInput from "react-text-mask"

import { Modal } from "Global/BulmaModal"
import EntityLookup from "Global/EntityLookup"
import Loader from "Global/Loader"

import { getPicklistValues, getEntityDetails, saveEntityDetails, getUniqValueFromArray,
  updateAuditLog, checkNavAccessEntitlrment } from "GlobalUtils/helpers"

import FirmAddressListForm from "../EntityManagement/CommonComponents/FirmAddressListForm"
import HistoricalData from "../../GlobalComponents/HistoricalData"
import Notes from "../../GlobalComponents/Notes"

// Save Modal Definition
export const SaveModal = ({ message, closeModal, savemodalState, onConfirmed }) => {
  if (!savemodalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" autoFocus onClick={onConfirmed} >Yes</button>
            <button className="button" onClick={closeModal} >No</button>
          </div>
        </footer>
      </div>
    </div>
  )
}


const toastTimeout = 1000

const defaultValues = {
  isMuniVisorClient: false
}

const getInitialAddress = () => (
  [
    {
      addressName: "",
      isPrimary: false,
      isHeadQuarter: false,
      /* isActive: true, */
      website: "",
      officePhone: [
        {
          countryCode: "",
          phoneNumber: "",
          extension: ""
        }
      ],
      officeFax: [
        {
          faxNumber: ""
        }
      ],
      officeEmails: [
        {
          emailId: ""
        }
      ],
      addressLine1: "",
      addressLine2: "",
      country: "",
      state: "",
      city: "",
      zipCode: {
        zip1: "",
        zip2: ""
      },
      formatted_address: "",
      url: "",
      location: {
        longitude: "",
        latitude: ""
      }
    }
  ]
)

const getInitialState = () => ({
  expanded: {
    entityTypeSection: true,
  },
  entityId: "",
  entityType: "Third Party",
  viewOnly: true,
  entityName: "",
  entityAliases: [],
  aliasIndex: -1,
  marketRole: [],
  legalRole: [],
  taxId: "",
  addresses: getInitialAddress(),
  modalState: false,
  savemodalState: false,
  modalIndex: -1,
  modalType: "",
  error: { addresses: getInitialAddress() },
  generalError: "",
  updateData: {}
})

class ThirdPartyNew extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...getInitialState(), waiting: true,
      marketRolesList: [], legalTypes: [], countryResult: [],
      currentEntity: {}
    }
    this.onChangeEntityName = this.onChangeEntityName.bind(this)
    this.changeField = this.changeField.bind(this)
    this.saveBasicDetails = this.saveBasicDetails.bind(this)
    this.saveCallback = this.saveCallback.bind(this)
    this.changeMarketRole = this.changeMarketRole.bind(this)
    this.changeLegalRole = this.changeLegalRole.bind(this)
    this.onSaveAddress = this.onSaveAddress.bind(this)
    this.saveAddressChanges = this.saveAddressChanges.bind(this)
    this.getEntityData = this.getEntityData.bind(this)
    this.getEntityDetailsCallback = this.getEntityDetailsCallback.bind(this)
    this.addAlias = this.addAlias.bind(this)
    this.cancelAlias = this.cancelAlias.bind(this)
    // this.toggleModal = this.toggleModal.bind(this)
  }

  async componentDidMount() {
    console.log("did mount")
    const result = await getPicklistValues(["LKUPPARTICIPANTTYPE", /* "LKUPCOUNTRY", */ "LKUPLEGALTYPE"])
    console.log("result : ", result)
    let participantTypesList = result[0] && result[0][1] ? result[0][1] : []
    const disabledMarketRole = participantTypesList && participantTypesList.filter(f => !f.included).map(e => e.label)
    participantTypesList = participantTypesList && participantTypesList.map(e => e.label)
    // const marketRolesList = participantTypesList.filter(e => !(/ counsel/.test(e.toLocaleLowerCase())))
    // const legalTypes = participantTypesList.filter(e => (/ counsel/.test(e.toLocaleLowerCase())))
    // const countryResult = result[1] || []
    let legalTypes = result[1] && result[1][1] ? result[1][1] : []
    const disabledLegalTypes = legalTypes && legalTypes.filter(f => !f.included).map(e => e.label)
    legalTypes = legalTypes && legalTypes.map(e => e.label)

    const marketRolesList = participantTypesList.filter(e => !legalTypes.includes(e))
    marketRolesList.unshift("Legal")

    const { nav2 } = this.props
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit

    this.setState({
      /* countryResult, */ marketRolesList, legalTypes, disabledMarketRole, disabledLegalTypes,
      entityId: nav2 || "", viewOnly, waiting: false
    }, this.getEntityData)
  }

  async componentWillReceiveProps(nextProps) {
    const { nav2 } = nextProps
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit
    if (nav2 !== this.props.nav2) {
      this.setState({ entityId: nav2 || "", viewOnly }, this.getEntityData)
    }
  }

  onSaveAddress(addressList) {
    console.log("addressList : ", addressList)
    this.setState({ addressList }, this.saveAddressChanges)
  }

  onChangeEntityName(entityName) {
    // this.setState({ entityName })
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.entityName = ""
      return { entityName, error }
    })
  }

  getEntityDetailsCallback = async (err, res) => {
    if (err) {
      this.setState({ waiting: false, generalError: "Error in getting entity data" })
    } else {
      console.log("res : ", res)
      const currentEntity = res[0] || {}
      if(currentEntity.relationshipType !== "Third Party") {
        this.setState({
          generalError: `Wrong Entity Type provided for ${currentEntity.firmName}`,
          waiting: false
        })
        return
      }
      const { entityType, firmName, taxId, addresses, entityFlags,
        entityAliases, notes } = currentEntity
      const { legalTypes } = this.state

      let marketRole = ((entityFlags && entityFlags.marketRole) || []).filter(e => !legalTypes.includes(e))
      if (entityFlags && entityFlags.marketRole && entityFlags.marketRole.filter(e => legalTypes.includes(e)).length) {
        marketRole.unshift("Legal")
        marketRole = await getUniqValueFromArray(marketRole)
      }
      const legalRole = ((entityFlags && entityFlags.marketRole) || []).filter(e => legalTypes.includes(e))

      this.setState({
        waiting: false,
        notes: (notes || []),
        generalError: "",
        currentEntity,
        entityAliases,
        aliasIndex: -1,
        entityType,
        entityName: { _id: "", firmName, participantType: entityType, isExisting: true },
        marketRole,
        legalRole,
        taxId,
        addresses,
        error: { addresses: getInitialAddress() }
      })
    }
  }

  getEntityData() {
    const { entityId } = this.state
    if (entityId) {
      this.setState({ waiting: true })
      getEntityDetails(entityId, this.getEntityDetailsCallback)
    }
  }

  toggleExpand(val) {
    console.log("toggle : ", val)
    // e.preventDefault()
    this.setState(prevState => {
      const expanded = { ...prevState.expanded }
      Object.keys(expanded).forEach(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
      return { expanded }
    })
  }

  toggleSaveModal = () => {
    this.setState(prev => {
      const newState = !prev.savemodalState

      return { savemodalState: newState }
    })
  }

  showModal = () => {
  this.setState({
    savemodalState: true,
    onConfirmed: this.saveBasicDetails,
    modalMessage: "Are you sure you want to save the third party details?"
  })
}
  changeField(e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    this.setState(prevState => {
      const error = { ...prevState.error }
      error[name] = ""
      if (name === "taxId" && value.replace(/[^0-9]/g, "").length !== 9) {
        error.taxId = "Tax Id should have 9 digits"
      }
      return { [name]: value, error }
    })
  }

  changeMarketRole(marketRole) {
    if (marketRole.includes("Legal")) {
      this.setState(prevState => {
        if (prevState.marketRole.includes("Legal")) {
          return { marketRole }
        }
        return { marketRole, legalRole: prevState.legalTypes }
      })
    } else {
      this.setState({ marketRole, legalRole: [] })
    }
  }

  changeLegalRole(legalRole) {
    this.setState({ legalRole })
  }

  toggleModal(modalType, modalIndex) {
    this.setState(prev => {
      const modalState = !prev.modalState
      return { modalState, modalType, modalIndex }
    })
  }

  checkDupInArray(arr) {
    console.log("arr : ", arr)
    let result = [-1, -1]
    arr.some((e, i) => {
      if(e) {
        const dupIdx = arr.slice(i + 1).findIndex(d => d === e)
        if(dupIdx > -1) {
          result = [i, dupIdx+i+1]
          return true
        }
      }
    })
    console.log("dupResult : ", result)
    return result
  }

  checkEmptyArray(arr) {
    let result = -1
    arr.some((e, i) => {
      if(!e) {
        result = i
        return true
      }
    })
    return result
  }

  checkEmptyRows(rows) {
    let isEmpty
    rows.some((d, i) => {
      Object.keys(d).some(k => {
        if (!d[k]) {
          isEmpty = [i, k]
          return true
        }
      })
      if (isEmpty) {
        return true
      }
    })
    console.log("isEmpty : ", isEmpty)
    return isEmpty
  }

  editAlias(aliasIndex) {
    this.setState({ aliasIndex })
  }

  cancelEditAlias(i) {
    console.log(" cancel i : ", i)
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityAliases = [...prevState.entityAliases]
      const newItems = currentEntity && currentEntity.entityAliases &&
        currentEntity.entityAliases.length ?
        (entityAliases.length - currentEntity.entityAliases.length) : 0
      entityAliases[i] = currentEntity.entityAliases[(i - newItems)]
      return ({ entityAliases, aliasIndex: -1 })
    })
  }

  addAlias() {
    console.log("in addAlias")
    this.setState(prevState => {
      const error = { ...prevState.error }
      const entityAliases = [...prevState.entityAliases]
      if (entityAliases.length) {
        const emptyAliasIdx = this.checkEmptyArray(entityAliases)
        if(emptyAliasIdx > -1) {
          error.entityAliases = { ...error.entityAliases }
          error.entityAliases[emptyAliasIdx] = "Please provide a value here or reset before adding another Alias"
          return { error }
        }
      }
      error.entityAliases = ""
      entityAliases.unshift("")
      console.log("entityAliases : ", entityAliases)
      return { entityAliases, error }
    })
  }

  cancelAlias() {
    this.setState(prevState => {
      const { currentEntity } = prevState
      const entityAliases = [...(currentEntity.entityAliases || [])]
      const error = { ...prevState.error }
      error.entityAliases = ""
      return {
        entityAliases, modalType: "", modalIndex: -1,
        modalState: false, error, aliasIndex: -1
      }
    })
  }

  changeAlias(i, e) {
    const { value } = e.target
    this.setState(prevState => {
      const entityAliases = [...prevState.entityAliases]
      const error = { ...prevState.error }
      error.entityAliases = ""
      entityAliases[i] = value
      return { entityAliases, error }
    })
  }

  removeAlias(i) {
    this.setState(prevState => {
      const entityAliases = [...prevState.entityAliases]
      entityAliases.splice(i, 1)
      const error = { ...prevState.error }
      error.entityAliases = ""
      return {
        entityAliases, modalIndex: -1, modalType: "",
        modalState: false, error
      }
    })
  }

  arrayDiffs(oldArr=[], newArr=[]) {
    const removed = []
    const added = []
    oldArr.forEach(e => {
      if(e && !newArr.includes(e) && !removed.includes(e)) {
        removed.push(e)
      }
    })
    newArr.forEach(e => {
      if(e && !oldArr.includes(e) && !added.includes(e)) {
        added.push(e)
      }
    })
    return [removed, added]
  }

  saveBasicChangeLog(oldEntity, newEntity={}) {
    const { auth } = this.props
    const { entityId } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const primitiveFields = {
      "entityType": "Entity Type",
      "firmName": "Firm Name",
      "msrbRegistrantType": "MSRB Registrant Type",
      "msrbId": "MSRB Id",
      "taxId": "Tax Id",
      "businessStructure": "Business Structure",
      "numEmployees": "Number of Employees",
      "annualRevenue": "Annual Revenue",
      "primarySectors": "Primary Sector",
      "secondarySectors": "Secondary Sector"
    }
    const changeLog = []
    Object.keys(newEntity).forEach(k => {
      if(primitiveFields[k] && (oldEntity[k] !== newEntity[k])) {
        changeLog.push({
          userId,
          userName,
          log: oldEntity[k] ?
            `Changed ${primitiveFields[k]} from ${oldEntity[k]} to ${newEntity[k]}`
            : `Added ${newEntity[k]} as ${primitiveFields[k]}`,
          date
        })
      }
    })
    const oldFlags = (oldEntity.entityFlags && oldEntity.entityFlags.marketRole) || []
    const newFlags = (newEntity.entityFlags && newEntity.entityFlags.marketRole) || []
    const [removedFlags, addedFlags] = this.arrayDiffs(oldFlags, newFlags)
    if(removedFlags && removedFlags.length) {
      removedFlags.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Removed market role ${f}`,
          date
        })
      })
    }
    if(addedFlags && addedFlags.length) {
      addedFlags.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Added market role ${f}`,
          date
        })
      })
    }
    const oldAliases = (oldEntity.entityAliases) || []
    const newAliases = (newEntity.entityAliases) || []
    const [removedAliases, addedAliases] = this.arrayDiffs(oldAliases, newAliases)
    if(removedAliases && removedAliases.length) {
      removedAliases.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Removed alias ${f}`,
          date
        })
      })
    }
    if(addedAliases && addedAliases.length) {
      addedAliases.forEach(f => {
        changeLog.push({
          userId,
          userName,
          log: `Added alias ${f}`,
          date
        })
      })
    }
    if(changeLog.length) {
      updateAuditLog(entityId, changeLog)
    }
  }

  saveCallback(err, res) {
    if (!err) {
      const { _id } = res
      if (_id) {
        this.setState({ entityId: _id })
        // this.props.history.push(`/add-new-taks/${_id}`)
      }
      this.getEntityData()
      toast("Changes Saved", { autoClose: toastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      this.setState({ waiting: false })
      toast("Error in saving changes", { autoClose: toastTimeout, type: toast.TYPE.WARNING })
    }
  }

  checkNonEmptyErrorKey(error, keys) {
    if (!error) {
      return false
    }
    if (Object.getPrototypeOf(error).constructor.name === "Object") {
      keys = keys || Object.keys(error).filter(k => k !== "addresses")
      if (!keys.length) {
        return false
      }
      let nonEmptyError = false
      keys.some(k => {
        if (this.checkNonEmptyErrorKey(error[k])) {
          nonEmptyError = true
          return true
        }
      })
      return nonEmptyError
    }
    return true
  }

  checkAnyError(keys) {
    const { error } = this.state
    return this.checkNonEmptyErrorKey(error, keys)
  }

  saveBasicDetails = async () => {
    this.setState({ waiting: true, savemodalState: false  })
    const { entityId, entityType, entityName, marketRole, legalRole,
      taxId, entityAliases, currentEntity } = this.state
    const keys = ["entityType", "entityName", "marketRole", "taxId"]
    if (this.checkAnyError(keys)) {
      this.setState({ waiting: false })
      return
    }
    const requiredFields = currentEntity && currentEntity.relationshipType === "Third Party" || entityType === "Third Party" ? ["entityName"] : ["entityType", "entityName"]
    let emptyRequiredField = false
    requiredFields.forEach(d => {
      if (!this.state[d]) {
        emptyRequiredField = true
        this.setState(prevState => {
          const error = { ...prevState.error }
          error[d] = "Please provide a value"
          return { error, waiting: false }
        })
      }
    })
    if (emptyRequiredField) {
      this.setState({ waiting: false })
      return
    }
    const emptyAliasIdx = this.checkEmptyArray(entityAliases)
    console.log("emptyAliasIdx : ", emptyAliasIdx)
    if(emptyAliasIdx > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityAliases = { ...error.entityAliases }
        error.entityAliases[emptyAliasIdx] = "Please provide a value"
        return { error, waiting: false }
      })
      return
    }
    const dupResult = this.checkDupInArray(entityAliases)
    if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
      this.setState(prevState => {
        const error = { ...prevState.error }
        error.entityAliases = { ...error.entityAliases }
        error.entityAliases[dupResult[0]] = "Duplicate values are not allowed"
        error.entityAliases[dupResult[1]] = "Duplicate values are not allowed"
        return { error, waiting: false }
      })
      return
    }
    const firmName = entityName ? entityName.firmName || "" : ""
    const basicDetails = {
      entityType, firmName, entityAliases,
      msrbFirmName: firmName, taxId, ...defaultValues,
      entityFlags: { marketRole: await getUniqValueFromArray([...marketRole, ...legalRole]) }
    }
    this.saveBasicChangeLog(currentEntity, basicDetails)
    saveEntityDetails(entityId, basicDetails, "Third Party", this.saveCallback)
  }

  saveAddressChanges() {
    this.setState({ waiting: false })
    const { auth } = this.props
    const { entityId, addressList } = this.state
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    const userName = `${userFirstName} ${userLastName}`
    const date = new Date()
    const updateData = { addresses: addressList || [] }
    updateAuditLog(entityId, [{
      userId,
      userName,
      log: "Currently Address list updated",
      date
    }])
    saveEntityDetails(entityId, updateData, "", this.saveCallback)
  }

  renderBasicDetails() {
    const { expanded, entityId, legalRole, entityAliases, currentEntity, viewOnly,
      entityName, marketRole, marketRolesList, legalTypes, taxId, error, disabledMarketRole,
      modalState, modalType, modalIndex, aliasIndex, disabledLegalTypes } = this.state
    return (
      <article className={expanded.entityTypeSection ? "accordion is-active" : "accordion"}>
        <div className="accordion-header toggle">
          <p onClick={this.toggleExpand.bind(this, "entityTypeSection")}>
            Entity Type</p>
        </div>
        {expanded.entityTypeSection ?
          <div className={viewOnly ? "accordion-body view-only-mode" : "accordion-body"}>
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">Market Role</p>
                  <Multiselect
                    data={marketRolesList}
                    disabled={disabledMarketRole || []}
                    placeholder=" Select market role"
                    caseSensitive={false}
                    minLength={1}
                    value={marketRole}
                    filter="contains"
                    onChange={this.changeMarketRole}
                  />
                  {error && error.marketRole ?
                    <p className="has-text-danger">
                      {error.marketRole}
                    </p>
                    : undefined
                  }
                </div>
                {
                  marketRole.includes("Legal") ?
                    <div className="column">
                      <p className="multiExpLblBlk">Legal Type</p>
                      <Multiselect
                        data={legalTypes}
                        disabled={disabledLegalTypes || []}
                        placeholder=" Select legal types"
                        caseSensitive={false}
                        minLength={1}
                        value={legalRole}
                        filter="contains"
                        onChange={this.changeLegalRole}
                      />
                      {error && error.legalRole ?
                        <p className="has-text-danger">
                          {error.legalRole}
                        </p>
                        : undefined
                      }
                    </div>
                    : undefined
                }
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">
                    Entity Name
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                  </p>
                  <EntityLookup
                    entityName={entityName}
                    onChange={this.onChangeEntityName}
                    viewOnly={!!entityId}
                    participantType={legalRole ? "Bond Counsel" : ""}
                    entityType="Third Party"
                    isRequired={false}
                    isHide={entityName.firmName && !entityId}
                  />
                  {error && error.entityName ?
                    <p className="has-text-danger">
                      {error.entityName}
                    </p>
                    : undefined
                  }
                  {modalType === "alias" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Alias ${entityAliases[modalIndex]}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                            autoFocus onClick={this.removeAlias.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                  }
                  {currentEntity && currentEntity.entityAliases &&
                    currentEntity.entityAliases.length ?
                    <h6 className="title is-6">Aliases</h6>
                    : undefined
                  }
                  {
                    entityAliases.map((a, i) => {
                      if (!(currentEntity && currentEntity.entityAliases &&
                        currentEntity.entityAliases.length) ||
                        (i < (entityAliases.length - currentEntity.entityAliases.length))) {
                        return (
                          <div key={i} className="columns">
                            <div className="column is-11">
                              <div className="field is-grouped">
                                <input
                                  className="input is-small is-link"
                                  type="text"
                                  placeholder="Enter alias"
                                  custom={i}
                                  autoFocus
                                  value={a}
                                  onChange={this.changeAlias.bind(this, i)}
                                />
                                <button 
                                className="button is-small" 
                                aria-label={`Delete alias ${i+1}`}
                                onClick={this.toggleModal.bind(this, "alias", i)}>
                                <span className="has-text-danger">
                                  <i className="far fa-trash-alt" />
                                </span>
                              </button>
                              </div>
                              {error && error.entityAliases && error.entityAliases[i]
                                ? <p className="text-error has-text-danger">
                                  {error.entityAliases[i]}
                                </p>
                                : undefined
                              }
                            </div>
                          </div>
                        )
                      }
                      if (aliasIndex === i) {
                        return (
                          <div key={i} className="columns">
                            <div className="column is-11">
                              <div className="field is-grouped">
                                <input
                                  className="input is-small is-link"
                                  type="text"
                                  placeholder="alias"
                                  value={a}
                                  onChange={this.changeAlias.bind(this, i)}
                                />
                                <span className="has-text-danger">
                                  <i className="fa fa-times"
                                    onClick={this.cancelEditAlias.bind(this, i)}
                                  />
                                </span>
                              </div>
                              {error && error.entityAliases && error.entityAliases[i]
                                ? <p className="text-error has-text-danger">
                                  {error.entityAliases[i]}
                                </p>
                                : undefined
                              }
                            </div>
                          </div>
                        )
                      }
                      return (
                        <span className="level span-with-border-radius" key={i}>
                          {a}
                          { !viewOnly ?
                            <span className="has-text-danger is-pulled-right">
                              <i className="fa fa-pencil"
                                onClick={this.editAlias.bind(this, i)}
                              />
                            </span> : null
                          }
                        </span>
                      )
                    })
                  }
                  {!viewOnly ?
                    <div className="column">
                      <div className="level-right">
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                              onClick={this.addAlias}>Add Alias
                            </button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                              onClick={this.cancelAlias}>Cancel
                            </button>
                          </div>
                        </div>
                      </div>
                    </div> : null
                  }
                </div>
                <div className="column">
                  <p className="multiExpLblBlk">Tax Id</p>
                  <div>
                    <MaskedInput
                      mask={[/\d/, /\d/, /\d/, "-", /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                      className="input is-small is-link"
                      type="text"
                      name="taxId"
                      placeholder="Enter Tax ID"
                      value={taxId}
                      disabled={viewOnly || false}
                      onChange={this.changeField}
                    />
                    {error && error.taxId ?
                      <p className="has-text-danger">
                        {error.taxId}
                      </p>
                      : undefined
                    }
                  </div>
                </div>
              </div>
              {!viewOnly &&
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={()=>this.showModal()}>
                          Save Third Party
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          : undefined
        }
      </article>
    )
  }

  renderHistoricalData = () => {
    const {currentEntity} = this.state
    const data = currentEntity.historicalData || {}
    return <HistoricalData data={data}/>
  }

  renderAddress() {
    const { entityId, waiting, countryResult, addresses, error,
      viewOnly } = this.state

    if (!entityId) {
      return null
    }

    return (
      <FirmAddressListForm
        countryResult={countryResult}
        errorFirmDetail={error.addresses[0]}
        onChangeAddressType={this.onChangeAddressType}
        canEdit={!viewOnly}
        canView
        busy={waiting}
        addressList={addresses}
        onSaveAddress={this.onSaveAddress}
      />
    )
  }

  render() {
    const { waiting, generalError, currentEntity, entityId, savemodalState, notes, viewOnly } = this.state
    const { auth } = this.props
    const { userEntities: { userId, userFirstName, userLastName } } = auth
    if (generalError) {
      return (
        <div id="main">
          <strong className="has-text-danger">{generalError}</strong>
        </div>
      )
    }
    return (
      <div id="main">
        {waiting && <Loader />}
        <section className="accordions">
        <SaveModal
            closeModal={this.toggleSaveModal}
            savemodalState={this.state.savemodalState}
            message={this.state.modalMessage}
            onConfirmed={this.state.onConfirmed}
          />
          {this.renderBasicDetails()}
          {this.renderAddress()}
          {currentEntity && currentEntity.OnBoardingDataMigrationHistorical ? this.renderHistoricalData() : null}
          <Notes
            userId={userId}
            userFirstName={userFirstName}
            userLastName={userLastName}
            entityId={entityId}
            saveCallback={this.saveCallback}
            notes={notes}
            canEdit={viewOnly || false}
          />
        </section>
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default connect(mapStateToProps)(ThirdPartyNew)
