import React, {Component} from "react"
import SingleAccordion from "./SingleAccordion"

class AccordionsContainer extends Component {

  static getDerivedStateFromProps(props) {
    console.log("----GETDERIVEDSTATEFROMPROPS", props)
    const accordions = props
      .accordionMeta
      .map((r, i) => ({
        accIndex: i,
        accOpen: r.open,
        accHeader: r.title,
        AccordionRenderComponent: r.AccordionRenderComponent,
        AccordionButtons: r.AccordionButtons || (() => null)
      }))
    return {accordions}
  }

  constructor(props) {
    super(props)
    this.state = {
      accordions: []
    }
    this.toggleAccordion = this
      .toggleAccordion
      .bind(this)
  }

  componentDidMount() {
    console.log("-----PROPS", this.props)
    const {accordionMeta} = this.props
    const accordionInitate = accordionMeta.map((r, i) => ({
      accIndex: i,
      accOpen: r.open,
      accHeader: r.title,
      AccordionRenderComponent: r.AccordionRenderComponent,
      AccordionButtons: r.AccordionButtons || (() => null)
    }))
    this.setState({accordions: accordionInitate})
  }

  toggleAccordion = (aIndex) => {
    const {multiple} = this.props
    const {accordions} = this.state
    if (multiple) {
      const newMultipleState = accordions.map(({
        accIndex,
        accOpen,
        ...rest
      }) => accIndex === aIndex
        ? {
          accIndex,
          accOpen: !accOpen,
          ...rest
        }
        : {
          accIndex,
          accOpen,
          ...rest
        })
      this.setState({accordions: newMultipleState})
    } else {
      const newSingleState = accordions.map(({
        accIndex,
        accOpen,
        ...rest
      }) => accIndex === aIndex
        ? {
          accIndex,
          accOpen: !accOpen,
          ...rest
        }
        : {
          accIndex,
          accOpen: false,
          ...rest
        })
      this.setState({accordions: newSingleState})
    }
  }

  render() {
    const {accordions} = this.state
    return (accordions && <div className="container dealSoE">
      <section className="accordions box dealSoE">
        {accordions.map(({
          AccordionRenderComponent,
          ...restProps
        }) => <SingleAccordion
          key={restProps.accIndex}
          {...restProps}
          toggleAccordion={this.toggleAccordion}>
          {AccordionRenderComponent}
        </SingleAccordion>)
}
      </section>
    </div>)
  }
}

export default AccordionsContainer