import React from "react"
import dateFormat from "dateformat"
import isEmpty from "lodash/isEmpty"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import {connect} from "react-redux"
import CONST from "GlobalUtils/consts"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import Accordion from "../../../GlobalComponents/Accordion"
import DropDownListClear from "../../../GlobalComponents/DropDownListClear"
import RatingSection from "../../../GlobalComponents/RatingSection"
import {
  NumberInput,
  SelectLabelInput, TextLabelInput
} from "../../../GlobalComponents/TextViewBox"
import {
  fetchBillingManageExpense,
  managebillingexpensesstate,
  putBillingManageExpense
} from "../../../StateManagement/actions"
import Loader from "../../../GlobalComponents/Loader"
import { fetchAssigned } from "../../../StateManagement/actions/CreateTransaction"
import TableHeader from "../../../GlobalComponents/TableHeader"
import { TimeTrackingValidate } from "../Validation/BillingInvoiceValidate"

const cols = [
  [
    { name: "From Date ", required: true },
    { name: "To Date", required: true },
    { name: "Hours", required: true },
    { name: "Delete" }
  ],
  [
    { name: "User Name" },
    { name: "Role Type" },
    { name: "Hours" },
    { name: "Rate" },
    { name: "Total" },
    { name: "Action" }
  ]
]
const trackerDetails = {
  fromDate: null,
  toDate: null,
  hours: 0
}

const getPartKey = (type, subType) => {
  if(subType === "Bond Issue") return "dealPartContactId"
  if(type === "RFP") return "rfpParticipantContactId"
  return "partContactId"
}

class TimeTracking extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timeTracker: this.initialState().timeTracker,
      nonTranFees: [],
      selectedEntity: {},
      selectedTran: {},
      selectedUser: {},
      errorMessages: {},
      dropDown: {
        roleType: [],
        issuerList: [],
        users: [],
        transactions: [],
        expenseContextTypes: [],
        expenseTypes: []
      },
      trackerId: "",
      roleType: "",
      totalAmount: 0,
      totalHour: 0,
      stdHourlyRate: null,
      disableTracker: false,
      loading: true
    }
  }
  initialState = () => ({
    timeTracker: [cloneDeep(trackerDetails)]
  });
  async componentDidMount() {
    const { selectedInvoice, tenantId } = this.props
    const picResult = await getPicklistByPicklistName(["LKUPROLETYPES"])
    const result = (picResult.length && picResult[1]) || {}
    let stateObj = {
      selectedClient: {},
      selectedActivity: {},
      selectedInvoice: {},
      selectedUser: {}
    }
    if (selectedInvoice) {
      stateObj = selectedInvoice
    }
    await managebillingexpensesstate(
      { stateObj },
      async ({ selectedData, dropDownValues }) => {
        const activityDetails = dropDownValues.activityDetails.find(f => selectedInvoice && selectedInvoice.selectedActivity && f.activityId === selectedInvoice.selectedActivity.activityId) || {}
        if (selectedInvoice) {
          await fetchAssigned(
            selectedInvoice.selectedActivity &&
              selectedInvoice.selectedActivity.activityTranClientId,
            users => {
              this.setState(prevState => ({
                dropDown: {
                  ...prevState.dropDown,
                  issuerList: dropDownValues.clientDetails,
                  transactions: dropDownValues.activityDetails,
                  users: (activityDetails && activityDetails.eligibleUserDetails) || [],
                  roleType: result.LKUPROLETYPES || []
                },
                selectedEntity: selectedInvoice.selectedClient || {},
                selectedTran: selectedInvoice.selectedActivity,
                selectedUser: {},
                loading: false
              }),()=>{ this.getTracker() })
            }
          )
        } else {
          this.setState(prevState => ({
            dropDown: {
              ...prevState.dropDown,
              issuerList: dropDownValues.clientDetails,
              transactions: dropDownValues.activityDetails,
              roleType: result.LKUPROLETYPES || []
            },
            loading: false
          }))
        }
      },
      this.props.token
    )
  }

  onChange = (name, value) => {
    const { tenantId } = this.props
    if (name === "selectedEntity") {
      this.setState(
        {
          loading: true
        },
        async () => {
          await managebillingexpensesstate(
            {
              stateObj: {
                selectedClient: value,
                selectedActivity: {},
                selectedInvoice: {},
                selectedUser: {}
              }
            },
            async ({ selectedData, dropDownValues }) => {
              // await fetchAssigned(value.activityTranClientId, (users)=> {
              this.setState(prevState => ({
                dropDown: {
                  ...prevState.dropDown,
                  transactions: dropDownValues.activityDetails
                  // users: users.usersList,
                },
                selectedEntity: value || {},
                selectedTran: {},
                selectedUser: {},
                loading: false
              }))
              // })
            },
            this.props.token
          )
        }
      )
    } else {
      this.setState(
        {
          [name]: value,
          selectedUser: {},
          dropDown: {
            ...this.state.dropDown,
            users: (value && value.eligibleUserDetails) || []
          }
        },
        () => this.getTracker()
      )
    }
  }

  getTracker = async () => {
    const {
      selectedEntity,
      selectedTran,
    } = this.state
    if (
      Object.keys(selectedEntity).length &&
      Object.keys(selectedTran).length
    ) {
      const query = `/${selectedEntity.activityTranClientId}?activityId=${
        selectedTran.activityId
      }`
      const res = await fetchBillingManageExpense(query)
      const disableValue = ["Invoice Paid", "Invoice Sent to Client", "Sent for review"]
      const disableTracker = disableValue.indexOf(res && res.invoiceStatus) > -1
      this.setState(
        {
          tracker: res && res.userExpenses,
          amount: res && res.userTimeTrackerCount,
          nonTranFees: (res && res.nonTranFees) || (selectedTran && selectedTran.activityContract && selectedTran.activityContract.nonTranFees) || [],
          errorMessages: {},
          disableTracker
        },
        () => this.totalCount()
      )
    }
  }

  onSaveTrackers = () => {
    const {
      selectedEntity,
      selectedTran,
      selectedUser,
      timeTracker,
      trackerId,
      roleType,
      stdHourlyRate
    } = this.state
    let {nonTranFees} = this.state
    let payload = {}
    if (trackerId) {
      payload = {
        roleType,
        timeTracker
      }
      payload._id = trackerId
    } else {
      if(selectedTran && selectedTran.activityContract && selectedTran.activityContract.nonTranFees && selectedTran.activityContract.nonTranFees.length && stdHourlyRate){
        const roleIndex = selectedTran.activityContract.nonTranFees.findIndex(n => n.role === roleType)
        if(roleIndex > -1){
          selectedTran.activityContract.nonTranFees[roleIndex].stdHourlyRate = stdHourlyRate || selectedTran.activityContract.nonTranFees[roleIndex].stdHourlyRate || 0
        } else {
          const tranRate = {
            role: roleType || "",
            stdHourlyRate,
            quoatedRate: null,
          }
          selectedTran.activityContract.nonTranFees.push(tranRate)
        }
        this.setState({
          selectedTran
        })
      }
      payload = {
        ...selectedEntity,
        ...selectedTran,
        roleType,
        userId: selectedUser.userId,
        userFirstName: selectedUser.userFirstName || "",
        // userLastName: selectedUser.userLastName,
        timeTracker
      }
    }

    const errors = TimeTrackingValidate(payload, "timeTracker")
    if (errors && errors.error) {
      const errorMessages = {}
      console.log("=======>", errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) { //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) { //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] = {
            [err.path[1]]: {
              [err.path[2]]: err.message
            }
          }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: err.message
          }
        }
      })
      toast((errorMessages.timeTracker && errorMessages.timeTracker.undefined),{
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      return this.setState({ errorMessages, activeItem: [0, 1] })
    }

    this.setState(
      {
        loading: true
      },
      async () => {
        const fees = {
          role: payload.roleType,
          stdHourlyRate,
          quoatedRate: null
        }
        if(nonTranFees){
          const isExists = nonTranFees.findIndex(fee => fee.role === payload.roleType)
          if(isExists !== -1){
            nonTranFees[isExists].stdHourlyRate = stdHourlyRate
          }else {
            nonTranFees.push(fees)
          }
        }else {
          nonTranFees = [fees]
        }
        const res = await putBillingManageExpense({ ...payload, nonTranFees})
        if (res && res.status === 200) {
          toast("Time Tracker updated successfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS
          })
          const query = `/${selectedEntity.activityTranClientId}?activityId=${
            selectedTran.activityId
          }`
          const response = await fetchBillingManageExpense(query)
          this.setState({
            tracker: response && response.userExpenses,
            amount: response && response.userTimeTrackerCount,
            loading: false,
            // unset tracker
            timeTracker: [cloneDeep(trackerDetails)],
            nonTranFees: (response && response.nonTranFees) || (selectedTran && selectedTran.activityContract && selectedTran.activityContract.nonTranFees) || [],
            roleType: "",
            stdHourlyRate: null,
            trackerId: "",
            selectedUser: {},
            // timeTracker: (res.data && res.data.timeTracker) || [cloneDeep(trackerDetails)],
            // trackerId: (res.data && res.data._id) || "",
            errorMessages: {}
          })
          this.totalCount()
        } else {
          toast("Something went wrong, please try a latter.", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
          this.setState({
            loading: false
          })
        }
      }
    )
  }

  onHandleChange = (e, i, field) => {
    const { timeTracker } = this.state
    if (field === "toDate" || field === "fromDate") {
      timeTracker[i][field] = e
    } else {
      timeTracker[i][e.target.name] = e.target.value
    }
    this.setState({ timeTracker })
  }

  addTracker = () => {
    const { timeTracker } = this.state
    timeTracker.unshift(cloneDeep(trackerDetails))
    this.setState({ timeTracker, errorMessages: {} })
  }
  resetTracker = (e) => {
    e.stopPropagation()
    this.setState({
      timeTracker: this.initialState().timeTracker,
      errorMessages: {}
    },() => this.onEdit(this.state.selectedUser))
  }

  removeTracker = index => {
    const { timeTracker } = this.state
    timeTracker.splice(index, 1)
    this.setState({ timeTracker, errorMessages: {} })
  }

  handleOnChange = event => {
    const {nonTranFees, selectedTran} = this.state
    const {name, value} = event.target
    this.setState({
      [event.target.name]: event.target.value,
    },() => {
      if(name === "roleType" && value){
        const fees = nonTranFees.find(fee => value === fee.role)
        this.setState({
          stdHourlyRate: (fees && fees.stdHourlyRate) || null,
        })
      }
    })
  }

  onEdit = user => {
    const {nonTranFees} = this.state
    const tracker = cloneDeep(this.state.tracker)
    // const userName = `${user.userFirstName} ${user.userLastName}` || ""
    const fees = nonTranFees.find(fee => tracker[user.userId] && tracker[user.userId].roleType === fee.role)
    if (user && user.userId && tracker[user.userId]) {
      this.setState(
        {
          timeTracker:
            tracker[user.userId] && tracker[user.userId].timeTracker.length === 0
              ? [cloneDeep(trackerDetails)]
              : tracker[user.userId] && tracker[user.userId].timeTracker,
          trackerId: (tracker[user.userId] && tracker[user.userId]._id) || "",
          roleType: (tracker[user.userId] && tracker[user.userId].roleType) || "",
          stdHourlyRate: (fees && fees.stdHourlyRate) || null,
          selectedUser: user,
          errorMessages: {}
        },
        () => this.totalCount()
      )
    } else {
      this.setState(
        {
          timeTracker: [cloneDeep(trackerDetails)],
          roleType: "",
          trackerId: "",
          stdHourlyRate: (fees && fees.stdHourlyRate) || null,
          selectedUser: user,
          errorMessages: {}
        },
        () => this.totalCount()
      )
    }
  }

  totalCount = () => {
    const { dropDown, amount } = this.state
    let total = 0
    let hour = 0
    cloneDeep(dropDown.users).forEach(user => {
      // const userName = `${user.userFirstName} ${user.userLastName}` || ""
      total += (amount && amount[user.userId] && amount[user.userId].totalCost && amount[user.userId].totalCost  && parseFloat(amount[user.userId].totalCost)) || 0
      hour += parseInt((amount && amount[user.userId] && amount[user.userId].totalHours) || 0,10)
    })
    this.setState({
      totalAmount: total && total.toFixed(2) || 0,
      totalHour: hour
    })
  }

  render() {
    const {
      loading,
      dropDown,
      selectedEntity,
      selectedTran,
      selectedUser,
      timeTracker,
      errorMessages,
      roleType,
      stdHourlyRate,
      amount,
      totalHour,
      disableTracker,
      totalAmount
    } = this.state

    if (loading) {
      return <Loader />
    }
    return (
      <div>
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Time Tracking"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <div className="columns">
                      <div className="column">
                        <p className="multiExpLbl">Select Issuer</p>
                        <div className="control">
                          <DropDownListClear
                            filter
                            data={dropDown.issuerList || []}
                            defaultValue=""
                            value={selectedEntity.activityClientName || ""}
                            valueField={a => a}
                            textField="activityClientName"
                            isHideButton={selectedEntity.activityClientName}
                            onChange={val =>
                              this.onChange("selectedEntity", val)
                            }
                            onClear={() => {
                              this.onChange("selectedEntity", {})
                            }}
                          />
                        </div>
                      </div>
                      <div className="column">
                        <p className="multiExpLbl">Transaction / Issue Name</p>
                        <div className="control">
                          <DropDownListClear
                            filter
                            data={dropDown.transactions || []}
                            value={selectedTran.activityDescription || ""}
                            disabled={isEmpty(dropDown.transactions || [])}
                            valueField={a => a}
                            textField="activityDescription"
                            groupBy={({
                              activityTranType,
                              activityTranSubType
                            }) =>
                              `${activityTranType} - ${activityTranSubType}`
                            }
                            isHideButton={selectedTran.activityDescription}
                            onChange={val => this.onChange("selectedTran", val)}
                            onClear={() => {this.onChange("selectedTran", {})}}
                          />
                        </div>
                      </div>
                    </div>
                    <div>
                      {selectedTran && selectedTran.activityDescription ? (
                        <div className="tbl-scroll">
                          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                            <TableHeader className="th-has-mandatory-astrisk" cols={cols[1]} />
                            <tbody>
                              {dropDown.users.map((user, index) => {
                                // const userName = `${user.userFirstName} ${user.userLastName}` || ""
                                return (
                                  <tr key={index.toString()}>
                                    <td>{`${user.userFirstName} ${user.userLastName}` || "--"}</td>
                                    <td>
                                      {(amount &&
                                        amount[user.userId] &&
                                        amount[user.userId].roleType) ||
                                        "--"}
                                    </td>
                                    <td>
                                      {(amount &&
                                        amount[user.userId] &&
                                        amount[user.userId].totalHours) ? Number(amount[user.userId].totalHours).toLocaleString() :
                                        "0"}
                                    </td>
                                    <td>
                                      {amount &&
                                      amount[user.userId] &&
                                      amount[user.userId].rate
                                        ? `$${Number(amount[user.userId].rate).toLocaleString()}`
                                        : "-"}
                                    </td>
                                    <td>
                                      {amount &&
                                      amount[user.userId] &&
                                      amount[user.userId].totalCost
                                        ? `$${Number(amount[user.userId].totalCost).toLocaleString()}`
                                        : "$0"}
                                    </td>
                                    <td>
                                      {!disableTracker ?
                                        <span className="has-text-link">
                                          <a
                                            className="fas fa-pencil-alt"
                                            onClick={() => this.onEdit(user)}
                                            title="Edit Tracker"
                                          />
                                        </span> :
                                        <span className="has-text-link">
                                          <a
                                            className="fas fa-eye"
                                            onClick={() => this.onEdit(user)}
                                            title="View Tracker"
                                          />
                                        </span>
                                      }
                                    </td>
                                  </tr>
                                )
                              })}
                              <tr>
                                <td>Total</td>
                                <td>-</td>
                                <td>{totalHour ? Number(totalHour).toLocaleString() : "-"}</td>
                                <td>-</td>
                                <td>{totalAmount ? `$${Number(totalAmount).toLocaleString()}` : "$0"}</td>
                                <td />
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      ) : null}
                      <hr />
                      {selectedUser && selectedUser.userFirstName ? (
                        <div>
                          <div className="columns">
                            <div className="field is-grouped is-pulled-left">
                              <div className="column roltype">
                                <p className="multiExpLbl">Role Type<span className="has-text-danger">*</span></p>
                                <div className="control">
                                  <div className="Select Firm User select-hack">
                                    <SelectLabelInput
                                      name="roleType"
                                      error={(errorMessages && errorMessages.roleType && "Required") || ""}
                                      list={dropDown.roleType}
                                      value={roleType || ""}
                                      onChange={this.handleOnChange}
                                      disabled={disableTracker}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="column roltype">
                                <p className="multiExpLbl">Rate</p>
                                <div className="control">
                                  <NumberInput
                                    name="stdHourlyRate"
                                    prefix="$"
                                    value={stdHourlyRate || 0}
                                    decimalScale={4}
                                    onChange={this.handleOnChange}
                                    disabled={disableTracker}
                                  />
                                  <small className="text-error has-text-danger">
                                    {(errorMessages && errorMessages.stdHourlyRate) || ""}
                                  </small>
                                </div>
                              </div>
                            </div>
                            <div className="column">
                              <p className="multiExpLbl">User Name</p>
                              <div className="control">
                                <div className="Select Firm User select-hack">
                                  {`${selectedUser && selectedUser.userFirstName} ${selectedUser && selectedUser.userLastName}` || ""}
                                </div>
                              </div>
                            </div>
                            <div className="field is-grouped is-pulled-right">
                              { !disableTracker ?
                                <div className="column rolebtn">
                                  <p className="multiExpLbl"/>
                                  <div className="control">
                                    <button onClick={this.addTracker} className="button is-link is-small">Add</button>
                                    <button onClick={this.resetTracker} className="button is-light is-small">Reset
                                    </button>
                                  </div>
                                </div> : null
                              }
                            </div>
                          </div>
                          <div className="tbl-scroll">
                          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                            <TableHeader className="th-has-mandatory-astrisk" cols={cols[0]} />
                            <tbody>
                              {timeTracker && timeTracker.length
                                ? timeTracker.map((tracker, i) => {
                                  const error = (errorMessages && errorMessages.timeTracker && errorMessages.timeTracker[i.toString()]) || {}
                                  return (
                                    <tr key={i}>
                                      <td className="multiExpTblVal">
                                        {/* <input id="datepickerDemo"
                                          className="input is-small is-link"
                                          type="date"
                                          name="fromDate"
                                          value={tracker.fromDate === "" || !tracker.fromDate ? "" : dateFormat(new Date(tracker.fromDate),"yyyy-mm-dd")}
                                          onChange={e => this.onHandleChange(new Date(e.target.value), i,"fromDate")}
                                          disabled={disableTracker}
                                        /> */}

                                        <TextLabelInput
                                          type="date"
                                          name="fromDate"
                                          value={(tracker.fromDate === "" || !tracker.fromDate) ? null : new Date(tracker.fromDate)}
                                          disabled={disableTracker}
                                          onChange={e => this.onHandleChange(new Date(e.target.value), i,"fromDate")}
                                        />
                                        <small className="text-error has-text-danger">
                                          {error.fromDate || ""}
                                        </small>
                                      </td>
                                      <td className="multiExpTblVal">
                                        {/* <input id="datepickerDemo"
                                          className="input is-small is-link"
                                          type="date"
                                          name="toDate"
                                          value={tracker.toDate === "" || !tracker.toDate ? "" : dateFormat(new Date(tracker.toDate),"yyyy-mm-dd")}
                                          onChange={e => this.onHandleChange(new Date(e.target.value),i,"toDate")}
                                          disabled={disableTracker}
                                        /> */}

                                        <TextLabelInput
                                          type="date"
                                          name="toDate"
                                          value={(tracker.toDate === "" || !tracker.toDate) ? null : new Date(tracker.toDate)}
                                          disabled={disableTracker}
                                          onChange={e => this.onHandleChange(new Date(e.target.value),i,"toDate")}
                                        />

                                        <small className="text-error has-text-danger">
                                          {error.toDate || ""}
                                        </small>
                                      </td>
                                      <td className="multiExpTblVal">
                                        <NumberInput
                                          name="hours"
                                          value={parseInt(tracker.hours || 0, 10)}
                                          onChange={e => this.onHandleChange(e, i)}
                                          disabled={disableTracker}
                                        />
                                        <small className="text-error has-text-danger">
                                          {error.hours || ""}
                                        </small>
                                      </td>
                                      <td className="multiExpTblVal">
                                        { !disableTracker ?
                                          <div className="field is-grouped">
                                            <div
                                              className="control"
                                              onClick={() =>
                                                this.removeTracker(i)
                                              }
                                            >
                                              <span className="has-text-link">
                                                <i className="far fa-trash-alt"/>
                                              </span>
                                            </div>
                                          </div> : null
                                        }
                                      </td>
                                    </tr>
                                  )
                                })
                                : null}
                            </tbody>
                          </table>
                          </div>
                          { !disableTracker ?
                            <div className="level">
                              <div className="level-left"/>
                              <div className="level-right">
                                <button
                                  className="button is-link is-small"
                                  onClick={this.onSaveTrackers}
                                  disabled={disableTracker}
                                >
                                  Save Details
                                </button>
                              </div>
                            </div> : null
                          }
                        </div>
                      ) : null}
                    </div>
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  tenantId: (auth && auth.tenantId) || "",
  userInfo: auth.userEntities,
  user: (auth && auth.userEntities) || {},
  token: auth.token,
})

export default connect(mapStateToProps, null)(TimeTracking)
