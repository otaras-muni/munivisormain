import React from "react"
import * as qs from "query-string"
import Loader from "../../../GlobalComponents/Loader"
import BillingExpenseReceipts from "./BillingExpenseReceiptsNew"
import FAServiceFee from "./FAServiceFee"
import ExpenseDetails from "./ExpenseDetails"
import TimeTracking from "./TimeTracking"
import {fetchBillingByInvoiceId} from "../../../StateManagement/actions/Billing"


class BillingExpenseRevenueMain extends React.Component {
  static defaultProps = {
    optionselected: "billing-exp-detail"
  }

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      SUBTABS: [
        {path: "billing-exp-detail", label: "Expense Detail",component:ExpenseDetails},
        {path: "billing-time-tracker", label: "Time Tracker",component:TimeTracking},
        // {path: "billing-exp-receipts", label: "Receipts",component:BillingExpenseReceipts},
        // {path: "billing-exp-faserv", label: "Contract",component:FAServiceFee},
        // {path: "billing-exp-audit", label: "Billing Audit",component:BillingAuditTrail},
      ],
      selectedData: null
    }
    this.identifyComponent = this.identifyComponent.bind(this)
  }

  async componentDidMount() {
    const queryString = qs.parse(this.props.history.location.search)
    const {SUBTABS} = this.state
    const {optionselected} = this.props
    let selectedData = null
    if(queryString && queryString.invoice){
      const billing = await fetchBillingByInvoiceId(queryString.invoice)
      if(billing && billing._id){
        selectedData = {
          selectedClient: {
            activityTranClientId: billing.activityTranClientId || "",
            activityClientName: billing.activityClientName || "",
          },
          selectedActivity:{
            activityId: billing.activityId || "",
            activityTranClientId: billing.activityTranClientId || "",
            activityClientName: billing.activityClientName || "",
            activityTranFirmId: billing.activityTranFirmId || "",
            activityTranFirmName: billing.activityTranFirmName || "",
            activityDescription: billing.activityDescription || "",
            activityTranType: billing.activityTranType || "",
            activityTranSubType: billing.activityTranSubType || ""
          },
          selectedInvoice:{
            invoiceId: billing._id || "",
            invoiceNumber: billing.invoiceNumber || ""
          },
          selectedUser:{}
        }
      }
    }
    this.setState({
      loading: false,
      SUBTABS,
      selection:optionselected,
      selectedData
    })
  }

  identifyComponent(selection){
    console.log("IDENTIFY COMPONENT")
    this.setState({selection})
  }

  renderTabs = (tabs,option) => tabs.map(t =>
    <li key={t.path} className={option === t.path ? "is-active" : "inactive-tab"} onClick={() => this.identifyComponent(t.path)}>
      <a className="tabSecLevel">{t.label}</a>
    </li>
  )

  renderViewSelection = (option) => (
    <ul>
      {this.renderTabs(this.state.SUBTABS,option)}
    </ul>
  )

  renderSelectedViewFromState =(sel) => {
    const { SUBTABS, selectedData } = this.state
    const ret = SUBTABS.filter( t => t.path === sel )
    const {component:Component} = ret[0]
    const newProps = {...this.props,componentDisplayClass:this.state.componentDisplayClass}
    return <Component selectedInvoice={selectedData} {...newProps} />
  }

  render() {
    // const {nav3} = this.props
    const {selection} = this.state
    const {optionselected} = this.props
    const initializeSelection = selection || optionselected

    const loading = () => <Loader/>

    if (this.state.loading) {
      return loading()
    }

    return (
      <div id="main">
        <div className="tabs">
          {this.renderViewSelection(initializeSelection)}
        </div>
        {initializeSelection ? this.renderSelectedViewFromState(initializeSelection) : null}
      </div>
    )
  }
}

export default BillingExpenseRevenueMain
