import React, { Component } from "react"
import { connect } from "react-redux"
import { DropdownList } from "react-widgets"
import uuidv1 from "uuid/v1"
import moment from "moment"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import isEmpty from "lodash/isEmpty"
import CONST, { muniApiBaseURL } from "GlobalUtils/consts"
import { getDocDetails, getPicklistByPicklistName, getFile } from "GlobalUtils/helpers"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Accordion from "../../../GlobalComponents/Accordion"
import ScheduleTransactionAndRetainer from "../../../GlobalComponents/ScheduleTransactionAndRetainer"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import { managebillingexpensesstate } from "../../../StateManagement/actions"
import {
  getfaservicefee,
  putfaservicefee
} from "../../../StateManagement/actions/Billing/Billing"
import Loader from "../../../GlobalComponents/Loader"
import TableHeader from "../../../GlobalComponents/TableHeader"
const generateName = uuidv1()
  .replace(/-/g, "")
  .toUpperCase()

const cols = [
  { name: "Contract ID" },
  { name: "Contract Name" },
  { name: "Contract Type" },
  { name: "Start Date" },
  { name: "End Date" }
]

class FAServiceFee extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
      sof: [cloneDeep(CONST.FAServiceFee.sof)],
      nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      tempSof: [cloneDeep(CONST.FAServiceFee.sof)],
      tempNonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      tempRetAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      isAttachWithInvoice: false,
      issuer: {},
      issueName: {},
      documents: [],
      digitize: false,
      contractObjectId: "",
      dropDown: {
        sofDesc: [],
        sofDealType: [],
        contractType: [],
        securityType: [],
        role: [],
        type: [],
        issuers: [],
        transactions: []
      },
      invoiceNumber: "",
      createdAt: "",
      objectName: "",
      fileName: "",
      notes: "",
      errorMessages: {},
      contracts: [],
      loading: true
    }
  }

  async componentDidMount() {
    const picResult = await getPicklistByPicklistName([
      "LKUPROLETYPES",
      "LKUPDESCRIPTIONTYPES",
      "LKUPDEALTYPES",
      "LKUPCONTRACTTYPES",
      "LKUPSECURITYTYPES",
      "LKUPTYPETYPES"
    ])
    const result = (picResult.length && picResult[1]) || {}
    const { selectedInvoice } = this.props
    if (selectedInvoice) {
      this.setState(
        prevState => ({
          dropDown: {
            ...prevState.dropDown,
            role: result.LKUPROLETYPES || [],
            sofDesc: result.LKUPDESCRIPTIONTYPES || [],
            sofDealType: result.LKUPDEALTYPES || [],
            contractType: result.LKUPCONTRACTTYPES || [],
            securityType: result.LKUPSECURITYTYPES || [],
            type: result.LKUPTYPETYPES || []
          }
        }),
        () => this.onChange(selectedInvoice.selectedActivity || {})
      )
    } else {
      await managebillingexpensesstate(
        {
          stateObj: {
            selectedClient: {},
            selectedActivity: {},
            selectedInvoice: {},
            selectedUser: {}
          }
        },
        ({ selectedData, dropDownValues }) => {
          this.setState(prevState => ({
            dropDown: {
              ...prevState.dropDown,
              role: result.LKUPROLETYPES || [],
              sofDesc: result.LKUPDESCRIPTIONTYPES || [],
              sofDealType: result.LKUPDEALTYPES || [],
              contractType: result.LKUPCONTRACTTYPES || [],
              securityType: result.LKUPSECURITYTYPES || [],
              type: result.LKUPTYPETYPES || [],
              issuers: dropDownValues.clientDetails,
              transactions: dropDownValues.activityDetails
            },
            selectedData,
            loading: false
          }))
        },
        this.props.auth.token
      )
    }
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({
      userName,
      log: `FA service fee ${change}`,
      date: new Date(),
      key: category
    })
  }

  onCancel = () => {
    this.setState({
      contractObjectId: "",
      contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
      sof: [cloneDeep(CONST.FAServiceFee.sof)],
      nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      documents: [],
      notes: ""
    })
  }

  getContrractDocLink = async docId => {
    const res = await getDocDetails(docId)
    console.log("res : ", res)
    const [error, doc] = res
    if (error) {
      toast(error.message, {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    } else {
      const { contextId, contextType, tenantId, name } = doc
      if (tenantId && contextId && contextType && name) {
        const a = document.getElementById("link")
        this.setState(
          {
            objectName: `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`,
            fileName: doc.originalName
          },
          () => a.click()
        )
      } else {
        toast("invalid doc details", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    }
  }

  onSave = async (childPayload, callBack) => {
    const { issuer, invoiceNumber, contractRef } = this.state
    const {
      activityClientName,
      activityDescription,
      activityTranFirmId,
      activityTranFirmName,
      activityTranSubType,
      activityTranType
    } = issuer
    childPayload.invoiceNumber =
      invoiceNumber || `INVOICE-${generateName.substring(0, 17)}`
    // childPayload.contractRef = contractRef
    if (!invoiceNumber) {
      childPayload = {
        ...childPayload,
        activityClientName,
        activityDescription,
        activityTranFirmId,
        activityTranFirmName,
        activityTranSubType,
        activityTranType
      }
    }

    const res = await putfaservicefee(
      issuer.activityTranClientId,
      issuer.activityId,
      childPayload
    )

    if (res && res.status === 200 && res.data && res.data.data) {
      toast("Contract updated successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.setState(
        {
          documents: res.data.data.documents || [],
          digitize: res.data.data.digitize || false,
          isAttachWithInvoice: true,
          contractObjectId: res.data.data._id || "",
          contractRef: res.data.data.contractRef,
          nonTranFees: res.data.data.nonTranFees,
          retAndEsc: res.data.data.retAndEsc,
          sof: res.data.data.sof,
          notes: res.data.data.notes || "",
          invoiceNumber: res.data.data.invoiceNumber,
          createdAt: res.data.data.createdAt
        },
        () => {
          const sof = res.data.data.sof.map(sofs => ({
            ...sofs,
            fees:
              sofs.feesType === "slab"
                ? sofs.fees
                : cloneDeep(CONST.FAServiceFee.sof.fees),
            feePar:
              sofs.feesType === "feePar"
                ? sofs.fees
                : cloneDeep(CONST.FAServiceFee.sof.feePar)
          }))
          callBack({
            status: true,
            state: {
              contractRef: res.data.data.contractRef,
              nonTranFees: res.data.data.nonTranFees,
              retAndEsc: res.data.data.retAndEsc,
              sof,
              notes: res.data.data.notes || "",
              documents: res.data.data.documents || [],
              digitize: res.data.data.digitize || false,
              contractObjectId: res.data.data._id || ""
            }
          })
        }
      )
    } else {
      toast("Something went wrong", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  onChange = async value => {
    this.setState(
      {
        loading: true
      },
      async () => {
        await managebillingexpensesstate(
          {
            stateObj: {
              selectedClient: value,
              selectedActivity: {},
              selectedInvoice: {},
              selectedUser: {}
            }
          },
          ({ selectedData, dropDownValues }) => {
            value = value.activityId
              ? dropDownValues.activityDetails.find(
                activity => activity.activityId === value.activityId
              )
              : value
            this.setState(
              prevState => ({
                dropDown: {
                  ...prevState.dropDown,
                  issuers: dropDownValues.clientDetails,
                  transactions: dropDownValues.activityDetails
                },
                invoiceNumber: "",
                contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
                issuer: value || {},
                issueName:
                  value && value.activityId
                    ? value
                    : selectedData.selectedClient || {},
                contracts:
                  (dropDownValues.contracts &&
                    dropDownValues.contracts.contracts) ||
                  [],
                loading: false
              }),
              () => this.onCancel()
            )
          },
          this.props.auth.token
        )
        if (value && value.activityId) {
          const res = await getfaservicefee(
            value.activityTranClientId,
            value.activityId
          )
          if (
            res &&
            res.data &&
            res.data.documents &&
            res.data.documents.length
          ) {
            this.setState(prevState => ({
              documents: res.data.documents || [],
              isAttachWithInvoice: true,
              digitize: res.data.digitize || false,
              contractObjectId: res.data._id || "",
              invoiceNumber: res.data.invoiceNumber,
              nonTranFees: res.data.nonTranFees || [
                cloneDeep(CONST.FAServiceFee.nonTranFees)
              ],
              retAndEsc: res.data.retAndEsc || [
                cloneDeep(CONST.FAServiceFee.retAndEsc)
              ],
              sof: res.data.sof || [cloneDeep(CONST.FAServiceFee.sof)],
              contractRef: {
                ...prevState.contractRef,
                ...res.data.contractRef
              },
              notes: res.data.notes || "",
              createdAt: res.data.createdAt || "",
              loading: false
            }))
          } else {
            const { contracts } = this.state
            let contract = (value && value.activityContract) || {}
            if (contracts && contracts.length) {
              if (
                Object.keys(contract).length &&
                contract.contractRef &&
                contract.contractRef._id
              ) {
                contract = contracts.find(
                  con => con.contractRef._id === contract.contractRef._id
                )
              }
            }

            let nonTranFees = (contract && contract.nonTranFees) || []
            if(res && res.data && res.data.nonTranFees && nonTranFees.length){
              if(nonTranFees.length){
                res.data.nonTranFees.forEach(fee => {
                  const feeIndex = nonTranFees.findIndex(nFee => nFee.role === fee.role)
                  if(feeIndex === -1){
                    nonTranFees.push(fee)
                  }
                })
              }else {
                nonTranFees = res.data.nonTranFees
              }
            }

            this.setState({
              documents: (contract && contract.documents) || [],
              digitize: (contract && contract.digitize) || false,
              isAttachWithInvoice: false,
              contractObjectId: (contract && contract._id) || "",
              errorMessages: {},
              contractRef: contract.contractRef || {},
              notes: (contract && contract.notes) || "",
              nonTranFees: (nonTranFees.length && nonTranFees) || [
                cloneDeep(CONST.FAServiceFee.nonTranFees)
              ],
              retAndEsc: (contract && contract.retAndEsc) || [
                cloneDeep(CONST.FAServiceFee.retAndEsc)
              ],
              sof: (contract && contract.sof) || [
                cloneDeep(CONST.FAServiceFee.sof)
              ],
              invoiceNumber: (res && res.data && res.data.invoiceNumber) || "",
              createdAt: (res && res.data && res.data.createdAt) || "",
              loading: false
            })
          }
        }
      }
    )
  }

  render() {
    const {
      loading,
      contracts,
      objectName,
      fileName,
      createdAt,
      isAttachWithInvoice,
      contractRef,
      sof,
      nonTranFees,
      digitize,
      retAndEsc,
      documents,
      notes,
      errorMessages,
      dropDown,
      issuer,
      issueName,
      invoiceNumber
    } = this.state
    const contractDoc =
      (documents && documents[0] && documents[0].docAWSFileLocation) || ""
    let contract = {}
    if (
      issueName &&
      issueName.activityContract &&
      contracts &&
      contracts.length
    ) {
      if (
        Object.keys(issueName.activityContract).length &&
        issueName.activityContract.contractRef &&
        issueName.activityContract.contractRef._id
      ) {
        contract = contracts.find(
          con =>
            con.contractRef._id === issueName.activityContract.contractRef._id
        )
      }
    }
    if (loading) {
      return <Loader />
    }
    return (
      <div>
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Contract"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <div className="columns">
                      <div className="column">
                        <p className="multiExpLbl">Issuer</p>
                        <div className="control">
                          <DropdownList
                            filter
                            data={dropDown.issuers || []}
                            defaultValue=""
                            value={issuer.activityClientName || ""}
                            valueField={a => a}
                            textField="activityClientName"
                            onChange={val => this.onChange(val)}
                          />
                        </div>
                      </div>
                      <div className="column">
                        <p className="multiExpLbl">Transaction / Issue Name</p>
                        <div className="control">
                          <DropdownList
                            filter
                            data={dropDown.transactions || []}
                            value={issueName.activityDescription}
                            disabled={isEmpty(dropDown.transactions || [])}
                            valueField={a => a}
                            textField="activityDescription"
                            groupBy={({
                              activityTranType,
                              activityTranSubType
                            }) =>
                              `${activityTranType} - ${activityTranSubType}`
                            }
                            onChange={val => this.onChange(val)}
                          />
                        </div>
                      </div>
                      <div className="column">
                        <p className="multiExpLbl">Invoice Number</p>
                        <div className="control">
                          {invoiceNumber || "--"}
                          {/* <TextLabelInput */}
                          {/* placeholder="" */}
                          {/* value={invoiceNumber || ""} */}
                          {/* style={{ width: "unset" }} */}
                          {/* disabled */}
                          {/* /> */}
                        </div>
                      </div>
                    </div>
                    {issuer &&
                    issuer.activityTranClientId &&
                    issuer.activityId ? (
                        <div>
                          <a
                            className="has-text-link"
                            style={{
                              fontSize: 12,
                              padding: "0 0 8px 11px",
                              marginTop: -10
                            }}
                            target="_blank"
                            href={`/clients-propects/${
                              issuer.activityTranClientId
                            }/contracts`}
                          >
                          Add New Contract
                          </a>
                          <i
                            className="fa fa-refresh"
                            style={{
                              fontSize: 15,
                              marginTop: -7,
                              cursor: "pointer",
                              padding: "0 0 0 10px"
                            }}
                            onClick={() => this.onChange(issuer)}
                          />
                          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                            <TableHeader cols={cols} />
                            <tbody>
                              {contract &&
                            Object.keys(contract).length &&
                            contract.contractRef &&
                            contract.contractRef._id ? (
                                  <tr>
                                    <td className="emmaTablesTd">
                                      <small>
                                        {" "}
                                        <a
                                          onClick={() =>
                                            this.getContrractDocLink(contractDoc)
                                          }
                                        >
                                          {(contract.contractRef &&
                                        contract.contractRef.contractId) ||
                                        "---"}
                                        </a>
                                      </small>
                                    </td>
                                    <td className="emmaTablesTd">
                                      <small>
                                        {(contract.contractRef &&
                                      contract.contractRef.contractName) ||
                                      "---"}
                                      </small>
                                    </td>
                                    <td className="emmaTablesTd">
                                      <small>
                                        {(contract.contractRef &&
                                      contract.contractRef.contractType) ||
                                      "---"}
                                      </small>
                                    </td>
                                    <td className="emmaTablesTd">
                                      <small>
                                        {contract.contractRef &&
                                    contract.contractRef.startDate
                                          ? moment(
                                            contract.contractRef.startDate
                                          ).format("MM-DD-YYYY")
                                          : ""}
                                      </small>
                                    </td>
                                    <td className="emmaTablesTd">
                                      <small>
                                        {contract.contractRef &&
                                    contract.contractRef.endDate
                                          ? moment(
                                            contract.contractRef.endDate
                                          ).format("MM-DD-YYYY")
                                          : ""}
                                      </small>
                                    </td>
                                  </tr>
                                ) : (
                                  <tr>
                                    <td style={{ textAlign: "center" }} colSpan={6}>
                                  No Contracts Attach
                                    </td>
                                  </tr>
                                )}
                            </tbody>
                          </table>
                        </div>
                      ) : null}
                    {/* { contractDoc &&
                    <div className="column">
                      <p className="multiExpLblBlk">Contract Document</p>
                      <p className="dashExpLblVal">
                        <DocLink docId={contractDoc} />
                      </p>
                    </div>
                  } */}
                  </div>
                )}
              </RatingSection>
              <br />
              {issueName &&
                issuer &&
                issueName.activityDescription &&
                issuer.activityClientName &&
                digitize && (
                <div>
                  {invoiceNumber && !isAttachWithInvoice ?
                    <small className="text-error has-text-danger">This contract is not attach with this invoice please
                        save and attach</small> : null
                  }
                  {invoiceNumber &&
                    Object.keys(contract).length &&
                    contract.contractRef &&
                    contract.contractRef._id ? (
                      <ScheduleTransactionAndRetainer
                        contextId={issuer.activityTranClientId || ""}
                        dropDown={dropDown || {}}
                        createdAt={createdAt}
                        billingContract
                        state={{
                          contractRef,
                          sof,
                          nonTranFees,
                          digitize,
                          retAndEsc,
                          documents,
                          notes,
                          errorMessages
                        }}
                        onSave={this.onSave}
                        onCancel={this.onCancel}
                      />
                    ) : (
                      !invoiceNumber && <div>Invoice not exists</div>
                    )}
                </div>
              )}
            </div>
          )}
        />
        <a
          id="link"
          onClick={() => getFile(`?objectName=${objectName}`, fileName)}
          download={fileName}
          hidden
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  auth: state.auth || {}
})

const WrappedComponent = withAuditLogs(FAServiceFee)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
