import React from "react"
import {NumberInput, SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"

const TransactionalFees = ({category, item, roles, scheduleIndex, error, index, onChangeItem, onRemove}) => {
  const onChange = (event) => {
    onChangeItem({
      ...item,
      [event.target.name]: event.target.value
    }, category, index)
  }

  return(
    <tr key={scheduleIndex}>
      <td>
        <SelectLabelInput error={error.role || ""} list={roles || []} name="role" value={item.role} onChange={onChange}/>
      </td>
      <td>
        <NumberInput prefix="$" error={error.stdHourlyRate || ""} name="stdHourlyRate" placeholder="$1000" value={item.stdHourlyRate || ""} onChange={onChange}/>
      </td>
      <td>
        <NumberInput prefix="$" error={error.quoatedRate || ""} name="quoatedRate" placeholder="$1000" value={item.quoatedRate || ""} onChange={onChange}/>
      </td>
      <td>
        <span className="has-text-link"  style={{cursor: "pointer"}} onClick={() => onRemove(category, index)}>
          <i className="far fa-trash-alt" />
        </span>
      </td>
    </tr>
  )
}
export default TransactionalFees
