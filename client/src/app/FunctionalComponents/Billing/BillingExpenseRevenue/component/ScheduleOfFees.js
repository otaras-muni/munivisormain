import React from "react"
import {NumberInput, SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"

const ScheduleOfFees = ({category, item, dropDown, scheduleIndex, feesIndex, error, onChangeItem, onRemove}) => {
  const onChange = (event) => {
    onChangeItem({
      ...item,
      [event.target.name]: event.target.value
    }, category, scheduleIndex, feesIndex)
  }

  return(
    <tr key={scheduleIndex}>
      <td>
        <SelectLabelInput error={error.sofDesc || ""} list={dropDown || []} name="sofDesc" value={item.sofDesc || ""} onChange={onChange}/>
      </td>
      <td>
        <NumberInput prefix="$" error={error.sofAmt || ""} name="sofAmt" placeholder="$1000" value={item.sofAmt || ""} onChange={onChange}/>
      </td>
      <td>
        <NumberInput prefix="$" error={error.sofCharge || ""} name="sofCharge" placeholder="$1.00" value={item.sofCharge || ""} onChange={onChange} />
      </td>
      <td>
        <a className={`${item.sofDesc === "Amount Over" ? "isDisabled" : null }`} style={{cursor: "pointer"}} onClick={() => onRemove(scheduleIndex, feesIndex)}>
          <span className="has-text-link">
            <i className="far fa-trash-alt" />
          </span>
        </a>
      </td>
    </tr>
  )
}
export default ScheduleOfFees
