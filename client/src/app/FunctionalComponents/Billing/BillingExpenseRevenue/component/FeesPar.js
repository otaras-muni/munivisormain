import React from "react"
import {NumberInput, SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"

const FeesPar = ({category, item, dropDown, scheduleIndex, feesIndex, error, onChangeItem, onRemove}) => {
  const onChange = (event) => {
    onChangeItem({
      ...item,
      [event.target.name]: event.target.value
    }, category, scheduleIndex, feesIndex)
  }

  return(
    <tr key={scheduleIndex}>
      <td>
        <p>Fee Per</p>
      </td>
      <td>
        <NumberInput prefix="$" error={error.sofAmt || ""} name="sofAmt" placeholder="$1000" value={item.sofAmt || ""} onChange={onChange}/>
      </td>
      <td>
        <NumberInput prefix="$" error={error.sofCharge || ""} name="sofCharge" placeholder="$1.00" value={item.sofCharge || ""} onChange={onChange} />
      </td>
    </tr>
  )
}
export default FeesPar
