import React, { Component } from "react"
import { connect } from "react-redux"
import dateFormat from "dateformat"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import isEmpty from "lodash/isEmpty"
import Loader from "../../../GlobalComponents/Loader"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import CONST from "GlobalUtils/consts"
import DropDownListClear from "../../../GlobalComponents/DropDownListClear"
import { fetchAssigned } from "../../../StateManagement/actions/CreateTransaction"
import {
  fetchBillingManageExpense,
  managebillingexpensesstate,
  putBillingManageExpense,
  pullExpenseReceipts,
  postExpenseReceipts
} from "../../../StateManagement/actions"
import {
  NumberInput,
  SelectLabelInput,
  TextLabelInput
} from "../../../GlobalComponents/TextViewBox"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import TableHeader from "../../../GlobalComponents/TableHeader"
import ReceiptsModal from "./ReceiptsModal"
import { ContextType } from "../../../../globalutilities/consts"
import {TimeTrackingValidate} from "../Validation/BillingInvoiceValidate"
import DocLink from "../../docs/DocLink"

const cols = [
  [
    { name: "Expense Type<span class=\"icon has-text-danger\"><i class=\"fas fa-asterisk extra-small-icon\" /></span>" },
    { name: "Total Amount<span class=\"icon has-text-danger\"><i class=\"fas fa-asterisk extra-small-icon\" /></span>" },
    { name: "Date<span class=\"icon has-text-danger\"><i class=\"fas fa-asterisk extra-small-icon\" /></span>" },
    { name: "Meeting Type" },
    // { name: "Detail" },
    { name: "Receipts" },
    // { name: "Add Receipts" },
    { name: "Delete" }
  ],
  [{ name: "User Name" }, { name: "Amount " }, { name: "Action" }]
]
const expenseDetails = {
  expenseType: "",
  expenseNumMiles: 0,
  expenseRatePerMile: 0,
  expenseAmount: "",
  expenseDate: "",
  expenseContextType: "",
  expenseDetail: ""
}
const getPartKey = (type, subType) => {
  if(subType === "Bond Issue") return "dealPartContactId"
  if(type === "RFP") return "rfpParticipantContactId"
  return "partContactId"
}
class ExpenseDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bucketName: CONST.bucketName,
      contextType: ContextType.billing.receipts,
      loading: true,
      disableExpense: false,
      selectedEntity: {},
      selectedTran: {},
      selectedUser: {},
      outOfPocketExpensesData: this.initialState().outOfPocketExpensesData,
      dropDown: {
        issuerList: [],
        users: [],
        transactions: [],
        expenseContextTypes: [],
        expenseTypes: []
      },
      expenseId: "",
      uploadedFiles: [],
      errorMessages: {},
      documents: [],
      receiptsModal: false,
      totalAmount: 0,
      confirmation: false
    }
  }

  initialState = () => ({
    outOfPocketExpensesData: [cloneDeep(expenseDetails)]
  });

  async componentDidMount() {
    const { selectedInvoice, tenantId } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPEXPENSETYPES",
      "LKUPEXPENSECONTEXTTYPES"
    ])
    const result = (picResult.length && picResult[1]) || {}
    let stateObj = {
      selectedClient: {},
      selectedActivity: {},
      selectedInvoice: {},
      selectedUser: {}
    }
    if (selectedInvoice) {
      stateObj = selectedInvoice
    }
    await managebillingexpensesstate(
      { stateObj },
      async ({ selectedData, dropDownValues }) => {
        const activityDetails = dropDownValues.activityDetails.find(f => selectedInvoice && selectedInvoice.selectedActivity && f.activityId === selectedInvoice.selectedActivity.activityId) || {}
        if (selectedInvoice) {
          await fetchAssigned(
            selectedInvoice.selectedActivity &&
              selectedInvoice.selectedActivity.activityTranClientId,
            users => {
              this.setState(
                prevState => ({
                  dropDown: {
                    ...prevState.dropDown,
                    issuerList: dropDownValues.clientDetails,
                    transactions: dropDownValues.activityDetails,
                    users: (activityDetails && activityDetails.eligibleUserDetails) || [],
                    expenseContextTypes: result.LKUPEXPENSECONTEXTTYPES || [],
                    expenseTypes: result.LKUPEXPENSETYPES || []
                  },
                  selectedEntity: selectedInvoice.selectedClient || {},
                  selectedTran: selectedInvoice.selectedActivity,
                  selectedUser: {},
                  loading: false
                }),
                () => this.getExpense()
              )
            }
          )
        } else {
          this.setState(prevState => ({
            dropDown: {
              ...prevState.dropDown,
              issuerList: dropDownValues.clientDetails,
              expenseContextTypes: result.LKUPEXPENSECONTEXTTYPES || [],
              expenseTypes: result.LKUPEXPENSETYPES || []
            },
            loading: false
          }))
        }
      },
      this.props.token
    )
  }

  onChange = (name, value) => {
    const { tenantId } = this.props
    if (name === "selectedEntity") {
      this.setState(
        {
          loading: true
        },
        async () => {
          await managebillingexpensesstate(
            {
              stateObj: {
                selectedClient: value,
                selectedActivity: {},
                selectedInvoice: {},
                selectedUser: {}
              }
            },
            async ({ selectedData, dropDownValues }) => {
              // await fetchAssigned(value.activityTranClientId, (users)=> {
              this.setState(prevState => ({
                dropDown: {
                  ...prevState.dropDown,
                  transactions: dropDownValues.activityDetails
                  // users: users.usersList,
                },
                selectedEntity: value || {},
                selectedTran: {},
                selectedUser: {},
                loading: false
              }))
              // })
            },
            this.props.token
          )
        }
      )
    } else {
      this.setState(
        {
          [name]: value,
          selectedUser: {},
          dropDown: {
            ...this.state.dropDown,
            users: (value && value.eligibleUserDetails) || []
          }
        },
        () => this.getExpense()
      )
    }
  }

  getExpense = async () => {
    const { selectedEntity, selectedTran } = this.state
    if (
      Object.keys(selectedEntity).length &&
      Object.keys(selectedTran).length
    ) {
      const query = `/${selectedEntity.activityTranClientId}?activityId=${
        selectedTran.activityId
      }`
      const expense = await fetchBillingManageExpense(query)
      const disableValue = ["Invoice Paid", "Invoice Sent to Client", "Sent for review"]
      const disableExpense = disableValue.indexOf(expense && expense.invoiceStatus) > -1
      this.setState(
        {
          expense: expense && expense.userExpenses,
          amount: expense && expense.userTimeTrackerCount,
          // outOfPocketExpensesData: (expense && expense.outOfPocketExpensesData && expense.outOfPocketExpensesData.length) ? expense.outOfPocketExpensesData : [cloneDeep(expenseDetails)],
          expenseId: (expense && expense._id) || "",
          disableExpense
        },
        () => this.totalCount()
      )
    }
  }

  renderAccordionBody = aIndex => {
    const { selectedData } = this.state
    const outOfPocketExpensesData =
      (selectedData && selectedData.selectedUserExpenses) || []
    return (
      <tbody>
        {outOfPocketExpensesData.map((row, rowindex) =>
          this.renderAccordionRow(aIndex, rowindex, row)
        )}
      </tbody>
    )
  }

  handleFieldChange = (rowIndex, e, field) => {
    let name = ""
    let value = ""
    if (field) {
      name = field
      value = e
    } else {
      name = e.target.name
      value = e.target.value
    }
    const outOfPocketExpensesData = cloneDeep(
      this.state.outOfPocketExpensesData || []
    )

    // expense Data is being managed by react-widgets and as a result the value is directly being set without the need to get from event.target
    if (name === "expenseDate") {
      outOfPocketExpensesData[rowIndex][name] = e
    } else {
      outOfPocketExpensesData[rowIndex][name] = value
    }

    if (name === "expenseNumMiles" || name === "expenseRatePerMile") {
      const numMiles = outOfPocketExpensesData[rowIndex].expenseNumMiles || 0
      const milesPer = outOfPocketExpensesData[rowIndex].expenseRatePerMile || 0
      outOfPocketExpensesData[rowIndex].expenseAmount = numMiles * milesPer
    }
    this.setState({ outOfPocketExpensesData })
  }

  handleIsConfirmChange = (e) =>{
    this.setState({[e.target.name]: !this.state.confirmation })
  }

  removeExpense = index => {
    const { outOfPocketExpensesData } = this.state
    outOfPocketExpensesData.splice(index, 1)
    this.setState({ outOfPocketExpensesData, errorMessages: {} })
  }

  addExpense = () => {
    const { outOfPocketExpensesData } = this.state
    outOfPocketExpensesData.unshift(cloneDeep(expenseDetails))
    this.setState({ outOfPocketExpensesData, errorMessages: {} })
  }

  resetExpense = (e) => {
    e.stopPropagation()
    const {selectedUser} = this.state
    this.setState({
      outOfPocketExpensesData: this.initialState().outOfPocketExpensesData,
      errorMessages: {}
    }, () => this.onEdit(selectedUser))
  }

  onSaveDetails = () => {
    const {
      selectedEntity,
      selectedTran,
      selectedUser,
      outOfPocketExpensesData,
      expenseId,
      confirmation
    } = this.state
    const payload = {
      ...selectedEntity,
      ...selectedTran,
      userId: selectedUser.userId,
      userFirstName: selectedUser.userFirstName,
      userLastName: selectedUser.userLastName,
      outOfPocketExpensesData,
      confirmation,
    }

    if (expenseId) {
      payload._id = expenseId
    }

    const errors = TimeTrackingValidate(payload, "expenseDetails")
    if (errors && errors.error) {
      const errorMessages = {}
      console.log("=======>", errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) { //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) { //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] = {
            [err.path[1]]: {
              [err.path[2]]: err.message
            }
          }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: err.message
          }
        }
      })
      console.log("=========================errorMessages====================>", errorMessages)
      toast((errorMessages.outOfPocketExpensesData && errorMessages.outOfPocketExpensesData.undefined),{
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      });
      return this.setState({ errorMessages, activeItem: [0, 1] })
    }

    this.setState(
      {
        loading: true
      },
      async () => {
        const res = await putBillingManageExpense(payload)
        if (res && res.status === 200) {
          toast("Expense details updated successfully", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.SUCCESS
          })
          const query = `/${selectedEntity.activityTranClientId}?activityId=${
            selectedTran.activityId
          }`
          const response = await fetchBillingManageExpense(query)
          this.setState({
            expense: response && response.userExpenses,
            amount: response && response.userTimeTrackerCount,
            loading: false,
            // unset edit item
            outOfPocketExpensesData: [cloneDeep(expenseDetails)],
            expenseId: "",
            selectedUser: {},
            //
            // expenseId: (res.data && res.data._id) || "",
            errorMessages: {}
          })
          this.totalCount()
        } else {
          toast("Something went wrong, please try a latter.", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })
          this.setState({
            loading: false
          })
        }
      }
    )
  }

  onModalChange = (state, type) => {
    this.setState(
      {
        ...state
      },
      () => {
        const { expenseId, pocketId, uploadedFiles } = this.state
        const { user } = this.props
        if (type === "upload" && uploadedFiles && uploadedFiles.length) {
          const query = `?expenseId=${expenseId}&pocketId=${pocketId}`
          const payload = uploadedFiles.map(file => ({
            docAWSFileLocation: file.fileId || "",
            docFileName: file.fileNeme || "",
            createdBy: user.userId || "",
            createdUserName: `${user.userFirstName} ${user.userLastName}`
          }))
          this.setState(
            {
              loading: true
            },
            async () => {
              const recieptsUpload = await postExpenseReceipts(query, {
                documents: payload
              })
              if (recieptsUpload && recieptsUpload.status === 200) {
                toast("Receipts uploaded successfully", {
                  autoClose: CONST.ToastTimeout,
                  type: toast.TYPE.SUCCESS
                })
                this.setState({
                  loading: false,
                  uploadedFiles: [],
                  outOfPocketExpensesData: recieptsUpload.data || [],
                  receiptsModal: false,
                  documents: []
                })
              } else {
                toast("Something went wrong, please try a latter.", {
                  autoClose: CONST.ToastTimeout,
                  type: toast.TYPE.ERROR
                })
                this.setState({
                  loading: false
                })
              }
            }
          )
        }
      }
    )
  }

  handleReceiptsModal = pocket => {
    this.setState({
      receiptsModal: true,
      pocketId: pocket._id,
      documents: pocket.documents || []
    })
  }

  onRemoveReceipt = (versionId, documentId) => {
    console.log("versionId : ", versionId)
    console.log("documentId : ", documentId)
    const { expenseId, pocketId } = this.state
    if (documentId) {
      const query = `?expenseId=${expenseId}&pocketId=${pocketId}&docId=${documentId}`
      this.setState(
        {
          loading: true
        },
        async () => {
          const recieptsUpload = await pullExpenseReceipts(query)
          if (recieptsUpload && recieptsUpload.status === 200) {
            toast("Receipts removed successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.setState({
              loading: false,
              uploadedFiles: [],
              outOfPocketExpensesData: recieptsUpload.data || [],
              receiptsModal: false,
              documents: []
            })
          } else {
            toast("Something went wrong, please try a latter.", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              loading: false
            })
          }
        }
      )
    }
  }

  onEdit = user => {
    const expense = cloneDeep(this.state.expense)
    if (user && user.userId && expense[user.userId]) {
      this.setState(
        {
          outOfPocketExpensesData:
            expense[user.userId] &&
            expense[user.userId].outOfPocketExpensesData.length === 0
              ? [cloneDeep(expenseDetails)]
              : expense[user.userId] && expense[user.userId].outOfPocketExpensesData,
          expenseId: (expense[user.userId] && expense[user.userId]._id) || "",
          errorMessages: {},
          selectedUser: user,
          confirmation: (expense[user.userId] && expense[user.userId].confirmation) || false
        },
        () => this.totalCount
      )
    } else {
      this.setState(
        {
          outOfPocketExpensesData: [cloneDeep(expenseDetails)],
          expenseId: "",
          selectedUser: user,
          errorMessages: {},
          confirmation: false
        },
        () => this.totalCount
      )
    }
  }

  totalCount = () => {
    const { dropDown, amount } = this.state
    let total = 0
    cloneDeep(dropDown.users).forEach(user => {
      // const userName = `${user.userFirstName} ${user.userLastName}` || ""
      total += (amount && amount[user.userId] && amount[user.userId].totalAmount) || 0
    })
    this.setState({
      totalAmount: total && total.toFixed(2) || 0,
    })
  }

  render() {
    const {
      loading,
      bucketName,
      dropDown,
      selectedEntity,
      selectedTran,
      selectedUser,
      outOfPocketExpensesData,
      errorMessages,
      amount,
      disableExpense,
      totalAmount,
      confirmation
    } = this.state
    const { user } = this.props

    if (loading) {
      return <Loader />
    }
    return (
      <Accordion
        multiple
        activeItem={[0]}
        boxHidden
        render={({ activeAccordions, onAccordion }) => (
          <div>
            <ReceiptsModal
              state={this.state}
              user={user}
              onModalChange={this.onModalChange}
              onRemove={this.onRemoveReceipt}
            />
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title="Enter Out of Pocket Expenses"
            >
              {activeAccordions.includes(0) && (
                <div>
                  <div className="columns">
                    <div className="column">
                      <p className="multiExpLbl">Select Issuer</p>
                      <div className="control">
                        <DropDownListClear
                          filter
                          data={dropDown.issuerList || []}
                          defaultValue=""
                          value={selectedEntity.activityClientName || ""}
                          valueField={a => a}
                          textField="activityClientName"
                          isHideButton={selectedEntity.activityClientName}
                          onChange={val => this.onChange("selectedEntity", val)}
                          onClear={() => {this.onChange("selectedEntity", {})}}
                        />
                      </div>
                    </div>
                    <div className="column">
                      <p className="multiExpLbl">Transaction / Issue Name</p>
                      <div className="control">
                        <DropDownListClear
                          filter
                          data={dropDown.transactions || []}
                          value={selectedTran.activityDescription || ""}
                          disabled={isEmpty(dropDown.transactions || [])}
                          valueField={a => a}
                          textField="activityDescription"
                          isHideButton={selectedTran.activityDescription}
                          groupBy={({
                            activityTranType,
                            activityTranSubType
                          }) => `${activityTranType} - ${activityTranSubType}`}
                          onChange={val => this.onChange("selectedTran", val)}
                          onClear={() => {this.onChange("selectedTran", {})}}
                        />
                      </div>
                    </div>
                  </div>
                  {selectedTran && selectedTran.activityDescription ? (
                    <div className="tbl-scroll">
                      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                        <TableHeader cols={cols[1]} />
                        <tbody>
                          {dropDown.users.map((user, index) => {
                            // const userName = `${user.userFirstName} ${user.userLastName}` || ""
                            return (
                              <tr key={index.toString()}>
                                <td>{`${user.userFirstName} ${user.userLastName}` || "--"}</td>
                                <td>
                                  {amount &&
                                  amount[user.userId] &&
                                  amount[user.userId].totalAmount
                                    ? `$${Number(amount[user.userId].totalAmount).toLocaleString()}`
                                    : "-"}
                                </td>
                                <td>
                                  {!disableExpense ?
                                    <span className="has-text-link">
                                      <a
                                        className="fas fa-pencil-alt"
                                        onClick={() => this.onEdit(user)}
                                        title="Edit Expense"
                                      />
                                    </span> :
                                    <span className="has-text-link">
                                      <a
                                        className="fas fa-eye"
                                        onClick={() => this.onEdit(user)}
                                        title="View Expense"
                                      />
                                    </span>
                                  }
                                </td>
                              </tr>
                            )
                          })}
                          <tr>
                            <td>Total</td>
                            <td>{totalAmount ? `$${Number(totalAmount).toLocaleString()}` : "-"}</td>
                            <td />
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  ) : null}
                  <hr />
                  {selectedUser && selectedUser.userFirstName ? (
                    <div className="tbl-scroll">
                      <div className="field is-grouped is-pulled-left">
                        <div className="column expdetails">
                          <p className="multiExpLbl">User Name</p>
                          <div className="control">
                            <div className="Select Firm User select-hack">
                              {`${selectedUser && selectedUser.userFirstName} ${selectedUser && selectedUser.userLastName}` || ""}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="field is-grouped is-pulled-right">
                        { !disableExpense ?
                          <div className="control">
                            <button onClick={this.addExpense} className="button is-link is-small">Add</button>
                            <button onClick={this.resetExpense} className="button is-light is-small">Reset</button>
                            {/* <a
                              className="has-text-link is-size-4"
                              onClick={this.addExpense}
                            >
                              <i className="far fa-plus-square" />
                            </a> */}
                          </div> : null
                        }
                      </div>
                      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                        <TableHeader cols={cols[0]} />
                        <tbody>
                          {outOfPocketExpensesData &&
                          outOfPocketExpensesData.length
                            ? outOfPocketExpensesData.map((pocket, i) => {
                              const error =
                                (errorMessages &&
                                  errorMessages.outOfPocketExpensesData &&
                                  errorMessages.outOfPocketExpensesData[
                                    i.toString()
                                  ]) ||
                                {}
                              const isExists = cloneDeep(
                                outOfPocketExpensesData
                              ).map(
                                item =>
                                  item.expenseType !== pocket.expenseType &&
                                    item.expenseType
                              )
                              const expenseTypes = cloneDeep(
                                dropDown.expenseTypes
                              ).filter(type => isExists.indexOf(type) === -1)
                              return (
                                <tr key={i.toString()}>
                                  <td className="emmaTablesTd">
                                    <div className="Select Firm User select-hack">
                                      <SelectLabelInput
                                        name="expenseType"
                                        list={dropDown.expenseTypes || []}
                                        value={pocket.expenseType || ""}
                                        onChange={e => this.handleFieldChange(i, e)}
                                        disabled={disableExpense}
                                      />
                                    </div>
                                    <small className="text-error has-text-danger">
                                      {error.expenseType || ""}
                                    </small>
                                  </td>
                                  {pocket.expenseType === "Mileage" ? (
                                    <td className="multiExpTblVal">
                                      <div className="columns">
                                        <div className="column">
                                          <NumberInput
                                            name="expenseNumMiles"
                                            type="text"
                                            placeholder="#miles"
                                            value={pocket.expenseNumMiles || 0}
                                            onChange={e => this.handleFieldChange(i, e)}
                                            disabled={disableExpense}
                                          />
                                        </div>
                                        <div className="column">
                                          <NumberInput
                                            prefix="$"
                                            name="expenseRatePerMile"
                                            type="text"
                                            placeholder="@ $0.545/mile"
                                            value={pocket.expenseRatePerMile || 0}
                                            onChange={e => this.handleFieldChange(i, e)}
                                            disabled={disableExpense}
                                          />
                                        </div>
                                        <div className="column">
                                          <NumberInput
                                            prefix="$"
                                            placeholder="auto-calc"
                                            type="text"
                                            value={pocket.expenseAmount || 0}
                                            disabled
                                          />
                                        </div>
                                      </div>
                                      <small className="text-error has-text-danger">
                                        {error.expenseAmount || ""}
                                      </small>
                                    </td>
                                  ) : (
                                    <td className="multiExpTblVal">
                                      <NumberInput
                                        prefix="$"
                                        name="expenseAmount"
                                        type="text"
                                        placeholder="$"
                                        value={pocket.expenseAmount || 0}
                                        onChange={e => this.handleFieldChange(i, e)}
                                        decimalScale={4}
                                        disabled={disableExpense}
                                      />
                                      <small className="text-error has-text-danger">
                                        {error.expenseAmount || ""}
                                      </small>
                                    </td>
                                  )}

                                  <td className="multiExpTblVal">
                                    {/* <input id="datepickerDemo"
                                      className="input is-small is-link"
                                      type="date"
                                      name="expenseDate"
                                      value={pocket.expenseDate === "" || !pocket.expenseDate ? "": dateFormat(new Date(pocket.expenseDate),"yyyy-mm-dd")}
                                      onChange={e => this.handleFieldChange(i, new Date(e.target.value), "expenseDate")}
                                      disabled={disableExpense}
                                    /> */}

                                    <TextLabelInput
                                      type="date"
                                      name="expenseDate"
                                      value={(pocket.expenseDate === "" || !pocket.expenseDate) ? null : new Date(pocket.expenseDate)}
                                      disabled={disableExpense}
                                      onChange={e => this.handleFieldChange(i, new Date(e.target.value), "expenseDate")}
                                    />
                                    <small className="text-error has-text-danger">
                                      {error.expenseDate || ""}
                                    </small>
                                    {/* <DateTimePicker
                                        time={false}
                                        name="expenseDate"
                                        value={
                                          pocket.expenseDate === "" ||
                                          !pocket.expenseDate
                                            ? null
                                            : new Date(pocket.expenseDate)
                                        }
                                        onChange={e =>
                                          this.handleFieldChange(
                                            i,
                                            e,
                                            "expenseDate"
                                          )
                                        }
                                      /> */}
                                  </td>
                                  <td className="emmaTablesTd">
                                    <div className="Select Firm User select-hack">
                                      <SelectLabelInput
                                        name="expenseContextType"
                                        list={dropDown.expenseContextTypes || []}
                                        value={pocket.expenseContextType || ""}
                                        onChange={e => this.handleFieldChange(i, e)}
                                        disabled={disableExpense}
                                      />
                                    </div>
                                    <small className="text-error has-text-danger">
                                      {error.expenseContextType || ""}
                                    </small>
                                  </td>
                                  {/* <td className="multiExpTblVal">
                                    <input
                                      className="input is-small is-link"
                                      type="text"
                                      placeholder=""
                                      value={pocket.expenseDetail || ""}
                                      name="expenseDetail"
                                      onChange={e =>
                                        this.handleFieldChange(i, e)
                                      }
                                    />
                                    <small className="text-error has-text-danger">
                                      {error.expenseDetail || ""}
                                    </small>
                                  </td> */}
                                  <td className="multiExpTblVal">
                                    {
                                      pocket.documents && pocket.documents.length ? pocket.documents.map((doc,i) => (
                                        <div key={i.toString()}>
                                          <div>
                                            <div className="field is-grouped-left">
                                              {/* <DocUpload docId={doc.docAWSFileLocation} versionMeta={{ uploadedBy: doc.createdUserName }} showFeedback
                                                bucketName={bucketName} contextType={contextType || ""}
                                                contextId={(selectedTran && selectedTran.activityId) || ""} tenantId={user.entityId} />
                                                &nbsp;&nbsp; */}
                                              <DocLink docId={doc.docAWSFileLocation} />
                                              {/* <DocModal
                                                onDeleteAll={this.onRemoveReceipt}
                                                documentId={doc._id}
                                                selectedDocId={doc.docAWSFileLocation} /> */}
                                            </div>
                                          </div>
                                        </div>
                                      )) : <span>{pocket._id ? "No Receipts": "N/A"}&nbsp;&nbsp;</span>
                                    }
                                    {pocket._id ? (
                                      <a
                                        className="control"
                                        onClick={() => this.handleReceiptsModal(pocket)}
                                        title="Add Receipts"
                                      >
                                      <span className="has-text-link">
                                        <i className="far fa-plus-square"/>
                                      </span>
                                      </a>
                                    ) : null}
                                  </td>
                                  <td className="multiExpTblVal">
                                    { !disableExpense ?
                                      <div className="field is-grouped">
                                        <div
                                          className="control"
                                          onClick={() => this.removeExpense(i)}
                                        >
                                          <span className="has-text-link">
                                            <i className="far fa-trash-alt"/>
                                          </span>
                                        </div>
                                      </div> : null
                                    }
                                  </td>
                                </tr>
                              )
                            })
                            : null}
                        </tbody>
                      </table>
                      <p className="multiExpTblVal">
                        <label className="checkbox is-active">
                          <input type="checkbox" name="confirmation" onChange={e => this.handleIsConfirmChange(e)} checked={confirmation} disabled={disableExpense}/>
                          We confirm that costs and expenses related to the
                          entertainment of any person, including, but not
                          limited to, an official or other personnel of the
                          municipal entity or obligated person are not included
                        </label>
                      </p>
                      <br />
                      { !disableExpense ?
                        <div className="control level-right">
                          <button
                            className="button is-link is-small"
                            onClick={this.onSaveDetails}
                            disabled={!confirmation || disableExpense}
                          >
                            Save Details
                          </button>
                        </div> : null
                      }
                    </div>
                  ) : null}
                </div>
              )}
            </RatingSection>
          </div>
        )}
      />
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  tenantId: (auth && auth.tenantId) || "",
  userInfo: auth.userEntities,
  user: (auth && auth.userEntities) || {},
  token: auth.token
})

export default connect(mapStateToProps, null)(ExpenseDetails)
