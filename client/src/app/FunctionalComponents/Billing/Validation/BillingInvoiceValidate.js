import Joi from "joi-browser"

const bankDetailsSchema = Joi.object().keys({
  bankName: Joi.string().label("Bank Name").optional().required(),
  addressLine1: Joi.string().label("Address Line1").optional().required(),
  addressLine2: Joi.string().allow("").optional(),
  ABA: Joi.string().label("ABA#").optional().required(),
  accountOf: Joi.string().optional().required(),
  accountNo: Joi.number().label("Account No").optional().required(),
  attention: Joi.string().label("Bank Contact Name").optional().required(),
  telephone: Joi.string().label("Telephone").optional().required(),
  _id: Joi.string().required().optional(),
})

const BillingInvoiceSchema = Joi.object().keys({
  activityId: Joi.string().required().optional(),
  activityDescription: Joi.string().required().optional(),
  activityClientAddress: Joi.object().keys().optional(),
  activityTranType: Joi.string().required().optional(),
  activityTranSubType: Joi.string().required().optional(),
  activityTranFirmId: Joi.string().required().optional(),
  activityTranFirmName: Joi.string().required().optional(),
  activityTranClientId: Joi.string().required().optional(),
  activityLeadAdvisorId: Joi.string().required().optional(),
  activityLeadAdvisorName: Joi.string().required().optional(),
  activityParAmount: Joi.number().allow(null,"").required(),
  activityClientName: Joi.string().required().optional(),
  totalOutOfPocketExpenses: Joi.number().allow(null,"").required(),
  totalOverallExpenses: Joi.number().allow(null,"").required(),
  totalFinancialAdvisoryFees: Joi.number().min(0).allow(null,"").label("Financial Advisory Fees").required(),
  totalConsultingEngFees: Joi.number().allow(null,"").required(),
  invoiceNumber: Joi.string().required().optional(),
  invoiceNotes: Joi.string().allow("").optional(),
  bankDetails: bankDetailsSchema,
  outOfPocketExpenses: Joi.object().keys().required(),
  consultingCharge: Joi.object().keys().required(),
  _id: Joi.string().required().optional(),
})

export const BillingInvoiceValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, BillingInvoiceSchema, { abortEarly: false, stripUnknown:false })

const timeTrackingSchemas = Joi.object().keys({
  roleType: Joi.string().allow("").optional(),
  fromDate: Joi.date().example(new Date("2016-01-01")).label("From Date").required(),
  date: Joi.date().example(new Date("2016-01-01")).allow(null,""),
  toDate: Joi.date().example(new Date("2016-01-01")).label("To Date").min(Joi.ref("fromDate")).required(),
  hours: Joi.number().min(1).label("Hours").required(),
  _id: Joi.string().allow("").optional(),
})

const expenseData = Joi.object().keys({
  expenseType: Joi.string().label("Expense Type").required(),
  expenseNumMiles: Joi.number().label("Miles").min(0).allow(null,"").optional(),
  expenseRatePerMile: Joi.number().min(0).label("Rate Per Mile").allow(null,"").optional(),
  expenseAmount: Joi.number().min(0).label("Total Amount").required(),
  expenseDate: Joi.date().required().label("Date").optional(),
  expenseContextType: Joi.string().allow("").label("Context Type").optional(),
  expenseDetail: Joi.string().allow("").label("Expense Type").optional(),
  documents: Joi.array().optional(),
  _id: Joi.string().allow("").optional(),
})

const leadAdvisor = Joi.object().keys({
  userFirstName: Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userId: Joi.string().allow("").optional()
})

const timeTrackingSchema = type => Joi.object().keys({
  activityClientName: Joi.string().required().label("Activity ClientName").optional(),
  activityContract:  Joi.object().keys().required().label("Activity Contract").optional(),
  activityDescription: Joi.string().required().label("Activity Description").optional(),
  activityId: Joi.string().required().label("Activity Id").optional(),
  activityTranClientId: Joi.string().required().label("Activity Client Id").optional(),
  activityTranFirmId: Joi.string().required().label("Activity Firm Id").optional(),
  activityTranFirmName: Joi.string().required().label("Activity Firm Name").optional(),
  activityTranSubType: Joi.string().required().label("Activity Sub Type").optional(),
  activityTranType: Joi.string().required().label("Activity Type").optional(),
  eligibleUserDetails: Joi.array().label("Eligible User Details").optional(),
  roleType: type === "expenseDetails" ? Joi.string().allow("").label("Role Type").optional() : Joi.string().label("Role Type").required(),
  timeTracker: type === "expenseDetails" ? Joi.array().label("Time Tracker").optional() : Joi.array().min(1).label("Time Tracker").items(timeTrackingSchemas).required(),
  outOfPocketExpensesData: type === "timeTracker" ? Joi.array().label("Out Of Pocket Expenses Data").optional() : Joi.array().min(1).label("Out Of Pocket Expenses Data").items(expenseData).required(),
  activityLeadAdvisor: leadAdvisor,
  activityParticipants: Joi.array().label("Activity Participants").optional(),
  userFirstName: Joi.string().required().label("First Name").optional(),
  userId: Joi.string().required().label("User Id").optional(),
  userLastName: Joi.string().label("Last Name").required().optional(),
  _id: Joi.string().allow("").optional(),
  confirmation: Joi.boolean().required().optional(),
})


export const TimeTrackingValidate = (inputTransDistribute, type) => Joi.validate(inputTransDistribute, timeTrackingSchema(type), { abortEarly: false, stripUnknown:false })

