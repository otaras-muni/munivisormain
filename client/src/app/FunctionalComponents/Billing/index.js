import React from "react"
import { NavLink } from "react-router-dom"
import Loader from "Global/Loader"
import BillingDashboard from "./BillingDashboard"
import BillingExpenseRevenueMain from "./BillingExpenseRevenue"


const activeStyle = {
  backgroundColor: "#fff",
  color: "#4a4a4a",
  borderColor: "#F29718",
  borderWidth: "3px",
  borderBottomWidth: "0px"
}

class BillingMain extends React.Component {

  static defaultProps = {
    option: "billing-dashboard"
  }




  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      TABS: [
        { path: "billing-dashboard", label: "Dashboard" },
        { path: "billing-expensedetail", label: "Manage Expense Revenue" },
      ]
    }
  }

  componentWillMount() {
    const { TABS } = this.state
    this.setState({
      loading: false,
      TABS
    })
  }

  renderTabs = (tabs, option) => tabs.map(t =>
    <li key={t.path} className={option === t.path ? "is-active" : "inactive-tab"}>
      <NavLink activeStyle={activeStyle} to={`/tools-billing/${t.path}`}>{t.label}</NavLink>
    </li>
  )

  renderViewSelection = (option) => (
    <nav className="tabs is-boxed">
      <ul>
        {this.renderTabs(this.state.TABS, option)}
      </ul>
    </nav>
  )

  renderSelectedView = (option) => {
    switch (option) {
    case "billing-dashboard":
      return <BillingDashboard {...this.props} />
    case "billing-expensedetail":
      return <BillingExpenseRevenueMain {...this.props} option="" />
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { option, nav2 } = this.props
    const selectedOption = nav2 || option
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <section className="hero is-small">
          {/* <div className="hero-body">
            <div className="columns is-vcentered">
              <div className="column">
                <div className="subtitle">
                  <nav className="breadcrumb has-arrow-separator is-small is-right" aria-label="breadcrumbs">
                    <ul>
                      <li>
                        BILLING
                      </li>
                      <li className="is-active">
                        {selectedOption}
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div> */}

          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(selectedOption)}
            </div>
          </div>
        </section>
        <section>
          {this.renderSelectedView(selectedOption)}
        </section>
      </div>
    )
  }
}

export default BillingMain
