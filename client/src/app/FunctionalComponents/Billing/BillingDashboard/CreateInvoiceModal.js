import React from "react"
import NumberFormat from "react-number-format"
import { Modal } from "../../../FunctionalComponents/TaskManagement/components/Modal"
import PhoneInput, {formatPhoneNumber} from "react-phone-number-input"
import {
  MultiSelect,
  NumberInput,
  TextLabelBox,
  TextLabelInput
} from "../../../GlobalComponents/TextViewBox"
import Loader from "../../../GlobalComponents/Loader"

export const CreateInvoiceModal = props => {
  const disabled = props && props.state && props.state.disableInvoice || false
  const toggleModal = () => {
    props.onModalChange({
      modalVisible: !props.state.modalVisible,
      activity: {},
      newExpenses: []
    })
  }

  const onTextChange = (e, type) => {
    if (type === "bankDetails") {
      props.onModalChange({
        bankDetails: {
          ...props.state.bankDetails,
          [e.target.name]: e.target.value
        }
      })
    } else if (type === "telephone") {
      const Internationals = e && formatPhoneNumber(e, "International") || ""
      props.onModalChange({
        bankDetails: {
          ...props.state.bankDetails,
          telephone: Internationals
        }
      })
    } else {
      props.onModalChange({ [e.target.name]: e.target.value }, type)
    }
  }

  const onChange = (e, flag, type) => {
    if (flag === "Consulting Charge") {
      props.onModalChange(
        {
          consultingCharge: {
            ...props.state.consultingCharge,
            [e.target.name]: e.target.value
          }
        },
        type
      )
    } else {
      props.onModalChange(
        {
          outOfPocketExpenses: {
            ...props.state.outOfPocketExpenses,
            [e.target.name]: e.target.value
          }
        },
        flag
      )
    }
  }

  const onChangeNewExpenses = (e, index, flag) => {
    props.state.newExpenses[index][e.target.name] = e.target.value
    props.onModalChange(
      {
        newExpenses: props.state.newExpenses
      },
      flag
    )
  }

  const errors =
    props.state.errorMessages &&
    Object.keys(props.state.errorMessages || {}).length
      ? props.state.errorMessages
      : {}

  return (
    <Modal
      closeModal={toggleModal}
      modalState={props.state.modalVisible}
      title="Add Invoice"
      saveModal={disabled ? false : props.onSave}
      modalWidth={{ width: "70%" }}
      disabled={props.state.loading || props.state.modalLoading || false}
    >
      {props.state.modalLoading ? (
        <Loader/>
      ) : (
        <div>
          <section className="container has-text-centered">
            <p className="title innerPgTitle">
              {(props.user && props.user.firmName) || ""}
            </p>
            <br/>
          </section>
          <div className="columns">
            <TextLabelBox
              label="Client Name"
              value={(props.state.activity && props.state.activity.activityClientName) || ""}
            />
            <TextLabelBox
              label="Title or Client Company Name"
              value={(props.state.activity && props.state.activity.activityDescription) || ""}
            />
            <TextLabelBox
              label="Address"
              value={
                (props.state.activity &&
                  props.state.activity.activityClientAddress &&
                  props.state.activity.activityClientAddress
                    .formatted_address) ||
                ""
              }
            />
            {/* <TextLabelBox label="City, State zip code" value={props.state.activity.activityClientAddress && Object.keys(props.state.activity.activityClientAddress).length ? `${props.state.activity.activityClientAddress.city}, ${props.state.activity.activityClientAddress.state},
        ${props.state.activity.activityClientAddress.zipCode.zip1}-${props.state.activity.activityClientAddress.zipCode.zip2}` : "" || ""}/> */}
          </div>
          <br/>

          <div className="columns">
            <div className="column" >
              <p>
                <b>
                  Information
                </b>
              </p>
            </div>
            <div className="column">
              <p>
                <b>
                  Computed Expenses / Revenue
                </b>
              </p>
            </div>
            <div className="column">
              <p>
                <b>Revised Expenses / Revenue</b>
              </p>
            </div>
          </div>

          {!disabled ?
            <div className="columns">
              <div className="column"/>
              <div className="column">
                <button className="button is-link is-small" onClick={props.onClick} disabled={disabled}>Override
                  Expenses
                </button>
              </div>
              <div className="column"/>
            </div> : null
          }

          <div className="columns">
            <div className="column">
              <p className="multiExpLblBlk">
                For Financial Advisory services provided to{" "}
                <span style={{ color: "black" }}>
                  {" "}
                  {(props.state.activity &&
                    props.state.activity.activityClientName) ||
                  ""}
                  , for their $
                  {(props.state.activity.activityParAmount || 0)}{" "}
                  {`${props.state.activity.activityTranType || ""} / ${props
                    .state.activity.activityTranSubType || ""}`}
                  ,{" "}
                  {(props.state.activity &&
                    props.state.activity.activityDescription) ||
                  ""}{" "}
                </span>{" "}
              </p>
            </div>
            <div className="column">
              <small>
                {/* {props && props.state && props.state.computedFinancialAdvisoryFees ? `$${Number(props.state.computedFinancialAdvisoryFees).toLocaleString()}` : "$0"} */}
                Not computed by the system
              </small>
            </div>
            <div className="column">
              <NumberInput
                prefix="$"
                name="totalFinancialAdvisoryFees"
                error={errors.totalFinancialAdvisoryFees || ""}
                type="text"
                placeholder="$"
                value={props.state.totalFinancialAdvisoryFees || 0}
                disabled={disabled}
                onChange={e => onTextChange(e, "addTotal")}
                decimalScale={4}
              />
            </div>
          </div>

          {/* <div className="columns">
              <div className="column">
                <p className="multiExpLblBlk">For services related to the  <span style={{color:"red"}}> {(props.state.activity && props.state.activity.activityDescription) || ""} </span> </p>
              </div>
              <div className="column is-one-fifth">
                <small>
                  <NumberFormat className="input is-fullwidth multiExpLblBlk" thousandSeparator style={{width: 300, background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={parseInt(props.state.totalConsultingEngFees || 0,10)} />
                </small>
              </div>
              <div className="column">
                <NumberInput prefix="$" name="totalConsultingEngFees" error={errors.totalConsultingEngFees || ""} type="text" placeholder="$" value={parseInt(props.state.totalConsultingEngFees || 0,10)} onChange={(e) => onTextChange(e, "addTotal")}/>
              </div>
            </div> */}

          <div className="columns">
            <div className="column">
              <p className="title innerPgTitle">Consulting Charges</p>
            </div>
          </div>
          {props.state.roleType && props.state.roleType.length
            ? props.state.roleType.map(key => (
              <div className="columns" key={key}>
                <div className="column">
                  <p className="multiExpLblBlk">{key || ""}</p>
                </div>
                <div className="column">
                  <small>
                    {props.state.computedConsultingCharge && props.state.computedConsultingCharge[key] ? `$${Number(props.state.computedConsultingCharge[key]).toLocaleString()}` : ""}
                    {/* <NumberFormat
                      className="input is-fullwidth multiExpLblBlk"
                      thousandSeparator
                      style={{
                        width: 300,
                        background: "transparent",
                        border: "none"
                      }}
                      decimalScale={4}
                      prefix="$"
                      disabled
                      value={(props.state.computedConsultingCharge && props.state.computedConsultingCharge[key])}
                    /> */}
                  </small>
                </div>
                <div className="column">
                  <NumberInput
                    prefix="$"
                    name={key || ""}
                    type="text"
                    placeholder={key || ""}
                    error={(errors && errors.consultingCharge && errors.consultingCharge[key]) || ""}
                    value={(props.consultingCharge && props.consultingCharge[key])}
                    onChange={e => onChange(e, "Consulting Charge", "addTotal")}
                    disabled={disabled}
                    decimalScale={4}
                  />
                </div>
              </div>
            ))
            : null}
          {/* {
              props.state.roleType && props.state.roleType.length ? props.state.roleType.map(key => (
                <div className="columns has-text-centered" key={key} style={{ borderBottom: "1px solid #eae2e2", height: 38 }}>
                  <div className="column">
                    <p className="multiExpLbl">{key || ""}</p>
                  </div>
                  <div className="column">
                    <small>
                      <NumberFormat className="input" thousandSeparator style={{width: 300, background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={parseInt((props.state.consultingCharge && props.state.consultingCharge[key]) || 0,10)} />
                    </small>
                  </div>
                </div> )) : null
            } */}

          <div className="columns">
            <div className="column">
              <p className="title innerPgTitle">Out of Pocket Expenses</p>
            </div>
            {!disabled ?
              <div className="column">
                <div className="control is-pulled-right">
                  <a className="has-text-link is-size-5" onClick={props.onAdd}>
                    <i className="far fa-plus-square"/>
                  </a>
                </div>
              </div> : null
            }
          </div>
          {props.pocketExpenses
            ? props.pocketExpenses.map(pocket => (
              <div className="columns" key={pocket}>
                <div className="column">
                  <p className="multiExpLblBlk">{pocket || ""}</p>
                </div>
                <div className="column">
                  <small>
                    {props.state.pocketExpense &&
                    props.state.pocketExpense[pocket] ? (
                        `$${Number(props.state.pocketExpense[pocket]).toLocaleString()}`  || 0
                        /* <NumberFormat
                          className="input is-fullwidth multiExpLblBlk"
                          thousandSeparator
                          style={{
                            width: 300,
                            background: "transparent",
                            border: "none"
                          }}
                          decimalScale={4}
                          prefix="$"
                          disabled
                          value={props.state.pocketExpense[pocket] || 0}
                        /> */
                      ) : (
                        "-"
                      )}
                  </small>
                </div>
                <div className="column">
                  <NumberInput
                    prefix="$"
                    name={pocket || ""}
                    type="text"
                    placeholder={pocket || ""}
                    error={(errors && errors.outOfPocketExpenses && errors.outOfPocketExpenses[pocket]) || ""}
                    value={props.state.outOfPocketExpenses[pocket] || 0}
                    onChange={e => onChange(e, "addTotal")}
                    disabled={disabled}
                    decimalScale={4}
                  />
                </div>
              </div>
            ))
            : null}
          {props.state.newExpenses
            ? props.state.newExpenses.map((pocket, i) => (
              <div className="columns" key={i.toString()}>
                <div className="column">
                  <TextLabelInput
                    name="key"
                    placeholder="Name"
                    value={pocket.key || ""}
                    onChange={e => onChangeNewExpenses(e, i, "addTotal")}
                    disabled={disabled}
                  />
                </div>
                <div className="column">
                  <small>
                    {/* {parseInt(pocket.value || 0,10) ?
                      <NumberFormat className="input is-fullwidth is-size-7" thousandSeparator style={{width: 300, background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={parseInt(pocket.value || 0,10)} />
                      : "---" } */}
                  </small>
                </div>
                <div className="column">
                  <NumberInput
                    prefix="$"
                    name="value"
                    type="text"
                    placeholder="Value"
                    error={(errors && errors.outOfPocketExpenses && errors.outOfPocketExpenses[pocket.key]) || ""}
                    value={pocket.value || 0}
                    onChange={e => onChangeNewExpenses(e, i, "addTotal")}
                    disabled={disabled}
                    decimalScale={4}
                  />
                  <span
                    className="has-text-link is-size-5 is-pulled-right"
                    onClick={() => props.onRemove(i)}
                  >
                    <i className="far fa-trash-alt"/>
                  </span>
                </div>
              </div>
            ))
            : null}
          <hr/>
          <div className="columns">
            <div className="column">
               <p className="multiExpLblBlk">
                Total Of Computed Expenses / Revenue
              </p>
            </div>
            <div className="column">
              <small className="has-text-grey">
                {props.state && props.state.computedExpensesRevenueTotal ?
                  `$${Number(props.state.computedExpensesRevenueTotal).toLocaleString()}` : ("-")}
              </small>
            </div>
            <div className="column">
              <span className="multiExpLblBlk">Grand Total&nbsp;&nbsp;&nbsp;</span>
              <small className="has-text-grey">
                {props.state && props.state.grandTotal ?
                  `$${Number(props.state.grandTotal).toLocaleString()}` : ("-")}
              </small>
            </div>
          </div>
          {/* <div className="columns">
            <div className="column">
              <p className="multiExpLblBlk">Grand Total</p>
            </div>
            <div className="column"/>
            <div className="column">
              <small className="has-text-grey">
                ${props.state && props.state.grandTotal ?
                  Number(props.state.grandTotal).toLocaleString() : ("-")}
              </small>
            </div>
          </div> */}
          <div className="columns">
            <div className="column">
              <p className="multiExpLblBlk">
                Total Out-of-pocket or Additional Expenses
              </p>
            </div>
            <div className="column">
              <small className="has-text-grey">
                {props.state && props.state.totalOutOfPocketExpenses ?
                  `$${Number(props.state.totalOutOfPocketExpenses).toLocaleString()}` : ("-")}
              </small>
            </div>
            <div className="column"/>
          </div>
          <div className="columns">
            <div className="column is-full">
              <p className="multiExpLbl">Notes/Comments</p>
              <div className="control">
                <textarea
                  className="textarea"
                  name="notes"
                  value={props.state.notes || ""}
                  disabled={disabled}
                  onChange={onTextChange}
                />
              </div>
            </div>
          </div>

          <section className="container has-text-centered">
            <p className="title innerPgTitle">Bank Details</p>
            <br/>
          </section>
          <div className="columns">
            <TextLabelInput
              name="bankName"
              error={errors.bankName || ""}
              label={<span>Bank Name <span className="icon has-text-danger"><i
                className="fas fa-asterisk extra-small-icon"/></span></span>}
              placeholder="Name"
              value={props.state.bankDetails.bankName || ""}
              onChange={e => onTextChange(e, "bankDetails")}
              disabled={disabled}
            />
            <TextLabelInput
              name="ABA"
              error={errors.ABA || ""}
              label={<span>ABA# <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span></span>}
              placeholder="ABA"
              value={props.state.bankDetails.ABA || ""}
              onChange={e => onTextChange(e, "bankDetails")}
              disabled={disabled}
            />
          </div>
          <div className="columns">
            <TextLabelInput
              name="addressLine1"
              error={errors.addressLine1 || ""}
              label={<span>Address Line1 <span className="icon has-text-danger"><i
                className="fas fa-asterisk extra-small-icon"/></span></span>}
              placeholder="Address"
              value={props.state.bankDetails.addressLine1 || ""}
              onChange={e => onTextChange(e, "bankDetails")}
              disabled={disabled}
            />
            <TextLabelInput
              name="addressLine2"
              error={errors.addressLine2 || ""}
              label="Address Line2"
              placeholder="Address"
              value={props.state.bankDetails.addressLine2 || ""}
              onChange={e => onTextChange(e, "bankDetails")}
              disabled={disabled}
            />
          </div>

          <section className="container has-text-centered">
            <p className="title innerPgTitle" style={{ color: "black" }}>
              ACH Deposits Use ABA#: TBD
            </p>
            <br/>
          </section>
          <div className="columns">
            <TextLabelBox
              label="For the account of"
              error={errors.accountOf || ""}
              value={(props.state.bankDetails && props.state.bankDetails.accountOf) || ""}
            />
            {/* <TextLabelInput name="accountOf" label="For the account of" placeholder="Name" value={props.state.bankDetails.accountOf || ""} onChange={(e) => onTextChange(e, "bankDetails")} /> */}
            <div className="column">
              <p className="multiExpLbl">
                <span>Account No.<span className="icon has-text-danger">
                  <i className="fas fa-asterisk extra-small-icon"/></span>
                </span>
              </p>
              <div className="control">
                <NumberFormat
                  title="Account No."
                  className="input is-small is-link"
                  name="accountNo"
                  disabled={disabled}
                  value={(props.state.bankDetails.accountNo && parseInt(props.state.bankDetails.accountNo || 0, 10)) || ""}
                  placeholder="Account No."
                  onChange={e => onTextChange(e, "bankDetails")}
                />
                {errors.accountNo && (
                  <p
                    className="is-small text-error has-text-danger "
                    style={{ fontSize: 12 }}
                  >
                    {errors.accountNo}
                  </p>
                )}
              </div>
            </div>
          </div>

          <section className="container has-text-centered">
            <p className="title innerPgTitle" style={{ color: "black" }}>
              Re:{" "}
              {(props.state.activity &&
                props.state.activity.activityClientName) ||
              ""}{" "}
              &{" "}
              {(props.state.activity &&
                props.state.activity.activityDescription) ||
              ""}
            </p>
            <br/>
          </section>
          <div className="columns">
            <TextLabelInput
              name="attention"
              label={<span>Bank Contact Name<span className="icon has-text-danger"><i
                className="fas fa-asterisk extra-small-icon"/></span></span>}
              error={errors.attention || ""}
              placeholder="Name"
              value={props.state.bankDetails.attention || ""}
              onChange={e => onTextChange(e, "bankDetails")}
              disabled={disabled}
            />
            <div className="column">
              <p className="multiExpLbl">
                <span>Telephone<span className="icon has-text-danger">
                  <i className="fas fa-asterisk extra-small-icon"/></span>
                </span>
              </p>
              <div className="control">
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  mask="_"
                  className="input is-small is-link"
                  name="telephone"
                  disabled={disabled}
                  value={props.state.bankDetails.telephone || ""}
                  placeholder="Phone"
                  onChange={e => onTextChange(e, "bankDetails")}
                /> */}
                <PhoneInput
                  value={ props.state.bankDetails.telephone || "" }
                  disabled={disabled}
                  onChange={e => onTextChange(e, "telephone")}
                />
                {errors.telephone && (
                  <p
                    className="is-small text-error has-text-danger "
                    style={{ fontSize: 12 }}
                  >
                    {errors.telephone}
                  </p>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </Modal>
  )
}

export default CreateInvoiceModal
