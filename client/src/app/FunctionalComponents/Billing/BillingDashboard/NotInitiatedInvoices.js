import React from "react";
import { Link } from "react-router-dom";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { numberWithCommas, transactionUrl } from "GlobalUtils/helpers";

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  );
  try {
    return string.replace(reg, str => `<mark>${str}</mark>`);
  } catch (ex) {
    return string
  }
};

const NotInitiatedInvoices = ({
  invoicesList,
  onAddInvoice,
  pageSizeOptions = [5, 10, 20, 50, 100],
  search
  }) => {
    const columns = [
    {
      Header: "Client Name",
      id: "activityClientName",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value;
        return (
          <div className="hpTablesTd wrap-cell-text">
            <Link
              to={`/clients-propects/${item.activityTranClientId || ""}/entity` || ""}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.activityClientName || "-", search)
              }}
            />
          </div>
        );
      },
      sortMethod: (a, b) => a.activityClientName.localeCompare(b.activityClientName)
    }, {
      Header: "Transaction Name",
      id: "activityDescription",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value;
        return (
          <div className="hpTablesTd wrap-cell-text">
            {transactionUrl(item.activityTranType, item.activityTranSubType, item.activityDescription, item.activityId, search)}
          </div>
        );
      },
      sortMethod: (a, b) => a.activityTranType.localeCompare(b.activityTranType)
    }, {
      Header: "Type",
      id: "activityTranType",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value;
        return (
          <div className="hpTablesTd wrap-cell-text">
            <small>
              {`${item.activityTranType || ""} / ${item.activityTranSubType || ""}`}
            </small>
          </div>
        );
      },
      sortMethod: (a, b) => a.activityTranType.localeCompare(b.activityTranType)
    },
    {
      Header: "Action",
      id: "action",
      accessor: item => item,
      Cell: row => {
        const item = row.value;
        return (
          <div className="control">
            <button
              className="button is-link is-small is-fullwidth"
              onClick={() => onAddInvoice(item)}
            >
              Add Invoice
            </button>
          </div>
        );
      }
    }
  ];

    return (
      <div className="column">
        <ReactTable
          columns={columns}
          data={invoicesList}
          showPaginationBottom
          defaultPageSize={10}
          pageSizeOptions={pageSizeOptions}
          className="-striped -highlight is-bordered"
          style={{ overflowX: "auto" }}
          showPageJump
          minRows={2}
        />
      </div>
    );
};

export default NotInitiatedInvoices;
