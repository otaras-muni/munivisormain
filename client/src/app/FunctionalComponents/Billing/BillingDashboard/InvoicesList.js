import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import "react-table/react-table.css"
import { numberWithCommas, transactionUrl } from "GlobalUtils/helpers"
import { SelectLabelInput } from "../../../GlobalComponents/TextViewBox"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const InvoicesList = ({
  invoicesList,
  pageSizeOptions = [5, 10, 20, 50, 100],
  search,
  onEditInvoice,
  onInvoiceDownload,
  invoiceAction,
  handleInvoiceStatusChange,
  invoiceStatus
}) => {
  const disableValue = ["Invoice Paid", "Invoice Sent to Client", "Sent for review"]
  const columns = [
    {
      Header: "Invoice",
      id: "invoiceNumber",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <Link
              to={`/tools-billing/billing-expensedetail?invoice=${item._id}`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.invoiceNumber || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.invoiceNumber.localeCompare(b.invoiceNumber)
    },
    {
      id: "activityClientName",
      Header: "Client Name",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <Link
              to={`/clients-propects/${item.activityTranClientId || ""}/entity`}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.activityClientName || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.activityClientName.localeCompare(b.activityClientName)
    },
    {
      id: "activityDescription",
      Header: "Transaction Name",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return transactionUrl(item.activityTranType, item.activityTranSubType, item.activityDescription, item.activityId, search)
      },
      sortMethod: (a, b) =>
        a.activityDescription.localeCompare(b.activityDescription)
    },
    /* {
      id: "totalFinancialAdvisoryFees",
      Header: ()=>(<span title='Financial Advisory Services'>Financial Advisory Services</span>),
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            ${item.totalFinancialAdvisoryFees}
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.totalFinancialAdvisoryFees - b.totalFinancialAdvisoryFees
    },
    {
      id: "totalOutOfPocketExpenses",
      Header: ()=>(<span title='Out-of-Pocket Expenses'>Out-of-Pocket Expenses</span>),
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            ${(item && item.totalOutOfPocketExpenses && parseFloat(item.totalOutOfPocketExpenses.toFixed(4))) || 0}
          </div>
        )
      },
      sortMethod: (a, b) =>
        a.totalOutOfPocketExpenses - b.totalOutOfPocketExpenses
    }, */
    {
      id: "totalOverallExpenses",
      Header: "Grand Total",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            {(item && item.totalOverallExpenses) ? `$${Number(item.totalOverallExpenses).toLocaleString()}` : "-"}
          </div>
        )
      },
      sortMethod: (a, b) => a.totalOverallExpenses - b.totalOverallExpenses
    },
    {
      id: "action",
      Header: "Action",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const disable = ["Invoice Paid", "Invoice Sent to Client", "Sent for review"]
        const disableValue = disable.indexOf((invoiceAction && invoiceAction[item._id])) > -1
        return (
          <SelectLabelInput
            list={invoiceStatus || []}
            name="invoiceStatus"
            value={(invoiceAction && invoiceAction[item._id]) || ""}
            disabled={invoiceAction && invoiceAction[item._id] === "Invoice Paid" || false}
            disableValue={disableValue ? ["In Progress", "Invoice Generated"] : []}
            onChange={e => handleInvoiceStatusChange(e, item._id)}
          />
        )
      },
      sortMethod: (a, b) => a.invoiceStatus.localeCompare(b.invoiceStatus)
    },
    {
      id: "edit",
      Header: "Edit",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const disable = disableValue.indexOf((invoiceAction && invoiceAction[item._id])) > -1
        return (
          <div className="field is-grouped">
            {disable ?
              <div className="control">
                <a onClick={() => onEditInvoice(item)}>
                  <span className="has-text-link">
                    <i className="fas fa-eye" title="View Invoice"/>
                  </span>
                </a>
              </div> :
              <div className="control">
                <a onClick={() => onEditInvoice(item)}>
                  <span className="has-text-link">
                    <i className="fas fa-pencil-alt" title="Edit Invoice"/>
                  </span>
                </a>
              </div>
            }
          </div>
        )
      }
    },
    {
      id: "download",
      Header: "Download",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="field is-grouped">
            <div className="control">
              <a onClick={() => onInvoiceDownload(item)}>
                <span className="has-text-link">
                  <i className="fas fa-download" title="Download Invoice"/>
                </span>
              </a>
            </div>
          </div>
        )
      }
    }
  ]
  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={invoicesList}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default InvoicesList
