import React, { Component } from "react"
import { toast } from "react-toastify"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import { DropdownList } from "react-widgets"
import swal from "sweetalert"
import CONST from "GlobalUtils/consts"
import { getPicklistByPicklistName, transactionUrl } from "GlobalUtils/helpers"
import { pdfInvoiceDownload } from "GlobalUtils/pdfutils"
import {
  searchInvoiceDashboard,
  postInvoiceStatusUpdateFromDashboard,
  fetchAllTransactionForBilling,
  createInvoice,
  getBillingPocketExpense
} from "../../../StateManagement/actions"
import CreateInvoiceModal from "./CreateInvoiceModal"
import Loader from "../../../GlobalComponents/Loader"
import { BillingInvoiceValidate } from "../Validation/BillingInvoiceValidate"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Accordion from "../../../GlobalComponents/Accordion"
import InvoicesList from "./InvoicesList"
import NotInitiatedInvoices from "./NotInitiatedInvoices"
import { fetchIssuers } from "../../../StateManagement/actions/CreateTransaction"

const invoiceDetails = {
  invoiceId: "",
  notes: "",
  outOfPocketExpenses: {},
  consultingCharge: {},
  totalOutOfPocketExpenses: 0,
  totalFinancialAdvisoryFees: 0,
  totalConsultingEngFees: 0,
  invoiceNumber: "",
  grandTotal: 0,
  activity: {},
  disableInvoice: false,
  bankDetails: {
    bankName: "",
    addressLine1: "",
    addressLine2: "",
    ABA: "",
    accountOf: "",
    accountNo: "",
    attention: "",
    telephone: ""
  }
}

class BillingDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pocketExpenses: [],
      searchTerm: "",
      ...cloneDeep(invoiceDetails),
      selectedInvoice: {},
      selectedEntity: {},
      errorMessages: {},
      invoiceAction: null,
      searchResults: [],
      transactions: [],
      issuerList: [],
      invoices: [],
      modalVisible: false,
      loading: true,
      newExpenses: [],
      roleType: [],
      pocketExpense: {},
      computedConsultingCharge: {},
      computedFinancialAdvisoryFees: 0,
      confirmAlert: CONST.confirmAlert
    }
  }

  async componentDidMount() {
    const { user } = this.props
    const picResult = await getPicklistByPicklistName([
      "LKUPEXPENSETYPES",
      "LKUPROLETYPES",
      "LKUPINVOICESTATUS"
    ])
    const result = (picResult.length && picResult[1]) || {}
    // const entities = await getEligibleEntitiesForLoggedInUser()
    this.getEntities()
    await searchInvoiceDashboard(
      "",
      res => {
        if (res && Array.isArray(res)) {
          console.log("CMD", res)
          const reInitiateInvoiceState = (res || []).reduce(
            (acc, b) => ({ ...acc, [b._id]: b.invoiceStatus || "" }),
            {}
          )
          this.setState(prevState => ({
            searchResults: res,
            invoiceAction: reInitiateInvoiceState,
            loading: false,
            // issuerList: entities || [],
            pocketExpenses: (result && result.LKUPEXPENSETYPES && result.LKUPEXPENSETYPES.filter(f => f.included).map(e => e.label)) || [],
            roleType: (result && result.LKUPROLETYPES && result.LKUPROLETYPES.filter(f => f.included).map(e => e.label)) || [],
            invoiceStatus: (result && result.LKUPINVOICESTATUS) || [],
            bankDetails: {
              ...prevState.bankDetails,
              accountOf: (user && user.firmName) || ""
            }
          }))
        }
      },
      this.props.token
    )
  }

  manageSearchTermChange = e => {
    this.setState(
      {
        searchTerm: e.target.value
      },
      () => this.onSearchInvoice()
    )
  }

  getEntities = () => {
    fetchIssuers(this.props.user.entityId, (res) => {
      this.setState({
        issuerList: res.issuerList || [],
      })
    })
  }

  onSearchInvoice = async () => {
    if (this.state.searchTerm.length >= 0) {
      await searchInvoiceDashboard(
        this.state.searchTerm,
        res => {
          const reInitiateInvoiceState = res.reduce(
            (acc, b) => ({ ...acc, [b._id]: b.invoiceStatus || "" }),
            {}
          )
          this.setState(ps => ({
            ...ps,
            searchResults: res,
            invoiceAction: reInitiateInvoiceState
          }))
        },
        this.props.token
      )
    } else {
      this.setState({ searchResults: [], invoiceAction: {} })
    }
  }

  onModalChange = (state, flag) => {
    this.setState(
      {
        ...state,
        errorMessages: {},
      },
      () => {
        if (flag === "addTotal") {
          this.expensesCount()
        }
      }
    )
  }

  expensesCount = () => {
    const outOfPocketExpenses = cloneDeep(this.state.outOfPocketExpenses)
    const {
      newExpenses,
      totalFinancialAdvisoryFees,
      consultingCharge
    } = this.state
    let totalOutOfPocketExpenses = 0
    let totalConsultingCharge = 0

    newExpenses.forEach(newEx => {
      outOfPocketExpenses[newEx.key] = newEx.value
    })
    Object.keys(outOfPocketExpenses || {}).forEach(key => {
      totalOutOfPocketExpenses += parseFloat(outOfPocketExpenses[key] || 0)
    })

    Object.keys(consultingCharge || {}).forEach(key => {
      totalConsultingCharge += parseFloat(consultingCharge[key] || 0)
    })

    const grandTotal =
      totalOutOfPocketExpenses +
      parseFloat(totalFinancialAdvisoryFees || 0) +
      parseFloat(totalConsultingCharge || 0)
    this.setState({
      totalOutOfPocketExpenses,
      grandTotal,
      totalConsultingEngFees: totalConsultingCharge
    }, () => {
      let computedExpensesRevenueTotal = null
      if(this.state.computedFinancialAdvisoryFees){
        computedExpensesRevenueTotal = computedExpensesRevenueTotal + (this.state.computedFinancialAdvisoryFees || 0)
      }
      if(Object.keys(this.state.computedConsultingCharge).length){
        this.state.roleType.forEach(role => {
          if(this.state.computedConsultingCharge[role]){
            computedExpensesRevenueTotal = computedExpensesRevenueTotal + (this.state.computedConsultingCharge[role] || 0)
          }
        })
      }
      if(Object.keys(this.state.pocketExpense).length){
        this.state.pocketExpenses.forEach(p => {
          if(this.state.pocketExpense[p]){
            computedExpensesRevenueTotal = computedExpensesRevenueTotal + (this.state.pocketExpense[p] || 0)
          }
        })
      }
      this.setState({
        computedExpensesRevenueTotal
      })
    })
  }

  onSave = async () => {
    const {
      totalOutOfPocketExpenses,
      invoiceId,
      newExpenses,
      consultingCharge,
      invoiceNumber,
      grandTotal,
      activity,
      totalFinancialAdvisoryFees,
      totalConsultingEngFees,
      bankDetails,
      notes
    } = this.state
    const outOfPocketExpenses = cloneDeep(this.state.outOfPocketExpenses)
    newExpenses.forEach(newEx => {
      outOfPocketExpenses[newEx.key] = newEx.value
    })

    const payload = {
      totalOutOfPocketExpenses,
      totalOverallExpenses: grandTotal,
      totalFinancialAdvisoryFees,
      totalConsultingEngFees,
      outOfPocketExpenses,
      consultingCharge,
      invoiceNumber: invoiceId
        ? invoiceNumber
        : `INVOICE-${Math.random()
          .toString(36)
          .toUpperCase()
          .substring(2, 17)}`,
      ...activity,
      invoiceNotes: notes,
      bankDetails
    }
    console.log(payload)

    const errors = BillingInvoiceValidate(payload)
    const errorMessages = {}

    if(payload && payload.outOfPocketExpenses && Object.keys(payload.outOfPocketExpenses).length) {
      Object.keys(payload.outOfPocketExpenses).forEach(key => {
        if(payload.outOfPocketExpenses[key] && parseInt(payload.outOfPocketExpenses[key], 10) < 0){
          errorMessages.outOfPocketExpenses = {
            ...errorMessages.outOfPocketExpenses,
            [key]: `${key} must be larger than 0`
          }
        }
      })
    }
    if(payload && payload.consultingCharge && Object.keys(payload.consultingCharge).length) {
      Object.keys(payload.consultingCharge).forEach(key => {
        if(payload.consultingCharge[key] && parseInt(payload.consultingCharge[key], 10) < 0){
          errorMessages.consultingCharge = {
            ...errorMessages.consultingCharge,
            [key]: `${key} must be larger than 0`
          }
        }
      })
    }

    if (errors && errors.error) {
      console.log("=======>", errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = err.message
      })
    }

    if(Object.keys(errorMessages).length){
      console.log("==errorMessages====>", errorMessages)
      return this.setState({ errorMessages })
    }

    if (invoiceId) payload.invoiceId = invoiceId

    const transactions = this.state.transactions.filter(
      tran => tran.activityId !== activity.activityId
    )
    payload.invoiceStatus = "In Progress"
    const resInvoice = await createInvoice(payload)

    this.setState(
      {
        loading: true,
        modalVisible: false
      },
      () => {
        if (resInvoice && resInvoice.status === 200) {
          console.log(resInvoice.data)
          if(payload.invoiceId){
            toast("Invoice Updated Successfully.", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
          }else {
            toast("Invoice Created Successfully.", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
          }

          this.setState(
            {
              loading: false,
              transactions,
              errorMessages: {},
              newExpenses: [],
              ...cloneDeep(invoiceDetails)
            },
            () => this.onSearchInvoice()
          )
        } else {
          console.log(resInvoice.error)

          toast((resInvoice && resInvoice.message) || "Something went wrong, Please try a later", {
            autoClose: CONST.ToastTimeout,
            type: toast.TYPE.ERROR
          })

          this.setState({
            loading: false,
            errorMessages: {},
            newExpenses: [],
            ...cloneDeep(invoiceDetails)
          })
        }
      }
    )
  }

  redirectToTarget = () => {
    this.props.history.push("/tools-billing/billing-expensedetail")
  }

  handleInvoiceStatusChange = (e, invoiceId) => {
    const { confirmAlert } = this.state
    const val = e.target.value
    confirmAlert.text = `You want to this invoice status change to ${val}?`
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        if (val) {
          this.setState(
            ps => ({
              ...ps,
              invoiceAction: { ...ps.invoiceAction, [invoiceId]: val }
            }),
            () =>
              postInvoiceStatusUpdateFromDashboard(
                { invoiceid: invoiceId, status: val },
                data => {
                  toast(
                    `Updated status of Invoice - ${data.invoiceNumber} to ${val}`,
                    { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
                  )
                  this.setState({ selectedInvoice: data })
                },
                this.props.token
              )
          )
        }
      }
    })
  }

  onEditInvoice = invoice => {
    const { pocketExpenses, invoiceAction } = this.state
    const { user } = this.props
    const disableValue = ["Invoice Paid", "Invoice Sent to Client", "Sent for review"]
    const disableInvoice = disableValue.indexOf(invoiceAction[invoice && invoice._id]) > -1
    if (invoice && invoice.activityId) {
      this.setState(
        {
          modalLoading: true,
          loading: true
        },
        async () => {
          const query = `?activityId=${invoice.activityId}&type=${
            invoice.activityTranType
          }&subType=${invoice.activityTranSubType}`
          const pocketExpense = await getBillingPocketExpense(query)
          const newExpenses = []
          Object.keys(invoice.outOfPocketExpenses || {}).forEach(key => {
            if (pocketExpenses.indexOf(key) === -1) {
              newExpenses.push({
                key,
                value: invoice.outOfPocketExpenses[key]
              })
            }
          })
          this.setState(
            {
              ...cloneDeep(invoiceDetails),
              bankDetails: invoice.bankDetails || {
                accountOf: (user && user.firmName) || ""
              },
              invoiceNumber: invoice.invoiceNumber || "",
              notes: invoice.invoiceNotes || "",
              invoiceId: invoice._id || "",
              activity: {
                activityId: invoice.activityId || "",
                activityDescription: invoice.activityDescription || "",
                activityTranType: invoice.activityTranType || "",
                activityTranSubType: invoice.activityTranSubType || "",
                activityTranFirmId: invoice.activityTranFirmId || "",
                activityTranFirmName: invoice.activityTranFirmName || "",
                activityTranClientId: invoice.activityTranClientId || "",
                activityClientName: invoice.activityClientName || "",
                activityClientAddress: invoice.activityClientAddress || {},
                activityParAmount: parseInt(
                  pocketExpense.activityParAmount || 0,
                  10
                )
              },
              outOfPocketExpenses: invoice.outOfPocketExpenses || {},
              totalOutOfPocketExpenses: invoice.totalOutOfPocketExpenses || 0,
              totalFinancialAdvisoryFees:
                invoice.totalFinancialAdvisoryFees || 0,
              totalConsultingEngFees: invoice.totalConsultingEngFees || 0,
              grandTotal: invoice.totalOverallExpenses || 0,
              pocketExpense: (pocketExpense && pocketExpense.pocketCount) || {},
              consultingCharge: (invoice && invoice.consultingCharge) || {},
              newExpenses,
              computedConsultingCharge: (pocketExpense && pocketExpense.timeTrackerCount) || {},
              computedFinancialAdvisoryFees: (pocketExpense && pocketExpense.totalFinancialAdvisoryFees) || 0,
              disableInvoice,
              loading: false
            },
            () => {
              this.setState({
                modalVisible: true,
                modalLoading: false
              })
              this.expensesCount()
            }
          )
        }
      )
    }
  }

  onAddInvoice = activity => {
    const { user } = this.props
    if (activity && activity.activityId) {
      this.setState(
        {
          loading: true
        },
        async () => {
          const query = `?activityId=${activity.activityId}&type=${
            activity.activityTranType
          }&subType=${activity.activityTranSubType}`
          const pocketExpense = await getBillingPocketExpense(query)
          this.setState(
            {
              ...cloneDeep(invoiceDetails),
              outOfPocketExpenses: {
                ...pocketExpense.pocketCount
              },
              consultingCharge: {
                ...pocketExpense.timeTrackerCount
              },
              pocketExpense: (pocketExpense && pocketExpense.pocketCount) || {},
              computedConsultingCharge:
                (pocketExpense && pocketExpense.timeTrackerCount) || {},
              activity: {
                ...activity,
                activityParAmount:
                  (pocketExpense && pocketExpense.activityParAmount) || 0
              },
              bankDetails: {
                ...cloneDeep(invoiceDetails.bankDetails),
                accountOf: (user && user.firmName) || ""
              },
              newExpenses: [],
              computedFinancialAdvisoryFees:
                (pocketExpense && pocketExpense.totalFinancialAdvisoryFees) || 0
            },
            () => {
              this.setState({
                modalVisible: true,
                loading: false
              })
              this.expensesCount()
            }
          )
        }
      )
    }
  }

  onAddNewExpenses = () => {
    const { newExpenses } = this.state
    newExpenses.push({
      key: "",
      value: ""
    })
    this.setState({ newExpenses })
  }

  onRemoveExpenses = index => {
    const { newExpenses } = this.state
    newExpenses.splice(index, 1)
    this.setState({ newExpenses }, () => this.expensesCount())
  }

  onIssuerChange = item => {
    this.setState(
      {
        selectedEntity: {
          firmId: item.id,
          firmName: item.firmName
        },
        // loading: true
      },
      async () => {
        const tranInvoiceRes = await fetchAllTransactionForBilling(
          this.state.selectedEntity.firmId
        )
        this.setState({
          searchResults: (tranInvoiceRes && tranInvoiceRes.invoices) || [],
          transactions: (tranInvoiceRes && tranInvoiceRes.transactions) || [],
          invoices: (tranInvoiceRes && tranInvoiceRes.invoices) || [],
          // loading: false
        })
      }
    )
  }

  onInvoiceDownload = invoice => {
    const { user } = this.state
    if (invoice && invoice.activityId) {
      this.setState(
        {
          loading: true
        },
        async () => {
          const query = `?activityId=${invoice.activityId}&type=${
            invoice.activityTranType
          }&subType=${invoice.activityTranSubType}`
          const pocketExpense = await getBillingPocketExpense(query)
          const pocketCount = (pocketExpense && pocketExpense.pocketCount) || {}
          const consultingCharge =
            (pocketExpense && pocketExpense.timeTrackerCount) || {}
          this.setState(
            {
              loading: false
            },
            () => {
              pdfInvoiceDownload(
                {
                  ...invoice,
                  pocketCount,
                  activityParAmount:
                    (pocketExpense && pocketExpense.activityParAmount) || 0
                },
                "invoice.pdf"
              )
            }
          )
        }
      )
    }
  }

  onArrowClick = () => {
    const {
      pocketExpense,
      computedConsultingCharge,
      computedFinancialAdvisoryFees,
      totalFinancialAdvisoryFees
    } = this.state
    this.setState(
      prevState => ({
        outOfPocketExpenses: {
          ...prevState.outOfPocketExpenses,
          ...pocketExpense
        },
        consultingCharge: {
          ...prevState.consultingCharge,
          ...computedConsultingCharge
        },
        totalFinancialAdvisoryFees: !computedFinancialAdvisoryFees ? totalFinancialAdvisoryFees : computedFinancialAdvisoryFees
      }),
      () => this.expensesCount()
    )
  }

  render() {
    const {
      transactions,
      loading,
      searchResults,
      searchTerm,
      issuerList,
      selectedEntity,
      pocketExpenses,
      consultingCharge,
      invoiceAction,
      invoiceStatus
    } = this.state
    const { user } = this.props

    if (loading) {
      return <Loader />
    }
    return (
      <div id="main">
        <CreateInvoiceModal
          state={this.state}
          user={user}
          pocketExpenses={pocketExpenses}
          consultingCharge={consultingCharge}
          onModalChange={this.onModalChange}
          onSave={this.onSave}
          onAdd={this.onAddNewExpenses}
          onRemove={this.onRemoveExpenses}
          onClick={this.onArrowClick}
        />
        <div className="columns">
          <div className="column">
            <div className="control">
              <button
                className="button is-link is-pulled-right"
                onClick={() => this.redirectToTarget()}
              >
                Add New Expense Details
              </button>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <p className="control has-icons-left">
              <input
                className="input is-small is-link"
                type="text"
                placeholder="search & pick client name"
                value={searchTerm}
                onChange={e => this.manageSearchTermChange(e)}
              />
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
        </div>
        <Accordion
          multiple
          activeItem={[0, 1]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Transactions with Invoices"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    <InvoicesList
                      invoicesList={searchResults}
                      search={searchTerm}
                      invoiceStatus={invoiceStatus}
                      onEditInvoice={this.onEditInvoice}
                      onInvoiceDownload={this.onInvoiceDownload}
                      invoiceAction={invoiceAction}
                      handleInvoiceStatusChange={this.handleInvoiceStatusChange}
                    />
                  </div>
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Transactions without Invoices"
              >
                {activeAccordions.includes(1) && (
                  <div>
                    <div className="column">
                      <p className="multiExpTblVal">Entity Name</p>
                      <DropdownList
                        dropUp
                        filter
                        value={selectedEntity.firmId || ""}
                        data={issuerList || []}
                        textField="name"
                        valueField="id"
                        key="name"
                        groupBy={({ relType }) => relType}
                        onChange={this.onIssuerChange}
                      />
                    </div>
                    <NotInitiatedInvoices
                      invoicesList={transactions}
                      search={searchTerm}
                      onAddInvoice={this.onAddInvoice}
                    />
                  </div>
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userInfo: auth.userEntities,
  user: (auth && auth.userEntities) || {},
  token: auth.token,
})

export default connect(
  mapStateToProps,
  null
)(BillingDashboard)
