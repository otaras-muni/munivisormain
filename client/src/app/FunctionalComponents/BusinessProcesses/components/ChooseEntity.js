import React from "react"
import { Link } from "react-router-dom"

const ChooseEntity = () => (
  <div id="main" style={{ paddingTop: 45 }}>
    <div className="box choose-entity">
      <section className="container">
        <div className="tile is-ancestor">

          <div className="tile is-parent"><Link to="/addnew-client">
            <article className="tile is-child box has-text-centered section-padding-0">
              <p className="subtitle section-subtitle">Create a record for an entity / organization that you currently do business with.
              </p>
              <p className="title mgmtConTitle section-link section-btn-blue">Client</p>
            </article>
          </Link></div>
          <div className="tile is-parent"><Link to="/addnew-prospect">
            <article className="tile is-child box has-text-centered section-padding-0">
              <p className="subtitle section-subtitle">Create a record for an entity / organization that you expect to do business with.
              </p>
              <p className="title mgmtConTitle section-link  section-btn-orange">Prospect</p>
            </article>
          </Link></div>
          <div className="tile is-parent"><Link to="/addnew-thirdparty">
            <article className="tile is-child box has-text-centered section-padding-0">
              <p className="subtitle section-subtitle">Create a record for a 3rd party entity / organization like underwriter, bond counsel, etc.
              </p>
              <p className="title mgmtConTitle section-link section-btn-orange">3rd Party</p>
            </article>
          </Link></div>
        </div>
      </section>
    </div>
  </div>
)

export default ChooseEntity
