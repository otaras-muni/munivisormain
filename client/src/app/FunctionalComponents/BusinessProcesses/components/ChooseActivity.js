import React, { Component } from "react"
import { Link } from "react-router-dom"

export default class ChooseActivity extends Component {
  state = {}
  render() {
    return (
      <div id="main" style={{ paddingTop: 45 }}>
        <div className="box">
          <section className="container">
            <div className="tile is-ancestor">
              <div className="tile is-parent" style={{ marginBottom: 15 }}>
                <Link to="/createTran/debt">
                  <article className="tile is-child box has-text-centered section-padding-0" style={{ height: "100%" }}>
                    <p className="subtitle section-subtitle" style={{ height: "80%" }}>Record to manage activity related to Bond Issue, Bank Loan, Line or Letter of Credit.
                </p>
                    <p className="title mgmtConTitle section-link section-btn-blue">  Debt</p>
                  </article>
                </Link>
              </div>
              <div className="tile is-parent" style={{ marginBottom: 15 }}>
                <Link to="/createTran/derivative">
                  <article className="tile is-child box has-text-centered section-padding-0" style={{ height: "100%" }}>
                    <p className="subtitle section-subtitle" style={{ height: "80%" }}>Record to manage activity related to a Swap, Cap or similar.
                </p>
                    <p className="title mgmtConTitle section-link section-btn-blue">Derivative</p>
                  </article>
                </Link>
              </div>
              <div className="tile is-parent" style={{ marginBottom: 15 }}>
                <Link to="/createTran/marfp">
                  <article className="tile is-child box has-text-centered section-padding-0" style={{ height: "100%" }}>
                    <p className="subtitle section-subtitle" style={{ height: "80%" }}>Record to manage activity related to your firm's response to a MA RFP.
                </p>

                    <p className="title mgmtConTitle section-link section-btn-orange">MA-RFP</p>
                  </article>
                </Link>
              </div>
            </div>
            <div className="tile is-ancestor">
              <div className="tile is-parent" style={{ marginBottom: 15 }}>
                <Link to="/createTran/others">
                  <article className="tile is-child box has-text-centered section-padding-0" style={{ height: "100%" }}>
                    <p className="subtitle section-subtitle" style={{ height: "80%" }}>Record to manage activity related to rating presentation, referendum consulting or similar.
                </p>
                    <p className="title mgmtConTitle section-link section-btn-blue">Other Activity</p>
                  </article>
                </Link>
              </div>
              <div className="tile is-parent" style={{ marginBottom: 15 }}>
                <Link to="/createTran/rfp">
                  <article className="tile is-child box has-text-centered section-padding-0" style={{ height: "100%" }}>
                    <p className="subtitle section-subtitle" style={{ height: "80%" }}>Record to manage activity related to client's RFP process.
                </p>
                    <p className="title mgmtConTitle section-link section-btn-blue">Manage Client RFP</p>
                  </article>
                </Link>
              </div>
              <div className="tile is-parent" style={{ marginBottom: 15 }}>
                <Link to="/createTran/businessDevelopment">
                  <article className="tile is-child box has-text-centered section-padding-0" style={{ height: "100%" }}>
                    <p className="subtitle section-subtitle" style={{ height: "80%" }}>Record to manage activity related to meetings, conferences, research, or similar.
                </p>
                    <p className="title mgmtConTitle section-link section-btn-orange">Business Development</p>
                  </article>
                </Link>
              </div>
            </div>
          </section>
        </div>
      </div>
    )
  }
}

