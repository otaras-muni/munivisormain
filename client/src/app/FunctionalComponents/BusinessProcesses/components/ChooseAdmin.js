import React from "react"
import { Link } from "react-router-dom"

const ManagementConsole = props => (
  <div id="main">
    <section className="container">
      <div className="tile is-ancestor">
        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-firms" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fas fa-7x fa-building" />
                <hr />
                <p className="title mgmtConTitle">My Firm</p>
              </div>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-users" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fas fa-7x fa-users" />
                <hr />
                <p className="title mgmtConTitle">Users</p>
              </div>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-cltprosp" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fas fa-7x fa-handshake" />
                <hr />
                <p className="title mgmtConTitle">Clients</p>
              </div>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-thirdparty" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fab fa-7x fa-keycdn" />
                <hr />
                <p className="title mgmtConTitle">3rd Party</p>
              </div>
            </Link>
          </article>
        </div>
      </div>

      <div className="tile is-ancestor">
        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/configuration" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fab fa-7x fa-whmcs" />
                <hr />
                <p className="title mgmtConTitle">Configuration</p>
              </div>
            </Link>
          </article>
        </div>

        <div className="tile is-parent">
          <article className="tile is-child box has-text-centered">
            <Link to="/admin-migtools" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fas fa-7x fa-wrench" />
                <hr />
                <p className="title mgmtConTitle">Migration Tools</p>
              </div>
            </Link>
          </article>
        </div>

        <div className="tile is-parent" disabled>
          <article className="tile is-child box has-text-centered" disabled>
            <div disabled>
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fas fa-7x fa-battery-full" />
                <hr />
                <p className="title mgmtConTitle">Usage</p>
              </div>
            </div>
          </article>
        </div>

        <div className="tile is-parent" >
          <article className="tile is-child box has-text-centered" >
            <Link to="/admin-billing" className="hover-orange">
              <div style={{ width: "100%", height: "100%" }}>
                <i className="fas fa-7x fa-credit-card" />
                <hr />
                <p className="title mgmtConTitle">Billing</p>
              </div>
            </Link>
          </article>
        </div>
      </div>
    </section>
  </div>
)

export default ManagementConsole
