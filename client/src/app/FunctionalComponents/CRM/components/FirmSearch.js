import React from 'react';
import { ReactiveBase, ReactiveList, TextField, SingleDataList, DateRange } from '@appbaseio/reactivesearch';
import {
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_INDEX,
  ELASTIC_SEARCH_TYPES,
  ELASTIC_SEARCH_CREDENTIALS,
} from '../../../../constants';
import '../scss/firmsearch.scss';
import moment from 'moment';

const FirmSearch = () => (
    <ReactiveBase
      app={ELASTIC_SEARCH_INDEX}
      url={ELASTIC_SEARCH_URL}
      type={ELASTIC_SEARCH_TYPES.TASKS}
      credentials={ELASTIC_SEARCH_CREDENTIALS}
    >
      <div className="searchcontainer">
        <div className="searchfilter">
          <b> Search Firms </b>
          <TextField
            dataField="title"
            componentId="SearchBox"
          />
          <div className="searchlist">
            <ReactiveList
              componentId="ResultCard"
              dataField="title"
              size={8}
              pagination={true}
              loader=""
              react={{
                and: ['SearchBox', 'StatusSensor', 'DateSensor']
              }}
              onData={(res, key) => (
                <div key={key} className="searchlist-item">
                  <div className="title-container">
                    { res.title }
                    <div className="due-date"> {moment(res.dueDate).format('DD-MMM-YYYY')} </div>
                  </div>
                </div>
              )}
              showResultStats={false}
            />
          </div>
        </div>
      </div>
    </ReactiveBase>
);

export default FirmSearch;
