import axios from "axios"

import {mapAccordionsArrayToObject, mapAccordionsObjectToArray} from "GlobalUtils/helpers"

import {GET_FRUITS, SAVE_FRUITS, REMOVE_FRUIT, GET_AUTH_USER} from "../types"
import { muniApiBaseURL } from "GlobalUtils/consts.js"

export const getFruits = () => async dispatch => {
  console.log("in action getFruits")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}test/`)
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data[0].fruits) {
      // eslint-disable-next-line no-unused-vars
      const { _id, fruits } = res.data[0]
      dispatch({
        type: GET_FRUITS,
        payload: mapAccordionsArrayToObject(fruits)
      })
    } else {
      console.log("no data in getFruits: ")
      dispatch({
        type: GET_FRUITS,
        payload: mapAccordionsArrayToObject([])
      })
    }
  } catch (err) {
    console.log("err in getFruits: ", err)
    dispatch({
      type: GET_FRUITS,
      payload: mapAccordionsArrayToObject([])
    })
  }
}

export const saveFruits = (fruits) => async dispatch => {
  console.log("in action saveFruits")
  const data = mapAccordionsObjectToArray(fruits)
  console.log("data : ", data)
  try {
    const res = await axios.get(`${muniApiBaseURL}test/`)
    console.log("res : ", res)
    // eslint-disable-next-line no-underscore-dangle
    if (res && res.status >= 200 && res.status < 300 && res.data && res.data[0]._id) {
      const {_id} = res.data[0]
      console.log("_id : ", _id)
      const res1 = await axios.put(`${muniApiBaseURL}test/${_id}`, {fruits: data})
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300) {
        dispatch({type: SAVE_FRUITS, payload: fruits})
      } else {
        console.log("err in saveFruits: ")
      }
    } else {
      console.log("no id in getFruits: ")
    }
  } catch (err) {
    console.log("err in getFruits: ", err)
  }
}

export const removeFruit = (key, index) => ({
  type: REMOVE_FRUIT,
  payload: {
    key,
    index
  }
})

export const getAuthUser = userId => {
  console.log("in action getAuthUser : ", userId)
  return {type: GET_AUTH_USER, payload: userId}
}

export const fakeLogout = () => {
  console.log("in action fakeLogout : ")
  return {type: GET_AUTH_USER, payload: ""}
}
