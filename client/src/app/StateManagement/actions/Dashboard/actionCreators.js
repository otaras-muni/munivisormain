import axios from "axios"
import * as actions from "./actions"
import { startFetchingSearchPref, finsihFetchingSearchPref, startSavingSearchPref, finsihSavingSearchPref} from "../SearchPref"
import { muniApiBaseURL } from "../../../../globalutilities/consts"

export const updateDashboardSearchPref = (data) => (dispatch, getState) => {
  const { dashboardSearchPref } = getState()
  const payload = { ...dashboardSearchPref, ...data}

  dispatch({
    type: actions.UPDATE_DASHBOARD_PREF,
    payload
  })
}

export const fetchDashboardSearchPrefs = () => (dispatch, getState) => {
  dispatch(startFetchingSearchPref())
  const { auth: { token }} = getState()
  axios({
    method: "GET",
    url: `${muniApiBaseURL}searchpref/context/dash-task`,
    headers: { Authorization: token },
  }).then(res => {
    dispatch(updateDashboardSearchPref(res.data.data.searchPreference))
    setTimeout(() => {
      dispatch(finsihFetchingSearchPref())
    },100)
  }).catch(() => {
    dispatch(finsihFetchingSearchPref())
  })
}

export const saveDashboardSearchPrefs = () => (dispatch, getState) => {
  const { dashboardSearchPref, auth: { token } } = getState()
  dispatch(startSavingSearchPref())
  return axios({
    method: "POST",
    url: `${muniApiBaseURL}searchpref/context/dash-task`,
    headers: { Authorization: token },
    data: dashboardSearchPref
  }).then(() => {
    dispatch(finsihSavingSearchPref())
  }).catch(() => {
    dispatch(finsihSavingSearchPref())
  })
}
