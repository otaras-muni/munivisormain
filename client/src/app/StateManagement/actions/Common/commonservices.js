import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {getToken} from "GlobalUtils/helpers"
import FileSaver from "file-saver"

export const getUrlsForIDs = async (ids) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}common/constructurlsforids`, {ids}, {headers:{"Authorization":getToken()}})
    const urlObject = response && response.data && response.data.urlObject
    console.log ("THE URL OBJECT IS", urlObject)
    return urlObject
  } catch (err) {
    return {"error":"There is an error in getting URL information"}
  }
}

export const postGeneralDocs = async (payload) => {
  try {
    return await axios.post(`${muniApiBaseURL}common/postgeneraldocs`, payload, {headers:{"Authorization":getToken()}})
  } catch (err) {
    return {error: err.message}
  }
}

export const postCheckedAddToDL = async (type, tranId, payload) => {
  try {
    return await axios.post(`${muniApiBaseURL}common/checkedaddtodl/${type}/${tranId}`, payload, {headers:{"Authorization":getToken()}})
  } catch (err) {
    return {error: err.message}
  }
}

export const fetchAllRevenuePipeline = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}billing/revenuepipeline`, payload, {headers:{"Authorization": getToken()}})
    if(res && res.status === 200){
      return res.data
    }
    return {}
  } catch (error) {
    console.log(error)
    return error
  }
}

export const fetchDMObject = async () => {
  try {
    const res = await axios.get(`${muniApiBaseURL}dmMapping/mappinginfo`, {headers:{"Authorization": getToken()}})
    if(res && res.status === 200){
      return res.data
    }
    return {}
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putDMMappingObject = async (type, currentPayload, newPayload) => {
  try {
    const mapObject = {
      ...currentPayload,
      [type]: newPayload
    }
    const res = await axios.post(`${muniApiBaseURL}dmMapping/mappinginfo`, {mapObject}, {headers:{"Authorization": getToken()}})
    if(res && res.status === 200 && res.data){
      return res.data && res.data.data
    }
    return {}
  } catch (error) {
    console.log(error)
    return error
  }
}

export const postJSONData = async (payload, label, returnBlob) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}common/jsontoblob`, payload, {responseType: "arraybuffer", headers:{"Authorization":getToken()}})
    const blob = new Blob([response.data])
    if(returnBlob){
      return blob
    }
    FileSaver.saveAs(blob, `${label ? `${label}.xlsx` : "sample.xlsx"}`)
  } catch (err) {
    return {error: err.message}
  }
}

export const excelData = async (payload) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}common/checkexcelfiles`, payload, { headers:{"Authorization":getToken()}})
    return response
  } catch (err) {
    return {error: err.message}
  }
}

export const readDMExcel = async (payload) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}common/readdmexcel`, payload, { headers:{"Authorization":getToken()}})
    return response
  } catch (err) {
    return {error: err.message}
  }
}
