import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import  { getToken } from "GlobalUtils/helpers"

const token = localStorage.getItem("token")
export const fetchDocDetails = async (docId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}docs/${docId}`,{headers:{"Authorization":getToken()}})
    return response.data
  } catch (error) {
    return error
  }
}

export const getUsersDetailsWithRelatedActivity = async(entityId)=>{
  try{
    const response= await axios.get(`${muniApiBaseURL}tasks/contextdata?entityId=${entityId}`,{headers:{"Authorization":getToken()}})
    return response.data
  }catch(ex){
    return ex.error
  }
}

export const saveTaskManager = async(taskData, taskId)=>{
  let responseData = null
  try {
    if(taskId) {
      console.log("taskData, taskId : ", taskData, taskId)
      const response = await axios.post(`${muniApiBaseURL}tasks/update-task`,
        {taskId, taskData},{headers:{"Authorization":getToken()}})
      responseData = response.data
    } else {
      const response = await axios.post(`${muniApiBaseURL}tasks`,
        {taskData},{headers:{"Authorization":getToken()}})
      responseData = response.data
    }
  } catch (error) {
    return error
  }
  return responseData
}

export const getS3FileGetURL = (bucketName, fileName, callback) => {
  if(!bucketName || !fileName) {
    this.setState({ signedS3Url: "", showModal: false, showDSModal: false, signerEmail: "",
      showSigneeInput: "", error: "No bucket/file name provided", waiting: false })
    return
  }

  const opType = "download"
  axios.post(`${muniApiBaseURL}s3/get-signed-url`, { opType, bucketName, fileName }, {headers:{"Authorization":getToken()}})
    .then(res => {
      const signedS3Url = res.data.url
      callback({ signedS3Url, showModal: false, showDSModal: false, error: "", fileName: "" })
    })
    .catch(err => {
      callback({ signedS3Url: "", showModal: false, showDSModal: false, signerEmail: "",
        showSigneeInput: "", error: "Error!!", waiting: false })
    })
}

export const fetchDecideDetailsByTranId = async (transId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp/decide/${transId}`)
    return response.data
  } catch (error) {
    return error
  }
}

export const fetchCurrentFirmTasksList = async (filter) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}tasks/activefirmtask?filter=${filter}`, {headers:{"Authorization":getToken()}})
    return response.data
  } catch (error) {
    return error
  }
}

export const getRelatedInfo = async()=>{
  try {
    const res = await axios.get(`${muniApiBaseURL}controls/related-info`,{headers:{"Authorization":getToken()}})
    return res.data
  } catch (error) {
    return error
  }
}

export const getUsersInfo = async()=>{
  try {
    const res = await axios.get(`${muniApiBaseURL}controls/users`,{headers:{"Authorization":getToken()}})
    return res.data
  } catch (error) {
    return error
  }
}

export const getTaskDetailById = async (taskId)=>{
  try {
    const response = await axios.get(`${muniApiBaseURL}tasks/taskId/${taskId}`,{headers:{"Authorization":getToken()}})
    return response
  } catch (error) {
    return error
  }
}
export const updateTask = async(taskId,taskData)=>{
  try {
    // const response = await axios.put(`${muniApiBaseURL}tasks/taskId/${taskId}`,{taskData},{headers:{"Authorization":getToken()}})
    const response = await axios.post(`${muniApiBaseURL}tasks/update-task`,
      {taskId, taskData},{headers:{"Authorization":getToken()}})
    return response
  } catch (error) {
    return error
  }
}

export const getEligibleTransForLoggedInUser = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}common/geteligibletransforloggedinuser`, { headers: { "Authorization": getToken() } })
    if (response.data && response.data.data && Array.isArray(response.data.data)) {
      return response.data.data.map(tran => ({
        ...tran,
        name: (tran.tranAttributes && tran.tranAttributes.issueName) || (tran.tranAttributes && tran.tranAttributes.projectDescription) || "",
        id: tran.tranId,
        group: (tran.tranAttributes && tran.tranAttributes.subType) || (tran.tranAttributes && tran.tranAttributes.type) || ""
      }))
    }
    return []
  } catch (error) {
    return error
  }
}

export const getEligibleEntitiesForLoggedInUser = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}common/geteligiblentitiesforloggedinuser`, { headers: { "Authorization": getToken() } })
    if (response.data && response.data.data && Array.isArray(response.data.data)) {
      const data = response.data.data.filter(f => f.relType !== "Self") || []
      return data && data.map(entity => ({
        ...entity,
        name: entity.firmName || "",
        id: entity.entityId,
        group: entity.relType || ""
      }))
    }
    return []
  } catch (error) {
    return error
  }
}

export const getEligibleUsersForLoggedInUser = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}common/geteligibleusersforloggedinuser`, { headers: { "Authorization": getToken() } })
    if (response.data && response.data.data && Array.isArray(response.data.data)) {
      return response.data.data.map(user => ({
        ...user,
        name: `${user.userFirstName} ${user.userLastName}`,
        id: user.userId,
      }))
    }
    return []
  } catch (error) {
    return error
  }
}
