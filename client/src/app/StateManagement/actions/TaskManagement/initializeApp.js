import { makeActionCreator } from "./../../utilities/makeActionCreator"

export const INITIALIZE_APP = "INITIALIZE_APP"
export const initializeApp = makeActionCreator(INITIALIZE_APP,"")