import axios from "axios"
import * as actions from "./actions"
import { startDeletingSearchPref, finsihDeletingSearchPref, startFetchingSearchPref, finsihFetchingSearchPref, startSavingSearchPref, finsihSavingSearchPref} from "../SearchPref"
import { muniApiBaseURL } from "../../../../globalutilities/consts"

export const updateGlobalSearchPref = (filter, data) => (dispatch, getState) => {
  const { globalSearchPref, selectedGlobalPref } = getState()
  const currentData = globalSearchPref[selectedGlobalPref]
  const payload = { ...currentData, [filter]: data}

  dispatch({
    type: actions.UPDATE_GLOABAL_SEARCH_PREF,
    id: selectedGlobalPref,
    payload
  })
}

export const setSelectedGlobalSearchPref = (data) => ({
  type: actions.UPDATE_SELECTED_GLOABAL_SEARCH_PREF,
  data,
})

export const setPrefFilter = (pref) => ({
  type: actions.RENAME_SELECTED_PREF,
  pref
})


export const resetGlobalSearch = () => ({
  type: actions.RESET_GLOBAL_SEARCH
})

export const fetchGlobalSearchPrefs = () => (dispatch, getState) => {
  dispatch(startFetchingSearchPref())
  const { auth: { token }} = getState()
  axios({
    method: "GET",
    url: `${muniApiBaseURL}searchpref/context/tools-globalsearch`,
    headers: { Authorization: token },
  }).then(res => {
    dispatch(setPrefFilter(res.data.data.searchPreference))
    setTimeout(() => {
      dispatch(finsihFetchingSearchPref())
    },100)
  }).catch(() => {
    dispatch(finsihFetchingSearchPref())
  })
}

export const saveGlobalSearchFilter = (filterName) => (dispatch, getState) => {
  const {   globalSearchPref, selectedGlobalPref, auth: { token } } = getState()
  const pref = { ...globalSearchPref }
  const selectedPref = { ...pref[selectedGlobalPref] }
  delete pref[selectedGlobalPref]
  const key = filterName.replace(/[^a-zA-Z0-9]/g, "")
  pref[key] = selectedPref
  pref[key].filterName = filterName
  delete pref["0"]

  dispatch(startSavingSearchPref())
  return axios({
    method: "POST",
    url: `${muniApiBaseURL}searchpref/context/tools-globalsearch`,
    headers: { Authorization: token },
    data: pref
  }).then(() => {
    dispatch(setPrefFilter(pref))
    dispatch(setSelectedGlobalSearchPref(key))
    dispatch(finsihSavingSearchPref())
  }).catch(() => {
    dispatch(finsihSavingSearchPref())
  })
}

export const deleteGlobalSearchPref = () => (dispatch, getState) => {
  const {   globalSearchPref, selectedGlobalPref, auth: { token } } = getState()

  if (selectedGlobalPref !== "0") {
    const pref = { ...globalSearchPref }
    delete pref[selectedGlobalPref]
    delete pref["0"]

    dispatch(startDeletingSearchPref())
    return axios({
      method: "POST",
      url: `${muniApiBaseURL}searchpref/context/tools-globalsearch`,
      headers: { Authorization: token },
      data: pref
    }).then(() => {
      dispatch(setPrefFilter(pref))
      dispatch(setSelectedGlobalSearchPref("0"))
      dispatch(finsihDeletingSearchPref())
    }).catch(() => {
      dispatch(finsihDeletingSearchPref())
    })
  }

  return null
}
