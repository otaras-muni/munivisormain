import * as actions from "./actions"

export const startFetchingSearchPref = () => ({
  type: actions.START_FETCHING_SEARCH_PREF
})

export const finsihFetchingSearchPref = () => ({
  type: actions.FINISHI_FETCHING_SEARCH_PREF
})

export const startSavingSearchPref = () => ({
  type: actions.START_SAVING_SEARCH_PREF
})

export const finsihSavingSearchPref = () => ({
  type: actions.FINISHI_SAVING_SEARCH_PREF
})

export const startDeletingSearchPref = () => ({
  type: actions.START_DELETING_SEARCH_PREF
})

export const finsihDeletingSearchPref = () => ({
  type: actions.FINISHI_DELETING_SEARCH_PREF
})
