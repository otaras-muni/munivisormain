import axios from "axios"


import { muniApiBaseURL, muniAuthBaseURL } from "GlobalUtils/consts.js"
import { getLogoBase64 } from "GlobalUtils/pdfutils"
import {
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  PDF_LOGO,
  CHECKAUTH_SUCCESS,
  CHECKAUTH_ERROR,
  SIGN_OUT,
  GET_URLS_FOR_FORNTEND
} from "../types"

export const startSignUpProcess = ({ email, password }) => async dispatch => {
  try {
    localStorage.removeItem("token")
    localStorage.removeItem("email")

    localStorage.setItem("email", "")
    const loginResponse = await axios.post(`${muniAuthBaseURL}signup`, {
      email,
      password
    })
    const {
      token,
      authenticated,
      loginDetails,
      errorMessage
    } = loginResponse.data
    dispatch({
      type: SIGNUP_SUCCESS,
      payload: { token, authenticated, loginDetails, errorMessage }
    })
    return loginResponse.data && loginResponse.data.done ? "true" : loginResponse.error
  } catch (error) {
    const { data } = error.response
    const errorMessage = data.error
      ? data.error
      : "Unable to Sign Up as a user on the application"
    dispatch({
      type: SIGNUP_ERROR,
      payload: {
        token: "",
        authenticated: false,
        loginDetails: {},
        errorMessage
      }
    })
    return errorMessage || "Need strong password"
  }
}

export const getUserByResetId = async (resetId) => {
  try {
    localStorage.removeItem("token")
    localStorage.removeItem("email")

    localStorage.setItem("email", "")
    const loginResponse = await axios.get(`${muniAuthBaseURL}resetpassconfirm?id=${resetId}`)
    const {
      updatedUsers
    } = loginResponse.data
    return loginResponse.done ? loginResponse : loginResponse
  } catch (error) {
    const { data } = error.response
    const errorMessage = data.error
      ? data.error
      : "Invalid reset id"
    return errorMessage || "Invalid reset id"
  }
}

export const startSignInProcess = ({ email, password }) => async dispatch => {
  let errorMessage = ""
  let loginResponse = ""
  try {
    loginResponse = await axios.post(`${muniAuthBaseURL}signin`, {
      email,
      password
    })
    const {
      token,
      authenticated,
      loginDetails,
      errorMessage,
      exp
    } = loginResponse.data
    localStorage.setItem("token", token)
    localStorage.setItem("email", email)

    dispatch({
      type: LOGIN_SUCCESS,
      payload: { token, authenticated, loginDetails, errorMessage, exp }
    })
    dispatch({
      type: GET_ELIGIBLE_IDS_FOR_LOGGED_IN_USER,
      payload: eligibleIds
    })
    return ""
  } catch ({response}) {
    // const { data } = error.response
    errorMessage = response ? response.data.message : "Either password or user ID doesn't match"
    // dispatch ({ type:LOGIN_ERROR, payload:{token:"",authenticated:false,loginDetails:{},errorMessage}})
    return errorMessage
  }
}

export const checkAuth = tokenforheader => async dispatch => {
  console.log("tokenforheader : ", tokenforheader)
  try {
    const authResponse = await axios.get(`${muniAuthBaseURL}checkauth`, {
      headers: { Authorization: tokenforheader }
    })
    const {
      token,
      authenticated,
      loginDetails,
      errorMessage,
      exp
    } = authResponse.data

    if (loginDetails && loginDetails.userEntities && loginDetails.userEntities.length) {
      const docId = (loginDetails.userEntities[0].settings
        && loginDetails.userEntities[0].settings.logo
        && loginDetails.userEntities[0].settings.logo.awsDocId) || ""
      if (docId) {
        const baseURL = await getLogoBase64(docId)
        const toDataURL = url => fetch(url)
          .then(response => response.blob())
          .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob)
          }))

        toDataURL(baseURL)
          .then(dataUrl => {
            dispatch({
              type: PDF_LOGO,
              payload: { logo: dataUrl }
            })
          })
      } else {
        dispatch({
          type: PDF_LOGO,
          payload: { logo: "" }
        })
      }
    }
    let tenantId = ""
    if(loginDetails && loginDetails.relatedFaEntities && loginDetails.relatedFaEntities.length){
      tenantId = loginDetails.relatedFaEntities[0].entityId || ""
    }
    dispatch({
      type: CHECKAUTH_SUCCESS,
      payload: { token, authenticated, loginDetails, errorMessage, exp, tenantId }
    })
    // dispatch({ type: GET_ELIGIBLE_IDS_FOR_LOGGED_IN_USER, payload: eligibleIds })
  } catch (error) {
    const { data = {} } = (error && error.response) || {}
    const errorMessage = data.error ? data.error : "Unable to Check Authentication"
    localStorage.setItem("token", "")
    localStorage.setItem("email", "")
    localStorage.setItem("tableCache", "")
    dispatch({ type: SIGN_OUT })
    // dispatch({
    //   type: CHECKAUTH_ERROR,
    //   payload: {
    //     token: "",
    //     authenticated: false,
    //     loginDetails: {},
    //     errorMessage,
    //     exp: 0
    //   }
    // })
  }
}

export const signOut = () => {
  console.log("in action signOut")
  localStorage.setItem("token", "")
  localStorage.setItem("email", "")
  localStorage.setItem("tableCache", "")

  if(window.mvAuthWarningTimeout1) {
    window.mvAuthWarningTimeout1 = null
  }

  if(window.mvAuthWarningTimeout5) {
    window.mvAuthWarningTimeout5 = null
  }

  if(window.mvAuthTimeout) {
    window.mvAuthTimeout = null
  }
  window.location.href="/signin"
  return {
    type: SIGN_OUT
  }
}
