export const SIGNUP_START_ASYNC = "SIGNUP_START_ASYNC"
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS"
export const SIGNUP_ERROR = "SIGNUP_ERROR"

export const LOGIN_START_ASYNC = "LOGIN_START_ASYNC"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_ERROR = "LOGIN_ERROR"

export const CHECKAUTH_START_ASYNC = "CHECKAUTH_START_ASYNC"
export const CHECKAUTH_SUCCESS = "CHECKAUTH_SUCCESS"
export const CHECKAUTH_ERROR = "CHECKAUTH_ERROR"

// export const startSignUpProcess= (loginCredentials,callback) => ({ type:SIGNUP_START_ASYNC, payload:loginCredentials, callback})
// export const startSignInProcess = (loginCredentials,callback) => ({ type:LOGIN_START_ASYNC, payload:loginCredentials, callback})
// export const checkAuth = (token) => ({ type:CHECKAUTH_START_ASYNC, payload:token})
