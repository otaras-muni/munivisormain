import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {getToken} from "GlobalUtils/helpers"

export const fetchMarfpTransaction = async(type, tranId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}marfps/transaction/${type}/${tranId}`, {headers:{"Authorization":getToken()}})
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}

export const putMarfpStatusAndSec  = async(tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}marfps/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putMarfpTransaction = async(type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}marfps/transaction/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const postMARFPNotes = async(tranId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}marfps/notes/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putMarfpDocumentTransaction = async (type, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}marfps/transaction/${type}/${body._id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const pullMarfpParticipantsDetails = async(id, tranId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}marfps/participants/${tranId}?id=${id}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putMarfpParticipantsDetails = async(tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}marfps/participants/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}
