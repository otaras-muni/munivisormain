import axios from "axios"
import { muniApiBaseURL, token } from "GlobalUtils/consts.js"
import {getToken} from "GlobalUtils/helpers"

export const postFAServiceInfo = async(type, tranId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billing/`, {headers:{"Authorization":token}})
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}

export const getEligibleInvoices = async(type, tranId, callback) => {

  try {
    // const response = await axios.get(`${muniApiBaseURL}billing/`, {headers:{"Authorization":token}})
    const response = {
      data:[
        {
          finAdvisorEntityId:"123",
          finAdvisorEntityName:"Ford and Associates",
          issuerClientEntityId:"456",
          issuerClientEntityName:"City of Tampa",
          activityId:"456789",
          activityType:"Deal Issue",
          activityDescription:"City of Tampa Deal",
          invoiceNumber:"Inv 123",
          outOfPocketUserExpenses:[],
          invoiceStatus:"Open", // This information is available on the dashboard.
          lastUpdateDate:"2018-08-12"
        },
        {
          finAdvisorEntityId:"123",
          finAdvisorEntityName:"Ford and Associates",
          issuerClientEntityId:"756",
          issuerClientEntityName:"State of Florida",
          activityId:"6675",
          activityType:"Deal Issue",
          activityDescription:"State of Florida Deal",
          invoiceNumber:"Inv ABC",
          outOfPocketUserExpenses:[{
            tenantUserId:"nav123",
            userFirstName:"Naveen",
            userLastName:"Balawat",
            outOfPocketExpenses:[{
              expenseType:"Flight",
              expenseNumMiles:12,
              expenseRatePerMile:0.1,
              expenseAmount:1.2,
              expenseDate:"2018-08-01",
              expenseContextType:"Meeting",
              expenseDetail:"I went to meet someone",
              expenseUploadTime:"2018-08-02"
            }]
          }],
          invoiceStatus:"Open", // This information is available on the dashboard.
          lastUpdateDate:"2018-08-12"
        }
      ]
    }
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}

export const getfaservicefee = async (entityId, activityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billing/faservicefee?entityId=${entityId}&activityId=${activityId}`, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putfaservicefee = async (entityId, activityId, body) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}billing/faservicefee?entityId=${entityId}&activityId=${activityId}`, body, {headers:{"Authorization":getToken()}} )
    return response
  } catch (error) {
    console.log(error)
    return error
  }
}

export const fetchBillingByInvoiceId = async (invoiceId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billing/invoice/${invoiceId}`, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}
// I get invoices
// I add invoice

export const searchInvoiceDashboard = async(searchTerm, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billing/search/?searchterm=${searchTerm}`,{headers:{"Authorization":getToken()}})
    const {done,message,data} = response.data
    if(done) {
      callback(data)
    } else {
      callback({error:message})
    }
  } catch (e) {
    callback({error:e})
  }
}

export const postInvoiceStatusUpdateFromDashboard = async(body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}billing/dashboard/updateinvoice`, body, {headers:{"Authorization":getToken()}})
    const {done,message,data} = response.data
    if(done) {
      callback(data)
    } else {
      callback({error:message})
    }
  } catch (e) {
    callback({error:e})
  }
}

export const fetchAllTransactionForBilling = async(entityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billing/transactions?entityId=${entityId}`, {headers:{"Authorization":getToken()}})
    if(response && response.status === 200){
      return response.data
    }
    return []
  } catch (e) {
    console.log({error:e})
    return e
  }
}

export const createInvoice = async(body) => {
  try {
    return await axios.post(`${muniApiBaseURL}billing/createinvoice`, body, {headers:{"Authorization":getToken()}})
  } catch (e) {
    console.log({error:e})
    return e
  }
}

export const getBillingPocketExpense = async(query) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billingmanageexpense/tranexpense${query}`, {headers:{"Authorization":getToken()}})
    if(response && response.status === 200){
      return response.data
    }
    return {}
  } catch (e) {
    console.log({error:e})
    return e
  }
}

// /////////////////////// Billing Expenses Revenue //////////////////

export const createNewInvoice = async (body, callback, token) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}billing`, body, { headers: { "Authorization": token } })
    callback(response.data)
  }
  catch (e) {
    console.log(e)
  }
}

export const insertUpdateExpense = async (invoiceId, userId, body, callback, token) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}billing/${invoiceId}/userexpense/${userId}`, body, { headers: { "Authorization": token } })
    const { data: invoiceData } = response.data
    callback(invoiceData)
  }
  catch (e) {
    console.log(e)
  }
}

export const pullExpenseForInvoiceAndUser = async (invoiceId, userId, expenseId, callback, token) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}billing/${invoiceId}/userexpense/${userId}?id=${expenseId}`, { headers: { "Authorization": token } })
    const { data: invoiceData } = response.data
    callback(invoiceData)
  } catch (error) {
    callback({ error })
  }
}

export const managebillingexpensesstate = async (payload, callback) => {
  const response = await axios.post(`${muniApiBaseURL}billing/managebillingexpensesstate`, payload, { headers: { "Authorization": getToken() } })
  console.log("there is data", response && response.data)
  callback(response.data.newstatedetails)
}


export const getEligibleTransactionsForInvoices = async(callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}billing/getalleligibletransactions`,{headers:{"Authorization":getToken()}})
    const {done,message,data,tranInfo} = response.data

    // tranUserEntityId
    // tranUserId
    // activityTranFirmId

    if (!done ) {
      callback({error:message})
    }

    const formattedData = Array.isArray(data) && data.reduce( (acc, a) => {
      const newAcc = {...acc}
      const {activityTranClientName} = a

      if (acc[activityTranClientName]) {
        newAcc[activityTranClientName] = [...newAcc[activityTranClientName],a]
      } else {
        newAcc[activityTranClientName] = [ a]
      }
      return { ...newAcc }
    },{})

    const firmUsersPartOfTransaction = tranInfo.reduce( (acc, a) =>{
      const {activityId,activityTranFirmId, participantUsers  } = a
      const firmUsers = (participantUsers || []).filter( f => f.tranUserEntityId.toString() === activityTranFirmId.toString())
      return {...acc,[activityId]:firmUsers}
    },{})

    callback(formattedData,firmUsersPartOfTransaction)
  } catch (error) {
    callback({error})
  }
}

export const putBillingManageExpense = async (payload) => {
  try {
    return await axios.put(`${muniApiBaseURL}billingmanageexpense`, payload, {headers:{"Authorization":getToken()}})
  } catch (e) {
    console.log({error:e})
    return e
  }
}

export const fetchBillingManageExpense = async (query) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}billingmanageexpense${query}`, {headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res.data
    }
    return {}
  } catch (e) {
    console.log({error:e})
    return e
  }
}


export const fetchExpenseReceipts = async (query) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}billingmanageexpense/receipts${query}`, {headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res.data
    }
    return {}
  } catch (e) {
    console.log({error:e})
    return e
  }
}

export const postExpenseReceipts = async (query, payload) => {
  try {
    return await axios.put(`${muniApiBaseURL}billingmanageexpense/receipts${query}`, payload, {headers:{"Authorization":getToken()}})
  } catch (e) {
    console.log({error:e})
    return e
  }
}

export const pullExpenseReceipts = async (query) => {
  try {
    return await axios.delete(`${muniApiBaseURL}billingmanageexpense/receipts${query}`, {headers:{"Authorization":getToken()}})
  } catch (e) {
    console.log({error:e})
    return e
  }
}
