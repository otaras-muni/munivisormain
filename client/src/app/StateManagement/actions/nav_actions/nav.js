import { getUserDetails, getAccessPolicy, mapPrimaryNavArrayToObject } from "GlobalUtils/helpers"

import {
  GET_NAV_OPTIONS
} from "../types"

export const getNavOptions = token => async dispatch => {
  console.log("token : ", token)
  let navOptions
  const userPolicy = await getAccessPolicy(token)
  console.log("userPolicy : ", userPolicy)
  if(!userPolicy || !userPolicy.UIAccess || !userPolicy.UIAccess.primaryNav) {
    navOptions = {}
  } else {
    navOptions = mapPrimaryNavArrayToObject(userPolicy.UIAccess.primaryNav)
  }
  dispatch ({
    type: GET_NAV_OPTIONS,
    payload: navOptions
  })
}
