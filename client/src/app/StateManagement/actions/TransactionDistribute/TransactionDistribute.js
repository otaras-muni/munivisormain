import axios from "axios"
import {getToken} from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"

export const fetchTransaction = async(type, transId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp/transaction/${type}/${transId}`, {headers:{"Authorization":getToken()}})
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}

export const fetchSupplierContacts = async(clientId, serviceType, callback) => {
  try {
    const service = serviceType === "Underwriting Services" ? serviceType.replace(/\//g, "%2F") :
      (serviceType === "Bank Loans / Lease" || serviceType === "General/Local Counsel") ? encodeURIComponent(serviceType) : serviceType
    const response = await axios.get(`${muniApiBaseURL}entityUser/entitySupplier/${clientId}/${service}`, {headers:{"Authorization":getToken()}})
    let suppliersList = []
    if (response.data && Array.isArray(response.data.thirdPartyDetails)) {
      suppliersList = response
        .data.thirdPartyDetails
        .map(ent => {
          const newObject = {
            ...ent,
            name: ent.firmName,
            id: ent._id // eslint-disable-line
          }
          return newObject
        })
    }
    suppliersList = _.orderBy(suppliersList, ['name'])
    let userData = _.orderBy(response.data.userList, ['userFirstName'])
    callback({suppliersList, userList: userData})
  } catch (error) {
    callback({error})
  }
}

export const fetchRFPSupplierContacts = async(clientId, serviceType, tranType, callback) => {
  try {
    const service = serviceType === "Underwriting Services" ? serviceType.replace(/\//g, "%2F") :
      (serviceType === "Bank Loans / Lease" || serviceType === "General/Local Counsel") ? encodeURIComponent(serviceType) : serviceType
    const response = await axios.get(`${muniApiBaseURL}entityUser/entitySupplier/${clientId}/${service}?tranType=${tranType}`, {headers:{"Authorization":getToken()}})
    let suppliersList = []
    if (response.data && Array.isArray(response.data.thirdPartyDetails)) {
      suppliersList = response
        .data.thirdPartyDetails
        .map(ent => {
          const newObject = {
            ...ent,
            name: ent.firmName,
            id: ent._id // eslint-disable-line
          }
          return newObject
        })
    }
    callback({suppliersList, userList: response.data.userList})
  } catch (error) {
    callback({error})
  }
}

export const updateTransaction = async(id, type, payload, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}rfp/rfpDistribution/${id}?type=${type}`, payload, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const pullDistribute = async(id, type, delId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}rfp/rfpDistribution/${id}?type=${type}&delId=${delId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putRelatedTran = async (id, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}rfp/rfpDistribution/${id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const isValidForRfpSettings = async (id) => {
  try {
    return await axios.get(`${muniApiBaseURL}rfp/rfpEvaluation?id=${id}`, {headers:{"Authorization":getToken()}})
  } catch (error) {
    console.log(error)
    return error
  }
}
