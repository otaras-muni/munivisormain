import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts"
import {getToken} from "GlobalUtils/helpers"

import {
  GET_DOCS
} from "../types"

export const dispatchGetDocs = (dispatch, data) => {
  dispatch({
    type: GET_DOCS,
    payload: data
  })
}

export const getDocs = () => async dispatch => {
  console.log("in action getDocs")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}api/docs`,{headers:{"Authorization":getToken()}})
    if(res && res.status === 200 && res.data){
      dispatchGetDocs(dispatch, res.data)
    } else {
      console.log("no data in getConfig: ")
      dispatchGetDocs(dispatch, [])
    }
  } catch (err) {
    console.log("err in getDocs ", err)
    dispatchGetDocs(dispatch, [])
  }
}

export const getDocsByContextId = async( query ) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}docs${query}`, {headers:{"Authorization":getToken()}})
    return response
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putDocs = async( query, body, callback ) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}docs${query}`, body, {headers:{"Authorization":getToken()}})
    callback (response)
  } catch (error) {
    console.log(error)
    callback (error)
  }
}

export const getDocFolders = async( query ) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}docfolder${query}`, {headers:{"Authorization":getToken()}})
    return response
  } catch (error) {
    console.log(error)
    return error
  }
}

export const createDocFolders = async( body, callback ) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}docfolder`, body, {headers:{"Authorization":getToken()}})
    callback (response)
  } catch (error) {
    console.log(error)
    callback (error)
  }
}

export const putDocFolders = async( folderId, body, callback ) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}docfolder/${folderId}`, body, {headers:{"Authorization":getToken()}})
    callback (response)
  } catch (error) {
    console.log(error)
    callback (error)
  }
}

export const getS3ObjectVersions = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}s3/get-s3-object-versions`, payload,{headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res
    }
    return null
  } catch (err) {
    console.log("err in getS3ObjectVersions ", err.message)
    return null
  }
}

export const getSignedUrl = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}s3/get-signed-url`, payload,{headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res
    }
    return null
  } catch (err) {
    console.log("err in getSignedUrl ", err.message)
    return null
  }
}

export const getDocInfo = async (payload) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}docs`, {...payload,headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res
    }
    return null
  } catch (err) {
    console.log("err in getDocInfo ", err.message)
    return null
  }
}

export const addDocInDB = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}docs`, payload,{headers:{"Authorization":getToken()}})
    if(res && (res.status === 201 || res.status === 200)){
      return res
    }
    return null
  } catch (err) {
    console.log("err in addDocInDB ", err.message)
    return null
  }
}

export const updateVersionInDB = async (payload) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}docs/update-versions`, payload,{headers:{"Authorization":getToken()}})
    if(res && res.status === 200){
      return res
    }
    return null
  } catch (err) {
    console.log("err in addDocInDB ", err.message)
    return null
  }
}
