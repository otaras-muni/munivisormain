import axios from "axios"

import { mapAccordionsArrayToObject, mapAccordionsObjectToArray, updateAuditLog } from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {
  GET_CONFIG_CHECKLIST,
  SAVE_CONFIG_CHECKLIST,
  GET_CONFIG_PICKLIST,
  SAVE_CONFIG_PICKLIST,
  ADD_NEW_CHECKLIST,
  ADD_NEW_NOTIFICATION,
  GET_CONFIG_NOTIFICATIONS,
  SAVE_CONFIG_NOTIFICATIONS,
  TOGGLE_PICKLISTS_LOADED
} from "../types"

export const addNewChecklist = payload => ({type: ADD_NEW_CHECKLIST, payload})

export const dispatchGetConfigChecklist = (dispatch, getState, payload) => {
  const { configChecklists } = getState()
  console.log("configChecklists : ", configChecklists)
  if(!configChecklists.length) {
    dispatch({type: GET_CONFIG_CHECKLIST, payload})
  }
}

export const getConfigChecklist = (token) => async (dispatch, getState) => {
  token = token || localStorage.getItem("token")
  console.log("in action getConfigChecklist")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}configs/`, {
      headers:{"Authorization":token},
      params: { require: "checklists" }
    })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data && res.data.length) {
      const checklists = res.data
      const payload = checklists.map(c => {
        const { id, name, type, attributedTo, bestPractice, notes, lastUpdated, data } = c
        const item = { id, name, type, attributedTo, bestPractice, notes, lastUpdated }
        const [itemData, otherData] = mapAccordionsArrayToObject(data, true)
        const itemHeaders = {}
        Object.keys(otherData).forEach(k => {
          itemHeaders[k] = otherData[k].headers
        })
        item.data = itemData
        item.itemHeaders = itemHeaders
        return item
      })
      dispatchGetConfigChecklist(dispatch, getState, payload)
    } else {
      console.log("no data in getConfigChecklist: ")
      dispatchGetConfigChecklist(dispatch, getState, [])
    }
  } catch (err) {
    console.log("err in getConfigChecklist: ", err)
    dispatchGetConfigChecklist(dispatch, getState, [])
  }
}

export const dispatchSaveConfigChecklist = (dispatch, data) => {
  const payload = data.filter(e => e.type && e.data && Object.keys(e.data).length)
  dispatch({
    type: SAVE_CONFIG_CHECKLIST,
    payload
  })
}

export const saveConfigChecklist = (token, checklists, changeLog, callback, target) => async (dispatch, getState) => {
  console.log("in action saveConfigChecklist : ", checklists)
  if(target === "redux-store") {
    dispatchSaveConfigChecklist(dispatch, checklists)
    return
  }
  const configChecklists = [...checklists]
  checklists.forEach((checklist, i) => {
    configChecklists[i] = {...checklist}
    const otherData = {}
    Object.keys(checklist.itemHeaders).forEach(k => {
      otherData[k] = {}
      otherData[k].headers = checklist.itemHeaders[k]
    })
    configChecklists[i].data = mapAccordionsObjectToArray(checklist.data, otherData)
  })
  // const data = mapAccordionsObjectToArray(config)
  // console.log("data : ", data)
  try {
    const res = await axios.post(`${muniApiBaseURL}configs/update-checklists`,
      {checklists: configChecklists}, {headers:{"Authorization":token}})
    console.log("res : ", res)
    if (res && res.status >= 200 && res.status < 300) {
      // dispatchSaveConfigChecklist(dispatch, checklists)
      callback()
      await updateAuditLog("config", changeLog)
      // dispatchGetConfigChecklist(dispatch, getState, [])
    } else {
      callback("err")
      console.log("err in saveConfigChecklist put: ", res)
    }
    // const res = await axios.get(`${muniApiBaseURL}configs/`, {headers:{"Authorization":token}})
    // console.log("res : ", res)
    // if (res && res.status >= 200 && res.status < 300 && res.data && res.data[0] && res.data[0]._id) {
    //   const configObj = {
    //     ...res.data[0]
    //   }
    //   const {_id} = configObj
    //   configObj.checklists = configChecklists
    //   console.log("_id : ", _id)
    //   const res1 = await axios.put(`${muniApiBaseURL}configs/${_id}`, configObj)
    //   console.log("res1 : ", res1)
    //   if (res1 && res1.status >= 200 && res1.status < 300) {
    //     await updateAuditLog("config", changeLog)
    //     // dispatchGetConfigChecklist(dispatch, getState, [])
    //     dispatchSaveConfigChecklist(dispatch, checklists)
    //   } else {
    //     console.log("err in saveConfigChecklist put: ")
    //   }
    // } else {
    //   console.log("no id in saveConfigChecklist: ")
    //   // const configData = {   entityId: "5b0bc1d6e129104ae2e19623",   checklists: [
    //   //   {       type: "rfp",       data     }   ] } const res1 = await
    //   // axios.post(`${muniApiBaseURL}configs/", configData) console.log("res1 : ",
    //   // res1) if(res1 && res1.status >= 200 && res1.status < 300) {   await
    //   // updateAuditLog("config", changeLog)   dispatchSaveConfigChecklist(dispatch,
    //   // config) } else {   console.log("err in saveConfigChecklist post: ") }
    // }
  } catch (err) {
    callback("err")
    console.log("err in saveConfigChecklist: ", err)
  }
}

export const dispatchGetConfigPicklist = (dispatch, data) => {
  const [picklists,
    otherData] = mapAccordionsArrayToObject(data, true)
  const picklistsMeta = {}
  Object
    .keys(otherData)
    .forEach(k => {
      picklistsMeta[k] = otherData[k].meta
    })
  dispatch({
    type: GET_CONFIG_PICKLIST,
    payload: {
      picklists,
      picklistsMeta,
      loaded: true
    }
  })
}

export const deleteConfigPicklist = async (token, pickListItemsID) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}configs/update-specific?id=${pickListItemsID}`, {
      headers: { Authorization: token }
    })
    return response.data
  } catch (error) {
    return error
  }
}

export const getConfigPicklist = (token, systemName, search) => async dispatch => {
  console.log("in action getConfigPicklist")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}configs/`, {
      headers:{ "Authorization":token },
      params: { require: "picklists", names: systemName, searchTerm: search }
    })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data.length) {
      dispatchGetConfigPicklist(dispatch, res.data)
    } else {
      console.log("no data in getConfigPicklist: ")
      dispatchGetConfigPicklist(dispatch, [])
    }
  } catch (err) {
    console.log("err in getConfigPicklist: ", err)
    dispatchGetConfigPicklist(dispatch, [])
  }
}

export const getSpecificPicklist = async (token, systemName) => {
  console.log("in action getSpecificPicklist")
  let res
  try {
    systemName = encodeURIComponent(systemName)
    console.log("Edit picklist ...", systemName)
    res = await axios.get(`${muniApiBaseURL}configs/${systemName}`, {
      headers:{ "Authorization":token },
      params: { names: systemName }
    })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data.length) {
      return res.data[0]
    }
  } catch (err) {
    console.log("err in getConfigPicklist: ", err)
  }
}

export const saveSpecificPicklist = async (token, id, pickList) => {
  console.log("in action saveSpecificPicklist")
  let res
  try {
    res = await axios.post(`${muniApiBaseURL}configs/update-specific?id=${id}`, pickList, {
      headers:{ "Authorization":token }
    })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data.length) {
      return res.data[0]
    }
  } catch (err) {
    console.log("err in getConfigPicklist: ", err)
  }
}

export const dispatchSaveConfigPicklist = (dispatch, config, loaded) => {
  dispatch({
    type: SAVE_CONFIG_PICKLIST,
    payload: {
      ...config,
      loaded
    }
  })
}

export const saveConfigPicklist = (token, entityId, config, options, changeLog,
  target) => async dispatch => {
  console.log("in action saveConfigPicklist : ", config, options)
  if(target === "redux-store") {
    dispatchSaveConfigPicklist(dispatch, config, true)
    return
  }
  dispatch({ type: TOGGLE_PICKLISTS_LOADED, payload: false })
  const { picklists, picklistsMeta } = config
  const otherData = {}
  Object.keys(picklistsMeta).forEach(k => {
    otherData[k] = {}
    otherData[k].meta = picklistsMeta[k]
  })
  const data = mapAccordionsObjectToArray(picklists, otherData)
  console.log("data : ", data)
  try {
    const res = await axios.get(`${muniApiBaseURL}configs/get-config-id`, {headers:{"Authorization":token}})
    console.log("res : ", res)
    if (res && res.status >= 200 && res.status < 300 && res.data && res.data._id) {
      console.log("_id : ", res.data._id)
      const configObj = { _id:  res.data._id }
      configObj.newPicklists = data.filter(e => options.new.includes(e.title))
      configObj.changedPicklists = data.filter(e => options.changed.includes(e.title))
      configObj.removed = options.removed
      console.log("configObj : ", configObj)
      const res1 = await axios.post(`${muniApiBaseURL}configs/update-picklists`,
        configObj, {headers:{"Authorization":token}})
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300) {
        await updateAuditLog("config", changeLog)
        dispatchSaveConfigPicklist(dispatch, config, "saved")
      } else {
        console.log("err in saveConfigPicklist put: ")
        dispatch({ type: TOGGLE_PICKLISTS_LOADED, payload: "error" })
      }
    } else {
      console.log("no id in saveConfigPicklist: ")
      const configData = {
        entityId,
        picklists: data
      }
      const res1 = await axios.post(`${muniApiBaseURL}configs/`, configData)
      console.log("res1 : ", res1)
      if(res1 && res1.status >= 200 && res1.status < 300) {
        await updateAuditLog("config", changeLog)
        dispatchSaveConfigPicklist(dispatch, config, "saved")
      } else {
        console.log("err in saveConfigPicklist post: ")
        dispatch({ type: TOGGLE_PICKLISTS_LOADED, payload: "error" })
      }
    }
  } catch (err) {
    console.log("err in saveConfigPicklist: ", err)
    dispatch({ type: TOGGLE_PICKLISTS_LOADED, payload: "error" })
  }
}

export const addNewNotifications = payload => ({type: ADD_NEW_NOTIFICATION, payload})

export const dispatchGetConfigNotifications = (dispatch, getState, payload) => {
  const { configNotifications } = getState()
  console.log("configNotifications : ", configNotifications)
  if(!configNotifications.length) {
    dispatch({type: GET_CONFIG_NOTIFICATIONS, payload})
  }
}

export const getConfigNotifications = (token) => async (dispatch, getState) => {
  token = token || localStorage.getItem("token")
  console.log("in action getConfigNotifications")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}configs/`, {
      headers:{"Authorization":token},
      params: { require: "notifications" }
    })
    console.log("res : ", res)
    if(res && res.status === 200 && res.data && res.data.length) {
      const notifications = res.data
      dispatchGetConfigNotifications(dispatch, getState, notifications)
    } else {
      console.log("no data in getConfigNotifications: ")
      dispatchGetConfigNotifications(dispatch, getState, [])
    }
  } catch (err) {
    console.log("err in getConfigNotifications: ", err)
    dispatchGetConfigNotifications(dispatch, getState, [])
  }
}

export const dispatchSaveConfigNotifications = (dispatch, payload) => {
  dispatch({
    type: SAVE_CONFIG_NOTIFICATIONS,
    payload
  })
}

export const saveConfigNotifications = (token, notifications, changeLog, target) => async (dispatch) => {
  console.log("in action saveConfigNotifications : ", notifications)
  if(target === "redux-store") {
    dispatchSaveConfigNotifications(dispatch, notifications)
    return
  }

  try {
    const res = await axios.get(`${muniApiBaseURL}configs/`, {headers:{"Authorization":token}})
    console.log("res : ", res)
    if (res && res.status >= 200 && res.status < 300 && res.data && res.data[0] && res.data[0]._id) {
      const configObj = {
        ...res.data[0]
      }
      const {_id} = configObj
      configObj.notifications = notifications
      console.log("_id : ", _id)
      const res1 = await axios.put(`${muniApiBaseURL}configs/${_id}`, configObj)
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300) {
        // await updateAuditLog("config", changeLog)
        // dispatchGetConfigChecklist(dispatch, getState, [])
        dispatchSaveConfigNotifications(dispatch, notifications)
      } else {
        console.log("err in saveConfigNotifications put: ")
      }
    } else {
      console.log("no id in saveConfigNotifications: ")
      // const configData = {   entityId: "5b0bc1d6e129104ae2e19623",   checklists: [
      //   {       type: "rfp",       data     }   ] } const res1 = await
      // axios.post(`${muniApiBaseURL}configs/", configData) console.log("res1 : ",
      // res1) if(res1 && res1.status >= 200 && res1.status < 300) {   await
      // updateAuditLog("config", changeLog)   dispatchSaveConfigChecklist(dispatch,
      // config) } else {   console.log("err in saveConfigChecklist post: ") }
    }
  } catch (err) {
    console.log("err in saveConfigNotifications: ", err)
  }
}
