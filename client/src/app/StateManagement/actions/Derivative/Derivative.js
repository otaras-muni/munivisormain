import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {getToken} from "GlobalUtils/helpers"

export const fetchTransaction = async(type, tranId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}derivatives/transaction/${type}/${tranId}`, {headers:{"Authorization":getToken()}})
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}

export const putTransaction = async(type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}derivatives/transaction/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const postDerivativeNotes = async(tranId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}derivatives/notes/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putDocumentTransaction = async (type, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}derivatives/transaction/${type}/${body._id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const putParticipantsDetails = async(type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}derivatives/participants/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const pullParticipantsDetails = async(type, tranId, partId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}derivatives/participants/${type}/${tranId}?id=${partId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putDerivativeSummary  = async(type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}derivatives/transaction/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}
