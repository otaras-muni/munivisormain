import axios from "axios"
import * as types from "../types"
import { toast } from "react-toastify"
import { getToken } from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import { muniVisorApplicationStore } from "AppState/store"
import CONST from "../../../../globalutilities/consts"
import {SET_SUPERVISOR} from "../types"

export const fetchIssuers = async (queryParams, callback) => {
  try {
    const res = await axios.get(
      `${muniApiBaseURL}entity/getEntByRel/${queryParams}`,
      { headers: { Authorization: getToken() } }
    )
    let issuerList = []
    if (res.data && Array.isArray(res.data)) {
      issuerList = res.data.map(ent => ({
        ...ent,
        name: ent.firmName,
        id: ent._id
      }))
    }
    callback({ issuerList })
  } catch (err) {}
}

export const fetchAssigned = async (queryParams, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/getUsersByEnt/${queryParams}`,
      { headers: { Authorization: getToken() } }
    )
    let usersList = []
    if (response.data && Array.isArray(response.data)) {
      usersList = _.orderBy(response.data, ['userFirstName']).map(ent => {
        const newObject = {
          ...ent,
          name: `${ent.userFirstName} ${ent.userLastName}`,
          id: ent._id // eslint-disable-line
        }
        return newObject
      })
    }
    callback({ usersList })
  } catch (err) {}
}

export const fetchLeadAdvisor = async (queryParams, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/getSeries50/${queryParams}`,
      { headers: { Authorization: getToken() } }
    )
    let usersList = []
    if (response.data && Array.isArray(response.data)) {
      usersList = response.data.map(ent => {
        const newObject = {
          ...ent,
          name: `${ent.userFirstName} ${ent.userLastName}`,
          id: ent._id // eslint-disable-line
        }
        return newObject
      })
    }
    return usersList
    // callback({ usersList })

  } catch (err) {}
}

export const fetchUserTransactions = async clientId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}rfp/transactions/${clientId}`,
      { headers: { Authorization: getToken() } }
    )
    return {
      transactions: response.data || []
    }
  } catch (error) {
    console.log(error)
    return {
      transactions: []
    }
  }
}

export const getUserTransactions = async type => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}common/transactions/${type}`,
      { headers: { Authorization: getToken() } }
    )
    const transactions = response.data || []
    if (transactions.length) {
      transactions.forEach(tran => {
        ;(tran.name = tran.activityDescription || ""),
          (tran.id = tran.activityId || "")
      })
    }
    console.log("GET TRANSACTIONS ", transactions)
    return transactions
  } catch (error) {
    console.log(error)
    return []
  }
}

export const pullRelatedTransactions = async query => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}common/transactions/${query}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data || []
  } catch (error) {
    console.log(error)
    return []
  }
}

export const fetchEntNotEqSelf = async (clientId, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/getEntNotEqSelf/${clientId}`,
      { headers: { Authorization: getToken() } }
    )
    let entities = []
    if (response.data && Array.isArray(response.data)) {
      entities = response.data.map(ent => {
        const newObject = {
          ...ent,
          name: ent.firmName,
          id: ent._id // eslint-disable-line
        }
        return newObject
      })
    }
    callback(entities || [])
  } catch (error) {
    callback({ error })
  }
}

export const createTransaction = async (body, callback) => {
  try {
    const transType = body.rfpTranType
    if (transType === "Manage Deal/Issue for Client") {
      const deals = {
        dealIssueTranClientId: body.rfpTranClientId, // this is the financial advisor who is the client of munivisor
        dealIssueTranIssuerId: body.rfpTranIssuerId, // this is the issuer client
        dealIssueTranClientFirmName: body.rfpTranClientFirmName,
        dealIssueTranClientMSRBType: body.rfpTranClientMSRBType,
        dealIssueTranIssuerFirmName: body.rfpTranIssuerFirmName,
        dealIssueTranIssuerMSRBType: body.rfpTranIssuerMSRBType,
        dealIssueTranName: body.rfpTranName,
        dealIssueTranType: body.rfpTranType,
        dealIssueTranPurposeOfRequest: body.rfpTranPurposeOfRequest,
        dealIssueTranAssignedTo: body.rfpTranAssignedTo,
        dealIssueTranRelatedTo: body.rfpTranRelatedTo,
        dealIssueTranState: body.rfpTranState,
        dealIssueTranCounty: body.rfpTranCounty,
        dealIssueTranPrimarySector: body.rfpTranPrimarySector,
        dealIssueTranSecondarySector: body.rfpTranSecondarySector,
        dealIssueTranDateHired: body.rfpTranDateHired,
        dealIssueTranStartDate: body.rfpTranStartDate,
        dealIssueTranExpectedEndDate: body.rfpTranExpectedEndDate,
        dealIssueTranStatus: body.rfpTranStatus,
        dealIssueTransNotes: body.rfpTransNotes,
        updateUser: body.updateUser
      }
      const response = await axios.post(`${muniApiBaseURL}deals`, deals, {
        headers: { Authorization: getToken() }
      })
      callback(response)
    } else if (transType === "Manage RFP for Client") {
      const response = await axios.post(`${muniApiBaseURL}rfp`, body, {
        headers: { Authorization: getToken() }
      })
      callback(response)
    }
  } catch (error) {
    callback({ error })
  }
}

export const addRfpQuestion = async (tranId, payload, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}rfp/question/${tranId}`,
      payload,
      { headers: { Authorization: getToken() } }
    )
    callback({ response })
  } catch (error) {
    callback({ error })
  }
}

export const addRfpResponse = async (tranId, payload, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}rfp/response/${tranId}`,
      payload,
      { headers: { Authorization: getToken() } }
    )
    callback({ response })
  } catch (error) {
    callback({ error })
  }
}

export const createTranEmailAlert = async payload => {
  try {
    payload.type =
      payload.type === "loan"
        ? "bankloans"
        : payload.type === "marfp"
        ? "marfps"
        : payload.type === "derivative"
        ? "derivatives"
        : payload.type === "rfp"
        ? "rfps"
        : payload.type === "others"
        ? "others"
        : payload.type === "businessDevelopment"
        ? "busdev"
        : payload.type

    const types = [
      "deals",
      "rfps",
      "bankloans",
      "marfps",
      "derivatives",
      "others",
      "busdev"
    ]
    if (
      payload &&
      payload.tranId &&
      payload.type &&
      payload.emailParams &&
      types.indexOf(payload.type) !== -1
    ) {
      console.log("=============create tran email payload========", payload)
      const response = await axios.post(
        `${muniApiBaseURL}emails/createtranemail`,
        payload,
        { headers: { Authorization: getToken() } }
      )
      if (response && response.data && !response.data.done) {
        toast("Sorry, email not sent", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      } else {
        toast("E-mail Sent Successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
      }
    }
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
  }
}

export const checkSupervisorControls = async (userUpdate) => {
  try {
    const state = muniVisorApplicationStore.getState()
    if((state && state.supervisor && Object.keys(state.supervisor).length) && !userUpdate){
      return state.supervisor
    }
    const res = await axios.get(`${muniApiBaseURL}controls/check-supervisor`, {
      headers: { Authorization: getToken() }
    })
    if (res && res.data) {
      muniVisorApplicationStore.dispatch({
        type: SET_SUPERVISOR,
        payload: res.data
      })
      return res.data
    }
    console.log("err in getting check-supervisor")
    return {}
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
  }
}

export const getURLsForTenant = async (dispatch, tenantID) => {
  try {
    const result = await axios.get(
      `${muniApiBaseURL}common/geturlsfortenant/${tenantID}`,
      { headers: { Authorization: getToken() } }
    )
    if (result && result.data) {
      dispatch({
        type: types.GET_URLS_FOR_FORNTEND,
        payload: result.data.urlObject
      })
    }
    return true
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
  }
}

export const makeCopyOfTransaction = async payload => (
  await axios.post(
    `${muniApiBaseURL}common/makecopy`,
    payload,
    { headers: { Authorization: getToken() } }
  )
)
