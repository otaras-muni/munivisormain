import axios from "axios"
import {toast} from "react-toastify"

import { updateAuditLog, checkComplianceEntitlement } from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {
CHECK_CAC_SUPERVISOR,
GET_CONTROLS,
ADD_NEW_CONTROL,
SAVE_CONTROLS
} from "../types"
import {getToken} from "../../../../globalutilities/helpers"
import CONST from "../../../../globalutilities/consts"

export const checkCACSupervisor = (token, stateResult) => async (dispatch) => {
  // dispatch({ type: CHECK_CAC_SUPERVISOR, payload: false})
  let result = stateResult
  if(!stateResult){
    result = await checkComplianceEntitlement(token)
  }
  console.log("result : ", result)
  if(result && result.supervisor) {
    dispatch({ type: CHECK_CAC_SUPERVISOR, payload: true})
  } else {
    dispatch({ type: CHECK_CAC_SUPERVISOR, payload: false})
  }
}

export const addNewControl = payload => ({type: ADD_NEW_CONTROL, payload})

export const dispatchGetControls = (dispatch, getState, payload) => {
  const { controls } = getState()
  console.log("controls : ", controls)
  dispatch({type: GET_CONTROLS, payload})
}

export const getControls = (token, supervisor) => async (dispatch, getState) => {
  token = token || localStorage.getItem("token")
  console.log("in action getControls")
  let res
  const apiURL = supervisor ? `${muniApiBaseURL}controls` : `${muniApiBaseURL}controls-actions`
  try {
    res = await axios.get(apiURL, {headers:{"Authorization":token}})
    let payload = []
    if(res && res.status === 200 && res.data) {
      console.log("res : ", res)
      payload = (supervisor ? res.data.controls : res.data) || []
    }
    dispatchGetControls(dispatch, getState, payload)
  } catch (err) {
    console.log("err in getControls: ", err)
    dispatchGetControls(dispatch, getState, [])
  }
}

export const dispatchSaveControls = (dispatch, data) => {
  const payload = data && data.filter(e => e.id)
  dispatch({
    type: SAVE_CONTROLS,
    payload
  })
}

export const fetchAllControls = async () => {
  try {
    const res = await axios.get(`${muniApiBaseURL}controls/getControls`, {headers:{"Authorization": getToken()}})
    if (res && res.data) {
      return res.data
    }
  } catch (error) {
    return error
  }
}

export const fetchSpecificControls = async (id) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}controls/getSpecificControl?id=${id}`, {headers:{"Authorization": getToken()}})
    if (res && res.data && res.data.controls) {
      return res.data.controls[0]
    }
  } catch (error) {
    return error
  }
}

export const saveControls = (saveType, token, entityId, controls, callback, changeLog, target) => async (dispatch) => {
  console.log("in action saveControls : ", controls)
  callback = callback || (() => {})
  if(target === "redux-store") {
    dispatchSaveControls(dispatch, controls)
    return
  }
  try {
    const res = await axios.post(`${muniApiBaseURL}controls/saveControl?id=${entityId}&type=${saveType}`, {controls}, {headers:{"Authorization":token}})
    if (res && res.data) {
      await updateAuditLog("cac", changeLog)
      const controls = res.data && res.data.controls && res.data.controls[0]
      callback(controls)
      // dispatchSaveControls(dispatch, controls)
    }

    /* const res = await axios.get(`${muniApiBaseURL}controls`, {headers:{"Authorization":token}})
    console.log("res : ", res)
    if(res && res.status >= 200 && res.status < 300 && res.data && res.data._id) {
      const controlsObj = { ...res.data, controls }
      const { _id } = controlsObj
      console.log("_id : ", _id)
      const res1 = await axios.put(`${muniApiBaseURL}controls/${_id}`, controlsObj)
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300) {
        await updateAuditLog("cac", changeLog)
        dispatchSaveControls(dispatch, controls)
        callback()
      } else {
        console.log("err in saveControls put: ")
        callback("Error")
      }
    } else {
      const controlsObj = { entityId, controls }
      const res1 = await axios.post(`${muniApiBaseURL}controls/`, controlsObj)
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300) {
        await updateAuditLog("cac", changeLog)
        dispatchSaveControls(dispatch, controls)
        callback()
      } else {
        console.log("err in saveControls post: ")
        callback("Error")
      }
    } */
  } catch (err) {
    console.log("err in saveControls: ", err)
    callback("Error")
  }
}

export const saveDisableControls = (saveType, token, entityId, controls, callback, changeLog, target) => async (dispatch) => {
  console.log("in action saveControls : ", controls)
  callback = callback || (() => {})
  if(target === "redux-store") {
    dispatchSaveControls(dispatch, controls)
    return
  }
  try {
    const res = await axios.post(`${muniApiBaseURL}controls/saveDisableControls?id=${entityId}&type=${saveType}`, {controls}, {headers:{"Authorization":token}})
    if (res && res.data) {
      await updateAuditLog("cac", changeLog)
      const controls = res.data && res.data.controls && res.data.controls[0]
      callback(controls)
      // dispatchSaveControls(dispatch, controls)
    }

  } catch (err) {
    console.log("err in saveControls: ", err)
    callback("Error")
  }
}

export const saveControlsStatus = (token, entityId, status, changeLog) => async (dispatch, getState) => {
  console.log("in action saveControlsStatus : ", status)
  try {
    const res = await axios.get(`${muniApiBaseURL}controls`, {headers:{"Authorization":token}})
    console.log("res : ", res)
    if(res && res.status >= 200 && res.status < 300 && res.data && res.data._id) {
      const controlsObj = { ...res.data }
      const { _id } = controlsObj
      console.log("_id : ", _id)
      const controls = [ ...controlsObj.controls ]
      controls.forEach((e, i) => {
        const idx = status.findIndex(d => d.refId.toString() === e.refId)
        if(idx > -1) {
          controls[i] = { ...e, status: status[idx].status, dueDate: status[idx].dueDate }
        }
      })
      controlsObj.controls = controls
      const res1 = await axios.put(`${muniApiBaseURL}controls/${_id}`, controlsObj)
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300) {
        await updateAuditLog("cac", changeLog)
        // dispatchSaveControls(dispatch, controls)
      } else {
        console.log("err in saveControlsStatus put: ")
      }
    } else {
      console.log("controls not found in saveControlsStatus")
    }
  } catch (err) {
    console.log("err in saveControlsStatus: ", err)
  }
}

export const saveControlsAction = (token, id, control, changeLog, callback) => async (dispatch, getState) => {
  token = token || localStorage.getItem("token")
  console.log("in saveControlsAction : ", id, control)
  callback = callback || (() => {})
  if(!id || !token) {
    console.log("no id provided")
    return
  }
  // const { docsActions, checklist, comments, status } = data
  try {
    const { _id } = control
    if(_id) {
      console.log("_id : ", _id)
      const res1 = await axios.put(`${muniApiBaseURL}controls-actions/${_id}`, control,  {headers:{"Authorization":getToken()}})
      console.log("res1 : ", res1)
      if (res1 && res1.status >= 200 && res1.status < 300 && res1.data) {
        await updateAuditLog("cac", changeLog)
        callback("", res1 && res1.data)
        const { status, parentId } = control

        /*  let { controls } = getState()
        console.log("controls : ", controls)
        controls = [...controls ]
        const idx = controls.findIndex(e => e._id === id)
        if(idx > -1) {
          controls[idx] = control
          dispatchSaveControls(dispatch, controls)
          if(status !== "complete") {
            callback()
          }
        } else {
          console.log("should not happen")
          dispatchSaveControls(dispatch, [ ...controls, control ])
        } */

        // if(parentId && (status === "complete")) {
          /* const statusObj = { parentId, status }
          const res2 = await axios.post(`${muniApiBaseURL}controls/action-status?id=${_id}`, statusObj, {headers:{"Authorization": token}})
          if (res2 && res2.status >= 200 && res2.status < 300 && res2.data) {
          callback("", res2 && res2.data) */

          /* console.log("control action status update ok")
          const getTask = await axios.get(`${muniApiBaseURL}tasks/getSpecificTask?id=${_id}`, { headers: { Authorization: token || getToken() } })
          const taskId = getTask.data && getTask.data._id || ""
          if(taskId) {
            const payload = {"taskDetails.taskStatus": "Closed"}
            await axios.post(`${muniApiBaseURL}tasks/update-from-ui?tid=${taskId}`, payload,
              { headers: { Authorization: getToken() }})
          } */
        /* } else {
          console.log("err in control action status update : ", err)
          callback("Error")
        } */
        // }
      } else {
        console.log("err in saveControls put: ")
        callback("Error")
      }
    } else {
      console.log("no control found with id to update: ", id)
      callback("Error")
    }
  } catch(err) {
    console.log("err in saveControls: ", err)
    callback("Error")
  }
}

export const getControlsActions = async (token) => {
  token = token || localStorage.getItem("token")
  console.log("in action getControlsActions")
  let res
  const apiURL = `${muniApiBaseURL}controls-actions`
  try {
    res = await axios.get(apiURL, {headers:{"Authorization":token}})
    let payload = []
    if(res && res.status === 200 && res.data) {
      console.log("res : ", res)
      payload = (res.data) || []
    }
    return payload
    // dispatchGetControls(dispatch, getState, payload)
  } catch (err) {
    console.log("err in getControls: ", err)
    // dispatchGetControls(dispatch, getState, [])
  }
}

export const fetchSpecificControlsActions = async (id) => {
  try {
    const res = await axios.get(`${muniApiBaseURL}controls-actions/fetchSpecificControlsActions?id=${id}`, {headers:{"Authorization": getToken()}})
    if (res && res.data) {
      return res.data
    }
  } catch (error) {
    return error
  }
}

export const fetchControlsMonitor = async () => {
  try {
    const res = await axios.get(`${muniApiBaseURL}controls/fetchControlsMonitor`, {headers:{"Authorization": getToken()}})
    if (res && res.data) {
      return res.data
    }
  } catch (error) {
    return error
  }
}

export const putCACDashboard  = async(body, log) => {
  const {id, name} = body
  try {
    const response = await axios.post(`${muniApiBaseURL}controls/updateMonitor?id=${id}&type=${name}`, body, {headers:{"Authorization":getToken()}})
    if(response.status === 200){
      await updateAuditLog("cac", log)
      toast(`CAC ${name === "dueDate" ? "Due date" : "Status"} updated successfully`, { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
    }else {
      toast("Something went wrong please try a letter", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
    }
    return response
  } catch (error) {
    toast("Something went wrong please try a letter", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
    return error
  }
}
