import axios from "axios"
import { muniApiBaseURL, token } from "GlobalUtils/consts.js"
import {getToken} from "GlobalUtils/helpers"

export const getBussinessDevTask = async (entityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}tasks/activityTasks?activityId=${entityId}`, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const bussinessActivityDetails = async (activityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}busdev/${activityId}`, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const postBussinessActivityDetails = async (body) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}tasks/activityTasks`, body, {headers:{"Authorization":getToken()}} )
    return response
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putTaskDocumentTransaction = async (body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}tasks/transaction/${body._id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const putTaskDocumentTransactionStatus = async (sectionId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}tasks/transaction/${sectionId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const delTransactionDocStatuse = async (entityId, documentId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}tasks/transaction/${entityId}?documentId=${documentId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const searchTaskByTranEntity = async (searchText) => {
  try {
    const query = searchText ? `?text=${searchText}` : ""
    const response = await axios.get(`${muniApiBaseURL}busdev/tasks/search${query}`, {headers:{"Authorization":getToken()}})
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}
