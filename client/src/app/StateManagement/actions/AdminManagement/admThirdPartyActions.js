import axios from "axios"
import * as types from "./actionsType"
import { muniApiBaseURL } from "GlobalUtils/consts.js"

export const saveThirdPartyFirmDetail = ({firmDetails}) => async dispatch => {
  if (firmDetails._id) {
    const res1 = await axios.put(
      `${muniApiBaseURL}api/adminfirm/${firmDetails._id}`,
      { firmDetails }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      res1.data.error
        ? dispatch({ type: types.THIRD_PARTY_FIRM_ERROR, payload: res1.data.error })
        : dispatch({ type: types.THIRD_PARTY_UPDATE_FIRM_DETAIL, payload: res1.data })
    }
  } else {
    axios
      .post(`${muniApiBaseURL}api/adminfirm`, {
        firmDetails
      })
      .then(result => {
        dispatch({ type: types.THIRD_PARTY_NEW_FIRM_DETAIL, payload: result.data })
      })
      .catch(err => {
        dispatch({ type: types.THIRD_PARTY_FIRM_ERROR, payload: result.data.error })
      })
  }
}
  
export const getThirdPartyFirmDetailById = firmId => function(dispatch) {
  axios.get(`${muniApiBaseURL}api/adminfirm/${firmId}`).then(result => {
    dispatch({ type: types.THIRD_PARTY_FIRM_DETAIL_BY_ID, payload: result.data })
  })
}
export const getAllThirdPartyFirmList = entityId => dispatch => {
  axios.get(`${muniApiBaseURL}api/adminfirm/firms/Third Party/5b1a63bd471a1919e0ca97d4`)
    .then(result => {
      console.log("Result===>>>",result)
      dispatch({ type: types.THIRD_PARTY_FIRM_LIST, payload: result.data })
    })
}
