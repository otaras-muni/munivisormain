import axios from "axios"
import * as types from "./actionsType"
import { muniApiBaseURL } from "GlobalUtils/consts.js"

export const getAllThirdPartyFirmList = () => (dispatch) => {
  axios.get(`${muniApiBaseURL}third-parties`).then((result) => {
    console.log("Firm List==>>", result.data)
    dispatch({ type: types.ALL_FIRM_LIST, payload: result.data })
  })
}
