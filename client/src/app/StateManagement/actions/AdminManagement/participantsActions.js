import axios from "axios"
import * as types from "./actionsParticipants"
import { muniApiBaseURL } from "GlobalUtils/consts.js"

export const addDealParticipants = (participants) => {
  console.log("test action", participants)
  return function (dispatch) {
    axios.put(`${muniApiBaseURL}participantsteam`,
      {details:participants.details}
    ).then((result) => {	
      console.log("Result===>>>")
      dispatch({ type: types.Part_UT_FRM, payload: result.data})
    })
  } 
}
