import axios from "axios"
import CONST, { muniApiBaseURL } from "GlobalUtils/consts.js"
import { inheritPlatformConfig, getToken } from "GlobalUtils/helpers"
import { toast } from "react-toastify"
import * as types from "./actionsType"

const token = localStorage.getItem("token")

export const sendEmailAlert = async userId => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}emails/sendonboardingemail/${userId}`,
      {},
      { headers: { Authorization: getToken() } }
    )
    if (response && response.data && !response.data.done && ["Self"].includes(response.data.type)) {
      toast("Problem sending onboarding emails to Firm users", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
    else if (response && response.data && response.data.done) {
      toast("Email has send successfully.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
    }
  } catch (err) {
    console.log(err)
    toast(err.message, {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.ERROR
    })
  }
}

export const changeUsersStatus = async (body) => {
  try {
    const res = await axios.post(`${muniApiBaseURL}entityUser/update-user-status`, body, {headers:{ Authorization: getToken() }})
    if(res && res.data) {
      return res.data
    }
  } catch (error) {
    return error
  }
}

export const saveFirmDetail = firmDetails => async dispatch => {
  if (firmDetails._id) {
    const res1 = await axios.put(
      `${muniApiBaseURL}entity/firms/${firmDetails._id}`,
      { firmDetails },
      { headers: { Authorization: getToken() } }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      res1.data.error
        ? dispatch({ type: types.FIRM_ERROR, payload: res1.data.error })
        : dispatch({ type: types.UPDATED_FIRM_DETAIL, payload: res1.data })
    }
  } else {
    axios
      .post(
        `${muniApiBaseURL}entity/firms`,
        { firmDetails },
        { headers: { Authorization: getToken() } }
      )
      .then(result => {
        dispatch({ type: types.NEW_ADDED_FIRM_DETAIL, payload: result.data })
      })
      .catch(err, result => {
        dispatch({ type: types.FIRM_ERROR, payload: result.data.error })
      })
  }
}

export const getFirmDetailById = firmId => dispatch => {
  axios
    .get(`${muniApiBaseURL}entity/firms/${firmId}`, {
      headers: { Authorization: getToken() }
    })
    .then(result => {
      dispatch({ type: types.FIRM_DETAIL_BY_ID, payload: result.data })
    })
}

export const getAllFirmList = () => dispatch => {
  axios
    .get(`${muniApiBaseURL}entity/firms`, {
      headers: { Authorization: getToken() }
    })
    .then(result => {
      dispatch({ type: types.ADM_ALL_FIRM_LIST, payload: result.data })
    })
}

export const getAllThirdPartyFirmList = entityId => dispatch => {
  axios
    .get(`${muniApiBaseURL}entity/third-parties/entityId/${entityId}`, {
      headers: { Authorization: getToken() }
    })
    .then(result => {
      dispatch({ type: types.THIRD_PARTY_FIRM_LIST, payload: result.data })
    })
}

export const getThirdPartyFirmDetailById = firmId =>
  function (dispatch) {
    axios
      .get(`${muniApiBaseURL}entity/third-parties/${firmId}`, {
        headers: { Authorization: getToken() }
      })
      .then(result => {
        dispatch({
          type: types.THIRD_PARTY_FIRM_DETAIL_BY_ID,
          payload: result.data
        })
      })
  }

export const saveThirdPartyFirmDetail = ({
  firmDetails,
  entityId
}) => async dispatch => {
  if (firmDetails._id) {
    const res1 = await axios.put(
      `${muniApiBaseURL}entity/third-parties/${firmDetails._id}`,
      { firmDetails },
      { headers: { Authorization: getToken() } }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      res1.data.error
        ? dispatch({
          type: types.THIRD_PARTY_FIRM_ERROR,
          payload: res1.data.error
        })
        : dispatch({
          type: types.THIRD_PARTY_UPDATE_FIRM_DETAIL,
          payload: res1.data
        })
    }
  } else {
    axios
      .post(
        `${muniApiBaseURL}entity/third-parties/entityId/${entityId}`,
        { firmDetails },
        { headers: { Authorization: getToken() } }
      )
      .then(result => {
        dispatch({
          type: types.THIRD_PARTY_NEW_FIRM_DETAIL,
          payload: result.data
        })
      })
      .catch(err => {
        dispatch({
          type: types.THIRD_PARTY_FIRM_ERROR,
          payload: result.data.error
        })
      })
  }
}
export const getAllClientFirmList = entityId => dispatch => {
  axios
    .get(`${muniApiBaseURL}entity/clients/entityId/${entityId}`, {
      headers: { Authorization: getToken() }
    })
    .then(result => {
      dispatch({ type: types.CLIENT_FIRM_LIST, payload: result.data })
    })
}

export const saveClientFirmDetail = ({
  firmDetails,
  entityId
}) => async dispatch => {
  if (firmDetails._id) {
    const res1 = await axios.put(
      `${muniApiBaseURL}entity/clients/${firmDetails._id}`,
      { firmDetails },
      { headers: { Authorization: getToken() } }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      res1.data.error
        ? dispatch({ type: types.CLIENT_FIRM_ERROR, payload: res1.data.error })
        : dispatch({
          type: types.CLIENT_UPDATE_FIRM_DETAIL,
          payload: res1.data
        })
    }
  } else {
    axios
      .post(
        `${muniApiBaseURL}entity/clients/entityId/${entityId}`,
        { firmDetails },
        { headers: { Authorization: getToken() } }
      )
      .then(async result => {
        if (result.data && result.data._id) {
          const entityId = result.data._id
          const type = "self"
          const uiaccess = await axios.post(
            `${muniApiBaseURL}configs/create-ctp-config`,
            { entityId, type },
            { headers: { Authorization: getToken() } }
          )
          if (uiaccess.data && uiaccess.data.message !== "OK") {
            dispatch({
              type: types.CLIENT_FIRM_ERROR,
              payload: uiaccess.data.error
            })
          }
        }
        dispatch({ type: types.CLIENT_NEW_FIRM_DETAIL, payload: result.data })
      })
      .catch(err => {
        dispatch({ type: types.CLIENT_FIRM_ERROR, payload: err.error })
      })
  }
}

export const saveUserDetail = userDetails => async dispatch => {
  if (userDetails._id) {
    const res1 = await axios.put(
      `${muniApiBaseURL}entityUser/firms/${userDetails._id}`,
      { userDetails, mergeAddresses },
      { headers: { Authorization: getToken() } }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      res1.data.error
        ? dispatch({ type: types.USER_ERROR, payload: res1.data.error })
        : dispatch({ type: types.UPDATED_USERS_DETAIL, payload: res1.data })
    }
  } else {
    axios
      .post(
        `${muniApiBaseURL}entityUser/firms`,
        { userDetails },
        { headers: { Authorization: getToken() } }
      )
      .then(async result => {
        dispatch({ type: types.NEW_ADDED_USER_DETAIL, payload: result.data })
      })
      .catch(err => {
        dispatch({ type: types.USER_ERROR, payload: err.error })
      })
  }
}

export const getUserDetailById = userId => dispatch => {
  axios
    .get(`${muniApiBaseURL}entityUser/firms/${userId}`, {
      headers: { Authorization: getToken() }
    })
    .then(result => {
      dispatch({ type: types.USER_DETAIL_BY_ID, payload: result.data })
    })
}

export const getThirdPartyFirmUserById = userId => dispatch => {
  axios
    .get(`${muniApiBaseURL}entityUser/third-parties/${userId}`, {
      headers: { Authorization: getToken() }
    })
    .then(result => {
      dispatch({ type: types.USER_DETAIL_BY_ID, payload: result.data })
    })
}

export const findContactByName = async userName => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entityUser/user/${userName}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getUserById = async userId => {
  try {
    const response = await axios.get(`${muniApiBaseURL}users/${userId}`, { headers: { Authorization: getToken() } })
    return response
  } catch (error) {
    return error
  }
}

export const searchThirdPartyUsersList = async (id, searchVal) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entityRel/entityusersearch`,
      {
        entityId: id,
        relationshipType: "Third Party",
        searchString: searchVal
      },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const searchUsersList = async (id, searchVal) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entityRel/entityusersearch`,
      {
        entityId: id,
        relationshipType: "Self",
        searchString: searchVal
      },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}
export const searchClientUsersList = async (id, searchVal) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entityRel/entityusersearch`,
      {
        entityId: id,
        relationshipType: "Client",
        searchString: searchVal
      },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getAllUserFirmList = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/firms`, {
      headers: { Authorization: getToken() }
    })
    return response.data
  } catch (error) {
    return error
  }
}

export const getAllUserThirdPartyFirmList = async entityId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/third-parties/entityId/${entityId}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}
export const getAllUserClientFirmList = async entityId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/clients/entityId/${entityId}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}
const test = (result, firmDetail) => {
  const item1 = {}
  objResult.map((item, idx) => {
    Obj1.hasOwnProperty(item)
    item1[item] = Obj1[item]
  })
  return item1
}
export const deleteLinkCusipBorrower = async (entityId, docId, type) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/clients/deleteLinkCusipBorrower`,
      { entityId, type, docId },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}
export const searchFirms = value => dispatch =>
  axios
    .post(
      `${muniApiBaseURL}entityRel/testsearch`,
      {
        entityId: "5b1a63bd471a1919e0ca97eb",
        relationshipType: "Third Party",
        searchString: value
      },
      { headers: { Authorization: getToken() } }
    )
    .then(result => {
      const resultData = result.data.map((item, idx) => ({
        id: item.relEntityId,
        label: item.relEntMsrbFirmName
      }))
      return resultData
    })
    .catch(err => error)

export const addClientFirm = async ({ firmDetails, entityId, clientType }) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/clients/entityId/${entityId}`,
      { firmDetails, clientType },
      { headers: { Authorization: getToken() } }
    )
    console.log("===========>>>", response.data)
    if (response.data && response.data._id) {
      const entityId = response.data._id
      const type = clientType
      const uiaccess = await axios.post(
        `${muniApiBaseURL}configs/create-ctp-config`,
        { entityId, type },
        { headers: { Authorization: getToken() } }
      )
      if (uiaccess.data && uiaccess.data.message !== "OK") {
        return uiaccess.data.error
      }
    }
    return response.data
  } catch (error) {
    return error
  }
}
export const addTenetsFirm = async ({ firmDetails }) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/firms`,
      { firmDetails },
      { headers: { Authorization: getToken() } }
    )
    inheritPlatformConfig(response.data._id, token)
    return response.data
  } catch (error) {
    return error
  }
}

export const updateTenetsFirm = async ({ firmDetails, mergeAddresses }) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}entity/firms/${firmDetails._id}`,
      { firmDetails, mergeAddresses },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}
export const updateFirmLogoAndAuditSetting = async (payload) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/firms/${payload._id}`,
      payload,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}
export const addThirdPartyFirm = async ({ firmDetails, entityId }) => {
  try {
    const clientType = "Third Party"
    const response = await axios.post(
      `${muniApiBaseURL}entity/third-parties/entityId/${entityId}`,
      { firmDetails, clientType },
      { headers: { Authorization: getToken() } }
    )
    if (response.data && response.data._id) {
      const entityId = response.data._id
      const type = clientType
      const uiaccess = await axios.post(
        `${muniApiBaseURL}configs/create-ctp-config`,
        { entityId, type },
        { headers: { Authorization: getToken() } }
      )
      if (uiaccess.data && uiaccess.data.message !== "OK") {
        return uiaccess.data.error
      }
    }
    return response.data
  } catch (error) {
    return error
  }
}

export const updateThirdPartyFirm = async ({ firmDetails, mergeAddresses }) => {
  try {
    const res1 = await axios.put(
      `${muniApiBaseURL}entity/third-parties/${firmDetails._id}`,
      { firmDetails, mergeAddresses },
      { headers: { Authorization: getToken() } }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      return res1.data
    }
  } catch (error) {
    return error
  }
}

export const updateClientFirm = async ({
  firmDetails,
  mergeAddresses /*, mergeEntityLinkedCusips, mergeEntityBorObl*/
}) => {
  try {
    const res1 = await axios.put(
      `${muniApiBaseURL}entity/clients/${firmDetails._id}`,
      {
        firmDetails,
        mergeAddresses /*, mergeEntityLinkedCusips, mergeEntityBorObl*/
      },
      { headers: { Authorization: getToken() } }
    )
    if (res1 && res1.status >= 200 && res1.status < 300) {
      return res1.data
    }
  } catch (error) {
    return error
  }
}
export const addContactDetail = async userDetails => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entityUser/firms`,
      { userDetails },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const clientProspectFirmList = async payload => {
  try {
    const payload = payload === undefined ? "" : payload
    const response = await axios.post(
      `${muniApiBaseURL}entity/entityprospect`,
      payload,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getEntityList = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}dashboard/entitylist`,
      filteredValue,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getProjectsList = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}dashboard/projectsnew`,
      filteredValue,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const toggleReadStatus = async id => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}tasks/toggletask/` + id,
      {},
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getUserList = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}dashboard/userlist`,
      filteredValue,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getTasksTransactions = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}dashboard/taskstransactions`,
      filteredValue,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getTasksOthers = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}dashboard/tasksothers`,
      filteredValue,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const thirdPartyFirmList = async (entityId, filteredVal) => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}entity/entity-third-parties`,
      { entityId, filteredValue },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const entityUsersList = async (entityId, filteredVal) => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    const response = await axios.post(
      `${muniApiBaseURL}entityRel/entityUsers`,
      { entityId, filteredValue },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getEntityUsersListById = async entityId => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entityRel/entityusersearch`,
      { entityId, relationshipType: "Client" },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getFirmById = async firmId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/firms/${firmId}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return { error: error.response.data }
  }
}

export const getClientUserDetailById = async userId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entityUser/third-parties/${userId}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const updateUserDetailById = async (userDetails, mergeAddresses) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}entityUser/third-parties/${userDetails._id}`,
      { userDetails, mergeAddresses },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    const { response } = error
    return response.data
  }
}

export const checkDuplicateEntity = async entityVal => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/checkduplicateEntity`,
      { entityVal },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getClientFirmDetailById = firmId =>
  function (dispatch) {
    axios
      .get(`${muniApiBaseURL}entity/clients/${firmId}`, {
        headers: { Authorization: getToken() }
      })
      .then(result => {
        dispatch({ type: types.CLIENT_FIRM_DETAIL_BY_ID, payload: result.data })
      })
  }

export const getClientEntityDetailById = async firmId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/clients/${firmId}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getAllBusinessActivity = async entityId => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/business-activity`,
      { entityId },
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getAllEntity = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/allEnt`, {
      headers: { Authorization: getToken() }
    })
    return response.data
  } catch (error) {
    return error
  }
}

export const getAllTenetsEntity = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/allTenEnt`, {
      headers: { Authorization: getToken() }
    })
    return response.data
  } catch (error) {
    return error
  }
}

export const saveAddOns = async (firmAddOns, entityId) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}entity/firmAddOns/${entityId}`,
      { firmAddOns },
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getFirmUserById = async userId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entityUser/firms/${userId}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const checkPlatFormAdmin = async () => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entityUser/checkPlatFormAdmin`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const saveUserAddOns = async (userAddOns, userId) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}entityUser/userAddOns/${userId}`,
      { userAddOns },
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const saveMasterListSearchPrefs = async (
  contextname,
  searchname,
  searchPreference
) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/context/${contextname}`,
      { searchname, searchPreference },
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const fetchMasterListSearchPrefs = async contextname => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/context/${contextname}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const deleteMasterListSearchPrefs = async prefId => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/deleteprefsearch`,
      { prefId },
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const changeAddressStatus = async (
  entityId,
  addressId,
  field,
  status
) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/changeAddressStatus`,
      { entityId, addressId, field, status },
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const saveEntityDocs = async (docs, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}entity/docs/${docs._id}`,
      { entityDocuments: docs.documents },
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const updateEntityDocs = async (entId, query, docs) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}entity/docs/${entId}${query}`,
      docs,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getEntityDocs = async (entId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/docs/${entId}`, {
      headers: { Authorization: getToken() }
    })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const deleteEntityDoc = async (entId, docId) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}entity/docs/${entId}?docId=${docId}`,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getIssuerList = async issuerFilter => {
  try {
    const filter = { ...issuerFilter }
    if (filter.participantType === "Municipal Advisor") {
      filter.participantType = "Municipal Advisors"
    }
    if (filter.participantType === "501c3 - Obligor") {
      filter.participantType = "Governmental Entity / Issuer"
    }
    const response = await axios.post(
      `${muniApiBaseURL}entity/issuers`,
      filter,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getGlobalRefIssuer = async issuerFilter => {
  try {
    const filter = { ...issuerFilter }
    if (filter.participantType === "Municipal Advisor") {
      filter.participantType = "Municipal Advisors"
    }
    if (filter.participantType === "501c3 - Obligor") {
      filter.participantType = "Governmental Entity / Issuer"
    }
    const response = await axios.post(
      `${muniApiBaseURL}entity/allIssuers`,
      filter,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getAllTransactions = async (searchterm = "") => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}dashboard/projects/?searchterm=${searchterm}`,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const getTaskDetailById = async taskId => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}tasks/taskId/${taskId}`,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const postUserDocument = async (userId, payload) => {
  try {
    return await axios.post(
      `${muniApiBaseURL}entityUser/document/${userId}`,
      payload,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    return error
  }
}

export const putUserDocumentStatus = async (userId, query, payload) => {
  try {
    return await axios.put(
      `${muniApiBaseURL}entityUser/document/${userId}${query}`,
      payload,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    return error
  }
}

export const pullUserDocument = async (userId, docId) => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}entityUser/document/${userId}?docId=${docId}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    return error
  }
}
