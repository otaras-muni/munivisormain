import CONST from "../../../../globalutilities/consts"
import cloneDeep from "lodash.clonedeep"

export const getRfpEvaluationCategories = ({rfpEvaluationCategories, evalNotes}) => {

  const evalCategories = cloneDeep(CONST.EvalCategories.slice(0))
  if (rfpEvaluationCategories) {
    Object.keys(rfpEvaluationCategories).forEach(evalCat => {
      const category = evalCategories.find(cat => cat.key === evalCat)
      if (category && category.listItems) {
        rfpEvaluationCategories[evalCat].forEach(evalObj => {
          if (evalObj.evalItem && !category.listItems.find(x => x.name === evalObj.evalItem)) {
            category.listItems.push({
              name: evalObj.evalItem,
              priorities: ["High", "Medium", "Low"],
            })
          }
        })
      }
    })
  }
  return {
    rfpEvaluationCategories,
    evalNotes,
    evalCategories,
  }
}
