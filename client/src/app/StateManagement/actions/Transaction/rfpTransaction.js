import axios from "axios"

import {getToken} from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {GET_RFP_TRANSACTION_EVAL, RFP_TRANSACTION_STATE_CHANGE, GET_RFP_TRANSACTION_EVAL_FETCHING} from "../types"



export const fetchRfpTransaction = (queryParams) => async dispatch => {
  try {
    dispatch({type: GET_RFP_TRANSACTION_EVAL_FETCHING, payload: true})

    const response = await axios.get(`${muniApiBaseURL}rfp${queryParams}`, {headers:{"Authorization":getToken()}})
    let rfpEvaluations = []
    let rfpSelEvalCommittee = []
    let rfpParticipantDist = []
    if (response && response.data) {
      rfpEvaluations = response.data.rfpEvaluations || []
      rfpSelEvalCommittee = response.data.rfpSelEvalCommittee || []
      rfpParticipantDist = response.data.rfpParticipantDist || []
    }
    const payload = {
      rfpEvaluations,
      rfpSelEvalCommittee,
      rfpParticipantDist
    }
    dispatch({type: GET_RFP_TRANSACTION_EVAL, payload})
  } catch (err) {
    dispatch({type: GET_RFP_TRANSACTION_EVAL_FETCHING, payload: false})
  }
}

export const fetchRfpContacts = async(queryParams, cb) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp${queryParams}`, {headers:{"Authorization":getToken()}})
    let rfpSelEvalCommittee = []
    let rfpParticipantDist = []
    if (response && response.data) {
      rfpSelEvalCommittee = response.data.rfpEvaluationTeam || []
      rfpParticipantDist = response.data.rfpParticipants || []
    }
    const payload = {
      rfpSelEvalCommittee,
      rfpParticipantDist
    }

    cb({
      ...payload
    })

  } catch (err) {}
}

export const fetchRfpEvaluations = (queryParams) => async dispatch => {
  try {
    dispatch({type: GET_RFP_TRANSACTION_EVAL_FETCHING, payload: true})
    const response = await axios.get(`${muniApiBaseURL}rfp${queryParams}`, {headers:{"Authorization":getToken()}})
    let rfpEvaluationCategories = {
      costOfService: [],
      qualifications: [],
      financingApproach: [],
      legalIssues: []
    }
    let evalNotes = ""

    if (response && response.data && (response.data.rfpEvaluations || []).length > 0) {
      rfpEvaluationCategories = (response.data.rfpEvaluations[0].rfpEvaluationCategories) || rfpEvaluationCategories
      evalNotes = (response.data.rfpEvaluations[0].evalNotes) || ""
    }
    const payload = {
      rfpEvaluationCategories,
      evalNotes
    }

    dispatch({type: GET_RFP_TRANSACTION_EVAL, payload})

  } catch (err) {
    dispatch({type: GET_RFP_TRANSACTION_EVAL_FETCHING, payload: false})
  }
}

export const fetchRfpEvaluations1 = async(queryParams, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp${queryParams}`, {headers:{"Authorization":getToken()}})
    let rfpMemberEvaluations = {}
    if (response && response.data && response.data.rfpMemberEvaluations) {
      rfpMemberEvaluations = response.data.rfpMemberEvaluations || {}
    }
    callback(rfpMemberEvaluations)
  } catch (err) {
    callback(err)
  }
}

export const postRfpTransaction = async (payload) => {
  try {
    return await axios.post(`${muniApiBaseURL}rfp/rfpEvaluation`, {...payload, _id: (payload && payload.transactionId) || ""}, {headers:{"Authorization":getToken()}})
  } catch (err) {
    return err
  }
}

export const fetchTransactionQueMedia = async (payload, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp/transaction/media?transId=${payload.transId}&queId=${payload.queId}`, {headers:{"Authorization":getToken()}})
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}

export const postRfpMedia = async(cred, payload, changeLog, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}rfp/transaction/media?transId=${cred.transId}&queId=${cred.queId}`, payload, {headers:{"Authorization":getToken()}})
    callback(response.data)
    // if(changeLog.length) {   const ipRes = await
    // axios.get("https://api.ipify.org/?format=json")   if(ipRes && ipRes.data &&
    // ipRes.data.ip) {     changeLog.forEach(x => {       x.ip = ipRes.data.ip })
    // }   await updateAuditLog(cred.transId, changeLog) }
  } catch (error) {
    callback({error})
  }
}

export const fetchTrnsactionResponse = async(transId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp/response/${transId}`, {headers:{"Authorization":getToken()}})
    let data = response
    if (response && Array.isArray(response.data)) {
      data = response.data.length
        ? response.data[0]
        : response.data
    }
    callback(data)
  } catch (error) {
    callback({error})
  }
}

export const fetchDocDetails = async(docId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}docs/${docId}`, {headers:{"Authorization":getToken()}})
    return response.data
  } catch (error) {
    return error
  }
}

export const getS3FileGetURL = (bucketName, fileName, callback) => {
  if (!bucketName || !fileName) {
    this.setState({
      signedS3Url: "",
      showModal: false,
      showDSModal: false,
      signerEmail: "",
      showSigneeInput: "",
      error: "No bucket/file name provided",
      waiting: false
    })
    return
  }

  const opType = "download"
  axios
    .post(`${muniApiBaseURL}api/s3/get-signed-url`, {opType, bucketName, fileName}, {headers:{"Authorization":getToken()}})
    .then(res => {
      const signedS3Url = res.data.url
      callback({signedS3Url, showModal: false, showDSModal: false, error: "", fileName: ""})
    })
    .catch(err => {
      callback({
        signedS3Url: "",
        showModal: false,
        showDSModal: false,
        signerEmail: "",
        showSigneeInput: "",
        error: "Error!!",
        waiting: false
      })
    })
}

export const fetchDecideDetailsByTranId = async(transId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}rfp/decide/${transId}`, {headers:{"Authorization":getToken()}})
    return response.data
  } catch (error) {
    return error
  }
}

export const putRfpTransaction = async (type, transId, payload, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}rfp/transaction/${type}/${transId}`, payload, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const postRFPNotes = async(tranId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}rfp/notes/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putRfpEvaluationDocuments = async (query, payload) => {
  try {
    return await axios.put(`${muniApiBaseURL}rfp/rfpEvaluation${query}`, payload, {headers:{"Authorization":getToken()}})
  } catch (error) {
    return error
  }
}

export const pullRfpEvaluationDocument = async (query) => {
  try {
    return await axios.delete(`${muniApiBaseURL}rfp/rfpEvaluation${query}`, {headers:{"Authorization":getToken()}})
  } catch (error) {
    return error
  }
}

export const postProcesstasks = async(processId) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}tasks/processtasks`, {processId}, {headers:{"Authorization":getToken()}})
    if(response.status === 200) {
      console.log(response.data)
    }else {
      console.log(response)
    }
  } catch (error) {
    console.log(error.message)
  }
}

export function updateState(payload) {
  return {type: RFP_TRANSACTION_STATE_CHANGE, payload}
}
