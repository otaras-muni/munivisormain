import {toast} from "react-toastify"
import {getDeals, getToken} from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import axios from "axios"

import {GET_ALL_DEALS} from "../types"
import CONST from "../../../../globalutilities/consts"

export const getAllDeals = () => async dispatch => {
  console.log("in action getAllDeals")
  const deals = await getDeals()
  if (deals && deals.length) {
    dispatch({type: GET_ALL_DEALS, payload: deals})
  } else {
    dispatch({type: GET_ALL_DEALS, payload: []})
  }
}

export const fetchDealsTransaction = async(type, transId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}deals/transaction/${type}/${transId}`, {headers:{"Authorization":getToken()}})
    callback((response && response.data) || {})
  } catch (err) {
    callback(err)
  }
}

export const sendEmailAlert = async (payload) => {
  try {
    const categories = ["all", "myfirm", "otherfirms", "onlyme", "custom"]
    payload.type = payload.type === "loan" ? "bankloans" :
      payload.type === "marfp" ? "marfps" :
        payload.type === "derivative" ? "derivatives" :
          payload.type === "rfp" ? "rfps" :
            payload.type === "others" ? "others" : payload.type

    const types = ["deals", "rfps", "bankloans", "marfps", "derivatives", "others"]

    if(payload && payload.tranId && payload.type && payload.emailParams && payload.emailParams.category &&
      categories.indexOf(payload.emailParams.category) !== -1 && types.indexOf(payload.type) !== -1){
      payload.emailParams.sendEmailTo = payload.emailParams.category !== "onlyme" && payload.emailParams.sendEmailTo ? payload.emailParams.sendEmailTo.map(user => user.sendEmailTo) : []

      console.log("=============email payload========", payload)

      const response = await axios.post(`${muniApiBaseURL}emails/sendtranemail`, payload, {headers:{"Authorization":getToken()}})

      if(response && response.data && !response.data.done) {
        toast((response && response.data && response.data.message) || "Sorry, email not sent", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
      }else {
        toast("E-mail Sent Successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
      }

    }
  } catch (err) {
    console.log(err)
    toast(err.message, {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
  }
}

export const sendComplianceEmail = async (payload) => {
  try {
    const categories = ["all", "myfirm", "otherfirms", "onlyme", "custom"]
    delete payload.tranId

    if(payload && payload.type && payload.emailParams && payload.emailParams.category &&
      categories.indexOf(payload.emailParams.category) !== -1){
      payload.emailParams.sendEmailTo = payload.emailParams.category !== "onlyme" && payload.emailParams.sendEmailTo ? payload.emailParams.sendEmailTo.map(user => user.sendEmailTo) : []

      console.log("=============email payload========", payload)

      const response = await axios.post(`${muniApiBaseURL}emails/sendcomplianceemail`, payload, {headers:{"Authorization":getToken()}})

      if(response && response.data && !response.data.done) {
        toast((response && response.data && response.data.message) || "Sorry, email not sent", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
      }else {
        toast("E-mail Sent Successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
      }

    }
  } catch (err) {
    console.log(err)
    toast(err.message, {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
  }
}

export const sendCRMEmail = async (payload) => {
  try {
    const categories = ["all", "myfirm", "otherfirms", "onlyme", "custom"]
    delete payload.tranId

    if(payload && payload.type && payload.emailParams && payload.emailParams.category &&
      categories.indexOf(payload.emailParams.category) !== -1){
      payload.emailParams.sendEmailTo = payload.emailParams.category !== "onlyme" && payload.emailParams.sendEmailTo ? payload.emailParams.sendEmailTo.map(user => user.sendEmailTo) : []

      const response = await axios.post(`${muniApiBaseURL}emails/sendcrmemail`, payload, {headers:{"Authorization":getToken()}})

      if(response && response.data && !response.data.done) {
        toast((response && response.data && response.data.message) || "Sorry, email not sent", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
      }else {
        toast("E-mail Sent Successfully", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS,})
      }

    }
  } catch (err) {
    console.log(err)
    toast(err.message, {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
  }
}

/* export const fetchDealsRating = async (transId, contactId, callback) => {
  try {
    const response = await axios.get(`http://localhost:4000/api/deals/rating/${transId}/${contactId}`)
    callback((response && response.data) || {})
  } catch (err) {
    callback(err)
  }
}

export const putDealsRating = async (transId, contactId, body, changeLog, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/rating/${transId}/${contactId}`, body)
    callback(response)
    if (changeLog.length) {
      const ipRes = await axios.get("https://api.ipify.org/?format=json")
      if (ipRes && ipRes.data && ipRes.data.ip) {
        changeLog.forEach(x => {
          x.ip = ipRes.data.ip
        })
      }
      await updateAuditLog(body._id, changeLog)
    }
  } catch (err) {
    callback(err)
  }
} */

export const updateDealsTransaction = async (id, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/${id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putDealsRelatedTran = async (id, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/related/${id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putDealsTransaction = async (type, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/transaction/${type}/${body._id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const postDealsNotes = async(tranId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}deals/notes/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putTransactionDocStatus = async (tranId, type, body) => {
  try {
    return await axios.put(`${muniApiBaseURL}deals/transaction/document/${tranId}?tranType=${type}`, body, {headers:{"Authorization":getToken()}})
  } catch (err) {
    console.log(err)
  }
}

export const pullTransactionDoc = async (tranId, query) => {
  try {
    return await axios.delete(`${muniApiBaseURL}deals/transaction/document/${tranId}${query}`, {headers:{"Authorization":getToken()}})
  } catch (err) {
    console.log(err)
  }
}

export const delDealsRating = async (type, dealId, seriesId, ratingId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}deals/rating/${type}/${dealId}?ratingId=${ratingId}&seriesId=${seriesId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    console.log(err)
    callback(err)
  }
}
export const removeDealsParticipants = async(type, dealId, partId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}deals/transaction/${partId}/${dealId}?removeFrom=${type}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const fetchSeriesDetailsBySeriesId = async(dealId, seriesId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}deals/pricing/${seriesId}/${dealId}`, {headers:{"Authorization":getToken()}})
    callback((response && response.data) || {})
  } catch (err) {
    callback(err)
  }
}

export const putDealsPricingDetails = async (dealId, seriesId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/pricing/${seriesId}/${dealId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putSeriesDetails = async (dealId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/series/${body._id}/${dealId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const removeDealSeries = async (dealId, seriesId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}deals/series/${seriesId}/${dealId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const addOrUpdateDealsParticipants = async (type, dealId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/participants/${type}/${dealId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const postUnderwriterCheckAddToDL = async (type, dealId, body) => {
  try {
    return await axios.post(`${muniApiBaseURL}deals/participants/${type}/${dealId}`, body, {headers:{"Authorization":getToken()}})
  } catch (err) {
    console.log(err)
    return err
  }
}

export const addOrUpdateDealsRating = async (type, dealId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/rating/${type}/${dealId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putDealsCheckNTrack = async (dealId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/${dealId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const removeDealsCheckInTrack = async (dealId, type, trackId, category, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}deals/checkInTrack/${dealId}?type=${type}&trackId=${trackId}&category=${category}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const fetchParticipantsAndOtherUsers = async (id) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}common/getusersfortrandocs/${id}`, {headers:{"Authorization":getToken()}})
    const participants = []
    if(response && response.data){
      response.data.participants.forEach(part => {
        part.id = part._id
        part.name = `${part.userFirstName} ${part.userLastName}`
        part.group =  "Participants"
        participants.push(part)
      })
      response.data.allparticipants.forEach(part => {
        part.id = part._id
        part.name = `${part.userFirstName} ${part.userLastName}`
        part.group =  "Others"
        participants.push(part)
      })
    }
    return participants
  } catch (err) {
    console.log(err)
  }
}

export const getSampleExcel = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}excel/testexcel`, {headers:{"Authorization":getToken()}})
    return response.data
  } catch (err) {
    console.log(err)
  }
}
