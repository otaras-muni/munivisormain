import axios from "axios"
import {getToken} from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"


export const fetchOthersTransaction = async(type, transId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}others/transaction/${type}/${transId}`, {headers:{"Authorization":getToken()}})
    callback((response && response.data) || {})
  } catch (err) {
    callback(err)
  }
}

/* export const fetchDealsRating = async (transId, contactId, callback) => {
  try {
    const response = await axios.get(`http://localhost:4000/api/deals/rating/${transId}/${contactId}`)
    callback((response && response.data) || {})
  } catch (err) {
    callback(err)
  }
}

export const putDealsRating = async (transId, contactId, body, changeLog, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}deals/rating/${transId}/${contactId}`, body)
    callback(response)
    if (changeLog.length) {
      const ipRes = await axios.get("https://api.ipify.org/?format=json")
      if (ipRes && ipRes.data && ipRes.data.ip) {
        changeLog.forEach(x => {
          x.ip = ipRes.data.ip
        })
      }
      await updateAuditLog(body._id, changeLog)
    }
  } catch (err) {
    callback(err)
  }
} */

export const updateDealsTransaction = async (id, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/${id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putOthersRelatedTran = async (id, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/related/${id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putOthersTransaction = async (type, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/transaction/${type}/${body._id}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const postOthersNotes = async(tranId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}others/notes/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putOtherParticipantsDetails = async(type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/participants/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const postUnderwriterCheckAddToDL = async (type, dealId, body) => {
  try {
    return await axios.post(`${muniApiBaseURL}others/participants/${type}/${dealId}`, body, {headers:{"Authorization":getToken()}})
  } catch (err) {
    console.log(err)
    return err
  }
}

export const pullOtherParticipantsDetails = async(type, tranId, id, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}others/transaction/${id}/${tranId}?removeFrom=${type}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putTransactionDocStatus = async (tranId, type, body) => {
  try {
    return await axios.put(`${muniApiBaseURL}others/transaction/document/${tranId}?tranType=${type}`, body, {headers:{"Authorization":getToken()}})
  } catch (err) {
    console.log(err)
  }
}

export const pullTransactionDoc = async (tranId, query) => {
  try {
    return await axios.delete(`${muniApiBaseURL}others/transaction/document/${tranId}${query}`, {headers:{"Authorization":getToken()}})
  } catch (err) {
    console.log(err)
  }
}

export const delOthersRating = async (type, tranId, seriesId, ratingId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}others/rating/${type}/${tranId}?ratingId=${ratingId}&seriesId=${seriesId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    console.log(err)
    callback(err)
  }
}
export const removeDealsParticipants = async(type, tranId, partId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}others/transaction/${partId}/${tranId}?removeFrom=${type}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const  fetchSeriesDetailsBySeriesId = async(tranId, seriesId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}others/pricing/${seriesId}/${tranId}`, {headers:{"Authorization":getToken()}})
    callback((response && response.data) || {})
  } catch (err) {
    callback(err)
  }
}

export const putOthersPricingDetails = async (tranId, seriesId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/pricing/${seriesId}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putSeriesDetails = async (tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/series/${body._id}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const removeOtherSeries = async (tranId, seriesId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}others/series/${seriesId}/${tranId}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const addOrUpdateDealsParticipants = async (type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/participants/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const addOrUpdateOthersRating = async (type, tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/rating/${type}/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const putDealsCheckNTrack = async (tranId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const removeDealsCheckInTrack = async (tranId, type, trackId, category, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}others/checkInTrack/${tranId}?type=${type}&trackId=${trackId}&category=${category}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (err) {
    callback(err)
  }
}

export const fetchParticipantsAndOtherUsers = async (id) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}common/getusersfortrandocs/${id}`, {headers:{"Authorization":getToken()}})
    const participants = []
    if(response && response.data){
      response.data.participants.forEach(part => {
        part.id = part._id
        part.name = `${part.userFirstName} ${part.userLastName}`
        part.group =  "Participants"
        participants.push(part)
      })
      response.data.allparticipants.forEach(part => {
        part.id = part._id
        part.name = `${part.userFirstName} ${part.userLastName}`
        part.group =  "Others"
        participants.push(part)
      })
    }
    return participants
  } catch (err) {
    console.log(err)
  }
}
