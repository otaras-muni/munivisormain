import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import { getToken } from "GlobalUtils/helpers"

export const fetchBcaDetails = async callback => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}businessconduct/bcaDetails`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putBusConduct = async (body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}businessconduct/bcaDetails`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullBusConduct = async (bcaId, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}businessconduct/bcaDetails/${bcaId}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putBcaAuditLogs = async (body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}businessconduct/auditlogs`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullBusinessDocument = async query => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}businessconduct/documents${query}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log(error)
    return error
  }
}

// General Administration

export const fetchDesignateDetails = async callback => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}generaladmin/contactsorpersons`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const fetchGenAdminDocuments = async callback => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}generaladmin/contactsorpersons?type="documents"`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putGenAuditLogs = async (body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}generaladmin/auditlogs`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullGenAdmin = async (type, pullId, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}generaladmin/contactsorpersons/${pullId}?type=${type}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const getGenAdminById = async (type, pullId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}generaladmin/contactsorpersons/${pullId}?type=${type}`,{ headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putGenAdmin = async (type, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}generaladmin/contactsorpersons?type=${type}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putGenAdminDocument = async (query, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}generaladmin/documents${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullGenAdminDocument = async query => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}generaladmin/documents${query}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log(error)
    return error
  }
}

// Professional Qualificcation

export const fetchPqDetails = async (type, callback) => {
  try {
    const queryString = type ? `?type=${type}` : ""
    const response = await axios.get(
      `${muniApiBaseURL}profqualifications/personsortraining${queryString}`,
      { headers: { Authorization: getToken() } }
    )
    if (
      response &&
      response.data &&
      response.data.pqTrainingPrograms &&
      response.data.pqTrainingPrograms.length
    ) {
      response.data.pqTrainingPrograms.forEach(pqTrain => {
        pqTrain.trgAttendees = pqTrain.trgAttendees.map(attend => ({
          ...attend,
          id: attend.userId,
          name: attend.userFirstName
        }))
        pqTrain.trgConductedBy = {
          ...pqTrain.trgConductedBy,
          id: pqTrain.trgConductedBy.userId,
          name: pqTrain.trgConductedBy.userFirstName
        }
      })
    }
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const fetchSeriesPassedAndValid = async userId => {
  try {
    const res = await axios.get(
      `${muniApiBaseURL}profqualifications/userseriesdate/${userId}`,
      { headers: { Authorization: getToken() } }
    )
    return res.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putPqPersonsOrTraining = async (type, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}profqualifications/personsortraining?type=${type}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    if (
      response &&
      response.data &&
      response.data.pqTrainingPrograms &&
      response.data.pqTrainingPrograms.length
    ) {
      response.data.pqTrainingPrograms.forEach(pqTrain => {
        pqTrain.trgAttendees = pqTrain.trgAttendees.map(attend => ({
          ...attend,
          id: attend.userId,
          name: attend.userFirstName
        }))
        pqTrain.trgConductedBy = {
          ...pqTrain.trgConductedBy,
          id: pqTrain.trgConductedBy.userId,
          name: pqTrain.trgConductedBy.userFirstName
        }
      })
    }
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullPqPersonsOrTraining = async (type, pullId, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}profqualifications/personsortraining/${pullId}?type=${type}`,
      { headers: { Authorization: getToken() } }
    )
    if (
      response &&
      response.data &&
      response.data.pqTrainingPrograms &&
      response.data.pqTrainingPrograms.length
    ) {
      response.data.pqTrainingPrograms.forEach(pqTrain => {
        pqTrain.trgAttendees = pqTrain.trgAttendees.map(attend => ({
          ...attend,
          id: attend.userId,
          name: attend.userFirstName
        }))
        pqTrain.trgConductedBy = {
          ...pqTrain.trgConductedBy,
          id: pqTrain.trgConductedBy.userId,
          name: pqTrain.trgConductedBy.userFirstName
        }
      })
    }
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putPqAuditLogs = async (body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}profqualifications/auditlogs`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putPqDocuments = async (query, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}profqualifications/documents${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullPqDocuments = async query => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}profqualifications/documents${query}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log(error)
    return error
  }
}

// policies and procedurece
export const getSupervisor = async () => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}supervisor/policiesandprocedures`,
      { headers: { Authorization: getToken() } }
    )
    return response
  } catch (error) {
    return error
  }
}

export const putPoliciesProcedures = async (body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}supervisor/policiesandprocedures`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullPqDocument = async query => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}supervisor/policiesandprocedures${query}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log(error)
    return error
  }
}

export const getPoliciesProcedures = async callback => {
  try {
    const response = await axios.get(`${muniApiBaseURL}supervisor/details?type=policies`,{ headers: { Authorization: getToken() } })
    callback(response.data || {})
  } catch (error) {
    callback({ error })
  }
}

export const putPoliProAuditLogs = async (body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}supervisor/auditlogs`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const fetchResponsibleDetails = async callback => {
  try {
    const response = await axios.get(`${muniApiBaseURL}svandobligations/svorcertifications`,{ headers: { Authorization: getToken() } })
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putSupervisoryAuditLogs = async (body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}svandobligations/auditlogs`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putSupervisory = async (type, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}svandobligations/svorcertifications?type=${type}`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullSupervisory = async (type, pullId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}svandobligations/svorcertifications/${pullId}?type=${type}`,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const fetchSupervisoryDocuments = async callback => {
  try {
    const response = await axios.get(`${muniApiBaseURL}svandobligations/svorcertifications?type="documents"`,{ headers: { Authorization: getToken() } })
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putSupervisoryDocument = async (query, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}svandobligations/documents${query}`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullSupervisoryDocument = async query => {
  try {
    return await axios.delete(`${muniApiBaseURL}svandobligations/documents${query}`,{ headers: { Authorization: getToken() } })
  } catch (error) {
    console.log(error)
    return error
  }
}

// Client Education & Protection

export const fetchComplaintDetails = async callback => {
  try {
    const response = await axios.get(`${muniApiBaseURL}svcomplaintdetails/complaints`,{ headers: { Authorization: getToken() } })
    /* const complaints = []
    if(response && response.data && response.data.length){
      response.data.forEach(com => {
        complaints.push(com.keyDetails)
      })
    } */
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const fetchComplaintDetailsById = async (comId, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}svcomplaintdetails/details/alldetails?comId=${comId}`,{ headers: { Authorization: getToken() } })
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putComplaintDetails = async (type, comId, body, callback) => {
  try {
    const queryString = comId ? `?comId=${comId}` : ""
    const response = await axios.put(`${muniApiBaseURL}svcomplaintdetails/details/${type}${queryString}`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullCmplAssocPersonDetails = async (type,comId,pullId,callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}svcomplaintdetails/details/${type}?comId=${comId}&pullId=${pullId}`,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putsvCmplDocuments = async (query, comId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}svcomplaintdetails/documents?comId=${comId}&${query}` ,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullsvCmplDocuments = async query => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}svcomplaintdetails/documents${query}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putCmplDetailsAuditLogs = async (comId, body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}svcomplaintdetails/auditlogs?comId=${comId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullComplaintDocument = async query => {
  try {
    return await axios.delete(
      `${muniApiBaseURL}svcomplaintdetails/complaintsDocuments${query}`,
      { headers: { Authorization: getToken() } }
    )
  } catch (error) {
    console.log(error)
    return error
  }
}

export const pullComplaintsDetails = async (pullId,callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}svcomplaintdetails/complaintsDetails?pullId=${pullId}`,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

// Client Education G-10

export const putG10AdminDetails = async (query, body, callback) => {
  try {
    const payload = query === "?type=disclosureMessage" ? { body } : body
    const response = await axios.put(`${muniApiBaseURL}clientEducationG10/admin${query}`,payload,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const fetchG10AdminDetails = async callback => {
  try {
    const response = await axios.get(`${muniApiBaseURL}clientEducationG10/admin`,{ headers: { Authorization: getToken() } })
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const pullG10AdminDetails = async (query, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}clientEducationG10/admin${query}`,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putG10RecordKeeping = async (type, body, callback) => {
  try {
    // const queryString = comId ? `?comId=${comId}` : ""
    const response = await axios.put(`${muniApiBaseURL}clientEducationG10/recordkeeping${type}`,body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const fetchG10RecordKeeping = async (query, callback) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}clientEducationG10/recordkeeping${query}`,{ headers: { Authorization: getToken() } })
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

// Political Contributions

export const fetchAllPoliticalDisclosure = async query => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}policontributions${query}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const getPoliContributionsDetails = async (query, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}policontributions/details/${query}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const postAddNewUser = async (body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}policontributions/user`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putContributeDetailsDocuments = async (
  query,
  detailsId,
  body,
  callback
) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}policontributions/details/${detailsId}${query || ""}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const postPoliContributions = async (body, callback) => {
  try {
    const query = body.type ? `?type=${body.type}&discloseType=${body.discloseType}` : body.discloseType ? `?discloseType=${body.discloseType}` : ""
    delete body.type
    delete body.discloseType
    const response = await axios.post(
      `${muniApiBaseURL}policontributions/statedetails${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const getPoliContributionDetailsById = async (query) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}policontributions/statedetails${query}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const putQuestionAnswes = async (query, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}policontributions/questionanswer/${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const fetchMsrbSubmissionDocuments = async (query, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}policontributions/documents${query}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putMsrbSubmissionDocuments = async (query, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}policontributions/documents${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putPoliContributionAuditLogs = async (query, body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}policontributions/auditlogs${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putPolContribMuniEntity = async (query, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}policontributions/details/${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putContributeDetailsAuditLogs = async (
  detailsId,
  body,
  callback
) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}policontributions/auditlogs/${detailsId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullDisclosure = async (type, contributeId, pullId, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}policontributions/details/${contributeId}?pullId=${pullId}&type=${type}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullContributeDetailsDocuments = async (
  contributeId,
  pullId,
  type,
  callback
) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}policontributions/documents/?contributeId=${contributeId}&pullId=${pullId}&type=${type}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putPolContribSummary = async (query, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}policontriSummary/summary${query}`, body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const getPolContribSummary = async (query) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}policontriSummary/summary${query}`, { headers: { Authorization: getToken() } })
    return response.data
  } catch (error) {
    return error
  }
}

export const putPolContribSummaryStatus = async (query, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}policontriSummary/status${query}`, body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putContributeDetailsSumaryDocuments = async (query, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}policontriSummary/documents${query || ""}`, body,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullContributeDetailsSumaryDocuments = async (contributeId, pullId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}policontriSummary/documents?summaryId=${contributeId}&pullId=${pullId}`,{ headers: { Authorization: getToken() } })
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

// Gifts And Gratuities
export const fetchAllGifts = async query => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}giftsgratuities${query}`,
      { headers: { Authorization: getToken() } }
    )
    return response.data
  } catch (error) {
    return error
  }
}

export const postGiftsGratuitiesInfo = async (body, callback) => {
  const query = body.discloseType ? `?discloseType=${body.discloseType}` : ""
  delete body.discloseType
  try {
    const response = await axios.post(
      `${muniApiBaseURL}giftsgratuities/statedetails${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const postCheckGiftsGratuities = async (query, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}giftsgratuities/details/${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const getGiftsGrautitiesDetails = async (query, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}giftsgratuities/statedetails${query}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const pullGiftsGratuitiesDetails = async (
  type,
  giftsId,
  pullId,
  callback
) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}giftsgratuities/details/${giftsId}?pullId=${pullId}&type=${type}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putGiftsGrautitiesDocuments = async (
  query1,
  query,
  body,
  callback
) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}giftsgratuities/documents${query}${query1}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putGiftsGrautitiesAuditLogs = async (query, body, callback) => {
  try {
    const response = await axios.post(
      `${muniApiBaseURL}giftsgratuities/auditlogs${query}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const getUserGiftsDetails = async (query, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}giftsgratuities/giftdetails${query}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const getGiftDetail = async (query, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}giftsgratuities/getgiftdetail${query}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const getGiftDisclosure = async (sortBy, callback) => {
  try {
    const query = `?sortBy=${sortBy}`
    const response = await axios.get(
      `${muniApiBaseURL}giftsgratuities/getAllDisclosure${query}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}
