export const GET_FRUITS = "get_fruits"
export const SAVE_FRUITS = "save_fruits"
export const REMOVE_FRUIT = "remove_fruit"

export const GET_CONFIG_CHECKLIST = "get_config_checklist"
export const SAVE_CONFIG_CHECKLIST = "save_config_checklist"

export const GET_CONFIG_PICKLIST = "get_config_picklist"
export const SAVE_CONFIG_PICKLIST = "save_config_picklist"
export const TOGGLE_PICKLISTS_LOADED = "toggle_picklists_loaded"

export const GET_RFP_CHECKLISTS = "get_rfp_checklists"
export const SAVE_RFP_CHECKLISTS = "save_rfp_checklists"
export const ADD_NEW_CHECKLIST = "add_new_checklist"

export const GET_CONFIG_NOTIFICATIONS = "get_config_notifications"
export const SAVE_CONFIG_NOTIFICATIONS = "save_config_notifications"
export const ADD_NEW_NOTIFICATION = "add_new_notification"

export const GET_AUDIT_LOG = "get_audit_log"
export const GET_PICKLISTS = "get_picklists"
export const SET_SUPERVISOR = "SET_SUPERVISOR"
export const SET_PLATFORMADMIN = "SET_PLATFORMADMIN"

export const GET_NAV_OPTIONS = "get_nav_options"
export const SET_NAV_PERMISSION = "SET_NAV_PERMISSION"
export const SET_IP_ADDRESS = "SET_IP_ADDRESS"

export const GET_RFP_TRANSACTION_EVAL_RATING = "get_rfp_transaction_eval_rating"
export const POST_RFP_TRANSACTION_EVAL_RATING = "post_rfp_transaction_eval_rating"

export const GET_ELIGIBLE_TRANSACTIONS = "GET_ELIGIBLE_TRANSACTIONS"
export const GET_RFP_TRANSACTION_EVAL = "GET_RFP_TRANSACTION_EVAL"
export const POST_RFP_TRANSACTION_EVAL = "POST_RFP_TRANSACTION_EVAL"
export const RFP_TRANSACTION_STATE_CHANGE = "RFP_TRANSACTION_STATE_CHANGE"
export const GET_RFP_TRANSACTION_EVAL_FETCHING = "GET_RFP_TRANSACTION_EVAL_FETCHING"
export const GET_URLS_FOR_FORNTEND = "GET_URLS_FOR_FORNTEND"

export const GET_DOCS = "get_docs"
export const GET_AUTH_USER = "get_auth_user"

export const GET_ALL_DEALS = "get_all_deals"

export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS"
export const SIGNUP_ERROR = "SIGNUP_ERROR"

export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_ERROR = "LOGIN_ERROR"

export const CHECKAUTH_SUCCESS = "CHECKAUTH_SUCCESS"
export const PDF_LOGO = "PDF_LOGO"
export const AUDIT_UPDATE = "AUDIT_UPDATE"
export const CHECKAUTH_ERROR = "CHECKAUTH_ERROR"

export const SIGN_OUT = "sign_out"

export const GET_CONTROLS = "get_controls"
export const ADD_NEW_CONTROL = "add_new_controls"
export const SAVE_CONTROLS = "save_controls"
export const CHECK_CAC_SUPERVISOR = "check_cac_supervisor"

export const GET_MESSAGES = "get_messages"
