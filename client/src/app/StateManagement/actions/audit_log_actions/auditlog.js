import axios from "axios"
import {getToken} from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {GET_AUDIT_LOG} from "../types"

export const dispatchGetAuditLog = (dispatch, data) => {
  dispatch({type: GET_AUDIT_LOG, payload: data})
}

export const getAuditLog = (type) => async dispatch => {
  console.log("in action getAuditLog")
  const token = localStorage.getItem("token")
  let res
  try {
    res = await axios.get(`${muniApiBaseURL}auditLog?type=${type}`, {headers: { Authorization: token }})
    console.log("res : ", res)
    if(res && res.status === 200 && res.data ) {
      const data = []
      res.data.forEach(d => {
        data.push(...d.changeLog)
      })
      dispatchGetAuditLog(dispatch, data)
    } else {
      console.log("no data in getAuditLog: ")
      dispatchGetAuditLog(dispatch, [])
    }
  } catch (err) {
    console.log("err in getAuditLog: ", err)
    dispatchGetAuditLog(dispatch, [])
  }
}

export const getAuditLogByType = async (type, id, callback) => {
  const types = ["rfp", "deals", "derivative", "loan", "marfp",
    "others", "documents", "clientComplaints", "businessConduct",
    "supervisoryObligation", "generalAdmin", "profQualification", "configuration",
    "migrationTools", "clients-propects", "politicalContribution", "thirdparties", "cac"]
  try {
    let response = {}
    if(types.indexOf(type) > -1) {
      response = await axios.get(`${muniApiBaseURL}auditlog/transaction/${id}`, {headers: { Authorization: getToken() }})
    }
    callback(response.data)
  } catch (error) {
    callback({error})
  }
}
