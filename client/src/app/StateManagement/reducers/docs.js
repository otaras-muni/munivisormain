import {
  GET_DOCS
} from "../actions/types"

const INITIAL_STATE = []

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_DOCS:
    return [...action.payload]
  default:
    return state
  }
}
