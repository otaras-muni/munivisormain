import {
  UPDATE_GLOABAL_SEARCH_PREF,
  RENAME_SELECTED_PREF,
  RESET_GLOBAL_SEARCH
} from "../actions"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case UPDATE_GLOABAL_SEARCH_PREF:
    return {
      ...state,
      [action.id]: action.payload
    }
  case RENAME_SELECTED_PREF:
    return action.pref || state
  case RESET_GLOBAL_SEARCH:
    return INITIAL_STATE
  default:
    return state
  }
}
