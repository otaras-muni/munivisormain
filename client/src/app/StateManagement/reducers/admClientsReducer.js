import * as types from "AppState/actions/AdminManagement/actionsType"

import {
  SIGN_OUT
} from "../actions/types"

const defaultState = {}

const initialState = {
  updated : false
}
export default function admFirmDetailsReducer(state = initialState, action) {
  switch (action.type) {
  case types.CLIENT_FIRM_LIST:{
    const firmList = [{
      label: "Pick MSRB Firm Name",
      value: ""
    }]

    action.payload.map(item => {
      firmList.push({
        label: item.firmName,
        value: item._id
      })
    })
    return {
      ...state,
      firmList,
      updated: false,
      firmAdded:false
    }
  }
  case types.CLIENT_FIRM_ERROR:
    const error = Object.assign({}, action.payload)
    return {
      ...state,
      error
    }
  case types.CLIENT_FIRM_DETAIL_BY_ID:
    const firmDetailById = Object.assign({}, action.payload)
    return {
      ...state,
      firmDetailById,
      error: "",
      updated: false,
      firmAdded:false
    }
  case types.CLIENT_UPDATE_FIRM_DETAIL:
    const updatedFirmDetails = Object.assign({}, action.payload)
    return {
      ...state,
      updatedFirmDetails,
      error: "",
      updated: true,
      firmAdded:false
    }
  case types.CLIENT_NEW_FIRM_DETAIL:
    const newFirmDetails = Object.assign({}, action.payload)
    return {
      ...state,
      newFirmDetails,
      error: "",
      updated:false,
      firmAdded: true
    }
  case types.CLIENT_SAVE_FIRMDATA:
  {
    const {payload} = action
    const oldFirmDetails = Object.assign({},payload)
    return{
      oldFirmDetails
    }
  }
  default:
    return state
  }
}
