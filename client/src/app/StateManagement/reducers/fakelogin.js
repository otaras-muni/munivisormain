import {
  GET_AUTH_USER
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {  
  switch (action.type) {
  case GET_AUTH_USER:
    console.log("in reducer GET_AUTH_USER : ", action)
    return { userId: action.payload }
  default:
    return state
  }
}
