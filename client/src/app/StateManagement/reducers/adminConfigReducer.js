import {
  CONFIG_CHECKLISTS_UPDATE_SUCCESS,
  CONFIG_CHECKLISTS_UPDATE_FAILURE,
  CONFIG_PICKLISTS_UPDATE_SUCCESS,
  CONFIG_PICKLISTS_UPDATE_FAILURE
} from "../actions"

export const configReducer = (state = {}, action) => {
  const { payload } = action 
  switch (action.type) {
  case CONFIG_CHECKLISTS_UPDATE_SUCCESS:
    return {...state,...payload}
  case CONFIG_CHECKLISTS_UPDATE_FAILURE:
    return state
  default:
    return state
  }
}


export const pickListReducer = (state = {}, action) => {
  const { payload } = action 
  switch (action.type) {
  case CONFIG_PICKLISTS_UPDATE_SUCCESS:
    return {...state,...payload}
  case CONFIG_PICKLISTS_UPDATE_FAILURE:
    return state
  default:
    return state
  }
}