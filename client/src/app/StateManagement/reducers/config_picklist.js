import {
  GET_CONFIG_PICKLIST,
  SAVE_CONFIG_PICKLIST,
  TOGGLE_PICKLISTS_LOADED
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_CONFIG_PICKLIST:
    console.log("in reducer GET_CONFIG_PICKLIST : ", action)
    return action.payload
  case SAVE_CONFIG_PICKLIST:
    console.log("in reducer SAVE_CONFIG_PICKLIST : ", action)
    return action.payload
  case TOGGLE_PICKLISTS_LOADED:
    console.log("in reducer SAVE_CONFIG_PICKLIST : ", action)
    return { ...state, loaded: action.payload }
  default:
    return state
  }
}
