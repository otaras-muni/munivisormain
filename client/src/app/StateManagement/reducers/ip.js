import {
  SET_IP_ADDRESS
} from "../actions/types"

const INITIAL_STATE = null

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case SET_IP_ADDRESS:
    return action.payload
  default:
    return state
  }
}
