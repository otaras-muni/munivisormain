export const taskProjectsReducer = (state = [], action) => {
//  console.log("Inside Task Projects Reducer", state);
  switch (action.type) {
  case "ADD_PROJECT":
    return state.concat(action.payload)
  default:
    return state
  }
}

/* This is a reducer for just incrementing the counter piece of the state */
export const counterIncrementReducer = (state = 0, action) => {
//  console.log("Inside Counter reducer", state);
  switch (action.type) {
  case "INCREMENT_COUNTER":
    return state + action.payload
  case "DECREMENT_COUNTER":
    return state - action.payload
  case "JUNK_VALUE":
    throw new Error("Something Went Wrong Sorry")
  default:
    return state
  }
}

/* This is only responsible for the User piece of tghe state */
/* The User Reducer */

export const userReducer = (state = {}, action) => {
//  console.log("Inside User Reducer", state);

  switch (action.type) {
  case "ADD_AGE":
    return {
      ...state,
      ...action.payload
    }
  case "ADD_ADDRESS":
    return {
      ...state,
      address: action.payload
    }
  default:
    return state
  }
}

/* This is only responsible for the transaction piece of the state */

export const transactionReducer = (state = [], action) => {
  console.log("Inside Transaction Reducer", state)

  switch (action.type) {
  case "ADD_TRANSACTION":
    return {
      ...state,
      ...action.payload
    }
  default:
    return state
  }
}
