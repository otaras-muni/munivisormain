import {
  PDF_LOGO,
} from "../actions/types"

const INITIAL_STATE = ""

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
    case PDF_LOGO:
    return {
      firmLogo: (action.payload.logo) || "" ,
    }
  default:
    return state
  }
}
