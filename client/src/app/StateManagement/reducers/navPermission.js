import {
  SET_NAV_PERMISSION
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case SET_NAV_PERMISSION:
    return {...action.payload}
  default:
    return state
  }
}
