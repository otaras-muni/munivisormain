import * as types from "../actions/AdminManagement/actionsType"

const initialState = {
  updated: false,
  gridData: null,
  doesFileHasHeaders: true,
  uploadResult: {}
}
export default function admMigrationToolsReducer(state = initialState, action) {
  switch (action.type) {
    case types.ADD_GRID_DATA:
      const gridData = Object.assign({}, action.payload)
      return {
        ...state,
        gridData
      }
    case types.SET_STEP_INDEX:
      const selectedStepIndex = action.payload
      return {
        ...state,
        selectedStepIndex
      }
    case types.SET_FILE:
      const file = action.payload
      return {
        ...state,
        file
      }
    case types.SET_TYPE_OF_FILE:
      return {
        ...state,
        ...action.payload
      }
    case types.SET_INTERIM_STORAGE_KEY:
      const interimStorageKey = action.payload
      return {
        ...state,
        interimStorageKey
      }
    case types.SET_VALIDED_DATA:
      const validatedData = Object.assign({}, action.payload)
      return {
        ...state,
        validatedData
      }
    default:
      return state
  }
}
