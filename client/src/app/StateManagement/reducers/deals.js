import {
  GET_ALL_DEALS
} from "../actions/types"

const INITIAL_STATE = []

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_ALL_DEALS:
    console.log("in reducer GET_ALL_DEALS : ", action.payload)
    return action.payload
  default:
    return state
  }
}
