import {
  SET_SUPERVISOR,
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SUPERVISOR:
    return {
      ...action.payload,
    }
  default:
    return state
  }
}
