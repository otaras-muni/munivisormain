import {
  GET_AUDIT_LOG
} from "../actions/types";

const INITIAL_STATE = [];

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_AUDIT_LOG:
      return [...action.payload];
    default:
      return state;
  }
};
