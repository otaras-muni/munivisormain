import { combineReducers } from "redux"

import * as taskprojectreducers from "./project_reducer"
import test from "./test"
import configChecklists from "./config_checklist"
import configPicklist from "./config_picklist"
import configNotifications from "./config_notifications"
import auditLog from "./auditlog"
import {configReducer, pickListReducer} from "./adminConfigReducer"
import {authReducer} from "./authReducer"
import nav from "./nav"
import navPermission from "./navPermission"
import ip from "./ip"
import urls from "./urls"
import transaction from "./transaction"
import docs from "./docs"
import fakeAuthUser from "./fakelogin"
import deals from "./deals"
import admFirmDetails from "./admTrnFrmReducers"
import admThirdParty from "./admThridPartyReducer"
import admUserDetail from "./admUserReducer"
import admClientDetail from "./admClientsReducer"
import admMigrationToolsReducer  from './admMigrationToolsReducer'
import searchPref from "./searchPref"
import dashboardSearchPref from "./dashboardSearchPref"
import globalSearchPref from "./globalSearchPref"
import selectedGlobalPref from "./selectedGlobalSearch"
import picklists from "./picklists"
import controls from "./cac"
import messages from "./messages"
import clientDashboardSearchPref from "./clientDashboardSearchPref"
import selectedClientDashboardPref from "./selectedClientDashboardSearchPref"
import logo from "./logo"
import supervisor from "./supervisor"
import platformAdmin from "./platformAdmin"

import {
  SIGN_OUT,
} from "../actions/types"



const appReducer = combineReducers({
  taskProjects: taskprojectreducers.taskProjectsReducer,
  checkListConfigs:configReducer,
  pickListConfigs:pickListReducer,
  form:{},
  auth:authReducer,
  logo,
  transaction,
  admFirmDetails,
  admThirdParty,
  admClientDetail,
  admUserDetail,
  admMigrationToolsReducer,
  test,
  configChecklists,
  configPicklist,
  configNotifications,
  auditLog,
  nav,
  navPermission,
  ip,
  urls,
  docs,
  fakeAuthUser,
  deals,
  searchPref,
  dashboardSearchPref,
  globalSearchPref,
  selectedGlobalPref,
  picklists,
  controls,
  messages,
  clientDashboardSearchPref,
  selectedClientDashboardPref,
  supervisor,
  platformAdmin
})

const initialState = {
  auth:{
    userEmail: "",
    authenticated:false,
    relatedFaEntities: {},
    userEntities: {},
    token:""
  }
}


export const appGlobalReducers = (state={}, action) => {
  switch (action.type) {
  case SIGN_OUT:
    console.log("resetting the entire state to wipe out the state")
    localStorage.setItem("token", "")
    localStorage.setItem("email", "")
    return {...initialState}
  default:
    return appReducer(state, action)
  }
}

