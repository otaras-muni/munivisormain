import {
  START_FETCHING_SEARCH_PREF,
  FINISHI_FETCHING_SEARCH_PREF,
  START_SAVING_SEARCH_PREF,
  FINISHI_SAVING_SEARCH_PREF,
  START_DELETING_SEARCH_PREF,
  FINISHI_DELETING_SEARCH_PREF,
} from "../actions"

const INITIAL_STATE = {
  isFetching: false,
  isSaving: false,
  isDeleting: false,
}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case START_FETCHING_SEARCH_PREF:
    return {...state, isFetching: true}
  case FINISHI_FETCHING_SEARCH_PREF:
    return {...state, isFetching: false}
  case START_SAVING_SEARCH_PREF:
    return {...state, isSaving: true}
  case FINISHI_SAVING_SEARCH_PREF:
    return {...state, isSaving: false}
  case START_DELETING_SEARCH_PREF:
    return {...state, isDeleting: true}
  case FINISHI_DELETING_SEARCH_PREF:
    return {...state, isDeleting: false}
  default:
    return state
  }
}
