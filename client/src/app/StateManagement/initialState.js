//import _ from "lodash"
//import uuidv4 from "uuid/v4"
//import faker from "faker"
// import { Map, List, fromJS } from "immutable";

// Generaste a random set of projects
// For each project have a random set of boards that vary between 1 to 10
// For each board list have a number of acards that vary between 1 to 20
// For each card have 2 to 4 checklists
// For each card have 1 to 5 comments
// For each checklist have some 2 to 4 items
// All of the individual pieces should have an ID that is unique. There should not be any name space collision



export const testGeneratorConfig = {
  numProjects: 1,
  numBoards: 5,
  numCards: 10,
  numComments: 5,
  numChecklists: 3,
  numChecklistItems: 5,
  numUsersPerBoard: 5
}

console.log("Entering to create test data")

export const generateTaskManagementProjectsOriginal =({numProjects,numBoards,numCards,numComments,numChecklists,numChecklistItems,numUsersPerBoard}) => {}

// export const generateTaskManagementProjectsOriginal = ({numProjects,numBoards,numCards,numComments,numChecklists,numChecklistItems,numUsersPerBoard}) => _.range(numProjects).map(projId => {
//   const projUniqueId = uuidv4(projId)
//   const projBasicInstanceDetails = {
//     projectId: projUniqueId,
//     projectName: `Project - ${projId}`,
//     projectDescription: "This is a new Project that caters to some stuff",
//     projectCreateDate: faker.date.between("01/01/2018", "03/31/2018")
//   }

//   // Create the Boards

//   const boardListsToBEGenerated = Math.ceil(Math.random() * numBoards)
//   const boardsGenerated = _.range(boardListsToBEGenerated).map(boardId => {
//     const boardUniqueUsers = _.sampleSize(
//       [
//         "User1",
//         "User2",
//         "User3",
//         "User4",
//         "User5",
//         "User6",
//         "User7",
//         "User8",
//         "User9",
//         "User10"
//       ],
//       Math.ceil(Math.random() * numUsersPerBoard)
//     )

//     const boardUniqueId = uuidv4(projUniqueId, boardId)
//     const boardInstance = {
//       boardId: boardUniqueId,
//       boardTitle: `Board List - ${boardId}`,
//       boardDescription: faker.lorem.sentence(),
//       boardCreatedDate: faker.date.past(),
//       boardCreatedBy: _.sampleSize(boardUniqueUsers, 1),
//       boardMembers: boardUniqueUsers
//     }

//     // Create the cards within the boards

//     const cardsToBeGenerated = Math.ceil(Math.random() * numCards)
//     const cardsGenerated = _.range(cardsToBeGenerated).map(cardId => {
//       const cardUniqueId = uuidv4(boardUniqueId, cardId)
//       const cardInstance = {
//         cardId: boardUniqueId,
//         cardTitle: `Card Info ${boardId} - ${cardId}`,
//         cardDescription: faker.lorem.words(),
//         cardCreateDate: faker.date.past(),
//         cardDueDate: faker.date.past(),
//         cardCreatedBy: _.sampleSize(boardUniqueUsers, 1),
//         cardMembers: _.sampleSize(
//           boardUniqueUsers,
//           Math.floor(Math.random() * boardUniqueUsers.length)
//         )
//       }

//       // Add Checklists to the Cards

//       const checklistsTobeGenerated = Math.ceil(
//         Math.random() * numChecklists
//       )
//       const checklistsGenerated = _.range(checklistsTobeGenerated).map(
//         checklistId => {
//           const checkListUniqueId = uuidv4(cardUniqueId, checklistId)
//           const checkListInstance = {
//             checkListId: checkListUniqueId,
//             checkListTitle: `Checklist ${boardId} - ${cardId} - ${checklistId}`
//           }

//           // Generate Checklist Items and Add it to the checklistItems

//           const checklistItemsTobeGenerated = Math.ceil(
//             Math.random() * numChecklistItems
//           )
//           const checklistItemsGenerated = _.range(
//             checklistItemsTobeGenerated
//           ).map(checklistItemId => {
//             const checkListItemUniqueId = uuidv4(
//               checkListUniqueId,
//               checklistItemId
//             )
//             const checkListItemInstance = {
//               checkListItemId: checkListItemUniqueId,
//               checkListItem: `ChecklistITem ${boardId} - ${cardId} - ${checklistId} - ${checklistItemId}`
//             }
//             return {
//               [checkListItemInstance.checkListItemId]: checkListItemInstance
//             }
//           })
//           checkListInstance.checkListItems = checklistItemsGenerated
//           return { [checkListInstance.checkListId]: checkListInstance }
//         }
//       )

//       // Add Comments to the Cards

//       const cardCommentsTobeGenerated = Math.ceil(
//         Math.random() * numComments
//       )
//       const cardCommentsGenerated = _.range(cardCommentsTobeGenerated).map(
//         cardCommentId => {
//           const cardCommentUniqueId = uuidv4(cardUniqueId, cardCommentId)
//           const cardCommentInstance = {
//             cardCommentId: cardCommentUniqueId,
//             cardCommentMessage: `Checklist ${boardId} - ${cardId} - ${cardCommentId}`,
//             cardCommentDate: faker.date.past(),
//             cardCommentByUser: _.sampleSize(boardUniqueUsers, 1)
//           }

//           return {
//             [cardCommentInstance.cardCommentId]: cardCommentInstance
//           }
//         }
//       )
//       // Add Users to the Cards
//       cardInstance.cardChecklists = checklistsGenerated
//       cardInstance.cardComments = cardCommentsGenerated
//       return { [cardInstance.cardId]: cardInstance }
//     })

//     boardInstance.cards = cardsGenerated
//     return { [boardInstance.boardId]: boardInstance }
//   })

//   projBasicInstanceDetails.boards = boardsGenerated
//   return {
//     [projBasicInstanceDetails.projectId]: projBasicInstanceDetails
//   }
// })


export const generateTaskManagementProjects = ({numProjects,numBoards,numCards,numComments,numChecklists,numChecklistItems,numUsersPerBoard}) => {}
// export const generateTaskManagementProjects = ({numProjects,numBoards,numCards,numComments,numChecklists,numChecklistItems,numUsersPerBoard}) => _.range(numProjects).map(projId => {
//   const projUniqueId = uuidv4(projId)
//   const projBasicInstanceDetails = {
//     projectId: projUniqueId,
//     projectName: `Project - ${projId}`,
//     projectDescription: "This is a new Project that caters to some stuff",
//     projectCreateDate: faker.date.between("01/01/2018", "03/31/2018")
//   }

//   // Create the Boards

//   const boardListsToBEGenerated = Math.ceil(Math.random() * numBoards)
//   const boardsGenerated = _.range(boardListsToBEGenerated).map(boardId => {
//     const boardUniqueUsers = _.sampleSize(
//       [
//         "User1",
//         "User2",
//         "User3",
//         "User4",
//         "User5",
//         "User6",
//         "User7",
//         "User8",
//         "User9",
//         "User10"
//       ],
//       Math.ceil(Math.random() * numUsersPerBoard)
//     )

//     const boardUniqueId = uuidv4(projUniqueId, boardId)
//     const boardInstance = {
//       boardId: boardUniqueId,
//       boardTitle: `Board List - ${boardId}`,
//       boardDescription: faker.lorem.sentence(),
//       boardCreatedDate: faker.date.past(),
//       boardCreatedBy: _.sampleSize(boardUniqueUsers, 1),
//       boardMembers: boardUniqueUsers
//     }

//     // Create the cards within the boards

//     const cardsToBeGenerated = Math.ceil(Math.random() * numCards)
//     const cardsGenerated = _.range(cardsToBeGenerated).map(cardId => {
//       const cardUniqueId = uuidv4(boardUniqueId, cardId)
//       const cardInstance = {
//         cardId: boardUniqueId,
//         cardTitle: `Card Info ${boardId} - ${cardId}`,
//         cardDescription: faker.lorem.words(),
//         cardCreateDate: faker.date.past(),
//         cardDueDate: faker.date.past(),
//         cardCreatedBy: _.sampleSize(boardUniqueUsers, 1),
//         cardMembers: _.sampleSize(
//           boardUniqueUsers,
//           Math.floor(Math.random() * boardUniqueUsers.length)
//         )
//       }

//       // Add Checklists to the Cards

//       const checklistsTobeGenerated = Math.ceil(
//         Math.random() * numChecklists
//       )
//       const checklistsGenerated = _.range(checklistsTobeGenerated).map(
//         checklistId => {
//           const checkListUniqueId = uuidv4(cardUniqueId, checklistId)
//           const checkListInstance = {
//             checkListId: checkListUniqueId,
//             checkListTitle: `Checklist ${boardId} - ${cardId} - ${checklistId}`
//           }

//           // Generate Checklist Items and Add it to the checklistItems

//           const checklistItemsTobeGenerated = Math.ceil(
//             Math.random() * numChecklistItems
//           )
//           const checklistItemsGenerated = _.range(
//             checklistItemsTobeGenerated
//           ).map(checklistItemId => {
//             const checkListItemUniqueId = uuidv4(
//               checkListUniqueId,
//               checklistItemId
//             )
//             const checkListItemInstance = {
//               checkListItemId: checkListItemUniqueId,
//               checkListItem: `ChecklistITem ${boardId} - ${cardId} - ${checklistId} - ${checklistItemId}`
//             }
//             return {
//               [checkListItemInstance.checkListItemId]: checkListItemInstance
//             }
//           })
//           checkListInstance.checkListItems = checklistItemsGenerated
//           return checkListInstance
//         }
//       )

//       // Add Comments to the Cards

//       const cardCommentsTobeGenerated = Math.ceil(
//         Math.random() * numComments
//       )
//       const cardCommentsGenerated = _.range(cardCommentsTobeGenerated).map(
//         cardCommentId => {
//           const cardCommentUniqueId = uuidv4(cardUniqueId, cardCommentId)
//           const cardCommentInstance = {
//             cardCommentId: cardCommentUniqueId,
//             cardCommentMessage: `Checklist ${boardId} - ${cardId} - ${cardCommentId}`,
//             cardCommentDate: faker.date.past(),
//             cardCommentByUser: _.sampleSize(boardUniqueUsers, 1)
//           }

//           return cardCommentInstance
//         }
//       )
//       // Add Users to the Cards
//       cardInstance.cardChecklists = checklistsGenerated
//       cardInstance.cardComments = cardCommentsGenerated
//       return cardInstance
//     })

//     boardInstance.cards = cardsGenerated
//     return boardInstance
//   })

//   projBasicInstanceDetails.boards = boardsGenerated
//   return projBasicInstanceDetails

// })

// console.log (JSON.stringify(generateTaskManagementProjectsRevised(testGeneratorConfig),null, 2))
