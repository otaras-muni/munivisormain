import promiseMiddleware from "redux-promise"
import thunk from "redux-thunk"
import { createStore, applyMiddleware, compose } from "redux"
// import createSagaMiddleware from "redux-saga"
// import { generateTaskManagementProjects } from "./initialState"
import {errorHandler} from "./utilities/custommiddleware"

// Import all the reducers from all the key modules

import {appGlobalReducers} from "./reducers"
// import {appRootSaga} from "./sagas"

/* Set configura{ion for generating test data */

// const testGeneratorConfig = {
//   numProjects: 1,
//   numBoards: 5,
//   numCards: 10,
//   numComments: 5,
//   numChecklists: 3,
//   numChecklistItems: 5,
//   numUsersPerBoard: 5
// }

/* Testing Management */

// const initialState = {
//   taskProjects: generateTaskManagementProjects(testGeneratorConfig)
// }

// const constructInitialState =() => {
//   const token = localStorage.getItem("token")
//   const email = localStorage.getItem("email")
//   if (token) {
//     return {
//       user:email,
//       token,
//       error:"",
//       authenticated:true,
//     }
//   }
//
//   return {
//     user:email,
//     token:"",
//     error:"No token exists",
//     authenticated:false
//   }
//
//
// }
// const initialState = {
//   auth : constructInitialState()
// }

// const sagaMiddleware = createSagaMiddleware()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose


export const muniVisorApplicationStore = createStore(
  appGlobalReducers,
  {},
  // eslint-disable-next-line no-underscore-dangle
  composeEnhancers(applyMiddleware(
    thunk,
    promiseMiddleware,
    //    logger,
    errorHandler
  ))
)

// run the consolidated root saga that consolidates all the actions
// sagaMiddleware.run(appRootSaga)

// // Test out some sagas
// muniVisorApplicationStore.dispatch({
//   type: "INCREMENT_COUNTER",
//   payload: 10
// })
// muniVisorApplicationStore.dispatch({
//   type: "INCREMENT_COUNTER",
//   payload: 25
// })
// muniVisorApplicationStore.dispatch({
//   type: "DECREMENT_COUNTER",
//   payload: 25
// })
// muniVisorApplicationStore.dispatch({
//   type: "JUNK_VALUE",
//   payload: 25
// })
// muniVisorApplicationStore.dispatch({
//   type: "INCREMENT_COUNTER",
//   payload: 34
// })
// muniVisorApplicationStore.dispatch({
//   type: "ADD_AGE",
//   payload: {
//     age: 45
//   }
// })

/* muniVisorApplicationStore.dispatch({
  type: "ADD_PROJECT",
  payload: generateTaskManagementProjects({
    ...testGeneratorConfig,
    ...{
      numProjects: 1
    }
  })
}); */
// muniVisorApplicationStore.dispatch({
//   type: "ADD_PROJECT",
//   payload: generateTaskManagementProjects({
//     ...testGeneratorConfig,
//     ...{
//       numProjects: 1
//     }
//   })
// })

// muniVisorApplicationStore.dispatch({
//   type: "FETCH_CONFIG_TENANT_ASYNC",
//   checkListPath: ["checkLists", "deal","Cost of Issuance"],
//   successAction: "CONFIG_CHECKLISTS_UPDATE_SUCCESS",
//   failAction: "CONFIG_CHECKLISTS_UPDATE_FAILURE"
// })
//
// muniVisorApplicationStore.dispatch({
//   type: "FETCH_CONFIG_TENANT_ASYNC",
//   checkListPath: ["checkLists", "deal","Schedule of Events"],
//   successAction: "CONFIG_CHECKLISTS_UPDATE_SUCCESS",
//   failAction: "CONFIG_CHECKLISTS_UPDATE_FAILURE"
// })
//
// muniVisorApplicationStore.dispatch({
//   type: "FETCH_CONFIG_TENANT_ASYNC",
//   checkListPath: ["checkLists", "deal","Schedule of Events"],
//   successAction: "CONFIG_CHECKLISTS_UPDATE_SUCCESS",
//   failAction: "CONFIG_CHECKLISTS_UPDATE_FAILURE"
// })
//
// muniVisorApplicationStore.dispatch({
//   type: "FETCH_CONFIG_TENANT_ASYNC",
//   checkListPath: ["pickLists", "LIST1"],
//   successAction: "CONFIG_PICKLISTS_UPDATE_SUCCESS",
//   failAction: "CONFIG_PICKLISTS_UPDATE_FAILURE"
// })
// muniVisorApplicationStore.dispatch({
//   type: "FETCH_CONFIG_TENANT_ASYNC",
//   checkListPath: ["pickLists", "LIST2"],
//   successAction: "CONFIG_PICKLISTS_UPDATE_SUCCESS",
//   failAction: "CONFIG_PICKLISTS_UPDATE_FAILURE"
// })
