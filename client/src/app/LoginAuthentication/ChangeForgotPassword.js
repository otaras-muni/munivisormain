import React, { Component } from "react";
import { validateEmail } from "GlobalUtils/functionalutilities";
import { connect } from "react-redux";
import { passwordStrength } from "../../globalutilities/consts";
import { getUserByResetId } from "../StateManagement/actions/LoginAuthentication/auth";
import { changeUserPassword } from "GlobalUtils/helpers";
import { toast } from "react-toastify";
import CONST from "GlobalUtils/consts.js";
import { signOut } from "../../app/StateManagement/actions";

class ChangeForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.checkValidationsAndSetState = this.checkValidationsAndSetState.bind(
      this
    );
  }

  async componentWillMount() {
    this.setState({
      type: "",
      password: "",
      passwordConfirm: "",
      errors: {
        password: "",
        passwordConfirm: "",
        liDigit: "",
        liUpper: "",
        liLower: "",
        li8Chars: "",
        liSpecial: ""
      },
      blur: { passwordConfirm: false },
      passwordError: "",
      waiting: false
    });
  }

  onBlur(e) {
    const blurValues = { ...this.state.blur };
    this.setState(
      { blur: { ...blurValues, ...{ [e.target.name]: true } } },
      () => this.checkValidationsAndSetState()
    );
  }

  checkValidationsAndSetState() {
    const { email, password, passwordConfirm } = this.state;
    const valuesObject = {
      email,
      password,
      passwordConfirm
    };
    const errors = { ...this.state.errors };
    const blur = { ...this.state.blur };
    const errorKeys = Object.keys(this.state.errors);
    const strongRegex = new RegExp(
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})(?=.*[!@#$%^&*])"
    );
    const digitRegex = new RegExp("^(?=.*[0-9])");
    const lowerRegex = new RegExp("^(?=.*[a-z])");
    const upperRegex = new RegExp("^(?=.*[A-Z])");
    const chars8Regex = new RegExp("^(?=.{8,})");
    const specialRegex = new RegExp("^(?=.*[!@#$%^&*])");
    const isStrongPasswordValid = true;

    errorKeys.forEach(eKey => {
      //
      if (eKey.indexOf("li") === -1) {
        errors[eKey] = "";
        if (eKey === "password" || eKey === "passwordConfirm") {
          if (
            password === "" &&
            passwordConfirm === "" &&
            blur[eKey]
          ) {
            errors[eKey] = "Please enter a password";
          }
          if (
            password !== "" &&
            passwordConfirm !== "" &&
            password !== passwordConfirm &&
            blur[eKey]
          ) {
            errors[eKey] = "The passwords don't match";
          } else if (eKey === "password" && passwordStrength === "true") {
            if (digitRegex.test(password)) {
              errors.liDigit = true;
            } else {
              errors.liDigit = false;
            }
            if (lowerRegex.test(password)) {
              errors.liLower = true;
            } else {
              errors.liLower = false;
            }
            if (upperRegex.test(password)) {
              errors.liUpper = true;
            } else {
              errors.liUpper = false;
            }
            if (chars8Regex.test(password)) {
              errors.li8Chars = true;
            } else {
              errors.li8Chars = false;
            }
            if (specialRegex.test(password)) {
              errors.liSpecial = true;
            } else {
              errors.liSpecial = false;
            }
            if (strongRegex.test(password)) errors.password = "";
            else errors.password = "Error";
          }
        } else if (blur[eKey] && valuesObject[eKey] === "") {
          errors[eKey] = "Value is required";
        }
      }
    });

    this.setState({ errors: { ...errors } });
  }

  handleChange = e => {
    let { blur } = this.state
    const changeValue = e.target.value
    const changeKey = e.target.name
    if (changeKey === "passwordConfirm") {
      blur["passwordConfirm"] = true
    } else {
      blur["passwordConfirm"] = false
    }
    this.setState({ [changeKey]: changeValue, blur }, () =>
      this.checkValidationsAndSetState()
    );
  }

  onReset = async () => {
    this.setState({ waiting: true })
    const queryString = require("query-string");
    const parsed = queryString.parse(this.props.location.search);
    let response = getUserByResetId(parsed.id);
    response = await response;
    response = await response.data;
    this.setState({
      id: parsed.id,
      userId: response.userLoginCredentials ? response.userLoginCredentials.userEmailId : "",
      responseError:
        response.userLoginCredentials && response.userLoginCredentials.userEmailId ? "" :
          "Invalid password reset link"
    }, () => {
      if (response.userLoginCredentials && response.userLoginCredentials.userEmailId) {
        changeUserPassword(
          this.state.userId,
          this.state.password,
          "",
          this.onSaveCallback
        )
      }
    })
  };

  onSaveCallback = err => {
    this.setState({
      password: "",
      passwordConfirm: "",
      responseError: "",
      waiting: false
    });
    if (err) {
      toast("Error in changing password. Please try again.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      });
    } else {
      toast("Password changed successfully. Please login again.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      });
      signOut();
      this.props.history.push("/signin");
    }
  };

  render() {
    const { error } = this.props.auth;
    const { tags, errors, blur } = this.state;
    const hasErrors =
      errors && Object.values(errors).some(val => val !== "" && val !== true);
    return (
      <div id="main">
        <section className="container">
          <div className="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child is-2 is-hidden-touch" />
              <article className="tile is-child is-8 box">
                <p style={{ textAlign: "center" }} className="title">
                  Reset your password
                </p>
                {/* <div className="columns">
            <div className="column">
              <p>Email</p>
              <input
                name="email"
                className="input is-small is-link"
                type="email"
                placeholder="Email"
                onBlur={this.onBlur}
                onChange={(e) => this.handleChange(e)}
              />
            </div>
          </div> */}
                <div className="columns">
                  <div className="column">
                    <p>Password</p>
                    <input
                      name="password"
                      className="input is-small is-link"
                      type="password"
                      placeholder="password"
                      onBlur={this.onBlur}
                      //value={this.state.password}
                      onChange={this.handleChange}
                    />
                    <span className="container is-size-7">
                      Password should be as follows
                      <ul>
                        <li className={errors.liDigit ? "li-true" : "li-false"}>
                          Should contain at least one digit
                        </li>
                        <li className={errors.liUpper ? "li-true" : "li-false"}>
                          Should contain at least one upper case character
                        </li>
                        <li className={errors.liLower ? "li-true" : "li-false"}>
                          Should contain at least one lower case character
                        </li>
                        <li
                          className={errors.li8Chars ? "li-true" : "li-false"}>
                          Should be at least 8 characters long
                        </li>
                        <li
                          className={errors.liSpecial ? "li-true" : "li-false"}>
                          Should contain least 1 special character from
                          !@#$%^&amp;*
                        </li>
                      </ul>
                    </span>
                  </div>
                </div>

                <div className="columns">
                  <div className="column">
                    <p>Re-enter Password</p>
                    <input
                      name="passwordConfirm"
                      className="input is-small is-link"
                      type="password"
                      placeholder="Re-enter password"
                      onBlur={this.onBlur}
                      value={this.state.passwordConfirm}
                      onChange={e => this.handleChange(e)}
                    />
                    {errors.passwordConfirm ? (
                      <p className="help is-danger">{errors.passwordConfirm}</p>
                    ) : null}
                  </div>
                </div>
                {this.state.responseError ? (
                  <span style={{ color: "red" }}>
                    {" "}
                    {`${this.state.responseError}`}{" "}
                  </span>
                ) : null}
                <button disabled={
                  hasErrors || this.state.responseError ||
                  this.state.password === "" ||
                  this.state.passwordConfirm === ""
                }
                  className="button is-block is-info is-medium is-fullwidth"
                  onClick={this.onReset}>
                  Reset Password
                </button>
              </article>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default connect(({ auth }) => ({ auth }))(ChangeForgotPassword);
