import React, { Component } from "react"
import { Link } from "react-router-dom"

class RequestToken extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div id="main">
        <section className="container">
          <div className="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child is-3 is-hidden-touch" />
              <article className="tile is-child is-6 box has-text-centered">
                <p className="title">Your Token is invalid or expired?</p>
                <p className="subtitle">Let us help you.</p>
                <div className="content">
                  If you do not have an account,
                  <Link to="/splash">click here</Link> to set up a new account (it's free). Type your different email address in the field
                          below to receive an email to validate your email and reset your password. If you want to use same email address then please contact Admin.
                </div>
              </article>
              <article className="tile is-child is-3 is-hidden-touch" />
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default RequestToken