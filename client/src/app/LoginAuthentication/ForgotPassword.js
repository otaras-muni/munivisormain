import React, { Component } from "react"
import { Link } from "react-router-dom"
import { toast } from "react-toastify"
import { validateEmail } from "GlobalUtils/functionalutilities"
import CONST from "GlobalUtils/consts"
import { checkExistanceOfEmail, submitForgotPassword } from "GlobalUtils/helpers"

class ForgotPassword extends Component {
  constructor(props) {
    super(props)
    this.state = { email: "", error: "", submiterror: "", submitsuccess: "", accessed: false }
    this.handleForgotPassword = this.handleForgotPassword.bind(this)
    this.changeEmail = this.changeEmail.bind(this)
    this.handleForgotPassword = this.handleForgotPassword.bind(this)
  }

  onBlur(e) {
    this.setState({ accessed: true })
  }

  async changeEmail(e) {
    const chgEmail = e.target.value
    let newError = this.state.error
    const [validEmail] = validateEmail([chgEmail])
    let res
    if (validEmail) {
      res = await checkExistanceOfEmail(chgEmail)
      this.setState({ email: chgEmail })
      newError = res.error
    } else {
      newError = "Enter a valid Email"
    }
    this.setState((prevState) => ({ ...prevState, ...{ email: chgEmail, error: newError, accessed: true } }))
  }

  async handleForgotPassword() {
    console.log("Do something here to handle forgot password")
    const resp = await submitForgotPassword(this.state.email)
    console.log("The response from the server", resp)
    const { done, submiterror, submitsuccess, error } = resp
    if (done) {
      toast("Successfully submitted request", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.setState({ submiterror, submitsuccess }, () => this.props.history.push("/signin"))
    } else {
      toast(error || "Something went wrong. Try after sometime", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      this.setState({ submiterror, submitsuccess })
    }
  }

  render() {
    const { email, error, accessed } = this.state
    const displayButton = !!error || !accessed
    return (
      <div id="main">
        <section className="container">
          <div className="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child is-3 is-hidden-touch" />
              <article className="tile is-child is-6 box has-text-centered">
                <p className="title">Forgot your password?</p>
                <p className="subtitle">Let us help you.</p>
                <div className="content">
                  If you do not have an account,
                  <Link to="/splash">click here</Link> to set up a new account (it's free). Type your email address in the field
                          below to receive an email to validate your email and reset your password.
                </div>
              </article>
              <article className="tile is-child is-3 is-hidden-touch" />
            </div>
          </div>

          <div className="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child is-3 is-hidden-touch" />
              <article className="tile is-child is-6 box">
                <div className="columns">
                  <div className="column">
                    <p className="subtitle is-pulled-left">Enter your email address below.</p>
                  </div>
                </div>

                <div className="field">
                  <div className="control has-icons-left has-icons-right">
                    <input className="input" type="email" placeholder="Email" value={email} onChange={this.changeEmail} />
                    <span className="icon is-small is-left">
                      <i className="fas fa-envelope" />
                    </span>
                  </div>
                  {error ? <p className="help is-danger">{error}</p> : null}
                </div>
                <button disabled={displayButton} onClick={() => this.handleForgotPassword()} className="button is-block is-info is-medium is-fullwidth ">Get Password Reset Email</button>
              </article>
              <article className="tile is-child is-3 is-hidden-touch" />
            </div>
          </div>

        </section>
      </div>
    )
  }
}

export default ForgotPassword
