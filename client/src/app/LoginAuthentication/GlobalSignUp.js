import React, { Component } from "react"
import { connect } from "react-redux"
import axios from "axios"
import qs from "qs"
import { Link } from "react-router-dom"
import { validateEmail } from "GlobalUtils/functionalutilities"
import { getEmailIdsByParticipantType } from "GlobalUtils/helpers"
import { startSignUpProcess } from "AppState/actions"
import { passwordStrength } from "../../globalutilities/consts"


const messageReference = {
  iss: {
    type: "Issuer",
    header: "Issuers",
    footer:
      "By creating an account, you confirm that you are classified as an Issuer"
  },
  fa: {
    type: "Municipal Advisor",
    header: "Financial Advisors",
    footer:
      "By creating an account, you confirm that you are classified as a financial advisor or municipal advisor."
  },
  oth: {
    type: "Other",
    header: "Banker, Counsel or Other",
    footer:
      "By creating an account, you confirm that you are classified as a banker, counsel or other (not an issuer, financial advisor and/or external compliance consultant)."
  },
  comp: {
    type: "Compliance and Other Collaborators",
    header: "Compliance Officers / Consultants",
    footer:
      "By creating an account, you confirm that you are classified as a banker, counsel or other (not an issuer, financial advisor and/or external compliance consultant)."
  }
}

class GlobalSignUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: "",
      tags: {},
      dropdown: [],
      email: "",
      userFirstName: "",
      userLastName: "",
      firmName: "",
      userJobTitle: "",
      password: "",
      passwordConfirm: "",
      errors: {
        email: "",
        userFirstName: "",
        userLastName: "",
        firmName: "",
        password: "",
        passwordConfirm: ""
      },
      blur: {},
      doVerification: false,
      passwordError: ""
    }
    this.handleSignUp = this.handleSignUp.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.onBlur = this.onBlur.bind(this)
    this.checkValidationsAndSetState = this.checkValidationsAndSetState.bind(
      this
    )
  }

  async componentDidMount() {
    const query = qs.parse(this.props.history.location.search, {
      ignoreQueryPrefix: true
    })
    console.log("query : ", query)
    const msrbType = (messageReference && messageReference[query.type] && messageReference[query.type].type) || ""
    const dropdownValues = await getEmailIdsByParticipantType(msrbType)
    localStorage.removeItem("token")
    localStorage.removeItem("email")
    this.setState({
      dropdown: dropdownValues,
      type: msrbType || "",
      tags: (messageReference && messageReference[query.type]) || {}
    })
  }

  onBlur(e) {
    const blurValues = { ...this.state.blur }
    this.setState(
      { blur: { ...blurValues, ...{ [e.target.name]: true } } },
      () => this.checkValidationsAndSetState()
    )
  }

  checkValidationsAndSetState() {
    const {
      email,
      password,
      passwordConfirm,
      firmName,
      userFirstName,
      userLastName,
      userJobTitle
    } = this.state
    const valuesObject = {
      email,
      password,
      firmName,
      passwordConfirm,
      userFirstName,
      userLastName,
      userJobTitle
    }
    const errors = { ...this.state.errors }
    const blur = { ...this.state.blur }
    const errorKeys = Object.keys(this.state.errors)
    const strongRegex = new RegExp(
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})(?=.*[!@#$%^&*])"
    )
    const digitRegex = new RegExp(
      "^(?=.*[0-9])"
    )
    const lowerRegex = new RegExp(
      "^(?=.*[a-z])"
    )
    const upperRegex = new RegExp(
      "^(?=.*[A-Z])"
    )
    const chars8Regex = new RegExp(
      "^(?=.{8,})"
    )
    const specialRegex = new RegExp(
      "^(?=.*[!@#$%^&*])"
    )
    const isStrongPasswordValid = true

    errorKeys.forEach(eKey => {
      //
      if (eKey.indexOf("li") === -1) {
        errors[eKey] = ""
        if (eKey === "email") {
          const filteredVal = this.state.dropdown.reduce(
            (emailEligible, value) =>
              emailEligible || value.label.toLowerCase() === email.toLowerCase(),
            false
          )
          const [validEmail] = validateEmail(email)

          if (!validEmail && email !== "" && blur[eKey]) {
            errors[eKey] = "Enter a valid email"
          } else if (!validEmail && email === "" && blur[eKey]) {
            errors[eKey] = "No Email entered"
          } else if (validEmail && !filteredVal && blur[eKey]) {
            errors[eKey] =
              "The email is not eligible for signup. contact your admin"
          }
        } else if (eKey === "password" || eKey === "passwordConfirm") {
          if (
            password === "" &&
            passwordConfirm === "" &&
            blur[eKey] &&
            email !== ""
          ) {
            errors[eKey] = "Please enter a password"
          }
          if (
            password !== "" &&
            passwordConfirm !== "" &&
            password !== passwordConfirm &&
            email !== "" &&
            blur[eKey]
          ) {
            errors[eKey] = "The passwords don't match"
          } else if (eKey === "password" && passwordStrength === "true") {
            if (digitRegex.test(password)) {
              errors.liDigit = true
            } else {
              errors.liDigit = false
            }
            if (lowerRegex.test(password)) {
              errors.liLower = true
            } else {
              errors.liLower = false
            }
            if (upperRegex.test(password)) {
              errors.liUpper = true
            } else {
              errors.liUpper = false
            }
            if (chars8Regex.test(password)) {
              errors.li8Chars = true
            } else {
              errors.li8Chars = false
            }
            if (specialRegex.test(password)) {
              errors.liSpecial = true
            } else {
              errors.liSpecial = false
            }
            if (strongRegex.test(password))
              errors.password = ""
            else
              errors.password = "Error"
          }
        } else if (blur[eKey] && valuesObject[eKey] === "") {
          errors[eKey] = "Value is required"
        }
      }
    })

    this.setState({ errors: { ...errors } })
  }

  handleChange = (e) => {
    const changeValue = e.target.value
    const changeKey = e.target.name
    if (changeKey === "email") {
      const filteredVal = this.state.dropdown.filter(
        ({ label }) => label.toLowerCase() === changeValue.toLowerCase()
      )[0]
      if (filteredVal) {
        const { value } = filteredVal
        const { userFirstName, userLastName, userJobTitle } = value
        this.setState(
          {
            email: changeValue,
            userFirstName,
            userLastName,
            userJobTitle,
            firmName: value.entityInfo.firmName
          },
          () => this.checkValidationsAndSetState()
        )
      } else {
        this.setState(
          {
            [changeKey]: changeValue,
            userFirstName: "",
            userLastName: "",
            firmName: "",
            password: "",
            passwordConfirm: ""
          },
          () => this.checkValidationsAndSetState()
        )
      }
    } else {
      this.setState({ [changeKey]: changeValue }, () =>
        this.checkValidationsAndSetState()
      )
    }
  }

  async handleSignUp(event) {
    event.preventDefault()
    const returnValue = await this.props.startSignUpProcess({
      email: this.state.email,
      password: this.state.password
    })
    // localStorage.removeItem("token")
    // localStorage.removeItem("email")
    if (returnValue === "true") {
      this.setState({ doVerification: true, passwordError: "" })
    }
    else {
      this.setState(prevState => ({ ...prevState, doverification: false, passwordError: returnValue }))
    }
  }

  render() {
    const { error } = this.props.auth
    const { tags, errors, blur } = this.state
    const hasErrors = Object.values(errors).some(val => val !== "" && val !== true)
    const formPristine = Object.values(blur).length === 0

    return (
      <div id="main">
        {!this.state.doVerification ?
          <section className="container">
            <div className="tile is-ancestor">
              <div className="tile is-parent">
                <article className="tile is-child is-2 is-hidden-touch" />
                <article className="tile is-child is-8 box">
                  <div className="columns">
                    <div className="column">
                      <p className="subtitle is-pulled-left">{tags.header}</p>
                    </div>
                  </div>

                  <div className="columns">
                    <div className="column">
                      <div className="field is-grouped">
                        <div className="control">
                          <Link to="/splash">
                            <p className="title innerPgTitle">
                              Change account type
                          </p>
                          </Link>
                        </div>
                        <div className="control">
                          <Link to="/signin">
                            <p className="title innerPgTitle is-pulled-right">
                              Existing account
                          </p>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>

                  <hr />
                  <form onSubmit={this.handleSignUp}>
                    <div className="columns">
                      <div className="column">
                        <p>Email</p>
                        <input
                          name="email"
                          className="input is-small is-link"
                          value={this.state.email}
                          type="text"
                          onBlur={this.onBlur}
                          onChange={(e) => this.handleChange(e)}
                          placeholder=""
                        />
                        {errors.email ? (
                          <p className="help is-danger">{errors.email}</p>
                        ) : null}
                      </div>
                    </div>

                    <div className="columns">
                      <div className="column">
                        <p>First Name</p>
                        <input
                          name="userFirstName"
                          className="input is-small is-link"
                          value={this.state.userFirstName}
                          onChange={this.handleChange}
                          onBlur={this.onBlur}
                          type="text"
                          placeholder=""
                          disabled
                        />
                        {errors.userFirstName ? (
                          <p className="help is-danger">{errors.userFirstName}</p>
                        ) : null}
                      </div>

                      <div className="column">
                        <p>Last Name</p>
                        <input
                          name="userLastName"
                          className="input is-small is-link"
                          value={this.state.userLastName}
                          onChange={this.handleChange}
                          onBlur={this.onBlur}
                          type="text"
                          placeholder=""
                          disabled
                        />
                        {errors.userLastName ? (
                          <p className="help is-danger">{errors.userLastName}</p>
                        ) : null}
                      </div>
                    </div>

                    <div className="columns">
                      <div className="column">
                        <p>Firm Name</p>
                        <input
                          name="firmName"
                          className="input is-small is-link"
                          value={this.state.firmName}
                          onBlur={this.onBlur}
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Search"
                          disabled
                        />
                        {errors.firmName ? (
                          <p className="help is-danger">{errors.firmName}</p>
                        ) : null}
                      </div>
                    </div>

                    <div className="columns">
                      <div className="column">
                        <p>Password</p>
                        <input
                          name="password"
                          className="input is-small is-link"
                          type="password"
                          placeholder="password"
                          onBlur={this.onBlur}
                          value={this.state.password}
                          onChange={this.handleChange}
                        />
                        <span className="container is-size-7">Password should be as follows
                        <ul>
                            <li className={errors.liDigit ? "li-true" : "li-false"}>Should contain at least one digit</li>
                            <li className={errors.liUpper ? "li-true" : "li-false"}>Should contain at least one upper case character</li>
                            <li className={errors.liLower ? "li-true" : "li-false"}>Should contain at least one lower case character</li>
                            <li className={errors.li8Chars ? "li-true" : "li-false"}>Should be at least 8 characters long</li>
                            <li className={errors.liSpecial ? "li-true" : "li-false"}>Should contain least 1 special character from !@#$%^&amp;*</li>
                          </ul>
                        </span>
                      </div>
                    </div>

                    <div className="columns">
                      <div className="column">
                        <p>Re-enter Password</p>
                        <input
                          name="passwordConfirm"
                          className="input is-small is-link"
                          type="password"
                          placeholder="re-enter password"
                          onBlur={this.onBlur}
                          value={this.state.passwordConfirm}
                          onChange={this.handleChange}
                        />
                        {errors.passwordConfirm ? (
                          <p className="help is-danger">
                            {errors.passwordConfirm}
                          </p>
                        ) : null}
                      </div>
                    </div>
                    {error ? (
                      <span style={{ color: "red" }}> {`Error: ${error}`} </span>
                    ) : null}
                    {this.state.passwordError && this.state.passwordError !== "" ? (
                      <span style={{ color: "red" }}> {`Error: ${this.state.passwordError}`} </span>
                    ) : null}
                    <button
                      disabled={
                        hasErrors ||
                        this.state.password === "" ||
                        this.state.email === "" ||
                        this.state.passwordConfirm === ""
                      }
                      className="button is-block is-info is-medium is-fullwidth"
                    >
                      Sign Up
                  </button>
                    <p>{tags.footer}</p>
                  </form>
                </article>
                <article className="tile is-child is-2 is-hidden-touch" />
              </div>
            </div>
          </section>
          : <section className="container">
            <div className="tile is-ancestor">
              <div className="tile is-parent">
                <article className="tile is-child is-2 is-hidden-touch" />
                <article className="tile is-child is-8 box">
                  To complete sign up process, please verify your email by clicking on link sent to your email.
                </article>
                <article className="tile is-child is-2 is-hidden-touch" />
              </div>
            </div>
          </section>

        }
      </div>
    )
  }
}

export default connect(
  ({ auth }) => ({ auth }),
  { startSignUpProcess }
)(GlobalSignUp)
