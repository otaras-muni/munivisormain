import Joi from "joi-browser"
import dateFormat from "dateformat"

const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

const officePhone = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  countryCode: Joi.string()
    .allow("")
    .required()
    .label("Country Code")
    .optional(),
  phoneNumber: Joi.string()
    .allow("")
    .required()
    .label("Phone Number")
    .optional(),
  extension: Joi.string()
    .allow("")
    .label("Extension")
    .optional()
})

const officeFax = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  faxNumber: Joi.string()
    .allow("")
    .label("Fax Number")
    .optional()
})

const officeEmails = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  emailId: Joi.string()
    .allow("")
    .email()
    .required()
    .label("Email Id")
    .optional()
})

const entityAddressSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  addressName: Joi.string()
    .allow("")
    .label("Address Name")
    .optional(),
  isPrimary: Joi.boolean()
    .label("Is Primary")
    .optional(),
  isHeadQuarter: Joi.boolean()
    .required()
    .label("Is HeadQuarter")
    .optional(),
  /* isActive: Joi.boolean().required().label("Is Active").optional(), */
  website: Joi.string()
    .allow("")
    .label("WebSite")
    .optional(),
  officePhone: Joi.array().items(officePhone),
  officeFax: Joi.array().items(officeFax),
  officeEmails: Joi.array().items(officeEmails),
  addressLine1: Joi.string()
    .required()
    .label("Address Line 1")
    .optional(),
  addressLine2: Joi.string()
    .allow("")
    .label("Address Line 2")
    .optional(),
  country: Joi.string()
    .required()
    .label("Country")
    .optional(),
  county: Joi.string()
    .required()
    .label("County")
    .optional(),
  state: Joi.string()
    .required()
    .label("State")
    .optional(),
  city: Joi.string()
    .required()
    .label("City")
    .optional(),
  zipCode: {
    _id: Joi.string()
      .allow("")
      .optional(),
    zip1: Joi.string()
      .min(5)
      .max(5)
      .required()
      .label("Zip Code 1")
      .optional(),
    zip2: Joi.string()
      .min(4)
      .max(4)
      .allow("")
      .label("Zip Code 2")
      .optional()
  },
  formatted_address: Joi.string()
    .allow("")
    .label("Formatted Address")
    .optional(),
  url: Joi.string()
    .allow("")
    .label("URL")
    .optional(),
  location: {
    _id: Joi.string()
      .allow("")
      .optional(),
    longitude: Joi.string()
      .allow("")
      .label("Longitude")
      .optional(),
    latitude: Joi.string()
      .allow("")
      .label("Latitude")
      .optional()
  }
})

const firmAddOnsSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  serviceName: Joi.string()
    .label("Service Name")
    .required()
    .optional(),
  serviceEnabled: Joi.boolean()
    .required()
    .optional()
})

const marketRole = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  label: Joi.string()
    .label("Market Role")
    .optional(),
  value: Joi.string()
    .label("Market Role")
    .optional()
})

const entityFlagSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  marketRole: Joi.array()
    .items(
      Joi.string()
        .required()
        .optional()
    )
    .min(1)
    .unique()
    .required()
    .label("Market Role")
    .optional(),
  issuerFlags: Joi.array()
    .items(
      Joi.string()
        .required()
        .optional()
    )
    .min(1)
    .unique()
    .required()
    .label("Issuer Flag")
    .optional()
})

const entityLinkCusipSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  debtType: Joi.string()
    .required()
    .label("Debt Type")
    .optional(),
  associatedCusip6: Joi.string()
    .required()
    .label("Associated")
    .optional()
})

const entityLinkBorrowersObligorsSchema = Joi.object().keys({
  _id: Joi.string()
    .allow("")
    .optional(),
  borOblRel: Joi.string()
    .required()
    .optional(),
  borOblFirmName: Joi.string()
    .required()
    .optional(),
  borOblDebtType: Joi.string()
    .required()
    .optional(),
  borOblCusip6: Joi.string()
    .required()
    .optional(),
  borOblStartDate: Joi.date()
    .min(dateFormat(new Date(), "yyyy-mm-dd"))
    .required()
    .optional(),
  borOblEndDate: Joi.date()
    .min(dateFormat(new Date(), "yyyy-mm-dd"))
    .required()
    .optional()
})

// eslint-disable-next-line no-unused-vars
const firmDetailSchema = type =>
  Joi.object().keys({
    _id: Joi.string()
      .allow("")
      .optional(),
    entityFlags:
      type === "address" ? entityFlagSchema.optional() : entityFlagSchema,
    isMuniVisorClient: Joi.boolean()
      .required()
      .optional(),
    msrbFirmName: Joi.string()
      .allow("")
      .label("MSRB Firm Name")
      .optional(),
    msrbRegistrantType: Joi.string()
      .allow("")
      .label("MSRB Registrant Type")
      .optional(),
    msrbId: Joi.string()
      .allow("")
      .label("MSRB Id")
      .optional(),
    entityAliases: Joi.array()
      .items(
        Joi.string()
          .label("Aliases")
          .optional()
      )
      .optional(),
    firmName: Joi.string()
      .required()
      .label("Firm Name")
      .optional(),
    taxId: Joi.string()
      .allow(["", null])
      .label("Tax Id")
      .optional(),
    businessStructure: Joi.string()
      .allow(["", null])
      .label("Business Structure")
      .optional(),
    numEmployees: Joi.string()
      .allow("")
      .label("Number Of Employees")
      .optional(),
    annualRevenue: Joi.string()
      .allow("")
      .label("Annual Revenue")
      .optional(),
    addresses:
      type === "entity"
        ? Joi.array()
            .items(entityAddressSchema)
            .required()
            .optional()
        : Joi.array()
            .items(entityAddressSchema)
            .optional(),
    __v: Joi.number()
      .integer()
      .optional()
  })

export const validateThirdPartyDetail = (inputFirmDetail, type) =>
  Joi.validate(inputFirmDetail, firmDetailSchema(type), {
    abortEarly: false,
    stripUnknown: false
  })
