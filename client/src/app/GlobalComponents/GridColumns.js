import React from "react"
import { Link } from "react-router-dom"
import { numberWithCommas, transactionUrl } from "GlobalUtils/helpers"
import "ag-grid-community/dist/styles/ag-grid.css"
import "ag-grid-community/dist/styles/ag-theme-balham.css"
import "ag-grid-enterprise"
import dateFormat from "../../globalutilities/dateFormat"

const highlightSearch = (value, searchQuery) => {
  const reg = new RegExp(
    searchQuery ? searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&") : "",
    "i"
  )
  try {
    return value.replace(reg, str => `<mark>${str}</mark>`)
  } catch (ex) {
    return value
  }
}

export const tranColumns = (search, changeSelectedAction, user) => {

  const action = {
    field: "action",
    headerName: "Action",
    pinned: "right",
    cellRendererFramework: row => {
      const item = row.data
      return (
        <div className="hpTablesTd tooltips">
          <div className="select is-small is-link" style={{verticalAlign: "unset"}}>
            <select value="" onChange={(e) => changeSelectedAction(e.target.value, item)}>
              <option value="">Action</option>
              <option value="copy">Make a copy</option>
            </select>
          </div>
        </div>
      )
    }
  }

  const tranCols = [
    {
      headerName: "Client Name",
      field: "tranClientName",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div className="hpTablesTd">
            <Link
              to={`/clients-propects/${item.clientEntityId || ""}/entity` || ""}
              dangerouslySetInnerHTML={{
                __html: highlightSearch(item.tranClientName || "-", search)
              }}
            />
          </div>
        )
      }
    },
    {
      headerName: "Description",
      field: "activityDescription",
      cellRendererFramework: row => {
        const item = row.data
        return transactionUrl(item.tranType, item.tranSubType, item.activityDescription, item.tranId, search)
      }
    },
    {
      headerName: "Type",
      field: "tranType",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div
            className="hpTablesTd "
            dangerouslySetInnerHTML={{
              __html: highlightSearch(
                item.tranType === "Others"
                  ? item.tranSubType
                  : `${item.tranType} / ${item.tranSubType}` || "-",
                search
              )
            }}
          />
        )
      }
    },
    {
      headerName: "Principal Amount($)",
      field: "tranParAmount",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div className="hpTablesTd">
            {numberWithCommas(item.tranParAmount)}
          </div>
        )
      }
    },
    {
      headerName: "Lead Manager",
      field: "tranAssigneeDetails.userFullName",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div className="hpTablesTd">
            {
              (item.tranAssigneeDetails &&
                item.tranAssigneeDetails.userFullName) ?
                <Link
                  to={`/admin-users/${(item.tranAssigneeDetails &&
                    item.tranAssigneeDetails.userId) ||
                  ""}/users` || ""}
                  dangerouslySetInnerHTML={{
                    __html: highlightSearch((item.tranAssigneeDetails &&
                      item.tranAssigneeDetails.userFullName) ||
                      "-", search)
                  }}
                /> : "-"
            }
          </div>
        )
      }
    },
    {
      field: "middleName",
      headerName: "Coupon Type",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div
            className="hpTablesTd"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.middleName || "-", search)
            }}
          />
        )
      }
    },
    {
      headerName: "Purpose/Sector",
      field: "tranPrimarySector",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div className="hpTablesTd" dangerouslySetInnerHTML={{
            __html: highlightSearch(item.tranPrimarySector || "-", search)
          }} />
        )
      }
    },
    {
      headerName: "Expected Pricing Date",
      field: "tranPricingDate",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div className="hpTablesTd">
            {dateFormat(item.tranPricingDate || "")}
          </div>
        )
      }
    },
    {
      headerName: "Expected Closing Date",
      field: "tranExpectedAwardDate",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div className="hpTablesTd">
            {dateFormat(item.tranExpectedAwardDate || "")}
          </div>
        )
      }
    },
    {
      headerName: "Status",
      field: "tranStatus",
      cellRendererFramework: row => {
        const item = row.data
        return (
          <div
            className="hpTablesTd"
            dangerouslySetInnerHTML={{
              __html: highlightSearch(item.tranStatus || "-", search)
            }}
          />
        )
      }
    }
  ]

  if(user && user.userEntitlement === "global-edit"){
    tranCols.push(action)
  }
  return tranCols
}
