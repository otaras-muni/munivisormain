import React from "react"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"

export const DocMetaDetails  = (props) => {
  const toggleModal = () => {
    props.onModalChange({docModal: {modal: false, doc: {}}})
  }

  return(
    <Modal
      closeModal={toggleModal}
      modalState={props.modalState}
      title={props.doc.docFileName}>
      <div>
        <span><strong>Added by user : </strong></span>
        <span> {props.doc.createdUserName}</span>
      </div>
      <div>
        <span><strong>Type : </strong></span>
        <span> {props.doc.docType}</span>
      </div>
      <div>
        <span><strong>Category : </strong></span>
        <span> {props.doc.docCategory}</span>
      </div>
      <div>
        <span><strong>Sub category : </strong></span>
        <span> {props.doc.docSubCategory}</span>
      </div>
    </Modal>
  )
}
export default DocMetaDetails
