import React from "react"
import {Modal} from "../FunctionalComponents/TaskManagement/components/Modal"
import {MultiSelect} from "./TextViewBox"

export const DocumentSettingModal = (props) => {
  const onUserChange = (name, items) => {
    const deals = props.nav1 === "deals"
    const otherTrans = props.nav1 === "loan" || props.nav1 === "derivative" || props.nav1 === "marfp" || props.nav1 === "others"
    const contactName = deals ? "dealPartContactName" : otherTrans ? "partContactName" /* : props.nav1 === "rfp" ? "rfpParticipantContactName" */ : ""
    const contactId = deals ? "dealPartContactId" : otherTrans ? "partContactId" /* : props.nav1 === "rfp" ? "rfpParticipantContactId" */ : ""
    const firmName = deals ? "dealPartFirmName" : otherTrans ? "partFirmName" /* : props.nav1 === "rfp" ? "rfpParticipantFirmName" */ : ""
    const firmId = deals ? "dealPartFirmId" : otherTrans ? "partFirmId" /* : props.nav1 === "rfp" ? "rfpParticipantFirmId" */ : ""
    props.onChangeItems({
      [name]: items.map(item => {
        if(name === "users"){
          return {
            id: item.id || "",
            _id: item[firmId] || "",
            name: item[contactName] || "",
            contactId: item[contactId] || "",
            contactName: item[contactName] || "",
          }
        } else {
          return {
            id: item.id || "",
            _id: item[firmId] || "",
            name: item[firmName] || "",
            firmId: item[firmId] || "",
            firmName: item[firmName] || "",
          }
        }
      })
    }, name)
  }

  return(
    <Modal
      closeModal={props.onSettingModalClose}
      modalState={props.settingModal}
      saveModal={props.onSettingSave}
      disabled={props.selectUser === "firms" ? (props.setting && props.setting.firms.length === 0 ) : null || props.selectUser === "user" ? (props.setting && props.setting.users.length === 0 ) : null}
      fullWidth
      styleBody={{overflow: "unset"}}
      modalWidth={{overflow: "unset"}}
      title="Documents Setting"
    >
      <div className="columns">
        <div className="column">
          <label className="radio-button">
            Firm Level
            <input
              type="radio"
              name="selectUser"
              value="firms"
              checked={props.selectUser === "firms"}
              onChange={(e) => props.onSettingChange(e)}
            />
            <span className="checkmark" />
          </label>
        </div>
        <div className="column">
          <label className="radio-button">
            User Level
            <input
              type="radio"
              name="selectUser"
              value="user"
              checked={props.selectUser === "user"}
              onChange={(e) => props.onSettingChange(e)}
            />
            <span className="checkmark" />
          </label>
        </div>
      </div>
      {props.selectUser === "firms" || props.selectUser === "user" ?
        <div className="columns">
          <div className="column">
            <MultiSelect
              label="Select Firms"
              filter
              data={props.dropDown.firms}
              value={(props.setting && props.setting.firms ) || []}
              placeholder="Select firms"
              onChange={item => onUserChange("firms", item)}
              style={{ maxWidth: "unset" }}
              isFull
            />
          </div>
        </div>
        : null
      }
      {props.selectUser === "user" && props.setting && props.setting.firms.length > 0 ?
        <div className="columns">
          <div className="column">
            <MultiSelect
              label="Select Users"
              filter
              data={props.dropDown.users}
              value={(props.setting && props.setting.users ) || []}
              placeholder="Select firms"
              onChange={item => onUserChange("users", item)}
              style={{ maxWidth: "unset" }}
              isFull
            />
          </div>
        </div>
        : null
      }
    </Modal>
  )
}

export default DocumentSettingModal
