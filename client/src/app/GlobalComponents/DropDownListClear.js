import React from "react"
import { DropdownList } from "react-widgets"

const DropDownListClear = (props) => {
  return (
    <div className="custom-dropdown-list">
      <DropdownList
        {...props}
        filter="contains"
      />
      {
        props.isHideButton ?
          <i className="fa fa-times fa-1 custom-dropdown-close" onClick={props.onClear}/> : null
      }
    </div>
  )
}

export default DropDownListClear
