import React, { Component } from "react"
import { connect } from "react-redux"
import Dropzone from "react-dropzone"
import qs from "qs"
import fileSize from "file-size"
import moment from "moment"
import mime from "mime"

import Loader from "./Loader"
import withAuditLogs from "./withAuditLogs"
import {Modal} from "../FunctionalComponents/TaskManagement/components/Modal"
import {
  getSignedUrl,
  getDocInfo,
  getS3ObjectVersions,
  addDocInDB,
  updateVersionInDB
} from "../StateManagement/actions/docs_actions"

class DragAndDropFileInState extends Component {
  constructor(props) {
    super(props)
    this.state = {
      files: [],
      uploadedFiles: [],
      newVersion: [],
      conflictDocId: [],
      inProgress: false,
      showModal: false
    }
    this.uploadedFiles = []
  }

  componentWillReceiveProps(nextProps) {
    if(!this.props.isSave && nextProps.isSave){
      console.log("componentWillReceiveProps")
      this.setState({
        loadDocuments: nextProps.loadDocuments || []
      },() => this.checkOnS3File())
    }
  }

  onHandleDrop = files => {
    this.setState({
      conflictDocId: [],
      files
    },() => {
      if(this.props.saveFileInState){
        this.props.saveFileInState(files)
      }
    })
  }

  onFileUpload = async () => {
    this.setState({ inProgress: true })
    const { loadDocuments, conflictDocId, newVersion } = this.state
    const { user } = this.props
    const files = loadDocuments

    console.log("files : ", loadDocuments)
    if (files && Array.isArray(files) && files.length) {
      const { bucketName, contextId, contextType, tenantId } = this.props

      for (const i in files) {
        const { docCategory, docSubCategory, docType } = files[i]
        const { file } = files[i]
        let fileName = file.name
        const tags = {
          docCategory:
            (docCategory &&
              docCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
            "",
          docSubCategory:
            (docSubCategory &&
              docSubCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
            "",
          docType:
            (docType && docType.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
            "",
          docContext:
            (contextType &&
              contextType.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
            "",
          docName: (fileName &&
            fileName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) ||
            "" || "",
          uploadUserName:
            (user && `${user.userFirstName} ${user.userLastName}`) || "",
          uploadDate: moment(new Date()).format("MM-DD-YYYY"),
          ...this.props.tags
        }
        const extnIdx = fileName.lastIndexOf(".")
        if (extnIdx > -1) {
          fileName = `${fileName.substr(
            0,
            extnIdx
          )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
        }
        console.log("fileName2 : ", fileName)
        const contentType = file.type
        if (!fileName) {
          return this.setState({
            error: "No file name provided",
            inProgress: false
          })
        }
        let filePath
        if (tenantId && contextId && contextType) {
          filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
        }
        const opType = "upload"
        const options = { contentType, tags }

        const res = await getSignedUrl({  // eslint-disable-line
          opType,
          bucketName,
          fileName: filePath,
          options
        })
        if (res) {
          console.log("res : ", res)

          const findConflictFile = conflictDocId.find(doc =>
            doc.contextId === contextId &&
            doc.contextType === contextType &&
            doc.tenantId === tenantId &&
            doc.originalName === file.name
          )

          const document = findConflictFile && newVersion && newVersion.indexOf(findConflictFile._id) !== -1 ? findConflictFile : loadDocuments[i] // eslint-disable-line

          delete files[i].file

          const fileTags = files[i]
          this.uploadWithSignedUrl(
            bucketName,
            res.data.url,
            file,
            fileName,
            tags,
            document,
            fileTags,
            upFiles => {
              const conflictDocIds = conflictDocId.map(d => d._id)
              const docs = upFiles.uploadedFiles.filter(upFile => conflictDocIds.indexOf(upFile.docAWSFileLocation) === -1)
              console.log(parseInt(newVersion.length+docs.length, 10), parseInt(files.length, 10))

              if (parseInt(newVersion.length + docs.length, 10) === parseInt(files.length, 10)) {
                console.log("FUNCTION IN SIDE CONSOLE")
                console.log((parseInt(i,10)+1), parseInt(files.length, 10))
                if(this.props.audit !== "audit" || this.props && this.props.audit === undefined ) {
                  files && files.forEach(item => {
                    const catLog = item.docCategory && item.docCategory !== "N/A" ? `Category: ${item.docCategory}` : ""
                    const subCatlog = item.docSubCategory && item.docSubCategory !== "N/A" ? `, Sub category: ${item.docSubCategory}` : ""
                    const typeLog = item.docType && item.docType !== "N/A" ? ` Type: ${item.docType}` : ""
                    const log = `Documents upload ${catLog}${subCatlog}${typeLog} File: ${item.name || item.docFileName}`
                    this.props.addAuditLog({
                      userName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
                      log,
                      date: new Date(),
                      key: "documents"
                    })
                  })
                }
                this.props.onSave(docs)
                this.setState({
                  files: [],
                  loadDocuments: null,
                  error: "",
                  message: "",
                  newVersion: [],
                  conflictDocId: [],
                  inProgress: false
                })
                this.uploadedFiles = []
              }
            }
          )
        } else {
          this.setState({
            error: "Error in getting signed url for upload",
            message: "",
            inProgress: false
          })
        }
      }
    } else {
      console.log("No file")
      this.setState({ error: "No file", inProgress: false })
    }
  }

  checkOnS3File = async () => {
    this.setState({ inProgress: true })
    const { loadDocuments, conflictDocId, newVersion } = this.state
    const { contextId, contextType, tenantId } = this.props
    const files = loadDocuments

    if (files && Array.isArray(files) && files.length) {
      console.log("files : ", loadDocuments)

      for (const i in files) {
        const { file, docCategory, docSubCategory, docType } = files[i]
        const fileName = file.name
        if (!fileName) {
          return this.setState({
            error: "No file name provided",
            inProgress: false
          })
        }
        let filePath
        if (tenantId && contextId && contextType) {
          filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
        }
        const docFilter = {
          contextId,
          contextType,
          tenantId,
          originalName: fileName
        }

        const docMeta = {
          category: docCategory || "",
          subCategory: docSubCategory || "",
          type: docType || ""
        }
        Object.keys(docMeta || {}).forEach(k => {
          docFilter[`meta.${k}`] = docMeta[k]
        })

        const res = await getDocInfo({ params: { ...docFilter } })
        if(res){
          const doc = (res && res.data[0]) || {}
          const { _id } = doc
          if (_id) {
            conflictDocId.push(doc)
          }
          if ((parseInt(i,10)+1) === parseInt(files.length, 10)) {
            if(conflictDocId && conflictDocId.length){
              this.setState({
                conflictDocId,
                inProgress: false,
                showModal: true
              })
            }else {
              this.onFileUpload()
            }
          }
        }
        /* axios
          .get(`${muniApiBaseURL}docs`, { params: { ...docFilter },headers:{"Authorization":getToken()} })
          .then(async (res) => {
            const doc = (res && res.data[0]) || {}
            const { _id } = await doc
            if (_id) {
              conflictDocId.push(doc)
            }
            if ((parseInt(i,10)+1) === parseInt(files.length, 10)) {
              if(conflictDocId && conflictDocId.length){
                this.setState({
                  conflictDocId,
                  inProgress: false,
                  showModal: true
                })
              }else {
                this.onFileUpload()
              }
            }
          }) */
      }
    } else {
      console.log("No file")
      this.setState({ error: "No file", inProgress: false })
    }
  }

  uploadWithSignedUrl = (
    bucketName,
    signedS3Url,
    file,
    fileName,
    tags,
    document,
    fileTags,
    callback
  ) => {
    console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      bucketName,
      fileName,
      file.type
    )
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("no tagging")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = async () => {
      console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        console.log("file uploaded ok")
        // this.setState({ error: "", message: `Last upload successful for "${fileName}" at ${new Date()}` })
        if(document && document.meta){
          await this.updateVersionDocsDB(
            bucketName,
            file,
            fileName,
            document,
            fileTags,
            fileCallback => {
              callback({
                error: "",
                message: `Last upload successful for "${fileName}" at ${new Date()}`,
                ...fileCallback
              })
            }
          )
        }else {
          await this.updateDocsDB(
            bucketName,
            file,
            fileName,
            document,
            fileTags,
            fileCallback => {
              callback({
                error: "",
                message: `Last upload successful for "${fileName}" at ${new Date()}`,
                ...fileCallback
              })
            }
          )
        }
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded :", err)
      this.setDocUploadErr("Error in uploading to S3")
    }
    xhr.send(file)
  }

  updateDocsDB = async (bucketName, file, fileName, document, fileTags, cb) => {
    const { contextId, contextType, tenantId, docMeta, versionMeta, user } = this.props
    const {  name } = document
    const meta = { ...docMeta }
    let filePath
    if (tenantId && contextType && contextId) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    console.log("bucketName, fileName : ", bucketName, fileName)
    const res = await getS3ObjectVersions({
      bucketName,
      fileName: filePath
    })
    if(res){
      let versionId = ""
      let uploadDate = ""
      let size = ""
      res.data.result.Versions.some(v => {
        if (v.IsLatest) {
          versionId = v.VersionId
          uploadDate = v.LastModified
          size = fileSize(v.Size).human("jedec")
          return true
        }
      })
      if (versionId) {
        meta.versions = [
          {
            versionId,
            name: fileName,
            originalName: file.name,
            uploadDate,
            uploadedBy: (user && `${user.userFirstName} ${user.userLastName}`) || "",
            size,
            ...versionMeta
          }
        ]
        const doc = {
          name: fileName,
          originalName: file.name,
          meta: {
            ...meta,
            category: fileTags.docCategory,
            subCategory: fileTags.docSubCategory,
            type: fileTags.docType,
          },
          contextType,
          contextId,
          tenantId,
          folderId: document.folderId,
          folderName: document.folderName,
          createdBy: (user && `${user.userFirstName} ${user.userLastName}`) || "",
          createdDate: new Date()
        }
        const res = await addDocInDB(doc)
        if(res){
          console.log("inserted ok in docs db : ", res)
          this.uploadedFiles.push({
            ...fileTags,
            docFileName: file.name,
            docAWSFileLocation: res.data._id,
          })
          cb({
            message: "",
            error: "",
            inProgress: false,
            uploadedFiles: this.uploadedFiles
          })
        }else {
          this.setDocUploadErr("Error in inserting in docs DB")
        }
      } else {
        console.log("No version error")
        this.setDocUploadErr("S3 version error")
      }
    }else {
      this.setDocUploadErr("Error in getting S3 versions")
    }
  }

  updateVersionDocsDB = async ( bucketName, file, fileName, document, fileTags, cb) => {
    const type = mime.getExtension(file.type)
    const { _id, meta } = document
    const { contextId, contextType, tenantId } = this.props
    let filePath
    if (tenantId && contextType && contextId) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    const docId = _id || ""

    const res = await getS3ObjectVersions({
      bucketName,
      fileName: filePath
    })
    if(res){
      console.log("got versions : ", res)
      // let latestVersion = res.Versions.filter(v => v.IsLatest)[0];
      let versionId
      let uploadDate
      let size
      res.data.result.Versions.some(v => {
        if (v.IsLatest) {
          versionId = v.VersionId
          uploadDate = v.LastModified
          size = fileSize(v.Size).human("jedec")
          return true
        }
      })
      if (versionId) {
        if (docId) {
          const res = await getDocInfo({ params: { _id: docId } })
          if(res){
            console.log("res : ", res)
            const doc = (res && res.data[0]) || {}
            const { _id } = doc
            if (_id) {
              const res = await updateVersionInDB({
                _id,
                versions: [
                  {
                    versionId,
                    name: fileName,
                    originalName: file.name,
                    uploadDate,
                    size,
                    type,
                  }
                ],
                name: fileName,
                originalName: file.name,
                option: "add"
              })
              if(res){
                console.log("updated versions ok in docs db : ", res)
                this.setState({
                  message: "",
                  error: "",
                  inProgress: false
                })
                this.uploadedFiles.push({
                  ...fileTags,
                  docFileName: file.name,
                  docAWSFileLocation: _id,
                })
                cb({
                  message: "",
                  error: "",
                  inProgress: false,
                  uploadedFiles: this.uploadedFiles
                })
              }else {
                this.setDocUploadErr("Error in updating versions in docs DB")
              }
            } else {
              meta.versions = [
                {
                  versionId,
                  name: fileName,
                  originalName: file.name,
                  uploadDate,
                  size,
                  type,
                }
              ]
              const doc = {
                name: fileName,
                originalName: file.name,
                meta: {
                  ...meta,
                  category: fileTags.docCategory,
                  subCategory: fileTags.docSubCategory,
                  type: fileTags.docType,
                },
                contextType,
                contextId,
                tenantId
              }

              const res = await addDocInDB(doc)
              if(res){
                console.log("inserted ok in docs db : ", res)
                this.setState({
                  message: "",
                  error: "",
                  inProgress: false
                })
                this.uploadedFiles.push({
                  ...fileTags,
                  docFileName: file.name,
                  docAWSFileLocation: res.data._id,
                })
                cb({
                  message: "",
                  error: "",
                  inProgress: false,
                  uploadedFiles: this.uploadedFiles
                })
              }else {
                this.setDocUploadErr("Error in inserting in docs DB")
              }
            }
          }else {
            this.setDocUploadErr("Error in getting docs")
          }
        }
      } else {
        console.log("No version error")
        this.setDocUploadErr("S3 version error")
      }
    }else {
      this.setDocUploadErr("Error in getting S3 versions")
    }
  }

  setDocUploadErr = (msg) => {
    this.setState({
      message: "",
      error: msg || "Something went wrong!",
      inProgress: false
    },() =>this.saveReset())
  }

  handleCheckClick = (event) => {
    const {newVersion} = this.state

    if(newVersion.indexOf(event.target.value) !== -1){
      const finIndex = newVersion.indexOf(event.target.value)
      newVersion.splice(finIndex, 1)
    }else {
      newVersion.push(event.target.value)
    }

    this.setState({
      newVersion
    })
  }

  toggleModal = () => {
    this.setState({
      newVersion: [],
      conflictDocId: [],
      showModal: false,
    },() =>this.saveReset())
  }

  saveReset = () => {
    this.props.setStateParentLevel({
      isSaveDisabled: false,
      isSave: false
    })
  }
  onSave = () => {
    this.setState({
      showModal: false,
    },() => this.onFileUpload())
  }

  render() {
    const { error, conflictDocId, files, newVersion, showModal, inProgress } = this.state

    const styles = {
      width: "100%",
      height: 100,
      border: "2px dashed #ccc",
      padding: 35
    }

    if (inProgress) {
      return <Loader />
    }
    return (
      <section>
        <Modal
          closeModal={this.toggleModal}
          modalState={showModal}
          saveModal={this.onSave}
          title="New file or new version ?"
        >
          <p className="multiExpGrpLbl">
            We have found an existing file with the same name and upload options
            selected by you.
          </p>
          <p className="multiExpGrpLbl">
            Please confirm if you want to upload a new version to the existing
            file or upload as a separate new file?
          </p>

          {
            conflictDocId ?
              conflictDocId.map((doc, index) => (
                <div className="columns" key={index.toString()}>
                  <div className="column">
                    <p className="multiExpLblBlk">{doc.originalName} </p>
                  </div>
                  <div className="column" >
                    <label className="radio-button">
                      New Version
                      <input
                        type="radio"
                        value={doc._id}
                        checked={newVersion.indexOf(doc._id) !== -1}
                        onChange={this.handleCheckClick}
                      />
                      <span className="checkmark" />
                    </label>
                  </div>
                  <div className="column" >
                    <label className="radio-button">
                      New File
                      <input
                        type="radio"
                        value={doc._id}
                        checked={newVersion.indexOf(doc._id) === -1}
                        onChange={this.handleCheckClick}
                      />
                      <span className="checkmark" />
                    </label>
                  </div>
                </div>
              )) : null
          }
        </Modal>
        <div>
          {
            this.props.chooseMultiple ?
              <div className="dropzone">
                <Dropzone onDrop={this.onHandleDrop} style={styles}>
                  <p style={{ textAlign: "center" }}>
                  Try dropping some files here, or click to select files to upload.
                  </p>
                </Dropzone>
              </div>
              : null }
          <p className="is-small text-error has-text-danger">{error || ""}</p>
        </div>
      </section>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(DragAndDropFileInState)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
