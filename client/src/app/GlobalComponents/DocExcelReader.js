import React, { Component } from "react"
import mime from "mime"
import ExcelSaver from "./ExcelSaver"
import {excelData} from "../../../src/app/StateManagement/actions/Common"

class DocExcelReader extends Component {
  onClickUpload = event => {
    event.target.value = null
  }

  onfetchExcelData = async event => {
    const {jsonSheet} = this.props
    const file = event.target.files[0]
    const fileFormet = mime.getExtension(file.type)

    if(fileFormet === "xlsx" || fileFormet === "xls"){
      const reader = new FileReader()

      reader.onload = async event => {
        const data = event.target.result
        const payload = {}
        payload.data = data
        payload.jsonSheet = jsonSheet

        const res = await excelData(payload)

        if((res && res.status && res.status === 200 )){
          this.props.onExcelRead(res.data)
        }else {
          this.props.onExcelRead("Invalid data format")
        }
      }
      reader.onerror = ex => {
        console.log(ex)
      }
      reader.readAsBinaryString(file)
    }else{
      this.props.onExcelRead("Invalid file type")
    }
  }

  clickUpload = () => {
    this.uploadFileInput.click()
  }

  render() {
    const {jsonSheet, notShowSample} = this.props
    return (
      <div>
        <input
          type="file"
          ref={el => {
            this.uploadFileInput = el
          }}
          style={{ display: "none" }}
          onClick={this.onClickUpload}
          accept=".xls, .xlsx"
          onChange={this.onfetchExcelData}
        />
        <div
          className="file is-small is-link"
          onClick={this.clickUpload}
          style={{ display: "table-cell" }}
        >
          <span className="file-cta">
            <span className="file-icon">
              <i className="fas fa-upload" />
            </span>
            <span className="file-label">import</span>
          </span>
        </div>
        {(jsonSheet && !notShowSample) ? (
          <ExcelSaver jsonSheet={this.props.jsonSheet}/>
        ) : null}
      </div>
    )
  }
}

export default DocExcelReader
