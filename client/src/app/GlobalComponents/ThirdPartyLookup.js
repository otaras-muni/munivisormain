import React, { Component } from "react"

import { DropdownList } from "react-widgets"

import { getThirdPartyLookUpData } from "GlobalUtils/helpers"
import DropDownListClear from "./DropDownListClear";

class ThirdPartyLookup extends Component {
  constructor(props) {
    super(props)
    this.state = { entityName: "", waiting: false, entities: [],
      searchString: "" }
    // this.changeSearchString = this.changeSearchString.bind(this)
    this.onSearch = this.onSearch.bind(this)
    this.onCreate = this.onCreate.bind(this)
    this.selectEntity = this.selectEntity.bind(this)
    this.getEntityData = this.getEntityData.bind(this)
    this.performSearch = this.performSearch.bind(this)
    this.toggleOpen = this.toggleOpen.bind(this)
    // this.delayedSearch = this.delayedSearch.bind(this)
  }

  componentDidMount() {
    const { entityName } = this.props
    this.setState({ entityName })
  }

  componentWillReceiveProps(nextProps) {
    const { entityName } = nextProps
    if(entityName !== this.props.entityName) {
      this.setState({ entityName })
    }
  }

  onSearch(searchString) {
    // console.log("search value1 : ", searchString)
    window.clearTimeout(this.timeout)
    this.setState({ entities: [], waiting: true, searchString },
      this.performSearch)
    // if(this.timeout) {
    //   window.clearTimeout(this.timeout)
    //   this.timeout = null
    // } else {
    //   this.timeout = window.setTimeout(this.getEntityData.bind(this, value), 500)
    // }
  }

  onCreate(value) {
    const { participantType } = this.props
    console.log("create value : ", value)
    const entityName = { _id: "", firmName: value, isExisting: false,
      participantType }
    this.setState({ entityName })
    this.props.onChange(entityName)
  }

  onChange(value) {
    console.log("change value : ", value)
  }

  async getEntityData() {
    const { searchString } = this.state
    // console.log("search value3 : ", searchString)
    if(!searchString) {
      return { waiting: false, entities: [] }
    }
    const entities = await getThirdPartyLookUpData({
      searchString })
    // entities.sort((x, y) => {
    //   if(x.isExisting && !y.isExisting) {
    //     return -1
    //   } if(y.isExisting && !x.isExisting) {
    //     return 1
    //   }
    //   return (x-y)
    // })
    console.log("entities : ", entities)
    this.setState({ entities, waiting: false })
    // this.setState(async (prevState, props) => {
    //   console.log("search value3 : ", searchString)
    //   const { searchString } = prevState
    //   if(!searchString) {
    //     return { waiting: false, entities: [], entityName: "" }
    //   }
    //   const { entityType, participantType } = props
    //   const entities = await getEntityLookUpData({entityType,
    //     participantType, searchString })
    //   entities.sort((x, y) => {
    //     if(x.isExisting && !y.isExisting) {
    //       return -1
    //     } if(y.isExisting && !x.isExisting) {
    //       return 1
    //     }
    //     return (x-y)
    //   })
    //   console.log("entities : ", entities)
    //   return { entities, waiting: false }
    // })
  }

  performSearch() {
    const { searchString } = this.state
    // console.log("search value2 : ", searchString)
    if(searchString) {
      this.setState({ entityName: "" })
      this.timeout = window.setTimeout(this.getEntityData, 500)
    } else {
      window.clearTimeout(this.timeout)
      this.setState({ waiting: false, entities: [] })
    }
  }

  toggleOpen(value) {
    console.log("toggle value : ", value)
    if(!value) {
      const { entityName } = this.props
      if(entityName && !this.state.entityName) {
        this.setState( { entityName })
      }
    }
  }

  selectEntity(value) {
    console.log("select value : ", value)
    this.setState({ entityName: Object.keys(value).length === 0 ? "" : value.firmName })
    this.props.onChange(Object.keys(value).length === 0 ? "" : value)
  }

  render() {
    const { isRequired, viewOnly, style, notEditable, isWidth, error, isHide } = this.props
    const { waiting, entityName, entities } = this.state
    let disabled = false
    if(viewOnly) {
      disabled = true
    }
    return (
      <div className="columns"  style={style || null}>
        <div className={isWidth ? `column ${isWidth}` : "column is-11"}>
          {/* <DropdownList
            busy={waiting}
            value={entityName}
            data={entities}
            allowCreate="onFilter"
            textField="firmName"
            placeholder="Search by name"
            disabled={ disabled || notEditable }
            groupBy={value => (value.related ? "-Choose from Related Entities-" : "-Choose from Industry Reference-")}
            filter="contains"
            onSearch={this.onSearch}
            onCreate={this.onCreate}
            onChange={this.onChange}
            onSelect={this.selectEntity}
            onToggle={this.toggleOpen}
          /> */}
          <DropDownListClear
            busy={waiting}
            value={entityName}
            data={entities}
            allowCreate="onFilter"
            textField="firmName"
            placeholder="Search by name"
            disabled={ disabled || notEditable }
            groupBy={value => (value.related ? "-Choose from Related Entities-" : "-Choose from Industry Reference-")}
            filter="contains"
            onSearch={this.onSearch}
            onCreate={this.onCreate}
            onChange={this.onChange}
            onSelect={this.selectEntity}
            onToggle={this.toggleOpen}
            isHideButton={isHide}
            onClear={() => {this.selectEntity({})}}
          />
          {error ? <p className="text-error">{error}</p> : null}
        </div>
        {isRequired && <span className="icon has-text-danger column"><i className="fas fa-asterisk extra-small-icon" /></span>}
      </div>
    )
  }
}

export default ThirdPartyLookup
