/**
 * Created by 01 on 7/26/2018.
 */
import React from "react"
import { Prompt, withRouter } from "react-router-dom";
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import moment from "moment"
import { getPicklistByPicklistName, updateS3DocTags, getDocDetails, checkInvalidFiles } from "GlobalUtils/helpers"
import { fetchDocDetails, getS3FileGetURL } from "../StateManagement/actions"
import Accordion from "./Accordion"
import RatingSection from "./RatingSection"
import TableHeader from "./TableHeader"
import Loader from "./Loader"
import DocumentsView from "./DocumentsView"
import CONST from "../../globalutilities/consts"
import withAuditLogs from "./withAuditLogs"
import { DocumentsValidate } from "./DocumentsValidate"
import SendEmailModal from "./SendEmailModal"
import {
  sendEmailAlert,
  sendComplianceEmail,
  sendCRMEmail
} from "../StateManagement/actions/Transaction"
import DocModalDetails from "../FunctionalComponents/docs/DocModalDetails"
import DragAndDropFileInState from "./DragAndDropFileInState"
import DocumentList from "./DocumentList"

const cols = [
  [
    { name: "Category" },
    { name: "Sub-category" },
    { name: "Type" },
    { name: "Upload File<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
    { name: "Filename<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
    // { name: "Note" },
    { name: "Action" }
  ],
  [
    { name: "Filename" },
    { name: "Category" },
    { name: "Sub-category" },
    { name: "Type" },
    { name: "Created by" },
    { name: "LUD" },
    // { name: "Notes" },
    { name: "Status" },
    { name: "Action" }
  ]
]

const complianceCols = [
  [
    { name: "Category" },
    { name: "Sub-category" },
    { name: "Type" },
    { name: "Upload File<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
    { name: "Filename<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon' /></span>" },
    { name: "Action" }
  ],
  [
    { name: "Filename" },
    { name: "Type" },
    { name: "Last Updated By" },
    { name: "Last Updated On" },
    { name: "Status" },
    { name: "Action" }
  ]
]

class DocumentPage extends React.Component {
  constructor(props) {
    super(props)
    const { staticField, user } = this.props
    this.state = {
      bucketName: process.env.S3BUCKET,
      userName: "",
      columns: cols,
      loading: true,
      isSave: false,
      fileName: "",
      signedS3Url: "",
      selectedDocId: "",
      showSigneeInput: "",
      isEditable: "",
      auditLogs: [],
      uploadedFiles: [],
      loadDocuments: [],
      documents: [
        {
          ...cloneDeep(CONST.Documents),
          ...staticField,
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        }
      ],
      tempBankLoanDocuments: [
        {
          ...cloneDeep(CONST.Documents),
          ...staticField,
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        }
      ],
      errorMessages: {},
      waiting: false,
      doc: {},
      showModal: false,
      isSaveDisabled: false,
      documentsList: [],
      searchList: [],
      dropDown: {
        category: [],
        subCategory: [],
        docType: [],
        actions: [],
        suggestions: []
      },
      newDropDown:{},
      activeItem: [],
      modalState: false,
      chooseMultiple: false,
      searchText: "",
      fileTypeError: "",
      email: {
        category: "",
        message: "",
        subject: "",
        sendDocEmailLinks: true
      },
      sortBy: {
        type: "",
        sort: ""
      },
      errors: {}
    }
  }

  async componentDidMount() {
    const {
      documents,
      user,
      pickCategory,
      pickSubCategory,
      pickType,
      pickAction,
      transaction,
      nav1
    } = this.props
    let result = await getPicklistByPicklistName([
      pickSubCategory && pickCategory.toString(),
      pickSubCategory && pickSubCategory.toString(),
      pickAction && pickAction.toString(),
      (pickType && pickType.toString()) || "LKUPDOCTYPE",
      "LKUPDOCSTATUS"
    ])
    const multiSubCategory = (result[2] && result[2][pickCategory]) || {}
    result = (result.length && result[1]) || {}
    const subjectNote = transaction
      ? transaction.dealIssueTranType === "Deal"
        ? transaction.dealIssueTranIssueName ||
          transaction.dealIssueTranProjectDescription ||
          ""
        : transaction && transaction.actType === "MA-RFP"
          ? transaction.actIssueName || transaction.actProjectName || ""
          : transaction.rfpTranType === "RFP"
            ? transaction.rfpTranIssueName || transaction.rfpTranProjectDescription
            : (transaction && transaction.actTranIssueName) ||
          (transaction && transaction.actTranProjectDescription) ||
          ""
      : ""

    documents.forEach(doc => {
      doc.timeStamp = doc.LastUpdatedDate || doc.lastUpdatedDate ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix() : 0
    })
    this.setState(prevState => ({
      documentsList: documents && documents.length ? documents : [],
      dropDown: {
        ...this.state.dropDown,
        category: result[pickCategory] || [],
        subCategory: result[pickSubCategory] || [],
        multiSubCategory,
        docType: result[pickType] || result.LKUPDOCTYPE || [],
        actions: result[pickAction] || [],
        docStatus: result.LKUPDOCSTATUS || [],
        firms: cloneDeep(transaction && transaction[participants]) || [],
        // partUser: transaction && transaction.dealIssueParticipants || []
      },
      userName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
      loading: false,
      email: {
        ...prevState.email,
        subject: ["deals", "rfp", "loan", "marfp", "derivative", "others"].indexOf(nav1) !== -1? `Transaction - ${subjectNote} - Notification` : ""
      }
    }), () => {
      const {dropDown} = this.state
      const category = dropDown.category.map(e => e.label)
      const disabledCategory = dropDown.category.filter(e => !e.included).map(e => e.label)
      const docType = dropDown.docType.map(e => e.label)
      const disabledDocType = dropDown.docType.filter(e => !e.included).map(e => e.label)
      const subCategory = {}
      const subCategoryDisabled = {}
      dropDown.multiSubCategory && Object.keys(dropDown.multiSubCategory).forEach(data => {
        subCategory[data] = dropDown.multiSubCategory[data] && dropDown.multiSubCategory[data].map(e => e.label)
        subCategoryDisabled[data] = dropDown.multiSubCategory[data] && dropDown.multiSubCategory[data].filter(e => !e.included).map(e => e.label)
      })
      this.setState({
        newDropDown: {
          category,
          disabledCategory,
          docType,
          disabledDocType,
          subCategory,
          subCategoryDisabled
        }
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    const { documents } = this.props
    if (documents && nextProps.documents && documents.length !== nextProps.documents.length) {
      nextProps.documents.forEach(doc => {
        doc.timeStamp =
          doc.LastUpdatedDate || doc.lastUpdatedDate
            ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix()
            : 0
      })
      this.setState({
        documentsList: nextProps.documents
      })
    }
  }

  onChangeItem = (item, index, category, change, name) => {
    const items = this.state[category]
    items.splice(index, 1, item)
    if (change) {
      // this.updateAudit(category, change, name)
    }
    this.setState({
      [category]: items
    })
  }

  updateAudit = (category, change, key) => {
    const { userName } = this.state
    const { auditLogs } = this.props
    const alreadyExists = auditLogs.findIndex(audit => audit.key === key)
    const audit = {
      userName,
      log: `In ${category} ${change}`,
      date: new Date(),
      key
    }
    if(alreadyExists !== -1){
      auditLogs[alreadyExists] = audit
    }else {
      auditLogs.push(audit)
    }
    this.props.updateAuditLog(auditLogs)
  }

  onAdd = (key, RatingSection) => {
    const { userName } = this.state
    const { staticField, user } = this.props
    /* this.props.addAuditLog({
      userName,
      log: "Documents Add new Item",
      date: new Date(),
      key
    }) */
    this.setState({
      [key]: [
        ...this.state[key],
        key === "documents"
          ? {
            ...cloneDeep(CONST.Documents),
            ...staticField,
            createdBy: (this.props.user && this.props.user.userId) || "",
            createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
          }
          : {}
      ],activeItem: [RatingSection]
    })
  }

  onReset = category => {
    // const auditLogs = this.props.auditLogs.filter(x => x.key !== category)
    this.props.updateAuditLog([])
    this.setState(prevState => ({
      documents: cloneDeep(prevState.tempBankLoanDocuments)
    }))
  }

  updateSaveFlag = () => {
    const { documents } = this.state
    const {
      tranId,
      tags,
    } = this.props
    const payload = { _id: tranId, documents }

    documents &&
    documents.forEach(item => {
      item.docType === "" ? (item.docType = "N/A") : item.docType
      item.docCategory === "" ? (item.docCategory = "N/A") : item.docCategory
      item.docSubCategory === "" ? (item.docSubCategory = "N/A") : item.docSubCategory
    })
    const errors = DocumentsValidate(payload)
    if (tags) {
      delete tags.tenantId
      delete tags.tenantName
      delete tags.clientId
    }
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) { //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) { //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = "Required" // err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: "Required" // err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = "Required" // err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] = {
            [err.path[1]]: {
              [err.path[2]]: "Required" // err.message
            }
          }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: "Required" // err.message
          }
        }
      })
      this.setState({ errorMessages, activeItem: [0] })
      return
    }

    this.setState({
      isSaveDisabled: true,
      isSave: true,
      loadDocuments: payload.documents
    })
  }

  onSave = (documents) => {
    /* const { documents, email } = this.state
    const {
      tranId,
      nav1,
      user,
      tags,
      category,
      contextType,
      staticField
    } = this.props
    let payload = { _id: tranId, documents }
    documents &&
      documents.forEach(item => {
        item.docType === "" ? (item.docType = "N/A") : item.docType
        item.docCategory === "" ? (item.docCategory = "N/A") : item.docCategory
        item.docSubCategory === ""
          ? (item.docSubCategory = "N/A")
          : item.docSubCategory
      })
    const errors = DocumentsValidate(payload)
    if (tags) {
      delete tags.tenantId
      delete tags.tenantName
      delete tags.clientId
    }
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) {
          //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) {
            //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = "Required" // err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: "Required" // err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = "Required" // err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] = {
            [err.path[1]]: {
              [err.path[2]]: "Required" // err.message
            }
          }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: "Required" // err.message
          }
        }
      })
      this.setState({ errorMessages, activeItem: [0] })
      return
    }
    if (category) {
      payload = { _id: tranId, [category]: payload.documents }
    }

    documents.length &&
      documents.forEach(async doc => {
        const {
          docCategory,
          docSubCategory,
          docType,
          docAction,
          docAWSFileLocation,
          docFileName
        } = doc
        // const { contextName, ...restOfTheTags } = tags
        const revisedContext = tags && tags.contextName ? `${contextType}:${tags.contextName}` : contextType
        if (docAWSFileLocation) {
          const tag = {
            docCategory: (docCategory && docCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            docSubCategory: (docSubCategory && docSubCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            docContext: (revisedContext && revisedContext.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            docName: (docFileName && docFileName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            uploadUserName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
            uploadUserEntityName: user.firmName,
            uploadDate: moment(new Date()).format("MM-DD-YYYY")
          }
          console.log("The tags to be submitted are", tag)
          await updateS3DocTags(docAWSFileLocation, { ...tag })
        }
      })
    this.setState({
      isSaveDisabled: true
    },() => {
      this.props.onSave(payload, res => {
        if (res && res.status) {
          res.documentsList.forEach(doc => {
            doc.timeStamp =
              doc.LastUpdatedDate || doc.lastUpdatedDate
                ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix()
                : 0
          })
          this.setState({
            errorMessages: {},
            documents: [
              {
                ...cloneDeep(CONST.Documents),
                ...staticField,
                createdBy: (user && user.userId) || "",
                createdUserName:
                  (user && `${user.userFirstName} ${user.userLastName}`) || ""
              }
            ],
            documentsList: res.documentsList,
            isSaveDisabled: false,
            activeItem: [],
            uploadedFiles: []
          })
        } else {
          this.setState({
            errorMessages: {},
            isSaveDisabled: false,
            activeItem: []
          })
        }
      })
    }) */

    const {tranId, category, user, staticField} = this.props
    let payload = { _id: tranId, documents }
    if (category) {
      payload = { _id: tranId, [category]: payload.documents }
    }
    if(payload._id && documents.length){
      this.setState({
        isSaveDisabled: true
      },() => {
        this.props.onSave(payload, res => {
          if (res && res.status) {
            res.documentsList.forEach(doc => {
              doc.timeStamp =
                doc.LastUpdatedDate || doc.lastUpdatedDate
                  ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix()
                  : 0
            })
            this.setState({
              errorMessages: {},
              documents: [
                {
                  ...cloneDeep(CONST.Documents),
                  ...staticField,
                  createdBy: (user && user.userId) || "",
                  createdUserName:
                    (user && `${user.userFirstName} ${user.userLastName}`) || ""
                }
              ],
              documentsList: res.documentsList,
              isSaveDisabled: false,
              activeItem: [],
              isSave: false,
              uploadedFiles: []
            })
          } else {
            this.setState({
              errorMessages: {},
              isSaveDisabled: false,
              activeItem: [],
              isSave: false,
            })
          }
        })
      })
    }else {
      this.setState({
        errorMessages: {},
        documents:                                                                                                 [
          {
            ...cloneDeep(CONST.Documents),
            ...staticField,
            createdBy: (user && user.userId) || "",
            createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
          }
        ],
        isSaveDisabled: false,
        activeItem: [],
        uploadedFiles: [],
        isSave: false,
        loadDocuments: null
      })
    }
  }

  onFileAction = (value, doc) => {
    if (value === "See version history" || value === "open") {
      fetchDocDetails(doc.docAWSFileLocation).then(res => {
        if (value === "open") {
          this.getFileURL(this.state.bucketName, res.name, this.downloadFile)
        } else if (value === "See version history") {
          res.documentId = doc._id
          this.handleDocDetails(res)
        }
      })
    } else if (value === "Share document") {
      this.setState(prevState => ({
        modalState: true,
        email: {
          ...prevState.email,
          message: `Filename: ${doc.docFileName}, Category: ${doc.docCategory}, Sub Category: ${doc.docSubCategory}, type: ${doc.docType}`,
          docIds: [doc.docAWSFileLocation || ""]
        }
      }))
    }
  }

  onSearchText = e => {
    const { documentsList } = this.state
    this.setState({
      [e.target.name]: e.target.value
    },() => {
      if (documentsList.length) {
        this.onSearch()
      }
    })
  }

  onRemove = index => {
    const { documents } = this.state
    documents.splice(index, 1)
    this.setState({
      documents
    })
  }

  onSearch = () => {
    const { searchText, documentsList } = this.state
    if (searchText) {
      const searchList = documentsList.filter(obj =>
        [
          "docType",
          "docSubCategory",
          "docCategory",
          "docFileName",
          "docNote",
          "LastUpdatedDate",
          "lastUpdatedDate",
          "createdUserName"
        ].some(key => {
          if (key === "LastUpdatedDate" || key === "lastUpdatedDate") {
            return moment(obj[key])
              .format("MM.DD.YYYY hh:mm A")
              .toLowerCase()
              .includes(searchText.toLowerCase())
          }
          return (
            obj[key] &&
            obj[key].toLowerCase().includes(searchText.toLowerCase())
          )
        })
      )
      this.setState({
        searchList
      })
    }
  }

  handleDocDetails = res => {
    this.setState({
      showModal: !this.state.showModal,
      doc: res || {}
    })
  }

  onModalSave = () => {
    const { transaction, nav1, nav2 } = this.props
    const emailPayload = {
      tranId: (transaction && transaction._id) || "",
      type: nav1 === "compliance" ? nav2 : nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        subject: "Document Action",
        ...this.state.email
      }
    }
    this.setState(
      pre => ({
        modalState: !pre.modalState
      }),
      async () => {
        if (
          ["deals", "rfp", "loan", "marfp", "derivative", "others"].indexOf(
            this.props.nav1
          ) !== -1
        ) {
          await sendEmailAlert(emailPayload)
        } else if (nav1 === "compliance") {
          await sendComplianceEmail(emailPayload)
        } else if(nav1 === "clients-propects" || nav1 === "thirdparties"){
          emailPayload.id = nav2
          await sendCRMEmail(emailPayload)
        }
      }
    )
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  onStatusChange = (e, doc) => {
    const { documentsList } = this.state
    const { name, value } = e.target
    const { onStatusChange } = this.props
    if (onStatusChange) {
      this.props.addAuditLog({
        log: `document ${doc.docFileName} status change to ${e.target.value}`,
        key: "document"
      })
      onStatusChange(e, doc, res => {
        if (res && res.status) {
          documentsList.forEach(document => {
            if (document._id === doc._id) {
              document.docStatus = value
            }
          })
          this.setState({
            documentsList
          })
        }
      })
    }
  }

  deleteDoc = (versionId, documentId) => {
    console.log("versionId : ", versionId)
    console.log("documentId : ", documentId)
    const { onDeleteDoc, contextType } = this.props
    if (onDeleteDoc) {
      this.props.addAuditLog({
        log: `one document deleted from ${contextType}`,
        key: "document"
      })
      onDeleteDoc(documentId, res => {
        if (res && res.status) {
          res.documentsList.forEach(doc => {
            doc.timeStamp =
              doc.LastUpdatedDate || doc.lastUpdatedDate
                ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix()
                : 0
          })
          this.setState({
            documentsList: res.documentsList || []
          })
        }
      })
    }
  }

  actionButtons = (key, isDisabled, RatingSection) => {
    if (isDisabled) return

    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key, RatingSection)}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-light is-small"
            onClick={() => this.onReset(key)}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }
  getBidBucket = (filename, docId, index) => {
    const {user} = this.props
    const documents = cloneDeep(this.state.documents)
    documents[index].docAWSFileLocation = docId
    documents[index].docFileName = filename
    this.props.addAuditLog({
      userName: `${user.userFirstName}  ${user.userLastName}`,
      log: `Documents upload ${filename}`,
      date: new Date(),
      key: "documents"
    })

    this.setState({
      documents
    },() => {
      this.onChangeError("docAWSFileLocation", "documents", index)
    })
  }

  downloadFile = () => {
    this.downloadAnchor.click()
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, res => {
      this.setState({
        ...res
      },callback.bind(this, bucketName, fileName))
    })
  }

  onMultipleFileUpload = uploadedFiles => {
    console.log(uploadedFiles)
    const { documents } = this.state
    const { staticField, user } = this.props
    const docs =
      documents.filter(doc => doc.docAWSFileLocation && doc.docFileName) || []
    if (uploadedFiles && Array.isArray(uploadedFiles) && uploadedFiles.length) {
      uploadedFiles.forEach(file => {
        docs.push({
          docCategory: "",
          docSubCategory: "",
          docType: "",
          docAWSFileLocation: file.fileId,
          docAction: "",
          docFileName: file.fileNeme,
          markedPublic: {
            publicFlag: false,
            publicDate: new Date().toString()
          },
          sentToEmma: {
            emmaSendFlag: false,
            emmaSendDate: new Date().toString()
          },
          docNote: "",
          ...staticField,
          createdBy: (user && user.userId) || "",
          createdUserName:
            (user && `${user.userFirstName} ${user.userLastName}`) || ""
        })
      })
    }
    this.setState({
      uploadedFiles,
      chooseMultiple: false,
      documents: uploadedFiles && Array.isArray(uploadedFiles) && uploadedFiles.length ? docs : documents
    })
  }

  saveFileInState = (uploadedFiles, single, index) => {
    console.log(uploadedFiles)
    const { documents } = this.state
    const { staticField, user } = this.props
    const docs = documents|| []
    if(single){
      const invalidFile = checkInvalidFiles(uploadedFiles, "single")
      if(invalidFile){
        this.setState({
          fileTypeError: invalidFile || ""
        })
        return
      }
      documents[index].docFileName = uploadedFiles.name
      documents[index].file = uploadedFiles
    }else if (uploadedFiles && Array.isArray(uploadedFiles) && uploadedFiles.length) {
      if(docs && docs[0] && docs[0].docFileName === ""){
        docs.splice(0, 1)
      }
      const invalidFile = checkInvalidFiles(uploadedFiles, "multi")
      if(invalidFile){
        this.setState({
          fileTypeError: invalidFile || ""
        })
        return
      }
      uploadedFiles.forEach(file => {
        docs.push({
          file,
          docCategory: "",
          docSubCategory: "",
          docType: "",
          docAWSFileLocation: "",
          docAction: "",
          docFileName: file.name,
          markedPublic: {
            publicFlag: false,
            publicDate: new Date().toString()
          },
          sentToEmma: {
            emmaSendFlag: false,
            emmaSendDate: new Date().toString()
          },
          docNote: "",
          ...staticField,
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || ""
        })
      })
    }

    this.setState({
      uploadedFiles,
      chooseMultiple: false,
      fileTypeError: "",
      documents: uploadedFiles && Array.isArray(uploadedFiles) && uploadedFiles.length ? docs : documents
    })
  }

  toggleMultipleFile = () => {
    this.setState(prevState => ({
      chooseMultiple: !prevState.chooseMultiple
    }))
  }

  onUploadSuccess = (name, id) => {
    const {documentsList} = this.state
    const document = documentsList && documentsList.find(item => item.docAWSFileLocation === id)
    if (document) {
      this.onUpdate(document)
    }
  }

  onSort = type => {
    const { sortBy, documentsList } = this.state
    const key =
      type === "Category"
        ? "docCategory"
        : type === "Sub-category"
          ? "docSubCategory"
          : type === "Uploaded by"
            ? "createdUserName"
            : type === "Filename"
              ? "docFileName"
              : type === "Type"
                ? "docType"
                : type === "LUD" || type === "Last Updated On"
                  ? "timeStamp"
                  : type === "Notes"
                    ? "docNote"
                    : type === "Status"
                      ? "docStatus"
                      : ""
    let sort = ""

    if (key) {
      if (sortBy.type === type) {
        if (type && sortBy.sort === "desc") {
          if (key === "timeStamp") {
            documentsList.sort((a, b) => a[key] - b[key])
          } else {
            documentsList.sort((a, b) => {
              const nameA = (a[key] && a[key].toUpperCase()) || ""
              const nameB = (b[key] && b[key].toUpperCase()) || ""
              if (nameA < nameB) {
                return -1
              }
              if (nameA > nameB) {
                return 1
              }
              return 0
            })
          }
          sort = "asc"
        } else {
          if (key === "timeStamp") {
            documentsList.sort((a, b) => b[key] - a[key])
          } else {
            documentsList.sort((a, b) => {
              const nameA = (a[key] && a[key].toUpperCase()) || ""
              const nameB = (b[key] && b[key].toUpperCase()) || ""
              if (nameA > nameB) {
                return -1
              }
              if (nameA < nameB) {
                return 1
              }
              return 0
            })
          }
          sort = "desc"
        }
      } else {
        if (key === "timeStamp") {
          documentsList.sort((a, b) => a[key] - b[key])
        } else {
          documentsList.sort((a, b) => {
            const nameA = (a[key] && a[key].toUpperCase()) || ""
            const nameB = (b[key] && b[key].toUpperCase()) || ""
            if (nameA < nameB) {
              return -1
            }
            if (nameA > nameB) {
              return 1
            }
            return 0
          })
        }
        sort = "asc"
      }
      this.setState({
        sortBy: {
          type,
          sort
        },
        documentsList
      })
    }
  }

  onUpdate = async document => {
    const { onStatusChange, user, contextType } = this.props
    const { docs } = this.state
    const doc = document.docAWSFileLocation ? document : docs
    const meta = {
      docCategory: document.docCategory || "",
      docSubCategory: document.docSubCategory || "",
      type: document.docType || "",
    }
    if (onStatusChange) {
      this.props.addAuditLog({
        log: "document category or sub category or type change",
        key: "document"
      })
      const {
        docCategory,
        docSubCategory,
        docAWSFileLocation,
        docFileName,
      } = doc
      // const res = await getDocDetails(docAWSFileLocation)
      // const [error, document] = res
      // console.log("res ============== : ", document)
      if (docAWSFileLocation) {
        const userName = (user && `${user.userFirstName || ""} ${user.userLastName || ""}`) || ""
        const tag = {
          docCategory: (docCategory && docCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          docSubCategory: (docSubCategory && docSubCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          docContext: (contextType && contextType.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          docName: (docFileName && docFileName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          uploadUserName: (userName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          uploadUserEntityName: (user && user.firmName && user.firmName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          uploadDate: moment(new Date()).format("MM-DD-YYYY")
        }
        console.log("The tags to be submitted are", tag)
        await updateS3DocTags(docAWSFileLocation, { ...tag }, meta)
      }
      onStatusChange("", doc, res => {
        if (res && res.status) {
          res.documentsList.forEach(doc => {
            doc.timeStamp =
              doc.LastUpdatedDate || doc.lastUpdatedDate
                ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix()
                : 0
          })
          this.setState({
            errors: {},
            documentsList: res.documentsList,
            isEditable: ""
          },() => {
            this.onSearch()
          })
        } else {
          this.setState({
            errors: {},
            isEditable: ""
          })
        }
      })
    }
  }

  onEdit = (key, index, item) => {
    const { isEditable } = this.state
    const { user } = this.props
    const { docCategory, docSubCategory, docType } = item || {}
    this.props.addAuditLog({
      userName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
      log: "Documents one row edited",
      date: new Date(),
      key
    })
    if (isEditable || isEditable === 0) {
      swal("Warning", "First save the current document", "warning")
      return
    }
    this.setState({
      isEditable: index,
      tempMeta: {
        docCategory,
        docSubCategory,
        docType
      },
    })
  }

  onCancelDoc = (index) => {
    const {tempMeta} = this.state
    const { documents } = this.props
    const { docCategory, docSubCategory, docType } = tempMeta || {}
    documents[index].docCategory = docCategory || ""
    documents[index].docSubCategory = docSubCategory || ""
    documents[index].docType = docType || ""
    this.setState({
      isEditable: "",
      documentsList: documents,
      tempMeta: {}
    });
  };

  onChangeUpdate = (key, doc, value, index) => {
    const { documentsList } = this.state
    if (key === "docCategory") {
      doc.docSubCategory = ""
    }
    doc = {
      ...doc,
      [key]: (value && value.value) || ""
    }
    documentsList.splice(index, 1, doc)
    this.setState({
      documentsList
    })
  }

  onChangeError = (name, key, index) => {
    this.setState(prevState => ({
      errorMessages: {
        ...prevState.errorMessages,
        [key]: {
          ...(prevState.errorMessages && prevState.errorMessages[key]),
          [index]: {
            ...(prevState.errorMessages &&
              prevState.errorMessages[key] &&
              prevState.errorMessages[key][index.toString()]),
            [name]: ""
          }
        }
      }
    }))
  }

  highlightSearch = (string, searchQuery) => {
    const reg = new RegExp(
      searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
      "i"
    )
    return string.replace(reg, str => `<mark>${str}</mark>`)
  }

  setStateParentLevel = (state) => {
    this.setState({
      ...state
    })
  }

  clickUpload = (index) => {
    document.getElementById(index).click()
  }

  onClickUpload = (event) => {
    event.target.value = null
  }

  onChangeUpload = (event) => {
    this.saveFileInState(event.target.files[0], true, event.target.id)
  }

  render() {
    const {
      searchText,
      modalState,
      errors,
      fileTypeError,
      bucketName,
      chooseMultiple,
      columns,
      contextId,
      email,
      isSave,
      loadDocuments,
      searchList,
      documents,
      errorMessages,
      doc,
      dropDown,
      newDropDown,
      isSaveDisabled,
      loading,
      activeItem
    } = this.state
    const documentsList = searchText ? searchList : this.state.documentsList
    const {
      isNotTransaction,
      nav1,
      title,
      tranId,
      user,
      contextType,
      isDisabled,
      loggedInContactId,
      staticField,
      participants,
      tranAction,
      tableStyle
    } = this.props
    const isCompliance = nav1 === "compliance" || nav1 === "bus-development"
    const cols = isCompliance ? complianceCols : columns
    // const loading = <Loader />
    if(loading){
      return <Loader/>
    }
    return (
      <div className="bank-loan-documents">
        {/*{ this.state.loading ? <Loader/> : null}*/}
        {documents.filter(o => o.docFileName).length ? <Prompt
          when={true}
          message='You have unsaved documents, are you sure you want to leave?'
        /> : null}
        <SendEmailModal
          nav1={nav1}
          isNotTransaction={isNotTransaction}
          modalState={modalState}
          email={email}
          participants={participants}
          documentFlag
          onModalChange={this.onModalChange}
          onSave={this.onModalSave}
        />
        <Accordion
          multiple
          boxHidden
          isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [0]}
          render={({ activeAccordions, onAccordion }) => (
            <div style={{ paddingLeft: "0.25em", paddingRight: "0.25em" }}>

              <RatingSection
                onAccordion={() => onAccordion(0)}
                title={title || "Upload Documents"}
                actionButtons={this.actionButtons("documents", isDisabled, 0)}
              >
                {activeAccordions.includes(0) && (
                  <div style={{paddingLeft: "0.1em"}}>
                    {!isDisabled ?
                      <div>
                        <div className="column">
                          <small className="has-text-danger">{fileTypeError || ""}</small>
                          <div className="field is-grouped">
                            <div className="control">
                              <button
                                className="button is-link is-small"
                                onClick={this.toggleMultipleFile}
                              >
                                Choose Multiple File
                              </button>
                            </div>
                          </div>
                        </div>
                        <DragAndDropFileInState
                          bucketName={bucketName}
                          contextId={tranId || contextId}
                          {...this.props}
                          loadDocuments={loadDocuments}
                          isSave={isSave}
                          saveFileInState={this.saveFileInState}
                          setStateParentLevel={this.setStateParentLevel}
                          contextType={contextType || ""}
                          tenantId={user.entityId}
                          onSave={this.onSave}
                          docMeta={{
                            category: "",
                            subCategory: "",
                            type: ""
                          }}
                          chooseMultiple={chooseMultiple}
                        />
                        {!chooseMultiple ? (
                          <div className="tbl-scroll">
                            <table
                              id="my-table"
                              className="table is-bordered is-striped is-hoverable is-fullwidth"
                              style={tableStyle}
                            >
                              <TableHeader cols={cols[0]} />
                              {documents.map((doc, index) => {
                                const errors =
                                  errorMessages.documents &&
                                  errorMessages.documents[index.toString()]
                                return (
                                  <DocumentsView
                                    that={this}
                                    tranId={tranId}
                                    user={user}
                                    contextType={contextType}
                                    staticField={staticField}
                                    contextId={contextId}
                                    key={index.toString()}
                                    index={index}
                                    doc={doc}
                                    dropDown={dropDown}
                                    loggedInContactId={loggedInContactId}
                                    bucketName={bucketName}
                                    getBidBucket={this.getBidBucket}
                                    errors={errors}
                                    chooseMultiple={chooseMultiple}
                                    newDropDown={newDropDown}
                                    onChangeItem={(changedItem, change, name) => this.onChangeItem(changedItem, index, "documents", change, name)}
                                    onFileAction={this.onFileAction}
                                    onRemove={this.onRemove}
                                    deleteDoc={this.deleteDoc}
                                    isCompliance={isCompliance}
                                    onChangeError={this.onChangeError}
                                    clickUpload={this.clickUpload}
                                    onClickUpload={this.onClickUpload}
                                    onChangeUpload={this.onChangeUpload}
                                  />
                                )
                              })}
                            </table>
                          </div>
                        ): null }
                        {!chooseMultiple ? (
                          <div className="field is-grouped-center">
                            <div className="control">
                              <button
                                className="button is-link"
                                onClick={this.updateSaveFlag}
                                disabled={isSaveDisabled || false}
                              >
                                Save
                              </button>
                            </div>
                          </div>
                        ) : null}
                      </div>: null
                    }

                    <br />
                    <div className="box">
                      <div className="columns">
                        <div className="column">
                          <p className="control has-icons-left">
                            <input
                              className="input is-small is-link"
                              type="text"
                              placeholder="search"
                              name="searchText"
                              onChange={this.onSearchText}
                            />
                            <span className="icon is-left has-background-dark">
                              <i className="fas fa-search" />
                            </span>
                          </p>
                        </div>
                      </div>
                      <Accordion
                        multiple
                        boxHidden
                        activeItem={[0]}
                        render={() => (
                          <div>
                            <DocumentList
                              headers={cols[1]}
                              isCompliance={isCompliance}
                              isTransaction={false}
                              isDisabled={isDisabled}
                              isSaveDisabled={isSaveDisabled}
                              bucketName={this.state.bucketName}
                              isEditable={this.state.isEditable}
                              search={searchText}
                              dropDown={dropDown}
                              nav1={nav1}
                              user={user}
                              tranAction={tranAction}
                              contextId={tranId || contextId}
                              contextType={contextType || "DEALS"}
                              tenantId={user.entityId}
                              documentsList={documentsList}
                              errors={errors}
                              newDropDown={newDropDown}
                              onDeleteAll={this.deleteDoc}
                              onChangeUpdate={this.onChangeUpdate}
                              highlightSearch={this.highlightSearch}
                              onStatusChange={this.onStatusChange}
                              onFileAction={this.onFileAction}
                              onUpdate={this.onUpdate}
                              onEdit={this.onEdit}
                              onCancel={this.onCancelDoc}
                              onSettingModal={this.onSettingModal}
                              onUploadSuccess={this.onUploadSuccess}
                            />
                          </div>
                        )}
                      />
                    </div>
                  </div>
                )}
              </RatingSection>

            </div>
          )}
        />
        <br />

        <a
          className="hidden-download-anchor"
          style={{ display: "none" }}
          ref={el => {
            this.downloadAnchor = el
          }}
          href={this.state.signedS3Url}
          target="_blank"
        />
        {this.state.showModal ? (
          <DocModalDetails
            showModal={this.state.showModal}
            closeDocDetails={this.handleDocDetails}
            documentId={doc.documentId}
            onDeleteAll={this.deleteDoc}
            docMetaToShow={["category", "subCategory"]}
            versionMetaToShow={["uploadedBy"]}
            docId={doc._id}
          />
        ) : null}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(DocumentPage)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
