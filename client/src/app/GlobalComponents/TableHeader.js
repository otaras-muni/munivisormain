import React from "react"

const TableHeader = ({ cols, className, thStyle }) => (
  
  <thead className="rt-th  rt-resizable-header -cursor-pointer">
    <tr >
      {cols.map(col => (
        <th key={col.name} className={className} style={thStyle || col.style || null}>
          <p><span dangerouslySetInnerHTML={{ __html: col.name }} />
          {col.required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>:'' }
          </p>
        </th>
      ))}
    </tr>
  </thead>
 
)

export default TableHeader
