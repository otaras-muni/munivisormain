import React from "react"
import {DropDownSelect} from "./TextViewBox"
import DropDownListClear from "./DropDownListClear"


const DocumentsView = ({
  that,
  doc = {},
  staticField,
  index,
  dropDown,
  onChangeItem,
  isCompliance,
  errors = {},
  onRemove,
  chooseMultiple,
  folder,
  onFolderChange,
  onChangeError,
  clickUpload,
  onClickUpload,
  duplicateList,
  contextType,
  onChangeUpload,
  newDropDown
}) => {
  const onUserChange = async e => {
    let newItem = {}

    if (e.target.name === "docAction") {
      const sentToEmma = {
        emmaSendFlag: e.target.value === "Send to EMMA",
        emmaSendDate: new Date().toString()
      }
      const markedPublic = {
        publicFlag: e.target.value === "Mark Public",
        publicDate: new Date().toString()
      }
      newItem = {
        ...doc,
        [e.target.name]: e.target.value,
        markedPublic,
        sentToEmma
      }
    } else {
      newItem = {
        ...doc,
        [e.target.name]: e.target.value
      }
    }
    onChangeItem(
      newItem,
      e.target.title === "Note"
        ? null
        : `user ${e.target.title || "empty"} change to ${e.target.value}`,
      e.target.name
    )
    if (e.target.name && e.target.value) {
      onChangeError(e.target.name, "documents", index)
    }
  }

  const onChangeSelect = (name, value) => {
    const names =
      name === "docCategory" ? "Category" : name === "docSubCategory"
        ? "Sub-category" : name === "docType"
          ? "Type" : name
    if (name === "docCategory") {
      doc.docSubCategory = ""
    }
    onChangeItem(
      {
        ...doc,
        [name]: value !== "" ? value : ""
      },
      `user ${names} set to ${value !== "" ? value : ""}`,
      name
    )
    if (name && value) {
      onChangeError(name, "documents", index)
    }
  }

  /* const inputProps = {
    placeholder: "Type",
    value: doc.docType,
    onChange: (e, { newValue }) => onChangeSelect("docType", newValue),
    name: "docType",
  } */

  return (
    <tbody className="is-marginless is-paddingless doc-view">
      <tr>
        {folder ?
          <td>
            <div className="complain-details">
              <DropDownSelect
                name="folderName"
                data={contextType === "SHAREDOCS" ? duplicateList : dropDown.folderList || []}
                value={doc.folderName || ""}
                onChange={value => onFolderChange("folderName", value, "", index)}
              />
            </div>
          </td> : null
        }
        {!folder ?
          <td>
            {/* { staticField && staticField.docCategory ?   <small>{doc.docCategory || ""}</small>
            : <SelectLabelInput list={dropDown.category} error= {errors.docCategory || ""} name="docCategory" value={doc.docCategory} onChange={onUserChange} />
          } */}
            {staticField && staticField.docCategory ? (
              <small>{doc.docCategory || ""}</small>
            ) : (
              <div className="complain-details">
                {/* <DropDownSelect
                  name="category"
                  data={dropDown.category || []}
                  value={doc.docCategory || ""}
                  onChange={value => onChangeSelect("docCategory", value)}
                /> */}
                <DropDownListClear
                  filter
                  textField="label"
                  valueField={a => a}
                  data={newDropDown.category || []}
                  disabled={newDropDown.disabledCategory || []}
                  value={doc.docCategory || ""}
                  isHideButton={doc.docCategory}
                  onChange={value => onChangeSelect("docCategory", value)}
                  onClear={() => {onChangeSelect("docCategory", "")}}
                />
                {errors && errors.docCategory && (
                  <small className="text-error">{errors.docCategory}</small>
                )}
              </div>
            )}
          </td> : null
        }
        {!folder ?
          <td>
            {/* { staticField && staticField.docSubCategory ? <small>{doc.docSubCategory || ""}</small>
            : <SelectLabelInput list={dropDown.subCategory} error= {errors.docSubCategory || ""} name="docSubCategory" value={doc.docSubCategory} onChange={onUserChange} />
          } */}
            {staticField && staticField.docSubCategory ? (
              <small>{doc.docSubCategory || ""}</small>
            ) : (
              <div className="complain-details">
                {/* <DropDownSelect
                  name="subcategory"
                  data={(doc.docCategory && dropDown.multiSubCategory[doc.docCategory]) || []}
                  value={doc.docSubCategory || ""}
                  onChange={value => onChangeSelect("docSubCategory", value)}
                  defaultValue={doc.docSubCategory || ""}
                /> */}
                <DropDownListClear
                  filter
                  textField="label"
                  valueField={a => a}
                  data={(doc.docCategory && newDropDown.subCategory[doc.docCategory]) || []}
                  disabled={(doc.docCategory && newDropDown.subCategoryDisabled[doc.docCategory]) || []}
                  value={doc.docSubCategory || ""}
                  isHideButton={doc.docSubCategory}
                  onChange={value => onChangeSelect("docSubCategory", value)}
                  onClear={() => {onChangeSelect("docSubCategory", "")}}
                />
                {errors && errors.docSubCategory && (
                  <small className="text-error">{errors.docSubCategory}</small>
                )}
              </div>
            )}
          </td> : null
        }
        {!folder ?
          <td>
            {/* { staticField && staticField.docType ?  <small>{doc.docType || ""}</small>
            : <SelectLabelInput list={dropDown.docType} error= {errors.docType || ""} name="docType" value={doc.docType} onChange={onUserChange} />
          } */}
            {staticField && staticField.docType ? (
              <small>{doc.docType || ""}</small>
            ) : (
              <div className="complain-details">
                {/* <DropDownSelect
                  name="doctype"
                  data={dropDown.docType || []}
                  value={doc.docType || ""}
                  textField="name"
                  valueField="id"
                  key="name"
                  onChange={value => onChangeSelect("docType", value)}
                /> */}
                <DropDownListClear
                  filter
                  textField="label"
                  valueField={a => a}
                  data={newDropDown.docType || []}
                  disabled={newDropDown.disabledDocType || []}
                  value={doc.docType || ""}
                  isHideButton={doc.docType}
                  onChange={value => onChangeSelect("docType", value)}
                  onClear={() => {onChangeSelect("docType", "")}}
                />
                {errors.docType ? (
                  <p className="text-error">{errors.docType}</p>
                ) : null}
              </div>
            )}
          </td> : null
        }
        <td>
          <div style={{display: "table-cell"}}>
            <input
              type="file"
              style={{ display: "none" }}
              onClick={onClickUpload}
              id={index}
              onChange={(event) => onChangeUpload(event, index)}
              disabled={chooseMultiple}
            />
            <div 
              className={`file is-small ${chooseMultiple ? "disabled" : ""}`}
              style={{ cursor: chooseMultiple ? "not-allowed" : "pointer" }}
              onClick={() => clickUpload(index)}
            >
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-upload" />
                </span>
                <span className="file-label">
                 Choose a file…
                </span>
              </span>
            </div>
          </div>
          {errors.docFileName && <p className="text-error">{errors.docFileName}</p>}
        </td>
        <td>
          <div className="control">
            <div className="multiExpTblVal">
              {doc.docFileName}
            </div>
          </div>
        </td>
        {/* {!isCompliance && !folder ? (
          <td>
            <TextLabelInput
              title="Note"
              name="docNote"
              error={errors.docNote || ""}
              placeholder=""
              value={doc.docNote}
              onChange={onUserChange}
            />
          </td>
        ) : null} */}
        <td>
          <div className="field is-grouped">
            <div className="control">
            <button className="borderless btn-delete" onClick={() => onRemove(index)}>
                <span className="has-text-link">
                  <i className="far fa-trash-alt" title="Delete"/>
                </span>
              </button>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  )
}

export default DocumentsView
