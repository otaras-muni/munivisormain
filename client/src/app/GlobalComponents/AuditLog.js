import React, { Component } from "react"
import { connect } from "react-redux"
import ReactTable from "react-table"

import Loader from "./Loader"
import { getAuditLog } from "../StateManagement/actions"

class AuditLog extends Component {
  constructor(props) {
    super(props)
    this.state = { searchString: "", stringToMatch: "",
      pageSizeOptions: [5, 10, 20, 25, 50], pageSize: 5, pageSize1: 5 }
    this.changeSearchString = this.changeSearchString.bind(this)
    this.handleKeyPressinSearch = this.handleKeyPressinSearch.bind(this)
    this.onPageSizeChange = this.onPageSizeChange.bind(this)
  }

  componentDidMount() {
    const { getAuditLog, type } = this.props
    getAuditLog(type)
  }

  onPageSizeChange = (item) => {
    this.setState({
      pageSize: item
    })
  }

  changeSearchString(e) {
    const searchString = e.target.value
    if (!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString })
    }
  }

  handleKeyPressinSearch(e) {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  renderTableBody() {
    const { auditLog } = this.props
    const { stringToMatch } = this.state
    const match = stringToMatch.toLocaleLowerCase()
    const searchFields = ["userName", "log"]
    if (auditLog) {
      const data = stringToMatch ?
        auditLog.filter(e => {
          let found = false
          searchFields.some(d => {
            if (e[d].toLocaleLowerCase().includes(match) || e.ip.includes(match) ||
              new Date(e.date).toLocaleString().includes(match)) {
              found = true
              return true
            }
          })
          return found
        })
        : auditLog
      return data.map((e, i) => (
        <tr key={i}>
          <td>{e.userName}</td>
          <td>{e.log}</td>
          <td>{e.ip}</td>
          <td>{new Date(e.date).toLocaleString()}</td>
        </tr>
      ))
    }
    return <Loader />
  }

  renderTable() {
    const { auditLog } = this.props
    const { stringToMatch, pageSize, pageSizeOptions } = this.state
    const match = stringToMatch.toLocaleLowerCase()
    const searchFields = ["userName", "log"]
    if (auditLog) {
      const data = stringToMatch ?
        auditLog.filter(e => {
          let found = false
          searchFields.some(d => {
            if ((e[d] && e[d].toLocaleLowerCase().includes(match)) ||
              (e.ip && e.ip.includes(match)) ||
              (e.date && new Date(e.date).toLocaleString().includes(match))) {
              found = true
              return true
            }
          })
          return found
        })
        : auditLog
      return (
        <ReactTable
          columns={[
            {
              Header: () => <span title='User Name'>User</span>, // Custom header components!
              // Header: <span>Control Name</span>,
              id: "userName",
              className: "multiExpTblVal",
              accessor: e => e.userName,
              Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
                <small>{e.value}</small></div>
            },
            {
              Header: () => <span title='Action'>Action</span>, // Custom header components!
              // Header: <span>Control Name</span>,
              id: "log",
              className: "multiExpTblVal",
              accessor: e => e.log,
              Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
                <small>{e.value}</small></div>
            },
            {
              Header: () => <span title='IP Address'>IP</span>, // Custom header components!
              // Header: <span>Control Name</span>,
              id: "ip",
              className: "multiExpTblVal",
              accessor: e => e.ip,
              Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
                <small>{e.value}</small></div>
            },
            {
              Header: () => <span title='Date & Time'>Time</span>, // Custom header components!
              // Header: <span>Control Name</span>,
              id: "date",
              className: "multiExpTblVal",
              accessor: e => e.date,
              Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
                <small>{new Date(e.value).toLocaleString()}</small></div>
            },
          ]}
          data={data}
          defaultSorted={[
            {
              id: "date",
              desc: true
            }
          ]}
          defaultPageSize={pageSize}
          onPageSizeChange={this.onPageSizeChange}
          pageSizeOptions={pageSizeOptions}
          className=" overflow-auto -striped -highlight is-bordered "
          pageSize={pageSize}
          minRows={1}
        />
      )
    }
    return <Loader />
  }

  render() {
    return (
      <div className="top-bottom-margin">
        <div className="box left-right-margin">
          <div className="column">
            <p className="control has-icons-left">
              <input className="input is-fullwidth is-link is-small"
                type="text"
                placeholder="search"
                value={this.state.searchString}
                onChange={this.changeSearchString}
                onKeyPress={this.handleKeyPressinSearch}
              />
              <span className="icon is-small is-left has-background-dark has-text-white">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <div className="column overflow-auto">
            {this.renderTable()}
            {/* <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                <th>User</th>
                <th>Action</th>
                <th>IP</th>
                <th>Time</th>
              </thead>
              <tbody>
                {this.renderTableBody()}
              </tbody>
            </table> */}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ auditLog }) => ({ auditLog })


export default connect(mapStateToProps, { getAuditLog })(AuditLog)
