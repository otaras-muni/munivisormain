import Joi from "joi-browser"
import dateFormat from "dateformat"

const fees = Joi.object().keys({
  sofDesc: Joi.string().label("Description").required(),
  sofAmt: Joi.number().label("Amount").min(0).required(),
  sofCharge: Joi.number().label("Charge").min(0).required(),
  /* sofDealType: Joi.string().required(),
  sofMin: Joi.number().min(0.01).required(),
  sofMax: Joi.number().min(Joi.ref("sofMin")).required(), */
  _id: Joi.string().required().optional(),
})

const sof = Joi.object().keys({
  securityType: Joi.string().label("Security Type").required(),
  feesType: Joi.string().label("Fees Type").required(),
  sofDealType: Joi.string().label("Deal type").required() ,
  sofMin: Joi.number().label("Minimum").min(0.01).required(),
  sofMax: Joi.number().label("Maximum").min(Joi.ref("sofMin")).required(),
  fees: Joi.array().items(fees).required(),
  _id: Joi.string().required().optional(),
})

const contractRef = Joi.object().keys({
  contractId: Joi.string().required(),
  contractName: Joi.string().label("Contract Name").required(),
  contractType: Joi.string().label("Contract Type").required(),
  startDate: Joi.date().label("Start Date").example(new Date("2016-01-01")).required(),
  endDate: Joi.date().label("End Date").example(new Date("2016-01-01")).min(Joi.ref("startDate")).required(),
  _id: Joi.string().required().optional(),
})

const nonTranFees = Joi.object().keys({
  role: Joi.string().label("Role").required(),
  stdHourlyRate: Joi.number().label("Standard Hourly Rate").min(0).required(),
  quoatedRate: Joi.number().label("Quoted Hourly Rate").min(0).required(),
  _id: Joi.string().required().optional(),
})

const retAndEsc = (minDate) => Joi.object().keys({
  type: Joi.string().label("Type").required(),
  hoursCovered: Joi.number().label("Amount").min(0).required(),
  effStartDate: Joi.date().label("Effective Start Date").example(new Date("2016-01-01")).required(),
  // Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  effEndDate: Joi.date().label("Effective End Date").example(new Date("2016-01-01")).min(Joi.ref("effStartDate")).required(),
  _id: Joi.string().required().optional(),
})

const documents = Joi.object().keys({
  docCategory: Joi.string().allow("", null).label("Category"),
  docType: Joi.string().allow("", null).label("Type"),
  docSubCategory: Joi.string().allow("", null).optional(),
  docAWSFileLocation: Joi.string().label("File Name").required(),
  docFileName: Joi.string().allow("").label("File Name").optional(),
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  uploadedDate: Joi.date().example(new Date("2016-01-01")).optional(),
  _id: Joi.string().allow("").optional(),
}).or("docCategory", "docType")

const ScheduleTranSchema = (minDate) => Joi.object().keys({
  sof: Joi.array().items(sof).optional(),
  contractRef,
  documents: Joi.array().items(documents).min(1).optional(),
  nonTranFees: Joi.array().items(nonTranFees).optional(),
  retAndEsc: Joi.array().items(retAndEsc(minDate)).optional(),
  notes: Joi.string().label("Notes").allow("").optional(),
  digitize: Joi.boolean().optional(),
  _id: Joi.string().required().optional(),
})

export const ScheduleTranValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, ScheduleTranSchema(minDate), { abortEarly: false, stripUnknown:false })

