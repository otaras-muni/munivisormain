import React from "react"
import swal from "sweetalert"
import {withRouter} from "react-router-dom"
import cloneDeep from "lodash.clonedeep"
import {connect} from "react-redux"
import RatingSection from "./RatingSection"
import Accordion from "./Accordion"
import Participants from "./Participants"
import CONST from "../../globalutilities/consts"
import Loader from "./Loader"
import withAuditLogs from "./withAuditLogs"
import {ParticipantsValidate} from "./ParticipantsValidate"
import {fetchSupplierContacts} from "../StateManagement/actions/TransactionDistribute"
import {DropDownInput} from "./TextViewBox"

const mergeArray = (currentPart, newPart) => {
  const newObj = newPart.find(part => part.isNew)
  if(newObj){
    currentPart.push(newObj)
  }
  return currentPart
}

class ParticipantsView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: "",
      participants: [CONST.Participants],
      tempParticipants: [CONST.Participants],
      isEditable: "",
      searchText: "",
      loading: true,
      filterUser: [],
      allFirms: [],
      participantsType: [],
      userList: [],
      errorMessages: {},
      dropDown: {
        sortBy: ["Date", "Firm Name", "Type"],
        ascDsc: ["Ascending", "Descending"]
      },
      sortBy: "",
      ascDsc: "",
      searchList: [],
      isSaveDisabled: false,
      confirmAlert: CONST.confirmAlert,
      isRefresh: false,
    }
  }

  componentWillMount() {
    const {id, firmId, issuerId, participants, canEditTran} = this.props
    if(id) {
      this.setState({
        participants: (participants && participants.length) ? cloneDeep(participants) : canEditTran ? [CONST.Participants] : [],
        tempParticipants: (participants && participants.length) ? cloneDeep(participants) : canEditTran ? [CONST.Participants] : [],
        isEditable: (participants && participants.length)  ? "" : 0,
        loading: false,
      },() => {
        if(firmId) {
          this.getFirmDetails(firmId)
        }else {
          this.setState({loading: false})
        }
      })
    }
  }

  onItemChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    })
  }

  onReset = (key) => {
    const {tempParticipants} = this.state
    const auditLogs = this.props.auditLogs.filter(x=> x.key !== key)
    this.props.updateAuditLog(auditLogs)
    this.setState({
      loading: false,
      participants: cloneDeep(tempParticipants),
      isEditable: "",
    })
  }

  onAdd = (key) => {
    const { userName, isEditable} = this.state
    const insertRow = cloneDeep(this.state[key])
    insertRow.unshift(CONST.Participants)
    this.props.addAuditLog({userName, log: `${key} Add new Item`, date: new Date(), key})

    if(isEditable || isEditable === 0) { return swal("Warning", `First save the current ${key}`, "warning")  }

    this.setState({
      [key]: insertRow,
      // isEditable: insertRow.length ? 0 : 0
      isEditable: 0
    })
  }

  onBlur = (category, change) => {
    const { userName } = this.state
    this.props.addAuditLog({userName, log: `${change}`, date: new Date(), key: category})
  }

  actionButtons = (key, isDisabled, canEditTran) => {
    const {participants} = this.state
    const isAddNew = (participants && participants.find(part => part.isNew)) || false
    if(!canEditTran) return
    return ( //eslint-disable-line
      <div className="field is-grouped">
        <div className="control is-link"  onClick={() => this.getFirmDetails(this.props.firmId, this.setState({isRefresh: true}))}>
          <a title="Title refresh" className={this.state.isRefresh ? "fa fa-refresh fa-spin isDisabled" : "fa fa-refresh"}
             style={{
               fontSize: 25,
               padding: "2px 2px 3px 2px"
             }}
          />
        </div>
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key)} disabled={isDisabled || !!isAddNew}>Add</button>
        </div>
        <div className="control">
          <button className="button is-light is-small" onClick={() => this.onReset(key)} disabled={isDisabled}>Reset
          </button>
        </div>
      </div>
    )
  }

  onEdit = (key, index, participant) => {
    const {isEditable} = this.state
    this.props.addAuditLog({userName: this.state.userName, log: `Participants In ${key} one row edited`, date: new Date(), key})
    if(isEditable || isEditable === 0) { swal("Warning", `First save the current participants`, "warning"); return }
    let tempNewAddress = ""
    if (!participant.partUserAddress && !participant.isNew) {
      tempNewAddress = participant.partContactAddrLine1
    }
    this.setState({
      isEditable: index,
      tempNewAddress: tempNewAddress || ""
    })
  }

  onCancel = (key) => {
    this.setState({
      isEditable: "",
    })
  }

  onRemoveParticipants = (removeId, type, index, name, tableTitle, partiType) => {
    const loanId = this.props.id
    const {userName, isEditable, confirmAlert, sortBy, ascDsc} = this.state

    if(isEditable || isEditable === 0) { swal("Warning", `First save the current participants`, "warning"); return }

    confirmAlert.text = `You want to delete this ${tableTitle}?`
    swal(confirmAlert)
      .then((willDelete) => {
        if (type && loanId && removeId) {
          this.props.addAuditLog({
            userName,
            log: `Removed participant ${name}`,
            date: new Date(),
            key: "participants"
          })
          if (willDelete) {
            this.setState({
              isSaveDisabled: true
            }, () => {
              this.props.onRemove(removeId, (res) => {
                if (res && res.status) {
                  this.props.submitAuditLogs(loanId)
                  let newPart = this.state.participants.find(part => part.isNew)
                  let participants = (res.participants && res.participants.length) ? cloneDeep(res.participants) : []
                  newPart = newPart ? cloneDeep(newPart) : !participants.length ? cloneDeep(CONST.Participants) : ""
                  if (newPart) {
                    participants.push(newPart)
                  }
                  this.setState({
                    participants,
                    tempParticipants: (res.participants && res.participants.length) ? cloneDeep(res.participants) : [CONST.Participants],
                    isEditable: (res.participants && res.participants.length) ? "" : 0,
                    errorMessages: {},
                    isSaveDisabled: false
                  }, () => {
                    this.addUserStatusInParticipants()
                    if(sortBy && ascDsc) {
                      this.onSortByChange(sortBy, ascDsc)
                    }
                  })
                } else {
                  this.setState({
                    isSaveDisabled: false
                  })
                }
              })
            })
          }
        } else {
          const {participants} = this.state
          if (willDelete) {
            if (type === "participants") {
              participants.splice(index, 1)
            }
            this.setState({
              participants,
              isEditable: "",
              errorMessages: {},
              isSaveDisabled: false
            })
          } else {
            this.setState({
              isSaveDisabled: false
            })
          }
        }
    })
  }

  onCheckAddTODL = async (e, item, participantType) => {
    const {type, id} = this.props
    const payload = {
      _id: item._id,
      addToDL: e.target.checked
    }
    this.props.onCheckAddTODL(e, item, participantType, type, id, payload, (res) => {
      if(res && res) {
        this.setState({
          participants: res && res.participants || [],
          tempParticipants: res && res.participants || [],
          isEditable: "",
          errorMessages: {},
          isSaveDisabled: false
        })
      }else {
        this.setState({
          isSaveDisabled: false
        })
      }
    })
  }

  onSave = (key, participant, index) => {
    const {sortBy, ascDsc} = this.state
    const {nav1, nav2} = this.props.match.params
    delete participant.partContactAddToDL
    delete participant.partUserActive
    const errors = ParticipantsValidate(participant)

    if(errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        const message = "Required"
        if(errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if(errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.path[0]] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.path[0]]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.path[0]]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({errorMessages})
      return
    }

    if(!participant.partUserAddress) {
      delete participant.partUserAddress
    }
    if(!participant._id){
      participant.createdDate = new Date()
    }
    console.log(participant)
    this.setState({
      isSaveDisabled: true
    }, () => {
      delete participant.isNew
      this.props.onSave(participant, (res) => {
        if (res && res.status) {
           let participants = (res.participants && res.participants.length) ? cloneDeep(res.participants) : []
           participants = mergeArray(participants, participants.length ? cloneDeep(this.state.participants) : [cloneDeep(CONST.Participants)])
          this.props.submitAuditLogs(nav2)
           this.setState({
            participants,
            tempParticipants: (res.participants && res.participants.length) ? cloneDeep(res.participants) : [CONST.Participants],
            isEditable: "",
            errorMessages: {},
            isSaveDisabled: false
          }, async () => {
             this.addUserStatusInParticipants()
             if(sortBy && ascDsc) {
               this.onSortByChange(sortBy, ascDsc)
             }
           })
        }else {
          this.setState({
            isSaveDisabled: false
          })
        }
      })
    })
  }

  getFirmDetails = (firmId) => {
    const {transaction} = this.props
    const issuerId = transaction.actTranClientId || transaction.actIssuerClient || ""
    fetchSupplierContacts(firmId, `All?issuerId=${issuerId}`, (allFirms)=> {
      const userList = allFirms && allFirms.userList ? allFirms.userList.map(e => ({ ...e, name: `${e.userFirstName} ${e.userLastName}`  , id: e._id })) : []
      const participantsType = []
      allFirms && allFirms.suppliersList.forEach(ent => {
        ent.entityFlags && ent.entityFlags.marketRole && ent.entityFlags.marketRole.forEach(service => {
          if(participantsType.indexOf(service) === -1) participantsType.push(service)
        })
      })

      participantsType.unshift({
        name: "Issuer",
        id: "Issuer",
        firmId: transaction.actTranClientId || transaction.actIssuerClient || "",
        firmName: transaction.actTranClientName || transaction.actIssuerClientEntityName || transaction.actTranClientName || "",
      },{
        name: "Municipal Advisor",
        id: "Municipal Advisor",
        firmId: transaction.actTranFirmId || "",
        firmName: transaction.actTranFirmName || "",
      })

      const partFirms =  []
      allFirms && allFirms.suppliersList.forEach(ent => {
        ent.entityFlags && ent.entityFlags.marketRole && ent.entityFlags.marketRole.forEach(service => {
          if(ent._id === transaction.dealIssueTranClientId){
            partFirms.push({...ent, entityFlags: "Municipal Advisor",})
          } if(ent._id === transaction.dealIssueTranIssuerId){
            partFirms.push({...ent, entityFlags: "Issuer"})
          }else {
            partFirms.push({...ent, entityFlags: service})
          }
        })
      })
      this.setState({
        loading: false,
        isRefresh: false,
        allFirms: partFirms,
        userList,
        participantsType,
      }, () => {
        this.onSortByChange("Date", "Descending")
        this.addUserStatusInParticipants()
      })
    })
  }

  addUserStatusInParticipants = () => {
    const {participants, userList} = this.state
    if (participants && userList && participants.length && userList.length) {
      participants && participants.forEach(ent => {
        userList && userList.forEach(item => {
          if (ent.partContactId === item._id) {
            ent.partUserActive = item.userStatus
          }
        })
      })
    }
    this.setState({participants})
  }

  onSortByChange = (value, ascDsc) => {
    const {participants, searchList, searchText} = this.state
    const sort = searchText ? searchList : participants
    sort.sort(function(a, b){
      if(value === "Firm Name"){
        const nameA=a.partFirmName.toLowerCase(), nameB=b.partFirmName.toLowerCase()
        if (ascDsc === "Descending") {
          if (nameA > nameB)
            return -1
          if (nameA < nameB)
            return 1
          return 0
        } else {
          if (nameA < nameB)
            return -1
          if (nameA > nameB)
            return 1
          return 0
        }
      } else if(value === "Type"){
        const nameA=a.partType.toLowerCase(), nameB=b.partType.toLowerCase()
        if (ascDsc === "Descending") {
          if (nameA > nameB)
            return -1
          if (nameA < nameB)
            return 1
          return 0
        } else {
          if (nameA < nameB)
            return -1
          if (nameA > nameB)
            return 1
          return 0
        }
      } else if(value === "Date"){
        const dateA=new Date(a.createdDate), dateB=new Date(b.createdDate)
        if(ascDsc === "Descending"){
          return dateB-dateA
        } else {
          return dateA-dateB
        }
      }
    })
    this.setState({
      participants,
      searchList,
      sortBy: value,
      ascDsc: ascDsc
    })
  }

  onSearchText = (e) => {
    const { participants } = this.state
    this.setState({
      [e.target.name]: e.target.value
    }, () => {
      if (participants.length) {
        this.onSearch()
      }
    })
  }

  onSearch = () => {
    const { searchText, participants } = this.state
    if (searchText) {
      const searchList = participants.filter(obj => ["partType", "partFirmName", "partContactName", "partContactPhone", "partContactEmail", "partFirmName", "partContactAddrLine1", "partContactAddrLine2", "participantState"].some(key => {
        return obj[key] && obj[key].toLowerCase().includes(searchText.toLowerCase())
      }))
      this.setState({
        searchList
      })
    }
  }

  render() {
    const {searchText, searchList, allFirms, userList, errorMessages, participantsType, isSaveDisabled, isEditable, dropDown, ascDsc, sortBy, tempNewAddress} = this.state
    const participants = searchText ? searchList : this.state.participants
    const {canEditTran,type, underwriters} = this.props
    const loading = () => <Loader/>

    if(this.state.loading) {
      return loading()
    }
    return (
      <div>
        <div className="columns">
          <div className="column is-half">
            <p className="control has-icons-left">
              <input className="input is-small is-link" type="text" name="searchText" placeholder="search" onChange={this.onSearchText} title="Search"/>
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <div className="column">
            <DropDownInput  placeholder="Sort By" data={dropDown.sortBy} value={sortBy} onChange={(value) => this.onSortByChange(value)}/>
          </div>
          {sortBy &&
          <div className="column">
            <DropDownInput  data={dropDown.ascDsc} value={ascDsc || "Ascending"} onChange={(value) => this.onSortByChange(sortBy, value)}/>
          </div> }
        </div>
        <Accordion multiple activeItem={[0]} boxHidden render={({activeAccordions, onAccordion}) =>
          <div>
            <RatingSection onAccordion={() => onAccordion(0)} title={this.props.title || "Add Participants"}
               actionButtons={this.actionButtons("participants", isSaveDisabled, canEditTran)} style={{overflowY: "unset"}}>
              {activeAccordions.includes(0) &&
              <div>
                {Array.isArray(participants) && participants.length ? participants.map((participant, i) => {
                  const errors = (errorMessages && errorMessages.participants && errorMessages.participants[i.toString()]) || {}
                  const users = userList.filter(e => participant.partFirmId === (e.entityId && e.entityId._id))
                  const isExistsUser = (participants && participants.filter(part => part.partFirmId === participant.partFirmId && part.partType === participant.partType).map(p => p.partContactId)) || []
                  const filterUser = users.filter(user => isExistsUser.indexOf(user.id) === -1)
                  let partFirms = allFirms.filter(firm =>  firm.entityFlags === participant.partType)
                  if(participant.partType === "Underwriting Services" && type === "Others"){
                    const underWritingFirm = underwriters.map(f => f.partFirmId)
                    partFirms = partFirms.filter(partF => underWritingFirm.indexOf(partF.id) !== -1)
                  }
                  const user = cloneDeep(users).find(e => e.id === participant.partContactId && e.userEmails)
                  const userEmails = (user && user.userEmails.map(u => u.emailId)) || []
                  const userPhones = (user && user.userPhone.map(u => u.phoneNumber)) || []
                  const userAddresses = user ? user.userAddresses.map(u => ({
                    ...u,
                    id: u._id,
                    name: u.addressName,
                  })) : []

                  const newAddress = {id: "", name: "Enter new Address"}
                  userAddresses && userAddresses.unshift(newAddress)

                  return (
                    <div key={i.toString()}>
                      <Participants
                        category="participants" index={i}
                        firms={partFirms}
                        userPhones={userPhones}
                        userAddresses={userAddresses /* _.orderBy(userAddresses, ['name']) */}
                        userEmails={userEmails}
                        partType={_.orderBy(participantsType)}
                        errors={errors} userList={filterUser}
                        isEditable={isEditable}
                        participant={participant}
                        onItemChange={this.onItemChange}
                        canEditTran={canEditTran}
                        onBlur={this.onBlur}
                        onRemove={this.onRemoveParticipants}
                        onEdit={this.onEdit}
                        onSave={this.onSave}
                        onCancel={this.onCancel}
                        onCheckAddTODL={this.onCheckAddTODL}
                        isSaveDisabled={isSaveDisabled}
                        tableTitle={this.props.title}
                        onParticipantsRefresh={() => this.getFirmDetails(this.props.firmId)}
                        searchText={searchText}
                        tempNewAddress={tempNewAddress}
                      />
                      {i !== (participants.length-1) ? <hr/> : null}
                    </div>
                  )
                }): null}
              </div>
              }
            </RatingSection>
          </div>
        }/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(ParticipantsView)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))

