import React from "react"
import PropTypes from "prop-types"

export const Modal = ({ children, closeModal, modalState, title, fullWidth,
  showBackground, addedClass, contentClass }) => {
  if(!modalState) {
    return null
  }

  return(
    <div className={`modal is-active ${addedClass || ""}`}>
      <div className="modal-background" onClick={closeModal}
        role="presentation"
        style={showBackground ? {opacity: 0.1} : null}
        onKeyPress={ () => {} } />
      <div className="modal-card" style={fullWidth ? {width: "100%"} : null}>
        <header className="modal-card-head">
          <p className="modal-card-title">{title}</p>
          <button className="delete" onClick={closeModal} />
        </header>
        <section className="modal-card-body">
          <div className={contentClass || "content"}>
            {children}
          </div>
        </section>
        <footer className="modal-card-foot">
          <button className="button" onClick={closeModal} onKeyPress={ () => {} }>Cancel</button>
        </footer>
      </div>
    </div>
  )
}

Modal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  modalState: PropTypes.bool.isRequired,
}

class BulmaModalCard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      modalState: false
    }

    this.toggleModal = this.toggleModal.bind(this)
  }

  toggleModal() {
    this.setState((prev) => {
      const newState = !prev.modalState

      return { modalState: newState }
    })
  }

  render() {
    return(
      <section className="section">
        <div className="container">
          <div className="has-text-centered content">
            <h1 className="title">React + Bulma modal NEW TEST</h1>
            <hr />
            <button className="button is-primary" onClick={this.toggleModal} >
              Open Modal
            </button>
          </div>

          <Modal
            closeModal={this.toggleModal}
            modalState={this.state.modalState}
            title="Example modal title"
          >
            <p>NAVEEN BALAWAT SOUJANYA AMAZING asdfsadf HO - Lorem ipsum dolor sit amet,afsadfsadf consectetur adipiscing asdfsadf. Maecenas sit amet justo in arcu efficitur malesuada nec ut diam. Aenean a iaculis eros. Proin nec purus congue, rutrum sapien id, sodales ante. Nam imperdiet sapien pretium leo dapibus euismod. Ut ac venenatis nunc. Praesent viverra purus vel lacus ullamcorper porta a a augue. Proin rhoncus tempus leo sed ultricies. In luctus aliquam placerat. Cras efficitur enim vitae vulputate consequat. Nulla tellus est, fringilla quis nisi eu, aliquam finibus eros.</p>
            <p>Aliquam est dui, variussafseu tempor ac, ornare vel magna. Suspendisse potenti. Nullam gravida fermentum turpis, at ultricies risus bibendum sit amet. Nulla et arcu id nisi semper ullamcorper cursus sed magna. Phasellus pulvinar ligula vehicula consequat sagittis. Donec tristique tellus sed ex euismod ullamcorper. Vivamus nibh metus, scelerisque sed lorem eget, auctor lobortis sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin congue auctor diam, efficitur dignissim neque. Pellentesque vitae odio ut odio auctor feugiat. Curabitur eget mauris nibh. Vestibulum massa nunc, iaculis at purus venenatis, mollis tincidunt tortor.</p>
          </Modal>
        </div>
      </section>
    )
  }
}

export default BulmaModalCard
