import React from "react"
import swal from "sweetalert"
import { SelectLabelInput, MultiSelect } from "./TextViewBox"
import { getUserTransactions } from "../StateManagement/actions/CreateTransaction"
import Loader from "./Loader"
import CONST from "../../globalutilities/consts"

class RelatedTransactions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      allTrans: {},
      transactions: [],
      filterTrans: [],
      loading: false,
      activityType: "",
      confirmAlert: CONST.confirmAlert
    }
  }

  onChangeGetTransactions = event => {
    const { name, value } = event.target

    this.setState(
      {
        [name]: value,
        transactions: [],
        loading: true
      },
      async () => {
        if (value) {
          const transactions = await getUserTransactions(value)
          this.setState(
            prevState => ({
              transactions,
              allTrans: {
                ...prevState.allTrans,
                [value]: transactions
              },
              loading: false
            }),
            () => {
              this.props.onRelatedTranSelect([])
              this.onFilterRelatedTrans()
            }
          )
        } else {
          this.setState({
            loading: false,
            transactions: [],
            filterTrans: []
          },() => this.props.onRelatedTranSelect([]))
        }
      }
    )
  }

  onRelatedTranSelect = items => {
    items = items.map(tran => ({
      id: tran.activityId || tran.id || "",
      name: tran.activityDescription || tran.name || "",
      relTranId: tran.activityId || tran.relTranId || "",
      relTranIssueName: tran.activityIssueName || tran.relTranIssueName || "",
      relTranProjectName:
        tran.activityDescription || tran.relTranProjectName || "",
      relTranClientId: tran.activityTranClientId || tran.relTranClientId || "",
      relTranClientName:
        tran.activityClientName || tran.relTranClientName || "",
      relType: this.props.item.actTranRelatedType || tran.relType || ""
    }))
    this.props.onRelatedTranSelect(items)
  }

  onFilterRelatedTrans = () => {
    const { transactions } = this.state
    const { transaction, item } = this.props
    const actTranRelatedTo =
      transaction.actTranRelatedTo ||
      transaction.actRelTrans ||
      transaction.rfpTranRelatedTo ||
      transaction.dealIssueTranRelatedTo ||
      []
    const actTranRelatedType =
      item.actTranRelatedType || item.rfpTranRelatedType || ""

    const related = actTranRelatedTo
      .filter(r => r.relType === actTranRelatedType)
      .map(r => r.relTranId)
    const filterTrans = transactions.filter(
      e => related && related.indexOf(e.id) === -1
    )

    console.log("*****related*****", related)
    console.log("*****filterTrans*****", filterTrans)
    this.setState({
      filterTrans
    })
  }

  onChange = e => {
    this.props.onChange(e)
    this.onFilterRelatedTrans()
  }

  onCancel = () => {
    this.setState(
      {
        activityType: "",
        transactions: [],
        filterTrans: []
      },
      () => this.props.onRelatedtranCancel()
    )
  }

  onRemove = (related, index) => {
    const { confirmAlert } = this.state
    confirmAlert.text = `You want to delete this ${related &&
      related.relTranProjectName}?`
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        if (related && related._id) {
          this.props.deleteRelatedTran(related._id) //eslint-disable-line
          this.onFilterRelatedTrans()
        }
        if (related) {
          this.props.deleteRelatedTran(index)
        }
      }
    })
  }

  onSave = () => {
    this.setState({
      activityType: "",
      transactions: [],
    },() => this.props.onRelatedtranSave())
  }

  render() {
    const { allTrans, activityType, loading, filterTrans } = this.state
    const {
      relatedTypes,
      relatedTransactions,
      errorMessages,
      isSaveDisabled,
      canEditTran
    } = this.props
    let { item } = this.props

    item = {
      ...this.props.item,
      actTranRelatedTo:
        item.actTranRelatedTo ||
        item.actRelTrans ||
        item.rfpTranRelatedTo ||
        item.dealIssueTranRelatedTo ||
        [],
      actTranRelatedType:
        item.actTranRelatedType || item.rfpTranRelatedType || ""
    }

    const transactions = this.state.transactions.map(tran => ({
      ...tran,
      group: tran.actTranType
        ? tran.actTranType
        : tran.actType
        ? tran.actType
        : tran.rfpTranType === "RFP"
        ? tran.rfpTranType
        : tran.dealIssueTranSubType === "Bond Issue"
        ? "Deal"
        : ""
    }))

    if (
      allTrans &&
      Object.keys(allTrans).length &&
      item.actTranRelatedTo &&
      item.actTranRelatedTo.length
    ) {
      item.actTranRelatedTo.forEach(relatedTran => {
        let finRelatedTran = {}
        Object.keys(allTrans).forEach(key => {
          if (!finRelatedTran || !Object.keys(finRelatedTran).length) {
            finRelatedTran = allTrans[key].find(
              tran => tran.activityId === relatedTran
            )
          }
        })
        if (finRelatedTran && Object.keys(finRelatedTran).length) {
          transactions.push(finRelatedTran)
        }
      })
    }

    if (loading) {
      return <Loader />
    }

    return (
      <div>
        {canEditTran ? (
          <div className="columns">
            <div className="column">
              <SelectLabelInput
                error={errorMessages.actTranRelatedType || ""}
                list={relatedTypes || []}
                name="actTranRelatedType"
                value={item.actTranRelatedType}
                onChange={this.onChange}
              />
            </div>
            <div className="column">
              <div className="select is-fullwidth is-link is-small">
                <select
                  name="activityType"
                  value={activityType || ""}
                  onChange={this.onChangeGetTransactions}
                >
                  <option value="">Select Transaction Type</option>
                  <option value="RFP">RFP</option>
                  <option value="Deals">Deal</option>
                  <option value="BankLoans">Bank Loan</option>
                  <option value="Others">Others</option>
                  <option value="Derivatives">Derivatives</option>
                  <option value="ActMaRFP">MA-RFP</option>
                  <option value="ActBusDev">Business Development</option>
                </select>
              </div>
            </div>
            <div className="column">
              {item.actTranRelatedType ? (
                <MultiSelect
                  filter
                  data={filterTrans}
                  value={item.actTranRelatedTo}
                  onChange={this.onRelatedTranSelect}
                  error={errorMessages.actTranRelatedTo || ""}
                  style={{ maxWidth: "unset" }}
                />
              ) : null}
            </div>
            <div className="column">
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={this.onSave}
                    disabled={
                      isSaveDisabled ||
                      !item.actTranRelatedType ||
                      !item.actTranRelatedTo.length ||
                      false
                    }
                  >
                    Save
                  </button>
                </div>
                <div className="control">
                  <button
                    className="button is-light is-small"
                    onClick={this.onCancel}
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>Type</th>
              <th>Issue Name</th>
              <th>Project Name</th>
              { canEditTran ? <th>Delete</th> : null }
            </tr>
          </thead>
          <tbody>
            {relatedTransactions.length
              ? relatedTransactions.map((related, i) => (
                  <tr key={i.toString()}>
                    <td className="">
                      <p className="multiExpLbl ">
                        {related.relType || related.type}
                      </p>
                    </td>
                    <td className="">
                      <p className="multiExpLbl ">
                        {related.relTranIssueName || "---"}
                      </p>
                    </td>
                    <td className=" is-two-thirds">
                      <p className="multiExpLbl ">
                        {related.relTranProjectName ||
                          related.relTranIssueName ||
                          "---"}
                      </p>
                    </td>
                    {canEditTran ?
                      <td>
                        <div className="field is-grouped">
                          <div className="control">
                            <a onClick={() => this.onRemove(related, i)}>
                              {" "}
                              {/* eslint-disable-line */}
                              <span className="has-text-link">
                              <i className="far fa-trash-alt" />
                            </span>
                            </a>
                          </div>
                        </div>
                      </td> :null
                    }
                  </tr>
                ))
              : null}
          </tbody>
        </table>
      </div>
    )
  }
}

export default RelatedTransactions
