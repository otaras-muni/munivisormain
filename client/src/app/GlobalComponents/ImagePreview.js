import React, { Component } from "react"

import Loader from "./LineLoader"
import { getDocDetails } from "GlobalUtils/helpers"

class ImagePreview extends Component {
  constructor(props) {
    super(props)
    this.state = { fileName: "", waiting: true, error: "", objectName: "" }
  }

  async componentDidMount() {
    await this.getDocInfo(this.props.docId)
  }

  async componentWillReceiveProps(nextProps) {
    if((nextProps.docId !== this.props.docId) ||
      (nextProps.name !== this.props.name)) {
      await this.getDocInfo(nextProps.docId)
    }
  }

  async getDocInfo(_id) {
    const res = await getDocDetails(_id)
    console.log("res : ", res)
    const [error, doc] = res
    if(error) {
      this.setState({ error, waiting: false })
    } else {
      let err = ""
      let objectName = ""
      const { contextId, contextType, tenantId, name } = doc
      if(tenantId && contextId && contextType && name) {
        objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      } else {
        err = "invalid doc details"
      }
      this.setState({ fileName: doc.originalName, waiting: false, error: err, objectName })
    }
  }

  render() {
    const { fileName, waiting, error, objectName } = this.state

    return (
      <div className="multiExpTblVal">
        { fileName ?
          <img src={`${process.env.API_URL}/api/s3/get-s3-object-as-stream?objectName=${encodeURIComponent(objectName)}`} alt={fileName} width={180}/>
          : null
        }
        {(waiting || !fileName) && <span><Loader /></span>}
        {error && <strong>{error}</strong>}
      </div>
    )
  }
}

export default ImagePreview
