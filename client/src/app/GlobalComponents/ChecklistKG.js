import React, { Component } from "react"
import { connect } from "react-redux"

import { getConfigChecklist, saveConfigChecklist } from "../StateManagement/actions"

class ChecklistKG extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions: {}, expanded: [], validationError: {},
      accordionTitleToChange: "", keyChanges: {}, removedKeys: [],
      changeLog: [], generalError: "" }
    this.validateSubmition = this.validateSubmition.bind(this)
    this.saveAccordions = this.saveAccordions.bind(this)
    this.addAccordion = this.addAccordion.bind(this)
    this.resetAllAcccordions = this.resetAllAcccordions.bind(this)
    this.saveData = this.saveData.bind(this)
    this.logAndSave = this.logAndSave.bind(this)
    this.saveConfigInDB = this.saveConfigInDB.bind(this)
    this.changeAccordionTitle = this.changeAccordionTitle.bind(this)
    this.initialData = {
      "Add a title": [
        {
          consider: true,
          label: "Add a label"
        }
      ]
    }
  }

  componentDidMount() {
    const { config } = this.props
    console.log("setting data in componentDidMount")
    // this.setDataInState(config)
    if(!config || !Object.keys(config).length) {
      console.log("getting db data")
      this.props.getConfigChecklist()
    } else {
      this.setDataInState(config)
    }
  }

  setDataInState(data) {
    if(!data || !Object.keys(data).length) {
      data = this.initialData
    }
    this.setState({ accordions: data, validationError: {} })
  }

  componentWillReceiveProps(nextProps) {
    const { config } = nextProps
    console.log("setting data in componentWillReceiveProps")
    this.setDataInState(config)
  }


  componentWillUnmount() {
    console.log("in componentWillUnmount")
    const { accordions, changeLog } = this.state
    this.props.saveConfigChecklist(accordions, changeLog, "redux-store")
  }

  setaccordionTitleToChange(accordionTitleToChange, e) {
    this.setState({ accordionTitleToChange })
  }

  removeAccordion(key) {
    this.setState(prevState => {
      const accordions = {}
      const keys = Object.keys(prevState.accordions).filter(k => k !== key)
      keys.forEach(k => {
        accordions[k] = prevState.accordions[k]
      })
      const removedKeys = [ ...prevState.removedKeys ]
      removedKeys.push(key)
      return { accordions, removedKeys }
    })
  }

  addAccordion() {
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      const numKeys = Object.keys(accordions).length
      accordions[`Add Title ${numKeys}`] = this.initialData["Add a title"]
      return { accordions }
    })
  }

  updateKeyChanges(prevKeyChanges, oldKey, newKey) {
    console.log("prevKeyChanges : ", JSON.stringify(prevKeyChanges))
    console.log("oldKey : ", oldKey)
    console.log("newKey : ", newKey)

    let newKeyChanges = { ...prevKeyChanges }
    const oldKeys = Object.keys(prevKeyChanges)

    if(!oldKeys.length) {
      newKeyChanges[oldKey] = newKey
      return newKeyChanges
    }

    if(newKeyChanges.hasOwnProperty(newKey)) {
      newKeyChanges = {}
      console.log("changed to original value !!")
      oldKeys.forEach(k => {
        if(k !== newKey) {
          newKeyChanges[k] = prevKeyChanges[k]
        }
      })
      return newKeyChanges
    }
    let foundOldKey = false
    oldKeys.some(k => {
      if(prevKeyChanges[k] === oldKey) {
        console.log("found match for key : ", k)
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        foundOldKey = true
        newKeyChanges[k] = newKey
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        return true
      }
    })
    if(!foundOldKey) {
      console.log("match not found : ", oldKey)
      newKeyChanges[oldKey] = newKey
    }

    return newKeyChanges
  }

  changeAccordionTitle(e) {
    const newKey = e.target.value
    console.log("newKey : ", newKey)
    this.setState(prevState => {
      const { accordionTitleToChange } = prevState

      if(accordionTitleToChange === newKey) {
        return {}
      }

      const accordionsKeys = Object.keys(prevState.accordions)
      if(accordionsKeys.includes(newKey)) {
        return { generalError: "Duplicate Title" }
      }

      const prevKeyChanges = { ...prevState.keyChanges }
      let keyChanges = {}
      const accordions = {}

      accordionsKeys.forEach(k => {
        console.log("k : ", k)
        console.log("accordionTitleToChange : ", accordionTitleToChange)
        if(k === accordionTitleToChange) {
          console.log("doing new key")
          accordions[newKey] = prevState.accordions[accordionTitleToChange]
          keyChanges = this.updateKeyChanges(prevKeyChanges, accordionTitleToChange, newKey)
        } else {
          console.log("doing old key")
          accordions[k] = prevState.accordions[k]
        }
      })
      return { accordions, accordionTitleToChange: newKey, keyChanges, generalError: "" }
    })
  }

  saveConfigInDB() {
    const { accordions, changeLog } = this.state
    console.log("changeLog : ", changeLog)
    this.props.saveConfigChecklist(accordions, changeLog)
  }

  newItemLog(e) {
    let logMessage = ""
    if(!e) {
      return logMessage
    }
    Object.keys(e).forEach(k => {
      if(k !== "_id") {
        logMessage += ` ${k} - ${e[k]}`
      }
    })
    return logMessage
  }

  checkElemChange(newItem, oldItem) {
    let elemChange = ""
    Object.keys(newItem).forEach(k => {
      if(newItem[k] !== oldItem[k]) {
        elemChange += ` ${k} - ${oldItem[k]} to ${newItem[k]}`
      }
    })
    return elemChange
  }

  logAndSave() {
    const userName = "Anon"
    const date = new Date()
    // let dateString = date.toLocaleString();
    const oldData = this.props.config
    const { accordions, keyChanges, removedKeys } = this.state
    const changeLog = []
    const newData = {}

    removedKeys.forEach(k => {
      changeLog.push({userName, log: `removed checklist with title ${k}`, date})
    })

    Object.keys(accordions).forEach(k => {
      console.log("k : ", k)
      let oldKey
      Object.keys(keyChanges).some(c => {
        console.log("c : ", c)
        if((keyChanges[c] === k)  && oldData.hasOwnProperty(c)) {
          oldKey = c
          changeLog.push({userName, log: `changed checklist Title ${c} to ${k}`, date})
          return true
        }
      })
      console.log("oldKey : ", oldKey)
      if(oldKey) {
        newData[oldKey] = accordions[k]
      } else {
        newData[k] = accordions[k]
      }
    })
    console.log("oldData : ", oldData)
    console.log("newData : ", newData)

    Object.keys(newData).forEach(n => {
      const newItems = newData[n]
      const oldItems = oldData[n]
      if(oldItems) {
        const len = oldItems.length
        newItems.forEach((e, i) => {
          if(i < len) {
            const elemChange = this.checkElemChange(e, oldItems[i])
            if(elemChange) {
              changeLog.push({userName, log: `changed item${i+1} in checklist with title ${n} - ${elemChange}`, date})
            }
          } else {
            changeLog.push({userName, log: `added following item in checklist with title ${n} - ${this.newItemLog(e)}`, date})
          }
        })
      } else {
        let newListMessage = `added new checklist with title ${n} with below items : `
        newItems.forEach((e, i) => { newListMessage += ` |${i+1}: ${this.newItemLog(e)}|`} )
        changeLog.push({userName, log: newListMessage, date})
      }
    })

    this.setState({ changeLog, accordionTitleToChange: "" }, this.saveConfigInDB)
  }

  saveData() {
    const { accordions, validationError } = this.state
    const errKeys = Object.keys(validationError)
    console.log("errKeys : ", errKeys)
    let save = true
    if(errKeys.length) {
      let error = false
      errKeys.some(i => {
        const itemKeys = Object.keys(validationError[i])
        console.log("itemKeys : ", itemKeys)
        if(!itemKeys.length) {
          return true
        }
        itemKeys.some(j => {
          const subItemKeys = Object.keys(validationError[i][j])
          console.log("subItemKeys : ", subItemKeys)
          if(!subItemKeys.length) {
            return true
          }
          subItemKeys.some(k => {
            console.log("subItem : ", validationError[i][j][k])
            if(validationError[i][j][k]) {
              error = true
              return true
            }
          })
          if(error) {
            return true
          }
        })
        if(error) {
          return true
        }
      })
      if(error) {
        save = false
      }
    }

    if(save) {
      this.logAndSave()
    }
  }

  validateSubmition() {
    this.setState(prevState => {
      const { accordions, validationError } = prevState
      const newErr = { ...validationError }
      Object.keys(accordions).forEach(a => {
        Object.keys(accordions[a]).forEach(k => {
          if(!accordions[a][k].label) {
            newErr[a] = { ...newErr[a] }
            newErr[a][k] = { ...newErr[a][k]}
            newErr[a][k].label = "No input provided"
          }
        })
      })
      return { validationError: newErr }
    }, this.saveData)
  }

  saveAccordions() {
    this.validateSubmition()
  }

  handleClick(key, e) {
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if(!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  };

  changeConsider(key, i, event) {
    // console.log(" key: ", key);
    // console.log(" i: ", i);
    const val = event.target.checked
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], consider: val}
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i], consider: ""}
      return { accordions, validationError }
    })
  }

  changeLabel(key, i, e) {
    const { value } = e.target
    console.log(" key: ", key)
    console.log(" i: ", i)
    console.log(" val: ", value)
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], label: value}
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i], label: ""}
      return { accordions, validationError }
    })
  }

  addAcccordionItem(key, e) {
    this.setState(prevState => {
      const expanded = [ ...prevState.expanded ]
      if(!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key].push({
        consider: true,
        label: ""
      })
      return { accordions, expanded  }
    })
  }

  resetAcccordion(key, e) {
    this.setState((prevState, props) => {
      const expanded = [ ...prevState.expanded ]
      if(!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      if(props.config[key]) {
        accordions[key] = [...props.config[key]]
      } else {
        accordions[key] = this.initialData["Add a title"]
      }
      return { accordions, expanded, validationError: {}  }
    })
    // this.setState({ accordions: data });
  }

  resetAllAcccordions() {
    this.props.getConfigChecklist()
    this.setState({ changeLog: [], removedKeys: [], keyChanges: {} })
  }

  renderTitleInputFieldWithIcon(key) {
    if(this.state.accordionTitleToChange === key) {
      return (
        <div className="field is-grouped">
          <div className="control">
            <input autoFocus
              className="input is-small is-link"
              type="text"
              value={key}
              onChange={this.changeAccordionTitle} />
          </div>
          <div className="control">
            <span className="icon is-small is-right">
              <i className="fa fa-times" onClick={this.setaccordionTitleToChange.bind(this, "")} />
            </span>
          </div>
        </div>
      )
    }
    return (
      <div className="field is-grouped">
        <div className="control">
          <strong>{key} </strong>
        </div>
        <div className="control">
          <i className="fa fa-edit" onClick={this.setaccordionTitleToChange.bind(this, key)} />
        </div>
      </div>
    )

  }

  renderHeader(key) {
    return (
      <nav
        className="level"
        style={{
          backgroundColor: "lightblue",
          paddingLeft: "5px",
          paddingRight: "5px",
          marginBottom: "2px"
        }}
        role="presentation"
        onKeyPress={() =>{}}
      >
        <div className="level-left">
          {this.renderTitleInputFieldWithIcon(key)}
        </div>

        <div className="level-right">
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small" type="button"
                onClick={this.addAcccordionItem.bind(this, key)}>Add</button>
            </div>
            <div className="control">
              <button className="button is-light is-small" type="button"
                onClick={this.resetAcccordion.bind(this, key)}>Reset</button>
            </div>
            <div className="control">
              <i className="fa fa-trash-o is-size-4"
                onClick={this.removeAccordion.bind(this, key)} />
            </div>
            <div className="control">
              <i className={this.state.expanded.includes(key) ?
                "fa fa-minus-circle" : "fa fa-plus-circle"}
              onClick={this.handleClick.bind(this, key)} />
            </div>
          </div>
        </div>
      </nav>
    )
  }

  renderAccordionBodyItem(e, key, i) {
    return (
      <tr key={key+i}>
        <td>
          <input
            className="checkbox"
            type="checkbox"
            checked={e.consider}
            onChange={this.changeConsider.bind(this, key, i)}
          />
          {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].consider ?
            <strong>{this.state.validationError[key][i].consider}</strong>
            : undefined
          }
        </td>
        <td>
          <input
            className="input is-small is-link"
            type="text"
            value={e.label}
            onChange={this.changeLabel.bind(this, key, i)}
          />
          {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].label ?
            <strong>{this.state.validationError[key][i].label}</strong>
            : undefined
          }
        </td>
      </tr>
    )
  }

  renderAccordionBody(key, accordion) {
    return (
      <table>
        <tbody>
          {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
        </tbody>
      </table>
    )
  }

  renderAccordion(key, accordion) {
    return (
      <div key={key}>
        {this.renderHeader(key)}
        {this.state.expanded.includes(key) ?
          this.renderAccordionBody(key, accordion)
          : undefined
        }
      </div>
    )
  }

  renderAccordions(accordions) {
    return Object.keys(accordions).map(a => this.renderAccordion(a, accordions[a]))
  }

  render() {
    return (
      <div>
        {this.renderAccordions(this.state.accordions)}
        <strong>{this.state.generalError}</strong>
          <div className="field is-grouped-center">
        <div>
          <button className="button is-link" onClick={this.addAccordion}>Add a new checklist</button>
          <button className="button is-link" onClick={this.resetAllAcccordions}>Undo All Changes</button>
        </div>
        <button className="button is-link" onClick={this.saveAccordions}>Save</button>
      </div>
      </div>
    )
  }

}

const mapStateToProps = ({ configChecklist }) => {
  console.log("configChecklist in state : ", configChecklist)
  return { config: configChecklist }
}

export default connect(mapStateToProps, { getConfigChecklist, saveConfigChecklist })(ChecklistKG)
