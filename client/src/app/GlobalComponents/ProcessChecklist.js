import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import qs from "qs"

import {
  mapAccordionsArrayToObject,
  mapAccordionsObjectToArray,
  getUsersFirmsDetails,
  checkNavAccessEntitlrment
} from "GlobalUtils/helpers"
import SelectChecklist from "./SelectChecklist"
import AttachChecklist from "./AttachChecklist"
import Loader from "./Loader"

class ProcessChecklist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checklists: [],
      selectedListId: "",
      showSelectChecklist: false,
      isSideBarOpen: true,
      participants: [],
      error: "",
      waiting: true,
      viewOnly: false,
    }
    this.updateList = this.updateList.bind(this)
    this.selectChecklist = this.selectChecklist.bind(this)
    // this.changeRoute = this.changeRoute.bind(this)
  }

  async componentDidMount() {
    const {
      checklists,
      location,
      match: {
        params: { nav2 }
      }} = this.props
    const query = qs.parse(location.search, { ignoreQueryPrefix: true }) || {}
    let selectedListId = query.cid || ""
    console.log("process componentDidMount : ", location, query)
    if (!selectedListId && checklists.length) {
      selectedListId = checklists[0].id || ""
    }
    if (selectedListId) {
      this.updateURL(selectedListId)
    }
    const action = await checkNavAccessEntitlrment(nav2)
    const viewOnly = action && nav2 && !action.edit

    const userIds = this.props.participants.map(e => e._id)
    const userFirms = await getUsersFirmsDetails(userIds)
    let participants = []
    const firms = {}
    let error = ""
    if (userFirms) {
      (userFirms || []).forEach(e => {
        firms[e._id] = { entityId: e.entityId, userFirmName: e.userFirmName }
      })
      console.log("firms : ", firms)
      participants = (this.props.participants || []).map(e => ({
        ...e,
        ...firms[e._id]
      }))
    } else {
      error = "Error in retrieving Participants Details."
    }

    console.log("participants : ", participants)
    this.setState({
      checklists: checklists || [],
      selectedListId,
      participants,
      error,
      waiting: false,
      viewOnly
    })
  }

  componentWillReceiveProps(nextProps) {
    const { checklists } = nextProps
    let { selectedListId } = this.state
    if (!selectedListId && checklists.length) {
      this.setState({ waiting: true })
      selectedListId = checklists[0].id || ""
    }
    if (selectedListId) {
      this.setState({ waiting: true })
      this.updateURL(selectedListId)
    }
    this.setState({ checklists, selectedListId, waiting: false })
  }

  componentWillUnmount() {
    console.log("attach componentWillUnmount")
  }

  // changeRoute() {
  //   const { selectedListId } = this.state
  //   this.props.history.push({
  //     search: `?cid=${selectedListId}`
  //   })
  // }

  changeChecklistStatus(id, e) {
    const status = e.target.value
    console.log("id, e : ", id, status)
    this.setState(prevState => {
      const checklists = [...prevState.checklists]
      const idx = checklists.findIndex(e => e.id === id)
      if (idx > -1) {
        checklists[idx] = { ...checklists[idx], status }
      }
      return { checklists }
    })
  }

  updateURL(selectedListId) {
    const url = window.location.href
    const url1 = url.split("?cid=")[0]
    const newUrl = `${url1}?cid=${selectedListId}`
    window.history.pushState({ path: newUrl }, "", newUrl)
  }

  selectList(selectedListId) {
    this.setState({ waiting: true })
    console.log("selectedListId : ", selectedListId)
    this.updateURL(selectedListId)
    this.setState({ selectedListId, showSelectChecklist: false, waiting: false })
  }

  updateList(lists) {
    if (lists && lists.length) {
      this.setState({ waiting: true })
      console.log("1.Here are all the Lists of checklists")
      this.setState(prevState => {
        const checklists = [
          ...prevState.checklists,
          ...lists.map(e => this.mapChecklistFormatFromObjectToArrray(e))
        ]
        console.log("2. PRINTING THE NAME OF THE CHECKLIST")
        const selectedListId = prevState.selectedListId || lists[0].id
        this.updateURL(selectedListId)
        return { checklists, selectedListId, showSelectChecklist: false,  waiting: false }
      })
    }
  }

  selectChecklist() {
    this.setState({ showSelectChecklist: true, selectedListId: "" })
  }

  mapChecklistFormatFromObjectToArrray({
    id,
    name,
    type,
    data,
    status,
    itemHeaders
  }) {
    const otherData = {}
    Object.keys(itemHeaders).forEach(k => {
      otherData[k] = {}
      otherData[k].headers = itemHeaders[k]
    })
    const checklist = {}
    checklist.id = id
    checklist.name = name
    checklist.type = type
    checklist.status = status || "active"
    checklist.data = mapAccordionsObjectToArray(data, otherData)
    return checklist
  }

  mapChecklistFormatFromArrrayToObject(checklist) {
    if (checklist && checklist.data) {
      const [itemData, otherData] = mapAccordionsArrayToObject(
        checklist.data,
        true
      )
      const itemHeaders = {}
      Object.keys(otherData).forEach(k => {
        itemHeaders[k] = otherData[k].headers
      })
      return { ...checklist, data: itemData, itemHeaders }
    }
    return {}
  }

  handleSideBarToggle = () => {
    this.setState({
      isSideBarOpen: !this.state.isSideBarOpen
    })
  }

  renderChecklistsSelection(ids, viewOnly) {
    const { isSideBarOpen, selectedListId } = this.state
    const iconClass = isSideBarOpen ? "right" : "left"

    return isSideBarOpen ? (
      <div
        className="column box is-one-quarter processchklist"
        style={{
          overflowX: "hidden",
          overflowY: "hidden",
          marginBottom: "0px",
          marginTop: "12px",
          marginLeft: "15px",
          marginRight: "5px",
          padding: "0px",
          borderRadius: "0px"
        }}
      >
        <div>
          <header className="card-header hero is-link">
            <p
              className="card-header-title has-text-white"
              style={{ justifyContent: "center" }}
            >
              Linked checklists and trackers
            </p>
          </header>
        </div>

        {/* <div className="facet-title">
          <span
            className={`fas fa-lg fa-angle-double-${iconClass}`}
            onClick={this.handleSideBarToggle}
          />
        </div> */}

        {ids.length ? (
          <div className="column">
            {ids.map(id => (
              <div className="field is-grouped"  key={id.id}>
                <div className={viewOnly ? "control view-only-mode" : "control"}>
                  <div className="select is-link is-small">
                    <select
                      value={id.status}
                      onChange={this.changeChecklistStatus.bind(this, id.id)}
                    >
                      <option value="active">Active/Keep</option>
                      <option value="archive">Archive/Drop</option>
                    </select>
                  </div>
                </div>
                <div className="control">
                  <p>
                    <a
                      title={id.name}
                      onClick={this.selectList.bind(this, id.id)}
                      className={
                        selectedListId === id.id
                          ? "has-background-warning checklist-anchor"
                          : "has-background-white checklist-anchor"
                      }
                    >
                      {id.name}
                    </a>
                  </p>
                </div>
              </div>
            ))}
          </div>
        ) : (
          undefined
        )}
        {!viewOnly ?
          <div className="column">
            <button
              className="button is-link is-fullwidth"
              onClick={this.selectChecklist}
            >
              Add a checklist or tracker
            </button>
          </div>
          : undefined
        }
      </div>
    ) : null
    /*        <div className="facet-title">
        <span
          className={`fas fa-lg fa-angle-double-${iconClass}`}
          onClick={this.handleSideBarToggle}
        />
      </div> */
  }

  // renderChecklistsSelection_old(ids) {
  //   return (
  //     <div className="column box is-one-quarter">
  //       <div className="column">
  //         <p className="title innerPgTitle">Linked checklists and trackers</p>
  //       </div>
  //       {
  //         ids.length ?
  //           <div className="column">
  //             {ids.map(id => (
  //               <p key={id.id}>
  //                 <a onClick={this.selectList.bind(this, id.id)}>
  //                   {id.name}
  //                 </a>
  //               </p>
  //             ))}
  //           </div>
  //           : undefined
  //       }
  //       <div className="column">
  //         <button className="button is-link is-small"
  //           onClick={this.selectChecklist}>Add a checklist or tracker</button>
  //       </div>
  //     </div>
  //   )
  // }

  render() {
    const {
      match: {
        params: { nav2 }
      },
      auth,
      tenantId,
      isDisabled,
    } = this.props
    const entityId = auth && auth.userEntities && auth.userEntities.entityId
    let archiveStatus
    const {
      selectedListId,
      checklists,
      showSelectChecklist,
      error,
      waiting,
    } = this.state
    if (error) {
      return <strong className="has-text-danger">{error}</strong>
    }
    let selectedChecklist = this.props.checklists.filter(
      e => e.id === selectedListId
    )[0]
    if (!selectedChecklist) {
      selectedChecklist = checklists.filter(e => e.id === selectedListId)[0]
      selectedChecklist = this.mapChecklistFormatFromArrrayToObject(
        selectedChecklist
      )
    } else {
      selectedChecklist = this.mapChecklistFormatFromArrrayToObject(
        selectedChecklist
      )
      archiveStatus = selectedChecklist.status
    }
    if (selectedListId && selectedChecklist) {
      const idx = checklists.findIndex(e => e.id === selectedListId)
      if (idx > -1) {
        selectedChecklist = {
          ...selectedChecklist,
          status: checklists[idx].status
        }
        if (checklists[idx].status !== "archive") {
          archiveStatus = "active"
        }
      }
    }
    console.log("view parms : ", archiveStatus, entityId, tenantId, nav2, showSelectChecklist, selectedListId)
    const viewOnly = (
      archiveStatus === "archive" || entityId !== tenantId || (this.state.viewOnly) || isDisabled
    )
    return (
      <div>
        <div>
          <div className="switchContainer">
            <span className="filterText"> Side Menu</span>
            <label className="customswitch" style={{ marginLeft: "10px" }}>
              <input
                type="checkbox"
                onChange={this.handleSideBarToggle}
                checked={this.state.isSideBarOpen}
              />
              <span className="customslider round" />
            </label>
          </div>

          <div className="column">
            {waiting && <Loader />}
            <div className="columns">
              {this.renderChecklistsSelection(
                checklists.map(e => ({
                  id: e.id,
                  name: e.name,
                  status: e.status
                })),
                viewOnly
              )}
              {selectedListId ? (
                <div className="column no-padding-top">
                  <AttachChecklist
                    viewMode={viewOnly}
                    checklists={this.state.checklists}
                    totalThresholds={this.props.totalThresholds}
                    selectedListId={this.state.selectedListId}
                    participants={this.state.participants}
                    onSaveChecklist={this.props.onSaveChecklist}
                    processId={nav2}
                    checklist={selectedChecklist}
                  />
                </div>
              ) : (
                undefined
              )}
              {(showSelectChecklist || !selectedListId) ? (
                viewOnly ?
                  <div className="column">
                    {!waiting && <p>There is no checklist attached to the transaction</p>}
                  </div>
                  :
                  <div className="column" style={{ overflow: "auto" }}>
                    <SelectChecklist
                      totalThresholds={this.props.totalThresholds}
                      participants={this.props.participants}
                      alreadySelectedLists={checklists
                        .map(e => e.id)
                        .filter(v => v)}
                      onLinkToTxn={this.updateList}
                    />
                  </div>
              )
                : undefined
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default connect(mapStateToProps)(withRouter(ProcessChecklist))
