import React from "react"
import CKEditor from "react-ckeditor-component"
import {Modal} from "../FunctionalComponents/TaskManagement/components/Modal"
import {TextLabelBox, TextLabelInput} from "./TextViewBox"
import {COPYTABS} from "../../globalutilities/consts"

export const CloneTransaction = ({ closeModal, state, onChange, onSave }) => {
  const onEditorChange = (e, name) => {
    const value = e.editor.getData()
    onChange({target: {name, value, type: "text"}})
  }
  const {tranType, tranSubType} = state.copyFrom
  const transactionType =
    tranSubType === "BOND ISSUE" ? "BOND ISSUE" :
      tranSubType === "BANK LOAN" ? "BANK LOAN" :
        tranType === "DERIVATIVE" ? "DERIVATIVE" :
          tranType === "RFP" ? "RFP" :
            tranType === "OTHERS" ? "OTHERS" :
              tranType === "BUSINESSDEVELOPMENT" ? "BUSINESSDEVELOPMENT" :
                tranType === "MA-RFP" ? "MA-RFP" : "BANK LOAN"
  return (
    <Modal
      closeModal={closeModal}
      modalState={state.copyModalState}
      title="Make a copy"
      saveModal={onSave}
      styleBody={{ minHeight: 300 }}
    >
      <div>
        <div className="columns">
          <TextLabelBox
            label="Client Name"
            value={(state.copyFrom && state.copyFrom.tranClientName) || ""}
          />
          <TextLabelBox
            label="Lead Manager"
            value={(state.copyFrom && state.copyFrom.tranAssigneeDetails && state.copyFrom.tranAssigneeDetails.userFullName) || ""}
          />
        </div>
        <div className="columns">
          <TextLabelInput title="Issue Name" label="Issue Name" error={(state.errors && state.errors.issueName) || ""} type="text" name="issueName" value={(state.makeCopy && state.makeCopy.issueName) || ""}
            placeholder="Issue Name" onChange = {onChange}/>
        </div>
        <div className="columns">
          <TextLabelInput title="Project Name" label="Project Name" error={(state.errors && state.errors.projectName) || ""} type="text" name="projectName" value={(state.makeCopy && state.makeCopy.projectName) || ""}
            placeholder="Project Name" onChange = {onChange}/>
        </div>
        {
          transactionType && COPYTABS[transactionType] && transactionType && COPYTABS[transactionType].length ?
            <div className="has-text-weight-bold">Which tabs details you need to copy?</div>
            :null
        }
        <div className="columns">
          <div className="column">
            {
              transactionType && COPYTABS[transactionType] ? COPYTABS[transactionType].map(tab => (
                <div key={tab.name} className="control" style={{display: "inline-block", width: "50%"}}>
                  <label className="checkbox-button"> {tab.name}
                    <input type="checkbox" name="tabs" value={tab.key} checked={state.makeCopy.tabs.indexOf(tab.key) !== -1 || false} onClick={onChange} onChange={onChange} />
                    <span className="checkmark" />
                  </label>
                </div>
              )) : null
            }
          </div>
        </div>
        <hr/>
        <div className="columns">
          <div className="column">
            <div className="title is-6 has-text-weight-semibold has-text-centered">
              Email Body
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Message</p>
            <div className="control">
              <CKEditor
                activeClass="p10"
                content={(state.makeCopy && state.makeCopy.emailMessage) || ""}
                events={{
                  change: e => onEditorChange(e, "emailMessage")
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}
