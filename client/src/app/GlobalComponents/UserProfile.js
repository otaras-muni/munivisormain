import React, { Component } from "react"
import { connect } from "react-redux"
// import { withRouter } from "react-router-dom"
import { toast } from "react-toastify"
import { Combobox } from "react-widgets"
import MaskedInput from "react-text-mask"
import PhoneInput, {formatPhoneNumber, isValidPhoneNumber} from "react-phone-number-input"

import { Modal } from "Global/BulmaModal"
import { checkPasswordValid, changeUserPassword, getEntityUserDetails,
  saveEntityUserDetails } from "GlobalUtils/helpers"
import Loader from "./Loader"


import { signOut } from "../StateManagement/actions"
import CONST, { passwordStrength } from "../../globalutilities/consts"
import { timeZones } from "../../globalutilities/dateutils"

class UserProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: { profile: true, password: false }, currentUser: {},
      userId: "", userEmail: "",  firmName: "", timeZone: {},
      userPhone: [], userFax: [],
      userFirstName: "", userMiddleName: "", userLastName: "",
      currentPassword: "", newPassword: "", confirmPassword: "",
      modalState:false, modalType: "", modalIndex: -1,
      validationError: {}, waiting: false, errors: {}, generalError: ""
    }
    this.changeField = this.changeField.bind(this)
    this.changePassword = this.changePassword.bind(this)
    this.onSaveCallback = this.onSaveCallback.bind(this)
    this.toggleExpand = this.toggleExpand.bind(this)
    this.changeTimeZone = this.changeTimeZone.bind(this)
    this.getUserDetailsCallback = this.getUserDetailsCallback.bind(this)
    this.addPhone = this.addPhone.bind(this)
    this.resetPhone = this.resetPhone.bind(this)
    this.addFax = this.addFax.bind(this)
    this.resetFax = this.resetFax.bind(this)
    this.saveProfileDetails = this.saveProfileDetails.bind(this)
    this.saveCallback = this.saveCallback.bind(this)
  }

  async componentDidMount() {
    const { auth } = this.props
    this.setUserAuthInState(auth, true)
  }

  componentWillReceiveProps(nextProps) {
    const { auth } = nextProps
    this.setUserAuthInState(auth)
  }

  onSaveCallback(err) {
    this.setState({
      currentPassword: "", newPassword: "", confirmPassword: "",
      validationError: {}, waiting: false
    })
    if (err) {
      toast("Error in changing password. Please try again.", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    } else {
      toast("Password changed successfully. Please login again.", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
      this.props.signOut()
      this.props.history.push("/signin")
    }
  }

  getUserDetailsCallback(err, res) {
    if (err) {
      this.setState({ waiting: false, generalError: "Error in getting user data" })
    } else {
      console.log("res : ", res)
      const currentUser = res || {}

      const { userFirstName, userMiddleName, userLastName, userPhone,
        userFax, userJobTitle, userManagerEmail, userJoiningDate, userEmployeeID,
        userExitDate, userCostCenter, timeZone } = currentUser

      this.setState({
        waiting: false,
        generalError: "",
        currentUser,
        userFirstName,
        userMiddleName,
        userLastName,
        timeZone,
        userPhone,
        userFax,
        userJobTitle,
        userManagerEmail,
        userJoiningDate,
        userEmployeeID,
        userExitDate,
        userCostCenter,
        error: {}
      })
    }
  }

  getUserData() {
    const { userId } = this.state
    if (userId) {
      this.setState({ waiting: true })
      getEntityUserDetails(userId, this.getUserDetailsCallback)
    }
  }

  setUserAuthInState(auth, getUser) {
    const userId = (auth && auth.userEntities && auth.userEntities.userId) || ""
    const userEmail = (auth && auth.loginDetails && auth.loginDetails._id) || ""
    const firmName = (auth && auth.loginDetails && auth.loginDetails.relatedFaEntities &&
     auth.loginDetails.relatedFaEntities[0] && auth.loginDetails.relatedFaEntities[0].firmName) || ""
    if(!userId || !userEmail) {
      this.setState({ userId: "", userEmail: "", firmName: "",
        generalError: "No login information found" })
    } else {
      this.setState({ userId, userEmail, firmName, generalError: "" }, getUser ?
        this.getUserData: (() => {}))
    }
  }

  checkPasswordStrngth = (password) => {
    const { errors } = this.state
    const strongRegex = new RegExp(
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})(?=.*[!@#$%^&*])"
    )
    const digitRegex = new RegExp(
      "^(?=.*[0-9])"
    )
    const lowerRegex = new RegExp(
      "^(?=.*[a-z])"
    )
    const upperRegex = new RegExp(
      "^(?=.*[A-Z])"
    )
    const chars8Regex = new RegExp(
      "^(?=.{8,})"
    )
    const specialRegex = new RegExp(
      "^(?=.*[!@#$%^&*])"
    )

    if (digitRegex.test(password)) {
      errors.liDigit = true
    } else {
      errors.liDigit = false
    }
    if (lowerRegex.test(password)) {
      errors.liLower = true
    } else {
      errors.liLower = false
    }
    if (upperRegex.test(password)) {
      errors.liUpper = true
    } else {
      errors.liUpper = false
    }
    if (chars8Regex.test(password)) {
      errors.li8Chars = true
    } else {
      errors.li8Chars = false
    }
    if (specialRegex.test(password)) {
      errors.liSpecial = true
    } else {
      errors.liSpecial = false
    }
    if (strongRegex.test(password))
      errors.password = ""
    else
      errors.password = "Error"

    this.setState({ errors })
  }

  async checkCurrentPasswordValid(email, password) {

    const res = await checkPasswordValid(email, password)
    return res
  }

  checkNonEmptyErrorKey(error, keys) {
    if (!error) {
      return false
    }
    if (Object.getPrototypeOf(error).constructor.name === "Object") {
      keys = keys || Object.keys(error).filter(k => k !== "userAddresses")
      if (!keys.length) {
        return false
      }
      let nonEmptyError = false
      keys.some(k => {
        if (this.checkNonEmptyErrorKey(error[k])) {
          nonEmptyError = true
          return true
        }
      })
      return nonEmptyError
    }
    return true
  }

  checkEmptyArray(arr) {
    let result = -1
    arr.some((e, i) => {
      if(!e) {
        result = i
        return true
      }
    })
    return result
  }

  checkAnyError(keys) {
    const { error } = this.state
    return this.checkNonEmptyErrorKey(error, keys)
  }

  toggleExpand(val) {
    this.setState(prevState => {
      const expanded = { ...prevState.expanded }
      Object.keys(expanded).forEach(item => {
        if (item === val) {
          expanded[item] = !expanded[item]
        }
      })
      return { expanded }
    })
  }

  toggleModal(modalType, modalIndex) {
    this.setState(prev => {
      const modalState = !prev.modalState
      return { modalState, modalType, modalIndex }
    })
  }

  changeField(e) {
    const { name, value } = e.target
    this.setState(prevState => {
      const validationError = { ...prevState.validationError }
      if (!value) {
        validationError[name] = ""
      } else {
        validationError[name] = ""
      }
      if (name === "newPassword" && passwordStrength === "true") {
        this.checkPasswordStrngth(value)
      }
      return { [name]: value, validationError }
    })
  }

  changeTimeZone(timeZone) {
    console.log("timeZone : ", timeZone)
    this.setState({ timeZone })
  }

  changePhone(i, e) {
    const { name, type } = e.target
    let value = type === "checkbox" ? e.target.checked : e.target.value
    console.log("i, name, val : ", i, name, value)
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userPhone = { ...error.userPhone }
      error.userPhone[i] = ""
      if (name === "phoneNumber") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length < 10) {
          error.userPhone[i] = "Phone number should have 10 digits"
        }
      }
      if (name === "extension") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length > 8) {
          error.userPhone[i] = "Ext should have max 8 digits"
        }
      }
      const userPhone = [...prevState.userPhone]
      if(name === "phonePrimary") {
        const pi = userPhone.findIndex(p => p.phonePrimary)
        if(pi > -1) {
          userPhone[pi].phonePrimary = false
        }
      }
      userPhone[i] = { ...userPhone[i] }
      userPhone[i][name] = value
      return { userPhone, error }
    })
  }

  addPhone() {
    this.setState(prevState => {
      const userPhone = [...prevState.userPhone]
      const len = userPhone.length
      const keys = ["userPhone"]
      const error = { ...prevState.error }
      error.userPhone = {}
      if (this.checkAnyError(keys)) {
        return {}
      }
      if (len) {
        const lastPhone = userPhone[len-1]
        if (lastPhone && lastPhone.phoneNumber) {
          userPhone.push({ phoneNumber: "", extension: "", phonePrimary: false })
        } else {
          error.userPhone[len-1] = "Please provide a value here before adding another phone"
        }
      } else {
        userPhone.push({ phoneNumber: "", extension: "", phonePrimary: false })
      }
      return { userPhone, error }
    })
  }

  resetPhone() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userPhone = {}
      const { currentUser } = prevState
      if (!currentUser || !currentUser.userPhone || currentUser.userPhone.length < 1) {
        return { userPhone: [{ phoneNumber: "", extension: "", phonePrimary: false }], error }
      }
      return { userPhone: [...currentUser.userPhone], error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removePhone(i) {
    this.setState(prevState => {
      const userPhone = [ ...prevState.userPhone ]
      userPhone.splice(i, 1)
      const error = { ...prevState.error }
      error.userPhone = { ...error.userPhone }
      error.userPhone[i] = ""
      return { userPhone, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  changeFax(i, e) {
    const { name, type } = e.target
    let value = type === "checkbox" ? e.target.checked : e.target.value
    console.log("i, name, val : ", i, name, value)
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userFax = { ...error.userFax }
      error.userFax[i] = ""
      if (name === "faxNumber") {
        value = value.replace(/[^0-9]/g, "")
        if(value.length < 10) {
          error.userFax[i] = "Fax number should have 10 digits"
        }
      }
      const userFax = [...prevState.userFax]
      if(name === "faxPrimary") {
        const pi = userFax.findIndex(p => p.faxPrimary)
        if(pi > -1) {
          userFax[pi].faxPrimary = false
        }
      }
      userFax[i] = { ...userFax[i] }
      userFax[i][name] = value
      return { userFax, error }
    })
  }

  addFax() {
    this.setState(prevState => {
      const userFax = [...prevState.userFax]
      const len = userFax.length
      const error = { ...prevState.error }
      error.userFax = {}
      const keys = ["userFax"]
      if (this.checkAnyError(keys)) {
        return {}
      }

      if (len) {
        const lastFax = userFax[len-1]
        if (lastFax && lastFax.faxNumber) {
          userFax.push({ faxNumber: "", faxPrimary: false })
        } else {
          error.userFax[len-1] = "Please provide a value  here before adding another fax"
        }
      } else {
        userFax.push({ faxNumber: "", faxPrimary: false })
      }
      return { userFax, error }
    })
  }

  resetFax() {
    this.setState(prevState => {
      const error = { ...prevState.error }
      error.userFax = {}
      const { currentUser } = prevState
      if (!currentUser || !currentUser.userFax || currentUser.userFax.length < 1) {
        return { userFax: [{ faxNumber: "", faxPrimary: false }], error }
      }
      return { userFax: [...currentUser.userFax], error, modalIndex: -1, modalType: "",
        modalState: false }
    })
  }

  removeFax(i) {
    this.setState(prevState => {
      const userFax = [ ...prevState.userFax ]
      userFax.splice(i, 1)
      const error = { ...prevState.error }
      error.userFax = { ...error.userFax }
      error.userFax[i] = ""
      return { userFax, modalIndex: -1, modalType: "",
        modalState: false, error }
    })
  }

  saveCallback(err, res) {
    if (!err) {
      const { _id } = res
      if (_id) {
        this.setState({ userId: _id })
        // this.props.history.push(`/add-new-taks/${_id}`)
      }
      this.getUserData()
      toast("Changes Saved", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
    } else {
      this.setState({ waiting: false })
      toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    }
  }

  saveProfileDetails() {
    this.setState({ waiting: true })
    const { userId, userFirstName, userMiddleName, userLastName,
      userPhone, userFax, timeZone } = this.state
    const checkArrays = [
      {key: "userPhone", field: "phoneNumber"},
      {key: "userFax", field: "faxNumber"},
    ]
    let emptyArrayError
    checkArrays.some(a => {
      const arr = this.state[a.key].map(d => d[a.field])
      console.log("arr : ", arr)
      const emptyIdx = this.checkEmptyArray(arr)
      console.log("emptyIdx : ", emptyIdx)
      if(emptyIdx > -1) {
        this.setState(prevState => {
          const error = { ...prevState.error }
          error[a.key] = { ...error[a.key] }
          error[a.key][emptyIdx] = "Please provide a value"
          return { error }
        })
        emptyArrayError = true
        return true
      }
    })
    if(emptyArrayError) {
      toast("Please fix highlighted errors", { autoClose: 5000, type: toast.TYPE.WARNING });
      this.setState({ waiting: false })
      return
    }
    const basicDetails = { userFirstName, userMiddleName, userLastName,
      userPhone, userFax }
    console.log("timeZone : ", timeZone)
    if(timeZone && timeZone.text) {
      basicDetails.timeZone = timeZone
    }
    saveEntityUserDetails(userId, basicDetails, this.saveCallback)
  }

  async changePassword() {
    this.setState({ waiting: true })
    const { currentPassword, newPassword, confirmPassword } = this.state
    const { loginDetails: { _id } } = this.props
    if (newPassword !== confirmPassword) {
      this.setState(prevState => {
        const validationError = { ...prevState.validationError }
        validationError.confirmPassword = "New password does not match"
        return { validationError, waiting: false }
      })
      return
    }
    if (newPassword === currentPassword) {
      this.setState(prevState => {
        const validationError = { ...prevState.validationError }
        validationError.newPassword = "New password cannot be same as the current password"
        validationError.confirmPassword = ""
        return { validationError, waiting: false }
      })
      return
    }
    if (this.state.errors.password === "Error") {
      this.setState(prevState => {
        const validationError = { ...prevState.validationError }
        validationError.newPassword = "New password isn't strong enough"
        validationError.confirmPassword = ""
        return { validationError, waiting: false }
      })
      return
    }
    const res = await this.checkCurrentPasswordValid(_id, currentPassword)
    console.log("res : ", res)
    if (!res) {
      this.setState(prevState => {
        const validationError = { ...prevState.validationError }
        validationError.currentPassword = "Invalid Password"
        return { validationError, waiting: false }
      })
      return
    }
    console.log("changing")
    changeUserPassword(_id, newPassword, "UserProfile", this.onSaveCallback)
  }

  renderPasswordChange() {
    const { currentPassword, newPassword, confirmPassword, validationError,
      errors, expanded } = this.state
    const { loginDetails: { _id, userEntities } } = this.props
    const { userFirstName, userLastName } = userEntities[0]
    return (
      <section className="accordions">
        <article className="accordion is-active">
          <div className="accordion-header toggle">
            <p onClick={this.toggleExpand.bind(this, "password")}>Change Password</p>
          </div>

          {expanded.password ?
            <div className="accordion-body">
              <div className="accordion-content">
                <span>{`${userFirstName} ${userLastName} - ${_id}`}</span>
                <div className="columns">
                  <div className="column is-one-third">
                    <p className="multiExpLbl ">Current password</p>
                    <input className="input is-small is-link"
                      name="currentPassword"
                      value={currentPassword}
                      onChange={this.changeField}
                      type="password"
                      placeholder="" />
                    {validationError && validationError.currentPassword &&
                      <small className="text-error">{validationError.currentPassword}</small>}
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-one-third">
                    <p className="multiExpLbl ">New password</p>
                    <input className="input is-small is-link"
                      type="password"
                      name="newPassword"
                      value={newPassword}
                      onChange={this.changeField}
                      placeholder=""
                    />
                    <span className="container is-size-7">Password should be as follows
                      <ul>
                        <li className={errors.liDigit ? "li-true" : "li-false"}>Should contain at least one digit</li>
                        <li className={errors.liUpper ? "li-true" : "li-false"}>Should contain at least one upper case character</li>
                        <li className={errors.liLower ? "li-true" : "li-false"}>Should contain at least one lower case character</li>
                        <li className={errors.li8Chars ? "li-true" : "li-false"}>Should be at least 8 characters long</li>
                        <li className={errors.liSpecial ? "li-true" : "li-false"}>Should contain least 1 special character from !@#$%^&amp;*</li>
                      </ul>
                    </span>
                    {validationError && validationError.newPassword &&
                      <small className="text-error">{validationError.newPassword}</small>}
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-one-third">
                    <p className="multiExpLbl ">Confirm password</p>
                    <input className="input is-small is-link"
                      type="password"
                      name="confirmPassword"
                      value={confirmPassword}
                      onChange={this.changeField}
                      placeholder="" />
                    {validationError && validationError.confirmPassword &&
                      <small className="text-error">{validationError.confirmPassword}</small>}
                  </div>
                </div>
                <button className="button is-link is-small"
                  onClick={this.changePassword}>Change Password</button>
              </div>
            </div>
            : undefined
          }
        </article>
      </section>
    )
  }

  onPhoneNumberChange = (e, type, idx, key) => {
    const { businessAddress } = this.state
    let { userPhone, userFax } = this.state
    const Internationals = e && formatPhoneNumber(e, "International") || ""
    const phoneError = type === "userPhone" ? "Invalid phone number" : "Invalid fax number"
    const errors = Internationals ? (isValidPhoneNumber(Internationals) ? undefined : phoneError) : ""
    if(type === "userPhone"){
      userPhone = [...userPhone, ]
      if(userPhone && Array.isArray(userPhone)){
        if(userPhone.length){
          userPhone[idx] = {
            ...userPhone[idx],
            [key]: Internationals
          }
        }else {
          userPhone.push({
            [key]: Internationals
          })
        }
      }
    } else if(type === "userFax"){
      userFax = [...userFax, ]
      if(userFax && Array.isArray(userFax)){
        if(userFax.length){
          userFax[idx] = {
            ...userFax[idx],
            [key]: Internationals
          }
        }else {
          userFax.push({
            [key]: Internationals
          })
        }
      }
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress,
      userPhone,
      userFax,
      error: {
        ...prevState.error,
        [type]: {
          ...prevState.error[type],
          [idx]: errors || ""
        }
      }
    }))
  }

  renderProfileSection() {
    const { expanded, firmName, userEmail, userFirstName, userMiddleName,
      userLastName, userPhone, userFax, modalType, modalIndex, modalState,
      error } = this.state
    const phones = [...userPhone]
    if (!phones.length) {
      phones.push({ phoneNumber: "", extension: "", phonePrimary: false })
    }

    const fax = [...userFax]
    if (!fax.length) {
      fax.push({ faxNumber: "", faxPrimary: false })
    }
    return (
      <section className="accordions">
        <article className="accordion is-active">
          <div className="accordion-header toggle">
            <p onClick={this.toggleExpand.bind(this, "profile")}>Profile</p>
          </div>

          {expanded.profile ?
            <div className="accordion-body">
              <div className="accordion-content" >
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLblBlk">
                        Login Id:
                    </p>
                    <p className="multiExpLblBlk">
                      {userEmail}
                    </p>
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">
                        Firm Name:
                    </p>
                    <p className="multiExpLblBlk">
                      {firmName}
                    </p>
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">
                        Time Zone:
                    </p>
                    <Combobox
                      data={timeZones}
                      textField='text'
                      caseSensitive={false}
                      minLength={1}
                      filter='contains'
                      onChange={this.changeTimeZone}
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLblBlk">
                      First Name
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                    </p>
                    <input
                      className="input is-small is-link"
                      type="text"
                      name="userFirstName"
                      placeholder="John"
                      value={userFirstName}
                      onChange={this.changeField}
                    />
                    {error && error.userFirstName ?
                      <p className="has-text-danger">
                        {error.userFirstName}
                      </p>
                      : undefined
                    }
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">
                      Middle Name
                      {/* <span className="icon has-text-white is-invisible"><i className="fas fa-asterisk extra-small-icon" /></span> */}
                    </p>
                    <input
                      className="input is-small is-link"
                      type="text"
                      name="userMiddleName"
                      placeholder="M"
                      value={userMiddleName}
                      onChange={this.changeField}
                    />
                    {error && error.userMiddleName ?
                      <p className="has-text-danger">
                        {error.userMiddleName}
                      </p>
                      : undefined
                    }
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">
                      Last Name
                      <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>
                    </p>
                    <input
                      className="input is-small is-link"
                      type="text"
                      name="userLastName"
                      placeholder="Smith"
                      value={userLastName}
                      onChange={this.changeField}
                    />
                    {error && error.userLastName ?
                      <p className="has-text-danger">
                        {error.userLastName}
                      </p>
                      : undefined
                    }
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <p className="multiExpLblBlk">
                      Email
                    </p>
                    <p className="multiExpLblBlk">
                      {userEmail}
                    </p>
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">
                      Phone
                    </p>
                    {modalType === "phone" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Phone ${(phones[modalIndex] && phones[modalIndex].phoneNumber) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                                    onClick={this.removePhone.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                                    onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                    }
                    {
                      phones.map((e, i) => {
                        const { phoneNumber, extension, phonePrimary } = e
                        const value = phoneNumber && phoneNumber.includes("+") || false
                        const Internationals = phoneNumber && formatPhoneNumber(value ? phoneNumber : `+1${phoneNumber}`, "International") || "International"
                        return (
                          <div key={i}>
                            <div className="field is-grouped-left">
                              <div className="control">
                                {/* <MaskedInput
                                  mask={["(", /[0-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                                  className="input is-small is-link is-fullwidth"
                                  type="text"
                                  name="phoneNumber"
                                  placeholder="Phone Number"
                                  value={phoneNumber}
                                  onChange={this.changePhone.bind(this, i)}
                                /> */}
                                <PhoneInput
                                  value={ Internationals || "" }
                                  onChange={event => {this.onPhoneNumberChange(event,"userPhone",i, "phoneNumber")}}
                                />
                              </div>
                              <div className="control">
                                <MaskedInput
                                  mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                  className="input is-small is-link"
                                  type="text"
                                  name="extension"
                                  size={8}
                                  placeholder="Ext"
                                  value={extension}
                                  onChange={this.changePhone.bind(this, i)}
                                />
                              </div>
                              {!phonePrimary &&
                              <span className="has-text-link"
                                    onClick={this.toggleModal.bind(this, "phone", i)}>
                                  <i className="far fa-trash-alt" />
                                </span>
                              }
                            </div>
                            {error && error.userPhone && error.userPhone[i] ?
                              <p className="has-text-danger" style={{fontSize: 12}}>
                                {error.userPhone[i]}
                              </p>
                              : undefined
                            }
                            <div className="field is-grouped-left">
                              <p className="multiExpLblBlk">
                                is primary
                                <input
                                  type="checkbox"
                                  name="phonePrimary"
                                  checked={!!phonePrimary}
                                  onChange={this.changePhone.bind(this, i)}
                                />
                              </p>
                            </div>
                          </div>
                        )
                      })
                    }
                    <div>
                      <div className="field is-grouped is-pulled-left">
                        <div className="control">
                          <button className="button is-link is-small" type="button"
                                  onClick={this.addPhone}>Add More</button>
                        </div>
                        <div className="control">
                          <button className="button is-light is-small" type="button"
                                  onClick={this.resetPhone}>Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <p className="multiExpLblBlk">
                      Fax
                    </p>
                    {modalType === "fax" &&
                    <Modal
                      closeModal={this.toggleModal.bind(this, "", -1)}
                      modalState={modalState}
                      showBackground
                      title={`Delete Fax ${(fax[modalIndex] && fax[modalIndex].faxNumber) || ""}`}
                    >
                      <div>
                        <div className="field is-grouped">
                          <div className="control">
                            <button className="button is-link is-small" type="button"
                                    onClick={this.removeFax.bind(this, modalIndex)}>Delete</button>
                          </div>
                          <div className="control">
                            <button className="button is-light is-small" type="button"
                                    onClick={this.toggleModal.bind(this, "", -1)}>Cancel</button>
                          </div>
                        </div>
                      </div>
                    </Modal>
                    }
                    {
                      fax.map((e, i) => {
                        const { faxNumber, faxPrimary } = e
                        const value = faxNumber && faxNumber.includes("+") || false
                        const Internationals = faxNumber && formatPhoneNumber(value ? faxNumber : `+1${faxNumber}`, "International") || "International"
                        return (
                          <div key={i}>
                            <div className="field is-grouped-left">
                              <div className="control">
                                {/* <MaskedInput
                                  mask={["+", "1", "(", /[1-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
                                  className="input is-small is-link"
                                  name="faxNumber"
                                  type="text"
                                  placeholder="fax"
                                  value={faxNumber}
                                  onChange={this.changeFax.bind(this, i)}
                                /> */}
                                <PhoneInput
                                  value={ Internationals || "" }
                                  onChange={event => {this.onPhoneNumberChange(event,"userFax",i, "faxNumber")}}
                                />
                              </div>
                              {!faxPrimary &&
                              <span className="has-text-link"
                                    onClick={this.toggleModal.bind(this, "fax", i)}>
                                  <i className="far fa-trash-alt" />
                                </span>
                              }
                            </div>
                            {error && error.userFax && error.userFax[i] ?
                              <p className="has-text-danger" style={{fontSize: 12}}>
                                {error.userFax[i]}
                              </p>
                              : undefined
                            }
                            <div className="field is-grouped-left">
                              <p className="multiExpLblBlk">
                                is primary
                                <input
                                  type="checkbox"
                                  name="faxPrimary"
                                  checked={!!faxPrimary}
                                  onChange={this.changeFax.bind(this, i)}
                                />
                              </p>
                            </div>
                          </div>
                        )
                      })
                    }
                    <div className="field is-grouped is-pulled-left">
                      <div className="control">
                        <button className="button is-link is-small" type="button"
                                onClick={this.addFax}>Add More</button>
                      </div>
                      <div className="control">
                        <button className="button is-light is-small" type="button"
                                onClick={this.resetFax}>Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped-center">
                      <div className="control">
                        <button className="button is-link" onClick={this.saveProfileDetails}>
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            : undefined
          }
        </article>
      </section>
    )
  }

  render() {
    const { waiting, generalError } = this.state
    const { loginDetails } = this.props
    const isEditable = (loginDetails && loginDetails.userEntities && loginDetails.userEntities[0] && loginDetails.userEntities[0].userEntitlement === "global-edit") || false

    if(generalError) {
      return (
        <div id="main">
          <strong className="has-text-danger">{generalError}</strong>
        </div>
      )
    }
    return (
      <div id="main">
        {waiting && <Loader />}
        {this.renderProfileSection()}
        {this.renderPasswordChange()}
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => ({ auth })

export default connect(mapStateToProps, { signOut })(UserProfile)
