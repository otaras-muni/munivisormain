const data = {
  formSection1: [
    {
      fname: "naveen",
      lname: "balawat"
    }, {
      fname: "naveen",
      lname: "balawat"
    }
  ],
  formSection2: {},
  formSection3: [
    {
      fname: "naveen",
      lname: "balawat"
    }, {
      fname: "naveen",
      lname: "balawat"
    }
  ]
}

const UIConfig = {
  "myform": {
    name: "this is my form",
    onSubmit: () => console.log("please ravi"),
    onClear: () => console.log("I am clearing everything"),
    sections: [
      {
        sectionName: "formSection1",
        sectionTitle: "This is My Form with a Title",
        sectionType: "AccordionWithAdd",
        fields: [
          {
            name: "fname",
            f1: "firstName",
            etc: "sdk",
            asdf: "skadf",
            onChange: () => console.log("This is the greatest component on the earth")
          }, {
            name: "lname",
            f1: "lastName",
            etc: "sdk",
            asdf: "skadf",
            onChange: () => console.log("This is the another greatest component")
          }
        ]
      }, {
        sectionName: "formSection2",
        sectionTitle: "This is another form section that has ",
        sectionType: "ColumnsWithRows",
        fields: [
          {
            name: "fname",
            f1: "firstName",
            etc: "sdk",
            asdf: "skadf",
            row: 1,
            col: 2,
            onChange: () => console.log("This is the greatest component on the earth")
          }, {
            name: "lname",
            f1: "lastName",
            etc: "sdk",
            asdf: "skadf",
            row: 1,
            col: 2,
            onChange: () => console.log("This is the another greatest component")
          }
        ]
      }
    ]
  }
}
