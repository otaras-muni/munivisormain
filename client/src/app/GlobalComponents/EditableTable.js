import React, { Component } from "react"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import get from "lodash.get"
import swal from 'sweetalert'
import TableHeader from "./TableHeader"
import RatingSection from "./RatingSection"
import Accordion from "./Accordion"
import TableBody from "./TableBody"
import CONST from "../../globalutilities/consts"

const mergeArray = (currentList, newItems) => {
  const newObj = newItems.find(part => part.isNew)
  if (newObj) {
    currentList.push(newObj)
  }
  return currentList
}

class EditableTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errorMessages: {
        designateFirmContacts: {}
      },
      tables: [],
      isEditable: {},
      isSaveDisabled: {},
      bucketName: CONST.bucketName,
      confirmAlert: CONST.confirmAlert
    }
  }

  componentWillMount() {
    let { isEditable } = this.state
    const { tables } = this.props
    const section = {}
    tables.forEach(tbl => {
      section[tbl.key] = tbl.list && tbl.list.length ? cloneDeep(tbl.list) : [{
        ...cloneDeep(CONST.SuperVisor[tbl.key]),
        isNew: true
      }]
      isEditable[tbl.key] = (tbl.list && tbl.list.length) ? "" : 0
    })
    this.setState({
      ...section,
      isEditable,
    })
  }

  actionButtons = (key, isDisabled, tableName) => {
    const {duplicateData} = this.state
    const list = this.state[key]
    const isAddNew = (list && list.find(part => part.isNew)) || false
    const { canSupervisorEdit } = this.props
    const disable = typeof (duplicateData && duplicateData[key]) === "number"
    if (canSupervisorEdit !== undefined && !canSupervisorEdit) return
    return ( //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link is-small" onClick={() => this.onAdd(key, tableName)} disabled={isDisabled || disable || !!isAddNew}>Add</button>
          <button className="button is-light is-small" onClick={() => this.onReset(key, tableName)}>Reset</button>
        </div>
      </div>
    )
  }
  
  onReset = (key, tableName) => {
    const { isEditable } = this.state
    const { user, tables } = this.props
    const insertRow = cloneDeep(this.state[key])
    const tableObject = tables.find(tbl => tbl.key === key)
    if (isEditable[key] || isEditable[key] === 0) {
    if (tableObject.hasOwnProperty("row")) {
      insertRow.shift({
        ...cloneDeep(tableObject.row),
        isNew: true
      })
    }

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: ""
      }
    })
  }
    console.log(`add ${key} =====>`, insertRow)
 }
 
  onAdd = (key, tableName) => {
    const { isEditable } = this.state
    const { user, tables } = this.props
    const insertRow = cloneDeep(this.state[key])
    const tableObject = tables.find(tbl => tbl.key === key)

    if (isEditable[key] || isEditable[key] === 0) { swal("Warning", `First save the current item`, "warning"); return }

    if (tableObject.hasOwnProperty("row")) {
      this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${tableName} Add new Item`, date: new Date(), key })
      insertRow.unshift({
        ...cloneDeep(tableObject.row),
        isNew: true
      })
    }

    this.setState({
      [key]: insertRow,
      isEditable: {
        ...isEditable,
        [key]: insertRow.length ? 0 : 0
      },
      duplicateData: {
        ...this.state.duplicateData,
        [key]: ""
      }
    })

    console.log(`add ${key} =====>`, insertRow)
  }

  onBlur = (category, change, tableTitle) => {
    const { user } = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${change} from ${tableTitle}`, date: new Date(), key: category })
  }

  onChange = (item, category, index) => {
    const items = this.state[category]
    items[index] = item
    this.setState({
      [category]: items,
    }, () => {
      if(this.props.onContactChange){
        if(item && item.userContactType){
          this.props.onContactChange(item.userContactType)
        }
      }
    })
  }

  onEdit = (key, index, tableTitle, item) => {
    const { user } = this.props
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `In ${tableTitle} one row edited`, date: new Date(), key })
    if (this.state.isEditable[key]) { swal("Warning", `First save the current item`, "warning"); return }
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: index,
      },
    }, () => {
      if(key === "giftsToRecipients") {
        const select = {
          id: item && item.recipientEntityId || "",
          msrbFirmName: item && item.recipientEntityName || "",
          name: item && item.recipientEntityName || "",
          _id: item && item._id || ""
        }
        this.onParentChange("recipientEntityName", key, select, index, "", "Recipient's employer")
      }
    })
  }

  onCancel = (key) => {
    this.setState({
      isEditable: {
        ...this.state.isEditable,
        [key]: "",
      },
    })
    /*const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
    this.props.updateAuditLog(remainLogs) */
  }

  onRemove = (removeId, type, index, tableHeader) => {
    console.log(removeId, type, index)
    const { isEditable, confirmAlert } = this.state
    confirmAlert.text = "You want to delete?"
    swal(confirmAlert).then((willDelete) => {
      if (willDelete) {
        if (type && removeId) {
          this.props.addAuditLog({ userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `Remove item from ${tableHeader}`, date: new Date(), key: type })
          this.setState({
            isSaveDisabled: {
              ...this.state.isSaveDisabled,
              [type]: true
            }
          }, () => {
            this.props.onRemove(type, removeId, (res) => {
              if (res && res.status) {
                let newItem = this.state[type].find(item => item.isNew)
                let list = (res && res.list.length) ? cloneDeep(res.list) : []
                newItem = newItem ? cloneDeep(newItem) : !list.length ? {
                  ...cloneDeep(CONST.SuperVisor[type]),
                  isNew: true
                } : ""
                if (newItem) {
                  list.push(newItem)
                }
                this.setState({
                  [type]: list,
                  isEditable: {
                    ...isEditable,
                    [type]: (res.list && res.list.length) ? "" : 0
                  },
                  errorMessages: {},
                  isSaveDisabled: {
                    ...this.state.isSaveDisabled,
                    [type]: false
                  }
                })
              } else {
                this.setState({
                  isSaveDisabled: {
                    ...this.state.isSaveDisabled,
                    [type]: false
                  }
                })
              }
            })
          })
        } else {
          const list = this.state[type]
          if (list.length) {
            list.splice(index, 1)
            this.setState({
              [type]: list,
              isEditable: {
                ...isEditable,
                [type]: ""
              },
              errorMessages: {},
              duplicateData: {
                ...this.state.duplicateData,
                [type]: ""
              }
            })
          }
        }
      }
    })
  }

  onSave = (key, item, index) => {
    const { tables } = this.props
    const table = tables.find(tbl => tbl.key === key)
    const itemData = {
      ...item
    }
    const minDate = this.props.minDate || itemData.createdDate || ""
    delete itemData.isNew
    const errors = (table && table.handleError) ? table.handleError(itemData, minDate) : null

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        const message = err.message ? err.message : "Required"
        if (errorMessages.hasOwnProperty(key)) { //eslint-disable-line
          if (errorMessages[key].hasOwnProperty(index)) { //eslint-disable-line
            errorMessages[key][index][err.context.key] = message // err.message
          } else {
            errorMessages[key][index] = {
              [err.context.key]: message
            }
          }
        } else {
          errorMessages[key] = {
            [index]: {
              [err.context.key]: message
            }
          }
        }
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      return
    }

    console.log(`===============${key}==========`, item)
    this.setState({
      isSaveDisabled: {
        ...this.state.isSaveDisabled,
        [key]: true
      }
    }, () => {
      delete item.isNew
      this.props.onSave(key, item, (res) => {
        if (res && res.status) {
          let list = (res && res.list.length) ? cloneDeep(res.list) : []
          list = mergeArray(list, list.length ? cloneDeep(this.state[key]) : [{
            ...cloneDeep(CONST.SuperVisor[key]),
            isNew: true
          }])
          this.setState({
            [key]: list,
            errorMessages: {},
            isEditable: {
              ...this.state.isEditable,
              [key]: "",
            },
            isSaveDisabled: {
              ...this.state.isSaveDisabled,
              [key]: false
            },
            duplicateData: {
              ...this.state.duplicateData,
              [key]: ""
            },
          })
        } else {
          this.setState({
            isSaveDisabled: {
              ...this.state.isSaveDisabled,
              [key]: false
            },
            duplicateData: {
              ...this.state.duplicateData,
              [key]: index
            },
            errorMessages: {},
          })
        }
      })
    })
  }

  onParentChange = (e, key, select, index, td, inputTitle) => {
    const { user } = this.props
    this.props.onParentChange(e && e.target ? e.target.name : e, key, select, index, (res) => {
      if (res && res.status) {
        const list = this.state[key]
        list[index] = {
          ...list[index],
          ...res.object,
        }
        if (res.childKey) {
          list[index] = {
            ...list[index],
            [res.childKey]: {
              ...list[index][res.childKey],
              ...res.childObject
            }
          }
        }
        if(select.name && inputTitle){
          this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${inputTitle} select ${select.name}`, date: new Date(), key })
        }
        this.setState({
          [key]: list,
        })
      }
    })
  }

  getBidBucket = (key, filename, docId, index) => {
    const documents = cloneDeep(this.state.documents)
    this.props.onParentChange("doc", "documents", { docFileName: filename, docAWSFileLocation: docId }, index, (res) => {
      if (res && res.status) {
        const list = this.state[key]
        list[index] = {
          ...list[index],
          ...res.object,
        }
        this.setState({
          [key]: list,
        })
      }
    })
    this.props.addAuditLog({ userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `Documents upload ${filename}`, date: new Date(), key: "documents" })
  }

  deleteDoc = (type, versionId, removeId) => {
    this.props.addAuditLog({ userName: `${this.props.user.userFirstName} ${this.props.user.userLastName}`, log: `document removed from ${type}`, date: new Date(), key: type })
    this.props.deleteDoc(type, versionId, removeId, (res) => {
      if (res && res.status) {
        let newItem = this.state[type].find(item => item.isNew)
        let list = (res && res.list.length) ? cloneDeep(res.list) : []
        newItem = newItem ? cloneDeep(newItem) : !list.length ? {
          ...cloneDeep(CONST.SuperVisor[type]),
          isNew: true
        } : ""
        if (newItem) {
          list.push(newItem)
        }
        this.setState({
          [type]: list,
        })
      }
    })
  }

  render() {
    const { errorMessages, isEditable, isSaveDisabled, bucketName } = this.state
    const { tables, dropDown, user, contextType, contextId, tableStyle, minDate, maxDate, getUsers } = this.props
    const canEdit = this.props.canSupervisorEdit === undefined ? true : this.props.canSupervisorEdit
    // const style = { overflowY: "unset", overflowX: "auto" }
    const style = {}
    !canEdit ? cloneDeep(tables).forEach(table => {
      table.header = table.header.filter(head => head.name !== "Action")
    }) : this.props.tables || []

    return (
      <Accordion multiple
        activeItem={[0]}
        boxHidden
        render={({ activeAccordions, onAccordion }) =>
          <div>
            {
              tables.map((table, i) => (
                <RatingSection key={i} onAccordion={() => onAccordion(i)} title={table.name} actionButtons={this.actionButtons(table.key, isSaveDisabled[table.key], table.name)}
                  style={tableStyle || style}>
                  {activeAccordions.includes(i) &&
                    <div className="tbl-scroll">
                      <table className="table is-bordered is-striped is-hoverable is-fullwidth" style={tableStyle}>
                        <TableHeader cols={table.header} />
                        {
                          this.state[table.key] && this.state[table.key].map((item, index) => {
                            // let isExists = this.state[table.key].map(e => e.userId !== item.userId && e.userId)
                            // if (table.dropDown && table.dropDown.userIdKey) {
                            //   isExists = this.state[table.key].map(e => get(e, table.dropDown.userIdKey, "") !== get(item, table.dropDown.userIdKey, "") && get(e, table.dropDown.userIdKey, ""))
                            // }
                            // isExists = isExists.filter(v => v !== "")
                            // const users = (dropDown && dropDown.usersList) ? dropDown.usersList.filter(e => isExists.indexOf(e._id) === -1) : []
                            const error = (errorMessages && errorMessages[table.key] && errorMessages[table.key][index.toString()]) || {}
                            return <TableBody key={index.toString()} index={index} titles={table.header || []}  canEdit={canEdit}  minDate={minDate} maxDate={maxDate} tableTitle={table.name} body={table.tbody} error={error} bucketName={bucketName} item={item} users={dropDown.usersList} category={table.key} dropDown={dropDown} onItemChange={this.onChange} isSaveDisabled={isSaveDisabled[table.key] || false}
                              onBlur={this.onBlur} isEditable={isEditable} onSave={this.onSave} onCancel={this.onCancel} onEdit={this.onEdit} onRemove={this.onRemove} deleteDoc={(vId, dId) => this.deleteDoc(table.key, vId, dId)} duplicateData={(this.state.duplicateData && this.state.duplicateData[table.key]) === index ? Number : "" || false}
                              getBidBucket={(filename, docId, index) => this.getBidBucket(table.key, filename, docId, index)} contextType={contextType || ""} contextId={contextId || ""} tenantId={user.entityId} onParentChange={this.onParentChange} getUsers={getUsers}/>
                          })
                        }
                        <TableBody />
                      </table>
                      {table.children}
                    </div>
                  }
                </RatingSection>
              ))
            }
          </div>
        } />
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

export default connect(mapStateToProps, null)(EditableTable)
