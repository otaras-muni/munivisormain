import React, { Component } from "react"
import { connect } from "react-redux"
import Dropzone from "react-dropzone"
import axios from "axios"
import moment from "moment"
import qs from "qs"
import fileSize from "file-size"
import { muniApiBaseURL } from "GlobalUtils/consts"
import Loader from "./Loader"
import {getToken} from "GlobalUtils/helpers"
import withAuditLogs from "./withAuditLogs"

class DragAndDropFile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      files: [],
      uploadedFiles: [],
      inProgress: false
    }
  }

  onHandleDrop = files => {
    this.setState(
      {
        files
      },
      () => {
        this.onFileUpload()
      }
    )
  }

  onFileUpload = async () => {
    this.setState({ inProgress: true })
    const { files } = this.state
    console.log("files : ", files)
    if (files && Array.isArray(files) && files.length) {
      for (let i in files) {
        const {
          bucketName,
          contextId,
          contextType,
          tenantId,
          tags
        } = this.props
        const file = files[i]
        let fileName = file.name
        const extnIdx = fileName.lastIndexOf(".")
        if (extnIdx > -1) {
          fileName = `${fileName.substr(
            0,
            extnIdx
          )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
        }
        console.log("fileName2 : ", fileName)
        const contentType = file.type
        if (!fileName) {
          return this.setState({
            error: "No file name provided",
            inProgress: false
          })
        }
        let filePath
        if (tenantId && contextId && contextType) {
          filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
        }
        const opType = "upload"
        const options = { contentType, tags }

        const res = await axios.post(`${muniApiBaseURL}s3/get-signed-url`, {
          opType,
          bucketName,
          fileName: filePath,
          options
        },{headers:{"Authorization":getToken()}})
        if (res && res.status === 200) {
          console.log("res : ", res)
          await this.uploadWithSignedUrl(
            bucketName,
            res.data.url,
            file,
            fileName,
            tags,
            upFiles => {
              if (
                upFiles &&
                upFiles.uploadedFiles &&
                upFiles.uploadedFiles.length === files.length
              ) {
                files &&
                  files.forEach(item => {
                    this.props.addAuditLog({
                      userName: this.state.userName,
                      log: `Documents upload ${item.name}`,
                      date: new Date(),
                      key: "documents"
                    })
                  })
                this.props.onMultipleFileUpload(upFiles.uploadedFiles || [])
                this.setState({
                  files: [],
                  error: "",
                  message: "",
                  inProgress: false,
                  uploadedFiles: []
                })
              }
            }
          )
        } else {
          console.log("err : ", err)
          this.setState({
            error: "Error in getting signed url for upload",
            message: "",
            inProgress: false
          })
        }
      }
    } else {
      console.log("No file")
      this.setState({ error: "No file", inProgress: false })
    }
  }

  uploadWithSignedUrl = (
    bucketName,
    signedS3Url,
    file,
    fileName,
    tags,
    callback
  ) => {
    console.log(
      "in uploadWithSignedUrl : ",
      signedS3Url,
      bucketName,
      fileName,
      file.type
    )
    const xhr = new XMLHttpRequest()
    xhr.open("PUT", signedS3Url, true)
    xhr.setRequestHeader("Content-Type", file.type)
    if (tags) {
      console.log("no tagging")
      // xhr.setRequestHeader("x-amz-tagging", qs.stringify(tags))
    }
    xhr.onload = async () => {
      console.log("readyState : ", xhr.readyState)
      if (xhr.status === 200) {
        console.log("file uploaded ok")
        // this.setState({ error: "", message: `Last upload successful for "${fileName}" at ${new Date()}` })
        await this.updateDocsDB(bucketName, file, fileName, fileCallback => {
          callback({
            error: "",
            message: `Last upload successful for "${fileName}" at ${new Date()}`,
            ...fileCallback
          })
        })
      }
    }
    xhr.onerror = err => {
      console.log("error in file uploaded :", err)
      this.setState({
        message: "",
        error: "Error in uploading to S3",
        inProgress: false
      })
    }
    xhr.send(file)
  }

  updateDocsDB = (bucketName, file, fileName, cb) => {
    const {
      contextId,
      contextType,
      tenantId,
      docMeta,
      versionMeta,
      folderId,
      user
    } = this.props
    const meta = { ...docMeta }
    let filePath
    if (tenantId && contextType && contextId) {
      filePath = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
    }
    console.log("bucketName, fileName : ", bucketName, fileName)
    axios
      .post(`${muniApiBaseURL}s3/get-s3-object-versions`, {
        bucketName,
        fileName: filePath
      },{headers:{"Authorization":getToken()}})
      .then(res => {
        if (res.data.error) {
          console.log("err in getting version : ", res.error)
          this.setState({
            message: "",
            error: "Error in getting S3 versions",
            inProgress: false
          })
        } else {
          let versionId
          let uploadDate
          let size
          res.data.result.Versions.some(v => {
            if (v.IsLatest) {
              versionId = v.VersionId
              uploadDate = v.LastModified
              size = fileSize(v.Size).human("jedec")
              return true
            }
          })
          if (versionId) {
            meta.versions = [
              {
                versionId,
                name: fileName,
                originalName: file.name,
                uploadDate: moment(new Date()).format("MM-DD-YYYY"),
                uploadedBy: (user && `${user.userFirstName} ${user.userLastName}`) || "",
                size: fileSize(file.size).human("jedec"),
                ...versionMeta
              }
            ]
            const doc = {
              name: fileName,
              originalName: file.name,
              meta,
              contextType,
              contextId,
              tenantId,
              folderId
            }
            axios
              .post(`${muniApiBaseURL}docs`, doc,{headers:{"Authorization":getToken()}})
              .then(res => {
                console.log("inserted ok in docs db : ", res)
                const { uploadedFiles } = this.state
                uploadedFiles.push({
                  fileNeme: file.name,
                  fileId: res.data._id,
                  ...res.meta
                })
                cb({
                  message: "",
                  error: "",
                  inProgress: false,
                  uploadedFiles
                })
                /* this.setState({
                  message: "", error: "", inProgress: false,
                  uploadedFiles
                }) */
              })
              .catch(err => {
                console.log("err in inserting in docs db : ", err)
                this.setState({
                  message: "",
                  error: "Error in inserting in docs DB",
                  inProgress: false
                })
              })
          } else {
            console.log("No version error")
            this.setState({
              message: "",
              error: "S3 version error",
              inProgress: false
            })
          }
        }
      })
      .catch(err => {
        console.log("err in getting version : ", err)
        this.setState({
          message: "",
          error: "Error in getting S3 versions",
          inProgress: false
        })
      })
  }

  render() {
    const { error, message, inProgress } = this.state
    const styles = {
      width: "100%",
      height: 100,
      border: "2px dashed #ccc",
      padding: 35
    }

    if (inProgress) {
      return <Loader />
    }
    return (
      <section>
        <div className="dropzone">
          <Dropzone onDrop={this.onHandleDrop} style={styles}>
            <p style={{ textAlign: "center" }}>
              Try dropping some files here, or click to select files to upload.
            </p>
          </Dropzone>
        </div>
        <p className="is-small text-error has-text-danger">{error || ""}</p>
        {/* <aside>
          <h2>Dropped files</h2>
          <ul>
            {
              this.state.files.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
            }
          </ul>
        </aside> */}
      </section>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(DragAndDropFile)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
