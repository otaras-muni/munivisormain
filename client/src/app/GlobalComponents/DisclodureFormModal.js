import React from "react"
import {Link} from "react-router-dom"
import {Modal} from "./BulmaModal"

const DisclodureFormModal = ({toggleModal, modalState, title, path1, path2, info, type}) => (
  <Modal
    closeModal={toggleModal}
    modalState={modalState}
    title={title || `Information Q${(info && info.quarter) || ""} ${(info && info.year) || ""}`}
  >
    {
      type === "political" ?
        <div>
          <div className="columns">
            <div className="column is-vcentered">
              <p className="title innerPgTitle">
                <Link to={path1} className="button is-warning is-small">Confirm</Link>
                <strong> that you have <u>NOT</u> made any political contributions last
                  quarter and complete your G-37 obligation.
                </strong>
              </p>
            </div>
          </div>
          <div className="columns">
            <div className="column is-vcentered">
              <p className="title innerPgTitle">
                <Link to={path2} className="button is-warning is-small">Complete</Link>
                <strong> G-37 disclosure obligation.
                </strong>
              </p>
            </div>
          </div>
        </div> :
        <div>
          <div className="columns">
            <div className="column is-vcentered">
              <p className="title innerPgTitle">
                <Link to="/compliance/cmp-sup-gifts/disclosure?status=Pre-approval" className="button is-warning is-small" >Pre-approval</Link>
                <strong> from your supervisor before you make a gift. </strong>
              </p>
            </div>
          </div>
          <div className="columns">
            <div className="column is-vcentered">
              <p className="title innerPgTitle">
                <Link to="/compliance/cmp-sup-gifts/disclosure" className="button is-warning is-small" >Complete</Link>
                <strong> G-20 disclosure obligation. </strong>
              </p>
            </div>
          </div>
        </div>
    }

  </Modal>
)

export default DisclodureFormModal
