import React from "react"
import moment from "moment"
import DatePicker from "react-datepicker"
import NumberFormat from "react-number-format"
import { DropdownList, Multiselect } from "react-widgets"
import AsyncCreatableSelect from "react-select/lib/AsyncCreatable"
import CustomDropDown from "./CustomDropDown"

import "react-datepicker/dist/react-datepicker.css"
import {dateFormatted} from "../../globalutilities/helpers"

export const TextLabelBox = ({ className, label, value }) => (
  <div className={`column ${className || ""}`}>
    <p className="multiExpLblBlk">{label}</p>
    <p className="dashExpLblVal">{value}</p>
  </div>
)

export const AnchorLabelBox = ({ label, href, value }) => (
  <div className="column">
    <p className="dashExpLbl">{label}</p>
    <a href={href}>
      <small>{value}</small>
    </a>
  </div>
)

export const TextLabelInput = ({
  type,
  name,
  style,
  label,
  value,
  className,
  title,
  onChange,
  placeholder,
  error,
  min,
  max,
  onBlur,
  disabled,
  required
}) => {
  const props = {
    name,
    value,
    style,
    title: title || label || "",
    placeholder: type === "date" ? "MM-DD-YYYY" : placeholder || "",
    onChange,
    type: type || "text",
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }

  if (min) {
    props.minDate = dateFormatted(min)
  }
  if (max) {
    props.maxDate = dateFormatted(max)
  }

  if (type === "date") {
    return (
      <div
        className={className || `${label ? "column" : "w-100"}`}
        style={style || { minWidth: 100 }}
      >
        {label && (
          <p className="multiExpLblBlk">
            {label}
            {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
          </p>
        )}
        <DatePicker
          selected={value || ""}
          {...props}
          onChange={(val) => {
            onChange({target: {name, value: val ? dateFormatted(val) : null}})
          }}
          className="input is-small is-link"
        />
        {error && (
          <p
            className="is-small text-error has-text-danger "
            style={{ fontSize: 12 }}
          >
            {error}
          </p>
        )}
      </div>
    )
  } else {
    return (
      <div
        className={className || `${label ? "column" : "w-100"}`}
        style={style || { minWidth: 100 }}
      >
        {label && (
          <p className="multiExpLblBlk">
            {label}
            {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
          </p>
        )}
        {/* {disabled ? (
        <small>{value}</small>
      ) : ( */}
        <input className="input is-small is-link" {...props} />
        {/* )} */}
        {error && (
          <p
            className="is-small text-error has-text-danger "
            style={{ fontSize: 12 }}
          >
            {error}
          </p>
        )}
      </div>
    )
  }
}



export const TextInput = ({
  type,
  name,
  style,
  label,
  value,
  className,
  title,
  onChange,
  placeholder,
  error,
  min,
  max,
  onBlur,
  disabled,
  required
}) => {
  const props = {
    name,
    value,
    style,
    title: title || label || "",
    placeholder: placeholder || "",
    onChange,
    type: type || "text",
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }

  if (min) {
    props.min = moment(new Date(min).toISOString().substring(0, 10)).format(
      "YYYY-MM-DD"
    )
  }
  if (max) {
    props.max = moment(new Date(max).toISOString().substring(0, 10)).format(
      "YYYY-MM-DD"
    )
  }
  return (
    <div
      className={className || `${label ? "column" : "w-100"}`}
      style={style || { minWidth: 100 }}
    >
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
        </p>
      )}
      <input className="input is-small is-link" {...props} />
      {error && (
        <p
          className="is-small text-error has-text-danger "
          style={{ fontSize: 12 }}
        >
          {error}
        </p>
      )}
    </div>
  )
}

export const SelectLabelInput = ({ list = [], name, placeholder, className, label, title, value, onChange, error, onBlur, disabled, inputStyle, required, disableValue }) => {
  const props = {
    name,
    value,
    title: title || label || "",
    onChange: e => onChange(e),
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }
  disableValue =
    (disableValue && Array.isArray(disableValue) && disableValue) || []

  return (
    <div className={className || `${label ? "column" : ""}`}>
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
        </p>
      )}
      {/* {disabled ? (
        <small>{value}</small>
      ) : ( */}
      <div>
        <div
          className="select is-small  is-fullwidth is-link"
          style={inputStyle || { width: "100%" }}
        >
          <select style={inputStyle || { width: "100%" }} {...props}>
            <option value="">{placeholder || "Pick"}</option>
            {
              list.map((name, i) => (
                <option key={i} disabled={disableValue.indexOf(name && name.label) !== -1 || !name.included}>
                  {name && name.label}
                </option>
              ))
            }
          </select>
        </div>
        {error && (
          <p
            className="is-small text-error has-text-danger"
            style={{ fontSize: 12 }}
          >
            {error}
          </p>
        )}
      </div>
      {/* )} */}
    </div>
  )
}

export const NumberInput = ({
  type,
  name,
  label,
  prefix,
  suffix,
  value,
  title,
  format,
  className,
  thousandSeparator,
  onChange,
  placeholder,
  error,
  onBlur,
  disabled,
  decimalScale,
  required,
  style
}) => {
  const onChaneInput = e => {
    const event = {}
    if(e.value === "-"){
      e.value = 0
    }
    event.target = {
      name,
      title: title || label || "",
      placeholder: placeholder || "",
      type: type || "text",
      ...e,
    }
    onChange(event)
  }
  const valType = prefix || suffix || ""
  const props = {
    name,
    title: title || label || "",
    value: value || "",
    placeholder: placeholder || "",
    // onChange: onChaneInput,
    onValueChange: onChaneInput,
    type: type || "text",
    disabled: disabled || false,
    prefix: prefix || "",
    suffix: suffix || "",
    style,
    onBlur: onBlur || (() => { }),
    thousandSeparator:
      thousandSeparator === undefined ? true : thousandSeparator,
    decimalScale: parseInt(decimalScale, 10) || 2
  }

  if (format) {
    delete props.thousandSeparator
    props.format = format
  }
  return (
    <div className={className || `${label ? "column" : ""}`}>
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
        </p>
      )}
      {/* {disabled ? (
        <small>
          {value
            ? `${
            valType === "$"
              ? `$${value}`
              : valType === "%"
                ? `${value}%`
                : value
            }`
            : ""}
        </small>
      ) : ( */}
          <NumberFormat
            className="input is-fullwidth is-small is-link"
            {...props}
          />
       {/* )} */}
      {error && (
        <p
          className="is-small text-error has-text-danger"
          style={{ fontSize: 12 }}
        >
          {error}
        </p>
      )}
    </div>
  )
}



export const NumberDisableInput = ({
  type,
  name,
  label,
  prefix,
  suffix,
  value,
  title,
  format,
  className,
  thousandSeparator,
  onChange,
  placeholder,
  error,
  onBlur,
  disabled,
  decimalScale,
  required,
  style
}) => {
  const onChaneInput = e => {
    e.target.value = e.target.value
      .replace(/\$/g, "")
      .replace(/%/g, "")
      .replace(/,/g, "")
    onChange(e)
  }
  const valType = prefix || suffix || ""
  const props = {
    name,
    title: title || label || "",
    value: value || "",
    placeholder: placeholder || "",
    onChange: onChaneInput,
    type: type || "text",
    disabled: disabled || false,
    prefix: prefix || "",
    suffix: suffix || "",
    style,
    onBlur: onBlur || (() => { }),
    thousandSeparator:
      thousandSeparator === undefined ? true : thousandSeparator,
    decimalScale: parseInt(decimalScale, 10) || 2
  }

  if (format) {
    delete props.thousandSeparator
    props.format = format
  }
  return (
    <div className={className || `${label ? "column" : ""}`}>
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
        </p>
      )}
      <NumberFormat
        className="input is-fullwidth is-small is-link"
        {...props}
      />
      {error && (
        <p
          className="is-small text-error has-text-danger"
          style={{ fontSize: 12 }}
        >
          {error}
        </p>
      )}
    </div>
  )
}

export const DropDownInput = ({
  filter,
  data = [],
  label,
  groupBy,
  value,
  style,
  className,
  message,
  placeholder,
  disableValue,
  onChange,
  error,
  disabled,
  isFull,
  textField,
  valueField,
  required,
  busy
}) => {
  const props = {
    filter: filter || false,
    data: data || [],
    textField: textField || "name",
    value: value || "",
    valueField: valueField || "id",
    onChange,
    message,
    groupBy: groupBy || "",
    placeholder: placeholder || "",
    disabled: disabled || false,
    defaultValue: "",
    busy
  }
  return (
    <div
      className={`${
        label ? `column ${isFull && "is-full"}` : isFull ? "column is-full" : ""
        }`}
    >
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span> : null}
        </p>
      )}
      {disabled ? (
        <small>{disableValue || value}</small>
      ) : (
          <div
            className={`${className || ""}`}
            style={style || { minWidth: 100 }}
          >
            <DropdownList
              {...props}
              busySpinner={<span className="fas fa-sync fa-spin is-link" />}
            />
            {error && (
              <p
                className="is-small text-error has-text-danger"
                style={{ fontSize: 12 }}
              >
                {error}
              </p>
            )}
          </div>
        )}
    </div>
  )
}



export const DropDownDisableInput = ({
  filter,
  data = [],
  label,
  groupBy,
  value,
  style,
  className,
  message,
  placeholder,
  disableValue,
  onChange,
  error,
  disabled,
  isFull,
  textField,
  valueField,
  required,
  busy
}) => {
  const props = {
    filter: filter || false,
    data: data || [],
    textField: textField || "name",
    value: value || "",
    valueField: valueField || "id",
    onChange,
    message,
    groupBy: groupBy || "",
    placeholder: placeholder || "",
    disabled: disabled || false,
    defaultValue: "",
    busy
  }
  return (
    <div
      className={`${
        label ? `column ${isFull && "is-full"}` : isFull ? "column is-full" : ""
        }`}
    >
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"></i></span> : null}
        </p>
      )}
      <div
        className={`${className || ""}`}
        style={style || { minWidth: 100 }}
      >
        <DropdownList
          {...props}
          busySpinner={<span className="fas fa-sync fa-spin is-link" />}
        />
        {error && (
          <p
            className="is-small text-error has-text-danger"
            style={{ fontSize: 12 }}
          >
            {error}
          </p>
        )}
      </div>
    </div>
  )
}

export const DropDownSelect = ({
  data = [],
  value,
  onChange,
  label,
  groupBy,
  style,
  className,
  message,
  placeholder,
  disableValue,
  error,
  disabled,
  isFull,
  required
}) => {
  data = data || []
  data = data.map(item => {
    return { value: item, label: item }
  })
  const props = {
    options: data || [],
    value: { value: value, label: value },
    isClearable: true,
    onChange
  }
  return (
    <div
      style={{ fontSize: 12 }}
      className={`${
        label ? `column ${isFull && "is-full"}` : isFull ? "column is-full" : ""
        }`}
    >
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required && <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>}
        </p>
      )}
      {disabled ? (
        <small>{disableValue || value}</small>
      ) : (
          <div
            className={`${className || ""}`}
            style={style || { minWidth: 100 }}
          >
            <CustomDropDown {...props} />
            {error && (
              <p
                className="is-small text-error has-text-danger"
                style={{ fontSize: 12 }}
              >
                {error}
              </p>
            )}
          </div>
        )}
    </div>
  )
}

export const DocDropDownSelect = ({
  data = [],
  value,
  onChange,
  label,
  groupBy,
  style,
  className,
  message,
  placeholder,
  disableValue,
  error,
  disabled,
  isFull,
  required
}) => {
  data = data || []
  data = data.map(item => {
    return { value: item.label, label: <span style={{color: item.included ? "grey" : ""}}>{item.label}</span>, disabled: !item.included}
  })
  const props = {
    options: data || [],
    value: { value: value, label: value },
    isClearable: true,
    onChange
  }
  return (
    <div
      style={{ fontSize: 12 }}
      className={`${
        label ? `column ${isFull && "is-full"}` : isFull ? "column is-full" : ""
      }`}
    >
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required && <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span>}
        </p>
      )}
      <div
        className={`${className || ""}`}
        style={style || { minWidth: 100 }}
      >
        <CustomDropDown {...props} />
        {error && (
          <p
            className="is-small text-error has-text-danger"
            style={{ fontSize: 12 }}
          >
            {error}
          </p>
        )}
      </div>
    </div>
  )
}

export const MultiSelect = ({
  filter,
  data = [],
  label,
  value,
  groupBy,
  style,
  className,
  message,
  placeholder,
  disableValue,
  onChange,
  error,
  disabled,
  required,
  isFull
}) => {
  const props = {
    filter: filter || false,
    data: data || [],
    textField: "name",
    value: value || [],
    valueField: "id",
    onChange,
    message,
    groupBy: groupBy || "",
    placeholder: placeholder || "",
    disabled: disabled || false
  }
  style = {
    ...style,
    fontSize: 12
  }
  return (
    <div
      className={`${
        label ? `column ${isFull && "is-full"}` : isFull ? "column is-full" : ""
        }`}
    >
      {label && <p className="multiExpLblBlk">
        {label}
        {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}
      </p>}
      {disabled ? (
        <span>
          {value
            ? value.map((mul, i) => (
              <small key={i}>
                {mul.name}
                {disableValue.length - 1 !== i ? "," : ""}
              </small>
            ))
            : ""}
        </span>
      ) : (
          <div
            className={`${className || ""}`}
            style={style || { minWidth: 100, fontSize: 12 }}
          >
            <Multiselect {...props} />
            {error && (
              <p
                className="is-small text-error has-text-danger"
                style={{ fontSize: 12 }}
              >
                {error}
              </p>
            )}
          </div>
        )}
    </div>
  )
}

export const ZipCodeNumberInput = ({
  type,
  name,
  label,
  value,
  format,
  onChange,
  placeholder,
  error,
  disabled
}) => {
  const props = {
    type,
    name,
    label,
    value,
    format,
    onChange,
    placeholder,
    error,
    disabled
  }
  return (
    <div className={`${label ? "column" : ""}`}>
      {label && <p className="multiExpLblBlk">{label}</p>}
      {disabled ? (
        <small>{value}</small>
      ) : (
          <NumberFormat
            className="input is-fullwidth is-small is-link"
            {...props}
          />
        )}
      {error && <small className="text-error">{error}</small>}
    </div>
  )
}

export const ZipCodeNumberDisableInput = ({
  type,
  name,
  label,
  value,
  format,
  onChange,
  placeholder,
  error,
  disabled
}) => {
  const props = {
    type,
    name,
    label,
    value,
    format,
    onChange,
    placeholder,
    error,
    disabled
  }
  return (
    <div className={`${label ? "column" : ""}`}>
      {label && <p className="multiExpLblBlk">{label}</p>}
      <NumberFormat
        className="input is-fullwidth is-small is-link"
        {...props}
      />
      {error && <small className="text-error">{error}</small>}
    </div>
  )
}

export const AsyncCreatableSelectInput = ({
  multi,
  labelKey,
  groupBy,
  cacheOptions,
  loadOptions = [],
  backspaceRemoves,
  allowCreateWhileLoadingboolean,
  onBlur,
  label,
  value,
  style,
  className,
  disableValue,
  onChange,
  error,
  disabled,
  isFull
}) => {
  const props = {
    multi: multi || false,
    cacheOptions: cacheOptions || false,
    value,
    groupBy: groupBy || "",
    onChange,
    labelKey,
    loadOptions,
    backspaceRemoves,
    allowCreateWhileLoadingboolean: allowCreateWhileLoadingboolean || false,
    disabled: disabled || false,
    onBlur
  }
  style = {
    ...style,
    fontSize: 12
  }
  return (
    <div className={`${label ? "column" : ""} ${className}`}>
      {label && <p className="multiExpLbl">{label}</p>}
      {disabled ? (
        <small className="multiExpTblVal">{disableValue || ""}</small>
      ) : (
          <div
            className={`${className || ""}`}
            style={style || { minWidth: 100, fontSize: 12 }}
          >
            <AsyncCreatableSelect {...props} />
            {error && <p className="text-error">{error}</p>}
          </div>
        )}
    </div>
  )
}
