import React from 'react';
import Select from 'react-select';
import ReactDOM from 'react-dom';
import TetherComponent from './TetherComponent';
import "react-select/dist/react-select.css"

export default class CustomDropDown extends Select {
    constructor(props) {
        super(props);

        this.renderOuter = this._renderOuter;
    }

    componentDidMount() {
        super.componentDidMount.call(this);

        this.dropdownFieldNode = ReactDOM.findDOMNode(this);
    }

    _renderOuter() {
        const menu = super.renderOuter.apply(this, arguments);

        const options = {
            attachment: 'top left',
            targetAttachment: 'bottom left',
            constraints: [
                {
                    to: 'window',
                    attachment: 'together',
                }
            ]
        };

        return (
            <TetherComponent
                target={this.dropdownFieldNode}
                options={options}
                matchWidth
            >
                {/* Apply position:static to our menu so that it's parent will get the correct dimensions and we can tether the parent */}
                {React.cloneElement(menu, { style: { position: 'static', fontSize: 13 } })}
            </TetherComponent>
        )
    }
}