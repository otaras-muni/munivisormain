import React from "react"
import PDF from "react-pdf-js"
import moment from "moment"
import CKEditor from "react-ckeditor-component"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"
import { SelectLabelInput } from "./TextViewBox"
import Loader from "../GlobalComponents/Loader"
import CONST from "../../globalutilities/consts"
import {getFile} from "../../globalutilities/helpers"

export const TranCreateModal = props => {
  const toggleModal = () => {
    props.onModalChange({ modalState: !props.modalState })
  }

  const contractShow = () => {
    props.onModalChange({ contractShow: !props.contractShow })
  }

  const onChange = (e, name) => {
    if (!name && e.target.name === "clientActive") {
      props.tranConfig.clientActive = e.target.value === "clientActive"
      props.tranConfig.svComplianceFlag = !props.tranConfig.clientActive
      props.tranConfig.message =
        e.target.value === "clientActive"
          ? ""
          : !props.svControls.firmHasSupervisor
            ? "There is neither a Supervisory Principal or Compliance Officer designated for your firm. Please contact admin to set up one."
            : ""
    } else if (name && name === "message") {
      props.tranConfig.message = e.editor.getData()
    } else {
      props.tranConfig[e.target.name] =
        e.target.type === "checkbox" ? e.target.checked : e.target.value
    }

    props.onModalChange({
      tranConfig: props.tranConfig
    })
  }

  return (
    <Modal
      closeModal={toggleModal}
      disabled={
        props.contract.length === 0 ? null : !props.contractEnable.pdfLink
      }
      modalState={props.modalState}
      title="Compliance Checks and Email Notification"
      saveModal={(props.expireContract && props.expireContract.length && props.contractEnable && props.contractEnable.expired) ? props.onContractConfirm : props.onSave}
      modalWidth={{ width: "70%", height: "100%" }}
    >
      <div className="columns" style={{ marginTop: 5 }}>
        <div className="tabs">
          <li
            className={!props.contractShow ? "is-active" : ""}
            onClick={contractShow}
          >
           <a className="tabSecLevel" autoFocus tabIndex={0}>
              <span>Contracts</span>
            </a>
          </li>
          <li
            className={props.contractShow ? "is-active" : ""}
            onClick={contractShow}
          >
            <a className="tabSecLevel" tabIndex={0}>
              <span>Email</span>
            </a>
          </li>
        </div>
      </div>

      <div className="columns">
        <div className="column is-two-fifths">
          <span className="is-link" style={{ fontSize: 12 }}>
            <a
              target="_blank"
              href={`/clients-propects/${props.issuerId || ""}/contracts`}
            >
              Add or update client contracts?
            </a>
          </span>
          <i
            className="fa fa-refresh"
            style={{
              fontSize: 12,
              marginTop: -7,
              cursor: "pointer",
              padding: "0 0 0 10px"
            }}
            onClick={props.onContractRefresh}
          />
        </div>
        <div className="column" />
      </div>

      <div className="columns">
        <div
          className="column"
          style={{ display: `${!props.contractShow ? "block" : "none"}` }}
        >
          {props.contract.length > 0 ? (
            props.contract.map((contract, index) => (
              <label className="radio-button" key={index.toString()}>
                <span style={{fontSize: 18}} className={contract && contract.expired ? "has-text-danger" : ""} title={contract && contract.expired ? "Contract Expired": ""}>{contract.contractRef.contractName} </span>
                <span style={{fontSize: 13}} className={contract && contract.expired ? "has-text-danger" : ""} title={contract && contract.expired ? "Contract Expired": ""}>
                  <b>(Start Date:</b> {moment(contract.contractRef.startDate).format("MM-DD-YYYY")},&nbsp;
                  <b>End Date:</b> {moment(contract.contractRef.endDate).format("MM-DD-YYYY")})
                </span>
                {!(contract && contract.documents && contract.documents.length) ? <span className="text-error">Document not found</span> : null }
                <input
                  type="radio"
                  value={contract.contractRef.contractId}
                  checked={
                    props.contractEnable &&
                    props.contractEnable.contractRef &&
                    props.contractEnable.contractRef.contractId ===
                    contract.contractRef.contractId
                  }
                  onChange={e => props.handleCheckClick(e, contract)}
                />
                <span className="checkmark" />
              </label>
            ))
          ) : (
            <h5>No Contract Available</h5>
          )}

          { props.contractEnable && props.contractEnable.fileType === "pdf" ?
            <div className="columns is-centered">
              <div className="column" style={{fontSize: 12, textAlign: "center"}}>
                Note: Please use the page button to go over the contract and scroll to the end of last page to confirm review and scope of services.
              </div>
            </div> : null
          }

          <div className="columns is-centered">
            {props.contractEnable && props.contractEnable.fileType === "pdf" ? (
              <div>
                {props.pages ? (
                  <div className="column">
                    <div className="field">
                      <div className="control">
                        <span>
                          <button
                            className="button"
                            style={{ marginTop: -5 }}
                            onClick={() => props.onNumberChange(1, "button")}
                            disabled={props.page === 1}
                          >
                            First Page
                          </button>
                          &nbsp;&nbsp;&nbsp; Page{" "}
                          <input
                            style={{
                              width: 70,
                              marginTop: -5,
                              textAlign: "center"
                            }}
                            className="input"
                            name="number"
                            type="number"
                            min={1}
                            max={props.pages}
                            value={props.number}
                            onChange={e => props.onNumberChange(e)}
                          />{" "}
                          of {props.pages}&nbsp;&nbsp;&nbsp;
                          <button
                            className="button"
                            style={{ marginTop: -5 }}
                            onClick={() =>
                              props.onNumberChange(props.pages, "button")
                            }
                            disabled={props.pages === props.page}
                          >
                            Last Page
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>

          {props.contractEnable.fileType === "pdf" ? (
            props.pages ? null : (
              <Loader />
            )
          ) : null}

          <div className="columns">
            {props.contractEnable ? (
              <div className="column" style={{ textAlign: "center" }}>
                {props.contractEnable.fileType === "pdf" ? (
                  <div className="box">
                    {props.contract.length > 0 &&
                    props.contractEnable &&
                    props.contractEnable.pdfLink ? (
                        <PDF
                          file={`${
                            process.env.API_URL
                          }/api/s3/get-s3-object-as-stream?objectName=${
                            encodeURIComponent(props.contractEnable.pdfLink)
                          }`}
                          onDocumentComplete={props.onDocumentComplete}
                          page={props.page}
                        />
                      ) : null}
                  </div>
                ) : (
                  <div className="multiExpTblVal">
                    {props.contractEnable.fileName ? (
                      <a
                        onClick={() => getFile(`?objectName=${props.contractEnable.pdfLink}`, props.contractEnable.fileName)}
                        download={props.contractEnable.fileName}
                        style={{ fontSize: 22 }}
                      >
                        Click Here To Download {props.contractEnable.fileName}
                      </a>
                    ) : null}
                    {!props.contractEnable.fileName ? (
                      <span>
                        <Loader />
                      </span>
                    ) : null}
                  </div>
                )}
              </div>
            ) : null}
          </div>
        </div>
      </div>

      <div className="columns is-centered">
        {!props.contractShow &&
        props.contractEnable &&
        props.contractEnable.fileType === "pdf" ? (
            <div>
              {props.pages ? (
                <div className="column">
                  <div className="field">
                    <div className="control">
                      <span>
                        <button
                          className="button"
                          style={{ marginTop: -5 }}
                          onClick={() => props.onNumberChange(1, "button")}
                          disabled={props.page === 1}
                        >
                          First Page
                        </button>
                        &nbsp;&nbsp;&nbsp; Page{" "}
                        <input
                          style={{
                            width: 70,
                            marginTop: -5,
                            textAlign: "center"
                          }}
                          className="input"
                          name="number"
                          type="number"
                          min={1}
                          max={props.pages}
                          value={props.number}
                          onChange={e => props.onNumberChange(e)}
                        />{" "}
                        of {props.pages}&nbsp;&nbsp;&nbsp;
                        <button
                          className="button"
                          style={{ marginTop: -5 }}
                          onClick={() =>
                            props.onNumberChange(props.pages, "button")
                          }
                          disabled={props.pages === props.page}
                        >
                          Last Page
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          ) : null}
      </div>

      { !props.contractShow && props.contractEnable && (props.contractEnable.pdfLink && !props.contractEnable.expired) && props.pages === props.page ? (
        <div>
          <div className="columns">
            <div className="column">
              <label className="checkbox-button">
                Reviewed
                <input
                  type="checkbox"
                  name="reviewed"
                  value={
                    props.contractEnable.contractRef &&
                    props.contractEnable.contractRef.contractId
                  }
                  checked={props.contractEnable.reviewed}
                  onChange={() => {}}
                  onClick={e => props.onReviewed(e, "review")}
                />
                <span className="checkmark" />
              </label>
            </div>
          </div>
          {props.contractEnable.fileType !== "pdf" ? (
            props.contractEnable.reviewed ? (
              <div className="columns">
                <div className="column">
                  <label className="radio-button">
                    Yes - The transaction is covered under the scope of
                    services.
                    <input
                      type="radio"
                      name="agree"
                      value="yes"
                      checked={props.contractEnable.agree === "yes"}
                      onChange={() => props.onReviewed("yes")}
                    />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="column">
                  <label className="radio-button">
                    No - The transaction is not covered under the scope of
                    services. I still want to create transaction and let the
                    compliance officer resolve contract details.
                    <input
                      type="radio"
                      name="agree"
                      value="no"
                      checked={props.contractEnable.agree === "no"}
                      onChange={() => props.onReviewed("no")}
                    />
                    <span className="checkmark" />
                  </label>
                </div>
              </div>
            ) : null
          ) : (
            <div className="columns">
              {props.pages ? (
                <div className="column">
                  <label className="radio-button">
                    Yes - The transaction is covered under the scope of
                    services.
                    <input
                      type="radio"
                      name="agree"
                      value="yes"
                      checked={props.contractEnable.agree === "yes"}
                      onChange={() => props.onReviewed("yes")}
                    />
                    <span className="checkmark" />
                  </label>
                </div>
              ) : null}
              {props.pages ? (
                <div className="column">
                  <label className="radio-button">
                    No - The transaction is not covered under the scope of
                    services. I still want to create transaction and let the
                    compliance officer resolve contract details.
                    <input
                      type="radio"
                      name="agree"
                      value="no"
                      checked={props.contractEnable.agree === "no"}
                      onChange={() => props.onReviewed("no")}
                    />
                    <span className="checkmark" />
                  </label>
                </div>
              ) : null}
            </div>
          )}
        </div>
      ) : null}

      {props.contractShow ? (
        <div className="columns">
          <div className="column">
            <label className="checkbox-button">
              {" "}
              You want to send email?
              <input
                type="checkbox"
                name="sendEmail"
                value="none"
                checked={props.tranConfig.sendEmail || false}
                onClick={onChange}
                onChange={onChange}
              />
              <span className="checkmark" />
            </label>
          </div>
        </div>
      ) : null}

      {props.contractShow ? (
        <div className="columns">
          <SelectLabelInput
            label="Choose a default transaction status "
            disableValue={
              props.opportunity ? CONST.tranStatus : CONST.opportunityStatus
            }
            error={!props.tranConfig.defaultStatus ? "Required" : ""}
            list={props.tranStatus || []}
            name="defaultStatus"
            value={props.tranConfig.defaultStatus === "" ? props.opportunity ? "In Progress" : "Pre-Pricing" : props.tranConfig.defaultStatus}
            // value={props.opportunity ? "In Progress" : "Pre-Pricing"}
            onChange={onChange}
          />
        </div>
      ) : null}

      {props.contractShow ? (
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Message</p>
            <div className="control">
              <CKEditor
                activeClass="p10"
                content={props.tranConfig.message}
                events={{
                  /* "blur": this.onBlur,
                "afterPaste": this.afterPaste, */
                  change: e => onChange(e, "message")
                }}
              />
              {/* <textarea className="textarea" onChange={onChange} value={props.tranConfig.message} name="message" placeholder="" /> */}
            </div>
          </div>
        </div>
      ) : null}
      <div className="columns">
        <div className="column">
          {!props.svControls.firmHasSupervisor ? (
            <p className="text-error">
              There is neither a Supervisory Principal or Compliance Officer
              designated for your firm. Please contact admin to set up one.
            </p>
          ) : null}
        </div>
      </div>
    </Modal>
  )
}
export default TranCreateModal
