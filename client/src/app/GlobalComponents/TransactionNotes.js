import React from "react"
import connect from "react-redux/es/connect/connect"
import RatingSection from "./RatingSection"
import NotesReactTable from "./NotesReactTable"
import Accordion from "./Accordion"
import Loader from "./Loader"

class TransactionNotes extends React.Component {
  constructor() {
    super()
    this.state = {
      notes: [],
      noteObject: {},
      active: [],
      note: "",
      loading: true
    }
  }

  componentWillMount(){
    const { notesList } = this.props
    this.setState({
      notes: notesList || [],
      loading: false
    })
  }

  onSave = () => {
    const { note, noteObject } = this.state
    const { user } = this.props
    let payload = {}
    if(note){
      if(noteObject && noteObject._id){
        payload = {
          _id: noteObject && noteObject._id || "",
          note,
          createdAt: noteObject && noteObject.createdAt || new Date(),
          updatedAt: new Date(),
          updatedByName: `${user && user.userFirstName || ""} ${user && user.userLastName || ""}`,
          updatedById: user && user.userId || "" ,
        }
      } else {
        payload = {
          note,
          createdAt: new Date(),
          updatedAt: new Date(),
          updatedByName: `${user && user.userFirstName || ""} ${user && user.userLastName || ""}`,
          updatedById: user && user.userId || "" ,
        }
      }
    }
    this.setState({
      loading: true
    }, () => {
      this.props.onSave(payload, (res) => {
        this.setState({
          notes: res && res.notesList || [],
          noteObject: {},
          note: "",
          active: [0],
          loading: false
        })
      })
    })
  }

  onEditNotes = (note) => {
    this.setState({
      noteObject: note,
      note: note && note.note || ""
    })
  }

  onCancel = () => {
    this.setState({
      note: "",
      noteObject: {}
    })
  }

  onNotesChange = (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  render(){
    const { notes, note, active } = this.state
    const { canEditTran } = this.props

    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return(
      <Accordion multiple
        activeItem={active || []}
        boxHidden
        render={({activeAccordions, onAccordion}) =>
          <div>
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title="Notes"
            >
              {activeAccordions.includes(0) && (
                <div>
                  <NotesReactTable
                    notes={notes}
                    onEditNotes={this.onEditNotes}
                    canEdit={!canEditTran || false}
                  />
                  <div className="columns">
                    <div className="column is-full">
                      <p className="multiExpLbl">Notes</p>
                      <div className="control">
                        <textarea className="textarea" disabled={!canEditTran || false} name="note" value={note || ""} onChange={(e) => this.onNotesChange(e)}/>
                      </div>
                    </div>
                  </div>

                  <div className="column is-full" >
                    { canEditTran ?
                      <div className="field is-grouped-center">
                        <div className="control">
                          <button
                            className="button is-link"
                            onClick={this.onSave}
                            disabled={!note}
                          >
                            Save Note
                          </button>
                        </div>
                        <div className="control">
                          <button
                            className="button is-light"
                            onClick={this.onCancel}
                          >
                            Cancel
                          </button>
                        </div>
                      </div> : null
                    }
                  </div>

                </div>
              )}
            </RatingSection>
          </div>
        }
      />
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})
export default connect( mapStateToProps, null)(TransactionNotes)
