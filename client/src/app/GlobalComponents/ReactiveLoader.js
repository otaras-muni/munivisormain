import React from "react"
import loader from "../../images/loader2 .gif"

const Loader = () => (
  <div
    style={{
      position: "absolute",
      textAlign: "center",
      top: 100,
      left: 400,
      right: 0,
      bottom: 0,
      zIndex: 1001,
      height: "100vh",
      background: "rgba(255,255,255,0.5)",
      borderRadius: 3
    }}
  >
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        width: "100%",
        height: "100%",
        alignItems: "center"
      }}
    >
      <img src={loader} height={100} width={100} alt="loader" />
    </div>
  </div>
)

export default Loader
