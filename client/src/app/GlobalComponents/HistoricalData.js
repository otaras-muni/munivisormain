import React from "react"
import Accordion from "./Accordion"
import RatingSection from "./RatingSection"

const HistoricalData = ({data}) => {
  if(!data || (data && !Object.keys(data).length)){
    return null
  }
  return (
    <Accordion multiple activeItem={[]} boxHidden render={({activeAccordions, onAccordion}) =>
      <RatingSection onAccordion={() => onAccordion(0)} title="Historical Data">
        {activeAccordions.includes(0) &&
        <div>
          <div className="migtools" style={{marginTop: 0}}>
            {
              Object.keys(data).map((key, index) => {
                if (data[key]) {
                  return (
                    <div className="mapping-input" key={index.toString()}>
                      <div className="multiExpLblBlk">{key}</div>
                      <div className="is-small  is-fullwidth is-link">{data[key] || ""}</div>
                    </div>
                  )
                }
                return null
              })
            }
          </div>
        </div>
        }
      </RatingSection>
    }/>
  )
}
export default HistoricalData
