import React from "react"
import CONST from "../../globalutilities/consts"

const TranSummaryStatus = ({
  tranStatus,
  selectedStatus,
  transaction,
  onChange,
  disabled,
  pdfDownload
}) => {
  const tranStatusProps = {
    title: "Transaction status",
    name: "actTranStatus",
    value: selectedStatus || "",
    disabled
  }
  if (onChange) {
    tranStatusProps.onChange = onChange
  } else {
    tranStatusProps.readOnly = true
  }

  tranStatus = transaction.opportunity && transaction.opportunity.status ?
    tranStatus.filter(val => CONST.opportunityStatus.indexOf(val && val.label) !== -1) :
    tranStatus.filter(val => CONST.opportunityStatus.indexOf(val && val.label) === -1)

  return (
    <div className="columns">
      <div className="column ">
        <span>Transaction status: &nbsp;&nbsp;</span>
        {disabled ? (
          <small>{selectedStatus}</small>
        ) : (
          <div className="select is-small is-link">
            <select {...tranStatusProps} disabled={disabled}>
              <option value="">Pick</option>
              {tranStatus.map((status, i) => {
                const disabled =
                  transaction.opportunity && transaction.opportunity.status
                    ? CONST.tranStatus.indexOf(status && status.label) !== -1
                    : CONST.opportunityStatus.indexOf(status && status.label) !== -1
                return (
                  <option key={i} disabled={disabled || !status.included}>
                    {status && status.label}
                  </option>
                )
              })}
            </select>
          </div>
        )}
      </div>
      {transaction &&
      transaction.opportunity &&
      transaction.opportunity.status ? (
          <div className="column">
            <span>Opportunity: &nbsp;&nbsp;</span>
            <span>
              <label className="switch">
                <input autoFocus 
                  type="checkbox"
                  name="opportunity"
                  checked={
                    (transaction && transaction.opportunity) ||
                    transaction.opportunity.status ||
                    false
                  }
                  disabled
                />
                <span className="slider round" />
              </label>
            </span>
          </div>
        ) : null}
      <div className="column is-hidden-touch">
        <div className="field is-grouped">
          <div className="control" onClick={pdfDownload}>
            <span className="has-text-link">
              <i className="far fa-2x fa-file-pdf has-text-link" title="PDF Download"/>
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}
export default TranSummaryStatus
