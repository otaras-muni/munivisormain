import React from "react"
import { Multiselect } from "react-widgets"
import CKEditor from "react-ckeditor-component"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"
import {MultiSelect, TextLabelInput} from "./TextViewBox"

export const SelectWinnerEmailSendModal  = (props) => {
  const toggleModal = () => {
    props.onModalChange({ modalState: !props.modalState })
  }

  const onChange = (e) => {
    let email = {}
    if(e.target.name === "category"){
      email = {
        ...props.email,
        sendEmailTo: [],
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }else {
      email = {
        ...props.email,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }
    props.onModalChange({
      email
    })
  }

  const onEditorChange = (e, name) => {
    props.onModalChange( props.onModalChange({
      [name]: e.editor.getData()
    }, name))
  }

  const onSelect = (items) => {
    props.onModalChange({
      email: {
        ...props.email,
        sendEmailTo: items
      }
    })
  }

  // const isTransaction = ["deals", "rfp", "loan", "marfp", "derivative", "others"].indexOf(props.nav1) !== -1

  return(
    <Modal
      closeModal={toggleModal}
      modalState={props.modalState}
      title="Compliance Checks and Email Notification"
      saveModal={props.onSave}
    >
      <div className="columns multi-select">
        <MultiSelect
          filter label="Send email to"
          data={props.participants}
          value={props.supplierParticipants || []}
          disableValue={props.supplierParticipants || []}
          disabled
          onChange={(items) => onSelect(items)}
          style={{width: "100%"}}
          />
      </div>
      <div className="columns">
        <TextLabelInput label="Subject" name="subject" type="text" value={props.email.subject || ""} onChange={onChange}/>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Message</p>
          <div className="control">
            <CKEditor
              activeClass="p10"
              content={props.email.message}
              events={{
                /* "blur": this.onBlur,
                "afterPaste": this.afterPaste, */
                "change": (e) => onEditorChange(e, "message")
              }}
            />
          </div>
        </div>
        {/* <TextLabelInput label="Message" name="message" type="text" value={props.email.message || ""} onChange={onChange}/> */}
      </div>
    </Modal>
  )
}
export default SelectWinnerEmailSendModal
