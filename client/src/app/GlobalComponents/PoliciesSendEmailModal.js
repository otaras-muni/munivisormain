import React from "react"
import { Multiselect } from "react-widgets"
import CKEditor from "react-ckeditor-component"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"
import { MultiSelect, TextLabelInput } from "./TextViewBox"

export const PoliciesSendEmailModal = props => {
  const toggleModal = () => {
    props.onModalChange({ modalState: !props.modalState })
  }

  const onChange = e => {
    let email = {}
    if (e.target.name === "category") {
      email = {
        ...props.email,
        sendEmailTo: [],
        [e.target.name]: e.target.value
      }
    } else {
      email = {
        ...props.email,
        [e.target.name]: e.target.value
      }
    }
    props.onModalChange({
      email
    })
  }

  return (
    <Modal
      closeModal={toggleModal}
      modalState={props.modalState}
      title="Compliance Checks and Email Notification"
      saveModal={props.onSave}
    >
      <div className="columns">
        <div className="column">
          <div className="control">
            <label className="radio-button">
              Send emails ?
              <input
                type="radio"
                name="category"
                value="custom"
                checked={props.email.category === "custom" || false}
                onClick={onChange}
                onChange={onChange}
              />
              <span className="checkmark" />
            </label>
            <label className="radio-button">
              do not send emails ?
              <input
                type="radio"
                name="category"
                value="none"
                checked={props.email.category === "none" || false}
                onClick={onChange}
                onChange={onChange}
              />
              <span className="checkmark" />
            </label>
          </div>
        </div>
      </div>
    </Modal>
  )
}
export default PoliciesSendEmailModal
