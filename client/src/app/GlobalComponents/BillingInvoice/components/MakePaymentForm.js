import React, {Component} from "react"
import swal from "sweetalert"
import {toast} from "react-toastify"
import {
  CardElement,
  StripeProvider,
  Elements,
  injectStripe
} from "react-stripe-elements"
import "./CheckoutForm.scss"
import { makePayment } from "GlobalUtils/helpers"
import CONST, { STRIPE_CLIENT_PUBLIC_KEY } from "../../../../globalutilities/consts"
import Loader from "../../Loader"


const createOptions = (fontSize, padding) => ({
  style: {
    base: {
      fontSize,
      color: "#32325D",
      fontWeight: 500,
      fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
      fontSmoothing: "antialiased",
      "::placeholder": {
        color: "#CFD7DF"
      },
      padding
    },
    invalid: {
      color: "#E25950"
    }
  }
})

const inputStyle = (fontSize) => ({
  fontSize,
  color: "#32325D",
  fontWeight: 500,
  fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
  fontSmoothing: "antialiased",
  width: "100%",
  "::placeholder": {
    color: "#CFD7DF",
    opacity: "0.6"
  }
})

class _CardForm extends React.Component {
  constructor() {
    super()
    this.state = {
      error: "",
      confirmAlert: CONST.confirmAlert,
      loading: false
    }
  }

  handleError = (error) => {
    this.setState({error, loading: false})
  }

  handleSubmit = (ev) => {
    ev.preventDefault()
    const  { name, email, amount } = ev.target
    const  { confirmAlert } = this.state
    const  { invoice } = this.props
    const { tenantId, addresses, _id, invoiceId, appId, payableAmount } = invoice
    const address = addresses.find(a => a.isPrimary)
    this.setState({
      loading: true
    }, () => {
      if (this.props.stripe) {
        this.props.stripe.createToken().then(async (payload) => {
          console.log("[token]", payload)
          if(payload && payload.error) {
            this.handleError(payload.error.message || "Something went wrong!")
            return toast.error(payload.error.message || "Something went wrong!", {
              position: toast.POSITION.TOP_RIGHT
            })
          }
          confirmAlert.text = "You Make This Payment?"
          swal(confirmAlert).then(willDelete => {
            if (willDelete) {

              const pay = {
                metadata: {
                  name: name.value,
                  email: email.value,
                },
                receipt_email: email.value,
                // eslint-disable-next-line radix
                amount: parseInt(parseFloat(payableAmount || 0).toFixed(2) * 100),
                address,
                tenantId,
                appId,
                invoiceId: _id,
                invoiceNumber: invoiceId,
                ...payload
              }

              makePayment(pay, response => {
                if (response && response.data && response.data.done){

                  this.setState({error: "", loading: false})
                  toast.success("Payment successfully.", {
                    position: toast.POSITION.TOP_RIGHT
                  })
                  this.props.onMakePaymentSuccess(response.data)

                } else {

                  this.handleError((response && response.data && response.data.message) || "Something went wrong!")
                  toast.error((response && response.data && response.data.message) || "Something went wrong!", {
                    position: toast.POSITION.TOP_RIGHT
                  })

                }
              })

            } else {
              this.setState({error: "", loading: false})
            }
          })

        })
      } else {
        console.log("Stripe.js hasn't loaded yet.")
      }
    })
  }

  handleBlur = () => {
    console.log("[blur]")
  }

  handleChange = (change) => {
    this.setState({
      error: ""
    })
  }

  handleClick = () => {
    console.log("[click]")
  }

  handleFocus = () => {
    console.log("[focus]")
  }

  handleReady = () => {
    console.log("[ready]")
  }

  render() {
    const { error, loading } = this.state
    const { invoice } = this.props
    return (
      <form onSubmit={this.handleSubmit}>
        { loading ? <Loader/> : null }

        <div className="columns">
          <div className="column">
            <label>Name
              <input
                name="name"
                type="text"
                placeholder="Jane Doe"
                style={inputStyle(this.props.fontSize)}
                required
              />
            </label>
          </div>
          <div className="column">
            <label>Email
              <input
                name="email"
                type="email"
                placeholder="jane.doe@example.com"
                style={inputStyle(this.props.fontSize)}
                required
              />
            </label>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <label>Amount
              <p style={inputStyle(this.props.fontSize)}>{`$${invoice && parseFloat(invoice.payableAmount).toFixed(2)}` || "$0"}</p>
            </label>
          </div>
        </div>

        <label>Card details
          <CardElement
            onBlur={this.handleBlur}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onReady={this.handleReady}
            {...createOptions(this.props.fontSize)}
          />
        </label>
        {error ? <p className="error message">{error}</p> : null}
        <button>Make payment</button>
      </form>
    )
  }
}
const CardForm = injectStripe(_CardForm)

class CheckoutFormContainer extends Component {
  constructor() {
    super()
    this.state = {
      elementFontSize: window.innerWidth < 450 ? "14px" : "16px",
      loading: false
    }
    window.addEventListener("resize", () => {
      if (window.innerWidth < 450 && this.state.elementFontSize !== "14px") {
        this.setState({elementFontSize: "14px"})
      } else if (
        window.innerWidth >= 450 &&
        this.state.elementFontSize !== "16px"
      ) {
        this.setState({elementFontSize: "16px"})
      }
    })
  }

  render() {
    const {elementFontSize} = this.state
    const {invoice} = this.props

    return (

      <StripeProvider apiKey={STRIPE_CLIENT_PUBLIC_KEY}>
        <div className="example payment">
          <Elements>
            <div>
              <div className="column" />
              <div className="column" style={{backgroundColor: "#f6f9fc", border: "2px solid #9dbbf9", padding: 15, borderRadius: 10}}>
                <CardForm fontSize={elementFontSize} invoice={invoice} onMakePaymentSuccess={this.props.onMakePaymentSuccess}/>
              </div>
              <div className="column text-center"/>
            </div>
          </Elements>
          <br/>
        </div>
      </StripeProvider>

    )
  }
}

export default CheckoutFormContainer
