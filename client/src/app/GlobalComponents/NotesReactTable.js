import React from "react"
import moment from "moment"
import ReactTable from "react-table"
import "react-table/react-table.css"


const NotesReactTable = ({
  notes,
  onEditNotes,
  canEdit
}) => {
  const columns = [
    {
      id: "note",
      Header: "Note",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.note || "--" }
          </div>
        )
      },
      sortMethod: (a, b) => a.note.localeCompare(b.note)
    },
    {
      id: "updatedAt",
      Header: "Last Updated Date",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.updatedAt ? moment(item.updatedAt).format("MM-DD-YYYY hh:mm A") : "" }
          </div>
        )
      },
      sortMethod: (a, b) => a.updatedAt.localeCompare(b.updatedAt)
    },
    {
      id: "updatedByName",
      Header: "Last Updated By",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.updatedByName || "" }
          </div>
        )
      },
      sortMethod: (a, b) => a.updatedByName - b.updatedByName
    },
    {
      id: "edit",
      Header: "Action",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <a className={!canEdit ? "fas fa-pencil-alt" : "fas fa-eye"}  title="Edit" onClick={() => onEditNotes(item)}/>
          </div>
        )
      }
    }
  ]

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={notes}
        showPaginationBottom
        defaultPageSize={5}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default NotesReactTable
