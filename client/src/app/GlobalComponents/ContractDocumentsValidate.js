import Joi from "joi-browser"


const docListSchema = Joi.object().keys({
  docCategory: Joi.string().label("Category").required(),
  docSubCategory: Joi.string().allow("").optional(),
  docType: Joi.string().label("Type").required(),
  docAWSFileLocation: Joi.string().label("File Name").required(),
  docFileName: Joi.string().allow("").label("File Name").optional(),
  createdBy: Joi.string().required(),
  createdUserName: Joi.string().required(),
  uploadedDate: Joi.date().example(new Date("2016-01-01")).optional(),
  _id: Joi.string().allow("").optional(),
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  documents: Joi.array().min(1).items(docListSchema).required(),
})

export const ContractDocumentsValidate = (inputTransDetail) => Joi.validate(inputTransDetail, DocDetailsDealsSchema, { abortEarly: false, stripUnknown:false })
