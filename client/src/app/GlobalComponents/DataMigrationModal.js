import React from "react"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"

const DataMigrationModal  = (props) => (
  <Modal
    closeModal={props.toggleModal}
    modalState={props.modalState}
    title="Data Migration Errors"
  >
    <div className="columns">
      <div className="column">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>Column Name</th>
              <th>Number of errors</th>
            </tr>
          </thead>
          <tbody>
            {
              Object.keys(props.data).map((key, index) => {
                if (props.data[key]) {
                  return (
                    <tr key={index.toString()}>
                      <td>
                        <span>
                          {key}
                        </span>
                      </td>
                      <td>
                        <span>
                          {props.data[key] || ""}
                        </span>
                      </td>
                    </tr>
                  )
                }
              })
            }
          </tbody>
        </table>
        {/* {
          Object.keys(props.data).map((key, index) => {
            if (props.data[key]) {
              return (
                <div className="mapping-input" key={index.toString()}>
                  <div className="multiExpLblBlk">{key} Related Errors </div>
                  <div className="is-small  is-fullwidth is-link">{props.data[key] || ""}</div>
                </div>
              )
            }
            return null
          })
        } */}
      </div>
    </div>
  </Modal>
)
export default DataMigrationModal
