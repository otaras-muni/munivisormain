import React, { Component } from "react"
import moment from "moment"
import { connect } from "react-redux"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import CONST from "GlobalUtils/consts"
import { updateS3DocTags } from "GlobalUtils/helpers"
import RatingSection from "./RatingSection"
import Accordion from "./Accordion"
import TableHeader from "./TableHeader"
import ScheduleOfFees from "../FunctionalComponents/Billing/BillingExpenseRevenue/component/ScheduleOfFees"
import TransactionalFees from "../FunctionalComponents/Billing/BillingExpenseRevenue/component/TransactionalFees"
import RetainerAndEscalator from "../FunctionalComponents/Billing/BillingExpenseRevenue/component/RetainerAndEscalator"
import withAuditLogs from "./withAuditLogs"
import {
  DropDownInput,
  NumberInput,
  SelectLabelInput,
  TextLabelInput
} from "./TextViewBox"
import Loader from "./Loader"
import { ScheduleTranValidate } from "./ScheduleTranValidate"
import { ContextType } from "../../globalutilities/consts"
import SingleContractDocuments from "./SingleContractDocuments"
import FeesPar from "../FunctionalComponents/Billing/BillingExpenseRevenue/component/FeesPar"

// Save Modal Definition
export const SaveModal = ({ message, closeModal, savemodalState, onConfirmed }) => {
  if (!savemodalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" autoFocus onClick={onConfirmed} >Yes</button>
            <button className="button" onClick={closeModal} >No</button>
          </div>
        </footer>
      </div>
    </div>
  )
}


const cols = [
  [
    { name: "Description<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Amount/Slab<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Charge<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Delete" }
  ],
  [
    { name: "Role <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Standard Hourly Rate <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Quoted Hourly Rate <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Delete" }
  ],
  [
    { name: "Type <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>"},
    { name: "Amount <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Effective Start Date <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Effective End Date <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Delete" }
  ],
  [
    { name: "Description" },
    { name: "Amount/Slab <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Charge <span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" }
  ]
]

class ScheduleTransactionAndRetainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
      sof: [cloneDeep(CONST.FAServiceFee.sof)],
      nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      documents: [],
      newDocuments: [],
      tempSof: [cloneDeep(CONST.FAServiceFee.sof)],
      tempNonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      tempRetAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      tempDocuments: [],
      notes: "",
      docSelect: "",
      activeItem: [],
      dropDown: {},
      errorMessages: {},
      savemodalState: false,
      loading: true,
      digitize: false,
      digitizeCheck: true,
      chooseMultiple: false,
      dateOverlap: ""
    }
  }

  async componentDidMount() {
    this.setState({
      dropDown: {
        ...this.props.dropDown
      },
      ...this.props.state,
      loading: false
    })
  }

  componentWillReceiveProps(nextProps) {
    let { sof } = this.state
    let next = {}
    let contract = {}
    if(nextProps && nextProps.states){
      next = nextProps.states
      contract = nextProps.states.contractRef
    }
    if (nextProps && next && next.contractRef && contract.contractName || nextProps && next && next.contractRef && contract.contractType || nextProps && next &&
      next.contractRef && contract.startDate || nextProps && next && next.contractRef && contract.endDate || nextProps && next && next.newDocuments && next.newDocuments[0].docCategory) {
      this.setState({
        ...nextProps.states
      })
    } else {
      this.setState({
        dropDown: {
          ...nextProps.dropDown
        },
        ...nextProps.state,
        sof: [cloneDeep(CONST.FAServiceFee.sof)],
        digitizeCheck: !(
          nextProps.state &&
          nextProps.state.documents &&
          nextProps.state.documents.length
        ),
        newDocuments: [],
        loading: false
      },() => {
        if (nextProps.state.sof.length > 0) {
          sof = nextProps.state.sof.map(sofs => ({
            ...sofs,
            fees: sofs.feesType === "slab" ? sofs.fees : cloneDeep(CONST.FAServiceFee.sof.fees),
            feePar: sofs.feesType === "feePar" ? sofs.fees : cloneDeep(CONST.FAServiceFee.sof.feePar)
          }))
          this.setState({ sof })
        }
      },() => {
        if (nextProps.state && nextProps.state.contractRef && nextProps.state.contractRef.contractName === "") {
          this.setState({
            digitize: false
          })
        }
      })
    }
  }

  toggleSaveModal = () => {
    this.setState(prev => {
      const newState = !prev.savemodalState

      return { savemodalState: newState }
    })
  }

  showModal = () => {
  this.setState({
    savemodalState: true,
    onConfirmed: this.onSave,
    modalMessage: "Are you sure you want to save the transaction details?"
  })
}

  actionButtons = (key, isDisabled, data) => {
    return (
      //eslint-disable-line
      <div className="field is-grouped">
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onAdd(key, data)}
            disabled={isDisabled || false}
          >
            Add
          </button>
        </div>
        <div className="control">
          <button
            className="button is-link is-small"
            onClick={() => this.onReset(key)}
            disabled={isDisabled || false}
          >
            Reset
          </button>
        </div>
      </div>
    )
  }

  onAdd = (key, data) => {
    const newItems = this.state[key]
    newItems.push(cloneDeep(CONST.FAServiceFee[key]))
    this.setState({
      [key]: newItems,
      activeItem: [data]
    },() => {
      if (key === "sof") {
        const { sof } = this.state
        const index = sof.length - 1
        sof[index].feesType = "feePar"
        this.setState({ sof })
      }
    })
  }

  onReset = key => {
    const tempKey = key === "sof" ? "tempSof" : key === "nonTranFees" ? "tempNonTranFees" : key === "retAndEsc" ? "tempRetAndEsc" : ""
    const resetItems = this.state[tempKey]
    const resetData = this.state[key].filter(i => i._id)
    const blankResetData = resetItems.slice(resetItems.length-1, resetItems.length)
    this.setState({
      [key]: resetData.length ? resetData : blankResetData,
    })
  }

  onBlur = (event, change) => {
    const { user } = this.props
    const { contractName } = this.state.contractRef
    this.props.onBlur(event.target.value, change, contractName, this.state)
  }

  onContracrRefChange = event => {
    const { name, value } = event.target
    this.setState(prevState => ({
      contractRef: {
        ...prevState.contractRef,
        [name]: value
      }
    }))
  }

  onSofChanges = (event, sIndex) => {
    const sof = cloneDeep(this.state.sof)
    const { name, value } = event.target
    sof[sIndex][name] = value
    if(name === "securityType"){
      sof[sIndex].sofDealType = ""
    }
    this.setState({ sof })
  }

  onFeesChange = (item, key, sIndex, fIndex) => {
    const sof = cloneDeep(this.state.sof)
    if (key !== "feePar") {
      sof[sIndex].fees[fIndex] = item
    } else {
      item.sofDesc = "Fee Per"
      sof[sIndex].feePar[fIndex] = item
    }
    this.setState({ sof }, () => this.autoCountAmountOver())
  }

  onFeeRemove = (sIndex, fIndex) => {
    const { sof } = this.state
    sof[sIndex].fees.splice(fIndex, 1)
    this.setState({ sof }, () => this.autoCountAmountOver())
  }

  autoCountAmountOver = () => {
    const { sof } = this.state
    const amtOverIndex = sof.forEach((schedule, sIndex) => {
      sof[sIndex].fees.findIndex(fee => fee.sofDesc === "Amount Over")
      if (amtOverIndex !== -1) {
        let totalCost = 0
        // let totalCharge = 0
        const amtOver = sof[sIndex].fees.find(
          fee => fee.sofDesc === "Amount Over"
        )
        sof[sIndex].fees.forEach(fee => {
          if (fee.sofDesc !== "Amount Over") {
            console.log(fee.sofAmt, fee.sofCharge)
            totalCost += parseInt(fee.sofAmt || 0, 10)
            // totalCharge += parseInt(fee.sofCharge || 0, 10)
          }
        })
        if(amtOver){
        amtOver.sofAmt = totalCost
        }
        // amtOver.sofCharge = totalCharge
        sof[sIndex].fees[amtOverIndex] = amtOver
      }
    })
    this.setState({ sof })
  }

  onCancel = () => {
    this.setState({
      contractRef: cloneDeep(CONST.FAServiceFee.contractRef),
      sof: [cloneDeep(CONST.FAServiceFee.sof)],
      nonTranFees: [cloneDeep(CONST.FAServiceFee.nonTranFees)],
      retAndEsc: [cloneDeep(CONST.FAServiceFee.retAndEsc)],
      notes: "",
      newDocuments: []
    })
  }

  onSave = async () => {
    const {
      contractRef,
      nonTranFees,
      retAndEsc,
      digitize,
      documents,
      newDocuments,
      notes
    } = this.state
    const { user } = this.props
    let sof = cloneDeep(this.state.sof)
    const docs = documents.concat(newDocuments)
    docs.forEach(doc => {
      delete doc.timeStamp
    })
    let payload = { contractRef, documents: docs, digitize }
    if (digitize) {
      sof = sof.map(sofSchedule => {
        if (!sofSchedule.endDate) {
          delete sofSchedule.endDate
        }
        return sofSchedule
      })
      sof = sof.map(sofs => ({
        feesType: sofs.feesType === "feePar" ? "feePar" : "slab",
        securityType: sofs.securityType,
        sofDealType: sofs.sofDealType,
        sofMax: sofs.sofMax,
        sofMin: sofs.sofMin,
        fees: sofs.feesType === "feePar" ? sofs.feePar : sofs.fees
      }))
      payload = { ...payload, retAndEsc, nonTranFees, sof, notes }
    }
    payload.contractRef.contractId = (contractRef && contractRef.contractId) || `CONTRACT${Math.random().toString(36).toUpperCase().substring(2, 15)}`
    console.log("========Contract Details======", payload)
    const errors = ScheduleTranValidate(payload, this.props.createdAt || "")

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => {
        //eslint-disable-line
        if (errorMessages.hasOwnProperty(err.path[0])) {
          //eslint-disable-line
          if (
            errorMessages[err.path[0]].hasOwnProperty(err.path[1]) ||
            err.path[2] === "fees"
          ) {
            //eslint-disable-line
            if (err.path[2] === "fees") {
              //debugger
              if (
                errorMessages[err.path[0]][err.path[1]].hasOwnProperty(
                  err.path[2]
                )
              ) {
                if (
                  errorMessages[err.path[0]][err.path[1]][
                    err.path[2]
                  ].hasOwnProperty(err.path[3])
                ) {
                  errorMessages[err.path[0]][err.path[1]][err.path[2]][
                    err.path[3]
                  ] = {
                    ...errorMessages[err.path[0]][err.path[1]][err.path[2]][
                      err.path[3]
                    ],
                    [err.path[4]]: err.message
                  }
                } else {
                  errorMessages[err.path[0]][err.path[1]][err.path[2]] = {
                    ...errorMessages[err.path[0]][err.path[1]][err.path[2]],
                    [err.path[3]]: {
                      [err.path[4]]: err.message
                    }
                  }
                }
              } else {
                errorMessages[err.path[0]][err.path[1]] = {
                  ...errorMessages[err.path[0]][err.path[1]],
                  [err.path[2]]: {
                    [err.path[3]]: {
                      [err.path[4]]: err.message
                    }
                  }
                }
              }
            } else {
              errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message
            }
          } else {
            if (err.path[2]) {
              errorMessages[err.path[0]][err.path[1]] = {
                [err.path[2]]: err.path[2] === "fees" ? {} : err.message
              }
            } else {
              errorMessages[err.path[0]][err.path[1]] = err.message
            }
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          if (err.path[2] === "fees") {
            errorMessages[err.path[0]] = {
              [err.path[1]]: {
                [err.path[2]]: {
                  [err.path[3]]: {
                    [err.path[4]]: err.message
                  }
                }
              }
            }
          } else {
            if (err.path[2]) {
              errorMessages[err.path[0]] = {
                [err.path[1]]: {
                  [err.path[2]]: err.path[2] === "fees" ? {} : err.message
                }
              }
            } else {
              errorMessages[err.path[0]][err.path[1]] = err.message
            }
          }
        } else {
          if (err.path[1]) {
            if (errorMessages.hasOwnProperty(err.path[0])) {
              errorMessages[err.path[0]][err.path[1]] = err.message
            } else {
              errorMessages[err.path[0]] = {
                [err.path[1]]: err.message
              }
            }
          } else {
            errorMessages[err.path[0]] = err.message
          }
        }
      })
      console.log(errorMessages)
      return this.setState({ errorMessages,savemodalState: false, activeItem: [0, 1, 2] })
    }

    newDocuments.length &&
      newDocuments.forEach(async doc => {
        const {
          docCategory,
          docSubCategory,
          docAWSFileLocation,
          docFileName
        } = doc
        // const revisedContext = tags && tags.contextName ? `${contextType}:${tags.contextName}` : contextType
        if (docAWSFileLocation) {
          const userName = (user && `${user.userFirstName || ""} ${user.userLastName || ""}`) || ""
          const tag = {
            docCategory: (docCategory && docCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            docSubCategory: (docSubCategory && docSubCategory.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            docName: (docFileName && docFileName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            // docContext: revisedContext || "",
            uploadUserName: (userName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            uploadUserEntityName: (user && user.firmName && user.firmName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
            uploadDate: moment(new Date()).format("MM-DD-YYYY")
          }
          console.log("The tags to be submitted are", tag)
          await updateS3DocTags(docAWSFileLocation, { ...tag })
        }
      })
    payload.status = "Active"

    this.setState({
      errorMessages: {},
      activeItem: [],
      loading: true, 
      savemodalState: false
    },() => {
      this.props.onSave(payload, res => {
        if (res.status) {
          this.setState({
            ...res.state,
            newDocuments: [],
            digitizeCheck: true,
            loading: false
          })
        }else {
          this.setState({
            loading: false
          })
        }
      })
    })
  }

  onItemChange = (item, key, index) => {
    const changeItems = cloneDeep(this.state[key])
    changeItems[index] = item
    this.setState({ [key]: changeItems })
  }

  onItemRemove = (key, index) => {
    const { errorMessages } = this.state
    let { dateOverlap } = this.state
    const removeItems = this.state[key]
    removeItems.splice(index, 1)
    if (dateOverlap === index + 1) {
      dateOverlap = ""
    }
    if (errorMessages && errorMessages[key]) {
      delete errorMessages[key][index.toString()]
    }
    this.setState({ [key]: removeItems, errorMessages, dateOverlap })
  }

  onAddFaServieceFee = sIndex => {
    const { sof } = this.state
    sof[sIndex].fees.splice(sof[sIndex].fees.length-1, 0, {
      sofDesc: "",
      sofAmt: "",
      sofCharge: ""
    })
    this.setState({ sof })
  }

  onDocumentsChange = (name, value, docId) => {
    let { digitizeCheck, sof, contractRef } = this.state
    if (docId) {
      digitizeCheck = false
    }
    if (!value) {
      swal(
        "Warning",
        "Are you sure you want to remove the digitized Information?",
        "warning"
      ).then(willDelete => {
        if (willDelete) {
          if (!value) {
            this.setState({
              [name]: value,
              digitizeCheck
            })
          }
        }
      })
    } else {
      sof[0].feesType = "feePar"
      this.setState({
        [name]: value,
        digitizeCheck,
        sof
      },() => {
        if (value === true ? !value : value && value[0].docFileName) {
          this.props.onBlur(
            value && value[0].docFileName,
            "Documents upload",
            contractRef.contractName,
            this.state
          )
        }
      })
    }
  }

  retAndEscDateValid = (type, effStartDate, effEndDate, index) => {
    let retAndEsc = cloneDeep(this.state.retAndEsc)
    let status = false
    retAndEsc.splice(index, 1)
    retAndEsc.forEach(retEsc => {
      retEsc.startTime = retEsc.effStartDate ? moment(retEsc.effStartDate).unix() : 0
      retEsc.endTime = retEsc.effEndDate ? moment(retEsc.effEndDate).unix() : 0
    })

    if (effStartDate || effEndDate) {
      effStartDate = effStartDate ? moment(effStartDate).unix() : 0
      effEndDate = effEndDate ? moment(effEndDate).unix() : 0
      retAndEsc = retAndEsc.filter(retEsc => retEsc.type === type)
      retAndEsc.length
        ? retAndEsc.forEach(retEsc => {
          if ((effStartDate <= retEsc.startTime && effEndDate <= retEsc.startTime) || (effStartDate >= retEsc.endTime && effEndDate >= retEsc.endTime)) {
            status = true
          }
        })
        : (status = true)
    }

    if (!effStartDate || !effEndDate) {
      return status
    }

    if (!status) {
      toast("Selected date overlapping periods.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      this.setState(prevState => ({
        errorMessages: {
          ...prevState.errorMessages,
          retAndEsc: {
            ...(prevState.errorMessages && prevState.errorMessages.retAndEsc),
            [index]: {
              ...(prevState.errorMessages && prevState.errorMessages.retAndEsc && prevState.errorMessages.retAndEsc[index.toString()]),
              effStartDate: "Required",
              effEndDate: "Required"
            }
          }
        },
        dateOverlap: index + 1
      }))
    } else {
      this.setState(prevState => ({
        errorMessages: {
          ...prevState.errorMessages,
          retAndEsc: {
            ...(prevState.errorMessages && prevState.errorMessages.retAndEsc),
            [index]: {
              ...(prevState.errorMessages && prevState.errorMessages.retAndEsc && prevState.errorMessages.retAndEsc[index.toString()]),
              effStartDate: "",
              effEndDate: ""
            }
          }
        },
        dateOverlap: ""
      }))
    }

    return status
  }

  onSelectFeesType = (check, i) => {
    const { sof, errorMessages } = this.state
    sof[i].feesType = check || "feePar"
    if(errorMessages && errorMessages.sof && errorMessages.sof[i]) {
      errorMessages.sof[i].fees = {}
    }
    this.setState({
      sof,
      errorMessages
    })
  }

  onUploadReset = () => {
    this.setState({
      newDocuments: [],
      isEditable: "",
    })
  }

  render() {
    const {
      loading,
      activeItem,
      errorMessages,
      contractRef,
      docSelect,
      digitizeCheck,
      sof,
      digitize,
      newDocuments,
      documents,
      dropDown,
      nonTranFees,
      savemodalState,
      retAndEsc,
      notes,
      dateOverlap
    } = this.state
    const { billingContract } = this.props
    const errContractRef = (errorMessages && errorMessages.contractRef) || {}
    const docs = documents.concat(newDocuments)

    if (loading) {
      return <Loader />
    }
    return (
      <div>
        <Accordion
          multiple
          boxHidden={true}
          isRequired={!!activeItem.length}
          activeItem={activeItem.length ? activeItem : [0]}
          render={({ activeAccordions, onAccordion }) => (
            <div>
              {!billingContract ? (
                <div className="columns">
                  <SaveModal
                    closeModal={this.toggleSaveModal}
                    savemodalState={this.state.savemodalState}
                    message={this.state.modalMessage}
                    onConfirmed={this.state.onConfirmed}
                  />
                  <TextLabelInput
                    required
                    label="Contract Name"
                    error={errContractRef.contractName || ""}
                    placeholder="2016-SOF"
                    name="contractName"
                    value={contractRef.contractName || ""}
                    onChange={this.onContracrRefChange}
                  />

                  <SelectLabelInput
                    label="Contract Type"
                    error={errContractRef.contractType || ""}
                    list={dropDown.contractType || []}
                    name="contractType"
                    value={contractRef.contractType || ""}
                    onChange={this.onContracrRefChange}
                  />
                  <TextLabelInput
                    required
                    label="Start Date"
                    error={errContractRef.startDate || ""}
                    type="date"
                    name="startDate"
                    // value={contractRef.startDate ? moment(contractRef.startDate).format("YYYY-MM-DD") : ""}
                    value={(contractRef.startDate === "" || !contractRef.startDate) ? null : new Date(contractRef.startDate)}
                    onChange={this.onContracrRefChange}
                    onBlur={e => this.onBlur(e, "Start Date")}
                  />
                  <TextLabelInput
                    required
                    label="End Date"
                    type="date"
                    name="endDate"
                    error={errContractRef.endDate || ""}
                    // value={contractRef.endDate ? moment(contractRef.endDate).format("YYYY-MM-DD") : ""}
                    value={(contractRef.endDate === "" || !contractRef.endDate) ? null : new Date(contractRef.endDate)}
                    onChange={this.onContracrRefChange}
                    onBlur={e => this.onBlur(e, "End Date")}
                  />
                </div>
              ) : null}

              {!billingContract ? (
                <SingleContractDocuments
                  contractRef={contractRef}
                  contextId={this.props.contextId}
                  errorMessages={(errorMessages && errorMessages.documents) || {}}
                  tranId={this.props.nav2}
                  onDocSelect={this.onDocSelect}
                  documents={documents}
                  contextType={ContextType.contract}
                  onDeleteDoc={this.props.onDeleteDoc}
                  onParentDocuments={this.onDocumentsChange}
                  newDocuments={newDocuments}
                  onUploadReset={this.onUploadReset}
                  docSelect={docSelect}
                />
              ) : null}

              {/* {!billingContract ? (
                <div className="columns">
                  <div className="column">
                    <label className="checkbox">
                      <p className="multiExpLbl">
                        <input
                          type="checkbox"
                          name="digitize"
                          checked={digitize || false}
                          onClick={e => this.onDocumentsChange(e.target.name, e.target.checked)}
                          onChange={() => {}}
                          disabled={digitizeCheck || false}
                        />
                        &nbsp;&nbsp;do you want to digitize the information?
                      </p>
                    </label>
                  </div>
                </div>
              ) : null} */}

              {digitize ? (
                <div>
                  <RatingSection
                    onAccordion={() => onAccordion(0)}
                    title="Schedule of Fees"
                    actionButtons={this.actionButtons("sof", false, 0)}
                  >
                    {activeAccordions.includes(0) && (
                      <div>
                        {sof &&
                          sof.map((schedule, index) => {
                            const errors = (errorMessages.sof && errorMessages.sof[index.toString()]) || {}
                            const isExists = []
                            sof.forEach(sofSchedule => {
                              if (sofSchedule.securityType === schedule.securityType) {
                                if (sofSchedule.sofDealType !== schedule.sofDealType) {
                                  isExists.push(sofSchedule.sofDealType)
                                }
                              }
                            })
                            const dealTypes = dropDown.sofDealType.filter(type => isExists.indexOf(type) === -1)
                            return (
                              <div key={index.toString()}>
                                <div className="columns">
                                  <SelectLabelInput
                                    label="Security Type"
                                    required={this.props.isRequired}
                                    error={errors.securityType || ""}
                                    list={dropDown.securityType || []}
                                    name="securityType"
                                    value={schedule.securityType || ""}
                                    onChange={e => this.onSofChanges(e, index)}
                                  />

                                  <SelectLabelInput
                                    label="Deal type"
                                    required={this.props.isRequired}
                                    error={errors.sofDealType || ""}
                                    list={dealTypes || []}
                                    name="sofDealType"
                                    value={schedule.sofDealType || ""}
                                    onChange={e => this.onSofChanges(e, index)}
                                  />

                                  <NumberInput
                                    prefix="$"
                                    label="Minimum"
                                    required={this.props.isRequired}
                                    error={errors.sofMin || ""}
                                    name="sofMin"
                                    placeholder="$1.00"
                                    value={schedule.sofMin || ""}
                                    onChange={e => this.onSofChanges(e, index)}
                                  />

                                  <NumberInput
                                    prefix="$"
                                    label="Maximum"
                                    required={this.props.isRequired}
                                    error={(errors.sofMax && "Required (Value should be greater than Minimum value)") || ""}
                                    name="sofMax"
                                    placeholder="$1.15"
                                    value={schedule.sofMax || ""}
                                    onChange={e => this.onSofChanges(e, index)}
                                  />
                                </div>
                                <div className="columns">
                                  <div className="column is-one-fifth">
                                    <label className="radio-button">
                                      Fee Per
                                      <input
                                        type="radio"
                                        name={`feeType${index}`}
                                        value="feePar"
                                        checked={schedule.feesType === "feePar"}
                                        onChange={() => this.onSelectFeesType("feePar", index)}
                                      />
                                      <span className="checkmark" />
                                    </label>
                                  </div>
                                  <div className="column is-one-fifth">
                                    <label className="radio-button">
                                      Slab
                                      <input
                                        type="radio"
                                        name={`feeType${index}`}
                                        value="slab"
                                        checked={schedule.feesType === "slab"}
                                        onChange={() => this.onSelectFeesType("slab", index)}
                                      />
                                      <span className="checkmark" />
                                    </label>
                                  </div>
                                  <div className="column">
                                    <div className="field is-grouped is-pulled-right">
                                      {schedule.feesType === "slab" && (
                                        <div className="control">
                                          <a className="has-text-link is-size-4"
                                            onClick={() => this.onAddFaServieceFee(index)}
                                          >
                                            <i className="far fa-plus-square" />
                                          </a>
                                        </div>
                                      )}
                                      <div className="control">
                                        <a className="has-text-link is-size-4"
                                          onClick={() => this.onItemRemove("sof", index)}
                                        >
                                          <i className="far fa-trash-alt" />
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                {schedule.feesType === "slab" && (
                                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                                    <TableHeader cols={cols[0]} />
                                    <tbody>
                                      {schedule.fees && schedule.fees.length
                                        ? schedule.fees.map((fee, feesIndex) => {
                                          const feesErrors = (errors && errors.fees && errors.fees[feesIndex.toString()]) || {}
                                          const isExistsRole = schedule.fees.map(role => role.sofDesc === "Next" ? null : role.sofDesc !== fee.sofDesc && role.sofDesc) || []
                                          const filterRole = dropDown.sofDesc.filter(role => isExistsRole.indexOf(role) === -1)
                                          return (
                                            <ScheduleOfFees
                                              key={feesIndex.toString()}
                                              error={feesErrors}
                                              dropDown={filterRole || {}}
                                              category="sof"
                                              scheduleIndex={index}
                                              feesIndex={feesIndex}
                                              errors={errors}
                                              item={fee || {}}
                                              onChangeItem={this.onFeesChange}
                                              onRemove={this.onFeeRemove}
                                            />
                                          )
                                        })
                                        : null}
                                    </tbody>
                                  </table>
                                )}
                                {schedule.feesType === "feePar" && (
                                  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                                    <TableHeader cols={cols[3]} />
                                    <tbody>
                                      {schedule.feePar && schedule.feePar.length
                                        ? schedule.feePar.map((fee, feesIndex) => {
                                          const feesErrors = (errors && errors.fees && errors.fees[feesIndex.toString()]) || {}
                                          return (
                                            <FeesPar
                                              key={feesIndex.toString()}
                                              category="feePar"
                                              scheduleIndex={index}
                                              feesIndex={feesIndex}
                                              errors={errors}
                                              item={fee || {}}
                                              onChangeItem={this.onFeesChange}
                                              error={feesErrors}
                                              onRemove={this.onFeeRemove}
                                            />
                                          )
                                        })
                                        : null}
                                    </tbody>
                                  </table>
                                )}
                                {sof.length > 1 && sof.length !== index ? (<hr />) : null}
                              </div>
                            )
                          })}
                      </div>
                    )}
                  </RatingSection>

                  <RatingSection
                    onAccordion={() => onAccordion(1)}
                    title="Non Transactional Fees"
                    actionButtons={this.actionButtons("nonTranFees", false, 1)}
                  >
                    {activeAccordions.includes(1) && (
                      <div>
                        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                          <TableHeader cols={cols[1]} />
                          <tbody>
                            {nonTranFees &&
                              nonTranFees.map((transactionalFee, index) => {
                                const errors = (errorMessages.nonTranFees && errorMessages.nonTranFees[index.toString()]) || {}
                                const isExistsRole = nonTranFees.map(feeRole => feeRole.role !== transactionalFee.role && feeRole.role ) || []
                                const filterRole = dropDown.role.filter(role => isExistsRole.indexOf(role) === -1)
                                return (
                                  <TransactionalFees
                                    key={index.toString()}
                                    roles={filterRole || []}
                                    category="nonTranFees"
                                    index={index}
                                    error={errors}
                                    item={transactionalFee || {}}
                                    onChangeItem={this.onItemChange}
                                    onRemove={this.onItemRemove}
                                  />
                                )
                              })}
                          </tbody>
                        </table>
                      </div>
                    )}
                  </RatingSection>

                  <RatingSection
                    onAccordion={() => onAccordion(2)}
                    title="Retainer & Escalator"
                    actionButtons={this.actionButtons("retAndEsc", false, 2)}
                  >
                    {activeAccordions.includes(2) && (
                      <div>
                        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                          <TableHeader cols={cols[2]} />
                          <tbody>
                            {retAndEsc &&
                              retAndEsc.map((retainerFee, index) => {
                                const errors = (errorMessages.retAndEsc && errorMessages.retAndEsc[index.toString()]) || {}
                                const isExistsType = retAndEsc.map(retEscType => retEscType.type !== retainerFee.type && retEscType.type) || []
                                const filterType = dropDown.type.filter(ty => isExistsType.indexOf(ty) === -1)
                                return (
                                  <RetainerAndEscalator
                                    key={index.toString()}
                                    types={dropDown.type || []}
                                    category="retAndEsc"
                                    index={index}
                                    error={errors}
                                    retAndEscDateValid={this.retAndEscDateValid}
                                    item={retainerFee || {}}
                                    onChangeItem={this.onItemChange}
                                    onRemove={this.onItemRemove}
                                    dateOverlap={!!dateOverlap}
                                  />
                                )
                              })}
                          </tbody>
                        </table>
                      </div>
                    )}
                  </RatingSection>
                  <br />
                  <div className="columns">
                    <div className="column is-full">
                      <div className="field">
                        <div className="multiExpLbl ">
                          <p
                            className="multiExpLbl "
                            title="Notes and instructions by transaction participants"
                          >
                            Notes/Instructions
                          </p>
                        </div>
                        <div className="control">
                          <textarea
                            className="textarea"
                            name="notes"
                            value={notes || ""}
                            onChange={e => this.setState({ [e.target.name]: e.target.value })}
                            placeholder=""
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}

              <hr />
              {digitize || (!billingContract && digitize) || (!billingContract && !digitize) ? (
                <div className="columns">
                  <div className="column is-full">
                    <div className="field is-grouped">
                      <div className="control">
                        <button
                          className="button is-link is-small"
                          onClick={()=>this.showModal()}
                          disabled={dateOverlap || !docs.length || false}
                        >
                          Save
                        </button>
                      </div>
                      <div className="control">
                        <button
                          className="button is-light is-small"
                          onClick={this.props.onCancel}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  auth: state.auth || {}
})

const WrappedComponent = withAuditLogs(ScheduleTransactionAndRetainer)
export default connect(
  mapStateToProps,
  null
)(WrappedComponent)
