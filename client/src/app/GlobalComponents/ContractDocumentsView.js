import React from "react"
import DocUpload from "../FunctionalComponents/docs/DocUpload"
import {DocDropDownSelect} from "./TextViewBox"
import DocLink from "../FunctionalComponents/docs/DocLink"

const ContractDocumentsView = ({ doc = {}, staticField, tranId, user, contextType, contextId, index, dropDown, bucketName, getBidBucket, onChangeItem, errors = {} }) => {

  const onChangeSelect = (name, value) => {
    if (name === "docCategory") {
      doc.docSubCategory = ""
    }
    onChangeItem({
      ...doc,
      [name]: value !== null ? value.value : null,
    }, `user doc type set to ${value !== null ? value.value : null}`)
  }

  return (
    <tbody className="is-marginless is-paddingless">
      <tr>
        <td>
          {staticField && staticField.docCategory ? <small>{doc.docCategory || ""}</small> :
            <div className="complain-details">
              <DocDropDownSelect name="category" data={dropDown.category || []} value={doc.docCategory || ""}
                onChange={(value) => onChangeSelect("docCategory", value)} />
              {errors && errors.docCategory && <small className="text-error">{errors.docCategory}</small>}
            </div>
          }
        </td>
        <td>
          {staticField && staticField.docSubCategory ? <small>{doc.docSubCategory || ""}</small> :
            <div className="complain-details">
              <DocDropDownSelect name="subcategory" data={(doc.docCategory && dropDown.multiSubCategory[doc.docCategory]) || []} value={doc.docSubCategory || ""}
                onChange={(value) => onChangeSelect("docSubCategory", value)} defaultValue={doc.docSubCategory || ""} />
              {errors && errors.docSubCategory && <small className="text-error">{errors.docSubCategory}</small>}
            </div>
          }
        </td>
        <td>
          {
            staticField && staticField.docType ? <small>{doc.docType || ""}</small> :
              <div className="complain-details">
                <DocDropDownSelect name="doctype" data={dropDown.docType || []} value={doc.docType || ""}
                  textField="name" valueField="id" key="name" onChange={(value) => onChangeSelect("docType", value)} />
                {errors.docType ? <p className="text-error">{errors.docType}</p> : null}
              </div>
          }
        </td>
        <td>
          <DocUpload bucketName={bucketName} docMeta={{ category: doc.docCategory, subCategory: doc.docSubCategory, type: doc.docType }}
            versionMeta={{ uploadedBy: doc.createdUserName }}
            onUploadSuccess={getBidBucket} showFeedback
            bidIndex={index} contextId={tranId || contextId} contextType={contextType || ""}
            tenantId={user.entityId} disabled={!(doc.docCategory || doc.docType)} />
          {errors.docAWSFileLocation && <p className="text-error">{errors.docAWSFileLocation}</p>}
        </td>
        <td>
          <div className="control">
            {
              doc.docAWSFileLocation ?
                <div className="field is-grouped">
                  <DocLink docId={doc.docAWSFileLocation} />
                </div> : null
            }
          </div>
        </td>
      </tr>
      <tr>
        <td colSpan={5}>
          <small>Note: Please select Category or Type for Upload Document</small>

        </td>
      </tr>
    </tbody>
  )
}

export default ContractDocumentsView
