import React from "react"
import {SelectLabelInput, TextLabelInput} from "./TextViewBox"
import RatingSection from "./RatingSection"

const QuestionContent = ({questions, dropDown, errors={}, category, onChange, onSave, onRemove}) => (
  <table className="table is-bordered is-striped is-hoverable is-fullwidth">
    <thead />
    {
      questions && questions.length ? questions.map((question,i) => (
        <tbody key={i}>{/*eslint-disable-line*/}
          <tr>
            <td className="multiExpLbl ">{question.sectionHeader || ""}</td>
            {
              !question.sectionItems.length ?
                <td className="multiExpTblVal">
                  <SelectLabelInput list={dropDown.confirmation} name="sectionAffirm" placeholder="Not Applicable" value={question.sectionAffirm || ""} onChange={(e) => onChange(e, i)} />
                </td>
                : null
            }
          </tr>
          {!question.isNew && question.sectionItems && question.sectionItems.length ? question.sectionItems.map((item,j) =>
            <tr key={j}>{/*eslint-disable-line*/}
              <td className="multiExpTblVal">{item.sectionItemDesc}</td>
              <td className="multiExpTblVal">
                <SelectLabelInput list={dropDown.confirmation} name="sectionAffirm" placeholder="Not Applicable" value={item.sectionAffirm || ""} onChange={(e) => onChange(e, i, j)} />
              </td>
            </tr>
          ) : null}
          {
            question.isNew  && question.sectionItems && question.sectionItems.length ? question.sectionItems.map((item,j) =>
              <tr key={j}>{/*eslint-disable-line*/}
                <td>
                  <TextLabelInput placeholder="Text here..." error={errors.sectionItemDesc || ""} name="sectionItemDesc" value={item.sectionItemDesc || ""} onChange={(e) => onChange(e, i, j)}/>
                </td>
                <td>
                  <div className="field is-grouped">
                    <div className="control">
                      <a onClick={() => onRemove(i, j)}>
                        <span className="has-text-link">
                          <i className="far fa-trash-alt"/>
                        </span>
                      </a>
                    </div>
                  </div>
                </td>
              </tr>
            ) : null
          }
          {question.isNew ?
            <div className="column is-full">
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link is-small" onClick={() => onSave(category)} >Save</button>
                </div>
              </div>
            </div>
            : null }
        </tbody>
      )) : null
    }
  </table>
)

export default QuestionContent
