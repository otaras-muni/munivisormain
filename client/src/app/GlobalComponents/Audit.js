import React from "react"
import ReactTable from "react-table"
import moment from "moment"
import { withRouter } from "react-router-dom"
import { fetchTransaction } from "../StateManagement/actions/TransactionDistribute/index"
import { getAuditLogByType } from "../StateManagement/actions/audit_log_actions/index"
import Loader from "./Loader"
import { TextLabelInput } from "./TextViewBox"

// const cols = [
//   { name: "User" },
//   { name: "Action" },
//   { name: "Ip" },
//   { name: "Time" }
// ]



export const Modal = ({ children, closeModal, modalState, title }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => {}}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{title}</p>
          <button className="delete" onClick={closeModal} />
        </header>
        <section className="modal-card-body">
          <div className="content">{children}</div>
        </section>
      </div>
    </div>
  )
}

class Audit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSummary: false,
      loading: true,
      auditLogs: [],
      searchText: "",
      log: "",
      fromDate: "",
      toDate: "",
      sortType: "asc",
      searchList: [],
      modalState: false,
      moreAudit: false,
      activeColumn: 0,
    }
    this.toggleModal = this.toggleModal.bind(this)
  }

  componentWillMount() {
    const { auditLogs } = this.props
    auditLogs &&
      auditLogs.forEach(audit => {
        audit.timeStamp = audit.date ? moment(audit.date).unix() : 0
      })
    auditLogs && auditLogs.sort((a, b) => b.timeStamp - a.timeStamp)
    if (auditLogs) {
      this.setState({
        auditLogs,
        loading: false
      })
    } else {
      if (this.props.nav1 === "rfp") {
        this.getTransactionDetails(this.props.nav2)
      }
      this.getAuditLogs(this.props.nav1, this.props.nav2)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.auditLogs &&
      this.props.auditLogs &&
      nextProps.auditLogs.length !== this.props.auditLogs.length
    ) {
      nextProps &&
        nextProps.auditLogs.forEach(audit => {
          audit.timeStamp = audit.date ? moment(audit.date).unix() : 0
        })
      nextProps && nextProps.auditLogs.sort((a, b) => b.timeStamp - a.timeStamp)
      this.setState({
        auditLogs: nextProps.auditLogs
      })
    }
  }

  onSearchText = e => {
    const { auditLogs } = this.state
    this.setState({
      [e.target.name]: e.target.value
    },() => {
      if (auditLogs.length) {
        this.onSearch()
      }
    })
  }

  onSortHead = (type, col, idx) => {
    let {sortType} = this.state
    col.classes = sortType === "asc" ? "sort-asc" : "sort-desc"
    sortType = sortType === "asc" ? "desc" : "asc"

    this.setState({
      activeColumn: idx,
      sortType
    },() => this.onSearch(type, col.name))
  }

  onSearch = (type, name) => {
    const { searchText, auditLogs, fromDate, toDate } = this.state
    if (searchText) {
      let searchList = auditLogs.filter(obj =>
        ["userName", "log", "ip", "date"].some(key => {
          if (key === "date") {
            return moment(obj[key])
              .format("MM-DD-YYYY h:mm A")
              .toLowerCase()
              .includes(searchText.toLowerCase())
          }
          return (
            obj[key] &&
            obj[key].toLowerCase().includes(searchText.toLowerCase())
          )
        })
      )
      if (searchText && type && name) {
        searchList = this.onSort(searchList, type, name)
      }
      if (fromDate && toDate) {
        searchList = this.onDateSearch(searchList)
      }
      this.setState({
        searchList
      })
    } else {
      let searchList = []
      if (type && name) {
        searchList = this.onSort(auditLogs, type, name)
      }
      if (fromDate && toDate) {
        searchList = this.onDateSearch(auditLogs)
      }
      this.setState({
        searchList
      })
    }
  }

  getTransactionDetails = transId => {
    //eslint-disable-line
    fetchTransaction("summary", transId, transaction => {
      if (transaction && transaction.rfpTranClientId) {
        this.setState({
          transaction,
          loading: false
        })
      } else {
        this.props.history.push("/dash-projs")
      }
    })
  }

  getAuditLogs = (type, transId) => {
    getAuditLogByType(type, transId, res => {
      if (res && res._id) {
        res &&
          res.changeLog.forEach(audit => {
            audit.timeStamp = audit.date ? moment(audit.date).unix() : 0
          })
        res && res.changeLog.sort((a, b) => b.timeStamp - a.timeStamp)
        this.setState({
          auditLogs: res.changeLog,
          loading: false
        })
      } else {
        this.setState({
          loading: false
        })
      }
    })
  }

  toggleModal(log) {
    this.setState(prev => {
      const newState = !prev.modalState
      const tempDiv = document.createElement("div")
      tempDiv.innerHTML = log
      const input = tempDiv.innerText.replace(/\*/g, "")
      return { modalState: newState, log: input }
    })
  }

  loadMoreAudit = () => {
    this.setState(prevState => ({
      moreAudit: !prevState.moreAudit
    }))
  }

  onDateSearch = searchList => {
    const { fromDate, toDate } = this.state
    searchList = searchList.filter(
      sofs =>
        moment(fromDate).format("YYYY-MM-DD") <=
          moment(sofs.date).format("YYYY-MM-DD") &&
        moment(toDate).format("YYYY-MM-DD") >=
          moment(sofs.date).format("YYYY-MM-DD")
    )
    return searchList
  }

  onSort = (searchList, type, name) => {
    const names =
      name === "User"
        ? "userName"
        : name === "Action"
        ? "log"
        : name === "Ip"
        ? "ip"
        : name === "Time"
        ? "date"
        : name
    searchList.sort((a, b) => {
      const nameA = (a[names] && a[names].toUpperCase()) || ""
      const nameB = (b[names] && b[names].toUpperCase()) || ""
      if (type !== "asc") {
        if (nameA > nameB) {
          return -1
        }
        if (nameA < nameB) {
          return 1
        }
        return 0
      } else {
        if (nameA < nameB) {
          return -1
        }
        if (nameA > nameB) {
          return 1
        }
        return 0
      }
    })
    this.setState({ sortType: type === "asc" ? "desc" : "asc" })
    return searchList
  }

  highlightSearch = (string, searchQuery) => {
    const reg = new RegExp(
      searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
      "i"
    )
    return string.replace(reg, str => `<mark>${str}</mark>`)
  }

  render() {
    const {
      searchText,
      searchList,
      moreAudit,
      fromDate,
      toDate,
      sortType,
      activeColumn
    } = this.state
    const auditLogs = searchText || (fromDate && toDate) ? searchList : this.state.auditLogs
    const loading = <Loader />

    let columns = [
      {
        Header:"User",
        id:"userName",
        className:"",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {/* <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.userName )}}/> */}
              <small dangerouslySetInnerHTML={{__html: this.highlightSearch(doc.userName || "-", searchText)}}/>
            </div>
          )
        }
      },
      {
        Header:"Actions",
        id:"log",
        className:"",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
                          <small dangerouslySetInnerHTML={{__html: this.highlightSearch(doc.log || "-", searchText)}}/>
              {/* {doc.log} */}
              {/* <small dangerouslySetInnerHTML={{__html: doc.createdUserName}}/> */}
            </div>
          )
        }
      },
      {
        Header:"IP",
        id:"ip",
        className:"",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {/* <small dangerouslySetInnerHTML={{__html: doc.ip}}/> */}
              <small dangerouslySetInnerHTML={{__html: this.highlightSearch(doc.ip || "-", searchText)}}/>
            </div>
          )
        }
      },
      {
        Header:"Time",
        id:"date",
        className:"",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{
                __html: this.highlightSearch(doc.date ?
                  moment(doc.date).format("MM.DD.YYYY hh:mm A") : doc.date ? moment(doc.date).format("MM.DD.YYYY hh:mm A") : "" || "-", searchText)
              }}/>
            </div>
          )
        }
      }
    ] 

    if (this.state.loading) {
      return loading
    }
    return (
      <section>
        <div className="box">
          <div className="columns">
            <div className="column is-half" style={{marginTop: "12px"}}>
              <p className="multiExpLblBlk">Search</p>
              <p className="control has-icons-left">
                <input
                  className="input is-small is-link"
                  title="Search"
                  type="text"
                  name="searchText"
                  placeholder="search"
                  onChange={this.onSearchText}
                />
                <span className="icon is-left has-background-dark">
                  <i className="fas fa-search" />
                </span>
              </p>
            </div>
            <div className="column">
              <TextLabelInput
                label="Start Date"
                name="fromDate"
                type="date"
                onChange={e => this.onSearchText(e)}
              />
            </div>
            <div className="column">
              <TextLabelInput
                label="End Date"
                name="toDate"
                type="date"
                onChange={e => this.onSearchText(e)}
              />
            </div>
          </div>
        </div>
        <div className="box">
          <div className="overflow-auto">
            <ReactTable
              columns={columns}
              data={auditLogs}
              showPaginationBottom
              defaultPageSize={10}
              pageSizeOptions={ [5, 10, 20, 50, 100] }
              className="-striped -highlight is-bordered"
              style={{ overflowX: "auto" }}
              showPageJump
              minRows={2}
            />
            {/* <table className="table is-bordered is-striped is-hoverable is-fullwidth">
              <thead>
                <tr >
                  {cols.map( (col, i) => (
                    <th
                      key={col.name}
                      className = {activeColumn === i ? col.classes : ""}
                      onClick={() => this.onSortHead(sortType, col, i)}
                      style={{ cursor: "pointer" }}
                      title={col.name}
                    >
                      <p dangerouslySetInnerHTML={{ __html: col.name }} />
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {Array.isArray(auditLogs) && auditLogs.length
                  ? (auditLogs.length > 100 && !moreAudit ? auditLogs.slice(0, 100) : auditLogs ).map((log, i) => (
                    <tr key={i}>
                      <td className="emmaTablesTd">
                        <small
                          dangerouslySetInnerHTML={{__html: this.highlightSearch(log.userName || "-",searchText)}}
                        />
                      </td>
                      <td className="emmaTablesTd">
                        <div>
                          {log.log.length > 2 ? (
                            <div>
                              <small
                                dangerouslySetInnerHTML={{__html: this.highlightSearch(log.log.substring(0, 50).replace(/<*[^>]>/g, "") || "-",searchText)}}
                              />
                              <small>
                                <a
                                  onClick={event => {event.preventDefault()
                                    this.toggleModal(log.log)}}
                                >
                                  {" "}
                                  More...
                                </a>
                              </small>
                            </div>
                          ) : (
                            <small>{log.log}</small>
                          )}
                        </div>
                      </td>
                      <td className="emmaTablesTd">
                        <small
                          dangerouslySetInnerHTML={{__html: this.highlightSearch(log.ip || "-",searchText)}}
                        />
                      </td>
                      <td className="emmaTablesTd">
                        <small
                          dangerouslySetInnerHTML={{__html: this.highlightSearch(log.date ? moment(log.date).format("MM-DD-YYYY h:mm A"): "" || "-", searchText)}}
                        />
                      </td>
                    </tr>
                  ))
                  : null}
              </tbody> 
            </table> */}
          </div>
          {auditLogs.length > 100 ? (
            <div className="column">
              <button className="button is-link is-small" onClick={this.loadMoreAudit}
              >
                {" "}
                {moreAudit ? "Less" : "More"}
              </button>
            </div>
          ) : null}
          <Modal
            closeModal={this.toggleModal}
            modalState={this.state.modalState}
            title="Action Details"
          >
            <p>{this.state.log}</p>
          </Modal>
        </div>
      </section>
    )
  }
  
}




export default withRouter(Audit)
