import React, {Component} from "react"
import { getGlobalRefIssuer, thirdPartyFirmList } from "AppState/actions/AdminManagement/admTrnActions"
import Autosuggest from "react-autosuggest"
import {withRouter} from "react-router-dom"
import connect from "react-redux/es/connect/connect"

class BorrowerList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [],
      filteredBorrower: [],
      borrower: "",
      thirdPartyList: [],
    }
  }

  async componentDidMount() {
    const { userEntities } = this.props.auth
    const { value, thirdParty } = this.props
    let { thirdPartyList } = this.state
    const result = await thirdPartyFirmList(userEntities.entityId)
    if (result && result.length > 0) {
      const addedThirdParty = result.map((item) => item.firmId)
      thirdPartyList = addedThirdParty
    }
    if (thirdParty) {
      const issuerList = await getGlobalRefIssuer({entityType: "Third Party" })
      if(issuerList.status===200 && issuerList.data.globIssuer) {
        this.setState({
          list: issuerList.data.globIssuer,
          borrower: value,
          thirdPartyList
        })
      }
    }
  }

  async componentWillReceiveProps(nextProps) {
    let issuerList = []
    this.setState({
      borrower: nextProps.value
    })
    if( typeof nextProps.participantType !== "undefined" && nextProps.participantType !== "" && this.props.participantType !==  nextProps.participantType ) {
      issuerList = await getGlobalRefIssuer({participantType: nextProps.participantType, entityType: "Prospects/Clients" })
    } /* else if( typeof nextProps.entityType !== "undefined" && nextProps.entityType !== "" && this.props.entityType !==  nextProps.entityType ) {
      issuerList = await getGlobalRefIssuer({entityType: "Third Party" })
    } */
    if(issuerList.status === 200 && issuerList.data.globIssuer) {
      const alreadyAdded = issuerList.data.globIssuer.map((item) => {
        if(item.isExist) {
          return item.firmId
        }
      })

      if(alreadyAdded && Array.isArray(alreadyAdded) && alreadyAdded.length) {
        alreadyAdded.forEach(firmId => {
          if(document.getElementById(firmId)) {
            document.getElementById(firmId).parentElement.style.pointerEvents = "none"
          }
        })
      }
      this.setState({
        list: issuerList.data.globIssuer,
        entity: alreadyAdded
      })
    }
  }

  componentDidUpdate() {
    let { entity } = this.state
    this.props.thirdParty ? entity = this.state.thirdPartyList : entity
    if(entity && Array.isArray(entity) && entity.length) {
      entity.forEach(firmId => {
        if(document.getElementById(firmId)) {
          document.getElementById(firmId).parentElement.style.pointerEvents = "none"
          document.getElementById(firmId).parentElement.style.color = "rgb(167, 162, 162)"
        }
      })
    }
  }

  onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
    const e = {
      target: {
        name: "borrower",
        value: suggestionValue
      }
    }
    this.onChangeItem(e)
  }

  onChangeItem = (e) => {
    let {entity} = this.state
    const {name, value} = e.target
    this.setState({
      [name]: value
    }, this.props.onChange(value, "borrower"))
    this.props.thirdParty ? entity = this.state.thirdPartyList : entity
    if(entity && Array.isArray(entity) && entity.length) {
      entity.forEach(firmId => {
        if(document.getElementById(firmId)) {
          document.getElementById(firmId).parentElement.style.pointerEvents = "none"
          document.getElementById(firmId).parentElement.style.color = "rgb(167, 162, 162)"
        }
      })
    }
  }

  onBlurInput = (event) => {
    const {list} = this.state
    const alreadyAddedFirm = list.map((item) => {
      if(item.isExist) {
        return item.firmName
      }
    })
    if(alreadyAddedFirm.indexOf(event.target.value) !== -1) {
      this.props.onError({error: "Name is Already Exist"})
    } else {
      this.props.onError({error: ""})
    }
  }

  getBorrowerList = async (inputVal) => {
    const input = inputVal && inputVal.value.trim()
    this.getBorrower(input)
  }

  getBorrower = async (inputVal) => {
    const {list} = this.state
    const firmName = list && list.filter(item =>
      item.firmName.toLowerCase().includes(inputVal.toLowerCase()))
    const firm = firmName && firmName.filter(item => item.firmName.toLowerCase())

    this.setState({
      filteredBorrower: firm || []
    })
  }

  onBorrowerClear = () => {
    this.setState({
      filteredBorrower: []
    })
  }

  render() {
    const {filteredBorrower, borrower} = this.state

    const suggestVal = (filteredBorrower) => <div id={filteredBorrower.firmId}> {filteredBorrower.firmName} </div>

    const inputProps = {
      placeholder: "Borrower or Obligor Name",
      name: "borrower",
      value: borrower,
      onChange: this.onChangeItem,
      title: "Borrower or Obligor Name",
      onBlur: this.onBlurInput,
    }

    return (
      <Autosuggest
        suggestions={filteredBorrower || []}
        onSuggestionsFetchRequested={this.getBorrowerList || []}
        onSuggestionsClearRequested={this.onBorrowerClear}
        onSuggestionSelected={this.onSuggestionSelected}
        getSuggestionValue={filteredBorrower => filteredBorrower.firmName || ""}
        renderSuggestion={filteredBorrower => suggestVal(filteredBorrower)}
        const inputProps={inputProps}
      />
    )
  }
}

const mapStateToProps = state => {
  const { admClientDetail, auth } = state
  return { admClientDetail, auth }
}

export default withRouter(connect(mapStateToProps)(BorrowerList))
