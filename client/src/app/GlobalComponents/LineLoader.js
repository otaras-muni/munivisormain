import React from "react"
import Loader from "../../images/loader2 .gif"

const LineLoader = () => (
  <div>
    <div style={{textAlign: "center"}}>
      <img src={Loader} height={100} width={100} alt="loader"/>
    </div>
  </div>
)

export default LineLoader
