import React, { Component } from "react"
import {toast} from "react-toastify"
import RatingSection from "./RatingSection"
import NotesReactTable from "./NotesReactTable"
import Accordion from "./Accordion"
import {saveEntityDetails} from "../../globalutilities/helpers"

class Notes extends Component {

  constructor(props) {
    super(props)
    this.state = {
      note: "",
      edit: false,
      noteId: "",
    }
  }

  onNotesChange = (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  onEditNotes = (note) => {
    if(note){
      this.setState({
        noteId: (note && note._id) || "",
        edit: true,
        note: (note && note.note) || "",
      })
    }
  }

  onSaveCallback = async (err, res) => {
    if (!err) {
      this.setState({
        note: "",
        edit: false,
        noteId: "",
      })
      this.props.saveCallback(err, res)
    } else {
      toast("Error in saving changes", {
        autoClose: 2000,
        type: toast.TYPE.ERROR
      })
    }
  }

  onSaveNotes = async () => {
    const { userId, userFirstName, userLastName, entityId, notes } = this.props
    const { note, edit, noteId } = this.state
    if(note){
      if(edit){
        const index = notes.findIndex(f => f._id === noteId)
        notes[index] = {
          note,
          createdAt: new Date(),
          updatedAt: new Date(),
          updatedByName: `${userFirstName || ""} ${userLastName || ""}`,
          updatedById: userId,
        }
      } else {
        notes.push({
          note,
          createdAt: new Date(),
          updatedAt: new Date(),
          updatedByName: `${userFirstName || ""} ${userLastName || ""}`,
          updatedById: userId,
        })
      }
      const payload = {
        notes
      }
      saveEntityDetails(entityId, payload, "", this.onSaveCallback)
    }
  }

  render() {
    const { note } = this.state
    const { notes, canEdit, entityId } = this.props
    if(!entityId){
      return null
    }
    return (
      <Accordion
        multiple
        activeItem={[]}
        boxHidden
        render={({activeAccordions, onAccordion}) =>
          <div>
            <RatingSection
              onAccordion={() => onAccordion()}
              title="Notes"
            >
              {
                activeAccordions.includes() && (
                  <div>
                    <NotesReactTable
                      notes={notes}
                      onEditNotes={this.onEditNotes}
                      canEdit={canEdit || false}
                    />
                    <div className="columns">
                      <div className="column is-full">
                        <p className="multiExpLbl">Notes</p>
                        <div className="control">
                          <textarea
                            className="textarea"
                            name="note"
                            value={note || ""}
                            onChange={(e) => this.onNotesChange(e)}
                            disabled={canEdit || false}/>
                        </div>
                      </div>
                    </div>
                    { !canEdit ?
                      <div className="columns">
                        <div className="column">
                          <button className="button is-link is-small center" disabled={!note}
                            onClick={() => this.onSaveNotes()}>Save
                          </button>
                        </div>
                      </div> : null
                    }
                  </div>
                )}
            </RatingSection>
          </div>
        }
      />
    )
  }
}

export default Notes
