import React, { Component } from "react"

const HandleUnmount = (WrappedComponent) => class extends Component {
  constructor(props) {
    super(props)
    this.unmount = false
    // this.getWrappedComponent = this.getWrappedComponent.bind(this)
  }

  componentDidMount() {
    console.log("WrappedComponent.prototype.setState : ", WrappedComponent.prototype.setState)
    console.log("Component.prototype.setState : ", Component.prototype.setState)
    WrappedComponent.prototype.setState = Component.prototype.setState
  }

  componentWillUnmount() {
    console.log("unmounting HandleUnmount")
    this.unmount = true
    console.log("WrappedComponent.prototype.setState : ", WrappedComponent.prototype.setState)
    WrappedComponent.prototype.setState = (() => {})
  }

  // getWrappedComponent(component) {
  //   // const originalSetState = component.prototype.setState
  //   // // console.log("component : ", component)
  //   // if(originalSetState) {
  //   //   component.setState = (args1) => {
  //   //     console.log("setState : ", this.unmount)
  //   //     console.log("originalSetState : ", originalSetState)
  //   //     console.log("args1 : ", args1)
  //   //     if(this.unmount) {
  //   //       return null
  //   //     }
  //   //     return component.setState.call(args1)
  //   //   }
  //   // }
  //   return component
  // }

  render() {
    // const WrappedComponent = this.getWrappedComponent(component)
    // const WrappedComponent = component
    return (
      <WrappedComponent { ...this.props } />
    )
  }
}
export default HandleUnmount
