import React, { Component } from "react"
import { Multiselect } from "react-widgets"
import { toast } from "react-toastify"

import { pdfTableDownload } from "GlobalUtils/pdfutils"
import ExcelSaverMulti from "Global/ExcelSaverMulti"
import { Modal } from "Global/BulmaModal"
import {
  getChecklist,
  getPicklistValues,
  mapAccordionsObjectToArray,
  mapAccordionsArrayToObject,
  getChecklistTasks
} from "GlobalUtils/helpers"
import { SelectLabelInput, TextLabelInput } from "./TextViewBox";
import CONST from "GlobalUtils/consts"
import withAuditLogs from "../GlobalComponents/withAuditLogs"

import Loader from "./Loader"
import { connect } from "react-redux"
import swal from "sweetalert"

const ObjectID = require("bson-objectid")

const NUM_COLUMN_HEADERS = {
  template1: 0,
  template3: 2,
  template4: 1
}

const getInitialState = () => ({
  checklistId: "",
  accordions: {},
  itemHeaders: {},
  expanded: [],
  validationError: {},
  changeLog: [],
  generalError: "",
  waiting: false,
  priorityLookup: [],
  eventStatus: [],
  newLabelKey: "",
  newLabelIdx: "",
  totalCol1: {},
  totalCol2: {},
  selectOpen: {},
  showAll: {},
  jsonSheets: [],
  modalState: false,
  toggleShowAll: true,
  type: "",
  checklistName: "",
  startXlDownload: false,
  showTaskStatusId: "",
  showTaskStatusLabel: "",
  confirmAlert: CONST.confirmAlert
})

class AttachChecklist extends Component {
  constructor(props) {
    super(props)
    this.state = getInitialState()
    this.validateSubmition = this.validateSubmition.bind(this)
    this.saveAccordions = this.saveAccordions.bind(this)
    this.resetAllAcccordions = this.resetAllAcccordions.bind(this)
    // this.saveData = this.saveData.bind(this)
    this.logAndSave = this.logAndSave.bind(this)
    this.handleKeyPressLabel = this.handleKeyPressLabel.bind(this)
    this.toggleParticiantsSetting = this.toggleParticiantsSetting.bind(this)
    this.downloadToPDF = this.downloadToPDF.bind(this)
    this.downloadToXL = this.downloadToXL.bind(this)
    this.resetXLDownloadFlag = this.resetXLDownloadFlag.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.setWaitingFalse = this.setWaitingFalse.bind(this)
  }

  async componentDidMount() {
    console.log("attach componentDidMount")
    await this.setAccordions(this.props)
    // const result1 = await getPicklistValues("LKUPHIGHMEDLOW")
    // const result2 = await getPicklistValues("LKUPTRANSACTIONSTATUS")
    const [result1, result2] = await getPicklistValues([
      "LKUPHIGHMEDLOW",
      "LKUPSOEEVENTSTATUS"
    ])
    const priorityLookup = result1[1]
    const eventStatus = result2[1]
    this.setState({ priorityLookup, eventStatus })
  }

  async componentWillReceiveProps(nextProps) {
    console.log("attach componentWillReceiveProps")
    const { checklistId, checklist } = nextProps
    // console.log("this checklistId : ", this.props.checklistId)
    // console.log("next checklistId : ", checklistId)
    if (checklistId && this.props.checklistId !== checklistId) {
      await this.setAccordions(nextProps, this.props.checklistId)
    } else if (checklist && checklist.id !== this.props.checklist.id) {
      await this.setAccordions(nextProps, this.props.checklist)
    }
  }

  componentWillUnmount() {
    console.log("attach componentWillUnmount")
  }

  async setAccordions(props, prevChecklist) {
    let { checklistId, checklist, checklists } = props
    console.log("checklist : ", checklist)
    if (checklistId) {
      const [
        accordions,
        itemHeaders,
        type,
        name
      ] = await this.getAccordionsFromConfig(checklistId)
      this.setState({
        checklistId,
        accordions,
        itemHeaders,
        type,
        checklistName: name
      })
    } else if (checklist) {
      this.setState(prevState => {
        if (prevChecklist && prevChecklist.id) {
          checklists = checklists.filter(e => e.id !== prevChecklist.id)
          const { accordions, itemHeaders } = prevState
          checklist =
            this.mapChecklistFormatFromArrrayToObject(
              prevState.checklists.filter(e => e.id === checklist.id)[0]
            ) || checklist
          console.log("checklist : ", checklist)
          console.log("accordions : ", accordions)
          const otherData = {}
          Object.keys(itemHeaders).forEach(k => {
            otherData[k] = {}
            otherData[k].headers = itemHeaders[k]
          })
          const configChecklist = {}
          configChecklist.id = prevChecklist.id
          configChecklist.type = prevChecklist.type
          configChecklist.name = prevChecklist.name
          configChecklist.status = prevChecklist.status
          configChecklist.data = mapAccordionsObjectToArray(
            accordions,
            otherData
          )
          console.log("configChecklist : ", configChecklist)
          checklists.push(configChecklist)
        }
        const { data, type, itemHeaders, name } = checklist
        const accordionsData = {}
        if (data) {
          const { showAll, selectOpen, totalCol1, totalCol2 } = prevState
          console.log("data : ", data)
          Object.keys(data).forEach(k => {
            showAll[k] = data[k].map(e => "")
            selectOpen[k] = [...showAll[k]]
            accordionsData[k] = [...data[k]]
            totalCol1[k] = data[k].reduce((s, e) => s + +(e && e.values && e.values[0]), 0)
            totalCol2[k] = data[k].reduce((s, e) => s + +(e && e.values && e.values[1]), 0)
            this.initAccordionItemValues(accordionsData, k, type)
            console.log("accordionsData[k] : ", accordionsData[k])
          })
          const { toggleShowAll, eventStatus, priorityLookup } = prevState
          return {
            ...getInitialState(),
            toggleShowAll,
            eventStatus,
            priorityLookup,
            checklistId,
            accordions: accordionsData,
            checklists,
            itemHeaders,
            type,
            showAll,
            selectOpen,
            checklistName: name,
            totalCol1,
            totalCol2
          }
        }
      })
    } else {
      this.setState({ generalError: "No checklist to display !" })
    }
  }

  async getAccordionsFromConfig(checklistId) {
    const [accordions, itemHeaders, type, name] = await getChecklist(
      checklistId
    )
    Object.keys(accordions).forEach(k => {
      accordions[k] = [...accordions[k]]
      accordions[k].forEach((e, i) => {
        accordions[k][i] = { ...e }
        accordions[k][i].values = ["", ""]
      })
    })
    return [accordions, itemHeaders, type, name]
  }

  setNewLabelId(key, i, e) {
    e.stopPropagation()
    this.setState({ newLabelIdx: i, newLabelKey: key })
  }

  getAssignedToList(key, i) {
    let { participants } = this.props
    const { accordions, showAll, toggleShowAll } = this.state
    const { assignedToTypes } = accordions[key][i]
    if (assignedToTypes && assignedToTypes.length && !showAll[key][i] && !toggleShowAll) {
      participants = participants.filter(e => assignedToTypes.includes(e.type))
    }
    const entityIds = [...new Set(participants.map(e => e.entityId))]
    let data = []
    entityIds.forEach(d => {
      const idx = participants.findIndex(p => p.entityId === d)
      const name = participants[idx].userFirmName
      data.push({ _id: d, name, type: "entity" })
      participants
        .filter(p => p.entityId === d)
        .forEach(e => {
          const idx1 = data.findIndex(o => o._id === e._id)
          if (idx1 === -1) {
            data.push({ ...e })
          }
        })
    })
    if(accordions && accordions[key] && accordions[key].length && accordions[key] && accordions[key][i] && accordions[key][i].assignedTo && accordions[key][i].assignedTo.length){
      accordions[key][i].assignedTo.map(m => {
        if(m && m.type === "entity" && data && data.length){
          data = data.filter(d => d && d.entityId !== m._id)
        }
      })
    }
    // console.log("data : ", data)

    if (!showAll[key][i] && !toggleShowAll) {
      data.unshift({ name: "Show All Participants", type: "Option" })
    }
    return data
  }

  setWaitingFalse(err, data) {
    this.setState({ waiting: false, statusData: data || [] })
    if (err) {
      toast("Error in getting task status. Please try again.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
  }

  toggleModal() {
    this.setState(prev => {
      const modalState = !prev.modalState
      return { modalState, showTaskStatusId: "", showTaskStatusLabel: "" }
    })
  }

  showTaskStatus(key, i) {
    const { accordions } = this.state
    const { _id, label } = accordions[key][i]
    const { processId } = this.props
    this.setState({
      modalState: true,
      waiting: true,
      showTaskStatusId: _id,
      showTaskStatusLabel: label
    })
    getChecklistTasks(processId, _id, this.setWaitingFalse)
  }

  downloadToPDF() {
    const { type, accordions, checklistName, itemHeaders } = this.state
    const keys = Object.keys(accordions)
    const pdfData = []
    keys.forEach(k => {
      const headers = []
      const rows = []
      switch (type) {
      case "template1": {
        headers.push(
          "Consider",
          ...itemHeaders[k],
          "Priority",
          "Assigned to",
          "End Date",
          "Resolved"
        )
        accordions[k].forEach(e => {
          rows.push([
            !e.hasOwnProperty("consider") || e.consider ? "Y" : "N",
            e.label || "",
            e.priority || "",
            (e.assignedTo && e.assignedTo.length
              ? e.assignedTo.map(d => d.name)
              : [""]
            ).toString(),
            e.endDate ? new Date(e.endDate).toLocaleDateString() : "",
            e.hasOwnProperty("resolved") && e.resolved ? "Y" : "N"
          ])
        })
        break
      }
      case "template3": {
        headers.push(...itemHeaders[k])
        accordions[k].forEach(e => {
          rows.push([e.label || "", ...e.values])
        })
        break
      }
      case "template4": {
        headers.push(...itemHeaders[k], "Start Date", "End Date", "Status")
        accordions[k].forEach(e => {
          rows.push([
            e.label || "",
            (e.assignedTo && e.assignedTo.length
              ? e.assignedTo.map(d => d.name)
              : [""]
            ).toString(),
            e.startDate ? new Date(e.startDate).toLocaleDateString() : "",
            e.endDate ? new Date(e.endDate).toLocaleDateString() : "",
            e.eventStatus || ""
          ])
        })
        break
      }
      default:
        break
      }
      const item = { titles: [k], headers, rows }
      pdfData.push(item)
    })
    pdfTableDownload(checklistName, pdfData, `${checklistName}.pdf`)
  }

  downloadToXL() {
    const { type, accordions, itemHeaders } = this.state
    const keys = Object.keys(accordions)
    const jsonSheets = []
    keys.forEach(k => {
      const headers = []
      const data = []
      switch (type) {
      case "template1": {
        headers.push(
          "Consider",
          itemHeaders[k][0],
          "Priority",
          "Assigned to",
          "End Date",
          "Resolved"
        )
        accordions[k].forEach(e => {
          data.push({
            Consider: !e.hasOwnProperty("consider") || e.consider ? "Y" : "N",
            [itemHeaders[k][0]]: e.label || "",
            Priority: e.priority || "",
            "Assigned to": (e.assignedTo && e.assignedTo.length
              ? e.assignedTo.map(d => d.name)
              : [""]
            ).toString(),
            "End Date": e.endDate
              ? new Date(e.endDate).toLocaleDateString()
              : "",
            Resolved: e.hasOwnProperty("resolved") && e.resolved ? "Y" : "N"
          })
        })
        break
      }
      case "template3": {
        headers.push(...itemHeaders[k])
        accordions[k].forEach(e => {
          const item = { [itemHeaders[k][0]]: e.label || "" }
          itemHeaders[k].slice(1).forEach((d, i) => {
            item[d] = e.values[i]
          })
          data.push(item)
        })
        console.log("headers : ", headers)
        console.log("data : ", data)
        break
      }
      case "template4": {
        headers.push(...itemHeaders[k], "Start Date", "End Date", "Status")
        accordions[k].forEach(e => {
          data.push({
            [itemHeaders[k][0]]: e.label || "",
            [itemHeaders[k][1]]: (e.assignedTo && e.assignedTo.length
              ? e.assignedTo.map(d => d.name)
              : [""]
            ).toString(),
            "Start Date": e.startDate
              ? new Date(e.startDate).toLocaleDateString()
              : "",
            "End Date": e.endDate
              ? new Date(e.endDate).toLocaleDateString()
              : "",
            Status: e.eventStatus || ""
          })
        })
        break
      }
      default:
        break
      }
      jsonSheets.push({ name: k, data, headers })
    })
    this.setState(
      { jsonSheets, startXlDownload: true },
      this.resetXLDownloadFlag
    )
  }

  resetXLDownloadFlag() {
    this.setState({ startXlDownload: false })
  }

  mapChecklistFormatFromArrrayToObject(checklist) {
    if (checklist && checklist.data) {
      const [itemData, otherData] = mapAccordionsArrayToObject(
        checklist.data,
        true
      )
      const itemHeaders = {}
      Object.keys(otherData).forEach(k => {
        itemHeaders[k] = otherData[k].headers
      })
      return { ...checklist, data: itemData, itemHeaders }
    }
    return null
  }

  toggleParticiantsSetting() {
    this.setState(prevState => ({ toggleShowAll: !prevState.toggleShowAll }))
  }

  resetAllAcccordions() {
    const {confirmAlert} = this.state
    confirmAlert.text = "You want to undo all your checklist changes?"
    swal(confirmAlert)
      .then(async(willUndo) => {
        if (willUndo) {
          this.setAccordions(this.props)
          this.setState({
            validationError: {},
            changeLog: [],
            generalError: "",
            newLabelKey: "",
            newLabelIdx: "",
            totalCol1: {},
            totalCol2: {},
            toggleShowAll: true
          })
          toast("Your changes are reverted", {autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
        }
      })
  }

  saveData(updatedChecklists) {
    const { accordions, validationError, generalError } = this.state
    const errKeys = Object.keys(validationError)
    console.log("errKeys : ", errKeys)
    let save = true
    if (errKeys.length) {
      let error = false
      errKeys.some(i => {
        const itemKeys = Object.keys(validationError[i])
        console.log("itemKeys : ", itemKeys)
        if (!itemKeys.length) {
          return true
        }
        itemKeys.some(j => {
          const subItemKeys = Object.keys(validationError[i][j])
          console.log("subItemKeys : ", subItemKeys)
          if (!subItemKeys.length) {
            return true
          }
          subItemKeys.some(k => {
            console.log("subItem : ", validationError[i][j][k])
            if (validationError[i][j][k]) {
              error = true
              return true
            }
          })
          if (error) {
            return true
          }
        })
        if (error) {
          return true
        }
      })
      if (error) {
        save = false
      }
    }

    Object.keys(accordions).forEach(a => {
      Object.keys(accordions && accordions[a]).forEach(k => {
        const eventStatus = accordions[a] && accordions[a][k] && accordions[a][k].eventStatus || ""
        const endDate = accordions[a] && accordions[a][k] && accordions[a][k].endDate || ""
        const assignedTo = accordions[a] && accordions[a][k] && accordions[a][k].assignedTo || []
        if(!eventStatus && endDate && assignedTo.length){
          accordions[a][k].eventStatus = "Active"
        }
      })
    })

    if (save && !generalError) {
      this.setState({
        checklists: updatedChecklists,
        newLabelKey: "",
        newLabelIdx: ""
      })
      this.logAndSave(updatedChecklists)
    }
  }

  logAndSave(updatedChecklists) {
    // TODO -audit log
    this.props.onSaveChecklist(updatedChecklists, this.props.checklist.id)
    this.props.submitAuditLogs(this.props.processId)
  }

  handleClick(key) {
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  }

  resetAcccordion(key, e) {
    e.stopPropagation()
    this.setState((prevState, props) => {
      const { totalCol1, totalCol2 } = prevState
      const expanded = [...prevState.expanded]
      if (!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      if (props.checklist.data[key]) {
        accordions[key] = [...props.checklist.data[key]]
        this.initAccordionItemValues(accordions, key, props.checklist.type)
        if (props.checklist.type === "template3") {
          totalCol1[key] = accordions[key].reduce((s, e) => s + +e.values[0], 0)
          totalCol2[key] = accordions[key].reduce((s, e) => s + +e.values[1], 0)
        }
      }

      return {
        accordions,
        expanded,
        validationError: {},
        totalCol1,
        totalCol2,
        generalError: "",
        newLabelKey: "",
        newLabelIdx: ""
      }
    })
    // this.setState({ accordions: data });
  }

  removeItem(key, i) {
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      const validationError = {...prevState.validationError}
      if(validationError[key] && validationError[key][i]){
        delete validationError[key]
      }
      accordions[key] = [...accordions[key]]
      accordions[key].splice(i, 1)
      return { accordions, validationError }
    })
  }

  addAcccordionItem(key, e) {
    const {user} = this.props
    e.stopPropagation()
    this.setState(prevState => {
      const expanded = [...prevState.expanded]
      const { type } = prevState
      if (!expanded.includes(key)) {
        expanded.push(key)
      }
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key] = accordions[key].filter(e => e && e.label)
      const values = []
      for (let i = 0; i < NUM_COLUMN_HEADERS[type]; i++) {
        values.push(0)
      }
      accordions[key].unshift({
        _id: ObjectID(),
        label: "",
        values
      })
      this.props.addAuditLog({
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `Check-n-Track item add in ${key}`,
        date: new Date(),
        key
      })
      return {
        accordions,
        expanded,
        newLabelIdx: accordions[key].length ? 0 : 0,
        newLabelKey: key
      }
    })
  }

  saveAccordions() {
    const { checklist } = this.props
    const { accordions, itemHeaders, checklists } = this.state
    const otherData = {}
    Object.keys(itemHeaders).forEach(k => {
      otherData[k] = {}
      otherData[k].headers = itemHeaders[k]
    })
    const configChecklist = {}
    configChecklist.id = checklist.id
    configChecklist.type = checklist.type
    configChecklist.name = checklist.name
    configChecklist.status = checklist.status
    configChecklist.data = mapAccordionsObjectToArray(accordions, otherData)
    const updatedChecklists = []
    checklists.forEach(e => {
      if (e.id === checklist.id) {
        updatedChecklists.push(configChecklist)
      } else {
        updatedChecklists.push(e)
      }
    })
    console.log("updatedChecklists : ", updatedChecklists)
    this.validateSubmition(updatedChecklists)
  }

  validateSubmition(updatedChecklists) {
    this.setState(prevState => {
      const { accordions, validationError } = prevState
      let newErr = { ...validationError }
      Object.keys(accordions).forEach(a => {
        Object.keys(accordions[a]).forEach(k => {
          if (!accordions[a][k].label) {
            newErr[a] = { ...newErr[a] }
            newErr[a][k] = { ...newErr[a][k] }
            newErr[a][k].label = "No input provided"
          }
        })
      })
      const selectedList = updatedChecklists.find(f => f.id === this.props.selectedListId)
      if((selectedList && selectedList.type === "template1" || selectedList && selectedList.type === "template4") && selectedList && selectedList.data && selectedList.data.length){
        selectedList.data.forEach(u => {
          if(u && u.items && u.items.length){
            u.items.forEach((item, i) => {
              if(item && item.assignedTo && item.assignedTo.length && (!(item && item.endDate) || (item && item.endDate === undefined))){
                newErr = {
                  ...newErr,
                  [u.title]: {
                    ...((newErr && newErr[u.title]) || {}),
                    [i]: {
                      ...((newErr && newErr[u.title] && newErr[u.title][i]) || {}),
                      endDate: "End Date required"
                    }
                  }
                }
              } else {
                if(newErr && newErr[u.title] && newErr[u.title][i] && newErr[u.title][i].endDate){
                  newErr[u.title][i].endDate = ""
                }
              }
            })
          }
        })
      }
      return { validationError: newErr }
    }, this.saveData.bind(this, updatedChecklists))
  }

  initAccordionItemValues(accordionsData, k, type) {
    accordionsData[k].forEach((e, i) => {
      accordionsData[k][i] = { ...e }
      // console.log("e.values : ", e.values)
      if (!e.values) {
        const initValues = []
        for (let j = 0; j < NUM_COLUMN_HEADERS[type]; j++) {
          initValues.push(type === "template3" ? 0 : "")
        }
        // console.log("initValues : ", initValues)
        accordionsData[k][i].values = initValues
      } else {
        // console.log("in else : ", NUM_COLUMN_HEADERS[type], e.values.length)
        accordionsData[k][i].values = []
        for (let j = 0; j < NUM_COLUMN_HEADERS[type]; j++) {
          if (j < e.values.length) {
            // console.log("if j : ", j, e.values[j])
            accordionsData[k][i].values[j] = e.values[j]
          } else {
            // console.log("else j : ", j, e.values[j])
            accordionsData[k][i].values[j] = type === "template3" ? 0 : ""
          }
        }
      }
    })
  }

  handleKeyPressLabel(e) {
    if (e.key === "Enter") {
      this.setState({ newLabelIdx: "", newLabelKey: "" })
    }
  }

  // getAssignedToList(key, i) {
  //   const { accordions } = this.state
  //   const { assignedToTypes } = accordions[key][i]
  //   return this.props.participants.filter(e => assignedToTypes.includes(e.type))
  // }

  changeItemField(key, i, e) {
    const { name, type } = e.target
    const value = type === "checkbox" ? e.target.checked : e.target.value
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      accordions[key][i] = { ...accordions[key][i] }
      if ((name === "startDate" || name === "endDate") && !value) {
        const item = {}
        Object.keys(accordions[key][i]).forEach(k => {
          if (k !== name) {
            item[k] = accordions[key][i][k]
          }
        })
        accordions[key][i] = item
        return { accordions }
      }
      accordions[key][i][name] = value
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i] }
      if (name === "label") {
        validationError[key][i].label = ""
      }
      if (name === "endDate") {
        if (
          accordions[key][i].startDate &&
          value &&
          new Date(accordions[key][i].startDate) > new Date(value)
        ) {
          validationError[key][i].endDate =
            "End date cannot be before the start date"
        } else {
          validationError[key][i].endDate = ""
        }
      }
      if (name === "startDate") {
        if (
          accordions[key][i].endDate &&
          value &&
          new Date(accordions[key][i].endDate) < new Date(value)
        ) {
          validationError[key][i].startDate =
            "Start date cannot be after the end date"
        } else {
          validationError[key][i].startDate = ""
        }
      }
      return { accordions, validationError }
    }, () => {
      if(name !== "label") {
        this.onBlurInput(name, value, key)
      }
    })
  }

  onBlurInput = (name, value, key) => {
    const {user} = this.props
    // const { name, type } = e.target
    // const value = type === "checkbox" ? e.target.checked : e.target.value
    if (name && value) {
      this.props.addAuditLog({
        userName: `${user.userFirstName} ${user.userLastName}`,
        log: `Check-n-Track ${name || "empty"} change to ${value || "empty"} in ${key}`,
        date: new Date(),
        key
      })
    }
  }

  changeAssignedTo(key, i, values) {
    console.log("values : ", values)
    this.setState(prevState => {
      const selectOpen = { ...prevState.selectOpen }
      selectOpen[key] = [...selectOpen[key]]
      const showAll = { ...prevState.showAll }
      showAll[key] = [...showAll[key]]
      const idx = values.findIndex(e => e.name === "Show All Participants")
      if (idx > -1) {
        values = values.filter(e => e.name !== "Show All Participants")
        selectOpen[key][i] = true
        showAll[key][i] = true
      }
      const accordions = { ...prevState.accordions }
      accordions[key] = [...accordions[key]]
      // console.log("old val : ", accordions[key][i].assignedTo)
      const selectedEntity = values.filter(val => val.type === "entity").map(val => val._id)
      const selectedValue = values.filter(val => selectedEntity.indexOf(val.entityId) === -1)
      accordions[key][i] = { ...accordions[key][i], assignedTo: selectedValue }
      const validationError = { ...prevState.validationError }
      validationError[key] = { ...validationError[key] }
      validationError[key][i] = { ...validationError[key][i], assignedTo: "" }
      return { accordions, validationError, selectOpen, showAll }
    }, () => {
      this.onBlurInput("Responsible Party", values[0] && values[0].name, key)
    })
  }

  changeItemValues(key, i, j, e) {
    const { value } = e.target
    // const name = j === 0 ? "$/1000" : "Amount"
    this.setState(prevState => {
      let { type, totalCol1, totalCol2, /*generalError*/ } = prevState
      const totalThresholds = this.props.totalThresholds || []
      const accordions = { ...prevState.accordions }
      // console.log("accordions : ", JSON.stringify(accordions))
      accordions[key] = [...accordions[key]]
      accordions[key][i] = { ...accordions[key][i] }
      accordions[key][i].values = [...accordions[key][i].values]
      // console.log("accordions : ", JSON.stringify(accordions))
      accordions[key][i].values[j] = value
      if (type === "template3") {
        totalCol1[key] = accordions[key].reduce((s, e) => s + +e.values[0], 0)
        totalCol2[key] = accordions[key].reduce((s, e) => s + +e.values[1], 0)
        console.log("totalThresholds : ", totalThresholds)
        // if (
        //   (totalThresholds[0] && totalCol1[key] > totalThresholds[0]) ||
        //   (totalThresholds[1] && totalCol2[key] > totalThresholds[1])
        // ) {
        //   generalError = `Total exceeded the defined limit ${totalThresholds[0]} OR ${totalThresholds[1]} for ${key}`
        // } else {
        //   generalError = ""
        // }
      }
      return { accordions, totalCol1, totalCol2, /*generalError*/ }
    }/* , () => this.onBlurInput(name, value, key) */)
  }

  renderParticipantListItem({ item }) {
    if (item.type === "entity") {
      return <strong className="has-text-link">{item.name}</strong>
    }
    return <span className="has-text-black">{item.name}</span>
  }

  renderTagItem({ item }) {
    if (item.type === "entity") {
      return <strong className="has-text-link">{item.name}</strong>
    }
    return <span className="has-text-black">{item.name}</span>
  }

  renderColumnHeaders(k) {
    const { type, itemHeaders } = this.state
    const headers = []
    if (type && itemHeaders[k]) {
      for (let i = 0; i <= NUM_COLUMN_HEADERS[type]; i++) {
        headers.push(
          <th key={k + itemHeaders[k][i]}>
            <div className="control">
              <p className="emmaTablesTh">{itemHeaders[k][i]}</p>
            </div>
          </th>
        )
      }
    }
    switch (type) {
    case "template1":
      return (
        <tr>
          <th>
            <div className="control">
              <p className="emmaTablesTh">Consider</p>
            </div>
          </th>
          {headers}
          <th>
            <div className="control">
              <p className="emmaTablesTh">Priority</p>
            </div>
          </th>
          <th>
            <div className="control">
              <p className="emmaTablesTh">Assigned to</p>
            </div>
          </th>
          <th>
            <div className="control">
              <p className="emmaTablesTh">End Date</p>
            </div>
          </th>
          <th>
            <div className="control">
              <p className="emmaTablesTh">Resolved</p>
            </div>
          </th>
        </tr>
      )
    case "template3":
      return (
        <tr>
          {headers}
          <th>
            <p
              className="multiExpLbl "
              title="Drop checklist item if it is not applicable to this deal/issue"
            >
                Drop
            </p>
          </th>
        </tr>
      )
    case "template4":
      return (
        <tr>
          {headers}
          <th>
            <p
              className="multiExpLbl "
              title="Expected start date for related activity"
            >
                Start Date
            </p>
          </th>
          <th>
            <p
              className="multiExpLbl "
              title="Expected end date for related activity"
            >
                End Date
            </p>
          </th>
          <th>
            <p
              className="multiExpLbl "
              title="Status of event related activity"
            >
                Status
            </p>
          </th>
          <th>
            <p
              className="multiExpLbl "
              title="Drop checklist item if it is not applicable to this deal/issue"
            >
                Drop
            </p>
          </th>
        </tr>
      )
    default:
      return null
    }
  }

  renderTaskStatusModal() {
    const {
      showTaskStatusId,
      showTaskStatusLabel,
      waiting,
      modalState
    } = this.state
    if (showTaskStatusId) {
      return (
        <Modal
          addedClass="view-only-mode-overridden"
          closeModal={this.toggleModal}
          modalState={modalState}
          showBackground
          title="Task Status"
        >
          {waiting ? (
            <Loader />
          ) : (
            this.renderTaskStatusInModal(showTaskStatusLabel)
          )}
        </Modal>
      )
    }
  }

  renderAccordionBodyItem(e, key, i) {
    const {
      type,
      priorityLookup,
      eventStatus,
      newLabelKey,
      newLabelIdx
    } = this.state
    // const participants = this.props.participants || []
    switch (type) {
    case "template1":
      return (
        <tr key={key + i}>
          <td>
            <input
              className="checkbox"
              name="consider"
              type="checkbox"
              checked={e.hasOwnProperty("consider") ? e.consider : true}
              onChange={this.changeItemField.bind(this, key, i)}
            />
          </td>
          <td>
            {(newLabelKey === key && newLabelIdx === i) || !e.label ? (
              <input
                className="input is-small is-link"
                name="label"
                type="text"
                placeholder=""
                value={e.label}
                onKeyPress={this.handleKeyPressLabel}
                onChange={this.changeItemField.bind(this, key, i)}
                onBlur = {() => this.onBlurInput("label", e.label, key)}
              />
            ) : (
              <div>
                <a>
                  <span
                    className="is-link view-only-mode-overridden"
                    onClick={this.showTaskStatus.bind(this, key, i)}
                  >
                    {e.label}
                  </span>
                </a>
                <span className="is-pulled-right has-text-danger">
                  <i
                    className="fas fa-pencil-alt is-small"
                    onClick={this.setNewLabelId.bind(this, key, i)}
                  />
                </span>
              </div>
            )}
            {this.state.validationError &&
              this.state.validationError[key] &&
              this.state.validationError[key][i] &&
              this.state.validationError[key][i].label ? (
                <p className="has-text-danger">{this.state.validationError[key][i].label}</p>
              ) : (
                undefined
              )}
            {this.renderTaskStatusModal()}
          </td>
          {e.values.map((v, j) => (
            <td key={`colvalue${j}`}>
              <input
                className="input is-small is-link"
                type="text"
                placeholder=""
                value={e.values[j]}
                onChange={this.changeItemValues.bind(this, key, i, j)}
              />
            </td>
          ))}
          <td>
            {/* <div className="select is-small is-link">
              <select
                value={e.priority}
                name="priority"
                onChange={this.changeItemField.bind(this, key, i)}
              >
                <option value="">Pick priority</option>
                {priorityLookup.map((p, pi) => (
                  <option key={p + pi} value={p}>
                    {p}
                  </option>
                ))}
              </select>
            </div> */}
            <SelectLabelInput
              placeholder="Pick priority"
              list={priorityLookup || []}
              name="priority"
              value={e.priority || ""}
              onChange={this.changeItemField.bind(this, key, i)}
            />
          </td>
          <td>
            {/* <div className="select is-small is-link">
              <select value={e.assignedTo}
                name="assignedTo"
                onChange={this.changeItemField.bind(this, key, i)}>
                <option value="">Assigned to</option>
                {participants.map((p, pi) => (
                  <option key={p+pi} value={p._id}>
                    {p.name}
                  </option>
                ))
                }
              </select>
            </div> */}
            <Multiselect
              data={this.getAssignedToList(key, i)}
              textField="name"
              // groupBy="type"
              itemComponent={this.renderParticipantListItem}
              tagComponent={this.renderTagItem}
              caseSensitive={false}
              minLength={1}
              // open={!!selectOpen[key][i]}
              value={e.assignedTo && e.assignedTo.length ? e.assignedTo : []}
              filter="contains"
              onChange={this.changeAssignedTo.bind(this, key, i)}
            />
          </td>
          <td>
            {/* <input
              className="input is-small is-link no-wrap-date"
              type="date"
              name="endDate"
              value={e.endDate || ""}
              onChange={this.changeItemField.bind(this, key, i)}
            /> */}
            <TextLabelInput
              type="date"
              name="endDate"
              value={(e.endDate === "" || !e.endDate) ? null : new Date(e.endDate)}
              onChange={this.changeItemField.bind(this, key, i)}
            />
            {this.state.validationError &&
              this.state.validationError[key] &&
              this.state.validationError[key][i] &&
              this.state.validationError[key][i].endDate ? (
                <p className="has-text-danger">{this.state.validationError[key][i].endDate}</p>
              ) : (
                undefined
              )}
          </td>
          <td>
            <input
              className="checkbox"
              type="checkbox"
              name="resolved"
              checked={e.hasOwnProperty("resolved") ? e.resolved : false}
              onChange={this.changeItemField.bind(this, key, i)}
            />
          </td>
        </tr>
      )
    case "template3":
      return (
        <tr key={key + i}>
          <td>
            {(newLabelKey === key && newLabelIdx === i) || !e.label ? (
              <input
                className="input is-small is-link"
                name="label"
                type="text"
                placeholder=""
                value={e.label}
                onKeyPress={this.handleKeyPressLabel}
                onChange={this.changeItemField.bind(this, key, i)}
                onBlur = {() => this.onBlurInput("label", e.label, key)}
              />
            ) : (
              <span onClick={this.setNewLabelId.bind(this, key, i)}>
                {e.label}
              </span>
            )}
            {this.state.validationError &&
              this.state.validationError[key] &&
              this.state.validationError[key][i] &&
              this.state.validationError[key][i].label ? (
                <p className="has-text-danger">{this.state.validationError[key][i].label}</p>
              ) : (
                undefined
              )}
          </td>
          {e.values.map((v, j) => (
            <td key={`colvalue${j}`}>
              <input
                className="input is-small is-link"
                type="number"
                value={e.values[j]}
                onChange={this.changeItemValues.bind(this, key, i, j)}
              />
            </td>
          ))}
          <td className="multiExpTblVal">
            <a>
              <span className="has-text-link">
                <i
                  className="far fa-trash-alt"
                  onClick={this.removeItem.bind(this, key, i)}
                />
              </span>
            </a>
          </td>
        </tr>
      )
    case "template4":
      return (
        <tr key={key + i}>
          <td>
            {(newLabelKey === key && newLabelIdx === i) || !e.label ? (
              <input
                className="input is-small is-link"
                name="label"
                type="text"
                placeholder=""
                value={e.label}
                onKeyPress={this.handleKeyPressLabel}
                onChange={this.changeItemField.bind(this, key, i)}
              />
            ) : (
              <div>
                <a>
                  <span
                    className="is-link view-only-mode-overridden"
                    onClick={this.showTaskStatus.bind(this, key, i)}
                  >
                    {e.label}
                  </span>
                </a>
                <span className="is-pulled-right has-text-danger">
                  <i
                    className="fas fa-pencil-alt is-small"
                    onClick={this.setNewLabelId.bind(this, key, i)}
                  />
                </span>
              </div>
            )}
            {this.state.validationError &&
              this.state.validationError[key] &&
              this.state.validationError[key][i] &&
              this.state.validationError[key][i].label ? (
                <p className="has-text-danger">{this.state.validationError[key][i].label}</p>
              ) : (
                undefined
              )}
            {this.renderTaskStatusModal()}
          </td>
          <td>
            {/* <div className="select is-small is-link">
              <select value={e.responsibleParty}
                name="responsibleParty"
                onChange={this.changeItemField.bind(this, key, i)}>
                <option value="">Responsible Party</option>
                {participants.map((p, pi) => (
                  <option key={p+pi} value={p._id}>
                    {p.name}
                  </option>
                ))
                }
              </select>
            </div> */}
            <div className="select is-link is-fullwidth is-small tp-select-state" style={{height : "100%"}}>
              <Multiselect
                data={this.getAssignedToList(key, i)}
                textField="name"
                // groupBy="type"
                itemComponent={this.renderParticipantListItem}
                tagComponent={this.renderTagItem}
                caseSensitive={false}
                minLength={1}
                value={e.assignedTo && e.assignedTo.length ? e.assignedTo : []}
                filter="contains"
                onChange={this.changeAssignedTo.bind(this, key, i)}
              />
            </div>
          </td>
          <td>
            {/* <input
              className="input is-small is-link"
              type="date"
              name="startDate"
              value={e.startDate || ""}
              onChange={this.changeItemField.bind(this, key, i)}
            /> */}
            <TextLabelInput
              type="date"
              name="startDate"
              value={(e.startDate === "" || !e.startDate) ? null : new Date(e.startDate)}
              onChange={this.changeItemField.bind(this, key, i)}
            />
            {this.state.validationError &&
              this.state.validationError[key] &&
              this.state.validationError[key][i] &&
              this.state.validationError[key][i].startDate ? (
                <p className="has-text-danger">{this.state.validationError[key][i].startDate}</p>
              ) : (
                undefined
              )}
          </td>
          <td>
            {/* <input
              className="input is-small is-link"
              type="date"
              name="endDate"
              value={e.endDate || ""}
              onChange={this.changeItemField.bind(this, key, i)}
            /> */}
            <TextLabelInput
              type="date"
              name="endDate"
              value={(e.endDate === "" || !e.endDate) ? null : new Date(e.endDate)}
              onChange={this.changeItemField.bind(this, key, i)}
            />
            {this.state.validationError &&
              this.state.validationError[key] &&
              this.state.validationError[key][i] &&
              this.state.validationError[key][i].endDate ? (
                <p className="has-text-danger">{this.state.validationError[key][i].endDate}</p>
              ) : (
                undefined
              )}
          </td>
          <td>
            <div className="select is-small is-link">
              <select
                value={e.eventStatus}
                name="eventStatus"
                onChange={this.changeItemField.bind(this, key, i)}
              >
                <option value="">Pick Status</option>
                {eventStatus.map((p, pi) => (
                  <option key={p && p.label + pi} value={p && p.label} disabled={!p.included}>
                    {p && p.label}
                  </option>
                ))}
              </select>
            </div>
          </td>
          <td className="multiExpTblVal">
            <a>
              <span className="has-text-link">
                <i
                  className="far fa-trash-alt"
                  onClick={this.removeItem.bind(this, key, i)}
                />
              </span>
            </a>
          </td>
        </tr>
      )
    default:
      return null
    }
  }

  renderAccordionBody(key, accordion) {
    const { type, totalCol1, totalCol2 } = this.state
    const { viewMode } = this.props
    return (
      <div className="tbl-scroll">
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <thead>{this.renderColumnHeaders(key)}</thead>
        <tbody className={viewMode ? "view-only-mode" : ""}>
          {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
          {type === "template3" ? (
            <tr>
              <td>
                <p className="multiExpLbl" />
              </td>
              <td>
                <p className="multiExpLbl">
                  <strong>
                    {new Intl.NumberFormat("en-US").format(
                      +totalCol1[key] || 0
                    )}
                  </strong>
                </p>
              </td>
              <td>
                <p className="multiExpLbl">
                  <strong>
                    {new Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: "USD"
                    }).format(+totalCol2[key] || 0)}
                  </strong>
                </p>
              </td>
              <td>
                <p className="multiExpLbl" />
              </td>
            </tr>
          ) : (
            undefined
          )}
        </tbody>
      </table>
      </div>
    )
  }

  renderHeader(key) {
    const { viewMode } = this.props
    return (
      <div
        className="accordion-header toggle"
        onClick={this.handleClick.bind(this, key)}
      >
        <span>{key}</span>
        <div className={viewMode ? "field is-grouped view-only-mode" : "field is-grouped"}>
          <div className="control">
            <button
              className="button is-link is-small"
              onClick={this.addAcccordionItem.bind(this, key)}
            >
              Add List Item
            </button>
          </div>
          <div className="control">
            <button
              className="button is-light is-small"
              onClick={this.resetAcccordion.bind(this, key)}
            >
              Reset
            </button>
          </div>
          {/* <div className="control">
            <i className={this.state.expanded.includes(key) ?
              "fa fa-minus-circle" : "fa fa-plus-circle"}
            onClick={this.handleClick.bind(this, key)} />
          </div> */}
        </div>
      </div>
    )
  }

  renderAccordion(key, accordion, ai) {
    const { expanded } = this.state
    let expand = false
    if (expanded.length) {
      if (expanded.includes(key)) {
        expand = true
      }
    } else if (ai === 0) {
      expand = true
    }
    return (
      <section className="accordions" key={key}>
        <article className="accordion is-active">
          {this.renderHeader(key)}
          {expand ? this.renderAccordionBody(key, accordion) : undefined}
        </article>
      </section>
    )
  }

  renderAccordions(accordions) {
    return Object.keys(accordions).map((a, ai) =>
      this.renderAccordion(a, accordions[a], ai)
    )
  }

  renderToggleParticipantsOption() {
    const { toggleShowAll } = this.state
    const { viewMode } = this.props
    return (
      <div className={viewMode ? "control view-only-mode" : "control"} >
        <strong>Select responsible party from : </strong>
        <label className="radio">
          <input
            type="radio"
            checked={!toggleShowAll}
            onChange={this.toggleParticiantsSetting}
          />
          <span> Predefined lists(if any) </span>
        </label>
        <label className="radio">
          <input
            type="radio"
            checked={toggleShowAll}
            onChange={this.toggleParticiantsSetting}
          />
          <span> All participants </span>
        </label>
      </div>
    )
  }

  renderDownloadButtons() {
    return (
      <div className="field is-grouped">
        <div className="control">
          <span className="has-text-link">
            <i
              className="far fa-2x fa-file-excel has-text-link"
              onClick={this.downloadToXL}
              title="Excel Download"
            />
          </span>
        </div>
        <div className="control">
          <span className="has-text-link">
            <i
              className="far fa-2x fa-file-pdf has-text-link"
              onClick={this.downloadToPDF}
              title="PDF Download"
            />
          </span>
        </div>
      </div>
    )
  }

  renderTaskStatusInModal(label) {
    const { statusData } = this.state
    return (
      <div className="tbl-scroll">
        <h4>{label}</h4>
        <table className="table is-bordered is-striped is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl">S. No.</p>
              </th>
              <th>
                <p className="multiExpLbl">Assignee</p>
              </th>
              <th>
                <p className="multiExpLbl">Status</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {statusData.map((e, i) => {
              const {
                taskDetails: { _id, taskAssigneeName, taskStatus }
              } = e
              return (
                <tr key={_id || i}>
                  <td className="emmaTablesTd">{i + 1}</td>
                  <td className="emmaTablesTd">{taskAssigneeName}</td>
                  <td className="emmaTablesTd">{taskStatus}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    )
  }

  render() {
    const {
      generalError,
      accordions,
      jsonSheets,
      checklistName,
      startXlDownload
    } = this.state
    const {
      viewMode,
      checklist: { name, type }
    } = this.props
    return (
      <div>
        <h4 className="subtitle is-4">{name}</h4>
        {generalError && (
          <strong className="has-text-danger">{generalError}</strong>
        )}
        {this.renderDownloadButtons()}
        {["template1", "template4"].includes(type)
          ? this.renderToggleParticipantsOption()
          : undefined}
        {this.renderAccordions(accordions)}
        {viewMode ? (
          undefined
        ) : (
          <div className="field is-grouped-center">
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={this.resetAllAcccordions}
              >
                Undo All Changes
              </button>
            </div>
            <div className="control">
              <button
                className="button is-link is-small"
                onClick={this.saveAccordions}
              >
                Save
              </button>
            </div>
          </div>
        )}
        {generalError && (
          <strong className="has-text-danger">{generalError}</strong>
        )}
        <div style={{ display: "none" }}>
          <ExcelSaverMulti
            startDownload={startXlDownload}
            afterDownload={this.resetXLDownloadFlag}
            label={checklistName}
            jsonSheets={jsonSheets}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: (state.auth && state.auth.userEntities) || {}
})
const WrappedComponent = withAuditLogs(AttachChecklist)

export default connect(mapStateToProps, null)(WrappedComponent)
