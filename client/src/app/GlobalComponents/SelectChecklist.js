import React, { Component } from "react"
import { connect } from "react-redux"

import { Modal } from "Global/BulmaModal"
import { mapAccordionsArrayToObject, mapAccordionsObjectToArray } from "GlobalUtils/helpers"
import { getConfigChecklist } from "../StateManagement/actions"
import AttachChecklist from "./AttachChecklist"
import Loader from "./Loader"

class SelectChecklist extends Component {
  constructor(props) {
    super(props)
    this.state={selectedListIds: [], searchString: "", stringToMatch: "",
      viewListId: "", viewChecklists: [], modalState: false, viewListName: "" }
    this.changeSearchString = this.changeSearchString.bind(this)
    this.handleKeyPressinSearch = this.handleKeyPressinSearch.bind(this)
    this.viewAllChecklists = this.viewAllChecklists.bind(this)
    this.linkToTransaction = this.linkToTransaction.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
  }

  componentDidMount() {
    this.props.getConfigChecklist(this.props.token)
  }

  changeSearchString(e) {
    const searchString = e.target.value
    if(!searchString) {
      this.setState({ searchString: "", stringToMatch: "" })
    } else {
      this.setState({ searchString })
    }
  }

  toggleModal() {
    this.setState((prev) => {
      const newState = !prev.modalState
      let { viewListId, viewChecklists, viewListName } = prev
      if(!newState) {
        viewListId = ""
        viewListName = ""
        viewChecklists = []
      }
      return { modalState: newState, viewListId, viewChecklists, viewListName }
    })
  }

  mapChecklistFormatFromObjectToArrray({ id, name, type, data, status, itemHeaders }) {
    const otherData = {}
    Object.keys(itemHeaders).forEach(k => {
      otherData[k] = {}
      otherData[k].headers = itemHeaders[k]
    })
    const checklist = {}
    checklist.id = id
    checklist.name = name
    checklist.type = type
    checklist.status = status
    checklist.data = mapAccordionsObjectToArray(data, otherData)
    return checklist
  }

  viewChecklist(viewListId) {
    if(viewListId) {
      const { configChecklists } = this.props
      const viewChecklists = [this.mapChecklistFormatFromObjectToArrray(
        configChecklists.filter(c => c.id === viewListId)[0])]
      const viewListName = viewChecklists[0].name
      this.setState({ viewListId, viewListName, viewChecklists, modalState: true })
    }
  }

  handleKeyPressinSearch(e) {
    if (e.key === "Enter") {
      const { searchString } = this.state
      this.setState({ stringToMatch: searchString })
    }
  }

  viewAllChecklists() {
    this.setState({ searchString: "" })
  }

  linkToTransaction() {
    const { selectedListIds } = this.state
    const { onLinkToTxn, configChecklists } = this.props
    const checklists = configChecklists.filter(e => selectedListIds.includes(e.id))
    this.props.onLinkToTxn(checklists)
  }

  changeListSelection(id) {
    this.setState(prevState => {
      let selectedListIds = [ ...prevState.selectedListIds ]
      if(selectedListIds.includes(id)) {
        selectedListIds = selectedListIds.filter(e => e !== id)
      } else {
        selectedListIds.push(id)
      }
      return { selectedListIds }
    })
  }

  renderTableHeader() {
    return (
      <tr>
        <th>
          <p className="emmaTablesTh">ID</p>
        </th>
        <th>
          <p className="emmaTablesTh">Check List Name</p>
        </th>
        <th>
          <p className="emmaTablesTh">Attributed to</p>
        </th>
        <th>
          <p className="emmaTablesTh">Best Practice</p>
        </th>
        <th>
          <p className="emmaTablesTh">Notes</p>
        </th>
        <th>
          <p className="emmaTablesTh">Last Updated</p>
        </th>
        <th>
          <p className="emmaTablesTh">Attach</p>
        </th>
      </tr>
    )
  }

  renderChecklistsRows(checklists) {
    const { selectedListIds } = this.state
    return checklists.map(d =>
      <tr key={d.id}>
        <td className="emmaTablesTd">
          <a onClick={this.viewChecklist.bind(this, d.id)}>{d.id}</a>
        </td>
        <td className="emmaTablesTd">
          <small>{d.name}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.attributedTo}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.bestPractice}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{d.notes}</small>
        </td>
        <td className="emmaTablesTd">
          <small>{new Date(d.lastUpdated.date).toLocaleString()} {d.lastUpdated.by}</small>
        </td>
        <td>
          <input className="checkbox"
            type="checkbox"
            checked={selectedListIds.includes(d.id)}
            onChange={this.changeListSelection.bind(this, d.id)}
          />
        </td>
      </tr>
    )
  }

  renderChecklists(checklists) {
    return (
      <section className="accordions box">
        <p className="title innerPgTitle">Search to attach a checklist or tracker to the transaction</p>
        <div className="columns">
          <div className="column">
            <p className="control has-icons-left">
              <input className="input is-small is-link"
                type="text"
                placeholder="search by checklist name"
                value={this.state.searchString}
                onChange={this.changeSearchString}
                onKeyPress={this.handleKeyPressinSearch}
              />
              <span className="icon is-left has-background-dark">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <div className="column">
            <button className="button is-link is-small"
              onClick={this.viewAllChecklists}>View all available checklists</button>
          </div>
        </div>
        <div className="box" style={{overflow:'auto'}}>
          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <thead>
              {this.renderTableHeader()}
            </thead>
            <tbody>
              {this.renderChecklistsRows(checklists)}
            </tbody>
          </table>
        </div>
      </section>
    )
  }

  render() {
    const { stringToMatch, viewListId, viewListName, selectedListIds } = this.state
    const { configChecklists, alreadySelectedLists } = this.props
    const matchingLists = stringToMatch ?
      configChecklists.filter(c => c.name.toLowerCase().includes(stringToMatch.toLowerCase())) : configChecklists
    if(viewListId) {
      return (
        <Modal closeModal={this.toggleModal}
          fullWidth
          modalState={this.state.modalState}
          title={`Preview Checklist ${viewListName}`}>
          <AttachChecklist
            checklists={this.state.viewChecklists}
            totalThresholds={this.props.totalThresholds}
            participants={this.props.participants}
            onSaveChecklist={() => {}}
            viewMode
            checklist={configChecklists.filter(c => c.id === viewListId)[0]} />
        </Modal>
      )
    }
    if(configChecklists && configChecklists.length) {
      return (
        <div>
          {this.renderChecklists(matchingLists.filter(e => !alreadySelectedLists.includes(e.id)))}
          <div className="column is-grouped-center">
            <button className="button is-link"
              onClick={this.linkToTransaction}
              disabled={!selectedListIds.length}
            >Link to transaction</button>
          </div>
        </div>
      )
    }
    return <Loader />
  }
}

const mapStateToProps = ({ configChecklists }) => ({ configChecklists })

export default connect(mapStateToProps, { getConfigChecklist })(SelectChecklist)
