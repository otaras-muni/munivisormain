import React from "react"
import breadCrumbData from "../../jsonData/breadCrumbData"

function getBreadCrumbLinks() {
  var breadCrumbItem = breadCrumbData.sequences.filter((seq) => seq.link === window.location.pathname);
  if (breadCrumbItem.length === 0) {
    breadCrumbItem = {
      "link": window.location.pathname,
      "sequenceKeys": [
        "current"
      ]
    };
  } else {
    breadCrumbItem = breadCrumbItem[0];
  }

  var currentBreadCrumbArray = [];
  var liItem;

  breadCrumbItem.sequenceKeys.forEach(seqItem => {
    liItem = breadCrumbData.links.filter((link) => link.key === seqItem);
    if (liItem.length === 0) {
      liItem = {
        "key": "current",
        "title": "Current",
        "link": seqItem.link
      };
    } else {
      liItem = liItem[0];
    }
    currentBreadCrumbArray.push(liItem);
  });

  return currentBreadCrumbArray;
}

const BreadCrumb = () => (
  <div className="breadcrumbStyle">
    <nav className="breadcrumb has-succeeds-separator is-right" aria-label="breadcrumbs">
      <ul>
        {
          getBreadCrumbLinks().map(liItem => {
            return <li key={liItem.key} className="is-uppercase has-text-weight-bold"> <a href={liItem.link}>{liItem.title}</a></li>
          })
        }
      </ul>
    </nav>
  </div >
)

export default BreadCrumb
