import React from "react"

const props = {
  paddingTop: 0,
  paddingBottom: 0
}

const RatingSection = ({
  children,
  actionButtons,
  title,
  headerStyle,
  onAccordion,
  style
}) => (
  <article className="accordion is-active">
    <div className="accordion-header">
        <button className="borderless btn-accordion" onClick={onAccordion || null}>
          <p style={headerStyle || {}} >{title}</p>
        </button>
      <div>
        {actionButtons || null}
      </div>
      <i className={`${children ? "fas fa-chevron-down":"fas fa-chevron-up"}`} onClick={onAccordion || null}/>
    </div>
    <div className="accordion-body" style={style}>
      <div
        className="accordion-content"
        style={!children ? props : { overflow: "initial" }}
      >
        {children}
      </div>
    </div>
  </article>
)

export default RatingSection
