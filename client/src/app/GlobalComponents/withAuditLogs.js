import React from "react"
import {compose} from "redux"
import {connect} from "react-redux"
import {
  updateAuditLog,
  getIpAddress
} from "GlobalUtils/helpers"
import {SET_IP_ADDRESS} from "../StateManagement/actions/types"
import {checkSupervisorControls} from "../StateManagement/actions/CreateTransaction"

const withAuditLogs = (WrappedComponent) => {
  class HOC extends React.Component {
    state = {
      auditLogs: [],
      ipAddress: "",
    };

    async componentDidMount() {
      let {ip, user} = this.props
      const isSuperVisor = await checkSupervisorControls()
      const submitAudit = (user && user.settings && user.settings.auditFlag === "no") ? false
        : (user && user.settings && user.settings.auditFlag === "currentState") ? true
          : (user && user.settings && user && user.settings.auditFlag === "supervisor" && (isSuperVisor && isSuperVisor.supervisor)) || false
      if(!ip && submitAudit){
        ip = await getIpAddress()
        this.props.setIpAddress(ip)
      }
      if(!this._unmounted){
        this.setState({
          ipAddress: ip,
          submitAudit
        })
      }
    }

    componentWillUnmount() {
      this._unmounted = true
    }

    addAuditLog = (log) => {
      const {ipAddress} = this.state
      if (!this._unmounted) {
        this.setState({
          auditLogs: [...this.state.auditLogs, {...log, ip: ipAddress}],
        })
      }
    }

    addMultiAuditLog = (logs) => {
      const {ipAddress} = this.state
      logs = logs.map(log => ({
        ...log,
        ip: ipAddress
      }))
      if (!this._unmounted) {
        this.setState({
          auditLogs: [...this.state.auditLogs, ...logs],
        })
      }
    }

    updateAuditLog = (auditLogs) => {
      if (!this._unmounted) {
        this.setState({
          auditLogs,
        })
      }
    }

    submitAuditLogs = async (key) => {
      const {auditLogs, ipAddress, submitAudit} = this.state
      const {user} = this.props
      if (submitAudit) {
        if(auditLogs.length) {
          auditLogs.forEach(x => {
            x.ip = ipAddress
            x.userId = user.userId || ""
            x.date = new Date().toUTCString()
            x.userName = `${user.userFirstName} ${user.userLastName}` || ""
          })
          await updateAuditLog(key, auditLogs)
          if (!this._unmounted) {
            this.setState({
              auditLogs: [],
            })
          }
        }
      }
    }

    render() {
      return (
        <WrappedComponent
          updateAuditLog={this.updateAuditLog}
          addAuditLog={this.addAuditLog}
          submitAuditLogs={this.submitAuditLogs}
          addMultiAuditLog={this.addMultiAuditLog}
          auditLogs={this.state.auditLogs || []}
          {...this.props}
        />
      )
    }
  }

  return HOC
}

const mapStateToProps = (state) => ({
  user: (state.auth && state.auth.userEntities) || {},
  ip: state.ip || null,
})

const mapDispatchToProps = dispatch => ({
  setIpAddress: ip => dispatch({
    type: SET_IP_ADDRESS,
    payload: ip
  })
})

const composedHoc = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withAuditLogs
)

export default composedHoc
