/**
 * Created by 01 on 7/26/2018.
 */
import React from "react"
import { Prompt, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import moment from "moment"
import { getDocDetails, updateS3DocTags, getPicklistByPicklistName } from "GlobalUtils/helpers"
import { fetchDocDetails, getS3FileGetURL } from "../StateManagement/actions"
import Accordion from "./Accordion"
import RatingSection from "./RatingSection"
import TableHeader from "./TableHeader"
import Loader from "./Loader"
import ContractDocumentsView from "./ContractDocumentsView"
import CONST from "../../globalutilities/consts"
import withAuditLogs from "./withAuditLogs"
import {DocDropDownSelect, DropDownInput, DropDownSelect} from "./TextViewBox"
import { ContractDocumentsValidate } from "./ContractDocumentsValidate"
import DocLink from "../FunctionalComponents/docs/DocLink"
import DocModal from "../FunctionalComponents/docs/DocModal"
import DocUpload from "../FunctionalComponents/docs/DocUpload"
import DocModalDetails from "../FunctionalComponents/docs/DocModalDetails"

const cols = [
  [
    { name: "Category"},
    { name: "Sub-category"},
    { name: "Type" },
    { name: "Upload File<span class='icon has-text-danger'><i class='fas fa-asterisk extra-small-icon'/></span>" },
    { name: "Filename" },
  ],
  [
    { name: "Filename" },
    { name: "Category" },
    { name: "Sub-category" },
    { name: "Type" },
    { name: "Created by" },
    { name: "Uploaded Date" },
  ]
]

class SingleContractDocuments extends React.Component {
  constructor(props) {
    super(props)
    const { staticField, user } = this.props
    this.state = {
      bucketName: CONST.bucketName,
      userName: "",
      columns: cols,
      loading: true,
      fileName: "",
      signedS3Url: "",
      selectedDocId: "",
      showSigneeInput: "",
      isEditable: "",
      auditLogs: [],
      uploadedFiles: [],
      documents: [{
        ...cloneDeep(CONST.FAServiceFee.documents),
        ...staticField,
        createdBy: (user && user.userId) || "",
        createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
      }],
      tempDocuments: [{
        ...cloneDeep(CONST.FAServiceFee.documents),
        ...staticField,
        createdBy: (user && user.userId) || "",
        createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
      }],
      errorMessages: {},
      waiting: false,
      doc: {},
      showModal: false,
      isSaveDisabled: false,
      documentsList: [],
      searchList: [],
      dropDown: {
        category: [],
        subCategory: [],
        docType: [],
        actions: [],
        suggestions: [],
      },
      activeItem: [],
      modalState: false,
      chooseMultiple: false,
      email: {
        category: "",
        message: "",
        subject: "",
        sendDocEmailLinks: true
      },
      sortBy: {
        type: "",
        sort: ""
      },
      errors: {}
    }
  }

  async componentDidMount() {
    const { documents, user } = this.props
    let result = await getPicklistByPicklistName(["LKUPDOCCATEGORIES", "LKUPDOCTYPE" ])
    const multiSubCategory = (result[2] && result[2].LKUPDOCCATEGORIES) || {}
    result = (result.length && result[1]) || {}

    documents.forEach(doc => {
      doc.timeStamp = doc.uploadedDate ? moment(doc.uploadedDate).unix() : 0
    })

    this.setState({
      documentsList: (documents && documents.length) ? cloneDeep(documents) : [],
      dropDown: {
        ...this.state.dropDown,
        category: (result && result.LKUPDOCCATEGORIES) || [],
        multiSubCategory,
        docType: (result && result.LKUPDOCTYPE) || [],
      },
      contextId: this.props.nav2,
      userName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
      loading: false,
    })
  }

  componentWillReceiveProps(nextProps) {
    const { documents, errorMessages, newDocuments, user } = this.props
    if (documents && nextProps.documents && documents !== nextProps.documents) {
      nextProps.documents.forEach(doc => {
        doc.timeStamp = doc.LastUpdatedDate || doc.lastUpdatedDate ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix() : 0
      })
      this.setState({
        documentsList: nextProps.documents
      })
    }
    if(errorMessages && nextProps.errorMessages && errorMessages !== nextProps.errorMessages){
      this.setState({
        errorMessages: {
          documents: nextProps.errorMessages
        }
      })
    }
    if(newDocuments && nextProps.newDocuments && newDocuments !== nextProps.newDocuments){
      this.setState({
        documents: nextProps.newDocuments.length ? nextProps.newDocuments : [{
          ...cloneDeep(CONST.FAServiceFee.documents),
          createdBy: (user && user.userId) || "",
          createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
        }]
      })
    }
  }

  onChangeItem = (item, index, category, change) => {
    const items = this.state[category]
    items.splice(index, 1, item)
    this.setState({
      [category]: items,
    },() => this.props.onParentDocuments("newDocuments", items))
  }

  onSave = () => {
    const { documents } = this.state
    const { tranId, user, tags, category, contextType, staticField } = this.props
    let payload = { _id: tranId, documents }
    const errors = ContractDocumentsValidate(payload)
    if (tags) {
      delete tags.tenantId
      delete tags.tenantName
      delete tags.clientId
    }
    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.map(err => { //eslint-disable-line
        if (errorMessages.hasOwnProperty([err.path[0]])) { //eslint-disable-line
          if (errorMessages[err.path[0]].hasOwnProperty(err.path[1])) { //eslint-disable-line
            errorMessages[err.path[0]][err.path[1]][err.path[2]] = "Required" // err.message
          } else if (err.path[2]) {
            errorMessages[err.path[0]][err.path[1]] = {
              [err.path[2]]: "Required" // err.message
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = "Required" // err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          errorMessages[err.path[0]] =
            {
              [err.path[1]]: {
                [err.path[2]]: "Required" // err.message
              }
            }
        } else {
          errorMessages[err.path[0]] = {
            [err.path[1]]: "Required" // err.message
          }
        }
      })
      this.setState({ errorMessages, activeItem: [0] })
      return
    }
    if (category) {
      payload = { _id: tranId, [category]: payload.documents }
    }

    documents.length && documents.forEach(async (doc) => {
      const { docCategory, docSubCategory, docAWSFileLocation, docFileName } = doc
      const revisedContext = tags && tags.contextName ? `${contextType}:${tags.contextName}` : contextType
      if (docAWSFileLocation) {
        const userName = (user && `${user.userFirstName || ""} ${user.userLastName || ""}`) || ""
        const tag = {
          docCategory,
          docSubCategory,
          docContext: revisedContext || "",
          docName: docFileName,
          uploadUserName: (userName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          uploadUserEntityName: (user && user.firmName && user.firmName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, " ")) || "",
          uploadDate: moment(new Date()).format("MM-DD-YYYY"),
        }
        console.log("The tags to be submitted are", tag)
        await updateS3DocTags(docAWSFileLocation, { ...tag })
      }
    })
    this.setState({
      isSaveDisabled: true,
    }, () => {
      this.props.onSave(payload, (res) => {
        if (res && res.status) {
          res.documentsList.forEach(doc => {
            doc.timeStamp = doc.LastUpdatedDate || doc.lastUpdatedDate ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix() : 0
          })
          this.setState({
            errorMessages: {},
            documents: [{
              ...cloneDeep(CONST.FAServiceFee.documents),
              ...staticField,
              createdBy: (user && user.userId) || "",
              createdUserName: (user && `${user.userFirstName} ${user.userLastName}`) || "",
            }],
            documentsList: res.documentsList,
            isSaveDisabled: false,
            activeItem: [],
            uploadedFiles: [],
          })
        } else {
          this.setState({
            errorMessages: {},
            isSaveDisabled: false,
            activeItem: []
          })
        }
      })
    })
  }

  onFileAction = (value, doc) => {
    if (value === "See version history" || value === "open") {
      fetchDocDetails(doc.docAWSFileLocation).then(res => {
        if (value === "open") {
          this.getFileURL(this.state.bucketName, res.name, this.downloadFile)
        } else if (value === "See version history") {
          res.documentId = doc._id
          this.handleDocDetails(res)
        }
      })
    } else if (value === "Share document") {
      this.setState(prevState => ({
        modalState: true,
        email: {
          ...prevState.email,
          message: `Filename: ${doc.docFileName}, Category: ${doc.docCategory}, Sub Category: ${doc.docSubCategory}, type: ${doc.docType}`,
          docIds: [doc.docAWSFileLocation || ""],
        }
      }))
    }
  }

  handleDocDetails = (res) => {
    this.setState({
      showModal: !this.state.showModal,
      doc: res || {},
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  deleteDoc = (versionId, documentId) => {
    console.log("versionId : ", versionId)
    console.log("documentId : ", documentId)
    const { onDeleteDoc, contextType } = this.props
    if (onDeleteDoc) {
      this.props.addAuditLog({ log: `one document deleted from ${contextType}`, key: "document" })
      onDeleteDoc(documentId, (res) => {
        if (res && res.status) {
          res.documentsList.forEach(doc => {
            doc.timeStamp = doc.LastUpdatedDate || doc.lastUpdatedDate ? moment(doc.LastUpdatedDate || doc.lastUpdatedDate).unix() : 0
          })
          this.setState({
            documentsList: res.documentsList || [],
          })
        }
      })
    }
  }

  getBidBucket = (filename, docId, index) => {
    const documents = cloneDeep(this.state.documents)
    documents[index].docAWSFileLocation = docId
    documents[index].docFileName = filename
    this.props.addAuditLog({ userName: this.state.userName, log: `Documents upload ${filename}`, date: new Date(), key: "documents" })

    this.setState({
      documents,
    },() => this.props.onParentDocuments("newDocuments", documents, docId))
  }

  downloadFile = () => {
    this.downloadAnchor.click()
  }

  getFileURL = (bucketName, fileName, callback) => {
    getS3FileGetURL(bucketName, fileName, (res) => {
      this.setState({
        ...res
      }, callback.bind(this, bucketName, fileName))
    })
  }

  onSort = (type) => {
    const { sortBy, documentsList } = this.state
    const key = type === "Category" ? "docCategory" :
      type === "Sub-category" ? "docSubCategory" :
        type === "Uploaded by" ? "createdUserName" :
          type === "Filename" ? "docFileName" :
            type === "Type" ? "docType" :
              type === "Uploaded Date" ? "timeStamp" :
                type === "Created by" ? "createdUserName" : ""
    let sort = ""

    if (key) {
      if (sortBy.type === type) {
        if (type && sortBy.sort === "desc") {
          if (key === "timeStamp") {
            documentsList.sort((a, b) => a[key] - b[key])
          } else {
            documentsList.sort((a, b) => {
              const nameA = (a[key] && a[key].toUpperCase()) || ""
              const nameB = (b[key] && b[key].toUpperCase()) || ""
              if (nameA < nameB) {
                return -1
              }
              if (nameA > nameB) {
                return 1
              }
              return 0
            })
          }
          sort = "asc"
        } else {
          if (key === "timeStamp") {
            documentsList.sort((a, b) => b[key] - a[key])
          } else {
            documentsList.sort((a, b) => {
              const nameA = (a[key] && a[key].toUpperCase()) || ""
              const nameB = (b[key] && b[key].toUpperCase()) || ""
              if (nameA > nameB) {
                return -1
              }
              if (nameA < nameB) {
                return 1
              }
              return 0
            })
          }
          sort = "desc"
        }
      } else {
        if (key === "timeStamp") {
          documentsList.sort((a, b) => a[key] - b[key])
        } else {
          documentsList.sort((a, b) => {
            const nameA = (a[key] && a[key].toUpperCase()) || ""
            const nameB = (b[key] && b[key].toUpperCase()) || ""
            if (nameA < nameB) {
              return -1
            }
            if (nameA > nameB) {
              return 1
            }
            return 0
          })
        }
        sort = "asc"
      }
      this.setState({
        sortBy: {
          type,
          sort
        },
        documentsList
      })
    }
  }

  onEdit = (key, index) => {
    const { isEditable } = this.state
    const { user } = this.props
    this.props.addAuditLog({ userName: (user && user.userName) || "", log: `Documents one row edited`, date: new Date(), key })
    if (isEditable || isEditable === 0) { swal("Warning", `First save the current document`, "warning"); return }
    this.setState({
      isEditable: index,
    })
  }

  onChangeUpdate = (key, doc, value, index) => {
    const { documentsList } = this.state
    if (key === "docCategory") {
      doc.docSubCategory = ""
    }
    doc = {
      ...doc,
      [key]: value,
    }
    documentsList.splice(index, 1, doc)
    this.setState({
      documentsList
    })
  }

  actionButtons = () => {
    return (
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-light is-small" onClick={() => this.props.onUploadReset()} >Reset
          </button>
        </div>
      </div>
    )
  }

  render() {
    const { searchText, errors, bucketName, chooseMultiple, searchList, documents, errorMessages, doc, dropDown, isSaveDisabled, activeItem } = this.state
    const documentsList = searchText ? searchList : this.state.documentsList
    const { tranId, user, contextId, contextType, isDisabled, loggedInContactId, staticField, contractRef } = this.props
    const loading = (
      <Loader />
    )

    if (this.state.loading) {
      return loading
    }
    return (
      <div className="bank-loan-documents">
        {documents.filter(o => o.docFileName).length ? <Prompt
          when={true}
          message='You have unsaved documents, are you sure you want to leave?'
        /> : null}
        <Accordion multiple boxHidden isRequired={!!activeItem.length} activeItem={activeItem.length ? activeItem : [0]} render={({ activeAccordions, onAccordion }) =>
          <div style={{ paddingLeft: "0.25em", paddingRight: "0.25em" }}>
            <RatingSection onAccordion={() => onAccordion(0)} title="Upload Contract Document" actionButtons={this.actionButtons(documents)}>
              {activeAccordions.includes(0) &&
                <div style={{ paddingLeft: "0.1em" }}>
                  { !isDisabled && !documentsList.length &&
                    <div>
                      <table id="my-table" className="table is-bordered is-striped is-hoverable is-fullwidth" style={{fontSize: "smaller"}}>
                        <TableHeader cols={cols[0]}/>
                        {
                          documents.map((doc, index) => {
                            const errors = (errorMessages.documents && errorMessages.documents[index.toString()])
                            return (
                              <ContractDocumentsView key={index} tranId={tranId} user={user} contextType={contextType}
                                                     staticField={staticField} contextId={contextId || ""}
                                                     index={index} doc={doc} dropDown={dropDown}
                                                     loggedInContactId={loggedInContactId}
                                                     bucketName={bucketName} getBidBucket={this.getBidBucket}
                                                     errors={errors} onFileAction={this.onFileAction}
                                                     onChangeItem={(changedItem, change) => this.onChangeItem(changedItem, index, "documents", change)}
                              />
                            )
                          })
                        }
                      </table>
                    </div>
                  }

                  {
                    contractRef && contractRef._id ?
                    <div>
                      <table id="my-table" className="table is-bordered is-striped is-hoverable is-fullwidth" style={{ paddingLeft: "0.25em", paddingRight: "0.25em" }}>
                        <thead>
                        <tr>
                          {
                            cols[1].map(col => (
                              <th key={col.name} onClick={() => this.onSort(col.name)} style={{ cursor: "pointer" }}>
                                <p dangerouslySetInnerHTML={{ __html: col.name }} />
                              </th>
                            ))
                          }
                        </tr>
                        </thead>
                        <tbody>
                        {
                          documentsList && documentsList.map((doc, index) => {
                            const isEditable = (this.state.isEditable === index)
                            return (
                              <tr key={doc._id}>
                                <td>
                                  <div className="field is-grouped-left">
                                    {
                                      !isDisabled &&
                                      <DocUpload bucketName={this.state.bucketName} docMeta={{ category: doc.docCategory, subCategory: doc.docSubCategory, type: doc.docType }}
                                                 docId={doc.docAWSFileLocation}
                                                 versionMeta={{ uploadedBy: doc.createdUserName }} showFeedback
                                                 contextId={contextId || ""} contextType={contextType || ""}
                                                 tenantId={user.entityId} />
                                    }
                                    &nbsp;&nbsp;
                                    <DocLink docId={doc.docAWSFileLocation} />
                                    {
                                      !isDisabled &&
                                      <DocModal
                                        onDeleteAll={this.deleteDoc}
                                        documentId={doc._id}
                                        docMetaToShow={["category", "subCategory"]}
                                        versionMetaToShow={["uploadedBy"]}
                                        selectedDocId={doc.docAWSFileLocation} />
                                    }
                                  </div>
                                </td>
                                <td>
                                  <div className="complain-details">
                                    {
                                      isEditable ?
                                        <DropDownSelect data={dropDown.category || []} value={doc.docCategory || ""}
                                                        textField="name" valueField="id" key="name" onChange={(value) => this.onChangeUpdate("docCategory", doc, value, index)} />
                                        : <small>{doc.docCategory}</small>
                                    }
                                    {errors && errors.docCategory && <small className="text-error">{errors.docCategory}</small>}
                                  </div>

                                </td>
                                <td>
                                  <div className="complain-details">
                                    {
                                      isEditable ?
                                        <DropDownSelect data={(doc.docCategory && dropDown.multiSubCategory[doc.docCategory]) || []} value={doc.docSubCategory || ""}
                                                        textField="name" valueField="id" key="name" onChange={(value) => this.onChangeUpdate("docSubCategory", doc, value, index)} />
                                        : <small>{doc.docSubCategory}</small>
                                    }
                                    {errors && errors.docSubCategory && <small className="text-error">{errors.docSubCategory}</small>}
                                  </div>

                                </td>
                                <td>
                                  {
                                    isEditable ?
                                      <div className="complain-details">
                                        <DropDownInput filter data={dropDown.docType || []} value={doc.docType || ""}
                                                       textField="name" valueField="id" key="name" onChange={(value) => this.onChangeUpdate("docType", doc, value, index)} />
                                        {errors.docType ? <p className="text-error">{errors.docType}</p> : null}
                                      </div>
                                      : <small>{doc.docType}</small>
                                  }
                                </td>
                                <td>
                                  <small>{doc.createdUserName || ""}</small>
                                </td>
                                <td>
                                  <small>{doc.uploadedDate ? moment(doc.uploadedDate).format("MM.DD.YYYY hh:mm A") : ""}</small>
                                </td>

                              </tr>
                            )
                          })
                        }
                        </tbody>
                      </table>
                    </div>
                    : null
                  }
              </div>
              }
            </RatingSection>
          </div>
        } />
        <a className="hidden-download-anchor"
           style={{ display: "none" }}
           ref={el => { this.downloadAnchor = el }}
           href={this.state.signedS3Url} target="_blank" />
        {this.state.showModal ?
          <DocModalDetails showModal={this.state.showModal}
                           closeDocDetails={this.handleDocDetails}
                           documentId={doc.documentId}
                           onDeleteAll={this.deleteDoc}
                           docMetaToShow={["category", "subCategory"]}
                           versionMetaToShow={["uploadedBy"]}
                           docId={doc._id} /> : null
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(SingleContractDocuments)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
