import React, { Component } from "react"
import { connect } from "react-redux"
import Combobox from "react-widgets/lib/Combobox"

import { getFruits, saveFruits, removeFruit } from "../StateManagement/actions"

const lkupdata = ["BANANA", "ORANGE", "APPLE"]

class AccordionKG extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions: {}, expanded: [], err: {} }
    this.validateSubmition = this.validateSubmition.bind(this)
    this.saveAccordions = this.saveAccordions.bind(this)
    this.saveData = this.saveData.bind(this)
  }

  componentDidMount() {
    console.log("setting data in componentDidMount")
    this.props.getFruits()
  }

  componentWillReceiveProps(nextProps) {
    console.log("setting data in componentWillReceiveProps")
    this.setState({ accordions: nextProps.fruits, err: {} })
  }

  saveData() {
    const { accordions, err } = this.state
    const errKeys = Object.keys(err)
    console.log("errKeys : ", errKeys)
    let save = true
    if(errKeys.length) {
      let error = false
      errKeys.some(i => {
        const itemKeys = Object.keys(err[i])
        console.log("itemKeys : ", itemKeys)
        if(!itemKeys.length) {
          return true
        }
        itemKeys.some(j => {
          const subItemKeys = Object.keys(err[i][j])
          console.log("subItemKeys : ", subItemKeys)
          if(!subItemKeys.length) {
            return true
          }
          subItemKeys.some(k => {
            console.log("subItem : ", err[i][j][k])
            if(err[i][j][k]) {
              error = true
              return true
            }
          })
          if(error) {
            return true
          }
        })
        if(error) {
          return true
        }
      })
      if(error) {
        save = false
      }
    }

    if(save) {
      this.props.saveFruits(accordions)
    }
  }

  validateSubmition() {
    this.setState(prevState => {
      const { accordions, err } = prevState
      const newErr = { ...err }
      Object.keys(accordions).forEach(a => {
        Object.keys(accordions[a]).forEach(k => {
          if(!accordions[a][k].fruit) {
            newErr[a] = { ...newErr[a] }
            newErr[a][k] = { ...newErr[a][k]}
            newErr[a][k].fruit = "No Value"
          }
        })
      })
      return { err: newErr }
    }, this.saveData)
  }

  saveAccordions() {
    this.validateSubmition()
  }

  handleClick(key, e) {
    e.stopPropagation()
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if(!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  };

  changeFruit(key, i, val) {
    // console.log(" key: ", key);
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], fruit: val}
      const err = { ...prevState.err }
      err[key] = { ...err[key] }
      err[key][i] = { ...err[key][i], fruit: ""}
      return { accordions, err }
    })
  }

  changeDate(key, i, val) {
    // console.log(" key: ", key);
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], date: val}
      const err = { ...prevState.err }
      err[key] = { ...err[key] }
      err[key][i] = { ...err[key][i], date: ""}
      return { accordions, err  }
    })
  }

  changeApplicable(key, i, event) {
    // console.log(" key: ", key);
    // console.log(" i: ", i);
    const val = event.target.checked
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], applicable: val}
      const err = { ...prevState.err }
      err[key] = { ...err[key] }
      err[key][i] = { ...err[key][i], applicable: ""}
      return { accordions, err }
    })
  }

  addAcccordionItem(key, e) {
    e.stopPropagation()
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key].push({
        fruit: "",
        date: "",
        applicable: true
      })
      return { accordions  }
    })
  }

  resetAcccordion(key, e) {
    e.stopPropagation()
    this.setState((prevState, props) => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [...props.fruits[key]]
      return { accordions, err: {}  }
    })
    // this.setState({ accordions: data });
  }

  renderNav(key) {
    return (
      <nav
        className="level"
        style={{
          backgroundColor: "lightblue",
          paddingLeft: "5px",
          paddingRight: "5px",
          marginBottom: "2px"
        }}
        role="presentation"
        onKeyPress={() =>{}}
        onClick={this.handleClick.bind(this, key)}
      >
        <div className="level-left">
          <div className="level-item">
            <p className="subtitle is-5">
              <strong>123</strong>
                                          posts
            </p>
          </div>
          <div className="level-item">
            <div className="field has-addons">
              <p className="control">
                <input className="input" type="text" placeholder="Find a post"/>
              </p>
              <p className="control">
                <button className="button">
                                                  Search
                </button>
              </p>
            </div>
          </div>
        </div>

        <div className="level-right">
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small" type="button"  onClick={this.addAcccordionItem.bind(this, key)}>
                                              Add
              </button>
            </div>
            <div className="control">
              <button className="button is-light is-small" type="button" onClick={this.resetAcccordion.bind(this, key)}>
                                              Reset
              </button>
            </div>
          </div>
        </div>
      </nav>
    )
  }

  renderAccordionBodyItem(e, key, i) {
    return (
      <tr key={key+i}>
        <td>
          <Combobox
            className="is-primary is-small"
            data={lkupdata}
            value={e.fruit}
            disabled={e.platformConfig}
            onChange={this.changeFruit.bind(this, key, i)}
          />
          {this.state.err && this.state.err[key] && this.state.err[key][i] &&
            this.state.err[key][i].fruit ?
            <strong>{this.state.err[key][i].fruit}</strong>
            : undefined
          }
        </td>
        <td>
          <input
            className="input is-small is-link"
            type="datetime-local"
            value={e.date}
            onChange={this.changeDate.bind(this, key, i)}
          />
          {this.state.err && this.state.err[key] && this.state.err[key][i] &&
            this.state.err[key][i].date ?
            <strong>{this.state.err[key][i].date}</strong>
            : undefined
          }
        </td>
        <td>
          <input
            className="checkbox"
            type="checkbox"
            checked={e.applicable}
            onChange={this.changeApplicable.bind(this, key, i)}
          />
          {this.state.err && this.state.err[key] && this.state.err[key][i] &&
            this.state.err[key][i].applicable ?
            <strong>{this.state.err[key][i].applicable}</strong>
            : undefined
          }
        </td>
      </tr>
    )
  }

  renderAccordionBody(key, accordion) {
    return (
      <table>
        <tbody>
          {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
        </tbody>
      </table>
    )
  }

  renderAccordion(key, accordion) {
    return (
      <div key={key}>
        {this.renderNav(key)}
        {this.state.expanded.includes(key) ?
          this.renderAccordionBody(key, accordion)
          : undefined
        }
      </div>
    )
  }

  renderAccordions(accordions) {
    return Object.keys(accordions).map(a => this.renderAccordion(a, accordions[a]))
  }

  render() {
    return (
      <div>
        {this.renderAccordions(this.state.accordions)}
        <button onClick={this.saveAccordions}>Save</button>
      </div>
    )
  }

}

const mapStateToProps = ({ test }) => {
  console.log("test in state : ", test)
  return { fruits: test }
}

export default connect(mapStateToProps, { getFruits, saveFruits, removeFruit })(AccordionKG)
