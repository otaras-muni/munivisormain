import React, { Component } from "react"
import { connect } from "react-redux"
import { Combobox } from "react-widgets"
import { toast } from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import swal from "sweetalert"
import { updateAuditLog } from "GlobalUtils/helpers"
import CONST, { primaryPicklists } from "GlobalUtils/consts"

import Loader from "./Loader"
import { deleteConfigPicklist, getConfigPicklist, saveConfigPicklist } from "../StateManagement/actions";
import { getSpecificPicklist, saveSpecificPicklist } from "../StateManagement/actions/config_actions"
import RatingSection from "./RatingSection"
import Accordion from "./Accordion"

const ObjectID = require("bson-objectid")

class Picklists extends Component {
  constructor(props) {
    super(props)
    this.state = {
      accordions: {}, nonEdiAccordions: {}, picklistsMeta: {}, nonEdiPicklistsMeta: {}, secondLevelKey: "",
      generalError: "", validationError: {}, systemName: "", newPicklist: false,
      selectedPicklistKey: "", secondLevelIdx: -1, thirdLevelKey: "", matchedAccordionsKeys: [], nonEdiMatchedAccordionsKeys: [],
      thirdLevelIdx: -1, secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "",
      changedPicklists: [], newPicklists: [], removedMetaKeys: [], metaView: true, nonEdiAc: false, edit: true, loading: false,
      pickListItems: [], isSaveDisabled: true, activeItem: [0], changeHistory: []
    }
  }

  async componentDidMount() {
    const { token } = this.props
    this.props.getConfigPicklist(token)
  }

  componentWillReceiveProps(nextProps) {
    const { config, picklistsMeta, loaded } = nextProps
    if (loaded) {
      if (loaded === "saved") {
        toast("Changes Saved", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
      } else if (loaded === "error") {
        toast("Error in saving changes", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
      }
      console.log("setting data in componentWillReceiveProps")
      this.setDataInState(config, picklistsMeta)
    }
  }

  componentWillUnmount() {
    console.log("in componentWillUnmount")
    const { token, entityId } = this.props
    const { accordions, picklistsMeta, newPicklists,
      changedPicklists, removedMetaKeys } = this.state
    this.props.saveConfigPicklist(token, entityId, {
      picklists: accordions,
      picklistsMeta
    }, {
      new: newPicklists, changed: changedPicklists,
      removed: removedMetaKeys
    }, [], "redux-store")
  }

  setDataInState = (data, picklistsMeta) => {
    if (!data || !Object.keys(data).length) {
      data = this.initialAccordionsData()
    }
    if (!picklistsMeta || !Object.keys(picklistsMeta).length) {
      picklistsMeta = this.initialPicklistsMeta()
    }

    const ediTablePicklist = {}
    const nonEdiTablePicklist = {}
    Object.keys(picklistsMeta).filter(key => {
      if(picklistsMeta && picklistsMeta[key] && picklistsMeta[key].editable){
        ediTablePicklist[key] = picklistsMeta[key]
      } else {
        nonEdiTablePicklist[key] = picklistsMeta[key]
      }
    })
    const matchedAccordionsKeys = Object.keys(ediTablePicklist)
    const nonEdiMatchedAccordionsKeys = Object.keys(nonEdiTablePicklist)
    const editableData = {}
    const nonEditableData = {}
    Object.keys(data).forEach(a => {
      if(matchedAccordionsKeys.indexOf(a) !== -1){
        editableData[a] = data[a]
      } else if(nonEdiMatchedAccordionsKeys.indexOf(a) !== -1){
        nonEditableData[a] = data[a]
      }
    })
    this.setState({
      allPicklistsMeta: picklistsMeta,
      accordions: editableData,
      picklistsMeta: ediTablePicklist,
      matchedAccordionsKeys,
      nonEdiAccordions: nonEditableData,
      nonEdiPicklistsMeta: nonEdiTablePicklist,
      nonEdiMatchedAccordionsKeys,
      validationError: {}
    })
  }

  setaccordionTitleToChange = (accordionTitleToChange) => {
    this.setState({ accordionTitleToChange })
  }

  toggleMetaView = (name) => {
    if(name === "meta"){
      this.setState(prevState => ({ metaView: !prevState.metaView }))
    } else {
      this.setState(prevState => ({ nonEdiAc: !prevState.nonEdiAc }))
    }
  }

  initialAccordionsData = () => ({
    "Add a title": [
      {
        label: "",
        included: true,
        visible: true,
        items: []
      }
    ]
  })

  initialPicklistsMeta = () => ({
    "Add a title": {
      key: ObjectID(),
      systemName: "",
      subListLevel2: false,
      subListLevel3: false,
      systemConfig: true,
      restrictedList: false,
      externalList: false,
      bestPractice: true
    }
  })

  removeAccordion = (id) => {
    const { systemName } = this.state
    const { token, userId, userFirstName, userLastName } = this.props
    const userName = `${userFirstName} ${userLastName}`
    swal({
      title: "Are you sure?",
      text: "Are you sure that you want to delete this picklist?",
      icon: "warning",
      dangerMode: true,
    }).then(async(willDelete) => {
      if (willDelete) {
        const changelog = { userId, userName, log: `${systemName} picklists deleted.`, date: new Date() }
        await updateAuditLog("config", [changelog])
        const data = await deleteConfigPicklist(token, id)
        if(data){
          this.setState({
            selectedPicklistKey: "", secondLevelKey: "", generalError: "", systemName: "", accordionTitleToChange: "",
            searchString: "", secondLevelIdx: -1, thirdLevelKey: "", thirdLevelIdx: -1,
            secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "", pickListItems: {}
          }, () => {
            this.props.getConfigPicklist(token)
          })
          swal("Deleted!", "Your picklist has been deleted!", "success");
        }
      }
    })
  }

  addAccordion = () => {
    this.setState(prevState => {
      const pickListItems = {}
      const newPicklists = [...prevState.newPicklists]
      const accordions = { ...prevState.accordions }
      const numKeys = Object.keys(accordions).length
      pickListItems.title = `Add Title ${numKeys}`
      pickListItems.items = this.initialAccordionsData()["Add a title"]
      pickListItems.meta = this.initialPicklistsMeta()["Add a title"]
      newPicklists.push(pickListItems.title)
      const activeItem = [2]
      return {
        config: {},
        pickListItems,
        newPicklists,
        selectedPicklistKey: pickListItems.title, secondLevelKey: "",
        accordionTitleToChange: "", searchString: "", metaView: false,
        generalError: "", systemName: "", newPicklist: true,
        secondLevelIdx: -1, thirdLevelKey: "", thirdLevelIdx: -1, activeItem,
        secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "", edit: true
      }
    })
  }

  updateKeyChanges = (prevKeyChanges, oldKey, newKey) => {
    console.log("prevKeyChanges : ", JSON.stringify(prevKeyChanges))
    console.log("oldKey : ", oldKey)
    console.log("newKey : ", newKey)

    let newKeyChanges = { ...prevKeyChanges }
    const oldKeys = Object.keys(prevKeyChanges)

    if (!oldKeys.length) {
      newKeyChanges[oldKey] = newKey
      return newKeyChanges
    }

    if (newKeyChanges.hasOwnProperty(newKey)) {
      newKeyChanges = {}
      console.log("changed to original value !!")
      oldKeys.forEach(k => {
        if (k !== newKey) {
          newKeyChanges[k] = prevKeyChanges[k]
        }
      })
      return newKeyChanges
    }
    let foundOldKey = false
    oldKeys.some(k => {
      if (prevKeyChanges[k] === oldKey) {
        console.log("found match for key : ", k)
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        foundOldKey = true
        newKeyChanges[k] = newKey
        console.log("newKeyChanges : ", JSON.stringify(newKeyChanges))
        return true
      }
    })
    if (!foundOldKey) {
      console.log("match not found : ", oldKey)
      newKeyChanges[oldKey] = newKey
    }

    return newKeyChanges
  }

  changeAccordionTitle = (e) => {
    const newKey = e.target.value
    console.log("newKey : ", newKey)
    this.setState(prevState => {
      const { accordionTitleToChange } = prevState

      if (accordionTitleToChange === newKey) {
        return {}
      }

      const accordionsKeys = Object.keys(prevState.accordions)
      if (accordionsKeys.includes(newKey)) {
        return { generalError: "Duplicate Title" }
      }

      const prevKeyChanges = { ...prevState.keyChanges }
      let keyChanges = {}
      const accordions = {}
      const picklistsMeta = {}
      const matchedAccordionsKeys = [...prevState.matchedAccordionsKeys]
      const newPicklists = [...prevState.newPicklists]
      const changedPicklists = [...prevState.changedPicklists]
      accordionsKeys.forEach(k => {
        // console.log("k : ", k)
        // console.log("accordionTitleToChange : ", accordionTitleToChange)
        if (k === accordionTitleToChange) {
          // console.log("doing new key")
          accordions[newKey] = prevState.accordions[accordionTitleToChange]
          picklistsMeta[newKey] = prevState.picklistsMeta[accordionTitleToChange]
          keyChanges = this.updateKeyChanges(prevKeyChanges, accordionTitleToChange, newKey)
          const i = matchedAccordionsKeys.indexOf(k)
          matchedAccordionsKeys[i] = newKey
          const j1 = newPicklists.indexOf(k)
          newPicklists[j1] = newKey
          const k1 = changedPicklists.indexOf(k)
          changedPicklists[k1] = newKey
        } else {
          // console.log("doing old key")
          accordions[k] = prevState.accordions[k]
          picklistsMeta[k] = prevState.picklistsMeta[k]
        }
      })
      // console.log("accordions : ", accordions);
      // console.log("picklistsMeta : ", picklistsMeta);
      return {
        accordions, accordionTitleToChange: newKey, keyChanges, newPicklists,
        selectedPicklistKey: newKey, picklistsMeta, matchedAccordionsKeys,
        generalError: "", changedPicklists
      }
    })
  }

  changeSelectedPicklistKey = (selectedPicklistKey, systemName, edit) => {
    const { token, loaded } = this.props
    const { picklistsMeta, changedPickList } = this.state
    let level2 = false
    let level3 = false
    if(edit) {
      level2 = picklistsMeta[selectedPicklistKey] && picklistsMeta[selectedPicklistKey].subListLevel2 || false
      level3 = picklistsMeta[selectedPicklistKey] && picklistsMeta[selectedPicklistKey].subListLevel3 || false
    }

    this.setState({
      loading: loaded
    },async () =>  {
      const data = await getSpecificPicklist(token, systemName)

      if(changedPickList === (data.picklists[0] && data.picklists[0].title)) {
        if(level2) {
          data.picklists[0] && data.picklists[0].items.forEach((e, i) => {
            data.picklists[0] = { ...data.picklists[0] }
            if (!e.items.length) {
              e.items.push({
                label: "Add a label",
                included: true,
                visible: true,
                items: []
              })
            }
          })
          data.picklists[0].meta.subListLevel2 = true
        }

        if(level3) {
          if(!level2) {
            return { generalError: "Please select 2nd Level first" }
          }
          data.picklists[0] && data.picklists[0].items.forEach((e, i) => {
            data.picklists[0] = { ...data.picklists[0] }

            e && e.items.forEach((f, j) => {
              e.items[j] = { ...e.items[j]}
              if (!e.items.length) {
                e.items.push({
                  label: "Add a label",
                  included: true,
                  visible: true,
                  items: []
                })
              }
            })
          })
          data.picklists[0].meta.subListLevel3 = true
        }
      }
      if(edit) {
        if(!level2) {
          data.picklists[0] && data.picklists[0].items &&
          data.picklists[0].items.forEach((e) => { e.items = [] })
          data.picklists[0].meta.subListLevel2 = false
        }
        if(!level3) {
          data.picklists[0] && data.picklists[0].items &&
          data.picklists[0].items.forEach((e) => {
            e && e.items && e.items.forEach((f) => {
              f.items = []
            })
          })
          data.picklists[0].meta.subListLevel3 = false
        }
      }
      const toggleAccordion = level2 ? [2,3] : [2,3,4]

      this.setState(prevState => {
        const changedPicklists = [...prevState.changedPicklists]
        if (!changedPicklists.includes(selectedPicklistKey)) {
          changedPicklists.push(selectedPicklistKey)
        }
        return {
          pickListItems: data.picklists[0],
          config: cloneDeep(data.picklists[0]),
          loading: !loaded, validationError: {},
          selectedPicklistKey, secondLevelKey: "",
          accordionTitleToChange: "", generalError: "", systemName,
          secondLevelIdx: -1, thirdLevelKey: "", thirdLevelIdx: -1,
          secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "",
          changedPicklists, metaView: false, nonEdiAc: false, edit, activeItem: toggleAccordion
        }
      })
    })
  }

  changeSystemName = (systemName) => {
    this.setState(prevState => {
      const picklistsMeta = { ...prevState.picklistsMeta }
      const key = prevState.selectedPicklistKey
      picklistsMeta[key] = { ...picklistsMeta[key], systemName }
      return { picklistsMeta, systemName }
    })
  }

  searchPicklists = (allPicklists, searchString) => {
    const keys = Object.keys(allPicklists)
    searchString = searchString.toLowerCase()
    const matching = {}
    keys.forEach(i => {
      if (i.toLowerCase().includes(searchString)) {
        matching[i] = allPicklists[i]
      } else if (allPicklists[i].systemName.toLowerCase().includes(searchString)) {
        matching[i] = allPicklists[i]
      }
    })

    const ediTablePicklist = {}
    const nonEdiTablePicklist = {}

    Object.keys(matching).filter(key => {
      if(matching && matching[key] && matching[key].editable){
        ediTablePicklist[key] = matching[key]
      } else {
        nonEdiTablePicklist[key] = matching[key]
      }
    })

    const matchedAccordionsKeys = Object.keys(ediTablePicklist)
    const nonEdiMatchedAccordionsKeys = Object.keys(nonEdiTablePicklist)

    return { ediTablePicklist, nonEdiTablePicklist, matchedAccordionsKeys, nonEdiMatchedAccordionsKeys }
  }

  changeSearchString = (e) => {
    const searchString = e.target.value
    this.setState({ searchString }, searchString ? null : this.performSearch)
  }

  handleKeyPressinSearch = (e) => {
    if (e.key === "Enter") {
      this.performSearch()
    }
  }

  performSearch = () => {
    console.log("performSearch")
    const { token } = this.props
    this.props.getConfigPicklist(token, "", this.state.searchString)
    this.setState({
      selectedPicklistKey: "", secondLevelKey: "",
      accordionTitleToChange: "", generalError: "", systemName: "",
      secondLevelIdx: -1, thirdLevelKey: "", thirdLevelIdx: -1,
      secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "",
      activeItem: [0, 1], pickListItems: {}
    })
  }

  changePicklistMeta = async (event, key, k, systemName) => {
    const val = event && event.target.checked
    let secondLevelClearConfirmation = ""
    let thirdLevelClearConfirmation = ""

    this.setState(prevState => {
      const picklistsMeta = { ...prevState.picklistsMeta }
      if (k === "subListLevel2") {
        if (val) {
          // pickListItems.meta.subListLevel2 = true
          picklistsMeta[key].subListLevel2 = true
        } else {
          secondLevelClearConfirmation = key
          if (picklistsMeta[key].subListLevel3) {
            thirdLevelClearConfirmation = key
            picklistsMeta[key] = { ...picklistsMeta[key] }
            picklistsMeta[key].subListLevel3 = false
          }
        }
      }

      if (k === "subListLevel3") {
        if (val) {
          if (!picklistsMeta[key].subListLevel2) {
            return { generalError: "Please select 2nd Level first" }
          }
          picklistsMeta[key].subListLevel3 = true

        } else {
          thirdLevelClearConfirmation = key
        }
      }

      picklistsMeta[key] = { ...picklistsMeta[key] }
      picklistsMeta[key][k] = val
      const changedPicklists = [...prevState.changedPicklists]
      if (!changedPicklists.includes(key)) {
        changedPicklists.push(key)
      }
      return {
        picklistsMeta, secondLevelClearConfirmation, systemName, changedPickList: key,
        thirdLevelClearConfirmation, changedPicklists, generalError: ""
      }
    })
  }

  clearNestedChecklist = () => {
    const { pickListItems, picklistsMeta, secondLevelClearConfirmation, thirdLevelClearConfirmation } = this.state

    if (secondLevelClearConfirmation) {
      // const name = picklistsMeta[secondLevelClearConfirmation].systemName
      this.setState(prevState =>
        // picklistsMeta[secondLevelClearConfirmation].subListLevel2 = false
        ({ pickListItems, picklistsMeta, secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "" })
      )
    }
    if (thirdLevelClearConfirmation && !secondLevelClearConfirmation) {
      // const name = picklistsMeta[secondLevelClearConfirmation].systemName
      this.setState(prevState =>
        // picklistsMeta[thirdLevelClearConfirmation].subListLevel3 = false
        ({ pickListItems, picklistsMeta, secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "" })
      )
    }
  }

  cancelClearNestedChecklist = () => {
    this.setState(prevState => {
      const { secondLevelClearConfirmation, thirdLevelClearConfirmation } = prevState
      const key = secondLevelClearConfirmation || thirdLevelClearConfirmation
      const picklistsMeta = { ...prevState.picklistsMeta }
      picklistsMeta[key] = { ...picklistsMeta[key] }
      picklistsMeta[key].subListLevel2 = true
      if (thirdLevelClearConfirmation) {
        picklistsMeta[key].subListLevel3 = true
      }
      return { picklistsMeta, secondLevelClearConfirmation: "", thirdLevelClearConfirmation: "" }
    })
  }

  saveConfigInDB = () => {
    const { changeLog, changedPicklists, allPicklistsMeta,
      selectedPicklistKey, pickListItems, systemName, newPicklist, changeHistory } = this.state
    const { token, userId, userFirstName, userLastName } = this.props
    const userName = `${userFirstName} ${userLastName}`
    console.log("changeLog : ", changeLog)
    let history = []
    changeHistory.forEach(row => {
      history.push({ userId, userName, log: row, date: new Date() })
    })
    let isSystemName = false
    if (newPicklist) {
      isSystemName = allPicklistsMeta && Object.keys(allPicklistsMeta).some((key) => allPicklistsMeta[key].systemName && allPicklistsMeta[key].systemName === systemName)
    }

    if(!systemName) {
      return toast("System Name can not be blank", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR })
    } else if (isSystemName) {
      return toast("System Name must be unique", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    }
    pickListItems.meta.systemName = systemName
    pickListItems.meta.editable = true
    pickListItems.title = selectedPicklistKey || changedPicklists
    this.setState(
      {
        isSaveDisabled: false
      },
      async () => {
        const res = await saveSpecificPicklist(token, pickListItems._id || "", pickListItems)
        const changelog = { userId, userName, log: `${systemName} picklists updated.`, date: new Date() }
        if(history && history.length){
          history = history.concat(changelog)
        }
        await updateAuditLog("config", history && history.length ? history : [changelog])
        if(res) {
          toast("PickList Changes Updated", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
          this.setState({
            pickListItems: res && res.picklists[0],
            isSaveDisabled: true,
            newPicklist: false,
          },
          () => this.props.getConfigPicklist(token)
          )
        }
      }
    )
  }

  newItemLog = (e) => {
    let logMessage = ""
    if (!e) {
      return logMessage
    }
    Object.keys(e).forEach(k => {
      if (k !== "_id") {
        logMessage += ` ${k} - ${e[k]}`
      }
    })
    return logMessage
  }

  checkElemChange = (newItem, oldItem) => {
    let elemChange = ""
    Object.keys(newItem).forEach(k => {
      if (newItem[k] !== oldItem[k]) {
        elemChange += ` ${k} - ${oldItem[k]} to ${newItem[k]}`
      }
    })
    return elemChange
  }

  logAndSave = () => {
    this.setState({ accordionTitleToChange: "" }, this.saveConfigInDB)
  }

  saveData = () => {
    console.log("in saveData")
    const { validationError } = this.state
    // const errKeys = Object.keys(validationError)
    // console.log("errKeys : ", errKeys)
    // let save = true
    // if(errKeys.length) {
    //   let error = false
    //   errKeys.some(i => {
    //     const itemKeys = Object.keys(validationError[i])
    //     console.log("itemKeys : ", itemKeys)
    //     if(!itemKeys.length) {
    //       return true
    //     }
    //     itemKeys.some(j => {
    //       const subItemKeys = Object.keys(validationError[i][j])
    //       console.log("subItemKeys : ", subItemKeys)
    //       if(!subItemKeys.length) {
    //         return true
    //       }
    //       subItemKeys.some(k => {
    //         console.log("subItem : ", validationError[i][j][k])
    //         if(Object.getPrototypeOf(validationError[i][j][k]).constructor.name === "Object") {
    //           //console.log("validationError obj at 3rd level : ", validationError[i][j][k])
    //
    //         } else if(validationError[i][j][k]) {
    //           error = true
    //           return true
    //         }
    //       })
    //       if(error) {
    //         return true
    //       }
    //     })
    //     if(error) {
    //       return true
    //     }
    //   })
    //   if(error) {
    //     save = false
    //   }
    // }
    let error = false
    const errKeys = Object.keys(validationError)

    const checkErrorRecursively = (err) => {
      console.log("err1 : ", err)
      if (Object.getPrototypeOf(err).constructor.name === "Object") {
        const errKeys = Object.keys(err)
        console.log("errKeys : ", errKeys)
        if (!errKeys.length) {
          error = false
          return true
        }
        errKeys.some(k => checkErrorRecursively(err[k]))
      } else {
        console.log("err2 : ", err)
        if (err) {
          error = true
          return true
        }
        error = false
        return true
      }
    }

    if (!errKeys.length) {
      error = false
      // return true
    }
    errKeys.some(i => {
      const itemKeys = Object.keys(validationError[i])
      if (!itemKeys.length) {
        error = false
        return true
      }
      itemKeys.forEach(j => {
        const subItemKeys = Object.keys(validationError[i][j])
        if (!subItemKeys.length) {
          error = false
          return true
        }
        subItemKeys.some(k => checkErrorRecursively(validationError[i][j][k]))
      })
    })

    console.log("error : ", error)

    if (!error) {
      this.logAndSave()
    }
  }

  validateSubmition = () => {
    console.log("in validateSubmition")
    const {pickListItems} = this.state
    const data = pickListItems.items
    this.setState(prevState => {
      const { validationError } = prevState
      let newErr = {}

      Object.keys(data).forEach((a, i) => {
        if (!data[a].label) {
          newErr = {
            ...newErr,
            first: {
              ...newErr.first,
              [i] : "Required"
            }
          }
        }
        if (pickListItems.meta && pickListItems.meta.subListLevel2) {
          Object.keys(data[a].items).forEach((e, j) => {
            if (!data[a].items[e].label) {
              newErr = {
                ...newErr,
                second: {
                  ...newErr.second,
                  [j] : "Required"
                }
              }
            }
            if (pickListItems.meta && pickListItems.meta.subListLevel3) {
              Object.keys(data[a].items[e].items).forEach((f, k) => {
                if (!data[a].items[e].items[f].label) {
                  newErr = {
                    ...newErr,
                    third: {
                      ...newErr.third,
                      [k] : "Required"
                    }
                  }
                }
              })
            }
          })
        }
      })
      return { validationError: newErr }
    }, this.saveData)
  }

  // validateSubmitionTest() {
  //   this.setState(prevState => {
  //     const { accordions, picklistsMeta, validationError } = prevState
  //     const newErr = { ...validationError }
  //     Object.keys(accordions).forEach(a => {
  //       Object.keys(accordions[a]).forEach(k => {
  //         if(!accordions[a][k].label) {
  //           newErr[a] = { ...newErr[a] }
  //           newErr[a][k] = { ...newErr[a][k]}
  //           newErr[a][k].label = "No input provided"
  //         }
  //         if(picklistsMeta[a].subListLevel2) {
  //           Object.keys(accordions[a][k].items).forEach(e => {
  //             if(!accordions[a][k].items[e].label) {
  //               newErr[a] = { ...newErr[a] }
  //               newErr[a][k] = { ...newErr[a][k]}
  //               // newErr[a][k].items = [ ...newErr[a][k].items ]
  //               // newErr[a][k].items[e] = { ...newErr[a][k].items[e] }
  //               // newErr[a][k].items[e].label = "No input provided"
  //               newErr[a][k][e] = { ...newErr[a][k][e]}
  //               newErr[a][k][e].label = "No input provided"
  //             }
  //             if(picklistsMeta[a].subListLevel3) {
  //               Object.keys(accordions[a][k].items[e].items).forEach(f => {
  //                 if(!accordions[a][k].items[e].items[f].label) {
  //                   newErr[a] = { ...newErr[a] }
  //                   newErr[a][k] = { ...newErr[a][k]}
  //                   // newErr[a][k].items = [ ...newErr[a][k].items ]
  //                   // newErr[a][k].items[e] = { ...newErr[a][k].items[e] }
  //                   // newErr[a][k].items[e].items = [ ...newErr[a][k].items[e].items ]
  //                   // newErr[a][k].items[e].items[f] = { ...newErr[a][k].items[e].items[f] }
  //                   // newErr[a][k].items[e].items[f].label = "No input provided"
  //                   newErr[a][k][e] = { ...newErr[a][k][e]}
  //                   newErr[a][k][e][f] = { ...newErr[a][k][e][f]}
  //                   newErr[a][k][e][f].label = "No input provided"
  //                 }
  //               })
  //             }
  //           })
  //         }
  //       })
  //     })
  //     return { validationError: newErr }
  //   })
  // }

  saveAccordions = () => {
    console.log("in saveAccordions")
    this.validateSubmition()
  }

  changeIncluded = (key, i, event, j, level, type, third, thirdKey) => {
    const {pickListItems} = this.state
    const val = event.target.checked

    if(type === "second") {
      pickListItems.items.map((e => {
        if(e.label === level) {
          e.items [i] = { ...e.items [i], included: val }
        }
      }))
    } else if(type === "third") {
      pickListItems.items.map((e => {
        if(e.label === level) {
          e.items.map((item => {
            // item.items.map((data) => {
            item.items [i] = { ...item.items [i], included: val }
            // })
          }))
        }
      }))
    }
    else {
      pickListItems.items [i] = { ...pickListItems.items[i], included: val }
    }
    this.setState({
      pickListItems
    })
  }

  changeVisible = (key, i, event, j, level, type, third, thirdKey) => {
    const {pickListItems} = this.state
    const val = event.target.checked

    if(type === "second") {
      pickListItems.items.map((e => {
        if(e.label === level) {
          e.items [i] = { ...e.items [i], visible: val }
        }
      }))
    } else if(type === "third") {
      pickListItems.items.map((e => {
        if(e.label === level) {
          e.items.map((item => {
            // item.items.map((data) => {
            item.items [i] = { ...item.items [i], visible: val }
            // })
          }))
        }
      }))
    } else {
      pickListItems.items [i] = { ...pickListItems.items[i], visible: val }
    }
    this.setState({
      pickListItems
    })
  }

  changeLabel = (key, i, e, type) => {
    const {pickListItems} = this.state
    const { value } = e.target
    pickListItems.items [i] = { ...pickListItems.items[i], label: value }
    this.setState({
      pickListItems
    })
  }

  onBlur = (e, key, name) => {
    const { value } = e.target
    const { changeHistory } = this.state
    const row = `${key} in ${name} change to ${value}`
    changeHistory.push(row)
    this.setState({changeHistory})
  }

  changeSecondLabel = (event, key, secondLevelIdx, i, level) => {
    const {pickListItems} = this.state
    const { value } = event.target
    /* pickListItems.items.map((data => {
      if(data.label === level) {
        data.items [i] = { ...data.items [i], label: value }
      }
    })) */
    pickListItems.items[secondLevelIdx].items[i].label = value
    this.setState({
      pickListItems
    })
  }

  changeThirdLabel = (event, key, secondLevelIdx, thirdLevelIdx, i, level, thirdLevel) => {
    const {pickListItems} = this.state
    const { value } = event.target
    pickListItems.items[secondLevelIdx].items[thirdLevelIdx].items[i].label = value
    this.setState({
      pickListItems
    })

  }

  addAcccordionItem = (key, level, secondLevelIdx, thirdLevelIdx) => {
    // const secondLevelIdx = level === 2 ? levelIdx : -1
    // const thirdLevelIdx = level === 3 ? levelIdx : -1
    this.setState(prevState => {
      const pickListItems = cloneDeep(prevState.pickListItems)
      if (level === 3) {
        if (secondLevelIdx >= 0 && thirdLevelIdx >= 0) {
          pickListItems.items[secondLevelIdx] = cloneDeep(pickListItems.items[secondLevelIdx])
          pickListItems.items[secondLevelIdx].items = cloneDeep(pickListItems.items[secondLevelIdx].items)
          pickListItems.items[secondLevelIdx].items[thirdLevelIdx] = cloneDeep(pickListItems.items[secondLevelIdx].items[thirdLevelIdx])
          pickListItems.items[secondLevelIdx].items[thirdLevelIdx].items.unshift(this.initialAccordionsData()["Add a title"][0])
        }
      } else if (level === 2) {
        if (secondLevelIdx >= 0) {
          pickListItems.items[secondLevelIdx] = { ...pickListItems.items[secondLevelIdx] }
          pickListItems.items[secondLevelIdx].items = [...pickListItems.items[secondLevelIdx].items]
          pickListItems.items[secondLevelIdx].items.unshift(this.initialAccordionsData()["Add a title"][0])
        }
      } else {
        pickListItems.items.unshift(this.initialAccordionsData()["Add a title"][0])
      }
      return { pickListItems }
    })
  }

  resetAcccordion = (key, level, secondLevelIdx, thirdLevelIdx) => {
    console.log("params : ", key, level, secondLevelIdx, thirdLevelIdx)
    const {secondLevelKey, thirdLevelKey} = this.state
    this.setState((prevState, props) => {
      const pickListItems = cloneDeep(prevState.pickListItems)
      const picklistsMeta = { ...prevState.picklistsMeta }
      const config = { ...prevState.config }
      let validationError = { ...prevState.validationError }
      if (level === 3) {
        if (secondLevelIdx >= 0 && thirdLevelIdx >= 0) {
          pickListItems.items[secondLevelIdx] = cloneDeep(pickListItems.items[secondLevelIdx])
          pickListItems.items[secondLevelIdx].items = cloneDeep(pickListItems.items[secondLevelIdx].items)
          pickListItems.items[secondLevelIdx].items[thirdLevelIdx] = cloneDeep(pickListItems.items[secondLevelIdx].items[thirdLevelIdx])
          const secondLevelItems = config && config.items && config.items.find(s => s.label === secondLevelKey) || {}
          const configsLabel = Object.keys(secondLevelItems).length === 0 ? [] : (secondLevelItems && secondLevelItems.items && secondLevelItems.items.map(e => e.label.toLowerCase())) || []

          if ((configsLabel.includes(thirdLevelKey.toLowerCase()))) {
            const thirdLevelItems = secondLevelItems && secondLevelItems.items && secondLevelItems.items.find(a => a.label === thirdLevelKey) || {}
            pickListItems.items[secondLevelIdx].items[thirdLevelIdx].items = Object.keys(thirdLevelItems).length === 0 ?
              [this.initialAccordionsData()["Add a title"][0]] : thirdLevelItems && thirdLevelItems.items
          } else {
            pickListItems.items[secondLevelIdx].items[thirdLevelIdx].items = []
          }

          /* pickListItems.items[secondLevelIdx].items[thirdLevelIdx].items = config.items &&
            config.items[secondLevelIdx] && config.items[secondLevelIdx].items &&
              config.items[secondLevelIdx].items[thirdLevelIdx] && config.items[secondLevelIdx].items[thirdLevelIdx].items ?
            [...config.items[secondLevelIdx].items[thirdLevelIdx].items] : [this.initialAccordionsData()["Add a title"][0]] */

          if (validationError && validationError.third) {
            validationError.third = {}
          }
          /* Object.keys(validationError).forEach(k1 => {
            Object.keys(validationError[k1]).forEach(k2 => {
              Object.keys(validationError[k1][k2]).forEach(k3 => {
                Object.keys(validationError[k1][k2][k3]).forEach(k4 => {
                  if (Object.getPrototypeOf(validationError[k1][k2][k3][k4]).constructor.name === "Object") {
                    validationError[k1][k2][k3][k4] = {}
                  }
                })
              })
            })
          }) */
        }
      } else if (level === 2) {
        if (secondLevelIdx >= 0) {
          pickListItems.items[secondLevelIdx] = { ...pickListItems.items[secondLevelIdx] }
          const configsLabel = config && config.items && config.items.map(e => e.label.toLowerCase()) || []

          if ((configsLabel.includes(secondLevelKey.toLowerCase()))) {
            const secondLevelItems = config.items.find(a => a.label === secondLevelKey) || {}
            pickListItems.items[secondLevelIdx].items = Object.keys(secondLevelItems).length === 0 ?
              [this.initialAccordionsData()["Add a title"][0]] : secondLevelItems && secondLevelItems.items

            /* pickListItems.items[secondLevelIdx].items = config.items &&
            config.items[secondLevelIdx] && config.items[secondLevelIdx].items ?
              [...config.items[secondLevelIdx].items] : [this.initialAccordionsData()["Add a title"][0]] */
          } else {
            pickListItems.items[secondLevelIdx].items = []
          }
          if (validationError && validationError.second) {
            validationError.second = {}
          }
          /* Object.keys(validationError).forEach(k1 => {
            Object.keys(validationError[k1]).forEach(k2 => {
              Object.keys(validationError[k1][k2]).forEach(k3 => {
                if (Object.getPrototypeOf(validationError[k1][k2][k3]).constructor.name === "Object") {
                  validationError[k1][k2][k3] = {}
                }
              })
            })
          }) */
        }
      } else if (config && Object.keys(config).length) {
        pickListItems.items = [...config.items]
        picklistsMeta[key] = { ...config.meta }
        validationError = {}
      } else {
        pickListItems.items = this.initialAccordionsData()["Add a title"]
        picklistsMeta[key] = this.initialPicklistsMeta()["Add a title"]
        validationError = {}
      }

      return { pickListItems, validationError }
    })
    // this.setState({ accordions: data });
  }

  resetAllAcccordions = () => {
    const { token } = this.props
    this.props.getConfigPicklist(token)
    this.setState({
      secondLevelKey: "", accordionTitleToChange: "", metaView: true,
      keyChanges: {}, removedKeys: [], searchString: "",
      generalError: "", validationError: {}, systemName: "",
      selectedPicklistKey: "", secondLevelIdx: -1, thirdLevelKey: "",
      thirdLevelIdx: -1, secondLevelClearConfirmation: "",
      thirdLevelClearConfirmation: "", changedPicklists: [], newPicklists: []
    })
  }

  changeSecondLevelKey = (e) => {
    let {validationError} = this.state
    console.log("val : ", e.target.value)
    console.log("val : ", JSON.parse(e.target.value))
    const { idx, val } = JSON.parse(e.target.value)
    console.log("idx , val : ", idx, val)
    validationError = {
      ...validationError,
      second: {}
    }
    this.setState({ secondLevelIdx: idx, secondLevelKey: val, validationError })
  }

  changeThirdLevelKey = (e) => {
    let {validationError} = this.state
    // console.log("val : ", e.target.value)
    // console.log("val : ", JSON.parse(e.target.value))
    const { idx, val } = JSON.parse(e.target.value)
    // console.log("idx , val : ", idx, val)
    validationError = {
      ...validationError,
      third: {}
    }
    this.setState({ thirdLevelIdx: idx, thirdLevelKey: val, validationError })
  }

  renderTitleInputFieldWithIcon = (key) => {
    if (this.state.accordionTitleToChange === key) {
      return (
        <div className="field is-grouped is-pulled-left">
          <div className="w-75">
            <input autoFocus
              className="input is-small is-link"
              type="text"
              value={key}
              onChange={this.changeAccordionTitle} />
          </div>
          <div className="control">
            <i className="fa fa-times" onClick={() => this.setaccordionTitleToChange("")} />
          </div>
        </div>
      )
    }
    return (
      <div className="field is-grouped is-pulled-left">
        <div className="w-75">
          <strong>{key}</strong>
        </div>
        { this.state.edit ?
          <div className="control">
            <i className="fa fa-edit" onClick={() => this.setaccordionTitleToChange(key)} />
          </div> : null }
      </div>
    )
  };

  renderHeader = (key) => (
    <div className="field is-grouped">
      <div className="control">
        <button className="button is-link is-small" type="button"
          onClick={() => this.addAcccordionItem()} disabled={!this.state.edit}>Add
        </button>
      </div>
      <div className="control">
        <button className="button is-light is-small" type="button"
          onClick={() => this.resetAcccordion(key)} disabled={!this.state.edit}>Reset
        </button>
      </div>
      <div className="control">
        {this.state.edit ?
          <div className="control">
            <i className="fa fa-trash-o is-size-4 " onClick={() => this.removeAccordion(this.state.pickListItems && this.state.pickListItems._id)}/>
          </div> : null}
      </div>
    </div>
  );

  renderNestedListHeader = (key, level) => {
    const { secondLevelIdx, thirdLevelIdx, edit } = this.state
    // const levelIdx = level === 2 ? secondLevelIdx : thirdLevelIdx
    // const header = level === 2 ? `Configure 2nd Linked Sublist for ${key}` :
    //   `Configure 3rd Linked Sublist for ${key}`
    return (
      <div className="columns">
        <div className="column">
          <div className="level-right">
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-link is-small" type="button"
                  onClick={() => this.addAcccordionItem(key, level, secondLevelIdx, thirdLevelIdx)} disabled={!edit}>Add</button>
              </div>
              <div className="control">
                <button className="button is-light is-small" type="button"
                  onClick={() => this.resetAcccordion(key, level, secondLevelIdx, thirdLevelIdx)} disabled={!edit} >Reset</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderAccordionBodyItem = (e, key, i) => (
    <tr key={key + i}>
      <td className="emmaTablesTd">
        <input
          className="input is-small is-link" style={{fontSize:"12px"}}
          type="text"
          value={e.label}
          onChange={(event) => this.changeLabel(key, i, event)}
          onBlur={(e) => this.onBlur(e, `Screen Name ${key}`, "Name")}
          disabled={!this.state.edit}
        />
        {this.state.validationError && this.state.validationError.first && this.state.validationError.first[i] ?
          <small className="text-error has-text-danger">{this.state.validationError.first[i]}</small>
          : undefined
        }
      </td>
      <td style={{ "textAlign": "left" }} className="emmaTablesTd">
        <input
          className="checkbox"
          type="checkbox"
          checked={e.included}
          onChange={(event) => this.changeIncluded(key, i, event)}
          disabled={!this.state.edit}
        />
        {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].included ?
          <strong>{this.state.validationError[key][i].included}</strong>
          : undefined
        } */}
      </td>
      <td style={{ "textAlign": "left" }} className="emmaTablesTd">
        <input
          className="checkbox"
          type="checkbox"
          checked={e.visible}
          onChange={(event) => this.changeVisible(key, i, event)}
          disabled={!this.state.edit}
        />
        {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][i] &&
            this.state.validationError[key][i].visible ?
          <strong>{this.state.validationError[key][i].visible}</strong>
          : undefined
        } */}
      </td>
    </tr>
  )

  renderSecondLevelAccordionBodyItem = (e, key, secondLevelIdx, secondLevelKey, i) => {
    const {edit} = this.state
    return (
      <tr key={key + i}>
        <td className="emmaTablesTd">
          <input
            className="input is-small is-link"
            type="text"
            value={e.label}
            disabled={!edit}
            onChange={(event) => this.changeSecondLabel(event, key, secondLevelIdx, i, secondLevelKey)}
          />
          {this.state.validationError && this.state.validationError.second && this.state.validationError.second[i] ?
            <small className="text-error has-text-danger">{this.state.validationError.second[i]}</small>
            : undefined
          }
          {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][secondLevelIdx] &&
            this.state.validationError[key][secondLevelIdx][i] && this.state.validationError[key][secondLevelIdx][i].label ?
            <strong>{this.state.validationError[key][secondLevelIdx][i].label}</strong>
            : undefined
          } */}
        </td>
        <td style={{ "textAlign": "left" }} className="emmaTablesTd">
          <input
            className="checkbox"
            type="checkbox"
            checked={e.included}
            disabled={!edit}
            onChange={(event) => this.changeIncluded( key, i, event, secondLevelIdx, secondLevelKey, "second")}
          />
          {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][secondLevelIdx] &&
            this.state.validationError[key][secondLevelIdx][i] && this.state.validationError[key][secondLevelIdx][i].included ?
            <strong>{this.state.validationError[key][secondLevelIdx][i].included}</strong>
            : undefined
          } */}
        </td>
        <td style={{ "textAlign": "left" }} className="emmaTablesTd">
          <input
            className="checkbox"
            type="checkbox"
            checked={e.visible}
            disabled={!edit}
            onChange={(event) => this.changeVisible(key, i, event, secondLevelIdx, secondLevelKey, "second")}
          />
          {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][secondLevelIdx] &&
            this.state.validationError[key][secondLevelIdx][i] && this.state.validationError[key][secondLevelIdx][i].visible ?
            <strong>{this.state.validationError[key][secondLevelIdx][i].visible}</strong>
            : undefined
          } */}
        </td>
      </tr>
    )
  }

  renderThirdLevelAccordionBodyItem = (e, key, secondLevelIdx,
    secondLevelKey, thirdLevelIdx, thirdLevelKey, i) => {
    const {edit} = this.state
    return (
      <tr key={key + i}>
        <td className="emmaTablesTd">
          <input
            className="input is-small is-link"
            type="text"
            value={e.label}
            disabled={!edit}
            onChange={(event) => this.changeThirdLabel(event, key, secondLevelIdx, thirdLevelIdx, i,secondLevelKey, thirdLevelKey)}
          />
          {this.state.validationError && this.state.validationError.third && this.state.validationError.third[i] ?
            <small className="text-error has-text-danger">{this.state.validationError.third[i]}</small>
            : undefined
          }
          {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][secondLevelIdx] &&
            this.state.validationError[key][secondLevelIdx][thirdLevelIdx] && this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i] &&
            this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i].label ?
            <strong>{this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i].label}</strong>
            : undefined
          } */}
        </td>
        <td style={{ "textAlign": "left" }} className="emmaTablesTd">
          <input
            className="checkbox"
            type="checkbox"
            checked={e.included}
            disabled={!edit}
            onChange={(event) => this.changeIncluded( key, i, event, secondLevelIdx, secondLevelKey, "third", thirdLevelIdx, thirdLevelKey )}
          />
          {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][secondLevelIdx] &&
            this.state.validationError[key][secondLevelIdx][thirdLevelIdx] && this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i] &&
            this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i].included ?
            <strong>{this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i].included}</strong>
            : undefined
          } */}
        </td>
        <td style={{ "textAlign": "left" }} className="emmaTablesTd">
          <input
            className="checkbox"
            type="checkbox"
            checked={e.visible}
            disabled={!edit}
            onChange={(event) => this.changeVisible(key, i, event, secondLevelIdx, secondLevelKey, "third", thirdLevelIdx, thirdLevelKey )}
          />
          {/* {this.state.validationError && this.state.validationError[key] && this.state.validationError[key][secondLevelIdx] &&
            this.state.validationError[key][secondLevelIdx][thirdLevelIdx] && this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i] &&
            this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i].visible ?
            <strong>{this.state.validationError[key][secondLevelIdx][thirdLevelIdx][i].visible}</strong>
            : undefined
          } */}
        </td>
      </tr>
    )
  }

  renderAccordionBody = (key) => {
    const {pickListItems} = this.state
    const pickListArray = pickListItems.items
    return (
      <div className="tbl-scroll">
        <div className="column is one-third is-pulled-left">
          <label className="label">System Name</label>
          <Combobox
            style={{"maxWidth": "350px"}}
            data={primaryPicklists}
            caseSensitive={false}
            minLength={1}
            disabled={!this.state.edit}
            value={this.state.systemName}
            onChange={this.changeSystemName}
            filter='contains'
          />
        </div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl " title="Picklist item name as it appears on the screen">Name</p>
              </th>
              <th>
                <p className="multiExpLbl " title="Item is part of picklist">Included</p>
              </th>
              <th>
                <p className="multiExpLbl " title="Item is visible in picklist">Visible</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {pickListArray.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
          </tbody>
        </table>
      </div>
    )
  }

  renderSecondLevelAccordionBody = (key) => {
    const {secondLevelIdx, secondLevelKey, pickListItems} = this.state
    const secondLevelItems = (secondLevelIdx >= 0) && pickListItems && pickListItems.items && pickListItems.items[secondLevelIdx] ? pickListItems.items[secondLevelIdx].items: []

    return (
      <div className="tbl-scroll">
        <div className="column is one-third is-pulled-left">
          <label className="label">Pick related level 1 picklist item</label>
          <div className="select is-link is-small">
            <select value={JSON.stringify({ idx: secondLevelIdx, val: secondLevelKey })}
              onChange={this.changeSecondLevelKey} >
              <option value={JSON.stringify({ idx: -1, val: "" })}>Pick Level 2 List Item</option>
              {
                pickListItems && pickListItems.items ? pickListItems.items.map((e, i) => (
                  <option key={i.toString()} value={JSON.stringify({ idx: i, val: e.label })}>{e.label}</option>
                )) : null
              }
            </select>
          </div>
        </div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl " title="Picklist item name as it appears on the screen">Name</p>
              </th>
              <th>
                <p className="multiExpLbl " title="Item is part of picklist">Included</p>
              </th>
              <th>
                <p className="multiExpLbl " title="Item is visible in picklist">Visible</p>
              </th>
            </tr>
          </thead>
          <tbody>
            {secondLevelItems.length ? secondLevelItems.map((e, i) => this.renderSecondLevelAccordionBodyItem(e, key, secondLevelIdx, secondLevelKey, i)) : null}
          </tbody>
        </table>
      </div>
    )
  }

  renderThirdLevelAccordionBody = (key) => {
    const {secondLevelIdx, secondLevelKey, thirdLevelKey, thirdLevelIdx, pickListItems} = this.state
    const secondLevelItems = (secondLevelIdx >= 0) && pickListItems && pickListItems.items && pickListItems.items[secondLevelIdx] ?  pickListItems.items[secondLevelIdx].items: []
    const thirdLevelItems = secondLevelItems && secondLevelItems.length && (secondLevelIdx >= 0) && (thirdLevelIdx >= 0) &&
        secondLevelItems[thirdLevelIdx] ? secondLevelItems[thirdLevelIdx].items : []

    return (
      <div className="tbl-scroll">
        <div className="column is one-third is-pulled-left">
          <label className="label">Pick related level 2 picklist item</label>
          <div className="select is-link is-small">
            <select value={JSON.stringify({ idx: thirdLevelIdx, val: thirdLevelKey })}
              onChange={this.changeThirdLevelKey}>
              <option value={JSON.stringify({ idx: -1, val: "" })}>Pick Level 3 List Item</option>
              {secondLevelItems &&
              secondLevelItems.map((e, i) => (
                <option key={i.toString()}
                  value={JSON.stringify({ idx: i, val: e.label })}>{e.label}</option>
              ))
              }
            </select>
          </div>
        </div>
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>
                <p className="multiExpLbl " title="Picklist item name as it appears on the screen">Name</p>
              </th>
              <th>
                <p className="multiExpLbl " title="Item is part of picklist">Included</p>
              </th>
              <th>
                <p className="multiExpLbl " title="Item is visible in picklist">Visible</p>
              </th>
            </tr>
          </thead>
          <tbody>
            { thirdLevelItems.map((e, i) => this.renderThirdLevelAccordionBodyItem(e, key, secondLevelIdx, secondLevelKey, thirdLevelIdx, thirdLevelKey, i))}
          </tbody>
        </table>
      </div>
    )
  }

  renderSelectedPicklist = (key) => {
    const {pickListItems} = this.state
    const pickListArray = pickListItems.items
    return (
      <div>
        {this.renderAccordionBody(key, pickListArray)}
      </div>
    )
  }

  highlightSearch = (string, searchQuery) => {
    const reg = new RegExp(
      searchQuery && searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
      "i"
    )
    return string.replace(reg, str => `<mark>${str}</mark>`)
  }

  renderPicklistMeta = (key, picklistMeta, i) =>
    // console.log("key : ", key)
    // console.log("picklistMeta : ", picklistMeta)
    (
      <tr key={key + i}>
        <td className="emmaTablesTd">
          {/* {key} */}
          <small dangerouslySetInnerHTML={{__html: this.highlightSearch(key || "-", this.state.searchString)}}/>
        </td>
        <td className="emmaTablesTd">
          {/* {picklistMeta.systemName} */}
          <small dangerouslySetInnerHTML={{__html: this.highlightSearch(picklistMeta.systemName || "-", this.state.searchString)}}/>
        </td>
        {/* {Object.keys(picklistMeta).filter(k => (k !== "systemName" && k !== "_id" && k !== "key")).map(k => (
          <td key={key + k} className="emmaTablesTd">
            <label className="checkbox">
              <input type="checkbox" checked={picklistMeta[k]}
                     onChange={(event) => this.changePicklistMeta(event, key, k, picklistMeta.systemName)} disabled={!picklistMeta.editable}/>
            </label>
          </td>
        ))} */}

        <td key={key + i + 2} className="emmaTablesTd">
          <label className="checkbox">
            <input type="checkbox" checked={picklistMeta && picklistMeta.subListLevel2}
              onChange={(event) => this.changePicklistMeta(event, key, "subListLevel2", picklistMeta.systemName)} disabled={!picklistMeta.editable}/>
          </label>
        </td>
        <td key={key + i + 3} className="emmaTablesTd">
          <label className="checkbox">
            <input type="checkbox" checked={picklistMeta && picklistMeta.subListLevel3}
              onChange={(event) => this.changePicklistMeta(event, key, "subListLevel3", picklistMeta.systemName)} disabled={!picklistMeta.editable}/>
          </label>
        </td>

        {picklistMeta.editable ?
          <td className="emmaTablesTd">
            {picklistMeta.restrictedList ?
              <span>Not Applicable</span>
              :
              <span className="has-text-link" title="Edit" style={{cursor: "pointer"}}
                onClick={() => this.changeSelectedPicklistKey(key, picklistMeta.systemName, true)}>
                <i className="fas fa-pencil-alt is-small"/>
              </span>
            }
          </td> :
          <td>
            <span className="has-text-link" title="View" style={{cursor: "pointer"}}
              onClick={() => this.changeSelectedPicklistKey(key, picklistMeta.systemName, false)}>
              <i className="far fa-eye"/>
            </span>
          </td>
        }
      </tr>
    )


  renderPicklistsMetaHeaders = () => (
    <thead>
      <tr>
        <th>
          <p className="multiExpLbl " title="Picklist screen name or label">Screen Name</p>
        </th>
        <th>
          <p className="multiExpLbl " title="Picklist reference name in database">System Name</p>
        </th>
        <th>
          <p className="multiExpLbl " title="Picklist has a linked secondary sublist">2nd Sublist</p>
        </th>
        <th>
          <p className="multiExpLbl " title="Picklist has a linked tertiary sublist">3rd Sublist</p>
        </th>
        {/* <th>
          <p className="multiExpLbl " title="Picklist is auto-populated by system">System</p>
        </th>
        <th>
          <p className="multiExpLbl " title="Admin user configuration is restricted">Restricted</p>
        </th>
        <th>
          <p className="multiExpLbl " title="Picklist is populated from external data source">External Data</p>
        </th>
        <th>
          <p className="multiExpLbl " title="Picklist driven by external compliance consultant or industry best practice">Best Practice</p>
        </th>
        <th>
          <p className="multiExpLbl ">Update</p>
        </th> */}
      </tr>
    </thead>
  )

  renderSimpleHeader = (header, name) => (
    <div className="accordion-header toggle"
      onClick={() => this.toggleMetaView(name)}
      onDoubleClick={() => this.toggleMetaView(name)}>
      <p>{header}</p>
    </div>
  )

  renderPicklists =(matchedAccordionsKeys, picklistsMeta) => {
    const { searchString } = this.state
    if ((!searchString && !matchedAccordionsKeys.length) || !this.props.loaded) {
      return <Loader />
    }

    return (
      <div className="tbl-scroll">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          {this.renderPicklistsMetaHeaders()}
          {searchString && !matchedAccordionsKeys.length
            ? <strong>No match found</strong> :
            <tbody>
              {matchedAccordionsKeys.map((a, i) => this.renderPicklistMeta(a, picklistsMeta[a], i))}
            </tbody>
          }
        </table>
      </div>
    )
  }

  nonEdiRenderPicklists = (nonEdiMatchedAccordionsKeys, nonEdiPicklistsMeta) => {
    const { searchString } = this.state
    return (
      <div className="tbl-scroll">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          {this.renderPicklistsMetaHeaders()}
          {searchString && !nonEdiMatchedAccordionsKeys.length
            ? <strong>No match found</strong> :
            <tbody>
              {nonEdiMatchedAccordionsKeys.map((a, i) => this.renderPicklistMeta(a, nonEdiPicklistsMeta[a], i))}
            </tbody>
          }
        </table>
      </div>
    )
  }

  renderSecondSublist = (key, accordion) => (
    <div>
      {this.renderSecondLevelAccordionBody(key, accordion)}
    </div>
  )

  renderThirdSublist = (key, accordion) => (
    <div>
      {this.renderThirdLevelAccordionBody(key, accordion)}
    </div>
  )

  render() {
    const { matchedAccordionsKeys, accordions, pickListItems, picklistsMeta, selectedPicklistKey, nonEdiMatchedAccordionsKeys, nonEdiAccordions, nonEdiPicklistsMeta,
      searchString, secondLevelClearConfirmation, thirdLevelClearConfirmation, edit, isSaveDisabled, activeItem  } = this.state
    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }
    return (
      <section className="accordions box">
        <div className="columns">
          <div className="column is-11">
            <span className="control has-icons-left">
              <input className="input is-link is-small"
                type="text"
                placeholder="search by screen label, picklist value or name (e.g. doc or sector)"
                value={searchString}
                onChange={this.changeSearchString}
                onKeyPress={this.handleKeyPressinSearch}
              />
              <span className="icon is-left has-background-dark" style={{ marginTop: -2 }}>
                <i className="fas fa-search" />
              </span>
            </span>
          </div>
          <div className="column is-1">
            <button className="button is-link is-small" onClick={() => this.performSearch()}>Search</button>
          </div>
        </div>
        <Accordion
          multiple
          isRequired={!!activeItem.length}
          activeItem={activeItem}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Possible Matches"
              >
                {activeAccordions.includes(0) && (
                  <div>
                    {this.renderPicklists(matchedAccordionsKeys, picklistsMeta)}
                  </div>
                )}
              </RatingSection>
              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Non Editable Possible Matches"
              >
                {activeAccordions.includes(1) && (
                  <div>
                    {this.nonEdiRenderPicklists(nonEdiMatchedAccordionsKeys, nonEdiPicklistsMeta)}
                  </div>
                )}
              </RatingSection>
              {
                selectedPicklistKey ?
                  <RatingSection
                    onAccordion={() => onAccordion(2)} style={{ overflowY: "unset" }}
                    actionButtons={this.renderHeader(selectedPicklistKey)}
                    title={this.renderTitleInputFieldWithIcon(selectedPicklistKey)}
                  >
                    {activeAccordions.includes(2) && (
                      <div>
                        {selectedPicklistKey && this.renderSelectedPicklist(selectedPicklistKey,
                          edit ? accordions[selectedPicklistKey] : nonEdiAccordions[selectedPicklistKey], picklistsMeta[selectedPicklistKey])}
                      </div>
                    )}
                  </RatingSection>
                  : null
              }

              {
                pickListItems && pickListItems.meta && pickListItems.meta.subListLevel2 &&
                  <RatingSection
                    onAccordion={() => onAccordion(3)}
                    actionButtons={this.renderNestedListHeader(selectedPicklistKey, 2)}
                    title={`Configure 2nd Linked Sublist for ${selectedPicklistKey}`}
                  >
                    {activeAccordions.includes(3) && (
                      <div>
                        {pickListItems && pickListItems.meta && pickListItems.meta.subListLevel2 &&
                        this.renderSecondSublist(selectedPicklistKey,  edit ? accordions[selectedPicklistKey] : nonEdiAccordions[selectedPicklistKey], picklistsMeta[selectedPicklistKey])}
                      </div>
                    )}
                  </RatingSection>
              }

              {
                pickListItems && pickListItems.meta && pickListItems.meta.subListLevel3 &&
                <RatingSection
                  onAccordion={() => onAccordion(4)}
                  actionButtons={this.renderNestedListHeader(selectedPicklistKey, 3)}
                  title={`Configure 3rd Linked Sublist for ${selectedPicklistKey}`}
                >
                  {activeAccordions.includes(4) && (
                    <div>
                      {pickListItems && pickListItems.meta && pickListItems.meta.subListLevel3 &&
                      this.renderThirdSublist(selectedPicklistKey,  edit ? accordions[selectedPicklistKey] : nonEdiAccordions[selectedPicklistKey], picklistsMeta[selectedPicklistKey])}
                    </div>
                  )}
                </RatingSection>
              }

            </div>
          )}
        />

        <div className={(secondLevelClearConfirmation ||
          thirdLevelClearConfirmation ? "modal is-active" : "modal")}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <button className="delete" onClick={this.cancelClearNestedChecklist} />
            </header>
            <section className="modal-card-body">
              <p>Unchecking this will delete any existing elements at this level</p>
            </section>
            <footer className="modal-card-foot">
              <button onClick={this.clearNestedChecklist}>Proceed</button>
              <button onClick={this.cancelClearNestedChecklist}>Cancel</button>
            </footer>
          </div>
        </div>
        <strong>{this.state.generalError}</strong>
        <div className="field is-grouped-center">
          <div>
            <button style={{ margin: 10 }} className="button is-link " onClick={this.addAccordion}>Add new picklist</button>
            <button style={{ margin: 10 }} className="button is-link " onClick={this.resetAllAcccordions} disabled={!edit}>Undo All Changes</button>
          </div>
          <button style={{ margin: 10 }} className="button is-link " onClick={this.saveAccordions} disabled={!edit || !isSaveDisabled || !selectedPicklistKey}>Save</button>
        </div>
      </section>
    )
  }
}

const mapStateToProps = ({ configPicklist, auth: { token, loginDetails: { userEntities } } }) => {
  console.log("configPicklist in state : ", configPicklist)
  const { entityId, userId, userFirstName, userLastName } = userEntities[0]
  return {
    token, entityId, userId, userFirstName, userLastName,
    config: configPicklist.picklists || {},
    picklistsMeta: configPicklist.picklistsMeta || {},
    loaded: configPicklist.loaded
  }
}

export default connect(mapStateToProps, { getConfigPicklist, saveConfigPicklist })(Picklists)
