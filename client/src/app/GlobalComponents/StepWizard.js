import React from 'react'
import { withRouter } from "react-router-dom"
import styled from "styled-components"

const StepContainerStyle = {
  height: 'calc(100vh - 60px)',
  display: 'flex'
}

const SelectedStepLabelStyle = {
  fontWeight: 500,
  borderLeft: '2px solid #209cee',
  padding: '5px'
}

const StepBody = styled.div`
flex: 1;
overflow: auto;
`

class StepWizard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...props };
  }

  componentDidMount() {
    this.setState({
      selectedStep: { ...this.state.steps[0] },
      selectedStepIndex: 0
    });
  }

  stepChangeHandler = () => {
    if (this.state.selectedStep.nextClickEvent()) {
      let { selectedStepIndex } = this.state;
      const selectedContainer = { ...this.state.steps[++selectedStepIndex] };
      this.setState({
        selectedStep: selectedContainer,
        selectedStepIndex: selectedStepIndex
      });
    }
  }

  cancelWizardHandler = () => {
    this.props.history.goBack();
  }

  render() {
    console.log(this.state.currentStepParent);
    return (
      <div className="column" style={StepContainerStyle}>
        <div className="column is-3" style={{ display: "inline-block", borderRight: '1px solid #d4cccc' }}>
          <ul>
            {this.state.steps.map((step, index) => {
              return (
                <li key={index} style={index === this.state.selectedStepIndex ? SelectedStepLabelStyle : { padding: '5px' }}>{step.label}</li>
              )
            })}
          </ul>
        </div>
        <div className="column is-9" style={{ display: "flex", flexDirection: 'column' }}>
          <StepBody>
            {this.state.selectedStep ? this.state.selectedStep.body : null}
          </StepBody>
          <div>
            <button className="button is-pulled-left" onClick={this.cancelWizardHandler}>Cancel</button>
            {this.state.selectedStepIndex === this.state.steps.length - 1 ?
              <button className="button is-info is-pulled-right" onClick={this.stepChangeHandler}>Submit</button>
              :
              <button className="button is-info is-pulled-right" onClick={this.stepChangeHandler}>Next</button>
            }

          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(StepWizard)