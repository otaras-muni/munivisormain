import React from "react"
import swal from "sweetalert"
import DropDownListClear from "./DropDownListClear"
import { SelectLabelInput } from "./TextViewBox"
import { getUserTransactions } from "../StateManagement/actions/CreateTransaction"
import Loader from "./Loader"
import CONST from "../../globalutilities/consts"

class ReletedTranGlob extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      transactions: [],
      loading: false,
      waiting: false,
      activityType: "",
      searchString: "",
      confirmAlert: CONST.confirmAlert
    }
  }

  onChangeGetTransactions = event => {
    const { name, value } = event.target
    const {searchString} = this.state
    const { item, relatedTransactions, tranId } = this.props

    this.setState(
      {
        [name]: value,
        transactions: [],
        loading: true
      },
      async () => {
        if (value) {
          let transactions = await getUserTransactions(`${value}?searchTerm=${searchString || ""}&tranId=${tranId}`)
          const actTranRelatedTo = relatedTransactions || []
          const actTranRelatedType =
            item.actTranRelatedType || item.rfpTranRelatedType || ""

          const related = actTranRelatedTo
            .filter(r => r.relType === actTranRelatedType)
            .map(r => r.relTranId)

          transactions = transactions.filter(e => related && related.indexOf(e.id) === -1)

          this.setState({
            transactions,
            loading: false,
          },
          () => {
            this.props.onRelatedTranSelect([])
          })
        } else {
          this.setState({
            loading: false,
            transactions: [],
          },() => this.props.onRelatedTranSelect([]))
        }
      }
    )
  }

  onRelatedTranSelect = tran => {
    const subType =  (tran && tran.activityTranSubType === "Bond Issue" || tran && tran.activityTranSubType === "Bank Loan" || tran && tran.activityTranSubType === "Lines and Letter" ||
      tran && tran.activityTranSubType === "Letter Of Credit" || tran && tran.activityTranSubType === "Lines Of Credit")
    const item = {
      id: tran.activityId || tran.id || "",
      name: tran.activityDescription || tran.name || "",
      relTranId: tran.activityId || tran.relTranId || "",
      relTranIssueName: tran.activityIssueName || tran.relTranIssueName || "",
      relTranProjectName: tran.activityDescription || tran.relTranProjectName || "",
      relTranClientId: tran.activityTranClientId || tran.relTranClientId || "",
      relTranClientName: tran.activityClientName || tran.relTranClientName || "",
      relType: this.props.item.actTranRelatedType || tran.relType || "",
      tranType: subType ? tran.activityTranSubType : tran && tran.activityTranType || ""
    }
    this.props.onRelatedTranSelect(item)
  }

  onChange = e => {
    this.props.onChange(e)
    this.setState({
      activityType: "",
      transactions: [],
    })
  }

  onCancel = () => {
    this.setState(
      {
        activityType: "",
        transactions: [],
      },
      () => this.props.onRelatedtranCancel()
    )
  }

  onRemove = (related, index) => {
    const { confirmAlert } = this.state
    confirmAlert.text = `You want to delete this ${related &&
    related.relTranProjectName}?`
    swal(confirmAlert).then(willDelete => {
      if (willDelete) {
        if (related) {
          this.props.deleteRelatedTran(related._id || index, related.relTranId, related.relType, related.tranType) //eslint-disable-line
        }
      }
    })
  }

  onSearch = (searchString) => {
    window.clearTimeout(this.timeout)
    this.setState({ searchString, waiting: true }, () => this.getSuggestions())
  }

  onSave = () => {
    this.setState({
      activityType: ""
    },() => this.props.onRelatedtranSave())
  }

  getSuggestions = async () => {

    const { searchString, activityType } = this.state
    const { item, relatedTransactions, tranId } = this.props
    let transactions = await getUserTransactions(`${activityType}?searchTerm=${searchString || ""}&tranId=${tranId || ""}`)

    const actTranRelatedTo = relatedTransactions || []
    const actTranRelatedType =
      item.actTranRelatedType || item.rfpTranRelatedType || ""

    const related = actTranRelatedTo
      .filter(r => r.relType === actTranRelatedType)
      .map(r => r.relTranId)

    transactions = transactions.filter(e => related && related.indexOf(e.id) === -1)

    this.setState({
      transactions,
      waiting: false
    })
  }

  render() {
    const { activityType, loading, transactions, waiting } = this.state
    const {
      relatedTypes,
      relatedTransactions,
      errorMessages,
      isSaveDisabled,
      canEditTran
    } = this.props
    let { item } = this.props

    item = {
      ...this.props.item,
      actTranRelatedTo:
        item.actTranRelatedTo ||
        item.actRelTrans ||
        item.rfpTranRelatedTo ||
        item.dealIssueTranRelatedTo ||
        "",
      actTranRelatedType:
        item.actTranRelatedType || item.rfpTranRelatedType || ""
    }

    if (loading) {
      return <Loader />
    }

    return (
      <div>
        {canEditTran ? (
          <div className="columns">
            <div className="column">
              <SelectLabelInput
                error={errorMessages.actTranRelatedType || ""}
                list={relatedTypes || []}
                name="actTranRelatedType"
                value={item.actTranRelatedType}
                onChange={this.onChange}
              />
            </div>
            <div className="column">
              <div className="select is-fullwidth is-link is-small">
                <select
                  name="activityType"
                  value={activityType || ""}
                  onChange={this.onChangeGetTransactions}
                >
                  <option value="">Select Transaction Type</option>
                  <option value="BankLoans">Bank Loan</option>
                  <option value="ActBusDev">Business Development</option>
                  <option value="Derivatives">Derivatives</option>
                  <option value="Deals">Deal</option>
                  <option value="ActMaRFP">MA-RFP</option>
                  <option value="Others">Others</option>
                  <option value="RFP">RFP</option>
                </select>
              </div>
            </div>
            <div className="column">
              {item.actTranRelatedType ? (
                <DropDownListClear
                  filter="contains"
                  busy={waiting}
                  data={transactions}
                  value={(item.actTranRelatedTo && item.actTranRelatedTo.name) || ""}
                  /* disabled={disable} */
                  isHideButton={item.actTranRelatedTo && item.actTranRelatedTo.name}
                  textField="name"
                  placeholder="Search by name"
                  onSearch={this.onSearch}
                  onSelect={this.onRelatedTranSelect}
                  onClear={() => {this.onRelatedTranSelect({})}}
                />
              ) : null}
            </div>
            <div className="column">
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className="button is-link is-small"
                    onClick={this.onSave}
                    disabled={
                      isSaveDisabled ||
                      // !item.actTranRelatedType ||
                      !item.actTranRelatedTo && !Object.keys(item.actTranRelatedTo).length ||
                      !(item.actTranRelatedTo && item.actTranRelatedTo.name) ||
                      false
                    }
                  >
                    Save
                  </button>
                </div>
                <div className="control">
                  <button
                    className="button is-light is-small"
                    onClick={this.onCancel}
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>Type</th>
              <th>Issue Name</th>
              <th>Project Name</th>
              { canEditTran ? <th>Delete</th> : null }
            </tr>
          </thead>
          <tbody>
            {relatedTransactions.length
              ? relatedTransactions.map((related, i) => (
                <tr key={i.toString()}>
                  <td className="">
                    <p className="multiExpLbl ">
                      {related.relType || related.type}
                    </p>
                  </td>
                  <td className="">
                    <p className="multiExpLbl ">
                      {related.relTranIssueName || "-"}
                    </p>
                  </td>
                  <td className=" is-two-thirds">
                    <p className="multiExpLbl ">
                      {related.relTranProjectName || "-"}
                    </p>
                  </td>
                  {canEditTran ?
                    <td>
                      <div className="field is-grouped">
                        <div className="control">
                          <a onClick={() => this.onRemove(related, i)}> {/* eslint-disable-line */}
                            {" "}
                            <span className="has-text-link">
                              <i className="far fa-trash-alt" title="Delete"/>
                            </span>
                          </a>
                        </div>
                      </div>
                    </td> :null
                  }
                </tr>
              ))
              : null}
          </tbody>
        </table>
      </div>
    )
  }
}

export default ReletedTranGlob
