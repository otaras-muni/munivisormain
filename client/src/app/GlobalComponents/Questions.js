import React, {Component} from "react"
import cloneDeep from "lodash.clonedeep"
import RatingSection from "./RatingSection"
import Accordion from "./Accordion"
import QuestionContent from "./QuestionContent"

class Questions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dropDown:{
        confirmation: ["Yes", "No"],
      },
      [this.props.category]: [],
      others:  {
        isNew: true,
        sectionHeader: "Others",
        sectionItems:[
          {
            sectionItemDesc: "",
            sectionAffirm: ""
          }
        ]
      },
    }
  }

  async componentWillMount() {
    const{ category, questions } = this.props
    this.setState({
      [category]: questions,
    })
  }

  onChange = (e, i, j) => {
    const {category} = this.props
    const {name, value} = e.target
    const row = cloneDeep(this.state[category])
    if(name === "sectionAffirm" && typeof j === "undefined"){
      row[i].sectionAffirm = value
    }else {
      row[i].sectionItems[j][name] = value
    }
    this.setState({[category]: row},() => this.props.onChangeItem(category, row,  value))
  }

  actionButtons = (key) => ( //eslint-disable-line
    <div className="field is-grouped">
      <div className="control">
        <button className="button is-link is-small" onClick={()=>this.onAdd(key)}>Add</button>
      </div>
    </div>
  )

  onRemove =(i, j) => {
    const {category} = this.props
    const questions = cloneDeep(this.state[category])
    if(questions[i]){
      if(questions[i].sectionItems.length > 1){
        questions[i].sectionItems.splice(j, 1)
      }else {
        questions.splice(i, 1)
      }
    }
    this.setState({
      [category]: questions
    },() => this.props.onChangeItem(category, questions))
  }

  onAdd = (key) => {
    const {others} = this.state
    const questions = this.state[key]
    const othersQuestion = questions.find(que => que.isNew)

    if(othersQuestion) {
      const existsIndex = questions.findIndex(que => que.isNew)
      othersQuestion.sectionItems.push({
        sectionItemDesc: "",
        sectionAffirm: ""
      })
      questions[existsIndex] = othersQuestion
    }else {
      const insertRow = cloneDeep(others)
      questions.push(insertRow)
    }

    this.setState({
      [key]: questions
    },() => this.props.onChangeItem(key, questions))
  }

  render () {
    const{ dropDown } = this.state
    const{ title, bodyTitle, category } = this.props

    return(
      <Accordion multiple activeItem={[0]} boxHidden render={({activeAccordions, onAccordion}) =>
        <div>
          {
            <RatingSection onAccordion={() => onAccordion(0)} title={title} style={{overflowY: "unset"}}>
              {activeAccordions.includes(0) &&
                  <div>
                    <p className="title innerPgTitle">
                      <strong>{bodyTitle || ""}</strong>
                    </p>
                    <div style={{overflowX: "auto"}}>
                      <QuestionContent {...this.props} onChange={this.onChange} dropDown={dropDown} onSave={this.props.onSave} onRemove={this.onRemove}/>
                    </div>
                  </div>
              }
            </RatingSection>
          }
        </div>
      }/>
    )
  }
}

export default Questions
