import React from "react"
import ReactTable from "react-table"
import moment from "moment"
import "react-table/react-table.css"
import DocModal from "../FunctionalComponents/docs/DocModal"
import { SelectLabelInput, DocDropDownSelect } from "./TextViewBox"
import DocUpload from "../FunctionalComponents/docs/DocUpload"
import DocLink from "../FunctionalComponents/docs/DocLink"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const DocumentList = ({
  documentsList,
  pageSizeOptions = [5, 10, 20, 50, 100],
  dropDown,
  newDropDown,
  isCompliance,
  isDisabled,
  isSaveDisabled,
  isTransaction,
  bucketName,
  isEditable,
  search,
  nav1,
  tranFirmId,
  user,
  transaction,
  tranAction,
  contextId,
  contextType,
  tenantId,
  errors,
  onChangeUpdate,
  onStatusChange,
  onFileAction,
  onDeleteAll,
  onUpdate,
  onEdit,
  onCancel,
  onSettingModal,
  onUploadSuccess
}) => {

  let columns = []
  const editableIndex = isEditable
  if (isCompliance) {
    columns = [
      {
        Header: "Filename",
        id: "docFileName",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="field is-grouped-left">
              {!isDisabled || tranAction && (tranAction.canEditDocument && tranAction.canClient) ?
                <DocUpload
                  onUploadSuccess={onUploadSuccess}
                  bucketName={bucketName}
                  docMeta={{
                    category: doc.docCategory,
                    subCategory: doc.docSubCategory,
                    type: doc.docType
                  }}
                  docId={doc.docAWSFileLocation}
                  versionMeta={{
                    uploadedBy: doc.createdUserName
                  }}
                  showFeedback
                  contextId={contextId}
                  contextType={contextType || "DEALS"}
                  tenantId={tenantId}
                /> : null
              }
              &nbsp;&nbsp;
              <DocLink docId={doc.docAWSFileLocation}/>
              {/* {!isDisabled && ( */}
              <DocModal
                onDeleteAll={onDeleteAll}
                documentId={doc._id}
                docMetaToShow={["category", "subCategory"]}
                versionMetaToShow={["uploadedBy"]}
                selectedDocId={doc.docAWSFileLocation}
                transaction={transaction}
                tranAction={tranAction}
                doc={doc}
                nav1={nav1}
                user={user}
              />
              {/* )} */}
            </div>
          )
        },
        sortMethod: (a, b) => a.docFileName.localeCompare(b.docFileName)
      },
      {
        id: "docType",
        Header: "Type",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {isEditable ? (
                <div className="complain-details">
                  <DocDropDownSelect
                    filter
                    data={dropDown && dropDown.docType || []}
                    value={doc.docType || ""}
                    textField="name"
                    valueField="id"
                    key="name"
                    onChange={value => onChangeUpdate("docType", doc, value, row.index)}
                  />
                  {errors.docType ? (<p className="text-error">{errors.docType}</p>) : null}
                </div>
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docType || "-", search)}}/>
              )}
            </div>
          )
        },
        sortMethod: (a, b) => a.docType.localeCompare(b.docType)
      },
      {
        id: "createdUserName",
        Header: "Last Updated By",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.createdUserName || "-", search)}}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.createdUserName.localeCompare(b.createdUserName)
      },
      {
        id: "LastUpdatedDate",
        Header: "Last Updated On",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{
                __html: highlightSearch(doc.LastUpdatedDate ?
                  moment(doc.LastUpdatedDate).format("MM.DD.YYYY hh:mm A") : doc.lastUpdatedDate ? moment(doc.lastUpdatedDate).format("MM.DD.YYYY hh:mm A") : "" || "-", search)
              }}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.LastUpdatedDate ? a.LastUpdatedDate.localeCompare(b.LastUpdatedDate) : a.lastUpdatedDate.localeCompare(b.lastUpdatedDate)
      },
      // {
      //   id: "docStatus",
      //   Header: "Status",
      //   accessor: item => item,
      //   Cell: row => {
      //     const doc = row.value
      //     isEditable = editableIndex === row.index
      //     return (
      /* <SelectLabelInput */
      // list={dropDown.docStatus}
      // name="docStatus"
      // value={doc.docStatus}
      // onChange={e => onStatusChange(e, doc)}
      // disabled={isDisabled || isEditable || false}
      // />
      // )
      // },
      // sortMethod: (a, b) => a.docStatus.localeCompare(b.docStatus)
      // },
      {
        id: "docAction",
        Header: "Action",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {!isEditable ? (
                <div className="select is-small is-fullwidth is-link">
                  <select
                    name="docAction"
                    value=""
                    onChange={e => onFileAction(e.target.value, doc)}
                    disabled={isDisabled || false}
                  >
                    <option value="">Pick</option>
                    <option>See version history</option>
                    <option disabled>Send to EMMA</option>
                    <option>Share document</option>
                  </select>
                </div>
              ) : null}
            </div>
          )
        }
      },
      {
        id: "edit",
        Header: "Edit",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {!isDisabled ?
                <div className="field is-grouped">
                  <div className="control">
                    <a onClick={isEditable ? () => onUpdate(doc) : () => onEdit("Documents", row.index, doc)}
                      className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                      <span className="has-text-link">
                        {isEditable ? (<i className="far fa-save" title="Save"/>) : (<i className="fas fa-pencil-alt" title="Edit"/>)}
                      </span>
                    </a>&nbsp;&nbsp;
                    {
                      isEditable ? <a onClick={()=> onCancel(row.index)}>
                              <span className="has-text-link">
                                <i className="fa fa-times" title="Cancel"/>
                              </span>
                      </a> : null
                    }
                  </div>
                </div>
                : null
              }
            </div>
          )
        }
      }
    ]
  } else if (isTransaction) {
    columns = [
      {
        Header: "Filename",
        id: "docFileName",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="field is-grouped-left">
              {!isDisabled || (tranAction.canEditDocument && tranAction.canClient) ?
                <DocUpload
                  onUploadSuccess={onUploadSuccess}
                  bucketName={bucketName}
                  docMeta={{
                    category: doc.docCategory,
                    subCategory: doc.docSubCategory,
                    type: doc.docType
                  }}
                  docId={doc.docAWSFileLocation}
                  versionMeta={{
                    uploadedBy: doc.createdUserName
                  }}
                  showFeedback
                  contextId={contextId}
                  contextType={contextType || "DEALS"}
                  tenantId={tenantId}
                /> : null
              }
              &nbsp;&nbsp;
              <DocLink docId={doc.docAWSFileLocation}/>
              {/* {!isDisabled && ( */}
              <DocModal
                onDeleteAll={onDeleteAll}
                documentId={doc._id}
                docMetaToShow={["category", "subCategory"]}
                versionMetaToShow={["uploadedBy"]}
                selectedDocId={doc.docAWSFileLocation}
                transaction={transaction}
                tranAction={tranAction}
                doc={doc}
                nav1={nav1}
                user={user}
              />
              {/* )} */}
            </div>
          )
        },
        sortMethod: (a, b) => a.docFileName.localeCompare(b.docFileName)
      },
      {
        id: "docCategory",
        Header: "Category",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="complain-details">
              {isEditable ? (
                <DocDropDownSelect
                  data={dropDown && dropDown.category || []}
                  value={doc.docCategory || ""}
                  textField="name"
                  valueField="id"
                  key="name"
                  onChange={value => onChangeUpdate("docCategory", doc, value, row.index)}
                />
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docCategory || "-", search)}}/>
              )}
              {errors && errors.docCategory && (<small className="text-error">{errors.docCategory}</small>)}
            </div>
          )
        },
        sortMethod: (a, b) =>
          a.docCategory.localeCompare(b.docCategory)
      },
      {
        id: "docSubCategory",
        Header: "Sub-category",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="complain-details">
              {isEditable ? (
                <DocDropDownSelect
                  data={(doc.docCategory && dropDown.multiSubCategory[doc.docCategory]) || []}
                  value={doc.docSubCategory || ""}
                  textField="name"
                  valueField="id"
                  key="name"
                  onChange={value => onChangeUpdate("docSubCategory", doc, value, row.index)}
                />
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docSubCategory || "-", search)}}/>
              )}
              {errors && errors.docSubCategory && (<small className="text-error">{errors.docSubCategory}</small>)}
            </div>
          )
        },
        sortMethod: (a, b) => a.docSubCategory.localeCompare(b.docSubCategory)
      },
      {
        id: "docType",
        Header: "Type",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {isEditable ? (
                <div className="complain-details">
                  <DocDropDownSelect
                    filter
                    data={dropDown && dropDown.docType || []}
                    value={doc.docType || ""}
                    textField="name"
                    valueField="id"
                    key="name"
                    onChange={value => onChangeUpdate("docType", doc, value, row.index)}
                  />
                  {errors.docType ? (<p className="text-error">{errors.docType}</p>) : null}
                </div>
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docType || "-", search)}}/>
              )}
            </div>
          )
        },
        sortMethod: (a, b) => a.docType.localeCompare(b.docType)
      },
      {
        id: "createdUserName",
        Header: "Created by",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.createdUserName || "-", search)}}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.createdUserName.localeCompare(b.createdUserName)
      },
      {
        id: "LastUpdatedDate",
        Header: "LUD",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{
                __html: highlightSearch(doc.LastUpdatedDate ? moment(doc.LastUpdatedDate).format("MM.DD.YYYY hh:mm A") : doc.lastUpdatedDate
                  ? moment(doc.lastUpdatedDate).format("MM.DD.YYYY hh:mm A") : "" || "-", search)
              }}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.LastUpdatedDate ? a.LastUpdatedDate.localeCompare(b.LastUpdatedDate) : a.lastUpdatedDate.localeCompare(b.lastUpdatedDate)
      },
      /* {
        id: "docNote",
        Header: "Notes",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docNote || "-", search)}}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.docNote.localeCompare(b.docNote)
      }, */
      {
        id: "docStatus",
        Header: "Status",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          const tranDisable = transaction[tranFirmId] !== user.entityId
          return (
            <SelectLabelInput
              list={dropDown.docStatus}
              name="docStatus"
              value={doc.docStatus}
              onChange={e => onStatusChange(e, doc)}
              disabled={isEditable || isDisabled || tranDisable || false }
            />
          )
        },
        sortMethod: (a, b) => a.docStatus.localeCompare(b.docStatus)
      },
      {
        id: "docAction",
        Header: "Action",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          const tranDisable = transaction[tranFirmId] === user.entityId ? false : user.userId !== doc.createdBy
          return (
            <div>
              {!isEditable ? (
                <div className="select is-small is-fullwidth is-link">
                  <select
                    name="docAction"
                    value=""
                    onChange={e => onFileAction(e.target.value, doc)}
                    disabled={!isDisabled || isEditable || !isDisabled ? tranDisable : true || false}
                  >
                    <option value="">Pick</option>
                    <option>See version history</option>
                    <option disabled>Send to EMMA</option>
                    <option>Share document</option>
                  </select>
                </div>
              ) : null}
            </div>
          )
        }
      },
      {
        id: "edit",
        Header: "Edit",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {!isDisabled ?
                <div className="field is-grouped">
                  {transaction[tranFirmId] === user.entityId || user.userId === doc.createdBy ?
                    <div className="control">
                      <a onClick={isEditable ? () => onUpdate(doc) : () => onEdit("Documents", row.index, doc )}
                        className={`${isSaveDisabled ? "isDisabled" : ""}`}
                      >
                        <span className="has-text-link">
                          {isEditable ? (<i className="far fa-save" title="Save"/>) : (
                            <i className="fas fa-pencil-alt" title="Edit"/>)}
                        </span>
                      </a>&nbsp;&nbsp;
                      {
                        isEditable ? <a onClick={()=> onCancel(row.index)}>
                              <span className="has-text-link">
                                <i className="fa fa-times" title="Cancel"/>
                              </span>
                        </a> : null
                      }
                    </div> : null
                  }
                  {/* {isTransaction && (transaction[tranFirmId] === user.entityId || user.userId === doc.createdBy) ?
                    <div className="control">
                      {doc.docStatus === "Public" ?
                        <a onClick={() => onSettingModal(doc)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                          <span className="has-text-link">
                            <i className="fas fa-cog"/>
                          </span>
                        </a>
                        : null
                      }
                    </div>
                    : null
                  } */}
                </div>
                : null
              }
            </div>
          )
        }
      }
    ]
  } else {
    columns = [
      {
        Header: "Filename",
        id: "docFileName",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="field is-grouped-left">
              {!isDisabled && (
                <DocUpload
                  onUploadSuccess={onUploadSuccess}
                  bucketName={bucketName}
                  docMeta={{
                    category: doc.docCategory,
                    subCategory: doc.docSubCategory,
                    type: doc.docType
                  }}
                  docId={doc.docAWSFileLocation}
                  versionMeta={{
                    uploadedBy: doc.createdUserName
                  }}
                  showFeedback
                  contextId={contextId}
                  contextType={contextType}
                  tenantId={tenantId}
                />
              )}
              &nbsp;&nbsp;
              <DocLink docId={doc.docAWSFileLocation} />
              {!isDisabled && (
                <DocModal
                  onDeleteAll={onDeleteAll}
                  documentId={doc._id}
                  docMetaToShow={["category","subCategory"]}
                  versionMetaToShow={["uploadedBy"]}
                  selectedDocId={doc.docAWSFileLocation}
                />
              )}
            </div>
          )
        },
        sortMethod: (a, b) => a.docFileName.localeCompare(b.docFileName)
      },
      {
        id: "docCategory",
        Header: "Category",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="complain-details">
              {isEditable ? (
                <DocDropDownSelect
                  data={dropDown && dropDown.category || []}
                  value={doc.docCategory || ""}
                  textField="name"
                  valueField="id"
                  key="name"
                  onChange={value => onChangeUpdate("docCategory", doc, value, row.index)}
                />
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docCategory || "-", search)}}/>
              )}
              {errors && errors.docCategory && (<small className="text-error">{errors.docCategory}</small>)}
            </div>
          )
        },
        sortMethod: (a, b) =>
          a.docCategory.localeCompare(b.docCategory)
      },
      {
        id: "docSubCategory",
        Header: "Sub-category",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div className="complain-details">
              {isEditable ? (
                <DocDropDownSelect
                  data={(doc.docCategory && dropDown.multiSubCategory[doc.docCategory]) || []}
                  value={doc.docSubCategory || ""}
                  textField="name"
                  valueField="id"
                  key="name"
                  onChange={value => onChangeUpdate("docSubCategory", doc, value, row.index)}
                />
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docSubCategory || "-", search)}}/>
              )}
              {errors && errors.docSubCategory && (<small className="text-error">{errors.docSubCategory}</small>)}
            </div>
          )
        },
        sortMethod: (a, b) => a.docSubCategory.localeCompare(b.docSubCategory)
      },
      {
        id: "docType",
        Header: "Type",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {isEditable ? (
                <div className="complain-details">
                  <DocDropDownSelect
                    filter
                    data={dropDown && dropDown.docType || []}
                    value={doc.docType || ""}
                    textField="name"
                    valueField="id"
                    key="name"
                    onChange={value => onChangeUpdate("docType", doc, value, row.index)}
                  />
                  {errors.docType ? (<p className="text-error">{errors.docType}</p>) : null}
                </div>
              ) : (
                <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docType || "-", search)}}/>
              )}
            </div>
          )
        },
        sortMethod: (a, b) => a.docType.localeCompare(b.docType)
      },
      {
        id: "createdUserName",
        Header: "Created by",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.createdUserName || "-", search)}}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.createdUserName.localeCompare(b.createdUserName)
      },
      {
        id: "LastUpdatedDate",
        Header: "LUD",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{
                __html: highlightSearch(doc.LastUpdatedDate ? moment(doc.LastUpdatedDate).format("MM.DD.YYYY hh:mm A") : doc.lastUpdatedDate
                  ? moment(doc.lastUpdatedDate).format("MM.DD.YYYY hh:mm A") : "" || "-", search)
              }}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.LastUpdatedDate ? a.LastUpdatedDate.localeCompare(b.LastUpdatedDate) : a.lastUpdatedDate.localeCompare(b.lastUpdatedDate)
      },
      /* {
        id: "docNote",
        Header: "Notes",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <small dangerouslySetInnerHTML={{__html: highlightSearch(doc.docNote || "-", search)}}/>
            </div>
          )
        },
        sortMethod: (a, b) => a.docNote.localeCompare(b.docNote)
      }, */
      {
        id: "docStatus",
        Header: "Status",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <SelectLabelInput
              list={dropDown.docStatus}
              name="docStatus"
              value={doc.docStatus}
              onChange={e => onStatusChange(e, doc)}
              disabled={isDisabled || isEditable || false}
            />
          )
        },
        sortMethod: (a, b) => a.docStatus.localeCompare(b.docStatus)
      },
      {
        id: "docAction",
        Header: "Action",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {!isEditable ? (
                <div className="select is-small is-fullwidth is-link">
                  <select
                    name="docAction"
                    value=""
                    onChange={e => onFileAction(e.target.value, doc)}
                    disabled={isDisabled || isEditable || false}
                  >
                    <option value="">Pick</option>
                    <option>See version history</option>
                    <option disabled>Send to EMMA</option>
                    <option>Share document</option>
                  </select>
                </div>
              ) : null}
            </div>
          )
        }
      },
      {
        id: "edit",
        Header: "Edit",
        accessor: item => item,
        Cell: row => {
          const doc = row.value
          isEditable = editableIndex === row.index
          return (
            <div>
              {!isDisabled ?
                <div className="field is-grouped">
                  <div className="control">
                    <a onClick={isEditable ? () => onUpdate(doc) : () => onEdit("Documents",row.index, doc)} className={`${isSaveDisabled ? "isDisabled" : ""}`}
                    >
                      <span className="has-text-link">
                        {isEditable ? (<i className="far fa-save" title="Save"/>) : (<i className="fas fa-pencil-alt" title="Edit"/>)}
                      </span>
                    </a>&nbsp;&nbsp;
                    {
                      isEditable ? <a onClick={()=> onCancel(row.index)}>
                              <span className="has-text-link">
                                <i className="fa fa-times" title="Cancel"/>
                              </span>
                      </a> : null
                    }
                  </div>
                </div>
                : null
              }
            </div>
          )
        }
      }
    ]
  }

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={documentsList}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default DocumentList
