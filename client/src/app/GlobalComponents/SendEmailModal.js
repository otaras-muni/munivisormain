import React from "react"
import { Multiselect } from "react-widgets"
import CKEditor from "react-ckeditor-component"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"
import {MultiSelect, TextLabelInput} from "./TextViewBox"

export const SendEmailModal  = (props) => {
  const toggleModal = () => {
    props.onModalChange({ modalState: !props.modalState })
  }

  const onChange = (e) => {
    let email = {}
    if(e.target.name === "category"){
      email = {
        ...props.email,
        sendEmailTo: [],
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }else {
      email = {
        ...props.email,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    }
    props.onModalChange({
      email
    })
  }

  const onEditorChange = (e, name) => {
    props.onModalChange( props.onModalChange({
      [name]: e.editor.getData()
    }, name))
  }

  const onSelect = (items) => {
    props.onModalChange({
      email: {
        ...props.email,
        sendEmailTo: items
      }
    })
  }

  // const isTransaction = ["deals", "rfp", "loan", "marfp", "derivative", "others"].indexOf(props.nav1) !== -1

  return(
    <Modal
      closeModal={toggleModal}
      modalState={props.modalState}
      title="Compliance Checks and Email Notification"
      saveModal={props.onSave}
      disabled={!(props.email && props.email.sendEmailTo && props.email.sendEmailTo.length) && props.email.category === "custom"}
    >
      <div className="columns">
        <div className="column">
          <div className="control">
            {!props.isNotTransaction && <label className="radio-button">Do you want to send an email to all participants ?
              <input type="radio" name="category" value="all" checked={props.email.category === "all" || false} onClick={onChange} onChange={onChange}/>
              <span className="checkmark" />
            </label> }
            {!props.isNotTransaction && <label className="radio-button">Do you want to send an email to your firm participants only ?
              <input type="radio" name="category" value="myfirm" checked={props.email.category === "myfirm" || false} onClick={onChange} onChange={onChange} />
              <span className="checkmark" />
            </label> }
            {!props.isNotTransaction && <label className="radio-button">Do you want to send emails to users from other participants ?
              <input type="radio" name="category" value="otherfirms" checked={props.email.category === "otherfirms" || false} onClick={onChange} onChange={onChange}/>
              <span className="checkmark" />
            </label> }
            {props.isNotTransaction && <label className="radio-button">Send Emails to me
              <input type="radio" name="category" value="onlyme" checked={props.email.category === "onlyme" || false} onClick={onChange} onChange={onChange}/>
              <span className="checkmark" />
            </label> }
            <label className="radio-button">Add additional recipients as needed ?
              <input type="radio" name="category" value="custom" checked={props.email.category === "custom" || false} onClick={onChange} onChange={onChange}/>
              <span className="checkmark" />
            </label>
            {!props.isNotTransaction && <label className="radio-button">None
              <input type="radio" name="category" value="none" checked={props.email.category === "none" || false} onClick={onChange} onChange={onChange}/>
              <span className="checkmark" />
            </label> }
            {
              props.documentFlag ?
                <label className="checkbox-button"> Send link(s) to email recipients to download documents
                  <input type="checkbox" name="sendDocEmailLinks" value="none" checked={props.email.sendDocEmailLinks || false} onClick={onChange} onChange={onChange} />
                  <span className="checkmark" />
                </label> : null
            }
          </div>
        </div>
      </div>
      {
        props.email.category === "custom" ?
          <div>
            <div className="columns multi-select">
              <MultiSelect filter label="Send email to" data={props.participants} value={props.email.sendEmailTo || []} onChange={(items) => onSelect(items)}  style={{width: "100%"}}/>
            </div>
            <div className="columns">
              <a className="has-text-link" style={{fontSize: 12,padding: "0 0 8px 11px",marginTop: -10}} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
              <i className="fa fa-refresh" style={{fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px"}} onClick={props.onParticipantsRefresh}/>
            </div>

          </div> : null
      }
      <div className="columns">
        <TextLabelInput label="Subject" name="subject" type="text" value={props.email.subject || ""} onChange={onChange}/>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Message</p>
          <div className="control">
            <CKEditor
              activeClass="p10"
              content={props.email.message}
              events={{
                /* "blur": this.onBlur,
                "afterPaste": this.afterPaste, */
                "change": (e) => onEditorChange(e, "message")
              }}
            />
          </div>
        </div>
        {/* <TextLabelInput label="Message" name="message" type="text" value={props.email.message || ""} onChange={onChange}/> */}
      </div>
    </Modal>
  )
}
export default SendEmailModal
