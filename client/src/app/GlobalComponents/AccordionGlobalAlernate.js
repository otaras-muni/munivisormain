import React, { Component } from "react"

import Combobox from "react-widgets/lib/Combobox"

const lkupdata = ["BANANA", "ORANGE", "APPLE"]

const data = {
  accordion1: [
    {
      fruit: "APPLE",
      date: "",
      applicable: true,
      platformConfig: true
    },
    {
      fruit: "ORANGE",
      date: "",
      applicable: true,
      platformConfig: true
    }
  ],
  accordion2: [
    {
      fruit: "BANANA",
      date: "",
      applicable: true,
      platformConfig: true
    },
    {
      fruit: "ORANGE",
      date: "",
      applicable: true,
      platformConfig: true
    }
  ]
}

export class AccordionGlobalAlt extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions: [], expanded: [] }
  }

  componentWillMount() {
    this.setState({ accordions: data })
  }

  handleClick(key, e) {
    e.stopPropagation()
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if(!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  };

  changeFruit(key, i, val) {
    // console.log(" key: ", key);
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], fruit: val}
      return { accordions }
    })
  }

  changeDate(key, i, val) {
    // console.log(" key: ", key);
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], date: val}
      return { accordions }
    })
  }

  changeApplicable(key, i, event) {
    // console.log(" key: ", key);
    // console.log(" i: ", i);
    const val = event.target.checked
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], applicable: val}
      return { accordions }
    })
  }

  addAcccordionItem(key) {
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key].push({
        fruit: "",
        date: "",
        applicable: true
      })
      return { accordions }
    })
  }

  resetAcccordion(key) {
    this.setState({ accordions: data })
  }

  renderNav(key) {
    return (
      <nav
        className="level"
        style={{
          backgroundColor: "lightblue",
          paddingLeft: "5px",
          paddingRight: "5px",
          marginBottom: "2px"
        }}
        role="presentation"
        onKeyPress={() =>{}}
        onClick={this.handleClick.bind(this, key)}
      >
        <div className="level-left">
          <div className="level-item">
            <p className="subtitle is-5">
              <strong>123</strong>
                                            posts
            </p>
          </div>
          <div className="level-item">
            <div className="field has-addons">
              <p className="control">
                <input className="input" type="text" placeholder="Find a post"/>
              </p>
              <p className="control">
                <button className="button">
                                                    Search
                </button>
              </p>
            </div>
          </div>
        </div>

        <div className="level-right">
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small" type="button"  onClick={this.addAcccordionItem.bind(this, key)}>
                                                Add
              </button>
            </div>
            <div className="control">
              <button className="button is-light is-small" type="button" onClick={this.resetAcccordion.bind(this, key)}>
                                                Reset
              </button>
            </div>
          </div>
        </div>
      </nav>
    )
  }

  renderAccordionBodyItem(e, key, i) {
    return (
      <tr key={key+i}>
        <td>
          <Combobox
            className="is-primary is-small"
            data={lkupdata}
            value={e.fruit}
            disabled={e.platformConfig}
            onChange={this.changeFruit.bind(this, key, i)}
          />
        </td>
        <td>
          <input
            className="input is-small is-link"
            type="datetime-local"
            value={e.date}
            onChange={this.changeDate.bind(this, key, i)}
          />
        </td>
        <td>
          <input
            className="checkbox"
            type="checkbox"
            checked={e.applicable}
            onChange={this.changeApplicable.bind(this, key, i)}
          />
        </td>
      </tr>
    )
  }

  renderAccordionBody(key, accordion) {
    return (
      <table>
        <tbody>
          {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
        </tbody>
      </table>
    )
  }

  renderAccordion(key, accordion) {
    return (
      <div key={key}>
        {this.renderNav(key)}
        {this.state.expanded.includes(key) ?
          this.renderAccordionBody(key, accordion)
          : undefined
        }
      </div>
    )
  }

  renderAccordions(data) {
    const { accordions } = this.state
    return Object.keys(accordions).map(a => this.renderAccordion(a, accordions[a]))
  }

  render() {
    return (
      <div>
        {this.renderAccordions(data)}
      </div>
    )
  }

}

/*
export class AccordionGlobalAlt extends Component {
  constructor(props) {
    super(props)
    this.state = { accordions: [], expanded: [] }
    this.changeDate = this.changeDate.bind(this)
    this.changeFruit = this.changeFruit.bind(this)
    this.changeApplicable = this.changeApplicable.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.addAcccordionItem = this.addAcccordionItem.bind(this)
    this.resetAcccordion = this.resetAcccordion.bind(this)
  }

  componentDidMount() {
    this.setState({ accordions: data })
  }

  handleClick= (key) => (e) => {
    e.stopPropagation()
    this.setState(prevState => {
      let expanded = [...prevState.expanded]
      if(!expanded.includes(key)) {
        expanded.push(key)
      } else {
        expanded = expanded.filter(k => k !== key)
      }
      return { expanded }
    })
  };

  changeFruit= (key, i) => (val) => {
    // console.log(" key: ", key);
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], fruit: val}
      return { accordions }
    })
  }

  changeDate= (key, i) => (val) => {
    // console.log(" key: ", key);
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], date: val}
      return { accordions }
    })
  }

  changeApplicable= (key, i) => (event) => {
    // console.log(" key: ", key);
    // console.log(" i: ", i);
    const val = event.target.checked
    // console.log(" val: ", val);
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key][i] = { ...accordions[key][i], applicable: val}
      return { accordions }
    })
  }

  // eslint-disable-next-line no-unused-expressions
  addAcccordionItem = (key) => {
    this.setState(prevState => {
      const accordions = { ...prevState.accordions }
      accordions[key] = [ ...accordions[key] ]
      accordions[key].push({
        fruit: "",
        date: "",
        applicable: true
      })
      return { accordions }
    })
  }

  // eslint-disable-next-line no-unused-vars
  resetAcccordion = (key) => {
    this.setState({ accordions: data })
  }

  renderNav(key) {
    return (
      <nav
        className="level"
        style={{
          backgroundColor: "lightblue",
          paddingLeft: "5px",
          paddingRight: "5px",
          marginBottom: "2px"
        }}
        role="presentation"
        onKeyPress={() =>{}}
        onClick={this.handleClick(key)}
      >
        <div className="level-left">
          <div className="level-item">
            <p className="subtitle is-5">
              <strong>123</strong>
                                          posts
            </p>
          </div>
          <div className="level-item">
            <div className="field has-addons">
              <p className="control">
                <input className="input" type="text" placeholder="Find a post"/>
              </p>
              <p className="control">
                <button className="button">
                                                  Search
                </button>
              </p>
            </div>
          </div>
        </div>

        <div className="level-right">
          <div className="field is-grouped">
            <div className="control">
              <button className="button is-link is-small" type="button"  onClick={this.addAcccordionItem(key)}>
                                              Add
              </button>
            </div>
            <div className="control">
              <button className="button is-link is-small" type="button" onClick={this.resetAcccordion(key)}>
                                              Reset
              </button>
            </div>
          </div>
        </div>
      </nav>
    )
  }

  renderAccordionBodyItem(e, key, i) {
    return (
      <tr key={key+i}>
        <td>
          <Combobox
            className="is-primary is-small"
            data={lkupdata}
            value={e.fruit}
            disabled={e.platformConfig}
            onChange={this.changeFruit(key, i)}
          />
        </td>
        <td>
          <input
            className="input is-small is-link"
            type="datetime-local"
            value={e.date}
            onChange={this.changeDate(key, i)}
          />
        </td>
        <td>
          <input
            className="checkbox"
            type="checkbox"
            checked={e.applicable}
            onChange={this.changeApplicable(key, i)}
          />
        </td>
      </tr>
    )
  }

  renderAccordionBody(key, accordion) {
    return (
      <table>
        <tbody>
          {accordion.map((e, i) => this.renderAccordionBodyItem(e, key, i))}
        </tbody>
      </table>
    )
  }

  renderAccordion(key, accordion) {
    return (
      <div key={key}>
        {this.renderNav(key)}
        {this.state.expanded.includes(key) ?
          this.renderAccordionBody(key, accordion)
          : undefined
        }
      </div>
    )
  }

  // eslint-disable-next-line no-unused-vars
  renderAccordions(data) {
    const { accordions } = this.state
    return Object.keys(accordions).map(a => this.renderAccordion(a, accordions[a]))
  }

  render() {
    return (
      <div>
        {this.renderAccordions(data)}
      </div>
    )
  }

}
*/


