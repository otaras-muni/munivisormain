import React, { Component } from "react"
import { connect } from "react-redux"
import ReactTable from "react-table"

import { getMessages } from "../../app/StateManagement/actions"

class Messages extends Component {
  constructor(props) {
    super(props)
    this.state = { selectedId: "", pageSizeOptions: [5, 10, 20, 25, 50], pageSize: 10 }
    this.goBack = this.goBack.bind(this)
    this.pollMessages = this.pollMessages.bind(this)
    this.onPageSizeChange = this.onPageSizeChange.bind(this)
  }

  componentDidMount() {
    this.props.getMessages(this.props.token)
    if (!this.interval) {
      this.interval = window.setInterval(this.pollMessages, 20000)
    }
  }

  componentWillUnmount() {
    console.log("clearing interval on unmount of nav")
    window.clearInterval(this.interval)
    this.interval = null
  }

  onPageSizeChange = (item) => {
    this.setState({
      pageSize: item
    })
  }

  pollMessages() {
    console.log("in pollMessages : ", !!this.props.token)
    const { token } = this.props
    if (token) {
      this.props.getMessages(token)
    }
  }

  selectMessage(selectedId) {
    this.setState({ selectedId })
  }

  goBack() {
    this.setState({ selectedId: "" })
  }

  renderSelectedMessage() {
    const message = this.props.messages.filter(e => e._id === this.state.selectedId)[0]
    if (message) {
      return <div>
        <button className="button is-link is-small" onClick={this.goBack}>Back</button>
        <div className="border-withul top-bottom-margin">
          <p>{message.description}</p>
        </div>
      </div>
    }
  }

  renderMessagesList() {
    const { pageSize, pageSizeOptions } = this.state
    const messages = [...this.props.messages] || []
    messages.sort((a, b) => (new Date(b.sentDate) - new Date(a.sentDate)))
    return (
      <ReactTable
        columns={[
          {
            Header: "Message",
            id: "_id",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <a onClick={this.selectMessage.bind(this, e.value._id)}>
                {`${e.value.description.substring(0, 20)}...`}</a></div>
          },
          {
            Header: "Date",
            id: "date",
            className: "multiExpTblVal",
            accessor: e => e.sentDate,
            Cell: e => <small className="hpTablesTd wrap-cell-text">{new Date(e.value).toLocaleString()}</small>
          }
        ]}
        data={messages}
        defaultPageSize={pageSize}
        onPageSizeChange={this.onPageSizeChange}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        pageSize={pageSize}
        minRows={1}
      />
    )
    return messages.map(e => (
      <p key={e._id}>
        <a onClick={this.selectMessage.bind(this, e._id)}>{e._id}</a>
      </p>
    ))
  }

  render() {
    const { selectedId } = this.state
    return (
      <div id="main">
        <div className="box">
          {selectedId ?
            this.renderSelectedMessage() :
            this.renderMessagesList()
          }
        </div>
      </div >
    )
  }
}

const mapStateToProps = ({ messages }) => ({ messages })

export default connect(mapStateToProps, { getMessages })(Messages)
