import Joi from "joi-browser"

const markedPublic = Joi.object().keys({
  publicFlag: Joi.boolean().required().optional(),
  publicDate: Joi.string().required().optional()
})

const sentToEmma = Joi.object().keys({
  emmaSendFlag: Joi.boolean().required().optional(),
  emmaSendDate: Joi.string().required().optional()
})

const docListSchema = Joi.object().keys({
  docCategory: Joi.string().allow("").required(),
  docSubCategory: Joi.string().allow("").required(),
  docType: Joi.string().allow("").required(),
  docAWSFileLocation: Joi.string().allow("").optional(),
  docFileName: Joi.string().required(),
  docNote: Joi.string().allow("").optional(),
  docStatus: Joi.string().allow("").optional(),
  markedPublic,
  sentToEmma,
  docAction: Joi.string().allow("").optional(),
  folderName: Joi.string().allow("").optional(),
  folderId: Joi.string().allow("").optional(),
  createdBy: Joi.string().required(),
  file: Joi.object().optional(),
  createdUserName: Joi.string().required(),
  _id: Joi.string().allow("").optional()
})

const DocDetailsDealsSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  documents: Joi.array().min(1).items(docListSchema).required()
})

export const DocumentsValidate = inputTransDetail =>
  Joi.validate(inputTransDetail, DocDetailsDealsSchema, {abortEarly: false, stripUnknown: false})
