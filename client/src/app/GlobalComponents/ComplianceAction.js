import React from "react"
import RatingSection from "./RatingSection"
import Accordion from "./Accordion"

const ComplianceAction = ({title, bodyTitle, notes}) => (
  <Accordion multiple
             activeItem={[0]}
             boxHidden
             render={({activeAccordions, onAccordion}) =>
               <div>
                 {
                   <RatingSection onAccordion={() => onAccordion(0)} title={title} style={{overflowY: "unset"}}>
                     {activeAccordions.includes(0) &&
                     <div className="columns">
                       <div className="column is-vcentered">
                         <p className="title innerPgTitle">
                           <strong>{bodyTitle || ""}</strong>
                         </p>
                         <br/>
                         {
                           notes && notes.length ? notes.map((note,i) => (
                             <div key={i}>
                               <p className="multiExpTblVal" ><strong>{note.title || ""}</strong></p>
                               {note.list && note.list.length ? note.list.map((n,j) => <p key={j} className="multiExpTblVal">{n}</p>) : null}
                               {i !== notes.length-1 && <hr />}
                             </div>
                           )) : null
                         }
                         <br/>
                         <p>
                           <a className="button is-warning is-small" >Go to the CAC</a>
                         </p>
                       </div>
                     </div>
                     }
                   </RatingSection>
                 }
               </div>
             }/>
)

export default ComplianceAction
