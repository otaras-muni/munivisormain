import React from "react"

const Disclaimer = () => (
  <div className="columns">
    <div className="column multiExpTblVal is-fullwidth">
      Disclaimer: This compliance resource does not mandate a municipal advisor to implement any specific practice described in
      this resource that extends beyond the requirements of existing applicable rules, and implementation
      of any specific practice described in this resource does not create a safe harbor or obviate
      the obligation of each municipal advisor to develop supervisory procedures that are reasonably
      designed to ensure that, in the conduct of its municipal advisory activities, the firm and its
      associated persons are in compliance with the applicable rules.
    </div>
  </div>
)

export default Disclaimer
