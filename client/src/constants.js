// export const ELASTIC_SEARCH_URL = "https://4lsWFk01i:bbdff276-fdda-4118-a0dc-c0bf120de054@scalr.api.appbase.io/"
// require("dotenv").config()

export const ELASTIC_SEARCH_URL = `${process.env.API_URL ||
  window.location.origin}/search/`
export const ELASTIC_SEARCH_INDEX = process.env.ES_INDEX
export const ELASTIC_SEARCH_CREDENTIALS = process.env.ES_CREDENTIALS
export const ELASTIC_SEARCH_TYPES = {
  DEALS: "tranagencydeals",
  RFPS: "tranagencyrfps",
  MARFPS: "actmarfps",
  BANK_LOAN: "tranbankloans",
  DERIVITIVE: "tranderivatives",
  OTHERS: "tranagencyothers",
  BUSINESSDEVELOPMENT: "actbusdevs",
  TASKS: "mvtasks"
}

export const DOCUMENT_TYPE = "doc_no_base64"

export const DATE_FORMAT = "DD.MM.YYYY"

export const FACET_TYPES = {
  DYNAMIC_RANGE_SLIDER: "DynamicRangeSlider",
  SINGLE_DATA_LIST: "SingleDataList",
  MULTI_DATA_LIST: "MultiDataList",
  DATE_RANGE: "DateRange"
}

export const ContextTypeForDocumentSearch = {
  DEALS: "Debt Issuance",
  ENTITYDOCS: "Clients / Prospects - Documents",
  RFPS: "Manage RFPs for Clients",
  BANKLOANS: "Debt Issue - Bank Loans",
  MARFPS: "MA RFPs",
  DERIVATIVES: "Derivatives",
  OTHERS: "Other Transactions",
  SVOBLIGATIONS: "Supervisory Compliance Obligations",
  SVPROFQUALIFICATIONS: "Compliance - Professional Qualifications",
  SVGENADMIN: "Compliance - General Administration",
  SVCLIENTCOMPLAINTS: "Compliance - Client Education & Research - Compliants",
  SVBUSINESSCONDUCT: "Compliance Business Conduct",
  POLICIESPROCEDURES: "Compliance - Policies & Procedures",
  SVMSRBSUBMISSIONS: "Compliance - MSRB Submissions",
  SVPOLICONTRELATEDDOCS: "Compliance - Political Contributions",
  SVGIFTSGRATUITIES: "Compliance - Gifts and Gratuities",
  BILLINGEXPENSERECEIPTS: "Billing & Exepenses Receipts",
  BUSDEV: "MA Business Development Activities",
  ENTITYCONTRACT: "Entity Contract",
  CONTROLS: "Controls",
  THIRDPARTYDOCS: "Third Party - Documents",
  GENERALDOCS: "General - Documents",
  USERDOCS: "User - Documents",
  TASKS: "Tasks - Documents",
  ENTITYLOGO: "Firm - Entity Logo",
  MIGRATIONTOOLS: "Firm - Migration and Onboarding Files",
  USERSDOCS: "User documents",
  SHAREDOCS: "Shared documents"
}

export const ORIGINAL_SEARCH_FIELD = [
  { field: "dealIssueUnderwriters.dealPartType", weight: 90 },
  { field: "dealIssueParticipants.dealPartFirmName", weight: 90 },
  { field: "dealIssueParticipants.dealPartContactName", weight: 90 },
  { field: "dealIssueParticipants.dealPartContactEmail", weight: 65 },
  { field: "dealIssueSeriesDetails.seriesRatings.longTermRating", weight: 60 },
  { field: "dealIssueSeriesDetails.seriesRatings.longTermOutlook", weight: 60 },
  {
    field: "dealIssueSeriesDetails.seriesRatings.shortTermOutlook",
    weight: 60
  },
  { field: "dealIssueSeriesDetails.seriesRatings.shortTermRating", weight: 60 },
  { field: "dealIssueSeriesDetails.seriesPricingData.term", weight: 65 },
  { field: "dealIssueSeriesDetails.seriesPricingData.amount", weight: 70 },
  { field: "dealIssueSeriesDetails.seriesPricingData.cusip", weight: 95 },
  { field: "dealIssueSeriesDetails.seriesName", weight: 95 },
  { field: "dealIssueSeriesDetails.description", weight: 95 },
  { field: "dealIssueTranClientFirmName", weight: 45 },
  { field: "dealIssueTranIssuerFirmName", weight: 100 },
  { field: "dealIssueTranIssuerMSRBType", weight: 90 },
  { field: "dealIssueTranName", weight: 95 },
  { field: "dealIssueTranType", weight: 90 },
  { field: "dealIssueTranState", weight: 85 },
  { field: "dealIssueTranPrimarySector", weight: 95 },
  { field: "dealIssueTranSecondarySector", weight: 95 },
  { field: "dealIssueTranStatus", weight: 95 },
  { field: "dealIssueTranSubType", weight: 95 },
  { field: "dealIssueTranIssueName", weight: 100 },
  { field: "dealIssueTranProjectDescription", weight: 95 },
  { field: "dealIssueofferingType", weight: 95 },
  { field: "bankLoanParticipants.partType", weight: 90 },
  { field: "bankLoanParticipants.partFirmName", weight: 90 },
  { field: "bankLoanParticipants.partContactName", weight: 85 },
  { field: "bankLoanLinkCusips.cusip", weight: 90 },
  { field: "bankLoanLinkCusips.description", weight: 90 },
  { field: "bankLoanDocuments.docCategory", weight: 80 },
  { field: "bankLoanDocuments.docSubCategory", weight: 80 },
  { field: "bankLoanDocuments.docType", weight: 80 },
  { field: "bankLoanDocuments.docFileName", weight: 80 },
  { field: "actTranType", weight: 95 },
  { field: "actTranSubType", weight: 95 },
  { field: "actTranClientName", weight: 100 },
  { field: "actTranPrimarySector", weight: 95 },
  { field: "actTranSecondarySector", weight: 95 },
  { field: "actTranFirmLeadAdvisorName", weight: 90 },
  { field: "actTranIssueName", weight: 100 },
  { field: "actTranProjectDescription", weight: 90 },
  { field: "relTranIssueName", weight: 85 },
  { field: "relTranProjectName", weight: 85 },
  { field: "relTranClientName", weight: 100 },
  { field: "bankLoanSummary.actTranStatus", weight: 90 },
  { field: "bankLoanSummary.actTranSecondarySector", weight: 85 },
  { field: "bankLoanSummary.actTranPrimarySector", weight: 85 },
  { field: "bankLoanSummary.actTranBorrowerName", weight: 80 },
  { field: "bankLoanSummary.actTranObligorName", weight: 80 },
  { field: "bankLoanSummary.actTranState", weight: 85 },
  { field: "bankLoanSummary.actTranCounty", weight: 85 },
  { field: "bankLoanSummary.actTranType", weight: 90 },
  { field: "derivativeParticipants.partType", weight: 90 },
  { field: "derivativeParticipants.partFirmName", weight: 90 },
  { field: "derivativeParticipants.partContactName", weight: 85 },
  { field: "derivativeCounterparties.cntrPartyFirmName", weight: 90 },
  { field: "derivativeCounterparties.cntrPartyFirmtype", weight: 90 },
  { field: "derivativeDocuments.docCategory", weight: 80 },
  { field: "derivativeDocuments.docSubCategory", weight: 80 },
  { field: "derivativeDocuments.docType", weight: 80 },
  { field: "derivativeDocuments.docFileName", weight: 80 },
  { field: "actTranType", weight: 95 },
  { field: "actTranSubType", weight: 95 },
  { field: "actTranClientName", weight: 100 },
  { field: "actTranPrimarySector", weight: 95 },
  { field: "actTranSecondarySector", weight: 95 },
  { field: "actTranFirmLeadAdvisorName", weight: 90 },
  { field: "actTranIssueName", weight: 90 },
  { field: "actTranProjectDescription", weight: 90 },
  { field: "actTranRelatedTo.relTranIssueName", weight: 85 },
  { field: "actTranRelatedTo.relTranProjectName", weight: 85 },
  { field: "actTranRelatedTo.relTranClientName", weight: 100 },
  { field: "derivativeSummary.tranProjectDescription", weight: 95 },
  { field: "derivativeSummary.tranType", weight: 95 },
  { field: "derivativeSummary.tranPrimarySector", weight: 95 },
  { field: "derivativeSummary.tranSecondaryType", weight: 95 },
  { field: "derivativeSummary.tranStatus", weight: 95 },
  { field: "derivativeSummary.tranBorrowerName", weight: 80 },
  { field: "derivativeSummary.tranGuarantorName", weight: 80 },
  { field: "actRelTrans.relTranIssueName", weight: 80 },
  { field: "actRelTrans.relTranProjectName", weight: 80 },
  { field: "actRelTrans.relTranClientName", weight: 85 },
  { field: "maRfpParticipants.partType", weight: 90 },
  { field: "maRfpParticipants.partFirmName", weight: 90 },
  { field: "maRfpParticipants.partContactName", weight: 85 },
  { field: "maRfpDocuments.docCategory", weight: 80 },
  { field: "maRfpDocuments.docSubCategory", weight: 80 },
  { field: "maRfpDocuments.docType", weight: 80 },
  { field: "maRfpDocuments.docFileName", weight: 80 },
  { field: "actType", weight: 95 },
  { field: "actSubType", weight: 95 },
  { field: "actStatus", weight: 95 },
  { field: "actLeadFinAdvClientEntityName", weight: 85 },
  { field: "actIssuerClientEntityName", weight: 100 },
  { field: "actPrimarySector", weight: 90 },
  { field: "actSecondarySector", weight: 90 },
  { field: "actOppType", weight: 80 },
  { field: "actOppStatus", weight: 80 },
  { field: "maRfpSummary.actState", weight: 85 },
  { field: "maRfpSummary.actCounty", weight: 80 },
  { field: "maRfpSummary.actPrimarySector", weight: 90 },
  { field: "maRfpSummary.actSecondarySector", weight: 90 },
  { field: "rfpParticipants.rfpParticipantFirmName", weight: 85 },
  { field: "rfpBidDocuments.rfpBidDocName", weight: 85 },
  { field: "rfpBidDocuments.rfpBidDocId", weight: 85 },
  { field: "rfpBidDocuments.rfpBidDocType", weight: 85 },
  { field: "rfpTranIssuerFirmName", weight: 100 },
  { field: "rfpTranName", weight: 95 },
  { field: "rfpTranType", weight: 93 },
  { field: "rfpTranRelatedTo.relTranIssueName", weight: 95 },
  { field: "rfpTranRelatedTo.relTranProjectName", weight: 95 },
  { field: "rfpTranState", weight: 90 },
  { field: "rfpTranPrimarySector", weight: 95 },
  { field: "rfpTranSecondarySector", weight: 95 },
  { field: "rfpTranFirmLeadAdvisorName", weight: 90 },
  { field: "rfpTranIssueName", weight: 95 },
  { field: "rfpTranProjectDescription", weight: 90 }
]

export const SEARCH_FIELDS = [
  { field: "dealIssueTranIssuerFirmName", weight: 100 },
  { field: "actTranClientName", weight: 100 },
  { field: "dealIssueTranIssueName", weight: 100 },
  { field: "dealIssueTranProjectDescription", weight: 95 },
  { field: "actTranIssueName", weight: 100 },
  { field: "actTranProjectDescription", weight: 90 },
  { field: "dealIssueTranType", weight: 90 },
  { field: "actTranType", weight: 95 },
  { field: "actTranSubType", weight: 95 },
  {
    field: "dealIssueSeriesDetails.seriesPricingDetails.dealSeriesRateType",
    weight: 90
  },
  { field: "bankLoanTerms.paymentType", weight: 90 },
  { field: "derivTradeClientPayLeg.paymentType", weight: 90 },
  { field: "derivTradeDealerPayLeg.paymentType", weight: 90 },
  { field: "dealIssueTranPrimarySector", weight: 95 },
  { field: "actTranPrimarySector", weight: 95 },
  { field: "dealIssueTranStatus", weight: 95 },
  { field: "bankLoanSummary.actTranStatus", weight: 90 },
  { field: "derivativeSummary.tranStatus", weight: 95 },
  { field: "rfpTranIssuerFirmName", weight: 100 },
  { field: "rfpTranIssueName", weight: 95 },
  { field: "rfpTranProjectDescription", weight: 90 },
  { field: "rfpTranType", weight: 93 },
  { field: "rfpTranPrimarySector", weight: 95 },
  { field: "rfpTranStatus", weight: 90 },
  { field: "actIssuerClientEntityName", weight: 100 },
  { field: "actProjectName", weight: 100 },
  { field: "actType", weight: 95 },
  { field: "actLeadFinAdvClientEntityName", weight: 91 },
  { field: "actPrimarySector", weight: 90 },
  { field: "actStatus", weight: 95 },
  { field: "actTranStatus", weight: 95 },
  { field: "actTranFirmName", weight: 100 }
]

export const getFieldsAndWeights = () => {
  const fields = SEARCH_FIELDS
  const ones = fields.map(() => 1)

  const searchFields = [
    ...fields.map(f => f.field),
    ...fields.map(f => `${f.field}.raw`),
    ...fields.map(f => `${f.field}.searchable`),
    ...fields.map(f => `${f.field}.autoSuggest`)
  ]

  const fieldWeights = [
    ...fields.map(f => (f.weight / 100) * 3),
    ...fields.map(f => f.weight),
    ...ones,
    ...ones
  ]

  return {
    searchFields,
    fieldWeights
  }
}
