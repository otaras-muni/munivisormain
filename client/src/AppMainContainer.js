import React from "react"
import { withRouter } from "react-router"
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom"
import { Provider } from "react-redux"
import { muniVisorApplicationStore } from "AppState/store"

import "react-toastify/dist/ReactToastify.css"
import {ToastContainer} from "react-toastify"
import { MAX_NAV_LEVELS } from "GlobalUtils/consts"
import { pdfDownload } from "GlobalUtils/pdfutils"
import NavMain from "./NavMain"

const getNavLevels = () => {
  const levels = []
  let path = ""
  for(let i = 1; i <= MAX_NAV_LEVELS; i++) {
    path += `/:nav${i}`
    levels.push(path)
  }
  return levels
}

const NAV_LEVLES = getNavLevels()

// console.log("NAV_LEVLES : ", NAV_LEVLES)

const getRouteComponent = props => {
  const navParams = props.match.params || {}
  const NavWithRouter = withRouter(NavMain)
  return (
    <div>
      <NavWithRouter { ...navParams } />
      <ToastContainer />
    </div>
  )
}

const RouteComponent = (props) => getRouteComponent(props)

export const AppMainContainer = () => (
  <Provider store={muniVisorApplicationStore}>
    <Router>
      <div>
        <Link to="/dashboard">
          <img className="main-logo" src="/images/munivisor.png" alt="MuniVisor" />
        </Link>
        <Switch>
          <Route
            exact
            path="/"
            component={RouteComponent}
          />
          {
            NAV_LEVLES.map(e => (
              <Route key={e}
                exact
                path={e}
                component={RouteComponent}
              />
            ))
          }
        </Switch>
      </div>
    </Router>
  </Provider>
)
